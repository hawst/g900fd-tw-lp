.class Lcom/amap/api/a/z$a;
.super Ljava/lang/Object;
.source "CustomGLOverlayLayer.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amap/api/a/z;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/amap/api/a/z;


# direct methods
.method constructor <init>(Lcom/amap/api/a/z;)V
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Lcom/amap/api/a/z$a;->a:Lcom/amap/api/a/z;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 31
    check-cast p1, Lcom/amap/api/a/aa;

    .line 32
    check-cast p2, Lcom/amap/api/a/aa;

    .line 34
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    .line 35
    :try_start_0
    invoke-virtual {p1}, Lcom/amap/api/a/aa;->getZIndex()I

    move-result v0

    invoke-virtual {p2}, Lcom/amap/api/a/aa;->getZIndex()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 36
    const/4 v0, 0x1

    .line 44
    :goto_0
    return v0

    .line 37
    :cond_0
    invoke-virtual {p1}, Lcom/amap/api/a/aa;->getZIndex()I

    move-result v0

    invoke-virtual {p2}, Lcom/amap/api/a/aa;->getZIndex()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-ge v0, v1, :cond_1

    .line 38
    const/4 v0, -0x1

    goto :goto_0

    .line 41
    :catch_0
    move-exception v0

    .line 42
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 44
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
