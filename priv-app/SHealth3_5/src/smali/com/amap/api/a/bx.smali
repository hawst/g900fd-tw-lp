.class Lcom/amap/api/a/bx;
.super Lcom/amap/api/a/bn;
.source "TileServerHandler.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/amap/api/a/bn",
        "<",
        "Ljava/util/ArrayList",
        "<",
        "Lcom/amap/api/a/m$a;",
        ">;",
        "Ljava/util/ArrayList",
        "<",
        "Lcom/amap/api/a/m$a;",
        ">;>;"
    }
.end annotation


# instance fields
.field private i:Lcom/amap/api/a/as;


# direct methods
.method public constructor <init>(Ljava/util/ArrayList;Ljava/net/Proxy;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/amap/api/a/m$a;",
            ">;",
            "Ljava/net/Proxy;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/amap/api/a/bn;-><init>(Ljava/lang/Object;Ljava/net/Proxy;Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/a/bx;->i:Lcom/amap/api/a/as;

    .line 42
    return-void
.end method

.method private a(Lcom/amap/api/a/m$a;I)V
    .locals 5

    .prologue
    .line 229
    if-eqz p1, :cond_0

    if-gez p2, :cond_1

    .line 257
    :cond_0
    :goto_0
    return-void

    .line 238
    :cond_1
    iget-object v0, p0, Lcom/amap/api/a/bx;->i:Lcom/amap/api/a/as;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/a/bx;->i:Lcom/amap/api/a/as;

    iget-object v0, v0, Lcom/amap/api/a/as;->o:Lcom/amap/api/a/bq;

    if-eqz v0, :cond_0

    .line 241
    iget-object v0, p0, Lcom/amap/api/a/bx;->i:Lcom/amap/api/a/as;

    iget-object v2, v0, Lcom/amap/api/a/as;->o:Lcom/amap/api/a/bq;

    .line 243
    invoke-virtual {v2}, Lcom/amap/api/a/bq;->size()I

    move-result v3

    .line 245
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_0

    .line 246
    invoke-virtual {v2, v1}, Lcom/amap/api/a/bq;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/a/m$a;

    .line 247
    if-nez v0, :cond_3

    .line 245
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 250
    :cond_3
    invoke-virtual {v0, p1}, Lcom/amap/api/a/m$a;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 252
    iput p2, v0, Lcom/amap/api/a/m$a;->g:I

    goto :goto_0
.end method

.method private a(Landroid/graphics/Bitmap;)[B
    .locals 3

    .prologue
    .line 223
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 224
    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v2, 0x64

    invoke-virtual {p1, v1, v2, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 225
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Ljava/io/InputStream;Lcom/amap/api/a/m$a;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v6, -0x1

    .line 171
    if-eqz p2, :cond_0

    if-nez p1, :cond_2

    :cond_0
    move v0, v6

    .line 209
    :cond_1
    :goto_0
    return v0

    .line 174
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 175
    iget v0, p2, Lcom/amap/api/a/m$a;->b:I

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 176
    const-string v0, "-"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 177
    iget v0, p2, Lcom/amap/api/a/m$a;->c:I

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 178
    const-string v0, "-"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 179
    iget v0, p2, Lcom/amap/api/a/m$a;->d:I

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 181
    iget-object v0, p0, Lcom/amap/api/a/bx;->i:Lcom/amap/api/a/as;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/amap/api/a/bx;->i:Lcom/amap/api/a/as;

    iget-object v0, v0, Lcom/amap/api/a/as;->m:Lcom/amap/api/a/p;

    if-nez v0, :cond_4

    :cond_3
    move v0, v6

    .line 182
    goto :goto_0

    .line 184
    :cond_4
    iget-object v0, p0, Lcom/amap/api/a/bx;->i:Lcom/amap/api/a/as;

    iget-object v0, v0, Lcom/amap/api/a/as;->m:Lcom/amap/api/a/p;

    const/4 v3, 0x0

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v2, p1

    move-object v4, v1

    invoke-virtual/range {v0 .. v5}, Lcom/amap/api/a/p;->a([BLjava/io/InputStream;ZLjava/util/List;Ljava/lang/String;)I

    move-result v0

    .line 194
    if-gez v0, :cond_5

    move v0, v6

    .line 195
    goto :goto_0

    .line 197
    :cond_5
    invoke-direct {p0, p2, v0}, Lcom/amap/api/a/bx;->a(Lcom/amap/api/a/m$a;I)V

    .line 198
    iget-object v1, p0, Lcom/amap/api/a/bx;->i:Lcom/amap/api/a/as;

    if-eqz v1, :cond_1

    .line 201
    iget-object v1, p0, Lcom/amap/api/a/bx;->i:Lcom/amap/api/a/as;

    iget-boolean v1, v1, Lcom/amap/api/a/as;->g:Z

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 202
    iget-object v1, p0, Lcom/amap/api/a/bx;->i:Lcom/amap/api/a/as;

    iget-object v1, v1, Lcom/amap/api/a/as;->m:Lcom/amap/api/a/p;

    invoke-virtual {v1, v0}, Lcom/amap/api/a/p;->a(I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/amap/api/a/bx;->a(Landroid/graphics/Bitmap;)[B

    move-result-object v1

    .line 204
    iget-object v2, p0, Lcom/amap/api/a/bx;->i:Lcom/amap/api/a/as;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/amap/api/a/bx;->i:Lcom/amap/api/a/as;

    iget-object v2, v2, Lcom/amap/api/a/as;->n:Lcom/amap/api/a/q;

    if-eqz v2, :cond_1

    .line 205
    iget-object v2, p0, Lcom/amap/api/a/bx;->i:Lcom/amap/api/a/as;

    iget-object v2, v2, Lcom/amap/api/a/as;->n:Lcom/amap/api/a/q;

    iget v3, p2, Lcom/amap/api/a/m$a;->b:I

    iget v4, p2, Lcom/amap/api/a/m$a;->c:I

    iget v5, p2, Lcom/amap/api/a/m$a;->d:I

    invoke-virtual {v2, v1, v3, v4, v5}, Lcom/amap/api/a/q;->a([BIII)Z

    goto :goto_0
.end method

.method protected synthetic a(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amap/api/maps2d/AMapException;
        }
    .end annotation

    .prologue
    .line 25
    invoke-virtual {p0, p1}, Lcom/amap/api/a/bx;->b(Ljava/io/InputStream;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/amap/api/a/as;)V
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lcom/amap/api/a/bx;->i:Lcom/amap/api/a/as;

    .line 52
    return-void
.end method

.method protected a()[B
    .locals 1

    .prologue
    .line 261
    const/4 v0, 0x0

    return-object v0
.end method

.method protected b()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 153
    iget-object v0, p0, Lcom/amap/api/a/bx;->i:Lcom/amap/api/a/as;

    iget-object v1, v0, Lcom/amap/api/a/as;->j:Lcom/amap/api/a/cd;

    iget-object v0, p0, Lcom/amap/api/a/bx;->b:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/a/m$a;

    iget v2, v0, Lcom/amap/api/a/m$a;->b:I

    iget-object v0, p0, Lcom/amap/api/a/bx;->b:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/a/m$a;

    iget v3, v0, Lcom/amap/api/a/m$a;->c:I

    iget-object v0, p0, Lcom/amap/api/a/bx;->b:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/a/m$a;

    iget v0, v0, Lcom/amap/api/a/m$a;->d:I

    invoke-virtual {v1, v2, v3, v0}, Lcom/amap/api/a/cd;->a(III)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected b(Ljava/io/InputStream;)Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/amap/api/a/m$a;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amap/api/maps2d/AMapException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 82
    iget-object v0, p0, Lcom/amap/api/a/bx;->b:Ljava/lang/Object;

    if-nez v0, :cond_1

    .line 118
    :cond_0
    :goto_0
    return-object v1

    .line 90
    :cond_1
    iget-object v0, p0, Lcom/amap/api/a/bx;->b:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 92
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_4

    .line 93
    iget-object v0, p0, Lcom/amap/api/a/bx;->b:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/a/m$a;

    .line 99
    invoke-virtual {p0, p1, v0}, Lcom/amap/api/a/bx;->a(Ljava/io/InputStream;Lcom/amap/api/a/m$a;)I

    move-result v4

    .line 100
    if-gez v4, :cond_3

    .line 101
    if-nez v1, :cond_2

    .line 102
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 104
    :cond_2
    new-instance v4, Lcom/amap/api/a/m$a;

    invoke-direct {v4, v0}, Lcom/amap/api/a/m$a;-><init>(Lcom/amap/api/a/m$a;)V

    .line 105
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    move-object v0, v1

    .line 92
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_1

    .line 109
    :cond_4
    if-eqz p1, :cond_0

    .line 111
    :try_start_0
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 113
    :catch_0
    move-exception v0

    .line 114
    new-instance v0, Lcom/amap/api/maps2d/AMapException;

    const-string v1, "IO \u64cd\u4f5c\u5f02\u5e38 - IOException"

    invoke-direct {v0, v1}, Lcom/amap/api/maps2d/AMapException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected synthetic e()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/amap/api/a/bx;->f()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method protected f()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/amap/api/a/m$a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 56
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 57
    iget-object v0, p0, Lcom/amap/api/a/bx;->b:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/a/m$a;

    .line 58
    new-instance v3, Lcom/amap/api/a/m$a;

    invoke-direct {v3, v0}, Lcom/amap/api/a/m$a;-><init>(Lcom/amap/api/a/m$a;)V

    .line 60
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 63
    :cond_0
    return-object v1
.end method
