.class abstract Lcom/amap/api/a/bg;
.super Ljava/lang/Object;
.source "MultiTouchGestureDetector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/amap/api/a/bg$1;,
        Lcom/amap/api/a/bg$a;,
        Lcom/amap/api/a/bg$b;
    }
.end annotation


# static fields
.field static b:Lcom/amap/api/a/b;

.field static k:F

.field private static q:Ljava/lang/reflect/Method;

.field private static r:Ljava/lang/reflect/Method;

.field private static s:Z

.field private static t:Z


# instance fields
.field a:Lcom/amap/api/a/bg$b;

.field c:I

.field d:Landroid/graphics/Matrix;

.field e:Landroid/graphics/Matrix;

.field f:Landroid/graphics/PointF;

.field g:Landroid/graphics/PointF;

.field h:Landroid/graphics/PointF;

.field i:F

.field j:F

.field l:Z

.field m:Z

.field n:Z

.field public o:I

.field public p:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 33
    const/high16 v0, 0x3f800000    # 1.0f

    sput v0, Lcom/amap/api/a/bg;->k:F

    .line 44
    sput-boolean v1, Lcom/amap/api/a/bg;->s:Z

    .line 45
    sput-boolean v1, Lcom/amap/api/a/bg;->t:Z

    return-void
.end method

.method constructor <init>()V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput v1, p0, Lcom/amap/api/a/bg;->c:I

    .line 26
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/amap/api/a/bg;->d:Landroid/graphics/Matrix;

    .line 27
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/amap/api/a/bg;->e:Landroid/graphics/Matrix;

    .line 28
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/amap/api/a/bg;->f:Landroid/graphics/PointF;

    .line 29
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/amap/api/a/bg;->g:Landroid/graphics/PointF;

    .line 30
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/amap/api/a/bg;->h:Landroid/graphics/PointF;

    .line 31
    iput v2, p0, Lcom/amap/api/a/bg;->i:F

    .line 32
    iput v2, p0, Lcom/amap/api/a/bg;->j:F

    .line 34
    iput-boolean v1, p0, Lcom/amap/api/a/bg;->l:Z

    .line 35
    iput-boolean v1, p0, Lcom/amap/api/a/bg;->m:Z

    .line 36
    iput-boolean v1, p0, Lcom/amap/api/a/bg;->n:Z

    .line 47
    iput v1, p0, Lcom/amap/api/a/bg;->o:I

    .line 48
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/amap/api/a/bg;->p:J

    .line 100
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/amap/api/a/bg$b;Lcom/amap/api/a/b;)Lcom/amap/api/a/bg;
    .locals 2

    .prologue
    .line 58
    .line 59
    new-instance v0, Lcom/amap/api/a/bg$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/amap/api/a/bg$a;-><init>(Lcom/amap/api/a/bg$1;)V

    .line 60
    iput-object p1, v0, Lcom/amap/api/a/bg;->a:Lcom/amap/api/a/bg$b;

    .line 61
    sput-object p2, Lcom/amap/api/a/bg;->b:Lcom/amap/api/a/b;

    .line 62
    return-object v0
.end method

.method static synthetic a()Z
    .locals 1

    .prologue
    .line 13
    sget-boolean v0, Lcom/amap/api/a/bg;->s:Z

    return v0
.end method

.method static synthetic b()Ljava/lang/reflect/Method;
    .locals 1

    .prologue
    .line 13
    sget-object v0, Lcom/amap/api/a/bg;->q:Ljava/lang/reflect/Method;

    return-object v0
.end method

.method static synthetic b(Landroid/view/MotionEvent;)V
    .locals 0

    .prologue
    .line 13
    invoke-static {p0}, Lcom/amap/api/a/bg;->c(Landroid/view/MotionEvent;)V

    return-void
.end method

.method static synthetic c()Ljava/lang/reflect/Method;
    .locals 1

    .prologue
    .line 13
    sget-object v0, Lcom/amap/api/a/bg;->r:Ljava/lang/reflect/Method;

    return-object v0
.end method

.method private static c(Landroid/view/MotionEvent;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 86
    sget-boolean v0, Lcom/amap/api/a/bg;->t:Z

    if-eqz v0, :cond_1

    .line 98
    :cond_0
    :goto_0
    return-void

    .line 88
    :cond_1
    sput-boolean v1, Lcom/amap/api/a/bg;->t:Z

    .line 90
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "getX"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/amap/api/a/bg;->q:Ljava/lang/reflect/Method;

    .line 92
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "getY"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/amap/api/a/bg;->r:Ljava/lang/reflect/Method;

    .line 94
    sget-object v0, Lcom/amap/api/a/bg;->q:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/amap/api/a/bg;->r:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_0

    .line 95
    const/4 v0, 0x1

    sput-boolean v0, Lcom/amap/api/a/bg;->s:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 96
    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public abstract a(Landroid/view/MotionEvent;)Z
.end method
