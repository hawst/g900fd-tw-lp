.class Lcom/amap/api/a/bj;
.super Ljava/lang/Object;
.source "PolygonDelegateImp.java"

# interfaces
.implements Lcom/amap/api/a/al;


# instance fields
.field private a:Lcom/amap/api/a/b;

.field private b:F

.field private c:Z

.field private d:Ljava/lang/String;

.field private e:F

.field private f:I

.field private g:I

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/autonavi/amap/mapcore2d/IPoint;",
            ">;"
        }
    .end annotation
.end field

.field private i:I

.field private j:I

.field private k:Lcom/amap/api/maps2d/model/LatLngBounds;


# direct methods
.method public constructor <init>(Lcom/amap/api/a/b;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput v0, p0, Lcom/amap/api/a/bj;->b:F

    .line 22
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/a/bj;->c:Z

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/a/bj;->h:Ljava/util/List;

    .line 28
    iput v1, p0, Lcom/amap/api/a/bj;->i:I

    iput v1, p0, Lcom/amap/api/a/bj;->j:I

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/a/bj;->k:Lcom/amap/api/maps2d/model/LatLngBounds;

    .line 32
    iput-object p1, p0, Lcom/amap/api/a/bj;->a:Lcom/amap/api/a/b;

    .line 34
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/a/bj;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/bj;->d:Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 38
    :goto_0
    return-void

    .line 35
    :catch_0
    move-exception v0

    .line 36
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public a(F)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 65
    iput p1, p0, Lcom/amap/api/a/bj;->b:F

    .line 66
    return-void
.end method

.method public a(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 229
    iput p1, p0, Lcom/amap/api/a/bj;->f:I

    .line 230
    return-void
.end method

.method public a(Landroid/graphics/Canvas;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 188
    iget-object v0, p0, Lcom/amap/api/a/bj;->h:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/a/bj;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 215
    :cond_0
    :goto_0
    return-void

    .line 191
    :cond_1
    new-instance v3, Landroid/graphics/Path;

    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    .line 192
    new-instance v1, Lcom/amap/api/a/ac;

    iget-object v0, p0, Lcom/amap/api/a/bj;->h:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/autonavi/amap/mapcore2d/IPoint;

    iget v4, v0, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    iget-object v0, p0, Lcom/amap/api/a/bj;->h:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/autonavi/amap/mapcore2d/IPoint;

    iget v0, v0, Lcom/autonavi/amap/mapcore2d/IPoint;->x:I

    invoke-direct {v1, v4, v0}, Lcom/amap/api/a/ac;-><init>(II)V

    .line 194
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 195
    iget-object v4, p0, Lcom/amap/api/a/bj;->a:Lcom/amap/api/a/b;

    invoke-virtual {v4}, Lcom/amap/api/a/b;->s()Lcom/amap/api/a/bl;

    move-result-object v4

    invoke-interface {v4, v1, v0}, Lcom/amap/api/a/bl;->a(Lcom/amap/api/a/ac;Landroid/graphics/Point;)Landroid/graphics/Point;

    move-result-object v0

    .line 197
    iget v1, v0, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    invoke-virtual {v3, v1, v0}, Landroid/graphics/Path;->moveTo(FF)V

    move v1, v2

    .line 198
    :goto_1
    iget-object v0, p0, Lcom/amap/api/a/bj;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 199
    new-instance v4, Lcom/amap/api/a/ac;

    iget-object v0, p0, Lcom/amap/api/a/bj;->h:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/autonavi/amap/mapcore2d/IPoint;

    iget v5, v0, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    iget-object v0, p0, Lcom/amap/api/a/bj;->h:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/autonavi/amap/mapcore2d/IPoint;

    iget v0, v0, Lcom/autonavi/amap/mapcore2d/IPoint;->x:I

    invoke-direct {v4, v5, v0}, Lcom/amap/api/a/ac;-><init>(II)V

    .line 201
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 202
    iget-object v5, p0, Lcom/amap/api/a/bj;->a:Lcom/amap/api/a/b;

    invoke-virtual {v5}, Lcom/amap/api/a/b;->s()Lcom/amap/api/a/bl;

    move-result-object v5

    invoke-interface {v5, v4, v0}, Lcom/amap/api/a/bl;->a(Lcom/amap/api/a/ac;Landroid/graphics/Point;)Landroid/graphics/Point;

    move-result-object v0

    .line 203
    iget v4, v0, Landroid/graphics/Point;->x:I

    int-to-float v4, v4

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    invoke-virtual {v3, v4, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 198
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 205
    :cond_2
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 206
    invoke-virtual {p0}, Lcom/amap/api/a/bj;->h()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 207
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 208
    invoke-virtual {v3}, Landroid/graphics/Path;->close()V

    .line 209
    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 210
    invoke-virtual {p1, v3, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 211
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 212
    invoke-virtual {p0}, Lcom/amap/api/a/bj;->j()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 213
    invoke-virtual {p0}, Lcom/amap/api/a/bj;->g()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 214
    invoke-virtual {p1, v3, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto/16 :goto_0
.end method

.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/maps2d/model/LatLng;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 55
    invoke-virtual {p0, p1}, Lcom/amap/api/a/bj;->b(Ljava/util/List;)V

    .line 56
    return-void
.end method

.method public a(Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 75
    iput-boolean p1, p0, Lcom/amap/api/a/bj;->c:Z

    .line 76
    return-void
.end method

.method public a()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 174
    iget-object v2, p0, Lcom/amap/api/a/bj;->k:Lcom/amap/api/maps2d/model/LatLngBounds;

    if-nez v2, :cond_1

    .line 181
    :cond_0
    :goto_0
    return v0

    .line 177
    :cond_1
    iget-object v2, p0, Lcom/amap/api/a/bj;->a:Lcom/amap/api/a/b;

    invoke-virtual {v2}, Lcom/amap/api/a/b;->x()Lcom/amap/api/maps2d/model/LatLngBounds;

    move-result-object v2

    .line 178
    if-nez v2, :cond_2

    move v0, v1

    .line 179
    goto :goto_0

    .line 181
    :cond_2
    iget-object v3, p0, Lcom/amap/api/a/bj;->k:Lcom/amap/api/maps2d/model/LatLngBounds;

    invoke-virtual {v3, v2}, Lcom/amap/api/maps2d/model/LatLngBounds;->contains(Lcom/amap/api/maps2d/model/LatLngBounds;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/amap/api/a/bj;->k:Lcom/amap/api/maps2d/model/LatLngBounds;

    invoke-virtual {v3, v2}, Lcom/amap/api/maps2d/model/LatLngBounds;->intersects(Lcom/amap/api/maps2d/model/LatLngBounds;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public a(Lcom/amap/api/a/ak;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 86
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Lcom/amap/api/a/ak;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/amap/api/a/bj;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 88
    :cond_0
    const/4 v0, 0x1

    .line 90
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/amap/api/maps2d/model/LatLng;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 273
    invoke-virtual {p0}, Lcom/amap/api/a/bj;->i()Ljava/util/List;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/amap/api/a/a/p;->a(Lcom/amap/api/maps2d/model/LatLng;Ljava/util/List;)Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 42
    iget-object v0, p0, Lcom/amap/api/a/bj;->a:Lcom/amap/api/a/b;

    invoke-virtual {p0}, Lcom/amap/api/a/bj;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/a/b;->a(Ljava/lang/String;)Z

    .line 43
    return-void
.end method

.method public b(F)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 219
    iput p1, p0, Lcom/amap/api/a/bj;->e:F

    .line 220
    return-void
.end method

.method public b(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 239
    iput p1, p0, Lcom/amap/api/a/bj;->g:I

    .line 240
    return-void
.end method

.method b(Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/maps2d/model/LatLng;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 94
    .line 95
    invoke-static {}, Lcom/amap/api/maps2d/model/LatLngBounds;->builder()Lcom/amap/api/maps2d/model/LatLngBounds$Builder;

    move-result-object v7

    .line 96
    iget-object v0, p0, Lcom/amap/api/a/bj;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 97
    if-eqz p1, :cond_2

    .line 98
    const/4 v6, 0x0

    .line 99
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move-object v1, v6

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/amap/api/maps2d/model/LatLng;

    .line 100
    invoke-virtual {v6, v1}, Lcom/amap/api/maps2d/model/LatLng;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 102
    new-instance v5, Lcom/autonavi/amap/mapcore2d/IPoint;

    invoke-direct {v5}, Lcom/autonavi/amap/mapcore2d/IPoint;-><init>()V

    .line 103
    iget-object v0, p0, Lcom/amap/api/a/bj;->a:Lcom/amap/api/a/b;

    iget-wide v1, v6, Lcom/amap/api/maps2d/model/LatLng;->latitude:D

    iget-wide v3, v6, Lcom/amap/api/maps2d/model/LatLng;->longitude:D

    invoke-virtual/range {v0 .. v5}, Lcom/amap/api/a/b;->a(DDLcom/autonavi/amap/mapcore2d/IPoint;)V

    .line 104
    iget-object v0, p0, Lcom/amap/api/a/bj;->h:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 105
    invoke-virtual {v7, v6}, Lcom/amap/api/maps2d/model/LatLngBounds$Builder;->include(Lcom/amap/api/maps2d/model/LatLng;)Lcom/amap/api/maps2d/model/LatLngBounds$Builder;

    move-object v1, v6

    .line 107
    goto :goto_0

    .line 108
    :cond_1
    iget-object v0, p0, Lcom/amap/api/a/bj;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 109
    const/4 v0, 0x1

    if-le v2, v0, :cond_2

    .line 110
    iget-object v0, p0, Lcom/amap/api/a/bj;->h:Ljava/util/List;

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/autonavi/amap/mapcore2d/IPoint;

    .line 111
    iget-object v1, p0, Lcom/amap/api/a/bj;->h:Ljava/util/List;

    add-int/lit8 v3, v2, -0x1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/autonavi/amap/mapcore2d/IPoint;

    .line 112
    iget v3, v0, Lcom/autonavi/amap/mapcore2d/IPoint;->x:I

    iget v4, v1, Lcom/autonavi/amap/mapcore2d/IPoint;->x:I

    if-ne v3, v4, :cond_2

    iget v0, v0, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    iget v1, v1, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    if-ne v0, v1, :cond_2

    .line 113
    iget-object v0, p0, Lcom/amap/api/a/bj;->h:Ljava/util/List;

    add-int/lit8 v1, v2, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 117
    :cond_2
    invoke-virtual {v7}, Lcom/amap/api/maps2d/model/LatLngBounds$Builder;->build()Lcom/amap/api/maps2d/model/LatLngBounds;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/bj;->k:Lcom/amap/api/maps2d/model/LatLngBounds;

    .line 118
    iput v9, p0, Lcom/amap/api/a/bj;->i:I

    .line 119
    iput v9, p0, Lcom/amap/api/a/bj;->j:I

    .line 120
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lcom/amap/api/a/bj;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 48
    const-string v0, "Polygon"

    invoke-static {v0}, Lcom/amap/api/a/ab;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/bj;->d:Ljava/lang/String;

    .line 50
    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/bj;->d:Ljava/lang/String;

    return-object v0
.end method

.method public d()F
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 70
    iget v0, p0, Lcom/amap/api/a/bj;->b:F

    return v0
.end method

.method public e()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/amap/api/a/bj;->c:Z

    return v0
.end method

.method public f()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 139
    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public g()F
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 224
    iget v0, p0, Lcom/amap/api/a/bj;->e:F

    return v0
.end method

.method public h()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 234
    iget v0, p0, Lcom/amap/api/a/bj;->f:I

    return v0
.end method

.method public i()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/maps2d/model/LatLng;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/amap/api/a/bj;->k()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public j()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 244
    iget v0, p0, Lcom/amap/api/a/bj;->g:I

    return v0
.end method

.method k()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/maps2d/model/LatLng;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 123
    iget-object v0, p0, Lcom/amap/api/a/bj;->h:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 124
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 125
    iget-object v0, p0, Lcom/amap/api/a/bj;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/autonavi/amap/mapcore2d/IPoint;

    .line 126
    if-eqz v0, :cond_0

    .line 127
    new-instance v3, Lcom/autonavi/amap/mapcore2d/DPoint;

    invoke-direct {v3}, Lcom/autonavi/amap/mapcore2d/DPoint;-><init>()V

    .line 128
    iget-object v4, p0, Lcom/amap/api/a/bj;->a:Lcom/amap/api/a/b;

    iget v5, v0, Lcom/autonavi/amap/mapcore2d/IPoint;->x:I

    iget v0, v0, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    invoke-virtual {v4, v5, v0, v3}, Lcom/amap/api/a/b;->b(IILcom/autonavi/amap/mapcore2d/DPoint;)V

    .line 129
    new-instance v0, Lcom/amap/api/maps2d/model/LatLng;

    iget-wide v4, v3, Lcom/autonavi/amap/mapcore2d/DPoint;->y:D

    iget-wide v6, v3, Lcom/autonavi/amap/mapcore2d/DPoint;->x:D

    invoke-direct {v0, v4, v5, v6, v7}, Lcom/amap/api/maps2d/model/LatLng;-><init>(DD)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 134
    :goto_1
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public m()V
    .locals 0

    .prologue
    .line 269
    return-void
.end method
