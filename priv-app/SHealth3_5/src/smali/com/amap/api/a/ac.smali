.class public Lcom/amap/api/a/ac;
.super Ljava/lang/Object;
.source "GeoPoint.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final a:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/amap/api/a/ac;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:J

.field private c:J

.field private d:D

.field private e:D


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 145
    new-instance v0, Lcom/amap/api/a/ad;

    invoke-direct {v0}, Lcom/amap/api/a/ad;-><init>()V

    sput-object v0, Lcom/amap/api/a/ac;->a:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const-wide/high16 v2, -0x8000000000000000L

    const-wide/16 v0, 0x1

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    iput-wide v2, p0, Lcom/amap/api/a/ac;->b:J

    .line 9
    iput-wide v2, p0, Lcom/amap/api/a/ac;->c:J

    .line 10
    iput-wide v0, p0, Lcom/amap/api/a/ac;->d:D

    .line 11
    iput-wide v0, p0, Lcom/amap/api/a/ac;->e:D

    .line 14
    iput-wide v4, p0, Lcom/amap/api/a/ac;->b:J

    .line 15
    iput-wide v4, p0, Lcom/amap/api/a/ac;->c:J

    .line 16
    return-void
.end method

.method public constructor <init>(DDJJ)V
    .locals 4

    .prologue
    const-wide/high16 v2, -0x8000000000000000L

    const-wide/16 v0, 0x1

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    iput-wide v2, p0, Lcom/amap/api/a/ac;->b:J

    .line 9
    iput-wide v2, p0, Lcom/amap/api/a/ac;->c:J

    .line 10
    iput-wide v0, p0, Lcom/amap/api/a/ac;->d:D

    .line 11
    iput-wide v0, p0, Lcom/amap/api/a/ac;->e:D

    .line 47
    iput-wide p1, p0, Lcom/amap/api/a/ac;->d:D

    .line 48
    iput-wide p3, p0, Lcom/amap/api/a/ac;->e:D

    .line 49
    iput-wide p5, p0, Lcom/amap/api/a/ac;->b:J

    .line 50
    iput-wide p7, p0, Lcom/amap/api/a/ac;->c:J

    .line 51
    return-void
.end method

.method public constructor <init>(DDZ)V
    .locals 6

    .prologue
    const-wide/high16 v4, -0x8000000000000000L

    const-wide v2, 0x412e848000000000L    # 1000000.0

    const-wide/16 v0, 0x1

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    iput-wide v4, p0, Lcom/amap/api/a/ac;->b:J

    .line 9
    iput-wide v4, p0, Lcom/amap/api/a/ac;->c:J

    .line 10
    iput-wide v0, p0, Lcom/amap/api/a/ac;->d:D

    .line 11
    iput-wide v0, p0, Lcom/amap/api/a/ac;->e:D

    .line 26
    const/4 v0, 0x1

    if-ne p5, v0, :cond_0

    .line 27
    mul-double v0, p1, v2

    double-to-long v0, v0

    iput-wide v0, p0, Lcom/amap/api/a/ac;->b:J

    .line 28
    mul-double v0, p3, v2

    double-to-long v0, v0

    iput-wide v0, p0, Lcom/amap/api/a/ac;->c:J

    .line 33
    :goto_0
    return-void

    .line 30
    :cond_0
    iput-wide p1, p0, Lcom/amap/api/a/ac;->d:D

    .line 31
    iput-wide p3, p0, Lcom/amap/api/a/ac;->e:D

    goto :goto_0
.end method

.method public constructor <init>(II)V
    .locals 4

    .prologue
    const-wide/high16 v2, -0x8000000000000000L

    const-wide/16 v0, 0x1

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    iput-wide v2, p0, Lcom/amap/api/a/ac;->b:J

    .line 9
    iput-wide v2, p0, Lcom/amap/api/a/ac;->c:J

    .line 10
    iput-wide v0, p0, Lcom/amap/api/a/ac;->d:D

    .line 11
    iput-wide v0, p0, Lcom/amap/api/a/ac;->e:D

    .line 20
    int-to-long v0, p1

    iput-wide v0, p0, Lcom/amap/api/a/ac;->b:J

    .line 21
    int-to-long v0, p2

    iput-wide v0, p0, Lcom/amap/api/a/ac;->c:J

    .line 22
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const-wide/high16 v2, -0x8000000000000000L

    const-wide/16 v0, 0x1

    .line 140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    iput-wide v2, p0, Lcom/amap/api/a/ac;->b:J

    .line 9
    iput-wide v2, p0, Lcom/amap/api/a/ac;->c:J

    .line 10
    iput-wide v0, p0, Lcom/amap/api/a/ac;->d:D

    .line 11
    iput-wide v0, p0, Lcom/amap/api/a/ac;->e:D

    .line 141
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/amap/api/a/ac;->b:J

    .line 142
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/amap/api/a/ac;->c:J

    .line 143
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/amap/api/a/ad;)V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0, p1}, Lcom/amap/api/a/ac;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 2

    .prologue
    .line 90
    iget-wide v0, p0, Lcom/amap/api/a/ac;->c:J

    long-to-int v0, v0

    return v0
.end method

.method public a(D)V
    .locals 0

    .prologue
    .line 37
    iput-wide p1, p0, Lcom/amap/api/a/ac;->e:D

    .line 38
    return-void
.end method

.method public b()I
    .locals 2

    .prologue
    .line 95
    iget-wide v0, p0, Lcom/amap/api/a/ac;->b:J

    long-to-int v0, v0

    return v0
.end method

.method public b(D)V
    .locals 0

    .prologue
    .line 42
    iput-wide p1, p0, Lcom/amap/api/a/ac;->d:D

    .line 43
    return-void
.end method

.method public c()J
    .locals 2

    .prologue
    .line 99
    iget-wide v0, p0, Lcom/amap/api/a/ac;->c:J

    return-wide v0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 104
    iget-wide v0, p0, Lcom/amap/api/a/ac;->b:J

    return-wide v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 157
    const/4 v0, 0x0

    return v0
.end method

.method public e()D
    .locals 4

    .prologue
    .line 108
    iget-wide v0, p0, Lcom/amap/api/a/ac;->e:D

    invoke-static {v0, v1}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v0

    const-wide/16 v2, 0x1

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 110
    iget-wide v0, p0, Lcom/amap/api/a/ac;->c:J

    invoke-static {v0, v1}, Lcom/amap/api/a/y;->a(J)D

    move-result-wide v0

    .line 111
    const-wide v2, 0x41731bf84570a3d7L    # 2.003750834E7

    mul-double/2addr v0, v2

    const-wide v2, 0x4066800000000000L    # 180.0

    div-double/2addr v0, v2

    .line 112
    iput-wide v0, p0, Lcom/amap/api/a/ac;->e:D

    .line 114
    :cond_0
    iget-wide v0, p0, Lcom/amap/api/a/ac;->e:D

    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 55
    if-ne p0, p1, :cond_1

    .line 72
    :cond_0
    :goto_0
    return v0

    .line 57
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 58
    goto :goto_0

    .line 59
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 60
    goto :goto_0

    .line 61
    :cond_3
    check-cast p1, Lcom/amap/api/a/ac;

    .line 62
    iget-wide v2, p0, Lcom/amap/api/a/ac;->b:J

    iget-wide v4, p1, Lcom/amap/api/a/ac;->b:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    move v0, v1

    .line 63
    goto :goto_0

    .line 64
    :cond_4
    iget-wide v2, p0, Lcom/amap/api/a/ac;->c:J

    iget-wide v4, p1, Lcom/amap/api/a/ac;->c:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_5

    move v0, v1

    .line 65
    goto :goto_0

    .line 66
    :cond_5
    iget-wide v2, p0, Lcom/amap/api/a/ac;->d:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    iget-wide v4, p1, Lcom/amap/api/a/ac;->d:D

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-eqz v2, :cond_6

    move v0, v1

    .line 68
    goto :goto_0

    .line 69
    :cond_6
    iget-wide v2, p0, Lcom/amap/api/a/ac;->e:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    iget-wide v4, p1, Lcom/amap/api/a/ac;->e:D

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    .line 71
    goto :goto_0
.end method

.method public f()D
    .locals 4

    .prologue
    .line 118
    iget-wide v0, p0, Lcom/amap/api/a/ac;->d:D

    invoke-static {v0, v1}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v0

    const-wide/16 v2, 0x1

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 120
    iget-wide v0, p0, Lcom/amap/api/a/ac;->b:J

    invoke-static {v0, v1}, Lcom/amap/api/a/y;->a(J)D

    move-result-wide v0

    .line 121
    const-wide v2, 0x4056800000000000L    # 90.0

    add-double/2addr v0, v2

    const-wide v2, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v0, v2

    const-wide v2, 0x4076800000000000L    # 360.0

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->tan(D)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    const-wide v2, 0x3f91df46a2529d39L    # 0.017453292519943295

    div-double/2addr v0, v2

    .line 124
    const-wide v2, 0x41731bf84570a3d7L    # 2.003750834E7

    mul-double/2addr v0, v2

    const-wide v2, 0x4066800000000000L    # 180.0

    div-double/2addr v0, v2

    .line 125
    iput-wide v0, p0, Lcom/amap/api/a/ac;->d:D

    .line 127
    :cond_0
    iget-wide v0, p0, Lcom/amap/api/a/ac;->d:D

    return-wide v0
.end method

.method public g()Lcom/amap/api/a/ac;
    .locals 9

    .prologue
    .line 131
    new-instance v0, Lcom/amap/api/a/ac;

    iget-wide v1, p0, Lcom/amap/api/a/ac;->d:D

    iget-wide v3, p0, Lcom/amap/api/a/ac;->e:D

    iget-wide v5, p0, Lcom/amap/api/a/ac;->b:J

    iget-wide v7, p0, Lcom/amap/api/a/ac;->c:J

    invoke-direct/range {v0 .. v8}, Lcom/amap/api/a/ac;-><init>(DDJJ)V

    return-object v0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    const/16 v5, 0x20

    .line 77
    .line 79
    iget-wide v0, p0, Lcom/amap/api/a/ac;->b:J

    iget-wide v2, p0, Lcom/amap/api/a/ac;->b:J

    ushr-long/2addr v2, v5

    xor-long/2addr v0, v2

    long-to-int v0, v0

    add-int/lit8 v0, v0, 0x1f

    .line 80
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/amap/api/a/ac;->c:J

    iget-wide v3, p0, Lcom/amap/api/a/ac;->c:J

    ushr-long/2addr v3, v5

    xor-long/2addr v1, v3

    long-to-int v1, v1

    add-int/2addr v0, v1

    .line 82
    iget-wide v1, p0, Lcom/amap/api/a/ac;->d:D

    invoke-static {v1, v2}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v1

    .line 83
    mul-int/lit8 v0, v0, 0x1f

    ushr-long v3, v1, v5

    xor-long/2addr v1, v3

    long-to-int v1, v1

    add-int/2addr v0, v1

    .line 84
    iget-wide v1, p0, Lcom/amap/api/a/ac;->e:D

    invoke-static {v1, v2}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v1

    .line 85
    mul-int/lit8 v0, v0, 0x1f

    ushr-long v3, v1, v5

    xor-long/2addr v1, v3

    long-to-int v1, v1

    add-int/2addr v0, v1

    .line 86
    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 162
    iget-wide v0, p0, Lcom/amap/api/a/ac;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 163
    iget-wide v0, p0, Lcom/amap/api/a/ac;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 164
    return-void
.end method
