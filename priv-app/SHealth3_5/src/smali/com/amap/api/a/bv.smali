.class public Lcom/amap/api/a/bv;
.super Landroid/view/View;
.source "TileOverlayView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/amap/api/a/bv$a;
    }
.end annotation


# instance fields
.field a:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/amap/api/a/ap;",
            ">;"
        }
    .end annotation
.end field

.field b:Lcom/amap/api/a/bv$a;

.field c:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/amap/api/a/af;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/amap/api/a/af;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 12
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/a/bv;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 13
    new-instance v0, Lcom/amap/api/a/bv$a;

    invoke-direct {v0, p0}, Lcom/amap/api/a/bv$a;-><init>(Lcom/amap/api/a/bv;)V

    iput-object v0, p0, Lcom/amap/api/a/bv;->b:Lcom/amap/api/a/bv$a;

    .line 14
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/a/bv;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 38
    iput-object p2, p0, Lcom/amap/api/a/bv;->d:Lcom/amap/api/a/af;

    .line 39
    return-void
.end method


# virtual methods
.method a()Lcom/amap/api/a/af;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/amap/api/a/bv;->d:Lcom/amap/api/a/af;

    return-object v0
.end method

.method protected a(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 46
    iget-object v0, p0, Lcom/amap/api/a/bv;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/a/ap;

    .line 47
    invoke-interface {v0}, Lcom/amap/api/a/ap;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 48
    invoke-interface {v0, p1}, Lcom/amap/api/a/ap;->a(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 51
    :cond_1
    return-void
.end method

.method public a(Lcom/amap/api/a/ap;)V
    .locals 1

    .prologue
    .line 88
    invoke-virtual {p0, p1}, Lcom/amap/api/a/bv;->b(Lcom/amap/api/a/ap;)Z

    .line 89
    iget-object v0, p0, Lcom/amap/api/a/bv;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 90
    invoke-virtual {p0}, Lcom/amap/api/a/bv;->c()V

    .line 91
    return-void
.end method

.method public a(Z)V
    .locals 3

    .prologue
    .line 98
    iget-object v0, p0, Lcom/amap/api/a/bv;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/a/ap;

    .line 99
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/amap/api/a/ap;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 100
    invoke-interface {v0, p1}, Lcom/amap/api/a/ap;->b(Z)V

    goto :goto_0

    .line 103
    :cond_1
    return-void
.end method

.method protected b()Z
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/amap/api/a/bv;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 55
    const/4 v0, 0x1

    .line 57
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lcom/amap/api/a/ap;)Z
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/amap/api/a/bv;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method c()V
    .locals 5

    .prologue
    .line 79
    iget-object v0, p0, Lcom/amap/api/a/bv;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v2

    .line 80
    iget-object v0, p0, Lcom/amap/api/a/bv;->b:Lcom/amap/api/a/bv$a;

    invoke-static {v2, v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 81
    iget-object v0, p0, Lcom/amap/api/a/bv;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    .line 82
    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 83
    iget-object v4, p0, Lcom/amap/api/a/bv;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    check-cast v0, Lcom/amap/api/a/ap;

    invoke-virtual {v4, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 82
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 85
    :cond_0
    return-void
.end method
