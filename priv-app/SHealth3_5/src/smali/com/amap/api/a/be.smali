.class Lcom/amap/api/a/be;
.super Ljava/lang/Object;
.source "MarkerDelegateImp.java"

# interfaces
.implements Lcom/amap/api/a/aj;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/amap/api/a/be$a;
    }
.end annotation


# static fields
.field private static a:I


# instance fields
.field private b:I

.field private c:F

.field private d:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/amap/api/maps2d/model/BitmapDescriptor;",
            ">;"
        }
    .end annotation
.end field

.field private e:I

.field private f:Ljava/lang/String;

.field private g:Lcom/amap/api/maps2d/model/LatLng;

.field private h:Lcom/amap/api/maps2d/model/LatLng;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:F

.field private l:F

.field private m:Z

.field private n:Z

.field private o:Lcom/amap/api/a/ay;

.field private p:Ljava/lang/Object;

.field private q:Z

.field private r:Lcom/amap/api/a/be$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const/4 v0, 0x0

    sput v0, Lcom/amap/api/a/be;->a:I

    return-void
.end method

.method public constructor <init>(Lcom/amap/api/maps2d/model/MarkerOptions;Lcom/amap/api/a/ay;)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput v1, p0, Lcom/amap/api/a/be;->b:I

    .line 22
    const/4 v0, 0x0

    iput v0, p0, Lcom/amap/api/a/be;->c:F

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/a/be;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 27
    const/16 v0, 0x14

    iput v0, p0, Lcom/amap/api/a/be;->e:I

    .line 34
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Lcom/amap/api/a/be;->k:F

    .line 35
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/amap/api/a/be;->l:F

    .line 36
    iput-boolean v1, p0, Lcom/amap/api/a/be;->m:Z

    .line 37
    iput-boolean v2, p0, Lcom/amap/api/a/be;->n:Z

    .line 40
    iput-boolean v1, p0, Lcom/amap/api/a/be;->q:Z

    .line 119
    iput-object p2, p0, Lcom/amap/api/a/be;->o:Lcom/amap/api/a/ay;

    .line 120
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/MarkerOptions;->isGps()Z

    move-result v0

    iput-boolean v0, p0, Lcom/amap/api/a/be;->q:Z

    .line 121
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/MarkerOptions;->getPosition()Lcom/amap/api/maps2d/model/LatLng;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 122
    iget-boolean v0, p0, Lcom/amap/api/a/be;->q:Z

    if-eqz v0, :cond_0

    .line 124
    :try_start_0
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/MarkerOptions;->getPosition()Lcom/amap/api/maps2d/model/LatLng;

    move-result-object v0

    iget-wide v0, v0, Lcom/amap/api/maps2d/model/LatLng;->longitude:D

    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/MarkerOptions;->getPosition()Lcom/amap/api/maps2d/model/LatLng;

    move-result-object v2

    iget-wide v2, v2, Lcom/amap/api/maps2d/model/LatLng;->latitude:D

    invoke-static {v0, v1, v2, v3}, Lcom/autonavi/a/a;->a(DD)[D

    move-result-object v0

    .line 127
    new-instance v1, Lcom/amap/api/maps2d/model/LatLng;

    const/4 v2, 0x1

    aget-wide v2, v0, v2

    const/4 v4, 0x0

    aget-wide v4, v0, v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/amap/api/maps2d/model/LatLng;-><init>(DD)V

    iput-object v1, p0, Lcom/amap/api/a/be;->h:Lcom/amap/api/maps2d/model/LatLng;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 132
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/MarkerOptions;->getPosition()Lcom/amap/api/maps2d/model/LatLng;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/be;->g:Lcom/amap/api/maps2d/model/LatLng;

    .line 134
    :cond_1
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/MarkerOptions;->getAnchorU()F

    move-result v0

    iput v0, p0, Lcom/amap/api/a/be;->k:F

    .line 135
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/MarkerOptions;->getAnchorV()F

    move-result v0

    iput v0, p0, Lcom/amap/api/a/be;->l:F

    .line 137
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/MarkerOptions;->isVisible()Z

    move-result v0

    iput-boolean v0, p0, Lcom/amap/api/a/be;->n:Z

    .line 138
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/MarkerOptions;->getSnippet()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/be;->j:Ljava/lang/String;

    .line 139
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/MarkerOptions;->getTitle()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/be;->i:Ljava/lang/String;

    .line 140
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/MarkerOptions;->isDraggable()Z

    move-result v0

    iput-boolean v0, p0, Lcom/amap/api/a/be;->m:Z

    .line 141
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/MarkerOptions;->getPeriod()I

    move-result v0

    iput v0, p0, Lcom/amap/api/a/be;->e:I

    .line 142
    invoke-virtual {p0}, Lcom/amap/api/a/be;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/be;->f:Ljava/lang/String;

    .line 143
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/MarkerOptions;->getIcons()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/amap/api/a/be;->b(Ljava/util/ArrayList;)V

    .line 145
    iget-object v0, p0, Lcom/amap/api/a/be;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/amap/api/a/be;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 147
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/MarkerOptions;->getIcon()Lcom/amap/api/maps2d/model/BitmapDescriptor;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/amap/api/a/be;->b(Lcom/amap/api/maps2d/model/BitmapDescriptor;)V

    .line 152
    :cond_2
    return-void

    .line 128
    :catch_0
    move-exception v0

    .line 129
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/MarkerOptions;->getPosition()Lcom/amap/api/maps2d/model/LatLng;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/be;->h:Lcom/amap/api/maps2d/model/LatLng;

    goto :goto_0
.end method

.method static synthetic a(Lcom/amap/api/a/be;I)I
    .locals 0

    .prologue
    .line 19
    iput p1, p0, Lcom/amap/api/a/be;->b:I

    return p1
.end method

.method static synthetic a(Lcom/amap/api/a/be;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/amap/api/a/be;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method static synthetic b(Lcom/amap/api/a/be;)I
    .locals 1

    .prologue
    .line 19
    iget v0, p0, Lcom/amap/api/a/be;->b:I

    return v0
.end method

.method private b(FF)Lcom/autonavi/amap/mapcore2d/IPoint;
    .locals 8

    .prologue
    .line 250
    const-wide v0, 0x400921fb54442d18L    # Math.PI

    iget v2, p0, Lcom/amap/api/a/be;->c:F

    float-to-double v2, v2

    mul-double/2addr v0, v2

    const-wide v2, 0x4066800000000000L    # 180.0

    div-double/2addr v0, v2

    double-to-float v0, v0

    .line 251
    new-instance v1, Lcom/autonavi/amap/mapcore2d/IPoint;

    invoke-direct {v1}, Lcom/autonavi/amap/mapcore2d/IPoint;-><init>()V

    .line 252
    float-to-double v2, p1

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    float-to-double v4, p2

    float-to-double v6, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    double-to-int v2, v2

    iput v2, v1, Lcom/autonavi/amap/mapcore2d/IPoint;->x:I

    .line 253
    float-to-double v2, p2

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    float-to-double v4, p1

    float-to-double v6, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    double-to-int v0, v2

    iput v0, v1, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    .line 254
    return-object v1
.end method

.method private b(Lcom/amap/api/maps2d/model/BitmapDescriptor;)V
    .locals 2

    .prologue
    .line 112
    if-eqz p1, :cond_0

    .line 113
    invoke-virtual {p0}, Lcom/amap/api/a/be;->t()V

    .line 114
    iget-object v0, p0, Lcom/amap/api/a/be;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/BitmapDescriptor;->clone()Lcom/amap/api/maps2d/model/BitmapDescriptor;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 116
    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/amap/api/a/be;)I
    .locals 2

    .prologue
    .line 19
    iget v0, p0, Lcom/amap/api/a/be;->b:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/amap/api/a/be;->b:I

    return v0
.end method

.method private static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 44
    sget v0, Lcom/amap/api/a/be;->a:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/amap/api/a/be;->a:I

    .line 45
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/amap/api/a/be;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Lcom/amap/api/a/be;)Lcom/amap/api/a/ay;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/amap/api/a/be;->o:Lcom/amap/api/a/ay;

    return-object v0
.end method

.method static synthetic e(Lcom/amap/api/a/be;)I
    .locals 1

    .prologue
    .line 19
    iget v0, p0, Lcom/amap/api/a/be;->e:I

    return v0
.end method


# virtual methods
.method public a(F)V
    .locals 2

    .prologue
    const/high16 v1, 0x43b40000    # 360.0f

    .line 407
    neg-float v0, p1

    rem-float/2addr v0, v1

    add-float/2addr v0, v1

    rem-float/2addr v0, v1

    iput v0, p0, Lcom/amap/api/a/be;->c:F

    .line 408
    invoke-virtual {p0}, Lcom/amap/api/a/be;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 409
    iget-object v0, p0, Lcom/amap/api/a/be;->o:Lcom/amap/api/a/ay;

    invoke-virtual {v0, p0}, Lcom/amap/api/a/ay;->e(Lcom/amap/api/a/aj;)V

    .line 410
    iget-object v0, p0, Lcom/amap/api/a/be;->o:Lcom/amap/api/a/ay;

    invoke-virtual {v0, p0}, Lcom/amap/api/a/ay;->d(Lcom/amap/api/a/aj;)V

    .line 412
    :cond_0
    return-void
.end method

.method public a(FF)V
    .locals 1

    .prologue
    .line 380
    iget v0, p0, Lcom/amap/api/a/be;->k:F

    cmpl-float v0, v0, p1

    if-nez v0, :cond_1

    iget v0, p0, Lcom/amap/api/a/be;->l:F

    cmpl-float v0, v0, p2

    if-nez v0, :cond_1

    .line 388
    :cond_0
    :goto_0
    return-void

    .line 382
    :cond_1
    iput p1, p0, Lcom/amap/api/a/be;->k:F

    .line 383
    iput p2, p0, Lcom/amap/api/a/be;->l:F

    .line 384
    invoke-virtual {p0}, Lcom/amap/api/a/be;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 385
    iget-object v0, p0, Lcom/amap/api/a/be;->o:Lcom/amap/api/a/ay;

    invoke-virtual {v0, p0}, Lcom/amap/api/a/ay;->e(Lcom/amap/api/a/aj;)V

    .line 386
    iget-object v0, p0, Lcom/amap/api/a/be;->o:Lcom/amap/api/a/ay;

    invoke-virtual {v0, p0}, Lcom/amap/api/a/ay;->d(Lcom/amap/api/a/aj;)V

    goto :goto_0
.end method

.method public a(I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 472
    if-gt p1, v0, :cond_0

    .line 473
    iput v0, p0, Lcom/amap/api/a/be;->e:I

    .line 477
    :goto_0
    return-void

    .line 475
    :cond_0
    iput p1, p0, Lcom/amap/api/a/be;->e:I

    goto :goto_0
.end method

.method public a(Landroid/graphics/Canvas;Lcom/amap/api/a/af;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x1

    .line 430
    iget-boolean v0, p0, Lcom/amap/api/a/be;->n:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/amap/api/a/be;->c()Lcom/amap/api/maps2d/model/LatLng;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/amap/api/a/be;->x()Lcom/amap/api/maps2d/model/BitmapDescriptor;

    move-result-object v0

    if-nez v0, :cond_1

    .line 453
    :cond_0
    :goto_0
    return-void

    .line 434
    :cond_1
    invoke-virtual {p0}, Lcom/amap/api/a/be;->w()Lcom/autonavi/amap/mapcore2d/IPoint;

    move-result-object v2

    .line 436
    invoke-virtual {p0}, Lcom/amap/api/a/be;->s()Ljava/util/ArrayList;

    move-result-object v0

    .line 437
    if-eqz v0, :cond_0

    .line 440
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-le v3, v4, :cond_2

    .line 441
    iget v3, p0, Lcom/amap/api/a/be;->b:I

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/maps2d/model/BitmapDescriptor;

    invoke-virtual {v0}, Lcom/amap/api/maps2d/model/BitmapDescriptor;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 445
    :goto_1
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v3

    if-nez v3, :cond_0

    .line 446
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 447
    iget v3, p0, Lcom/amap/api/a/be;->c:F

    iget v4, v2, Lcom/autonavi/amap/mapcore2d/IPoint;->x:I

    int-to-float v4, v4

    iget v5, v2, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    int-to-float v5, v5

    invoke-virtual {p1, v3, v4, v5}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 448
    iget v3, v2, Lcom/autonavi/amap/mapcore2d/IPoint;->x:I

    int-to-float v3, v3

    invoke-virtual {p0}, Lcom/amap/api/a/be;->y()F

    move-result v4

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    iget v2, v2, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/amap/api/a/be;->z()F

    move-result v4

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v4, v5

    sub-float/2addr v2, v4

    invoke-virtual {p1, v0, v3, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 451
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_0

    .line 442
    :cond_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ne v3, v4, :cond_3

    .line 443
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/maps2d/model/BitmapDescriptor;

    invoke-virtual {v0}, Lcom/amap/api/maps2d/model/BitmapDescriptor;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method public a(Lcom/amap/api/maps2d/model/BitmapDescriptor;)V
    .locals 1

    .prologue
    .line 316
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/amap/api/a/be;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    if-nez v0, :cond_1

    .line 326
    :cond_0
    :goto_0
    return-void

    .line 319
    :cond_1
    iget-object v0, p0, Lcom/amap/api/a/be;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    .line 320
    iget-object v0, p0, Lcom/amap/api/a/be;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 322
    invoke-virtual {p0}, Lcom/amap/api/a/be;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 323
    iget-object v0, p0, Lcom/amap/api/a/be;->o:Lcom/amap/api/a/ay;

    invoke-virtual {v0, p0}, Lcom/amap/api/a/ay;->e(Lcom/amap/api/a/aj;)V

    .line 324
    iget-object v0, p0, Lcom/amap/api/a/be;->o:Lcom/amap/api/a/ay;

    invoke-virtual {v0, p0}, Lcom/amap/api/a/ay;->d(Lcom/amap/api/a/aj;)V

    goto :goto_0
.end method

.method public a(Lcom/amap/api/maps2d/model/LatLng;)V
    .locals 6

    .prologue
    .line 277
    iget-boolean v0, p0, Lcom/amap/api/a/be;->q:Z

    if-eqz v0, :cond_0

    .line 279
    :try_start_0
    iget-wide v0, p1, Lcom/amap/api/maps2d/model/LatLng;->longitude:D

    iget-wide v2, p1, Lcom/amap/api/maps2d/model/LatLng;->latitude:D

    invoke-static {v0, v1, v2, v3}, Lcom/autonavi/a/a;->a(DD)[D

    move-result-object v0

    .line 281
    new-instance v1, Lcom/amap/api/maps2d/model/LatLng;

    const/4 v2, 0x1

    aget-wide v2, v0, v2

    const/4 v4, 0x0

    aget-wide v4, v0, v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/amap/api/maps2d/model/LatLng;-><init>(DD)V

    iput-object v1, p0, Lcom/amap/api/a/be;->h:Lcom/amap/api/maps2d/model/LatLng;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 286
    :cond_0
    :goto_0
    iput-object p1, p0, Lcom/amap/api/a/be;->g:Lcom/amap/api/maps2d/model/LatLng;

    .line 287
    return-void

    .line 282
    :catch_0
    move-exception v0

    .line 283
    iput-object p1, p0, Lcom/amap/api/a/be;->h:Lcom/amap/api/maps2d/model/LatLng;

    goto :goto_0
.end method

.method public a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 457
    iput-object p1, p0, Lcom/amap/api/a/be;->p:Ljava/lang/Object;

    .line 458
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 291
    iput-object p1, p0, Lcom/amap/api/a/be;->i:Ljava/lang/String;

    .line 292
    return-void
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/amap/api/maps2d/model/BitmapDescriptor;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 487
    if-nez p1, :cond_1

    .line 499
    :cond_0
    :goto_0
    return-void

    .line 490
    :cond_1
    invoke-virtual {p0, p1}, Lcom/amap/api/a/be;->b(Ljava/util/ArrayList;)V

    .line 491
    iget-object v0, p0, Lcom/amap/api/a/be;->r:Lcom/amap/api/a/be$a;

    if-nez v0, :cond_2

    .line 492
    new-instance v0, Lcom/amap/api/a/be$a;

    invoke-direct {v0, p0}, Lcom/amap/api/a/be$a;-><init>(Lcom/amap/api/a/be;)V

    iput-object v0, p0, Lcom/amap/api/a/be;->r:Lcom/amap/api/a/be$a;

    .line 493
    iget-object v0, p0, Lcom/amap/api/a/be;->r:Lcom/amap/api/a/be$a;

    invoke-virtual {v0}, Lcom/amap/api/a/be$a;->start()V

    .line 495
    :cond_2
    invoke-virtual {p0}, Lcom/amap/api/a/be;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 496
    iget-object v0, p0, Lcom/amap/api/a/be;->o:Lcom/amap/api/a/ay;

    invoke-virtual {v0, p0}, Lcom/amap/api/a/ay;->e(Lcom/amap/api/a/aj;)V

    .line 497
    iget-object v0, p0, Lcom/amap/api/a/be;->o:Lcom/amap/api/a/ay;

    invoke-virtual {v0, p0}, Lcom/amap/api/a/ay;->d(Lcom/amap/api/a/aj;)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 311
    iput-boolean p1, p0, Lcom/amap/api/a/be;->m:Z

    .line 312
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lcom/amap/api/a/be;->o:Lcom/amap/api/a/ay;

    invoke-virtual {v0, p0}, Lcom/amap/api/a/ay;->b(Lcom/amap/api/a/aj;)Z

    move-result v0

    return v0
.end method

.method public a(Lcom/amap/api/a/aj;)Z
    .locals 2

    .prologue
    .line 416
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Lcom/amap/api/a/aj;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/amap/api/a/be;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 418
    :cond_0
    const/4 v0, 0x1

    .line 420
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Landroid/graphics/Rect;
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/high16 v9, 0x3f800000    # 1.0f

    .line 214
    invoke-virtual {p0}, Lcom/amap/api/a/be;->w()Lcom/autonavi/amap/mapcore2d/IPoint;

    move-result-object v1

    .line 215
    if-nez v1, :cond_0

    .line 216
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v2, v2, v2, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 246
    :goto_0
    return-object v0

    .line 218
    :cond_0
    invoke-virtual {p0}, Lcom/amap/api/a/be;->q()I

    move-result v2

    .line 219
    invoke-virtual {p0}, Lcom/amap/api/a/be;->v()I

    move-result v3

    .line 220
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 221
    iget v4, p0, Lcom/amap/api/a/be;->c:F

    const/4 v5, 0x0

    cmpl-float v4, v4, v5

    if-nez v4, :cond_1

    .line 222
    iget v4, v1, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    int-to-float v4, v4

    int-to-float v5, v3

    iget v6, p0, Lcom/amap/api/a/be;->l:F

    mul-float/2addr v5, v6

    sub-float/2addr v4, v5

    float-to-int v4, v4

    iput v4, v0, Landroid/graphics/Rect;->top:I

    .line 223
    iget v4, v1, Lcom/autonavi/amap/mapcore2d/IPoint;->x:I

    int-to-float v4, v4

    iget v5, p0, Lcom/amap/api/a/be;->k:F

    int-to-float v6, v2

    mul-float/2addr v5, v6

    sub-float/2addr v4, v5

    float-to-int v4, v4

    iput v4, v0, Landroid/graphics/Rect;->left:I

    .line 224
    iget v4, v1, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    int-to-float v4, v4

    int-to-float v3, v3

    iget v5, p0, Lcom/amap/api/a/be;->l:F

    sub-float v5, v9, v5

    mul-float/2addr v3, v5

    add-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, v0, Landroid/graphics/Rect;->bottom:I

    .line 225
    iget v1, v1, Lcom/autonavi/amap/mapcore2d/IPoint;->x:I

    int-to-float v1, v1

    iget v3, p0, Lcom/amap/api/a/be;->k:F

    sub-float v3, v9, v3

    int-to-float v2, v2

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->right:I

    goto :goto_0

    .line 227
    :cond_1
    iget v4, p0, Lcom/amap/api/a/be;->k:F

    neg-float v4, v4

    int-to-float v5, v2

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/amap/api/a/be;->l:F

    sub-float/2addr v5, v9

    int-to-float v6, v3

    mul-float/2addr v5, v6

    invoke-direct {p0, v4, v5}, Lcom/amap/api/a/be;->b(FF)Lcom/autonavi/amap/mapcore2d/IPoint;

    move-result-object v4

    .line 229
    iget v5, p0, Lcom/amap/api/a/be;->k:F

    neg-float v5, v5

    int-to-float v6, v2

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/amap/api/a/be;->l:F

    int-to-float v7, v3

    mul-float/2addr v6, v7

    invoke-direct {p0, v5, v6}, Lcom/amap/api/a/be;->b(FF)Lcom/autonavi/amap/mapcore2d/IPoint;

    move-result-object v5

    .line 231
    iget v6, p0, Lcom/amap/api/a/be;->k:F

    sub-float v6, v9, v6

    int-to-float v7, v2

    mul-float/2addr v6, v7

    iget v7, p0, Lcom/amap/api/a/be;->l:F

    int-to-float v8, v3

    mul-float/2addr v7, v8

    invoke-direct {p0, v6, v7}, Lcom/amap/api/a/be;->b(FF)Lcom/autonavi/amap/mapcore2d/IPoint;

    move-result-object v6

    .line 233
    iget v7, p0, Lcom/amap/api/a/be;->k:F

    sub-float v7, v9, v7

    int-to-float v2, v2

    mul-float/2addr v2, v7

    iget v7, p0, Lcom/amap/api/a/be;->l:F

    sub-float/2addr v7, v9

    int-to-float v3, v3

    mul-float/2addr v3, v7

    invoke-direct {p0, v2, v3}, Lcom/amap/api/a/be;->b(FF)Lcom/autonavi/amap/mapcore2d/IPoint;

    move-result-object v2

    .line 235
    iget v3, v1, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    iget v7, v4, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    iget v8, v5, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    iget v9, v6, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    iget v10, v2, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    invoke-static {v9, v10}, Ljava/lang/Math;->max(II)I

    move-result v9

    invoke-static {v8, v9}, Ljava/lang/Math;->max(II)I

    move-result v8

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v7

    sub-int/2addr v3, v7

    iput v3, v0, Landroid/graphics/Rect;->top:I

    .line 237
    iget v3, v1, Lcom/autonavi/amap/mapcore2d/IPoint;->x:I

    iget v7, v4, Lcom/autonavi/amap/mapcore2d/IPoint;->x:I

    iget v8, v5, Lcom/autonavi/amap/mapcore2d/IPoint;->x:I

    iget v9, v6, Lcom/autonavi/amap/mapcore2d/IPoint;->x:I

    iget v10, v2, Lcom/autonavi/amap/mapcore2d/IPoint;->x:I

    invoke-static {v9, v10}, Ljava/lang/Math;->min(II)I

    move-result v9

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v8

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v7

    add-int/2addr v3, v7

    iput v3, v0, Landroid/graphics/Rect;->left:I

    .line 239
    iget v3, v1, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    iget v7, v4, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    iget v8, v5, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    iget v9, v6, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    iget v10, v2, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    invoke-static {v9, v10}, Ljava/lang/Math;->min(II)I

    move-result v9

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v8

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v7

    sub-int/2addr v3, v7

    iput v3, v0, Landroid/graphics/Rect;->bottom:I

    .line 241
    iget v1, v1, Lcom/autonavi/amap/mapcore2d/IPoint;->x:I

    iget v3, v4, Lcom/autonavi/amap/mapcore2d/IPoint;->x:I

    iget v4, v5, Lcom/autonavi/amap/mapcore2d/IPoint;->x:I

    iget v5, v6, Lcom/autonavi/amap/mapcore2d/IPoint;->x:I

    iget v2, v2, Lcom/autonavi/amap/mapcore2d/IPoint;->x:I

    invoke-static {v5, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v4, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    goto/16 :goto_0
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 301
    iput-object p1, p0, Lcom/amap/api/a/be;->j:Ljava/lang/String;

    .line 302
    return-void
.end method

.method public b(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/amap/api/maps2d/model/BitmapDescriptor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/amap/api/a/be;->t()V

    .line 96
    if-eqz p1, :cond_2

    .line 97
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/maps2d/model/BitmapDescriptor;

    .line 98
    if-eqz v0, :cond_0

    .line 99
    iget-object v2, p0, Lcom/amap/api/a/be;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Lcom/amap/api/maps2d/model/BitmapDescriptor;->clone()Lcom/amap/api/maps2d/model/BitmapDescriptor;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 102
    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_2

    .line 103
    iget-object v0, p0, Lcom/amap/api/a/be;->r:Lcom/amap/api/a/be$a;

    if-nez v0, :cond_2

    .line 104
    new-instance v0, Lcom/amap/api/a/be$a;

    invoke-direct {v0, p0}, Lcom/amap/api/a/be$a;-><init>(Lcom/amap/api/a/be;)V

    iput-object v0, p0, Lcom/amap/api/a/be;->r:Lcom/amap/api/a/be$a;

    .line 105
    iget-object v0, p0, Lcom/amap/api/a/be;->r:Lcom/amap/api/a/be$a;

    invoke-virtual {v0}, Lcom/amap/api/a/be$a;->start()V

    .line 109
    :cond_2
    return-void
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 367
    iput-boolean p1, p0, Lcom/amap/api/a/be;->n:Z

    .line 368
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/amap/api/a/be;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 369
    iget-object v0, p0, Lcom/amap/api/a/be;->o:Lcom/amap/api/a/ay;

    invoke-virtual {v0, p0}, Lcom/amap/api/a/ay;->e(Lcom/amap/api/a/aj;)V

    .line 371
    :cond_0
    return-void
.end method

.method public c()Lcom/amap/api/maps2d/model/LatLng;
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lcom/amap/api/a/be;->g:Lcom/amap/api/maps2d/model/LatLng;

    return-object v0
.end method

.method public d()Lcom/amap/api/maps2d/model/LatLng;
    .locals 1

    .prologue
    .line 467
    iget-boolean v0, p0, Lcom/amap/api/a/be;->q:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/a/be;->h:Lcom/amap/api/maps2d/model/LatLng;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/be;->g:Lcom/amap/api/maps2d/model/LatLng;

    goto :goto_0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lcom/amap/api/a/be;->f:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 270
    const-string v0, "Marker"

    invoke-static {v0}, Lcom/amap/api/a/be;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/be;->f:Ljava/lang/String;

    .line 272
    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/be;->f:Ljava/lang/String;

    return-object v0
.end method

.method public f()Lcom/autonavi/amap/mapcore2d/FPoint;
    .locals 3

    .prologue
    .line 185
    new-instance v0, Lcom/autonavi/amap/mapcore2d/FPoint;

    invoke-direct {v0}, Lcom/autonavi/amap/mapcore2d/FPoint;-><init>()V

    .line 186
    iget-object v1, p0, Lcom/amap/api/a/be;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/amap/api/a/be;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v1

    if-eqz v1, :cond_0

    .line 187
    invoke-virtual {p0}, Lcom/amap/api/a/be;->q()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/amap/api/a/be;->k:F

    mul-float/2addr v1, v2

    iput v1, v0, Lcom/autonavi/amap/mapcore2d/FPoint;->x:F

    .line 188
    invoke-virtual {p0}, Lcom/amap/api/a/be;->v()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/amap/api/a/be;->l:F

    mul-float/2addr v1, v2

    iput v1, v0, Lcom/autonavi/amap/mapcore2d/FPoint;->y:F

    .line 190
    :cond_0
    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 296
    iget-object v0, p0, Lcom/amap/api/a/be;->i:Ljava/lang/String;

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 306
    iget-object v0, p0, Lcom/amap/api/a/be;->j:Ljava/lang/String;

    return-object v0
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 342
    iget-boolean v0, p0, Lcom/amap/api/a/be;->m:Z

    return v0
.end method

.method public j()V
    .locals 1

    .prologue
    .line 347
    invoke-virtual {p0}, Lcom/amap/api/a/be;->m()Z

    move-result v0

    if-nez v0, :cond_0

    .line 351
    :goto_0
    return-void

    .line 350
    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/be;->o:Lcom/amap/api/a/ay;

    invoke-virtual {v0, p0}, Lcom/amap/api/a/ay;->d(Lcom/amap/api/a/aj;)V

    goto :goto_0
.end method

.method public k()V
    .locals 1

    .prologue
    .line 355
    invoke-virtual {p0}, Lcom/amap/api/a/be;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 356
    iget-object v0, p0, Lcom/amap/api/a/be;->o:Lcom/amap/api/a/ay;

    invoke-virtual {v0, p0}, Lcom/amap/api/a/ay;->e(Lcom/amap/api/a/aj;)V

    .line 358
    :cond_0
    return-void
.end method

.method public l()Z
    .locals 1

    .prologue
    .line 362
    iget-object v0, p0, Lcom/amap/api/a/be;->o:Lcom/amap/api/a/ay;

    invoke-virtual {v0, p0}, Lcom/amap/api/a/ay;->f(Lcom/amap/api/a/aj;)Z

    move-result v0

    return v0
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 375
    iget-boolean v0, p0, Lcom/amap/api/a/be;->n:Z

    return v0
.end method

.method public n()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 50
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/a/be;->a()Z

    .line 59
    iget-object v0, p0, Lcom/amap/api/a/be;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/maps2d/model/BitmapDescriptor;

    .line 60
    invoke-virtual {v0}, Lcom/amap/api/maps2d/model/BitmapDescriptor;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 61
    if-eqz v0, :cond_0

    .line 62
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 75
    :catch_0
    move-exception v0

    .line 76
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 77
    const-string v0, "destroy erro"

    const-string v1, "MarkerDelegateImp destroy"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    :goto_1
    iput-object v2, p0, Lcom/amap/api/a/be;->r:Lcom/amap/api/a/be$a;

    .line 83
    return-void

    .line 66
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    iput-object v0, p0, Lcom/amap/api/a/be;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 67
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/a/be;->g:Lcom/amap/api/maps2d/model/LatLng;

    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/a/be;->p:Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public o()I
    .locals 1

    .prologue
    .line 425
    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public p()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 462
    iget-object v0, p0, Lcom/amap/api/a/be;->p:Ljava/lang/Object;

    return-object v0
.end method

.method public q()I
    .locals 1

    .prologue
    .line 175
    invoke-virtual {p0}, Lcom/amap/api/a/be;->x()Lcom/amap/api/maps2d/model/BitmapDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/maps2d/model/BitmapDescriptor;->getWidth()I

    move-result v0

    .line 176
    return v0
.end method

.method public r()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 481
    iget v0, p0, Lcom/amap/api/a/be;->e:I

    return v0
.end method

.method public s()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/amap/api/maps2d/model/BitmapDescriptor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 503
    iget-object v0, p0, Lcom/amap/api/a/be;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/amap/api/a/be;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 504
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 505
    iget-object v0, p0, Lcom/amap/api/a/be;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/maps2d/model/BitmapDescriptor;

    .line 506
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 510
    :goto_1
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method t()V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/amap/api/a/be;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    if-nez v0, :cond_0

    .line 87
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/a/be;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 91
    :goto_0
    return-void

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/be;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    goto :goto_0
.end method

.method public u()Lcom/autonavi/amap/mapcore2d/IPoint;
    .locals 7

    .prologue
    const-wide v5, 0x412e848000000000L    # 1000000.0

    .line 155
    invoke-virtual {p0}, Lcom/amap/api/a/be;->c()Lcom/amap/api/maps2d/model/LatLng;

    move-result-object v0

    if-nez v0, :cond_0

    .line 156
    const/4 v0, 0x0

    .line 171
    :goto_0
    return-object v0

    .line 158
    :cond_0
    new-instance v1, Lcom/autonavi/amap/mapcore2d/IPoint;

    invoke-direct {v1}, Lcom/autonavi/amap/mapcore2d/IPoint;-><init>()V

    .line 160
    iget-boolean v0, p0, Lcom/amap/api/a/be;->q:Z

    if-eqz v0, :cond_1

    .line 161
    new-instance v0, Lcom/amap/api/a/ac;

    invoke-virtual {p0}, Lcom/amap/api/a/be;->d()Lcom/amap/api/maps2d/model/LatLng;

    move-result-object v2

    iget-wide v2, v2, Lcom/amap/api/maps2d/model/LatLng;->latitude:D

    mul-double/2addr v2, v5

    double-to-int v2, v2

    invoke-virtual {p0}, Lcom/amap/api/a/be;->d()Lcom/amap/api/maps2d/model/LatLng;

    move-result-object v3

    iget-wide v3, v3, Lcom/amap/api/maps2d/model/LatLng;->longitude:D

    mul-double/2addr v3, v5

    double-to-int v3, v3

    invoke-direct {v0, v2, v3}, Lcom/amap/api/a/ac;-><init>(II)V

    .line 167
    :goto_1
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 168
    iget-object v3, p0, Lcom/amap/api/a/be;->o:Lcom/amap/api/a/ay;

    invoke-virtual {v3}, Lcom/amap/api/a/ay;->a()Lcom/amap/api/a/b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/amap/api/a/b;->s()Lcom/amap/api/a/bl;

    move-result-object v3

    invoke-interface {v3, v0, v2}, Lcom/amap/api/a/bl;->a(Lcom/amap/api/a/ac;Landroid/graphics/Point;)Landroid/graphics/Point;

    .line 169
    iget v0, v2, Landroid/graphics/Point;->x:I

    iput v0, v1, Lcom/autonavi/amap/mapcore2d/IPoint;->x:I

    .line 170
    iget v0, v2, Landroid/graphics/Point;->y:I

    iput v0, v1, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    move-object v0, v1

    .line 171
    goto :goto_0

    .line 164
    :cond_1
    new-instance v0, Lcom/amap/api/a/ac;

    invoke-virtual {p0}, Lcom/amap/api/a/be;->c()Lcom/amap/api/maps2d/model/LatLng;

    move-result-object v2

    iget-wide v2, v2, Lcom/amap/api/maps2d/model/LatLng;->latitude:D

    mul-double/2addr v2, v5

    double-to-int v2, v2

    invoke-virtual {p0}, Lcom/amap/api/a/be;->c()Lcom/amap/api/maps2d/model/LatLng;

    move-result-object v3

    iget-wide v3, v3, Lcom/amap/api/maps2d/model/LatLng;->longitude:D

    mul-double/2addr v3, v5

    double-to-int v3, v3

    invoke-direct {v0, v2, v3}, Lcom/amap/api/a/ac;-><init>(II)V

    goto :goto_1
.end method

.method public v()I
    .locals 1

    .prologue
    .line 180
    invoke-virtual {p0}, Lcom/amap/api/a/be;->x()Lcom/amap/api/maps2d/model/BitmapDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/maps2d/model/BitmapDescriptor;->getHeight()I

    move-result v0

    .line 181
    return v0
.end method

.method public w()Lcom/autonavi/amap/mapcore2d/IPoint;
    .locals 1

    .prologue
    .line 203
    invoke-virtual {p0}, Lcom/amap/api/a/be;->u()Lcom/autonavi/amap/mapcore2d/IPoint;

    move-result-object v0

    .line 204
    if-nez v0, :cond_0

    .line 205
    const/4 v0, 0x0

    .line 210
    :cond_0
    return-object v0
.end method

.method public x()Lcom/amap/api/maps2d/model/BitmapDescriptor;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 330
    iget-object v0, p0, Lcom/amap/api/a/be;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/a/be;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 331
    :cond_0
    invoke-virtual {p0}, Lcom/amap/api/a/be;->t()V

    .line 332
    iget-object v0, p0, Lcom/amap/api/a/be;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-static {}, Lcom/amap/api/maps2d/model/BitmapDescriptorFactory;->defaultMarker()Lcom/amap/api/maps2d/model/BitmapDescriptor;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 337
    :cond_1
    iget-object v0, p0, Lcom/amap/api/a/be;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/maps2d/model/BitmapDescriptor;

    :goto_0
    return-object v0

    .line 333
    :cond_2
    iget-object v0, p0, Lcom/amap/api/a/be;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 334
    iget-object v0, p0, Lcom/amap/api/a/be;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    .line 335
    invoke-virtual {p0}, Lcom/amap/api/a/be;->x()Lcom/amap/api/maps2d/model/BitmapDescriptor;

    move-result-object v0

    goto :goto_0
.end method

.method public y()F
    .locals 1

    .prologue
    .line 392
    iget v0, p0, Lcom/amap/api/a/be;->k:F

    return v0
.end method

.method public z()F
    .locals 1

    .prologue
    .line 397
    iget v0, p0, Lcom/amap/api/a/be;->l:F

    return v0
.end method
