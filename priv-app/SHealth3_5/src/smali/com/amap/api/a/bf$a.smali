.class public Lcom/amap/api/a/bf$a;
.super Ljava/lang/Object;
.source "Mediator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amap/api/a/bf;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "a"
.end annotation


# instance fields
.field public a:Lcom/amap/api/a/bq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/amap/api/a/bq",
            "<",
            "Lcom/amap/api/a/as;",
            ">;"
        }
    .end annotation
.end field

.field public b:Z

.field public c:Z

.field d:Ljava/lang/String;

.field e:Ljava/lang/String;

.field f:Ljava/lang/String;

.field g:Ljava/lang/String;

.field final synthetic h:Lcom/amap/api/a/bf;

.field private i:Z

.field private j:Z

.field private k:Lcom/amap/api/a/b$b;

.field private l:I

.field private m:I

.field private n:Z


# direct methods
.method public constructor <init>(Lcom/amap/api/a/bf;Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 507
    iput-object p1, p0, Lcom/amap/api/a/bf$a;->h:Lcom/amap/api/a/bf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 492
    iput-boolean v1, p0, Lcom/amap/api/a/bf$a;->i:Z

    .line 493
    iput-boolean v3, p0, Lcom/amap/api/a/bf$a;->j:Z

    .line 497
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/a/bf$a;->a:Lcom/amap/api/a/bq;

    .line 498
    sget-object v0, Lcom/amap/api/a/b$b;->a:Lcom/amap/api/a/b$b;

    iput-object v0, p0, Lcom/amap/api/a/bf$a;->k:Lcom/amap/api/a/b$b;

    .line 504
    iput-boolean v1, p0, Lcom/amap/api/a/bf$a;->b:Z

    .line 505
    iput-boolean v1, p0, Lcom/amap/api/a/bf$a;->c:Z

    .line 531
    iput v1, p0, Lcom/amap/api/a/bf$a;->l:I

    .line 532
    iput v1, p0, Lcom/amap/api/a/bf$a;->m:I

    .line 533
    const-string v0, "GridMapV2"

    iput-object v0, p0, Lcom/amap/api/a/bf$a;->d:Ljava/lang/String;

    .line 534
    const-string v0, "SatelliteMap"

    iput-object v0, p0, Lcom/amap/api/a/bf$a;->e:Ljava/lang/String;

    .line 535
    const-string v0, "GridTmc"

    iput-object v0, p0, Lcom/amap/api/a/bf$a;->f:Ljava/lang/String;

    .line 537
    const-string v0, "SateliteTmc"

    iput-object v0, p0, Lcom/amap/api/a/bf$a;->g:Ljava/lang/String;

    .line 865
    iput-boolean v1, p0, Lcom/amap/api/a/bf$a;->n:Z

    .line 508
    if-nez p2, :cond_0

    .line 529
    :goto_0
    return-void

    .line 511
    :cond_0
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 512
    const-string/jumbo v0, "window"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 514
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 515
    iget v0, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    div-int/lit16 v0, v0, 0x100

    add-int/lit8 v0, v0, 0x2

    .line 517
    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    div-int/lit16 v1, v1, 0x100

    add-int/lit8 v1, v1, 0x2

    .line 519
    mul-int v2, v0, v1

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    iput v0, p0, Lcom/amap/api/a/bf$a;->l:I

    .line 521
    iget v0, p0, Lcom/amap/api/a/bf$a;->l:I

    div-int/lit8 v0, v0, 0x8

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/amap/api/a/bf$a;->m:I

    .line 522
    iget v0, p0, Lcom/amap/api/a/bf$a;->m:I

    if-nez v0, :cond_2

    .line 523
    iput v3, p0, Lcom/amap/api/a/bf$a;->m:I

    .line 528
    :cond_1
    :goto_1
    invoke-direct {p0, p2}, Lcom/amap/api/a/bf$a;->a(Landroid/content/Context;)V

    goto :goto_0

    .line 524
    :cond_2
    iget v0, p0, Lcom/amap/api/a/bf$a;->m:I

    if-le v0, v4, :cond_1

    .line 525
    iput v4, p0, Lcom/amap/api/a/bf$a;->m:I

    goto :goto_1
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 540
    iget-object v0, p0, Lcom/amap/api/a/bf$a;->a:Lcom/amap/api/a/bq;

    if-nez v0, :cond_0

    .line 541
    new-instance v0, Lcom/amap/api/a/bq;

    invoke-direct {v0}, Lcom/amap/api/a/bq;-><init>()V

    iput-object v0, p0, Lcom/amap/api/a/bf$a;->a:Lcom/amap/api/a/bq;

    .line 551
    :cond_0
    new-instance v0, Lcom/amap/api/a/as;

    invoke-direct {v0}, Lcom/amap/api/a/as;-><init>()V

    .line 552
    new-instance v1, Lcom/amap/api/a/bf$a$1;

    invoke-direct {v1, p0}, Lcom/amap/api/a/bf$a$1;-><init>(Lcom/amap/api/a/bf$a;)V

    iput-object v1, v0, Lcom/amap/api/a/as;->j:Lcom/amap/api/a/cd;

    .line 562
    iget-object v1, p0, Lcom/amap/api/a/bf$a;->d:Ljava/lang/String;

    iput-object v1, v0, Lcom/amap/api/a/as;->a:Ljava/lang/String;

    .line 563
    iput-boolean v2, v0, Lcom/amap/api/a/as;->e:Z

    .line 564
    iput-boolean v2, v0, Lcom/amap/api/a/as;->d:Z

    .line 565
    iput-boolean v2, v0, Lcom/amap/api/a/as;->f:Z

    .line 566
    iput-boolean v2, v0, Lcom/amap/api/a/as;->g:Z

    .line 567
    sget v1, Lcom/amap/api/a/x;->c:I

    iput v1, v0, Lcom/amap/api/a/as;->b:I

    .line 568
    sget v1, Lcom/amap/api/a/x;->d:I

    iput v1, v0, Lcom/amap/api/a/as;->c:I

    .line 569
    invoke-virtual {p0, v0, p1}, Lcom/amap/api/a/bf$a;->a(Lcom/amap/api/a/as;Landroid/content/Context;)Z

    .line 570
    return-void
.end method

.method private a(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 1011
    iget-object v0, p0, Lcom/amap/api/a/bf$a;->a:Lcom/amap/api/a/bq;

    invoke-virtual {v0}, Lcom/amap/api/a/bq;->size()I

    move-result v2

    .line 1013
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    .line 1014
    iget-object v0, p0, Lcom/amap/api/a/bf$a;->a:Lcom/amap/api/a/bq;

    invoke-virtual {v0, v1}, Lcom/amap/api/a/bq;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/a/as;

    .line 1015
    if-nez v0, :cond_1

    .line 1013
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1018
    :cond_1
    iget-boolean v3, v0, Lcom/amap/api/a/as;->f:Z

    if-eqz v3, :cond_0

    .line 1021
    invoke-virtual {v0, p1}, Lcom/amap/api/a/as;->a(Landroid/graphics/Canvas;)V

    goto :goto_1

    .line 1023
    :cond_2
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 776
    .line 777
    iget-object v0, p0, Lcom/amap/api/a/bf$a;->a:Lcom/amap/api/a/bq;

    invoke-virtual {v0}, Lcom/amap/api/a/bq;->size()I

    move-result v2

    .line 778
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 779
    iget-object v0, p0, Lcom/amap/api/a/bf$a;->a:Lcom/amap/api/a/bq;

    invoke-virtual {v0, v1}, Lcom/amap/api/a/bq;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/a/as;

    .line 780
    if-nez v0, :cond_0

    .line 778
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 783
    :cond_0
    iput v1, v0, Lcom/amap/api/a/as;->k:I

    goto :goto_1

    .line 785
    :cond_1
    return-void
.end method

.method private b(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1031
    iget-boolean v0, p0, Lcom/amap/api/a/bf$a;->j:Z

    if-eqz v0, :cond_0

    .line 1032
    iget-object v0, p0, Lcom/amap/api/a/bf$a;->h:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->f:Lcom/amap/api/a/ab;

    invoke-virtual {v0, p1}, Lcom/amap/api/a/ab;->a(Landroid/graphics/Canvas;)V

    .line 1034
    :cond_0
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x1

    .line 666
    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-ne v0, v5, :cond_1

    .line 686
    :cond_0
    return-void

    .line 670
    :cond_1
    iget-object v0, p0, Lcom/amap/api/a/bf$a;->a:Lcom/amap/api/a/bq;

    invoke-virtual {v0}, Lcom/amap/api/a/bq;->size()I

    move-result v3

    move v1, v2

    .line 671
    :goto_0
    if-ge v1, v3, :cond_0

    .line 672
    iget-object v0, p0, Lcom/amap/api/a/bf$a;->a:Lcom/amap/api/a/bq;

    invoke-virtual {v0, v1}, Lcom/amap/api/a/bq;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/a/as;

    .line 673
    if-nez v0, :cond_3

    .line 671
    :cond_2
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 676
    :cond_3
    iget-object v4, v0, Lcom/amap/api/a/as;->a:Ljava/lang/String;

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    iget-boolean v4, v0, Lcom/amap/api/a/as;->e:Z

    if-ne v4, v5, :cond_2

    .line 678
    iget-boolean v4, v0, Lcom/amap/api/a/as;->f:Z

    if-ne v4, v5, :cond_2

    .line 679
    iput-boolean v2, v0, Lcom/amap/api/a/as;->f:Z

    goto :goto_1
.end method

.method private c(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1037
    iget-object v0, p0, Lcom/amap/api/a/bf$a;->h:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->g:Lcom/amap/api/a/b;

    iget-object v0, v0, Lcom/amap/api/a/b;->i:Lcom/amap/api/a/ay;

    invoke-virtual {v0, p1}, Lcom/amap/api/a/ay;->a(Landroid/graphics/Canvas;)V

    .line 1038
    return-void
.end method

.method private c(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 689
    iget-object v0, p0, Lcom/amap/api/a/bf$a;->a:Lcom/amap/api/a/bq;

    if-nez v0, :cond_0

    move v0, v1

    .line 703
    :goto_0
    return v0

    .line 692
    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/bf$a;->a:Lcom/amap/api/a/bq;

    invoke-virtual {v0}, Lcom/amap/api/a/bq;->size()I

    move-result v4

    move v3, v1

    .line 694
    :goto_1
    if-ge v3, v4, :cond_3

    .line 695
    iget-object v0, p0, Lcom/amap/api/a/bf$a;->a:Lcom/amap/api/a/bq;

    invoke-virtual {v0, v3}, Lcom/amap/api/a/bq;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/a/as;

    .line 696
    if-nez v0, :cond_2

    .line 694
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 699
    :cond_2
    iget-object v0, v0, Lcom/amap/api/a/as;->a:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-ne v0, v2, :cond_1

    move v0, v2

    .line 700
    goto :goto_0

    :cond_3
    move v0, v1

    .line 703
    goto :goto_0
.end method


# virtual methods
.method a(Ljava/lang/String;)Lcom/amap/api/a/as;
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x1

    .line 788
    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eq v0, v5, :cond_0

    iget-object v0, p0, Lcom/amap/api/a/bf$a;->a:Lcom/amap/api/a/bq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/a/bf$a;->a:Lcom/amap/api/a/bq;

    invoke-virtual {v0}, Lcom/amap/api/a/bq;->size()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 803
    :goto_0
    return-object v0

    .line 793
    :cond_1
    iget-object v0, p0, Lcom/amap/api/a/bf$a;->a:Lcom/amap/api/a/bq;

    invoke-virtual {v0}, Lcom/amap/api/a/bq;->size()I

    move-result v3

    .line 794
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_4

    .line 795
    iget-object v0, p0, Lcom/amap/api/a/bf$a;->a:Lcom/amap/api/a/bq;

    invoke-virtual {v0, v2}, Lcom/amap/api/a/bq;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/a/as;

    .line 796
    if-nez v0, :cond_3

    .line 794
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 799
    :cond_3
    iget-object v4, v0, Lcom/amap/api/a/as;->a:Ljava/lang/String;

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-ne v4, v5, :cond_2

    goto :goto_0

    :cond_4
    move-object v0, v1

    .line 803
    goto :goto_0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 888
    iget-object v0, p0, Lcom/amap/api/a/bf$a;->h:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/a/bf$a;->h:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-static {v0}, Lcom/amap/api/a/bf$d;->a(Lcom/amap/api/a/bf$d;)Lcom/amap/api/a/b;

    move-result-object v0

    if-nez v0, :cond_1

    .line 892
    :cond_0
    :goto_0
    return-void

    .line 891
    :cond_1
    iget-object v0, p0, Lcom/amap/api/a/bf$a;->h:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-static {v0}, Lcom/amap/api/a/bf$d;->a(Lcom/amap/api/a/bf$d;)Lcom/amap/api/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/a/b;->postInvalidate()V

    goto :goto_0
.end method

.method public a(Landroid/graphics/Canvas;Landroid/graphics/Matrix;FF)V
    .locals 2

    .prologue
    .line 965
    iget-boolean v0, p0, Lcom/amap/api/a/bf$a;->i:Z

    if-eqz v0, :cond_3

    .line 967
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 968
    invoke-virtual {p1, p3, p4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 969
    invoke-virtual {p1, p2}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 970
    invoke-direct {p0, p1}, Lcom/amap/api/a/bf$a;->a(Landroid/graphics/Canvas;)V

    .line 971
    iget-object v0, p0, Lcom/amap/api/a/bf$a;->h:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->g:Lcom/amap/api/a/b;

    iget-object v0, v0, Lcom/amap/api/a/b;->h:Lcom/amap/api/a/bv;

    invoke-virtual {v0}, Lcom/amap/api/a/bv;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 972
    invoke-direct {p0, p1}, Lcom/amap/api/a/bf$a;->b(Landroid/graphics/Canvas;)V

    .line 974
    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/bf$a;->h:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->g:Lcom/amap/api/a/b;

    iget-object v0, v0, Lcom/amap/api/a/b;->h:Lcom/amap/api/a/bv;

    invoke-virtual {v0, p1}, Lcom/amap/api/a/bv;->a(Landroid/graphics/Canvas;)V

    .line 975
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 976
    iget-object v0, p0, Lcom/amap/api/a/bf$a;->h:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->g:Lcom/amap/api/a/b;

    iget-object v0, v0, Lcom/amap/api/a/b;->h:Lcom/amap/api/a/bv;

    invoke-virtual {v0}, Lcom/amap/api/a/bv;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 977
    invoke-direct {p0, p1}, Lcom/amap/api/a/bf$a;->b(Landroid/graphics/Canvas;)V

    .line 980
    :cond_1
    iget-boolean v0, p0, Lcom/amap/api/a/bf$a;->b:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/amap/api/a/bf$a;->c:Z

    if-nez v0, :cond_2

    .line 981
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/amap/api/a/bf$a;->a(Z)V

    .line 982
    iget-object v0, p0, Lcom/amap/api/a/bf$a;->h:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-static {v0}, Lcom/amap/api/a/bf$d;->a(Lcom/amap/api/a/bf$d;)Lcom/amap/api/a/b;

    move-result-object v0

    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    invoke-virtual {v0, v1}, Lcom/amap/api/a/b;->b(Landroid/graphics/Matrix;)Z

    .line 983
    iget-object v0, p0, Lcom/amap/api/a/bf$a;->h:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-static {v0}, Lcom/amap/api/a/bf$d;->a(Lcom/amap/api/a/bf$d;)Lcom/amap/api/a/b;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/amap/api/a/b;->c(F)Z

    .line 984
    iget-object v0, p0, Lcom/amap/api/a/bf$a;->h:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-static {v0}, Lcom/amap/api/a/bf$d;->a(Lcom/amap/api/a/bf$d;)Lcom/amap/api/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/a/b;->I()V

    .line 996
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/amap/api/a/bf$a;->h:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0}, Lcom/amap/api/a/bf$d;->h()V

    .line 997
    invoke-direct {p0, p1}, Lcom/amap/api/a/bf$a;->c(Landroid/graphics/Canvas;)V

    .line 998
    return-void

    .line 991
    :cond_3
    invoke-direct {p0, p1}, Lcom/amap/api/a/bf$a;->a(Landroid/graphics/Canvas;)V

    .line 992
    invoke-direct {p0, p1}, Lcom/amap/api/a/bf$a;->b(Landroid/graphics/Canvas;)V

    .line 993
    iget-object v0, p0, Lcom/amap/api/a/bf$a;->h:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->g:Lcom/amap/api/a/b;

    iget-object v0, v0, Lcom/amap/api/a/b;->h:Lcom/amap/api/a/bv;

    invoke-virtual {v0, p1}, Lcom/amap/api/a/bv;->a(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 926
    iput-boolean p1, p0, Lcom/amap/api/a/bf$a;->i:Z

    .line 927
    return-void
.end method

.method public a(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 1111
    const/4 v0, 0x0

    .line 1112
    return v0
.end method

.method public a(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1134
    const/4 v0, 0x0

    .line 1135
    return v0
.end method

.method a(Lcom/amap/api/a/as;Landroid/content/Context;)Z
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x1

    .line 707
    if-nez p1, :cond_1

    .line 748
    :cond_0
    :goto_0
    return v6

    .line 711
    :cond_1
    iget-object v0, p1, Lcom/amap/api/a/as;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eq v0, v7, :cond_0

    .line 714
    iget-object v0, p1, Lcom/amap/api/a/as;->a:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/amap/api/a/bf$a;->c(Ljava/lang/String;)Z

    move-result v0

    .line 715
    if-eq v0, v7, :cond_0

    .line 719
    new-instance v0, Lcom/amap/api/a/bq;

    invoke-direct {v0}, Lcom/amap/api/a/bq;-><init>()V

    iput-object v0, p1, Lcom/amap/api/a/as;->o:Lcom/amap/api/a/bq;

    .line 720
    new-instance v0, Lcom/amap/api/a/p;

    iget v1, p0, Lcom/amap/api/a/bf$a;->l:I

    iget v2, p0, Lcom/amap/api/a/bf$a;->m:I

    iget-boolean v3, p1, Lcom/amap/api/a/as;->h:Z

    iget-wide v4, p1, Lcom/amap/api/a/as;->i:J

    invoke-direct/range {v0 .. v5}, Lcom/amap/api/a/p;-><init>(IIZJ)V

    iput-object v0, p1, Lcom/amap/api/a/as;->m:Lcom/amap/api/a/p;

    .line 722
    new-instance v0, Lcom/amap/api/a/q;

    iget-object v1, p0, Lcom/amap/api/a/bf$a;->h:Lcom/amap/api/a/bf;

    iget-object v1, v1, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-static {v1}, Lcom/amap/api/a/bf$d;->a(Lcom/amap/api/a/bf$d;)Lcom/amap/api/a/b;

    move-result-object v1

    iget-boolean v1, v1, Lcom/amap/api/a/b;->c:Z

    invoke-direct {v0, p2, v1, p1}, Lcom/amap/api/a/q;-><init>(Landroid/content/Context;ZLcom/amap/api/a/as;)V

    iput-object v0, p1, Lcom/amap/api/a/as;->n:Lcom/amap/api/a/q;

    .line 724
    iget-object v0, p1, Lcom/amap/api/a/as;->n:Lcom/amap/api/a/q;

    iget-object v1, p1, Lcom/amap/api/a/as;->m:Lcom/amap/api/a/p;

    invoke-virtual {v0, v1}, Lcom/amap/api/a/q;->a(Lcom/amap/api/a/p;)V

    .line 727
    iget-object v0, p0, Lcom/amap/api/a/bf$a;->a:Lcom/amap/api/a/bq;

    invoke-virtual {v0}, Lcom/amap/api/a/bq;->size()I

    move-result v0

    .line 728
    iget-boolean v1, p1, Lcom/amap/api/a/as;->e:Z

    if-eqz v1, :cond_2

    if-nez v0, :cond_4

    .line 729
    :cond_2
    iget-object v0, p0, Lcom/amap/api/a/bf$a;->a:Lcom/amap/api/a/bq;

    invoke-virtual {v0, p1}, Lcom/amap/api/a/bq;->add(Ljava/lang/Object;)Z

    move-result v0

    .line 744
    :goto_1
    invoke-direct {p0}, Lcom/amap/api/a/bf$a;->b()V

    .line 745
    iget-boolean v1, p1, Lcom/amap/api/a/as;->f:Z

    if-ne v1, v7, :cond_3

    .line 746
    iget-object v1, p1, Lcom/amap/api/a/as;->a:Ljava/lang/String;

    invoke-virtual {p0, v1, v7}, Lcom/amap/api/a/bf$a;->a(Ljava/lang/String;Z)Z

    :cond_3
    move v6, v0

    .line 748
    goto :goto_0

    .line 733
    :cond_4
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_2
    if-ltz v1, :cond_7

    .line 734
    iget-object v0, p0, Lcom/amap/api/a/bf$a;->a:Lcom/amap/api/a/bq;

    invoke-virtual {v0, v1}, Lcom/amap/api/a/bq;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/a/as;

    .line 735
    if-nez v0, :cond_6

    .line 733
    :cond_5
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_2

    .line 738
    :cond_6
    iget-boolean v0, v0, Lcom/amap/api/a/as;->e:Z

    if-ne v0, v7, :cond_5

    .line 739
    iget-object v0, p0, Lcom/amap/api/a/bf$a;->a:Lcom/amap/api/a/bq;

    invoke-virtual {v0, v1, p1}, Lcom/amap/api/a/bq;->add(ILjava/lang/Object;)V

    move v0, v6

    .line 740
    goto :goto_1

    :cond_7
    move v0, v6

    goto :goto_1
.end method

.method a(Ljava/lang/String;Z)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 573
    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-ne v0, v2, :cond_0

    move v0, v1

    .line 604
    :goto_0
    return v0

    .line 577
    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/bf$a;->a:Lcom/amap/api/a/bq;

    invoke-virtual {v0}, Lcom/amap/api/a/bq;->size()I

    move-result v4

    move v3, v1

    .line 578
    :goto_1
    if-ge v3, v4, :cond_5

    .line 579
    iget-object v0, p0, Lcom/amap/api/a/bf$a;->a:Lcom/amap/api/a/bq;

    invoke-virtual {v0, v3}, Lcom/amap/api/a/bq;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/a/as;

    .line 580
    if-nez v0, :cond_2

    .line 578
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 583
    :cond_2
    iget-object v5, v0, Lcom/amap/api/a/as;->a:Ljava/lang/String;

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-ne v5, v2, :cond_1

    .line 584
    iput-boolean p2, v0, Lcom/amap/api/a/as;->f:Z

    .line 585
    iget-boolean v5, v0, Lcom/amap/api/a/as;->e:Z

    if-nez v5, :cond_3

    move v0, v2

    .line 587
    goto :goto_0

    .line 589
    :cond_3
    if-ne p2, v2, :cond_1

    .line 590
    iget v3, v0, Lcom/amap/api/a/as;->b:I

    iget v4, v0, Lcom/amap/api/a/as;->c:I

    if-le v3, v4, :cond_4

    .line 591
    iget-object v3, p0, Lcom/amap/api/a/bf$a;->h:Lcom/amap/api/a/bf;

    iget-object v3, v3, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    iget v4, v0, Lcom/amap/api/a/as;->b:I

    invoke-virtual {v3, v4}, Lcom/amap/api/a/bf$d;->b(I)V

    .line 593
    iget-object v3, p0, Lcom/amap/api/a/bf$a;->h:Lcom/amap/api/a/bf;

    iget-object v3, v3, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    iget v0, v0, Lcom/amap/api/a/as;->c:I

    invoke-virtual {v3, v0}, Lcom/amap/api/a/bf$d;->c(I)V

    .line 596
    :cond_4
    invoke-direct {p0, p1}, Lcom/amap/api/a/bf$a;->b(Ljava/lang/String;)V

    .line 597
    iget-object v0, p0, Lcom/amap/api/a/bf$a;->h:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0, v1, v1}, Lcom/amap/api/a/bf$d;->a(ZZ)V

    move v0, v2

    .line 598
    goto :goto_0

    :cond_5
    move v0, v1

    .line 604
    goto :goto_0
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 1001
    iput-boolean p1, p0, Lcom/amap/api/a/bf$a;->j:Z

    .line 1002
    return-void
.end method

.method public b(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 1123
    const/4 v0, 0x0

    .line 1124
    return v0
.end method

.method protected b(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    .line 1147
    const/4 v0, 0x0

    .line 1148
    iget-object v1, p0, Lcom/amap/api/a/bf$a;->h:Lcom/amap/api/a/bf;

    iget-object v1, v1, Lcom/amap/api/a/bf;->a:Lcom/amap/api/a/bf$e;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v1, v2, v3}, Lcom/amap/api/a/bf$e;->a(II)Lcom/amap/api/a/ac;

    .line 1151
    return v0
.end method
