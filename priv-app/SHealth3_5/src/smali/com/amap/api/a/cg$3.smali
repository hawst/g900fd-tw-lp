.class Lcom/amap/api/a/cg$3;
.super Ljava/lang/Object;
.source "ZoomControllerView.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/amap/api/a/cg;-><init>(Landroid/content/Context;Lcom/amap/api/a/av;Lcom/amap/api/a/af;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/amap/api/a/cg;


# direct methods
.method constructor <init>(Lcom/amap/api/a/cg;)V
    .locals 0

    .prologue
    .line 123
    iput-object p1, p0, Lcom/amap/api/a/cg$3;->a:Lcom/amap/api/a/cg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 126
    iget-object v0, p0, Lcom/amap/api/a/cg$3;->a:Lcom/amap/api/a/cg;

    invoke-static {v0}, Lcom/amap/api/a/cg;->c(Lcom/amap/api/a/cg;)Lcom/amap/api/a/af;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/a/af;->f()F

    move-result v0

    iget-object v1, p0, Lcom/amap/api/a/cg$3;->a:Lcom/amap/api/a/cg;

    invoke-static {v1}, Lcom/amap/api/a/cg;->c(Lcom/amap/api/a/cg;)Lcom/amap/api/a/af;

    move-result-object v1

    invoke-interface {v1}, Lcom/amap/api/a/af;->h()F

    move-result v1

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_1

    .line 141
    :cond_0
    :goto_0
    return v2

    .line 129
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_2

    .line 130
    iget-object v0, p0, Lcom/amap/api/a/cg$3;->a:Lcom/amap/api/a/cg;

    invoke-static {v0}, Lcom/amap/api/a/cg;->e(Lcom/amap/api/a/cg;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/amap/api/a/cg$3;->a:Lcom/amap/api/a/cg;

    invoke-static {v1}, Lcom/amap/api/a/cg;->i(Lcom/amap/api/a/cg;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 131
    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 132
    iget-object v0, p0, Lcom/amap/api/a/cg$3;->a:Lcom/amap/api/a/cg;

    invoke-static {v0}, Lcom/amap/api/a/cg;->e(Lcom/amap/api/a/cg;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/amap/api/a/cg$3;->a:Lcom/amap/api/a/cg;

    invoke-static {v1}, Lcom/amap/api/a/cg;->f(Lcom/amap/api/a/cg;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 136
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/a/cg$3;->a:Lcom/amap/api/a/cg;

    invoke-static {v0}, Lcom/amap/api/a/cg;->c(Lcom/amap/api/a/cg;)Lcom/amap/api/a/af;

    move-result-object v0

    invoke-static {}, Lcom/amap/api/a/t;->b()Lcom/amap/api/a/t;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/amap/api/a/af;->b(Lcom/amap/api/a/t;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 137
    :catch_0
    move-exception v0

    .line 138
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
