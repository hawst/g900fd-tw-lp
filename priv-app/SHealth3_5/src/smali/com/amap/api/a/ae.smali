.class public Lcom/amap/api/a/ae;
.super Ljava/lang/Object;
.source "GroundOverlayDelegateImp.java"

# interfaces
.implements Lcom/amap/api/a/ah;


# instance fields
.field final a:D

.field final b:D

.field private c:Lcom/amap/api/a/b;

.field private d:Lcom/amap/api/maps2d/model/BitmapDescriptor;

.field private e:Lcom/amap/api/maps2d/model/LatLng;

.field private f:F

.field private g:F

.field private h:Lcom/amap/api/maps2d/model/LatLngBounds;

.field private i:F

.field private j:F

.field private k:Z

.field private l:F

.field private m:F

.field private n:F

.field private o:Ljava/lang/String;

.field private p:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Lcom/amap/api/a/b;)V
    .locals 3

    .prologue
    const/high16 v2, 0x3f000000    # 0.5f

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-wide v0, 0x3f91df46a2529d37L    # 0.01745329251994329

    iput-wide v0, p0, Lcom/amap/api/a/ae;->a:D

    .line 23
    const-wide v0, 0x41584dae328f5c29L    # 6371000.79

    iput-wide v0, p0, Lcom/amap/api/a/ae;->b:D

    .line 32
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/a/ae;->k:Z

    .line 33
    const/4 v0, 0x0

    iput v0, p0, Lcom/amap/api/a/ae;->l:F

    .line 34
    iput v2, p0, Lcom/amap/api/a/ae;->m:F

    .line 35
    iput v2, p0, Lcom/amap/api/a/ae;->n:F

    .line 40
    iput-object p1, p0, Lcom/amap/api/a/ae;->c:Lcom/amap/api/a/b;

    .line 42
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/a/ae;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/ae;->o:Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 46
    :goto_0
    return-void

    .line 43
    :catch_0
    move-exception v0

    .line 44
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method private b(Lcom/amap/api/maps2d/model/LatLng;)Lcom/amap/api/a/ac;
    .locals 6

    .prologue
    const-wide v4, 0x412e848000000000L    # 1000000.0

    .line 331
    if-nez p1, :cond_0

    .line 332
    const/4 v0, 0x0

    .line 337
    :goto_0
    return-object v0

    .line 334
    :cond_0
    iget-wide v0, p1, Lcom/amap/api/maps2d/model/LatLng;->latitude:D

    mul-double/2addr v0, v4

    double-to-int v1, v0

    .line 335
    iget-wide v2, p1, Lcom/amap/api/maps2d/model/LatLng;->longitude:D

    mul-double/2addr v2, v4

    double-to-int v2, v2

    .line 336
    new-instance v0, Lcom/amap/api/a/ac;

    invoke-direct {v0, v1, v2}, Lcom/amap/api/a/ac;-><init>(II)V

    goto :goto_0
.end method

.method private o()V
    .locals 13

    .prologue
    const/high16 v12, 0x3f800000    # 1.0f

    const-wide v6, 0x3f91df46a2529d37L    # 0.01745329251994329

    .line 105
    iget v0, p0, Lcom/amap/api/a/ae;->f:F

    float-to-double v0, v0

    const-wide v2, 0x41584dae328f5c29L    # 6371000.79

    iget-object v4, p0, Lcom/amap/api/a/ae;->e:Lcom/amap/api/maps2d/model/LatLng;

    iget-wide v4, v4, Lcom/amap/api/maps2d/model/LatLng;->latitude:D

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    mul-double/2addr v2, v6

    div-double/2addr v0, v2

    .line 107
    iget v2, p0, Lcom/amap/api/a/ae;->g:F

    float-to-double v2, v2

    const-wide v4, 0x40fb25af0c031ddeL    # 111194.94043265979

    div-double/2addr v2, v4

    .line 109
    new-instance v4, Lcom/amap/api/maps2d/model/LatLngBounds;

    new-instance v5, Lcom/amap/api/maps2d/model/LatLng;

    iget-object v6, p0, Lcom/amap/api/a/ae;->e:Lcom/amap/api/maps2d/model/LatLng;

    iget-wide v6, v6, Lcom/amap/api/maps2d/model/LatLng;->latitude:D

    iget v8, p0, Lcom/amap/api/a/ae;->n:F

    sub-float v8, v12, v8

    float-to-double v8, v8

    mul-double/2addr v8, v2

    sub-double/2addr v6, v8

    iget-object v8, p0, Lcom/amap/api/a/ae;->e:Lcom/amap/api/maps2d/model/LatLng;

    iget-wide v8, v8, Lcom/amap/api/maps2d/model/LatLng;->longitude:D

    iget v10, p0, Lcom/amap/api/a/ae;->m:F

    float-to-double v10, v10

    mul-double/2addr v10, v0

    sub-double/2addr v8, v10

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/amap/api/maps2d/model/LatLng;-><init>(DD)V

    new-instance v6, Lcom/amap/api/maps2d/model/LatLng;

    iget-object v7, p0, Lcom/amap/api/a/ae;->e:Lcom/amap/api/maps2d/model/LatLng;

    iget-wide v7, v7, Lcom/amap/api/maps2d/model/LatLng;->latitude:D

    iget v9, p0, Lcom/amap/api/a/ae;->n:F

    float-to-double v9, v9

    mul-double/2addr v2, v9

    add-double/2addr v2, v7

    iget-object v7, p0, Lcom/amap/api/a/ae;->e:Lcom/amap/api/maps2d/model/LatLng;

    iget-wide v7, v7, Lcom/amap/api/maps2d/model/LatLng;->longitude:D

    iget v9, p0, Lcom/amap/api/a/ae;->m:F

    sub-float v9, v12, v9

    float-to-double v9, v9

    mul-double/2addr v0, v9

    add-double/2addr v0, v7

    invoke-direct {v6, v2, v3, v0, v1}, Lcom/amap/api/maps2d/model/LatLng;-><init>(DD)V

    invoke-direct {v4, v5, v6}, Lcom/amap/api/maps2d/model/LatLngBounds;-><init>(Lcom/amap/api/maps2d/model/LatLng;Lcom/amap/api/maps2d/model/LatLng;)V

    iput-object v4, p0, Lcom/amap/api/a/ae;->h:Lcom/amap/api/maps2d/model/LatLngBounds;

    .line 114
    return-void
.end method

.method private p()V
    .locals 13

    .prologue
    .line 117
    iget-object v0, p0, Lcom/amap/api/a/ae;->h:Lcom/amap/api/maps2d/model/LatLngBounds;

    iget-object v0, v0, Lcom/amap/api/maps2d/model/LatLngBounds;->southwest:Lcom/amap/api/maps2d/model/LatLng;

    .line 118
    iget-object v1, p0, Lcom/amap/api/a/ae;->h:Lcom/amap/api/maps2d/model/LatLngBounds;

    iget-object v1, v1, Lcom/amap/api/maps2d/model/LatLngBounds;->northeast:Lcom/amap/api/maps2d/model/LatLng;

    .line 120
    new-instance v2, Lcom/amap/api/maps2d/model/LatLng;

    iget-wide v3, v0, Lcom/amap/api/maps2d/model/LatLng;->latitude:D

    const/high16 v5, 0x3f800000    # 1.0f

    iget v6, p0, Lcom/amap/api/a/ae;->n:F

    sub-float/2addr v5, v6

    float-to-double v5, v5

    iget-wide v7, v1, Lcom/amap/api/maps2d/model/LatLng;->latitude:D

    iget-wide v9, v0, Lcom/amap/api/maps2d/model/LatLng;->latitude:D

    sub-double/2addr v7, v9

    mul-double/2addr v5, v7

    add-double/2addr v3, v5

    iget-wide v5, v0, Lcom/amap/api/maps2d/model/LatLng;->longitude:D

    iget v7, p0, Lcom/amap/api/a/ae;->m:F

    float-to-double v7, v7

    iget-wide v9, v1, Lcom/amap/api/maps2d/model/LatLng;->longitude:D

    iget-wide v11, v0, Lcom/amap/api/maps2d/model/LatLng;->longitude:D

    sub-double/2addr v9, v11

    mul-double/2addr v7, v9

    add-double/2addr v5, v7

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/amap/api/maps2d/model/LatLng;-><init>(DD)V

    iput-object v2, p0, Lcom/amap/api/a/ae;->e:Lcom/amap/api/maps2d/model/LatLng;

    .line 125
    const-wide v2, 0x41584dae328f5c29L    # 6371000.79

    iget-object v4, p0, Lcom/amap/api/a/ae;->e:Lcom/amap/api/maps2d/model/LatLng;

    iget-wide v4, v4, Lcom/amap/api/maps2d/model/LatLng;->latitude:D

    const-wide v6, 0x3f91df46a2529d37L    # 0.01745329251994329

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    iget-wide v4, v1, Lcom/amap/api/maps2d/model/LatLng;->longitude:D

    iget-wide v6, v0, Lcom/amap/api/maps2d/model/LatLng;->longitude:D

    sub-double/2addr v4, v6

    mul-double/2addr v2, v4

    const-wide v4, 0x3f91df46a2529d37L    # 0.01745329251994329

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Lcom/amap/api/a/ae;->f:F

    .line 127
    const-wide v2, 0x41584dae328f5c29L    # 6371000.79

    iget-wide v4, v1, Lcom/amap/api/maps2d/model/LatLng;->latitude:D

    iget-wide v0, v0, Lcom/amap/api/maps2d/model/LatLng;->latitude:D

    sub-double v0, v4, v0

    mul-double/2addr v0, v2

    const-wide v2, 0x3f91df46a2529d37L    # 0.01745329251994329

    mul-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Lcom/amap/api/a/ae;->g:F

    .line 128
    return-void
.end method

.method private q()V
    .locals 4

    .prologue
    .line 132
    iget-object v0, p0, Lcom/amap/api/a/ae;->d:Lcom/amap/api/maps2d/model/BitmapDescriptor;

    if-nez v0, :cond_0

    .line 140
    :goto_0
    return-void

    .line 134
    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/ae;->d:Lcom/amap/api/maps2d/model/BitmapDescriptor;

    invoke-virtual {v0}, Lcom/amap/api/maps2d/model/BitmapDescriptor;->getWidth()I

    move-result v0

    .line 135
    iget-object v1, p0, Lcom/amap/api/a/ae;->d:Lcom/amap/api/maps2d/model/BitmapDescriptor;

    invoke-virtual {v1}, Lcom/amap/api/maps2d/model/BitmapDescriptor;->getHeight()I

    move-result v1

    .line 136
    iget-object v2, p0, Lcom/amap/api/a/ae;->d:Lcom/amap/api/maps2d/model/BitmapDescriptor;

    invoke-virtual {v2}, Lcom/amap/api/maps2d/model/BitmapDescriptor;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    .line 137
    iget-object v3, p0, Lcom/amap/api/a/ae;->d:Lcom/amap/api/maps2d/model/BitmapDescriptor;

    invoke-virtual {v3}, Lcom/amap/api/maps2d/model/BitmapDescriptor;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 138
    int-to-float v0, v0

    int-to-float v3, v3

    div-float/2addr v0, v3

    .line 139
    int-to-float v0, v1

    int-to-float v1, v2

    div-float/2addr v0, v1

    .line 140
    goto :goto_0
.end method


# virtual methods
.method public a(F)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 63
    iput p1, p0, Lcom/amap/api/a/ae;->j:F

    .line 64
    return-void
.end method

.method public a(FF)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 208
    cmpl-float v0, p1, v4

    if-ltz v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Width must be non-negative"

    invoke-static {v0, v3}, Lcom/amap/api/a/a/a;->b(ZLjava/lang/Object;)V

    .line 210
    cmpl-float v0, p2, v4

    if-ltz v0, :cond_1

    :goto_1
    const-string v0, "Height must be non-negative"

    invoke-static {v1, v0}, Lcom/amap/api/a/a/a;->b(ZLjava/lang/Object;)V

    .line 212
    iget v0, p0, Lcom/amap/api/a/ae;->f:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/amap/api/a/ae;->g:F

    cmpl-float v0, v0, p2

    if-eqz v0, :cond_2

    .line 213
    iput p1, p0, Lcom/amap/api/a/ae;->f:F

    .line 214
    iput p2, p0, Lcom/amap/api/a/ae;->g:F

    .line 215
    invoke-direct {p0}, Lcom/amap/api/a/ae;->o()V

    .line 220
    :goto_2
    return-void

    :cond_0
    move v0, v2

    .line 208
    goto :goto_0

    :cond_1
    move v1, v2

    .line 210
    goto :goto_1

    .line 217
    :cond_2
    iput p1, p0, Lcom/amap/api/a/ae;->f:F

    .line 218
    iput p2, p0, Lcom/amap/api/a/ae;->g:F

    goto :goto_2
.end method

.method public a(Landroid/graphics/Canvas;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/high16 v6, 0x437f0000    # 255.0f

    const/4 v1, 0x0

    .line 293
    iget-boolean v0, p0, Lcom/amap/api/a/ae;->k:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/amap/api/a/ae;->e:Lcom/amap/api/maps2d/model/LatLng;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/a/ae;->h:Lcom/amap/api/maps2d/model/LatLngBounds;

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/ae;->d:Lcom/amap/api/maps2d/model/BitmapDescriptor;

    if-nez v0, :cond_2

    .line 327
    :cond_1
    :goto_0
    return-void

    .line 298
    :cond_2
    invoke-virtual {p0}, Lcom/amap/api/a/ae;->g()V

    .line 299
    iget v0, p0, Lcom/amap/api/a/ae;->f:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_3

    iget v0, p0, Lcom/amap/api/a/ae;->g:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1

    .line 302
    :cond_3
    iget-object v0, p0, Lcom/amap/api/a/ae;->d:Lcom/amap/api/maps2d/model/BitmapDescriptor;

    invoke-virtual {v0}, Lcom/amap/api/maps2d/model/BitmapDescriptor;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/ae;->p:Landroid/graphics/Bitmap;

    .line 303
    iget-object v0, p0, Lcom/amap/api/a/ae;->p:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/amap/api/a/ae;->p:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 305
    iget-object v0, p0, Lcom/amap/api/a/ae;->h:Lcom/amap/api/maps2d/model/LatLngBounds;

    iget-object v0, v0, Lcom/amap/api/maps2d/model/LatLngBounds;->southwest:Lcom/amap/api/maps2d/model/LatLng;

    .line 306
    iget-object v1, p0, Lcom/amap/api/a/ae;->h:Lcom/amap/api/maps2d/model/LatLngBounds;

    iget-object v1, v1, Lcom/amap/api/maps2d/model/LatLngBounds;->northeast:Lcom/amap/api/maps2d/model/LatLng;

    .line 307
    invoke-direct {p0, v0}, Lcom/amap/api/a/ae;->b(Lcom/amap/api/maps2d/model/LatLng;)Lcom/amap/api/a/ac;

    move-result-object v0

    .line 308
    invoke-direct {p0, v1}, Lcom/amap/api/a/ae;->b(Lcom/amap/api/maps2d/model/LatLng;)Lcom/amap/api/a/ac;

    move-result-object v1

    .line 309
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 310
    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3}, Landroid/graphics/Point;-><init>()V

    .line 312
    iget-object v4, p0, Lcom/amap/api/a/ae;->c:Lcom/amap/api/a/b;

    invoke-virtual {v4}, Lcom/amap/api/a/b;->s()Lcom/amap/api/a/bl;

    move-result-object v4

    invoke-interface {v4, v0, v2}, Lcom/amap/api/a/bl;->a(Lcom/amap/api/a/ac;Landroid/graphics/Point;)Landroid/graphics/Point;

    .line 313
    iget-object v0, p0, Lcom/amap/api/a/ae;->c:Lcom/amap/api/a/b;

    invoke-virtual {v0}, Lcom/amap/api/a/b;->s()Lcom/amap/api/a/bl;

    move-result-object v0

    invoke-interface {v0, v1, v3}, Lcom/amap/api/a/bl;->a(Lcom/amap/api/a/ac;Landroid/graphics/Point;)Landroid/graphics/Point;

    .line 314
    iget-object v0, p0, Lcom/amap/api/a/ae;->c:Lcom/amap/api/a/b;

    invoke-virtual {v0}, Lcom/amap/api/a/b;->s()Lcom/amap/api/a/bl;

    move-result-object v0

    iget v1, p0, Lcom/amap/api/a/ae;->f:F

    invoke-interface {v0, v1}, Lcom/amap/api/a/bl;->a(F)F

    move-result v0

    .line 316
    iget-object v1, p0, Lcom/amap/api/a/ae;->c:Lcom/amap/api/a/b;

    invoke-virtual {v1}, Lcom/amap/api/a/b;->s()Lcom/amap/api/a/bl;

    move-result-object v1

    iget v4, p0, Lcom/amap/api/a/ae;->g:F

    invoke-interface {v1, v4}, Lcom/amap/api/a/bl;->a(F)F

    move-result v1

    .line 318
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    .line 319
    iget v5, v2, Landroid/graphics/Point;->x:I

    int-to-float v5, v5

    add-float/2addr v0, v5

    float-to-int v0, v0

    .line 320
    iget v5, v3, Landroid/graphics/Point;->y:I

    int-to-float v5, v5

    add-float/2addr v1, v5

    float-to-int v1, v1

    .line 321
    new-instance v5, Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v3, v3

    int-to-float v0, v0

    int-to-float v1, v1

    invoke-direct {v5, v2, v3, v0, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 322
    iget v0, p0, Lcom/amap/api/a/ae;->l:F

    mul-float/2addr v0, v6

    sub-float v0, v6, v0

    float-to-int v0, v0

    .line 323
    invoke-virtual {v4, v0}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 324
    const/4 v0, 0x1

    invoke-virtual {v4, v0}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 325
    iget-object v0, p0, Lcom/amap/api/a/ae;->p:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1, v5, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto/16 :goto_0
.end method

.method public a(Lcom/amap/api/maps2d/model/BitmapDescriptor;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 281
    iput-object p1, p0, Lcom/amap/api/a/ae;->d:Lcom/amap/api/maps2d/model/BitmapDescriptor;

    .line 282
    invoke-direct {p0}, Lcom/amap/api/a/ae;->q()V

    .line 283
    return-void
.end method

.method public a(Lcom/amap/api/maps2d/model/LatLng;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 178
    iget-object v0, p0, Lcom/amap/api/a/ae;->e:Lcom/amap/api/maps2d/model/LatLng;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/a/ae;->e:Lcom/amap/api/maps2d/model/LatLng;

    invoke-virtual {v0, p1}, Lcom/amap/api/maps2d/model/LatLng;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 179
    iput-object p1, p0, Lcom/amap/api/a/ae;->e:Lcom/amap/api/maps2d/model/LatLng;

    .line 180
    invoke-direct {p0}, Lcom/amap/api/a/ae;->o()V

    .line 185
    :goto_0
    return-void

    .line 182
    :cond_0
    iput-object p1, p0, Lcom/amap/api/a/ae;->e:Lcom/amap/api/maps2d/model/LatLng;

    goto :goto_0
.end method

.method public a(Lcom/amap/api/maps2d/model/LatLngBounds;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 235
    iget-object v0, p0, Lcom/amap/api/a/ae;->h:Lcom/amap/api/maps2d/model/LatLngBounds;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/a/ae;->h:Lcom/amap/api/maps2d/model/LatLngBounds;

    invoke-virtual {v0, p1}, Lcom/amap/api/maps2d/model/LatLngBounds;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 237
    iput-object p1, p0, Lcom/amap/api/a/ae;->h:Lcom/amap/api/maps2d/model/LatLngBounds;

    .line 238
    invoke-direct {p0}, Lcom/amap/api/a/ae;->p()V

    .line 242
    :goto_0
    return-void

    .line 240
    :cond_0
    iput-object p1, p0, Lcom/amap/api/a/ae;->h:Lcom/amap/api/maps2d/model/LatLngBounds;

    goto :goto_0
.end method

.method public a(Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 73
    iput-boolean p1, p0, Lcom/amap/api/a/ae;->k:Z

    .line 74
    return-void
.end method

.method public a()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 165
    iget-object v2, p0, Lcom/amap/api/a/ae;->h:Lcom/amap/api/maps2d/model/LatLngBounds;

    if-nez v2, :cond_1

    .line 172
    :cond_0
    :goto_0
    return v0

    .line 168
    :cond_1
    iget-object v2, p0, Lcom/amap/api/a/ae;->c:Lcom/amap/api/a/b;

    invoke-virtual {v2}, Lcom/amap/api/a/b;->x()Lcom/amap/api/maps2d/model/LatLngBounds;

    move-result-object v2

    .line 169
    if-nez v2, :cond_2

    move v0, v1

    .line 170
    goto :goto_0

    .line 172
    :cond_2
    iget-object v3, p0, Lcom/amap/api/a/ae;->h:Lcom/amap/api/maps2d/model/LatLngBounds;

    invoke-virtual {v2, v3}, Lcom/amap/api/maps2d/model/LatLngBounds;->contains(Lcom/amap/api/maps2d/model/LatLngBounds;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/amap/api/a/ae;->h:Lcom/amap/api/maps2d/model/LatLngBounds;

    invoke-virtual {v3, v2}, Lcom/amap/api/maps2d/model/LatLngBounds;->intersects(Lcom/amap/api/maps2d/model/LatLngBounds;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public a(Lcom/amap/api/a/ak;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 84
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Lcom/amap/api/a/ak;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/amap/api/a/ae;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 86
    :cond_0
    const/4 v0, 0x1

    .line 88
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 50
    iget-object v0, p0, Lcom/amap/api/a/ae;->c:Lcom/amap/api/a/b;

    invoke-virtual {p0}, Lcom/amap/api/a/ae;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/a/b;->a(Ljava/lang/String;)Z

    .line 51
    return-void
.end method

.method public b(F)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 194
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Width must be non-negative"

    invoke-static {v0, v1}, Lcom/amap/api/a/a/a;->b(ZLjava/lang/Object;)V

    .line 196
    iget v0, p0, Lcom/amap/api/a/ae;->f:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_1

    .line 197
    iput p1, p0, Lcom/amap/api/a/ae;->f:F

    .line 198
    iput p1, p0, Lcom/amap/api/a/ae;->g:F

    .line 199
    invoke-direct {p0}, Lcom/amap/api/a/ae;->o()V

    .line 204
    :goto_1
    return-void

    .line 194
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 201
    :cond_1
    iput p1, p0, Lcom/amap/api/a/ae;->f:F

    .line 202
    iput p1, p0, Lcom/amap/api/a/ae;->g:F

    goto :goto_1
.end method

.method public b(FF)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 287
    iput p1, p0, Lcom/amap/api/a/ae;->m:F

    .line 288
    iput p2, p0, Lcom/amap/api/a/ae;->n:F

    .line 289
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lcom/amap/api/a/ae;->o:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 56
    const-string v0, "GroundOverlay"

    invoke-static {v0}, Lcom/amap/api/a/ab;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/ae;->o:Ljava/lang/String;

    .line 58
    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/ae;->o:Ljava/lang/String;

    return-object v0
.end method

.method public c(F)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/high16 v1, 0x43b40000    # 360.0f

    .line 251
    neg-float v0, p1

    rem-float/2addr v0, v1

    add-float/2addr v0, v1

    rem-float/2addr v0, v1

    .line 252
    iget v1, p0, Lcom/amap/api/a/ae;->i:F

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v1

    float-to-double v3, v0

    invoke-static {v3, v4}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    .line 254
    iput v0, p0, Lcom/amap/api/a/ae;->i:F

    .line 258
    :goto_0
    return-void

    .line 256
    :cond_0
    iput v0, p0, Lcom/amap/api/a/ae;->i:F

    goto :goto_0
.end method

.method public d()F
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 68
    iget v0, p0, Lcom/amap/api/a/ae;->j:F

    return v0
.end method

.method public d(F)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 267
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Transparency must be in the range [0..1]"

    invoke-static {v0, v1}, Lcom/amap/api/a/a/a;->b(ZLjava/lang/Object;)V

    .line 270
    iput p1, p0, Lcom/amap/api/a/ae;->l:F

    .line 271
    return-void

    .line 267
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/amap/api/a/ae;->k:Z

    return v0
.end method

.method public f()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 93
    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public g()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 97
    iget-object v0, p0, Lcom/amap/api/a/ae;->e:Lcom/amap/api/maps2d/model/LatLng;

    if-nez v0, :cond_1

    .line 98
    invoke-direct {p0}, Lcom/amap/api/a/ae;->p()V

    .line 102
    :cond_0
    :goto_0
    return-void

    .line 99
    :cond_1
    iget-object v0, p0, Lcom/amap/api/a/ae;->h:Lcom/amap/api/maps2d/model/LatLngBounds;

    if-nez v0, :cond_0

    .line 100
    invoke-direct {p0}, Lcom/amap/api/a/ae;->o()V

    goto :goto_0
.end method

.method public h()Lcom/amap/api/maps2d/model/LatLng;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 189
    iget-object v0, p0, Lcom/amap/api/a/ae;->e:Lcom/amap/api/maps2d/model/LatLng;

    return-object v0
.end method

.method public i()F
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 224
    iget v0, p0, Lcom/amap/api/a/ae;->f:F

    return v0
.end method

.method public j()F
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 229
    iget v0, p0, Lcom/amap/api/a/ae;->g:F

    return v0
.end method

.method public k()Lcom/amap/api/maps2d/model/LatLngBounds;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 246
    iget-object v0, p0, Lcom/amap/api/a/ae;->h:Lcom/amap/api/maps2d/model/LatLngBounds;

    return-object v0
.end method

.method public l()F
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 262
    iget v0, p0, Lcom/amap/api/a/ae;->i:F

    return v0
.end method

.method public m()V
    .locals 2

    .prologue
    .line 146
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/a/ae;->b()V

    .line 147
    iget-object v0, p0, Lcom/amap/api/a/ae;->d:Lcom/amap/api/maps2d/model/BitmapDescriptor;

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/amap/api/a/ae;->d:Lcom/amap/api/maps2d/model/BitmapDescriptor;

    invoke-virtual {v0}, Lcom/amap/api/maps2d/model/BitmapDescriptor;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 149
    if-eqz v0, :cond_0

    .line 150
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 152
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/a/ae;->d:Lcom/amap/api/maps2d/model/BitmapDescriptor;

    .line 155
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/a/ae;->e:Lcom/amap/api/maps2d/model/LatLng;

    .line 156
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/a/ae;->h:Lcom/amap/api/maps2d/model/LatLngBounds;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 161
    :goto_0
    return-void

    .line 157
    :catch_0
    move-exception v0

    .line 158
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 159
    const-string v0, "destroy erro"

    const-string v1, "GroundOverlayDelegateImp destroy"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public n()F
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 275
    iget v0, p0, Lcom/amap/api/a/ae;->l:F

    return v0
.end method
