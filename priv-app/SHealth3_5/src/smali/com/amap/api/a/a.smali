.class Lcom/amap/api/a/a;
.super Ljava/lang/Object;
.source "AMapCallback.java"


# instance fields
.field a:Lcom/autonavi/amap/mapcore2d/IPoint;

.field b:I

.field c:Lcom/autonavi/amap/mapcore2d/IPoint;

.field private d:Lcom/amap/api/a/b;


# direct methods
.method public constructor <init>(Lcom/amap/api/a/b;)V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Lcom/autonavi/amap/mapcore2d/IPoint;

    invoke-direct {v0}, Lcom/autonavi/amap/mapcore2d/IPoint;-><init>()V

    iput-object v0, p0, Lcom/amap/api/a/a;->a:Lcom/autonavi/amap/mapcore2d/IPoint;

    .line 26
    new-instance v0, Lcom/autonavi/amap/mapcore2d/IPoint;

    invoke-direct {v0}, Lcom/autonavi/amap/mapcore2d/IPoint;-><init>()V

    iput-object v0, p0, Lcom/amap/api/a/a;->c:Lcom/autonavi/amap/mapcore2d/IPoint;

    .line 19
    iput-object p1, p0, Lcom/amap/api/a/a;->d:Lcom/amap/api/a/b;

    .line 20
    return-void
.end method

.method private a(I)I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 123
    iget-object v0, p0, Lcom/amap/api/a/a;->d:Lcom/amap/api/a/b;

    invoke-virtual {v0}, Lcom/amap/api/a/b;->s()Lcom/amap/api/a/bl;

    move-result-object v0

    .line 124
    invoke-interface {v0, v3, v3}, Lcom/amap/api/a/bl;->a(II)Lcom/amap/api/a/ac;

    move-result-object v1

    .line 125
    const/16 v2, 0xa

    invoke-interface {v0, v3, v2}, Lcom/amap/api/a/bl;->a(II)Lcom/amap/api/a/ac;

    move-result-object v0

    .line 126
    invoke-virtual {v1}, Lcom/amap/api/a/ac;->b()I

    move-result v1

    invoke-virtual {v0}, Lcom/amap/api/a/ac;->b()I

    move-result v0

    sub-int v0, v1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 128
    mul-int/2addr v0, p1

    div-int/lit8 v0, v0, 0xa

    return v0
.end method

.method private b(Lcom/amap/api/a/t;)V
    .locals 13

    .prologue
    const-wide/high16 v11, 0x4000000000000000L    # 2.0

    const-wide v9, 0x412e848000000000L    # 1000000.0

    .line 107
    iget-object v0, p1, Lcom/amap/api/a/t;->i:Lcom/amap/api/maps2d/model/LatLngBounds;

    .line 108
    iget v1, p1, Lcom/amap/api/a/t;->j:I

    .line 109
    iget-object v2, v0, Lcom/amap/api/maps2d/model/LatLngBounds;->northeast:Lcom/amap/api/maps2d/model/LatLng;

    iget-wide v2, v2, Lcom/amap/api/maps2d/model/LatLng;->latitude:D

    mul-double/2addr v2, v9

    iget-object v4, v0, Lcom/amap/api/maps2d/model/LatLngBounds;->southwest:Lcom/amap/api/maps2d/model/LatLng;

    iget-wide v4, v4, Lcom/amap/api/maps2d/model/LatLng;->latitude:D

    mul-double/2addr v4, v9

    sub-double/2addr v2, v4

    double-to-int v2, v2

    .line 110
    iget-object v3, v0, Lcom/amap/api/maps2d/model/LatLngBounds;->northeast:Lcom/amap/api/maps2d/model/LatLng;

    iget-wide v3, v3, Lcom/amap/api/maps2d/model/LatLng;->longitude:D

    mul-double/2addr v3, v9

    iget-object v5, v0, Lcom/amap/api/maps2d/model/LatLngBounds;->southwest:Lcom/amap/api/maps2d/model/LatLng;

    iget-wide v5, v5, Lcom/amap/api/maps2d/model/LatLng;->longitude:D

    mul-double/2addr v5, v9

    sub-double/2addr v3, v5

    double-to-int v3, v3

    .line 111
    iget-object v4, v0, Lcom/amap/api/maps2d/model/LatLngBounds;->northeast:Lcom/amap/api/maps2d/model/LatLng;

    iget-wide v4, v4, Lcom/amap/api/maps2d/model/LatLng;->latitude:D

    mul-double/2addr v4, v9

    iget-object v6, v0, Lcom/amap/api/maps2d/model/LatLngBounds;->southwest:Lcom/amap/api/maps2d/model/LatLng;

    iget-wide v6, v6, Lcom/amap/api/maps2d/model/LatLng;->latitude:D

    mul-double/2addr v6, v9

    add-double/2addr v4, v6

    div-double/2addr v4, v11

    double-to-int v4, v4

    .line 112
    iget-object v5, v0, Lcom/amap/api/maps2d/model/LatLngBounds;->northeast:Lcom/amap/api/maps2d/model/LatLng;

    iget-wide v5, v5, Lcom/amap/api/maps2d/model/LatLng;->longitude:D

    mul-double/2addr v5, v9

    iget-object v0, v0, Lcom/amap/api/maps2d/model/LatLngBounds;->southwest:Lcom/amap/api/maps2d/model/LatLng;

    iget-wide v7, v0, Lcom/amap/api/maps2d/model/LatLng;->longitude:D

    mul-double/2addr v7, v9

    add-double/2addr v5, v7

    div-double/2addr v5, v11

    double-to-int v0, v5

    .line 113
    new-instance v5, Lcom/amap/api/a/ac;

    invoke-direct {v5, v4, v0}, Lcom/amap/api/a/ac;-><init>(II)V

    .line 114
    iget-object v0, p0, Lcom/amap/api/a/a;->d:Lcom/amap/api/a/b;

    invoke-virtual {v0}, Lcom/amap/api/a/b;->D()Lcom/amap/api/a/av;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/amap/api/a/av;->a(Lcom/amap/api/a/ac;)V

    .line 115
    iget-object v0, p0, Lcom/amap/api/a/a;->d:Lcom/amap/api/a/b;

    invoke-virtual {v0}, Lcom/amap/api/a/b;->D()Lcom/amap/api/a/av;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lcom/amap/api/a/av;->a(II)V

    .line 116
    invoke-direct {p0, v1}, Lcom/amap/api/a/a;->a(I)I

    .line 120
    return-void
.end method


# virtual methods
.method protected a(Lcom/amap/api/a/t;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const-wide v5, 0x412e848000000000L    # 1000000.0

    .line 134
    iget-object v0, p0, Lcom/amap/api/a/a;->d:Lcom/amap/api/a/b;

    invoke-virtual {v0}, Lcom/amap/api/a/b;->f()F

    move-result v0

    float-to-int v0, v0

    .line 140
    iget-object v1, p1, Lcom/amap/api/a/t;->a:Lcom/amap/api/a/t$a;

    sget-object v2, Lcom/amap/api/a/t$a;->h:Lcom/amap/api/a/t$a;

    if-ne v1, v2, :cond_1

    .line 141
    iget-object v1, p0, Lcom/amap/api/a/a;->d:Lcom/amap/api/a/b;

    iget-object v1, v1, Lcom/amap/api/a/b;->a:Lcom/amap/api/a/av;

    iget v2, p1, Lcom/amap/api/a/t;->b:F

    float-to-int v2, v2

    iget v3, p1, Lcom/amap/api/a/t;->c:F

    float-to-int v3, v3

    invoke-virtual {v1, v2, v3}, Lcom/amap/api/a/av;->c(II)V

    .line 144
    iget-object v1, p0, Lcom/amap/api/a/a;->d:Lcom/amap/api/a/b;

    invoke-virtual {v1}, Lcom/amap/api/a/b;->postInvalidate()V

    .line 179
    :goto_0
    iget v1, p0, Lcom/amap/api/a/a;->b:I

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/amap/api/a/a;->d:Lcom/amap/api/a/b;

    invoke-virtual {v0}, Lcom/amap/api/a/b;->q()Lcom/amap/api/a/aq;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/a/aq;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lcom/amap/api/a/a;->d:Lcom/amap/api/a/b;

    invoke-virtual {v0}, Lcom/amap/api/a/b;->K()V

    .line 183
    :cond_0
    return-void

    .line 145
    :cond_1
    iget-object v1, p1, Lcom/amap/api/a/t;->a:Lcom/amap/api/a/t$a;

    sget-object v2, Lcom/amap/api/a/t$a;->b:Lcom/amap/api/a/t$a;

    if-ne v1, v2, :cond_2

    .line 146
    iget-object v1, p0, Lcom/amap/api/a/a;->d:Lcom/amap/api/a/b;

    invoke-virtual {v1}, Lcom/amap/api/a/b;->D()Lcom/amap/api/a/av;

    move-result-object v1

    invoke-virtual {v1}, Lcom/amap/api/a/av;->c()Z

    goto :goto_0

    .line 147
    :cond_2
    iget-object v1, p1, Lcom/amap/api/a/t;->a:Lcom/amap/api/a/t$a;

    sget-object v2, Lcom/amap/api/a/t$a;->e:Lcom/amap/api/a/t$a;

    if-ne v1, v2, :cond_3

    .line 148
    iget-object v1, p0, Lcom/amap/api/a/a;->d:Lcom/amap/api/a/b;

    invoke-virtual {v1}, Lcom/amap/api/a/b;->D()Lcom/amap/api/a/av;

    move-result-object v1

    invoke-virtual {v1}, Lcom/amap/api/a/av;->d()Z

    goto :goto_0

    .line 149
    :cond_3
    iget-object v1, p1, Lcom/amap/api/a/t;->a:Lcom/amap/api/a/t$a;

    sget-object v2, Lcom/amap/api/a/t$a;->f:Lcom/amap/api/a/t$a;

    if-ne v1, v2, :cond_4

    .line 150
    iget v1, p1, Lcom/amap/api/a/t;->d:F

    float-to-int v1, v1

    .line 151
    iget-object v2, p0, Lcom/amap/api/a/a;->d:Lcom/amap/api/a/b;

    invoke-virtual {v2}, Lcom/amap/api/a/b;->D()Lcom/amap/api/a/av;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/amap/api/a/av;->c(I)I

    goto :goto_0

    .line 152
    :cond_4
    iget-object v1, p1, Lcom/amap/api/a/t;->a:Lcom/amap/api/a/t$a;

    sget-object v2, Lcom/amap/api/a/t$a;->g:Lcom/amap/api/a/t$a;

    if-ne v1, v2, :cond_6

    .line 153
    iget v1, p1, Lcom/amap/api/a/t;->e:F

    .line 154
    iget-object v2, p0, Lcom/amap/api/a/a;->d:Lcom/amap/api/a/b;

    int-to-float v3, v0

    add-float/2addr v1, v3

    float-to-int v1, v1

    invoke-virtual {v2, v1}, Lcom/amap/api/a/b;->a(I)I

    move-result v1

    .line 155
    iget-object v2, p1, Lcom/amap/api/a/t;->m:Landroid/graphics/Point;

    .line 156
    sub-int v3, v1, v0

    int-to-float v3, v3

    .line 157
    if-eqz v2, :cond_5

    .line 158
    iget-object v1, p0, Lcom/amap/api/a/a;->d:Lcom/amap/api/a/b;

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v2, v4}, Lcom/amap/api/a/b;->a(FLandroid/graphics/Point;Z)V

    goto :goto_0

    .line 160
    :cond_5
    iget-object v2, p0, Lcom/amap/api/a/a;->d:Lcom/amap/api/a/b;

    invoke-virtual {v2}, Lcom/amap/api/a/b;->D()Lcom/amap/api/a/av;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/amap/api/a/av;->c(I)I

    goto :goto_0

    .line 162
    :cond_6
    iget-object v1, p1, Lcom/amap/api/a/t;->a:Lcom/amap/api/a/t$a;

    sget-object v2, Lcom/amap/api/a/t$a;->i:Lcom/amap/api/a/t$a;

    if-ne v1, v2, :cond_7

    .line 163
    iget-object v1, p1, Lcom/amap/api/a/t;->h:Lcom/amap/api/maps2d/model/CameraPosition;

    .line 164
    iget-object v2, p0, Lcom/amap/api/a/a;->d:Lcom/amap/api/a/b;

    invoke-virtual {v2}, Lcom/amap/api/a/b;->D()Lcom/amap/api/a/av;

    move-result-object v2

    iget v3, v1, Lcom/amap/api/maps2d/model/CameraPosition;->zoom:F

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Lcom/amap/api/a/av;->c(I)I

    .line 165
    iget-object v2, v1, Lcom/amap/api/maps2d/model/CameraPosition;->target:Lcom/amap/api/maps2d/model/LatLng;

    iget-wide v2, v2, Lcom/amap/api/maps2d/model/LatLng;->latitude:D

    mul-double/2addr v2, v5

    double-to-int v2, v2

    .line 166
    iget-object v1, v1, Lcom/amap/api/maps2d/model/CameraPosition;->target:Lcom/amap/api/maps2d/model/LatLng;

    iget-wide v3, v1, Lcom/amap/api/maps2d/model/LatLng;->longitude:D

    mul-double/2addr v3, v5

    double-to-int v1, v3

    .line 167
    iget-object v3, p0, Lcom/amap/api/a/a;->d:Lcom/amap/api/a/b;

    invoke-virtual {v3}, Lcom/amap/api/a/b;->D()Lcom/amap/api/a/av;

    move-result-object v3

    new-instance v4, Lcom/amap/api/a/ac;

    invoke-direct {v4, v2, v1}, Lcom/amap/api/a/ac;-><init>(II)V

    invoke-virtual {v3, v4}, Lcom/amap/api/a/av;->a(Lcom/amap/api/a/ac;)V

    goto/16 :goto_0

    .line 168
    :cond_7
    iget-object v1, p1, Lcom/amap/api/a/t;->a:Lcom/amap/api/a/t$a;

    sget-object v2, Lcom/amap/api/a/t$a;->c:Lcom/amap/api/a/t$a;

    if-ne v1, v2, :cond_8

    .line 169
    iget-object v1, p1, Lcom/amap/api/a/t;->h:Lcom/amap/api/maps2d/model/CameraPosition;

    .line 170
    iget-object v2, v1, Lcom/amap/api/maps2d/model/CameraPosition;->target:Lcom/amap/api/maps2d/model/LatLng;

    iget-wide v2, v2, Lcom/amap/api/maps2d/model/LatLng;->latitude:D

    mul-double/2addr v2, v5

    double-to-int v2, v2

    .line 171
    iget-object v1, v1, Lcom/amap/api/maps2d/model/CameraPosition;->target:Lcom/amap/api/maps2d/model/LatLng;

    iget-wide v3, v1, Lcom/amap/api/maps2d/model/LatLng;->longitude:D

    mul-double/2addr v3, v5

    double-to-int v1, v3

    .line 172
    iget-object v3, p0, Lcom/amap/api/a/a;->d:Lcom/amap/api/a/b;

    invoke-virtual {v3}, Lcom/amap/api/a/b;->D()Lcom/amap/api/a/av;

    move-result-object v3

    new-instance v4, Lcom/amap/api/a/ac;

    invoke-direct {v4, v2, v1}, Lcom/amap/api/a/ac;-><init>(II)V

    invoke-virtual {v3, v4}, Lcom/amap/api/a/av;->a(Lcom/amap/api/a/ac;)V

    goto/16 :goto_0

    .line 173
    :cond_8
    iget-object v1, p1, Lcom/amap/api/a/t;->a:Lcom/amap/api/a/t$a;

    sget-object v2, Lcom/amap/api/a/t$a;->j:Lcom/amap/api/a/t$a;

    if-eq v1, v2, :cond_9

    iget-object v1, p1, Lcom/amap/api/a/t;->a:Lcom/amap/api/a/t$a;

    sget-object v2, Lcom/amap/api/a/t$a;->k:Lcom/amap/api/a/t$a;

    if-ne v1, v2, :cond_a

    .line 175
    :cond_9
    invoke-direct {p0, p1}, Lcom/amap/api/a/a;->b(Lcom/amap/api/a/t;)V

    goto/16 :goto_0

    .line 177
    :cond_a
    const/4 v1, 0x1

    iput-boolean v1, p1, Lcom/amap/api/a/t;->o:Z

    goto/16 :goto_0
.end method
