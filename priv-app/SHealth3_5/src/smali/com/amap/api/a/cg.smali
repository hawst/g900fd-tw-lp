.class Lcom/amap/api/a/cg;
.super Landroid/widget/LinearLayout;
.source "ZoomControllerView.java"


# instance fields
.field private a:Landroid/graphics/Bitmap;

.field private b:Landroid/graphics/Bitmap;

.field private c:Landroid/graphics/Bitmap;

.field private d:Landroid/graphics/Bitmap;

.field private e:Landroid/graphics/Bitmap;

.field private f:Landroid/graphics/Bitmap;

.field private g:Landroid/widget/ImageView;

.field private h:Landroid/widget/ImageView;

.field private i:Lcom/amap/api/a/av;

.field private j:Lcom/amap/api/a/af;

.field private k:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/amap/api/a/av;Lcom/amap/api/a/af;)V
    .locals 4

    .prologue
    const/16 v3, 0x14

    const/4 v2, 0x0

    .line 53
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 27
    iput v2, p0, Lcom/amap/api/a/cg;->k:I

    .line 54
    invoke-virtual {p0, v2}, Lcom/amap/api/a/cg;->setWillNotDraw(Z)V

    .line 56
    iput-object p2, p0, Lcom/amap/api/a/cg;->i:Lcom/amap/api/a/av;

    .line 57
    iput-object p3, p0, Lcom/amap/api/a/cg;->j:Lcom/amap/api/a/af;

    .line 60
    :try_start_0
    const-string/jumbo v0, "zoomin_selected.png"

    invoke-static {v0}, Lcom/amap/api/a/a/p;->a(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/cg;->a:Landroid/graphics/Bitmap;

    .line 61
    iget-object v0, p0, Lcom/amap/api/a/cg;->a:Landroid/graphics/Bitmap;

    sget v1, Lcom/amap/api/a/x;->a:F

    invoke-static {v0, v1}, Lcom/amap/api/a/a/p;->a(Landroid/graphics/Bitmap;F)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/cg;->a:Landroid/graphics/Bitmap;

    .line 63
    const-string/jumbo v0, "zoomin_unselected.png"

    invoke-static {v0}, Lcom/amap/api/a/a/p;->a(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/cg;->b:Landroid/graphics/Bitmap;

    .line 64
    iget-object v0, p0, Lcom/amap/api/a/cg;->b:Landroid/graphics/Bitmap;

    sget v1, Lcom/amap/api/a/x;->a:F

    invoke-static {v0, v1}, Lcom/amap/api/a/a/p;->a(Landroid/graphics/Bitmap;F)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/cg;->b:Landroid/graphics/Bitmap;

    .line 67
    const-string/jumbo v0, "zoomout_selected.png"

    invoke-static {v0}, Lcom/amap/api/a/a/p;->a(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/cg;->c:Landroid/graphics/Bitmap;

    .line 68
    iget-object v0, p0, Lcom/amap/api/a/cg;->c:Landroid/graphics/Bitmap;

    sget v1, Lcom/amap/api/a/x;->a:F

    invoke-static {v0, v1}, Lcom/amap/api/a/a/p;->a(Landroid/graphics/Bitmap;F)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/cg;->c:Landroid/graphics/Bitmap;

    .line 70
    const-string/jumbo v0, "zoomout_unselected.png"

    invoke-static {v0}, Lcom/amap/api/a/a/p;->a(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/cg;->d:Landroid/graphics/Bitmap;

    .line 71
    iget-object v0, p0, Lcom/amap/api/a/cg;->d:Landroid/graphics/Bitmap;

    sget v1, Lcom/amap/api/a/x;->a:F

    invoke-static {v0, v1}, Lcom/amap/api/a/a/p;->a(Landroid/graphics/Bitmap;F)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/cg;->d:Landroid/graphics/Bitmap;

    .line 74
    const-string/jumbo v0, "zoomin_pressed.png"

    invoke-static {v0}, Lcom/amap/api/a/a/p;->a(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/cg;->e:Landroid/graphics/Bitmap;

    .line 75
    const-string/jumbo v0, "zoomout_pressed.png"

    invoke-static {v0}, Lcom/amap/api/a/a/p;->a(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/cg;->f:Landroid/graphics/Bitmap;

    .line 77
    iget-object v0, p0, Lcom/amap/api/a/cg;->e:Landroid/graphics/Bitmap;

    sget v1, Lcom/amap/api/a/x;->a:F

    invoke-static {v0, v1}, Lcom/amap/api/a/a/p;->a(Landroid/graphics/Bitmap;F)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/cg;->e:Landroid/graphics/Bitmap;

    .line 79
    iget-object v0, p0, Lcom/amap/api/a/cg;->f:Landroid/graphics/Bitmap;

    sget v1, Lcom/amap/api/a/x;->a:F

    invoke-static {v0, v1}, Lcom/amap/api/a/a/p;->a(Landroid/graphics/Bitmap;F)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/cg;->f:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 86
    :goto_0
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/amap/api/a/cg;->g:Landroid/widget/ImageView;

    .line 87
    iget-object v0, p0, Lcom/amap/api/a/cg;->g:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/amap/api/a/cg;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 88
    iget-object v0, p0, Lcom/amap/api/a/cg;->g:Landroid/widget/ImageView;

    new-instance v1, Lcom/amap/api/a/cg$1;

    invoke-direct {v1, p0}, Lcom/amap/api/a/cg$1;-><init>(Lcom/amap/api/a/cg;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 104
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/amap/api/a/cg;->h:Landroid/widget/ImageView;

    .line 105
    iget-object v0, p0, Lcom/amap/api/a/cg;->h:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/amap/api/a/cg;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 107
    iget-object v0, p0, Lcom/amap/api/a/cg;->h:Landroid/widget/ImageView;

    new-instance v1, Lcom/amap/api/a/cg$2;

    invoke-direct {v1, p0}, Lcom/amap/api/a/cg$2;-><init>(Lcom/amap/api/a/cg;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    iget-object v0, p0, Lcom/amap/api/a/cg;->g:Landroid/widget/ImageView;

    new-instance v1, Lcom/amap/api/a/cg$3;

    invoke-direct {v1, p0}, Lcom/amap/api/a/cg$3;-><init>(Lcom/amap/api/a/cg;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 144
    iget-object v0, p0, Lcom/amap/api/a/cg;->h:Landroid/widget/ImageView;

    new-instance v1, Lcom/amap/api/a/cg$4;

    invoke-direct {v1, p0}, Lcom/amap/api/a/cg$4;-><init>(Lcom/amap/api/a/cg;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 177
    iget-object v0, p0, Lcom/amap/api/a/cg;->g:Landroid/widget/ImageView;

    const/4 v1, -0x2

    invoke-virtual {v0, v2, v2, v3, v1}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 178
    iget-object v0, p0, Lcom/amap/api/a/cg;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v2, v2, v3, v3}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 179
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/amap/api/a/cg;->setOrientation(I)V

    .line 181
    iget-object v0, p0, Lcom/amap/api/a/cg;->g:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/amap/api/a/cg;->addView(Landroid/view/View;)V

    .line 182
    iget-object v0, p0, Lcom/amap/api/a/cg;->h:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/amap/api/a/cg;->addView(Landroid/view/View;)V

    .line 184
    return-void

    .line 82
    :catch_0
    move-exception v0

    .line 83
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/amap/api/a/cg;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/amap/api/a/cg;->c:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic b(Lcom/amap/api/a/cg;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/amap/api/a/cg;->h:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic c(Lcom/amap/api/a/cg;)Lcom/amap/api/a/af;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/amap/api/a/cg;->j:Lcom/amap/api/a/af;

    return-object v0
.end method

.method static synthetic d(Lcom/amap/api/a/cg;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/amap/api/a/cg;->b:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic e(Lcom/amap/api/a/cg;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/amap/api/a/cg;->g:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic f(Lcom/amap/api/a/cg;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/amap/api/a/cg;->a:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic g(Lcom/amap/api/a/cg;)Lcom/amap/api/a/av;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/amap/api/a/cg;->i:Lcom/amap/api/a/av;

    return-object v0
.end method

.method static synthetic h(Lcom/amap/api/a/cg;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/amap/api/a/cg;->d:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic i(Lcom/amap/api/a/cg;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/amap/api/a/cg;->e:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic j(Lcom/amap/api/a/cg;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/amap/api/a/cg;->f:Landroid/graphics/Bitmap;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 31
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/a/cg;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 32
    iget-object v0, p0, Lcom/amap/api/a/cg;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 33
    iget-object v0, p0, Lcom/amap/api/a/cg;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 34
    iget-object v0, p0, Lcom/amap/api/a/cg;->d:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 35
    iget-object v0, p0, Lcom/amap/api/a/cg;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 36
    iget-object v0, p0, Lcom/amap/api/a/cg;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/a/cg;->a:Landroid/graphics/Bitmap;

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/a/cg;->b:Landroid/graphics/Bitmap;

    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/a/cg;->c:Landroid/graphics/Bitmap;

    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/a/cg;->d:Landroid/graphics/Bitmap;

    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/a/cg;->e:Landroid/graphics/Bitmap;

    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/a/cg;->f:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 48
    :goto_0
    return-void

    .line 44
    :catch_0
    move-exception v0

    .line 45
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public a(F)V
    .locals 2

    .prologue
    .line 199
    iget-object v0, p0, Lcom/amap/api/a/cg;->j:Lcom/amap/api/a/af;

    invoke-interface {v0}, Lcom/amap/api/a/af;->h()F

    move-result v0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_1

    iget-object v0, p0, Lcom/amap/api/a/cg;->j:Lcom/amap/api/a/af;

    invoke-interface {v0}, Lcom/amap/api/a/af;->i()F

    move-result v0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    .line 201
    iget-object v0, p0, Lcom/amap/api/a/cg;->g:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/amap/api/a/cg;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 202
    iget-object v0, p0, Lcom/amap/api/a/cg;->h:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/amap/api/a/cg;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 210
    :cond_0
    :goto_0
    return-void

    .line 203
    :cond_1
    iget-object v0, p0, Lcom/amap/api/a/cg;->j:Lcom/amap/api/a/af;

    invoke-interface {v0}, Lcom/amap/api/a/af;->i()F

    move-result v0

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_2

    .line 204
    iget-object v0, p0, Lcom/amap/api/a/cg;->h:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/amap/api/a/cg;->d:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 205
    iget-object v0, p0, Lcom/amap/api/a/cg;->g:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/amap/api/a/cg;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 206
    :cond_2
    iget-object v0, p0, Lcom/amap/api/a/cg;->j:Lcom/amap/api/a/af;

    invoke-interface {v0}, Lcom/amap/api/a/af;->h()F

    move-result v0

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/amap/api/a/cg;->g:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/amap/api/a/cg;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 208
    iget-object v0, p0, Lcom/amap/api/a/cg;->h:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/amap/api/a/cg;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 187
    iput p1, p0, Lcom/amap/api/a/cg;->k:I

    .line 188
    iget-object v0, p0, Lcom/amap/api/a/cg;->g:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/amap/api/a/cg;->removeView(Landroid/view/View;)V

    .line 189
    iget-object v0, p0, Lcom/amap/api/a/cg;->h:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/amap/api/a/cg;->removeView(Landroid/view/View;)V

    .line 190
    iget-object v0, p0, Lcom/amap/api/a/cg;->g:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/amap/api/a/cg;->addView(Landroid/view/View;)V

    .line 191
    iget-object v0, p0, Lcom/amap/api/a/cg;->h:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/amap/api/a/cg;->addView(Landroid/view/View;)V

    .line 192
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 195
    iget v0, p0, Lcom/amap/api/a/cg;->k:I

    return v0
.end method
