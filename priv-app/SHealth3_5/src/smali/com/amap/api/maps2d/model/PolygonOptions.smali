.class public final Lcom/amap/api/maps2d/model/PolygonOptions;
.super Ljava/lang/Object;
.source "PolygonOptions.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Lcom/amap/api/maps2d/model/PolygonOptionsCreator;


# instance fields
.field a:Ljava/lang/String;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/maps2d/model/LatLng;",
            ">;"
        }
    .end annotation
.end field

.field private c:F

.field private d:I

.field private e:I

.field private f:F

.field private g:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    new-instance v0, Lcom/amap/api/maps2d/model/PolygonOptionsCreator;

    invoke-direct {v0}, Lcom/amap/api/maps2d/model/PolygonOptionsCreator;-><init>()V

    sput-object v0, Lcom/amap/api/maps2d/model/PolygonOptions;->CREATOR:Lcom/amap/api/maps2d/model/PolygonOptionsCreator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/high16 v1, -0x1000000

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lcom/amap/api/maps2d/model/PolygonOptions;->c:F

    .line 22
    iput v1, p0, Lcom/amap/api/maps2d/model/PolygonOptions;->d:I

    .line 23
    iput v1, p0, Lcom/amap/api/maps2d/model/PolygonOptions;->e:I

    .line 24
    const/4 v0, 0x0

    iput v0, p0, Lcom/amap/api/maps2d/model/PolygonOptions;->f:F

    .line 25
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/maps2d/model/PolygonOptions;->g:Z

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/maps2d/model/PolygonOptions;->b:Ljava/util/List;

    .line 33
    return-void
.end method


# virtual methods
.method public add(Lcom/amap/api/maps2d/model/LatLng;)Lcom/amap/api/maps2d/model/PolygonOptions;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/amap/api/maps2d/model/PolygonOptions;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    return-object p0
.end method

.method public varargs add([Lcom/amap/api/maps2d/model/LatLng;)Lcom/amap/api/maps2d/model/PolygonOptions;
    .locals 2

    .prologue
    .line 82
    iget-object v0, p0, Lcom/amap/api/maps2d/model/PolygonOptions;->b:Ljava/util/List;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 83
    return-object p0
.end method

.method public addAll(Ljava/lang/Iterable;)Lcom/amap/api/maps2d/model/PolygonOptions;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/amap/api/maps2d/model/LatLng;",
            ">;)",
            "Lcom/amap/api/maps2d/model/PolygonOptions;"
        }
    .end annotation

    .prologue
    .line 93
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 94
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/maps2d/model/LatLng;

    .line 96
    iget-object v2, p0, Lcom/amap/api/maps2d/model/PolygonOptions;->b:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 98
    :cond_0
    return-object p0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x0

    return v0
.end method

.method public fillColor(I)Lcom/amap/api/maps2d/model/PolygonOptions;
    .locals 0

    .prologue
    .line 141
    iput p1, p0, Lcom/amap/api/maps2d/model/PolygonOptions;->e:I

    .line 142
    return-object p0
.end method

.method public getFillColor()I
    .locals 1

    .prologue
    .line 213
    iget v0, p0, Lcom/amap/api/maps2d/model/PolygonOptions;->e:I

    return v0
.end method

.method public getPoints()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/maps2d/model/LatLng;",
            ">;"
        }
    .end annotation

    .prologue
    .line 179
    iget-object v0, p0, Lcom/amap/api/maps2d/model/PolygonOptions;->b:Ljava/util/List;

    return-object v0
.end method

.method public getStrokeColor()I
    .locals 1

    .prologue
    .line 203
    iget v0, p0, Lcom/amap/api/maps2d/model/PolygonOptions;->d:I

    return v0
.end method

.method public getStrokeWidth()F
    .locals 1

    .prologue
    .line 193
    iget v0, p0, Lcom/amap/api/maps2d/model/PolygonOptions;->c:F

    return v0
.end method

.method public getZIndex()F
    .locals 1

    .prologue
    .line 223
    iget v0, p0, Lcom/amap/api/maps2d/model/PolygonOptions;->f:F

    return v0
.end method

.method public isVisible()Z
    .locals 1

    .prologue
    .line 233
    iget-boolean v0, p0, Lcom/amap/api/maps2d/model/PolygonOptions;->g:Z

    return v0
.end method

.method public strokeColor(I)Lcom/amap/api/maps2d/model/PolygonOptions;
    .locals 0

    .prologue
    .line 130
    iput p1, p0, Lcom/amap/api/maps2d/model/PolygonOptions;->d:I

    .line 131
    return-object p0
.end method

.method public strokeWidth(F)Lcom/amap/api/maps2d/model/PolygonOptions;
    .locals 0

    .prologue
    .line 119
    iput p1, p0, Lcom/amap/api/maps2d/model/PolygonOptions;->c:F

    .line 120
    return-object p0
.end method

.method public visible(Z)Lcom/amap/api/maps2d/model/PolygonOptions;
    .locals 0

    .prologue
    .line 163
    iput-boolean p1, p0, Lcom/amap/api/maps2d/model/PolygonOptions;->g:Z

    .line 164
    return-object p0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/amap/api/maps2d/model/PolygonOptions;->b:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 56
    iget v0, p0, Lcom/amap/api/maps2d/model/PolygonOptions;->c:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 57
    iget v0, p0, Lcom/amap/api/maps2d/model/PolygonOptions;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 58
    iget v0, p0, Lcom/amap/api/maps2d/model/PolygonOptions;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 59
    iget v0, p0, Lcom/amap/api/maps2d/model/PolygonOptions;->f:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 60
    iget-boolean v0, p0, Lcom/amap/api/maps2d/model/PolygonOptions;->g:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 61
    iget-object v0, p0, Lcom/amap/api/maps2d/model/PolygonOptions;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 62
    return-void

    .line 60
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public zIndex(F)Lcom/amap/api/maps2d/model/PolygonOptions;
    .locals 0

    .prologue
    .line 152
    iput p1, p0, Lcom/amap/api/maps2d/model/PolygonOptions;->f:F

    .line 153
    return-object p0
.end method
