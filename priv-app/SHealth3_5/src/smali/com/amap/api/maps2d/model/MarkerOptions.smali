.class public final Lcom/amap/api/maps2d/model/MarkerOptions;
.super Ljava/lang/Object;
.source "MarkerOptions.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Lcom/amap/api/maps2d/model/MarkerOptionsCreator;


# instance fields
.field a:Ljava/lang/String;

.field private b:Lcom/amap/api/maps2d/model/LatLng;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:F

.field private f:F

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/amap/api/maps2d/model/BitmapDescriptor;",
            ">;"
        }
    .end annotation
.end field

.field private k:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    new-instance v0, Lcom/amap/api/maps2d/model/MarkerOptionsCreator;

    invoke-direct {v0}, Lcom/amap/api/maps2d/model/MarkerOptionsCreator;-><init>()V

    sput-object v0, Lcom/amap/api/maps2d/model/MarkerOptions;->CREATOR:Lcom/amap/api/maps2d/model/MarkerOptionsCreator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Lcom/amap/api/maps2d/model/MarkerOptions;->e:F

    .line 16
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/amap/api/maps2d/model/MarkerOptions;->f:F

    .line 17
    iput-boolean v1, p0, Lcom/amap/api/maps2d/model/MarkerOptions;->g:Z

    .line 18
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/maps2d/model/MarkerOptions;->h:Z

    .line 20
    iput-boolean v1, p0, Lcom/amap/api/maps2d/model/MarkerOptions;->i:Z

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/maps2d/model/MarkerOptions;->j:Ljava/util/ArrayList;

    .line 25
    const/16 v0, 0x14

    iput v0, p0, Lcom/amap/api/maps2d/model/MarkerOptions;->k:I

    .line 38
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/amap/api/maps2d/model/MarkerOptions;->j:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 189
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/maps2d/model/MarkerOptions;->j:Ljava/util/ArrayList;

    .line 191
    :cond_0
    return-void
.end method


# virtual methods
.method public anchor(FF)Lcom/amap/api/maps2d/model/MarkerOptions;
    .locals 0

    .prologue
    .line 117
    iput p1, p0, Lcom/amap/api/maps2d/model/MarkerOptions;->e:F

    .line 118
    iput p2, p0, Lcom/amap/api/maps2d/model/MarkerOptions;->f:F

    .line 119
    return-object p0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 195
    const/4 v0, 0x0

    return v0
.end method

.method public draggable(Z)Lcom/amap/api/maps2d/model/MarkerOptions;
    .locals 0

    .prologue
    .line 133
    iput-boolean p1, p0, Lcom/amap/api/maps2d/model/MarkerOptions;->g:Z

    .line 134
    return-object p0
.end method

.method public getAnchorU()F
    .locals 1

    .prologue
    .line 168
    iget v0, p0, Lcom/amap/api/maps2d/model/MarkerOptions;->e:F

    return v0
.end method

.method public getAnchorV()F
    .locals 1

    .prologue
    .line 172
    iget v0, p0, Lcom/amap/api/maps2d/model/MarkerOptions;->f:F

    return v0
.end method

.method public getIcon()Lcom/amap/api/maps2d/model/BitmapDescriptor;
    .locals 2

    .prologue
    .line 160
    iget-object v0, p0, Lcom/amap/api/maps2d/model/MarkerOptions;->j:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/maps2d/model/MarkerOptions;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_0

    .line 161
    iget-object v0, p0, Lcom/amap/api/maps2d/model/MarkerOptions;->j:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/maps2d/model/BitmapDescriptor;

    .line 163
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getIcons()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/amap/api/maps2d/model/BitmapDescriptor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 75
    iget-object v0, p0, Lcom/amap/api/maps2d/model/MarkerOptions;->j:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getPeriod()I
    .locals 1

    .prologue
    .line 101
    iget v0, p0, Lcom/amap/api/maps2d/model/MarkerOptions;->k:I

    return v0
.end method

.method public getPosition()Lcom/amap/api/maps2d/model/LatLng;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/amap/api/maps2d/model/MarkerOptions;->b:Lcom/amap/api/maps2d/model/LatLng;

    return-object v0
.end method

.method public getSnippet()Ljava/lang/String;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/amap/api/maps2d/model/MarkerOptions;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/amap/api/maps2d/model/MarkerOptions;->c:Ljava/lang/String;

    return-object v0
.end method

.method public icon(Lcom/amap/api/maps2d/model/BitmapDescriptor;)Lcom/amap/api/maps2d/model/MarkerOptions;
    .locals 1

    .prologue
    .line 110
    invoke-direct {p0}, Lcom/amap/api/maps2d/model/MarkerOptions;->a()V

    .line 111
    iget-object v0, p0, Lcom/amap/api/maps2d/model/MarkerOptions;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 112
    iget-object v0, p0, Lcom/amap/api/maps2d/model/MarkerOptions;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 113
    return-object p0
.end method

.method public icons(Ljava/util/ArrayList;)Lcom/amap/api/maps2d/model/MarkerOptions;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/amap/api/maps2d/model/BitmapDescriptor;",
            ">;)",
            "Lcom/amap/api/maps2d/model/MarkerOptions;"
        }
    .end annotation

    .prologue
    .line 65
    iput-object p1, p0, Lcom/amap/api/maps2d/model/MarkerOptions;->j:Ljava/util/ArrayList;

    .line 66
    return-object p0
.end method

.method public isDraggable()Z
    .locals 1

    .prologue
    .line 176
    iget-boolean v0, p0, Lcom/amap/api/maps2d/model/MarkerOptions;->g:Z

    return v0
.end method

.method public isGps()Z
    .locals 1

    .prologue
    .line 184
    iget-boolean v0, p0, Lcom/amap/api/maps2d/model/MarkerOptions;->i:Z

    return v0
.end method

.method public isVisible()Z
    .locals 1

    .prologue
    .line 180
    iget-boolean v0, p0, Lcom/amap/api/maps2d/model/MarkerOptions;->h:Z

    return v0
.end method

.method public period(I)Lcom/amap/api/maps2d/model/MarkerOptions;
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 87
    if-gt p1, v0, :cond_0

    .line 88
    iput v0, p0, Lcom/amap/api/maps2d/model/MarkerOptions;->k:I

    .line 92
    :goto_0
    return-object p0

    .line 90
    :cond_0
    iput p1, p0, Lcom/amap/api/maps2d/model/MarkerOptions;->k:I

    goto :goto_0
.end method

.method public position(Lcom/amap/api/maps2d/model/LatLng;)Lcom/amap/api/maps2d/model/MarkerOptions;
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lcom/amap/api/maps2d/model/MarkerOptions;->b:Lcom/amap/api/maps2d/model/LatLng;

    .line 106
    return-object p0
.end method

.method public setGps(Z)Lcom/amap/api/maps2d/model/MarkerOptions;
    .locals 0

    .prologue
    .line 143
    iput-boolean p1, p0, Lcom/amap/api/maps2d/model/MarkerOptions;->i:Z

    .line 144
    return-object p0
.end method

.method public snippet(Ljava/lang/String;)Lcom/amap/api/maps2d/model/MarkerOptions;
    .locals 0

    .prologue
    .line 128
    iput-object p1, p0, Lcom/amap/api/maps2d/model/MarkerOptions;->d:Ljava/lang/String;

    .line 129
    return-object p0
.end method

.method public title(Ljava/lang/String;)Lcom/amap/api/maps2d/model/MarkerOptions;
    .locals 0

    .prologue
    .line 123
    iput-object p1, p0, Lcom/amap/api/maps2d/model/MarkerOptions;->c:Ljava/lang/String;

    .line 124
    return-object p0
.end method

.method public visible(Z)Lcom/amap/api/maps2d/model/MarkerOptions;
    .locals 0

    .prologue
    .line 138
    iput-boolean p1, p0, Lcom/amap/api/maps2d/model/MarkerOptions;->h:Z

    .line 139
    return-object p0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 200
    iget-object v0, p0, Lcom/amap/api/maps2d/model/MarkerOptions;->b:Lcom/amap/api/maps2d/model/LatLng;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 201
    iget-object v0, p0, Lcom/amap/api/maps2d/model/MarkerOptions;->j:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/maps2d/model/MarkerOptions;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_0

    .line 202
    iget-object v0, p0, Lcom/amap/api/maps2d/model/MarkerOptions;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 204
    :cond_0
    iget-object v0, p0, Lcom/amap/api/maps2d/model/MarkerOptions;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 205
    iget-object v0, p0, Lcom/amap/api/maps2d/model/MarkerOptions;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 206
    iget v0, p0, Lcom/amap/api/maps2d/model/MarkerOptions;->e:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 207
    iget v0, p0, Lcom/amap/api/maps2d/model/MarkerOptions;->f:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 208
    iget-boolean v0, p0, Lcom/amap/api/maps2d/model/MarkerOptions;->h:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 209
    iget-boolean v0, p0, Lcom/amap/api/maps2d/model/MarkerOptions;->g:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 210
    iget-boolean v0, p0, Lcom/amap/api/maps2d/model/MarkerOptions;->i:Z

    if-eqz v0, :cond_3

    :goto_2
    int-to-byte v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 211
    iget-object v0, p0, Lcom/amap/api/maps2d/model/MarkerOptions;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 212
    iget-object v0, p0, Lcom/amap/api/maps2d/model/MarkerOptions;->j:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 213
    return-void

    :cond_1
    move v0, v2

    .line 208
    goto :goto_0

    :cond_2
    move v0, v2

    .line 209
    goto :goto_1

    :cond_3
    move v1, v2

    .line 210
    goto :goto_2
.end method
