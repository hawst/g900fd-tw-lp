.class public final Lcom/amap/api/maps2d/model/BitmapDescriptor;
.super Ljava/lang/Object;
.source "BitmapDescriptor.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static final CREATOR:Lcom/amap/api/maps2d/model/BitmapDescriptorCreator;


# instance fields
.field a:I

.field b:I

.field c:Landroid/graphics/Bitmap;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    new-instance v0, Lcom/amap/api/maps2d/model/BitmapDescriptorCreator;

    invoke-direct {v0}, Lcom/amap/api/maps2d/model/BitmapDescriptorCreator;-><init>()V

    sput-object v0, Lcom/amap/api/maps2d/model/BitmapDescriptor;->CREATOR:Lcom/amap/api/maps2d/model/BitmapDescriptorCreator;

    return-void
.end method

.method constructor <init>(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput v0, p0, Lcom/amap/api/maps2d/model/BitmapDescriptor;->a:I

    .line 12
    iput v0, p0, Lcom/amap/api/maps2d/model/BitmapDescriptor;->b:I

    .line 16
    if-eqz p1, :cond_0

    .line 17
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/amap/api/maps2d/model/BitmapDescriptor;->a:I

    .line 18
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/amap/api/maps2d/model/BitmapDescriptor;->b:I

    .line 19
    iput-object p1, p0, Lcom/amap/api/maps2d/model/BitmapDescriptor;->c:Landroid/graphics/Bitmap;

    .line 21
    :cond_0
    return-void
.end method

.method private constructor <init>(Landroid/graphics/Bitmap;II)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput v0, p0, Lcom/amap/api/maps2d/model/BitmapDescriptor;->a:I

    .line 12
    iput v0, p0, Lcom/amap/api/maps2d/model/BitmapDescriptor;->b:I

    .line 23
    iput p2, p0, Lcom/amap/api/maps2d/model/BitmapDescriptor;->a:I

    .line 24
    iput p3, p0, Lcom/amap/api/maps2d/model/BitmapDescriptor;->b:I

    .line 25
    iput-object p1, p0, Lcom/amap/api/maps2d/model/BitmapDescriptor;->c:Landroid/graphics/Bitmap;

    .line 26
    return-void
.end method


# virtual methods
.method public clone()Lcom/amap/api/maps2d/model/BitmapDescriptor;
    .locals 4

    .prologue
    .line 29
    new-instance v0, Lcom/amap/api/maps2d/model/BitmapDescriptor;

    iget-object v1, p0, Lcom/amap/api/maps2d/model/BitmapDescriptor;->c:Landroid/graphics/Bitmap;

    invoke-static {v1}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    iget v2, p0, Lcom/amap/api/maps2d/model/BitmapDescriptor;->a:I

    iget v3, p0, Lcom/amap/api/maps2d/model/BitmapDescriptor;->b:I

    invoke-direct {v0, v1, v2, v3}, Lcom/amap/api/maps2d/model/BitmapDescriptor;-><init>(Landroid/graphics/Bitmap;II)V

    .line 30
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 9
    invoke-virtual {p0}, Lcom/amap/api/maps2d/model/BitmapDescriptor;->clone()Lcom/amap/api/maps2d/model/BitmapDescriptor;

    move-result-object v0

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    return v0
.end method

.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 35
    iget-object v0, p0, Lcom/amap/api/maps2d/model/BitmapDescriptor;->c:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lcom/amap/api/maps2d/model/BitmapDescriptor;->b:I

    return v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lcom/amap/api/maps2d/model/BitmapDescriptor;->a:I

    return v0
.end method

.method public recycle()V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/amap/api/maps2d/model/BitmapDescriptor;->c:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/maps2d/model/BitmapDescriptor;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/amap/api/maps2d/model/BitmapDescriptor;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 74
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/maps2d/model/BitmapDescriptor;->c:Landroid/graphics/Bitmap;

    .line 76
    :cond_0
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/amap/api/maps2d/model/BitmapDescriptor;->c:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 65
    iget v0, p0, Lcom/amap/api/maps2d/model/BitmapDescriptor;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 66
    iget v0, p0, Lcom/amap/api/maps2d/model/BitmapDescriptor;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 67
    return-void
.end method
