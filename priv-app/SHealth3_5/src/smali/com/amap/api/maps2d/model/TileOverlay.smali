.class public final Lcom/amap/api/maps2d/model/TileOverlay;
.super Ljava/lang/Object;
.source "TileOverlay.java"


# instance fields
.field private a:Lcom/amap/api/a/ap;


# direct methods
.method public constructor <init>(Lcom/amap/api/a/ap;)V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object p1, p0, Lcom/amap/api/maps2d/model/TileOverlay;->a:Lcom/amap/api/a/ap;

    .line 11
    return-void
.end method


# virtual methods
.method public clearTileCache()V
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/amap/api/maps2d/model/TileOverlay;->a:Lcom/amap/api/a/ap;

    invoke-interface {v0}, Lcom/amap/api/a/ap;->b()V

    .line 19
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/amap/api/maps2d/model/TileOverlay;->a:Lcom/amap/api/a/ap;

    iget-object v1, p0, Lcom/amap/api/maps2d/model/TileOverlay;->a:Lcom/amap/api/a/ap;

    invoke-interface {v0, v1}, Lcom/amap/api/a/ap;->a(Lcom/amap/api/a/ap;)Z

    move-result v0

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/amap/api/maps2d/model/TileOverlay;->a:Lcom/amap/api/a/ap;

    invoke-interface {v0}, Lcom/amap/api/a/ap;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getZIndex()F
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/amap/api/maps2d/model/TileOverlay;->a:Lcom/amap/api/a/ap;

    invoke-interface {v0}, Lcom/amap/api/a/ap;->d()F

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/amap/api/maps2d/model/TileOverlay;->a:Lcom/amap/api/a/ap;

    invoke-interface {v0}, Lcom/amap/api/a/ap;->f()I

    move-result v0

    return v0
.end method

.method public isVisible()Z
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/amap/api/maps2d/model/TileOverlay;->a:Lcom/amap/api/a/ap;

    invoke-interface {v0}, Lcom/amap/api/a/ap;->e()Z

    move-result v0

    return v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/amap/api/maps2d/model/TileOverlay;->a:Lcom/amap/api/a/ap;

    invoke-interface {v0}, Lcom/amap/api/a/ap;->a()V

    .line 15
    return-void
.end method

.method public setVisible(Z)V
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/amap/api/maps2d/model/TileOverlay;->a:Lcom/amap/api/a/ap;

    invoke-interface {v0, p1}, Lcom/amap/api/a/ap;->a(Z)V

    .line 35
    return-void
.end method

.method public setZIndex(F)V
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/amap/api/maps2d/model/TileOverlay;->a:Lcom/amap/api/a/ap;

    invoke-interface {v0, p1}, Lcom/amap/api/a/ap;->a(F)V

    .line 27
    return-void
.end method
