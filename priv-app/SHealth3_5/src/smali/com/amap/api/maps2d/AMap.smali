.class public final Lcom/amap/api/maps2d/AMap;
.super Ljava/lang/Object;
.source "AMap.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/amap/api/maps2d/AMap$OnMapScreenShotListener;,
        Lcom/amap/api/maps2d/AMap$OnMapLoadedListener;,
        Lcom/amap/api/maps2d/AMap$OnMapClickListener;,
        Lcom/amap/api/maps2d/AMap$OnMapLongClickListener;,
        Lcom/amap/api/maps2d/AMap$OnCameraChangeListener;,
        Lcom/amap/api/maps2d/AMap$OnMarkerClickListener;,
        Lcom/amap/api/maps2d/AMap$OnMarkerDragListener;,
        Lcom/amap/api/maps2d/AMap$OnInfoWindowClickListener;,
        Lcom/amap/api/maps2d/AMap$CancelableCallback;,
        Lcom/amap/api/maps2d/AMap$OnMyLocationChangeListener;,
        Lcom/amap/api/maps2d/AMap$InfoWindowAdapter;
    }
.end annotation


# static fields
.field public static final MAP_TYPE_NORMAL:I = 0x1

.field public static final MAP_TYPE_SATELLITE:I = 0x2


# instance fields
.field private final a:Lcom/amap/api/a/af;

.field private b:Lcom/amap/api/maps2d/UiSettings;

.field private c:Lcom/amap/api/maps2d/Projection;


# direct methods
.method protected constructor <init>(Lcom/amap/api/a/af;)V
    .locals 0

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    iput-object p1, p0, Lcom/amap/api/maps2d/AMap;->a:Lcom/amap/api/a/af;

    .line 91
    return-void
.end method


# virtual methods
.method a()Lcom/amap/api/a/af;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/amap/api/maps2d/AMap;->a:Lcom/amap/api/a/af;

    return-object v0
.end method

.method public final addCircle(Lcom/amap/api/maps2d/model/CircleOptions;)Lcom/amap/api/maps2d/model/Circle;
    .locals 2

    .prologue
    .line 178
    :try_start_0
    new-instance v0, Lcom/amap/api/maps2d/model/Circle;

    invoke-virtual {p0}, Lcom/amap/api/maps2d/AMap;->a()Lcom/amap/api/a/af;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/amap/api/a/af;->a(Lcom/amap/api/maps2d/model/CircleOptions;)Lcom/amap/api/a/ag;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/amap/api/maps2d/model/Circle;-><init>(Lcom/amap/api/a/ag;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 179
    :catch_0
    move-exception v0

    .line 180
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final addGroundOverlay(Lcom/amap/api/maps2d/model/GroundOverlayOptions;)Lcom/amap/api/maps2d/model/GroundOverlay;
    .locals 2

    .prologue
    .line 202
    :try_start_0
    new-instance v0, Lcom/amap/api/maps2d/model/GroundOverlay;

    invoke-virtual {p0}, Lcom/amap/api/maps2d/AMap;->a()Lcom/amap/api/a/af;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/amap/api/a/af;->a(Lcom/amap/api/maps2d/model/GroundOverlayOptions;)Lcom/amap/api/a/ah;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/amap/api/maps2d/model/GroundOverlay;-><init>(Lcom/amap/api/a/ah;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 204
    :catch_0
    move-exception v0

    .line 205
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final addMarker(Lcom/amap/api/maps2d/model/MarkerOptions;)Lcom/amap/api/maps2d/model/Marker;
    .locals 2

    .prologue
    .line 194
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps2d/AMap;->a()Lcom/amap/api/a/af;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/a/af;->a(Lcom/amap/api/maps2d/model/MarkerOptions;)Lcom/amap/api/maps2d/model/Marker;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 195
    :catch_0
    move-exception v0

    .line 196
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final addPolygon(Lcom/amap/api/maps2d/model/PolygonOptions;)Lcom/amap/api/maps2d/model/Polygon;
    .locals 2

    .prologue
    .line 186
    :try_start_0
    new-instance v0, Lcom/amap/api/maps2d/model/Polygon;

    invoke-virtual {p0}, Lcom/amap/api/maps2d/AMap;->a()Lcom/amap/api/a/af;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/amap/api/a/af;->a(Lcom/amap/api/maps2d/model/PolygonOptions;)Lcom/amap/api/a/al;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/amap/api/maps2d/model/Polygon;-><init>(Lcom/amap/api/a/al;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 187
    :catch_0
    move-exception v0

    .line 188
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final addPolyline(Lcom/amap/api/maps2d/model/PolylineOptions;)Lcom/amap/api/maps2d/model/Polyline;
    .locals 2

    .prologue
    .line 170
    :try_start_0
    new-instance v0, Lcom/amap/api/maps2d/model/Polyline;

    invoke-virtual {p0}, Lcom/amap/api/maps2d/AMap;->a()Lcom/amap/api/a/af;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/amap/api/a/af;->a(Lcom/amap/api/maps2d/model/PolylineOptions;)Lcom/amap/api/a/am;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/amap/api/maps2d/model/Polyline;-><init>(Lcom/amap/api/a/am;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 171
    :catch_0
    move-exception v0

    .line 172
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final addTileOverlay(Lcom/amap/api/maps2d/model/TileOverlayOptions;)Lcom/amap/api/maps2d/model/TileOverlay;
    .locals 2

    .prologue
    .line 211
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps2d/AMap;->a()Lcom/amap/api/a/af;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/a/af;->a(Lcom/amap/api/maps2d/model/TileOverlayOptions;)Lcom/amap/api/maps2d/model/TileOverlay;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 212
    :catch_0
    move-exception v0

    .line 213
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final animateCamera(Lcom/amap/api/maps2d/CameraUpdate;)V
    .locals 2

    .prologue
    .line 128
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps2d/AMap;->a()Lcom/amap/api/a/af;

    move-result-object v0

    invoke-virtual {p1}, Lcom/amap/api/maps2d/CameraUpdate;->a()Lcom/amap/api/a/t;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/amap/api/a/af;->b(Lcom/amap/api/a/t;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 133
    return-void

    .line 130
    :catch_0
    move-exception v0

    .line 131
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final animateCamera(Lcom/amap/api/maps2d/CameraUpdate;JLcom/amap/api/maps2d/AMap$CancelableCallback;)V
    .locals 2

    .prologue
    .line 150
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    :try_start_0
    const-string v1, "durationMs must be positive"

    invoke-static {v0, v1}, Lcom/amap/api/a/a/a;->b(ZLjava/lang/Object;)V

    .line 152
    invoke-virtual {p0}, Lcom/amap/api/maps2d/AMap;->a()Lcom/amap/api/a/af;

    move-result-object v0

    invoke-virtual {p1}, Lcom/amap/api/maps2d/CameraUpdate;->a()Lcom/amap/api/a/t;

    move-result-object v1

    invoke-interface {v0, v1, p2, p3, p4}, Lcom/amap/api/a/af;->a(Lcom/amap/api/a/t;JLcom/amap/api/maps2d/AMap$CancelableCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 158
    return-void

    .line 150
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 155
    :catch_0
    move-exception v0

    .line 156
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final animateCamera(Lcom/amap/api/maps2d/CameraUpdate;Lcom/amap/api/maps2d/AMap$CancelableCallback;)V
    .locals 2

    .prologue
    .line 138
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps2d/AMap;->a()Lcom/amap/api/a/af;

    move-result-object v0

    invoke-virtual {p1}, Lcom/amap/api/maps2d/CameraUpdate;->a()Lcom/amap/api/a/t;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Lcom/amap/api/a/af;->a(Lcom/amap/api/a/t;Lcom/amap/api/maps2d/AMap$CancelableCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 145
    return-void

    .line 142
    :catch_0
    move-exception v0

    .line 143
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final clear()V
    .locals 2

    .prologue
    .line 219
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps2d/AMap;->a()Lcom/amap/api/a/af;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/a/af;->k()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 223
    return-void

    .line 220
    :catch_0
    move-exception v0

    .line 221
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final getCameraPosition()Lcom/amap/api/maps2d/model/CameraPosition;
    .locals 2

    .prologue
    .line 99
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps2d/AMap;->a()Lcom/amap/api/a/af;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/a/af;->g()Lcom/amap/api/maps2d/model/CameraPosition;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 100
    :catch_0
    move-exception v0

    .line 101
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final getMapScreenMarkers()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/maps2d/model/Marker;",
            ">;"
        }
    .end annotation

    .prologue
    .line 466
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/maps2d/AMap;->a:Lcom/amap/api/a/af;

    invoke-interface {v0}, Lcom/amap/api/a/af;->Q()Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 467
    :catch_0
    move-exception v0

    .line 468
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public getMapScreenShot(Lcom/amap/api/maps2d/AMap$OnMapScreenShotListener;)V
    .locals 1

    .prologue
    .line 444
    invoke-virtual {p0}, Lcom/amap/api/maps2d/AMap;->a()Lcom/amap/api/a/af;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/a/af;->a(Lcom/amap/api/maps2d/AMap$OnMapScreenShotListener;)V

    .line 445
    return-void
.end method

.method public final getMapType()I
    .locals 2

    .prologue
    .line 227
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps2d/AMap;->a()Lcom/amap/api/a/af;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/a/af;->l()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 228
    :catch_0
    move-exception v0

    .line 229
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final getMaxZoomLevel()F
    .locals 1

    .prologue
    .line 107
    invoke-virtual {p0}, Lcom/amap/api/maps2d/AMap;->a()Lcom/amap/api/a/af;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/a/af;->h()F

    move-result v0

    return v0
.end method

.method public final getMinZoomLevel()F
    .locals 1

    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/amap/api/maps2d/AMap;->a()Lcom/amap/api/a/af;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/a/af;->i()F

    move-result v0

    return v0
.end method

.method public final getMyLocation()Landroid/location/Location;
    .locals 2

    .prologue
    .line 315
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps2d/AMap;->a()Lcom/amap/api/a/af;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/a/af;->p()Landroid/location/Location;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 316
    :catch_0
    move-exception v0

    .line 317
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final getProjection()Lcom/amap/api/maps2d/Projection;
    .locals 2

    .prologue
    .line 351
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/maps2d/AMap;->c:Lcom/amap/api/maps2d/Projection;

    if-nez v0, :cond_0

    .line 352
    new-instance v0, Lcom/amap/api/maps2d/Projection;

    invoke-virtual {p0}, Lcom/amap/api/maps2d/AMap;->a()Lcom/amap/api/a/af;

    move-result-object v1

    invoke-interface {v1}, Lcom/amap/api/a/af;->r()Lcom/amap/api/a/an;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/amap/api/maps2d/Projection;-><init>(Lcom/amap/api/a/an;)V

    iput-object v0, p0, Lcom/amap/api/maps2d/AMap;->c:Lcom/amap/api/maps2d/Projection;

    .line 355
    :cond_0
    iget-object v0, p0, Lcom/amap/api/maps2d/AMap;->c:Lcom/amap/api/maps2d/Projection;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 356
    :catch_0
    move-exception v0

    .line 357
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public getScalePerPixel()F
    .locals 1

    .prologue
    .line 448
    invoke-virtual {p0}, Lcom/amap/api/maps2d/AMap;->a()Lcom/amap/api/a/af;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/a/af;->w()F

    move-result v0

    return v0
.end method

.method public final getUiSettings()Lcom/amap/api/maps2d/UiSettings;
    .locals 2

    .prologue
    .line 339
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/maps2d/AMap;->b:Lcom/amap/api/maps2d/UiSettings;

    if-nez v0, :cond_0

    .line 340
    new-instance v0, Lcom/amap/api/maps2d/UiSettings;

    invoke-virtual {p0}, Lcom/amap/api/maps2d/AMap;->a()Lcom/amap/api/a/af;

    move-result-object v1

    invoke-interface {v1}, Lcom/amap/api/a/af;->q()Lcom/amap/api/a/aq;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/amap/api/maps2d/UiSettings;-><init>(Lcom/amap/api/a/aq;)V

    iput-object v0, p0, Lcom/amap/api/maps2d/AMap;->b:Lcom/amap/api/maps2d/UiSettings;

    .line 343
    :cond_0
    iget-object v0, p0, Lcom/amap/api/maps2d/AMap;->b:Lcom/amap/api/maps2d/UiSettings;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 344
    :catch_0
    move-exception v0

    .line 345
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 285
    const-string v0, "V2.2.0.2"

    return-object v0
.end method

.method public invalidate()V
    .locals 0

    .prologue
    .line 478
    invoke-virtual {p0}, Lcom/amap/api/maps2d/AMap;->postInvalidate()V

    .line 479
    return-void
.end method

.method public final isMyLocationEnabled()Z
    .locals 2

    .prologue
    .line 273
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps2d/AMap;->a()Lcom/amap/api/a/af;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/a/af;->n()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 274
    :catch_0
    move-exception v0

    .line 275
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final isTrafficEnabled()Z
    .locals 2

    .prologue
    .line 257
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps2d/AMap;->a()Lcom/amap/api/a/af;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/a/af;->m()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 258
    :catch_0
    move-exception v0

    .line 259
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final moveCamera(Lcom/amap/api/maps2d/CameraUpdate;)V
    .locals 2

    .prologue
    .line 119
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps2d/AMap;->a()Lcom/amap/api/a/af;

    move-result-object v0

    invoke-virtual {p1}, Lcom/amap/api/maps2d/CameraUpdate;->a()Lcom/amap/api/a/t;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/amap/api/a/af;->a(Lcom/amap/api/a/t;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 124
    return-void

    .line 121
    :catch_0
    move-exception v0

    .line 122
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public postInvalidate()V
    .locals 1

    .prologue
    .line 474
    invoke-virtual {p0}, Lcom/amap/api/maps2d/AMap;->a()Lcom/amap/api/a/af;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/a/af;->P()V

    .line 475
    return-void
.end method

.method public final setInfoWindowAdapter(Lcom/amap/api/maps2d/AMap$InfoWindowAdapter;)V
    .locals 2

    .prologue
    .line 425
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps2d/AMap;->a()Lcom/amap/api/a/af;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/a/af;->a(Lcom/amap/api/maps2d/AMap$InfoWindowAdapter;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 429
    return-void

    .line 426
    :catch_0
    move-exception v0

    .line 427
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final setLocationSource(Lcom/amap/api/maps2d/LocationSource;)V
    .locals 2

    .prologue
    .line 323
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps2d/AMap;->a()Lcom/amap/api/a/af;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/a/af;->a(Lcom/amap/api/maps2d/LocationSource;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 327
    return-void

    .line 324
    :catch_0
    move-exception v0

    .line 325
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final setMapType(I)V
    .locals 2

    .prologue
    .line 235
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps2d/AMap;->a()Lcom/amap/api/a/af;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/a/af;->b(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 239
    return-void

    .line 236
    :catch_0
    move-exception v0

    .line 237
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final setMyLocationEnabled(Z)V
    .locals 2

    .prologue
    .line 300
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps2d/AMap;->a()Lcom/amap/api/a/af;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/a/af;->c(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 304
    return-void

    .line 301
    :catch_0
    move-exception v0

    .line 302
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final setMyLocationRotateAngle(F)V
    .locals 2

    .prologue
    .line 249
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/maps2d/AMap;->a:Lcom/amap/api/a/af;

    invoke-interface {v0, p1}, Lcom/amap/api/a/af;->a(F)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 253
    return-void

    .line 250
    :catch_0
    move-exception v0

    .line 251
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final setMyLocationStyle(Lcom/amap/api/maps2d/model/MyLocationStyle;)V
    .locals 2

    .prologue
    .line 331
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps2d/AMap;->a()Lcom/amap/api/a/af;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/a/af;->a(Lcom/amap/api/maps2d/model/MyLocationStyle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 335
    return-void

    .line 332
    :catch_0
    move-exception v0

    .line 333
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final setOnCameraChangeListener(Lcom/amap/api/maps2d/AMap$OnCameraChangeListener;)V
    .locals 2

    .prologue
    .line 364
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps2d/AMap;->a()Lcom/amap/api/a/af;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/a/af;->a(Lcom/amap/api/maps2d/AMap$OnCameraChangeListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 368
    return-void

    .line 365
    :catch_0
    move-exception v0

    .line 366
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final setOnInfoWindowClickListener(Lcom/amap/api/maps2d/AMap$OnInfoWindowClickListener;)V
    .locals 2

    .prologue
    .line 417
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps2d/AMap;->a()Lcom/amap/api/a/af;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/a/af;->a(Lcom/amap/api/maps2d/AMap$OnInfoWindowClickListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 421
    return-void

    .line 418
    :catch_0
    move-exception v0

    .line 419
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final setOnMapClickListener(Lcom/amap/api/maps2d/AMap$OnMapClickListener;)V
    .locals 2

    .prologue
    .line 372
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps2d/AMap;->a()Lcom/amap/api/a/af;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/a/af;->a(Lcom/amap/api/maps2d/AMap$OnMapClickListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 376
    return-void

    .line 373
    :catch_0
    move-exception v0

    .line 374
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final setOnMapLoadedListener(Lcom/amap/api/maps2d/AMap$OnMapLoadedListener;)V
    .locals 2

    .prologue
    .line 433
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps2d/AMap;->a()Lcom/amap/api/a/af;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/a/af;->a(Lcom/amap/api/maps2d/AMap$OnMapLoadedListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 437
    return-void

    .line 434
    :catch_0
    move-exception v0

    .line 435
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final setOnMapLongClickListener(Lcom/amap/api/maps2d/AMap$OnMapLongClickListener;)V
    .locals 2

    .prologue
    .line 390
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps2d/AMap;->a()Lcom/amap/api/a/af;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/a/af;->a(Lcom/amap/api/maps2d/AMap$OnMapLongClickListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 394
    return-void

    .line 391
    :catch_0
    move-exception v0

    .line 392
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final setOnMarkerClickListener(Lcom/amap/api/maps2d/AMap$OnMarkerClickListener;)V
    .locals 2

    .prologue
    .line 399
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps2d/AMap;->a()Lcom/amap/api/a/af;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/a/af;->a(Lcom/amap/api/maps2d/AMap$OnMarkerClickListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 403
    return-void

    .line 400
    :catch_0
    move-exception v0

    .line 401
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final setOnMarkerDragListener(Lcom/amap/api/maps2d/AMap$OnMarkerDragListener;)V
    .locals 2

    .prologue
    .line 408
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps2d/AMap;->a()Lcom/amap/api/a/af;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/a/af;->a(Lcom/amap/api/maps2d/AMap$OnMarkerDragListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 412
    return-void

    .line 409
    :catch_0
    move-exception v0

    .line 410
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final setOnMyLocationChangeListener(Lcom/amap/api/maps2d/AMap$OnMyLocationChangeListener;)V
    .locals 2

    .prologue
    .line 381
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps2d/AMap;->a()Lcom/amap/api/a/af;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/a/af;->a(Lcom/amap/api/maps2d/AMap$OnMyLocationChangeListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 385
    return-void

    .line 382
    :catch_0
    move-exception v0

    .line 383
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public setTrafficEnabled(Z)V
    .locals 2

    .prologue
    .line 265
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps2d/AMap;->a()Lcom/amap/api/a/af;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/a/af;->b(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 269
    return-void

    .line 266
    :catch_0
    move-exception v0

    .line 267
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final stopAnimation()V
    .locals 2

    .prologue
    .line 162
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps2d/AMap;->a()Lcom/amap/api/a/af;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/a/af;->j()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 166
    return-void

    .line 163
    :catch_0
    move-exception v0

    .line 164
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method
