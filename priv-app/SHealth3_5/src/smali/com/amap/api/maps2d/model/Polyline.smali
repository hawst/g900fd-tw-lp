.class public Lcom/amap/api/maps2d/model/Polyline;
.super Ljava/lang/Object;
.source "Polyline.java"


# instance fields
.field private final a:Lcom/amap/api/a/am;


# direct methods
.method public constructor <init>(Lcom/amap/api/a/am;)V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/amap/api/maps2d/model/Polyline;->a:Lcom/amap/api/a/am;

    .line 14
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 141
    instance-of v0, p1, Lcom/amap/api/maps2d/model/Polyline;

    if-nez v0, :cond_0

    .line 142
    const/4 v0, 0x0

    .line 144
    :goto_0
    return v0

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/maps2d/model/Polyline;->a:Lcom/amap/api/a/am;

    check-cast p1, Lcom/amap/api/maps2d/model/Polyline;

    iget-object v1, p1, Lcom/amap/api/maps2d/model/Polyline;->a:Lcom/amap/api/a/am;

    invoke-interface {v0, v1}, Lcom/amap/api/a/am;->a(Lcom/amap/api/a/ak;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 146
    :catch_0
    move-exception v0

    .line 147
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public getColor()I
    .locals 2

    .prologue
    .line 66
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/maps2d/model/Polyline;->a:Lcom/amap/api/a/am;

    invoke-interface {v0}, Lcom/amap/api/a/am;->h()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 67
    :catch_0
    move-exception v0

    .line 68
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public getId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 24
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/maps2d/model/Polyline;->a:Lcom/amap/api/a/am;

    invoke-interface {v0}, Lcom/amap/api/a/am;->c()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 25
    :catch_0
    move-exception v0

    .line 26
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public getPoints()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/maps2d/model/LatLng;",
            ">;"
        }
    .end annotation

    .prologue
    .line 38
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/maps2d/model/Polyline;->a:Lcom/amap/api/a/am;

    invoke-interface {v0}, Lcom/amap/api/a/am;->i()Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 39
    :catch_0
    move-exception v0

    .line 40
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public getWidth()F
    .locals 2

    .prologue
    .line 52
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/maps2d/model/Polyline;->a:Lcom/amap/api/a/am;

    invoke-interface {v0}, Lcom/amap/api/a/am;->g()F
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 53
    :catch_0
    move-exception v0

    .line 54
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public getZIndex()F
    .locals 2

    .prologue
    .line 82
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/maps2d/model/Polyline;->a:Lcom/amap/api/a/am;

    invoke-interface {v0}, Lcom/amap/api/a/am;->d()F
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 83
    :catch_0
    move-exception v0

    .line 84
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 154
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/maps2d/model/Polyline;->a:Lcom/amap/api/a/am;

    invoke-interface {v0}, Lcom/amap/api/a/am;->f()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 155
    :catch_0
    move-exception v0

    .line 156
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public isDottedLine()Z
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/amap/api/maps2d/model/Polyline;->a:Lcom/amap/api/a/am;

    invoke-interface {v0}, Lcom/amap/api/a/am;->j()Z

    move-result v0

    return v0
.end method

.method public isGeodesic()Z
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/amap/api/maps2d/model/Polyline;->a:Lcom/amap/api/a/am;

    invoke-interface {v0}, Lcom/amap/api/a/am;->k()Z

    move-result v0

    return v0
.end method

.method public isVisible()Z
    .locals 2

    .prologue
    .line 98
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/maps2d/model/Polyline;->a:Lcom/amap/api/a/am;

    invoke-interface {v0}, Lcom/amap/api/a/am;->e()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 99
    :catch_0
    move-exception v0

    .line 100
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public remove()V
    .locals 2

    .prologue
    .line 17
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/maps2d/model/Polyline;->a:Lcom/amap/api/a/am;

    invoke-interface {v0}, Lcom/amap/api/a/am;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 21
    return-void

    .line 18
    :catch_0
    move-exception v0

    .line 19
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public setColor(I)V
    .locals 2

    .prologue
    .line 59
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/maps2d/model/Polyline;->a:Lcom/amap/api/a/am;

    invoke-interface {v0, p1}, Lcom/amap/api/a/am;->a(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 63
    return-void

    .line 60
    :catch_0
    move-exception v0

    .line 61
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public setDottedLine(Z)V
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/amap/api/maps2d/model/Polyline;->a:Lcom/amap/api/a/am;

    invoke-interface {v0, p1}, Lcom/amap/api/a/am;->b(Z)V

    .line 127
    return-void
.end method

.method public setGeodesic(Z)V
    .locals 2

    .prologue
    .line 106
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/maps2d/model/Polyline;->a:Lcom/amap/api/a/am;

    invoke-interface {v0}, Lcom/amap/api/a/am;->k()Z

    move-result v0

    if-eq v0, p1, :cond_0

    .line 107
    invoke-virtual {p0}, Lcom/amap/api/maps2d/model/Polyline;->getPoints()Ljava/util/List;

    move-result-object v0

    .line 108
    iget-object v1, p0, Lcom/amap/api/maps2d/model/Polyline;->a:Lcom/amap/api/a/am;

    invoke-interface {v1, p1}, Lcom/amap/api/a/am;->c(Z)V

    .line 109
    invoke-virtual {p0, v0}, Lcom/amap/api/maps2d/model/Polyline;->setPoints(Ljava/util/List;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 114
    :cond_0
    return-void

    .line 111
    :catch_0
    move-exception v0

    .line 112
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public setPoints(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/maps2d/model/LatLng;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 31
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/maps2d/model/Polyline;->a:Lcom/amap/api/a/am;

    invoke-interface {v0, p1}, Lcom/amap/api/a/am;->a(Ljava/util/List;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 35
    return-void

    .line 32
    :catch_0
    move-exception v0

    .line 33
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public setVisible(Z)V
    .locals 2

    .prologue
    .line 90
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/maps2d/model/Polyline;->a:Lcom/amap/api/a/am;

    invoke-interface {v0, p1}, Lcom/amap/api/a/am;->a(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    return-void

    .line 91
    :catch_0
    move-exception v0

    .line 92
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public setWidth(F)V
    .locals 2

    .prologue
    .line 45
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/maps2d/model/Polyline;->a:Lcom/amap/api/a/am;

    invoke-interface {v0, p1}, Lcom/amap/api/a/am;->b(F)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 49
    return-void

    .line 46
    :catch_0
    move-exception v0

    .line 47
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public setZIndex(F)V
    .locals 2

    .prologue
    .line 74
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/maps2d/model/Polyline;->a:Lcom/amap/api/a/am;

    invoke-interface {v0, p1}, Lcom/amap/api/a/am;->a(F)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 78
    return-void

    .line 75
    :catch_0
    move-exception v0

    .line 76
    new-instance v1, Lcom/amap/api/maps2d/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method
