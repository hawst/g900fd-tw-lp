.class public final Lfi/firstbeat/ete/f;
.super Ljava/lang/Object;


# static fields
.field private static final a:[I

.field private static final b:[I

.field private static final c:[I

.field private static final d:[I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x2

    const/16 v0, 0x19

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lfi/firstbeat/ete/f;->a:[I

    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lfi/firstbeat/ete/f;->b:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_2

    sput-object v0, Lfi/firstbeat/ete/f;->c:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_3

    sput-object v0, Lfi/firstbeat/ete/f;->d:[I

    return-void

    :array_0
    .array-data 4
        0x1fd89
        0x1f629
        0x1e9f4
        0x1d907
        0x1c38b
        0x1a9b6
        0x18bc8
        0x16a0a
        0x144cf
        0x11c74
        0xf15b
        0xc3ef
        0x94a0
        0x63e3
        0x322f
        0x0
        -0x322f
        -0x63e3
        -0x94a0
        -0xc3ef
        -0xf15b
        -0x11c74
        -0x144cf
        -0x16a0a
        -0x18bc8
    .end array-data

    :array_1
    .array-data 4
        0x432693
        -0x2fda83
        -0xb812a
        -0xa6118
        -0x7d13dc
        0x34618f
        -0x1c5f1
        0x872ca
    .end array-data

    :array_2
    .array-data 4
        -0x5017d6
        -0x3d0b80
    .end array-data

    :array_3
    .array-data 4
        -0xd5218
        -0x54efe
    .end array-data
.end method

.method public static a([IIILfi/firstbeat/ete/b;)I
    .locals 10

    iget-object v5, p3, Lfi/firstbeat/ete/b;->c:[I

    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    const/4 v1, 0x0

    aput v1, v5, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    if-lez p1, :cond_2

    const/4 v0, 0x4

    const v1, 0xea60

    div-int/2addr v1, p1

    aput v1, v5, v0

    const/4 v0, 0x0

    :goto_1
    const/16 v1, 0x20

    if-ge v0, v1, :cond_2

    aget v1, p0, v0

    if-lez v1, :cond_1

    aget v1, p0, v0

    const/4 v2, 0x4

    aget v2, v5, v2

    sub-int/2addr v1, v2

    aput v1, p0, v0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x7

    const/4 v1, 0x0

    aput v1, v5, v0

    const/16 v0, 0x8

    const/16 v1, 0x2666

    aput v1, v5, v0

    const/4 v0, 0x1

    move v1, v0

    :goto_2
    const/16 v0, 0x19

    if-gt v1, v0, :cond_5

    const/4 v0, 0x4

    const/4 v2, 0x0

    aput v2, v5, v0

    const/4 v0, 0x5

    const/4 v2, 0x0

    aput v2, v5, v0

    const/4 v0, 0x6

    const/4 v2, 0x0

    aput v2, v5, v0

    const/4 v0, 0x0

    :goto_3
    const/16 v2, 0x1f

    if-ge v0, v2, :cond_3

    const/4 v2, 0x4

    aget v3, p0, v0

    shl-int/lit8 v3, v3, 0x10

    sget-object v4, Lfi/firstbeat/ete/f;->a:[I

    add-int/lit8 v6, v1, -0x1

    aget v4, v4, v6

    const/4 v6, 0x5

    aget v6, v5, v6

    invoke-static {v4, v6}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v4

    add-int/2addr v3, v4

    const/4 v4, 0x6

    aget v4, v5, v4

    sub-int/2addr v3, v4

    aput v3, v5, v2

    const/4 v2, 0x6

    const/4 v3, 0x5

    aget v3, v5, v3

    aput v3, v5, v2

    const/4 v2, 0x5

    const/4 v3, 0x4

    aget v3, v5, v3

    aput v3, v5, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_3
    const/4 v0, 0x4

    const/16 v2, 0x1f

    aget v2, p0, v2

    shl-int/lit8 v2, v2, 0x10

    sget-object v3, Lfi/firstbeat/ete/f;->a:[I

    add-int/lit8 v4, v1, -0x1

    aget v3, v3, v4

    const/4 v4, 0x5

    aget v4, v5, v4

    invoke-static {v3, v4}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v3

    add-int/2addr v2, v3

    const/4 v3, 0x6

    aget v3, v5, v3

    sub-int/2addr v2, v3

    aput v2, v5, v0

    const/4 v0, 0x4

    const/4 v2, 0x4

    aget v2, v5, v2

    invoke-static {v2}, Lfi/firstbeat/ete/e;->a(I)I

    move-result v2

    aput v2, v5, v0

    const/4 v0, 0x5

    const/4 v2, 0x5

    aget v2, v5, v2

    invoke-static {v2}, Lfi/firstbeat/ete/e;->a(I)I

    move-result v2

    aput v2, v5, v0

    const/4 v0, 0x4

    const/4 v2, 0x4

    aget v2, v5, v2

    const/4 v3, 0x4

    aget v3, v5, v3

    mul-int/2addr v2, v3

    sget-object v3, Lfi/firstbeat/ete/f;->a:[I

    add-int/lit8 v4, v1, -0x1

    aget v3, v3, v4

    const/4 v4, 0x4

    aget v4, v5, v4

    mul-int/2addr v3, v4

    invoke-static {v3}, Lfi/firstbeat/ete/e;->a(I)I

    move-result v3

    const/4 v4, 0x5

    aget v4, v5, v4

    mul-int/2addr v3, v4

    sub-int/2addr v2, v3

    const/4 v3, 0x5

    aget v3, v5, v3

    const/4 v4, 0x5

    aget v4, v5, v4

    mul-int/2addr v3, v4

    add-int/2addr v2, v3

    aput v2, v5, v0

    const/4 v0, 0x4

    aget v2, v5, v0

    add-int/lit8 v2, v2, 0x1

    aput v2, v5, v0

    const/4 v0, 0x0

    mul-int/lit16 v2, v1, 0xa28

    aput v2, v5, v0

    const/4 v0, 0x2

    aget v2, v5, v0

    const/4 v3, 0x0

    aget v3, v5, v3

    const/4 v4, 0x4

    aget v4, v5, v4

    invoke-static {v3, v4}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v3

    add-int/2addr v2, v3

    aput v2, v5, v0

    const/4 v0, 0x3

    aget v2, v5, v0

    const/4 v3, 0x4

    aget v3, v5, v3

    add-int/2addr v2, v3

    aput v2, v5, v0

    const/4 v0, 0x4

    aget v0, v5, v0

    const/4 v2, 0x7

    aget v2, v5, v2

    if-le v0, v2, :cond_4

    const/4 v0, 0x0

    aget v0, v5, v0

    const/16 v2, 0x2666

    if-lt v0, v2, :cond_4

    const/4 v0, 0x7

    const/4 v2, 0x4

    aget v2, v5, v2

    aput v2, v5, v0

    const/16 v0, 0x8

    const/4 v2, 0x0

    aget v2, v5, v2

    aput v2, v5, v0

    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_2

    :cond_5
    const/4 v0, 0x1

    shl-int/lit8 v1, p1, 0x10

    div-int/lit16 v1, v1, 0xc8

    aput v1, v5, v0

    const/4 v0, 0x2

    const/4 v1, 0x2

    aget v1, v5, v1

    const/4 v2, 0x3

    aget v2, v5, v2

    invoke-static {v1, v2}, Lfi/firstbeat/ete/e;->b(II)I

    move-result v1

    aput v1, v5, v0

    const/4 v0, 0x3

    const/16 v1, 0x8

    aget v1, v5, v1

    aput v1, v5, v0

    const/4 v0, 0x7

    const/4 v1, 0x0

    aput v1, v5, v0

    const/16 v0, 0x8

    aput p2, v5, v0

    const/4 v0, 0x1

    move v1, v0

    :goto_4
    const/16 v0, 0x19

    if-gt v1, v0, :cond_b

    const/4 v0, 0x4

    const/4 v2, 0x0

    aput v2, v5, v0

    const/4 v0, 0x5

    const/4 v2, 0x0

    aput v2, v5, v0

    const/4 v0, 0x6

    const/4 v2, 0x0

    aput v2, v5, v0

    const/4 v0, 0x0

    :goto_5
    const/16 v2, 0x1f

    if-ge v0, v2, :cond_6

    const/4 v2, 0x4

    aget v3, p0, v0

    shl-int/lit8 v3, v3, 0x10

    sget-object v4, Lfi/firstbeat/ete/f;->a:[I

    add-int/lit8 v6, v1, -0x1

    aget v4, v4, v6

    const/4 v6, 0x5

    aget v6, v5, v6

    invoke-static {v4, v6}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v4

    add-int/2addr v3, v4

    const/4 v4, 0x6

    aget v4, v5, v4

    sub-int/2addr v3, v4

    aput v3, v5, v2

    const/4 v2, 0x6

    const/4 v3, 0x5

    aget v3, v5, v3

    aput v3, v5, v2

    const/4 v2, 0x5

    const/4 v3, 0x4

    aget v3, v5, v3

    aput v3, v5, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_6
    const/4 v0, 0x4

    const/16 v2, 0x1f

    aget v2, p0, v2

    shl-int/lit8 v2, v2, 0x10

    sget-object v3, Lfi/firstbeat/ete/f;->a:[I

    add-int/lit8 v4, v1, -0x1

    aget v3, v3, v4

    const/4 v4, 0x5

    aget v4, v5, v4

    invoke-static {v3, v4}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v3

    add-int/2addr v2, v3

    const/4 v3, 0x6

    aget v3, v5, v3

    sub-int/2addr v2, v3

    aput v2, v5, v0

    const/4 v0, 0x4

    const/4 v2, 0x4

    aget v2, v5, v2

    invoke-static {v2}, Lfi/firstbeat/ete/e;->a(I)I

    move-result v2

    aput v2, v5, v0

    const/4 v0, 0x5

    const/4 v2, 0x5

    aget v2, v5, v2

    invoke-static {v2}, Lfi/firstbeat/ete/e;->a(I)I

    move-result v2

    aput v2, v5, v0

    const/4 v0, 0x4

    const/4 v2, 0x4

    aget v2, v5, v2

    const/4 v3, 0x4

    aget v3, v5, v3

    mul-int/2addr v2, v3

    sget-object v3, Lfi/firstbeat/ete/f;->a:[I

    add-int/lit8 v4, v1, -0x1

    aget v3, v3, v4

    const/4 v4, 0x4

    aget v4, v5, v4

    mul-int/2addr v3, v4

    invoke-static {v3}, Lfi/firstbeat/ete/e;->a(I)I

    move-result v3

    const/4 v4, 0x5

    aget v4, v5, v4

    mul-int/2addr v3, v4

    sub-int/2addr v2, v3

    const/4 v3, 0x5

    aget v3, v5, v3

    const/4 v4, 0x5

    aget v4, v5, v4

    mul-int/2addr v3, v4

    add-int/2addr v2, v3

    aput v2, v5, v0

    const/4 v0, 0x4

    aget v2, v5, v0

    add-int/lit8 v2, v2, 0x1

    aput v2, v5, v0

    const/4 v0, 0x0

    mul-int/lit16 v2, v1, 0xa28

    aput v2, v5, v0

    const/16 v0, 0xa

    aget v0, v5, v0

    const v2, 0x9999

    if-ge v0, v2, :cond_9

    const/16 v6, 0xa

    const v0, 0x8bdd

    const/4 v2, 0x0

    move v3, v0

    move v4, v2

    :goto_6
    const/4 v0, 0x2

    if-ge v4, v0, :cond_8

    sget-object v0, Lfi/firstbeat/ete/f;->d:[I

    aget v2, v0, v4

    const/4 v0, 0x0

    :goto_7
    const/4 v7, 0x4

    if-ge v0, v7, :cond_7

    aget v7, v5, v0

    sget-object v8, Lfi/firstbeat/ete/f;->b:[I

    shl-int/lit8 v9, v4, 0x2

    add-int/2addr v9, v0

    aget v8, v8, v9

    invoke-static {v7, v8}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v7

    add-int/2addr v2, v7

    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_7
    invoke-static {v2}, Lfi/firstbeat/ete/e;->c(I)I

    move-result v0

    sget-object v2, Lfi/firstbeat/ete/f;->c:[I

    aget v2, v2, v4

    invoke-static {v0, v2}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v0

    add-int/2addr v0, v3

    add-int/lit8 v2, v4, 0x1

    move v3, v0

    move v4, v2

    goto :goto_6

    :cond_8
    invoke-static {v3}, Lfi/firstbeat/ete/e;->c(I)I

    move-result v0

    aput v0, v5, v6

    :cond_9
    const/16 v0, 0x9

    const/4 v2, 0x4

    aget v2, v5, v2

    const/16 v3, 0xa

    aget v3, v5, v3

    invoke-static {v2, v3}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v2

    aput v2, v5, v0

    const/16 v0, 0x9

    aget v0, v5, v0

    const/4 v2, 0x7

    aget v2, v5, v2

    if-le v0, v2, :cond_a

    const/4 v0, 0x7

    const/16 v2, 0x9

    aget v2, v5, v2

    aput v2, v5, v0

    const/16 v0, 0x8

    const/4 v2, 0x0

    aget v2, v5, v2

    aput v2, v5, v0

    :cond_a
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_4

    :cond_b
    iget v0, p3, Lfi/firstbeat/ete/b;->B:I

    if-nez v0, :cond_c

    const/16 v0, 0x8

    aget v0, v5, v0

    iput v0, p3, Lfi/firstbeat/ete/b;->B:I

    :cond_c
    const/16 v0, 0x8

    aget v0, v5, v0

    const/high16 v1, 0x20000

    iget v2, p3, Lfi/firstbeat/ete/b;->B:I

    invoke-static {v1, v2}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v1

    add-int/2addr v0, v1

    const/high16 v1, 0x30000

    invoke-static {v0, v1}, Lfi/firstbeat/ete/e;->b(II)I

    move-result v0

    iput v0, p3, Lfi/firstbeat/ete/b;->B:I

    iget v0, p3, Lfi/firstbeat/ete/b;->B:I

    return v0
.end method

.method public static a([I[I)V
    .locals 10

    const/high16 v4, 0x1900000

    const/16 v9, 0x20

    const/4 v3, 0x0

    const/16 v0, 0x1f

    move v1, v4

    move v5, v3

    move v2, v3

    move v6, v0

    :goto_0
    if-lez v6, :cond_4

    aget v0, p0, v6

    shl-int/lit8 v7, v0, 0x10

    if-le v1, v7, :cond_0

    sub-int v0, v1, v7

    div-int/lit16 v1, v7, 0x190

    invoke-static {v7, v1}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v1

    add-int/2addr v1, v5

    :goto_1
    add-int/lit8 v5, v6, -0x1

    move v6, v5

    move v5, v1

    move v1, v0

    goto :goto_0

    :cond_0
    div-int/lit16 v0, v1, 0x190

    invoke-static {v7, v0}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v0

    add-int/2addr v0, v5

    invoke-static {v0}, Lfi/firstbeat/ete/e;->a(I)I

    move-result v0

    aput v0, p1, v2

    add-int/lit8 v2, v2, 0x1

    if-lt v2, v9, :cond_2

    :cond_1
    return-void

    :cond_2
    sub-int v0, v7, v1

    div-int/lit16 v0, v0, 0x190

    shr-int/lit8 v5, v0, 0x10

    move v0, v3

    :goto_2
    if-ge v0, v5, :cond_3

    aget v8, p0, v6

    aput v8, p1, v2

    add-int/lit8 v2, v2, 0x1

    if-ge v2, v9, :cond_1

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    sub-int v0, v7, v1

    mul-int/lit16 v1, v5, 0x190

    shl-int/lit8 v1, v1, 0x10

    sub-int v1, v0, v1

    sub-int v0, v4, v1

    div-int/lit16 v1, v1, 0x190

    invoke-static {v7, v1}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v1

    goto :goto_1

    :cond_4
    :goto_3
    if-ge v2, v9, :cond_1

    add-int/lit8 v0, v2, -0x1

    aget v0, p1, v0

    aput v0, p1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_3
.end method
