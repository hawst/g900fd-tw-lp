.class public final Lfi/firstbeat/ete/d;
.super Ljava/lang/Object;


# static fields
.field private static final a:[I

.field private static final b:[I

.field private static final c:[I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x3

    const/16 v0, 0xc

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lfi/firstbeat/ete/d;->a:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lfi/firstbeat/ete/d;->b:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_2

    sput-object v0, Lfi/firstbeat/ete/d;->c:[I

    return-void

    nop

    :array_0
    .array-data 4
        -0x9283b
        -0x415f
        -0x2204e
        0x1dee5
        0x1025fe
        0x13417a
        -0x1abd2b
        0x1d415f
        0x13b07
        0x1c2a5
        0x117560
        -0x115594
    .end array-data

    :array_1
    .array-data 4
        -0x1c5dfd
        0x20cd
        0xaa05
    .end array-data

    :array_2
    .array-data 4
        0xf022d
        -0xc4dec
        -0x29827
    .end array-data
.end method

.method public static a(IIIIII)I
    .locals 9

    const/4 v0, 0x4

    new-array v4, v0, [I

    const/4 v0, 0x0

    shl-int/lit8 v1, p0, 0x10

    shl-int/lit8 v2, p2, 0x10

    invoke-static {v1, v2}, Lfi/firstbeat/ete/e;->b(II)I

    move-result v1

    aput v1, v4, v0

    const/4 v0, 0x1

    aput p1, v4, v0

    const/4 v0, 0x2

    sub-int v1, p4, p3

    div-int/lit8 v1, v1, 0x5

    aput v1, v4, v0

    const/4 v0, 0x3

    sub-int v1, p4, p5

    div-int/lit8 v1, v1, 0x5

    aput v1, v4, v0

    const/high16 v5, 0x640000

    const v0, 0x1c54cf

    const/4 v1, 0x0

    move v3, v1

    :goto_0
    const/4 v1, 0x3

    if-ge v3, v1, :cond_1

    sget-object v1, Lfi/firstbeat/ete/d;->c:[I

    aget v1, v1, v3

    const/4 v2, 0x0

    :goto_1
    const/4 v6, 0x4

    if-ge v2, v6, :cond_0

    aget v6, v4, v2

    sget-object v7, Lfi/firstbeat/ete/d;->a:[I

    shl-int/lit8 v8, v3, 0x2

    add-int/2addr v8, v2

    aget v7, v7, v8

    invoke-static {v6, v7}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v6

    add-int/2addr v1, v6

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_0
    invoke-static {v1}, Lfi/firstbeat/ete/e;->c(I)I

    move-result v1

    sget-object v2, Lfi/firstbeat/ete/d;->b:[I

    aget v2, v2, v3

    invoke-static {v1, v2}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    :cond_1
    const/16 v1, 0x7ae

    if-ge v0, v1, :cond_2

    const/16 v0, 0x7ae

    :cond_2
    const/high16 v1, 0x10000

    if-le v0, v1, :cond_3

    const/high16 v0, 0x10000

    :cond_3
    invoke-static {v5, v0}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v0

    invoke-static {v0}, Lfi/firstbeat/ete/e;->a(I)I

    move-result v0

    return v0
.end method
