.class public final Lfi/firstbeat/ete/a;
.super Ljava/lang/Object;


# static fields
.field private static final a:[[I

.field private static final b:[[I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/16 v0, 0x9

    new-array v0, v0, [[I

    new-array v1, v3, [I

    fill-array-data v1, :array_0

    aput-object v1, v0, v4

    new-array v1, v3, [I

    fill-array-data v1, :array_1

    aput-object v1, v0, v5

    new-array v1, v3, [I

    fill-array-data v1, :array_2

    aput-object v1, v0, v3

    new-array v1, v3, [I

    fill-array-data v1, :array_3

    aput-object v1, v0, v6

    new-array v1, v3, [I

    fill-array-data v1, :array_4

    aput-object v1, v0, v7

    const/4 v1, 0x5

    new-array v2, v3, [I

    fill-array-data v2, :array_5

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v3, [I

    fill-array-data v2, :array_6

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-array v2, v3, [I

    fill-array-data v2, :array_7

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-array v2, v3, [I

    fill-array-data v2, :array_8

    aput-object v2, v0, v1

    sput-object v0, Lfi/firstbeat/ete/a;->a:[[I

    const/16 v0, 0xb

    new-array v0, v0, [[I

    new-array v1, v3, [I

    fill-array-data v1, :array_9

    aput-object v1, v0, v4

    new-array v1, v3, [I

    fill-array-data v1, :array_a

    aput-object v1, v0, v5

    new-array v1, v3, [I

    fill-array-data v1, :array_b

    aput-object v1, v0, v3

    new-array v1, v3, [I

    fill-array-data v1, :array_c

    aput-object v1, v0, v6

    new-array v1, v3, [I

    fill-array-data v1, :array_d

    aput-object v1, v0, v7

    const/4 v1, 0x5

    new-array v2, v3, [I

    fill-array-data v2, :array_e

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v3, [I

    fill-array-data v2, :array_f

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-array v2, v3, [I

    fill-array-data v2, :array_10

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-array v2, v3, [I

    fill-array-data v2, :array_11

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-array v2, v3, [I

    fill-array-data v2, :array_12

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-array v2, v3, [I

    fill-array-data v2, :array_13

    aput-object v2, v0, v1

    sput-object v0, Lfi/firstbeat/ete/a;->b:[[I

    return-void

    :array_0
    .array-data 4
        0xfa
        0xfa
    .end array-data

    :array_1
    .array-data 4
        0x12c
        0x122
    .end array-data

    :array_2
    .array-data 4
        0x190
        0x17c
    .end array-data

    :array_3
    .array-data 4
        0x258
        0x226
    .end array-data

    :array_4
    .array-data 4
        0x44c
        0x352
    .end array-data

    :array_5
    .array-data 4
        0x578
        0x438
    .end array-data

    :array_6
    .array-data 4
        0x640
        0x55f
    .end array-data

    :array_7
    .array-data 4
        0x6d6
        0x60e
    .end array-data

    :array_8
    .array-data 4
        0x73a
        0x6ef
    .end array-data

    :array_9
    .array-data 4
        0xfa
        0xfb
    .end array-data

    :array_a
    .array-data 4
        0x12c
        0x136
    .end array-data

    :array_b
    .array-data 4
        0x190
        0x1a4
    .end array-data

    :array_c
    .array-data 4
        0x1f4
        0x23a
    .end array-data

    :array_d
    .array-data 4
        0x352
        0x47e
    .end array-data

    :array_e
    .array-data 4
        0x41a
        0x578
    .end array-data

    :array_f
    .array-data 4
        0x4b0
        0x5dc
    .end array-data

    :array_10
    .array-data 4
        0x640
        0x6d6
    .end array-data

    :array_11
    .array-data 4
        0x6d6
        0x6f4
    .end array-data

    :array_12
    .array-data 4
        0x708
        0x708
    .end array-data

    :array_13
    .array-data 4
        0xbb8
        0xbb8
    .end array-data
.end method

.method private static a(IIILfi/firstbeat/ete/b;)I
    .locals 9

    const/16 v8, 0xbb8

    const/16 v7, 0x1b

    const/4 v0, 0x0

    const/16 v6, 0x19

    const/16 v5, 0x1a

    iget-object v1, p3, Lfi/firstbeat/ete/b;->b:[I

    aput v0, v1, v6

    aput p0, v1, v5

    aput v0, v1, v7

    if-ge p0, p1, :cond_4

    :cond_0
    aget v2, v1, v7

    const/16 v3, 0x14

    if-lt v2, v3, :cond_3

    aput v8, v1, v5

    :cond_1
    :goto_0
    aget v1, v1, v5

    if-le p1, v1, :cond_6

    :cond_2
    :goto_1
    return v0

    :cond_3
    aget v2, v1, v7

    add-int/lit8 v2, v2, 0x1

    aput v2, v1, v7

    aget v2, v1, v5

    const/16 v3, 0xb

    sget-object v4, Lfi/firstbeat/ete/a;->b:[[I

    invoke-static {v2, v3, v4}, Lfi/firstbeat/ete/a;->a(II[[I)I

    move-result v2

    aput v2, v1, v5

    aget v2, v1, v6

    aget v3, v1, v5

    add-int/2addr v2, v3

    aput v2, v1, v6

    aget v2, v1, v6

    if-ge v2, p2, :cond_1

    aget v2, v1, v5

    if-ge v2, v8, :cond_1

    aget v2, v1, v5

    if-gt p1, v2, :cond_0

    goto :goto_0

    :cond_4
    aget v2, v1, v5

    const/16 v3, 0x9

    sget-object v4, Lfi/firstbeat/ete/a;->a:[[I

    invoke-static {v2, v3, v4}, Lfi/firstbeat/ete/a;->a(II[[I)I

    move-result v2

    aput v2, v1, v5

    aget v2, v1, v6

    aget v3, v1, v5

    add-int/2addr v2, v3

    aput v2, v1, v6

    aget v2, v1, v6

    if-ge v2, p2, :cond_5

    aget v2, v1, v5

    const/16 v3, 0xfa

    if-le v2, v3, :cond_5

    aget v2, v1, v5

    if-lt p1, v2, :cond_4

    :cond_5
    aget v1, v1, v5

    if-lt p1, v1, :cond_2

    :cond_6
    const/4 v0, 0x1

    goto :goto_1
.end method

.method private static a(II[[I)I
    .locals 6

    const/4 v1, 0x1

    const/4 v5, 0x0

    add-int/lit8 v0, p1, -0x1

    aget-object v0, p2, v0

    aget v0, v0, v5

    if-le p0, v0, :cond_0

    add-int/lit8 v0, p1, -0x1

    aget-object v0, p2, v0

    aget v0, v0, v1

    :goto_0
    return v0

    :cond_0
    aget-object v0, p2, v5

    aget v0, v0, v5

    if-ge p0, v0, :cond_2

    aget-object v0, p2, v5

    aget v0, v0, v1

    goto :goto_0

    :goto_1
    aget-object v2, p2, v0

    aget v2, v2, v5

    if-ge v2, p0, :cond_1

    add-int/lit8 v2, p1, -0x1

    if-ge v0, v2, :cond_1

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v2, v0, -0x1

    aget-object v2, p2, v2

    aget v2, v2, v1

    aget-object v3, p2, v0

    aget v3, v3, v1

    add-int/lit8 v4, v0, -0x1

    aget-object v4, p2, v4

    aget v1, v4, v1

    sub-int v1, v3, v1

    add-int/lit8 v3, v0, -0x1

    aget-object v3, p2, v3

    aget v3, v3, v5

    sub-int v3, p0, v3

    mul-int/2addr v1, v3

    aget-object v3, p2, v0

    aget v3, v3, v5

    add-int/lit8 v0, v0, -0x1

    aget-object v0, p2, v0

    aget v0, v0, v5

    sub-int v0, v3, v0

    div-int v0, v1, v0

    add-int/2addr v0, v2

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public static a(I[I)I
    .locals 3

    const/4 v0, 0x1

    div-int/lit8 v1, p0, 0x8

    aget v1, p1, v1

    rem-int/lit8 v2, p0, 0x8

    shl-int v2, v0, v2

    and-int/2addr v1, v2

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(II[I)V
    .locals 4

    const/4 v3, 0x1

    if-ne p1, v3, :cond_0

    div-int/lit8 v0, p0, 0x8

    div-int/lit8 v1, p0, 0x8

    aget v1, p2, v1

    rem-int/lit8 v2, p0, 0x8

    shl-int v2, p1, v2

    or-int/2addr v1, v2

    aput v1, p2, v0

    :goto_0
    return-void

    :cond_0
    div-int/lit8 v0, p0, 0x8

    div-int/lit8 v1, p0, 0x8

    aget v1, p2, v1

    rem-int/lit8 v2, p0, 0x8

    shl-int v2, v3, v2

    xor-int/lit8 v2, v2, -0x1

    and-int/2addr v1, v2

    aput v1, p2, v0

    goto :goto_0
.end method

.method public static a([I[ILfi/firstbeat/ete/b;)V
    .locals 9

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x4

    if-ge v0, v1, :cond_0

    const/4 v1, 0x0

    aput v1, p1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_1
    const/16 v1, 0x20

    if-ge v0, v1, :cond_3

    aget v1, p0, v0

    const/16 v2, 0x11d

    if-lt v1, v2, :cond_1

    aget v1, p0, v0

    const/16 v2, 0x708

    if-le v1, v2, :cond_2

    :cond_1
    const/4 v1, 0x1

    invoke-static {v0, v1, p1}, Lfi/firstbeat/ete/a;->a(II[I)V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    const/4 v0, 0x4

    new-array v4, v0, [I

    iget-object v5, p2, Lfi/firstbeat/ete/b;->b:[I

    const/4 v1, 0x1

    const/4 v0, 0x0

    :goto_2
    const/4 v2, 0x4

    if-ge v0, v2, :cond_4

    aget v2, p1, v0

    aput v2, v4, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    if-eqz v1, :cond_e

    const/4 v1, 0x0

    const/16 v0, -0x29a

    const/4 v3, 0x0

    :goto_3
    const/16 v2, 0x20

    if-ge v3, v2, :cond_4

    invoke-static {v3, v4}, Lfi/firstbeat/ete/a;->b(I[I)I

    move-result v2

    if-nez v2, :cond_d

    const/16 v2, -0x29a

    if-ne v0, v2, :cond_5

    add-int/lit8 v0, v3, -0x1

    const/16 v2, 0x9

    const/4 v6, 0x0

    aput v6, v5, v2

    :goto_4
    if-ltz v0, :cond_5

    invoke-static {v0, p1}, Lfi/firstbeat/ete/a;->a(I[I)I

    move-result v2

    const/4 v6, 0x1

    if-ne v2, v6, :cond_5

    const/16 v2, 0x9

    aget v6, v5, v2

    aget v7, p0, v0

    add-int/2addr v6, v7

    aput v6, v5, v2

    add-int/lit8 v0, v0, -0x1

    goto :goto_4

    :cond_5
    add-int/lit8 v2, v3, 0x1

    const/16 v6, 0xa

    const/4 v7, 0x0

    aput v7, v5, v6

    :goto_5
    const/16 v6, 0x20

    if-ge v2, v6, :cond_6

    invoke-static {v2, p1}, Lfi/firstbeat/ete/a;->a(I[I)I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_6

    const/16 v6, 0xa

    aget v7, v5, v6

    aget v8, p0, v2

    add-int/2addr v7, v8

    aput v7, v5, v6

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_6
    if-eqz v3, :cond_7

    add-int/lit8 v6, v3, -0x1

    invoke-static {v6, p1}, Lfi/firstbeat/ete/a;->a(I[I)I

    move-result v6

    if-eqz v6, :cond_8

    :cond_7
    add-int/lit8 v6, v3, 0x1

    const/16 v7, 0x20

    if-eq v6, v7, :cond_a

    add-int/lit8 v6, v3, 0x1

    invoke-static {v6, p1}, Lfi/firstbeat/ete/a;->a(I[I)I

    move-result v6

    if-nez v6, :cond_a

    :cond_8
    if-ltz v0, :cond_9

    aget v0, p0, v0

    aget v6, p0, v3

    const/16 v7, 0x9

    aget v7, v5, v7

    invoke-static {v0, v6, v7, p2}, Lfi/firstbeat/ete/a;->a(IIILfi/firstbeat/ete/b;)I

    move-result v0

    if-eqz v0, :cond_a

    :cond_9
    const/16 v0, 0x20

    if-ge v2, v0, :cond_b

    aget v0, p0, v3

    aget v6, p0, v2

    const/16 v7, 0xa

    aget v7, v5, v7

    invoke-static {v0, v6, v7, p2}, Lfi/firstbeat/ete/a;->a(IIILfi/firstbeat/ete/b;)I

    move-result v0

    if-nez v0, :cond_b

    :cond_a
    const/4 v0, 0x1

    :goto_6
    const/4 v6, 0x1

    if-ne v0, v6, :cond_c

    div-int/lit8 v6, v3, 0x8

    div-int/lit8 v7, v3, 0x8

    aget v7, v4, v7

    rem-int/lit8 v8, v3, 0x8

    shl-int/2addr v0, v8

    or-int/2addr v0, v7

    aput v0, v4, v6

    :goto_7
    invoke-static {v3, v4}, Lfi/firstbeat/ete/a;->b(I[I)I

    move-result v0

    const/4 v6, 0x1

    if-ne v0, v6, :cond_19

    const/4 v0, 0x1

    :goto_8
    const/16 v1, 0x9

    const/16 v6, 0xa

    aget v6, v5, v6

    aput v6, v5, v1

    move v1, v0

    move v0, v3

    move v3, v2

    goto/16 :goto_3

    :cond_b
    const/4 v0, 0x0

    goto :goto_6

    :cond_c
    div-int/lit8 v0, v3, 0x8

    div-int/lit8 v6, v3, 0x8

    aget v6, v4, v6

    const/4 v7, 0x1

    rem-int/lit8 v8, v3, 0x8

    shl-int/2addr v7, v8

    xor-int/lit8 v7, v7, -0x1

    and-int/2addr v6, v7

    aput v6, v4, v0

    goto :goto_7

    :cond_d
    add-int/lit8 v3, v3, 0x1

    const/16 v0, -0x29a

    goto/16 :goto_3

    :cond_e
    const/4 v0, 0x0

    :goto_9
    const/4 v1, 0x4

    if-ge v0, v1, :cond_f

    aget v1, v4, v0

    aput v1, p1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    :cond_f
    iget-object v0, p2, Lfi/firstbeat/ete/b;->b:[I

    const/4 v1, 0x0

    const/4 v2, -0x1

    aput v2, v0, v1

    const/4 v1, 0x1

    const/4 v2, -0x1

    aput v2, v0, v1

    const/4 v1, 0x5

    const/4 v2, 0x0

    aput v2, v0, v1

    :goto_a
    const/4 v1, 0x5

    aget v1, v0, v1

    const/16 v2, 0x20

    if-ge v1, v2, :cond_10

    const/4 v1, 0x5

    aget v1, v0, v1

    invoke-static {v1, p1}, Lfi/firstbeat/ete/a;->a(I[I)I

    move-result v1

    if-eqz v1, :cond_10

    const/4 v1, 0x5

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    goto :goto_a

    :cond_10
    const/4 v1, 0x5

    aget v1, v0, v1

    const/16 v2, 0x20

    if-eq v1, v2, :cond_16

    const/4 v1, 0x5

    const/4 v2, 0x0

    aput v2, v0, v1

    :goto_b
    const/4 v1, 0x5

    aget v1, v0, v1

    const/16 v2, 0x20

    if-ge v1, v2, :cond_16

    const/4 v1, 0x5

    aget v1, v0, v1

    invoke-static {v1, p1}, Lfi/firstbeat/ete/a;->a(I[I)I

    move-result v1

    if-eqz v1, :cond_15

    const/4 v1, 0x5

    aget v1, v0, v1

    aget v1, p0, v1

    const/16 v2, 0xbb8

    if-ge v1, v2, :cond_15

    const/4 v1, 0x5

    aget v1, v0, v1

    aget v1, p0, v1

    const/16 v2, 0xfa

    if-le v1, v2, :cond_15

    const/4 v1, 0x1

    aget v1, v0, v1

    const/4 v2, 0x5

    aget v2, v0, v2

    if-ge v1, v2, :cond_11

    const/4 v1, 0x1

    const/4 v2, 0x5

    aget v2, v0, v2

    aput v2, v0, v1

    :goto_c
    const/4 v1, 0x1

    aget v1, v0, v1

    const/16 v2, 0x20

    if-ge v1, v2, :cond_11

    const/4 v1, 0x1

    aget v1, v0, v1

    invoke-static {v1, p1}, Lfi/firstbeat/ete/a;->a(I[I)I

    move-result v1

    if-eqz v1, :cond_11

    const/4 v1, 0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    goto :goto_c

    :cond_11
    const/4 v1, 0x0

    aget v1, v0, v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_13

    const/4 v1, 0x2

    const/4 v2, 0x5

    aget v2, v0, v2

    aget v2, p0, v2

    aput v2, v0, v1

    const/4 v1, 0x3

    const/4 v2, 0x1

    aget v2, v0, v2

    aget v2, p0, v2

    aput v2, v0, v1

    :goto_d
    const/4 v1, 0x2

    aget v1, v0, v1

    const/4 v2, 0x5

    aget v2, v0, v2

    aget v2, p0, v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3, p2}, Lfi/firstbeat/ete/a;->a(IIILfi/firstbeat/ete/b;)I

    move-result v1

    if-eqz v1, :cond_12

    const/4 v1, 0x5

    aget v1, v0, v1

    aget v1, p0, v1

    const/4 v2, 0x3

    aget v2, v0, v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3, p2}, Lfi/firstbeat/ete/a;->a(IIILfi/firstbeat/ete/b;)I

    move-result v1

    if-eqz v1, :cond_12

    const/4 v1, 0x5

    aget v1, v0, v1

    const/4 v2, 0x0

    invoke-static {v1, v2, p1}, Lfi/firstbeat/ete/a;->a(II[I)V

    :cond_12
    :goto_e
    const/4 v1, 0x5

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    goto/16 :goto_b

    :cond_13
    const/4 v1, 0x1

    aget v1, v0, v1

    const/16 v2, 0x20

    if-ne v1, v2, :cond_14

    const/4 v1, 0x2

    const/4 v2, 0x0

    aget v2, v0, v2

    aget v2, p0, v2

    aput v2, v0, v1

    const/4 v1, 0x3

    const/4 v2, 0x5

    aget v2, v0, v2

    aget v2, p0, v2

    aput v2, v0, v1

    goto :goto_d

    :cond_14
    const/4 v1, 0x4

    const/4 v2, 0x1

    aget v2, v0, v2

    aget v2, p0, v2

    const/4 v3, 0x0

    aget v3, v0, v3

    aget v3, p0, v3

    sub-int/2addr v2, v3

    const/4 v3, 0x1

    aget v3, v0, v3

    const/4 v4, 0x0

    aget v4, v0, v4

    sub-int/2addr v3, v4

    div-int/2addr v2, v3

    aput v2, v0, v1

    const/4 v1, 0x2

    const/4 v2, 0x0

    aget v2, v0, v2

    aget v2, p0, v2

    const/4 v3, 0x4

    aget v3, v0, v3

    const/4 v4, 0x5

    aget v4, v0, v4

    add-int/lit8 v4, v4, -0x1

    const/4 v5, 0x0

    aget v5, v0, v5

    sub-int/2addr v4, v5

    mul-int/2addr v3, v4

    add-int/2addr v2, v3

    aput v2, v0, v1

    const/4 v1, 0x3

    const/4 v2, 0x0

    aget v2, v0, v2

    aget v2, p0, v2

    const/4 v3, 0x4

    aget v3, v0, v3

    const/4 v4, 0x5

    aget v4, v0, v4

    add-int/lit8 v4, v4, 0x1

    const/4 v5, 0x0

    aget v5, v0, v5

    sub-int/2addr v4, v5

    mul-int/2addr v3, v4

    add-int/2addr v2, v3

    aput v2, v0, v1

    goto/16 :goto_d

    :cond_15
    const/4 v1, 0x5

    aget v1, v0, v1

    invoke-static {v1, p1}, Lfi/firstbeat/ete/a;->a(I[I)I

    move-result v1

    if-nez v1, :cond_12

    const/4 v1, 0x0

    const/4 v2, 0x5

    aget v2, v0, v2

    aput v2, v0, v1

    goto :goto_e

    :cond_16
    const/4 v0, 0x1

    :goto_f
    const/16 v1, 0x1f

    if-ge v0, v1, :cond_18

    add-int/lit8 v1, v0, -0x1

    invoke-static {v1, p1}, Lfi/firstbeat/ete/a;->a(I[I)I

    move-result v1

    if-eqz v1, :cond_17

    add-int/lit8 v1, v0, 0x1

    invoke-static {v1, p1}, Lfi/firstbeat/ete/a;->a(I[I)I

    move-result v1

    if-eqz v1, :cond_17

    const/4 v1, 0x1

    invoke-static {v0, v1, p1}, Lfi/firstbeat/ete/a;->a(II[I)V

    :cond_17
    add-int/lit8 v0, v0, 0x1

    goto :goto_f

    :cond_18
    return-void

    :cond_19
    move v0, v1

    goto/16 :goto_8
.end method

.method public static a([I[I[ILfi/firstbeat/ete/b;)V
    .locals 11

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v1, 0x0

    const/4 v8, 0x1

    const/16 v7, 0x9

    iget-object v2, p3, Lfi/firstbeat/ete/b;->b:[I

    const/16 v0, 0x1f

    aput v0, v2, v1

    const/16 v0, 0x1f

    aput v0, v2, v8

    aput v9, v2, v7

    :goto_0
    aget v0, v2, v7

    if-ge v0, v7, :cond_12

    aget v0, v2, v7

    aput v1, v2, v0

    aget v0, v2, v7

    add-int/lit8 v0, v0, 0x1

    aput v0, v2, v7

    goto :goto_0

    :cond_0
    const/4 v3, 0x4

    aget v3, v2, v3

    if-nez v3, :cond_f

    const/4 v3, 0x6

    aget v3, v2, v3

    const/4 v4, 0x5

    aget v4, v2, v4

    if-lt v3, v4, :cond_1

    aget v3, v2, v9

    if-nez v3, :cond_e

    :cond_1
    aget v3, v2, v8

    add-int/lit8 v3, v3, 0x1

    aget v4, p1, v3

    add-int/2addr v4, v0

    aput v4, p1, v3

    :cond_2
    :goto_1
    aget v3, v2, v9

    add-int/lit8 v3, v3, -0x1

    aput v3, v2, v1

    aput v1, v2, v9

    aput v1, v2, v10

    :goto_2
    aget v3, v2, v1

    if-ltz v3, :cond_10

    aget v3, v2, v8

    if-ltz v3, :cond_10

    aget v3, v2, v1

    aput v3, v2, v7

    :goto_3
    aget v3, v2, v7

    if-ltz v3, :cond_3

    aget v3, v2, v7

    invoke-static {v3, p2}, Lfi/firstbeat/ete/a;->a(I[I)I

    move-result v3

    if-ne v3, v8, :cond_6

    aget v3, v2, v7

    aput v3, v2, v10

    :cond_3
    aget v3, v2, v10

    aput v3, v2, v7

    :goto_4
    aget v3, v2, v7

    if-ltz v3, :cond_4

    aget v3, v2, v7

    invoke-static {v3, p2}, Lfi/firstbeat/ete/a;->a(I[I)I

    move-result v3

    if-nez v3, :cond_8

    aget v3, v2, v7

    add-int/lit8 v3, v3, 0x1

    aput v3, v2, v9

    :cond_4
    aget v3, v2, v8

    if-gez v3, :cond_9

    :cond_5
    return-void

    :cond_6
    aget v3, v2, v8

    if-ltz v3, :cond_7

    aget v3, v2, v8

    add-int/lit8 v4, v3, -0x1

    aput v4, v2, v8

    aget v4, v2, v7

    aget v4, p0, v4

    aput v4, p1, v3

    :cond_7
    aget v3, v2, v7

    add-int/lit8 v3, v3, -0x1

    aput v3, v2, v7

    goto :goto_3

    :cond_8
    aget v3, v2, v7

    aget v3, p0, v3

    add-int/2addr v0, v3

    aget v3, v2, v7

    add-int/lit8 v3, v3, -0x1

    aput v3, v2, v7

    goto :goto_4

    :cond_9
    aget v3, v2, v10

    if-nez v3, :cond_a

    invoke-static {v1, p2}, Lfi/firstbeat/ete/a;->a(I[I)I

    move-result v3

    if-eqz v3, :cond_10

    :cond_a
    aget v3, v2, v9

    if-nez v3, :cond_b

    aget v3, v2, v10

    const/16 v4, 0x1f

    if-eq v3, v4, :cond_10

    :cond_b
    aget v3, v2, v9

    if-nez v3, :cond_c

    const/4 v3, 0x5

    aget v4, v2, v10

    add-int/lit8 v4, v4, 0x1

    aget v4, p0, v4

    aput v4, v2, v3

    :goto_5
    aget v3, v2, v10

    const/16 v4, 0x1f

    if-ne v3, v4, :cond_d

    const/4 v3, 0x6

    const/4 v4, 0x5

    aget v4, v2, v4

    aput v4, v2, v3

    :goto_6
    const/4 v3, 0x7

    const/4 v4, 0x5

    aget v4, v2, v4

    const/4 v5, 0x6

    aget v5, v2, v5

    add-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x2

    aput v4, v2, v3

    const/4 v3, 0x4

    mul-int/lit8 v4, v0, 0x2

    const/4 v5, 0x7

    aget v5, v2, v5

    add-int/2addr v4, v5

    const/4 v5, 0x7

    aget v5, v2, v5

    mul-int/lit8 v5, v5, 0x2

    div-int/2addr v4, v5

    aput v4, v2, v3

    const/4 v3, 0x4

    aget v3, v2, v3

    if-le v3, v8, :cond_0

    const/16 v3, 0x8

    const/4 v4, 0x6

    aget v4, v2, v4

    const/4 v5, 0x4

    aget v5, v2, v5

    add-int/lit8 v5, v5, 0x1

    div-int/2addr v4, v5

    const/4 v5, 0x5

    aget v5, v2, v5

    const/4 v6, 0x4

    aget v6, v2, v6

    add-int/lit8 v6, v6, 0x1

    div-int/2addr v5, v6

    sub-int/2addr v4, v5

    aput v4, v2, v3

    const/4 v3, 0x4

    aget v3, v2, v3

    aput v3, v2, v7

    :goto_7
    aget v3, v2, v7

    if-lez v3, :cond_2

    aget v3, v2, v8

    if-ltz v3, :cond_2

    aget v3, v2, v8

    const/4 v4, 0x5

    aget v4, v2, v4

    const/16 v5, 0x8

    aget v5, v2, v5

    aget v6, v2, v7

    mul-int/2addr v5, v6

    add-int/2addr v4, v5

    aput v4, p1, v3

    aget v3, v2, v7

    add-int/lit8 v3, v3, -0x1

    aput v3, v2, v7

    aget v3, v2, v8

    add-int/lit8 v3, v3, -0x1

    aput v3, v2, v8

    goto :goto_7

    :cond_c
    const/4 v3, 0x5

    aget v4, v2, v9

    add-int/lit8 v4, v4, -0x1

    aget v4, p0, v4

    aput v4, v2, v3

    goto :goto_5

    :cond_d
    const/4 v3, 0x6

    aget v4, v2, v10

    add-int/lit8 v4, v4, 0x1

    aget v4, p0, v4

    aput v4, v2, v3

    goto :goto_6

    :cond_e
    aget v3, v2, v8

    add-int/lit8 v4, v3, -0x1

    aput v4, v2, v8

    const/4 v4, 0x5

    aget v4, v2, v4

    add-int/2addr v4, v0

    aput v4, p1, v3

    aget v3, v2, v9

    add-int/lit8 v3, v3, -0x1

    aput v3, v2, v9

    goto/16 :goto_1

    :cond_f
    aget v3, v2, v8

    add-int/lit8 v4, v3, -0x1

    aput v4, v2, v8

    aput v0, p1, v3

    goto/16 :goto_1

    :cond_10
    aget v0, v2, v8

    const/16 v3, 0x1f

    if-ne v0, v3, :cond_11

    aget v0, v2, v8

    add-int/lit8 v3, v0, -0x1

    aput v3, v2, v8

    aput v1, p1, v0

    :cond_11
    :goto_8
    aget v0, v2, v8

    if-ltz v0, :cond_5

    aget v0, v2, v8

    aget v1, v2, v8

    add-int/lit8 v1, v1, 0x1

    aget v1, p1, v1

    aput v1, p1, v0

    aget v0, v2, v8

    add-int/lit8 v0, v0, -0x1

    aput v0, v2, v8

    goto :goto_8

    :cond_12
    move v0, v1

    goto/16 :goto_2
.end method

.method private static b(I[I)I
    .locals 3

    const/4 v0, 0x1

    div-int/lit8 v1, p0, 0x8

    aget v1, p1, v1

    rem-int/lit8 v2, p0, 0x8

    shl-int v2, v0, v2

    and-int/2addr v1, v2

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
