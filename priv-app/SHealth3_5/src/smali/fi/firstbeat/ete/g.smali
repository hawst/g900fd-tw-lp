.class public final Lfi/firstbeat/ete/g;
.super Ljava/lang/Object;


# static fields
.field private static final a:[[I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v3, 0x9

    new-array v0, v3, [[I

    const/4 v1, 0x0

    new-array v2, v3, [I

    fill-array-data v2, :array_0

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-array v2, v3, [I

    fill-array-data v2, :array_1

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-array v2, v3, [I

    fill-array-data v2, :array_2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-array v2, v3, [I

    fill-array-data v2, :array_3

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-array v2, v3, [I

    fill-array-data v2, :array_4

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-array v2, v3, [I

    fill-array-data v2, :array_5

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v3, [I

    fill-array-data v2, :array_6

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-array v2, v3, [I

    fill-array-data v2, :array_7

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-array v2, v3, [I

    fill-array-data v2, :array_8

    aput-object v2, v0, v1

    sput-object v0, Lfi/firstbeat/ete/g;->a:[[I

    return-void

    :array_0
    .array-data 4
        0x4
        0x18
        0x9
        0x9
        0x6
        0x5
        0x3
        0x4
        -0x1
    .end array-data

    :array_1
    .array-data 4
        0x5
        0x19
        0xa
        0xa
        0x7
        0x4
        0x3
        0x5
        0x0
    .end array-data

    :array_2
    .array-data 4
        0xf
        0x2d
        0x19
        0x12
        0x13
        0x8
        0xb
        0xe
        0x5
    .end array-data

    :array_3
    .array-data 4
        0x1e
        0x3c
        0x2d
        0x18
        0x24
        0xc
        0x18
        0x15
        0xf
    .end array-data

    :array_4
    .array-data 4
        0x2d
        0x46
        0x41
        0x1c
        0x35
        0x12
        0x25
        0x20
        0x19
    .end array-data

    :array_5
    .array-data 4
        0x3c
        0x4b
        0x55
        0x1e
        0x46
        0x17
        0x32
        0x2b
        0x23
    .end array-data

    :array_6
    .array-data 4
        0x5a
        0x5a
        0x79
        0x26
        0x67
        0x1f
        0x4e
        0x38
        0x3b
    .end array-data

    :array_7
    .array-data 4
        0x78
        0x71
        0x9e
        0x3c
        0x87
        0x3c
        0x69
        0x56
        0x53
    .end array-data

    :array_8
    .array-data 4
        0xb5
        0x9d
        0xe6
        0x4b
        0xc8
        0x5a
        0xa0
        0x82
        0x82
    .end array-data
.end method

.method public static a(II)I
    .locals 7

    const/16 v5, 0x445a

    const/16 v0, 0x28

    const/16 v1, 0x1e

    const/16 v2, 0x14

    const/16 v3, 0xa

    if-lt p0, v0, :cond_0

    const/16 v4, 0x32

    if-gt p0, v4, :cond_0

    const v1, 0x1d3b6

    shl-int/lit8 v2, p1, 0x10

    invoke-static {v1, v2}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v1

    const v2, 0x92147

    add-int/2addr v1, v2

    const v2, 0x2e148

    shl-int/lit8 v3, p1, 0x10

    invoke-static {v2, v3}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v2

    const v3, 0xe6666

    add-int/2addr v2, v3

    :goto_0
    sub-int v0, p0, v0

    shl-int/lit8 v0, v0, 0x10

    div-int/lit8 v0, v0, 0xa

    sub-int/2addr v2, v1

    invoke-static {v0, v2}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    return v0

    :cond_0
    if-lt p0, v1, :cond_1

    const v0, 0xcccd

    shl-int/lit8 v2, p1, 0x10

    invoke-static {v0, v2}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v0

    const/high16 v2, 0x40000

    add-int/2addr v0, v2

    const v2, 0x1d3b6

    shl-int/lit8 v3, p1, 0x10

    invoke-static {v2, v3}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v2

    const v3, 0x92147

    add-int/2addr v2, v3

    move v6, v1

    move v1, v0

    move v0, v6

    goto :goto_0

    :cond_1
    if-lt p0, v2, :cond_2

    shl-int/lit8 v0, p1, 0x10

    invoke-static {v5, v0}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v0

    const v1, 0x1547a

    add-int/2addr v0, v1

    const v1, 0xcccd

    shl-int/lit8 v3, p1, 0x10

    invoke-static {v1, v3}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v1

    const/high16 v3, 0x40000

    add-int/2addr v1, v3

    move v6, v2

    move v2, v1

    move v1, v0

    move v0, v6

    goto :goto_0

    :cond_2
    if-lt p0, v3, :cond_3

    const/4 v0, 0x0

    shl-int/lit8 v1, p1, 0x10

    invoke-static {v5, v1}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v1

    const v2, 0x1547a

    add-int/2addr v1, v2

    move v2, v1

    move v1, v0

    move v0, v3

    goto :goto_0

    :cond_3
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private static a(IIII)I
    .locals 4

    const/16 v1, 0xb4

    const/16 v0, 0x1e

    if-le p0, v0, :cond_4

    sub-int v0, p1, p3

    mul-int/lit8 v0, v0, 0x64

    const v2, 0x14f5c

    mul-int/2addr v2, p0

    invoke-static {v0, v2}, Lfi/firstbeat/ete/e;->b(II)I

    move-result v0

    invoke-static {v0}, Lfi/firstbeat/ete/e;->a(I)I

    move-result v0

    :goto_0
    const/4 v2, 0x5

    invoke-static {p0, p1, v2}, Lfi/firstbeat/ete/c;->a(III)I

    move-result v2

    if-le p1, v2, :cond_1

    if-lt v0, v1, :cond_0

    const/4 v0, -0x5

    :cond_0
    :goto_1
    return v0

    :cond_1
    const/4 v2, 0x1

    :goto_2
    if-ge v2, v1, :cond_2

    const/16 v3, 0x3c

    invoke-static {p0, p2, v3}, Lfi/firstbeat/ete/c;->a(III)I

    move-result p2

    if-gt p2, p1, :cond_2

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_2
    if-ge v2, v1, :cond_3

    if-ge v2, v0, :cond_3

    move v0, v2

    goto :goto_1

    :cond_3
    if-lt v0, v1, :cond_0

    const/4 v0, -0x6

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method private static a(IIIII)I
    .locals 3

    rsub-int/lit8 v0, p2, 0x0

    if-nez v0, :cond_0

    :goto_0
    return p1

    :cond_0
    mul-int v0, p1, p4

    rsub-int/lit8 v1, p2, 0x0

    div-int/2addr v0, v1

    const/4 v1, 0x0

    rsub-int/lit8 v2, p2, 0x0

    div-int/2addr v1, v2

    add-int/2addr v0, v1

    add-int/2addr p1, v0

    goto :goto_0
.end method

.method public static a(ILfi/firstbeat/ete/b;)I
    .locals 13

    invoke-static {p1}, Lfi/firstbeat/ete/g;->a(Lfi/firstbeat/ete/b;)I

    move-result v0

    iget v1, p1, Lfi/firstbeat/ete/b;->Q:I

    if-lt v0, v1, :cond_0

    iget v0, p1, Lfi/firstbeat/ete/b;->Q:I

    const/16 v1, 0xa

    if-le v0, v1, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget v0, p1, Lfi/firstbeat/ete/b;->R:I

    if-nez v0, :cond_4

    iget v0, p1, Lfi/firstbeat/ete/b;->Q:I

    const/16 v1, 0xa

    if-ne v0, v1, :cond_1

    iget-object v0, p1, Lfi/firstbeat/ete/b;->S:[I

    const/4 v1, 0x1

    aget v0, v0, v1

    if-lez v0, :cond_1

    iget-object v0, p1, Lfi/firstbeat/ete/b;->S:[I

    const/4 v1, 0x1

    aget v0, v0, v1

    const/16 v1, 0x5a

    if-le v0, v1, :cond_2

    :cond_1
    iget v0, p1, Lfi/firstbeat/ete/b;->Q:I

    const/16 v1, 0xa

    if-le v0, v1, :cond_3

    iget-object v0, p1, Lfi/firstbeat/ete/b;->T:[I

    const/4 v1, 0x1

    aget v0, v0, v1

    if-lez v0, :cond_3

    iget-object v0, p1, Lfi/firstbeat/ete/b;->T:[I

    const/4 v1, 0x1

    aget v0, v0, v1

    const/16 v1, 0x5a

    if-gt v0, v1, :cond_3

    iget-object v0, p1, Lfi/firstbeat/ete/b;->T:[I

    const/4 v1, 0x1

    aget v0, v0, v1

    iget v1, p1, Lfi/firstbeat/ete/b;->C:I

    const v2, 0xea60

    div-int/2addr v1, v2

    add-int/2addr v0, v1

    const/16 v1, 0xb4

    if-gt v0, v1, :cond_3

    :cond_2
    const/4 v0, 0x6

    goto :goto_0

    :cond_3
    const/4 v0, 0x5

    goto :goto_0

    :cond_4
    iget v0, p1, Lfi/firstbeat/ete/b;->C:I

    div-int/lit16 v0, v0, 0x3e8

    div-int/lit8 v2, v0, 0x3c

    iget v3, p1, Lfi/firstbeat/ete/b;->R:I

    const/4 v1, 0x0

    const/4 v0, 0x1

    :goto_1
    const/16 v4, 0x9

    if-ge v0, v4, :cond_25

    sget-object v4, Lfi/firstbeat/ete/g;->a:[[I

    aget-object v4, v4, v0

    const/4 v5, 0x0

    aget v4, v4, v5

    if-ge v3, v4, :cond_6

    :goto_2
    if-nez v0, :cond_7

    const/4 v0, 0x0

    :goto_3
    mul-int/lit8 v0, v0, 0x3c

    iget v1, p1, Lfi/firstbeat/ete/b;->C:I

    div-int/lit16 v1, v1, 0x3e8

    sub-int/2addr v0, v1

    iget v1, p1, Lfi/firstbeat/ete/b;->C:I

    div-int/lit16 v1, v1, 0x3e8

    const/16 v2, 0x2a30

    if-ge v1, v2, :cond_5

    if-gtz v0, :cond_8

    :cond_5
    const/4 v0, -0x2

    goto :goto_0

    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_7
    const/4 v1, 0x0

    sget-object v4, Lfi/firstbeat/ete/g;->a:[[I

    aget-object v4, v4, v0

    const/4 v5, 0x0

    aget v4, v4, v5

    sget-object v5, Lfi/firstbeat/ete/g;->a:[[I

    aget-object v5, v5, v0

    const/4 v6, 0x3

    aget v5, v5, v6

    add-int/2addr v4, v5

    sget-object v5, Lfi/firstbeat/ete/g;->a:[[I

    aget-object v5, v5, v0

    const/4 v6, 0x4

    aget v5, v5, v6

    const/4 v6, 0x0

    invoke-static {v1, v4, v5, v6, v2}, Lfi/firstbeat/ete/g;->a(IIIII)I

    move-result v1

    const/4 v4, 0x0

    sget-object v5, Lfi/firstbeat/ete/g;->a:[[I

    add-int/lit8 v6, v0, -0x1

    aget-object v5, v5, v6

    const/4 v6, 0x0

    aget v5, v5, v6

    sget-object v6, Lfi/firstbeat/ete/g;->a:[[I

    add-int/lit8 v7, v0, -0x1

    aget-object v6, v6, v7

    const/4 v7, 0x3

    aget v6, v6, v7

    add-int/2addr v5, v6

    sget-object v6, Lfi/firstbeat/ete/g;->a:[[I

    add-int/lit8 v7, v0, -0x1

    aget-object v6, v6, v7

    const/4 v7, 0x4

    aget v6, v6, v7

    const/4 v7, 0x0

    invoke-static {v4, v5, v6, v7, v2}, Lfi/firstbeat/ete/g;->a(IIIII)I

    move-result v4

    sget-object v5, Lfi/firstbeat/ete/g;->a:[[I

    add-int/lit8 v6, v0, -0x1

    aget-object v5, v5, v6

    const/4 v6, 0x0

    aget v5, v5, v6

    sub-int v5, v3, v5

    sub-int/2addr v1, v4

    mul-int/2addr v1, v5

    sget-object v5, Lfi/firstbeat/ete/g;->a:[[I

    aget-object v5, v5, v0

    const/4 v6, 0x0

    aget v5, v5, v6

    sget-object v6, Lfi/firstbeat/ete/g;->a:[[I

    add-int/lit8 v7, v0, -0x1

    aget-object v6, v6, v7

    const/4 v7, 0x0

    aget v6, v6, v7

    sub-int/2addr v5, v6

    div-int/2addr v1, v5

    add-int/2addr v1, v4

    const/4 v4, 0x0

    sget-object v5, Lfi/firstbeat/ete/g;->a:[[I

    aget-object v5, v5, v0

    const/4 v6, 0x0

    aget v5, v5, v6

    sget-object v6, Lfi/firstbeat/ete/g;->a:[[I

    aget-object v6, v6, v0

    const/4 v7, 0x0

    aget v6, v6, v7

    const/4 v7, 0x0

    invoke-static {v4, v5, v6, v7, v2}, Lfi/firstbeat/ete/g;->a(IIIII)I

    move-result v4

    const/4 v5, 0x0

    sget-object v6, Lfi/firstbeat/ete/g;->a:[[I

    add-int/lit8 v7, v0, -0x1

    aget-object v6, v6, v7

    const/4 v7, 0x0

    aget v6, v6, v7

    sget-object v7, Lfi/firstbeat/ete/g;->a:[[I

    add-int/lit8 v8, v0, -0x1

    aget-object v7, v7, v8

    const/4 v8, 0x0

    aget v7, v7, v8

    const/4 v8, 0x0

    invoke-static {v5, v6, v7, v8, v2}, Lfi/firstbeat/ete/g;->a(IIIII)I

    move-result v5

    sget-object v6, Lfi/firstbeat/ete/g;->a:[[I

    add-int/lit8 v7, v0, -0x1

    aget-object v6, v6, v7

    const/4 v7, 0x0

    aget v6, v6, v7

    sub-int/2addr v3, v6

    sub-int/2addr v4, v5

    mul-int/2addr v3, v4

    sget-object v4, Lfi/firstbeat/ete/g;->a:[[I

    aget-object v4, v4, v0

    const/4 v6, 0x0

    aget v4, v4, v6

    sget-object v6, Lfi/firstbeat/ete/g;->a:[[I

    add-int/lit8 v0, v0, -0x1

    aget-object v0, v6, v0

    const/4 v6, 0x0

    aget v0, v0, v6

    sub-int v0, v4, v0

    div-int v0, v3, v0

    add-int/2addr v0, v5

    add-int/2addr v2, v0

    sub-int v0, v1, v0

    mul-int/lit8 v0, v0, 0x3

    div-int/lit8 v0, v0, 0x4

    add-int/2addr v0, v2

    goto/16 :goto_3

    :cond_8
    iget-object v0, p1, Lfi/firstbeat/ete/b;->T:[I

    const/4 v1, 0x1

    aget v0, v0, v1

    if-gez v0, :cond_a

    const/4 v1, 0x0

    :goto_4
    const/4 v0, 0x6

    if-ge v1, v0, :cond_9

    iget v0, p1, Lfi/firstbeat/ete/b;->z:I

    const/16 v2, 0xa

    if-lt v0, v2, :cond_9

    iget v0, p1, Lfi/firstbeat/ete/b;->t:I

    iget v2, p1, Lfi/firstbeat/ete/b;->u:I

    sub-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0xc

    const/high16 v2, -0x10000

    if-ge v0, v2, :cond_1c

    :cond_9
    move v0, v1

    goto/16 :goto_0

    :cond_a
    iget-object v0, p1, Lfi/firstbeat/ete/b;->T:[I

    const/4 v1, 0x1

    aget v2, v0, v1

    iget v3, p1, Lfi/firstbeat/ete/b;->R:I

    const/4 v1, 0x0

    const/4 v0, 0x1

    :goto_5
    const/16 v4, 0x9

    if-ge v0, v4, :cond_24

    sget-object v4, Lfi/firstbeat/ete/g;->a:[[I

    aget-object v4, v4, v0

    const/4 v5, 0x0

    aget v4, v4, v5

    if-ge v3, v4, :cond_b

    :goto_6
    if-nez v0, :cond_c

    const/4 v1, 0x0

    goto :goto_4

    :cond_b
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_c
    const/4 v1, 0x0

    sget-object v4, Lfi/firstbeat/ete/g;->a:[[I

    aget-object v4, v4, v0

    const/4 v5, 0x0

    aget v4, v4, v5

    sget-object v5, Lfi/firstbeat/ete/g;->a:[[I

    aget-object v5, v5, v0

    const/4 v6, 0x1

    aget v5, v5, v6

    add-int/2addr v4, v5

    sget-object v5, Lfi/firstbeat/ete/g;->a:[[I

    aget-object v5, v5, v0

    const/4 v6, 0x2

    aget v5, v5, v6

    const/4 v6, 0x0

    invoke-static {v1, v4, v5, v6, p0}, Lfi/firstbeat/ete/g;->a(IIIII)I

    move-result v1

    const/4 v4, 0x0

    sget-object v5, Lfi/firstbeat/ete/g;->a:[[I

    add-int/lit8 v6, v0, -0x1

    aget-object v5, v5, v6

    const/4 v6, 0x0

    aget v5, v5, v6

    sget-object v6, Lfi/firstbeat/ete/g;->a:[[I

    add-int/lit8 v7, v0, -0x1

    aget-object v6, v6, v7

    const/4 v7, 0x1

    aget v6, v6, v7

    add-int/2addr v5, v6

    sget-object v6, Lfi/firstbeat/ete/g;->a:[[I

    add-int/lit8 v7, v0, -0x1

    aget-object v6, v6, v7

    const/4 v7, 0x2

    aget v6, v6, v7

    const/4 v7, 0x0

    invoke-static {v4, v5, v6, v7, p0}, Lfi/firstbeat/ete/g;->a(IIIII)I

    move-result v4

    sget-object v5, Lfi/firstbeat/ete/g;->a:[[I

    add-int/lit8 v6, v0, -0x1

    aget-object v5, v5, v6

    const/4 v6, 0x0

    aget v5, v5, v6

    sub-int v5, v3, v5

    sub-int/2addr v1, v4

    mul-int/2addr v1, v5

    sget-object v5, Lfi/firstbeat/ete/g;->a:[[I

    aget-object v5, v5, v0

    const/4 v6, 0x0

    aget v5, v5, v6

    sget-object v6, Lfi/firstbeat/ete/g;->a:[[I

    add-int/lit8 v7, v0, -0x1

    aget-object v6, v6, v7

    const/4 v7, 0x0

    aget v6, v6, v7

    sub-int/2addr v5, v6

    div-int/2addr v1, v5

    add-int/2addr v1, v4

    const/4 v4, 0x0

    sget-object v5, Lfi/firstbeat/ete/g;->a:[[I

    aget-object v5, v5, v0

    const/4 v6, 0x0

    aget v5, v5, v6

    sget-object v6, Lfi/firstbeat/ete/g;->a:[[I

    aget-object v6, v6, v0

    const/4 v7, 0x3

    aget v6, v6, v7

    add-int/2addr v5, v6

    sget-object v6, Lfi/firstbeat/ete/g;->a:[[I

    aget-object v6, v6, v0

    const/4 v7, 0x4

    aget v6, v6, v7

    const/4 v7, 0x0

    invoke-static {v4, v5, v6, v7, p0}, Lfi/firstbeat/ete/g;->a(IIIII)I

    move-result v4

    const/4 v5, 0x0

    sget-object v6, Lfi/firstbeat/ete/g;->a:[[I

    add-int/lit8 v7, v0, -0x1

    aget-object v6, v6, v7

    const/4 v7, 0x0

    aget v6, v6, v7

    sget-object v7, Lfi/firstbeat/ete/g;->a:[[I

    add-int/lit8 v8, v0, -0x1

    aget-object v7, v7, v8

    const/4 v8, 0x3

    aget v7, v7, v8

    add-int/2addr v6, v7

    sget-object v7, Lfi/firstbeat/ete/g;->a:[[I

    add-int/lit8 v8, v0, -0x1

    aget-object v7, v7, v8

    const/4 v8, 0x4

    aget v7, v7, v8

    const/4 v8, 0x0

    invoke-static {v5, v6, v7, v8, p0}, Lfi/firstbeat/ete/g;->a(IIIII)I

    move-result v5

    sget-object v6, Lfi/firstbeat/ete/g;->a:[[I

    add-int/lit8 v7, v0, -0x1

    aget-object v6, v6, v7

    const/4 v7, 0x0

    aget v6, v6, v7

    sub-int v6, v3, v6

    sub-int/2addr v4, v5

    mul-int/2addr v4, v6

    sget-object v6, Lfi/firstbeat/ete/g;->a:[[I

    aget-object v6, v6, v0

    const/4 v7, 0x0

    aget v6, v6, v7

    sget-object v7, Lfi/firstbeat/ete/g;->a:[[I

    add-int/lit8 v8, v0, -0x1

    aget-object v7, v7, v8

    const/4 v8, 0x0

    aget v7, v7, v8

    sub-int/2addr v6, v7

    div-int/2addr v4, v6

    add-int/2addr v4, v5

    const/4 v5, 0x0

    sget-object v6, Lfi/firstbeat/ete/g;->a:[[I

    aget-object v6, v6, v0

    const/4 v7, 0x0

    aget v6, v6, v7

    sget-object v7, Lfi/firstbeat/ete/g;->a:[[I

    aget-object v7, v7, v0

    const/4 v8, 0x0

    aget v7, v7, v8

    const/4 v8, 0x0

    invoke-static {v5, v6, v7, v8, p0}, Lfi/firstbeat/ete/g;->a(IIIII)I

    move-result v5

    const/4 v6, 0x0

    sget-object v7, Lfi/firstbeat/ete/g;->a:[[I

    add-int/lit8 v8, v0, -0x1

    aget-object v7, v7, v8

    const/4 v8, 0x0

    aget v7, v7, v8

    sget-object v8, Lfi/firstbeat/ete/g;->a:[[I

    add-int/lit8 v9, v0, -0x1

    aget-object v8, v8, v9

    const/4 v9, 0x0

    aget v8, v8, v9

    const/4 v9, 0x0

    invoke-static {v6, v7, v8, v9, p0}, Lfi/firstbeat/ete/g;->a(IIIII)I

    move-result v6

    sget-object v7, Lfi/firstbeat/ete/g;->a:[[I

    add-int/lit8 v8, v0, -0x1

    aget-object v7, v7, v8

    const/4 v8, 0x0

    aget v7, v7, v8

    sub-int v7, v3, v7

    sub-int/2addr v5, v6

    mul-int/2addr v5, v7

    sget-object v7, Lfi/firstbeat/ete/g;->a:[[I

    aget-object v7, v7, v0

    const/4 v8, 0x0

    aget v7, v7, v8

    sget-object v8, Lfi/firstbeat/ete/g;->a:[[I

    add-int/lit8 v9, v0, -0x1

    aget-object v8, v8, v9

    const/4 v9, 0x0

    aget v8, v8, v9

    sub-int/2addr v7, v8

    div-int/2addr v5, v7

    add-int/2addr v5, v6

    const/4 v6, 0x0

    sget-object v7, Lfi/firstbeat/ete/g;->a:[[I

    aget-object v7, v7, v0

    const/4 v8, 0x0

    aget v7, v7, v8

    sget-object v8, Lfi/firstbeat/ete/g;->a:[[I

    aget-object v8, v8, v0

    const/4 v9, 0x5

    aget v8, v8, v9

    sub-int/2addr v7, v8

    sget-object v8, Lfi/firstbeat/ete/g;->a:[[I

    aget-object v8, v8, v0

    const/4 v9, 0x6

    aget v8, v8, v9

    const/4 v9, 0x0

    invoke-static {v6, v7, v8, v9, p0}, Lfi/firstbeat/ete/g;->a(IIIII)I

    move-result v6

    const/4 v7, 0x0

    sget-object v8, Lfi/firstbeat/ete/g;->a:[[I

    add-int/lit8 v9, v0, -0x1

    aget-object v8, v8, v9

    const/4 v9, 0x0

    aget v8, v8, v9

    sget-object v9, Lfi/firstbeat/ete/g;->a:[[I

    add-int/lit8 v10, v0, -0x1

    aget-object v9, v9, v10

    const/4 v10, 0x5

    aget v9, v9, v10

    sub-int/2addr v8, v9

    sget-object v9, Lfi/firstbeat/ete/g;->a:[[I

    add-int/lit8 v10, v0, -0x1

    aget-object v9, v9, v10

    const/4 v10, 0x6

    aget v9, v9, v10

    const/4 v10, 0x0

    invoke-static {v7, v8, v9, v10, p0}, Lfi/firstbeat/ete/g;->a(IIIII)I

    move-result v7

    sget-object v8, Lfi/firstbeat/ete/g;->a:[[I

    add-int/lit8 v9, v0, -0x1

    aget-object v8, v8, v9

    const/4 v9, 0x0

    aget v8, v8, v9

    sub-int v8, v3, v8

    sub-int/2addr v6, v7

    mul-int/2addr v6, v8

    sget-object v8, Lfi/firstbeat/ete/g;->a:[[I

    aget-object v8, v8, v0

    const/4 v9, 0x0

    aget v8, v8, v9

    sget-object v9, Lfi/firstbeat/ete/g;->a:[[I

    add-int/lit8 v10, v0, -0x1

    aget-object v9, v9, v10

    const/4 v10, 0x0

    aget v9, v9, v10

    sub-int/2addr v8, v9

    div-int/2addr v6, v8

    add-int/2addr v6, v7

    const/4 v7, 0x0

    sget-object v8, Lfi/firstbeat/ete/g;->a:[[I

    aget-object v8, v8, v0

    const/4 v9, 0x0

    aget v8, v8, v9

    sget-object v9, Lfi/firstbeat/ete/g;->a:[[I

    aget-object v9, v9, v0

    const/4 v10, 0x7

    aget v9, v9, v10

    sub-int/2addr v8, v9

    sget-object v9, Lfi/firstbeat/ete/g;->a:[[I

    aget-object v9, v9, v0

    const/16 v10, 0x8

    aget v9, v9, v10

    const/4 v10, 0x0

    invoke-static {v7, v8, v9, v10, p0}, Lfi/firstbeat/ete/g;->a(IIIII)I

    move-result v7

    const/4 v8, 0x0

    sget-object v9, Lfi/firstbeat/ete/g;->a:[[I

    add-int/lit8 v10, v0, -0x1

    aget-object v9, v9, v10

    const/4 v10, 0x0

    aget v9, v9, v10

    sget-object v10, Lfi/firstbeat/ete/g;->a:[[I

    add-int/lit8 v11, v0, -0x1

    aget-object v10, v10, v11

    const/4 v11, 0x7

    aget v10, v10, v11

    sub-int/2addr v9, v10

    sget-object v10, Lfi/firstbeat/ete/g;->a:[[I

    add-int/lit8 v11, v0, -0x1

    aget-object v10, v10, v11

    const/16 v11, 0x8

    aget v10, v10, v11

    const/4 v11, 0x0

    invoke-static {v8, v9, v10, v11, p0}, Lfi/firstbeat/ete/g;->a(IIIII)I

    move-result v8

    sget-object v9, Lfi/firstbeat/ete/g;->a:[[I

    add-int/lit8 v10, v0, -0x1

    aget-object v9, v9, v10

    const/4 v10, 0x0

    aget v9, v9, v10

    sub-int/2addr v3, v9

    sub-int/2addr v7, v8

    mul-int/2addr v3, v7

    sget-object v7, Lfi/firstbeat/ete/g;->a:[[I

    aget-object v7, v7, v0

    const/4 v9, 0x0

    aget v7, v7, v9

    sget-object v9, Lfi/firstbeat/ete/g;->a:[[I

    add-int/lit8 v0, v0, -0x1

    aget-object v0, v9, v0

    const/4 v9, 0x0

    aget v0, v0, v9

    sub-int v0, v7, v0

    div-int v0, v3, v0

    add-int/2addr v0, v8

    sub-int/2addr v1, v4

    mul-int/lit8 v3, v1, 0xb

    div-int/lit8 v3, v3, 0xc

    add-int/2addr v3, v4

    invoke-static {v2, v3}, Lfi/firstbeat/ete/g;->b(II)I

    move-result v3

    if-eqz v3, :cond_d

    const/4 v1, 0x0

    goto/16 :goto_4

    :cond_d
    mul-int/lit8 v3, v1, 0x9

    div-int/lit8 v3, v3, 0xc

    add-int/2addr v3, v4

    invoke-static {v2, v3}, Lfi/firstbeat/ete/g;->b(II)I

    move-result v3

    if-eqz v3, :cond_e

    const/4 v1, 0x1

    goto/16 :goto_4

    :cond_e
    mul-int/lit8 v3, v1, 0x7

    div-int/lit8 v3, v3, 0xc

    add-int/2addr v3, v4

    invoke-static {v2, v3}, Lfi/firstbeat/ete/g;->b(II)I

    move-result v3

    if-eqz v3, :cond_f

    const/4 v1, 0x2

    goto/16 :goto_4

    :cond_f
    mul-int/lit8 v3, v1, 0x5

    div-int/lit8 v3, v3, 0xc

    add-int/2addr v3, v4

    invoke-static {v2, v3}, Lfi/firstbeat/ete/g;->b(II)I

    move-result v3

    if-eqz v3, :cond_10

    const/4 v1, 0x3

    goto/16 :goto_4

    :cond_10
    mul-int/lit8 v3, v1, 0x3

    div-int/lit8 v3, v3, 0xc

    add-int/2addr v3, v4

    invoke-static {v2, v3}, Lfi/firstbeat/ete/g;->b(II)I

    move-result v3

    if-eqz v3, :cond_11

    const/4 v1, 0x4

    goto/16 :goto_4

    :cond_11
    div-int/lit8 v1, v1, 0xc

    add-int/2addr v1, v4

    invoke-static {v2, v1}, Lfi/firstbeat/ete/g;->b(II)I

    move-result v1

    if-eqz v1, :cond_12

    const/4 v1, 0x5

    goto/16 :goto_4

    :cond_12
    sub-int v1, v4, v5

    mul-int/lit8 v3, v1, 0x3

    div-int/lit8 v3, v3, 0x4

    add-int/2addr v3, v5

    invoke-static {v2, v3}, Lfi/firstbeat/ete/g;->b(II)I

    move-result v3

    if-eqz v3, :cond_13

    const/4 v1, 0x6

    goto/16 :goto_4

    :cond_13
    div-int/lit8 v1, v1, 0x4

    add-int/2addr v1, v5

    invoke-static {v2, v1}, Lfi/firstbeat/ete/g;->b(II)I

    move-result v1

    if-eqz v1, :cond_14

    const/4 v1, 0x7

    goto/16 :goto_4

    :cond_14
    sub-int v1, v5, v6

    div-int/lit8 v3, v1, 0x4

    sub-int v3, v5, v3

    invoke-static {v2, v3}, Lfi/firstbeat/ete/g;->b(II)I

    move-result v3

    if-eqz v3, :cond_15

    const/16 v1, 0x8

    goto/16 :goto_4

    :cond_15
    mul-int/lit8 v1, v1, 0x3

    div-int/lit8 v1, v1, 0x4

    sub-int v1, v5, v1

    invoke-static {v2, v1}, Lfi/firstbeat/ete/g;->b(II)I

    move-result v1

    if-eqz v1, :cond_16

    const/16 v1, 0x9

    goto/16 :goto_4

    :cond_16
    sub-int v0, v6, v0

    div-int/lit8 v1, v0, 0xc

    sub-int v1, v6, v1

    invoke-static {v2, v1}, Lfi/firstbeat/ete/g;->b(II)I

    move-result v1

    if-eqz v1, :cond_17

    const/16 v1, 0xa

    goto/16 :goto_4

    :cond_17
    mul-int/lit8 v1, v0, 0x3

    div-int/lit8 v1, v1, 0xc

    sub-int v1, v6, v1

    invoke-static {v2, v1}, Lfi/firstbeat/ete/g;->b(II)I

    move-result v1

    if-eqz v1, :cond_18

    const/16 v1, 0xb

    goto/16 :goto_4

    :cond_18
    mul-int/lit8 v1, v0, 0x5

    div-int/lit8 v1, v1, 0xc

    sub-int v1, v6, v1

    invoke-static {v2, v1}, Lfi/firstbeat/ete/g;->b(II)I

    move-result v1

    if-eqz v1, :cond_19

    const/16 v1, 0xc

    goto/16 :goto_4

    :cond_19
    mul-int/lit8 v1, v0, 0x7

    div-int/lit8 v1, v1, 0xc

    sub-int v1, v6, v1

    invoke-static {v2, v1}, Lfi/firstbeat/ete/g;->b(II)I

    move-result v1

    if-eqz v1, :cond_1a

    const/16 v1, 0xd

    goto/16 :goto_4

    :cond_1a
    mul-int/lit8 v0, v0, 0x9

    div-int/lit8 v0, v0, 0xc

    sub-int v0, v6, v0

    invoke-static {v2, v0}, Lfi/firstbeat/ete/g;->b(II)I

    move-result v0

    if-eqz v0, :cond_1b

    const/16 v1, 0xe

    goto/16 :goto_4

    :cond_1b
    const/16 v1, 0xf

    goto/16 :goto_4

    :cond_1c
    iget v3, p1, Lfi/firstbeat/ete/b;->R:I

    const/4 v2, 0x0

    const/4 v0, 0x1

    :goto_7
    const/16 v4, 0x9

    if-ge v0, v4, :cond_23

    sget-object v4, Lfi/firstbeat/ete/g;->a:[[I

    aget-object v4, v4, v0

    const/4 v5, 0x0

    aget v4, v4, v5

    if-ge v3, v4, :cond_1d

    :goto_8
    if-nez v0, :cond_1e

    const/4 v0, 0x0

    :goto_9
    sub-int v3, v0, p0

    if-gez v3, :cond_1f

    move v0, v1

    goto/16 :goto_0

    :cond_1d
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_1e
    const/4 v2, 0x0

    sget-object v4, Lfi/firstbeat/ete/g;->a:[[I

    aget-object v4, v4, v0

    const/4 v5, 0x0

    aget v4, v4, v5

    sget-object v5, Lfi/firstbeat/ete/g;->a:[[I

    aget-object v5, v5, v0

    const/4 v6, 0x5

    aget v5, v5, v6

    sub-int/2addr v4, v5

    sget-object v5, Lfi/firstbeat/ete/g;->a:[[I

    aget-object v5, v5, v0

    const/4 v6, 0x6

    aget v5, v5, v6

    const/4 v6, 0x0

    invoke-static {v2, v4, v5, v6, p0}, Lfi/firstbeat/ete/g;->a(IIIII)I

    move-result v2

    const/4 v4, 0x0

    sget-object v5, Lfi/firstbeat/ete/g;->a:[[I

    add-int/lit8 v6, v0, -0x1

    aget-object v5, v5, v6

    const/4 v6, 0x0

    aget v5, v5, v6

    sget-object v6, Lfi/firstbeat/ete/g;->a:[[I

    add-int/lit8 v7, v0, -0x1

    aget-object v6, v6, v7

    const/4 v7, 0x5

    aget v6, v6, v7

    sub-int/2addr v5, v6

    sget-object v6, Lfi/firstbeat/ete/g;->a:[[I

    add-int/lit8 v7, v0, -0x1

    aget-object v6, v6, v7

    const/4 v7, 0x6

    aget v6, v6, v7

    const/4 v7, 0x0

    invoke-static {v4, v5, v6, v7, p0}, Lfi/firstbeat/ete/g;->a(IIIII)I

    move-result v4

    sget-object v5, Lfi/firstbeat/ete/g;->a:[[I

    add-int/lit8 v6, v0, -0x1

    aget-object v5, v5, v6

    const/4 v6, 0x0

    aget v5, v5, v6

    sub-int v5, v3, v5

    sub-int/2addr v2, v4

    mul-int/2addr v2, v5

    sget-object v5, Lfi/firstbeat/ete/g;->a:[[I

    aget-object v5, v5, v0

    const/4 v6, 0x0

    aget v5, v5, v6

    sget-object v6, Lfi/firstbeat/ete/g;->a:[[I

    add-int/lit8 v7, v0, -0x1

    aget-object v6, v6, v7

    const/4 v7, 0x0

    aget v6, v6, v7

    sub-int/2addr v5, v6

    div-int/2addr v2, v5

    add-int/2addr v2, v4

    const/4 v4, 0x0

    sget-object v5, Lfi/firstbeat/ete/g;->a:[[I

    aget-object v5, v5, v0

    const/4 v6, 0x0

    aget v5, v5, v6

    sget-object v6, Lfi/firstbeat/ete/g;->a:[[I

    aget-object v6, v6, v0

    const/4 v7, 0x0

    aget v6, v6, v7

    const/4 v7, 0x0

    invoke-static {v4, v5, v6, v7, p0}, Lfi/firstbeat/ete/g;->a(IIIII)I

    move-result v4

    const/4 v5, 0x0

    sget-object v6, Lfi/firstbeat/ete/g;->a:[[I

    add-int/lit8 v7, v0, -0x1

    aget-object v6, v6, v7

    const/4 v7, 0x0

    aget v6, v6, v7

    sget-object v7, Lfi/firstbeat/ete/g;->a:[[I

    add-int/lit8 v8, v0, -0x1

    aget-object v7, v7, v8

    const/4 v8, 0x0

    aget v7, v7, v8

    const/4 v8, 0x0

    invoke-static {v5, v6, v7, v8, p0}, Lfi/firstbeat/ete/g;->a(IIIII)I

    move-result v5

    sget-object v6, Lfi/firstbeat/ete/g;->a:[[I

    add-int/lit8 v7, v0, -0x1

    aget-object v6, v6, v7

    const/4 v7, 0x0

    aget v6, v6, v7

    sub-int/2addr v3, v6

    sub-int/2addr v4, v5

    mul-int/2addr v3, v4

    sget-object v4, Lfi/firstbeat/ete/g;->a:[[I

    aget-object v4, v4, v0

    const/4 v6, 0x0

    aget v4, v4, v6

    sget-object v6, Lfi/firstbeat/ete/g;->a:[[I

    add-int/lit8 v0, v0, -0x1

    aget-object v0, v6, v0

    const/4 v6, 0x0

    aget v0, v0, v6

    sub-int v0, v4, v0

    div-int v0, v3, v0

    add-int/2addr v0, v5

    sub-int v2, v0, v2

    mul-int/lit8 v2, v2, 0x3

    div-int/lit8 v2, v2, 0x4

    sub-int/2addr v0, v2

    add-int/2addr v0, p0

    goto/16 :goto_9

    :cond_1f
    iget v0, p1, Lfi/firstbeat/ete/b;->z:I

    add-int/lit8 v4, v0, 0x1e

    const/16 v0, 0x64

    if-le v4, v0, :cond_20

    move v0, v1

    goto/16 :goto_0

    :cond_20
    iget v2, p1, Lfi/firstbeat/ete/b;->t:I

    iget v0, p1, Lfi/firstbeat/ete/b;->Q:I

    iget v5, p1, Lfi/firstbeat/ete/b;->l:I

    invoke-static {v0, v5}, Lfi/firstbeat/ete/g;->a(II)I

    move-result v5

    const/4 v0, 0x0

    move v12, v0

    move v0, v2

    move v2, v12

    :goto_a
    if-ge v2, v3, :cond_21

    const/16 v6, 0x3c

    invoke-static {v4, v0, v6}, Lfi/firstbeat/ete/c;->a(III)I

    move-result v0

    if-gt v0, v5, :cond_21

    add-int/lit8 v2, v2, 0x1

    goto :goto_a

    :cond_21
    iget v2, p1, Lfi/firstbeat/ete/b;->Q:I

    iget v3, p1, Lfi/firstbeat/ete/b;->l:I

    invoke-static {v2, v3}, Lfi/firstbeat/ete/g;->a(II)I

    move-result v2

    if-ge v0, v2, :cond_22

    move v0, v1

    goto/16 :goto_0

    :cond_22
    const/4 v0, 0x6

    goto/16 :goto_0

    :cond_23
    move v0, v2

    goto/16 :goto_8

    :cond_24
    move v0, v1

    goto/16 :goto_6

    :cond_25
    move v0, v1

    goto/16 :goto_2
.end method

.method public static a(Lfi/firstbeat/ete/b;)I
    .locals 4

    const/16 v0, 0x32

    move v1, v0

    :goto_0
    if-lez v1, :cond_2

    iget v2, p0, Lfi/firstbeat/ete/b;->l:I

    invoke-static {v1, v2}, Lfi/firstbeat/ete/g;->a(II)I

    move-result v2

    iget v3, p0, Lfi/firstbeat/ete/b;->w:I

    if-gt v2, v3, :cond_1

    if-ne v1, v0, :cond_0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v1, 0xa

    iget v3, p0, Lfi/firstbeat/ete/b;->l:I

    invoke-static {v0, v3}, Lfi/firstbeat/ete/g;->a(II)I

    move-result v0

    iget v3, p0, Lfi/firstbeat/ete/b;->w:I

    sub-int/2addr v3, v2

    shr-int/lit8 v3, v3, 0x10

    mul-int/lit8 v3, v3, 0xa

    sub-int/2addr v0, v2

    shr-int/lit8 v0, v0, 0x10

    div-int v0, v3, v0

    add-int/2addr v0, v1

    goto :goto_1

    :cond_1
    add-int/lit8 v1, v1, -0xa

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static b(II)I
    .locals 1

    if-le p0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lfi/firstbeat/ete/b;)I
    .locals 4

    iget v0, p0, Lfi/firstbeat/ete/b;->Q:I

    iget v1, p0, Lfi/firstbeat/ete/b;->l:I

    invoke-static {v0, v1}, Lfi/firstbeat/ete/g;->a(II)I

    move-result v0

    iget v1, p0, Lfi/firstbeat/ete/b;->w:I

    if-le v1, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v1, p0, Lfi/firstbeat/ete/b;->z:I

    iget v2, p0, Lfi/firstbeat/ete/b;->t:I

    iget v3, p0, Lfi/firstbeat/ete/b;->v:I

    invoke-static {v1, v0, v2, v3}, Lfi/firstbeat/ete/g;->a(IIII)I

    move-result v0

    goto :goto_0
.end method

.method public static c(Lfi/firstbeat/ete/b;)I
    .locals 4

    invoke-static {p0}, Lfi/firstbeat/ete/g;->a(Lfi/firstbeat/ete/b;)I

    move-result v0

    rem-int/lit8 v1, v0, 0xa

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, 0xa

    const/16 v1, 0x32

    if-le v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v1, p0, Lfi/firstbeat/ete/b;->z:I

    iget v2, p0, Lfi/firstbeat/ete/b;->l:I

    invoke-static {v0, v2}, Lfi/firstbeat/ete/g;->a(II)I

    move-result v0

    iget v2, p0, Lfi/firstbeat/ete/b;->t:I

    iget v3, p0, Lfi/firstbeat/ete/b;->v:I

    invoke-static {v1, v0, v2, v3}, Lfi/firstbeat/ete/g;->a(IIII)I

    move-result v0

    goto :goto_0
.end method

.method public static d(Lfi/firstbeat/ete/b;)I
    .locals 8

    const/16 v3, 0x1e

    const/16 v0, 0x14

    const/4 v7, 0x5

    const/4 v1, 0x1

    const/16 v2, 0xa

    invoke-static {p0}, Lfi/firstbeat/ete/g;->a(Lfi/firstbeat/ete/b;)I

    move-result v4

    iget v5, p0, Lfi/firstbeat/ete/b;->Q:I

    if-le v5, v2, :cond_1

    iget v5, p0, Lfi/firstbeat/ete/b;->V:I

    const/4 v6, -0x1

    if-ne v5, v6, :cond_1

    const/16 v0, 0x18

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v5, p0, Lfi/firstbeat/ete/b;->Q:I

    if-le v4, v5, :cond_2

    iget v5, p0, Lfi/firstbeat/ete/b;->Q:I

    if-ne v5, v2, :cond_6

    :cond_2
    if-ne v4, v0, :cond_3

    iget v5, p0, Lfi/firstbeat/ete/b;->Y:I

    if-ge v5, v0, :cond_3

    iput v0, p0, Lfi/firstbeat/ete/b;->Y:I

    move v0, v3

    goto :goto_0

    :cond_3
    if-ne v4, v3, :cond_4

    iget v5, p0, Lfi/firstbeat/ete/b;->Y:I

    if-ge v5, v3, :cond_4

    iput v3, p0, Lfi/firstbeat/ete/b;->Y:I

    const/16 v0, 0x1f

    goto :goto_0

    :cond_4
    const/16 v5, 0x28

    if-ne v4, v5, :cond_5

    iget v5, p0, Lfi/firstbeat/ete/b;->Y:I

    const/16 v6, 0x28

    if-ge v5, v6, :cond_5

    const/16 v0, 0x28

    iput v0, p0, Lfi/firstbeat/ete/b;->Y:I

    const/16 v0, 0x20

    goto :goto_0

    :cond_5
    const/16 v5, 0x32

    if-ne v4, v5, :cond_6

    iget v5, p0, Lfi/firstbeat/ete/b;->Y:I

    const/16 v6, 0x32

    if-ge v5, v6, :cond_6

    const/16 v0, 0x32

    iput v0, p0, Lfi/firstbeat/ete/b;->Y:I

    const/16 v0, 0x21

    goto :goto_0

    :cond_6
    iget v5, p0, Lfi/firstbeat/ete/b;->Q:I

    if-ne v5, v2, :cond_f

    iget v5, p0, Lfi/firstbeat/ete/b;->R:I

    if-nez v5, :cond_f

    iget v5, p0, Lfi/firstbeat/ete/b;->m:I

    mul-int/lit8 v5, v5, 0xa

    iget v6, p0, Lfi/firstbeat/ete/b;->i:I

    mul-int/lit8 v6, v6, 0x9

    if-ge v5, v6, :cond_0

    const/16 v5, 0x28

    if-ge v4, v5, :cond_8

    iget v5, p0, Lfi/firstbeat/ete/b;->V:I

    if-le v5, v7, :cond_8

    iget-object v5, p0, Lfi/firstbeat/ete/b;->S:[I

    aget v5, v5, v1

    if-ltz v5, :cond_7

    const/16 v5, 0xf

    if-ge v4, v5, :cond_8

    :cond_7
    move v0, v1

    goto :goto_0

    :cond_8
    if-ge v4, v0, :cond_a

    iget v0, p0, Lfi/firstbeat/ete/b;->V:I

    if-le v0, v7, :cond_9

    const/4 v0, 0x2

    goto :goto_0

    :cond_9
    iget v0, p0, Lfi/firstbeat/ete/b;->V:I

    if-ltz v0, :cond_a

    const/4 v0, 0x3

    goto :goto_0

    :cond_a
    if-ge v4, v3, :cond_c

    iget v0, p0, Lfi/firstbeat/ete/b;->V:I

    if-le v0, v7, :cond_b

    const/4 v0, 0x6

    goto :goto_0

    :cond_b
    iget v0, p0, Lfi/firstbeat/ete/b;->V:I

    if-ltz v0, :cond_c

    const/4 v0, 0x7

    goto/16 :goto_0

    :cond_c
    const/16 v0, 0x28

    if-ge v4, v0, :cond_e

    iget v0, p0, Lfi/firstbeat/ete/b;->V:I

    if-le v0, v7, :cond_d

    move v0, v2

    goto/16 :goto_0

    :cond_d
    iget v0, p0, Lfi/firstbeat/ete/b;->V:I

    if-ltz v0, :cond_e

    const/16 v0, 0xb

    goto/16 :goto_0

    :cond_e
    const/16 v0, 0xe

    goto/16 :goto_0

    :cond_f
    iget v3, p0, Lfi/firstbeat/ete/b;->Q:I

    if-le v3, v2, :cond_13

    iget v3, p0, Lfi/firstbeat/ete/b;->R:I

    if-lez v3, :cond_13

    iget v3, p0, Lfi/firstbeat/ete/b;->V:I

    const/16 v5, 0x9

    if-gt v3, v5, :cond_0

    iget v0, p0, Lfi/firstbeat/ete/b;->V:I

    if-le v0, v7, :cond_11

    iget v0, p0, Lfi/firstbeat/ete/b;->V:I

    if-ge v0, v2, :cond_11

    iget-object v0, p0, Lfi/firstbeat/ete/b;->T:[I

    aget v0, v0, v1

    if-lez v0, :cond_10

    iget-object v0, p0, Lfi/firstbeat/ete/b;->T:[I

    aget v0, v0, v1

    iget v2, p0, Lfi/firstbeat/ete/b;->R:I

    if-gt v0, v2, :cond_10

    const/16 v0, 0xf

    if-lt v4, v0, :cond_10

    const/16 v0, 0x16

    goto/16 :goto_0

    :cond_10
    move v0, v1

    goto/16 :goto_0

    :cond_11
    iget v0, p0, Lfi/firstbeat/ete/b;->V:I

    if-ltz v0, :cond_12

    iget v0, p0, Lfi/firstbeat/ete/b;->V:I

    const/4 v1, 0x6

    if-ge v0, v1, :cond_12

    const/16 v0, 0x17

    goto/16 :goto_0

    :cond_12
    iget v0, p0, Lfi/firstbeat/ete/b;->V:I

    const/4 v1, -0x2

    if-ne v0, v1, :cond_16

    const/16 v0, 0x19

    goto/16 :goto_0

    :cond_13
    iget v0, p0, Lfi/firstbeat/ete/b;->Q:I

    if-le v0, v2, :cond_16

    iget v0, p0, Lfi/firstbeat/ete/b;->R:I

    if-nez v0, :cond_16

    iget v0, p0, Lfi/firstbeat/ete/b;->Q:I

    if-ge v4, v0, :cond_16

    iget v0, p0, Lfi/firstbeat/ete/b;->V:I

    if-le v0, v7, :cond_15

    const/16 v0, 0xf

    if-ge v4, v0, :cond_14

    move v0, v1

    goto/16 :goto_0

    :cond_14
    const/16 v0, 0x16

    goto/16 :goto_0

    :cond_15
    iget v0, p0, Lfi/firstbeat/ete/b;->V:I

    if-ltz v0, :cond_16

    const/16 v0, 0x17

    goto/16 :goto_0

    :cond_16
    const/4 v0, 0x0

    goto/16 :goto_0
.end method
