.class public Lfi/firstbeat/ete/ETEc;
.super Ljava/lang/Object;

# interfaces
.implements Lfi/firstbeat/ete/ETE;


# static fields
.field private static a:Lfi/firstbeat/ete/b;

.field private static b:Lfi/firstbeat/ete/b;

.field private static final c:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lfi/firstbeat/ete/b;

    invoke-direct {v0}, Lfi/firstbeat/ete/b;-><init>()V

    sput-object v0, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    const/4 v0, 0x0

    sput-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    const/16 v0, 0xe

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lfi/firstbeat/ete/ETEc;->c:[I

    return-void

    :array_0
    .array-data 4
        0x0
        0x18
        0x50
        0xa8
        0x120
        0x160
        0x270
        0x2d0
        0x3e0
        0x440
        0x4ac
        0x550
        0x638
        0x6fc
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a()I
    .locals 3

    const/4 v2, 0x1

    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v0, v0, Lfi/firstbeat/ete/b;->F:I

    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v1, v1, Lfi/firstbeat/ete/b;->G:I

    sub-int/2addr v0, v1

    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v1, v1, Lfi/firstbeat/ete/b;->M:I

    if-lez v1, :cond_1

    const/16 v1, 0x18

    if-lt v0, v1, :cond_1

    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v1, v1, Lfi/firstbeat/ete/b;->N:I

    if-ne v1, v2, :cond_0

    const/16 v0, 0x3c

    :goto_0
    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v1, v1, Lfi/firstbeat/ete/b;->M:I

    const v2, 0x38000

    invoke-static {v1, v2}, Lfi/firstbeat/ete/e;->b(II)I

    move-result v1

    mul-int/2addr v1, v0

    rsub-int/lit8 v0, v0, 0x64

    sget-object v2, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v2, v2, Lfi/firstbeat/ete/b;->O:I

    mul-int/2addr v0, v2

    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x64

    :goto_1
    return v0

    :cond_0
    mul-int/lit8 v0, v0, 0x28

    div-int/lit16 v0, v0, 0x168

    goto :goto_0

    :cond_1
    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v0, v0, Lfi/firstbeat/ete/b;->N:I

    if-ne v0, v2, :cond_2

    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v0, v0, Lfi/firstbeat/ete/b;->O:I

    goto :goto_1
.end method

.method private static a(III)I
    .locals 9

    const/4 v0, 0x0

    const/4 v1, 0x0

    sget-object v2, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    if-nez v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    sget-object v3, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    const/4 v2, 0x0

    :goto_1
    const/16 v4, 0x1f

    if-ge v2, v4, :cond_1

    iget-object v4, v3, Lfi/firstbeat/ete/b;->a:[I

    iget-object v5, v3, Lfi/firstbeat/ete/b;->a:[I

    add-int/lit8 v6, v2, 0x1

    aget v5, v5, v6

    aput v5, v4, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    iget-object v2, v3, Lfi/firstbeat/ete/b;->a:[I

    const/16 v4, 0x1f

    aput p0, v2, v4

    const/4 v2, 0x1

    iput v2, v3, Lfi/firstbeat/ete/b;->d:I

    sget-object v2, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v3, v2, Lfi/firstbeat/ete/b;->C:I

    add-int/2addr v3, p0

    iput v3, v2, Lfi/firstbeat/ete/b;->C:I

    const/16 v2, 0x1388

    if-gt p0, v2, :cond_2

    const v2, 0x8e39

    if-lt p1, v2, :cond_2

    const v2, 0x6cccd

    if-gt p1, v2, :cond_2

    if-ltz p2, :cond_2

    const/high16 v2, 0x27100000

    if-gt p2, v2, :cond_2

    div-int/lit16 v2, p0, 0xc8

    sget-object v3, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v4, v3, Lfi/firstbeat/ete/b;->H:I

    mul-int v5, v2, p1

    add-int/2addr v4, v5

    iput v4, v3, Lfi/firstbeat/ete/b;->H:I

    sget-object v3, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v4, v3, Lfi/firstbeat/ete/b;->I:I

    mul-int v5, v2, p2

    div-int/lit8 v5, v5, 0xa

    add-int/2addr v4, v5

    iput v4, v3, Lfi/firstbeat/ete/b;->I:I

    sget-object v3, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v4, v3, Lfi/firstbeat/ete/b;->E:I

    add-int/2addr v2, v4

    iput v2, v3, Lfi/firstbeat/ete/b;->E:I

    :cond_2
    sget-object v2, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v2, v2, Lfi/firstbeat/ete/b;->C:I

    div-int/lit16 v2, v2, 0x1388

    sget-object v3, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v3, v3, Lfi/firstbeat/ete/b;->D:I

    if-eq v2, v3, :cond_1b

    sget-object v2, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    sget-object v3, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v3, v3, Lfi/firstbeat/ete/b;->t:I

    iput v3, v2, Lfi/firstbeat/ete/b;->u:I

    sget-object v2, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v2, v2, Lfi/firstbeat/ete/b;->E:I

    const/4 v3, 0x5

    if-le v2, v3, :cond_6

    const v0, 0x8001

    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v1, v1, Lfi/firstbeat/ete/b;->H:I

    sget-object v2, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v2, v2, Lfi/firstbeat/ete/b;->E:I

    div-int/2addr v1, v2

    sget-object v2, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v2, v2, Lfi/firstbeat/ete/b;->J:I

    if-lez v2, :cond_3

    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v0, v0, Lfi/firstbeat/ete/b;->J:I

    sub-int v0, v1, v0

    :cond_3
    sget-object v2, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iput v1, v2, Lfi/firstbeat/ete/b;->J:I

    const v1, 0x10001

    sget-object v2, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    const/4 v3, 0x0

    iput v3, v2, Lfi/firstbeat/ete/b;->L:I

    sget-object v2, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v2, v2, Lfi/firstbeat/ete/b;->I:I

    sget-object v3, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v3, v3, Lfi/firstbeat/ete/b;->E:I

    div-int/2addr v2, v3

    mul-int/lit8 v2, v2, 0xa

    sget-object v3, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v3, v3, Lfi/firstbeat/ete/b;->K:I

    if-gtz v3, :cond_4

    sget-object v3, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v3, v3, Lfi/firstbeat/ete/b;->K:I

    if-nez v3, :cond_5

    const/high16 v3, 0x10000

    if-gt v2, v3, :cond_5

    :cond_4
    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v1, v1, Lfi/firstbeat/ete/b;->K:I

    sub-int v1, v2, v1

    sget-object v3, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    sget-object v4, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v4, v4, Lfi/firstbeat/ete/b;->J:I

    mul-int/lit8 v4, v4, 0x5

    invoke-static {v1, v4}, Lfi/firstbeat/ete/e;->b(II)I

    move-result v4

    iput v4, v3, Lfi/firstbeat/ete/b;->L:I

    :cond_5
    sget-object v3, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iput v2, v3, Lfi/firstbeat/ete/b;->K:I

    move v6, v1

    move v7, v0

    :goto_2
    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v0, v0, Lfi/firstbeat/ete/b;->i:I

    if-nez v0, :cond_8

    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_6
    sget-object v2, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v2, v2, Lfi/firstbeat/ete/b;->E:I

    if-nez v2, :cond_7

    sget-object v2, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    const/4 v3, 0x0

    iput v3, v2, Lfi/firstbeat/ete/b;->J:I

    sget-object v2, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    const/4 v3, 0x0

    iput v3, v2, Lfi/firstbeat/ete/b;->K:I

    sget-object v2, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    const/4 v3, 0x0

    iput v3, v2, Lfi/firstbeat/ete/b;->L:I

    :cond_7
    move v6, v1

    move v7, v0

    goto :goto_2

    :cond_8
    sget-object v8, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    const/4 v0, 0x1

    invoke-static {v8, v0}, Lfi/firstbeat/ete/b;->a(Lfi/firstbeat/ete/b;I)V

    iget v0, v8, Lfi/firstbeat/ete/b;->p:I

    if-lez v0, :cond_1c

    iget v0, v8, Lfi/firstbeat/ete/b;->n:I

    iput v0, v8, Lfi/firstbeat/ete/b;->m:I

    iget v0, v8, Lfi/firstbeat/ete/b;->t:I

    if-nez v0, :cond_9

    iget v0, v8, Lfi/firstbeat/ete/b;->n:I

    iput v0, v8, Lfi/firstbeat/ete/b;->m:I

    :cond_9
    const/4 v0, 0x0

    :goto_3
    const/16 v1, 0x20

    if-ge v0, v1, :cond_a

    iget-object v1, v8, Lfi/firstbeat/ete/b;->b:[I

    const/4 v2, 0x0

    aput v2, v1, v0

    iget-object v1, v8, Lfi/firstbeat/ete/b;->c:[I

    const/4 v2, 0x0

    aput v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_a
    const/4 v0, 0x0

    :goto_4
    const/16 v1, 0x20

    if-ge v0, v1, :cond_b

    iget-object v1, v8, Lfi/firstbeat/ete/b;->c:[I

    iget-object v2, v8, Lfi/firstbeat/ete/b;->a:[I

    aget v2, v2, v0

    aput v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_b
    iget-object v0, v8, Lfi/firstbeat/ete/b;->a:[I

    iget-object v1, v8, Lfi/firstbeat/ete/b;->c:[I

    iget-object v2, v8, Lfi/firstbeat/ete/b;->f:[I

    invoke-static {v0, v1, v2, v8}, Lfi/firstbeat/ete/a;->a([I[I[ILfi/firstbeat/ete/b;)V

    iget-object v0, v8, Lfi/firstbeat/ete/b;->c:[I

    iget-object v1, v8, Lfi/firstbeat/ete/b;->b:[I

    invoke-static {v0, v1}, Lfi/firstbeat/ete/f;->a([I[I)V

    iget-object v0, v8, Lfi/firstbeat/ete/b;->b:[I

    iget v1, v8, Lfi/firstbeat/ete/b;->m:I

    iget v2, v8, Lfi/firstbeat/ete/b;->A:I

    invoke-static {v0, v1, v2, v8}, Lfi/firstbeat/ete/f;->a([IIILfi/firstbeat/ete/b;)I

    move-result v0

    iput v0, v8, Lfi/firstbeat/ete/b;->A:I

    iget v0, v8, Lfi/firstbeat/ete/b;->A:I

    iput v0, v8, Lfi/firstbeat/ete/b;->q:I

    :goto_5
    iget v3, v8, Lfi/firstbeat/ete/b;->r:I

    iget v0, v8, Lfi/firstbeat/ete/b;->s:I

    iput v0, v8, Lfi/firstbeat/ete/b;->r:I

    iget v0, v8, Lfi/firstbeat/ete/b;->n:I

    iget v1, v8, Lfi/firstbeat/ete/b;->i:I

    if-le v0, v1, :cond_1e

    const/16 v0, 0x64

    :goto_6
    iget v1, v8, Lfi/firstbeat/ete/b;->r:I

    const/4 v2, 0x5

    invoke-static {v0, v1, v2}, Lfi/firstbeat/ete/c;->a(III)I

    move-result v0

    iput v0, v8, Lfi/firstbeat/ete/b;->s:I

    iget v0, v8, Lfi/firstbeat/ete/b;->m:I

    iget v1, v8, Lfi/firstbeat/ete/b;->A:I

    iget v2, v8, Lfi/firstbeat/ete/b;->i:I

    iget v4, v8, Lfi/firstbeat/ete/b;->r:I

    iget v5, v8, Lfi/firstbeat/ete/b;->s:I

    invoke-static/range {v0 .. v5}, Lfi/firstbeat/ete/d;->a(IIIIII)I

    move-result v0

    iput v0, v8, Lfi/firstbeat/ete/b;->z:I

    iget v0, v8, Lfi/firstbeat/ete/b;->z:I

    iget v1, v8, Lfi/firstbeat/ete/b;->t:I

    const/4 v2, 0x5

    invoke-static {v0, v1, v2}, Lfi/firstbeat/ete/c;->a(III)I

    move-result v0

    iput v0, v8, Lfi/firstbeat/ete/b;->t:I

    iget v0, v8, Lfi/firstbeat/ete/b;->z:I

    const/16 v1, 0x1e

    if-le v0, v1, :cond_1f

    iget v0, v8, Lfi/firstbeat/ete/b;->v:I

    iget v1, v8, Lfi/firstbeat/ete/b;->z:I

    mul-int/lit16 v1, v1, 0x1bf2

    div-int/lit8 v1, v1, 0x64

    add-int/2addr v0, v1

    iput v0, v8, Lfi/firstbeat/ete/b;->v:I

    :cond_c
    :goto_7
    iget v0, v8, Lfi/firstbeat/ete/b;->t:I

    iget v1, v8, Lfi/firstbeat/ete/b;->w:I

    if-le v0, v1, :cond_d

    iget v0, v8, Lfi/firstbeat/ete/b;->t:I

    iput v0, v8, Lfi/firstbeat/ete/b;->w:I

    :cond_d
    iget v0, v8, Lfi/firstbeat/ete/b;->v:I

    iget v1, v8, Lfi/firstbeat/ete/b;->w:I

    if-le v0, v1, :cond_e

    iget v0, v8, Lfi/firstbeat/ete/b;->v:I

    iput v0, v8, Lfi/firstbeat/ete/b;->w:I

    :cond_e
    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v1, v1, Lfi/firstbeat/ete/b;->O:I

    div-int/lit8 v1, v1, 0x64

    sget-object v2, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v2, v2, Lfi/firstbeat/ete/b;->z:I

    mul-int/2addr v1, v2

    sget-object v2, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v2, v2, Lfi/firstbeat/ete/b;->P:I

    mul-int/2addr v1, v2

    div-int/lit16 v1, v1, 0x3e8

    const v2, 0x10e333

    invoke-static {v1, v2}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v1

    iput v1, v0, Lfi/firstbeat/ete/b;->x:I

    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v1, v0, Lfi/firstbeat/ete/b;->y:I

    sget-object v2, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v2, v2, Lfi/firstbeat/ete/b;->x:I

    div-int/lit8 v2, v2, 0x3c

    mul-int/lit8 v2, v2, 0x5

    add-int/2addr v1, v2

    iput v1, v0, Lfi/firstbeat/ete/b;->y:I

    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget-object v0, v0, Lfi/firstbeat/ete/b;->S:[I

    const/4 v1, 0x1

    sget-object v2, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    invoke-static {v2}, Lfi/firstbeat/ete/g;->c(Lfi/firstbeat/ete/b;)I

    move-result v2

    aput v2, v0, v1

    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget-object v0, v0, Lfi/firstbeat/ete/b;->T:[I

    const/4 v1, 0x1

    sget-object v2, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    invoke-static {v2}, Lfi/firstbeat/ete/g;->b(Lfi/firstbeat/ete/b;)I

    move-result v2

    aput v2, v0, v1

    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v0, v0, Lfi/firstbeat/ete/b;->V:I

    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    sget-object v2, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v2, v2, Lfi/firstbeat/ete/b;->C:I

    div-int/lit16 v2, v2, 0x3e8

    div-int/lit8 v2, v2, 0x3c

    sget-object v3, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    invoke-static {v2, v3}, Lfi/firstbeat/ete/g;->a(ILfi/firstbeat/ete/b;)I

    move-result v2

    iput v2, v1, Lfi/firstbeat/ete/b;->V:I

    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget-object v1, v1, Lfi/firstbeat/ete/b;->S:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    const/4 v2, -0x5

    if-ne v1, v2, :cond_f

    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget-object v1, v1, Lfi/firstbeat/ete/b;->S:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    const/16 v2, 0xb4

    if-gt v1, v2, :cond_f

    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget-object v1, v1, Lfi/firstbeat/ete/b;->S:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    if-gtz v1, :cond_10

    :cond_f
    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget-object v1, v1, Lfi/firstbeat/ete/b;->S:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    if-lez v1, :cond_11

    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget-object v1, v1, Lfi/firstbeat/ete/b;->S:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    if-ltz v1, :cond_11

    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget-object v1, v1, Lfi/firstbeat/ete/b;->S:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    const/16 v2, 0xb4

    if-gt v1, v2, :cond_11

    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget-object v1, v1, Lfi/firstbeat/ete/b;->S:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    sget-object v2, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget-object v2, v2, Lfi/firstbeat/ete/b;->S:[I

    const/4 v3, 0x0

    aget v2, v2, v3

    sub-int/2addr v1, v2

    const/16 v2, 0xa

    if-lt v1, v2, :cond_11

    :cond_10
    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget-object v1, v1, Lfi/firstbeat/ete/b;->S:[I

    const/4 v2, 0x1

    sget-object v3, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget-object v3, v3, Lfi/firstbeat/ete/b;->S:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    add-int/lit8 v3, v3, 0xa

    aput v3, v1, v2

    :cond_11
    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget-object v1, v1, Lfi/firstbeat/ete/b;->T:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    const/4 v2, -0x5

    if-ne v1, v2, :cond_12

    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget-object v1, v1, Lfi/firstbeat/ete/b;->T:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    const/16 v2, 0xb4

    if-gt v1, v2, :cond_12

    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget-object v1, v1, Lfi/firstbeat/ete/b;->T:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    if-gtz v1, :cond_13

    :cond_12
    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget-object v1, v1, Lfi/firstbeat/ete/b;->T:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    if-lez v1, :cond_20

    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget-object v1, v1, Lfi/firstbeat/ete/b;->T:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    if-ltz v1, :cond_20

    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget-object v1, v1, Lfi/firstbeat/ete/b;->T:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    const/16 v2, 0xb4

    if-gt v1, v2, :cond_20

    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget-object v1, v1, Lfi/firstbeat/ete/b;->T:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    sget-object v2, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget-object v2, v2, Lfi/firstbeat/ete/b;->T:[I

    const/4 v3, 0x0

    aget v2, v2, v3

    sub-int/2addr v1, v2

    const/16 v2, 0xa

    if-lt v1, v2, :cond_20

    :cond_13
    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget-object v1, v1, Lfi/firstbeat/ete/b;->T:[I

    const/4 v2, 0x1

    sget-object v3, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget-object v3, v3, Lfi/firstbeat/ete/b;->T:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    add-int/lit8 v3, v3, 0xa

    aput v3, v1, v2

    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    const/4 v2, 0x1

    iput v2, v1, Lfi/firstbeat/ete/b;->U:I

    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    sget-object v2, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v2, v2, Lfi/firstbeat/ete/b;->C:I

    div-int/lit16 v2, v2, 0x3e8

    div-int/lit8 v2, v2, 0x3c

    sget-object v3, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    invoke-static {v2, v3}, Lfi/firstbeat/ete/g;->a(ILfi/firstbeat/ete/b;)I

    move-result v2

    iput v2, v1, Lfi/firstbeat/ete/b;->V:I

    :goto_8
    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget-object v1, v1, Lfi/firstbeat/ete/b;->T:[I

    const/4 v2, 0x0

    sget-object v3, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget-object v3, v3, Lfi/firstbeat/ete/b;->T:[I

    const/4 v4, 0x1

    aget v3, v3, v4

    aput v3, v1, v2

    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget-object v1, v1, Lfi/firstbeat/ete/b;->S:[I

    const/4 v2, 0x0

    sget-object v3, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget-object v3, v3, Lfi/firstbeat/ete/b;->S:[I

    const/4 v4, 0x1

    aget v3, v3, v4

    aput v3, v1, v2

    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v1, v1, Lfi/firstbeat/ete/b;->U:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_14

    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v1, v1, Lfi/firstbeat/ete/b;->V:I

    if-le v1, v0, :cond_14

    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iput v0, v1, Lfi/firstbeat/ete/b;->V:I

    :cond_14
    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    invoke-static {v0}, Lfi/firstbeat/ete/g;->d(Lfi/firstbeat/ete/b;)I

    move-result v0

    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v1, v1, Lfi/firstbeat/ete/b;->C:I

    sget-object v2, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v2, v2, Lfi/firstbeat/ete/b;->X:I

    sub-int/2addr v1, v2

    sget-object v2, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v2, v2, Lfi/firstbeat/ete/b;->W:I

    if-eq v0, v2, :cond_16

    const/16 v2, 0x3a98

    if-gt v1, v2, :cond_15

    const/16 v2, 0x2710

    if-le v1, v2, :cond_16

    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v1, v1, Lfi/firstbeat/ete/b;->W:I

    const/16 v2, 0x1e

    if-ge v1, v2, :cond_16

    :cond_15
    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iput v0, v1, Lfi/firstbeat/ete/b;->W:I

    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v1, v1, Lfi/firstbeat/ete/b;->C:I

    iput v1, v0, Lfi/firstbeat/ete/b;->X:I

    :cond_16
    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v0, v0, Lfi/firstbeat/ete/b;->F:I

    const/16 v1, 0x168

    if-ge v0, v1, :cond_1a

    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v0, v0, Lfi/firstbeat/ete/b;->E:I

    const/4 v1, 0x5

    if-le v0, v1, :cond_1a

    const/16 v0, -0x8000

    if-lt v7, v0, :cond_1a

    const v0, 0x8000

    if-gt v7, v0, :cond_1a

    const/high16 v0, -0x10000

    if-lt v6, v0, :cond_1a

    const/high16 v0, 0x10000

    if-gt v6, v0, :cond_1a

    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v0, v0, Lfi/firstbeat/ete/b;->u:I

    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v1, v1, Lfi/firstbeat/ete/b;->t:I

    if-ge v0, v1, :cond_1a

    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v0, v0, Lfi/firstbeat/ete/b;->p:I

    if-lez v0, :cond_1a

    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v0, v0, Lfi/firstbeat/ete/b;->m:I

    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v1, v1, Lfi/firstbeat/ete/b;->i:I

    sget-object v2, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v2, v2, Lfi/firstbeat/ete/b;->J:I

    sget-object v3, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v3, v3, Lfi/firstbeat/ete/b;->L:I

    const/16 v4, -0x11dc

    if-lt v3, v4, :cond_17

    const/16 v4, 0x4242

    if-le v3, v4, :cond_21

    :cond_17
    const/4 v0, -0x1

    :cond_18
    :goto_9
    if-lez v0, :cond_26

    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v1, v1, Lfi/firstbeat/ete/b;->F:I

    if-nez v1, :cond_25

    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iput v0, v1, Lfi/firstbeat/ete/b;->M:I

    :cond_19
    :goto_a
    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v2, v1, Lfi/firstbeat/ete/b;->F:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lfi/firstbeat/ete/b;->F:I

    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v1, v1, Lfi/firstbeat/ete/b;->M:I

    sub-int v0, v1, v0

    invoke-static {v0}, Lfi/firstbeat/ete/e;->b(I)I

    move-result v0

    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v1, v1, Lfi/firstbeat/ete/b;->F:I

    const/16 v2, 0x30

    if-lt v1, v2, :cond_1a

    const/high16 v1, 0x70000

    if-le v0, v1, :cond_1a

    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v1, v1, Lfi/firstbeat/ete/b;->G:I

    add-int/lit8 v1, v1, 0x2

    iput v1, v0, Lfi/firstbeat/ete/b;->G:I

    :cond_1a
    :goto_b
    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    const/4 v1, 0x0

    iput v1, v0, Lfi/firstbeat/ete/b;->H:I

    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    const/4 v1, 0x0

    iput v1, v0, Lfi/firstbeat/ete/b;->I:I

    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    const/4 v1, 0x0

    iput v1, v0, Lfi/firstbeat/ete/b;->E:I

    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v1, v0, Lfi/firstbeat/ete/b;->D:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lfi/firstbeat/ete/b;->D:I

    :cond_1b
    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_1c
    iget v0, v8, Lfi/firstbeat/ete/b;->t:I

    if-nez v0, :cond_1d

    iget v0, v8, Lfi/firstbeat/ete/b;->n:I

    iput v0, v8, Lfi/firstbeat/ete/b;->m:I

    :cond_1d
    iget v0, v8, Lfi/firstbeat/ete/b;->q:I

    iput v0, v8, Lfi/firstbeat/ete/b;->A:I

    goto/16 :goto_5

    :cond_1e
    mul-int/lit8 v0, v0, 0x64

    div-int/2addr v0, v1

    goto/16 :goto_6

    :cond_1f
    iget v0, v8, Lfi/firstbeat/ete/b;->v:I

    add-int/lit16 v0, v0, -0xaab

    iput v0, v8, Lfi/firstbeat/ete/b;->v:I

    iget v0, v8, Lfi/firstbeat/ete/b;->v:I

    if-gez v0, :cond_c

    const/4 v0, 0x0

    iput v0, v8, Lfi/firstbeat/ete/b;->v:I

    goto/16 :goto_7

    :cond_20
    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    const/4 v2, 0x0

    iput v2, v1, Lfi/firstbeat/ete/b;->U:I

    goto/16 :goto_8

    :cond_21
    shl-int/lit8 v0, v0, 0x10

    shl-int/lit8 v1, v1, 0x10

    invoke-static {v0, v1}, Lfi/firstbeat/ete/e;->b(II)I

    move-result v1

    const/high16 v0, 0x10000

    if-le v1, v0, :cond_22

    const/4 v0, -0x2

    goto/16 :goto_9

    :cond_22
    const v0, 0x1923a

    invoke-static {v0, v1}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v0

    neg-int v0, v0

    const v4, 0x2923a

    add-int/2addr v0, v4

    const v4, 0x132994

    invoke-static {v4, v3}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v3

    const v4, 0xb199a

    add-int/2addr v3, v4

    invoke-static {v3, v2}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v2

    const v3, 0x55555

    add-int/2addr v2, v3

    invoke-static {v0, v2}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v0

    const/high16 v2, 0x150000

    if-ge v0, v2, :cond_23

    const/4 v0, -0x3

    goto/16 :goto_9

    :cond_23
    const/high16 v2, 0x500000

    if-le v0, v2, :cond_24

    const/4 v0, -0x4

    goto/16 :goto_9

    :cond_24
    const v2, 0xb333

    if-ge v1, v2, :cond_18

    const/4 v0, -0x5

    goto/16 :goto_9

    :cond_25
    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v1, v1, Lfi/firstbeat/ete/b;->F:I

    if-lez v1, :cond_19

    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    sget-object v2, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v2, v2, Lfi/firstbeat/ete/b;->F:I

    sget-object v3, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v3, v3, Lfi/firstbeat/ete/b;->M:I

    mul-int/2addr v2, v3

    add-int/2addr v2, v0

    sget-object v3, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v3, v3, Lfi/firstbeat/ete/b;->F:I

    add-int/lit8 v3, v3, 0x1

    div-int/2addr v2, v3

    iput v2, v1, Lfi/firstbeat/ete/b;->M:I

    goto/16 :goto_a

    :cond_26
    const/4 v1, -0x4

    if-ne v0, v1, :cond_1a

    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v1, v1, Lfi/firstbeat/ete/b;->G:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lfi/firstbeat/ete/b;->G:I

    goto/16 :goto_b
.end method


# virtual methods
.method public Analyzer(III)I
    .locals 1

    invoke-static {p1, p2, p3}, Lfi/firstbeat/ete/ETEc;->a(III)I

    move-result v0

    return v0
.end method

.method public AnalyzerHr(III)I
    .locals 6

    const/4 v1, 0x0

    const v0, 0xea60

    div-int v3, v0, p1

    const/16 v0, 0x1388

    if-lt v3, v0, :cond_0

    invoke-static {v3, p2, p3}, Lfi/firstbeat/ete/ETEc;->a(III)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    sget-object v2, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v2, v2, Lfi/firstbeat/ete/b;->e:I

    add-int/lit16 v2, v2, 0x1388

    iput v2, v0, Lfi/firstbeat/ete/b;->e:I

    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v0, v0, Lfi/firstbeat/ete/b;->e:I

    if-ge v0, v3, :cond_1

    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iput v1, v0, Lfi/firstbeat/ete/b;->e:I

    invoke-static {v3, p2, p3}, Lfi/firstbeat/ete/ETEc;->a(III)I

    move-result v0

    goto :goto_0

    :cond_1
    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v0, v0, Lfi/firstbeat/ete/b;->e:I

    div-int/2addr v0, v3

    sget-object v2, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v2, v2, Lfi/firstbeat/ete/b;->e:I

    mul-int v4, v0, v3

    sub-int/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    sget-object v4, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v4, v4, Lfi/firstbeat/ete/b;->e:I

    add-int/lit8 v5, v0, 0x1

    mul-int/2addr v5, v3

    sub-int/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    if-le v2, v4, :cond_2

    add-int/lit8 v0, v0, 0x1

    :cond_2
    sget-object v2, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    sget-object v4, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v4, v4, Lfi/firstbeat/ete/b;->e:I

    mul-int v5, v0, v3

    sub-int/2addr v4, v5

    iput v4, v2, Lfi/firstbeat/ete/b;->e:I

    move v2, v1

    :goto_1
    if-ge v2, v0, :cond_4

    invoke-static {v3, p2, p3}, Lfi/firstbeat/ete/ETEc;->a(III)I

    move-result v4

    if-nez v4, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public AnalyzerW(II)I
    .locals 3

    const/4 v0, 0x0

    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const v1, 0x11a4b

    invoke-static {v1, p2}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v1

    const v2, 0x1f8812

    add-int/2addr v1, v2

    sget-object v2, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v2, v2, Lfi/firstbeat/ete/b;->P:I

    div-int/2addr v1, v2

    add-int/lit16 v1, v1, -0x7b01

    sget-object v2, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iput v0, v2, Lfi/firstbeat/ete/b;->e:I

    invoke-static {p1, v1, v0}, Lfi/firstbeat/ete/ETEc;->a(III)I

    move-result v0

    goto :goto_0
.end method

.method public ETEVersion()Ljava/lang/String;
    .locals 1

    const-string v0, "3.5.9"

    return-object v0
.end method

.method public GetResult(Lfi/firstbeat/ete/ETEresults;)I
    .locals 8

    const v3, -0x26666

    const/16 v4, 0x64

    const/4 v5, 0x1

    const/4 v1, 0x0

    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    if-nez v0, :cond_0

    :goto_0
    return v1

    :cond_0
    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v0, v0, Lfi/firstbeat/ete/b;->d:I

    if-nez v0, :cond_4

    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v0, v0, Lfi/firstbeat/ete/b;->n:I

    iput v0, p1, Lfi/firstbeat/ete/ETEresults;->ETEcorrectedHr:I

    :goto_1
    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v0, v0, Lfi/firstbeat/ete/b;->g:I

    iput v0, p1, Lfi/firstbeat/ete/ETEresults;->ETEartifactPercent:I

    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v0, v0, Lfi/firstbeat/ete/b;->j:I

    iput v0, p1, Lfi/firstbeat/ete/ETEresults;->ETEminimalHr:I

    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v0, v0, Lfi/firstbeat/ete/b;->k:I

    iput v0, p1, Lfi/firstbeat/ete/ETEresults;->ETEmaximalHr:I

    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v0, v0, Lfi/firstbeat/ete/b;->t:I

    iput v0, p1, Lfi/firstbeat/ete/ETEresults;->ETEepoc:I

    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v0, v0, Lfi/firstbeat/ete/b;->w:I

    iput v0, p1, Lfi/firstbeat/ete/ETEresults;->ETEtrainingLoadPeak:I

    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    invoke-static {v0}, Lfi/firstbeat/ete/g;->a(Lfi/firstbeat/ete/b;)I

    move-result v0

    iput v0, p1, Lfi/firstbeat/ete/ETEresults;->ETEtrainingEffect:I

    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v0, v0, Lfi/firstbeat/ete/b;->x:I

    iput v0, p1, Lfi/firstbeat/ete/ETEresults;->ETEenergyExpenditure:I

    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v0, v0, Lfi/firstbeat/ete/b;->y:I

    iput v0, p1, Lfi/firstbeat/ete/ETEresults;->ETEenergyExpenditureCumulative:I

    invoke-static {}, Lfi/firstbeat/ete/ETEc;->a()I

    move-result v0

    iput v0, p1, Lfi/firstbeat/ete/ETEresults;->ETEmaximalMET:I

    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v0, v0, Lfi/firstbeat/ete/b;->z:I

    iput v0, p1, Lfi/firstbeat/ete/ETEresults;->ETEmaximalMETpercentage:I

    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v0, v0, Lfi/firstbeat/ete/b;->F:I

    sget-object v2, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v2, v2, Lfi/firstbeat/ete/b;->G:I

    sub-int/2addr v0, v2

    sget-object v2, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v2, v2, Lfi/firstbeat/ete/b;->M:I

    if-lez v2, :cond_5

    const/16 v2, 0x18

    if-lt v0, v2, :cond_5

    mul-int/lit8 v0, v0, 0x5

    div-int/lit8 v0, v0, 0x3c

    iput v0, p1, Lfi/firstbeat/ete/ETEresults;->ETEmaximalMETminutes:I

    :goto_2
    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v0, v0, Lfi/firstbeat/ete/b;->D:I

    sget-object v2, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v2, v2, Lfi/firstbeat/ete/b;->Z:I

    mul-int/lit8 v0, v0, 0x5

    div-int/lit8 v0, v0, 0x3c

    sub-int/2addr v2, v0

    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v0, v0, Lfi/firstbeat/ete/b;->N:I

    if-nez v0, :cond_b

    invoke-static {}, Lfi/firstbeat/ete/ETEc;->a()I

    move-result v0

    if-lez v0, :cond_b

    sget-object v6, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v6, v6, Lfi/firstbeat/ete/b;->O:I

    sub-int/2addr v0, v6

    const v6, 0x38000

    invoke-static {v0, v6}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v0

    if-lez v0, :cond_a

    const/high16 v6, 0x10000

    mul-int/lit8 v7, v0, 0x5

    div-int/lit8 v7, v7, 0x6

    sub-int/2addr v6, v7

    invoke-static {v2, v6}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v2

    if-gez v2, :cond_a

    :goto_3
    sget-object v2, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    invoke-static {v2}, Lfi/firstbeat/ete/g;->a(Lfi/firstbeat/ete/b;)I

    move-result v2

    const/16 v6, 0x23

    if-gt v2, v6, :cond_6

    mul-int/lit8 v2, v2, 0x3a

    add-int/lit16 v2, v2, -0x240

    :goto_4
    if-gez v0, :cond_9

    if-ge v0, v3, :cond_1

    move v0, v3

    :cond_1
    mul-int/lit16 v0, v0, 0x258

    invoke-static {v0}, Lfi/firstbeat/ete/e;->a(I)I

    move-result v0

    sub-int v0, v2, v0

    :goto_5
    if-ge v1, v0, :cond_2

    move v1, v0

    :cond_2
    iput v1, p1, Lfi/firstbeat/ete/ETEresults;->ETEresourceRecovery:I

    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v0, v0, Lfi/firstbeat/ete/b;->J:I

    iput v0, p1, Lfi/firstbeat/ete/ETEresults;->ETEspeed:I

    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v0, v0, Lfi/firstbeat/ete/b;->Q:I

    const/16 v1, 0xa

    if-le v0, v1, :cond_8

    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v0, v0, Lfi/firstbeat/ete/b;->w:I

    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v1, v1, Lfi/firstbeat/ete/b;->Q:I

    sget-object v2, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v2, v2, Lfi/firstbeat/ete/b;->l:I

    invoke-static {v1, v2}, Lfi/firstbeat/ete/g;->a(II)I

    move-result v1

    invoke-static {v0, v1}, Lfi/firstbeat/ete/e;->b(II)I

    move-result v0

    mul-int/lit8 v0, v0, 0x64

    invoke-static {v0}, Lfi/firstbeat/ete/e;->a(I)I

    move-result v0

    if-le v0, v4, :cond_3

    move v0, v4

    :cond_3
    :goto_6
    iput v0, p1, Lfi/firstbeat/ete/ETEresults;->ETEpercentAchieved:I

    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget-object v0, v0, Lfi/firstbeat/ete/b;->S:[I

    aget v0, v0, v5

    iput v0, p1, Lfi/firstbeat/ete/ETEresults;->ETEtimeToNextLevel:I

    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget-object v0, v0, Lfi/firstbeat/ete/b;->T:[I

    aget v0, v0, v5

    iput v0, p1, Lfi/firstbeat/ete/ETEresults;->ETEtimeToTarget:I

    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v0, v0, Lfi/firstbeat/ete/b;->V:I

    iput v0, p1, Lfi/firstbeat/ete/ETEresults;->ETEintensityControl:I

    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v0, v0, Lfi/firstbeat/ete/b;->W:I

    iput v0, p1, Lfi/firstbeat/ete/ETEresults;->ETEphraseNumber:I

    move v1, v5

    goto/16 :goto_0

    :cond_4
    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    invoke-static {v0, v1}, Lfi/firstbeat/ete/b;->a(Lfi/firstbeat/ete/b;I)V

    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iget v0, v0, Lfi/firstbeat/ete/b;->n:I

    iput v0, p1, Lfi/firstbeat/ete/ETEresults;->ETEcorrectedHr:I

    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iput v1, v0, Lfi/firstbeat/ete/b;->d:I

    goto/16 :goto_1

    :cond_5
    iput v1, p1, Lfi/firstbeat/ete/ETEresults;->ETEmaximalMETminutes:I

    goto/16 :goto_2

    :cond_6
    const/16 v6, 0x2d

    if-gt v2, v6, :cond_7

    mul-int/lit16 v2, v2, 0x90

    add-int/lit16 v2, v2, -0xe10

    goto :goto_4

    :cond_7
    mul-int/lit16 v2, v2, 0x120

    add-int/lit16 v2, v2, -0x2760

    goto :goto_4

    :cond_8
    move v0, v4

    goto :goto_6

    :cond_9
    move v0, v2

    goto :goto_5

    :cond_a
    move v1, v2

    goto/16 :goto_3

    :cond_b
    move v0, v1

    move v1, v2

    goto/16 :goto_3
.end method

.method public SetParameters(Lfi/firstbeat/ete/FBTvars;I)I
    .locals 8

    const/16 v7, 0x64

    const/16 v2, 0x46

    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v0, 0x0

    sget-object v1, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    if-nez v1, :cond_0

    move p2, v0

    :cond_0
    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iput v0, v1, Lfi/firstbeat/ete/b;->e:I

    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iput v0, v1, Lfi/firstbeat/ete/b;->r:I

    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iput v0, v1, Lfi/firstbeat/ete/b;->s:I

    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iput v0, v1, Lfi/firstbeat/ete/b;->t:I

    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iput v0, v1, Lfi/firstbeat/ete/b;->u:I

    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iput v0, v1, Lfi/firstbeat/ete/b;->v:I

    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iput v0, v1, Lfi/firstbeat/ete/b;->w:I

    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iput v0, v1, Lfi/firstbeat/ete/b;->y:I

    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iput v0, v1, Lfi/firstbeat/ete/b;->M:I

    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iput v0, v1, Lfi/firstbeat/ete/b;->F:I

    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iput v0, v1, Lfi/firstbeat/ete/b;->G:I

    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iput v0, v1, Lfi/firstbeat/ete/b;->E:I

    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    const/16 v5, 0xa

    iput v5, v1, Lfi/firstbeat/ete/b;->Q:I

    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    const/16 v5, 0x1e

    iput v5, v1, Lfi/firstbeat/ete/b;->R:I

    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iget-object v1, v1, Lfi/firstbeat/ete/b;->S:[I

    aput v3, v1, v0

    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iget-object v1, v1, Lfi/firstbeat/ete/b;->S:[I

    aput v3, v1, v4

    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iget-object v1, v1, Lfi/firstbeat/ete/b;->T:[I

    aput v3, v1, v0

    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iget-object v1, v1, Lfi/firstbeat/ete/b;->T:[I

    aput v3, v1, v4

    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iput v0, v1, Lfi/firstbeat/ete/b;->U:I

    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iput v0, v1, Lfi/firstbeat/ete/b;->V:I

    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iput v0, v1, Lfi/firstbeat/ete/b;->W:I

    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    const/16 v5, 0xa

    iput v5, v1, Lfi/firstbeat/ete/b;->Y:I

    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    const/16 v5, 0xa

    iput v5, v1, Lfi/firstbeat/ete/b;->X:I

    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iput v0, v1, Lfi/firstbeat/ete/b;->N:I

    if-nez p2, :cond_2

    move v1, v0

    :goto_0
    const/16 v5, 0x20

    if-ge v1, v5, :cond_1

    sget-object v5, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iget-object v5, v5, Lfi/firstbeat/ete/b;->b:[I

    aput v0, v5, v1

    sget-object v5, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iget-object v5, v5, Lfi/firstbeat/ete/b;->a:[I

    aput v0, v5, v1

    sget-object v5, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iget-object v5, v5, Lfi/firstbeat/ete/b;->c:[I

    aput v0, v5, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iput v0, v1, Lfi/firstbeat/ete/b;->d:I

    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    const/16 v5, 0x3c

    iput v5, v1, Lfi/firstbeat/ete/b;->o:I

    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iput v0, v1, Lfi/firstbeat/ete/b;->p:I

    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iput v0, v1, Lfi/firstbeat/ete/b;->g:I

    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    const/16 v5, 0x4ccd

    iput v5, v1, Lfi/firstbeat/ete/b;->q:I

    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iput v0, v1, Lfi/firstbeat/ete/b;->m:I

    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iput v0, v1, Lfi/firstbeat/ete/b;->n:I

    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iput v0, v1, Lfi/firstbeat/ete/b;->C:I

    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iput v0, v1, Lfi/firstbeat/ete/b;->D:I

    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iput v0, v1, Lfi/firstbeat/ete/b;->B:I

    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iput v0, v1, Lfi/firstbeat/ete/b;->x:I

    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iput v0, v1, Lfi/firstbeat/ete/b;->J:I

    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iput v0, v1, Lfi/firstbeat/ete/b;->H:I

    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iput v0, v1, Lfi/firstbeat/ete/b;->K:I

    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iput v0, v1, Lfi/firstbeat/ete/b;->I:I

    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iput v0, v1, Lfi/firstbeat/ete/b;->L:I

    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iput v0, v1, Lfi/firstbeat/ete/b;->O:I

    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iput v0, v1, Lfi/firstbeat/ete/b;->P:I

    :cond_2
    iget v1, p1, Lfi/firstbeat/ete/FBTvars;->age:I

    const/16 v5, 0x8

    if-lt v1, v5, :cond_3

    iget v1, p1, Lfi/firstbeat/ete/FBTvars;->age:I

    const/16 v5, 0x6e

    if-gt v1, v5, :cond_3

    iget v1, p1, Lfi/firstbeat/ete/FBTvars;->height:I

    if-lt v1, v7, :cond_3

    iget v1, p1, Lfi/firstbeat/ete/FBTvars;->height:I

    const/16 v5, 0xfa

    if-gt v1, v5, :cond_3

    iget v1, p1, Lfi/firstbeat/ete/FBTvars;->weight:I

    const/16 v5, 0x23

    if-lt v1, v5, :cond_3

    iget v1, p1, Lfi/firstbeat/ete/FBTvars;->weight:I

    const/16 v5, 0xfa

    if-gt v1, v5, :cond_3

    iget v1, p1, Lfi/firstbeat/ete/FBTvars;->gender:I

    if-eqz v1, :cond_3

    iget v1, p1, Lfi/firstbeat/ete/FBTvars;->gender:I

    const/4 v5, 0x2

    if-le v1, v5, :cond_4

    :cond_3
    move v0, v3

    :goto_1
    return v0

    :cond_4
    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iget v5, p1, Lfi/firstbeat/ete/FBTvars;->weight:I

    iput v5, v1, Lfi/firstbeat/ete/b;->P:I

    iget v1, p1, Lfi/firstbeat/ete/FBTvars;->maxMET:I

    if-lez v1, :cond_7

    iget v1, p1, Lfi/firstbeat/ete/FBTvars;->maxMET:I

    const/high16 v5, 0x60000

    if-lt v1, v5, :cond_5

    iget v1, p1, Lfi/firstbeat/ete/FBTvars;->maxMET:I

    const v5, 0x16db6e

    if-le v1, v5, :cond_6

    :cond_5
    move v0, v3

    goto :goto_1

    :cond_6
    iget v1, p1, Lfi/firstbeat/ete/FBTvars;->gender:I

    if-ne v1, v4, :cond_8

    iget v1, p1, Lfi/firstbeat/ete/FBTvars;->maxMET:I

    const v5, 0x1b6dc

    add-int/2addr v1, v5

    :goto_2
    const v5, 0x116db7

    if-lt v1, v5, :cond_9

    const v5, 0x116db7

    sub-int/2addr v1, v5

    const v5, 0x12492

    invoke-static {v1, v5}, Lfi/firstbeat/ete/e;->b(II)I

    move-result v1

    shr-int/lit8 v1, v1, 0x10

    mul-int/lit8 v1, v1, 0x5

    add-int/lit8 v1, v1, 0x4b

    iput v1, p1, Lfi/firstbeat/ete/FBTvars;->AC:I

    iget v1, p1, Lfi/firstbeat/ete/FBTvars;->AC:I

    if-le v1, v7, :cond_7

    iput v7, p1, Lfi/firstbeat/ete/FBTvars;->AC:I

    :cond_7
    :goto_3
    iget v1, p1, Lfi/firstbeat/ete/FBTvars;->monthlyLoad:I

    if-lez v1, :cond_11

    move v1, v0

    :goto_4
    const/16 v5, 0xe

    if-ge v1, v5, :cond_10

    iget v5, p1, Lfi/firstbeat/ete/FBTvars;->monthlyLoad:I

    sget-object v6, Lfi/firstbeat/ete/ETEc;->c:[I

    aget v6, v6, v1

    if-lt v5, v6, :cond_10

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_8
    iget v1, p1, Lfi/firstbeat/ete/FBTvars;->maxMET:I

    goto :goto_2

    :cond_9
    iget v1, p1, Lfi/firstbeat/ete/FBTvars;->age:I

    const/16 v5, 0x41

    if-le v1, v5, :cond_d

    const/16 v1, 0x41

    :cond_a
    :goto_5
    shl-int/lit8 v1, v1, 0x10

    iget v5, p1, Lfi/firstbeat/ete/FBTvars;->gender:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_e

    iget v5, p1, Lfi/firstbeat/ete/FBTvars;->maxMET:I

    const/16 v6, 0x1380

    invoke-static {v6, v1}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v1

    add-int/2addr v1, v5

    const v5, 0x211f7

    sub-int/2addr v1, v5

    const/16 v5, -0x2495

    invoke-static {v1, v1}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v6

    invoke-static {v5, v6}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v5

    const v6, 0x50785

    invoke-static {v6, v1}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v1

    add-int/2addr v1, v5

    const v5, 0x24036e

    sub-int/2addr v1, v5

    :goto_6
    if-gez v1, :cond_b

    move v1, v0

    :cond_b
    const v5, 0x7000e

    if-le v1, v5, :cond_c

    const v1, 0x7000e

    :cond_c
    invoke-static {v1}, Lfi/firstbeat/ete/e;->a(I)I

    move-result v1

    mul-int/lit8 v1, v1, 0xa

    iput v1, p1, Lfi/firstbeat/ete/FBTvars;->AC:I

    goto :goto_3

    :cond_d
    const/16 v5, 0x19

    if-ge v1, v5, :cond_a

    const/16 v1, 0x19

    goto :goto_5

    :cond_e
    iget v5, p1, Lfi/firstbeat/ete/FBTvars;->maxMET:I

    const v6, 0xca0ea

    if-le v5, v6, :cond_f

    const v5, 0xca0ea

    const/16 v6, 0x10f3

    invoke-static {v6, v1}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v1

    add-int/2addr v1, v5

    const v5, 0x1b6c4

    sub-int/2addr v1, v5

    :goto_7
    const/16 v5, -0x42f9

    invoke-static {v1, v1}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v6

    invoke-static {v5, v6}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v5

    const v6, 0x741ca

    invoke-static {v6, v1}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v1

    add-int/2addr v1, v5

    const v5, 0x2a7da9

    sub-int/2addr v1, v5

    goto :goto_6

    :cond_f
    iget v5, p1, Lfi/firstbeat/ete/FBTvars;->maxMET:I

    const/16 v6, 0x10f3

    invoke-static {v6, v1}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v1

    add-int/2addr v1, v5

    const v5, 0x1b6c4

    sub-int/2addr v1, v5

    goto :goto_7

    :cond_10
    add-int/lit8 v1, v1, -0x1

    const/16 v5, 0x8

    if-ge v1, v5, :cond_15

    mul-int/lit8 v1, v1, 0xa

    :goto_8
    iget v5, p1, Lfi/firstbeat/ete/FBTvars;->AC:I

    if-ge v5, v1, :cond_11

    iput v1, p1, Lfi/firstbeat/ete/FBTvars;->AC:I

    :cond_11
    iget v1, p1, Lfi/firstbeat/ete/FBTvars;->AC:I

    if-gt v1, v2, :cond_16

    iget v1, p1, Lfi/firstbeat/ete/FBTvars;->AC:I

    rem-int/lit8 v1, v1, 0xa

    if-nez v1, :cond_16

    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iget v5, p1, Lfi/firstbeat/ete/FBTvars;->AC:I

    iput v5, v1, Lfi/firstbeat/ete/b;->l:I

    :goto_9
    iget v1, p1, Lfi/firstbeat/ete/FBTvars;->minHr:I

    if-nez v1, :cond_18

    const/16 v1, 0x3c

    iput v1, p1, Lfi/firstbeat/ete/FBTvars;->minHr:I

    :cond_12
    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iget v5, p1, Lfi/firstbeat/ete/FBTvars;->minHr:I

    iput v5, v1, Lfi/firstbeat/ete/b;->h:I

    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iput v0, v1, Lfi/firstbeat/ete/b;->j:I

    iget v1, p1, Lfi/firstbeat/ete/FBTvars;->maxHr:I

    if-nez v1, :cond_13

    const v1, 0x38000

    iget v5, p1, Lfi/firstbeat/ete/FBTvars;->age:I

    mul-int/lit16 v5, v5, 0x2c4

    sub-int/2addr v1, v5

    mul-int/lit8 v1, v1, 0x3c

    invoke-static {v1}, Lfi/firstbeat/ete/e;->a(I)I

    move-result v1

    iput v1, p1, Lfi/firstbeat/ete/FBTvars;->maxHr:I

    :cond_13
    iget v1, p1, Lfi/firstbeat/ete/FBTvars;->maxHr:I

    if-lt v1, v7, :cond_1a

    iget v1, p1, Lfi/firstbeat/ete/FBTvars;->maxHr:I

    const/16 v5, 0xf0

    if-gt v1, v5, :cond_1a

    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iget v3, p1, Lfi/firstbeat/ete/FBTvars;->maxHr:I

    iput v3, v1, Lfi/firstbeat/ete/b;->i:I

    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iput v0, v1, Lfi/firstbeat/ete/b;->k:I

    iget v0, p1, Lfi/firstbeat/ete/FBTvars;->maxMET:I

    if-nez v0, :cond_1d

    sget-object v0, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iget v0, v0, Lfi/firstbeat/ete/b;->l:I

    if-le v0, v2, :cond_1b

    move v0, v2

    :goto_a
    iget v1, p1, Lfi/firstbeat/ete/FBTvars;->weight:I

    shl-int/lit8 v1, v1, 0x10

    iget v2, p1, Lfi/firstbeat/ete/FBTvars;->height:I

    shl-int/lit8 v2, v2, 0x10

    div-int/lit8 v2, v2, 0x64

    iget v3, p1, Lfi/firstbeat/ete/FBTvars;->height:I

    shl-int/lit8 v3, v3, 0x10

    div-int/lit8 v3, v3, 0x64

    invoke-static {v2, v3}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v2

    invoke-static {v1, v2}, Lfi/firstbeat/ete/e;->b(II)I

    move-result v1

    const v2, 0x385cee

    mul-int/lit16 v0, v0, 0x312d

    add-int/2addr v0, v2

    iget v2, p1, Lfi/firstbeat/ete/FBTvars;->age:I

    mul-int/lit16 v2, v2, 0x6189

    sub-int/2addr v0, v2

    const v2, 0xc106

    invoke-static {v2, v1}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v1

    sub-int/2addr v0, v1

    const v1, 0xafcac

    iget v2, p1, Lfi/firstbeat/ete/FBTvars;->gender:I

    add-int/lit8 v2, v2, -0x1

    mul-int/2addr v1, v2

    add-int/2addr v0, v1

    const v1, 0x38000

    invoke-static {v0, v1}, Lfi/firstbeat/ete/e;->b(II)I

    move-result v0

    const/high16 v1, 0x60000

    if-ge v0, v1, :cond_1c

    const/high16 v0, 0x60000

    :cond_14
    :goto_b
    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iput v0, v1, Lfi/firstbeat/ete/b;->O:I

    sget-object v0, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iput v4, v0, Lfi/firstbeat/ete/b;->N:I

    :goto_c
    sget-object v0, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iget v1, p1, Lfi/firstbeat/ete/FBTvars;->resourceRecovery:I

    iput v1, v0, Lfi/firstbeat/ete/b;->Z:I

    sget-object v0, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    sput-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    move v0, v4

    goto/16 :goto_1

    :cond_15
    add-int/lit8 v1, v1, -0x7

    mul-int/lit8 v1, v1, 0x5

    add-int/lit8 v1, v1, 0x46

    goto/16 :goto_8

    :cond_16
    iget v1, p1, Lfi/firstbeat/ete/FBTvars;->AC:I

    if-le v1, v2, :cond_17

    iget v1, p1, Lfi/firstbeat/ete/FBTvars;->AC:I

    rem-int/lit8 v1, v1, 0x5

    if-nez v1, :cond_17

    sget-object v1, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iget v5, p1, Lfi/firstbeat/ete/FBTvars;->AC:I

    iput v5, v1, Lfi/firstbeat/ete/b;->l:I

    goto/16 :goto_9

    :cond_17
    move v0, v3

    goto/16 :goto_1

    :cond_18
    iget v1, p1, Lfi/firstbeat/ete/FBTvars;->minHr:I

    const/16 v5, 0x1e

    if-lt v1, v5, :cond_19

    iget v1, p1, Lfi/firstbeat/ete/FBTvars;->minHr:I

    const/16 v5, 0x50

    if-le v1, v5, :cond_12

    :cond_19
    move v0, v3

    goto/16 :goto_1

    :cond_1a
    move v0, v3

    goto/16 :goto_1

    :cond_1b
    iget v0, p1, Lfi/firstbeat/ete/FBTvars;->AC:I

    goto/16 :goto_a

    :cond_1c
    const v1, 0x16db6e

    if-le v0, v1, :cond_14

    const v0, 0x16db6e

    goto :goto_b

    :cond_1d
    sget-object v0, Lfi/firstbeat/ete/ETEc;->a:Lfi/firstbeat/ete/b;

    iget v1, p1, Lfi/firstbeat/ete/FBTvars;->maxMET:I

    iput v1, v0, Lfi/firstbeat/ete/b;->O:I

    goto :goto_c
.end method

.method public SetTargets(II)I
    .locals 1

    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    if-eqz v0, :cond_0

    const/16 v0, 0xa

    if-lt p1, v0, :cond_0

    const/16 v0, 0x32

    if-gt p1, v0, :cond_0

    const/16 v0, 0xb4

    if-gt p2, v0, :cond_0

    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iput p1, v0, Lfi/firstbeat/ete/b;->Q:I

    sget-object v0, Lfi/firstbeat/ete/ETEc;->b:Lfi/firstbeat/ete/b;

    iput p2, v0, Lfi/firstbeat/ete/b;->R:I

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSolutionIdentifier()Ljava/lang/String;
    .locals 1

    const-string v0, "FirstbeatFitnessEngine_ETE_Coach_E3sYhHLd"

    return-object v0
.end method
