.class public final Lfi/firstbeat/ete/e;
.super Ljava/lang/Object;


# static fields
.field private static final a:[I

.field private static final b:[I

.field private static final c:[I

.field private static final d:[I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x3

    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lfi/firstbeat/ete/e;->a:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lfi/firstbeat/ete/e;->b:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_2

    sput-object v0, Lfi/firstbeat/ete/e;->c:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_3

    sput-object v0, Lfi/firstbeat/ete/e;->d:[I

    return-void

    :array_0
    .array-data 4
        0x3
        0x5
        0x7
    .end array-data

    :array_1
    .array-data 4
        -0xa9c
        -0x24d
        -0x56
    .end array-data

    :array_2
    .array-data 4
        0x4672
        0x175d
        0x4bc
    .end array-data

    :array_3
    .array-data 4
        0x7f2b
        0xc2cc
        0xef12
    .end array-data
.end method

.method public static a(I)I
    .locals 1

    const v0, 0x8000

    add-int/2addr v0, p0

    shr-int/lit8 v0, v0, 0x10

    return v0
.end method

.method public static a(II)I
    .locals 4

    int-to-long v0, p0

    int-to-long v2, p1

    mul-long/2addr v0, v2

    const/16 v2, 0x10

    shr-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public static b(I)I
    .locals 0

    if-gez p0, :cond_0

    neg-int p0, p0

    :cond_0
    return p0
.end method

.method public static b(II)I
    .locals 4

    int-to-long v0, p0

    const/16 v2, 0x20

    shl-long/2addr v0, v2

    int-to-long v2, p1

    div-long/2addr v0, v2

    const/16 v2, 0x10

    shr-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public static c(I)I
    .locals 6

    const/high16 v1, 0x10000

    const/4 v5, 0x3

    if-gez p0, :cond_3

    neg-int v0, p0

    :goto_0
    const/4 v2, 0x0

    :goto_1
    if-ge v2, v5, :cond_0

    sget-object v3, Lfi/firstbeat/ete/e;->a:[I

    aget v3, v3, v2

    shl-int/lit8 v3, v3, 0x10

    if-ge v0, v3, :cond_4

    sget-object v3, Lfi/firstbeat/ete/e;->b:[I

    aget v3, v3, v2

    invoke-static {v0, v0}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v4

    invoke-static {v3, v4}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v3

    sget-object v4, Lfi/firstbeat/ete/e;->c:[I

    aget v4, v4, v2

    invoke-static {v4, v0}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v0

    add-int/2addr v0, v3

    sget-object v3, Lfi/firstbeat/ete/e;->d:[I

    aget v3, v3, v2

    add-int/2addr v0, v3

    :cond_0
    if-ne v2, v5, :cond_1

    const v2, 0x98000

    if-ge v0, v2, :cond_5

    const v0, 0xffbe

    :cond_1
    :goto_2
    if-gez p0, :cond_2

    sub-int v0, v1, v0

    :cond_2
    return v0

    :cond_3
    move v0, p0

    goto :goto_0

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_2
.end method
