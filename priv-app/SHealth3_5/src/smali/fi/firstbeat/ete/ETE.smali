.class public interface abstract Lfi/firstbeat/ete/ETE;
.super Ljava/lang/Object;


# virtual methods
.method public abstract Analyzer(III)I
.end method

.method public abstract AnalyzerHr(III)I
.end method

.method public abstract AnalyzerW(II)I
.end method

.method public abstract ETEVersion()Ljava/lang/String;
.end method

.method public abstract GetResult(Lfi/firstbeat/ete/ETEresults;)I
.end method

.method public abstract SetParameters(Lfi/firstbeat/ete/FBTvars;I)I
.end method

.method public abstract SetTargets(II)I
.end method

.method public abstract getSolutionIdentifier()Ljava/lang/String;
.end method
