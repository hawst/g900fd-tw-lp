.class public Lfi/firstbeat/ete/b;
.super Ljava/lang/Object;


# instance fields
.field public A:I

.field public B:I

.field public C:I

.field public D:I

.field public E:I

.field public F:I

.field public G:I

.field public H:I

.field public I:I

.field public J:I

.field public K:I

.field public L:I

.field public M:I

.field public N:I

.field public O:I

.field public P:I

.field public Q:I

.field public R:I

.field public S:[I

.field public T:[I

.field public U:I

.field public V:I

.field public W:I

.field public X:I

.field public Y:I

.field public Z:I

.field public a:[I

.field public b:[I

.field public c:[I

.field public d:I

.field public e:I

.field public f:[I

.field public g:I

.field public h:I

.field public i:I

.field public j:I

.field public k:I

.field public l:I

.field public m:I

.field public n:I

.field public o:I

.field public p:I

.field public q:I

.field public r:I

.field public s:I

.field public t:I

.field public u:I

.field public v:I

.field public w:I

.field public x:I

.field public y:I

.field public z:I


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x2

    const/16 v1, 0x20

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v0, v1, [I

    iput-object v0, p0, Lfi/firstbeat/ete/b;->a:[I

    new-array v0, v1, [I

    iput-object v0, p0, Lfi/firstbeat/ete/b;->b:[I

    new-array v0, v1, [I

    iput-object v0, p0, Lfi/firstbeat/ete/b;->c:[I

    const/4 v0, 0x4

    new-array v0, v0, [I

    iput-object v0, p0, Lfi/firstbeat/ete/b;->f:[I

    new-array v0, v2, [I

    iput-object v0, p0, Lfi/firstbeat/ete/b;->S:[I

    new-array v0, v2, [I

    iput-object v0, p0, Lfi/firstbeat/ete/b;->T:[I

    return-void
.end method

.method public static a(Lfi/firstbeat/ete/b;I)V
    .locals 8

    const/16 v7, 0xc8

    const/4 v6, 0x5

    const/4 v5, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lfi/firstbeat/ete/b;->a:[I

    iget-object v1, p0, Lfi/firstbeat/ete/b;->f:[I

    invoke-static {v0, v1, p0}, Lfi/firstbeat/ete/a;->a([I[ILfi/firstbeat/ete/b;)V

    const/16 v0, 0x1f

    move v1, v2

    move v3, v0

    move v0, v2

    :goto_0
    if-ltz v3, :cond_1

    const/16 v4, 0x1388

    if-ge v0, v4, :cond_1

    iget-object v4, p0, Lfi/firstbeat/ete/b;->a:[I

    aget v4, v4, v3

    if-eqz v4, :cond_1

    iget-object v4, p0, Lfi/firstbeat/ete/b;->f:[I

    invoke-static {v3, v4}, Lfi/firstbeat/ete/a;->a(I[I)I

    move-result v4

    if-nez v4, :cond_0

    add-int/lit8 v1, v1, 0x1

    iget-object v4, p0, Lfi/firstbeat/ete/b;->a:[I

    aget v4, v4, v3

    add-int/2addr v0, v4

    :cond_0
    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    :cond_1
    rsub-int/lit8 v4, v3, 0x20

    sub-int/2addr v4, v1

    mul-int/lit8 v4, v4, 0x64

    rsub-int/lit8 v3, v3, 0x20

    div-int v3, v4, v3

    iput v3, p0, Lfi/firstbeat/ete/b;->g:I

    iget v3, p0, Lfi/firstbeat/ete/b;->g:I

    const/16 v4, 0x32

    if-le v3, v4, :cond_4

    iget v0, p0, Lfi/firstbeat/ete/b;->o:I

    const/16 v1, 0xa0

    if-le v0, v1, :cond_3

    iget v0, p0, Lfi/firstbeat/ete/b;->o:I

    add-int/lit8 v0, v0, -0x4

    iput v0, p0, Lfi/firstbeat/ete/b;->o:I

    :cond_2
    :goto_1
    iget v0, p0, Lfi/firstbeat/ete/b;->o:I

    iput v0, p0, Lfi/firstbeat/ete/b;->n:I

    iput v2, p0, Lfi/firstbeat/ete/b;->p:I

    :goto_2
    return-void

    :cond_3
    iget v0, p0, Lfi/firstbeat/ete/b;->o:I

    const/16 v1, 0x64

    if-le v0, v1, :cond_2

    iget v0, p0, Lfi/firstbeat/ete/b;->o:I

    add-int/lit8 v0, v0, -0x2

    iput v0, p0, Lfi/firstbeat/ete/b;->o:I

    goto :goto_1

    :cond_4
    if-lez v0, :cond_5

    const v2, 0x1d4c0

    mul-int/2addr v1, v2

    add-int/2addr v1, v0

    mul-int/lit8 v0, v0, 0x2

    div-int v0, v1, v0

    iput v0, p0, Lfi/firstbeat/ete/b;->o:I

    :cond_5
    if-ne p1, v5, :cond_6

    iget v0, p0, Lfi/firstbeat/ete/b;->p:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lfi/firstbeat/ete/b;->p:I

    :cond_6
    iget v0, p0, Lfi/firstbeat/ete/b;->p:I

    if-le v0, v7, :cond_7

    iput v7, p0, Lfi/firstbeat/ete/b;->p:I

    :cond_7
    iget v0, p0, Lfi/firstbeat/ete/b;->o:I

    iget v1, p0, Lfi/firstbeat/ete/b;->h:I

    if-ge v0, v1, :cond_9

    if-ne p1, v5, :cond_9

    iget v0, p0, Lfi/firstbeat/ete/b;->p:I

    if-le v0, v6, :cond_9

    iget v0, p0, Lfi/firstbeat/ete/b;->o:I

    iget v1, p0, Lfi/firstbeat/ete/b;->j:I

    if-lt v0, v1, :cond_8

    iget v0, p0, Lfi/firstbeat/ete/b;->j:I

    if-nez v0, :cond_9

    :cond_8
    iget v0, p0, Lfi/firstbeat/ete/b;->o:I

    iput v0, p0, Lfi/firstbeat/ete/b;->j:I

    :cond_9
    iget v0, p0, Lfi/firstbeat/ete/b;->o:I

    iget v1, p0, Lfi/firstbeat/ete/b;->i:I

    if-le v0, v1, :cond_a

    if-ne p1, v5, :cond_a

    iget v0, p0, Lfi/firstbeat/ete/b;->p:I

    if-le v0, v6, :cond_a

    iget v0, p0, Lfi/firstbeat/ete/b;->o:I

    iget v1, p0, Lfi/firstbeat/ete/b;->k:I

    if-le v0, v1, :cond_a

    iget v0, p0, Lfi/firstbeat/ete/b;->o:I

    iput v0, p0, Lfi/firstbeat/ete/b;->k:I

    :cond_a
    iget v0, p0, Lfi/firstbeat/ete/b;->o:I

    iput v0, p0, Lfi/firstbeat/ete/b;->n:I

    goto :goto_2
.end method
