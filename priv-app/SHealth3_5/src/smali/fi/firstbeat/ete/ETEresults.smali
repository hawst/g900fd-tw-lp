.class public Lfi/firstbeat/ete/ETEresults;
.super Ljava/lang/Object;


# instance fields
.field public ETEartifactPercent:I

.field public ETEcorrectedHr:I

.field public ETEenergyExpenditure:I

.field public ETEenergyExpenditureCumulative:I

.field public ETEepoc:I

.field public ETEintensityControl:I

.field public ETEmaximalHr:I

.field public ETEmaximalMET:I

.field public ETEmaximalMETminutes:I

.field public ETEmaximalMETpercentage:I

.field public ETEminimalHr:I

.field public ETEpercentAchieved:I

.field public ETEphraseNumber:I

.field public ETEresourceRecovery:I

.field public ETEspeed:I

.field public ETEtimeToNextLevel:I

.field public ETEtimeToTarget:I

.field public ETEtrainingEffect:I

.field public ETEtrainingLoadPeak:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
