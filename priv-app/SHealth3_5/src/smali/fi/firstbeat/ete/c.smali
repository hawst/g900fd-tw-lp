.class public final Lfi/firstbeat/ete/c;
.super Ljava/lang/Object;


# static fields
.field private static final a:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x23

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lfi/firstbeat/ete/c;->a:[I

    return-void

    :array_0
    .array-data 4
        0x0
        0x1
        0xa
        0x35
        0xa7
        0x198
        0x34d
        0x61e
        0xa70
        0x10b7
        0x197a
        0x254e
        0x34d5
        0x48c5
        0x61e1
        0x80fc
        0xa6fa
        0xd4cc
        0x10b76
        0x14c09
        0x197a7
        0x1ef81
        0x254d8
        0x2c8fd
        0x34d4f
        0x3e33f
        0x48c4c
        0x54a06
        0x61e0a
        0x70a09
        0x80fbf
        0x930fa
        0xa6f99
        0xbcd87
        0xc4979
    .end array-data
.end method

.method private static a(I)I
    .locals 4

    const/16 v0, 0x63

    if-le p0, v0, :cond_0

    const v0, 0xc4979

    :goto_0
    return v0

    :cond_0
    div-int/lit8 v0, p0, 0x3

    sget-object v1, Lfi/firstbeat/ete/c;->a:[I

    aget v1, v1, v0

    sget-object v2, Lfi/firstbeat/ete/c;->a:[I

    add-int/lit8 v3, v0, 0x1

    aget v2, v2, v3

    sget-object v3, Lfi/firstbeat/ete/c;->a:[I

    aget v0, v3, v0

    sub-int v0, v2, v0

    rem-int/lit8 v2, p0, 0x3

    mul-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x3

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public static a(III)I
    .locals 4

    const/4 v2, 0x5

    if-eq p2, v2, :cond_0

    const/16 v0, 0x3c

    if-ne p2, v0, :cond_0

    const v0, 0xe8ba

    :goto_0
    invoke-static {v0, p1}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v1

    if-ne p2, v2, :cond_1

    const/16 v0, 0x1555

    invoke-static {p1, v0, p0}, Lfi/firstbeat/ete/c;->b(III)I

    move-result v0

    invoke-static {p0}, Lfi/firstbeat/ete/c;->b(I)I

    move-result v2

    sub-int v3, v1, p1

    invoke-static {v2, v3}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v2

    add-int/2addr v0, v2

    :goto_1
    if-le v0, v1, :cond_2

    :goto_2
    return v0

    :cond_0
    const v0, 0xfdf9

    goto :goto_0

    :cond_1
    const v0, 0xffff

    invoke-static {p1, v0, p0}, Lfi/firstbeat/ete/c;->b(III)I

    move-result v0

    invoke-static {p0}, Lfi/firstbeat/ete/c;->b(I)I

    move-result v2

    sub-int v3, v1, p1

    invoke-static {v2, v3}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v2

    add-int/2addr v0, v2

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method private static b(I)I
    .locals 4

    const v3, 0x8000

    const/high16 v0, 0x10000

    if-nez p0, :cond_0

    :goto_0
    return v0

    :cond_0
    shl-int/lit8 v1, p0, 0x10

    const/high16 v2, 0x640000

    invoke-static {v1, v2}, Lfi/firstbeat/ete/e;->b(II)I

    move-result v1

    if-ltz v1, :cond_1

    if-gt v1, v3, :cond_1

    invoke-static {v1, v1}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    goto :goto_0

    :cond_1
    if-le v1, v3, :cond_2

    if-gt v1, v0, :cond_2

    sub-int v2, v0, v1

    sub-int/2addr v0, v1

    invoke-static {v2, v0}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(III)I
    .locals 6

    const v5, 0xdeb8

    invoke-static {p2}, Lfi/firstbeat/ete/c;->a(I)I

    move-result v0

    const v1, 0x8546

    mul-int/2addr v1, p2

    mul-int v2, p2, v5

    div-int/lit8 v2, v2, 0x64

    add-int/lit8 v2, v2, 0x1

    if-le p0, v1, :cond_0

    sub-int v3, p0, v1

    invoke-static {v1, v2}, Lfi/firstbeat/ete/e;->b(II)I

    move-result v4

    invoke-static {v4, v0}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v4

    if-lt v3, v4, :cond_0

    invoke-static {v0, p1}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v0

    add-int/2addr v0, p0

    :goto_0
    return v0

    :cond_0
    invoke-static {p2}, Lfi/firstbeat/ete/c;->a(I)I

    move-result v3

    mul-int v4, p2, v5

    div-int/lit8 v4, v4, 0x64

    add-int/2addr v3, v4

    add-int/lit8 v3, v3, 0x1

    invoke-static {p0, v3}, Lfi/firstbeat/ete/e;->b(II)I

    move-result v4

    add-int/2addr v4, p1

    invoke-static {v1, v2}, Lfi/firstbeat/ete/e;->b(II)I

    move-result v2

    if-lt v4, v2, :cond_1

    invoke-static {v0, v4}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v0

    add-int/2addr v0, v1

    goto :goto_0

    :cond_1
    invoke-static {p1, v3}, Lfi/firstbeat/ete/e;->a(II)I

    move-result v0

    add-int/2addr v0, p0

    goto :goto_0
.end method
