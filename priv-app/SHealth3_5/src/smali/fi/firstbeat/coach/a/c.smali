.class public final Lfi/firstbeat/coach/a/c;
.super Lfi/firstbeat/coach/a/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lfi/firstbeat/coach/a/a;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(D)I
    .locals 1

    invoke-super {p0, p1, p2}, Lfi/firstbeat/coach/a/a;->a(D)I

    move-result v0

    return v0
.end method

.method public final bridge synthetic a(I)I
    .locals 1

    invoke-super {p0, p1}, Lfi/firstbeat/coach/a/a;->a(I)I

    move-result v0

    return v0
.end method

.method public final bridge synthetic a(II)I
    .locals 1

    invoke-super {p0, p1, p2}, Lfi/firstbeat/coach/a/a;->a(II)I

    move-result v0

    return v0
.end method

.method public final bridge synthetic b(D)D
    .locals 2

    invoke-super {p0, p1, p2}, Lfi/firstbeat/coach/a/a;->b(D)D

    move-result-wide v0

    return-wide v0
.end method

.method public final bridge synthetic b(I)I
    .locals 1

    invoke-super {p0, p1}, Lfi/firstbeat/coach/a/a;->b(I)I

    move-result v0

    return v0
.end method

.method public final bridge synthetic b(II)I
    .locals 1

    invoke-super {p0, p1, p2}, Lfi/firstbeat/coach/a/a;->b(II)I

    move-result v0

    return v0
.end method

.method public final c(II)I
    .locals 7

    const/16 v5, 0x445a

    const/16 v0, 0x28

    const/16 v1, 0x1e

    const/16 v2, 0x14

    const/16 v3, 0xa

    if-lt p1, v0, :cond_0

    const/16 v4, 0x32

    if-gt p1, v4, :cond_0

    const v1, 0x1d3b6

    invoke-super {p0, p2}, Lfi/firstbeat/coach/a/a;->a(I)I

    move-result v2

    invoke-super {p0, v1, v2}, Lfi/firstbeat/coach/a/a;->a(II)I

    move-result v1

    const v2, 0x92147

    add-int/2addr v1, v2

    const v2, 0x2e148

    invoke-super {p0, p2}, Lfi/firstbeat/coach/a/a;->a(I)I

    move-result v3

    invoke-super {p0, v2, v3}, Lfi/firstbeat/coach/a/a;->a(II)I

    move-result v2

    const v3, 0xe6666

    add-int/2addr v2, v3

    :goto_0
    sub-int v0, p1, v0

    invoke-super {p0, v0}, Lfi/firstbeat/coach/a/a;->a(I)I

    move-result v0

    div-int/lit8 v0, v0, 0xa

    sub-int/2addr v2, v1

    invoke-super {p0, v0, v2}, Lfi/firstbeat/coach/a/a;->a(II)I

    move-result v0

    add-int/2addr v0, v1

    invoke-super {p0, v0}, Lfi/firstbeat/coach/a/a;->b(I)I

    move-result v0

    :goto_1
    return v0

    :cond_0
    if-lt p1, v1, :cond_1

    const v0, 0xcccd

    invoke-super {p0, p2}, Lfi/firstbeat/coach/a/a;->a(I)I

    move-result v2

    invoke-super {p0, v0, v2}, Lfi/firstbeat/coach/a/a;->a(II)I

    move-result v0

    const/high16 v2, 0x40000

    add-int/2addr v0, v2

    const v2, 0x1d3b6

    invoke-super {p0, p2}, Lfi/firstbeat/coach/a/a;->a(I)I

    move-result v3

    invoke-super {p0, v2, v3}, Lfi/firstbeat/coach/a/a;->a(II)I

    move-result v2

    const v3, 0x92147

    add-int/2addr v2, v3

    move v6, v1

    move v1, v0

    move v0, v6

    goto :goto_0

    :cond_1
    if-lt p1, v2, :cond_2

    invoke-super {p0, p2}, Lfi/firstbeat/coach/a/a;->a(I)I

    move-result v0

    invoke-super {p0, v5, v0}, Lfi/firstbeat/coach/a/a;->a(II)I

    move-result v0

    const v1, 0x1547a

    add-int/2addr v0, v1

    const v1, 0xcccd

    invoke-super {p0, p2}, Lfi/firstbeat/coach/a/a;->a(I)I

    move-result v3

    invoke-super {p0, v1, v3}, Lfi/firstbeat/coach/a/a;->a(II)I

    move-result v1

    const/high16 v3, 0x40000

    add-int/2addr v1, v3

    move v6, v2

    move v2, v1

    move v1, v0

    move v0, v6

    goto :goto_0

    :cond_2
    if-lt p1, v3, :cond_3

    const/4 v0, 0x0

    invoke-super {p0, p2}, Lfi/firstbeat/coach/a/a;->a(I)I

    move-result v1

    invoke-super {p0, v5, v1}, Lfi/firstbeat/coach/a/a;->a(II)I

    move-result v1

    const v2, 0x1547a

    add-int/2addr v1, v2

    move v2, v1

    move v1, v0

    move v0, v3

    goto :goto_0

    :cond_3
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public final d(II)I
    .locals 4

    const/16 v0, 0x32

    move v1, v0

    :goto_0
    if-lez v1, :cond_2

    invoke-virtual {p0, v1, p2}, Lfi/firstbeat/coach/a/c;->c(II)I

    move-result v2

    if-gt v2, p1, :cond_1

    if-ne v1, v0, :cond_0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v1, 0xa

    invoke-virtual {p0, v0, p2}, Lfi/firstbeat/coach/a/c;->c(II)I

    move-result v0

    sub-int v3, p1, v2

    invoke-super {p0, v3}, Lfi/firstbeat/coach/a/a;->a(I)I

    move-result v3

    sub-int/2addr v0, v2

    div-int v0, v3, v0

    mul-int/lit8 v0, v0, 0xa

    invoke-super {p0, v0}, Lfi/firstbeat/coach/a/a;->b(I)I

    move-result v0

    add-int/2addr v0, v1

    goto :goto_1

    :cond_1
    add-int/lit8 v1, v1, -0xa

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final e(II)I
    .locals 1

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1, p2}, Lfi/firstbeat/coach/a/c;->d(II)I

    move-result v0

    goto :goto_0
.end method
