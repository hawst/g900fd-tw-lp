.class final Lfi/firstbeat/coach/a/d;
.super Lfi/firstbeat/coach/a/a;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lfi/firstbeat/coach/a/a;-><init>()V

    new-instance v0, Lfi/firstbeat/coach/a/g;

    invoke-direct {v0}, Lfi/firstbeat/coach/a/g;-><init>()V

    return-void
.end method


# virtual methods
.method public final c(II)I
    .locals 8

    const/16 v0, 0x28

    if-lt p1, v0, :cond_0

    const/16 v0, 0x32

    if-gt p1, v0, :cond_0

    const-wide v0, 0x3ffd3b645a1cac08L    # 1.827

    int-to-double v2, p2

    mul-double/2addr v0, v2

    const-wide v2, 0x4022428f5c28f5c3L    # 9.13

    add-double/2addr v2, v0

    const-wide v0, 0x40070a3d70a3d70aL    # 2.88

    int-to-double v4, p2

    mul-double/2addr v0, v4

    const-wide v4, 0x402ccccccccccccdL    # 14.4

    add-double/2addr v0, v4

    const/16 v4, 0x28

    :goto_0
    sub-int v4, p1, v4

    int-to-double v4, v4

    const-wide/high16 v6, 0x4024000000000000L    # 10.0

    div-double/2addr v4, v6

    sub-double/2addr v0, v2

    mul-double/2addr v0, v4

    add-double/2addr v0, v2

    invoke-virtual {p0, v0, v1}, Lfi/firstbeat/coach/a/d;->a(D)I

    move-result v0

    :goto_1
    return v0

    :cond_0
    const/16 v0, 0x1e

    if-lt p1, v0, :cond_1

    const-wide v0, 0x3fe999999999999aL    # 0.8

    int-to-double v2, p2

    mul-double/2addr v0, v2

    const-wide/high16 v2, 0x4010000000000000L    # 4.0

    add-double/2addr v2, v0

    const-wide v0, 0x3ffd3b645a1cac08L    # 1.827

    int-to-double v4, p2

    mul-double/2addr v0, v4

    const-wide v4, 0x4022428f5c28f5c3L    # 9.13

    add-double/2addr v0, v4

    const/16 v4, 0x1e

    goto :goto_0

    :cond_1
    const/16 v0, 0x14

    if-lt p1, v0, :cond_2

    const-wide v0, 0x3fd116872b020c4aL    # 0.267

    int-to-double v2, p2

    mul-double/2addr v0, v2

    const-wide v2, 0x3ff547ae147ae148L    # 1.33

    add-double/2addr v2, v0

    const-wide v0, 0x3fe999999999999aL    # 0.8

    int-to-double v4, p2

    mul-double/2addr v0, v4

    const-wide/high16 v4, 0x4010000000000000L    # 4.0

    add-double/2addr v0, v4

    const/16 v4, 0x14

    goto :goto_0

    :cond_2
    const/16 v0, 0xa

    if-lt p1, v0, :cond_3

    const-wide/16 v2, 0x0

    const-wide v0, 0x3fd116872b020c4aL    # 0.267

    int-to-double v4, p2

    mul-double/2addr v0, v4

    const-wide v4, 0x3ff547ae147ae148L    # 1.33

    add-double/2addr v0, v4

    const/16 v4, 0xa

    goto :goto_0

    :cond_3
    const/4 v0, -0x1

    goto :goto_1
.end method
