.class public final Lfi/firstbeat/coach/a/e;
.super Lfi/firstbeat/coach/a/a;


# static fields
.field private static final a:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0xe

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lfi/firstbeat/coach/a/e;->a:[I

    return-void

    :array_0
    .array-data 4
        0x0
        0x6
        0x14
        0x2a
        0x48
        0x58
        0x9c
        0xb4
        0xf8
        0x110
        0x12b
        0x154
        0x18e
        0x1bf
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lfi/firstbeat/coach/a/a;-><init>()V

    return-void
.end method

.method public static c(II)I
    .locals 4

    const/16 v3, 0xd

    const/16 v0, 0x46

    if-gt p1, v0, :cond_0

    div-int/lit8 v0, p1, 0xa

    :goto_0
    add-int/lit8 v1, p0, -0x1e

    div-int/lit8 v1, v1, 0x5

    add-int/2addr v0, v1

    if-gez v0, :cond_2

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    sget-object v0, Lfi/firstbeat/coach/a/e;->a:[I

    aget v0, v0, v1

    sget-object v2, Lfi/firstbeat/coach/a/e;->a:[I

    add-int/lit8 v1, v1, 0x1

    aget v1, v2, v1

    :goto_2
    sub-int/2addr v1, v0

    rem-int/lit8 v2, p0, 0x5

    mul-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x5

    add-int/2addr v0, v1

    return v0

    :cond_0
    add-int/lit8 v0, p1, -0x4b

    div-int/lit8 v0, v0, 0x5

    add-int/lit8 v0, v0, 0x8

    goto :goto_0

    :cond_1
    sget-object v0, Lfi/firstbeat/coach/a/e;->a:[I

    aget v0, v0, v3

    add-int/lit8 v2, v1, -0xd

    mul-int/lit8 v2, v2, 0x32

    add-int/2addr v0, v2

    sget-object v2, Lfi/firstbeat/coach/a/e;->a:[I

    aget v2, v2, v3

    add-int/lit8 v1, v1, -0xc

    mul-int/lit8 v1, v1, 0x32

    add-int/2addr v1, v2

    goto :goto_2

    :cond_2
    move v1, v0

    goto :goto_1
.end method


# virtual methods
.method public final bridge synthetic a(D)I
    .locals 1

    invoke-super {p0, p1, p2}, Lfi/firstbeat/coach/a/a;->a(D)I

    move-result v0

    return v0
.end method

.method public final bridge synthetic a(I)I
    .locals 1

    invoke-super {p0, p1}, Lfi/firstbeat/coach/a/a;->a(I)I

    move-result v0

    return v0
.end method

.method public final bridge synthetic a(II)I
    .locals 1

    invoke-super {p0, p1, p2}, Lfi/firstbeat/coach/a/a;->a(II)I

    move-result v0

    return v0
.end method

.method public final a(III)I
    .locals 2

    invoke-static {p2, p3}, Lfi/firstbeat/coach/a/e;->c(II)I

    move-result v0

    invoke-static {p1, p3}, Lfi/firstbeat/coach/a/e;->c(II)I

    move-result v1

    sub-int/2addr v0, v1

    if-gez v0, :cond_0

    const/4 v0, -0x1

    :cond_0
    return v0
.end method

.method public final bridge synthetic b(D)D
    .locals 2

    invoke-super {p0, p1, p2}, Lfi/firstbeat/coach/a/a;->b(D)D

    move-result-wide v0

    return-wide v0
.end method

.method public final bridge synthetic b(I)I
    .locals 1

    invoke-super {p0, p1}, Lfi/firstbeat/coach/a/a;->b(I)I

    move-result v0

    return v0
.end method

.method public final bridge synthetic b(II)I
    .locals 1

    invoke-super {p0, p1, p2}, Lfi/firstbeat/coach/a/a;->b(II)I

    move-result v0

    return v0
.end method

.method public final d(II)I
    .locals 5

    const/4 v4, 0x1

    if-gez p1, :cond_1

    const/4 v0, -0x1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/16 v1, 0xa

    const/16 v0, 0x32

    :goto_1
    sub-int v2, v0, v1

    if-eq v2, v4, :cond_2

    sub-int v2, v1, v0

    if-ne v2, v4, :cond_3

    :cond_2
    invoke-static {v0, p2}, Lfi/firstbeat/coach/a/e;->c(II)I

    move-result v2

    if-le v2, p1, :cond_0

    move v0, v1

    goto :goto_0

    :cond_3
    sub-int v2, v0, v1

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v1

    invoke-static {v2, p2}, Lfi/firstbeat/coach/a/e;->c(II)I

    move-result v3

    if-le v3, p1, :cond_4

    move v0, v2

    goto :goto_1

    :cond_4
    move v1, v2

    goto :goto_1
.end method
