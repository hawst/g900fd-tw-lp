.class Lfi/firstbeat/coach/a/a;
.super Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x3

    new-array v0, v2, [I

    aput v2, v0, v3

    const/4 v1, 0x5

    aput v1, v0, v4

    const/4 v1, 0x7

    aput v1, v0, v5

    new-array v0, v2, [I

    const/16 v1, -0xa9c

    aput v1, v0, v3

    const/16 v1, -0x24d

    aput v1, v0, v4

    const/16 v1, -0x56

    aput v1, v0, v5

    new-array v0, v2, [I

    const/16 v1, 0x4672

    aput v1, v0, v3

    const/16 v1, 0x175d

    aput v1, v0, v4

    const/16 v1, 0x4bc

    aput v1, v0, v5

    new-array v0, v2, [I

    const/16 v1, 0x7f2b

    aput v1, v0, v3

    const v1, 0xc2cc

    aput v1, v0, v4

    const v1, 0xef12

    aput v1, v0, v5

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(D)I
    .locals 2

    const-wide/high16 v0, 0x40f0000000000000L    # 65536.0

    mul-double/2addr v0, p1

    double-to-int v0, v0

    return v0
.end method

.method public a(I)I
    .locals 1

    shl-int/lit8 v0, p1, 0x10

    return v0
.end method

.method public a(II)I
    .locals 4

    int-to-long v0, p1

    int-to-long v2, p2

    mul-long/2addr v0, v2

    const/16 v2, 0x10

    shr-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public b(D)D
    .locals 2

    const-wide/high16 v0, 0x40f0000000000000L    # 65536.0

    div-double v0, p1, v0

    return-wide v0
.end method

.method public b(I)I
    .locals 1

    const v0, 0x8000

    add-int/2addr v0, p1

    shr-int/lit8 v0, v0, 0x10

    return v0
.end method

.method public b(II)I
    .locals 4

    int-to-long v0, p1

    const/16 v2, 0x20

    shl-long/2addr v0, v2

    int-to-long v2, p2

    div-long/2addr v0, v2

    const/16 v2, 0x10

    shr-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method
