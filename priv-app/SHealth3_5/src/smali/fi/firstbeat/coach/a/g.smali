.class final Lfi/firstbeat/coach/a/g;
.super Lfi/firstbeat/coach/a/a;


# static fields
.field private static final a:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x23

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lfi/firstbeat/coach/a/g;->a:[I

    return-void

    :array_0
    .array-data 4
        0x0
        0x1
        0xa
        0x35
        0xa7
        0x198
        0x34d
        0x61e
        0xa70
        0x10b7
        0x197a
        0x254e
        0x34d5
        0x48c5
        0x61e1
        0x80fc
        0xa6fa
        0xd4cc
        0x10b76
        0x14c09
        0x197a7
        0x1ef81
        0x254d8
        0x2c8fd
        0x34d4f
        0x3e33f
        0x48c4c
        0x54a06
        0x61e0a
        0x70a09
        0x80fbf
        0x930fa
        0xa6f99
        0xbcd87
        0xc4979
    .end array-data
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lfi/firstbeat/coach/a/a;-><init>()V

    return-void
.end method

.method private c(I)I
    .locals 4

    const/16 v0, 0x63

    if-le p1, v0, :cond_0

    const v0, 0xc4979

    :goto_0
    return v0

    :cond_0
    div-int/lit8 v0, p1, 0x3

    sget-object v1, Lfi/firstbeat/coach/a/g;->a:[I

    aget v1, v1, v0

    sget-object v2, Lfi/firstbeat/coach/a/g;->a:[I

    add-int/lit8 v3, v0, 0x1

    aget v2, v2, v3

    sget-object v3, Lfi/firstbeat/coach/a/g;->a:[I

    aget v0, v3, v0

    sub-int v0, v2, v0

    rem-int/lit8 v2, p1, 0x3

    mul-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x3

    add-int/2addr v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final c(II)D
    .locals 12

    const v11, 0xdeb8

    const/4 v1, 0x0

    const/4 v10, 0x1

    move v4, v1

    move v5, v1

    :goto_0
    if-ge v4, p2, :cond_6

    const v0, 0xe8ba

    invoke-virtual {p0, v0, v5}, Lfi/firstbeat/coach/a/g;->a(II)I

    move-result v2

    const v0, 0xffff

    invoke-direct {p0, p1}, Lfi/firstbeat/coach/a/g;->c(I)I

    move-result v3

    const v6, 0x8546

    mul-int/2addr v6, p1

    mul-int v7, p1, v11

    div-int/lit8 v7, v7, 0x64

    add-int/lit8 v7, v7, 0x1

    if-le v5, v6, :cond_0

    sub-int v8, v5, v6

    invoke-virtual {p0, v6, v7}, Lfi/firstbeat/coach/a/g;->b(II)I

    move-result v9

    invoke-virtual {p0, v9, v3}, Lfi/firstbeat/coach/a/g;->a(II)I

    move-result v9

    if-lt v8, v9, :cond_0

    invoke-virtual {p0, v3, v0}, Lfi/firstbeat/coach/a/g;->a(II)I

    move-result v0

    add-int/2addr v0, v5

    move v3, v0

    :goto_1
    if-nez p1, :cond_2

    invoke-virtual {p0, v10}, Lfi/firstbeat/coach/a/g;->a(I)I

    move-result v0

    :goto_2
    sub-int v5, v2, v5

    invoke-virtual {p0, v0, v5}, Lfi/firstbeat/coach/a/g;->a(II)I

    move-result v0

    add-int/2addr v0, v3

    if-le v0, v2, :cond_5

    :goto_3
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v5, v0

    goto :goto_0

    :cond_0
    invoke-direct {p0, p1}, Lfi/firstbeat/coach/a/g;->c(I)I

    move-result v8

    mul-int v9, p1, v11

    div-int/lit8 v9, v9, 0x64

    add-int/2addr v8, v9

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {p0, v5, v8}, Lfi/firstbeat/coach/a/g;->b(II)I

    move-result v9

    add-int/2addr v9, v0

    invoke-virtual {p0, v6, v7}, Lfi/firstbeat/coach/a/g;->b(II)I

    move-result v7

    if-lt v9, v7, :cond_1

    invoke-virtual {p0, v3, v9}, Lfi/firstbeat/coach/a/g;->a(II)I

    move-result v0

    add-int/2addr v0, v6

    move v3, v0

    goto :goto_1

    :cond_1
    invoke-virtual {p0, v0, v8}, Lfi/firstbeat/coach/a/g;->a(II)I

    move-result v0

    add-int/2addr v0, v5

    move v3, v0

    goto :goto_1

    :cond_2
    invoke-virtual {p0, p1}, Lfi/firstbeat/coach/a/g;->a(I)I

    move-result v0

    const/16 v6, 0x64

    invoke-virtual {p0, v6}, Lfi/firstbeat/coach/a/g;->a(I)I

    move-result v6

    invoke-virtual {p0, v0, v6}, Lfi/firstbeat/coach/a/g;->b(II)I

    move-result v0

    if-ltz v0, :cond_3

    const v6, 0x8000

    if-gt v0, v6, :cond_3

    invoke-virtual {p0, v10}, Lfi/firstbeat/coach/a/g;->a(I)I

    move-result v6

    invoke-virtual {p0, v0, v0}, Lfi/firstbeat/coach/a/g;->a(II)I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    sub-int v0, v6, v0

    goto :goto_2

    :cond_3
    const/16 v6, 0x4000

    if-le v0, v6, :cond_4

    invoke-virtual {p0, v10}, Lfi/firstbeat/coach/a/g;->a(I)I

    move-result v6

    if-gt v0, v6, :cond_4

    invoke-virtual {p0, v10}, Lfi/firstbeat/coach/a/g;->a(I)I

    move-result v6

    sub-int/2addr v6, v0

    invoke-virtual {p0, v10}, Lfi/firstbeat/coach/a/g;->a(I)I

    move-result v7

    sub-int v0, v7, v0

    invoke-virtual {p0, v6, v0}, Lfi/firstbeat/coach/a/g;->a(II)I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_2

    :cond_5
    move v0, v2

    goto :goto_3

    :cond_6
    int-to-double v0, v5

    invoke-virtual {p0, v0, v1}, Lfi/firstbeat/coach/a/g;->b(D)D

    move-result-wide v0

    return-wide v0
.end method
