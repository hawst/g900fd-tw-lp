.class public Lfi/firstbeat/coach/a/b;
.super Ljava/lang/Object;


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lfi/firstbeat/coach/a/b;->f:I

    return-void
.end method

.method private static a(D)D
    .locals 14

    const-wide/16 v0, 0x0

    cmpl-double v0, p0, v0

    if-gtz v0, :cond_1

    const-wide/high16 v0, 0x7ff8000000000000L    # NaN

    :cond_0
    return-wide v0

    :cond_1
    const-wide/16 v4, 0x0

    const/4 v0, 0x0

    :goto_0
    const-wide/16 v1, 0x0

    cmpl-double v1, p0, v1

    if-lez v1, :cond_2

    const-wide/high16 v1, 0x3ff0000000000000L    # 1.0

    cmpg-double v1, p0, v1

    if-gtz v1, :cond_2

    const-wide/high16 v1, 0x4000000000000000L    # 2.0

    mul-double/2addr p0, v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const-wide/high16 v1, 0x4000000000000000L    # 2.0

    div-double v1, p0, v1

    add-int/lit8 v6, v0, -0x1

    const-wide/high16 v7, 0x3ff0000000000000L    # 1.0

    sub-double v7, v1, v7

    const-wide/high16 v9, 0x3ff0000000000000L    # 1.0

    add-double v0, v1, v9

    div-double v2, v7, v0

    mul-double v7, v2, v2

    const-wide/16 v0, 0x1

    :goto_1
    const-wide/16 v9, 0x32

    cmp-long v9, v0, v9

    if-gez v9, :cond_3

    long-to-double v9, v0

    div-double v9, v2, v9

    add-double/2addr v4, v9

    mul-double/2addr v2, v7

    const-wide/16 v9, 0x2

    add-long/2addr v0, v9

    goto :goto_1

    :cond_3
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    mul-double v1, v4, v0

    const/4 v0, 0x0

    move v11, v0

    move-wide v12, v1

    move-wide v0, v12

    move v2, v11

    :goto_2
    if-ge v2, v6, :cond_0

    const-wide v3, -0x4019d1bd0105c611L    # -0.6931471805599453

    add-double/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move-wide v0, v3

    goto :goto_2
.end method

.method public static a(DD)D
    .locals 13

    const-wide/16 v0, 0x0

    cmpl-double v0, p2, v0

    if-nez v0, :cond_0

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    cmpl-double v0, p2, v0

    if-nez v0, :cond_1

    const-wide v0, 0x4005bf0a8b145769L    # Math.E

    goto :goto_0

    :cond_1
    invoke-static/range {p2 .. p3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-long v5, v0

    long-to-double v0, v5

    cmpl-double v0, p2, v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_7

    const/4 v0, 0x0

    const-wide/16 v1, 0x0

    cmpg-double v1, p2, v1

    if-gez v1, :cond_2

    const/4 v0, 0x1

    :cond_2
    const-wide v3, 0x4005bf0a8b145769L    # Math.E

    const-wide/16 v1, 0x1

    move-wide v7, v1

    move-wide v1, v3

    :goto_2
    if-eqz v0, :cond_4

    neg-long v3, v5

    :goto_3
    cmp-long v3, v7, v3

    if-gez v3, :cond_5

    const-wide v3, 0x4005bf0a8b145769L    # Math.E

    mul-double/2addr v3, v1

    const-wide/16 v1, 0x1

    add-long/2addr v1, v7

    move-wide v7, v1

    move-wide v1, v3

    goto :goto_2

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    move-wide v3, v5

    goto :goto_3

    :cond_5
    if-eqz v0, :cond_6

    const-wide/high16 v3, 0x3ff0000000000000L    # 1.0

    div-double v0, v3, v1

    goto :goto_0

    :cond_6
    move-wide v0, v1

    goto :goto_0

    :cond_7
    const-wide v0, 0x4005bf0a8b145769L    # Math.E

    const-wide/16 v2, 0x0

    cmpl-double v2, v0, v2

    if-gtz v2, :cond_8

    const-wide/high16 v0, 0x7ff8000000000000L    # NaN

    :goto_4
    mul-double/2addr v0, p2

    const-wide/16 v2, 0x0

    cmpl-double v2, v0, v2

    if-nez v2, :cond_b

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    goto :goto_0

    :cond_8
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpl-double v2, v0, v2

    if-nez v2, :cond_9

    const-wide/16 v0, 0x0

    goto :goto_4

    :cond_9
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpl-double v2, v0, v2

    if-lez v2, :cond_a

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    div-double v0, v2, v0

    invoke-static {v0, v1}, Lfi/firstbeat/coach/a/b;->a(D)D

    move-result-wide v0

    neg-double v0, v0

    goto :goto_4

    :cond_a
    invoke-static {v0, v1}, Lfi/firstbeat/coach/a/b;->a(D)D

    move-result-wide v0

    goto :goto_4

    :cond_b
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v2, 0x0

    cmpg-double v2, v0, v2

    if-gez v2, :cond_d

    const/4 v2, 0x1

    move v10, v2

    :goto_5
    if-eqz v10, :cond_c

    neg-double v0, v0

    :cond_c
    const-wide/16 v2, 0x2

    move-wide v6, v0

    move-wide v11, v2

    move-wide v2, v4

    move-wide v4, v11

    :goto_6
    const-wide/16 v8, 0x32

    cmp-long v8, v4, v8

    if-gez v8, :cond_e

    add-double v8, v2, v6

    mul-double v2, v6, v0

    long-to-double v6, v4

    div-double v6, v2, v6

    const-wide/16 v2, 0x1

    add-long/2addr v2, v4

    move-wide v4, v2

    move-wide v2, v8

    goto :goto_6

    :cond_d
    const/4 v2, 0x0

    move v10, v2

    goto :goto_5

    :cond_e
    if-eqz v10, :cond_f

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    div-double/2addr v0, v2

    goto/16 :goto_0

    :cond_f
    move-wide v0, v2

    goto/16 :goto_0
.end method
