.class public final Lfi/firstbeat/coach/a/h;
.super Ljava/lang/Object;


# instance fields
.field private a:Lfi/firstbeat/coach/a/d;

.field private b:Lfi/firstbeat/coach/a/g;

.field private c:Lfi/firstbeat/coach/a/a;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x6

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/16 v2, 0x25

    aput v2, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0x31

    aput v2, v0, v1

    const/4 v1, 0x2

    const/16 v2, 0x3d

    aput v2, v0, v1

    const/4 v1, 0x3

    const/16 v2, 0x4a

    aput v2, v0, v1

    const/4 v1, 0x4

    const/16 v2, 0x5a

    aput v2, v0, v1

    const/4 v1, 0x5

    const/16 v2, 0x65

    aput v2, v0, v1

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lfi/firstbeat/coach/a/d;

    invoke-direct {v0}, Lfi/firstbeat/coach/a/d;-><init>()V

    iput-object v0, p0, Lfi/firstbeat/coach/a/h;->a:Lfi/firstbeat/coach/a/d;

    new-instance v0, Lfi/firstbeat/coach/a/g;

    invoke-direct {v0}, Lfi/firstbeat/coach/a/g;-><init>()V

    iput-object v0, p0, Lfi/firstbeat/coach/a/h;->b:Lfi/firstbeat/coach/a/g;

    new-instance v0, Lfi/firstbeat/coach/a/a;

    invoke-direct {v0}, Lfi/firstbeat/coach/a/a;-><init>()V

    iput-object v0, p0, Lfi/firstbeat/coach/a/h;->c:Lfi/firstbeat/coach/a/a;

    return-void
.end method

.method public static a(IIIII)D
    .locals 8

    const/16 v0, 0x46

    if-le p3, v0, :cond_0

    const/16 p3, 0x46

    :cond_0
    int-to-double v0, p2

    int-to-double v2, p1

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    div-double/2addr v2, v4

    int-to-double v4, p1

    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    div-double/2addr v4, v6

    mul-double/2addr v2, v4

    div-double/2addr v0, v2

    const-wide v2, 0x404c2e76c8b43958L    # 56.363

    const-wide v4, 0x3ffebc6a7ef9db23L    # 1.921

    int-to-double v6, p3

    mul-double/2addr v4, v6

    const-wide/high16 v6, 0x4024000000000000L    # 10.0

    div-double/2addr v4, v6

    add-double/2addr v2, v4

    const-wide v4, 0x3fd8624dd2f1a9fcL    # 0.381

    int-to-double v6, p0

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    const-wide v4, 0x3fe820c49ba5e354L    # 0.754

    mul-double/2addr v0, v4

    sub-double v0, v2, v0

    const-wide v2, 0x4025f95810624dd3L    # 10.987

    add-int/lit8 v4, p4, -0x1

    int-to-double v4, v4

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    const-wide/high16 v2, 0x4035000000000000L    # 21.0

    cmpg-double v2, v0, v2

    if-gez v2, :cond_2

    const-wide/high16 v0, 0x4035000000000000L    # 21.0

    :cond_1
    :goto_0
    return-wide v0

    :cond_2
    const-wide/high16 v2, 0x4054000000000000L    # 80.0

    cmpl-double v2, v0, v2

    if-lez v2, :cond_1

    const-wide/high16 v0, 0x4054000000000000L    # 80.0

    goto :goto_0
.end method

.method public static a(DID)I
    .locals 8

    const/4 v0, 0x0

    const-wide/16 v1, 0x0

    cmpl-double v1, p3, v1

    if-lez v1, :cond_0

    if-lez p2, :cond_0

    int-to-double v0, p2

    const-wide/high16 v2, 0x404e000000000000L    # 60.0

    div-double/2addr v0, v2

    div-double v0, p3, v0

    const-wide/high16 v2, 0x4020000000000000L    # 8.0

    cmpl-double v2, v0, v2

    if-lez v2, :cond_3

    const-wide/high16 v2, 0x400c000000000000L    # 3.5

    mul-double/2addr v0, v2

    div-double/2addr v0, p0

    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    mul-double/2addr v0, v2

    double-to-int v0, v0

    :cond_0
    :goto_0
    const/16 v1, 0x64

    if-le v0, v1, :cond_1

    const/16 v0, 0x64

    :cond_1
    if-gez v0, :cond_2

    const/4 v0, 0x0

    :cond_2
    return v0

    :cond_3
    const-wide/high16 v2, 0x400c000000000000L    # 3.5

    const-wide v4, 0x4005bf0a8b145769L    # Math.E

    const-wide v6, 0x3fd0a3d70a3d70a4L    # 0.26

    mul-double/2addr v0, v6

    invoke-static {v4, v5, v0, v1}, Lfi/firstbeat/coach/a/b;->a(DD)D

    move-result-wide v0

    mul-double/2addr v0, v2

    div-double/2addr v0, p0

    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    mul-double/2addr v0, v2

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    add-double/2addr v0, v2

    double-to-int v0, v0

    goto :goto_0
.end method


# virtual methods
.method public final a(II)D
    .locals 2

    const-wide/16 v0, 0x0

    if-lez p2, :cond_0

    iget-object v0, p0, Lfi/firstbeat/coach/a/h;->b:Lfi/firstbeat/coach/a/g;

    invoke-virtual {v0, p2, p1}, Lfi/firstbeat/coach/a/g;->c(II)D

    move-result-wide v0

    :cond_0
    return-wide v0
.end method

.method public final a(DIII)[D
    .locals 16

    const/4 v3, 0x2

    new-array v3, v3, [D

    fill-array-data v3, :array_0

    const-wide/16 v6, 0x0

    move/from16 v0, p5

    int-to-double v4, v0

    const-wide/high16 v8, 0x404e000000000000L    # 60.0

    div-double/2addr v4, v8

    const-wide/high16 v8, 0x4039000000000000L    # 25.0

    mul-double/2addr v4, v8

    move-object/from16 v0, p0

    iget-object v8, v0, Lfi/firstbeat/coach/a/h;->c:Lfi/firstbeat/coach/a/a;

    move-object/from16 v0, p0

    iget-object v9, v0, Lfi/firstbeat/coach/a/h;->a:Lfi/firstbeat/coach/a/d;

    move/from16 v0, p3

    move/from16 v1, p4

    invoke-virtual {v9, v0, v1}, Lfi/firstbeat/coach/a/d;->c(II)I

    move-result v9

    int-to-double v9, v9

    invoke-virtual {v8, v9, v10}, Lfi/firstbeat/coach/a/a;->b(D)D

    move-result-wide v10

    const/16 v8, 0xa

    move/from16 v0, p3

    if-gt v0, v8, :cond_4

    :goto_0
    return-object v3

    :cond_0
    move-wide/from16 v0, p1

    move/from16 v2, p5

    invoke-static {v0, v1, v2, v6, v7}, Lfi/firstbeat/coach/a/h;->a(DID)I

    move-result v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lfi/firstbeat/coach/a/h;->b:Lfi/firstbeat/coach/a/g;

    move/from16 v0, p5

    invoke-virtual {v13, v12, v0}, Lfi/firstbeat/coach/a/g;->c(II)D

    move-result-wide v12

    cmpl-double v12, v12, v10

    if-ltz v12, :cond_2

    move-wide v4, v6

    :goto_1
    cmpg-double v6, v8, v4

    if-gtz v6, :cond_3

    add-double v6, v4, v8

    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    div-double/2addr v6, v12

    sub-double v12, v4, v8

    const-wide v14, 0x3fb999999999999aL    # 0.1

    cmpg-double v12, v12, v14

    if-gtz v12, :cond_0

    move-wide/from16 v0, p1

    move/from16 v2, p5

    invoke-static {v0, v1, v2, v4, v5}, Lfi/firstbeat/coach/a/h;->a(DID)I

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lfi/firstbeat/coach/a/h;->b:Lfi/firstbeat/coach/a/g;

    move/from16 v0, p5

    invoke-virtual {v7, v6, v0}, Lfi/firstbeat/coach/a/g;->c(II)D

    move-result-wide v7

    cmpl-double v7, v7, v10

    if-ltz v7, :cond_1

    const/4 v7, 0x0

    aput-wide v4, v3, v7

    const/4 v4, 0x1

    int-to-double v5, v6

    aput-wide v5, v3, v4

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    const-wide/high16 v5, -0x4010000000000000L    # -1.0

    aput-wide v5, v3, v4

    const/4 v4, 0x1

    const-wide/high16 v5, -0x4010000000000000L    # -1.0

    aput-wide v5, v3, v4

    goto :goto_0

    :cond_2
    move-wide v8, v6

    goto :goto_1

    :cond_3
    const/4 v4, 0x0

    const-wide/high16 v5, -0x4010000000000000L    # -1.0

    aput-wide v5, v3, v4

    const/4 v4, 0x1

    const-wide/high16 v5, -0x4010000000000000L    # -1.0

    aput-wide v5, v3, v4

    goto :goto_0

    :cond_4
    move-wide v8, v6

    goto :goto_1

    :array_0
    .array-data 8
        0x0
        0x0
    .end array-data
.end method
