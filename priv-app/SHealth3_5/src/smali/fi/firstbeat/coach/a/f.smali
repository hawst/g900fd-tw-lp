.class public final Lfi/firstbeat/coach/a/f;
.super Ljava/lang/Object;


# static fields
.field private static c:S

.field private static d:S

.field private static e:S

.field private static f:S

.field private static g:S

.field private static h:S

.field private static i:S

.field private static j:S

.field private static k:S

.field private static l:S

.field private static m:[[S

.field private static n:[[S

.field private static final o:[I


# instance fields
.field private a:Lfi/firstbeat/coach/a/c;

.field private b:Lfi/firstbeat/coach/a/e;

.field private p:I

.field private q:I

.field private r:[I

.field private s:I

.field private t:I

.field private u:I

.field private v:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x3

    const/4 v6, 0x1

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x7

    sput-short v4, Lfi/firstbeat/coach/a/f;->c:S

    sput-short v6, Lfi/firstbeat/coach/a/f;->d:S

    sput-short v5, Lfi/firstbeat/coach/a/f;->e:S

    sput-short v7, Lfi/firstbeat/coach/a/f;->f:S

    const/4 v0, 0x4

    sput-short v0, Lfi/firstbeat/coach/a/f;->g:S

    const/4 v0, 0x5

    sput-short v0, Lfi/firstbeat/coach/a/f;->h:S

    const/4 v0, 0x6

    sput-short v0, Lfi/firstbeat/coach/a/f;->i:S

    sput-short v3, Lfi/firstbeat/coach/a/f;->j:S

    sput-short v4, Lfi/firstbeat/coach/a/f;->k:S

    sput-short v5, Lfi/firstbeat/coach/a/f;->l:S

    const/16 v0, 0x8

    new-array v0, v0, [[S

    new-array v1, v3, [S

    fill-array-data v1, :array_0

    aput-object v1, v0, v4

    new-array v1, v3, [S

    fill-array-data v1, :array_1

    aput-object v1, v0, v6

    new-array v1, v3, [S

    fill-array-data v1, :array_2

    aput-object v1, v0, v5

    new-array v1, v3, [S

    fill-array-data v1, :array_3

    aput-object v1, v0, v7

    const/4 v1, 0x4

    new-array v2, v3, [S

    fill-array-data v2, :array_4

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-array v2, v3, [S

    fill-array-data v2, :array_5

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v3, [S

    fill-array-data v2, :array_6

    aput-object v2, v0, v1

    new-array v1, v3, [S

    fill-array-data v1, :array_7

    aput-object v1, v0, v3

    sput-object v0, Lfi/firstbeat/coach/a/f;->m:[[S

    const/16 v0, 0x8

    new-array v0, v0, [[S

    new-array v1, v3, [S

    fill-array-data v1, :array_8

    aput-object v1, v0, v4

    new-array v1, v3, [S

    fill-array-data v1, :array_9

    aput-object v1, v0, v6

    new-array v1, v3, [S

    fill-array-data v1, :array_a

    aput-object v1, v0, v5

    new-array v1, v3, [S

    fill-array-data v1, :array_b

    aput-object v1, v0, v7

    const/4 v1, 0x4

    new-array v2, v3, [S

    fill-array-data v2, :array_c

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-array v2, v3, [S

    fill-array-data v2, :array_d

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v3, [S

    fill-array-data v2, :array_e

    aput-object v2, v0, v1

    new-array v1, v3, [S

    fill-array-data v1, :array_f

    aput-object v1, v0, v3

    sput-object v0, Lfi/firstbeat/coach/a/f;->n:[[S

    const/16 v0, 0xe

    new-array v0, v0, [I

    fill-array-data v0, :array_10

    sput-object v0, Lfi/firstbeat/coach/a/f;->o:[I

    return-void

    :array_0
    .array-data 2
        0x14s
        0x0s
        0x14s
        0x0s
        0x1es
        0x0s
        0x0s
    .end array-data

    nop

    :array_1
    .array-data 2
        0x14s
        0x0s
        0x14s
        0x0s
        0x1es
        0x0s
        0x0s
    .end array-data

    nop

    :array_2
    .array-data 2
        0x14s
        0x1es
        0x0s
        0xas
        0x0s
        0x1es
        0x0s
    .end array-data

    nop

    :array_3
    .array-data 2
        0x14s
        0x1es
        0x0s
        0xas
        0x0s
        0x1es
        0x0s
    .end array-data

    nop

    :array_4
    .array-data 2
        0x1es
        0x14s
        0x0s
        0x14s
        0x1es
        0x0s
        0xas
    .end array-data

    nop

    :array_5
    .array-data 2
        0x28s
        0x14s
        0x0s
        0x28s
        0xas
        0x0s
        0x1es
    .end array-data

    nop

    :array_6
    .array-data 2
        0x0s
        0x14s
        0x0s
        0xas
        0x14s
        0x0s
        0xas
    .end array-data

    nop

    :array_7
    .array-data 2
        0x1es
        0x14s
        0xas
        0x14s
        0x0s
        0x1es
        0xas
    .end array-data

    nop

    :array_8
    .array-data 2
        0x1es
        0x0s
        0x19s
        0x0s
        0x19s
        0x0s
        0x0s
    .end array-data

    nop

    :array_9
    .array-data 2
        0x1es
        0x0s
        0x23s
        0x0s
        0x19s
        0x0s
        0x0s
    .end array-data

    nop

    :array_a
    .array-data 2
        0x2ds
        0x3cs
        0x0s
        0x1es
        0x0s
        0x2ds
        0x0s
    .end array-data

    nop

    :array_b
    .array-data 2
        0x4bs
        0x3cs
        0x0s
        0x2ds
        0x0s
        0x2ds
        0x0s
    .end array-data

    nop

    :array_c
    .array-data 2
        0x23s
        0x55s
        0x0s
        0x4bs
        0x3cs
        0x0s
        0x3cs
    .end array-data

    nop

    :array_d
    .array-data 2
        0x3cs
        0x2ds
        0x0s
        0x32s
        0x4bs
        0x0s
        0x3cs
    .end array-data

    nop

    :array_e
    .array-data 2
        0x0s
        0x4bs
        0x0s
        0x50s
        0x46s
        0x0s
        0x3cs
    .end array-data

    nop

    :array_f
    .array-data 2
        0x32s
        0x5as
        0x2ds
        0x5as
        0x0s
        0x46s
        0x2ds
    .end array-data

    nop

    :array_10
    .array-data 4
        0x0
        0x18
        0x50
        0xa8
        0x120
        0x160
        0x270
        0x2d0
        0x3e0
        0x440
        0x4ac
        0x550
        0x638
        0x6fc
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lfi/firstbeat/coach/a/c;

    invoke-direct {v0}, Lfi/firstbeat/coach/a/c;-><init>()V

    iput-object v0, p0, Lfi/firstbeat/coach/a/f;->a:Lfi/firstbeat/coach/a/c;

    new-instance v0, Lfi/firstbeat/coach/a/e;

    invoke-direct {v0}, Lfi/firstbeat/coach/a/e;-><init>()V

    iput-object v0, p0, Lfi/firstbeat/coach/a/f;->b:Lfi/firstbeat/coach/a/e;

    iput v1, p0, Lfi/firstbeat/coach/a/f;->p:I

    iput v1, p0, Lfi/firstbeat/coach/a/f;->q:I

    const/16 v0, 0x1c

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lfi/firstbeat/coach/a/f;->r:[I

    iput v1, p0, Lfi/firstbeat/coach/a/f;->s:I

    iput v1, p0, Lfi/firstbeat/coach/a/f;->t:I

    iput v1, p0, Lfi/firstbeat/coach/a/f;->u:I

    iput v1, p0, Lfi/firstbeat/coach/a/f;->v:I

    return-void

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method

.method public static a(Ljava/util/GregorianCalendar;)I
    .locals 4

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v1

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    const/4 v2, 0x5

    invoke-virtual {p0, v2}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v2

    const/4 v3, 0x3

    if-ge v0, v3, :cond_0

    add-int/lit8 v0, v0, 0xc

    add-int/lit8 v1, v1, -0x1

    :cond_0
    const v3, -0xa5c3d

    add-int/2addr v2, v3

    mul-int/lit16 v0, v0, 0x99

    add-int/lit8 v0, v0, -0x2

    div-int/lit8 v0, v0, 0x5

    add-int/2addr v0, v2

    mul-int/lit16 v2, v1, 0x16d

    add-int/2addr v0, v2

    div-int/lit8 v2, v1, 0x4

    add-int/2addr v0, v2

    div-int/lit8 v2, v1, 0x64

    sub-int/2addr v0, v2

    div-int/lit16 v1, v1, 0x190

    add-int/2addr v0, v1

    return v0
.end method

.method private a([III)I
    .locals 3

    const/4 v0, 0x0

    :goto_0
    if-gt p2, p3, :cond_0

    aget v1, p1, p2

    and-int/lit16 v1, v1, 0x3ff

    add-int/2addr v0, v1

    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lfi/firstbeat/coach/a/f;->b:Lfi/firstbeat/coach/a/e;

    iget v2, p0, Lfi/firstbeat/coach/a/f;->q:I

    invoke-virtual {v1, v0, v2}, Lfi/firstbeat/coach/a/e;->d(II)I

    move-result v0

    return v0
.end method

.method public static a()Ljava/lang/String;
    .locals 1

    const-string v0, "1.3.3"

    return-object v0
.end method

.method private c(Lfi/firstbeat/coach/a/b;)I
    .locals 3

    const/4 v0, 0x0

    iget v1, p1, Lfi/firstbeat/coach/a/b;->a:I

    if-eqz v1, :cond_1

    iget v1, p1, Lfi/firstbeat/coach/a/b;->c:I

    iget v2, p1, Lfi/firstbeat/coach/a/b;->a:I

    if-lt v1, v2, :cond_0

    iget v1, p1, Lfi/firstbeat/coach/a/b;->b:I

    iget v2, p1, Lfi/firstbeat/coach/a/b;->a:I

    if-le v1, v2, :cond_1

    :cond_0
    const/4 v0, -0x3

    :goto_0
    return v0

    :cond_1
    iget v1, p1, Lfi/firstbeat/coach/a/b;->b:I

    iget v2, p1, Lfi/firstbeat/coach/a/b;->c:I

    if-le v1, v2, :cond_2

    const/4 v0, -0x4

    goto :goto_0

    :cond_2
    iget v1, p0, Lfi/firstbeat/coach/a/f;->u:I

    if-eqz v1, :cond_3

    iget v1, p0, Lfi/firstbeat/coach/a/f;->u:I

    iget v2, p1, Lfi/firstbeat/coach/a/b;->c:I

    if-le v1, v2, :cond_3

    const/4 v0, -0x5

    goto :goto_0

    :cond_3
    iget v1, p0, Lfi/firstbeat/coach/a/f;->u:I

    if-eqz v1, :cond_4

    iget v1, p1, Lfi/firstbeat/coach/a/b;->c:I

    iget v2, p0, Lfi/firstbeat/coach/a/f;->u:I

    if-le v1, v2, :cond_4

    iget v1, p1, Lfi/firstbeat/coach/a/b;->c:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v0, v1, p1}, Lfi/firstbeat/coach/a/f;->a(IILfi/firstbeat/coach/a/b;)I

    :cond_4
    iget v1, p1, Lfi/firstbeat/coach/a/b;->b:I

    iget v2, p1, Lfi/firstbeat/coach/a/b;->c:I

    if-ne v1, v2, :cond_5

    iget v1, p0, Lfi/firstbeat/coach/a/f;->u:I

    if-nez v1, :cond_5

    iput v0, p0, Lfi/firstbeat/coach/a/f;->s:I

    iput v0, p0, Lfi/firstbeat/coach/a/f;->t:I

    :goto_1
    iget v1, p0, Lfi/firstbeat/coach/a/f;->u:I

    iget v2, p1, Lfi/firstbeat/coach/a/b;->c:I

    if-ne v1, v2, :cond_b

    iget-object v1, p0, Lfi/firstbeat/coach/a/f;->r:[I

    const/16 v2, 0x1b

    aget v1, v1, v2

    if-eqz v1, :cond_b

    iget v1, p1, Lfi/firstbeat/coach/a/b;->c:I

    iget v2, p1, Lfi/firstbeat/coach/a/b;->b:I

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lfi/firstbeat/coach/a/f;->t:I

    :goto_2
    iget v1, p1, Lfi/firstbeat/coach/a/b;->d:I

    iput v1, p0, Lfi/firstbeat/coach/a/f;->q:I

    iget v1, p1, Lfi/firstbeat/coach/a/b;->e:I

    if-ltz v1, :cond_c

    iget v1, p1, Lfi/firstbeat/coach/a/b;->e:I

    const/4 v2, 0x7

    if-gt v1, v2, :cond_c

    iget v1, p1, Lfi/firstbeat/coach/a/b;->e:I

    iput v1, p0, Lfi/firstbeat/coach/a/f;->p:I

    :goto_3
    iget v1, p1, Lfi/firstbeat/coach/a/b;->f:I

    iput v1, p0, Lfi/firstbeat/coach/a/f;->v:I

    goto :goto_0

    :cond_5
    iget v1, p0, Lfi/firstbeat/coach/a/f;->u:I

    if-eqz v1, :cond_6

    iget v1, p1, Lfi/firstbeat/coach/a/b;->c:I

    iget v2, p0, Lfi/firstbeat/coach/a/f;->u:I

    if-ne v1, v2, :cond_7

    :cond_6
    iget v1, p0, Lfi/firstbeat/coach/a/f;->u:I

    if-nez v1, :cond_9

    iget v1, p1, Lfi/firstbeat/coach/a/b;->c:I

    iget v2, p0, Lfi/firstbeat/coach/a/f;->u:I

    if-eq v1, v2, :cond_9

    :cond_7
    iget v1, p1, Lfi/firstbeat/coach/a/b;->a:I

    if-nez v1, :cond_8

    iget v1, p1, Lfi/firstbeat/coach/a/b;->c:I

    iget v2, p1, Lfi/firstbeat/coach/a/b;->b:I

    sub-int/2addr v1, v2

    iput v1, p0, Lfi/firstbeat/coach/a/f;->s:I

    goto :goto_1

    :cond_8
    iget v1, p1, Lfi/firstbeat/coach/a/b;->c:I

    iget v2, p1, Lfi/firstbeat/coach/a/b;->a:I

    sub-int/2addr v1, v2

    iput v1, p0, Lfi/firstbeat/coach/a/f;->s:I

    goto :goto_1

    :cond_9
    iget v1, p1, Lfi/firstbeat/coach/a/b;->a:I

    if-nez v1, :cond_a

    iget v1, p1, Lfi/firstbeat/coach/a/b;->c:I

    iget v2, p1, Lfi/firstbeat/coach/a/b;->b:I

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lfi/firstbeat/coach/a/f;->s:I

    goto :goto_1

    :cond_a
    iget v1, p1, Lfi/firstbeat/coach/a/b;->c:I

    iget v2, p1, Lfi/firstbeat/coach/a/b;->a:I

    sub-int/2addr v1, v2

    iput v1, p0, Lfi/firstbeat/coach/a/f;->s:I

    goto :goto_1

    :cond_b
    iget v1, p1, Lfi/firstbeat/coach/a/b;->c:I

    iget v2, p1, Lfi/firstbeat/coach/a/b;->b:I

    sub-int/2addr v1, v2

    iput v1, p0, Lfi/firstbeat/coach/a/f;->t:I

    goto :goto_2

    :cond_c
    iget v1, p0, Lfi/firstbeat/coach/a/f;->q:I

    const/16 v2, 0x28

    if-gt v1, v2, :cond_d

    iput v0, p0, Lfi/firstbeat/coach/a/f;->p:I

    :goto_4
    iget v1, p0, Lfi/firstbeat/coach/a/f;->p:I

    iput v1, p1, Lfi/firstbeat/coach/a/b;->e:I

    goto :goto_3

    :cond_d
    iget v1, p0, Lfi/firstbeat/coach/a/f;->q:I

    const/16 v2, 0x32

    if-ne v1, v2, :cond_e

    sget-short v1, Lfi/firstbeat/coach/a/f;->d:S

    iput v1, p0, Lfi/firstbeat/coach/a/f;->p:I

    goto :goto_4

    :cond_e
    iget v1, p0, Lfi/firstbeat/coach/a/f;->q:I

    const/16 v2, 0x3c

    if-ne v1, v2, :cond_f

    sget-short v1, Lfi/firstbeat/coach/a/f;->e:S

    iput v1, p0, Lfi/firstbeat/coach/a/f;->p:I

    goto :goto_4

    :cond_f
    iget v1, p0, Lfi/firstbeat/coach/a/f;->q:I

    const/16 v2, 0x46

    if-ne v1, v2, :cond_10

    sget-short v1, Lfi/firstbeat/coach/a/f;->f:S

    iput v1, p0, Lfi/firstbeat/coach/a/f;->p:I

    goto :goto_4

    :cond_10
    sget-short v1, Lfi/firstbeat/coach/a/f;->g:S

    iput v1, p0, Lfi/firstbeat/coach/a/f;->p:I

    goto :goto_4
.end method

.method private d()I
    .locals 6

    const/16 v5, 0x46

    const/16 v4, 0x1c

    const/4 v1, 0x0

    invoke-virtual {p0}, Lfi/firstbeat/coach/a/f;->c()I

    move-result v0

    iput v0, p0, Lfi/firstbeat/coach/a/f;->q:I

    iget v0, p0, Lfi/firstbeat/coach/a/f;->p:I

    sget-short v2, Lfi/firstbeat/coach/a/f;->h:S

    if-ge v0, v2, :cond_8

    iget v0, p0, Lfi/firstbeat/coach/a/f;->s:I

    if-ge v0, v4, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lfi/firstbeat/coach/a/f;->q:I

    const/16 v2, 0x28

    if-gt v0, v2, :cond_3

    move v0, v1

    :goto_1
    iget v2, p0, Lfi/firstbeat/coach/a/f;->p:I

    if-lt v0, v2, :cond_7

    iget v0, p0, Lfi/firstbeat/coach/a/f;->p:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lfi/firstbeat/coach/a/f;->p:I

    :cond_1
    :goto_2
    iput v1, p0, Lfi/firstbeat/coach/a/f;->s:I

    :cond_2
    :goto_3
    iget v0, p0, Lfi/firstbeat/coach/a/f;->p:I

    goto :goto_0

    :cond_3
    iget v0, p0, Lfi/firstbeat/coach/a/f;->q:I

    const/16 v2, 0x32

    if-ne v0, v2, :cond_4

    sget-short v0, Lfi/firstbeat/coach/a/f;->d:S

    goto :goto_1

    :cond_4
    iget v0, p0, Lfi/firstbeat/coach/a/f;->q:I

    const/16 v2, 0x3c

    if-ne v0, v2, :cond_5

    sget-short v0, Lfi/firstbeat/coach/a/f;->e:S

    goto :goto_1

    :cond_5
    iget v0, p0, Lfi/firstbeat/coach/a/f;->q:I

    if-ne v0, v5, :cond_6

    sget-short v0, Lfi/firstbeat/coach/a/f;->f:S

    goto :goto_1

    :cond_6
    sget-short v0, Lfi/firstbeat/coach/a/f;->g:S

    goto :goto_1

    :cond_7
    iget v2, p0, Lfi/firstbeat/coach/a/f;->p:I

    if-ge v0, v2, :cond_1

    iget v0, p0, Lfi/firstbeat/coach/a/f;->p:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lfi/firstbeat/coach/a/f;->p:I

    goto :goto_2

    :cond_8
    iget-object v0, p0, Lfi/firstbeat/coach/a/f;->r:[I

    const/16 v2, 0x15

    const/16 v3, 0x1b

    invoke-direct {p0, v0, v2, v3}, Lfi/firstbeat/coach/a/f;->a([III)I

    move-result v0

    iget v2, p0, Lfi/firstbeat/coach/a/f;->q:I

    if-ge v2, v5, :cond_9

    sget-short v0, Lfi/firstbeat/coach/a/f;->g:S

    iput v0, p0, Lfi/firstbeat/coach/a/f;->p:I

    goto :goto_3

    :cond_9
    iget v2, p0, Lfi/firstbeat/coach/a/f;->s:I

    const/4 v3, 0x6

    if-le v2, v3, :cond_2

    const/16 v2, 0x26

    if-lt v0, v2, :cond_a

    sget-short v0, Lfi/firstbeat/coach/a/f;->i:S

    iput v0, p0, Lfi/firstbeat/coach/a/f;->p:I

    :goto_4
    iput v1, p0, Lfi/firstbeat/coach/a/f;->s:I

    goto :goto_3

    :cond_a
    if-lt v0, v4, :cond_b

    iget v0, p0, Lfi/firstbeat/coach/a/f;->p:I

    sget-short v2, Lfi/firstbeat/coach/a/f;->j:S

    if-eq v0, v2, :cond_c

    :cond_b
    iget v0, p0, Lfi/firstbeat/coach/a/f;->p:I

    sget-short v2, Lfi/firstbeat/coach/a/f;->j:S

    if-eq v0, v2, :cond_c

    sget-short v0, Lfi/firstbeat/coach/a/f;->j:S

    iput v0, p0, Lfi/firstbeat/coach/a/f;->p:I

    goto :goto_4

    :cond_c
    sget-short v0, Lfi/firstbeat/coach/a/f;->h:S

    iput v0, p0, Lfi/firstbeat/coach/a/f;->p:I

    goto :goto_4
.end method


# virtual methods
.method public final a(IILfi/firstbeat/coach/a/b;)I
    .locals 7

    const/16 v6, 0x1c

    const/4 v1, 0x0

    const/16 v5, 0x1b

    if-lez p2, :cond_1

    :goto_0
    iget v0, p0, Lfi/firstbeat/coach/a/f;->u:I

    if-eqz v0, :cond_2

    iget v0, p0, Lfi/firstbeat/coach/a/f;->u:I

    if-ge p2, v0, :cond_2

    const/4 v1, -0x1

    :cond_0
    :goto_1
    return v1

    :cond_1
    iget p2, p3, Lfi/firstbeat/coach/a/b;->c:I

    goto :goto_0

    :cond_2
    iget v0, p0, Lfi/firstbeat/coach/a/f;->u:I

    if-nez v0, :cond_3

    iput p2, p0, Lfi/firstbeat/coach/a/f;->u:I

    :cond_3
    iget v0, p0, Lfi/firstbeat/coach/a/f;->u:I

    sub-int v0, p2, v0

    if-le v0, v5, :cond_4

    move v0, v1

    :goto_2
    if-ge v0, v5, :cond_b

    iget-object v2, p0, Lfi/firstbeat/coach/a/f;->r:[I

    aput v1, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget v0, p0, Lfi/firstbeat/coach/a/f;->u:I

    sub-int v0, p2, v0

    if-lez v0, :cond_8

    move v0, v1

    :goto_3
    iget v2, p0, Lfi/firstbeat/coach/a/f;->u:I

    sub-int v2, p2, v2

    rsub-int/lit8 v2, v2, 0x1c

    if-ge v0, v2, :cond_6

    if-ltz v0, :cond_5

    if-ge v0, v6, :cond_5

    iget v2, p0, Lfi/firstbeat/coach/a/f;->u:I

    sub-int v2, p2, v2

    add-int/2addr v2, v0

    if-ge v2, v6, :cond_5

    iget-object v2, p0, Lfi/firstbeat/coach/a/f;->r:[I

    iget-object v3, p0, Lfi/firstbeat/coach/a/f;->r:[I

    iget v4, p0, Lfi/firstbeat/coach/a/f;->u:I

    sub-int v4, p2, v4

    add-int/2addr v4, v0

    aget v3, v3, v4

    aput v3, v2, v0

    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_6
    iget v0, p0, Lfi/firstbeat/coach/a/f;->u:I

    sub-int v0, p2, v0

    const/4 v2, 0x1

    if-le v0, v2, :cond_b

    iget v0, p0, Lfi/firstbeat/coach/a/f;->u:I

    sub-int v0, p2, v0

    rsub-int/lit8 v0, v0, 0x1c

    :goto_4
    if-ge v0, v5, :cond_b

    if-ltz v0, :cond_7

    if-ge v0, v6, :cond_7

    iget-object v2, p0, Lfi/firstbeat/coach/a/f;->r:[I

    aput v1, v2, v0

    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_8
    iget v0, p0, Lfi/firstbeat/coach/a/f;->u:I

    if-ne p2, v0, :cond_b

    iput p2, p0, Lfi/firstbeat/coach/a/f;->u:I

    iget-object v0, p0, Lfi/firstbeat/coach/a/f;->r:[I

    aget v0, v0, v5

    and-int/lit16 v0, v0, 0x3ff

    add-int/2addr v0, p1

    const/16 v2, 0x3e8

    if-gt v0, v2, :cond_0

    iget-object v0, p0, Lfi/firstbeat/coach/a/f;->r:[I

    aget v0, v0, v5

    shr-int/lit8 v0, v0, 0xa

    const/16 v2, 0x3c

    if-gt v0, v2, :cond_0

    iget-object v0, p0, Lfi/firstbeat/coach/a/f;->r:[I

    aget v0, v0, v5

    if-lez v0, :cond_9

    if-lez p1, :cond_9

    iget-object v0, p0, Lfi/firstbeat/coach/a/f;->r:[I

    iget-object v2, p0, Lfi/firstbeat/coach/a/f;->r:[I

    aget v2, v2, v5

    shr-int/lit8 v2, v2, 0xa

    add-int/lit8 v2, v2, 0x1

    shl-int/lit8 v2, v2, 0xa

    iget-object v3, p0, Lfi/firstbeat/coach/a/f;->r:[I

    aget v3, v3, v5

    and-int/lit16 v3, v3, 0x3ff

    add-int/2addr v3, p1

    or-int/2addr v2, v3

    aput v2, v0, v5

    goto/16 :goto_1

    :cond_9
    iget-object v0, p0, Lfi/firstbeat/coach/a/f;->r:[I

    aget v0, v0, v5

    if-eqz v0, :cond_a

    if-eqz p1, :cond_0

    :cond_a
    iget-object v0, p0, Lfi/firstbeat/coach/a/f;->r:[I

    or-int/lit16 v2, p1, 0x400

    aput v2, v0, v5

    goto/16 :goto_1

    :cond_b
    if-eqz p1, :cond_c

    iget-object v0, p0, Lfi/firstbeat/coach/a/f;->r:[I

    or-int/lit16 v2, p1, 0x400

    aput v2, v0, v5

    :goto_5
    iput p2, p0, Lfi/firstbeat/coach/a/f;->u:I

    goto/16 :goto_1

    :cond_c
    iget-object v0, p0, Lfi/firstbeat/coach/a/f;->r:[I

    aput v1, v0, v5

    goto :goto_5
.end method

.method public final a(ILfi/firstbeat/coach/a/b;[I[I[I[I[I[I)I
    .locals 19

    const/4 v2, 0x1

    new-array v11, v2, [I

    const/4 v2, 0x0

    const/4 v3, 0x0

    aput v3, v11, v2

    const/4 v2, 0x7

    new-array v12, v2, [I

    fill-array-data v12, :array_0

    const/16 v2, 0x1c

    new-array v13, v2, [I

    move-object/from16 v0, p0

    iget-object v2, v0, Lfi/firstbeat/coach/a/f;->r:[I

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0x1c

    invoke-static {v2, v3, v13, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object/from16 v0, p3

    array-length v2, v0

    move/from16 v0, p1

    if-ne v2, v0, :cond_0

    move-object/from16 v0, p4

    array-length v2, v0

    move/from16 v0, p1

    if-ne v2, v0, :cond_0

    move-object/from16 v0, p5

    array-length v2, v0

    move/from16 v0, p1

    if-ne v2, v0, :cond_0

    move-object/from16 v0, p6

    array-length v2, v0

    move/from16 v0, p1

    if-ne v2, v0, :cond_0

    move-object/from16 v0, p7

    array-length v2, v0

    move/from16 v0, p1

    if-ne v2, v0, :cond_0

    move-object/from16 v0, p8

    array-length v2, v0

    move/from16 v0, p1

    if-eq v2, v0, :cond_2

    :cond_0
    const/4 v2, -0x6

    :cond_1
    :goto_0
    return v2

    :cond_2
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lfi/firstbeat/coach/a/f;->c(Lfi/firstbeat/coach/a/b;)I

    move-result v2

    if-ltz v2, :cond_1

    move-object/from16 v0, p0

    iget v2, v0, Lfi/firstbeat/coach/a/f;->s:I

    if-lez v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lfi/firstbeat/coach/a/f;->r:[I

    const/16 v3, 0x1b

    aget v2, v2, v3

    if-nez v2, :cond_3

    move-object/from16 v0, p0

    iget v2, v0, Lfi/firstbeat/coach/a/f;->t:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lfi/firstbeat/coach/a/f;->t:I

    :cond_3
    const/4 v2, 0x0

    move v10, v2

    :goto_1
    move/from16 v0, p1

    if-ge v10, v0, :cond_25

    if-nez v10, :cond_5

    move-object/from16 v0, p0

    iget v2, v0, Lfi/firstbeat/coach/a/f;->u:I

    move-object/from16 v0, p2

    iget v3, v0, Lfi/firstbeat/coach/a/b;->c:I

    if-eq v2, v3, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lfi/firstbeat/coach/a/f;->r:[I

    const/16 v3, 0x1b

    aget v2, v2, v3

    if-nez v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lfi/firstbeat/coach/a/f;->r:[I

    const/16 v3, 0x15

    const/4 v4, 0x0

    const/4 v5, 0x6

    invoke-static {v2, v3, v12, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :goto_2
    aget v2, p4, v10

    if-nez v2, :cond_24

    const/4 v2, 0x6

    const/4 v3, 0x0

    aput v3, v12, v2

    aget v14, p3, v10

    if-nez v14, :cond_6

    const/4 v2, 0x0

    const/4 v3, 0x0

    aput v3, v11, v2

    const/4 v2, 0x6

    const/4 v3, 0x0

    aput v3, v12, v2

    const/4 v2, 0x0

    :cond_4
    :goto_3
    aput v2, p4, v10

    const/4 v2, 0x0

    aget v2, v11, v2

    aput v2, p5, v10

    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lfi/firstbeat/coach/a/f;->r:[I

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-object v4, v0, Lfi/firstbeat/coach/a/f;->r:[I

    const/4 v5, 0x0

    const/16 v6, 0x1b

    invoke-static {v2, v3, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lfi/firstbeat/coach/a/f;->r:[I

    const/16 v3, 0x1b

    const/4 v4, 0x6

    aget v4, v12, v4

    aput v4, v2, v3

    move-object/from16 v0, p0

    iget v2, v0, Lfi/firstbeat/coach/a/f;->s:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lfi/firstbeat/coach/a/f;->s:I

    move-object/from16 v0, p0

    iget v2, v0, Lfi/firstbeat/coach/a/f;->t:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lfi/firstbeat/coach/a/f;->t:I

    invoke-direct/range {p0 .. p0}, Lfi/firstbeat/coach/a/f;->d()I

    move-object/from16 v0, p0

    iget v2, v0, Lfi/firstbeat/coach/a/f;->q:I

    aput v2, p6, v10

    move-object/from16 v0, p0

    iget v2, v0, Lfi/firstbeat/coach/a/f;->p:I

    aput v2, p7, v10

    move-object/from16 v0, p0

    iget-object v2, v0, Lfi/firstbeat/coach/a/f;->r:[I

    const/16 v3, 0x15

    const/16 v4, 0x1b

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4}, Lfi/firstbeat/coach/a/f;->a([III)I

    move-result v2

    aput v2, p8, v10

    add-int/lit8 v2, v10, 0x1

    move v10, v2

    goto/16 :goto_1

    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lfi/firstbeat/coach/a/f;->r:[I

    const/16 v3, 0x16

    const/4 v4, 0x0

    const/4 v5, 0x6

    invoke-static {v2, v3, v12, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_2

    :cond_6
    move-object/from16 v0, p0

    iget v2, v0, Lfi/firstbeat/coach/a/f;->v:I

    if-eqz v2, :cond_7

    move-object/from16 v0, p0

    iget v2, v0, Lfi/firstbeat/coach/a/f;->v:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_d

    :cond_7
    move-object/from16 v0, p0

    iget v2, v0, Lfi/firstbeat/coach/a/f;->p:I

    sget-short v3, Lfi/firstbeat/coach/a/f;->h:S

    if-ne v2, v3, :cond_b

    const/16 v2, 0x28

    :goto_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lfi/firstbeat/coach/a/f;->b:Lfi/firstbeat/coach/a/e;

    const/4 v4, 0x0

    const/4 v5, 0x5

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v4, v5}, Lfi/firstbeat/coach/a/f;->a([III)I

    move-result v4

    move-object/from16 v0, p0

    iget v5, v0, Lfi/firstbeat/coach/a/f;->q:I

    invoke-virtual {v3, v4, v2, v5}, Lfi/firstbeat/coach/a/e;->a(III)I

    move-result v15

    const/4 v8, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x7

    new-array v7, v3, [I

    fill-array-data v7, :array_1

    if-nez v14, :cond_f

    const/4 v3, 0x0

    const/4 v4, 0x0

    aput v4, v11, v3

    const/4 v3, 0x0

    :goto_6
    if-eqz v3, :cond_21

    const/4 v4, 0x6

    move-object/from16 v0, p0

    iget-object v5, v0, Lfi/firstbeat/coach/a/f;->a:Lfi/firstbeat/coach/a/c;

    move-object/from16 v0, p0

    iget v6, v0, Lfi/firstbeat/coach/a/f;->q:I

    invoke-virtual {v5, v3, v6}, Lfi/firstbeat/coach/a/c;->c(II)I

    move-result v5

    or-int/lit16 v5, v5, 0x400

    aput v5, v12, v4

    const/4 v4, 0x0

    const/4 v5, 0x6

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v4, v5}, Lfi/firstbeat/coach/a/f;->a([III)I

    move-result v4

    add-int/lit8 v2, v2, -0x1

    if-ge v4, v2, :cond_a

    move-object/from16 v0, p0

    iget v2, v0, Lfi/firstbeat/coach/a/f;->q:I

    const/16 v4, 0x50

    if-ge v2, v4, :cond_a

    div-int/lit8 v2, v3, 0xa

    mul-int/lit8 v2, v2, 0xa

    sub-int v2, v3, v2

    rsub-int/lit8 v2, v2, 0x8

    if-lez v2, :cond_a

    move-object/from16 v0, p0

    iget-object v4, v0, Lfi/firstbeat/coach/a/f;->a:Lfi/firstbeat/coach/a/c;

    add-int v5, v3, v2

    move-object/from16 v0, p0

    iget v6, v0, Lfi/firstbeat/coach/a/f;->q:I

    invoke-virtual {v4, v5, v6}, Lfi/firstbeat/coach/a/c;->c(II)I

    move-result v4

    if-le v4, v15, :cond_9

    :cond_8
    add-int/lit8 v2, v2, -0x1

    move-object/from16 v0, p0

    iget-object v4, v0, Lfi/firstbeat/coach/a/f;->a:Lfi/firstbeat/coach/a/c;

    add-int v5, v3, v2

    move-object/from16 v0, p0

    iget v6, v0, Lfi/firstbeat/coach/a/f;->q:I

    invoke-virtual {v4, v5, v6}, Lfi/firstbeat/coach/a/c;->c(II)I

    move-result v4

    if-le v4, v15, :cond_9

    add-int v4, v3, v2

    const/16 v5, 0xf

    if-le v4, v5, :cond_9

    if-gtz v2, :cond_8

    :cond_9
    add-int/2addr v3, v2

    const/4 v2, 0x6

    move-object/from16 v0, p0

    iget-object v4, v0, Lfi/firstbeat/coach/a/f;->a:Lfi/firstbeat/coach/a/c;

    move-object/from16 v0, p0

    iget v5, v0, Lfi/firstbeat/coach/a/f;->q:I

    invoke-virtual {v4, v3, v5}, Lfi/firstbeat/coach/a/c;->c(II)I

    move-result v4

    or-int/lit16 v4, v4, 0x400

    aput v4, v12, v2

    :cond_a
    move v2, v3

    const/16 v3, 0xf

    if-ge v2, v3, :cond_22

    const/4 v2, 0x6

    move-object/from16 v0, p0

    iget-object v3, v0, Lfi/firstbeat/coach/a/f;->a:Lfi/firstbeat/coach/a/c;

    const/16 v4, 0xf

    move-object/from16 v0, p0

    iget v5, v0, Lfi/firstbeat/coach/a/f;->q:I

    invoke-virtual {v3, v4, v5}, Lfi/firstbeat/coach/a/c;->c(II)I

    move-result v3

    or-int/lit16 v3, v3, 0x400

    aput v3, v12, v2

    const/4 v2, 0x0

    const/16 v3, 0x19

    aput v3, v11, v2

    const/16 v2, 0xf

    goto/16 :goto_3

    :cond_b
    move-object/from16 v0, p0

    iget v2, v0, Lfi/firstbeat/coach/a/f;->p:I

    sget-short v3, Lfi/firstbeat/coach/a/f;->i:S

    if-ne v2, v3, :cond_c

    const/16 v2, 0x14

    goto/16 :goto_5

    :cond_c
    move-object/from16 v0, p0

    iget v2, v0, Lfi/firstbeat/coach/a/f;->p:I

    sget-short v3, Lfi/firstbeat/coach/a/f;->j:S

    if-eq v2, v3, :cond_e

    const/16 v2, 0x23

    goto/16 :goto_5

    :cond_d
    move-object/from16 v0, p0

    iget v2, v0, Lfi/firstbeat/coach/a/f;->v:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_e

    move-object/from16 v0, p0

    iget v2, v0, Lfi/firstbeat/coach/a/f;->q:I

    const/16 v3, 0x3c

    if-ge v2, v3, :cond_e

    const/16 v2, 0x23

    goto/16 :goto_5

    :cond_e
    const/16 v2, 0x1e

    goto/16 :goto_5

    :cond_f
    move-object/from16 v0, p0

    iget v3, v0, Lfi/firstbeat/coach/a/f;->p:I

    sget-short v6, Lfi/firstbeat/coach/a/f;->h:S

    if-ne v3, v6, :cond_12

    const/4 v3, 0x1

    :cond_10
    :goto_7
    const/4 v6, 0x0

    :goto_8
    const/4 v7, 0x7

    if-ge v6, v7, :cond_16

    sget-object v7, Lfi/firstbeat/coach/a/f;->m:[[S

    move-object/from16 v0, p0

    iget v9, v0, Lfi/firstbeat/coach/a/f;->p:I

    aget-object v7, v7, v9

    aget-short v7, v7, v6

    add-int/2addr v7, v3

    if-le v7, v4, :cond_11

    sget-object v4, Lfi/firstbeat/coach/a/f;->m:[[S

    move-object/from16 v0, p0

    iget v5, v0, Lfi/firstbeat/coach/a/f;->p:I

    aget-object v4, v4, v5

    aget-short v4, v4, v6

    add-int/2addr v4, v3

    move v5, v6

    :cond_11
    add-int/lit8 v6, v6, 0x1

    goto :goto_8

    :cond_12
    const/4 v3, 0x5

    :goto_9
    const/4 v6, -0x5

    if-le v3, v6, :cond_15

    const/4 v6, 0x0

    :goto_a
    const/4 v9, 0x7

    if-ge v6, v9, :cond_14

    sget-object v9, Lfi/firstbeat/coach/a/f;->m:[[S

    move-object/from16 v0, p0

    iget v0, v0, Lfi/firstbeat/coach/a/f;->p:I

    move/from16 v16, v0

    aget-object v9, v9, v16

    aget-short v9, v9, v6

    const/16 v16, 0xa

    move/from16 v0, v16

    if-le v9, v0, :cond_13

    move-object/from16 v0, p0

    iget-object v9, v0, Lfi/firstbeat/coach/a/f;->a:Lfi/firstbeat/coach/a/c;

    sget-object v16, Lfi/firstbeat/coach/a/f;->m:[[S

    move-object/from16 v0, p0

    iget v0, v0, Lfi/firstbeat/coach/a/f;->p:I

    move/from16 v17, v0

    aget-object v16, v16, v17

    aget-short v16, v16, v6

    add-int v16, v16, v3

    move-object/from16 v0, p0

    iget v0, v0, Lfi/firstbeat/coach/a/f;->q:I

    move/from16 v17, v0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v9, v0, v1}, Lfi/firstbeat/coach/a/c;->c(II)I

    move-result v9

    or-int/lit16 v9, v9, 0x400

    aput v9, v7, v6

    :cond_13
    add-int/lit8 v6, v6, 0x1

    goto :goto_a

    :cond_14
    const/4 v6, 0x0

    const/4 v9, 0x6

    move-object/from16 v0, p0

    invoke-direct {v0, v7, v6, v9}, Lfi/firstbeat/coach/a/f;->a([III)I

    move-result v6

    add-int/lit8 v9, v2, 0x1

    if-le v6, v9, :cond_15

    add-int/lit8 v3, v3, -0x1

    goto :goto_9

    :cond_15
    const/4 v6, -0x1

    if-ne v3, v6, :cond_10

    const/4 v3, 0x0

    goto :goto_7

    :cond_16
    move-object/from16 v0, p0

    iget v6, v0, Lfi/firstbeat/coach/a/f;->p:I

    sget-short v7, Lfi/firstbeat/coach/a/f;->d:S

    if-le v6, v7, :cond_17

    const/4 v6, 0x4

    aget v6, v12, v6

    and-int/lit16 v6, v6, 0x3ff

    if-nez v6, :cond_17

    const/4 v6, 0x5

    aget v6, v12, v6

    and-int/lit16 v6, v6, 0x3ff

    if-nez v6, :cond_17

    const/4 v3, 0x0

    sget-object v6, Lfi/firstbeat/coach/a/f;->n:[[S

    move-object/from16 v0, p0

    iget v7, v0, Lfi/firstbeat/coach/a/f;->p:I

    aget-object v6, v6, v7

    aget-short v5, v6, v5

    aput v5, v11, v3

    move v3, v4

    goto/16 :goto_6

    :cond_17
    move-object/from16 v0, p0

    iget-object v5, v0, Lfi/firstbeat/coach/a/f;->a:Lfi/firstbeat/coach/a/c;

    const/4 v6, 0x4

    aget v6, v12, v6

    and-int/lit16 v6, v6, 0x3ff

    move-object/from16 v0, p0

    iget v7, v0, Lfi/firstbeat/coach/a/f;->q:I

    invoke-virtual {v5, v6, v7}, Lfi/firstbeat/coach/a/c;->e(II)I

    move-result v5

    const/16 v6, 0x1e

    if-le v5, v6, :cond_18

    move-object/from16 v0, p0

    iget-object v5, v0, Lfi/firstbeat/coach/a/f;->a:Lfi/firstbeat/coach/a/c;

    const/4 v6, 0x5

    aget v6, v12, v6

    and-int/lit16 v6, v6, 0x3ff

    move-object/from16 v0, p0

    iget v7, v0, Lfi/firstbeat/coach/a/f;->q:I

    invoke-virtual {v5, v6, v7}, Lfi/firstbeat/coach/a/c;->e(II)I

    move-result v5

    const/16 v6, 0x1e

    if-le v5, v6, :cond_18

    const/4 v3, 0x0

    const/4 v4, 0x0

    aput v4, v11, v3

    const/4 v3, 0x6

    const/4 v4, 0x0

    aput v4, v12, v3

    const/4 v3, 0x0

    goto/16 :goto_6

    :cond_18
    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lfi/firstbeat/coach/a/f;->s:I

    if-lez v5, :cond_1d

    const/4 v5, 0x6

    move v9, v5

    :goto_b
    if-ltz v9, :cond_1d

    const/4 v5, 0x0

    move v6, v5

    :goto_c
    move-object/from16 v0, p0

    iget v5, v0, Lfi/firstbeat/coach/a/f;->s:I

    if-ge v6, v5, :cond_1b

    rsub-int/lit8 v5, v6, 0x5

    if-ltz v5, :cond_1b

    move-object/from16 v0, p0

    iget-object v5, v0, Lfi/firstbeat/coach/a/f;->a:Lfi/firstbeat/coach/a/c;

    rsub-int/lit8 v16, v6, 0x5

    aget v16, v12, v16

    move/from16 v0, v16

    and-int/lit16 v0, v0, 0x3ff

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lfi/firstbeat/coach/a/f;->q:I

    move/from16 v17, v0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v5, v0, v1}, Lfi/firstbeat/coach/a/c;->e(II)I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v16

    sub-int v5, v9, v6

    if-gez v5, :cond_19

    add-int/lit8 v5, v5, 0x7

    :cond_19
    sget-object v17, Lfi/firstbeat/coach/a/f;->m:[[S

    move-object/from16 v0, p0

    iget v0, v0, Lfi/firstbeat/coach/a/f;->p:I

    move/from16 v18, v0

    aget-object v17, v17, v18

    aget-short v5, v17, v5

    const/16 v17, 0xa

    move/from16 v0, v17

    if-le v5, v0, :cond_1a

    add-int/2addr v5, v3

    :cond_1a
    div-int/lit8 v16, v16, 0xa

    div-int/lit8 v5, v5, 0xa

    move/from16 v0, v16

    if-ne v0, v5, :cond_1b

    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_c

    :cond_1b
    if-lt v6, v8, :cond_26

    add-int/lit8 v5, v9, 0x1

    const/4 v7, 0x7

    if-ne v5, v7, :cond_1c

    const/4 v5, 0x0

    :cond_1c
    :goto_d
    add-int/lit8 v7, v9, -0x1

    move v8, v6

    move v9, v7

    move v7, v5

    goto :goto_b

    :cond_1d
    sget-short v4, Lfi/firstbeat/coach/a/f;->l:S

    if-ne v14, v4, :cond_1f

    sget-object v4, Lfi/firstbeat/coach/a/f;->m:[[S

    move-object/from16 v0, p0

    iget v5, v0, Lfi/firstbeat/coach/a/f;->p:I

    aget-object v4, v4, v5

    aget-short v4, v4, v7

    if-nez v4, :cond_1f

    :goto_e
    sget-object v4, Lfi/firstbeat/coach/a/f;->m:[[S

    move-object/from16 v0, p0

    iget v5, v0, Lfi/firstbeat/coach/a/f;->p:I

    aget-object v4, v4, v5

    aget-short v4, v4, v7

    if-nez v4, :cond_1f

    const/4 v4, 0x6

    if-ne v7, v4, :cond_1e

    const/4 v7, 0x0

    goto :goto_e

    :cond_1e
    add-int/lit8 v7, v7, 0x1

    goto :goto_e

    :cond_1f
    const/4 v4, 0x0

    sget-object v5, Lfi/firstbeat/coach/a/f;->n:[[S

    move-object/from16 v0, p0

    iget v6, v0, Lfi/firstbeat/coach/a/f;->p:I

    aget-object v5, v5, v6

    aget-short v5, v5, v7

    aput v5, v11, v4

    sget-object v4, Lfi/firstbeat/coach/a/f;->m:[[S

    move-object/from16 v0, p0

    iget v5, v0, Lfi/firstbeat/coach/a/f;->p:I

    aget-object v4, v4, v5

    aget-short v4, v4, v7

    const/16 v5, 0xa

    if-le v4, v5, :cond_20

    sget-object v4, Lfi/firstbeat/coach/a/f;->m:[[S

    move-object/from16 v0, p0

    iget v5, v0, Lfi/firstbeat/coach/a/f;->p:I

    aget-object v4, v4, v5

    aget-short v4, v4, v7

    add-int/2addr v3, v4

    goto/16 :goto_6

    :cond_20
    sget-object v3, Lfi/firstbeat/coach/a/f;->m:[[S

    move-object/from16 v0, p0

    iget v4, v0, Lfi/firstbeat/coach/a/f;->p:I

    aget-object v3, v3, v4

    aget-short v3, v3, v7

    goto/16 :goto_6

    :cond_21
    move v2, v3

    :cond_22
    sget-short v3, Lfi/firstbeat/coach/a/f;->l:S

    if-ne v14, v3, :cond_23

    const/16 v3, 0xf

    if-ge v2, v3, :cond_23

    const/4 v2, 0x0

    const/16 v3, 0x19

    aput v3, v11, v2

    const/4 v2, 0x6

    move-object/from16 v0, p0

    iget-object v3, v0, Lfi/firstbeat/coach/a/f;->a:Lfi/firstbeat/coach/a/c;

    const/16 v4, 0xf

    move-object/from16 v0, p0

    iget v5, v0, Lfi/firstbeat/coach/a/f;->q:I

    invoke-virtual {v3, v4, v5}, Lfi/firstbeat/coach/a/c;->c(II)I

    move-result v3

    or-int/lit16 v3, v3, 0x400

    aput v3, v12, v2

    const/16 v2, 0xf

    goto/16 :goto_3

    :cond_23
    const/16 v3, 0xa

    if-gt v2, v3, :cond_4

    const/4 v2, 0x6

    const/4 v3, 0x0

    aput v3, v12, v2

    const/4 v2, 0x0

    const/4 v3, 0x0

    aput v3, v11, v2

    const/4 v2, 0x0

    goto/16 :goto_3

    :cond_24
    const/4 v2, 0x6

    move-object/from16 v0, p0

    iget-object v3, v0, Lfi/firstbeat/coach/a/f;->a:Lfi/firstbeat/coach/a/c;

    aget v4, p4, v10

    move-object/from16 v0, p0

    iget v5, v0, Lfi/firstbeat/coach/a/f;->q:I

    invoke-virtual {v3, v4, v5}, Lfi/firstbeat/coach/a/c;->c(II)I

    move-result v3

    or-int/lit16 v3, v3, 0x400

    aput v3, v12, v2

    goto/16 :goto_4

    :cond_25
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lfi/firstbeat/coach/a/f;->r:[I

    const/4 v4, 0x0

    const/16 v5, 0x1c

    invoke-static {v13, v2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_26
    move v5, v7

    move v6, v8

    goto/16 :goto_d

    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method

.method public final a(Lfi/firstbeat/coach/a/b;)I
    .locals 3

    const/4 v1, 0x0

    move v0, v1

    :goto_0
    const/16 v2, 0x1c

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lfi/firstbeat/coach/a/f;->r:[I

    aput v1, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iput v1, p0, Lfi/firstbeat/coach/a/f;->p:I

    iput v1, p0, Lfi/firstbeat/coach/a/f;->q:I

    iput v1, p0, Lfi/firstbeat/coach/a/f;->s:I

    iput v1, p0, Lfi/firstbeat/coach/a/f;->t:I

    iput v1, p0, Lfi/firstbeat/coach/a/f;->u:I

    iput v1, p0, Lfi/firstbeat/coach/a/f;->v:I

    invoke-direct {p0, p1}, Lfi/firstbeat/coach/a/f;->c(Lfi/firstbeat/coach/a/b;)I

    move-result v0

    const/4 v2, -0x1

    if-ge v0, v2, :cond_1

    :goto_1
    return v0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final b()I
    .locals 5

    const/4 v0, 0x0

    const/16 v4, 0x1c

    move v1, v0

    :goto_0
    iget v2, p0, Lfi/firstbeat/coach/a/f;->t:I

    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    if-ge v1, v2, :cond_0

    iget-object v2, p0, Lfi/firstbeat/coach/a/f;->r:[I

    rsub-int/lit8 v3, v1, 0x1b

    aget v2, v2, v3

    and-int/lit16 v2, v2, 0x3ff

    add-int/2addr v0, v2

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget v1, p0, Lfi/firstbeat/coach/a/f;->t:I

    const/4 v2, 0x7

    if-ge v1, v2, :cond_2

    :cond_1
    :goto_1
    return v0

    :cond_2
    iget v1, p0, Lfi/firstbeat/coach/a/f;->t:I

    if-ge v1, v4, :cond_1

    iget-object v1, p0, Lfi/firstbeat/coach/a/f;->r:[I

    const/16 v2, 0x1b

    aget v1, v1, v2

    and-int/lit16 v1, v1, 0x3ff

    if-gt v1, v4, :cond_3

    mul-int/lit8 v0, v0, 0x1c

    iget v1, p0, Lfi/firstbeat/coach/a/f;->t:I

    div-int/2addr v0, v1

    goto :goto_1

    :cond_3
    mul-int/lit8 v0, v0, 0x1c

    iget v1, p0, Lfi/firstbeat/coach/a/f;->t:I

    add-int/lit8 v1, v1, 0x1

    div-int/2addr v0, v1

    goto :goto_1
.end method

.method public final b(Lfi/firstbeat/coach/a/b;)I
    .locals 2

    invoke-direct {p0, p1}, Lfi/firstbeat/coach/a/f;->c(Lfi/firstbeat/coach/a/b;)I

    move-result v0

    const/4 v1, -0x1

    if-ge v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lfi/firstbeat/coach/a/f;->d()I

    move-result v0

    goto :goto_0
.end method

.method public final c()I
    .locals 3

    invoke-virtual {p0}, Lfi/firstbeat/coach/a/f;->b()I

    move-result v1

    if-lez v1, :cond_2

    const/4 v0, 0x0

    :goto_0
    const/16 v2, 0xe

    if-ge v0, v2, :cond_0

    sget-object v2, Lfi/firstbeat/coach/a/f;->o:[I

    aget v2, v2, v0

    if-lt v1, v2, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    add-int/lit8 v0, v0, -0x1

    const/16 v1, 0x8

    if-ge v0, v1, :cond_1

    mul-int/lit8 v0, v0, 0xa

    :goto_1
    iget v1, p0, Lfi/firstbeat/coach/a/f;->q:I

    if-ge v1, v0, :cond_2

    :goto_2
    return v0

    :cond_1
    add-int/lit8 v0, v0, -0x7

    mul-int/lit8 v0, v0, 0x5

    add-int/lit8 v0, v0, 0x46

    goto :goto_1

    :cond_2
    iget v0, p0, Lfi/firstbeat/coach/a/f;->q:I

    goto :goto_2
.end method
