.class public final Lfi/firstbeat/coach/b/a;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<First:",
        "Ljava/lang/Object;",
        "Second:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TFirst;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TSecond;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TFirst;TSecond;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lfi/firstbeat/coach/b/a;->a:Ljava/lang/Object;

    iput-object p2, p0, Lfi/firstbeat/coach/b/a;->b:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TFirst;"
        }
    .end annotation

    iget-object v0, p0, Lfi/firstbeat/coach/b/a;->a:Ljava/lang/Object;

    return-object v0
.end method

.method public final b()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TSecond;"
        }
    .end annotation

    iget-object v0, p0, Lfi/firstbeat/coach/b/a;->b:Ljava/lang/Object;

    return-object v0
.end method

.method public final compareTo(Ljava/lang/Object;)I
    .locals 4

    const/4 v2, 0x1

    const/4 v1, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    instance-of v0, p1, Lfi/firstbeat/coach/b/a;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/ClassCastException;

    invoke-direct {v0}, Ljava/lang/ClassCastException;-><init>()V

    throw v0

    :cond_1
    check-cast p1, Lfi/firstbeat/coach/b/a;

    iget-object v0, p0, Lfi/firstbeat/coach/b/a;->a:Ljava/lang/Object;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lfi/firstbeat/coach/b/a;->a:Ljava/lang/Object;

    if-nez v0, :cond_7

    :cond_2
    iget-object v0, p0, Lfi/firstbeat/coach/b/a;->a:Ljava/lang/Object;

    if-nez v0, :cond_5

    move v0, v1

    :goto_0
    iget-object v3, p1, Lfi/firstbeat/coach/b/a;->a:Ljava/lang/Object;

    if-nez v3, :cond_6

    move v3, v1

    :goto_1
    sub-int/2addr v0, v3

    :goto_2
    if-nez v0, :cond_4

    iget-object v3, p0, Lfi/firstbeat/coach/b/a;->b:Ljava/lang/Object;

    if-eqz v3, :cond_3

    iget-object v3, p1, Lfi/firstbeat/coach/b/a;->b:Ljava/lang/Object;

    if-nez v3, :cond_a

    :cond_3
    iget-object v0, p0, Lfi/firstbeat/coach/b/a;->b:Ljava/lang/Object;

    if-nez v0, :cond_8

    move v0, v1

    :goto_3
    iget-object v3, p1, Lfi/firstbeat/coach/b/a;->b:Ljava/lang/Object;

    if-nez v3, :cond_9

    :goto_4
    sub-int/2addr v0, v1

    :cond_4
    :goto_5
    return v0

    :cond_5
    move v0, v2

    goto :goto_0

    :cond_6
    move v3, v2

    goto :goto_1

    :cond_7
    iget-object v0, p0, Lfi/firstbeat/coach/b/a;->a:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/Comparable;

    if-eqz v0, :cond_b

    iget-object v0, p1, Lfi/firstbeat/coach/b/a;->a:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/Comparable;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lfi/firstbeat/coach/b/a;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Comparable;

    iget-object v3, p1, Lfi/firstbeat/coach/b/a;->a:Ljava/lang/Object;

    invoke-interface {v0, v3}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v0

    goto :goto_2

    :cond_8
    move v0, v2

    goto :goto_3

    :cond_9
    move v1, v2

    goto :goto_4

    :cond_a
    iget-object v1, p0, Lfi/firstbeat/coach/b/a;->b:Ljava/lang/Object;

    instance-of v1, v1, Ljava/lang/Comparable;

    if-eqz v1, :cond_4

    iget-object v1, p1, Lfi/firstbeat/coach/b/a;->b:Ljava/lang/Object;

    instance-of v1, v1, Ljava/lang/Comparable;

    if-eqz v1, :cond_4

    iget-object v0, p0, Lfi/firstbeat/coach/b/a;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Comparable;

    iget-object v1, p1, Lfi/firstbeat/coach/b/a;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v0

    goto :goto_5

    :cond_b
    move v0, v1

    goto :goto_2
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v1, p1, Lfi/firstbeat/coach/b/a;

    if-eqz v1, :cond_0

    check-cast p1, Lfi/firstbeat/coach/b/a;

    iget-object v1, p0, Lfi/firstbeat/coach/b/a;->a:Ljava/lang/Object;

    iget-object v2, p1, Lfi/firstbeat/coach/b/a;->a:Ljava/lang/Object;

    if-eq v1, v2, :cond_2

    iget-object v1, p0, Lfi/firstbeat/coach/b/a;->a:Ljava/lang/Object;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lfi/firstbeat/coach/b/a;->a:Ljava/lang/Object;

    iget-object v2, p1, Lfi/firstbeat/coach/b/a;->a:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    iget-object v1, p0, Lfi/firstbeat/coach/b/a;->b:Ljava/lang/Object;

    iget-object v2, p1, Lfi/firstbeat/coach/b/a;->b:Ljava/lang/Object;

    if-eq v1, v2, :cond_3

    iget-object v1, p0, Lfi/firstbeat/coach/b/a;->b:Ljava/lang/Object;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lfi/firstbeat/coach/b/a;->b:Ljava/lang/Object;

    iget-object v2, p1, Lfi/firstbeat/coach/b/a;->b:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lfi/firstbeat/coach/b/a;->a:Ljava/lang/Object;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iget-object v2, p0, Lfi/firstbeat/coach/b/a;->b:Ljava/lang/Object;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lfi/firstbeat/coach/b/a;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lfi/firstbeat/coach/b/a;->b:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1
.end method
