.class public Lfi/firstbeat/coach/eteintegration/CoachVars;
.super Lfi/firstbeat/ete/FBTvars;


# instance fields
.field public lastTrainingLevelUpdate:Ljava/util/GregorianCalendar;

.field public latestExerciseTime:Ljava/util/GregorianCalendar;

.field public latestFeedbackPhraseNumber:I

.field public now:Ljava/util/GregorianCalendar;

.field public previousToPreviousTrainingLevel:I

.field public previousTrainingLevel:I

.field public startDate:Ljava/util/GregorianCalendar;

.field public trainingGoal:I

.field public trainingLevel:I


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0}, Lfi/firstbeat/ete/FBTvars;-><init>()V

    iput v2, p0, Lfi/firstbeat/coach/eteintegration/CoachVars;->trainingGoal:I

    iput-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachVars;->now:Ljava/util/GregorianCalendar;

    iput-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachVars;->startDate:Ljava/util/GregorianCalendar;

    iput v1, p0, Lfi/firstbeat/coach/eteintegration/CoachVars;->trainingLevel:I

    iput-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachVars;->lastTrainingLevelUpdate:Ljava/util/GregorianCalendar;

    iput v1, p0, Lfi/firstbeat/coach/eteintegration/CoachVars;->previousTrainingLevel:I

    iput v1, p0, Lfi/firstbeat/coach/eteintegration/CoachVars;->previousToPreviousTrainingLevel:I

    iput v2, p0, Lfi/firstbeat/coach/eteintegration/CoachVars;->latestFeedbackPhraseNumber:I

    iput-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachVars;->latestExerciseTime:Ljava/util/GregorianCalendar;

    return-void
.end method
