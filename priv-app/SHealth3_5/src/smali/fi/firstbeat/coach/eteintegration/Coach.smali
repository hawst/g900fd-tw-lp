.class public interface abstract Lfi/firstbeat/coach/eteintegration/Coach;
.super Ljava/lang/Object;


# virtual methods
.method public abstract addAnalysedExercise(Ljava/util/GregorianCalendar;Lfi/firstbeat/ete/ETEresults;D)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lfi/firstbeat/coach/eteintegration/CoachException;
        }
    .end annotation
.end method

.method public abstract addEnergyExercise(Ljava/util/GregorianCalendar;III)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lfi/firstbeat/coach/eteintegration/CoachException;
        }
    .end annotation
.end method

.method public abstract addRunningExercise(Ljava/util/GregorianCalendar;IDI)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lfi/firstbeat/coach/eteintegration/CoachException;
        }
    .end annotation
.end method

.method public abstract addUserExercise(Ljava/util/GregorianCalendar;ILfi/firstbeat/coach/eteintegration/ExerciseIntensity;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lfi/firstbeat/coach/eteintegration/CoachException;
        }
    .end annotation
.end method

.method public abstract getFeedbackPhraseNumber()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lfi/firstbeat/coach/eteintegration/CoachException;
        }
    .end annotation
.end method

.method public abstract getFitnessClass()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lfi/firstbeat/coach/eteintegration/CoachException;
        }
    .end annotation
.end method

.method public abstract getFitnessLevelIncreaseIn28d()Ljava/lang/Integer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lfi/firstbeat/coach/eteintegration/CoachException;
        }
    .end annotation
.end method

.method public abstract getFitnessThresholds()[I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lfi/firstbeat/coach/eteintegration/CoachException;
        }
    .end annotation
.end method

.method public abstract getMonthlyLoad()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lfi/firstbeat/coach/eteintegration/CoachException;
        }
    .end annotation
.end method

.method public abstract getNextWorkout(ZI)Lfi/firstbeat/coach/eteintegration/TrainingProgramWorkout;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lfi/firstbeat/coach/eteintegration/CoachException;
        }
    .end annotation
.end method

.method public abstract getParameters()Lfi/firstbeat/coach/eteintegration/CoachVars;
.end method

.method public abstract getRecommendationExceededBy()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lfi/firstbeat/coach/eteintegration/CoachException;
        }
    .end annotation
.end method

.method public abstract getResourceRecovery()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lfi/firstbeat/coach/eteintegration/CoachException;
        }
    .end annotation
.end method

.method public abstract getRunningPerformance()[I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lfi/firstbeat/coach/eteintegration/CoachException;
        }
    .end annotation
.end method

.method public abstract getUserModifiedWorkoutProgram(Ljava/util/List;I)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lfi/firstbeat/coach/eteintegration/Plan;",
            ">;I)",
            "Ljava/util/List",
            "<",
            "Lfi/firstbeat/coach/eteintegration/TrainingProgramWorkout;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lfi/firstbeat/coach/eteintegration/CoachException;
        }
    .end annotation
.end method

.method public abstract getVersion()Ljava/lang/String;
.end method

.method public abstract getVo2max()D
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lfi/firstbeat/coach/eteintegration/CoachException;
        }
    .end annotation
.end method

.method public abstract getWeeklyTrainingLoad()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lfi/firstbeat/coach/eteintegration/CoachException;
        }
    .end annotation
.end method

.method public abstract getWeeklyTrainingLoadLowerLimit()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lfi/firstbeat/coach/eteintegration/CoachException;
        }
    .end annotation
.end method

.method public abstract getWeeklyTrainingLoadUpperLimit()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lfi/firstbeat/coach/eteintegration/CoachException;
        }
    .end annotation
.end method

.method public abstract getWorkoutProgram(IZI)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZI)",
            "Ljava/util/List",
            "<",
            "Lfi/firstbeat/coach/eteintegration/TrainingProgramWorkout;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lfi/firstbeat/coach/eteintegration/CoachException;
        }
    .end annotation
.end method

.method public abstract setParameters(Lfi/firstbeat/coach/eteintegration/CoachVars;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lfi/firstbeat/coach/eteintegration/CoachException;
        }
    .end annotation
.end method
