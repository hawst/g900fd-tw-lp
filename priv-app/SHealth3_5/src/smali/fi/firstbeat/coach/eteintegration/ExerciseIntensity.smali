.class public final enum Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;

.field public static final enum HARD:Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;

.field public static final enum LIGHT:Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;

.field public static final enum MAXIMAL:Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;

.field public static final enum MODERATE:Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;

.field public static final enum VERY_HARD:Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;

.field public static final enum VERY_LIGHT:Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;

    const-string v1, "VERY_LIGHT"

    invoke-direct {v0, v1, v3}, Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;->VERY_LIGHT:Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;

    new-instance v0, Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;

    const-string v1, "LIGHT"

    invoke-direct {v0, v1, v4}, Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;->LIGHT:Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;

    new-instance v0, Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;

    const-string v1, "MODERATE"

    invoke-direct {v0, v1, v5}, Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;->MODERATE:Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;

    new-instance v0, Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;

    const-string v1, "HARD"

    invoke-direct {v0, v1, v6}, Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;->HARD:Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;

    new-instance v0, Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;

    const-string v1, "VERY_HARD"

    invoke-direct {v0, v1, v7}, Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;->VERY_HARD:Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;

    new-instance v0, Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;

    const-string v1, "MAXIMAL"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;->MAXIMAL:Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;

    const/4 v0, 0x6

    new-array v0, v0, [Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;

    sget-object v1, Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;->VERY_LIGHT:Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;

    aput-object v1, v0, v3

    sget-object v1, Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;->LIGHT:Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;

    aput-object v1, v0, v4

    sget-object v1, Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;->MODERATE:Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;

    aput-object v1, v0, v5

    sget-object v1, Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;->HARD:Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;

    aput-object v1, v0, v6

    sget-object v1, Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;->VERY_HARD:Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;->MAXIMAL:Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;

    aput-object v2, v0, v1

    sput-object v0, Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;->$VALUES:[Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;
    .locals 1

    const-class v0, Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;

    return-object v0
.end method

.method public static values()[Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;
    .locals 1

    sget-object v0, Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;->$VALUES:[Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;

    invoke-virtual {v0}, [Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;

    return-object v0
.end method
