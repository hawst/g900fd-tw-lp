.class public final enum Lfi/firstbeat/coach/eteintegration/DayMode;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lfi/firstbeat/coach/eteintegration/DayMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lfi/firstbeat/coach/eteintegration/DayMode;

.field public static final enum AUTO:Lfi/firstbeat/coach/eteintegration/DayMode;

.field public static final enum REST:Lfi/firstbeat/coach/eteintegration/DayMode;

.field public static final enum TRAINING:Lfi/firstbeat/coach/eteintegration/DayMode;

.field public static final enum USER_SPECIFIED:Lfi/firstbeat/coach/eteintegration/DayMode;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lfi/firstbeat/coach/eteintegration/DayMode;

    const-string v1, "AUTO"

    invoke-direct {v0, v1, v2}, Lfi/firstbeat/coach/eteintegration/DayMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfi/firstbeat/coach/eteintegration/DayMode;->AUTO:Lfi/firstbeat/coach/eteintegration/DayMode;

    new-instance v0, Lfi/firstbeat/coach/eteintegration/DayMode;

    const-string v1, "REST"

    invoke-direct {v0, v1, v3}, Lfi/firstbeat/coach/eteintegration/DayMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfi/firstbeat/coach/eteintegration/DayMode;->REST:Lfi/firstbeat/coach/eteintegration/DayMode;

    new-instance v0, Lfi/firstbeat/coach/eteintegration/DayMode;

    const-string v1, "TRAINING"

    invoke-direct {v0, v1, v4}, Lfi/firstbeat/coach/eteintegration/DayMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfi/firstbeat/coach/eteintegration/DayMode;->TRAINING:Lfi/firstbeat/coach/eteintegration/DayMode;

    new-instance v0, Lfi/firstbeat/coach/eteintegration/DayMode;

    const-string v1, "USER_SPECIFIED"

    invoke-direct {v0, v1, v5}, Lfi/firstbeat/coach/eteintegration/DayMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfi/firstbeat/coach/eteintegration/DayMode;->USER_SPECIFIED:Lfi/firstbeat/coach/eteintegration/DayMode;

    const/4 v0, 0x4

    new-array v0, v0, [Lfi/firstbeat/coach/eteintegration/DayMode;

    sget-object v1, Lfi/firstbeat/coach/eteintegration/DayMode;->AUTO:Lfi/firstbeat/coach/eteintegration/DayMode;

    aput-object v1, v0, v2

    sget-object v1, Lfi/firstbeat/coach/eteintegration/DayMode;->REST:Lfi/firstbeat/coach/eteintegration/DayMode;

    aput-object v1, v0, v3

    sget-object v1, Lfi/firstbeat/coach/eteintegration/DayMode;->TRAINING:Lfi/firstbeat/coach/eteintegration/DayMode;

    aput-object v1, v0, v4

    sget-object v1, Lfi/firstbeat/coach/eteintegration/DayMode;->USER_SPECIFIED:Lfi/firstbeat/coach/eteintegration/DayMode;

    aput-object v1, v0, v5

    sput-object v0, Lfi/firstbeat/coach/eteintegration/DayMode;->$VALUES:[Lfi/firstbeat/coach/eteintegration/DayMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lfi/firstbeat/coach/eteintegration/DayMode;
    .locals 1

    const-class v0, Lfi/firstbeat/coach/eteintegration/DayMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lfi/firstbeat/coach/eteintegration/DayMode;

    return-object v0
.end method

.method public static values()[Lfi/firstbeat/coach/eteintegration/DayMode;
    .locals 1

    sget-object v0, Lfi/firstbeat/coach/eteintegration/DayMode;->$VALUES:[Lfi/firstbeat/coach/eteintegration/DayMode;

    invoke-virtual {v0}, [Lfi/firstbeat/coach/eteintegration/DayMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lfi/firstbeat/coach/eteintegration/DayMode;

    return-object v0
.end method
