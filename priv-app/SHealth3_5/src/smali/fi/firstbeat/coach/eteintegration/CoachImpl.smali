.class public Lfi/firstbeat/coach/eteintegration/CoachImpl;
.super Ljava/lang/Object;

# interfaces
.implements Lfi/firstbeat/coach/eteintegration/Coach;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lfi/firstbeat/coach/eteintegration/CoachImpl$1;
    }
.end annotation


# static fields
.field private static final a:Ljava/util/SortedMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/SortedMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final q:[I

.field private static final r:[I

.field private static final s:[I

.field private static final t:[D

.field private static final u:[I


# instance fields
.field private final b:Lfi/firstbeat/coach/a/f;

.field private final c:Lfi/firstbeat/coach/a/h;

.field private d:Lfi/firstbeat/coach/eteintegration/CoachVars;

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lfi/firstbeat/coach/b/a",
            "<",
            "Ljava/util/GregorianCalendar;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field private f:Z

.field private g:Ljava/util/GregorianCalendar;

.field private h:I

.field private i:Ljava/util/GregorianCalendar;

.field private j:D

.field private k:Ljava/util/GregorianCalendar;

.field private l:D

.field private m:Ljava/util/GregorianCalendar;

.field private n:D

.field private o:Ljava/util/GregorianCalendar;

.field private p:D


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    sput-object v0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a:Ljava/util/SortedMap;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a:Ljava/util/SortedMap;

    const/16 v1, 0xa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a:Ljava/util/SortedMap;

    const/16 v1, 0x14

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x14

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a:Ljava/util/SortedMap;

    const/16 v1, 0x1e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x2a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a:Ljava/util/SortedMap;

    const/16 v1, 0x28

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x48

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a:Ljava/util/SortedMap;

    const/16 v1, 0x32

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x58

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a:Ljava/util/SortedMap;

    const/16 v1, 0x3c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x9c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a:Ljava/util/SortedMap;

    const/16 v1, 0x46

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0xb4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a:Ljava/util/SortedMap;

    const/16 v1, 0x4b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0xf8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a:Ljava/util/SortedMap;

    const/16 v1, 0x50

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x110

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a:Ljava/util/SortedMap;

    const/16 v1, 0x55

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x12b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a:Ljava/util/SortedMap;

    const/16 v1, 0x5a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x154

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a:Ljava/util/SortedMap;

    const/16 v1, 0x5f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x18e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a:Ljava/util/SortedMap;

    const/16 v1, 0x64

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x1bf

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->q:[I

    const/16 v0, 0x36

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->r:[I

    const/16 v0, 0x36

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->s:[I

    const/16 v0, 0x1c

    new-array v0, v0, [D

    fill-array-data v0, :array_3

    sput-object v0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->t:[D

    const/16 v0, 0x8c

    new-array v0, v0, [I

    const/16 v1, 0x9

    const/16 v2, 0xb

    invoke-static {v4, v1, v2}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v1

    aput v1, v0, v4

    const/16 v1, 0x1e

    const/16 v2, 0x28

    invoke-static {v4, v1, v2}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v1

    aput v1, v0, v5

    const/16 v1, 0x2e

    invoke-static {v5, v7, v1}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v1

    aput v1, v0, v6

    const/16 v1, 0x15

    invoke-static {v6, v1, v8}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v1

    aput v1, v0, v7

    const/16 v1, 0x31

    const/16 v2, 0x11

    invoke-static {v8, v1, v2}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v1

    aput v1, v0, v8

    const/4 v1, 0x5

    const/16 v2, 0x8

    const/16 v3, 0x29

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x6

    const/16 v2, 0x30

    const/16 v3, 0xb

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x7

    const/16 v2, 0x1a

    invoke-static {v5, v4, v2}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x8

    const/16 v2, 0xd

    const/16 v3, 0x31

    invoke-static {v6, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x9

    const/16 v2, 0x22

    const/16 v3, 0x3b

    invoke-static {v8, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xa

    const/16 v2, 0x8

    const/16 v3, 0xe

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xb

    const/16 v2, 0x1b

    const/16 v3, 0x27

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xc

    const/16 v2, 0x39

    const/16 v3, 0x1a

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xd

    const/4 v2, 0x7

    const/16 v3, 0x10

    invoke-static {v6, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xe

    const/16 v2, 0x16

    invoke-static {v8, v2, v7}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xf

    const/4 v2, 0x7

    const/16 v3, 0x31

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x10

    const/16 v2, 0x1a

    const/16 v3, 0x16

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x11

    const/16 v2, 0x36

    const/16 v3, 0x2c

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x12

    const/16 v2, 0x13

    invoke-static {v6, v5, v2}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x13

    const/16 v2, 0xa

    const/16 v3, 0x13

    invoke-static {v8, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x14

    const/4 v2, 0x7

    const/16 v3, 0x1b

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x15

    const/16 v2, 0x19

    const/16 v3, 0xc

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x16

    const/16 v2, 0x34

    const/16 v3, 0x11

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x17

    const/16 v2, 0x37

    const/16 v3, 0x37

    invoke-static {v5, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x18

    const/16 v2, 0x3b

    const/16 v3, 0x23

    invoke-static {v7, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x19

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1a

    const/16 v2, 0x18

    const/16 v3, 0x8

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1b

    const/16 v2, 0x32

    invoke-static {v4, v2, v7}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1c

    const/16 v2, 0x32

    const/16 v3, 0x3b

    invoke-static {v5, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1d

    const/16 v2, 0x31

    const/16 v3, 0x2d

    invoke-static {v7, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1e

    const/4 v2, 0x6

    const/16 v3, 0x31

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1f

    const/16 v2, 0x17

    const/16 v3, 0x9

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x20

    const/16 v2, 0x30

    invoke-static {v4, v2, v5}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x21

    const/16 v2, 0x2e

    const/16 v3, 0x1b

    invoke-static {v5, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x22

    const/16 v2, 0x28

    const/16 v3, 0x2b

    invoke-static {v7, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x23

    const/4 v2, 0x6

    const/16 v3, 0x20

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x24

    const/16 v2, 0x16

    const/16 v3, 0xf

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x25

    const/16 v2, 0x2e

    const/16 v3, 0x9

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x26

    const/16 v2, 0x2a

    const/16 v3, 0x11

    invoke-static {v5, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x27

    const/16 v2, 0x20

    const/16 v3, 0x17

    invoke-static {v7, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x28

    const/4 v2, 0x6

    const/16 v3, 0x11

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x29

    const/16 v2, 0x15

    const/16 v3, 0x19

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2a

    const/16 v2, 0x2c

    const/16 v3, 0x19

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2b

    const/16 v2, 0x26

    const/16 v3, 0x1b

    invoke-static {v5, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2c

    const/16 v2, 0x18

    const/16 v3, 0x27

    invoke-static {v7, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2d

    const/4 v2, 0x6

    invoke-static {v4, v2, v7}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2e

    const/16 v2, 0x14

    const/16 v3, 0x27

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2f

    const/16 v2, 0x2a

    const/16 v3, 0x32

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x30

    const/16 v2, 0x22

    const/16 v3, 0x35

    invoke-static {v5, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x31

    const/16 v2, 0x11

    const/16 v3, 0x1d

    invoke-static {v7, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x32

    const/4 v2, 0x5

    const/16 v3, 0x32

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x33

    const/16 v2, 0x13

    const/16 v3, 0x39

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x34

    const/16 v2, 0x29

    const/16 v3, 0x15

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x35

    const/16 v2, 0x1f

    const/16 v3, 0x23

    invoke-static {v5, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x36

    const/16 v2, 0xa

    const/16 v3, 0x31

    invoke-static {v7, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x37

    const/4 v2, 0x5

    const/16 v3, 0x26

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x38

    const/16 v2, 0x13

    const/16 v3, 0x11

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x39

    const/16 v2, 0x27

    const/16 v3, 0x3b

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x3a

    const/16 v2, 0x1c

    const/16 v3, 0x1f

    invoke-static {v5, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x3b

    const/16 v2, 0x24

    invoke-static {v7, v8, v2}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x3c

    const/4 v2, 0x5

    const/16 v3, 0x1b

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x3d

    const/16 v2, 0x12

    const/16 v3, 0x28

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x3e

    const/16 v2, 0x26

    const/16 v3, 0x2a

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x3f

    const/16 v2, 0x19

    const/16 v3, 0x28

    invoke-static {v5, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x40

    const/16 v2, 0x3a

    const/16 v3, 0x2f

    invoke-static {v6, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x41

    const/4 v2, 0x5

    const/16 v3, 0x10

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x42

    const/16 v2, 0x12

    const/4 v3, 0x5

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x43

    const/16 v2, 0x25

    const/16 v3, 0x1f

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x44

    const/16 v2, 0x17

    invoke-static {v5, v2, v4}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x45

    const/16 v2, 0x35

    const/16 v3, 0x14

    invoke-static {v6, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x46

    const/4 v2, 0x5

    const/4 v3, 0x6

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x47

    const/16 v2, 0x11

    const/16 v3, 0x21

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x48

    const/16 v2, 0x24

    const/16 v3, 0x18

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x49

    const/16 v2, 0x14

    const/16 v3, 0x1e

    invoke-static {v5, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x4a

    const/16 v2, 0x30

    const/16 v3, 0xe

    invoke-static {v6, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x4b

    const/16 v2, 0x39

    invoke-static {v4, v8, v2}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x4c

    const/16 v2, 0x11

    invoke-static {v4, v2, v7}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x4d

    const/16 v2, 0x23

    const/16 v3, 0x16

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x4e

    const/16 v2, 0x12

    const/16 v3, 0x9

    invoke-static {v5, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x4f

    const/16 v2, 0x2b

    const/16 v3, 0x19

    invoke-static {v6, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x50

    const/16 v2, 0x31

    invoke-static {v4, v8, v2}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x51

    const/16 v2, 0x10

    const/16 v3, 0x22

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x52

    const/16 v2, 0x22

    const/16 v3, 0x17

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x53

    const/16 v2, 0xf

    const/16 v3, 0x39

    invoke-static {v5, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x54

    const/16 v2, 0x26

    const/16 v3, 0x36

    invoke-static {v6, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x55

    const/16 v2, 0x29

    invoke-static {v4, v8, v2}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x56

    const/16 v2, 0x10

    const/4 v3, 0x7

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x57

    const/16 v2, 0x21

    const/16 v3, 0x1c

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x58

    const/16 v2, 0xd

    const/16 v3, 0x35

    invoke-static {v5, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x59

    const/16 v2, 0x22

    const/16 v3, 0x26

    invoke-static {v6, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x5a

    const/16 v2, 0x21

    invoke-static {v4, v8, v2}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x5b

    const/16 v2, 0xf

    const/16 v3, 0x2a

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x5c

    const/16 v2, 0x20

    const/16 v3, 0x23

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x5d

    const/16 v2, 0xb

    const/16 v3, 0x38

    invoke-static {v5, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x5e

    const/16 v2, 0x1e

    const/16 v3, 0x24

    invoke-static {v6, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x5f

    const/16 v2, 0x1a

    invoke-static {v4, v8, v2}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x60

    const/16 v2, 0xf

    const/16 v3, 0x12

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x61

    const/16 v2, 0x1f

    const/16 v3, 0x2e

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x62

    const/16 v2, 0xa

    const/4 v3, 0x5

    invoke-static {v5, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x63

    const/16 v2, 0x1a

    const/16 v3, 0x2f

    invoke-static {v6, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x64

    const/16 v2, 0x13

    invoke-static {v4, v8, v2}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x65

    const/16 v2, 0xe

    const/16 v3, 0x37

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x66

    const/16 v2, 0x1f

    invoke-static {v4, v2, v4}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x67

    const/16 v2, 0x8

    const/16 v3, 0x15

    invoke-static {v5, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x68

    const/16 v2, 0x17

    const/16 v3, 0xa

    invoke-static {v6, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x69

    const/16 v2, 0xd

    invoke-static {v4, v8, v2}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x6a

    const/16 v2, 0xe

    const/16 v3, 0x21

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x6b

    const/16 v2, 0x1e

    const/16 v3, 0x10

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x6c

    const/4 v2, 0x6

    const/16 v3, 0x2a

    invoke-static {v5, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x6d

    const/16 v2, 0x13

    const/16 v3, 0x2c

    invoke-static {v6, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x6e

    const/4 v2, 0x7

    invoke-static {v4, v8, v2}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x6f

    const/16 v2, 0xe

    const/16 v3, 0xd

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x70

    const/16 v2, 0x1d

    const/16 v3, 0x22

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x71

    const/4 v2, 0x5

    const/16 v3, 0x8

    invoke-static {v5, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x72

    const/16 v2, 0x10

    const/16 v3, 0x1d

    invoke-static {v6, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x73

    invoke-static {v4, v8, v6}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x74

    const/16 v2, 0xd

    const/16 v3, 0x36

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x75

    const/16 v2, 0x1c

    const/16 v3, 0x37

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x76

    const/16 v2, 0x27

    invoke-static {v5, v7, v2}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x77

    const/16 v2, 0xd

    const/16 v3, 0x17

    invoke-static {v6, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x78

    const/16 v2, 0x38

    invoke-static {v4, v7, v2}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x79

    const/16 v2, 0xd

    const/16 v3, 0x23

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x7a

    const/16 v2, 0x1c

    const/16 v3, 0x11

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x7b

    const/16 v2, 0xf

    invoke-static {v5, v6, v2}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x7c

    const/16 v2, 0xa

    const/16 v3, 0x1b

    invoke-static {v6, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x7d

    const/16 v2, 0x33

    invoke-static {v4, v7, v2}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x7e

    const/16 v2, 0xd

    const/16 v3, 0x12

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x7f

    const/16 v2, 0x1b

    const/16 v3, 0x29

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x80

    const/16 v2, 0x36

    invoke-static {v5, v4, v2}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x81

    const/4 v2, 0x7

    const/16 v3, 0x26

    invoke-static {v6, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x82

    const/16 v2, 0x2e

    invoke-static {v4, v7, v2}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x83

    const/16 v2, 0xd

    invoke-static {v4, v2, v5}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x84

    const/16 v2, 0x1b

    const/4 v3, 0x7

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x85

    const/16 v2, 0x3b

    const/16 v3, 0x26

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x86

    const/16 v2, 0x39

    invoke-static {v6, v8, v2}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x87

    const/16 v2, 0x29

    invoke-static {v4, v7, v2}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x88

    const/16 v2, 0xc

    const/16 v3, 0x2d

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x89

    const/16 v2, 0x1a

    const/16 v3, 0x22

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x8a

    const/16 v2, 0x3a

    const/16 v3, 0x19

    invoke-static {v4, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x8b

    const/16 v2, 0x18

    invoke-static {v6, v6, v2}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(III)I

    move-result v2

    aput v2, v0, v1

    sput-object v0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->u:[I

    return-void

    :array_0
    .array-data 4
        0x18
        0x1d
        0x22
        0x27
        0x2c
        0x31
        0x36
        0x3b
    .end array-data

    :array_1
    .array-data 4
        0x1f
        0x25
        0x2b
        0x32
        0x38
        0x3e
        0x1e
        0x23
        0x2a
        0x30
        0x35
        0x3b
        0x1c
        0x22
        0x28
        0x2d
        0x33
        0x38
        0x1b
        0x20
        0x26
        0x2b
        0x30
        0x36
        0x19
        0x1f
        0x23
        0x29
        0x2e
        0x33
        0x18
        0x1d
        0x22
        0x27
        0x2b
        0x30
        0x17
        0x1b
        0x20
        0x24
        0x29
        0x2e
        0x15
        0x1a
        0x1e
        0x22
        0x27
        0x2b
        0x14
        0x18
        0x1c
        0x20
        0x24
        0x28
    .end array-data

    :array_2
    .array-data 4
        0x1a
        0x1f
        0x24
        0x29
        0x2e
        0x33
        0x19
        0x1e
        0x23
        0x28
        0x2c
        0x31
        0x18
        0x1d
        0x21
        0x25
        0x2a
        0x2e
        0x17
        0x1b
        0x1f
        0x23
        0x28
        0x2c
        0x15
        0x19
        0x1d
        0x21
        0x25
        0x29
        0x14
        0x17
        0x1b
        0x1f
        0x23
        0x26
        0x12
        0x16
        0x19
        0x1d
        0x20
        0x24
        0x11
        0x14
        0x17
        0x1b
        0x1e
        0x21
        0xf
        0x12
        0x15
        0x18
        0x1b
        0x1e
    .end array-data

    :array_3
    .array-data 8
        0x403e000000000000L    # 30.0
        0x4040000000000000L    # 32.0
        0x4041000000000000L    # 34.0
        0x4042000000000000L    # 36.0
        0x4043000000000000L    # 38.0
        0x4044000000000000L    # 40.0
        0x4045000000000000L    # 42.0
        0x4046000000000000L    # 44.0
        0x4047000000000000L    # 46.0
        0x4048000000000000L    # 48.0
        0x4049000000000000L    # 50.0
        0x404a000000000000L    # 52.0
        0x404b000000000000L    # 54.0
        0x404c000000000000L    # 56.0
        0x404d000000000000L    # 58.0
        0x404e000000000000L    # 60.0
        0x404f000000000000L    # 62.0
        0x4050000000000000L    # 64.0
        0x4050800000000000L    # 66.0
        0x4051000000000000L    # 68.0
        0x4051800000000000L    # 70.0
        0x4052000000000000L    # 72.0
        0x4052800000000000L    # 74.0
        0x4053000000000000L    # 76.0
        0x4053800000000000L    # 78.0
        0x4054000000000000L    # 80.0
        0x4054800000000000L    # 82.0
        0x4055000000000000L    # 84.0
    .end array-data
.end method

.method public constructor <init>()V
    .locals 5

    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lfi/firstbeat/coach/a/f;

    invoke-direct {v0}, Lfi/firstbeat/coach/a/f;-><init>()V

    iput-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->b:Lfi/firstbeat/coach/a/f;

    new-instance v0, Lfi/firstbeat/coach/a/h;

    invoke-direct {v0}, Lfi/firstbeat/coach/a/h;-><init>()V

    iput-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->c:Lfi/firstbeat/coach/a/h;

    iput-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->e:Ljava/util/List;

    iput-boolean v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->f:Z

    iput-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->g:Ljava/util/GregorianCalendar;

    iput v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->h:I

    iput-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->i:Ljava/util/GregorianCalendar;

    iput-wide v2, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->j:D

    iput-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->k:Ljava/util/GregorianCalendar;

    iput-wide v2, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->l:D

    iput-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->m:Ljava/util/GregorianCalendar;

    iput-wide v2, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->n:D

    iput-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->o:Ljava/util/GregorianCalendar;

    iput-wide v2, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->p:D

    return-void
.end method

.method public static Divfx(II)I
    .locals 4

    int-to-long v0, p0

    const/16 v2, 0x20

    shl-long/2addr v0, v2

    int-to-long v2, p1

    div-long/2addr v0, v2

    const/16 v2, 0x10

    shr-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public static Mulfx(II)I
    .locals 4

    int-to-long v0, p0

    int-to-long v2, p1

    mul-long/2addr v0, v2

    const/16 v2, 0x10

    shr-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public static RoundFxToI(I)I
    .locals 1

    const v0, 0x8000

    add-int/2addr v0, p0

    shr-int/lit8 v0, v0, 0x10

    return v0
.end method

.method private a(D)I
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lfi/firstbeat/coach/eteintegration/CoachException;
        }
    .end annotation

    const/4 v1, 0x7

    const-wide/high16 v2, 0x400c000000000000L    # 3.5

    mul-double/2addr v2, p1

    invoke-virtual {p0}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->getFitnessThresholds()[I

    move-result-object v4

    const/4 v0, 0x1

    :goto_0
    if-ge v0, v1, :cond_1

    add-int/lit8 v5, v0, -0x1

    aget v5, v4, v5

    int-to-double v5, v5

    cmpg-double v5, v2, v5

    if-gtz v5, :cond_0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private static a(III)I
    .locals 1

    mul-int/lit8 v0, p0, 0x3c

    add-int/2addr v0, p1

    mul-int/lit8 v0, v0, 0x3c

    add-int/2addr v0, p2

    return v0
.end method

.method private static a(IZ)I
    .locals 4

    const/16 v3, 0x28

    const/16 v2, 0x1e

    const/16 v1, 0x14

    const/16 v0, 0xa

    if-lt p0, v0, :cond_2

    if-ge p0, v1, :cond_2

    if-eqz p1, :cond_1

    const/4 v0, 0x6

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    if-lt p0, v1, :cond_4

    if-ge p0, v2, :cond_4

    if-eqz p1, :cond_3

    const/4 v0, 0x7

    goto :goto_0

    :cond_3
    const/4 v0, 0x2

    goto :goto_0

    :cond_4
    if-lt p0, v2, :cond_6

    if-ge p0, v3, :cond_6

    if-eqz p1, :cond_5

    const/16 v0, 0x8

    goto :goto_0

    :cond_5
    const/4 v0, 0x3

    goto :goto_0

    :cond_6
    if-lt p0, v3, :cond_8

    const/16 v1, 0x32

    if-ge p0, v1, :cond_8

    if-eqz p1, :cond_7

    const/16 v0, 0x9

    goto :goto_0

    :cond_7
    const/4 v0, 0x4

    goto :goto_0

    :cond_8
    if-nez p1, :cond_0

    const/4 v0, 0x5

    goto :goto_0
.end method

.method private a()Lfi/firstbeat/coach/a/b;
    .locals 2

    new-instance v0, Lfi/firstbeat/coach/a/b;

    invoke-direct {v0}, Lfi/firstbeat/coach/a/b;-><init>()V

    iget-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget-object v1, v1, Lfi/firstbeat/coach/eteintegration/CoachVars;->lastTrainingLevelUpdate:Ljava/util/GregorianCalendar;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget-object v1, v1, Lfi/firstbeat/coach/eteintegration/CoachVars;->lastTrainingLevelUpdate:Ljava/util/GregorianCalendar;

    invoke-static {v1}, Lfi/firstbeat/coach/a/f;->a(Ljava/util/GregorianCalendar;)I

    move-result v1

    iput v1, v0, Lfi/firstbeat/coach/a/b;->a:I

    :goto_0
    iget-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget-object v1, v1, Lfi/firstbeat/coach/eteintegration/CoachVars;->startDate:Ljava/util/GregorianCalendar;

    invoke-static {v1}, Lfi/firstbeat/coach/a/f;->a(Ljava/util/GregorianCalendar;)I

    move-result v1

    iput v1, v0, Lfi/firstbeat/coach/a/b;->b:I

    iget-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget-object v1, v1, Lfi/firstbeat/coach/eteintegration/CoachVars;->now:Ljava/util/GregorianCalendar;

    invoke-static {v1}, Lfi/firstbeat/coach/a/f;->a(Ljava/util/GregorianCalendar;)I

    move-result v1

    iput v1, v0, Lfi/firstbeat/coach/a/b;->c:I

    iget-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v1, v1, Lfi/firstbeat/coach/eteintegration/CoachVars;->AC:I

    iput v1, v0, Lfi/firstbeat/coach/a/b;->d:I

    iget-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v1, v1, Lfi/firstbeat/coach/eteintegration/CoachVars;->trainingLevel:I

    iput v1, v0, Lfi/firstbeat/coach/a/b;->e:I

    iget-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v1, v1, Lfi/firstbeat/coach/eteintegration/CoachVars;->trainingGoal:I

    iput v1, v0, Lfi/firstbeat/coach/a/b;->f:I

    return-object v0

    :cond_0
    const/4 v1, 0x0

    iput v1, v0, Lfi/firstbeat/coach/a/b;->a:I

    goto :goto_0
.end method

.method private static a(Ljava/util/GregorianCalendar;)Ljava/util/GregorianCalendar;
    .locals 3

    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    invoke-virtual {p0}, Ljava/util/GregorianCalendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/GregorianCalendar;->setTimeZone(Ljava/util/TimeZone;)V

    invoke-virtual {p0}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/GregorianCalendar;->setTimeInMillis(J)V

    return-object v0
.end method

.method private a(Ljava/util/GregorianCalendar;ID)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lfi/firstbeat/coach/eteintegration/CoachException;
        }
    .end annotation

    invoke-static {p1}, Lfi/firstbeat/coach/a/f;->a(Ljava/util/GregorianCalendar;)I

    move-result v0

    iget-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget-object v1, v1, Lfi/firstbeat/coach/eteintegration/CoachVars;->now:Ljava/util/GregorianCalendar;

    invoke-static {v1}, Lfi/firstbeat/coach/a/f;->a(Ljava/util/GregorianCalendar;)I

    move-result v1

    if-gt v0, v1, :cond_0

    iget-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget-object v1, v1, Lfi/firstbeat/coach/eteintegration/CoachVars;->startDate:Ljava/util/GregorianCalendar;

    invoke-static {v1}, Lfi/firstbeat/coach/a/f;->a(Ljava/util/GregorianCalendar;)I

    move-result v1

    if-ge v0, v1, :cond_1

    :cond_0
    new-instance v0, Lfi/firstbeat/coach/eteintegration/CoachException;

    const-string v1, "Exercise date is invalid"

    invoke-direct {v0, v1}, Lfi/firstbeat/coach/eteintegration/CoachException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->e:Ljava/util/List;

    new-instance v1, Lfi/firstbeat/coach/b/a;

    invoke-static {p1}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(Ljava/util/GregorianCalendar;)Ljava/util/GregorianCalendar;

    move-result-object v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lfi/firstbeat/coach/b/a;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->f:Z

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->i:Ljava/util/GregorianCalendar;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->i:Ljava/util/GregorianCalendar;

    invoke-virtual {v0, p1}, Ljava/util/GregorianCalendar;->before(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    invoke-static {p1}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(Ljava/util/GregorianCalendar;)Ljava/util/GregorianCalendar;

    move-result-object v0

    iput-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->i:Ljava/util/GregorianCalendar;

    iput-wide p3, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->j:D

    :cond_3
    return-void
.end method

.method private b()D
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lfi/firstbeat/coach/eteintegration/CoachException;
        }
    .end annotation

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v0, v0, Lfi/firstbeat/coach/eteintegration/CoachVars;->maxMET:I

    int-to-double v0, v0

    const-wide/high16 v2, 0x40f0000000000000L    # 65536.0

    div-double/2addr v0, v2

    const-wide/high16 v2, 0x400c000000000000L    # 3.5

    mul-double/2addr v0, v2

    const-wide/high16 v2, 0x4035000000000000L    # 21.0

    cmpg-double v2, v0, v2

    if-ltz v2, :cond_0

    const-wide/high16 v2, 0x4054000000000000L    # 80.0

    cmpl-double v2, v0, v2

    if-lez v2, :cond_1

    :cond_0
    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->c:Lfi/firstbeat/coach/a/h;

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v0, v0, Lfi/firstbeat/coach/eteintegration/CoachVars;->age:I

    iget-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v1, v1, Lfi/firstbeat/coach/eteintegration/CoachVars;->height:I

    iget-object v2, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v2, v2, Lfi/firstbeat/coach/eteintegration/CoachVars;->weight:I

    iget-object v3, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v3, v3, Lfi/firstbeat/coach/eteintegration/CoachVars;->AC:I

    iget-object v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v4, v4, Lfi/firstbeat/coach/eteintegration/CoachVars;->gender:I

    invoke-static {v0, v1, v2, v3, v4}, Lfi/firstbeat/coach/a/h;->a(IIIII)D

    move-result-wide v0

    :cond_1
    return-wide v0
.end method

.method private c()V
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lfi/firstbeat/coach/eteintegration/CoachException;
        }
    .end annotation

    const/16 v0, 0x41

    const/16 v1, 0x19

    const/4 v10, 0x1

    const-wide/high16 v8, 0x400c000000000000L    # 3.5

    const/4 v3, 0x0

    iput-boolean v10, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->f:Z

    iget-object v2, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v2, v2, Lfi/firstbeat/coach/eteintegration/CoachVars;->maxMET:I

    if-lez v2, :cond_a

    iget-object v2, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v2, v2, Lfi/firstbeat/coach/eteintegration/CoachVars;->maxMET:I

    int-to-double v4, v2

    const-wide/high16 v6, 0x40f0000000000000L    # 65536.0

    div-double/2addr v4, v6

    mul-double/2addr v4, v8

    cmpg-double v2, v4, v8

    if-ltz v2, :cond_0

    const-wide v6, 0x4057a00000000000L    # 94.5

    cmpl-double v2, v4, v6

    if-lez v2, :cond_1

    :cond_0
    new-instance v0, Lfi/firstbeat/coach/eteintegration/CoachException;

    const-string v1, "Maximal met is invalid"

    invoke-direct {v0, v1}, Lfi/firstbeat/coach/eteintegration/CoachException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v2, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v2, v2, Lfi/firstbeat/coach/eteintegration/CoachVars;->gender:I

    if-ne v2, v10, :cond_3

    iget-object v2, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v2, v2, Lfi/firstbeat/coach/eteintegration/CoachVars;->maxMET:I

    const v4, 0x1b6dc

    add-int/2addr v2, v4

    :goto_0
    const v4, 0x116db7

    if-lt v2, v4, :cond_4

    const v0, 0x116db7

    sub-int v0, v2, v0

    const v1, 0x12492

    invoke-static {v0, v1}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->Divfx(II)I

    move-result v0

    iget-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    shr-int/lit8 v0, v0, 0x10

    mul-int/lit8 v0, v0, 0x5

    add-int/lit8 v0, v0, 0x4b

    iput v0, v1, Lfi/firstbeat/coach/eteintegration/CoachVars;->AC:I

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v0, v0, Lfi/firstbeat/coach/eteintegration/CoachVars;->AC:I

    const/16 v1, 0x64

    if-le v0, v1, :cond_2

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    const/16 v1, 0x64

    iput v1, v0, Lfi/firstbeat/coach/eteintegration/CoachVars;->AC:I

    :cond_2
    :goto_1
    invoke-direct {p0}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a()Lfi/firstbeat/coach/a/b;

    move-result-object v8

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->b:Lfi/firstbeat/coach/a/f;

    invoke-virtual {v0, v8}, Lfi/firstbeat/coach/a/f;->a(Lfi/firstbeat/coach/a/b;)I

    move-result v0

    if-gez v0, :cond_b

    new-instance v0, Lfi/firstbeat/coach/eteintegration/CoachException;

    const-string v1, "Coach initialization failed."

    invoke-direct {v0, v1}, Lfi/firstbeat/coach/eteintegration/CoachException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iget-object v2, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v2, v2, Lfi/firstbeat/coach/eteintegration/CoachVars;->maxMET:I

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v2, v2, Lfi/firstbeat/coach/eteintegration/CoachVars;->age:I

    if-le v2, v0, :cond_7

    :goto_2
    shl-int/lit8 v0, v0, 0x10

    iget-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v1, v1, Lfi/firstbeat/coach/eteintegration/CoachVars;->gender:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_8

    iget-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v1, v1, Lfi/firstbeat/coach/eteintegration/CoachVars;->maxMET:I

    const/16 v2, 0x1380

    invoke-static {v2, v0}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->Mulfx(II)I

    move-result v0

    add-int/2addr v0, v1

    const v1, 0x211f7

    sub-int/2addr v0, v1

    const/16 v1, -0x2495

    invoke-static {v0, v0}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->Mulfx(II)I

    move-result v2

    invoke-static {v1, v2}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->Mulfx(II)I

    move-result v1

    const v2, 0x50785

    invoke-static {v2, v0}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->Mulfx(II)I

    move-result v0

    add-int/2addr v0, v1

    const v1, 0x24036e

    sub-int/2addr v0, v1

    :goto_3
    if-gez v0, :cond_5

    move v0, v3

    :cond_5
    const v1, 0x7000e

    if-le v0, v1, :cond_6

    const v0, 0x7000e

    :cond_6
    iget-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    invoke-static {v0}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->RoundFxToI(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0xa

    iput v0, v1, Lfi/firstbeat/coach/eteintegration/CoachVars;->AC:I

    goto :goto_1

    :cond_7
    if-ge v2, v1, :cond_1a

    move v0, v1

    goto :goto_2

    :cond_8
    iget-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v1, v1, Lfi/firstbeat/coach/eteintegration/CoachVars;->maxMET:I

    const v2, 0xca0ea

    if-le v1, v2, :cond_9

    const v1, 0xca0ea

    const/16 v2, 0x10f3

    invoke-static {v2, v0}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->Mulfx(II)I

    move-result v0

    add-int/2addr v0, v1

    const v1, 0x1b6c4

    sub-int/2addr v0, v1

    :goto_4
    const/16 v1, -0x42f9

    invoke-static {v0, v0}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->Mulfx(II)I

    move-result v2

    invoke-static {v1, v2}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->Mulfx(II)I

    move-result v1

    const v2, 0x741ca

    invoke-static {v2, v0}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->Mulfx(II)I

    move-result v0

    add-int/2addr v0, v1

    const v1, 0x2a7da9

    sub-int/2addr v0, v1

    goto :goto_3

    :cond_9
    iget-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v1, v1, Lfi/firstbeat/coach/eteintegration/CoachVars;->maxMET:I

    const/16 v2, 0x10f3

    invoke-static {v2, v0}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->Mulfx(II)I

    move-result v0

    add-int/2addr v0, v1

    const v1, 0x1b6c4

    sub-int/2addr v0, v1

    goto :goto_4

    :cond_a
    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    const/16 v1, 0x32

    iput v1, v0, Lfi/firstbeat/coach/eteintegration/CoachVars;->AC:I

    goto/16 :goto_1

    :cond_b
    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v1, v8, Lfi/firstbeat/coach/a/b;->e:I

    iput v1, v0, Lfi/firstbeat/coach/eteintegration/CoachVars;->trainingLevel:I

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->e:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfi/firstbeat/coach/b/a;

    iget-object v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->b:Lfi/firstbeat/coach/a/f;

    invoke-virtual {v0}, Lfi/firstbeat/coach/b/a;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0}, Lfi/firstbeat/coach/b/a;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/GregorianCalendar;

    invoke-static {v0}, Lfi/firstbeat/coach/a/f;->a(Ljava/util/GregorianCalendar;)I

    move-result v0

    invoke-virtual {v4, v1, v0, v8}, Lfi/firstbeat/coach/a/f;->a(IILfi/firstbeat/coach/a/b;)I

    goto :goto_5

    :cond_c
    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_d

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->e:Ljava/util/List;

    iget-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfi/firstbeat/coach/b/a;

    invoke-virtual {v0}, Lfi/firstbeat/coach/b/a;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/GregorianCalendar;

    invoke-static {v0}, Lfi/firstbeat/coach/a/f;->a(Ljava/util/GregorianCalendar;)I

    move-result v0

    :goto_6
    iget-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget-object v1, v1, Lfi/firstbeat/coach/eteintegration/CoachVars;->now:Ljava/util/GregorianCalendar;

    invoke-static {v1}, Lfi/firstbeat/coach/a/f;->a(Ljava/util/GregorianCalendar;)I

    move-result v1

    if-ge v0, v1, :cond_d

    iget-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->b:Lfi/firstbeat/coach/a/f;

    invoke-virtual {v1, v3, v0, v8}, Lfi/firstbeat/coach/a/f;->a(IILfi/firstbeat/coach/a/b;)I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_d
    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->b:Lfi/firstbeat/coach/a/f;

    invoke-virtual {v1}, Lfi/firstbeat/coach/a/f;->c()I

    move-result v1

    iput v1, v0, Lfi/firstbeat/coach/eteintegration/CoachVars;->AC:I

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v0, v0, Lfi/firstbeat/coach/eteintegration/CoachVars;->AC:I

    iput v0, v8, Lfi/firstbeat/coach/a/b;->d:I

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->b:Lfi/firstbeat/coach/a/f;

    invoke-virtual {v1}, Lfi/firstbeat/coach/a/f;->b()I

    move-result v1

    iput v1, v0, Lfi/firstbeat/coach/eteintegration/CoachVars;->monthlyLoad:I

    iget-object v9, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    new-instance v10, Lfi/firstbeat/coach/a/c;

    invoke-direct {v10}, Lfi/firstbeat/coach/a/c;-><init>()V

    iget v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->h:I

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->e:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->g:Ljava/util/GregorianCalendar;

    if-eqz v0, :cond_19

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v1, v0, -0x1

    :goto_7
    if-ltz v1, :cond_18

    iget-object v2, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->g:Ljava/util/GregorianCalendar;

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfi/firstbeat/coach/b/a;

    invoke-virtual {v0}, Lfi/firstbeat/coach/b/a;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/GregorianCalendar;->before(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f

    add-int/lit8 v0, v1, 0x1

    :goto_8
    iget-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->g:Ljava/util/GregorianCalendar;

    if-nez v1, :cond_10

    const-wide/16 v1, 0x0

    :goto_9
    move v6, v0

    move v7, v4

    :goto_a
    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v6, v0, :cond_13

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->e:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfi/firstbeat/coach/b/a;

    invoke-virtual {v0}, Lfi/firstbeat/coach/b/a;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/GregorianCalendar;

    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v4

    const-wide/16 v11, 0x0

    cmp-long v0, v1, v11

    if-lez v0, :cond_17

    int-to-long v11, v7

    sub-long v0, v4, v1

    const-wide/16 v13, 0x7530

    add-long/2addr v0, v13

    const-wide/32 v13, 0xea60

    div-long/2addr v0, v13

    sub-long v0, v11, v0

    long-to-int v0, v0

    :goto_b
    if-gez v0, :cond_16

    move v1, v3

    :goto_c
    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->e:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfi/firstbeat/coach/b/a;

    invoke-virtual {v0}, Lfi/firstbeat/coach/b/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v2, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v2, v2, Lfi/firstbeat/coach/eteintegration/CoachVars;->AC:I

    invoke-virtual {v10, v0, v2}, Lfi/firstbeat/coach/a/c;->d(II)I

    move-result v0

    const/16 v2, 0x23

    if-gt v0, v2, :cond_11

    mul-int/lit8 v0, v0, 0x3a

    add-int/lit16 v0, v0, -0x240

    :goto_d
    if-le v0, v1, :cond_e

    move v1, v0

    :cond_e
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    move v7, v1

    move-wide v1, v4

    goto :goto_a

    :cond_f
    add-int/lit8 v1, v1, -0x1

    goto :goto_7

    :cond_10
    iget-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->g:Ljava/util/GregorianCalendar;

    invoke-virtual {v1}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v1

    goto :goto_9

    :cond_11
    const/16 v2, 0x2d

    if-gt v0, v2, :cond_12

    mul-int/lit16 v0, v0, 0x90

    add-int/lit16 v0, v0, -0xe10

    goto :goto_d

    :cond_12
    mul-int/lit16 v0, v0, 0x120

    add-int/lit16 v0, v0, -0x2760

    goto :goto_d

    :cond_13
    int-to-long v4, v7

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget-object v0, v0, Lfi/firstbeat/coach/eteintegration/CoachVars;->now:Ljava/util/GregorianCalendar;

    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v6

    sub-long v0, v6, v1

    const-wide/16 v6, 0x7530

    add-long/2addr v0, v6

    const-wide/32 v6, 0xea60

    div-long/2addr v0, v6

    sub-long v0, v4, v0

    long-to-int v0, v0

    if-gez v0, :cond_15

    :goto_e
    iput v3, v9, Lfi/firstbeat/coach/eteintegration/CoachVars;->resourceRecovery:I

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->b:Lfi/firstbeat/coach/a/f;

    invoke-virtual {v0, v8}, Lfi/firstbeat/coach/a/f;->b(Lfi/firstbeat/coach/a/b;)I

    move-result v0

    if-lez v0, :cond_14

    iget-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v1, v1, Lfi/firstbeat/coach/eteintegration/CoachVars;->trainingLevel:I

    if-eq v0, v1, :cond_14

    iget-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget-object v2, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget-object v2, v2, Lfi/firstbeat/coach/eteintegration/CoachVars;->now:Ljava/util/GregorianCalendar;

    invoke-static {v2}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(Ljava/util/GregorianCalendar;)Ljava/util/GregorianCalendar;

    move-result-object v2

    iput-object v2, v1, Lfi/firstbeat/coach/eteintegration/CoachVars;->lastTrainingLevelUpdate:Ljava/util/GregorianCalendar;

    iget-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget-object v2, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v2, v2, Lfi/firstbeat/coach/eteintegration/CoachVars;->previousTrainingLevel:I

    iput v2, v1, Lfi/firstbeat/coach/eteintegration/CoachVars;->previousToPreviousTrainingLevel:I

    iget-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget-object v2, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v2, v2, Lfi/firstbeat/coach/eteintegration/CoachVars;->trainingLevel:I

    iput v2, v1, Lfi/firstbeat/coach/eteintegration/CoachVars;->previousTrainingLevel:I

    iget-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iput v0, v1, Lfi/firstbeat/coach/eteintegration/CoachVars;->trainingLevel:I

    :cond_14
    return-void

    :cond_15
    move v3, v0

    goto :goto_e

    :cond_16
    move v1, v0

    goto/16 :goto_c

    :cond_17
    move v0, v7

    goto/16 :goto_b

    :cond_18
    move v0, v1

    goto/16 :goto_8

    :cond_19
    move v0, v3

    goto/16 :goto_8

    :cond_1a
    move v0, v2

    goto/16 :goto_2
.end method


# virtual methods
.method public addAnalysedExercise(Ljava/util/GregorianCalendar;Lfi/firstbeat/ete/ETEresults;D)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lfi/firstbeat/coach/eteintegration/CoachException;
        }
    .end annotation

    const-wide/high16 v3, 0x40f0000000000000L    # 65536.0

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    if-nez v0, :cond_0

    new-instance v0, Lfi/firstbeat/coach/eteintegration/CoachException;

    const-string v1, "Personal background parameters are not set."

    invoke-direct {v0, v1}, Lfi/firstbeat/coach/eteintegration/CoachException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {p1}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(Ljava/util/GregorianCalendar;)Ljava/util/GregorianCalendar;

    move-result-object v0

    iget v1, p2, Lfi/firstbeat/ete/ETEresults;->ETEtrainingLoadPeak:I

    int-to-double v1, v1

    div-double/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->round(D)J

    move-result-wide v1

    long-to-int v1, v1

    invoke-direct {p0, v0, v1, p3, p4}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(Ljava/util/GregorianCalendar;ID)V

    iget-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->g:Ljava/util/GregorianCalendar;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->g:Ljava/util/GregorianCalendar;

    invoke-virtual {v1, v0}, Ljava/util/GregorianCalendar;->before(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    iput-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->g:Ljava/util/GregorianCalendar;

    iget v1, p2, Lfi/firstbeat/ete/ETEresults;->ETEresourceRecovery:I

    iput v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->h:I

    :cond_2
    iget v1, p2, Lfi/firstbeat/ete/ETEresults;->ETEmaximalMET:I

    if-lez v1, :cond_6

    invoke-static {v0}, Lfi/firstbeat/coach/a/f;->a(Ljava/util/GregorianCalendar;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1c

    iget-object v2, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget-object v2, v2, Lfi/firstbeat/coach/eteintegration/CoachVars;->now:Ljava/util/GregorianCalendar;

    invoke-static {v2}, Lfi/firstbeat/coach/a/f;->a(Ljava/util/GregorianCalendar;)I

    move-result v2

    if-gt v1, v2, :cond_4

    iget-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->k:Ljava/util/GregorianCalendar;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->k:Ljava/util/GregorianCalendar;

    invoke-virtual {v1, v0}, Ljava/util/GregorianCalendar;->before(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_3
    iput-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->k:Ljava/util/GregorianCalendar;

    iget v1, p2, Lfi/firstbeat/ete/ETEresults;->ETEmaximalMET:I

    int-to-double v1, v1

    div-double/2addr v1, v3

    iput-wide v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->l:D

    :cond_4
    iget-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->m:Ljava/util/GregorianCalendar;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->m:Ljava/util/GregorianCalendar;

    invoke-virtual {v1, v0}, Ljava/util/GregorianCalendar;->before(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    :cond_5
    iget-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->m:Ljava/util/GregorianCalendar;

    iput-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->o:Ljava/util/GregorianCalendar;

    iget-wide v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->n:D

    iput-wide v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->p:D

    iput-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->m:Ljava/util/GregorianCalendar;

    iget v0, p2, Lfi/firstbeat/ete/ETEresults;->ETEmaximalMET:I

    int-to-double v0, v0

    div-double/2addr v0, v3

    iput-wide v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->n:D

    :cond_6
    :goto_0
    return-void

    :cond_7
    iget-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->o:Ljava/util/GregorianCalendar;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->o:Ljava/util/GregorianCalendar;

    invoke-virtual {v1, v0}, Ljava/util/GregorianCalendar;->before(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    :cond_8
    iput-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->o:Ljava/util/GregorianCalendar;

    iget v0, p2, Lfi/firstbeat/ete/ETEresults;->ETEmaximalMET:I

    int-to-double v0, v0

    div-double/2addr v0, v3

    iput-wide v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->p:D

    goto :goto_0
.end method

.method public addEnergyExercise(Ljava/util/GregorianCalendar;III)I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lfi/firstbeat/coach/eteintegration/CoachException;
        }
    .end annotation

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    if-nez v0, :cond_0

    new-instance v0, Lfi/firstbeat/coach/eteintegration/CoachException;

    const-string v1, "Personal background parameters are not set."

    invoke-direct {v0, v1}, Lfi/firstbeat/coach/eteintegration/CoachException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-gtz p2, :cond_1

    new-instance v0, Lfi/firstbeat/coach/eteintegration/CoachException;

    const-string v1, "Duration is invalid"

    invoke-direct {v0, v1}, Lfi/firstbeat/coach/eteintegration/CoachException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-gtz p3, :cond_2

    new-instance v0, Lfi/firstbeat/coach/eteintegration/CoachException;

    const-string v1, "Energy expenditure is invalid"

    invoke-direct {v0, v1}, Lfi/firstbeat/coach/eteintegration/CoachException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    int-to-double v0, p3

    int-to-double v2, p2

    div-double/2addr v0, v2

    invoke-direct {p0}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->b()D

    move-result-wide v2

    div-double/2addr v0, v2

    iget-object v2, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v2, v2, Lfi/firstbeat/coach/eteintegration/CoachVars;->weight:I

    int-to-double v2, v2

    div-double/2addr v0, v2

    const-wide v2, 0x40d3880000000000L    # 20000.0

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    if-gtz p4, :cond_3

    iget-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->c:Lfi/firstbeat/coach/a/h;

    invoke-virtual {v1, p2, v0}, Lfi/firstbeat/coach/a/h;->a(II)D

    move-result-wide v0

    double-to-int p4, v0

    :cond_3
    invoke-static {p1}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(Ljava/util/GregorianCalendar;)Ljava/util/GregorianCalendar;

    move-result-object v0

    const-wide/16 v1, 0x0

    invoke-direct {p0, v0, p4, v1, v2}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(Ljava/util/GregorianCalendar;ID)V

    return p4
.end method

.method public addRunningExercise(Ljava/util/GregorianCalendar;IDI)I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lfi/firstbeat/coach/eteintegration/CoachException;
        }
    .end annotation

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    if-nez v0, :cond_0

    new-instance v0, Lfi/firstbeat/coach/eteintegration/CoachException;

    const-string v1, "Personal background parameters are not set."

    invoke-direct {v0, v1}, Lfi/firstbeat/coach/eteintegration/CoachException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-gtz p2, :cond_1

    new-instance v0, Lfi/firstbeat/coach/eteintegration/CoachException;

    const-string v1, "Duration is invalid"

    invoke-direct {v0, v1}, Lfi/firstbeat/coach/eteintegration/CoachException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const-wide/16 v0, 0x0

    cmpg-double v0, p3, v0

    if-gtz v0, :cond_2

    new-instance v0, Lfi/firstbeat/coach/eteintegration/CoachException;

    const-string v1, "Distance is invalid"

    invoke-direct {v0, v1}, Lfi/firstbeat/coach/eteintegration/CoachException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    invoke-static {p1}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(Ljava/util/GregorianCalendar;)Ljava/util/GregorianCalendar;

    move-result-object v0

    iget-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->c:Lfi/firstbeat/coach/a/h;

    invoke-direct {p0}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->b()D

    move-result-wide v1

    invoke-static {v1, v2, p2, p3, p4}, Lfi/firstbeat/coach/a/h;->a(DID)I

    move-result v1

    if-gtz p5, :cond_3

    iget-object v2, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->c:Lfi/firstbeat/coach/a/h;

    invoke-virtual {v2, p2, v1}, Lfi/firstbeat/coach/a/h;->a(II)D

    move-result-wide v1

    double-to-int p5, v1

    :cond_3
    invoke-direct {p0, v0, p5, p3, p4}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(Ljava/util/GregorianCalendar;ID)V

    return p5
.end method

.method public addUserExercise(Ljava/util/GregorianCalendar;ILfi/firstbeat/coach/eteintegration/ExerciseIntensity;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lfi/firstbeat/coach/eteintegration/CoachException;
        }
    .end annotation

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    if-nez v0, :cond_0

    new-instance v0, Lfi/firstbeat/coach/eteintegration/CoachException;

    const-string v1, "Personal background parameters are not set."

    invoke-direct {v0, v1}, Lfi/firstbeat/coach/eteintegration/CoachException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-gtz p2, :cond_1

    new-instance v0, Lfi/firstbeat/coach/eteintegration/CoachException;

    const-string v1, "Duration is invalid"

    invoke-direct {v0, v1}, Lfi/firstbeat/coach/eteintegration/CoachException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-nez p3, :cond_2

    new-instance v0, Lfi/firstbeat/coach/eteintegration/CoachException;

    const-string v1, "Intensity is invalid"

    invoke-direct {v0, v1}, Lfi/firstbeat/coach/eteintegration/CoachException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    const/4 v0, 0x0

    sget-object v1, Lfi/firstbeat/coach/eteintegration/CoachImpl$1;->a:[I

    invoke-virtual {p3}, Lfi/firstbeat/coach/eteintegration/ExerciseIntensity;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :goto_0
    iget-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->c:Lfi/firstbeat/coach/a/h;

    invoke-virtual {v1, p2, v0}, Lfi/firstbeat/coach/a/h;->a(II)D

    move-result-wide v0

    double-to-int v0, v0

    invoke-static {p1}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(Ljava/util/GregorianCalendar;)Ljava/util/GregorianCalendar;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-direct {p0, v1, v0, v2, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(Ljava/util/GregorianCalendar;ID)V

    return-void

    :pswitch_0
    const/16 v0, 0x23

    goto :goto_0

    :pswitch_1
    const/16 v0, 0x2d

    goto :goto_0

    :pswitch_2
    const/16 v0, 0x3c

    goto :goto_0

    :pswitch_3
    const/16 v0, 0x4b

    goto :goto_0

    :pswitch_4
    const/16 v0, 0x5a

    goto :goto_0

    :pswitch_5
    const/16 v0, 0x64

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public assertValidParameters(Lfi/firstbeat/coach/eteintegration/CoachVars;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lfi/firstbeat/coach/eteintegration/CoachException;
        }
    .end annotation

    const/16 v3, 0xfa

    const/4 v4, 0x2

    const/16 v2, 0x64

    if-nez p1, :cond_0

    new-instance v0, Lfi/firstbeat/coach/eteintegration/CoachException;

    const-string v1, "Personal background parameters are not set."

    invoke-direct {v0, v1}, Lfi/firstbeat/coach/eteintegration/CoachException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget v0, p1, Lfi/firstbeat/coach/eteintegration/CoachVars;->age:I

    const/16 v1, 0x8

    if-lt v0, v1, :cond_1

    iget v0, p1, Lfi/firstbeat/coach/eteintegration/CoachVars;->age:I

    const/16 v1, 0x6e

    if-le v0, v1, :cond_2

    :cond_1
    new-instance v0, Lfi/firstbeat/coach/eteintegration/CoachException;

    const-string v1, "Age is invalid"

    invoke-direct {v0, v1}, Lfi/firstbeat/coach/eteintegration/CoachException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget v0, p1, Lfi/firstbeat/coach/eteintegration/CoachVars;->height:I

    if-lt v0, v2, :cond_3

    iget v0, p1, Lfi/firstbeat/coach/eteintegration/CoachVars;->height:I

    if-le v0, v3, :cond_4

    :cond_3
    new-instance v0, Lfi/firstbeat/coach/eteintegration/CoachException;

    const-string v1, "Height is invalid"

    invoke-direct {v0, v1}, Lfi/firstbeat/coach/eteintegration/CoachException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    iget v0, p1, Lfi/firstbeat/coach/eteintegration/CoachVars;->height:I

    const/16 v1, 0x23

    if-lt v0, v1, :cond_5

    iget v0, p1, Lfi/firstbeat/coach/eteintegration/CoachVars;->height:I

    if-le v0, v3, :cond_6

    :cond_5
    new-instance v0, Lfi/firstbeat/coach/eteintegration/CoachException;

    const-string v1, "Weight is invalid"

    invoke-direct {v0, v1}, Lfi/firstbeat/coach/eteintegration/CoachException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    iget v0, p1, Lfi/firstbeat/coach/eteintegration/CoachVars;->gender:I

    if-lez v0, :cond_7

    iget v0, p1, Lfi/firstbeat/coach/eteintegration/CoachVars;->gender:I

    if-le v0, v4, :cond_8

    :cond_7
    new-instance v0, Lfi/firstbeat/coach/eteintegration/CoachException;

    const-string v1, "Gender is invalid"

    invoke-direct {v0, v1}, Lfi/firstbeat/coach/eteintegration/CoachException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    iget v0, p1, Lfi/firstbeat/coach/eteintegration/CoachVars;->AC:I

    if-eqz v0, :cond_9

    iget v0, p1, Lfi/firstbeat/coach/eteintegration/CoachVars;->AC:I

    const/16 v1, 0xa

    if-eq v0, v1, :cond_9

    iget v0, p1, Lfi/firstbeat/coach/eteintegration/CoachVars;->AC:I

    const/16 v1, 0x14

    if-eq v0, v1, :cond_9

    iget v0, p1, Lfi/firstbeat/coach/eteintegration/CoachVars;->AC:I

    const/16 v1, 0x1e

    if-eq v0, v1, :cond_9

    iget v0, p1, Lfi/firstbeat/coach/eteintegration/CoachVars;->AC:I

    const/16 v1, 0x28

    if-eq v0, v1, :cond_9

    iget v0, p1, Lfi/firstbeat/coach/eteintegration/CoachVars;->AC:I

    const/16 v1, 0x32

    if-eq v0, v1, :cond_9

    iget v0, p1, Lfi/firstbeat/coach/eteintegration/CoachVars;->AC:I

    const/16 v1, 0x3c

    if-eq v0, v1, :cond_9

    iget v0, p1, Lfi/firstbeat/coach/eteintegration/CoachVars;->AC:I

    const/16 v1, 0x46

    if-eq v0, v1, :cond_9

    iget v0, p1, Lfi/firstbeat/coach/eteintegration/CoachVars;->AC:I

    const/16 v1, 0x4b

    if-eq v0, v1, :cond_9

    iget v0, p1, Lfi/firstbeat/coach/eteintegration/CoachVars;->AC:I

    const/16 v1, 0x50

    if-eq v0, v1, :cond_9

    iget v0, p1, Lfi/firstbeat/coach/eteintegration/CoachVars;->AC:I

    const/16 v1, 0x55

    if-eq v0, v1, :cond_9

    iget v0, p1, Lfi/firstbeat/coach/eteintegration/CoachVars;->AC:I

    const/16 v1, 0x5a

    if-eq v0, v1, :cond_9

    iget v0, p1, Lfi/firstbeat/coach/eteintegration/CoachVars;->AC:I

    const/16 v1, 0x5f

    if-eq v0, v1, :cond_9

    iget v0, p1, Lfi/firstbeat/coach/eteintegration/CoachVars;->AC:I

    if-eq v0, v2, :cond_9

    new-instance v0, Lfi/firstbeat/coach/eteintegration/CoachException;

    const-string v1, "Activity class is invalid"

    invoke-direct {v0, v1}, Lfi/firstbeat/coach/eteintegration/CoachException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_9
    iget v0, p1, Lfi/firstbeat/coach/eteintegration/CoachVars;->trainingLevel:I

    const/4 v1, -0x1

    if-lt v0, v1, :cond_a

    iget v0, p1, Lfi/firstbeat/coach/eteintegration/CoachVars;->trainingLevel:I

    const/4 v1, 0x7

    if-le v0, v1, :cond_b

    :cond_a
    new-instance v0, Lfi/firstbeat/coach/eteintegration/CoachException;

    const-string v1, "Training level is invalid"

    invoke-direct {v0, v1}, Lfi/firstbeat/coach/eteintegration/CoachException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_b
    iget v0, p1, Lfi/firstbeat/coach/eteintegration/CoachVars;->maxHr:I

    if-eqz v0, :cond_d

    iget v0, p1, Lfi/firstbeat/coach/eteintegration/CoachVars;->maxHr:I

    if-lt v0, v2, :cond_c

    iget v0, p1, Lfi/firstbeat/coach/eteintegration/CoachVars;->maxHr:I

    const/16 v1, 0xf0

    if-le v0, v1, :cond_d

    :cond_c
    new-instance v0, Lfi/firstbeat/coach/eteintegration/CoachException;

    const-string v1, "Maximal heart rate is invalid"

    invoke-direct {v0, v1}, Lfi/firstbeat/coach/eteintegration/CoachException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_d
    iget v0, p1, Lfi/firstbeat/coach/eteintegration/CoachVars;->maxMET:I

    int-to-double v0, v0

    const-wide/high16 v2, 0x40f0000000000000L    # 65536.0

    div-double/2addr v0, v2

    const-wide/16 v2, 0x0

    cmpl-double v2, v0, v2

    if-eqz v2, :cond_f

    const-wide/high16 v2, 0x4018000000000000L    # 6.0

    cmpg-double v2, v0, v2

    if-ltz v2, :cond_e

    const-wide v2, 0x4036db6ae7d566cfL    # 22.8571

    cmpl-double v0, v0, v2

    if-lez v0, :cond_f

    :cond_e
    new-instance v0, Lfi/firstbeat/coach/eteintegration/CoachException;

    const-string v1, "Maximal met is invalid"

    invoke-direct {v0, v1}, Lfi/firstbeat/coach/eteintegration/CoachException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_f
    iget v0, p1, Lfi/firstbeat/coach/eteintegration/CoachVars;->trainingGoal:I

    if-ltz v0, :cond_10

    iget v0, p1, Lfi/firstbeat/coach/eteintegration/CoachVars;->trainingGoal:I

    if-le v0, v4, :cond_11

    :cond_10
    new-instance v0, Lfi/firstbeat/coach/eteintegration/CoachException;

    const-string v1, "Training goal is invalid"

    invoke-direct {v0, v1}, Lfi/firstbeat/coach/eteintegration/CoachException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_11
    iget-object v0, p1, Lfi/firstbeat/coach/eteintegration/CoachVars;->now:Ljava/util/GregorianCalendar;

    if-nez v0, :cond_12

    new-instance v0, Lfi/firstbeat/coach/eteintegration/CoachException;

    const-string v1, "The current date is invalid"

    invoke-direct {v0, v1}, Lfi/firstbeat/coach/eteintegration/CoachException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_12
    iget-object v0, p1, Lfi/firstbeat/coach/eteintegration/CoachVars;->startDate:Ljava/util/GregorianCalendar;

    if-eqz v0, :cond_13

    iget-object v0, p1, Lfi/firstbeat/coach/eteintegration/CoachVars;->now:Ljava/util/GregorianCalendar;

    iget-object v1, p1, Lfi/firstbeat/coach/eteintegration/CoachVars;->startDate:Ljava/util/GregorianCalendar;

    invoke-virtual {v0, v1}, Ljava/util/GregorianCalendar;->before(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    :cond_13
    new-instance v0, Lfi/firstbeat/coach/eteintegration/CoachException;

    const-string v1, "Start date is invalid"

    invoke-direct {v0, v1}, Lfi/firstbeat/coach/eteintegration/CoachException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_14
    iget-object v0, p1, Lfi/firstbeat/coach/eteintegration/CoachVars;->lastTrainingLevelUpdate:Ljava/util/GregorianCalendar;

    if-eqz v0, :cond_15

    iget-object v0, p1, Lfi/firstbeat/coach/eteintegration/CoachVars;->lastTrainingLevelUpdate:Ljava/util/GregorianCalendar;

    iget-object v1, p1, Lfi/firstbeat/coach/eteintegration/CoachVars;->startDate:Ljava/util/GregorianCalendar;

    invoke-virtual {v0, v1}, Ljava/util/GregorianCalendar;->before(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    new-instance v0, Lfi/firstbeat/coach/eteintegration/CoachException;

    const-string v1, "Last training level update date is invalid"

    invoke-direct {v0, v1}, Lfi/firstbeat/coach/eteintegration/CoachException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_15
    return-void
.end method

.method public getFeedbackPhraseNumber()I
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lfi/firstbeat/coach/eteintegration/CoachException;
        }
    .end annotation

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    if-nez v0, :cond_0

    new-instance v0, Lfi/firstbeat/coach/eteintegration/CoachException;

    const-string v1, "Personal background parameters are not set."

    invoke-direct {v0, v1}, Lfi/firstbeat/coach/eteintegration/CoachException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-boolean v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->f:Z

    if-nez v0, :cond_1

    invoke-direct {p0}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->c()V

    :cond_1
    invoke-virtual {p0}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->getWeeklyTrainingLoad()I

    move-result v1

    invoke-virtual {p0}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->getWeeklyTrainingLoadLowerLimit()I

    move-result v2

    invoke-virtual {p0}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->getWeeklyTrainingLoadUpperLimit()I

    move-result v3

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget-object v0, v0, Lfi/firstbeat/coach/eteintegration/CoachVars;->now:Ljava/util/GregorianCalendar;

    invoke-static {v0}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(Ljava/util/GregorianCalendar;)Ljava/util/GregorianCalendar;

    move-result-object v4

    const/16 v0, 0xb

    const/4 v5, 0x0

    invoke-virtual {v4, v0, v5}, Ljava/util/GregorianCalendar;->set(II)V

    const/16 v0, 0xc

    const/4 v5, 0x0

    invoke-virtual {v4, v0, v5}, Ljava/util/GregorianCalendar;->set(II)V

    const/16 v0, 0xd

    const/4 v5, 0x0

    invoke-virtual {v4, v0, v5}, Ljava/util/GregorianCalendar;->set(II)V

    const/16 v0, 0xe

    const/4 v5, 0x0

    invoke-virtual {v4, v0, v5}, Ljava/util/GregorianCalendar;->set(II)V

    const/4 v0, 0x5

    const/4 v5, -0x2

    invoke-virtual {v4, v0, v5}, Ljava/util/GregorianCalendar;->add(II)V

    const/4 v0, 0x0

    iget-object v5, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->i:Ljava/util/GregorianCalendar;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->i:Ljava/util/GregorianCalendar;

    invoke-virtual {v5, v4}, Ljava/util/GregorianCalendar;->before(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    const/4 v0, 0x1

    :cond_2
    iget-object v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->i:Ljava/util/GregorianCalendar;

    if-nez v4, :cond_4

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    const/4 v1, 0x1

    iput v1, v0, Lfi/firstbeat/coach/eteintegration/CoachVars;->latestFeedbackPhraseNumber:I

    :cond_3
    :goto_0
    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->i:Ljava/util/GregorianCalendar;

    iput-object v1, v0, Lfi/firstbeat/coach/eteintegration/CoachVars;->latestExerciseTime:Ljava/util/GregorianCalendar;

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v0, v0, Lfi/firstbeat/coach/eteintegration/CoachVars;->latestFeedbackPhraseNumber:I

    return v0

    :cond_4
    iget-object v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->i:Ljava/util/GregorianCalendar;

    iget-object v5, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget-object v5, v5, Lfi/firstbeat/coach/eteintegration/CoachVars;->latestExerciseTime:Ljava/util/GregorianCalendar;

    invoke-virtual {v4, v5}, Ljava/util/GregorianCalendar;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    if-nez v0, :cond_3

    :cond_5
    iget-wide v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->n:D

    const-wide/16 v6, 0x0

    cmpl-double v4, v4, v6

    if-lez v4, :cond_6

    iget-wide v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->p:D

    const-wide/16 v6, 0x0

    cmpl-double v4, v4, v6

    if-lez v4, :cond_6

    iget-wide v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->n:D

    invoke-direct {p0, v4, v5}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(D)I

    move-result v4

    iget-wide v5, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->p:D

    invoke-direct {p0, v5, v6}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(D)I

    move-result v5

    if-le v4, v5, :cond_6

    iget-object v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v4, v4, Lfi/firstbeat/coach/eteintegration/CoachVars;->latestFeedbackPhraseNumber:I

    const/4 v5, 0x2

    if-eq v4, v5, :cond_6

    iget-object v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v4, v4, Lfi/firstbeat/coach/eteintegration/CoachVars;->latestFeedbackPhraseNumber:I

    const/4 v5, 0x3

    if-eq v4, v5, :cond_6

    if-eqz v0, :cond_6

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    const/4 v1, 0x2

    iput v1, v0, Lfi/firstbeat/coach/eteintegration/CoachVars;->latestFeedbackPhraseNumber:I

    goto :goto_0

    :cond_6
    iget-wide v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->n:D

    const-wide/16 v6, 0x0

    cmpl-double v4, v4, v6

    if-lez v4, :cond_7

    iget-wide v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->p:D

    const-wide/16 v6, 0x0

    cmpl-double v4, v4, v6

    if-lez v4, :cond_7

    iget-wide v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->n:D

    iget-wide v6, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->p:D

    cmpl-double v4, v4, v6

    if-lez v4, :cond_7

    iget-wide v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->l:D

    const-wide/16 v6, 0x0

    cmpl-double v4, v4, v6

    if-lez v4, :cond_7

    iget-wide v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->n:D

    const-wide v6, 0x3ff07ae147ae147bL    # 1.03

    iget-wide v8, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->l:D

    mul-double/2addr v6, v8

    cmpl-double v4, v4, v6

    if-lez v4, :cond_7

    iget-object v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v4, v4, Lfi/firstbeat/coach/eteintegration/CoachVars;->latestFeedbackPhraseNumber:I

    const/4 v5, 0x2

    if-eq v4, v5, :cond_7

    iget-object v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v4, v4, Lfi/firstbeat/coach/eteintegration/CoachVars;->latestFeedbackPhraseNumber:I

    const/4 v5, 0x3

    if-eq v4, v5, :cond_7

    if-eqz v0, :cond_7

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    const/4 v1, 0x3

    iput v1, v0, Lfi/firstbeat/coach/eteintegration/CoachVars;->latestFeedbackPhraseNumber:I

    goto/16 :goto_0

    :cond_7
    iget-wide v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->n:D

    const-wide v6, 0x4021249249249249L    # 8.571428571428571

    cmpl-double v4, v4, v6

    if-lez v4, :cond_9

    iget-wide v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->j:D

    const-wide/high16 v6, 0x402e000000000000L    # 15.0

    cmpl-double v4, v4, v6

    if-ltz v4, :cond_9

    iget-object v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v4, v4, Lfi/firstbeat/coach/eteintegration/CoachVars;->latestFeedbackPhraseNumber:I

    const/4 v5, 0x4

    if-lt v4, v5, :cond_8

    iget-object v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v4, v4, Lfi/firstbeat/coach/eteintegration/CoachVars;->latestFeedbackPhraseNumber:I

    const/16 v5, 0x8

    if-le v4, v5, :cond_9

    :cond_8
    if-eqz v0, :cond_9

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    const/4 v1, 0x4

    iput v1, v0, Lfi/firstbeat/coach/eteintegration/CoachVars;->latestFeedbackPhraseNumber:I

    goto/16 :goto_0

    :cond_9
    iget-wide v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->n:D

    const-wide v6, 0x4021249249249249L    # 8.571428571428571

    cmpl-double v4, v4, v6

    if-lez v4, :cond_b

    iget-wide v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->j:D

    const-wide/high16 v6, 0x4024000000000000L    # 10.0

    cmpl-double v4, v4, v6

    if-ltz v4, :cond_b

    iget-object v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v4, v4, Lfi/firstbeat/coach/eteintegration/CoachVars;->latestFeedbackPhraseNumber:I

    const/4 v5, 0x4

    if-lt v4, v5, :cond_a

    iget-object v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v4, v4, Lfi/firstbeat/coach/eteintegration/CoachVars;->latestFeedbackPhraseNumber:I

    const/16 v5, 0x8

    if-le v4, v5, :cond_b

    :cond_a
    if-eqz v0, :cond_b

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    const/4 v1, 0x5

    iput v1, v0, Lfi/firstbeat/coach/eteintegration/CoachVars;->latestFeedbackPhraseNumber:I

    goto/16 :goto_0

    :cond_b
    iget-wide v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->n:D

    const-wide v6, 0x4021249249249249L    # 8.571428571428571

    cmpl-double v4, v4, v6

    if-lez v4, :cond_d

    iget-wide v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->j:D

    const-wide/high16 v6, 0x4014000000000000L    # 5.0

    cmpl-double v4, v4, v6

    if-ltz v4, :cond_d

    iget-object v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v4, v4, Lfi/firstbeat/coach/eteintegration/CoachVars;->latestFeedbackPhraseNumber:I

    const/4 v5, 0x4

    if-lt v4, v5, :cond_c

    iget-object v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v4, v4, Lfi/firstbeat/coach/eteintegration/CoachVars;->latestFeedbackPhraseNumber:I

    const/16 v5, 0x8

    if-le v4, v5, :cond_d

    :cond_c
    if-eqz v0, :cond_d

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    const/4 v1, 0x6

    iput v1, v0, Lfi/firstbeat/coach/eteintegration/CoachVars;->latestFeedbackPhraseNumber:I

    goto/16 :goto_0

    :cond_d
    iget-wide v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->n:D

    const-wide v6, 0x4021249249249249L    # 8.571428571428571

    cmpl-double v4, v4, v6

    if-lez v4, :cond_f

    iget-wide v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->j:D

    const-wide/high16 v6, 0x4008000000000000L    # 3.0

    cmpl-double v4, v4, v6

    if-ltz v4, :cond_f

    iget-object v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v4, v4, Lfi/firstbeat/coach/eteintegration/CoachVars;->latestFeedbackPhraseNumber:I

    const/4 v5, 0x4

    if-lt v4, v5, :cond_e

    iget-object v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v4, v4, Lfi/firstbeat/coach/eteintegration/CoachVars;->latestFeedbackPhraseNumber:I

    const/16 v5, 0x8

    if-le v4, v5, :cond_f

    :cond_e
    if-eqz v0, :cond_f

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    const/4 v1, 0x7

    iput v1, v0, Lfi/firstbeat/coach/eteintegration/CoachVars;->latestFeedbackPhraseNumber:I

    goto/16 :goto_0

    :cond_f
    iget-wide v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->n:D

    const-wide v6, 0x4021249249249249L    # 8.571428571428571

    cmpl-double v4, v4, v6

    if-lez v4, :cond_11

    iget-wide v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->j:D

    const-wide/16 v6, 0x0

    cmpl-double v4, v4, v6

    if-lez v4, :cond_11

    iget-object v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v4, v4, Lfi/firstbeat/coach/eteintegration/CoachVars;->latestFeedbackPhraseNumber:I

    const/4 v5, 0x4

    if-lt v4, v5, :cond_10

    iget-object v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v4, v4, Lfi/firstbeat/coach/eteintegration/CoachVars;->latestFeedbackPhraseNumber:I

    const/16 v5, 0x8

    if-le v4, v5, :cond_11

    :cond_10
    if-eqz v0, :cond_11

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    const/16 v1, 0x8

    iput v1, v0, Lfi/firstbeat/coach/eteintegration/CoachVars;->latestFeedbackPhraseNumber:I

    goto/16 :goto_0

    :cond_11
    iget-object v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget-object v4, v4, Lfi/firstbeat/coach/eteintegration/CoachVars;->lastTrainingLevelUpdate:Ljava/util/GregorianCalendar;

    if-eqz v4, :cond_12

    iget-object v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget-object v4, v4, Lfi/firstbeat/coach/eteintegration/CoachVars;->lastTrainingLevelUpdate:Ljava/util/GregorianCalendar;

    iget-object v5, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->i:Ljava/util/GregorianCalendar;

    invoke-virtual {v4, v5}, Ljava/util/GregorianCalendar;->before(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_12

    iget-object v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v4, v4, Lfi/firstbeat/coach/eteintegration/CoachVars;->trainingLevel:I

    if-ltz v4, :cond_12

    iget-object v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v4, v4, Lfi/firstbeat/coach/eteintegration/CoachVars;->trainingLevel:I

    const/4 v5, 0x5

    if-ge v4, v5, :cond_12

    iget-object v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v4, v4, Lfi/firstbeat/coach/eteintegration/CoachVars;->previousTrainingLevel:I

    if-ltz v4, :cond_12

    iget-object v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v4, v4, Lfi/firstbeat/coach/eteintegration/CoachVars;->previousTrainingLevel:I

    const/4 v5, 0x5

    if-ge v4, v5, :cond_12

    iget-object v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v4, v4, Lfi/firstbeat/coach/eteintegration/CoachVars;->trainingLevel:I

    iget-object v5, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v5, v5, Lfi/firstbeat/coach/eteintegration/CoachVars;->previousTrainingLevel:I

    if-le v4, v5, :cond_12

    if-eqz v0, :cond_12

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    const/16 v1, 0x9

    iput v1, v0, Lfi/firstbeat/coach/eteintegration/CoachVars;->latestFeedbackPhraseNumber:I

    goto/16 :goto_0

    :cond_12
    iget-object v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget-object v4, v4, Lfi/firstbeat/coach/eteintegration/CoachVars;->lastTrainingLevelUpdate:Ljava/util/GregorianCalendar;

    if-eqz v4, :cond_13

    iget-object v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget-object v4, v4, Lfi/firstbeat/coach/eteintegration/CoachVars;->lastTrainingLevelUpdate:Ljava/util/GregorianCalendar;

    iget-object v5, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->i:Ljava/util/GregorianCalendar;

    invoke-virtual {v4, v5}, Ljava/util/GregorianCalendar;->before(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_13

    iget-object v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v4, v4, Lfi/firstbeat/coach/eteintegration/CoachVars;->trainingLevel:I

    if-ltz v4, :cond_13

    iget-object v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v4, v4, Lfi/firstbeat/coach/eteintegration/CoachVars;->trainingLevel:I

    const/4 v5, 0x5

    if-ge v4, v5, :cond_13

    iget-object v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v4, v4, Lfi/firstbeat/coach/eteintegration/CoachVars;->previousToPreviousTrainingLevel:I

    if-ltz v4, :cond_13

    iget-object v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v4, v4, Lfi/firstbeat/coach/eteintegration/CoachVars;->previousToPreviousTrainingLevel:I

    const/4 v5, 0x5

    if-ge v4, v5, :cond_13

    iget-object v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v4, v4, Lfi/firstbeat/coach/eteintegration/CoachVars;->trainingLevel:I

    add-int/lit8 v4, v4, 0x1

    iget-object v5, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v5, v5, Lfi/firstbeat/coach/eteintegration/CoachVars;->previousToPreviousTrainingLevel:I

    if-ge v4, v5, :cond_13

    if-eqz v0, :cond_13

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    const/16 v1, 0xa

    iput v1, v0, Lfi/firstbeat/coach/eteintegration/CoachVars;->latestFeedbackPhraseNumber:I

    goto/16 :goto_0

    :cond_13
    iget-object v4, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget-object v4, v4, Lfi/firstbeat/coach/eteintegration/CoachVars;->now:Ljava/util/GregorianCalendar;

    invoke-static {v4}, Lfi/firstbeat/coach/a/f;->a(Ljava/util/GregorianCalendar;)I

    move-result v4

    iget-object v5, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget-object v5, v5, Lfi/firstbeat/coach/eteintegration/CoachVars;->startDate:Ljava/util/GregorianCalendar;

    invoke-static {v5}, Lfi/firstbeat/coach/a/f;->a(Ljava/util/GregorianCalendar;)I

    move-result v5

    sub-int/2addr v4, v5

    const/4 v5, 0x7

    if-ge v4, v5, :cond_14

    if-eqz v0, :cond_14

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v0, v0, Lfi/firstbeat/coach/eteintegration/CoachVars;->latestFeedbackPhraseNumber:I

    const/4 v1, 0x1

    if-gt v0, v1, :cond_3

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    const/16 v1, 0xc

    iput v1, v0, Lfi/firstbeat/coach/eteintegration/CoachVars;->latestFeedbackPhraseNumber:I

    goto/16 :goto_0

    :cond_14
    if-le v1, v3, :cond_15

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    const/16 v1, 0xd

    iput v1, v0, Lfi/firstbeat/coach/eteintegration/CoachVars;->latestFeedbackPhraseNumber:I

    goto/16 :goto_0

    :cond_15
    if-ge v1, v2, :cond_16

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    const/16 v1, 0xe

    iput v1, v0, Lfi/firstbeat/coach/eteintegration/CoachVars;->latestFeedbackPhraseNumber:I

    goto/16 :goto_0

    :cond_16
    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    const/16 v1, 0xf

    iput v1, v0, Lfi/firstbeat/coach/eteintegration/CoachVars;->latestFeedbackPhraseNumber:I

    goto/16 :goto_0
.end method

.method public getFitnessClass()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lfi/firstbeat/coach/eteintegration/CoachException;
        }
    .end annotation

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    if-nez v0, :cond_0

    new-instance v0, Lfi/firstbeat/coach/eteintegration/CoachException;

    const-string v1, "Personal background parameters are not set."

    invoke-direct {v0, v1}, Lfi/firstbeat/coach/eteintegration/CoachException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-direct {p0}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->b()D

    move-result-wide v0

    const-wide/high16 v2, 0x400c000000000000L    # 3.5

    div-double/2addr v0, v2

    invoke-direct {p0, v0, v1}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(D)I

    move-result v0

    return v0
.end method

.method public getFitnessLevelIncreaseIn28d()Ljava/lang/Integer;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lfi/firstbeat/coach/eteintegration/CoachException;
        }
    .end annotation

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    const-wide/16 v2, 0x0

    iget-wide v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->n:D

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    iget-wide v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->l:D

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    iget-wide v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->n:D

    iget-wide v2, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->l:D

    div-double/2addr v0, v2

    mul-double/2addr v0, v4

    sub-double/2addr v0, v4

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFitnessThresholds()[I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lfi/firstbeat/coach/eteintegration/CoachException;
        }
    .end annotation

    const/4 v5, 0x6

    const/4 v1, 0x0

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    if-nez v0, :cond_0

    new-instance v0, Lfi/firstbeat/coach/eteintegration/CoachException;

    const-string v1, "Personal background parameters are not set."

    invoke-direct {v0, v1}, Lfi/firstbeat/coach/eteintegration/CoachException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v3, v0, Lfi/firstbeat/coach/eteintegration/CoachVars;->age:I

    sget-object v0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->q:[I

    array-length v2, v0

    move v0, v1

    :goto_0
    sget-object v4, Lfi/firstbeat/coach/eteintegration/CoachImpl;->q:[I

    array-length v4, v4

    if-ge v0, v4, :cond_3

    sget-object v4, Lfi/firstbeat/coach/eteintegration/CoachImpl;->q:[I

    aget v4, v4, v0

    if-gt v3, v4, :cond_1

    :goto_1
    iget-object v2, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v2, v2, Lfi/firstbeat/coach/eteintegration/CoachVars;->gender:I

    new-array v3, v5, [I

    const/4 v4, 0x1

    if-le v2, v4, :cond_2

    sget-object v2, Lfi/firstbeat/coach/eteintegration/CoachImpl;->r:[I

    :goto_2
    mul-int/lit8 v0, v0, 0x6

    invoke-static {v2, v0, v3, v1, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v3

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    sget-object v2, Lfi/firstbeat/coach/eteintegration/CoachImpl;->s:[I

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_1
.end method

.method public getMonthlyLoad()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lfi/firstbeat/coach/eteintegration/CoachException;
        }
    .end annotation

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    if-nez v0, :cond_0

    new-instance v0, Lfi/firstbeat/coach/eteintegration/CoachException;

    const-string v1, "Personal background parameters are not set."

    invoke-direct {v0, v1}, Lfi/firstbeat/coach/eteintegration/CoachException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-boolean v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->f:Z

    if-nez v0, :cond_1

    invoke-direct {p0}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->c()V

    :cond_1
    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v0, v0, Lfi/firstbeat/coach/eteintegration/CoachVars;->monthlyLoad:I

    return v0
.end method

.method public getNextWorkout(ZI)Lfi/firstbeat/coach/eteintegration/TrainingProgramWorkout;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lfi/firstbeat/coach/eteintegration/CoachException;
        }
    .end annotation

    const/4 v0, 0x1

    invoke-virtual {p0, v0, p1, p2}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->getWorkoutProgram(IZI)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfi/firstbeat/coach/eteintegration/TrainingProgramWorkout;

    return-object v0
.end method

.method public getParameters()Lfi/firstbeat/coach/eteintegration/CoachVars;
    .locals 1

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    return-object v0
.end method

.method public getRecommendationExceededBy()I
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lfi/firstbeat/coach/eteintegration/CoachException;
        }
    .end annotation

    const-wide/high16 v5, 0x4059000000000000L    # 100.0

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    if-nez v0, :cond_0

    new-instance v0, Lfi/firstbeat/coach/eteintegration/CoachException;

    const-string v1, "Personal background parameters are not set."

    invoke-direct {v0, v1}, Lfi/firstbeat/coach/eteintegration/CoachException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->getWeeklyTrainingLoad()I

    move-result v0

    invoke-virtual {p0}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->getWeeklyTrainingLoadLowerLimit()I

    move-result v1

    invoke-virtual {p0}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->getWeeklyTrainingLoadUpperLimit()I

    move-result v2

    add-int/2addr v1, v2

    int-to-double v1, v1

    const-wide/high16 v3, 0x4000000000000000L    # 2.0

    div-double/2addr v1, v3

    int-to-double v3, v0

    div-double v0, v3, v1

    mul-double/2addr v0, v5

    sub-double/2addr v0, v5

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public getResourceRecovery()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lfi/firstbeat/coach/eteintegration/CoachException;
        }
    .end annotation

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    if-nez v0, :cond_0

    new-instance v0, Lfi/firstbeat/coach/eteintegration/CoachException;

    const-string v1, "Personal background parameters are not set."

    invoke-direct {v0, v1}, Lfi/firstbeat/coach/eteintegration/CoachException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-boolean v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->f:Z

    if-nez v0, :cond_1

    invoke-direct {p0}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->c()V

    :cond_1
    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v0, v0, Lfi/firstbeat/coach/eteintegration/CoachVars;->resourceRecovery:I

    return v0
.end method

.method public getRunningPerformance()[I
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lfi/firstbeat/coach/eteintegration/CoachException;
        }
    .end annotation

    const/4 v10, 0x5

    const/4 v1, 0x0

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    if-nez v0, :cond_0

    new-instance v0, Lfi/firstbeat/coach/eteintegration/CoachException;

    const-string v1, "Personal background parameters are not set."

    invoke-direct {v0, v1}, Lfi/firstbeat/coach/eteintegration/CoachException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-direct {p0}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->b()D

    move-result-wide v5

    new-array v7, v10, [I

    sget-object v0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->t:[D

    array-length v0, v0

    add-int/lit8 v3, v0, -0x1

    move v0, v1

    move v2, v1

    :goto_0
    sget-object v4, Lfi/firstbeat/coach/eteintegration/CoachImpl;->t:[D

    array-length v4, v4

    if-ge v0, v4, :cond_5

    sget-object v4, Lfi/firstbeat/coach/eteintegration/CoachImpl;->t:[D

    aget-wide v8, v4, v0

    cmpg-double v4, v8, v5

    if-gtz v4, :cond_1

    move v2, v0

    :cond_1
    sget-object v4, Lfi/firstbeat/coach/eteintegration/CoachImpl;->t:[D

    aget-wide v8, v4, v0

    cmpl-double v4, v8, v5

    if-ltz v4, :cond_2

    move v4, v2

    :goto_1
    if-ne v0, v4, :cond_3

    const-wide/16 v2, 0x0

    :goto_2
    if-ge v1, v10, :cond_4

    sget-object v5, Lfi/firstbeat/coach/eteintegration/CoachImpl;->u:[I

    mul-int/lit8 v6, v4, 0x5

    add-int/2addr v6, v1

    aget v5, v5, v6

    int-to-double v5, v5

    sget-object v8, Lfi/firstbeat/coach/eteintegration/CoachImpl;->u:[I

    mul-int/lit8 v9, v0, 0x5

    add-int/2addr v9, v1

    aget v8, v8, v9

    int-to-double v8, v8

    sub-double/2addr v8, v5

    mul-double/2addr v8, v2

    add-double/2addr v5, v8

    invoke-static {v5, v6}, Ljava/lang/Math;->round(D)J

    move-result-wide v5

    long-to-int v5, v5

    aput v5, v7, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    sget-object v2, Lfi/firstbeat/coach/eteintegration/CoachImpl;->t:[D

    aget-wide v2, v2, v4

    sub-double v2, v5, v2

    sget-object v5, Lfi/firstbeat/coach/eteintegration/CoachImpl;->t:[D

    aget-wide v5, v5, v0

    sget-object v8, Lfi/firstbeat/coach/eteintegration/CoachImpl;->t:[D

    aget-wide v8, v8, v4

    sub-double/2addr v5, v8

    div-double/2addr v2, v5

    goto :goto_2

    :cond_4
    return-object v7

    :cond_5
    move v0, v3

    move v4, v2

    goto :goto_1
.end method

.method public getUserModifiedWorkoutProgram(Ljava/util/List;I)Ljava/util/List;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lfi/firstbeat/coach/eteintegration/Plan;",
            ">;I)",
            "Ljava/util/List",
            "<",
            "Lfi/firstbeat/coach/eteintegration/TrainingProgramWorkout;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lfi/firstbeat/coach/eteintegration/CoachException;
        }
    .end annotation

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v3, v0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    if-nez v3, :cond_0

    new-instance v3, Lfi/firstbeat/coach/eteintegration/CoachException;

    const-string v4, "Personal background parameters are not set."

    invoke-direct {v3, v4}, Lfi/firstbeat/coach/eteintegration/CoachException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_0
    if-lez v4, :cond_1

    const/16 v3, 0x16d

    if-le v4, v3, :cond_2

    :cond_1
    new-instance v3, Lfi/firstbeat/coach/eteintegration/CoachException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Cannot produce a workout program for "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " days."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lfi/firstbeat/coach/eteintegration/CoachException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_2
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->f:Z

    if-nez v3, :cond_3

    invoke-direct/range {p0 .. p0}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->c()V

    :cond_3
    if-lez p2, :cond_4

    const/4 v3, 0x2

    move/from16 v0, p2

    if-le v0, v3, :cond_5

    :cond_4
    new-instance v3, Lfi/firstbeat/coach/eteintegration/CoachException;

    const-string v4, "Training goal is invalid"

    invoke-direct {v3, v4}, Lfi/firstbeat/coach/eteintegration/CoachException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_5
    invoke-direct/range {p0 .. p0}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a()Lfi/firstbeat/coach/a/b;

    move-result-object v5

    move/from16 v0, p2

    iput v0, v5, Lfi/firstbeat/coach/a/b;->f:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->e:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    const/16 v16, 0x0

    new-instance v3, Lfi/firstbeat/coach/a/c;

    invoke-direct {v3}, Lfi/firstbeat/coach/a/c;-><init>()V

    const/16 v6, 0xf

    move-object/from16 v0, p0

    iget-object v7, v0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v7, v7, Lfi/firstbeat/coach/eteintegration/CoachVars;->AC:I

    invoke-virtual {v3, v6, v7}, Lfi/firstbeat/coach/a/c;->c(II)I

    move-result v7

    move-object/from16 v0, p0

    iget-object v3, v0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->e:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    move v6, v3

    :goto_0
    if-ltz v6, :cond_6

    move-object/from16 v0, p0

    iget-object v3, v0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->e:Ljava/util/List;

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lfi/firstbeat/coach/b/a;

    invoke-virtual {v3}, Lfi/firstbeat/coach/b/a;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/GregorianCalendar;

    invoke-static {v3}, Lfi/firstbeat/coach/a/f;->a(Ljava/util/GregorianCalendar;)I

    move-result v3

    move-object/from16 v0, p0

    iget-object v8, v0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget-object v8, v8, Lfi/firstbeat/coach/eteintegration/CoachVars;->now:Ljava/util/GregorianCalendar;

    invoke-static {v8}, Lfi/firstbeat/coach/a/f;->a(Ljava/util/GregorianCalendar;)I

    move-result v8

    if-ne v3, v8, :cond_6

    move-object/from16 v0, p0

    iget-object v3, v0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->e:Ljava/util/List;

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lfi/firstbeat/coach/b/a;

    invoke-virtual {v3}, Lfi/firstbeat/coach/b/a;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-lt v3, v7, :cond_17

    const/4 v3, 0x1

    :goto_1
    add-int/lit8 v6, v6, -0x1

    move/from16 v16, v3

    goto :goto_0

    :cond_6
    new-array v6, v4, [I

    new-array v7, v4, [I

    new-array v8, v4, [I

    new-array v9, v4, [I

    new-array v10, v4, [I

    new-array v11, v4, [I

    const/4 v3, 0x0

    move v13, v3

    :goto_2
    if-ge v13, v4, :cond_e

    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lfi/firstbeat/coach/eteintegration/Plan;

    if-eqz v3, :cond_7

    iget-object v12, v3, Lfi/firstbeat/coach/eteintegration/Plan;->dayMode:Lfi/firstbeat/coach/eteintegration/DayMode;

    if-nez v12, :cond_9

    :cond_7
    const/4 v3, 0x1

    aput v3, v6, v13

    :cond_8
    :goto_3
    add-int/lit8 v3, v13, 0x1

    move v13, v3

    goto :goto_2

    :cond_9
    sget-object v14, Lfi/firstbeat/coach/eteintegration/CoachImpl$1;->b:[I

    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lfi/firstbeat/coach/eteintegration/Plan;

    iget-object v12, v12, Lfi/firstbeat/coach/eteintegration/Plan;->dayMode:Lfi/firstbeat/coach/eteintegration/DayMode;

    invoke-virtual {v12}, Lfi/firstbeat/coach/eteintegration/DayMode;->ordinal()I

    move-result v12

    aget v12, v14, v12

    packed-switch v12, :pswitch_data_0

    goto :goto_3

    :pswitch_0
    const/4 v3, 0x0

    aput v3, v6, v13

    goto :goto_3

    :pswitch_1
    const/4 v3, 0x1

    aput v3, v6, v13

    goto :goto_3

    :pswitch_2
    const/4 v3, 0x2

    aput v3, v6, v13

    goto :goto_3

    :pswitch_3
    const/4 v12, 0x2

    aput v12, v6, v13

    iget-object v12, v3, Lfi/firstbeat/coach/eteintegration/Plan;->trainingEffect:Ljava/lang/Integer;

    if-eqz v12, :cond_c

    iget-object v12, v3, Lfi/firstbeat/coach/eteintegration/Plan;->trainingEffect:Ljava/lang/Integer;

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    const/16 v14, 0xa

    if-lt v12, v14, :cond_a

    iget-object v12, v3, Lfi/firstbeat/coach/eteintegration/Plan;->trainingEffect:Ljava/lang/Integer;

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    const/16 v14, 0x32

    if-le v12, v14, :cond_b

    :cond_a
    new-instance v4, Lfi/firstbeat/coach/eteintegration/CoachException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Invalid training effect: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v3, Lfi/firstbeat/coach/eteintegration/Plan;->trainingEffect:Ljava/lang/Integer;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Lfi/firstbeat/coach/eteintegration/CoachException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_b
    iget-object v12, v3, Lfi/firstbeat/coach/eteintegration/Plan;->trainingEffect:Ljava/lang/Integer;

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    aput v12, v7, v13

    :cond_c
    iget-object v12, v3, Lfi/firstbeat/coach/eteintegration/Plan;->duration:Ljava/lang/Integer;

    if-eqz v12, :cond_8

    iget-object v12, v3, Lfi/firstbeat/coach/eteintegration/Plan;->duration:Ljava/lang/Integer;

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    if-gtz v12, :cond_d

    new-instance v4, Lfi/firstbeat/coach/eteintegration/CoachException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Invalid workout duration: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v3, Lfi/firstbeat/coach/eteintegration/Plan;->duration:Ljava/lang/Integer;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Lfi/firstbeat/coach/eteintegration/CoachException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_d
    iget-object v3, v3, Lfi/firstbeat/coach/eteintegration/Plan;->duration:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aput v3, v8, v13

    goto/16 :goto_3

    :cond_e
    move-object/from16 v0, p0

    iget-object v3, v0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->b:Lfi/firstbeat/coach/a/f;

    invoke-virtual/range {v3 .. v11}, Lfi/firstbeat/coach/a/f;->a(ILfi/firstbeat/coach/a/b;[I[I[I[I[I[I)I

    move-result v3

    if-gez v3, :cond_f

    new-instance v3, Lfi/firstbeat/coach/eteintegration/CoachException;

    const-string v4, "Error in training program creation."

    invoke-direct {v3, v4}, Lfi/firstbeat/coach/eteintegration/CoachException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_f
    invoke-direct/range {p0 .. p0}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->b()D

    move-result-wide v11

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    const/4 v3, 0x0

    move v5, v3

    :goto_4
    if-ge v5, v4, :cond_16

    new-instance v17, Lfi/firstbeat/coach/eteintegration/TrainingProgramWorkout;

    invoke-direct/range {v17 .. v17}, Lfi/firstbeat/coach/eteintegration/TrainingProgramWorkout;-><init>()V

    aget v13, v7, v5

    aget v14, v9, v5

    aget v15, v8, v5

    if-eqz v15, :cond_10

    move-object/from16 v0, p0

    iget-object v10, v0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->c:Lfi/firstbeat/coach/a/h;

    invoke-virtual/range {v10 .. v15}, Lfi/firstbeat/coach/a/h;->a(DIII)[D

    move-result-object v3

    const/4 v10, 0x0

    aget-wide v18, v3, v10

    move-wide/from16 v0, v18

    move-object/from16 v2, v17

    iput-wide v0, v2, Lfi/firstbeat/coach/eteintegration/TrainingProgramWorkout;->distance:D

    :cond_10
    move-object/from16 v0, p0

    iget-object v3, v0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget-object v3, v3, Lfi/firstbeat/coach/eteintegration/CoachVars;->now:Ljava/util/GregorianCalendar;

    invoke-static {v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(Ljava/util/GregorianCalendar;)Ljava/util/GregorianCalendar;

    move-result-object v3

    move-object/from16 v0, v17

    iput-object v3, v0, Lfi/firstbeat/coach/eteintegration/TrainingProgramWorkout;->date:Ljava/util/GregorianCalendar;

    if-eqz v16, :cond_13

    move-object/from16 v0, v17

    iget-object v3, v0, Lfi/firstbeat/coach/eteintegration/TrainingProgramWorkout;->date:Ljava/util/GregorianCalendar;

    const/4 v10, 0x5

    add-int/lit8 v14, v5, 0x1

    invoke-virtual {v3, v10, v14}, Ljava/util/GregorianCalendar;->add(II)V

    :goto_5
    move-object/from16 v0, v17

    iput v15, v0, Lfi/firstbeat/coach/eteintegration/TrainingProgramWorkout;->duration:I

    move-object/from16 v0, v17

    iput v13, v0, Lfi/firstbeat/coach/eteintegration/TrainingProgramWorkout;->trainingEffect:I

    if-eqz v16, :cond_14

    const/4 v3, 0x1

    :goto_6
    add-int/2addr v3, v5

    const/4 v10, 0x1

    if-gt v3, v10, :cond_12

    move-object/from16 v0, v17

    iget v10, v0, Lfi/firstbeat/coach/eteintegration/TrainingProgramWorkout;->trainingEffect:I

    if-gtz v5, :cond_11

    if-eqz v16, :cond_15

    :cond_11
    const/4 v3, 0x1

    :goto_7
    invoke-static {v10, v3}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(IZ)I

    move-result v3

    move-object/from16 v0, v17

    iput v3, v0, Lfi/firstbeat/coach/eteintegration/TrainingProgramWorkout;->phraseNumber:I

    :cond_12
    move-object/from16 v0, v17

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_4

    :cond_13
    move-object/from16 v0, v17

    iget-object v3, v0, Lfi/firstbeat/coach/eteintegration/TrainingProgramWorkout;->date:Ljava/util/GregorianCalendar;

    const/4 v10, 0x5

    invoke-virtual {v3, v10, v5}, Ljava/util/GregorianCalendar;->add(II)V

    goto :goto_5

    :cond_14
    const/4 v3, 0x0

    goto :goto_6

    :cond_15
    const/4 v3, 0x0

    goto :goto_7

    :cond_16
    return-object v6

    :cond_17
    move/from16 v3, v16

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lfi/firstbeat/coach/a/f;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getVo2max()D
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lfi/firstbeat/coach/eteintegration/CoachException;
        }
    .end annotation

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    if-nez v0, :cond_0

    new-instance v0, Lfi/firstbeat/coach/eteintegration/CoachException;

    const-string v1, "Personal background parameters are not set."

    invoke-direct {v0, v1}, Lfi/firstbeat/coach/eteintegration/CoachException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-direct {p0}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->b()D

    move-result-wide v0

    return-wide v0
.end method

.method public getWeeklyTrainingLoad()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lfi/firstbeat/coach/eteintegration/CoachException;
        }
    .end annotation

    const/4 v1, 0x0

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->e:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->e:Ljava/util/List;

    iget-object v2, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfi/firstbeat/coach/b/a;

    invoke-virtual {v0}, Lfi/firstbeat/coach/b/a;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/GregorianCalendar;

    invoke-static {v0}, Lfi/firstbeat/coach/a/f;->a(Ljava/util/GregorianCalendar;)I

    move-result v0

    iget-object v2, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget-object v2, v2, Lfi/firstbeat/coach/eteintegration/CoachVars;->now:Ljava/util/GregorianCalendar;

    invoke-static {v2}, Lfi/firstbeat/coach/a/f;->a(Ljava/util/GregorianCalendar;)I

    move-result v2

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v2, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget-object v2, v2, Lfi/firstbeat/coach/eteintegration/CoachVars;->now:Ljava/util/GregorianCalendar;

    invoke-static {v2}, Lfi/firstbeat/coach/a/f;->a(Ljava/util/GregorianCalendar;)I

    move-result v2

    if-eqz v0, :cond_1

    const/4 v0, 0x6

    :goto_1
    sub-int v3, v2, v0

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v1

    move v1, v0

    :goto_2
    if-ltz v1, :cond_2

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfi/firstbeat/coach/b/a;

    invoke-virtual {v0}, Lfi/firstbeat/coach/b/a;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/GregorianCalendar;

    invoke-static {v0}, Lfi/firstbeat/coach/a/f;->a(Ljava/util/GregorianCalendar;)I

    move-result v0

    if-lt v0, v3, :cond_2

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfi/firstbeat/coach/b/a;

    invoke-virtual {v0}, Lfi/firstbeat/coach/b/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/2addr v2, v0

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_2

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x7

    goto :goto_1

    :cond_2
    return v2
.end method

.method public getWeeklyTrainingLoadLowerLimit()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lfi/firstbeat/coach/eteintegration/CoachException;
        }
    .end annotation

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v0, v0, Lfi/firstbeat/coach/eteintegration/CoachVars;->trainingGoal:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    sget-object v0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a:Ljava/util/SortedMap;

    iget-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v1, v1, Lfi/firstbeat/coach/eteintegration/CoachVars;->AC:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    new-instance v0, Lfi/firstbeat/coach/a/e;

    invoke-direct {v0}, Lfi/firstbeat/coach/a/e;-><init>()V

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v0, v0, Lfi/firstbeat/coach/eteintegration/CoachVars;->trainingLevel:I

    const/4 v1, 0x6

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v0, v0, Lfi/firstbeat/coach/eteintegration/CoachVars;->trainingLevel:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_2

    :cond_1
    const/16 v0, 0x1e

    const/16 v1, 0x32

    invoke-static {v0, v1}, Lfi/firstbeat/coach/a/e;->c(II)I

    move-result v0

    goto :goto_0

    :cond_2
    const/16 v0, 0x14

    iget-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v1, v1, Lfi/firstbeat/coach/eteintegration/CoachVars;->AC:I

    invoke-static {v0, v1}, Lfi/firstbeat/coach/a/e;->c(II)I

    move-result v0

    goto :goto_0
.end method

.method public getWeeklyTrainingLoadUpperLimit()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lfi/firstbeat/coach/eteintegration/CoachException;
        }
    .end annotation

    const/4 v0, 0x5

    iget-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v1, v1, Lfi/firstbeat/coach/eteintegration/CoachVars;->trainingGoal:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v1, v1, Lfi/firstbeat/coach/eteintegration/CoachVars;->AC:I

    iget-object v2, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v2, v2, Lfi/firstbeat/coach/eteintegration/CoachVars;->AC:I

    const/16 v3, 0x46

    if-ge v2, v3, :cond_1

    const/16 v0, 0xa

    :cond_0
    :goto_0
    add-int/2addr v0, v1

    sget-object v1, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a:Ljava/util/SortedMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_1
    return v0

    :cond_1
    iget-object v2, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v2, v2, Lfi/firstbeat/coach/eteintegration/CoachVars;->AC:I

    const/16 v3, 0x64

    if-lt v2, v3, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    new-instance v1, Lfi/firstbeat/coach/a/e;

    invoke-direct {v1}, Lfi/firstbeat/coach/a/e;-><init>()V

    iget-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v1, v1, Lfi/firstbeat/coach/eteintegration/CoachVars;->trainingLevel:I

    if-ne v1, v0, :cond_3

    const/16 v0, 0x258

    goto :goto_1

    :cond_3
    const/16 v0, 0x28

    iget-object v1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v1, v1, Lfi/firstbeat/coach/eteintegration/CoachVars;->AC:I

    invoke-static {v0, v1}, Lfi/firstbeat/coach/a/e;->c(II)I

    move-result v0

    goto :goto_1
.end method

.method public getWorkoutProgram(IZI)Ljava/util/List;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZI)",
            "Ljava/util/List",
            "<",
            "Lfi/firstbeat/coach/eteintegration/TrainingProgramWorkout;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lfi/firstbeat/coach/eteintegration/CoachException;
        }
    .end annotation

    move-object/from16 v0, p0

    iget-object v1, v0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    if-nez v1, :cond_0

    new-instance v1, Lfi/firstbeat/coach/eteintegration/CoachException;

    const-string v2, "Personal background parameters are not set."

    invoke-direct {v1, v2}, Lfi/firstbeat/coach/eteintegration/CoachException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    if-lez p1, :cond_1

    const/16 v1, 0x16d

    move/from16 v0, p1

    if-le v0, v1, :cond_2

    :cond_1
    new-instance v1, Lfi/firstbeat/coach/eteintegration/CoachException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cannot produce a workout program for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " days."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lfi/firstbeat/coach/eteintegration/CoachException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->f:Z

    if-nez v1, :cond_3

    invoke-direct/range {p0 .. p0}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->c()V

    :cond_3
    if-lez p3, :cond_4

    const/4 v1, 0x2

    move/from16 v0, p3

    if-le v0, v1, :cond_5

    :cond_4
    new-instance v1, Lfi/firstbeat/coach/eteintegration/CoachException;

    const-string v2, "Training goal is invalid"

    invoke-direct {v1, v2}, Lfi/firstbeat/coach/eteintegration/CoachException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_5
    invoke-direct/range {p0 .. p0}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a()Lfi/firstbeat/coach/a/b;

    move-result-object v3

    move/from16 v0, p3

    iput v0, v3, Lfi/firstbeat/coach/a/b;->f:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    const/4 v14, 0x0

    new-instance v1, Lfi/firstbeat/coach/a/c;

    invoke-direct {v1}, Lfi/firstbeat/coach/a/c;-><init>()V

    const/16 v2, 0xf

    move-object/from16 v0, p0

    iget-object v4, v0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v4, v4, Lfi/firstbeat/coach/eteintegration/CoachVars;->AC:I

    invoke-virtual {v1, v2, v4}, Lfi/firstbeat/coach/a/c;->c(II)I

    move-result v4

    move-object/from16 v0, p0

    iget-object v1, v0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v2, v1

    :goto_0
    if-ltz v2, :cond_6

    move-object/from16 v0, p0

    iget-object v1, v0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->e:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfi/firstbeat/coach/b/a;

    invoke-virtual {v1}, Lfi/firstbeat/coach/b/a;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/GregorianCalendar;

    invoke-static {v1}, Lfi/firstbeat/coach/a/f;->a(Ljava/util/GregorianCalendar;)I

    move-result v1

    move-object/from16 v0, p0

    iget-object v5, v0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget-object v5, v5, Lfi/firstbeat/coach/eteintegration/CoachVars;->now:Ljava/util/GregorianCalendar;

    invoke-static {v5}, Lfi/firstbeat/coach/a/f;->a(Ljava/util/GregorianCalendar;)I

    move-result v5

    if-ne v1, v5, :cond_6

    move-object/from16 v0, p0

    iget-object v1, v0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->e:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfi/firstbeat/coach/b/a;

    invoke-virtual {v1}, Lfi/firstbeat/coach/b/a;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-lt v1, v4, :cond_11

    const/4 v1, 0x1

    :goto_1
    add-int/lit8 v2, v2, -0x1

    move v14, v1

    goto :goto_0

    :cond_6
    move/from16 v0, p1

    new-array v4, v0, [I

    move/from16 v0, p1

    new-array v5, v0, [I

    move/from16 v0, p1

    new-array v6, v0, [I

    move/from16 v0, p1

    new-array v7, v0, [I

    move/from16 v0, p1

    new-array v8, v0, [I

    move/from16 v0, p1

    new-array v9, v0, [I

    const/4 v1, 0x0

    :goto_2
    move/from16 v0, p1

    if-ge v1, v0, :cond_7

    const/4 v2, 0x1

    aput v2, v4, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_7
    if-eqz p2, :cond_8

    const/4 v1, 0x0

    const/4 v2, 0x2

    aput v2, v4, v1

    :cond_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->b:Lfi/firstbeat/coach/a/f;

    move/from16 v2, p1

    invoke-virtual/range {v1 .. v9}, Lfi/firstbeat/coach/a/f;->a(ILfi/firstbeat/coach/a/b;[I[I[I[I[I[I)I

    move-result v1

    if-gez v1, :cond_9

    new-instance v1, Lfi/firstbeat/coach/eteintegration/CoachException;

    const-string v2, "Error in training program creation."

    invoke-direct {v1, v2}, Lfi/firstbeat/coach/eteintegration/CoachException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_9
    invoke-direct/range {p0 .. p0}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->b()D

    move-result-wide v9

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    move v2, v1

    :goto_3
    move/from16 v0, p1

    if-ge v2, v0, :cond_10

    new-instance v4, Lfi/firstbeat/coach/eteintegration/TrainingProgramWorkout;

    invoke-direct {v4}, Lfi/firstbeat/coach/eteintegration/TrainingProgramWorkout;-><init>()V

    aget v11, v5, v2

    aget v12, v7, v2

    aget v13, v6, v2

    if-eqz v13, :cond_a

    move-object/from16 v0, p0

    iget-object v8, v0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->c:Lfi/firstbeat/coach/a/h;

    invoke-virtual/range {v8 .. v13}, Lfi/firstbeat/coach/a/h;->a(DIII)[D

    move-result-object v1

    const/4 v8, 0x0

    aget-wide v15, v1, v8

    iput-wide v15, v4, Lfi/firstbeat/coach/eteintegration/TrainingProgramWorkout;->distance:D

    :cond_a
    move-object/from16 v0, p0

    iget-object v1, v0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget-object v1, v1, Lfi/firstbeat/coach/eteintegration/CoachVars;->now:Ljava/util/GregorianCalendar;

    invoke-static {v1}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(Ljava/util/GregorianCalendar;)Ljava/util/GregorianCalendar;

    move-result-object v1

    iput-object v1, v4, Lfi/firstbeat/coach/eteintegration/TrainingProgramWorkout;->date:Ljava/util/GregorianCalendar;

    if-eqz v14, :cond_d

    iget-object v1, v4, Lfi/firstbeat/coach/eteintegration/TrainingProgramWorkout;->date:Ljava/util/GregorianCalendar;

    const/4 v8, 0x5

    add-int/lit8 v12, v2, 0x1

    invoke-virtual {v1, v8, v12}, Ljava/util/GregorianCalendar;->add(II)V

    :goto_4
    iput v13, v4, Lfi/firstbeat/coach/eteintegration/TrainingProgramWorkout;->duration:I

    iput v11, v4, Lfi/firstbeat/coach/eteintegration/TrainingProgramWorkout;->trainingEffect:I

    if-eqz v14, :cond_e

    const/4 v1, 0x1

    :goto_5
    add-int/2addr v1, v2

    const/4 v8, 0x1

    if-gt v1, v8, :cond_c

    iget v8, v4, Lfi/firstbeat/coach/eteintegration/TrainingProgramWorkout;->trainingEffect:I

    if-gtz v2, :cond_b

    if-eqz v14, :cond_f

    :cond_b
    const/4 v1, 0x1

    :goto_6
    invoke-static {v8, v1}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->a(IZ)I

    move-result v1

    iput v1, v4, Lfi/firstbeat/coach/eteintegration/TrainingProgramWorkout;->phraseNumber:I

    :cond_c
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    :cond_d
    iget-object v1, v4, Lfi/firstbeat/coach/eteintegration/TrainingProgramWorkout;->date:Ljava/util/GregorianCalendar;

    const/4 v8, 0x5

    invoke-virtual {v1, v8, v2}, Ljava/util/GregorianCalendar;->add(II)V

    goto :goto_4

    :cond_e
    const/4 v1, 0x0

    goto :goto_5

    :cond_f
    const/4 v1, 0x0

    goto :goto_6

    :cond_10
    return-object v3

    :cond_11
    move v1, v14

    goto/16 :goto_1
.end method

.method public setParameters(Lfi/firstbeat/coach/eteintegration/CoachVars;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lfi/firstbeat/coach/eteintegration/CoachException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lfi/firstbeat/coach/eteintegration/CoachImpl;->assertValidParameters(Lfi/firstbeat/coach/eteintegration/CoachVars;)V

    iput-object p1, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->d:Lfi/firstbeat/coach/eteintegration/CoachVars;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lfi/firstbeat/coach/eteintegration/CoachImpl;->f:Z

    return-void
.end method
