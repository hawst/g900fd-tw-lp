.class public Lfi/firstbeat/coach/eteintegration/Plan;
.super Ljava/lang/Object;


# instance fields
.field public dayMode:Lfi/firstbeat/coach/eteintegration/DayMode;

.field public duration:Ljava/lang/Integer;

.field public trainingEffect:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lfi/firstbeat/coach/eteintegration/DayMode;->AUTO:Lfi/firstbeat/coach/eteintegration/DayMode;

    iput-object v0, p0, Lfi/firstbeat/coach/eteintegration/Plan;->dayMode:Lfi/firstbeat/coach/eteintegration/DayMode;

    iput-object v1, p0, Lfi/firstbeat/coach/eteintegration/Plan;->trainingEffect:Ljava/lang/Integer;

    iput-object v1, p0, Lfi/firstbeat/coach/eteintegration/Plan;->duration:Ljava/lang/Integer;

    return-void
.end method
