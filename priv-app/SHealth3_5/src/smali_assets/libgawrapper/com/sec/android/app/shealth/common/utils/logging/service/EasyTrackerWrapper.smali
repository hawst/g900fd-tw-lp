.class public Lcom/sec/android/app/shealth/common/utils/logging/service/EasyTrackerWrapper;
.super Ljava/lang/Object;
.source "EasyTrackerWrapper.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/utils/logging/service/IGoogleAnalyticsEasyTracker;


# static fields
.field public static final LOG_APP_ID_STRING:Ljava/lang/String; = "app_id"

.field public static final LOG_EXTRA_STRING:Ljava/lang/String; = "extra"

.field public static final LOG_FEATURE_STRING:Ljava/lang/String; = "feature"

.field public static final LOG_VALUE_STRING:Ljava/lang/String; = "value"


# instance fields
.field private mTracker:Lcom/google/analytics/tracking/android/EasyTracker;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public activityStart(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/EasyTrackerWrapper;->mTracker:Lcom/google/analytics/tracking/android/EasyTracker;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/EasyTrackerWrapper;->mTracker:Lcom/google/analytics/tracking/android/EasyTracker;

    invoke-virtual {v0, p1}, Lcom/google/analytics/tracking/android/EasyTracker;->activityStart(Landroid/app/Activity;)V

    .line 67
    :cond_0
    return-void
.end method

.method public activityStop(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/EasyTrackerWrapper;->mTracker:Lcom/google/analytics/tracking/android/EasyTracker;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/EasyTrackerWrapper;->mTracker:Lcom/google/analytics/tracking/android/EasyTracker;

    invoke-virtual {v0, p1}, Lcom/google/analytics/tracking/android/EasyTracker;->activityStop(Landroid/app/Activity;)V

    .line 74
    :cond_0
    return-void
.end method

.method public getTracker(Landroid/content/Context;)Lcom/sec/android/app/shealth/common/utils/logging/service/IGoogleAnalyticsEasyTracker;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/EasyTrackerWrapper;->mTracker:Lcom/google/analytics/tracking/android/EasyTracker;

    if-nez v0, :cond_0

    .line 23
    invoke-static {p1}, Lcom/google/analytics/tracking/android/EasyTracker;->getInstance(Landroid/content/Context;)Lcom/google/analytics/tracking/android/EasyTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/EasyTrackerWrapper;->mTracker:Lcom/google/analytics/tracking/android/EasyTracker;

    .line 25
    :cond_0
    return-object p0
.end method

.method public send(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/EasyTrackerWrapper;->mTracker:Lcom/google/analytics/tracking/android/EasyTracker;

    if-eqz v0, :cond_0

    .line 44
    const-string v0, "value"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 45
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/EasyTrackerWrapper;->mTracker:Lcom/google/analytics/tracking/android/EasyTracker;

    .line 46
    const-string v1, "app_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 47
    const-string v2, "feature"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 48
    const-string v3, "extra"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 49
    const-string v4, "value"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    .line 46
    invoke-static {v1, v2, v3, v4}, Lcom/google/analytics/tracking/android/MapBuilder;->createEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)Lcom/google/analytics/tracking/android/MapBuilder;

    move-result-object v1

    .line 50
    invoke-virtual {v1}, Lcom/google/analytics/tracking/android/MapBuilder;->build()Ljava/util/Map;

    move-result-object v1

    .line 45
    invoke-virtual {v0, v1}, Lcom/google/analytics/tracking/android/EasyTracker;->send(Ljava/util/Map;)V

    .line 60
    :cond_0
    :goto_0
    return-void

    .line 52
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/EasyTrackerWrapper;->mTracker:Lcom/google/analytics/tracking/android/EasyTracker;

    .line 53
    const-string v1, "app_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 54
    const-string v2, "feature"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 55
    const-string v3, "extra"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 56
    const/4 v4, 0x0

    .line 53
    invoke-static {v1, v2, v3, v4}, Lcom/google/analytics/tracking/android/MapBuilder;->createEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)Lcom/google/analytics/tracking/android/MapBuilder;

    move-result-object v1

    .line 57
    invoke-virtual {v1}, Lcom/google/analytics/tracking/android/MapBuilder;->build()Ljava/util/Map;

    move-result-object v1

    .line 52
    invoke-virtual {v0, v1}, Lcom/google/analytics/tracking/android/EasyTracker;->send(Ljava/util/Map;)V

    goto :goto_0
.end method

.method public setCustomDimension(ILjava/lang/String;)V
    .locals 2
    .param p1, "index"    # I
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/EasyTrackerWrapper;->mTracker:Lcom/google/analytics/tracking/android/EasyTracker;

    if-eqz v0, :cond_0

    .line 31
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/EasyTrackerWrapper;->mTracker:Lcom/google/analytics/tracking/android/EasyTracker;

    invoke-static {p1}, Lcom/google/analytics/tracking/android/Fields;->customDimension(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/google/analytics/tracking/android/EasyTracker;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    :cond_0
    return-void
.end method

.method public setCustomMetric(ILjava/lang/String;)V
    .locals 2
    .param p1, "index"    # I
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/EasyTrackerWrapper;->mTracker:Lcom/google/analytics/tracking/android/EasyTracker;

    if-eqz v0, :cond_0

    .line 37
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/EasyTrackerWrapper;->mTracker:Lcom/google/analytics/tracking/android/EasyTracker;

    invoke-static {p1}, Lcom/google/analytics/tracking/android/Fields;->customMetric(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/google/analytics/tracking/android/EasyTracker;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    :cond_0
    return-void
.end method
