.class public Lcom/sec/android/app/shealth/common/utils/logging/service/StatServiceWrapper;
.super Ljava/lang/Object;
.source "StatServiceWrapper.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/utils/logging/service/IBaiduAnalyticsStatService;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "feature"    # Ljava/lang/String;
    .param p3, "extra"    # Ljava/lang/String;

    .prologue
    .line 21
    invoke-static {p1, p2, p3}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    return-void
.end method

.method public onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "feature"    # Ljava/lang/String;
    .param p3, "extra"    # Ljava/lang/String;
    .param p4, "value"    # I

    .prologue
    .line 26
    invoke-static {p1, p2, p3, p4}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    .line 27
    return-void
.end method

.method public onEventDuration(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "feature"    # Ljava/lang/String;
    .param p3, "extra"    # Ljava/lang/String;
    .param p4, "value"    # J

    .prologue
    .line 42
    invoke-static {p1, p2, p3, p4, p5}, Lcom/baidu/mobstat/StatService;->onEventDuration(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;J)V

    .line 43
    return-void
.end method

.method public onPause(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 36
    invoke-static {p1}, Lcom/baidu/mobstat/StatService;->onPause(Landroid/content/Context;)V

    .line 37
    return-void
.end method

.method public onResume(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    invoke-static {p1}, Lcom/baidu/mobstat/StatService;->onResume(Landroid/content/Context;)V

    .line 32
    return-void
.end method

.method public setAppChannel(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "flag"    # Z

    .prologue
    .line 11
    invoke-static {p1, p2, p3}, Lcom/baidu/mobstat/StatService;->setAppChannel(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 12
    return-void
.end method

.method public setDebugOn(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 16
    invoke-static {p1}, Lcom/baidu/mobstat/StatService;->setDebugOn(Z)V

    .line 17
    return-void
.end method
