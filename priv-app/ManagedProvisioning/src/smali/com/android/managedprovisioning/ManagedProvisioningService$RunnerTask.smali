.class Lcom/android/managedprovisioning/ManagedProvisioningService$RunnerTask;
.super Landroid/os/AsyncTask;
.source "ManagedProvisioningService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/managedprovisioning/ManagedProvisioningService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RunnerTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Landroid/content/Intent;",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/managedprovisioning/ManagedProvisioningService;


# direct methods
.method private constructor <init>(Lcom/android/managedprovisioning/ManagedProvisioningService;)V
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lcom/android/managedprovisioning/ManagedProvisioningService$RunnerTask;->this$0:Lcom/android/managedprovisioning/ManagedProvisioningService;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/managedprovisioning/ManagedProvisioningService;Lcom/android/managedprovisioning/ManagedProvisioningService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/android/managedprovisioning/ManagedProvisioningService;
    .param p2, "x1"    # Lcom/android/managedprovisioning/ManagedProvisioningService$1;

    .prologue
    .line 86
    invoke-direct {p0, p1}, Lcom/android/managedprovisioning/ManagedProvisioningService$RunnerTask;-><init>(Lcom/android/managedprovisioning/ManagedProvisioningService;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 86
    check-cast p1, [Landroid/content/Intent;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/android/managedprovisioning/ManagedProvisioningService$RunnerTask;->doInBackground([Landroid/content/Intent;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Landroid/content/Intent;)Ljava/lang/Void;
    .locals 2
    .param p1, "intents"    # [Landroid/content/Intent;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/android/managedprovisioning/ManagedProvisioningService$RunnerTask;->this$0:Lcom/android/managedprovisioning/ManagedProvisioningService;

    const/4 v1, 0x0

    aget-object v1, p1, v1

    # invokes: Lcom/android/managedprovisioning/ManagedProvisioningService;->initialize(Landroid/content/Intent;)V
    invoke-static {v0, v1}, Lcom/android/managedprovisioning/ManagedProvisioningService;->access$000(Lcom/android/managedprovisioning/ManagedProvisioningService;Landroid/content/Intent;)V

    .line 90
    iget-object v0, p0, Lcom/android/managedprovisioning/ManagedProvisioningService$RunnerTask;->this$0:Lcom/android/managedprovisioning/ManagedProvisioningService;

    # invokes: Lcom/android/managedprovisioning/ManagedProvisioningService;->startManagedProfileProvisioning()V
    invoke-static {v0}, Lcom/android/managedprovisioning/ManagedProvisioningService;->access$100(Lcom/android/managedprovisioning/ManagedProvisioningService;)V

    .line 91
    const/4 v0, 0x0

    return-object v0
.end method
