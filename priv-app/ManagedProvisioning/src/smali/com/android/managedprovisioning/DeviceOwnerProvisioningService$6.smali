.class Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$6;
.super Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask$Callback;
.source "DeviceOwnerProvisioningService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->startDeviceOwnerProvisioning(Lcom/android/managedprovisioning/ProvisioningParams;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

.field final synthetic val$params:Lcom/android/managedprovisioning/ProvisioningParams;


# direct methods
.method constructor <init>(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;Lcom/android/managedprovisioning/ProvisioningParams;)V
    .locals 0

    .prologue
    .line 304
    iput-object p1, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$6;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    iput-object p2, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$6;->val$params:Lcom/android/managedprovisioning/ProvisioningParams;

    invoke-direct {p0}, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask$Callback;-><init>()V

    return-void
.end method


# virtual methods
.method public onError()V
    .locals 2

    .prologue
    .line 312
    iget-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$6;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    const v1, 0x7f070029

    # invokes: Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->error(I)V
    invoke-static {v0, v1}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->access$700(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;I)V

    .line 313
    return-void
.end method

.method public onSuccess()V
    .locals 2

    .prologue
    .line 307
    iget-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$6;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    iget-object v1, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$6;->val$params:Lcom/android/managedprovisioning/ProvisioningParams;

    iget-object v1, v1, Lcom/android/managedprovisioning/ProvisioningParams;->mDeviceAdminPackageName:Ljava/lang/String;

    # invokes: Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->onProvisioningSuccess(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->access$1000(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;Ljava/lang/String;)V

    .line 308
    return-void
.end method
