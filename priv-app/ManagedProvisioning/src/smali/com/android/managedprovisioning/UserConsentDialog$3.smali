.class Lcom/android/managedprovisioning/UserConsentDialog$3;
.super Ljava/lang/Object;
.source "UserConsentDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/managedprovisioning/UserConsentDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/managedprovisioning/UserConsentDialog;

.field final synthetic val$dialog:Landroid/app/Dialog;


# direct methods
.method constructor <init>(Lcom/android/managedprovisioning/UserConsentDialog;Landroid/app/Dialog;)V
    .locals 0

    .prologue
    .line 102
    iput-object p1, p0, Lcom/android/managedprovisioning/UserConsentDialog$3;->this$0:Lcom/android/managedprovisioning/UserConsentDialog;

    iput-object p2, p0, Lcom/android/managedprovisioning/UserConsentDialog$3;->val$dialog:Landroid/app/Dialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/android/managedprovisioning/UserConsentDialog$3;->val$dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 106
    iget-object v0, p0, Lcom/android/managedprovisioning/UserConsentDialog$3;->this$0:Lcom/android/managedprovisioning/UserConsentDialog;

    # getter for: Lcom/android/managedprovisioning/UserConsentDialog;->mOnCancelRunnable:Ljava/lang/Runnable;
    invoke-static {v0}, Lcom/android/managedprovisioning/UserConsentDialog;->access$300(Lcom/android/managedprovisioning/UserConsentDialog;)Ljava/lang/Runnable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/android/managedprovisioning/UserConsentDialog$3;->this$0:Lcom/android/managedprovisioning/UserConsentDialog;

    # getter for: Lcom/android/managedprovisioning/UserConsentDialog;->mOnCancelRunnable:Ljava/lang/Runnable;
    invoke-static {v0}, Lcom/android/managedprovisioning/UserConsentDialog;->access$300(Lcom/android/managedprovisioning/UserConsentDialog;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 109
    :cond_0
    return-void
.end method
