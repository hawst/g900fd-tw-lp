.class Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$2;
.super Lcom/android/managedprovisioning/task/AddWifiNetworkTask$Callback;
.source "DeviceOwnerProvisioningService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->startDeviceOwnerProvisioning(Lcom/android/managedprovisioning/ProvisioningParams;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

.field final synthetic val$params:Lcom/android/managedprovisioning/ProvisioningParams;


# direct methods
.method constructor <init>(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;Lcom/android/managedprovisioning/ProvisioningParams;)V
    .locals 0

    .prologue
    .line 201
    iput-object p1, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$2;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    iput-object p2, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$2;->val$params:Lcom/android/managedprovisioning/ProvisioningParams;

    invoke-direct {p0}, Lcom/android/managedprovisioning/task/AddWifiNetworkTask$Callback;-><init>()V

    return-void
.end method


# virtual methods
.method public onError()V
    .locals 2

    .prologue
    .line 218
    iget-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$2;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    const v1, 0x7f07002b

    # invokes: Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->error(I)V
    invoke-static {v0, v1}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->access$700(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;I)V

    .line 219
    return-void
.end method

.method public onSuccess()V
    .locals 2

    .prologue
    .line 204
    iget-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$2;->val$params:Lcom/android/managedprovisioning/ProvisioningParams;

    iget-object v0, v0, Lcom/android/managedprovisioning/ProvisioningParams;->mDeviceAdminPackageDownloadLocation:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 206
    iget-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$2;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    const v1, 0x7f070021

    # invokes: Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->progressUpdate(I)V
    invoke-static {v0, v1}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->access$400(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;I)V

    .line 207
    iget-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$2;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    # getter for: Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mDownloadPackageTask:Lcom/android/managedprovisioning/task/DownloadPackageTask;
    invoke-static {v0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->access$500(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;)Lcom/android/managedprovisioning/task/DownloadPackageTask;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/managedprovisioning/task/DownloadPackageTask;->run()V

    .line 214
    :goto_0
    return-void

    .line 211
    :cond_0
    iget-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$2;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    const v1, 0x7f070023

    # invokes: Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->progressUpdate(I)V
    invoke-static {v0, v1}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->access$400(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;I)V

    .line 212
    iget-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$2;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    # getter for: Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mSetDevicePolicyTask:Lcom/android/managedprovisioning/task/SetDevicePolicyTask;
    invoke-static {v0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->access$600(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;)Lcom/android/managedprovisioning/task/SetDevicePolicyTask;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/managedprovisioning/task/SetDevicePolicyTask;->run()V

    goto :goto_0
.end method
