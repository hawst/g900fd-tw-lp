.class Lcom/android/managedprovisioning/ManagedProvisioningService$2;
.super Landroid/content/BroadcastReceiver;
.source "ManagedProvisioningService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/managedprovisioning/ManagedProvisioningService;->onProvisioningSuccess(Landroid/content/ComponentName;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/managedprovisioning/ManagedProvisioningService;


# direct methods
.method constructor <init>(Lcom/android/managedprovisioning/ManagedProvisioningService;)V
    .locals 0

    .prologue
    .line 310
    iput-object p1, p0, Lcom/android/managedprovisioning/ManagedProvisioningService$2;->this$0:Lcom/android/managedprovisioning/ManagedProvisioningService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 313
    const-string v1, "ACTION_PROFILE_PROVISIONING_COMPLETE broadcast received by mdm"

    invoke-static {v1}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 315
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.managedprovisioning.provisioning_success"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 316
    .local v0, "successIntent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/android/managedprovisioning/ManagedProvisioningService$2;->this$0:Lcom/android/managedprovisioning/ManagedProvisioningService;

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 318
    iget-object v1, p0, Lcom/android/managedprovisioning/ManagedProvisioningService$2;->this$0:Lcom/android/managedprovisioning/ManagedProvisioningService;

    invoke-virtual {v1}, Lcom/android/managedprovisioning/ManagedProvisioningService;->stopSelf()V

    .line 319
    return-void
.end method
