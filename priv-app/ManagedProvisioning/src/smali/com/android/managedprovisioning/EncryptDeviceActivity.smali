.class public Lcom/android/managedprovisioning/EncryptDeviceActivity;
.super Landroid/app/Activity;
.source "EncryptDeviceActivity.java"


# instance fields
.field private mCancelButton:Landroid/widget/Button;

.field private mEncryptButton:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method public static isDeviceEncrypted()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 77
    const-string v3, "mount"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Landroid/os/storage/IMountService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/storage/IMountService;

    move-result-object v1

    .line 81
    .local v1, "mountService":Landroid/os/storage/IMountService;
    :try_start_0
    invoke-interface {v1}, Landroid/os/storage/IMountService;->getEncryptionState()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-nez v3, :cond_0

    const/4 v2, 0x1

    .line 83
    :cond_0
    :goto_0
    return v2

    .line 82
    :catch_0
    move-exception v0

    .line 83
    .local v0, "e":Landroid/os/RemoteException;
    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 49
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 51
    invoke-virtual {p0}, Lcom/android/managedprovisioning/EncryptDeviceActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const/high16 v2, 0x7f030000

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 52
    .local v0, "contentView":Landroid/view/View;
    const v1, 0x7f090001

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/android/managedprovisioning/EncryptDeviceActivity;->mEncryptButton:Landroid/widget/Button;

    .line 53
    const/high16 v1, 0x7f090000

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/android/managedprovisioning/EncryptDeviceActivity;->mCancelButton:Landroid/widget/Button;

    .line 54
    invoke-virtual {p0, v0}, Lcom/android/managedprovisioning/EncryptDeviceActivity;->setContentView(Landroid/view/View;)V

    .line 55
    iget-object v1, p0, Lcom/android/managedprovisioning/EncryptDeviceActivity;->mEncryptButton:Landroid/widget/Button;

    new-instance v2, Lcom/android/managedprovisioning/EncryptDeviceActivity$1;

    invoke-direct {v2, p0}, Lcom/android/managedprovisioning/EncryptDeviceActivity$1;-><init>(Lcom/android/managedprovisioning/EncryptDeviceActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 68
    iget-object v1, p0, Lcom/android/managedprovisioning/EncryptDeviceActivity;->mCancelButton:Landroid/widget/Button;

    new-instance v2, Lcom/android/managedprovisioning/EncryptDeviceActivity$2;

    invoke-direct {v2, p0}, Lcom/android/managedprovisioning/EncryptDeviceActivity$2;-><init>(Lcom/android/managedprovisioning/EncryptDeviceActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 74
    return-void
.end method
