.class public Lcom/android/managedprovisioning/NetworkMonitor;
.super Ljava/lang/Object;
.source "NetworkMonitor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/managedprovisioning/NetworkMonitor$Callback;
    }
.end annotation


# instance fields
.field public final mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mCallback:Lcom/android/managedprovisioning/NetworkMonitor$Callback;

.field private mContext:Landroid/content/Context;

.field private mNetworkConnected:Z

.field private mReceiverRegistered:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/managedprovisioning/NetworkMonitor$Callback;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "callback"    # Lcom/android/managedprovisioning/NetworkMonitor$Callback;

    .prologue
    const/4 v1, 0x0

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object v1, p0, Lcom/android/managedprovisioning/NetworkMonitor;->mContext:Landroid/content/Context;

    .line 42
    iput-object v1, p0, Lcom/android/managedprovisioning/NetworkMonitor;->mCallback:Lcom/android/managedprovisioning/NetworkMonitor$Callback;

    .line 44
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/managedprovisioning/NetworkMonitor;->mNetworkConnected:Z

    .line 89
    new-instance v1, Lcom/android/managedprovisioning/NetworkMonitor$1;

    invoke-direct {v1, p0}, Lcom/android/managedprovisioning/NetworkMonitor$1;-><init>(Lcom/android/managedprovisioning/NetworkMonitor;)V

    iput-object v1, p0, Lcom/android/managedprovisioning/NetworkMonitor;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 57
    iput-object p1, p0, Lcom/android/managedprovisioning/NetworkMonitor;->mContext:Landroid/content/Context;

    .line 58
    iput-object p2, p0, Lcom/android/managedprovisioning/NetworkMonitor;->mCallback:Lcom/android/managedprovisioning/NetworkMonitor$Callback;

    .line 60
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 61
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 67
    const-string v1, "android.net.conn.INET_CONDITION_ACTION"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 69
    iget-object v1, p0, Lcom/android/managedprovisioning/NetworkMonitor;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 70
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/managedprovisioning/NetworkMonitor;->mReceiverRegistered:Z

    .line 72
    return-void
.end method

.method static synthetic access$000(Lcom/android/managedprovisioning/NetworkMonitor;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/managedprovisioning/NetworkMonitor;

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/android/managedprovisioning/NetworkMonitor;->mNetworkConnected:Z

    return v0
.end method

.method static synthetic access$002(Lcom/android/managedprovisioning/NetworkMonitor;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/android/managedprovisioning/NetworkMonitor;
    .param p1, "x1"    # Z

    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/android/managedprovisioning/NetworkMonitor;->mNetworkConnected:Z

    return p1
.end method

.method static synthetic access$100(Lcom/android/managedprovisioning/NetworkMonitor;)Lcom/android/managedprovisioning/NetworkMonitor$Callback;
    .locals 1
    .param p0, "x0"    # Lcom/android/managedprovisioning/NetworkMonitor;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/android/managedprovisioning/NetworkMonitor;->mCallback:Lcom/android/managedprovisioning/NetworkMonitor$Callback;

    return-object v0
.end method

.method public static isConnectedToWifi(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 108
    const-string v4, "connectivity"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 110
    .local v0, "cm":Landroid/net/ConnectivityManager;
    if-eqz v0, :cond_0

    .line 111
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    .line 112
    .local v2, "ni":Landroid/net/NetworkInfo;
    if-eqz v2, :cond_0

    .line 113
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v1

    .line 114
    .local v1, "detailedState":Landroid/net/NetworkInfo$DetailedState;
    sget-object v3, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    invoke-virtual {v1, v3}, Landroid/net/NetworkInfo$DetailedState;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 119
    .end local v1    # "detailedState":Landroid/net/NetworkInfo$DetailedState;
    .end local v2    # "ni":Landroid/net/NetworkInfo;
    :cond_0
    return v3
.end method


# virtual methods
.method public declared-synchronized close()V
    .locals 2

    .prologue
    .line 78
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/managedprovisioning/NetworkMonitor;->mCallback:Lcom/android/managedprovisioning/NetworkMonitor$Callback;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 87
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 81
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    iput-object v0, p0, Lcom/android/managedprovisioning/NetworkMonitor;->mCallback:Lcom/android/managedprovisioning/NetworkMonitor$Callback;

    .line 83
    iget-boolean v0, p0, Lcom/android/managedprovisioning/NetworkMonitor;->mReceiverRegistered:Z

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/android/managedprovisioning/NetworkMonitor;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/managedprovisioning/NetworkMonitor;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 85
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/managedprovisioning/NetworkMonitor;->mReceiverRegistered:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 78
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
