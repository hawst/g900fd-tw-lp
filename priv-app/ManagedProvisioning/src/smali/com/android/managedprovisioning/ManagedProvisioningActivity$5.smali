.class Lcom/android/managedprovisioning/ManagedProvisioningActivity$5;
.super Ljava/lang/Object;
.source "ManagedProvisioningActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/managedprovisioning/ManagedProvisioningActivity;->showManagedProfileExistsDialog(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/managedprovisioning/ManagedProvisioningActivity;

.field final synthetic val$existingManagedProfileUserId:I


# direct methods
.method constructor <init>(Lcom/android/managedprovisioning/ManagedProvisioningActivity;I)V
    .locals 0

    .prologue
    .line 421
    iput-object p1, p0, Lcom/android/managedprovisioning/ManagedProvisioningActivity$5;->this$0:Lcom/android/managedprovisioning/ManagedProvisioningActivity;

    iput p2, p0, Lcom/android/managedprovisioning/ManagedProvisioningActivity$5;->val$existingManagedProfileUserId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 425
    new-instance v0, Lcom/android/managedprovisioning/ManagedProvisioningActivity$5$1;

    invoke-direct {v0, p0}, Lcom/android/managedprovisioning/ManagedProvisioningActivity$5$1;-><init>(Lcom/android/managedprovisioning/ManagedProvisioningActivity$5;)V

    .line 435
    .local v0, "deleteListener":Landroid/content/DialogInterface$OnClickListener;
    iget-object v1, p0, Lcom/android/managedprovisioning/ManagedProvisioningActivity$5;->this$0:Lcom/android/managedprovisioning/ManagedProvisioningActivity;

    iget-object v2, p0, Lcom/android/managedprovisioning/ManagedProvisioningActivity$5;->this$0:Lcom/android/managedprovisioning/ManagedProvisioningActivity;

    const v3, 0x7f07000f

    invoke-virtual {v2, v3}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/android/managedprovisioning/ManagedProvisioningActivity;->buildDeleteManagedProfileDialog(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog;
    invoke-static {v1, v2, v0}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->access$400(Lcom/android/managedprovisioning/ManagedProvisioningActivity;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 438
    return-void
.end method
