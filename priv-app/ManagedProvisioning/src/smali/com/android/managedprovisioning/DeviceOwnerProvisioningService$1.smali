.class Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$1;
.super Ljava/lang/Object;
.source "DeviceOwnerProvisioningService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->onStartCommand(Landroid/content/Intent;II)I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;


# direct methods
.method constructor <init>(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;)V
    .locals 0

    .prologue
    .line 141
    iput-object p1, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$1;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 143
    iget-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$1;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    iget-object v1, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$1;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    # getter for: Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mParams:Lcom/android/managedprovisioning/ProvisioningParams;
    invoke-static {v1}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->access$000(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;)Lcom/android/managedprovisioning/ProvisioningParams;

    move-result-object v1

    # invokes: Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->initializeProvisioningEnvironment(Lcom/android/managedprovisioning/ProvisioningParams;)V
    invoke-static {v0, v1}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->access$100(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;Lcom/android/managedprovisioning/ProvisioningParams;)V

    .line 144
    iget-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$1;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    iget-object v1, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$1;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    # getter for: Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mParams:Lcom/android/managedprovisioning/ProvisioningParams;
    invoke-static {v1}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->access$000(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;)Lcom/android/managedprovisioning/ProvisioningParams;

    move-result-object v1

    # invokes: Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->startDeviceOwnerProvisioning(Lcom/android/managedprovisioning/ProvisioningParams;)V
    invoke-static {v0, v1}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->access$200(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;Lcom/android/managedprovisioning/ProvisioningParams;)V

    .line 145
    return-void
.end method
