.class final enum Lcom/android/managedprovisioning/WifiConfig$SecurityType;
.super Ljava/lang/Enum;
.source "WifiConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/managedprovisioning/WifiConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "SecurityType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/managedprovisioning/WifiConfig$SecurityType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/managedprovisioning/WifiConfig$SecurityType;

.field public static final enum NONE:Lcom/android/managedprovisioning/WifiConfig$SecurityType;

.field public static final enum WEP:Lcom/android/managedprovisioning/WifiConfig$SecurityType;

.field public static final enum WPA:Lcom/android/managedprovisioning/WifiConfig$SecurityType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 33
    new-instance v0, Lcom/android/managedprovisioning/WifiConfig$SecurityType;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lcom/android/managedprovisioning/WifiConfig$SecurityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/managedprovisioning/WifiConfig$SecurityType;->NONE:Lcom/android/managedprovisioning/WifiConfig$SecurityType;

    .line 34
    new-instance v0, Lcom/android/managedprovisioning/WifiConfig$SecurityType;

    const-string v1, "WPA"

    invoke-direct {v0, v1, v3}, Lcom/android/managedprovisioning/WifiConfig$SecurityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/managedprovisioning/WifiConfig$SecurityType;->WPA:Lcom/android/managedprovisioning/WifiConfig$SecurityType;

    .line 35
    new-instance v0, Lcom/android/managedprovisioning/WifiConfig$SecurityType;

    const-string v1, "WEP"

    invoke-direct {v0, v1, v4}, Lcom/android/managedprovisioning/WifiConfig$SecurityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/managedprovisioning/WifiConfig$SecurityType;->WEP:Lcom/android/managedprovisioning/WifiConfig$SecurityType;

    .line 32
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/android/managedprovisioning/WifiConfig$SecurityType;

    sget-object v1, Lcom/android/managedprovisioning/WifiConfig$SecurityType;->NONE:Lcom/android/managedprovisioning/WifiConfig$SecurityType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/managedprovisioning/WifiConfig$SecurityType;->WPA:Lcom/android/managedprovisioning/WifiConfig$SecurityType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/managedprovisioning/WifiConfig$SecurityType;->WEP:Lcom/android/managedprovisioning/WifiConfig$SecurityType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/managedprovisioning/WifiConfig$SecurityType;->$VALUES:[Lcom/android/managedprovisioning/WifiConfig$SecurityType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/managedprovisioning/WifiConfig$SecurityType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 32
    const-class v0, Lcom/android/managedprovisioning/WifiConfig$SecurityType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/managedprovisioning/WifiConfig$SecurityType;

    return-object v0
.end method

.method public static values()[Lcom/android/managedprovisioning/WifiConfig$SecurityType;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/android/managedprovisioning/WifiConfig$SecurityType;->$VALUES:[Lcom/android/managedprovisioning/WifiConfig$SecurityType;

    invoke-virtual {v0}, [Lcom/android/managedprovisioning/WifiConfig$SecurityType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/managedprovisioning/WifiConfig$SecurityType;

    return-object v0
.end method
