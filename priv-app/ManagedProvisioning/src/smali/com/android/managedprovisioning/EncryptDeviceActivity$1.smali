.class Lcom/android/managedprovisioning/EncryptDeviceActivity$1;
.super Ljava/lang/Object;
.source "EncryptDeviceActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/managedprovisioning/EncryptDeviceActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/managedprovisioning/EncryptDeviceActivity;


# direct methods
.method constructor <init>(Lcom/android/managedprovisioning/EncryptDeviceActivity;)V
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/android/managedprovisioning/EncryptDeviceActivity$1;->this$0:Lcom/android/managedprovisioning/EncryptDeviceActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 58
    iget-object v2, p0, Lcom/android/managedprovisioning/EncryptDeviceActivity$1;->this$0:Lcom/android/managedprovisioning/EncryptDeviceActivity;

    invoke-virtual {v2}, Lcom/android/managedprovisioning/EncryptDeviceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "com.android.managedprovisioning.RESUME"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 59
    .local v1, "resumeInfo":Landroid/os/Bundle;
    iget-object v2, p0, Lcom/android/managedprovisioning/EncryptDeviceActivity$1;->this$0:Lcom/android/managedprovisioning/EncryptDeviceActivity;

    invoke-static {v2, v1}, Lcom/android/managedprovisioning/BootReminder;->setProvisioningReminder(Landroid/content/Context;Landroid/os/Bundle;)V

    .line 62
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 63
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "android.app.action.START_ENCRYPTION"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 64
    iget-object v2, p0, Lcom/android/managedprovisioning/EncryptDeviceActivity$1;->this$0:Lcom/android/managedprovisioning/EncryptDeviceActivity;

    invoke-virtual {v2, v0}, Lcom/android/managedprovisioning/EncryptDeviceActivity;->startActivity(Landroid/content/Intent;)V

    .line 65
    return-void
.end method
