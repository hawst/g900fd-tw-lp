.class Lcom/android/managedprovisioning/ManagedProvisioningActivity$ServiceMessageReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ManagedProvisioningActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/managedprovisioning/ManagedProvisioningActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ServiceMessageReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/managedprovisioning/ManagedProvisioningActivity;


# direct methods
.method constructor <init>(Lcom/android/managedprovisioning/ManagedProvisioningActivity;)V
    .locals 0

    .prologue
    .line 211
    iput-object p1, p0, Lcom/android/managedprovisioning/ManagedProvisioningActivity$ServiceMessageReceiver;->this$0:Lcom/android/managedprovisioning/ManagedProvisioningActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 214
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 215
    .local v0, "action":Ljava/lang/String;
    const-string v2, "com.android.managedprovisioning.provisioning_success"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 216
    const-string v2, "Successfully provisioned.Finishing ManagedProvisioningActivity"

    invoke-static {v2}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 218
    iget-object v2, p0, Lcom/android/managedprovisioning/ManagedProvisioningActivity$ServiceMessageReceiver;->this$0:Lcom/android/managedprovisioning/ManagedProvisioningActivity;

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->setResult(I)V

    .line 219
    iget-object v2, p0, Lcom/android/managedprovisioning/ManagedProvisioningActivity$ServiceMessageReceiver;->this$0:Lcom/android/managedprovisioning/ManagedProvisioningActivity;

    invoke-virtual {v2}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->finish()V

    .line 228
    :cond_0
    :goto_0
    return-void

    .line 221
    :cond_1
    const-string v2, "com.android.managedprovisioning.error"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 222
    const-string v2, "ProvisioingErrorLogMessage"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 224
    .local v1, "errorLogMessage":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error reported: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 225
    iget-object v2, p0, Lcom/android/managedprovisioning/ManagedProvisioningActivity$ServiceMessageReceiver;->this$0:Lcom/android/managedprovisioning/ManagedProvisioningActivity;

    const v3, 0x7f070017

    invoke-virtual {v2, v3, v1}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->showErrorAndClose(ILjava/lang/String;)V

    goto :goto_0
.end method
