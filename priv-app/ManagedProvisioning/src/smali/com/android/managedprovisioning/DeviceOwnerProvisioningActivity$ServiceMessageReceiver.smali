.class Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity$ServiceMessageReceiver;
.super Landroid/content/BroadcastReceiver;
.source "DeviceOwnerProvisioningActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ServiceMessageReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;


# direct methods
.method constructor <init>(Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;)V
    .locals 0

    .prologue
    .line 186
    iput-object p1, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity$ServiceMessageReceiver;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x1

    .line 191
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 192
    .local v0, "action":Ljava/lang/String;
    const-string v3, "com.android.managedprovisioning.provisioning_success"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 193
    const-string v3, "Successfully provisioned"

    invoke-static {v3}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 195
    monitor-enter p0

    .line 196
    :try_start_0
    iget-object v3, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity$ServiceMessageReceiver;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;

    # getter for: Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->mDialog:Landroid/app/Dialog;
    invoke-static {v3}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->access$300(Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;)Landroid/app/Dialog;

    move-result-object v3

    if-nez v3, :cond_1

    .line 197
    iget-object v3, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity$ServiceMessageReceiver;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;

    # invokes: Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->onProvisioningSuccess()V
    invoke-static {v3}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->access$400(Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;)V

    .line 203
    :goto_0
    monitor-exit p0

    .line 222
    :cond_0
    :goto_1
    return-void

    .line 201
    :cond_1
    iget-object v3, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity$ServiceMessageReceiver;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;

    const/4 v4, 0x1

    # setter for: Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->mDone:Z
    invoke-static {v3, v4}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->access$502(Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;Z)Z

    goto :goto_0

    .line 203
    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 205
    :cond_2
    const-string v3, "com.android.managedprovisioning.error"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 206
    const-string v3, "UserVisibleErrorMessage-Id"

    const v4, 0x7f070029

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 210
    .local v1, "errorMessageId":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error reported with code "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity$ServiceMessageReceiver;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;

    invoke-virtual {v4}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 212
    iget-object v3, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity$ServiceMessageReceiver;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;

    # invokes: Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->error(IZ)V
    invoke-static {v3, v1, v5}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->access$600(Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;IZ)V

    goto :goto_1

    .line 213
    .end local v1    # "errorMessageId":I
    :cond_3
    const-string v3, "com.android.managedprovisioning.progress_update"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 214
    const-string v3, "ProgressMessageId"

    const/4 v4, -0x1

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 216
    .local v2, "progressMessage":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Progress update reported with code "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity$ServiceMessageReceiver;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;

    invoke-virtual {v4}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 218
    if-ltz v2, :cond_0

    .line 219
    iget-object v3, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity$ServiceMessageReceiver;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;

    # invokes: Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->progressUpdate(I)V
    invoke-static {v3, v2}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->access$700(Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;I)V

    goto :goto_1
.end method
