.class Lcom/android/managedprovisioning/ManagedProvisioningActivity$5$1;
.super Ljava/lang/Object;
.source "ManagedProvisioningActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/managedprovisioning/ManagedProvisioningActivity$5;->onClick(Landroid/content/DialogInterface;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/managedprovisioning/ManagedProvisioningActivity$5;


# direct methods
.method constructor <init>(Lcom/android/managedprovisioning/ManagedProvisioningActivity$5;)V
    .locals 0

    .prologue
    .line 426
    iput-object p1, p0, Lcom/android/managedprovisioning/ManagedProvisioningActivity$5$1;->this$1:Lcom/android/managedprovisioning/ManagedProvisioningActivity$5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 429
    iget-object v1, p0, Lcom/android/managedprovisioning/ManagedProvisioningActivity$5$1;->this$1:Lcom/android/managedprovisioning/ManagedProvisioningActivity$5;

    iget-object v1, v1, Lcom/android/managedprovisioning/ManagedProvisioningActivity$5;->this$0:Lcom/android/managedprovisioning/ManagedProvisioningActivity;

    const-string v2, "user"

    invoke-virtual {v1, v2}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    .line 431
    .local v0, "userManager":Landroid/os/UserManager;
    iget-object v1, p0, Lcom/android/managedprovisioning/ManagedProvisioningActivity$5$1;->this$1:Lcom/android/managedprovisioning/ManagedProvisioningActivity$5;

    iget v1, v1, Lcom/android/managedprovisioning/ManagedProvisioningActivity$5;->val$existingManagedProfileUserId:I

    invoke-virtual {v0, v1}, Landroid/os/UserManager;->removeUser(I)Z

    .line 432
    iget-object v1, p0, Lcom/android/managedprovisioning/ManagedProvisioningActivity$5$1;->this$1:Lcom/android/managedprovisioning/ManagedProvisioningActivity$5;

    iget-object v1, v1, Lcom/android/managedprovisioning/ManagedProvisioningActivity$5;->this$0:Lcom/android/managedprovisioning/ManagedProvisioningActivity;

    # invokes: Lcom/android/managedprovisioning/ManagedProvisioningActivity;->showStartProvisioningScreen()V
    invoke-static {v1}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->access$300(Lcom/android/managedprovisioning/ManagedProvisioningActivity;)V

    .line 433
    return-void
.end method
