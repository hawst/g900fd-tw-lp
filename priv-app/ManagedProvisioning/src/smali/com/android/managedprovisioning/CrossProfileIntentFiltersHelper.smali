.class public Lcom/android/managedprovisioning/CrossProfileIntentFiltersHelper;
.super Ljava/lang/Object;
.source "CrossProfileIntentFiltersHelper.java"


# direct methods
.method public static setFilters(Landroid/content/pm/PackageManager;II)V
    .locals 22
    .param p0, "pm"    # Landroid/content/pm/PackageManager;
    .param p1, "parentUserId"    # I
    .param p2, "managedProfileUserId"    # I

    .prologue
    .line 36
    const-string v21, "Setting cross-profile intent filters"

    invoke-static/range {v21 .. v21}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 40
    new-instance v13, Landroid/content/IntentFilter;

    invoke-direct {v13}, Landroid/content/IntentFilter;-><init>()V

    .line 41
    .local v13, "mimeTypeTelephony":Landroid/content/IntentFilter;
    const-string v21, "android.intent.action.DIAL"

    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 42
    const-string v21, "android.intent.action.VIEW"

    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 43
    const-string v21, "android.intent.action.CALL_EMERGENCY"

    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 44
    const-string v21, "android.intent.action.CALL_PRIVILEGED"

    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 45
    const-string v21, "android.intent.category.DEFAULT"

    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 46
    const-string v21, "android.intent.category.BROWSABLE"

    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 48
    :try_start_0
    const-string v21, "vnd.android.cursor.item/phone"

    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Landroid/content/IntentFilter;->addDataType(Ljava/lang/String;)V

    .line 49
    const-string v21, "vnd.android.cursor.item/phone_v2"

    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Landroid/content/IntentFilter;->addDataType(Ljava/lang/String;)V

    .line 50
    const-string v21, "vnd.android.cursor.item/person"

    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Landroid/content/IntentFilter;->addDataType(Ljava/lang/String;)V

    .line 51
    const-string v21, "vnd.android.cursor.dir/calls"

    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Landroid/content/IntentFilter;->addDataType(Ljava/lang/String;)V

    .line 52
    const-string v21, "vnd.android.cursor.item/calls"

    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Landroid/content/IntentFilter;->addDataType(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/IntentFilter$MalformedMimeTypeException; {:try_start_0 .. :try_end_0} :catch_4

    .line 56
    :goto_0
    const/16 v21, 0x2

    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p1

    move/from16 v3, v21

    invoke-virtual {v0, v13, v1, v2, v3}, Landroid/content/pm/PackageManager;->addCrossProfileIntentFilter(Landroid/content/IntentFilter;III)V

    .line 59
    new-instance v8, Landroid/content/IntentFilter;

    invoke-direct {v8}, Landroid/content/IntentFilter;-><init>()V

    .line 60
    .local v8, "callEmergency":Landroid/content/IntentFilter;
    const-string v21, "android.intent.action.CALL_EMERGENCY"

    move-object/from16 v0, v21

    invoke-virtual {v8, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 61
    const-string v21, "android.intent.action.CALL_PRIVILEGED"

    move-object/from16 v0, v21

    invoke-virtual {v8, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 62
    const-string v21, "android.intent.category.DEFAULT"

    move-object/from16 v0, v21

    invoke-virtual {v8, v0}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 63
    const-string v21, "android.intent.category.BROWSABLE"

    move-object/from16 v0, v21

    invoke-virtual {v8, v0}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 64
    const-string v21, "tel"

    move-object/from16 v0, v21

    invoke-virtual {v8, v0}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 65
    const-string v21, "sip"

    move-object/from16 v0, v21

    invoke-virtual {v8, v0}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 66
    const-string v21, "voicemail"

    move-object/from16 v0, v21

    invoke-virtual {v8, v0}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 67
    const/16 v21, 0x2

    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p1

    move/from16 v3, v21

    invoke-virtual {v0, v8, v1, v2, v3}, Landroid/content/pm/PackageManager;->addCrossProfileIntentFilter(Landroid/content/IntentFilter;III)V

    .line 70
    new-instance v9, Landroid/content/IntentFilter;

    invoke-direct {v9}, Landroid/content/IntentFilter;-><init>()V

    .line 71
    .local v9, "callVoicemail":Landroid/content/IntentFilter;
    const-string v21, "android.intent.action.DIAL"

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 72
    const-string v21, "android.intent.action.CALL"

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 73
    const-string v21, "android.intent.action.VIEW"

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 74
    const-string v21, "android.intent.category.DEFAULT"

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 75
    const-string v21, "android.intent.category.BROWSABLE"

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 76
    const-string v21, "voicemail"

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 77
    const/16 v21, 0x2

    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p1

    move/from16 v3, v21

    invoke-virtual {v0, v9, v1, v2, v3}, Landroid/content/pm/PackageManager;->addCrossProfileIntentFilter(Landroid/content/IntentFilter;III)V

    .line 82
    new-instance v6, Landroid/content/IntentFilter;

    invoke-direct {v6}, Landroid/content/IntentFilter;-><init>()V

    .line 83
    .local v6, "callDial":Landroid/content/IntentFilter;
    const-string v21, "android.intent.action.DIAL"

    move-object/from16 v0, v21

    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 84
    const-string v21, "android.intent.action.CALL"

    move-object/from16 v0, v21

    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 85
    const-string v21, "android.intent.action.VIEW"

    move-object/from16 v0, v21

    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 86
    const-string v21, "android.intent.category.DEFAULT"

    move-object/from16 v0, v21

    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 87
    const-string v21, "android.intent.category.BROWSABLE"

    move-object/from16 v0, v21

    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 88
    const-string v21, "tel"

    move-object/from16 v0, v21

    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 89
    const-string v21, "sip"

    move-object/from16 v0, v21

    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 90
    const/16 v21, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p1

    move/from16 v3, v21

    invoke-virtual {v0, v6, v1, v2, v3}, Landroid/content/pm/PackageManager;->addCrossProfileIntentFilter(Landroid/content/IntentFilter;III)V

    .line 92
    new-instance v5, Landroid/content/IntentFilter;

    invoke-direct {v5}, Landroid/content/IntentFilter;-><init>()V

    .line 93
    .local v5, "callButton":Landroid/content/IntentFilter;
    const-string v21, "android.intent.action.CALL_BUTTON"

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 94
    const-string v21, "android.intent.category.DEFAULT"

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 95
    const/16 v21, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p1

    move/from16 v3, v21

    invoke-virtual {v0, v5, v1, v2, v3}, Landroid/content/pm/PackageManager;->addCrossProfileIntentFilter(Landroid/content/IntentFilter;III)V

    .line 97
    new-instance v7, Landroid/content/IntentFilter;

    invoke-direct {v7}, Landroid/content/IntentFilter;-><init>()V

    .line 98
    .local v7, "callDialNoData":Landroid/content/IntentFilter;
    const-string v21, "android.intent.action.DIAL"

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 99
    const-string v21, "android.intent.action.CALL"

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 100
    const-string v21, "android.intent.category.DEFAULT"

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 101
    const-string v21, "android.intent.category.BROWSABLE"

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 102
    const/16 v21, 0x2

    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p1

    move/from16 v3, v21

    invoke-virtual {v0, v7, v1, v2, v3}, Landroid/content/pm/PackageManager;->addCrossProfileIntentFilter(Landroid/content/IntentFilter;III)V

    .line 105
    new-instance v20, Landroid/content/IntentFilter;

    invoke-direct/range {v20 .. v20}, Landroid/content/IntentFilter;-><init>()V

    .line 106
    .local v20, "smsMms":Landroid/content/IntentFilter;
    const-string v21, "android.intent.action.VIEW"

    invoke-virtual/range {v20 .. v21}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 107
    const-string v21, "android.intent.action.SENDTO"

    invoke-virtual/range {v20 .. v21}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 108
    const-string v21, "android.intent.category.DEFAULT"

    invoke-virtual/range {v20 .. v21}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 109
    const-string v21, "android.intent.category.BROWSABLE"

    invoke-virtual/range {v20 .. v21}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 110
    const-string v21, "sms"

    invoke-virtual/range {v20 .. v21}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 111
    const-string v21, "smsto"

    invoke-virtual/range {v20 .. v21}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 112
    const-string v21, "mms"

    invoke-virtual/range {v20 .. v21}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 113
    const-string v21, "mmsto"

    invoke-virtual/range {v20 .. v21}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 114
    const/16 v21, 0x2

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move/from16 v2, p2

    move/from16 v3, p1

    move/from16 v4, v21

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/pm/PackageManager;->addCrossProfileIntentFilter(Landroid/content/IntentFilter;III)V

    .line 117
    new-instance v14, Landroid/content/IntentFilter;

    invoke-direct {v14}, Landroid/content/IntentFilter;-><init>()V

    .line 118
    .local v14, "mobileNetworkSettings":Landroid/content/IntentFilter;
    const-string v21, "android.settings.DATA_ROAMING_SETTINGS"

    move-object/from16 v0, v21

    invoke-virtual {v14, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 119
    const-string v21, "android.settings.NETWORK_OPERATOR_SETTINGS"

    move-object/from16 v0, v21

    invoke-virtual {v14, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 120
    const-string v21, "android.intent.category.DEFAULT"

    move-object/from16 v0, v21

    invoke-virtual {v14, v0}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 121
    const/16 v21, 0x2

    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p1

    move/from16 v3, v21

    invoke-virtual {v0, v14, v1, v2, v3}, Landroid/content/pm/PackageManager;->addCrossProfileIntentFilter(Landroid/content/IntentFilter;III)V

    .line 124
    new-instance v12, Landroid/content/IntentFilter;

    invoke-direct {v12}, Landroid/content/IntentFilter;-><init>()V

    .line 125
    .local v12, "home":Landroid/content/IntentFilter;
    const-string v21, "android.intent.action.MAIN"

    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 126
    const-string v21, "android.intent.category.DEFAULT"

    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 127
    const-string v21, "android.intent.category.HOME"

    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 128
    const/16 v21, 0x2

    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p1

    move/from16 v3, v21

    invoke-virtual {v0, v12, v1, v2, v3}, Landroid/content/pm/PackageManager;->addCrossProfileIntentFilter(Landroid/content/IntentFilter;III)V

    .line 131
    new-instance v19, Landroid/content/IntentFilter;

    invoke-direct/range {v19 .. v19}, Landroid/content/IntentFilter;-><init>()V

    .line 132
    .local v19, "send":Landroid/content/IntentFilter;
    const-string v21, "android.intent.action.SEND"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 133
    const-string v21, "android.intent.action.SEND_MULTIPLE"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 134
    const-string v21, "android.intent.category.DEFAULT"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 136
    :try_start_1
    const-string v21, "*/*"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataType(Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/content/IntentFilter$MalformedMimeTypeException; {:try_start_1 .. :try_end_1} :catch_3

    .line 141
    :goto_1
    const/16 v21, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, p1

    move/from16 v3, p2

    move/from16 v4, v21

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/pm/PackageManager;->addCrossProfileIntentFilter(Landroid/content/IntentFilter;III)V

    .line 143
    new-instance v11, Landroid/content/IntentFilter;

    invoke-direct {v11}, Landroid/content/IntentFilter;-><init>()V

    .line 144
    .local v11, "getContent":Landroid/content/IntentFilter;
    const-string v21, "android.intent.action.GET_CONTENT"

    move-object/from16 v0, v21

    invoke-virtual {v11, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 145
    const-string v21, "android.intent.category.DEFAULT"

    move-object/from16 v0, v21

    invoke-virtual {v11, v0}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 146
    const-string v21, "android.intent.category.OPENABLE"

    move-object/from16 v0, v21

    invoke-virtual {v11, v0}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 148
    :try_start_2
    const-string v21, "*/*"

    move-object/from16 v0, v21

    invoke-virtual {v11, v0}, Landroid/content/IntentFilter;->addDataType(Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/content/IntentFilter$MalformedMimeTypeException; {:try_start_2 .. :try_end_2} :catch_2

    .line 152
    :goto_2
    const/16 v21, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p1

    move/from16 v3, v21

    invoke-virtual {v0, v11, v1, v2, v3}, Landroid/content/pm/PackageManager;->addCrossProfileIntentFilter(Landroid/content/IntentFilter;III)V

    .line 154
    new-instance v15, Landroid/content/IntentFilter;

    invoke-direct {v15}, Landroid/content/IntentFilter;-><init>()V

    .line 155
    .local v15, "openDocument":Landroid/content/IntentFilter;
    const-string v21, "android.intent.action.OPEN_DOCUMENT"

    move-object/from16 v0, v21

    invoke-virtual {v15, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 156
    const-string v21, "android.intent.category.DEFAULT"

    move-object/from16 v0, v21

    invoke-virtual {v15, v0}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 157
    const-string v21, "android.intent.category.OPENABLE"

    move-object/from16 v0, v21

    invoke-virtual {v15, v0}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 159
    :try_start_3
    const-string v21, "*/*"

    move-object/from16 v0, v21

    invoke-virtual {v15, v0}, Landroid/content/IntentFilter;->addDataType(Ljava/lang/String;)V
    :try_end_3
    .catch Landroid/content/IntentFilter$MalformedMimeTypeException; {:try_start_3 .. :try_end_3} :catch_1

    .line 163
    :goto_3
    const/16 v21, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p1

    move/from16 v3, v21

    invoke-virtual {v0, v15, v1, v2, v3}, Landroid/content/pm/PackageManager;->addCrossProfileIntentFilter(Landroid/content/IntentFilter;III)V

    .line 165
    new-instance v16, Landroid/content/IntentFilter;

    invoke-direct/range {v16 .. v16}, Landroid/content/IntentFilter;-><init>()V

    .line 166
    .local v16, "pick":Landroid/content/IntentFilter;
    const-string v21, "android.intent.action.PICK"

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 167
    const-string v21, "android.intent.category.DEFAULT"

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 169
    :try_start_4
    const-string v21, "*/*"

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataType(Ljava/lang/String;)V
    :try_end_4
    .catch Landroid/content/IntentFilter$MalformedMimeTypeException; {:try_start_4 .. :try_end_4} :catch_0

    .line 173
    :goto_4
    const/16 v21, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    move/from16 v2, p2

    move/from16 v3, p1

    move/from16 v4, v21

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/pm/PackageManager;->addCrossProfileIntentFilter(Landroid/content/IntentFilter;III)V

    .line 175
    new-instance v17, Landroid/content/IntentFilter;

    invoke-direct/range {v17 .. v17}, Landroid/content/IntentFilter;-><init>()V

    .line 176
    .local v17, "pickNoData":Landroid/content/IntentFilter;
    const-string v21, "android.intent.action.PICK"

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 177
    const-string v21, "android.intent.category.DEFAULT"

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 178
    const/16 v21, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move/from16 v2, p2

    move/from16 v3, p1

    move/from16 v4, v21

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/pm/PackageManager;->addCrossProfileIntentFilter(Landroid/content/IntentFilter;III)V

    .line 181
    new-instance v18, Landroid/content/IntentFilter;

    invoke-direct/range {v18 .. v18}, Landroid/content/IntentFilter;-><init>()V

    .line 182
    .local v18, "recognizeSpeech":Landroid/content/IntentFilter;
    const-string v21, "android.speech.action.RECOGNIZE_SPEECH"

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 183
    const-string v21, "android.intent.category.DEFAULT"

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 184
    const/16 v21, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, p2

    move/from16 v3, p1

    move/from16 v4, v21

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/pm/PackageManager;->addCrossProfileIntentFilter(Landroid/content/IntentFilter;III)V

    .line 186
    new-instance v10, Landroid/content/IntentFilter;

    invoke-direct {v10}, Landroid/content/IntentFilter;-><init>()V

    .line 187
    .local v10, "capture":Landroid/content/IntentFilter;
    const-string v21, "android.media.action.IMAGE_CAPTURE"

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 188
    const-string v21, "android.media.action.IMAGE_CAPTURE_SECURE"

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 189
    const-string v21, "android.media.action.VIDEO_CAPTURE"

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 190
    const-string v21, "android.media.action.STILL_IMAGE_CAMERA"

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 191
    const-string v21, "android.media.action.STILL_IMAGE_CAMERA_SECURE"

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 192
    const-string v21, "android.media.action.VIDEO_CAMERA"

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 193
    const-string v21, "android.intent.category.DEFAULT"

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 194
    const/16 v21, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p1

    move/from16 v3, v21

    invoke-virtual {v0, v10, v1, v2, v3}, Landroid/content/pm/PackageManager;->addCrossProfileIntentFilter(Landroid/content/IntentFilter;III)V

    .line 195
    return-void

    .line 170
    .end local v10    # "capture":Landroid/content/IntentFilter;
    .end local v17    # "pickNoData":Landroid/content/IntentFilter;
    .end local v18    # "recognizeSpeech":Landroid/content/IntentFilter;
    :catch_0
    move-exception v21

    goto/16 :goto_4

    .line 160
    .end local v16    # "pick":Landroid/content/IntentFilter;
    :catch_1
    move-exception v21

    goto/16 :goto_3

    .line 149
    .end local v15    # "openDocument":Landroid/content/IntentFilter;
    :catch_2
    move-exception v21

    goto/16 :goto_2

    .line 137
    .end local v11    # "getContent":Landroid/content/IntentFilter;
    :catch_3
    move-exception v21

    goto/16 :goto_1

    .line 53
    .end local v5    # "callButton":Landroid/content/IntentFilter;
    .end local v6    # "callDial":Landroid/content/IntentFilter;
    .end local v7    # "callDialNoData":Landroid/content/IntentFilter;
    .end local v8    # "callEmergency":Landroid/content/IntentFilter;
    .end local v9    # "callVoicemail":Landroid/content/IntentFilter;
    .end local v12    # "home":Landroid/content/IntentFilter;
    .end local v14    # "mobileNetworkSettings":Landroid/content/IntentFilter;
    .end local v19    # "send":Landroid/content/IntentFilter;
    .end local v20    # "smsMms":Landroid/content/IntentFilter;
    :catch_4
    move-exception v21

    goto/16 :goto_0
.end method
