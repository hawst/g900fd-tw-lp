.class public Lcom/android/managedprovisioning/MessageParser$ParseException;
.super Ljava/lang/Exception;
.source "MessageParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/managedprovisioning/MessageParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ParseException"
.end annotation


# instance fields
.field private mErrorMessageId:I


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "errorMessageId"    # I

    .prologue
    .line 329
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 330
    iput p2, p0, Lcom/android/managedprovisioning/MessageParser$ParseException;->mErrorMessageId:I

    .line 331
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/Throwable;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "errorMessageId"    # I
    .param p3, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 334
    invoke-direct {p0, p1, p3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 335
    iput p2, p0, Lcom/android/managedprovisioning/MessageParser$ParseException;->mErrorMessageId:I

    .line 336
    return-void
.end method


# virtual methods
.method public getErrorMessageId()I
    .locals 1

    .prologue
    .line 339
    iget v0, p0, Lcom/android/managedprovisioning/MessageParser$ParseException;->mErrorMessageId:I

    return v0
.end method
