.class public Lcom/android/managedprovisioning/IntentStore;
.super Ljava/lang/Object;
.source "IntentStore.java"


# instance fields
.field private mBooleanKeys:[Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mIntKeys:[Ljava/lang/String;

.field private mIntentTarget:Landroid/content/ComponentName;

.field private mLongKeys:[Ljava/lang/String;

.field private mPersistableBundleKeys:[Ljava/lang/String;

.field private mPrefs:Landroid/content/SharedPreferences;

.field private mPrefsName:Ljava/lang/String;

.field private mStringKeys:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/ComponentName;Ljava/lang/String;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intentTarget"    # Landroid/content/ComponentName;
    .param p3, "preferencesName"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/android/managedprovisioning/IntentStore;->mStringKeys:[Ljava/lang/String;

    .line 47
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/android/managedprovisioning/IntentStore;->mLongKeys:[Ljava/lang/String;

    .line 48
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/android/managedprovisioning/IntentStore;->mIntKeys:[Ljava/lang/String;

    .line 49
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/android/managedprovisioning/IntentStore;->mBooleanKeys:[Ljava/lang/String;

    .line 50
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/android/managedprovisioning/IntentStore;->mPersistableBundleKeys:[Ljava/lang/String;

    .line 57
    iput-object p1, p0, Lcom/android/managedprovisioning/IntentStore;->mContext:Landroid/content/Context;

    .line 58
    iput-object p3, p0, Lcom/android/managedprovisioning/IntentStore;->mPrefsName:Ljava/lang/String;

    .line 59
    invoke-virtual {p1, p3, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/android/managedprovisioning/IntentStore;->mPrefs:Landroid/content/SharedPreferences;

    .line 60
    iput-object p2, p0, Lcom/android/managedprovisioning/IntentStore;->mIntentTarget:Landroid/content/ComponentName;

    .line 61
    return-void
.end method

.method private persistableBundleToString(Landroid/os/PersistableBundle;)Ljava/lang/String;
    .locals 6
    .param p1, "bundle"    # Landroid/os/PersistableBundle;

    .prologue
    const/4 v3, 0x0

    .line 163
    if-nez p1, :cond_0

    .line 181
    :goto_0
    return-object v3

    .line 167
    :cond_0
    new-instance v2, Ljava/io/StringWriter;

    invoke-direct {v2}, Ljava/io/StringWriter;-><init>()V

    .line 168
    .local v2, "writer":Ljava/io/StringWriter;
    invoke-static {}, Landroid/util/Xml;->newSerializer()Lorg/xmlpull/v1/XmlSerializer;

    move-result-object v1

    .line 170
    .local v1, "serializer":Lorg/xmlpull/v1/XmlSerializer;
    :try_start_0
    invoke-interface {v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/Writer;)V

    .line 171
    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-interface {v1, v4, v5}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 172
    const/4 v4, 0x0

    const-string v5, "persistable_bundle"

    invoke-interface {v1, v4, v5}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 173
    invoke-virtual {p1, v1}, Landroid/os/PersistableBundle;->saveToXml(Lorg/xmlpull/v1/XmlSerializer;)V

    .line 174
    const/4 v4, 0x0

    const-string v5, "persistable_bundle"

    invoke-interface {v1, v4, v5}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 175
    invoke-interface {v1}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_1

    .line 181
    invoke-virtual {v2}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 176
    :catch_0
    move-exception v0

    .line 177
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    const-string v4, "Persistable bundle could not be stored as string."

    invoke-static {v4, v0}, Lcom/android/managedprovisioning/ProvisionLogger;->loge(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 176
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method private stringToPersistableBundle(Ljava/lang/String;)Landroid/os/PersistableBundle;
    .locals 6
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 185
    if-nez p1, :cond_0

    .line 207
    :goto_0
    return-object v3

    .line 192
    :cond_0
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v1

    .line 194
    .local v1, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    invoke-virtual {v1}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v2

    .line 195
    .local v2, "parser":Lorg/xmlpull/v1/XmlPullParser;
    new-instance v4, Ljava/io/StringReader;

    invoke-direct {v4, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-interface {v2, v4}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 197
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    .line 198
    const-string v4, "persistable_bundle"

    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 199
    invoke-static {v2}, Landroid/os/PersistableBundle;->restoreFromXml(Lorg/xmlpull/v1/XmlPullParser;)Landroid/os/PersistableBundle;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v3

    goto :goto_0

    .line 202
    .end local v1    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v2    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    :catch_0
    move-exception v0

    .line 203
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    invoke-static {v0}, Lcom/android/managedprovisioning/ProvisionLogger;->loge(Ljava/lang/Throwable;)V

    .line 206
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Persistable bundle could not be restored from string "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/managedprovisioning/ProvisionLogger;->loge(Ljava/lang/String;)V

    goto :goto_0

    .line 202
    :catch_1
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/android/managedprovisioning/IntentStore;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 90
    return-void
.end method

.method public load()Landroid/content/Intent;
    .locals 13

    .prologue
    const/4 v7, 0x0

    const/4 v12, 0x0

    .line 122
    iget-object v8, p0, Lcom/android/managedprovisioning/IntentStore;->mPrefs:Landroid/content/SharedPreferences;

    const-string v9, "isSet"

    invoke-interface {v8, v9, v12}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    if-nez v8, :cond_1

    move-object v5, v7

    .line 159
    :cond_0
    return-object v5

    .line 126
    :cond_1
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    .line 127
    .local v5, "result":Landroid/content/Intent;
    iget-object v8, p0, Lcom/android/managedprovisioning/IntentStore;->mIntentTarget:Landroid/content/ComponentName;

    invoke-virtual {v5, v8}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 129
    iget-object v0, p0, Lcom/android/managedprovisioning/IntentStore;->mStringKeys:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v4, :cond_3

    aget-object v3, v0, v2

    .line 130
    .local v3, "key":Ljava/lang/String;
    iget-object v8, p0, Lcom/android/managedprovisioning/IntentStore;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v8, v3, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 131
    .local v6, "value":Ljava/lang/String;
    if-eqz v6, :cond_2

    .line 132
    invoke-virtual {v5, v3, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 129
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 135
    .end local v3    # "key":Ljava/lang/String;
    .end local v6    # "value":Ljava/lang/String;
    :cond_3
    iget-object v0, p0, Lcom/android/managedprovisioning/IntentStore;->mLongKeys:[Ljava/lang/String;

    array-length v4, v0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v4, :cond_5

    aget-object v3, v0, v2

    .line 136
    .restart local v3    # "key":Ljava/lang/String;
    iget-object v8, p0, Lcom/android/managedprovisioning/IntentStore;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v8, v3}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 137
    iget-object v8, p0, Lcom/android/managedprovisioning/IntentStore;->mPrefs:Landroid/content/SharedPreferences;

    const-wide/16 v10, 0x0

    invoke-interface {v8, v3, v10, v11}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    invoke-virtual {v5, v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 135
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 140
    .end local v3    # "key":Ljava/lang/String;
    :cond_5
    iget-object v0, p0, Lcom/android/managedprovisioning/IntentStore;->mIntKeys:[Ljava/lang/String;

    array-length v4, v0

    const/4 v2, 0x0

    :goto_2
    if-ge v2, v4, :cond_7

    aget-object v3, v0, v2

    .line 141
    .restart local v3    # "key":Ljava/lang/String;
    iget-object v8, p0, Lcom/android/managedprovisioning/IntentStore;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v8, v3}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 142
    iget-object v8, p0, Lcom/android/managedprovisioning/IntentStore;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v8, v3, v12}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v8

    invoke-virtual {v5, v3, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 140
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 145
    .end local v3    # "key":Ljava/lang/String;
    :cond_7
    iget-object v0, p0, Lcom/android/managedprovisioning/IntentStore;->mBooleanKeys:[Ljava/lang/String;

    array-length v4, v0

    const/4 v2, 0x0

    :goto_3
    if-ge v2, v4, :cond_9

    aget-object v3, v0, v2

    .line 146
    .restart local v3    # "key":Ljava/lang/String;
    iget-object v8, p0, Lcom/android/managedprovisioning/IntentStore;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v8, v3}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 147
    iget-object v8, p0, Lcom/android/managedprovisioning/IntentStore;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v8, v3, v12}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    invoke-virtual {v5, v3, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 145
    :cond_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 150
    .end local v3    # "key":Ljava/lang/String;
    :cond_9
    iget-object v0, p0, Lcom/android/managedprovisioning/IntentStore;->mPersistableBundleKeys:[Ljava/lang/String;

    array-length v4, v0

    const/4 v2, 0x0

    :goto_4
    if-ge v2, v4, :cond_0

    aget-object v3, v0, v2

    .line 151
    .restart local v3    # "key":Ljava/lang/String;
    iget-object v8, p0, Lcom/android/managedprovisioning/IntentStore;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v8, v3}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_a

    .line 152
    iget-object v8, p0, Lcom/android/managedprovisioning/IntentStore;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v8, v3, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/android/managedprovisioning/IntentStore;->stringToPersistableBundle(Ljava/lang/String;)Landroid/os/PersistableBundle;

    move-result-object v1

    .line 153
    .local v1, "bundle":Landroid/os/PersistableBundle;
    if-eqz v1, :cond_a

    .line 154
    invoke-virtual {v5, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 150
    .end local v1    # "bundle":Landroid/os/PersistableBundle;
    :cond_a
    add-int/lit8 v2, v2, 0x1

    goto :goto_4
.end method

.method public save(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "data"    # Landroid/os/Bundle;

    .prologue
    .line 93
    iget-object v6, p0, Lcom/android/managedprovisioning/IntentStore;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 95
    .local v2, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    .line 96
    iget-object v0, p0, Lcom/android/managedprovisioning/IntentStore;->mStringKeys:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v5, :cond_0

    aget-object v4, v0, v3

    .line 97
    .local v4, "key":Ljava/lang/String;
    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v2, v4, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 96
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 99
    .end local v4    # "key":Ljava/lang/String;
    :cond_0
    iget-object v0, p0, Lcom/android/managedprovisioning/IntentStore;->mLongKeys:[Ljava/lang/String;

    array-length v5, v0

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v5, :cond_1

    aget-object v4, v0, v3

    .line 100
    .restart local v4    # "key":Ljava/lang/String;
    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-interface {v2, v4, v6, v7}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 99
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 102
    .end local v4    # "key":Ljava/lang/String;
    :cond_1
    iget-object v0, p0, Lcom/android/managedprovisioning/IntentStore;->mIntKeys:[Ljava/lang/String;

    array-length v5, v0

    const/4 v3, 0x0

    :goto_2
    if-ge v3, v5, :cond_2

    aget-object v4, v0, v3

    .line 103
    .restart local v4    # "key":Ljava/lang/String;
    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v2, v4, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 102
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 105
    .end local v4    # "key":Ljava/lang/String;
    :cond_2
    iget-object v0, p0, Lcom/android/managedprovisioning/IntentStore;->mBooleanKeys:[Ljava/lang/String;

    array-length v5, v0

    const/4 v3, 0x0

    :goto_3
    if-ge v3, v5, :cond_3

    aget-object v4, v0, v3

    .line 106
    .restart local v4    # "key":Ljava/lang/String;
    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    invoke-interface {v2, v4, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 105
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 108
    .end local v4    # "key":Ljava/lang/String;
    :cond_3
    iget-object v0, p0, Lcom/android/managedprovisioning/IntentStore;->mPersistableBundleKeys:[Ljava/lang/String;

    array-length v5, v0

    const/4 v3, 0x0

    :goto_4
    if-ge v3, v5, :cond_5

    aget-object v4, v0, v3

    .line 111
    .restart local v4    # "key":Ljava/lang/String;
    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v6

    check-cast v6, Landroid/os/PersistableBundle;

    invoke-direct {p0, v6}, Lcom/android/managedprovisioning/IntentStore;->persistableBundleToString(Landroid/os/PersistableBundle;)Ljava/lang/String;

    move-result-object v1

    .line 113
    .local v1, "bundleString":Ljava/lang/String;
    if-eqz v1, :cond_4

    .line 114
    invoke-interface {v2, v4, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 108
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 117
    .end local v1    # "bundleString":Ljava/lang/String;
    .end local v4    # "key":Ljava/lang/String;
    :cond_5
    const-string v6, "isSet"

    const/4 v7, 0x1

    invoke-interface {v2, v6, v7}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 118
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 119
    return-void
.end method

.method public setBooleanKeys([Ljava/lang/String;)Lcom/android/managedprovisioning/IntentStore;
    .locals 1
    .param p1, "keys"    # [Ljava/lang/String;

    .prologue
    .line 79
    if-nez p1, :cond_0

    const/4 v0, 0x0

    new-array p1, v0, [Ljava/lang/String;

    .end local p1    # "keys":[Ljava/lang/String;
    :cond_0
    iput-object p1, p0, Lcom/android/managedprovisioning/IntentStore;->mBooleanKeys:[Ljava/lang/String;

    .line 80
    return-object p0
.end method

.method public setIntKeys([Ljava/lang/String;)Lcom/android/managedprovisioning/IntentStore;
    .locals 1
    .param p1, "keys"    # [Ljava/lang/String;

    .prologue
    .line 74
    if-nez p1, :cond_0

    const/4 v0, 0x0

    new-array p1, v0, [Ljava/lang/String;

    .end local p1    # "keys":[Ljava/lang/String;
    :cond_0
    iput-object p1, p0, Lcom/android/managedprovisioning/IntentStore;->mIntKeys:[Ljava/lang/String;

    .line 75
    return-object p0
.end method

.method public setLongKeys([Ljava/lang/String;)Lcom/android/managedprovisioning/IntentStore;
    .locals 1
    .param p1, "keys"    # [Ljava/lang/String;

    .prologue
    .line 69
    if-nez p1, :cond_0

    const/4 v0, 0x0

    new-array p1, v0, [Ljava/lang/String;

    .end local p1    # "keys":[Ljava/lang/String;
    :cond_0
    iput-object p1, p0, Lcom/android/managedprovisioning/IntentStore;->mLongKeys:[Ljava/lang/String;

    .line 70
    return-object p0
.end method

.method public setPersistableBundleKeys([Ljava/lang/String;)Lcom/android/managedprovisioning/IntentStore;
    .locals 1
    .param p1, "keys"    # [Ljava/lang/String;

    .prologue
    .line 84
    if-nez p1, :cond_0

    const/4 v0, 0x0

    new-array p1, v0, [Ljava/lang/String;

    .end local p1    # "keys":[Ljava/lang/String;
    :cond_0
    iput-object p1, p0, Lcom/android/managedprovisioning/IntentStore;->mPersistableBundleKeys:[Ljava/lang/String;

    .line 85
    return-object p0
.end method

.method public setStringKeys([Ljava/lang/String;)Lcom/android/managedprovisioning/IntentStore;
    .locals 1
    .param p1, "keys"    # [Ljava/lang/String;

    .prologue
    .line 64
    if-nez p1, :cond_0

    const/4 v0, 0x0

    new-array p1, v0, [Ljava/lang/String;

    .end local p1    # "keys":[Ljava/lang/String;
    :cond_0
    iput-object p1, p0, Lcom/android/managedprovisioning/IntentStore;->mStringKeys:[Ljava/lang/String;

    .line 65
    return-object p0
.end method
