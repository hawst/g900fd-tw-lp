.class Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$IndirectHomeReceiver;
.super Landroid/content/BroadcastReceiver;
.source "DeviceOwnerProvisioningService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "IndirectHomeReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;


# direct methods
.method constructor <init>(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;)V
    .locals 0

    .prologue
    .line 163
    iput-object p1, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$IndirectHomeReceiver;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 166
    iget-object v2, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$IndirectHomeReceiver;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    # getter for: Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mDone:Z
    invoke-static {v2}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->access$300(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 187
    :goto_0
    return-void

    .line 171
    :cond_0
    iget-object v2, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$IndirectHomeReceiver;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    invoke-virtual {v2}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 172
    .local v0, "pm":Landroid/content/pm/PackageManager;
    new-instance v2, Landroid/content/ComponentName;

    iget-object v3, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$IndirectHomeReceiver;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    const-class v4, Lcom/android/managedprovisioning/HomeReceiverActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v3, 0x2

    const/4 v4, 0x1

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 177
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.app.action.PROFILE_PROVISIONING_COMPLETE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 178
    .local v1, "result":Landroid/content/Intent;
    iget-object v2, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$IndirectHomeReceiver;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    # getter for: Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mParams:Lcom/android/managedprovisioning/ProvisioningParams;
    invoke-static {v2}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->access$000(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;)Lcom/android/managedprovisioning/ProvisioningParams;

    move-result-object v2

    iget-object v2, v2, Lcom/android/managedprovisioning/ProvisioningParams;->mDeviceAdminPackageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 179
    const v2, 0x10000020

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 181
    iget-object v2, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$IndirectHomeReceiver;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    # getter for: Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mParams:Lcom/android/managedprovisioning/ProvisioningParams;
    invoke-static {v2}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->access$000(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;)Lcom/android/managedprovisioning/ProvisioningParams;

    move-result-object v2

    iget-object v2, v2, Lcom/android/managedprovisioning/ProvisioningParams;->mAdminExtrasBundle:Landroid/os/PersistableBundle;

    if-eqz v2, :cond_1

    .line 182
    const-string v2, "android.app.extra.PROVISIONING_ADMIN_EXTRAS_BUNDLE"

    iget-object v3, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$IndirectHomeReceiver;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    # getter for: Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mParams:Lcom/android/managedprovisioning/ProvisioningParams;
    invoke-static {v3}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->access$000(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;)Lcom/android/managedprovisioning/ProvisioningParams;

    move-result-object v3

    iget-object v3, v3, Lcom/android/managedprovisioning/ProvisioningParams;->mAdminExtrasBundle:Landroid/os/PersistableBundle;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 185
    :cond_1
    iget-object v2, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$IndirectHomeReceiver;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    invoke-virtual {v2, v1}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->sendBroadcast(Landroid/content/Intent;)V

    .line 186
    iget-object v2, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$IndirectHomeReceiver;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    invoke-virtual {v2}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->stopSelf()V

    goto :goto_0
.end method
