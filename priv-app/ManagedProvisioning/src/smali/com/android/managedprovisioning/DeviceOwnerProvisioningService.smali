.class public Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;
.super Landroid/app/Service;
.source "DeviceOwnerProvisioningService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$IndirectHomeReceiver;
    }
.end annotation


# instance fields
.field private mAddWifiNetworkTask:Lcom/android/managedprovisioning/task/AddWifiNetworkTask;

.field private mDeleteNonRequiredAppsTask:Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;

.field private mDone:Z

.field private mDownloadPackageTask:Lcom/android/managedprovisioning/task/DownloadPackageTask;

.field private mIndirectHomeReceiver:Landroid/content/BroadcastReceiver;

.field private mInstallPackageTask:Lcom/android/managedprovisioning/task/InstallPackageTask;

.field private mLastErrorMessage:I

.field private mLastProgressMessage:I

.field private mParams:Lcom/android/managedprovisioning/ProvisioningParams;

.field private mProvisioningInFlight:Z

.field private mSetDevicePolicyTask:Lcom/android/managedprovisioning/task/SetDevicePolicyTask;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 56
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 89
    iput-boolean v1, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mProvisioningInFlight:Z

    .line 92
    iput v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mLastProgressMessage:I

    .line 95
    iput v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mLastErrorMessage:I

    .line 98
    iput-boolean v1, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mDone:Z

    .line 163
    return-void
.end method

.method static synthetic access$000(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;)Lcom/android/managedprovisioning/ProvisioningParams;
    .locals 1
    .param p0, "x0"    # Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mParams:Lcom/android/managedprovisioning/ProvisioningParams;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;Lcom/android/managedprovisioning/ProvisioningParams;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;
    .param p1, "x1"    # Lcom/android/managedprovisioning/ProvisioningParams;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->initializeProvisioningEnvironment(Lcom/android/managedprovisioning/ProvisioningParams;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->onProvisioningSuccess(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;Lcom/android/managedprovisioning/ProvisioningParams;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;
    .param p1, "x1"    # Lcom/android/managedprovisioning/ProvisioningParams;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->startDeviceOwnerProvisioning(Lcom/android/managedprovisioning/ProvisioningParams;)V

    return-void
.end method

.method static synthetic access$300(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mDone:Z

    return v0
.end method

.method static synthetic access$400(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;I)V
    .locals 0
    .param p0, "x0"    # Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;
    .param p1, "x1"    # I

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->progressUpdate(I)V

    return-void
.end method

.method static synthetic access$500(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;)Lcom/android/managedprovisioning/task/DownloadPackageTask;
    .locals 1
    .param p0, "x0"    # Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mDownloadPackageTask:Lcom/android/managedprovisioning/task/DownloadPackageTask;

    return-object v0
.end method

.method static synthetic access$600(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;)Lcom/android/managedprovisioning/task/SetDevicePolicyTask;
    .locals 1
    .param p0, "x0"    # Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mSetDevicePolicyTask:Lcom/android/managedprovisioning/task/SetDevicePolicyTask;

    return-object v0
.end method

.method static synthetic access$700(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;I)V
    .locals 0
    .param p0, "x0"    # Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;
    .param p1, "x1"    # I

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->error(I)V

    return-void
.end method

.method static synthetic access$800(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;)Lcom/android/managedprovisioning/task/InstallPackageTask;
    .locals 1
    .param p0, "x0"    # Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mInstallPackageTask:Lcom/android/managedprovisioning/task/InstallPackageTask;

    return-object v0
.end method

.method static synthetic access$900(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;)Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;
    .locals 1
    .param p0, "x0"    # Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mDeleteNonRequiredAppsTask:Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;

    return-object v0
.end method

.method private error(I)V
    .locals 0
    .param p1, "dialogMessage"    # I

    .prologue
    .line 341
    iput p1, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mLastErrorMessage:I

    .line 342
    invoke-direct {p0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->sendError()V

    .line 344
    return-void
.end method

.method private initializeProvisioningEnvironment(Lcom/android/managedprovisioning/ProvisioningParams;)V
    .locals 4
    .param p1, "params"    # Lcom/android/managedprovisioning/ProvisioningParams;

    .prologue
    .line 387
    iget-object v1, p1, Lcom/android/managedprovisioning/ProvisioningParams;->mTimeZone:Ljava/lang/String;

    iget-wide v2, p1, Lcom/android/managedprovisioning/ProvisioningParams;->mLocalTime:J

    invoke-direct {p0, v1, v2, v3}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->setTimeAndTimezone(Ljava/lang/String;J)V

    .line 388
    iget-object v1, p1, Lcom/android/managedprovisioning/ProvisioningParams;->mLocale:Ljava/util/Locale;

    invoke-direct {p0, v1}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->setLocale(Ljava/util/Locale;)V

    .line 391
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.phone.PERFORM_CDMA_PROVISIONING"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 392
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 393
    const-string v1, "Starting cdma activation activity"

    invoke-static {v1}, Lcom/android/managedprovisioning/ProvisionLogger;->logv(Ljava/lang/String;)V

    .line 394
    invoke-virtual {p0, v0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->startActivity(Landroid/content/Intent;)V

    .line 395
    return-void
.end method

.method private onProvisioningSuccess(Ljava/lang/String;)V
    .locals 5
    .param p1, "deviceAdminPackage"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    .line 369
    const-string v2, "Reporting success."

    invoke-static {v2}, Lcom/android/managedprovisioning/ProvisionLogger;->logv(Ljava/lang/String;)V

    .line 370
    iput-boolean v4, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mDone:Z

    .line 375
    invoke-virtual {p0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 376
    .local v0, "pm":Landroid/content/pm/PackageManager;
    new-instance v2, Landroid/content/ComponentName;

    const-class v3, Lcom/android/managedprovisioning/HomeReceiverActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v2, v4, v4}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 380
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.android.managedprovisioning.provisioning_success"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 381
    .local v1, "successIntent":Landroid/content/Intent;
    const-class v2, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity$ServiceMessageReceiver;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 382
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 384
    return-void
.end method

.method private progressUpdate(I)V
    .locals 2
    .param p1, "progressMessage"    # I

    .prologue
    .line 355
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Reporting progress update: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 357
    iput p1, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mLastProgressMessage:I

    .line 358
    invoke-direct {p0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->sendProgressUpdateToActivity()V

    .line 359
    return-void
.end method

.method private registerHomeIntentReceiver()V
    .locals 3

    .prologue
    .line 157
    new-instance v1, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$IndirectHomeReceiver;

    invoke-direct {v1, p0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$IndirectHomeReceiver;-><init>(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;)V

    iput-object v1, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mIndirectHomeReceiver:Landroid/content/BroadcastReceiver;

    .line 158
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 159
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.android.managedprovisioning.home_indirect"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 160
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mIndirectHomeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 161
    return-void
.end method

.method private sendError()V
    .locals 4

    .prologue
    .line 347
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Reporting Error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget v3, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mLastErrorMessage:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 348
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.managedprovisioning.error"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 349
    .local v0, "intent":Landroid/content/Intent;
    const-class v1, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity$ServiceMessageReceiver;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 350
    const-string v1, "UserVisibleErrorMessage-Id"

    iget v2, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mLastErrorMessage:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 351
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 352
    return-void
.end method

.method private sendProgressUpdateToActivity()V
    .locals 3

    .prologue
    .line 362
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.managedprovisioning.progress_update"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 363
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "ProgressMessageId"

    iget v2, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mLastProgressMessage:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 364
    const-class v1, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity$ServiceMessageReceiver;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 365
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 366
    return-void
.end method

.method private setLocale(Ljava/util/Locale;)V
    .locals 3
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    .line 415
    if-eqz p1, :cond_0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 427
    :cond_0
    :goto_0
    return-void

    .line 419
    :cond_1
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Setting locale to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 422
    invoke-static {p1}, Lcom/android/internal/app/LocalePicker;->updateLocale(Ljava/util/Locale;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 423
    :catch_0
    move-exception v0

    .line 424
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "Failed to set the system locale."

    invoke-static {v1}, Lcom/android/managedprovisioning/ProvisionLogger;->loge(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setTimeAndTimezone(Ljava/lang/String;J)V
    .locals 4
    .param p1, "timeZone"    # Ljava/lang/String;
    .param p2, "localTime"    # J

    .prologue
    .line 399
    :try_start_0
    const-string v2, "alarm"

    invoke-virtual {p0, v2}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 400
    .local v0, "am":Landroid/app/AlarmManager;
    if-eqz p1, :cond_0

    .line 401
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Setting time zone to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 402
    invoke-virtual {v0, p1}, Landroid/app/AlarmManager;->setTimeZone(Ljava/lang/String;)V

    .line 404
    :cond_0
    const-wide/16 v2, 0x0

    cmp-long v2, p2, v2

    if-lez v2, :cond_1

    .line 405
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Setting time to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 406
    invoke-virtual {v0, p2, p3}, Landroid/app/AlarmManager;->setTime(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 412
    .end local v0    # "am":Landroid/app/AlarmManager;
    :cond_1
    :goto_0
    return-void

    .line 408
    :catch_0
    move-exception v1

    .line 409
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "Alarm manager failed to set the system time/timezone."

    invoke-static {v2}, Lcom/android/managedprovisioning/ProvisionLogger;->loge(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private startDeviceOwnerProvisioning(Lcom/android/managedprovisioning/ProvisioningParams;)V
    .locals 12
    .param p1, "params"    # Lcom/android/managedprovisioning/ProvisioningParams;

    .prologue
    const/4 v11, 0x0

    .line 195
    const-string v0, "Starting device owner provisioning"

    invoke-static {v0}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 198
    new-instance v0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;

    iget-object v2, p1, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiSsid:Ljava/lang/String;

    iget-boolean v3, p1, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiHidden:Z

    iget-object v4, p1, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiSecurityType:Ljava/lang/String;

    iget-object v5, p1, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiPassword:Ljava/lang/String;

    iget-object v6, p1, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiProxyHost:Ljava/lang/String;

    iget v7, p1, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiProxyPort:I

    iget-object v8, p1, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiProxyBypassHosts:Ljava/lang/String;

    iget-object v9, p1, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiPacUrl:Ljava/lang/String;

    new-instance v10, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$2;

    invoke-direct {v10, p0, p1}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$2;-><init>(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;Lcom/android/managedprovisioning/ProvisioningParams;)V

    move-object v1, p0

    invoke-direct/range {v0 .. v10}, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;-><init>(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/android/managedprovisioning/task/AddWifiNetworkTask$Callback;)V

    iput-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mAddWifiNetworkTask:Lcom/android/managedprovisioning/task/AddWifiNetworkTask;

    .line 222
    new-instance v0, Lcom/android/managedprovisioning/task/DownloadPackageTask;

    iget-object v2, p1, Lcom/android/managedprovisioning/ProvisioningParams;->mDeviceAdminPackageDownloadLocation:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/managedprovisioning/ProvisioningParams;->mDeviceAdminPackageChecksum:[B

    iget-object v4, p1, Lcom/android/managedprovisioning/ProvisioningParams;->mDeviceAdminPackageDownloadCookieHeader:Ljava/lang/String;

    new-instance v5, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$3;

    invoke-direct {v5, p0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$3;-><init>(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;)V

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/managedprovisioning/task/DownloadPackageTask;-><init>(Landroid/content/Context;Ljava/lang/String;[BLjava/lang/String;Lcom/android/managedprovisioning/task/DownloadPackageTask$Callback;)V

    iput-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mDownloadPackageTask:Lcom/android/managedprovisioning/task/DownloadPackageTask;

    .line 249
    new-instance v0, Lcom/android/managedprovisioning/task/InstallPackageTask;

    iget-object v1, p1, Lcom/android/managedprovisioning/ProvisioningParams;->mDeviceAdminPackageName:Ljava/lang/String;

    new-instance v2, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$4;

    invoke-direct {v2, p0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$4;-><init>(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;)V

    invoke-direct {v0, p0, v1, v2}, Lcom/android/managedprovisioning/task/InstallPackageTask;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/android/managedprovisioning/task/InstallPackageTask$Callback;)V

    iput-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mInstallPackageTask:Lcom/android/managedprovisioning/task/InstallPackageTask;

    .line 274
    new-instance v0, Lcom/android/managedprovisioning/task/SetDevicePolicyTask;

    iget-object v1, p1, Lcom/android/managedprovisioning/ProvisioningParams;->mDeviceAdminPackageName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07001d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$5;

    invoke-direct {v3, p0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$5;-><init>(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;)V

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/android/managedprovisioning/task/SetDevicePolicyTask;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/android/managedprovisioning/task/SetDevicePolicyTask$Callback;)V

    iput-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mSetDevicePolicyTask:Lcom/android/managedprovisioning/task/SetDevicePolicyTask;

    .line 299
    new-instance v0, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;

    iget-object v2, p1, Lcom/android/managedprovisioning/ProvisioningParams;->mDeviceAdminPackageName:Ljava/lang/String;

    const/high16 v4, 0x7f060000

    const v5, 0x7f060002

    const/4 v6, 0x1

    new-instance v8, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$6;

    invoke-direct {v8, p0, p1}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$6;-><init>(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;Lcom/android/managedprovisioning/ProvisioningParams;)V

    move-object v1, p0

    move v3, v11

    move v7, v11

    invoke-direct/range {v0 .. v8}, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;-><init>(Landroid/content/Context;Ljava/lang/String;IIIZZLcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask$Callback;)V

    iput-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mDeleteNonRequiredAppsTask:Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;

    .line 317
    invoke-direct {p0, p1}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->startFirstTask(Lcom/android/managedprovisioning/ProvisioningParams;)V

    .line 318
    return-void
.end method

.method private startFirstTask(Lcom/android/managedprovisioning/ProvisioningParams;)V
    .locals 1
    .param p1, "params"    # Lcom/android/managedprovisioning/ProvisioningParams;

    .prologue
    .line 321
    iget-object v0, p1, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiSsid:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 324
    const v0, 0x7f070020

    invoke-direct {p0, v0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->progressUpdate(I)V

    .line 325
    iget-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mAddWifiNetworkTask:Lcom/android/managedprovisioning/task/AddWifiNetworkTask;

    invoke-virtual {v0}, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->run()V

    .line 338
    :goto_0
    return-void

    .line 326
    :cond_0
    iget-object v0, p1, Lcom/android/managedprovisioning/ProvisioningParams;->mDeviceAdminPackageDownloadLocation:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 329
    const v0, 0x7f070021

    invoke-direct {p0, v0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->progressUpdate(I)V

    .line 330
    iget-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mDownloadPackageTask:Lcom/android/managedprovisioning/task/DownloadPackageTask;

    invoke-virtual {v0}, Lcom/android/managedprovisioning/task/DownloadPackageTask;->run()V

    goto :goto_0

    .line 335
    :cond_1
    const v0, 0x7f070023

    invoke-direct {p0, v0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->progressUpdate(I)V

    .line 336
    iget-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mSetDevicePolicyTask:Lcom/android/managedprovisioning/task/SetDevicePolicyTask;

    invoke-virtual {v0}, Lcom/android/managedprovisioning/task/SetDevicePolicyTask;->run()V

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 452
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 431
    const-string v0, "Device owner provisioning service ONCREATE."

    invoke-static {v0}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 432
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 436
    const-string v0, "Device owner provisioning service ONDESTROY"

    invoke-static {v0}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 437
    iget-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mAddWifiNetworkTask:Lcom/android/managedprovisioning/task/AddWifiNetworkTask;

    if-eqz v0, :cond_0

    .line 438
    iget-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mAddWifiNetworkTask:Lcom/android/managedprovisioning/task/AddWifiNetworkTask;

    invoke-virtual {v0}, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->cleanUp()V

    .line 440
    :cond_0
    iget-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mDownloadPackageTask:Lcom/android/managedprovisioning/task/DownloadPackageTask;

    if-eqz v0, :cond_1

    .line 441
    iget-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mDownloadPackageTask:Lcom/android/managedprovisioning/task/DownloadPackageTask;

    invoke-virtual {v0}, Lcom/android/managedprovisioning/task/DownloadPackageTask;->cleanUp()V

    .line 443
    :cond_1
    iget-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mIndirectHomeReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_2

    .line 444
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mIndirectHomeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 445
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mIndirectHomeReceiver:Landroid/content/BroadcastReceiver;

    .line 448
    :cond_2
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 113
    const-string v0, "Device owner provisioning service ONSTARTCOMMAND."

    invoke-static {v0}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 115
    monitor-enter p0

    .line 116
    :try_start_0
    iget-boolean v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mProvisioningInFlight:Z

    if-eqz v0, :cond_2

    .line 117
    const-string v0, "Provisioning already in flight."

    invoke-static {v0}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 119
    invoke-direct {p0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->sendProgressUpdateToActivity()V

    .line 122
    iget v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mLastErrorMessage:I

    if-ltz v0, :cond_0

    .line 123
    invoke-direct {p0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->sendError()V

    .line 127
    :cond_0
    iget-boolean v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mDone:Z

    if-eqz v0, :cond_1

    .line 128
    iget-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mParams:Lcom/android/managedprovisioning/ProvisioningParams;

    iget-object v0, v0, Lcom/android/managedprovisioning/ProvisioningParams;->mDeviceAdminPackageName:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->onProvisioningSuccess(Ljava/lang/String;)V

    .line 148
    :cond_1
    :goto_0
    monitor-exit p0

    .line 149
    const/4 v0, 0x2

    return v0

    .line 131
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mProvisioningInFlight:Z

    .line 132
    const-string v0, "First start of the service."

    invoke-static {v0}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 133
    const v0, 0x7f07001f

    invoke-direct {p0, v0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->progressUpdate(I)V

    .line 136
    const-string v0, "ProvisioningParams"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/android/managedprovisioning/ProvisioningParams;

    iput-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mParams:Lcom/android/managedprovisioning/ProvisioningParams;

    .line 138
    invoke-direct {p0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->registerHomeIntentReceiver()V

    .line 141
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$1;

    invoke-direct {v1, p0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$1;-><init>(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 148
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
