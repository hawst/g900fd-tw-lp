.class final Lcom/android/managedprovisioning/ProvisioningParams$1;
.super Ljava/lang/Object;
.source "ProvisioningParams.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/managedprovisioning/ProvisioningParams;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/android/managedprovisioning/ProvisioningParams;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/android/managedprovisioning/ProvisioningParams;
    .locals 8
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 100
    new-instance v1, Lcom/android/managedprovisioning/ProvisioningParams;

    invoke-direct {v1}, Lcom/android/managedprovisioning/ProvisioningParams;-><init>()V

    .line 101
    .local v1, "params":Lcom/android/managedprovisioning/ProvisioningParams;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/android/managedprovisioning/ProvisioningParams;->mTimeZone:Ljava/lang/String;

    .line 102
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v6

    iput-wide v6, v1, Lcom/android/managedprovisioning/ProvisioningParams;->mLocalTime:J

    .line 103
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Ljava/util/Locale;

    iput-object v2, v1, Lcom/android/managedprovisioning/ProvisioningParams;->mLocale:Ljava/util/Locale;

    .line 104
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiSsid:Ljava/lang/String;

    .line 105
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-ne v2, v3, :cond_0

    move v2, v3

    :goto_0
    iput-boolean v2, v1, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiHidden:Z

    .line 106
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiSecurityType:Ljava/lang/String;

    .line 107
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiPassword:Ljava/lang/String;

    .line 108
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiProxyHost:Ljava/lang/String;

    .line 109
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, v1, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiProxyPort:I

    .line 110
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiProxyBypassHosts:Ljava/lang/String;

    .line 111
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/android/managedprovisioning/ProvisioningParams;->mDeviceAdminPackageName:Ljava/lang/String;

    .line 112
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/android/managedprovisioning/ProvisioningParams;->mDeviceAdminPackageDownloadLocation:Ljava/lang/String;

    .line 113
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/android/managedprovisioning/ProvisioningParams;->mDeviceAdminPackageDownloadCookieHeader:Ljava/lang/String;

    .line 114
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 115
    .local v0, "checksumLength":I
    new-array v2, v0, [B

    iput-object v2, v1, Lcom/android/managedprovisioning/ProvisioningParams;->mDeviceAdminPackageChecksum:[B

    .line 116
    iget-object v2, v1, Lcom/android/managedprovisioning/ProvisioningParams;->mDeviceAdminPackageChecksum:[B

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readByteArray([B)V

    .line 117
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/os/PersistableBundle;

    iput-object v2, v1, Lcom/android/managedprovisioning/ProvisioningParams;->mAdminExtrasBundle:Landroid/os/PersistableBundle;

    .line 118
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-ne v2, v3, :cond_1

    :goto_1
    iput-boolean v3, v1, Lcom/android/managedprovisioning/ProvisioningParams;->mStartedByNfc:Z

    .line 119
    return-object v1

    .end local v0    # "checksumLength":I
    :cond_0
    move v2, v4

    .line 105
    goto :goto_0

    .restart local v0    # "checksumLength":I
    :cond_1
    move v3, v4

    .line 118
    goto :goto_1
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 97
    invoke-virtual {p0, p1}, Lcom/android/managedprovisioning/ProvisioningParams$1;->createFromParcel(Landroid/os/Parcel;)Lcom/android/managedprovisioning/ProvisioningParams;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/android/managedprovisioning/ProvisioningParams;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 124
    new-array v0, p1, [Lcom/android/managedprovisioning/ProvisioningParams;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 97
    invoke-virtual {p0, p1}, Lcom/android/managedprovisioning/ProvisioningParams$1;->newArray(I)[Lcom/android/managedprovisioning/ProvisioningParams;

    move-result-object v0

    return-object v0
.end method
