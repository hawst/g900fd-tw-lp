.class public Lcom/android/managedprovisioning/ManagedProvisioningActivity;
.super Landroid/app/Activity;
.source "ManagedProvisioningActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/managedprovisioning/ManagedProvisioningActivity$ManagedProvisioningFailedException;,
        Lcom/android/managedprovisioning/ManagedProvisioningActivity$ServiceMessageReceiver;
    }
.end annotation


# static fields
.field protected static final ALIAS_CHECK_CALLER:Landroid/content/ComponentName;

.field protected static final ALIAS_NO_CHECK_CALLER:Landroid/content/ComponentName;


# instance fields
.field private mContentView:Landroid/view/View;

.field private mMainTextView:Landroid/view/View;

.field private mMdmPackageName:Ljava/lang/String;

.field private mProgressView:Landroid/view/View;

.field private mServiceMessageReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 77
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.android.managedprovisioning"

    const-string v2, "com.android.managedprovisioning.ManagedProvisioningActivity"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->ALIAS_CHECK_CALLER:Landroid/content/ComponentName;

    .line 81
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.android.managedprovisioning"

    const-string v2, "com.android.managedprovisioning.ManagedProvisioningActivityNoCallerCheck"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->ALIAS_NO_CHECK_CALLER:Landroid/content/ComponentName;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 470
    return-void
.end method

.method static synthetic access$000(Lcom/android/managedprovisioning/ManagedProvisioningActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/managedprovisioning/ManagedProvisioningActivity;

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->checkEncryptedAndStartProvisioningService()V

    return-void
.end method

.method static synthetic access$100(Lcom/android/managedprovisioning/ManagedProvisioningActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/managedprovisioning/ManagedProvisioningActivity;

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->setupEnvironmentAndProvision()V

    return-void
.end method

.method static synthetic access$200(Lcom/android/managedprovisioning/ManagedProvisioningActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/managedprovisioning/ManagedProvisioningActivity;

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->pickLauncher()V

    return-void
.end method

.method static synthetic access$300(Lcom/android/managedprovisioning/ManagedProvisioningActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/managedprovisioning/ManagedProvisioningActivity;

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->showStartProvisioningScreen()V

    return-void
.end method

.method static synthetic access$400(Lcom/android/managedprovisioning/ManagedProvisioningActivity;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/android/managedprovisioning/ManagedProvisioningActivity;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Landroid/content/DialogInterface$OnClickListener;

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->buildDeleteManagedProfileDialog(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method private buildDeleteManagedProfileDialog(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog;
    .locals 4
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "deleteListener"    # Landroid/content/DialogInterface$OnClickListener;

    .prologue
    .line 448
    new-instance v1, Lcom/android/managedprovisioning/ManagedProvisioningActivity$6;

    invoke-direct {v1, p0}, Lcom/android/managedprovisioning/ManagedProvisioningActivity$6;-><init>(Lcom/android/managedprovisioning/ManagedProvisioningActivity;)V

    .line 456
    .local v1, "cancelListener":Landroid/content/DialogInterface$OnClickListener;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 457
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f070010

    invoke-virtual {p0, v3}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, p2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f070011

    invoke-virtual {p0, v3}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 462
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    return-object v2
.end method

.method private checkEncryptedAndStartProvisioningService()V
    .locals 6

    .prologue
    .line 302
    invoke-static {}, Lcom/android/managedprovisioning/EncryptDeviceActivity;->isDeviceEncrypted()Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "persist.sys.no_req_encrypt"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 307
    :cond_0
    new-instance v2, Lcom/android/managedprovisioning/UserConsentDialog;

    const/4 v3, 0x1

    new-instance v4, Lcom/android/managedprovisioning/ManagedProvisioningActivity$2;

    invoke-direct {v4, p0}, Lcom/android/managedprovisioning/ManagedProvisioningActivity$2;-><init>(Lcom/android/managedprovisioning/ManagedProvisioningActivity;)V

    const/4 v5, 0x0

    invoke-direct {v2, p0, v3, v4, v5}, Lcom/android/managedprovisioning/UserConsentDialog;-><init>(Landroid/content/Context;ILjava/lang/Runnable;Ljava/lang/Runnable;)V

    invoke-virtual {p0}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "UserConsentDialogFragment"

    invoke-virtual {v2, v3, v4}, Lcom/android/managedprovisioning/UserConsentDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 322
    :goto_0
    return-void

    .line 315
    :cond_1
    invoke-virtual {p0}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 316
    .local v1, "resumeExtras":Landroid/os/Bundle;
    const-string v2, "com.android.managedprovisioning.RESUME_TARGET"

    const-string v3, "profile_owner"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/android/managedprovisioning/EncryptDeviceActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v3, "com.android.managedprovisioning.RESUME"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    .line 319
    .local v0, "encryptIntent":Landroid/content/Intent;
    const/4 v2, 0x2

    invoke-virtual {p0, v0, v2}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method private currentLauncherSupportsManagedProfiles()Z
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 192
    new-instance v1, Landroid/content/Intent;

    const-string v6, "android.intent.action.MAIN"

    invoke-direct {v1, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 193
    .local v1, "intent":Landroid/content/Intent;
    const-string v6, "android.intent.category.HOME"

    invoke-virtual {v1, v6}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 195
    invoke-virtual {p0}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 196
    .local v4, "pm":Landroid/content/pm/PackageManager;
    const/high16 v6, 0x10000

    invoke-virtual {v4, v1, v6}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v3

    .line 199
    .local v3, "launcherResolveInfo":Landroid/content/pm/ResolveInfo;
    :try_start_0
    invoke-virtual {p0}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    iget-object v7, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v7, v7, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    .line 201
    .local v2, "launcherAppInfo":Landroid/content/pm/ApplicationInfo;
    iget v6, v2, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    invoke-direct {p0, v6}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->versionNumberAtLeastL(I)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    .line 203
    .end local v2    # "launcherAppInfo":Landroid/content/pm/ApplicationInfo;
    :goto_0
    return v5

    .line 202
    :catch_0
    move-exception v0

    .line 203
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    goto :goto_0
.end method

.method private initialize(Landroid/content/Intent;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/managedprovisioning/ManagedProvisioningActivity$ManagedProvisioningFailedException;
        }
    .end annotation

    .prologue
    .line 263
    :try_start_0
    invoke-virtual {p0}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "android.app.extra.PROVISIONING_ADMIN_EXTRAS_BUNDLE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/os/PersistableBundle;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 272
    const-string v1, "android.app.extra.PROVISIONING_DEVICE_ADMIN_PACKAGE_NAME"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->mMdmPackageName:Ljava/lang/String;

    .line 273
    iget-object v1, p0, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->mMdmPackageName:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 274
    new-instance v1, Lcom/android/managedprovisioning/ManagedProvisioningActivity$ManagedProvisioningFailedException;

    const-string v2, "Missing intent extra: android.app.extra.PROVISIONING_DEVICE_ADMIN_PACKAGE_NAME"

    invoke-direct {v1, p0, v2}, Lcom/android/managedprovisioning/ManagedProvisioningActivity$ManagedProvisioningFailedException;-><init>(Lcom/android/managedprovisioning/ManagedProvisioningActivity;Ljava/lang/String;)V

    throw v1

    .line 265
    :catch_0
    move-exception v0

    .line 266
    .local v0, "e":Ljava/lang/ClassCastException;
    new-instance v1, Lcom/android/managedprovisioning/ManagedProvisioningActivity$ManagedProvisioningFailedException;

    const-string v2, "Extra android.app.extra.PROVISIONING_ADMIN_EXTRAS_BUNDLE must be of type PersistableBundle."

    invoke-direct {v1, p0, v2, v0}, Lcom/android/managedprovisioning/ManagedProvisioningActivity$ManagedProvisioningFailedException;-><init>(Lcom/android/managedprovisioning/ManagedProvisioningActivity;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 278
    .end local v0    # "e":Ljava/lang/ClassCastException;
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->mMdmPackageName:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 284
    return-void

    .line 279
    :catch_1
    move-exception v0

    .line 280
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    new-instance v1, Lcom/android/managedprovisioning/ManagedProvisioningActivity$ManagedProvisioningFailedException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Mdm "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->mMdmPackageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is not installed. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p0, v2, v0}, Lcom/android/managedprovisioning/ManagedProvisioningActivity$ManagedProvisioningFailedException;-><init>(Lcom/android/managedprovisioning/ManagedProvisioningActivity;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private packageHasManageUsersPermission(Ljava/lang/String;)Z
    .locals 3
    .param p1, "pkg"    # Ljava/lang/String;

    .prologue
    .line 177
    invoke-virtual {p0}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "android.permission.MANAGE_USERS"

    invoke-virtual {v1, v2, p1}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 179
    .local v0, "packagePermission":I
    if-nez v0, :cond_0

    .line 180
    const/4 v1, 0x1

    .line 182
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private pickLauncher()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 340
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.HOME_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 341
    .local v0, "changeLauncherIntent":Landroid/content/Intent;
    const-string v1, "support_managed_profiles"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 342
    invoke-virtual {p0, v0, v2}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 344
    return-void
.end method

.method private setMdmIcon(Ljava/lang/String;Landroid/view/View;)V
    .locals 8
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "contentView"    # Landroid/view/View;

    .prologue
    .line 232
    if-eqz p1, :cond_0

    .line 233
    invoke-virtual {p0}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    .line 235
    .local v6, "pm":Landroid/content/pm/PackageManager;
    const/4 v7, 0x0

    :try_start_0
    invoke-virtual {v6, p1, v7}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 236
    .local v0, "ai":Landroid/content/pm/ApplicationInfo;
    if-eqz v0, :cond_0

    .line 237
    invoke-virtual {v6, p1}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 238
    .local v5, "packageIcon":Landroid/graphics/drawable/Drawable;
    const v7, 0x7f09000c

    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 239
    .local v4, "imageView":Landroid/widget/ImageView;
    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 241
    invoke-virtual {v6, v0}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 242
    .local v1, "appLabel":Ljava/lang/String;
    const v7, 0x7f09000d

    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 244
    .local v2, "deviceManagerName":Landroid/widget/TextView;
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 251
    .end local v0    # "ai":Landroid/content/pm/ApplicationInfo;
    .end local v1    # "appLabel":Ljava/lang/String;
    .end local v2    # "deviceManagerName":Landroid/widget/TextView;
    .end local v4    # "imageView":Landroid/widget/ImageView;
    .end local v5    # "packageIcon":Landroid/graphics/drawable/Drawable;
    .end local v6    # "pm":Landroid/content/pm/PackageManager;
    :cond_0
    :goto_0
    return-void

    .line 246
    .restart local v6    # "pm":Landroid/content/pm/PackageManager;
    :catch_0
    move-exception v3

    .line 248
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v7, "Package does not exist. Should never happen."

    invoke-static {v7}, Lcom/android/managedprovisioning/ProvisionLogger;->loge(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setupEnvironmentAndProvision()V
    .locals 2

    .prologue
    .line 326
    invoke-static {p0}, Lcom/android/managedprovisioning/BootReminder;->cancelProvisioningReminder(Landroid/content/Context;)V

    .line 328
    iget-object v0, p0, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->mProgressView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 329
    iget-object v0, p0, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->mMainTextView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 332
    invoke-direct {p0}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->currentLauncherSupportsManagedProfiles()Z

    move-result v0

    if-nez v0, :cond_0

    .line 333
    invoke-direct {p0}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->showCurrentLauncherInvalid()V

    .line 337
    :goto_0
    return-void

    .line 335
    :cond_0
    invoke-direct {p0}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->startManagedProvisioningService()V

    goto :goto_0
.end method

.method private showCurrentLauncherInvalid()V
    .locals 3

    .prologue
    .line 370
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f07001a

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f07001b

    new-instance v2, Lcom/android/managedprovisioning/ManagedProvisioningActivity$4;

    invoke-direct {v2, p0}, Lcom/android/managedprovisioning/ManagedProvisioningActivity$4;-><init>(Lcom/android/managedprovisioning/ManagedProvisioningActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f07001c

    new-instance v2, Lcom/android/managedprovisioning/ManagedProvisioningActivity$3;

    invoke-direct {v2, p0}, Lcom/android/managedprovisioning/ManagedProvisioningActivity$3;-><init>(Lcom/android/managedprovisioning/ManagedProvisioningActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 389
    return-void
.end method

.method private showManagedProfileExistsDialog(I)V
    .locals 2
    .param p1, "existingManagedProfileUserId"    # I

    .prologue
    .line 420
    new-instance v0, Lcom/android/managedprovisioning/ManagedProvisioningActivity$5;

    invoke-direct {v0, p0, p1}, Lcom/android/managedprovisioning/ManagedProvisioningActivity$5;-><init>(Lcom/android/managedprovisioning/ManagedProvisioningActivity;I)V

    .line 441
    .local v0, "warningListener":Landroid/content/DialogInterface$OnClickListener;
    const v1, 0x7f07000e

    invoke-virtual {p0, v1}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->buildDeleteManagedProfileDialog(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 444
    return-void
.end method

.method private showStartProvisioningScreen()V
    .locals 3

    .prologue
    .line 167
    iget-object v1, p0, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->mContentView:Landroid/view/View;

    const v2, 0x7f090007

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 168
    .local v0, "positiveButton":Landroid/widget/Button;
    new-instance v1, Lcom/android/managedprovisioning/ManagedProvisioningActivity$1;

    invoke-direct {v1, p0}, Lcom/android/managedprovisioning/ManagedProvisioningActivity$1;-><init>(Lcom/android/managedprovisioning/ManagedProvisioningActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 174
    return-void
.end method

.method private startManagedProvisioningService()V
    .locals 2

    .prologue
    .line 347
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/managedprovisioning/ManagedProvisioningService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 348
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    .line 349
    invoke-virtual {p0, v0}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 350
    return-void
.end method

.method private systemHasManagedProfileFeature()Z
    .locals 2

    .prologue
    .line 187
    invoke-virtual {p0}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 188
    .local v0, "pm":Landroid/content/pm/PackageManager;
    const-string v1, "android.software.managed_users"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method private versionNumberAtLeastL(I)Z
    .locals 1
    .param p1, "versionNumber"    # I

    .prologue
    .line 208
    const/16 v0, 0x15

    if-lt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method alreadyHasManagedProfile()I
    .locals 5

    .prologue
    .line 402
    const-string v4, "user"

    invoke-virtual {p0, v4}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/UserManager;

    .line 403
    .local v3, "userManager":Landroid/os/UserManager;
    invoke-virtual {p0}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->getUserId()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/os/UserManager;->getProfiles(I)Ljava/util/List;

    move-result-object v1

    .line 404
    .local v1, "profiles":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/UserInfo;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/UserInfo;

    .line 405
    .local v2, "userInfo":Landroid/content/pm/UserInfo;
    invoke-virtual {v2}, Landroid/content/pm/UserInfo;->isManagedProfile()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 406
    invoke-virtual {v2}, Landroid/content/pm/UserInfo;->getUserHandle()Landroid/os/UserHandle;

    move-result-object v4

    invoke-virtual {v4}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v4

    .line 409
    .end local v2    # "userInfo":Landroid/content/pm/UserInfo;
    :goto_0
    return v4

    :cond_1
    const/4 v4, -0x1

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 354
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 355
    if-nez p2, :cond_0

    .line 356
    const-string v0, "User canceled device encryption."

    invoke-static {v0}, Lcom/android/managedprovisioning/ProvisionLogger;->loge(Ljava/lang/String;)V

    .line 357
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->setResult(I)V

    .line 358
    invoke-virtual {p0}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->finish()V

    .line 367
    :cond_0
    :goto_0
    return-void

    .line 360
    :cond_1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 361
    if-nez p2, :cond_2

    .line 362
    invoke-direct {p0}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->showCurrentLauncherInvalid()V

    goto :goto_0

    .line 363
    :cond_2
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 364
    invoke-direct {p0}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->startManagedProvisioningService()V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 289
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v8, 0x7f070017

    .line 97
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 99
    const-string v6, "Managed provisioning activity ONCREATE"

    invoke-static {v6}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 101
    invoke-virtual {p0}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v5

    .line 102
    .local v5, "inflater":Landroid/view/LayoutInflater;
    const v6, 0x7f030005

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->mContentView:Landroid/view/View;

    .line 103
    iget-object v6, p0, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->mContentView:Landroid/view/View;

    const v7, 0x7f09000b

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->mMainTextView:Landroid/view/View;

    .line 104
    iget-object v6, p0, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->mContentView:Landroid/view/View;

    const v7, 0x7f09000a

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->mProgressView:Landroid/view/View;

    .line 105
    iget-object v6, p0, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->mContentView:Landroid/view/View;

    invoke-virtual {p0, v6}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->setContentView(Landroid/view/View;)V

    .line 108
    invoke-direct {p0}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->systemHasManagedProfileFeature()Z

    move-result v6

    if-nez v6, :cond_0

    .line 109
    const v6, 0x7f070018

    const-string v7, "Exiting managed provisioning, managed profiles feature is not available"

    invoke-virtual {p0, v6, v7}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->showErrorAndClose(ILjava/lang/String;)V

    .line 164
    :goto_0
    return-void

    .line 113
    :cond_0
    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    move-result-object v6

    invoke-virtual {v6}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v6

    if-eqz v6, :cond_1

    .line 114
    const v6, 0x7f070019

    const-string v7, "Exiting managed provisioning, calling user is not owner."

    invoke-virtual {p0, v6, v7}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->showErrorAndClose(ILjava/lang/String;)V

    goto :goto_0

    .line 120
    :cond_1
    new-instance v6, Lcom/android/managedprovisioning/ManagedProvisioningActivity$ServiceMessageReceiver;

    invoke-direct {v6, p0}, Lcom/android/managedprovisioning/ManagedProvisioningActivity$ServiceMessageReceiver;-><init>(Lcom/android/managedprovisioning/ManagedProvisioningActivity;)V

    iput-object v6, p0, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->mServiceMessageReceiver:Landroid/content/BroadcastReceiver;

    .line 121
    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    .line 122
    .local v3, "filter":Landroid/content/IntentFilter;
    const-string v6, "com.android.managedprovisioning.provisioning_success"

    invoke-virtual {v3, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 123
    const-string v6, "com.android.managedprovisioning.error"

    invoke-virtual {v3, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 124
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v6

    iget-object v7, p0, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->mServiceMessageReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v6, v7, v3}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 128
    :try_start_0
    invoke-virtual {p0}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->initialize(Landroid/content/Intent;)V
    :try_end_0
    .catch Lcom/android/managedprovisioning/ManagedProvisioningActivity$ManagedProvisioningFailedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 134
    iget-object v6, p0, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->mMdmPackageName:Ljava/lang/String;

    iget-object v7, p0, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->mContentView:Landroid/view/View;

    invoke-direct {p0, v6, v7}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->setMdmIcon(Ljava/lang/String;Landroid/view/View;)V

    .line 138
    invoke-virtual {p0}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v6

    sget-object v7, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->ALIAS_NO_CHECK_CALLER:Landroid/content/ComponentName;

    invoke-virtual {v6, v7}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 139
    .local v4, "hasManageUsersPermission":Z
    if-nez v4, :cond_3

    .line 141
    invoke-virtual {p0}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->getCallingPackage()Ljava/lang/String;

    move-result-object v0

    .line 142
    .local v0, "callingPackage":Ljava/lang/String;
    if-nez v0, :cond_2

    .line 143
    const-string v6, "Calling package is null. Was startActivityForResult used to start this activity?"

    invoke-virtual {p0, v8, v6}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->showErrorAndClose(ILjava/lang/String;)V

    goto :goto_0

    .line 129
    .end local v0    # "callingPackage":Ljava/lang/String;
    .end local v4    # "hasManageUsersPermission":Z
    :catch_0
    move-exception v1

    .line 130
    .local v1, "e":Lcom/android/managedprovisioning/ManagedProvisioningActivity$ManagedProvisioningFailedException;
    invoke-virtual {v1}, Lcom/android/managedprovisioning/ManagedProvisioningActivity$ManagedProvisioningFailedException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v8, v6}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->showErrorAndClose(ILjava/lang/String;)V

    goto :goto_0

    .line 148
    .end local v1    # "e":Lcom/android/managedprovisioning/ManagedProvisioningActivity$ManagedProvisioningFailedException;
    .restart local v0    # "callingPackage":Ljava/lang/String;
    .restart local v4    # "hasManageUsersPermission":Z
    :cond_2
    iget-object v6, p0, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->mMdmPackageName:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    invoke-direct {p0, v0}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->packageHasManageUsersPermission(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 150
    const-string v6, "Permission denied, calling package tried to set a different package as profile owner. The system MANAGE_USERS permission is required."

    invoke-virtual {p0, v8, v6}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->showErrorAndClose(ILjava/lang/String;)V

    goto :goto_0

    .line 158
    .end local v0    # "callingPackage":Ljava/lang/String;
    :cond_3
    invoke-virtual {p0}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->alreadyHasManagedProfile()I

    move-result v2

    .line 159
    .local v2, "existingManagedProfileUserId":I
    const/4 v6, -0x1

    if-eq v2, v6, :cond_4

    .line 160
    invoke-direct {p0, v2}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->showManagedProfileExistsDialog(I)V

    goto :goto_0

    .line 162
    :cond_4
    invoke-direct {p0}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->showStartProvisioningScreen()V

    goto/16 :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 293
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->mServiceMessageReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 294
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 295
    return-void
.end method

.method public showErrorAndClose(ILjava/lang/String;)V
    .locals 3
    .param p1, "resourceId"    # I
    .param p2, "logText"    # Ljava/lang/String;

    .prologue
    .line 392
    invoke-static {p2}, Lcom/android/managedprovisioning/ProvisionLogger;->loge(Ljava/lang/String;)V

    .line 393
    new-instance v0, Lcom/android/managedprovisioning/ManagedProvisioningErrorDialog;

    invoke-virtual {p0, p1}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/managedprovisioning/ManagedProvisioningErrorDialog;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "ErrorDialogFragment"

    invoke-virtual {v0, v1, v2}, Lcom/android/managedprovisioning/ManagedProvisioningErrorDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 395
    return-void
.end method
