.class public Lcom/android/managedprovisioning/MessageParser;
.super Ljava/lang/Object;
.source "MessageParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/managedprovisioning/MessageParser$ParseException;
    }
.end annotation


# static fields
.field protected static final DEVICE_OWNER_BOOLEAN_EXTRAS:[Ljava/lang/String;

.field protected static final DEVICE_OWNER_INT_EXTRAS:[Ljava/lang/String;

.field protected static final DEVICE_OWNER_LONG_EXTRAS:[Ljava/lang/String;

.field protected static final DEVICE_OWNER_PERSISTABLE_BUNDLE_EXTRAS:[Ljava/lang/String;

.field protected static final DEVICE_OWNER_STRING_EXTRAS:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 98
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "android.app.extra.PROVISIONING_TIME_ZONE"

    aput-object v1, v0, v3

    const-string v1, "android.app.extra.PROVISIONING_LOCALE"

    aput-object v1, v0, v4

    const-string v1, "android.app.extra.PROVISIONING_WIFI_SSID"

    aput-object v1, v0, v5

    const/4 v1, 0x3

    const-string v2, "android.app.extra.PROVISIONING_WIFI_SECURITY_TYPE"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "android.app.extra.PROVISIONING_WIFI_PASSWORD"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "android.app.extra.PROVISIONING_WIFI_PROXY_HOST"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "android.app.extra.PROVISIONING_WIFI_PROXY_BYPASS"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "android.app.extra.PROVISIONING_WIFI_PAC_URL"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "android.app.extra.PROVISIONING_DEVICE_ADMIN_PACKAGE_NAME"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "android.app.extra.PROVISIONING_DEVICE_ADMIN_PACKAGE_DOWNLOAD_LOCATION"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "android.app.extra.PROVISIONING_DEVICE_ADMIN_PACKAGE_DOWNLOAD_COOKIE_HEADER"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "android.app.extra.PROVISIONING_DEVICE_ADMIN_PACKAGE_CHECKSUM"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/managedprovisioning/MessageParser;->DEVICE_OWNER_STRING_EXTRAS:[Ljava/lang/String;

    .line 113
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "android.app.extra.PROVISIONING_LOCAL_TIME"

    aput-object v1, v0, v3

    sput-object v0, Lcom/android/managedprovisioning/MessageParser;->DEVICE_OWNER_LONG_EXTRAS:[Ljava/lang/String;

    .line 117
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "android.app.extra.PROVISIONING_WIFI_PROXY_PORT"

    aput-object v1, v0, v3

    sput-object v0, Lcom/android/managedprovisioning/MessageParser;->DEVICE_OWNER_INT_EXTRAS:[Ljava/lang/String;

    .line 121
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "android.app.extra.PROVISIONING_WIFI_HIDDEN"

    aput-object v1, v0, v3

    const-string v1, "com.android.managedprovisioning.extra.started_by_nfc"

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/managedprovisioning/MessageParser;->DEVICE_OWNER_BOOLEAN_EXTRAS:[Ljava/lang/String;

    .line 126
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "android.app.extra.PROVISIONING_ADMIN_EXTRAS_BUNDLE"

    aput-object v1, v0, v3

    sput-object v0, Lcom/android/managedprovisioning/MessageParser;->DEVICE_OWNER_PERSISTABLE_BUNDLE_EXTRAS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 325
    return-void
.end method

.method private checkValidityOfProvisioningParams(Lcom/android/managedprovisioning/ProvisioningParams;)V
    .locals 3
    .param p1, "params"    # Lcom/android/managedprovisioning/ProvisioningParams;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/managedprovisioning/MessageParser$ParseException;
        }
    .end annotation

    .prologue
    const v2, 0x7f070029

    .line 305
    iget-object v0, p1, Lcom/android/managedprovisioning/ProvisioningParams;->mDeviceAdminPackageName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 306
    new-instance v0, Lcom/android/managedprovisioning/MessageParser$ParseException;

    const-string v1, "Must provide the name of the device admin package."

    invoke-direct {v0, v1, v2}, Lcom/android/managedprovisioning/MessageParser$ParseException;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 309
    :cond_0
    iget-object v0, p1, Lcom/android/managedprovisioning/ProvisioningParams;->mDeviceAdminPackageDownloadLocation:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 310
    iget-object v0, p1, Lcom/android/managedprovisioning/ProvisioningParams;->mDeviceAdminPackageChecksum:[B

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/android/managedprovisioning/ProvisioningParams;->mDeviceAdminPackageChecksum:[B

    array-length v0, v0

    if-nez v0, :cond_2

    .line 312
    :cond_1
    new-instance v0, Lcom/android/managedprovisioning/MessageParser$ParseException;

    const-string v1, "Checksum of installer file is required for downloading device admin file, but not provided."

    invoke-direct {v0, v1, v2}, Lcom/android/managedprovisioning/MessageParser$ParseException;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 317
    :cond_2
    return-void
.end method

.method private parseProperties(Ljava/lang/String;)Lcom/android/managedprovisioning/ProvisioningParams;
    .locals 7
    .param p1, "data"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/managedprovisioning/MessageParser$ParseException;
        }
    .end annotation

    .prologue
    const v6, 0x7f070029

    .line 196
    new-instance v1, Lcom/android/managedprovisioning/ProvisioningParams;

    invoke-direct {v1}, Lcom/android/managedprovisioning/ProvisioningParams;-><init>()V

    .line 198
    .local v1, "params":Lcom/android/managedprovisioning/ProvisioningParams;
    :try_start_0
    new-instance v2, Ljava/util/Properties;

    invoke-direct {v2}, Ljava/util/Properties;-><init>()V

    .line 199
    .local v2, "props":Ljava/util/Properties;
    new-instance v4, Ljava/io/StringReader;

    invoke-direct {v4, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/util/Properties;->load(Ljava/io/Reader;)V

    .line 202
    const-string v4, "android.app.extra.PROVISIONING_TIME_ZONE"

    invoke-virtual {v2, v4}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/android/managedprovisioning/ProvisioningParams;->mTimeZone:Ljava/lang/String;

    .line 204
    const-string v4, "android.app.extra.PROVISIONING_LOCALE"

    invoke-virtual {v2, v4}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .local v3, "s":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 205
    invoke-static {v3}, Lcom/android/managedprovisioning/MessageParser;->stringToLocale(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v4

    iput-object v4, v1, Lcom/android/managedprovisioning/ProvisioningParams;->mLocale:Ljava/util/Locale;

    .line 207
    :cond_0
    const-string v4, "android.app.extra.PROVISIONING_WIFI_SSID"

    invoke-virtual {v2, v4}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiSsid:Ljava/lang/String;

    .line 208
    const-string v4, "android.app.extra.PROVISIONING_WIFI_SECURITY_TYPE"

    invoke-virtual {v2, v4}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiSecurityType:Ljava/lang/String;

    .line 209
    const-string v4, "android.app.extra.PROVISIONING_WIFI_PASSWORD"

    invoke-virtual {v2, v4}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiPassword:Ljava/lang/String;

    .line 210
    const-string v4, "android.app.extra.PROVISIONING_WIFI_PROXY_HOST"

    invoke-virtual {v2, v4}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiProxyHost:Ljava/lang/String;

    .line 211
    const-string v4, "android.app.extra.PROVISIONING_WIFI_PROXY_BYPASS"

    invoke-virtual {v2, v4}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiProxyBypassHosts:Ljava/lang/String;

    .line 212
    const-string v4, "android.app.extra.PROVISIONING_WIFI_PAC_URL"

    invoke-virtual {v2, v4}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiPacUrl:Ljava/lang/String;

    .line 213
    const-string v4, "android.app.extra.PROVISIONING_DEVICE_ADMIN_PACKAGE_NAME"

    invoke-virtual {v2, v4}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/android/managedprovisioning/ProvisioningParams;->mDeviceAdminPackageName:Ljava/lang/String;

    .line 215
    const-string v4, "android.app.extra.PROVISIONING_DEVICE_ADMIN_PACKAGE_DOWNLOAD_LOCATION"

    invoke-virtual {v2, v4}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/android/managedprovisioning/ProvisioningParams;->mDeviceAdminPackageDownloadLocation:Ljava/lang/String;

    .line 217
    const-string v4, "android.app.extra.PROVISIONING_DEVICE_ADMIN_PACKAGE_DOWNLOAD_COOKIE_HEADER"

    invoke-virtual {v2, v4}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/android/managedprovisioning/ProvisioningParams;->mDeviceAdminPackageDownloadCookieHeader:Ljava/lang/String;

    .line 219
    const-string v4, "android.app.extra.PROVISIONING_DEVICE_ADMIN_PACKAGE_CHECKSUM"

    invoke-virtual {v2, v4}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 220
    invoke-static {v3}, Lcom/android/managedprovisioning/MessageParser;->stringToByteArray(Ljava/lang/String;)[B

    move-result-object v4

    iput-object v4, v1, Lcom/android/managedprovisioning/ProvisioningParams;->mDeviceAdminPackageChecksum:[B

    .line 223
    :cond_1
    const-string v4, "android.app.extra.PROVISIONING_LOCAL_TIME"

    invoke-virtual {v2, v4}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 224
    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, v1, Lcom/android/managedprovisioning/ProvisioningParams;->mLocalTime:J

    .line 227
    :cond_2
    const-string v4, "android.app.extra.PROVISIONING_WIFI_PROXY_PORT"

    invoke-virtual {v2, v4}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 228
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    iput v4, v1, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiProxyPort:I

    .line 231
    :cond_3
    const-string v4, "android.app.extra.PROVISIONING_WIFI_HIDDEN"

    invoke-virtual {v2, v4}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 232
    invoke-static {v3}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, v1, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiHidden:Z

    .line 235
    :cond_4
    invoke-direct {p0, v1}, Lcom/android/managedprovisioning/MessageParser;->checkValidityOfProvisioningParams(Lcom/android/managedprovisioning/ProvisioningParams;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/IllformedLocaleException; {:try_start_0 .. :try_end_0} :catch_2

    .line 236
    return-object v1

    .line 237
    .end local v2    # "props":Ljava/util/Properties;
    .end local v3    # "s":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 238
    .local v0, "e":Ljava/io/IOException;
    new-instance v4, Lcom/android/managedprovisioning/MessageParser$ParseException;

    const-string v5, "Couldn\'t load payload"

    invoke-direct {v4, v5, v6, v0}, Lcom/android/managedprovisioning/MessageParser$ParseException;-><init>(Ljava/lang/String;ILjava/lang/Throwable;)V

    throw v4

    .line 240
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 241
    .local v0, "e":Ljava/lang/NumberFormatException;
    new-instance v4, Lcom/android/managedprovisioning/MessageParser$ParseException;

    const-string v5, "Incorrect numberformat."

    invoke-direct {v4, v5, v6, v0}, Lcom/android/managedprovisioning/MessageParser$ParseException;-><init>(Ljava/lang/String;ILjava/lang/Throwable;)V

    throw v4

    .line 243
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    :catch_2
    move-exception v0

    .line 244
    .local v0, "e":Ljava/util/IllformedLocaleException;
    new-instance v4, Lcom/android/managedprovisioning/MessageParser$ParseException;

    const-string v5, "Invalid locale."

    invoke-direct {v4, v5, v6, v0}, Lcom/android/managedprovisioning/MessageParser$ParseException;-><init>(Ljava/lang/String;ILjava/lang/Throwable;)V

    throw v4
.end method

.method public static stringToByteArray(Ljava/lang/String;)[B
    .locals 3
    .param p0, "s"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NumberFormatException;
        }
    .end annotation

    .prologue
    .line 346
    const/16 v1, 0x8

    :try_start_0
    invoke-static {p0, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 347
    :catch_0
    move-exception v0

    .line 348
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    new-instance v1, Ljava/lang/NumberFormatException;

    const-string v2, "Incorrect checksum format."

    invoke-direct {v1, v2}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static stringToLocale(Ljava/lang/String;)Ljava/util/Locale;
    .locals 3
    .param p0, "s"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/IllformedLocaleException;
        }
    .end annotation

    .prologue
    .line 354
    new-instance v0, Ljava/util/Locale$Builder;

    invoke-direct {v0}, Ljava/util/Locale$Builder;-><init>()V

    const-string v1, "_"

    const-string v2, "-"

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Locale$Builder;->setLanguageTag(Ljava/lang/String;)Ljava/util/Locale$Builder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale$Builder;->build()Ljava/util/Locale;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public addProvisioningParamsToBundle(Landroid/os/Bundle;Lcom/android/managedprovisioning/ProvisioningParams;)V
    .locals 4
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "params"    # Lcom/android/managedprovisioning/ProvisioningParams;

    .prologue
    .line 131
    const-string v0, "android.app.extra.PROVISIONING_TIME_ZONE"

    iget-object v1, p2, Lcom/android/managedprovisioning/ProvisioningParams;->mTimeZone:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    const-string v0, "android.app.extra.PROVISIONING_LOCALE"

    invoke-virtual {p2}, Lcom/android/managedprovisioning/ProvisioningParams;->getLocaleAsString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    const-string v0, "android.app.extra.PROVISIONING_WIFI_SSID"

    iget-object v1, p2, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiSsid:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    const-string v0, "android.app.extra.PROVISIONING_WIFI_SECURITY_TYPE"

    iget-object v1, p2, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiSecurityType:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    const-string v0, "android.app.extra.PROVISIONING_WIFI_PASSWORD"

    iget-object v1, p2, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiPassword:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    const-string v0, "android.app.extra.PROVISIONING_WIFI_PROXY_HOST"

    iget-object v1, p2, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiProxyHost:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    const-string v0, "android.app.extra.PROVISIONING_WIFI_PROXY_BYPASS"

    iget-object v1, p2, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiProxyBypassHosts:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    const-string v0, "android.app.extra.PROVISIONING_WIFI_PAC_URL"

    iget-object v1, p2, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiPacUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    const-string v0, "android.app.extra.PROVISIONING_DEVICE_ADMIN_PACKAGE_NAME"

    iget-object v1, p2, Lcom/android/managedprovisioning/ProvisioningParams;->mDeviceAdminPackageName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    const-string v0, "android.app.extra.PROVISIONING_DEVICE_ADMIN_PACKAGE_DOWNLOAD_LOCATION"

    iget-object v1, p2, Lcom/android/managedprovisioning/ProvisioningParams;->mDeviceAdminPackageDownloadLocation:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    const-string v0, "android.app.extra.PROVISIONING_DEVICE_ADMIN_PACKAGE_DOWNLOAD_COOKIE_HEADER"

    iget-object v1, p2, Lcom/android/managedprovisioning/ProvisioningParams;->mDeviceAdminPackageDownloadCookieHeader:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    const-string v0, "android.app.extra.PROVISIONING_DEVICE_ADMIN_PACKAGE_CHECKSUM"

    invoke-virtual {p2}, Lcom/android/managedprovisioning/ProvisioningParams;->getDeviceAdminPackageChecksumAsString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    const-string v0, "android.app.extra.PROVISIONING_LOCAL_TIME"

    iget-wide v2, p2, Lcom/android/managedprovisioning/ProvisioningParams;->mLocalTime:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 150
    const-string v0, "android.app.extra.PROVISIONING_WIFI_PROXY_PORT"

    iget v1, p2, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiProxyPort:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 152
    const-string v0, "android.app.extra.PROVISIONING_WIFI_HIDDEN"

    iget-boolean v1, p2, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiHidden:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 153
    const-string v0, "com.android.managedprovisioning.extra.started_by_nfc"

    iget-boolean v1, p2, Lcom/android/managedprovisioning/ProvisioningParams;->mStartedByNfc:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 155
    const-string v0, "android.app.extra.PROVISIONING_ADMIN_EXTRAS_BUNDLE"

    iget-object v1, p2, Lcom/android/managedprovisioning/ProvisioningParams;->mAdminExtrasBundle:Landroid/os/PersistableBundle;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 156
    return-void
.end method

.method public parseIntent(Landroid/content/Intent;)Lcom/android/managedprovisioning/ProvisioningParams;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/managedprovisioning/MessageParser$ParseException;
        }
    .end annotation

    .prologue
    .line 159
    const-string v0, "Processing intent."

    invoke-static {v0}, Lcom/android/managedprovisioning/ProvisionLogger;->logi(Ljava/lang/String;)V

    .line 160
    const-string v0, "android.nfc.extra.NDEF_MESSAGES"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 161
    invoke-virtual {p0, p1}, Lcom/android/managedprovisioning/MessageParser;->parseNfcIntent(Landroid/content/Intent;)Lcom/android/managedprovisioning/ProvisioningParams;

    move-result-object v0

    .line 163
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/managedprovisioning/MessageParser;->parseNonNfcIntent(Landroid/content/Intent;)Lcom/android/managedprovisioning/ProvisioningParams;

    move-result-object v0

    goto :goto_0
.end method

.method public parseNfcIntent(Landroid/content/Intent;)Lcom/android/managedprovisioning/ProvisioningParams;
    .locals 11
    .param p1, "nfcIntent"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/managedprovisioning/MessageParser$ParseException;
        }
    .end annotation

    .prologue
    .line 169
    const-string v8, "Processing Nfc Payload."

    invoke-static {v8}, Lcom/android/managedprovisioning/ProvisionLogger;->logi(Ljava/lang/String;)V

    .line 171
    const-string v8, "android.nfc.extra.NDEF_MESSAGES"

    invoke-virtual {p1, v8}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v0

    .local v0, "arr$":[Landroid/os/Parcelable;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v7, v0, v2

    .local v7, "rawMsg":Landroid/os/Parcelable;
    move-object v5, v7

    .line 173
    check-cast v5, Landroid/nfc/NdefMessage;

    .line 176
    .local v5, "msg":Landroid/nfc/NdefMessage;
    invoke-virtual {v5}, Landroid/nfc/NdefMessage;->getRecords()[Landroid/nfc/NdefRecord;

    move-result-object v8

    const/4 v9, 0x0

    aget-object v1, v8, v9

    .line 177
    .local v1, "firstRecord":Landroid/nfc/NdefRecord;
    new-instance v4, Ljava/lang/String;

    invoke-virtual {v1}, Landroid/nfc/NdefRecord;->getType()[B

    move-result-object v8

    sget-object v9, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-direct {v4, v8, v9}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 179
    .local v4, "mimeType":Ljava/lang/String;
    const-string v8, "application/com.android.managedprovisioning"

    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 180
    new-instance v8, Ljava/lang/String;

    invoke-virtual {v1}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v9

    sget-object v10, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-direct {v8, v9, v10}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    invoke-direct {p0, v8}, Lcom/android/managedprovisioning/MessageParser;->parseProperties(Ljava/lang/String;)Lcom/android/managedprovisioning/ProvisioningParams;

    move-result-object v6

    .line 182
    .local v6, "params":Lcom/android/managedprovisioning/ProvisioningParams;
    const/4 v8, 0x1

    iput-boolean v8, v6, Lcom/android/managedprovisioning/ProvisioningParams;->mStartedByNfc:Z

    .line 183
    return-object v6

    .line 171
    .end local v6    # "params":Lcom/android/managedprovisioning/ProvisioningParams;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 186
    .end local v1    # "firstRecord":Landroid/nfc/NdefRecord;
    .end local v4    # "mimeType":Ljava/lang/String;
    .end local v5    # "msg":Landroid/nfc/NdefMessage;
    .end local v7    # "rawMsg":Landroid/os/Parcelable;
    :cond_1
    new-instance v8, Lcom/android/managedprovisioning/MessageParser$ParseException;

    const-string v9, "Intent does not contain NfcRecord with the correct MIME type."

    const v10, 0x7f070029

    invoke-direct {v8, v9, v10}, Lcom/android/managedprovisioning/MessageParser$ParseException;-><init>(Ljava/lang/String;I)V

    throw v8
.end method

.method public parseNonNfcIntent(Landroid/content/Intent;)Lcom/android/managedprovisioning/ProvisioningParams;
    .locals 9
    .param p1, "intent"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/managedprovisioning/MessageParser$ParseException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 251
    const-string v4, "Processing intent."

    invoke-static {v4}, Lcom/android/managedprovisioning/ProvisionLogger;->logi(Ljava/lang/String;)V

    .line 252
    new-instance v3, Lcom/android/managedprovisioning/ProvisioningParams;

    invoke-direct {v3}, Lcom/android/managedprovisioning/ProvisioningParams;-><init>()V

    .line 254
    .local v3, "params":Lcom/android/managedprovisioning/ProvisioningParams;
    const-string v4, "android.app.extra.PROVISIONING_TIME_ZONE"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/android/managedprovisioning/ProvisioningParams;->mTimeZone:Ljava/lang/String;

    .line 255
    const-string v4, "android.app.extra.PROVISIONING_LOCALE"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 256
    .local v2, "localeString":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 257
    invoke-static {v2}, Lcom/android/managedprovisioning/MessageParser;->stringToLocale(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v4

    iput-object v4, v3, Lcom/android/managedprovisioning/ProvisioningParams;->mLocale:Ljava/util/Locale;

    .line 259
    :cond_0
    const-string v4, "android.app.extra.PROVISIONING_WIFI_SSID"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiSsid:Ljava/lang/String;

    .line 260
    const-string v4, "android.app.extra.PROVISIONING_WIFI_SECURITY_TYPE"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiSecurityType:Ljava/lang/String;

    .line 261
    const-string v4, "android.app.extra.PROVISIONING_WIFI_PASSWORD"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiPassword:Ljava/lang/String;

    .line 262
    const-string v4, "android.app.extra.PROVISIONING_WIFI_PROXY_HOST"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiProxyHost:Ljava/lang/String;

    .line 263
    const-string v4, "android.app.extra.PROVISIONING_WIFI_PROXY_BYPASS"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiProxyBypassHosts:Ljava/lang/String;

    .line 264
    const-string v4, "android.app.extra.PROVISIONING_WIFI_PAC_URL"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiPacUrl:Ljava/lang/String;

    .line 265
    const-string v4, "android.app.extra.PROVISIONING_DEVICE_ADMIN_PACKAGE_NAME"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/android/managedprovisioning/ProvisioningParams;->mDeviceAdminPackageName:Ljava/lang/String;

    .line 267
    const-string v4, "android.app.extra.PROVISIONING_DEVICE_ADMIN_PACKAGE_DOWNLOAD_LOCATION"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/android/managedprovisioning/ProvisioningParams;->mDeviceAdminPackageDownloadLocation:Ljava/lang/String;

    .line 269
    const-string v4, "android.app.extra.PROVISIONING_DEVICE_ADMIN_PACKAGE_DOWNLOAD_COOKIE_HEADER"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/android/managedprovisioning/ProvisioningParams;->mDeviceAdminPackageDownloadCookieHeader:Ljava/lang/String;

    .line 271
    const-string v4, "android.app.extra.PROVISIONING_DEVICE_ADMIN_PACKAGE_CHECKSUM"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 272
    .local v1, "hashString":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 273
    invoke-static {v1}, Lcom/android/managedprovisioning/MessageParser;->stringToByteArray(Ljava/lang/String;)[B

    move-result-object v4

    iput-object v4, v3, Lcom/android/managedprovisioning/ProvisioningParams;->mDeviceAdminPackageChecksum:[B

    .line 276
    :cond_1
    const-string v4, "android.app.extra.PROVISIONING_LOCAL_TIME"

    const-wide/16 v6, -0x1

    invoke-virtual {p1, v4, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    iput-wide v4, v3, Lcom/android/managedprovisioning/ProvisioningParams;->mLocalTime:J

    .line 279
    const-string v4, "android.app.extra.PROVISIONING_WIFI_PROXY_PORT"

    invoke-virtual {p1, v4, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    iput v4, v3, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiProxyPort:I

    .line 282
    const-string v4, "android.app.extra.PROVISIONING_WIFI_HIDDEN"

    invoke-virtual {p1, v4, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    iput-boolean v4, v3, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiHidden:Z

    .line 284
    const-string v4, "com.android.managedprovisioning.extra.started_by_nfc"

    invoke-virtual {p1, v4, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    iput-boolean v4, v3, Lcom/android/managedprovisioning/ProvisioningParams;->mStartedByNfc:Z

    .line 288
    :try_start_0
    const-string v4, "android.app.extra.PROVISIONING_ADMIN_EXTRAS_BUNDLE"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Landroid/os/PersistableBundle;

    iput-object v4, v3, Lcom/android/managedprovisioning/ProvisioningParams;->mAdminExtrasBundle:Landroid/os/PersistableBundle;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 296
    invoke-direct {p0, v3}, Lcom/android/managedprovisioning/MessageParser;->checkValidityOfProvisioningParams(Lcom/android/managedprovisioning/ProvisioningParams;)V

    .line 297
    return-object v3

    .line 290
    :catch_0
    move-exception v0

    .line 291
    .local v0, "e":Ljava/lang/ClassCastException;
    new-instance v4, Lcom/android/managedprovisioning/MessageParser$ParseException;

    const-string v5, "Extra android.app.extra.PROVISIONING_ADMIN_EXTRAS_BUNDLE must be of type PersistableBundle."

    const v6, 0x7f070029

    invoke-direct {v4, v5, v6, v0}, Lcom/android/managedprovisioning/MessageParser$ParseException;-><init>(Ljava/lang/String;ILjava/lang/Throwable;)V

    throw v4
.end method
