.class Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity$5;
.super Ljava/lang/Object;
.source "DeviceOwnerProvisioningActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->showCancelResetDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;


# direct methods
.method constructor <init>(Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;)V
    .locals 0

    .prologue
    .line 267
    iput-object p1, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity$5;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "id"    # I

    .prologue
    .line 270
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 271
    monitor-enter p0

    .line 272
    :try_start_0
    iget-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity$5;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;

    const/4 v1, 0x0

    # setter for: Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->mDialog:Landroid/app/Dialog;
    invoke-static {v0, v1}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->access$302(Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;Landroid/app/Dialog;)Landroid/app/Dialog;

    .line 273
    iget-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity$5;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;

    # getter for: Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->mDone:Z
    invoke-static {v0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->access$500(Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 274
    iget-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity$5;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;

    # invokes: Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->onProvisioningSuccess()V
    invoke-static {v0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->access$400(Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;)V

    .line 276
    :cond_0
    monitor-exit p0

    .line 277
    return-void

    .line 276
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
