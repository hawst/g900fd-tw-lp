.class public Lcom/android/managedprovisioning/task/InstallPackageTask;
.super Ljava/lang/Object;
.source "InstallPackageTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/managedprovisioning/task/InstallPackageTask$1;,
        Lcom/android/managedprovisioning/task/InstallPackageTask$Callback;,
        Lcom/android/managedprovisioning/task/InstallPackageTask$PackageInstallObserver;
    }
.end annotation


# instance fields
.field private final mCallback:Lcom/android/managedprovisioning/task/InstallPackageTask$Callback;

.field private final mContext:Landroid/content/Context;

.field private mPackageLocation:Ljava/lang/String;

.field private final mPackageName:Ljava/lang/String;

.field private mPackageVerifierEnable:I

.field private mPm:Landroid/content/pm/PackageManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/android/managedprovisioning/task/InstallPackageTask$Callback;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "callback"    # Lcom/android/managedprovisioning/task/InstallPackageTask$Callback;

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p3, p0, Lcom/android/managedprovisioning/task/InstallPackageTask;->mCallback:Lcom/android/managedprovisioning/task/InstallPackageTask$Callback;

    .line 55
    iput-object p1, p0, Lcom/android/managedprovisioning/task/InstallPackageTask;->mContext:Landroid/content/Context;

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/managedprovisioning/task/InstallPackageTask;->mPackageLocation:Ljava/lang/String;

    .line 57
    iput-object p2, p0, Lcom/android/managedprovisioning/task/InstallPackageTask;->mPackageName:Ljava/lang/String;

    .line 58
    return-void
.end method

.method static synthetic access$100(Lcom/android/managedprovisioning/task/InstallPackageTask;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/android/managedprovisioning/task/InstallPackageTask;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/android/managedprovisioning/task/InstallPackageTask;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/managedprovisioning/task/InstallPackageTask;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/managedprovisioning/task/InstallPackageTask;

    .prologue
    .line 40
    iget v0, p0, Lcom/android/managedprovisioning/task/InstallPackageTask;->mPackageVerifierEnable:I

    return v0
.end method

.method static synthetic access$300(Lcom/android/managedprovisioning/task/InstallPackageTask;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/android/managedprovisioning/task/InstallPackageTask;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/android/managedprovisioning/task/InstallPackageTask;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/managedprovisioning/task/InstallPackageTask;)Lcom/android/managedprovisioning/task/InstallPackageTask$Callback;
    .locals 1
    .param p0, "x0"    # Lcom/android/managedprovisioning/task/InstallPackageTask;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/android/managedprovisioning/task/InstallPackageTask;->mCallback:Lcom/android/managedprovisioning/task/InstallPackageTask$Callback;

    return-object v0
.end method

.method private packageContentIsCorrect()Z
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 90
    iget-object v6, p0, Lcom/android/managedprovisioning/task/InstallPackageTask;->mPm:Landroid/content/pm/PackageManager;

    iget-object v7, p0, Lcom/android/managedprovisioning/task/InstallPackageTask;->mPackageLocation:Ljava/lang/String;

    const/4 v8, 0x2

    invoke-virtual {v6, v7, v8}, Landroid/content/pm/PackageManager;->getPackageArchiveInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v4

    .line 92
    .local v4, "pi":Landroid/content/pm/PackageInfo;
    if-nez v4, :cond_0

    .line 93
    const-string v6, "Package could not be parsed successfully."

    invoke-static {v6}, Lcom/android/managedprovisioning/ProvisionLogger;->loge(Ljava/lang/String;)V

    .line 94
    iget-object v6, p0, Lcom/android/managedprovisioning/task/InstallPackageTask;->mCallback:Lcom/android/managedprovisioning/task/InstallPackageTask$Callback;

    invoke-virtual {v6, v5}, Lcom/android/managedprovisioning/task/InstallPackageTask$Callback;->onError(I)V

    .line 112
    :goto_0
    return v5

    .line 97
    :cond_0
    iget-object v6, v4, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    iget-object v7, p0, Lcom/android/managedprovisioning/task/InstallPackageTask;->mPackageName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 98
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Package name in apk ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v4, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ") does not match package name specified by programmer ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/managedprovisioning/task/InstallPackageTask;->mPackageName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/android/managedprovisioning/ProvisionLogger;->loge(Ljava/lang/String;)V

    .line 101
    iget-object v6, p0, Lcom/android/managedprovisioning/task/InstallPackageTask;->mCallback:Lcom/android/managedprovisioning/task/InstallPackageTask$Callback;

    invoke-virtual {v6, v5}, Lcom/android/managedprovisioning/task/InstallPackageTask$Callback;->onError(I)V

    goto :goto_0

    .line 104
    :cond_1
    iget-object v1, v4, Landroid/content/pm/PackageInfo;->receivers:[Landroid/content/pm/ActivityInfo;

    .local v1, "arr$":[Landroid/content/pm/ActivityInfo;
    array-length v3, v1

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_3

    aget-object v0, v1, v2

    .line 105
    .local v0, "ai":Landroid/content/pm/ActivityInfo;
    iget-object v6, v0, Landroid/content/pm/ActivityInfo;->permission:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    iget-object v6, v0, Landroid/content/pm/ActivityInfo;->permission:Ljava/lang/String;

    const-string v7, "android.permission.BIND_DEVICE_ADMIN"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 107
    const/4 v5, 0x1

    goto :goto_0

    .line 104
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 110
    .end local v0    # "ai":Landroid/content/pm/ActivityInfo;
    :cond_3
    const-string v6, "Installed package has no admin receiver."

    invoke-static {v6}, Lcom/android/managedprovisioning/ProvisionLogger;->loge(Ljava/lang/String;)V

    .line 111
    iget-object v6, p0, Lcom/android/managedprovisioning/task/InstallPackageTask;->mCallback:Lcom/android/managedprovisioning/task/InstallPackageTask$Callback;

    invoke-virtual {v6, v5}, Lcom/android/managedprovisioning/task/InstallPackageTask$Callback;->onError(I)V

    goto :goto_0
.end method


# virtual methods
.method public run(Ljava/lang/String;)V
    .locals 6
    .param p1, "packageLocation"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 61
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 62
    const-string v2, "Package Location is empty."

    invoke-static {v2}, Lcom/android/managedprovisioning/ProvisionLogger;->loge(Ljava/lang/String;)V

    .line 63
    iget-object v2, p0, Lcom/android/managedprovisioning/task/InstallPackageTask;->mCallback:Lcom/android/managedprovisioning/task/InstallPackageTask$Callback;

    invoke-virtual {v2, v5}, Lcom/android/managedprovisioning/task/InstallPackageTask$Callback;->onError(I)V

    .line 87
    :cond_0
    :goto_0
    return-void

    .line 66
    :cond_1
    iput-object p1, p0, Lcom/android/managedprovisioning/task/InstallPackageTask;->mPackageLocation:Ljava/lang/String;

    .line 68
    new-instance v0, Lcom/android/managedprovisioning/task/InstallPackageTask$PackageInstallObserver;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2}, Lcom/android/managedprovisioning/task/InstallPackageTask$PackageInstallObserver;-><init>(Lcom/android/managedprovisioning/task/InstallPackageTask;Lcom/android/managedprovisioning/task/InstallPackageTask$1;)V

    .line 69
    .local v0, "observer":Lcom/android/managedprovisioning/task/InstallPackageTask$PackageInstallObserver;
    iget-object v2, p0, Lcom/android/managedprovisioning/task/InstallPackageTask;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    iput-object v2, p0, Lcom/android/managedprovisioning/task/InstallPackageTask;->mPm:Landroid/content/pm/PackageManager;

    .line 71
    invoke-direct {p0}, Lcom/android/managedprovisioning/task/InstallPackageTask;->packageContentIsCorrect()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 73
    iget-object v2, p0, Lcom/android/managedprovisioning/task/InstallPackageTask;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "package_verifier_enable"

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/android/managedprovisioning/task/InstallPackageTask;->mPackageVerifierEnable:I

    .line 75
    iget-object v2, p0, Lcom/android/managedprovisioning/task/InstallPackageTask;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "package_verifier_enable"

    invoke-static {v2, v3, v5}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 77
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "file://"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/managedprovisioning/task/InstallPackageTask;->mPackageLocation:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 81
    .local v1, "packageUri":Landroid/net/Uri;
    iget-object v2, p0, Lcom/android/managedprovisioning/task/InstallPackageTask;->mPm:Landroid/content/pm/PackageManager;

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/android/managedprovisioning/task/InstallPackageTask;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v1, v0, v3, v4}, Landroid/content/pm/PackageManager;->installPackage(Landroid/net/Uri;Landroid/content/pm/IPackageInstallObserver;ILjava/lang/String;)V

    goto :goto_0
.end method
