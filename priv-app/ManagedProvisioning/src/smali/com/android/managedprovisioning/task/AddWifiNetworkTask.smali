.class public Lcom/android/managedprovisioning/task/AddWifiNetworkTask;
.super Ljava/lang/Object;
.source "AddWifiNetworkTask.java"

# interfaces
.implements Lcom/android/managedprovisioning/NetworkMonitor$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/managedprovisioning/task/AddWifiNetworkTask$Callback;
    }
.end annotation


# instance fields
.field private final mCallback:Lcom/android/managedprovisioning/task/AddWifiNetworkTask$Callback;

.field private final mContext:Landroid/content/Context;

.field private mDurationNextSleep:I

.field private final mHidden:Z

.field private mNetworkMonitor:Lcom/android/managedprovisioning/NetworkMonitor;

.field private final mPacUrl:Ljava/lang/String;

.field private final mPassword:Ljava/lang/String;

.field private final mProxyBypassHosts:Ljava/lang/String;

.field private final mProxyHost:Ljava/lang/String;

.field private final mProxyPort:I

.field private mRetriesLeft:I

.field private final mSecurityType:Ljava/lang/String;

.field private final mSsid:Ljava/lang/String;

.field private mWifiConfig:Lcom/android/managedprovisioning/WifiConfig;

.field private mWifiManager:Landroid/net/wifi/WifiManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/android/managedprovisioning/task/AddWifiNetworkTask$Callback;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "ssid"    # Ljava/lang/String;
    .param p3, "hidden"    # Z
    .param p4, "securityType"    # Ljava/lang/String;
    .param p5, "password"    # Ljava/lang/String;
    .param p6, "proxyHost"    # Ljava/lang/String;
    .param p7, "proxyPort"    # I
    .param p8, "proxyBypassHosts"    # Ljava/lang/String;
    .param p9, "pacUrl"    # Ljava/lang/String;
    .param p10, "callback"    # Lcom/android/managedprovisioning/task/AddWifiNetworkTask$Callback;

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mDurationNextSleep:I

    .line 56
    const/4 v0, 0x6

    iput v0, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mRetriesLeft:I

    .line 61
    iput-object p10, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mCallback:Lcom/android/managedprovisioning/task/AddWifiNetworkTask$Callback;

    .line 62
    iput-object p1, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mContext:Landroid/content/Context;

    .line 63
    iput-object p2, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mSsid:Ljava/lang/String;

    .line 64
    iput-boolean p3, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mHidden:Z

    .line 65
    iput-object p4, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mSecurityType:Ljava/lang/String;

    .line 66
    iput-object p5, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mPassword:Ljava/lang/String;

    .line 67
    iput-object p6, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mProxyHost:Ljava/lang/String;

    .line 68
    iput p7, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mProxyPort:I

    .line 69
    iput-object p8, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mProxyBypassHosts:Ljava/lang/String;

    .line 70
    iput-object p9, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mPacUrl:Ljava/lang/String;

    .line 71
    iget-object v0, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mContext:Landroid/content/Context;

    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 72
    new-instance v0, Lcom/android/managedprovisioning/WifiConfig;

    iget-object v1, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-direct {v0, v1}, Lcom/android/managedprovisioning/WifiConfig;-><init>(Landroid/net/wifi/WifiManager;)V

    iput-object v0, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mWifiConfig:Lcom/android/managedprovisioning/WifiConfig;

    .line 73
    return-void
.end method

.method private connectToProvidedNetwork()V
    .locals 11

    .prologue
    .line 99
    iget-object v0, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mWifiConfig:Lcom/android/managedprovisioning/WifiConfig;

    iget-object v1, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mSsid:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mHidden:Z

    iget-object v3, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mSecurityType:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mPassword:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mProxyHost:Ljava/lang/String;

    iget v6, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mProxyPort:I

    iget-object v7, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mProxyBypassHosts:Ljava/lang/String;

    iget-object v8, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mPacUrl:Ljava/lang/String;

    invoke-virtual/range {v0 .. v8}, Lcom/android/managedprovisioning/WifiConfig;->addNetwork(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)I

    move-result v10

    .line 102
    .local v10, "netId":I
    const/4 v0, -0x1

    if-ne v10, v0, :cond_2

    .line 103
    const-string v0, "Failed to save network."

    invoke-static {v0}, Lcom/android/managedprovisioning/ProvisionLogger;->loge(Ljava/lang/String;)V

    .line 104
    iget v0, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mRetriesLeft:I

    if-lez v0, :cond_1

    .line 105
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Retrying in "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mDurationNextSleep:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ms."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/managedprovisioning/ProvisionLogger;->loge(Ljava/lang/String;)V

    .line 107
    :try_start_0
    iget v0, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mDurationNextSleep:I

    int-to-long v0, v0

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 111
    :goto_0
    iget v0, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mDurationNextSleep:I

    mul-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mDurationNextSleep:I

    .line 112
    iget v0, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mRetriesLeft:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mRetriesLeft:I

    .line 113
    invoke-direct {p0}, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->connectToProvidedNetwork()V

    .line 130
    :cond_0
    :goto_1
    return-void

    .line 108
    :catch_0
    move-exception v9

    .line 109
    .local v9, "e":Ljava/lang/InterruptedException;
    const-string v0, "Retry interrupted."

    invoke-static {v0}, Lcom/android/managedprovisioning/ProvisionLogger;->loge(Ljava/lang/String;)V

    goto :goto_0

    .line 116
    .end local v9    # "e":Ljava/lang/InterruptedException;
    :cond_1
    const-string v0, "Already retried 6 times. Quit retrying and report error."

    invoke-static {v0}, Lcom/android/managedprovisioning/ProvisionLogger;->loge(Ljava/lang/String;)V

    .line 118
    iget-object v0, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mCallback:Lcom/android/managedprovisioning/task/AddWifiNetworkTask$Callback;

    invoke-virtual {v0}, Lcom/android/managedprovisioning/task/AddWifiNetworkTask$Callback;->onError()V

    goto :goto_1

    .line 122
    :cond_2
    iget-object v0, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->reconnect()Z

    move-result v0

    if-nez v0, :cond_0

    .line 123
    const-string v0, "Unable to connect to wifi"

    invoke-static {v0}, Lcom/android/managedprovisioning/ProvisionLogger;->loge(Ljava/lang/String;)V

    .line 124
    iget-object v0, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mCallback:Lcom/android/managedprovisioning/task/AddWifiNetworkTask$Callback;

    invoke-virtual {v0}, Lcom/android/managedprovisioning/task/AddWifiNetworkTask$Callback;->onError()V

    goto :goto_1
.end method

.method private enableWifi()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 133
    iget-object v4, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mWifiManager:Landroid/net/wifi/WifiManager;

    if-eqz v4, :cond_0

    .line 134
    iget-object v4, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager;->getWifiState()I

    move-result v1

    .line 135
    .local v1, "wifiState":I
    const/4 v4, 0x3

    if-ne v1, v4, :cond_1

    move v0, v3

    .line 136
    .local v0, "wifiOn":Z
    :goto_0
    if-nez v0, :cond_3

    .line 137
    iget-object v4, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v4, v3}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    move-result v4

    if-nez v4, :cond_2

    .line 146
    .end local v0    # "wifiOn":Z
    .end local v1    # "wifiState":I
    :cond_0
    :goto_1
    return v2

    .restart local v1    # "wifiState":I
    :cond_1
    move v0, v2

    .line 135
    goto :goto_0

    .restart local v0    # "wifiOn":Z
    :cond_2
    move v2, v3

    .line 140
    goto :goto_1

    :cond_3
    move v2, v3

    .line 143
    goto :goto_1
.end method

.method public static getWifiPickIntent()Landroid/content/Intent;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 181
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.net.wifi.PICK_WIFI_NETWORK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 182
    .local v0, "wifiIntent":Landroid/content/Intent;
    const-string v1, "only_access_points"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 183
    const-string v1, "extra_prefs_show_button_bar"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 184
    const-string v1, "wifi_enable_next_on_connect"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 185
    return-object v0
.end method

.method public static isConnectedToWifi(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 174
    const-string v2, "connectivity"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 176
    .local v0, "cm":Landroid/net/ConnectivityManager;
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 177
    .local v1, "info":Landroid/net/NetworkInfo;
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    return v2
.end method


# virtual methods
.method public cleanUp()V
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mNetworkMonitor:Lcom/android/managedprovisioning/NetworkMonitor;

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mNetworkMonitor:Lcom/android/managedprovisioning/NetworkMonitor;

    invoke-virtual {v0}, Lcom/android/managedprovisioning/NetworkMonitor;->close()V

    .line 169
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mNetworkMonitor:Lcom/android/managedprovisioning/NetworkMonitor;

    .line 171
    :cond_0
    return-void
.end method

.method public onNetworkConnected()V
    .locals 2

    .prologue
    .line 152
    iget-object v0, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/managedprovisioning/NetworkMonitor;->isConnectedToWifi(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mSsid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 154
    const-string v0, "Connected to the correct network"

    invoke-static {v0}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 155
    iget-object v0, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mNetworkMonitor:Lcom/android/managedprovisioning/NetworkMonitor;

    invoke-virtual {v0}, Lcom/android/managedprovisioning/NetworkMonitor;->close()V

    .line 156
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mNetworkMonitor:Lcom/android/managedprovisioning/NetworkMonitor;

    .line 157
    iget-object v0, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mCallback:Lcom/android/managedprovisioning/task/AddWifiNetworkTask$Callback;

    invoke-virtual {v0}, Lcom/android/managedprovisioning/task/AddWifiNetworkTask$Callback;->onSuccess()V

    .line 159
    :cond_0
    return-void
.end method

.method public onNetworkDisconnected()V
    .locals 0

    .prologue
    .line 164
    return-void
.end method

.method public run()V
    .locals 2

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->enableWifi()Z

    move-result v0

    if-nez v0, :cond_0

    .line 77
    const-string v0, "Failed to enable wifi"

    invoke-static {v0}, Lcom/android/managedprovisioning/ProvisionLogger;->loge(Ljava/lang/String;)V

    .line 78
    iget-object v0, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mCallback:Lcom/android/managedprovisioning/task/AddWifiNetworkTask$Callback;

    invoke-virtual {v0}, Lcom/android/managedprovisioning/task/AddWifiNetworkTask$Callback;->onError()V

    .line 96
    :goto_0
    return-void

    .line 82
    :cond_0
    new-instance v0, Lcom/android/managedprovisioning/NetworkMonitor;

    iget-object v1, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Lcom/android/managedprovisioning/NetworkMonitor;-><init>(Landroid/content/Context;Lcom/android/managedprovisioning/NetworkMonitor$Callback;)V

    iput-object v0, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mNetworkMonitor:Lcom/android/managedprovisioning/NetworkMonitor;

    .line 84
    iget-object v0, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->isConnectedToWifi(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 85
    iget-object v0, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mSsid:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 86
    const-string v0, "Wifi is supposed to be setup in activity, or a valid wifi ssid has to be specified."

    invoke-static {v0}, Lcom/android/managedprovisioning/ProvisionLogger;->loge(Ljava/lang/String;)V

    .line 88
    iget-object v0, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mCallback:Lcom/android/managedprovisioning/task/AddWifiNetworkTask$Callback;

    invoke-virtual {v0}, Lcom/android/managedprovisioning/task/AddWifiNetworkTask$Callback;->onError()V

    goto :goto_0

    .line 92
    :cond_1
    invoke-direct {p0}, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->connectToProvidedNetwork()V

    goto :goto_0

    .line 94
    :cond_2
    iget-object v0, p0, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->mCallback:Lcom/android/managedprovisioning/task/AddWifiNetworkTask$Callback;

    invoke-virtual {v0}, Lcom/android/managedprovisioning/task/AddWifiNetworkTask$Callback;->onSuccess()V

    goto :goto_0
.end method
