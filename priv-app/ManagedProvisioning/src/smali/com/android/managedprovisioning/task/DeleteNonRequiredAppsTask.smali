.class public Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;
.super Ljava/lang/Object;
.source "DeleteNonRequiredAppsTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask$Callback;,
        Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask$PackageDeleteObserver;
    }
.end annotation


# instance fields
.field private final mCallback:Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask$Callback;

.field private final mContext:Landroid/content/Context;

.field private final mDisableInstallShortcutListeners:Z

.field private final mIpm:Landroid/content/pm/IPackageManager;

.field private final mMdmPackageName:Ljava/lang/String;

.field private final mNewProfile:Z

.field private final mPm:Landroid/content/pm/PackageManager;

.field private final mReqAppsList:I

.field private final mUserId:I

.field private final mVendorReqAppsList:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;IIIZZLcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask$Callback;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mdmPackageName"    # Ljava/lang/String;
    .param p3, "userId"    # I
    .param p4, "requiredAppsList"    # I
    .param p5, "vendorRequiredAppsList"    # I
    .param p6, "newProfile"    # Z
    .param p7, "disableInstallShortcutListeners"    # Z
    .param p8, "callback"    # Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask$Callback;

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput-object p8, p0, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->mCallback:Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask$Callback;

    .line 81
    iput-object p1, p0, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->mContext:Landroid/content/Context;

    .line 82
    iput-object p2, p0, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->mMdmPackageName:Ljava/lang/String;

    .line 83
    iput p3, p0, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->mUserId:I

    .line 84
    const-string v0, "package"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/content/pm/IPackageManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/pm/IPackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->mIpm:Landroid/content/pm/IPackageManager;

    .line 85
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->mPm:Landroid/content/pm/PackageManager;

    .line 86
    iput p4, p0, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->mReqAppsList:I

    .line 87
    iput p5, p0, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->mVendorReqAppsList:I

    .line 88
    iput-boolean p6, p0, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->mNewProfile:Z

    .line 89
    iput-boolean p7, p0, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->mDisableInstallShortcutListeners:Z

    .line 90
    return-void
.end method

.method static synthetic access$000(Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;)Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask$Callback;
    .locals 1
    .param p0, "x0"    # Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->mCallback:Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask$Callback;

    return-object v0
.end method

.method private deleteNonRequiredApps()V
    .locals 15

    .prologue
    .line 107
    const-string v12, "Deleting non required apps."

    invoke-static {v12}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 109
    new-instance v2, Ljava/io/File;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v13, p0, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    sget-object v13, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "system_apps"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    sget-object v13, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "user"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget v13, p0, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->mUserId:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ".xml"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v2, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 111
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v12

    invoke-virtual {v12}, Ljava/io/File;->mkdirs()Z

    .line 113
    invoke-direct {p0}, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->getCurrentSystemApps()Ljava/util/Set;

    move-result-object v1

    .line 115
    .local v1, "currentApps":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-boolean v12, p0, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->mNewProfile:Z

    if-eqz v12, :cond_2

    .line 118
    new-instance v10, Ljava/util/HashSet;

    invoke-direct {v10}, Ljava/util/HashSet;-><init>()V

    .line 130
    .local v10, "previousApps":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :goto_0
    invoke-direct {p0, v1, v2}, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->writeSystemApps(Ljava/util/Set;Ljava/io/File;)V

    .line 131
    move-object v6, v1

    .line 132
    .local v6, "newApps":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v6, v10}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 134
    iget-boolean v12, p0, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->mDisableInstallShortcutListeners:Z

    if-eqz v12, :cond_0

    .line 135
    new-instance v0, Landroid/content/Intent;

    const-string v12, "com.android.launcher.action.INSTALL_SHORTCUT"

    invoke-direct {v0, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 136
    .local v0, "actionShortcut":Landroid/content/Intent;
    invoke-interface {v10}, Ljava/util/Set;->isEmpty()Z

    move-result v12

    if-eqz v12, :cond_5

    .line 139
    invoke-direct {p0, v0}, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->disableReceivers(Landroid/content/Intent;)V

    .line 149
    .end local v0    # "actionShortcut":Landroid/content/Intent;
    :cond_0
    move-object v9, v6

    .line 150
    .local v9, "packagesToDelete":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-virtual {p0}, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->getRequiredApps()Ljava/util/Set;

    move-result-object v12

    invoke-interface {v9, v12}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 151
    invoke-direct {p0}, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->getCurrentAppsWithLauncher()Ljava/util/Set;

    move-result-object v12

    invoke-interface {v9, v12}, Ljava/util/Set;->retainAll(Ljava/util/Collection;)Z

    .line 153
    iget-boolean v12, p0, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->mNewProfile:Z

    if-eqz v12, :cond_1

    .line 154
    const-string v12, "com.android.server.telecom"

    invoke-interface {v9, v12}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 156
    :cond_1
    invoke-interface {v9}, Ljava/util/Set;->size()I

    move-result v11

    .line 157
    .local v11, "size":I
    if-lez v11, :cond_6

    .line 158
    new-instance v7, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask$PackageDeleteObserver;

    invoke-interface {v9}, Ljava/util/Set;->size()I

    move-result v12

    invoke-direct {v7, p0, v12}, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask$PackageDeleteObserver;-><init>(Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;I)V

    .line 160
    .local v7, "packageDeleteObserver":Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask$PackageDeleteObserver;
    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 162
    .local v8, "packageName":Ljava/lang/String;
    :try_start_0
    iget-object v12, p0, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->mIpm:Landroid/content/pm/IPackageManager;

    iget v13, p0, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->mUserId:I

    const/4 v14, 0x4

    invoke-interface {v12, v8, v7, v13, v14}, Landroid/content/pm/IPackageManager;->deletePackageAsUser(Ljava/lang/String;Landroid/content/pm/IPackageDeleteObserver;II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 164
    :catch_0
    move-exception v4

    .line 166
    .local v4, "neverThrown":Landroid/os/RemoteException;
    const-string v12, "This should not happen."

    invoke-static {v12, v4}, Lcom/android/managedprovisioning/ProvisionLogger;->loge(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 120
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "neverThrown":Landroid/os/RemoteException;
    .end local v6    # "newApps":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v7    # "packageDeleteObserver":Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask$PackageDeleteObserver;
    .end local v8    # "packageName":Ljava/lang/String;
    .end local v9    # "packagesToDelete":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v10    # "previousApps":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v11    # "size":I
    :cond_2
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_3

    .line 121
    invoke-direct {p0, v2}, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->readSystemApps(Ljava/io/File;)Ljava/util/Set;

    move-result-object v10

    .restart local v10    # "previousApps":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    goto :goto_0

    .line 125
    .end local v10    # "previousApps":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_3
    invoke-direct {p0, v1, v2}, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->writeSystemApps(Ljava/util/Set;Ljava/io/File;)V

    .line 126
    iget-object v12, p0, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->mCallback:Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask$Callback;

    invoke-virtual {v12}, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask$Callback;->onSuccess()V

    .line 172
    :cond_4
    :goto_2
    return-void

    .line 143
    .restart local v0    # "actionShortcut":Landroid/content/Intent;
    .restart local v6    # "newApps":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .restart local v10    # "previousApps":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_5
    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .restart local v3    # "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 144
    .local v5, "newApp":Ljava/lang/String;
    invoke-virtual {v0, v5}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 145
    invoke-direct {p0, v0}, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->disableReceivers(Landroid/content/Intent;)V

    goto :goto_3

    .line 170
    .end local v0    # "actionShortcut":Landroid/content/Intent;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v5    # "newApp":Ljava/lang/String;
    .restart local v9    # "packagesToDelete":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .restart local v11    # "size":I
    :cond_6
    iget-object v12, p0, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->mCallback:Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask$Callback;

    invoke-virtual {v12}, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask$Callback;->onSuccess()V

    goto :goto_2
.end method

.method private disableComponent(Landroid/content/ComponentName;)V
    .locals 6
    .param p1, "toDisable"    # Landroid/content/ComponentName;

    .prologue
    .line 193
    :try_start_0
    iget-object v2, p0, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->mIpm:Landroid/content/pm/IPackageManager;

    const/4 v3, 0x2

    const/4 v4, 0x1

    iget v5, p0, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->mUserId:I

    invoke-interface {v2, p1, v3, v4, v5}, Landroid/content/pm/IPackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;III)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 202
    :goto_0
    return-void

    .line 196
    :catch_0
    move-exception v1

    .line 197
    .local v1, "neverThrown":Landroid/os/RemoteException;
    const-string v2, "This should not happen."

    invoke-static {v2, v1}, Lcom/android/managedprovisioning/ProvisionLogger;->loge(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 198
    .end local v1    # "neverThrown":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 199
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Component not found, not disabling it: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/content/ComponentName;->toShortString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/managedprovisioning/ProvisionLogger;->logw(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private disableNfcBluetoothSharing()V
    .locals 3

    .prologue
    .line 100
    const-string v0, "Disabling Nfc and Bluetooth sharing."

    invoke-static {v0}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 101
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.android.nfc"

    const-string v2, "com.android.nfc.BeamShareActivity"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->disableComponent(Landroid/content/ComponentName;)V

    .line 102
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.android.bluetooth"

    const-string v2, "com.android.bluetooth.opp.BluetoothOppLauncherActivity"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->disableComponent(Landroid/content/ComponentName;)V

    .line 104
    return-void
.end method

.method private disableReceivers(Landroid/content/Intent;)V
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 175
    iget-object v4, p0, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->mPm:Landroid/content/pm/PackageManager;

    const/4 v5, 0x0

    iget v6, p0, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->mUserId:I

    invoke-virtual {v4, p1, v5, v6}, Landroid/content/pm/PackageManager;->queryBroadcastReceivers(Landroid/content/Intent;II)Ljava/util/List;

    move-result-object v2

    .line 176
    .local v2, "receivers":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/ResolveInfo;

    .line 180
    .local v3, "ri":Landroid/content/pm/ResolveInfo;
    iget-object v4, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v4, :cond_0

    .line 181
    iget-object v0, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 187
    .local v0, "ci":Landroid/content/pm/ComponentInfo;
    :goto_1
    new-instance v4, Landroid/content/ComponentName;

    iget-object v5, v0, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    iget-object v6, v0, Landroid/content/pm/ComponentInfo;->name:Ljava/lang/String;

    invoke-direct {v4, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v4}, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->disableComponent(Landroid/content/ComponentName;)V

    goto :goto_0

    .line 182
    .end local v0    # "ci":Landroid/content/pm/ComponentInfo;
    :cond_0
    iget-object v4, v3, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    if-eqz v4, :cond_1

    .line 183
    iget-object v0, v3, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    .restart local v0    # "ci":Landroid/content/pm/ComponentInfo;
    goto :goto_1

    .line 185
    .end local v0    # "ci":Landroid/content/pm/ComponentInfo;
    :cond_1
    iget-object v0, v3, Landroid/content/pm/ResolveInfo;->providerInfo:Landroid/content/pm/ProviderInfo;

    .restart local v0    # "ci":Landroid/content/pm/ComponentInfo;
    goto :goto_1

    .line 189
    .end local v0    # "ci":Landroid/content/pm/ComponentInfo;
    .end local v3    # "ri":Landroid/content/pm/ResolveInfo;
    :cond_2
    return-void
.end method

.method private getCurrentAppsWithLauncher()Ljava/util/Set;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 227
    new-instance v2, Landroid/content/Intent;

    const-string v5, "android.intent.action.MAIN"

    invoke-direct {v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 228
    .local v2, "launcherIntent":Landroid/content/Intent;
    const-string v5, "android.intent.category.LAUNCHER"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 229
    iget-object v5, p0, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->mPm:Landroid/content/pm/PackageManager;

    const/16 v6, 0x2000

    iget v7, p0, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->mUserId:I

    invoke-virtual {v5, v2, v6, v7}, Landroid/content/pm/PackageManager;->queryIntentActivitiesAsUser(Landroid/content/Intent;II)Ljava/util/List;

    move-result-object v4

    .line 231
    .local v4, "resolveInfos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 232
    .local v0, "apps":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/ResolveInfo;

    .line 233
    .local v3, "resolveInfo":Landroid/content/pm/ResolveInfo;
    iget-object v5, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-interface {v0, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 235
    .end local v3    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    :cond_0
    return-object v0
.end method

.method private getCurrentSystemApps()Ljava/util/Set;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 209
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 210
    .local v2, "apps":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 212
    .local v1, "aInfos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    :try_start_0
    iget-object v5, p0, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->mIpm:Landroid/content/pm/IPackageManager;

    const/16 v6, 0x2000

    iget v7, p0, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->mUserId:I

    invoke-interface {v5, v6, v7}, Landroid/content/pm/IPackageManager;->getInstalledApplications(II)Landroid/content/pm/ParceledListSlice;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/pm/ParceledListSlice;->getList()Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 218
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ApplicationInfo;

    .line 219
    .local v0, "aInfo":Landroid/content/pm/ApplicationInfo;
    iget v5, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v5, v5, 0x1

    if-eqz v5, :cond_0

    .line 220
    iget-object v5, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-interface {v2, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 214
    .end local v0    # "aInfo":Landroid/content/pm/ApplicationInfo;
    .end local v3    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v4

    .line 216
    .local v4, "neverThrown":Landroid/os/RemoteException;
    const-string v5, "This should not happen."

    invoke-static {v5, v4}, Lcom/android/managedprovisioning/ProvisionLogger;->loge(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 223
    .end local v4    # "neverThrown":Landroid/os/RemoteException;
    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_1
    return-object v2
.end method

.method private readSystemApps(Ljava/io/File;)Ljava/util/Set;
    .locals 10
    .param p1, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x3

    .line 259
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 260
    .local v3, "result":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_0

    .line 289
    :goto_0
    return-object v3

    .line 264
    :cond_0
    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 266
    .local v4, "stream":Ljava/io/FileInputStream;
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v2

    .line 267
    .local v2, "parser":Lorg/xmlpull/v1/XmlPullParser;
    const/4 v7, 0x0

    invoke-interface {v2, v4, v7}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 269
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v6

    .line 270
    .local v6, "type":I
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v1

    .line 272
    .local v1, "outerDepth":I
    :cond_1
    :goto_1
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v6

    const/4 v7, 0x1

    if-eq v6, v7, :cond_4

    if-ne v6, v9, :cond_2

    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v7

    if-le v7, v1, :cond_4

    .line 273
    :cond_2
    if-eq v6, v9, :cond_1

    const/4 v7, 0x4

    if-eq v6, v7, :cond_1

    .line 276
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v5

    .line 277
    .local v5, "tag":Ljava/lang/String;
    const-string v7, "item"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 278
    const/4 v7, 0x0

    const-string v8, "value"

    invoke-interface {v2, v7, v8}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v3, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 284
    .end local v1    # "outerDepth":I
    .end local v2    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v4    # "stream":Ljava/io/FileInputStream;
    .end local v5    # "tag":Ljava/lang/String;
    .end local v6    # "type":I
    :catch_0
    move-exception v0

    .line 285
    .local v0, "e":Ljava/io/IOException;
    const-string v7, "IOException trying to read the system apps"

    invoke-static {v7, v0}, Lcom/android/managedprovisioning/ProvisionLogger;->loge(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 280
    .end local v0    # "e":Ljava/io/IOException;
    .restart local v1    # "outerDepth":I
    .restart local v2    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v4    # "stream":Ljava/io/FileInputStream;
    .restart local v5    # "tag":Ljava/lang/String;
    .restart local v6    # "type":I
    :cond_3
    :try_start_1
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unknown tag: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/android/managedprovisioning/ProvisionLogger;->loge(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 286
    .end local v1    # "outerDepth":I
    .end local v2    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v4    # "stream":Ljava/io/FileInputStream;
    .end local v5    # "tag":Ljava/lang/String;
    .end local v6    # "type":I
    :catch_1
    move-exception v0

    .line 287
    .local v0, "e":Lorg/xmlpull/v1/XmlPullParserException;
    const-string v7, "XmlPullParserException trying to read the system apps"

    invoke-static {v7, v0}, Lcom/android/managedprovisioning/ProvisionLogger;->loge(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 283
    .end local v0    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    .restart local v1    # "outerDepth":I
    .restart local v2    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v4    # "stream":Ljava/io/FileInputStream;
    .restart local v6    # "type":I
    :cond_4
    :try_start_2
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0
.end method

.method private writeSystemApps(Ljava/util/Set;Ljava/io/File;)V
    .locals 7
    .param p2, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/io/File;",
            ")V"
        }
    .end annotation

    .prologue
    .line 240
    .local p1, "packageNames":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :try_start_0
    new-instance v4, Ljava/io/FileOutputStream;

    const/4 v5, 0x0

    invoke-direct {v4, p2, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    .line 241
    .local v4, "stream":Ljava/io/FileOutputStream;
    new-instance v3, Lcom/android/internal/util/FastXmlSerializer;

    invoke-direct {v3}, Lcom/android/internal/util/FastXmlSerializer;-><init>()V

    .line 242
    .local v3, "serializer":Lorg/xmlpull/v1/XmlSerializer;
    const-string v5, "utf-8"

    invoke-interface {v3, v4, v5}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 243
    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-interface {v3, v5, v6}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 244
    const/4 v5, 0x0

    const-string v6, "system-apps"

    invoke-interface {v3, v5, v6}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 245
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 246
    .local v2, "packageName":Ljava/lang/String;
    const/4 v5, 0x0

    const-string v6, "item"

    invoke-interface {v3, v5, v6}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 247
    const/4 v5, 0x0

    const-string v6, "value"

    invoke-interface {v3, v5, v6, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 248
    const/4 v5, 0x0

    const-string v6, "item"

    invoke-interface {v3, v5, v6}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 253
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "packageName":Ljava/lang/String;
    .end local v3    # "serializer":Lorg/xmlpull/v1/XmlSerializer;
    .end local v4    # "stream":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v0

    .line 254
    .local v0, "e":Ljava/io/IOException;
    const-string v5, "IOException trying to write the system apps"

    invoke-static {v5, v0}, Lcom/android/managedprovisioning/ProvisionLogger;->loge(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 256
    .end local v0    # "e":Ljava/io/IOException;
    :goto_1
    return-void

    .line 250
    .restart local v1    # "i$":Ljava/util/Iterator;
    .restart local v3    # "serializer":Lorg/xmlpull/v1/XmlSerializer;
    .restart local v4    # "stream":Ljava/io/FileOutputStream;
    :cond_0
    const/4 v5, 0x0

    :try_start_1
    const-string v6, "system-apps"

    invoke-interface {v3, v5, v6}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 251
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    .line 252
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method


# virtual methods
.method protected getRequiredApps()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 293
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, p0, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->mReqAppsList:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 295
    .local v0, "requiredApps":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, p0, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->mVendorReqAppsList:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 297
    iget-object v1, p0, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->mMdmPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 298
    return-object v0
.end method

.method public run()V
    .locals 1

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->mNewProfile:Z

    if-eqz v0, :cond_0

    .line 94
    invoke-direct {p0}, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->disableNfcBluetoothSharing()V

    .line 96
    :cond_0
    invoke-direct {p0}, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->deleteNonRequiredApps()V

    .line 97
    return-void
.end method
