.class Lcom/android/managedprovisioning/task/DownloadPackageTask$1;
.super Landroid/content/BroadcastReceiver;
.source "DownloadPackageTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/managedprovisioning/task/DownloadPackageTask;->createDownloadReceiver()Landroid/content/BroadcastReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/managedprovisioning/task/DownloadPackageTask;


# direct methods
.method constructor <init>(Lcom/android/managedprovisioning/task/DownloadPackageTask;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/android/managedprovisioning/task/DownloadPackageTask$1;->this$0:Lcom/android/managedprovisioning/task/DownloadPackageTask;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 98
    const-string v6, "android.intent.action.DOWNLOAD_COMPLETE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 99
    new-instance v4, Landroid/app/DownloadManager$Query;

    invoke-direct {v4}, Landroid/app/DownloadManager$Query;-><init>()V

    .line 100
    .local v4, "q":Landroid/app/DownloadManager$Query;
    const/4 v6, 0x1

    new-array v6, v6, [J

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/android/managedprovisioning/task/DownloadPackageTask$1;->this$0:Lcom/android/managedprovisioning/task/DownloadPackageTask;

    # getter for: Lcom/android/managedprovisioning/task/DownloadPackageTask;->mDownloadId:J
    invoke-static {v8}, Lcom/android/managedprovisioning/task/DownloadPackageTask;->access$000(Lcom/android/managedprovisioning/task/DownloadPackageTask;)J

    move-result-wide v8

    aput-wide v8, v6, v7

    invoke-virtual {v4, v6}, Landroid/app/DownloadManager$Query;->setFilterById([J)Landroid/app/DownloadManager$Query;

    .line 101
    iget-object v6, p0, Lcom/android/managedprovisioning/task/DownloadPackageTask$1;->this$0:Lcom/android/managedprovisioning/task/DownloadPackageTask;

    # getter for: Lcom/android/managedprovisioning/task/DownloadPackageTask;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/android/managedprovisioning/task/DownloadPackageTask;->access$100(Lcom/android/managedprovisioning/task/DownloadPackageTask;)Landroid/content/Context;

    move-result-object v6

    const-string v7, "download"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/DownloadManager;

    .line 103
    .local v2, "dm":Landroid/app/DownloadManager;
    invoke-virtual {v2, v4}, Landroid/app/DownloadManager;->query(Landroid/app/DownloadManager$Query;)Landroid/database/Cursor;

    move-result-object v0

    .line 104
    .local v0, "c":Landroid/database/Cursor;
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 105
    const-string v6, "status"

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 106
    .local v1, "columnIndex":I
    const/16 v6, 0x8

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    if-ne v6, v7, :cond_1

    .line 107
    const-string v6, "local_filename"

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 109
    .local v3, "location":Ljava/lang/String;
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 110
    iget-object v6, p0, Lcom/android/managedprovisioning/task/DownloadPackageTask$1;->this$0:Lcom/android/managedprovisioning/task/DownloadPackageTask;

    # invokes: Lcom/android/managedprovisioning/task/DownloadPackageTask;->onDownloadSuccess(Ljava/lang/String;)V
    invoke-static {v6, v3}, Lcom/android/managedprovisioning/task/DownloadPackageTask;->access$200(Lcom/android/managedprovisioning/task/DownloadPackageTask;Ljava/lang/String;)V

    .line 118
    .end local v0    # "c":Landroid/database/Cursor;
    .end local v1    # "columnIndex":I
    .end local v2    # "dm":Landroid/app/DownloadManager;
    .end local v3    # "location":Ljava/lang/String;
    .end local v4    # "q":Landroid/app/DownloadManager$Query;
    :cond_0
    :goto_0
    return-void

    .line 111
    .restart local v0    # "c":Landroid/database/Cursor;
    .restart local v1    # "columnIndex":I
    .restart local v2    # "dm":Landroid/app/DownloadManager;
    .restart local v4    # "q":Landroid/app/DownloadManager$Query;
    :cond_1
    const/16 v6, 0x10

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    if-ne v6, v7, :cond_0

    .line 112
    const-string v6, "reason"

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 113
    .local v5, "reason":I
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 114
    iget-object v6, p0, Lcom/android/managedprovisioning/task/DownloadPackageTask$1;->this$0:Lcom/android/managedprovisioning/task/DownloadPackageTask;

    # invokes: Lcom/android/managedprovisioning/task/DownloadPackageTask;->onDownloadFail(I)V
    invoke-static {v6, v5}, Lcom/android/managedprovisioning/task/DownloadPackageTask;->access$300(Lcom/android/managedprovisioning/task/DownloadPackageTask;I)V

    goto :goto_0
.end method
