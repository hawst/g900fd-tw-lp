.class Lcom/android/managedprovisioning/task/InstallPackageTask$PackageInstallObserver;
.super Landroid/content/pm/IPackageInstallObserver$Stub;
.source "InstallPackageTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/managedprovisioning/task/InstallPackageTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PackageInstallObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/managedprovisioning/task/InstallPackageTask;


# direct methods
.method private constructor <init>(Lcom/android/managedprovisioning/task/InstallPackageTask;)V
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lcom/android/managedprovisioning/task/InstallPackageTask$PackageInstallObserver;->this$0:Lcom/android/managedprovisioning/task/InstallPackageTask;

    invoke-direct {p0}, Landroid/content/pm/IPackageInstallObserver$Stub;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/managedprovisioning/task/InstallPackageTask;Lcom/android/managedprovisioning/task/InstallPackageTask$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/android/managedprovisioning/task/InstallPackageTask;
    .param p2, "x1"    # Lcom/android/managedprovisioning/task/InstallPackageTask$1;

    .prologue
    .line 115
    invoke-direct {p0, p1}, Lcom/android/managedprovisioning/task/InstallPackageTask$PackageInstallObserver;-><init>(Lcom/android/managedprovisioning/task/InstallPackageTask;)V

    return-void
.end method


# virtual methods
.method public packageInstalled(Ljava/lang/String;I)V
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "returnCode"    # I

    .prologue
    const/4 v3, 0x1

    .line 119
    iget-object v0, p0, Lcom/android/managedprovisioning/task/InstallPackageTask$PackageInstallObserver;->this$0:Lcom/android/managedprovisioning/task/InstallPackageTask;

    # getter for: Lcom/android/managedprovisioning/task/InstallPackageTask;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/android/managedprovisioning/task/InstallPackageTask;->access$100(Lcom/android/managedprovisioning/task/InstallPackageTask;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "package_verifier_enable"

    iget-object v2, p0, Lcom/android/managedprovisioning/task/InstallPackageTask$PackageInstallObserver;->this$0:Lcom/android/managedprovisioning/task/InstallPackageTask;

    # getter for: Lcom/android/managedprovisioning/task/InstallPackageTask;->mPackageVerifierEnable:I
    invoke-static {v2}, Lcom/android/managedprovisioning/task/InstallPackageTask;->access$200(Lcom/android/managedprovisioning/task/InstallPackageTask;)I

    move-result v2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 122
    if-ne p2, v3, :cond_0

    iget-object v0, p0, Lcom/android/managedprovisioning/task/InstallPackageTask$PackageInstallObserver;->this$0:Lcom/android/managedprovisioning/task/InstallPackageTask;

    # getter for: Lcom/android/managedprovisioning/task/InstallPackageTask;->mPackageName:Ljava/lang/String;
    invoke-static {v0}, Lcom/android/managedprovisioning/task/InstallPackageTask;->access$300(Lcom/android/managedprovisioning/task/InstallPackageTask;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 124
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Package "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/managedprovisioning/task/InstallPackageTask$PackageInstallObserver;->this$0:Lcom/android/managedprovisioning/task/InstallPackageTask;

    # getter for: Lcom/android/managedprovisioning/task/InstallPackageTask;->mPackageName:Ljava/lang/String;
    invoke-static {v1}, Lcom/android/managedprovisioning/task/InstallPackageTask;->access$300(Lcom/android/managedprovisioning/task/InstallPackageTask;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is succesfully installed."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 126
    iget-object v0, p0, Lcom/android/managedprovisioning/task/InstallPackageTask$PackageInstallObserver;->this$0:Lcom/android/managedprovisioning/task/InstallPackageTask;

    # getter for: Lcom/android/managedprovisioning/task/InstallPackageTask;->mCallback:Lcom/android/managedprovisioning/task/InstallPackageTask$Callback;
    invoke-static {v0}, Lcom/android/managedprovisioning/task/InstallPackageTask;->access$400(Lcom/android/managedprovisioning/task/InstallPackageTask;)Lcom/android/managedprovisioning/task/InstallPackageTask$Callback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/managedprovisioning/task/InstallPackageTask$Callback;->onSuccess()V

    .line 141
    :goto_0
    return-void

    .line 128
    :cond_0
    const/16 v0, -0x19

    if-ne p2, v0, :cond_1

    .line 129
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Current version of "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/managedprovisioning/task/InstallPackageTask$PackageInstallObserver;->this$0:Lcom/android/managedprovisioning/task/InstallPackageTask;

    # getter for: Lcom/android/managedprovisioning/task/InstallPackageTask;->mPackageName:Ljava/lang/String;
    invoke-static {v1}, Lcom/android/managedprovisioning/task/InstallPackageTask;->access$300(Lcom/android/managedprovisioning/task/InstallPackageTask;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " higher than the version to be installed."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 131
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Package "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/managedprovisioning/task/InstallPackageTask$PackageInstallObserver;->this$0:Lcom/android/managedprovisioning/task/InstallPackageTask;

    # getter for: Lcom/android/managedprovisioning/task/InstallPackageTask;->mPackageName:Ljava/lang/String;
    invoke-static {v1}, Lcom/android/managedprovisioning/task/InstallPackageTask;->access$300(Lcom/android/managedprovisioning/task/InstallPackageTask;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " was not reinstalled."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 132
    iget-object v0, p0, Lcom/android/managedprovisioning/task/InstallPackageTask$PackageInstallObserver;->this$0:Lcom/android/managedprovisioning/task/InstallPackageTask;

    # getter for: Lcom/android/managedprovisioning/task/InstallPackageTask;->mCallback:Lcom/android/managedprovisioning/task/InstallPackageTask$Callback;
    invoke-static {v0}, Lcom/android/managedprovisioning/task/InstallPackageTask;->access$400(Lcom/android/managedprovisioning/task/InstallPackageTask;)Lcom/android/managedprovisioning/task/InstallPackageTask$Callback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/managedprovisioning/task/InstallPackageTask$Callback;->onSuccess()V

    goto :goto_0

    .line 136
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Installing package "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/managedprovisioning/task/InstallPackageTask$PackageInstallObserver;->this$0:Lcom/android/managedprovisioning/task/InstallPackageTask;

    # getter for: Lcom/android/managedprovisioning/task/InstallPackageTask;->mPackageName:Ljava/lang/String;
    invoke-static {v1}, Lcom/android/managedprovisioning/task/InstallPackageTask;->access$300(Lcom/android/managedprovisioning/task/InstallPackageTask;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " failed."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 137
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Errorcode returned by IPackageInstallObserver = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 139
    iget-object v0, p0, Lcom/android/managedprovisioning/task/InstallPackageTask$PackageInstallObserver;->this$0:Lcom/android/managedprovisioning/task/InstallPackageTask;

    # getter for: Lcom/android/managedprovisioning/task/InstallPackageTask;->mCallback:Lcom/android/managedprovisioning/task/InstallPackageTask$Callback;
    invoke-static {v0}, Lcom/android/managedprovisioning/task/InstallPackageTask;->access$400(Lcom/android/managedprovisioning/task/InstallPackageTask;)Lcom/android/managedprovisioning/task/InstallPackageTask$Callback;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/android/managedprovisioning/task/InstallPackageTask$Callback;->onError(I)V

    goto/16 :goto_0
.end method
