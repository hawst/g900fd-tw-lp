.class Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask$PackageDeleteObserver;
.super Landroid/content/pm/IPackageDeleteObserver$Stub;
.source "DeleteNonRequiredAppsTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PackageDeleteObserver"
.end annotation


# instance fields
.field private final mPackageCount:Ljava/util/concurrent/atomic/AtomicInteger;

.field final synthetic this$0:Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;


# direct methods
.method public constructor <init>(Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;I)V
    .locals 2
    .param p2, "packageCount"    # I

    .prologue
    .line 308
    iput-object p1, p0, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask$PackageDeleteObserver;->this$0:Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;

    invoke-direct {p0}, Landroid/content/pm/IPackageDeleteObserver$Stub;-><init>()V

    .line 306
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask$PackageDeleteObserver;->mPackageCount:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 309
    iget-object v0, p0, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask$PackageDeleteObserver;->mPackageCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0, p2}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 310
    return-void
.end method


# virtual methods
.method public packageDeleted(Ljava/lang/String;I)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "returnCode"    # I

    .prologue
    .line 314
    const/4 v1, 0x1

    if-eq p2, v1, :cond_0

    .line 315
    const-string v1, "Could not finish the provisioning: package deletion failed"

    invoke-static {v1}, Lcom/android/managedprovisioning/ProvisionLogger;->logw(Ljava/lang/String;)V

    .line 317
    iget-object v1, p0, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask$PackageDeleteObserver;->this$0:Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;

    # getter for: Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->mCallback:Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask$Callback;
    invoke-static {v1}, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->access$000(Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;)Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask$Callback;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask$Callback;->onError()V

    .line 319
    :cond_0
    iget-object v1, p0, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask$PackageDeleteObserver;->mPackageCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    .line 320
    .local v0, "currentPackageCount":I
    if-nez v0, :cond_1

    .line 321
    const-string v1, "All non-required system apps have been uninstalled."

    invoke-static {v1}, Lcom/android/managedprovisioning/ProvisionLogger;->logi(Ljava/lang/String;)V

    .line 322
    iget-object v1, p0, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask$PackageDeleteObserver;->this$0:Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;

    # getter for: Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->mCallback:Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask$Callback;
    invoke-static {v1}, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->access$000(Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;)Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask$Callback;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask$Callback;->onSuccess()V

    .line 324
    :cond_1
    return-void
.end method
