.class public Lcom/android/managedprovisioning/task/DownloadPackageTask;
.super Ljava/lang/Object;
.source "DownloadPackageTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/managedprovisioning/task/DownloadPackageTask$Callback;
    }
.end annotation


# instance fields
.field private final mCallback:Lcom/android/managedprovisioning/task/DownloadPackageTask$Callback;

.field private final mContext:Landroid/content/Context;

.field private mDoneDownloading:Z

.field private mDownloadId:J

.field private final mDownloadLocationFrom:Ljava/lang/String;

.field private mDownloadLocationTo:Ljava/lang/String;

.field private final mHash:[B

.field private final mHttpCookieHeader:Ljava/lang/String;

.field private mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;[BLjava/lang/String;Lcom/android/managedprovisioning/task/DownloadPackageTask$Callback;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "downloadLocation"    # Ljava/lang/String;
    .param p3, "hash"    # [B
    .param p4, "httpCookieHeader"    # Ljava/lang/String;
    .param p5, "callback"    # Lcom/android/managedprovisioning/task/DownloadPackageTask$Callback;

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput-object p5, p0, Lcom/android/managedprovisioning/task/DownloadPackageTask;->mCallback:Lcom/android/managedprovisioning/task/DownloadPackageTask$Callback;

    .line 67
    iput-object p1, p0, Lcom/android/managedprovisioning/task/DownloadPackageTask;->mContext:Landroid/content/Context;

    .line 68
    iput-object p2, p0, Lcom/android/managedprovisioning/task/DownloadPackageTask;->mDownloadLocationFrom:Ljava/lang/String;

    .line 69
    iput-object p3, p0, Lcom/android/managedprovisioning/task/DownloadPackageTask;->mHash:[B

    .line 70
    iput-object p4, p0, Lcom/android/managedprovisioning/task/DownloadPackageTask;->mHttpCookieHeader:Ljava/lang/String;

    .line 71
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/managedprovisioning/task/DownloadPackageTask;->mDoneDownloading:Z

    .line 72
    return-void
.end method

.method static synthetic access$000(Lcom/android/managedprovisioning/task/DownloadPackageTask;)J
    .locals 2
    .param p0, "x0"    # Lcom/android/managedprovisioning/task/DownloadPackageTask;

    .prologue
    .line 46
    iget-wide v0, p0, Lcom/android/managedprovisioning/task/DownloadPackageTask;->mDownloadId:J

    return-wide v0
.end method

.method static synthetic access$100(Lcom/android/managedprovisioning/task/DownloadPackageTask;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/android/managedprovisioning/task/DownloadPackageTask;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/android/managedprovisioning/task/DownloadPackageTask;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/managedprovisioning/task/DownloadPackageTask;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/managedprovisioning/task/DownloadPackageTask;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/android/managedprovisioning/task/DownloadPackageTask;->onDownloadSuccess(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/android/managedprovisioning/task/DownloadPackageTask;I)V
    .locals 0
    .param p0, "x0"    # Lcom/android/managedprovisioning/task/DownloadPackageTask;
    .param p1, "x1"    # I

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/android/managedprovisioning/task/DownloadPackageTask;->onDownloadFail(I)V

    return-void
.end method

.method private computeHash(Ljava/lang/String;)[B
    .locals 9
    .param p1, "fileLocation"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x2

    .line 164
    const/4 v2, 0x0

    .line 166
    .local v2, "fis":Ljava/io/InputStream;
    const/4 v4, 0x0

    .line 168
    .local v4, "hash":[B
    :try_start_0
    const-string v7, "SHA-1"

    invoke-static {v7}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v5

    .line 175
    .local v5, "md":Ljava/security/MessageDigest;
    :try_start_1
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 177
    .end local v2    # "fis":Ljava/io/InputStream;
    .local v3, "fis":Ljava/io/InputStream;
    const/16 v7, 0x100

    :try_start_2
    new-array v0, v7, [B

    .line 178
    .local v0, "buffer":[B
    const/4 v6, 0x0

    .line 179
    .local v6, "n":I
    :cond_0
    :goto_0
    const/4 v7, -0x1

    if-eq v6, v7, :cond_2

    .line 180
    invoke-virtual {v3, v0}, Ljava/io/InputStream;->read([B)I

    move-result v6

    .line 181
    if-lez v6, :cond_0

    .line 182
    const/4 v7, 0x0

    invoke-virtual {v5, v0, v7, v6}, Ljava/security/MessageDigest;->update([BII)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 186
    .end local v0    # "buffer":[B
    .end local v6    # "n":I
    :catch_0
    move-exception v1

    move-object v2, v3

    .line 187
    .end local v3    # "fis":Ljava/io/InputStream;
    .local v1, "e":Ljava/io/IOException;
    .restart local v2    # "fis":Ljava/io/InputStream;
    :goto_1
    :try_start_3
    const-string v7, "IO error."

    invoke-static {v7, v1}, Lcom/android/managedprovisioning/ProvisionLogger;->loge(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 188
    iget-object v7, p0, Lcom/android/managedprovisioning/task/DownloadPackageTask;->mCallback:Lcom/android/managedprovisioning/task/DownloadPackageTask$Callback;

    const/4 v8, 0x2

    invoke-virtual {v7, v8}, Lcom/android/managedprovisioning/task/DownloadPackageTask$Callback;->onError(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 192
    if-eqz v2, :cond_1

    .line 193
    :try_start_4
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .end local v1    # "e":Ljava/io/IOException;
    :cond_1
    :goto_2
    move-object v7, v4

    .line 199
    .end local v5    # "md":Ljava/security/MessageDigest;
    :goto_3
    return-object v7

    .line 169
    :catch_1
    move-exception v1

    .line 170
    .local v1, "e":Ljava/security/NoSuchAlgorithmException;
    const-string v7, "Hashing algorithm SHA-1 not supported."

    invoke-static {v7, v1}, Lcom/android/managedprovisioning/ProvisionLogger;->loge(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 171
    iget-object v7, p0, Lcom/android/managedprovisioning/task/DownloadPackageTask;->mCallback:Lcom/android/managedprovisioning/task/DownloadPackageTask$Callback;

    invoke-virtual {v7, v8}, Lcom/android/managedprovisioning/task/DownloadPackageTask$Callback;->onError(I)V

    .line 172
    const/4 v7, 0x0

    goto :goto_3

    .line 185
    .end local v1    # "e":Ljava/security/NoSuchAlgorithmException;
    .end local v2    # "fis":Ljava/io/InputStream;
    .restart local v0    # "buffer":[B
    .restart local v3    # "fis":Ljava/io/InputStream;
    .restart local v5    # "md":Ljava/security/MessageDigest;
    .restart local v6    # "n":I
    :cond_2
    :try_start_5
    invoke-virtual {v5}, Ljava/security/MessageDigest;->digest()[B
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result-object v4

    .line 192
    if-eqz v3, :cond_3

    .line 193
    :try_start_6
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    :cond_3
    move-object v2, v3

    .line 197
    .end local v3    # "fis":Ljava/io/InputStream;
    .restart local v2    # "fis":Ljava/io/InputStream;
    goto :goto_2

    .line 195
    .end local v2    # "fis":Ljava/io/InputStream;
    .restart local v3    # "fis":Ljava/io/InputStream;
    :catch_2
    move-exception v7

    move-object v2, v3

    .line 198
    .end local v3    # "fis":Ljava/io/InputStream;
    .restart local v2    # "fis":Ljava/io/InputStream;
    goto :goto_2

    .line 191
    .end local v0    # "buffer":[B
    .end local v6    # "n":I
    :catchall_0
    move-exception v7

    .line 192
    :goto_4
    if-eqz v2, :cond_4

    .line 193
    :try_start_7
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    .line 197
    :cond_4
    :goto_5
    throw v7

    .line 195
    .local v1, "e":Ljava/io/IOException;
    :catch_3
    move-exception v7

    goto :goto_2

    .end local v1    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v8

    goto :goto_5

    .line 191
    .end local v2    # "fis":Ljava/io/InputStream;
    .restart local v3    # "fis":Ljava/io/InputStream;
    :catchall_1
    move-exception v7

    move-object v2, v3

    .end local v3    # "fis":Ljava/io/InputStream;
    .restart local v2    # "fis":Ljava/io/InputStream;
    goto :goto_4

    .line 186
    :catch_5
    move-exception v1

    goto :goto_1
.end method

.method private createDownloadReceiver()Landroid/content/BroadcastReceiver;
    .locals 1

    .prologue
    .line 95
    new-instance v0, Lcom/android/managedprovisioning/task/DownloadPackageTask$1;

    invoke-direct {v0, p0}, Lcom/android/managedprovisioning/task/DownloadPackageTask$1;-><init>(Lcom/android/managedprovisioning/task/DownloadPackageTask;)V

    return-object v0
.end method

.method private onDownloadFail(I)V
    .locals 2
    .param p1, "errorCode"    # I

    .prologue
    .line 157
    const-string v0, "Downloading package failed."

    invoke-static {v0}, Lcom/android/managedprovisioning/ProvisionLogger;->loge(Ljava/lang/String;)V

    .line 158
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "COLUMN_REASON in DownloadManager response has value: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/managedprovisioning/ProvisionLogger;->loge(Ljava/lang/String;)V

    .line 160
    iget-object v0, p0, Lcom/android/managedprovisioning/task/DownloadPackageTask;->mCallback:Lcom/android/managedprovisioning/task/DownloadPackageTask$Callback;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/managedprovisioning/task/DownloadPackageTask$Callback;->onError(I)V

    .line 161
    return-void
.end method

.method private onDownloadSuccess(Ljava/lang/String;)V
    .locals 3
    .param p1, "location"    # Ljava/lang/String;

    .prologue
    .line 123
    iget-boolean v1, p0, Lcom/android/managedprovisioning/task/DownloadPackageTask;->mDoneDownloading:Z

    if-eqz v1, :cond_1

    .line 154
    :cond_0
    :goto_0
    return-void

    .line 127
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/managedprovisioning/task/DownloadPackageTask;->mDoneDownloading:Z

    .line 130
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Downloaded succesfully to: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 133
    invoke-direct {p0, p1}, Lcom/android/managedprovisioning/task/DownloadPackageTask;->computeHash(Ljava/lang/String;)[B

    move-result-object v0

    .line 134
    .local v0, "hash":[B
    if-eqz v0, :cond_0

    .line 140
    iget-object v1, p0, Lcom/android/managedprovisioning/task/DownloadPackageTask;->mHash:[B

    invoke-static {v1, v0}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 141
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SHA-1-hashes matched, both are "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0, v0}, Lcom/android/managedprovisioning/task/DownloadPackageTask;->byteArrayToString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 143
    iput-object p1, p0, Lcom/android/managedprovisioning/task/DownloadPackageTask;->mDownloadLocationTo:Ljava/lang/String;

    .line 144
    iget-object v1, p0, Lcom/android/managedprovisioning/task/DownloadPackageTask;->mCallback:Lcom/android/managedprovisioning/task/DownloadPackageTask$Callback;

    invoke-virtual {v1}, Lcom/android/managedprovisioning/task/DownloadPackageTask$Callback;->onSuccess()V

    goto :goto_0

    .line 146
    :cond_2
    const-string v1, "SHA-1-hash of downloaded file does not match given hash."

    invoke-static {v1}, Lcom/android/managedprovisioning/ProvisionLogger;->loge(Ljava/lang/String;)V

    .line 147
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SHA-1-hash of downloaded file: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0, v0}, Lcom/android/managedprovisioning/task/DownloadPackageTask;->byteArrayToString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/managedprovisioning/ProvisionLogger;->loge(Ljava/lang/String;)V

    .line 149
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SHA-1-hash provided by programmer: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/managedprovisioning/task/DownloadPackageTask;->mHash:[B

    invoke-virtual {p0, v2}, Lcom/android/managedprovisioning/task/DownloadPackageTask;->byteArrayToString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/managedprovisioning/ProvisionLogger;->loge(Ljava/lang/String;)V

    .line 152
    iget-object v1, p0, Lcom/android/managedprovisioning/task/DownloadPackageTask;->mCallback:Lcom/android/managedprovisioning/task/DownloadPackageTask$Callback;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/managedprovisioning/task/DownloadPackageTask$Callback;->onError(I)V

    goto/16 :goto_0
.end method


# virtual methods
.method byteArrayToString([B)Ljava/lang/String;
    .locals 1
    .param p1, "ba"    # [B

    .prologue
    .line 227
    const/16 v0, 0xb

    invoke-static {p1, v0}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public cleanUp()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 207
    iget-object v3, p0, Lcom/android/managedprovisioning/task/DownloadPackageTask;->mReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v3, :cond_0

    .line 209
    iget-object v3, p0, Lcom/android/managedprovisioning/task/DownloadPackageTask;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/managedprovisioning/task/DownloadPackageTask;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v3, v4}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 210
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/android/managedprovisioning/task/DownloadPackageTask;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 214
    :cond_0
    iget-object v3, p0, Lcom/android/managedprovisioning/task/DownloadPackageTask;->mContext:Landroid/content/Context;

    const-string v4, "download"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/DownloadManager;

    .line 216
    .local v0, "dm":Landroid/app/DownloadManager;
    new-array v3, v1, [J

    iget-wide v4, p0, Lcom/android/managedprovisioning/task/DownloadPackageTask;->mDownloadId:J

    aput-wide v4, v3, v2

    invoke-virtual {v0, v3}, Landroid/app/DownloadManager;->remove([J)I

    move-result v3

    if-ne v3, v1, :cond_1

    .line 217
    .local v1, "removeSuccess":Z
    :goto_0
    if-eqz v1, :cond_2

    .line 218
    const-string v2, "Successfully removed the device owner installer file."

    invoke-static {v2}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 223
    :goto_1
    return-void

    .end local v1    # "removeSuccess":Z
    :cond_1
    move v1, v2

    .line 216
    goto :goto_0

    .line 220
    .restart local v1    # "removeSuccess":Z
    :cond_2
    const-string v2, "Could not remove the device owner installer file."

    invoke-static {v2}, Lcom/android/managedprovisioning/ProvisionLogger;->loge(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public getDownloadedPackageLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/android/managedprovisioning/task/DownloadPackageTask;->mDownloadLocationTo:Ljava/lang/String;

    return-object v0
.end method

.method public run()V
    .locals 6

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/android/managedprovisioning/task/DownloadPackageTask;->createDownloadReceiver()Landroid/content/BroadcastReceiver;

    move-result-object v2

    iput-object v2, p0, Lcom/android/managedprovisioning/task/DownloadPackageTask;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 80
    iget-object v2, p0, Lcom/android/managedprovisioning/task/DownloadPackageTask;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/managedprovisioning/task/DownloadPackageTask;->mReceiver:Landroid/content/BroadcastReceiver;

    new-instance v4, Landroid/content/IntentFilter;

    const-string v5, "android.intent.action.DOWNLOAD_COMPLETE"

    invoke-direct {v4, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 83
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Starting download from "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/managedprovisioning/task/DownloadPackageTask;->mDownloadLocationFrom:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 84
    iget-object v2, p0, Lcom/android/managedprovisioning/task/DownloadPackageTask;->mContext:Landroid/content/Context;

    const-string v3, "download"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/DownloadManager;

    .line 86
    .local v0, "dm":Landroid/app/DownloadManager;
    new-instance v1, Landroid/app/DownloadManager$Request;

    iget-object v2, p0, Lcom/android/managedprovisioning/task/DownloadPackageTask;->mDownloadLocationFrom:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/DownloadManager$Request;-><init>(Landroid/net/Uri;)V

    .line 87
    .local v1, "request":Landroid/app/DownloadManager$Request;
    iget-object v2, p0, Lcom/android/managedprovisioning/task/DownloadPackageTask;->mHttpCookieHeader:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 88
    const-string v2, "Cookie"

    iget-object v3, p0, Lcom/android/managedprovisioning/task/DownloadPackageTask;->mHttpCookieHeader:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/app/DownloadManager$Request;->addRequestHeader(Ljava/lang/String;Ljava/lang/String;)Landroid/app/DownloadManager$Request;

    .line 89
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Downloading with http cookie header: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/managedprovisioning/task/DownloadPackageTask;->mHttpCookieHeader:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 91
    :cond_0
    invoke-virtual {v0, v1}, Landroid/app/DownloadManager;->enqueue(Landroid/app/DownloadManager$Request;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/managedprovisioning/task/DownloadPackageTask;->mDownloadId:J

    .line 92
    return-void
.end method
