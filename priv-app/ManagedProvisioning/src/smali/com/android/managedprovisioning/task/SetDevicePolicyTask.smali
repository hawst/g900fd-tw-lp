.class public Lcom/android/managedprovisioning/task/SetDevicePolicyTask;
.super Ljava/lang/Object;
.source "SetDevicePolicyTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/managedprovisioning/task/SetDevicePolicyTask$Callback;
    }
.end annotation


# instance fields
.field private mAdminReceiver:Ljava/lang/String;

.field private final mCallback:Lcom/android/managedprovisioning/task/SetDevicePolicyTask$Callback;

.field private final mContext:Landroid/content/Context;

.field private mDevicePolicyManager:Landroid/app/admin/DevicePolicyManager;

.field private final mOwner:Ljava/lang/String;

.field private mPackageManager:Landroid/content/pm/PackageManager;

.field private final mPackageName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/android/managedprovisioning/task/SetDevicePolicyTask$Callback;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "owner"    # Ljava/lang/String;
    .param p4, "callback"    # Lcom/android/managedprovisioning/task/SetDevicePolicyTask$Callback;

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p4, p0, Lcom/android/managedprovisioning/task/SetDevicePolicyTask;->mCallback:Lcom/android/managedprovisioning/task/SetDevicePolicyTask$Callback;

    .line 48
    iput-object p1, p0, Lcom/android/managedprovisioning/task/SetDevicePolicyTask;->mContext:Landroid/content/Context;

    .line 49
    iput-object p2, p0, Lcom/android/managedprovisioning/task/SetDevicePolicyTask;->mPackageName:Ljava/lang/String;

    .line 50
    iput-object p3, p0, Lcom/android/managedprovisioning/task/SetDevicePolicyTask;->mOwner:Ljava/lang/String;

    .line 51
    iget-object v0, p0, Lcom/android/managedprovisioning/task/SetDevicePolicyTask;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/managedprovisioning/task/SetDevicePolicyTask;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 52
    iget-object v0, p0, Lcom/android/managedprovisioning/task/SetDevicePolicyTask;->mContext:Landroid/content/Context;

    const-string v1, "device_policy"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    iput-object v0, p0, Lcom/android/managedprovisioning/task/SetDevicePolicyTask;->mDevicePolicyManager:Landroid/app/admin/DevicePolicyManager;

    .line 54
    return-void
.end method

.method private enableDevicePolicyApp()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 86
    iget-object v1, p0, Lcom/android/managedprovisioning/task/SetDevicePolicyTask;->mPackageManager:Landroid/content/pm/PackageManager;

    iget-object v2, p0, Lcom/android/managedprovisioning/task/SetDevicePolicyTask;->mPackageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I

    move-result v0

    .line 88
    .local v0, "enabledSetting":I
    if-eqz v0, :cond_0

    .line 89
    iget-object v1, p0, Lcom/android/managedprovisioning/task/SetDevicePolicyTask;->mPackageManager:Landroid/content/pm/PackageManager;

    iget-object v2, p0, Lcom/android/managedprovisioning/task/SetDevicePolicyTask;->mPackageName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v3}, Landroid/content/pm/PackageManager;->setApplicationEnabledSetting(Ljava/lang/String;II)V

    .line 92
    :cond_0
    return-void
.end method

.method private isPackageInstalled()Z
    .locals 11

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 68
    :try_start_0
    iget-object v8, p0, Lcom/android/managedprovisioning/task/SetDevicePolicyTask;->mPackageManager:Landroid/content/pm/PackageManager;

    iget-object v9, p0, Lcom/android/managedprovisioning/task/SetDevicePolicyTask;->mPackageName:Ljava/lang/String;

    const/4 v10, 0x2

    invoke-virtual {v8, v9, v10}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v5

    .line 70
    .local v5, "pi":Landroid/content/pm/PackageInfo;
    iget-object v1, v5, Landroid/content/pm/PackageInfo;->receivers:[Landroid/content/pm/ActivityInfo;

    .local v1, "arr$":[Landroid/content/pm/ActivityInfo;
    array-length v4, v1

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v0, v1, v3

    .line 71
    .local v0, "ai":Landroid/content/pm/ActivityInfo;
    iget-object v8, v0, Landroid/content/pm/ActivityInfo;->permission:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    iget-object v8, v0, Landroid/content/pm/ActivityInfo;->permission:Ljava/lang/String;

    const-string v9, "android.permission.BIND_DEVICE_ADMIN"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 73
    iget-object v8, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    iput-object v8, p0, Lcom/android/managedprovisioning/task/SetDevicePolicyTask;->mAdminReceiver:Ljava/lang/String;

    .line 81
    .end local v0    # "ai":Landroid/content/pm/ActivityInfo;
    .end local v1    # "arr$":[Landroid/content/pm/ActivityInfo;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    .end local v5    # "pi":Landroid/content/pm/PackageInfo;
    :goto_1
    return v6

    .line 70
    .restart local v0    # "ai":Landroid/content/pm/ActivityInfo;
    .restart local v1    # "arr$":[Landroid/content/pm/ActivityInfo;
    .restart local v3    # "i$":I
    .restart local v4    # "len$":I
    .restart local v5    # "pi":Landroid/content/pm/PackageInfo;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 77
    .end local v0    # "ai":Landroid/content/pm/ActivityInfo;
    :cond_1
    iget-object v6, p0, Lcom/android/managedprovisioning/task/SetDevicePolicyTask;->mCallback:Lcom/android/managedprovisioning/task/SetDevicePolicyTask$Callback;

    const/4 v8, 0x1

    invoke-virtual {v6, v8}, Lcom/android/managedprovisioning/task/SetDevicePolicyTask$Callback;->onError(I)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move v6, v7

    .line 78
    goto :goto_1

    .line 79
    .end local v1    # "arr$":[Landroid/content/pm/ActivityInfo;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    .end local v5    # "pi":Landroid/content/pm/PackageInfo;
    :catch_0
    move-exception v2

    .line 80
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    iget-object v6, p0, Lcom/android/managedprovisioning/task/SetDevicePolicyTask;->mCallback:Lcom/android/managedprovisioning/task/SetDevicePolicyTask$Callback;

    invoke-virtual {v6, v7}, Lcom/android/managedprovisioning/task/SetDevicePolicyTask$Callback;->onError(I)V

    move v6, v7

    .line 81
    goto :goto_1
.end method


# virtual methods
.method public run()V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/android/managedprovisioning/task/SetDevicePolicyTask;->isPackageInstalled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    invoke-direct {p0}, Lcom/android/managedprovisioning/task/SetDevicePolicyTask;->enableDevicePolicyApp()V

    .line 60
    invoke-virtual {p0}, Lcom/android/managedprovisioning/task/SetDevicePolicyTask;->setActiveAdmin()V

    .line 61
    invoke-virtual {p0}, Lcom/android/managedprovisioning/task/SetDevicePolicyTask;->setDeviceOwner()V

    .line 62
    iget-object v0, p0, Lcom/android/managedprovisioning/task/SetDevicePolicyTask;->mCallback:Lcom/android/managedprovisioning/task/SetDevicePolicyTask$Callback;

    invoke-virtual {v0}, Lcom/android/managedprovisioning/task/SetDevicePolicyTask$Callback;->onSuccess()V

    .line 64
    :cond_0
    return-void
.end method

.method public setActiveAdmin()V
    .locals 3

    .prologue
    .line 95
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Setting "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/managedprovisioning/task/SetDevicePolicyTask;->mPackageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " as active admin."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 96
    new-instance v0, Landroid/content/ComponentName;

    iget-object v1, p0, Lcom/android/managedprovisioning/task/SetDevicePolicyTask;->mPackageName:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/managedprovisioning/task/SetDevicePolicyTask;->mAdminReceiver:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    .local v0, "component":Landroid/content/ComponentName;
    iget-object v1, p0, Lcom/android/managedprovisioning/task/SetDevicePolicyTask;->mDevicePolicyManager:Landroid/app/admin/DevicePolicyManager;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/app/admin/DevicePolicyManager;->setActiveAdmin(Landroid/content/ComponentName;Z)V

    .line 98
    return-void
.end method

.method public setDeviceOwner()V
    .locals 3

    .prologue
    .line 101
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Setting "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/managedprovisioning/task/SetDevicePolicyTask;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " as device owner "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/managedprovisioning/task/SetDevicePolicyTask;->mOwner:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 102
    iget-object v0, p0, Lcom/android/managedprovisioning/task/SetDevicePolicyTask;->mDevicePolicyManager:Landroid/app/admin/DevicePolicyManager;

    iget-object v1, p0, Lcom/android/managedprovisioning/task/SetDevicePolicyTask;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/admin/DevicePolicyManager;->isDeviceOwner(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/android/managedprovisioning/task/SetDevicePolicyTask;->mDevicePolicyManager:Landroid/app/admin/DevicePolicyManager;

    iget-object v1, p0, Lcom/android/managedprovisioning/task/SetDevicePolicyTask;->mPackageName:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/managedprovisioning/task/SetDevicePolicyTask;->mOwner:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/app/admin/DevicePolicyManager;->setDeviceOwner(Ljava/lang/String;Ljava/lang/String;)Z

    .line 105
    :cond_0
    return-void
.end method
