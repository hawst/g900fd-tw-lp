.class public Lcom/android/managedprovisioning/BootReminder;
.super Landroid/content/BroadcastReceiver;
.source "BootReminder.java"


# static fields
.field private static final DEVICE_OWNER_INTENT_TARGET:Landroid/content/ComponentName;

.field private static final PROFILE_OWNER_INTENT_TARGET:Landroid/content/ComponentName;

.field private static final PROFILE_OWNER_PERSISTABLE_BUNDLE_EXTRAS:[Ljava/lang/String;

.field private static final PROFILE_OWNER_STRING_EXTRAS:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 43
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "android.app.extra.PROVISIONING_DEVICE_ADMIN_PACKAGE_NAME"

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/managedprovisioning/BootReminder;->PROFILE_OWNER_STRING_EXTRAS:[Ljava/lang/String;

    .line 48
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "android.app.extra.PROVISIONING_ADMIN_EXTRAS_BUNDLE"

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/managedprovisioning/BootReminder;->PROFILE_OWNER_PERSISTABLE_BUNDLE_EXTRAS:[Ljava/lang/String;

    .line 53
    sget-object v0, Lcom/android/managedprovisioning/ManagedProvisioningActivity;->ALIAS_NO_CHECK_CALLER:Landroid/content/ComponentName;

    sput-object v0, Lcom/android/managedprovisioning/BootReminder;->PROFILE_OWNER_INTENT_TARGET:Landroid/content/ComponentName;

    .line 62
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.android.managedprovisioning"

    const-string v2, "com.android.managedprovisioning.DeviceOwnerProvisioningActivity"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/android/managedprovisioning/BootReminder;->DEVICE_OWNER_INTENT_TARGET:Landroid/content/ComponentName;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method public static cancelProvisioningReminder(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 132
    invoke-static {p0}, Lcom/android/managedprovisioning/BootReminder;->getProfileOwnerIntentStore(Landroid/content/Context;)Lcom/android/managedprovisioning/IntentStore;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/managedprovisioning/IntentStore;->clear()V

    .line 133
    invoke-static {p0}, Lcom/android/managedprovisioning/BootReminder;->getDeviceOwnerIntentStore(Landroid/content/Context;)Lcom/android/managedprovisioning/IntentStore;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/managedprovisioning/IntentStore;->clear()V

    .line 134
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/android/managedprovisioning/BootReminder;->setNotification(Landroid/content/Context;Landroid/content/Intent;)V

    .line 135
    return-void
.end method

.method private static getDeviceOwnerIntentStore(Landroid/content/Context;)Lcom/android/managedprovisioning/IntentStore;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 144
    new-instance v0, Lcom/android/managedprovisioning/IntentStore;

    sget-object v1, Lcom/android/managedprovisioning/BootReminder;->DEVICE_OWNER_INTENT_TARGET:Landroid/content/ComponentName;

    const-string v2, "device-owner-provisioning-resume"

    invoke-direct {v0, p0, v1, v2}, Lcom/android/managedprovisioning/IntentStore;-><init>(Landroid/content/Context;Landroid/content/ComponentName;Ljava/lang/String;)V

    sget-object v1, Lcom/android/managedprovisioning/MessageParser;->DEVICE_OWNER_STRING_EXTRAS:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/managedprovisioning/IntentStore;->setStringKeys([Ljava/lang/String;)Lcom/android/managedprovisioning/IntentStore;

    move-result-object v0

    sget-object v1, Lcom/android/managedprovisioning/MessageParser;->DEVICE_OWNER_LONG_EXTRAS:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/managedprovisioning/IntentStore;->setLongKeys([Ljava/lang/String;)Lcom/android/managedprovisioning/IntentStore;

    move-result-object v0

    sget-object v1, Lcom/android/managedprovisioning/MessageParser;->DEVICE_OWNER_INT_EXTRAS:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/managedprovisioning/IntentStore;->setIntKeys([Ljava/lang/String;)Lcom/android/managedprovisioning/IntentStore;

    move-result-object v0

    sget-object v1, Lcom/android/managedprovisioning/MessageParser;->DEVICE_OWNER_BOOLEAN_EXTRAS:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/managedprovisioning/IntentStore;->setBooleanKeys([Ljava/lang/String;)Lcom/android/managedprovisioning/IntentStore;

    move-result-object v0

    sget-object v1, Lcom/android/managedprovisioning/MessageParser;->DEVICE_OWNER_PERSISTABLE_BUNDLE_EXTRAS:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/managedprovisioning/IntentStore;->setPersistableBundleKeys([Ljava/lang/String;)Lcom/android/managedprovisioning/IntentStore;

    move-result-object v0

    return-object v0
.end method

.method private static getProfileOwnerIntentStore(Landroid/content/Context;)Lcom/android/managedprovisioning/IntentStore;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 138
    new-instance v0, Lcom/android/managedprovisioning/IntentStore;

    sget-object v1, Lcom/android/managedprovisioning/BootReminder;->PROFILE_OWNER_INTENT_TARGET:Landroid/content/ComponentName;

    const-string v2, "profile-owner-provisioning-resume"

    invoke-direct {v0, p0, v1, v2}, Lcom/android/managedprovisioning/IntentStore;-><init>(Landroid/content/Context;Landroid/content/ComponentName;Ljava/lang/String;)V

    sget-object v1, Lcom/android/managedprovisioning/BootReminder;->PROFILE_OWNER_STRING_EXTRAS:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/managedprovisioning/IntentStore;->setStringKeys([Ljava/lang/String;)Lcom/android/managedprovisioning/IntentStore;

    move-result-object v0

    sget-object v1, Lcom/android/managedprovisioning/BootReminder;->PROFILE_OWNER_PERSISTABLE_BUNDLE_EXTRAS:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/managedprovisioning/IntentStore;->setPersistableBundleKeys([Ljava/lang/String;)Lcom/android/managedprovisioning/IntentStore;

    move-result-object v0

    return-object v0
.end method

.method private static setNotification(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x1

    .line 154
    const-string v3, "notification"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 156
    .local v0, "notificationManager":Landroid/app/NotificationManager;
    if-nez p1, :cond_0

    .line 157
    invoke-virtual {v0, v6}, Landroid/app/NotificationManager;->cancel(I)V

    .line 172
    :goto_0
    return-void

    .line 160
    :cond_0
    const/4 v3, 0x0

    const/high16 v4, 0x8000000

    invoke-static {p0, v3, p1, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 162
    .local v2, "resumePendingIntent":Landroid/app/PendingIntent;
    new-instance v3, Landroid/app/Notification$Builder;

    invoke-direct {v3, p0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v2}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v3

    const v4, 0x7f070015

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v3

    const v4, 0x7f070016

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v3

    const v4, 0x108047f

    invoke-virtual {v3, v4}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/app/Notification$Builder;->setVisibility(I)Landroid/app/Notification$Builder;

    move-result-object v3

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x1060058

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/app/Notification$Builder;->setColor(I)Landroid/app/Notification$Builder;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v1

    .line 171
    .local v1, "notify":Landroid/app/Notification$Builder;
    invoke-virtual {v1}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v3

    invoke-virtual {v0, v6, v3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0
.end method

.method public static setProvisioningReminder(Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 113
    const-string v2, "com.android.managedprovisioning.RESUME_TARGET"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 114
    .local v1, "resumeTarget":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 126
    :goto_0
    return-void

    .line 117
    :cond_0
    const-string v2, "profile_owner"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 118
    invoke-static {p0}, Lcom/android/managedprovisioning/BootReminder;->getProfileOwnerIntentStore(Landroid/content/Context;)Lcom/android/managedprovisioning/IntentStore;

    move-result-object v0

    .line 125
    .local v0, "intentStore":Lcom/android/managedprovisioning/IntentStore;
    :goto_1
    invoke-virtual {v0, p1}, Lcom/android/managedprovisioning/IntentStore;->save(Landroid/os/Bundle;)V

    goto :goto_0

    .line 119
    .end local v0    # "intentStore":Lcom/android/managedprovisioning/IntentStore;
    :cond_1
    const-string v2, "device_owner"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 120
    invoke-static {p0}, Lcom/android/managedprovisioning/BootReminder;->getDeviceOwnerIntentStore(Landroid/content/Context;)Lcom/android/managedprovisioning/IntentStore;

    move-result-object v0

    .restart local v0    # "intentStore":Lcom/android/managedprovisioning/IntentStore;
    goto :goto_1

    .line 122
    .end local v0    # "intentStore":Lcom/android/managedprovisioning/IntentStore;
    :cond_2
    const-string v2, "Unknown resume target for bootreminder."

    invoke-static {v2}, Lcom/android/managedprovisioning/ProvisionLogger;->loge(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 68
    const-string v4, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 71
    invoke-static {p1}, Lcom/android/managedprovisioning/BootReminder;->getProfileOwnerIntentStore(Landroid/content/Context;)Lcom/android/managedprovisioning/IntentStore;

    move-result-object v1

    .line 72
    .local v1, "profileOwnerIntentStore":Lcom/android/managedprovisioning/IntentStore;
    invoke-virtual {v1}, Lcom/android/managedprovisioning/IntentStore;->load()Landroid/content/Intent;

    move-result-object v3

    .line 73
    .local v3, "resumeProfileOwnerPrvIntent":Landroid/content/Intent;
    if-eqz v3, :cond_0

    .line 74
    invoke-static {}, Lcom/android/managedprovisioning/EncryptDeviceActivity;->isDeviceEncrypted()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 76
    invoke-virtual {v1}, Lcom/android/managedprovisioning/IntentStore;->clear()V

    .line 77
    invoke-static {p1, v3}, Lcom/android/managedprovisioning/BootReminder;->setNotification(Landroid/content/Context;Landroid/content/Intent;)V

    .line 82
    :cond_0
    invoke-static {p1}, Lcom/android/managedprovisioning/BootReminder;->getDeviceOwnerIntentStore(Landroid/content/Context;)Lcom/android/managedprovisioning/IntentStore;

    move-result-object v0

    .line 83
    .local v0, "deviceOwnerIntentStore":Lcom/android/managedprovisioning/IntentStore;
    invoke-virtual {v0}, Lcom/android/managedprovisioning/IntentStore;->load()Landroid/content/Intent;

    move-result-object v2

    .line 84
    .local v2, "resumeDeviceOwnerPrvIntent":Landroid/content/Intent;
    if-eqz v2, :cond_1

    .line 85
    invoke-virtual {v0}, Lcom/android/managedprovisioning/IntentStore;->clear()V

    .line 86
    const/high16 v4, 0x10000000

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 87
    invoke-virtual {p1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 90
    .end local v0    # "deviceOwnerIntentStore":Lcom/android/managedprovisioning/IntentStore;
    .end local v1    # "profileOwnerIntentStore":Lcom/android/managedprovisioning/IntentStore;
    .end local v2    # "resumeDeviceOwnerPrvIntent":Landroid/content/Intent;
    .end local v3    # "resumeProfileOwnerPrvIntent":Landroid/content/Intent;
    :cond_1
    return-void
.end method
