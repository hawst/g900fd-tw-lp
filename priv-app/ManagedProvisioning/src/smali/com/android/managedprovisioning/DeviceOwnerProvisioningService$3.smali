.class Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$3;
.super Lcom/android/managedprovisioning/task/DownloadPackageTask$Callback;
.source "DeviceOwnerProvisioningService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->startDeviceOwnerProvisioning(Lcom/android/managedprovisioning/ProvisioningParams;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;


# direct methods
.method constructor <init>(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;)V
    .locals 0

    .prologue
    .line 224
    iput-object p1, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$3;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    invoke-direct {p0}, Lcom/android/managedprovisioning/task/DownloadPackageTask$Callback;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(I)V
    .locals 2
    .param p1, "errorCode"    # I

    .prologue
    .line 235
    packed-switch p1, :pswitch_data_0

    .line 243
    iget-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$3;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    const v1, 0x7f070029

    # invokes: Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->error(I)V
    invoke-static {v0, v1}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->access$700(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;I)V

    .line 246
    :goto_0
    return-void

    .line 237
    :pswitch_0
    iget-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$3;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    const v1, 0x7f07002c

    # invokes: Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->error(I)V
    invoke-static {v0, v1}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->access$700(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;I)V

    goto :goto_0

    .line 240
    :pswitch_1
    iget-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$3;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    const v1, 0x7f07002d

    # invokes: Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->error(I)V
    invoke-static {v0, v1}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->access$700(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;I)V

    goto :goto_0

    .line 235
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onSuccess()V
    .locals 3

    .prologue
    .line 227
    iget-object v1, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$3;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    # getter for: Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mDownloadPackageTask:Lcom/android/managedprovisioning/task/DownloadPackageTask;
    invoke-static {v1}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->access$500(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;)Lcom/android/managedprovisioning/task/DownloadPackageTask;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/managedprovisioning/task/DownloadPackageTask;->getDownloadedPackageLocation()Ljava/lang/String;

    move-result-object v0

    .line 229
    .local v0, "downloadLocation":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$3;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    const v2, 0x7f070022

    # invokes: Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->progressUpdate(I)V
    invoke-static {v1, v2}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->access$400(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;I)V

    .line 230
    iget-object v1, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$3;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    # getter for: Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mInstallPackageTask:Lcom/android/managedprovisioning/task/InstallPackageTask;
    invoke-static {v1}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->access$800(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;)Lcom/android/managedprovisioning/task/InstallPackageTask;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/managedprovisioning/task/InstallPackageTask;->run(Ljava/lang/String;)V

    .line 231
    return-void
.end method
