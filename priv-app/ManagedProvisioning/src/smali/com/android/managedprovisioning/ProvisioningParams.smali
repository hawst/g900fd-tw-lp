.class public Lcom/android/managedprovisioning/ProvisioningParams;
.super Ljava/lang/Object;
.source "ProvisioningParams.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/android/managedprovisioning/ProvisioningParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public mAdminExtrasBundle:Landroid/os/PersistableBundle;

.field public mDeviceAdminPackageChecksum:[B

.field public mDeviceAdminPackageDownloadCookieHeader:Ljava/lang/String;

.field public mDeviceAdminPackageDownloadLocation:Ljava/lang/String;

.field public mDeviceAdminPackageName:Ljava/lang/String;

.field public mLocalTime:J

.field public mLocale:Ljava/util/Locale;

.field public mStartedByNfc:Z

.field public mTimeZone:Ljava/lang/String;

.field public mWifiHidden:Z

.field public mWifiPacUrl:Ljava/lang/String;

.field public mWifiPassword:Ljava/lang/String;

.field public mWifiProxyBypassHosts:Ljava/lang/String;

.field public mWifiProxyHost:Ljava/lang/String;

.field public mWifiProxyPort:I

.field public mWifiSecurityType:Ljava/lang/String;

.field public mWifiSsid:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 96
    new-instance v0, Lcom/android/managedprovisioning/ProvisioningParams$1;

    invoke-direct {v0}, Lcom/android/managedprovisioning/ProvisioningParams$1;-><init>()V

    sput-object v0, Lcom/android/managedprovisioning/ProvisioningParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/managedprovisioning/ProvisioningParams;->mLocalTime:J

    .line 39
    iput-boolean v2, p0, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiHidden:Z

    .line 43
    iput v2, p0, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiProxyPort:I

    .line 51
    new-array v0, v2, [B

    iput-object v0, p0, Lcom/android/managedprovisioning/ProvisioningParams;->mDeviceAdminPackageChecksum:[B

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x0

    return v0
.end method

.method public getDeviceAdminPackageChecksumAsString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 66
    iget-object v0, p0, Lcom/android/managedprovisioning/ProvisioningParams;->mDeviceAdminPackageChecksum:[B

    const/16 v1, 0xb

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLocaleAsString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lcom/android/managedprovisioning/ProvisioningParams;->mLocale:Ljava/util/Locale;

    if-eqz v0, :cond_0

    .line 59
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/android/managedprovisioning/ProvisioningParams;->mLocale:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/managedprovisioning/ProvisioningParams;->mLocale:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 61
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 6
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 77
    iget-object v0, p0, Lcom/android/managedprovisioning/ProvisioningParams;->mTimeZone:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 78
    iget-wide v4, p0, Lcom/android/managedprovisioning/ProvisioningParams;->mLocalTime:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 79
    iget-object v0, p0, Lcom/android/managedprovisioning/ProvisioningParams;->mLocale:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 80
    iget-object v0, p0, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiSsid:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 81
    iget-boolean v0, p0, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiHidden:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 82
    iget-object v0, p0, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiSecurityType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 83
    iget-object v0, p0, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiPassword:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 84
    iget-object v0, p0, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiProxyHost:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 85
    iget v0, p0, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiProxyPort:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 86
    iget-object v0, p0, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiProxyBypassHosts:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 87
    iget-object v0, p0, Lcom/android/managedprovisioning/ProvisioningParams;->mDeviceAdminPackageName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 88
    iget-object v0, p0, Lcom/android/managedprovisioning/ProvisioningParams;->mDeviceAdminPackageDownloadLocation:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 89
    iget-object v0, p0, Lcom/android/managedprovisioning/ProvisioningParams;->mDeviceAdminPackageDownloadCookieHeader:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 90
    iget-object v0, p0, Lcom/android/managedprovisioning/ProvisioningParams;->mDeviceAdminPackageChecksum:[B

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 91
    iget-object v0, p0, Lcom/android/managedprovisioning/ProvisioningParams;->mDeviceAdminPackageChecksum:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 92
    iget-object v0, p0, Lcom/android/managedprovisioning/ProvisioningParams;->mAdminExtrasBundle:Landroid/os/PersistableBundle;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 93
    iget-boolean v0, p0, Lcom/android/managedprovisioning/ProvisioningParams;->mStartedByNfc:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 94
    return-void

    :cond_0
    move v0, v2

    .line 81
    goto :goto_0

    :cond_1
    move v1, v2

    .line 93
    goto :goto_1
.end method
