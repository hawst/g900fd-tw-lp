.class public Lcom/android/managedprovisioning/UserConsentDialog;
.super Landroid/app/DialogFragment;
.source "UserConsentDialog.java"


# instance fields
.field private final mAdminMonitorsStringId:I

.field private final mContext:Landroid/content/Context;

.field private final mLearnMoreUri:Landroid/net/Uri;

.field private final mOnCancelRunnable:Ljava/lang/Runnable;

.field private final mOnUserConsentRunnable:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/Runnable;Ljava/lang/Runnable;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "ownerType"    # I
    .param p3, "onUserConsentRunnable"    # Ljava/lang/Runnable;
    .param p4, "onCancelRunnable"    # Ljava/lang/Runnable;

    .prologue
    .line 53
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/android/managedprovisioning/UserConsentDialog;->mContext:Landroid/content/Context;

    .line 55
    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    .line 56
    const v0, 0x7f070007

    iput v0, p0, Lcom/android/managedprovisioning/UserConsentDialog;->mAdminMonitorsStringId:I

    .line 57
    const-string v0, "https://support.google.com/android/work/answer/6090512"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/managedprovisioning/UserConsentDialog;->mLearnMoreUri:Landroid/net/Uri;

    .line 64
    :goto_0
    iput-object p3, p0, Lcom/android/managedprovisioning/UserConsentDialog;->mOnUserConsentRunnable:Ljava/lang/Runnable;

    .line 65
    iput-object p4, p0, Lcom/android/managedprovisioning/UserConsentDialog;->mOnCancelRunnable:Ljava/lang/Runnable;

    .line 66
    return-void

    .line 58
    :cond_0
    const/4 v0, 0x2

    if-ne p2, v0, :cond_1

    .line 59
    const v0, 0x7f070008

    iput v0, p0, Lcom/android/managedprovisioning/UserConsentDialog;->mAdminMonitorsStringId:I

    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/managedprovisioning/UserConsentDialog;->mLearnMoreUri:Landroid/net/Uri;

    goto :goto_0

    .line 62
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Illegal value for argument ownerType."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static synthetic access$000(Lcom/android/managedprovisioning/UserConsentDialog;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/android/managedprovisioning/UserConsentDialog;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/android/managedprovisioning/UserConsentDialog;->mLearnMoreUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/managedprovisioning/UserConsentDialog;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/android/managedprovisioning/UserConsentDialog;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/android/managedprovisioning/UserConsentDialog;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/managedprovisioning/UserConsentDialog;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/android/managedprovisioning/UserConsentDialog;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/android/managedprovisioning/UserConsentDialog;->mOnUserConsentRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/managedprovisioning/UserConsentDialog;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/android/managedprovisioning/UserConsentDialog;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/android/managedprovisioning/UserConsentDialog;->mOnCancelRunnable:Ljava/lang/Runnable;

    return-object v0
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 117
    iget-object v0, p0, Lcom/android/managedprovisioning/UserConsentDialog;->mOnCancelRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/android/managedprovisioning/UserConsentDialog;->mOnCancelRunnable:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 120
    :cond_0
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 70
    new-instance v0, Landroid/app/Dialog;

    iget-object v5, p0, Lcom/android/managedprovisioning/UserConsentDialog;->mContext:Landroid/content/Context;

    const v6, 0x7f08000f

    invoke-direct {v0, v5, v6}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 71
    .local v0, "dialog":Landroid/app/Dialog;
    const v5, 0x7f030003

    invoke-virtual {v0, v5}, Landroid/app/Dialog;->setContentView(I)V

    .line 72
    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 74
    const v5, 0x7f090004

    invoke-virtual {v0, v5}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 75
    .local v4, "text1":Landroid/widget/TextView;
    iget v5, p0, Lcom/android/managedprovisioning/UserConsentDialog;->mAdminMonitorsStringId:I

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 77
    const v5, 0x7f090006

    invoke-virtual {v0, v5}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 78
    .local v1, "linkText":Landroid/widget/TextView;
    iget-object v5, p0, Lcom/android/managedprovisioning/UserConsentDialog;->mLearnMoreUri:Landroid/net/Uri;

    if-eqz v5, :cond_0

    .line 79
    new-instance v5, Lcom/android/managedprovisioning/UserConsentDialog$1;

    invoke-direct {v5, p0}, Lcom/android/managedprovisioning/UserConsentDialog$1;-><init>(Lcom/android/managedprovisioning/UserConsentDialog;)V

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 90
    :goto_0
    const v5, 0x7f090007

    invoke-virtual {v0, v5}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    .line 91
    .local v3, "positiveButton":Landroid/widget/Button;
    new-instance v5, Lcom/android/managedprovisioning/UserConsentDialog$2;

    invoke-direct {v5, p0, v0}, Lcom/android/managedprovisioning/UserConsentDialog$2;-><init>(Lcom/android/managedprovisioning/UserConsentDialog;Landroid/app/Dialog;)V

    invoke-virtual {v3, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    const v5, 0x7f090008

    invoke-virtual {v0, v5}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 102
    .local v2, "negativeButton":Landroid/widget/Button;
    new-instance v5, Lcom/android/managedprovisioning/UserConsentDialog$3;

    invoke-direct {v5, p0, v0}, Lcom/android/managedprovisioning/UserConsentDialog$3;-><init>(Lcom/android/managedprovisioning/UserConsentDialog;Landroid/app/Dialog;)V

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 112
    return-object v0

    .line 87
    .end local v2    # "negativeButton":Landroid/widget/Button;
    .end local v3    # "positiveButton":Landroid/widget/Button;
    :cond_0
    const/16 v5, 0x8

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
