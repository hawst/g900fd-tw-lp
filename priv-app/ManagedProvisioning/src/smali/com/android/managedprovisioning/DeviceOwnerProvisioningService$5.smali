.class Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$5;
.super Lcom/android/managedprovisioning/task/SetDevicePolicyTask$Callback;
.source "DeviceOwnerProvisioningService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->startDeviceOwnerProvisioning(Lcom/android/managedprovisioning/ProvisioningParams;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;


# direct methods
.method constructor <init>(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;)V
    .locals 0

    .prologue
    .line 277
    iput-object p1, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$5;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    invoke-direct {p0}, Lcom/android/managedprovisioning/task/SetDevicePolicyTask$Callback;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(I)V
    .locals 2
    .param p1, "errorCode"    # I

    .prologue
    .line 285
    packed-switch p1, :pswitch_data_0

    .line 293
    iget-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$5;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    const v1, 0x7f070029

    # invokes: Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->error(I)V
    invoke-static {v0, v1}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->access$700(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;I)V

    .line 296
    :goto_0
    return-void

    .line 287
    :pswitch_0
    iget-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$5;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    const v1, 0x7f070030

    # invokes: Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->error(I)V
    invoke-static {v0, v1}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->access$700(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;I)V

    goto :goto_0

    .line 290
    :pswitch_1
    iget-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$5;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    const v1, 0x7f07002e

    # invokes: Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->error(I)V
    invoke-static {v0, v1}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->access$700(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;I)V

    goto :goto_0

    .line 285
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onSuccess()V
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$5;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    # getter for: Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mDeleteNonRequiredAppsTask:Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;
    invoke-static {v0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->access$900(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;)Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->run()V

    .line 281
    return-void
.end method
