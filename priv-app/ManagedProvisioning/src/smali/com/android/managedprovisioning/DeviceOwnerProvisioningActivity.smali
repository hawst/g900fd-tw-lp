.class public Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;
.super Landroid/app/Activity;
.source "DeviceOwnerProvisioningActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity$ServiceMessageReceiver;
    }
.end annotation


# instance fields
.field private mDialog:Landroid/app/Dialog;

.field private mDone:Z

.field private mOnWifiConnectedRunnable:Ljava/lang/Runnable;

.field private mProgressTextView:Landroid/widget/TextView;

.field private mServiceMessageReceiver:Landroid/content/BroadcastReceiver;

.field private mUserConsented:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 81
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->mUserConsented:Z

    .line 186
    return-void
.end method

.method static synthetic access$000(Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;Lcom/android/managedprovisioning/ProvisioningParams;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;
    .param p1, "x1"    # Lcom/android/managedprovisioning/ProvisioningParams;

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->showInterstitialAndProvision(Lcom/android/managedprovisioning/ProvisioningParams;)V

    return-void
.end method

.method static synthetic access$102(Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->mUserConsented:Z

    return p1
.end method

.method static synthetic access$200(Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;Lcom/android/managedprovisioning/ProvisioningParams;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;
    .param p1, "x1"    # Lcom/android/managedprovisioning/ProvisioningParams;

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->startDeviceOwnerProvisioningService(Lcom/android/managedprovisioning/ProvisioningParams;)V

    return-void
.end method

.method static synthetic access$300(Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;)Landroid/app/Dialog;
    .locals 1
    .param p0, "x0"    # Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->mDialog:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic access$302(Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;Landroid/app/Dialog;)Landroid/app/Dialog;
    .locals 0
    .param p0, "x0"    # Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;
    .param p1, "x1"    # Landroid/app/Dialog;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->mDialog:Landroid/app/Dialog;

    return-object p1
.end method

.method static synthetic access$400(Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->onProvisioningSuccess()V

    return-void
.end method

.method static synthetic access$500(Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->mDone:Z

    return v0
.end method

.method static synthetic access$502(Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->mDone:Z

    return p1
.end method

.method static synthetic access$600(Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;IZ)V
    .locals 0
    .param p0, "x0"    # Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->error(IZ)V

    return-void
.end method

.method static synthetic access$700(Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;
    .param p1, "x1"    # I

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->progressUpdate(I)V

    return-void
.end method

.method private error(IZ)V
    .locals 3
    .param p1, "dialogMessage"    # I
    .param p2, "resetRequired"    # Z

    .prologue
    .line 331
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f070001

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 335
    .local v0, "alertBuilder":Landroid/app/AlertDialog$Builder;
    if-eqz p2, :cond_0

    .line 336
    const v1, 0x7f070028

    new-instance v2, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity$6;

    invoke-direct {v2, p0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity$6;-><init>(Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 363
    :goto_0
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->mDialog:Landroid/app/Dialog;

    .line 364
    iget-object v1, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    .line 365
    return-void

    .line 352
    :cond_0
    const v1, 0x7f070027

    new-instance v2, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity$7;

    invoke-direct {v2, p0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity$7;-><init>(Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method

.method private onProvisioningSuccess()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 229
    invoke-virtual {p0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "device_provisioned"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 230
    invoke-virtual {p0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "user_setup_complete"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 233
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->setResult(I)V

    .line 234
    invoke-virtual {p0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->finish()V

    .line 235
    return-void
.end method

.method private progressUpdate(I)V
    .locals 1
    .param p1, "progressMessage"    # I

    .prologue
    .line 303
    iget-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->mProgressTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 304
    return-void
.end method

.method private requestEncryption(Lcom/android/managedprovisioning/MessageParser;Lcom/android/managedprovisioning/ProvisioningParams;)V
    .locals 4
    .param p1, "messageParser"    # Lcom/android/managedprovisioning/MessageParser;
    .param p2, "params"    # Lcom/android/managedprovisioning/ProvisioningParams;

    .prologue
    .line 238
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/android/managedprovisioning/EncryptDeviceActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 241
    .local v0, "encryptIntent":Landroid/content/Intent;
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 242
    .local v1, "resumeExtras":Landroid/os/Bundle;
    const-string v2, "com.android.managedprovisioning.RESUME_TARGET"

    const-string v3, "device_owner"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    invoke-virtual {p1, v1, p2}, Lcom/android/managedprovisioning/MessageParser;->addProvisioningParamsToBundle(Landroid/os/Bundle;Lcom/android/managedprovisioning/ProvisioningParams;)V

    .line 246
    const-string v2, "com.android.managedprovisioning.RESUME"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 248
    const/4 v2, 0x1

    invoke-virtual {p0, v0, v2}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 249
    return-void
.end method

.method private requestWifiPick()V
    .locals 2

    .prologue
    .line 252
    invoke-static {}, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->getWifiPickIntent()Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 253
    return-void
.end method

.method private showCancelResetDialog()V
    .locals 4

    .prologue
    .line 261
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f070024

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f070025

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f070026

    new-instance v3, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity$5;

    invoke-direct {v3, p0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity$5;-><init>(Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f070028

    new-instance v3, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity$4;

    invoke-direct {v3, p0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity$4;-><init>(Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 295
    .local v0, "alertBuilder":Landroid/app/AlertDialog$Builder;
    iget-object v1, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->mDialog:Landroid/app/Dialog;

    if-eqz v1, :cond_0

    .line 296
    iget-object v1, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    .line 298
    :cond_0
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->mDialog:Landroid/app/Dialog;

    .line 299
    iget-object v1, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    .line 300
    return-void
.end method

.method private showInterstitialAndProvision(Lcom/android/managedprovisioning/ProvisioningParams;)V
    .locals 4
    .param p1, "params"    # Lcom/android/managedprovisioning/ProvisioningParams;

    .prologue
    .line 158
    iget-boolean v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->mUserConsented:Z

    if-nez v0, :cond_0

    iget-boolean v0, p1, Lcom/android/managedprovisioning/ProvisioningParams;->mStartedByNfc:Z

    if-eqz v0, :cond_1

    .line 159
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->startDeviceOwnerProvisioningService(Lcom/android/managedprovisioning/ProvisioningParams;)V

    .line 177
    :goto_0
    return-void

    .line 163
    :cond_1
    new-instance v0, Lcom/android/managedprovisioning/UserConsentDialog;

    const/4 v1, 0x2

    new-instance v2, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity$2;

    invoke-direct {v2, p0, p1}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity$2;-><init>(Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;Lcom/android/managedprovisioning/ProvisioningParams;)V

    new-instance v3, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity$3;

    invoke-direct {v3, p0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity$3;-><init>(Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;)V

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/android/managedprovisioning/UserConsentDialog;-><init>(Landroid/content/Context;ILjava/lang/Runnable;Ljava/lang/Runnable;)V

    invoke-virtual {p0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "UserConsentDialogFragment"

    invoke-virtual {v0, v1, v2}, Lcom/android/managedprovisioning/UserConsentDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private startDeviceOwnerProvisioningService(Lcom/android/managedprovisioning/ProvisioningParams;)V
    .locals 2
    .param p1, "params"    # Lcom/android/managedprovisioning/ProvisioningParams;

    .prologue
    .line 180
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 181
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "ProvisioningParams"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 182
    invoke-virtual {p0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    .line 183
    invoke-virtual {p0, v0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 184
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 308
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 309
    if-nez p2, :cond_0

    .line 310
    const-string v0, "User canceled device encryption."

    invoke-static {v0}, Lcom/android/managedprovisioning/ProvisionLogger;->loge(Ljava/lang/String;)V

    .line 311
    invoke-virtual {p0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->finish()V

    .line 328
    :cond_0
    :goto_0
    return-void

    .line 313
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 314
    if-nez p2, :cond_2

    .line 315
    const-string v0, "User canceled wifi picking."

    invoke-static {v0}, Lcom/android/managedprovisioning/ProvisionLogger;->loge(Ljava/lang/String;)V

    .line 316
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->stopService(Landroid/content/Intent;)Z

    .line 318
    invoke-virtual {p0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->finish()V

    goto :goto_0

    .line 319
    :cond_2
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 320
    const-string v0, "Wifi request result is OK"

    invoke-static {v0}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 321
    invoke-static {p0}, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->isConnectedToWifi(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 322
    iget-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->mOnWifiConnectedRunnable:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 324
    :cond_3
    invoke-direct {p0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->requestWifiPick()V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 257
    invoke-direct {p0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->showCancelResetDialog()V

    .line 258
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v9, 0x0

    .line 85
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 87
    if-eqz p1, :cond_0

    .line 88
    const-string v7, "user_consented"

    invoke-virtual {p1, v7, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    iput-boolean v7, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->mUserConsented:Z

    .line 91
    :cond_0
    const-string v7, "Device owner provisioning activity ONCREATE"

    invoke-static {v7}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 94
    invoke-virtual {p0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "device_provisioned"

    invoke-static {v7, v8, v9}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v7

    if-eqz v7, :cond_1

    .line 95
    const-string v7, "Device already provisioned."

    invoke-static {v7}, Lcom/android/managedprovisioning/ProvisionLogger;->loge(Ljava/lang/String;)V

    .line 96
    const v7, 0x7f07002a

    invoke-direct {p0, v7, v9}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->error(IZ)V

    .line 155
    :goto_0
    return-void

    .line 100
    :cond_1
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v7

    if-eqz v7, :cond_2

    .line 101
    const-string v7, "Device owner can only be set up for USER_OWNER."

    invoke-static {v7}, Lcom/android/managedprovisioning/ProvisionLogger;->loge(Ljava/lang/String;)V

    .line 102
    const v7, 0x7f070029

    invoke-direct {p0, v7, v9}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->error(IZ)V

    goto :goto_0

    .line 107
    :cond_2
    invoke-virtual {p0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    .line 108
    .local v3, "inflater":Landroid/view/LayoutInflater;
    const v7, 0x7f030004

    const/4 v8, 0x0

    invoke-virtual {v3, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 109
    .local v0, "contentView":Landroid/view/View;
    invoke-virtual {p0, v0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->setContentView(Landroid/view/View;)V

    .line 110
    const v7, 0x7f090009

    invoke-virtual {p0, v7}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->mProgressTextView:Landroid/widget/TextView;

    .line 111
    const v7, 0x7f090003

    invoke-virtual {p0, v7}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 112
    .local v6, "titleText":Landroid/widget/TextView;
    if-eqz v6, :cond_3

    const v7, 0x7f07001e

    invoke-virtual {p0, v7}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 115
    :cond_3
    new-instance v7, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity$ServiceMessageReceiver;

    invoke-direct {v7, p0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity$ServiceMessageReceiver;-><init>(Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;)V

    iput-object v7, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->mServiceMessageReceiver:Landroid/content/BroadcastReceiver;

    .line 116
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    .line 117
    .local v2, "filter":Landroid/content/IntentFilter;
    const-string v7, "com.android.managedprovisioning.provisioning_success"

    invoke-virtual {v2, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 118
    const-string v7, "com.android.managedprovisioning.error"

    invoke-virtual {v2, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 119
    const-string v7, "com.android.managedprovisioning.progress_update"

    invoke-virtual {v2, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 120
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v7

    iget-object v8, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->mServiceMessageReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v7, v8, v2}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 124
    new-instance v5, Lcom/android/managedprovisioning/MessageParser;

    invoke-direct {v5}, Lcom/android/managedprovisioning/MessageParser;-><init>()V

    .line 126
    .local v5, "parser":Lcom/android/managedprovisioning/MessageParser;
    :try_start_0
    invoke-virtual {p0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/android/managedprovisioning/MessageParser;->parseIntent(Landroid/content/Intent;)Lcom/android/managedprovisioning/ProvisioningParams;

    move-result-object v4

    .line 127
    .local v4, "params":Lcom/android/managedprovisioning/ProvisioningParams;
    new-instance v7, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity$1;

    invoke-direct {v7, p0, v4}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity$1;-><init>(Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;Lcom/android/managedprovisioning/ProvisioningParams;)V

    iput-object v7, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->mOnWifiConnectedRunnable:Ljava/lang/Runnable;
    :try_end_0
    .catch Lcom/android/managedprovisioning/MessageParser$ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 139
    invoke-static {}, Lcom/android/managedprovisioning/EncryptDeviceActivity;->isDeviceEncrypted()Z

    move-result v7

    if-nez v7, :cond_4

    const-string v7, "persist.sys.no_req_encrypt"

    invoke-static {v7, v9}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    if-nez v7, :cond_4

    .line 141
    invoke-direct {p0, v5, v4}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->requestEncryption(Lcom/android/managedprovisioning/MessageParser;Lcom/android/managedprovisioning/ProvisioningParams;)V

    .line 142
    invoke-virtual {p0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->finish()V

    goto/16 :goto_0

    .line 132
    .end local v4    # "params":Lcom/android/managedprovisioning/ProvisioningParams;
    :catch_0
    move-exception v1

    .line 133
    .local v1, "e":Lcom/android/managedprovisioning/MessageParser$ParseException;
    const-string v7, "Could not read data from intent"

    invoke-static {v7, v1}, Lcom/android/managedprovisioning/ProvisionLogger;->loge(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 134
    invoke-virtual {v1}, Lcom/android/managedprovisioning/MessageParser$ParseException;->getErrorMessageId()I

    move-result v7

    invoke-direct {p0, v7, v9}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->error(IZ)V

    goto/16 :goto_0

    .line 148
    .end local v1    # "e":Lcom/android/managedprovisioning/MessageParser$ParseException;
    .restart local v4    # "params":Lcom/android/managedprovisioning/ProvisioningParams;
    :cond_4
    invoke-static {p0}, Lcom/android/managedprovisioning/task/AddWifiNetworkTask;->isConnectedToWifi(Landroid/content/Context;)Z

    move-result v7

    if-nez v7, :cond_5

    iget-object v7, v4, Lcom/android/managedprovisioning/ProvisioningParams;->mWifiSsid:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 149
    invoke-direct {p0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->requestWifiPick()V

    goto/16 :goto_0

    .line 154
    :cond_5
    invoke-direct {p0, v4}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->showInterstitialAndProvision(Lcom/android/managedprovisioning/ProvisioningParams;)V

    goto/16 :goto_0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 374
    const-string v0, "Device owner provisioning activity ONDESTROY"

    invoke-static {v0}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 375
    iget-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->mServiceMessageReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 376
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->mServiceMessageReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 377
    iput-object v2, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->mServiceMessageReceiver:Landroid/content/BroadcastReceiver;

    .line 379
    :cond_0
    iget-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->mDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    .line 380
    iget-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 381
    iput-object v2, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->mDialog:Landroid/app/Dialog;

    .line 383
    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 384
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 400
    const-string v0, "Device owner provisioning activity ONPAUSE"

    invoke-static {v0}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 401
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 402
    return-void
.end method

.method protected onRestart()V
    .locals 1

    .prologue
    .line 388
    const-string v0, "Device owner provisioning activity ONRESTART"

    invoke-static {v0}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 389
    invoke-super {p0}, Landroid/app/Activity;->onRestart()V

    .line 390
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 394
    const-string v0, "Device owner provisioning activity ONRESUME"

    invoke-static {v0}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 395
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 396
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 369
    const-string v0, "user_consented"

    iget-boolean v1, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->mUserConsented:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 370
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 406
    const-string v0, "Device owner provisioning activity ONSTOP"

    invoke-static {v0}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 407
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 408
    return-void
.end method
