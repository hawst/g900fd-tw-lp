.class public Lcom/android/managedprovisioning/WifiConfig;
.super Ljava/lang/Object;
.source "WifiConfig.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/managedprovisioning/WifiConfig$1;,
        Lcom/android/managedprovisioning/WifiConfig$SecurityType;
    }
.end annotation


# instance fields
.field private final mWifiManager:Landroid/net/wifi/WifiManager;


# direct methods
.method public constructor <init>(Landroid/net/wifi/WifiManager;)V
    .locals 0
    .param p1, "manager"    # Landroid/net/wifi/WifiManager;

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/android/managedprovisioning/WifiConfig;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 40
    return-void
.end method

.method private updateForProxy(Landroid/net/wifi/WifiConfiguration;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "wifiConf"    # Landroid/net/wifi/WifiConfiguration;
    .param p2, "proxyHost"    # Ljava/lang/String;
    .param p3, "proxyPort"    # I
    .param p4, "proxyBypassHosts"    # Ljava/lang/String;
    .param p5, "pacUrl"    # Ljava/lang/String;

    .prologue
    .line 126
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 136
    :goto_0
    return-void

    .line 129
    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 130
    new-instance v0, Landroid/net/ProxyInfo;

    invoke-direct {v0, p2, p3, p4}, Landroid/net/ProxyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 131
    .local v0, "proxy":Landroid/net/ProxyInfo;
    sget-object v1, Landroid/net/IpConfiguration$ProxySettings;->STATIC:Landroid/net/IpConfiguration$ProxySettings;

    invoke-virtual {p1, v1, v0}, Landroid/net/wifi/WifiConfiguration;->setProxy(Landroid/net/IpConfiguration$ProxySettings;Landroid/net/ProxyInfo;)V

    goto :goto_0

    .line 133
    .end local v0    # "proxy":Landroid/net/ProxyInfo;
    :cond_1
    new-instance v0, Landroid/net/ProxyInfo;

    invoke-direct {v0, p5}, Landroid/net/ProxyInfo;-><init>(Ljava/lang/String;)V

    .line 134
    .restart local v0    # "proxy":Landroid/net/ProxyInfo;
    sget-object v1, Landroid/net/IpConfiguration$ProxySettings;->PAC:Landroid/net/IpConfiguration$ProxySettings;

    invoke-virtual {p1, v1, v0}, Landroid/net/wifi/WifiConfiguration;->setProxy(Landroid/net/IpConfiguration$ProxySettings;Landroid/net/ProxyInfo;)V

    goto :goto_0
.end method


# virtual methods
.method public addNetwork(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)I
    .locals 8
    .param p1, "ssid"    # Ljava/lang/String;
    .param p2, "hidden"    # Z
    .param p3, "type"    # Ljava/lang/String;
    .param p4, "password"    # Ljava/lang/String;
    .param p5, "proxyHost"    # Ljava/lang/String;
    .param p6, "proxyPort"    # I
    .param p7, "proxyBypassHosts"    # Ljava/lang/String;
    .param p8, "pacUrl"    # Ljava/lang/String;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/android/managedprovisioning/WifiConfig;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/android/managedprovisioning/WifiConfig;->mWifiManager:Landroid/net/wifi/WifiManager;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    .line 51
    :cond_0
    new-instance v1, Landroid/net/wifi/WifiConfiguration;

    invoke-direct {v1}, Landroid/net/wifi/WifiConfiguration;-><init>()V

    .line 53
    .local v1, "wifiConf":Landroid/net/wifi/WifiConfiguration;
    if-eqz p3, :cond_1

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 54
    :cond_1
    sget-object v7, Lcom/android/managedprovisioning/WifiConfig$SecurityType;->NONE:Lcom/android/managedprovisioning/WifiConfig$SecurityType;

    .line 60
    .local v7, "securityType":Lcom/android/managedprovisioning/WifiConfig$SecurityType;
    :goto_0
    sget-object v0, Lcom/android/managedprovisioning/WifiConfig$SecurityType;->NONE:Lcom/android/managedprovisioning/WifiConfig$SecurityType;

    invoke-virtual {v7, v0}, Lcom/android/managedprovisioning/WifiConfig$SecurityType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 61
    sget-object v7, Lcom/android/managedprovisioning/WifiConfig$SecurityType;->WPA:Lcom/android/managedprovisioning/WifiConfig$SecurityType;

    .line 64
    :cond_2
    iput-object p1, v1, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    .line 65
    const/4 v0, 0x2

    iput v0, v1, Landroid/net/wifi/WifiConfiguration;->status:I

    .line 66
    iput-boolean p2, v1, Landroid/net/wifi/WifiConfiguration;->hiddenSSID:Z

    .line 67
    sget-object v0, Lcom/android/managedprovisioning/WifiConfig$1;->$SwitchMap$com$android$managedprovisioning$WifiConfig$SecurityType:[I

    invoke-virtual {v7}, Lcom/android/managedprovisioning/WifiConfig$SecurityType;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    :goto_1
    move-object v0, p0

    move-object v2, p5

    move v3, p6

    move-object v4, p7

    move-object/from16 v5, p8

    .line 80
    invoke-direct/range {v0 .. v5}, Lcom/android/managedprovisioning/WifiConfig;->updateForProxy(Landroid/net/wifi/WifiConfiguration;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 82
    iget-object v0, p0, Lcom/android/managedprovisioning/WifiConfig;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiManager;->addNetwork(Landroid/net/wifi/WifiConfiguration;)I

    move-result v6

    .line 84
    .local v6, "netId":I
    const/4 v0, -0x1

    if-eq v6, v0, :cond_3

    .line 86
    iget-object v0, p0, Lcom/android/managedprovisioning/WifiConfig;->mWifiManager:Landroid/net/wifi/WifiManager;

    const/4 v2, 0x1

    invoke-virtual {v0, v6, v2}, Landroid/net/wifi/WifiManager;->enableNetwork(IZ)Z

    .line 87
    iget-object v0, p0, Lcom/android/managedprovisioning/WifiConfig;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->saveConfiguration()Z

    .line 90
    :cond_3
    return v6

    .line 56
    .end local v6    # "netId":I
    .end local v7    # "securityType":Lcom/android/managedprovisioning/WifiConfig$SecurityType;
    :cond_4
    const-class v0, Lcom/android/managedprovisioning/WifiConfig$SecurityType;

    invoke-virtual {p3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v7

    check-cast v7, Lcom/android/managedprovisioning/WifiConfig$SecurityType;

    .restart local v7    # "securityType":Lcom/android/managedprovisioning/WifiConfig$SecurityType;
    goto :goto_0

    .line 69
    :pswitch_0
    iget-object v0, v1, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/BitSet;->set(I)V

    .line 70
    iget-object v0, v1, Landroid/net/wifi/WifiConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/BitSet;->set(I)V

    goto :goto_1

    .line 73
    :pswitch_1
    invoke-virtual {p0, v1, p4}, Lcom/android/managedprovisioning/WifiConfig;->updateForWPAConfiguration(Landroid/net/wifi/WifiConfiguration;Ljava/lang/String;)V

    goto :goto_1

    .line 76
    :pswitch_2
    invoke-virtual {p0, v1, p4}, Lcom/android/managedprovisioning/WifiConfig;->updateForWEPConfiguration(Landroid/net/wifi/WifiConfiguration;Ljava/lang/String;)V

    goto :goto_1

    .line 67
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected updateForWEPConfiguration(Landroid/net/wifi/WifiConfiguration;Ljava/lang/String;)V
    .locals 5
    .param p1, "wifiConf"    # Landroid/net/wifi/WifiConfiguration;
    .param p2, "password"    # Ljava/lang/String;

    .prologue
    const/16 v4, 0x22

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 108
    iget-object v1, p1, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v1, v3}, Ljava/util/BitSet;->set(I)V

    .line 109
    iget-object v1, p1, Landroid/net/wifi/WifiConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    invoke-virtual {v1, v3}, Ljava/util/BitSet;->set(I)V

    .line 110
    iget-object v1, p1, Landroid/net/wifi/WifiConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 111
    iget-object v1, p1, Landroid/net/wifi/WifiConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    invoke-virtual {v1, v3}, Ljava/util/BitSet;->set(I)V

    .line 112
    iget-object v1, p1, Landroid/net/wifi/WifiConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 113
    iget-object v1, p1, Landroid/net/wifi/WifiConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 114
    iget-object v1, p1, Landroid/net/wifi/WifiConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 115
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    .line 116
    .local v0, "length":I
    const/16 v1, 0xa

    if-eq v0, v1, :cond_0

    const/16 v1, 0x1a

    if-eq v0, v1, :cond_0

    const/16 v1, 0x3a

    if-ne v0, v1, :cond_1

    :cond_0
    const-string v1, "[0-9A-Fa-f]*"

    invoke-virtual {p2, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 117
    iget-object v1, p1, Landroid/net/wifi/WifiConfiguration;->wepKeys:[Ljava/lang/String;

    aput-object p2, v1, v3

    .line 121
    :goto_0
    iput v3, p1, Landroid/net/wifi/WifiConfiguration;->wepTxKeyIndex:I

    .line 122
    return-void

    .line 119
    :cond_1
    iget-object v1, p1, Landroid/net/wifi/WifiConfiguration;->wepKeys:[Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    goto :goto_0
.end method

.method protected updateForWPAConfiguration(Landroid/net/wifi/WifiConfiguration;Ljava/lang/String;)V
    .locals 4
    .param p1, "wifiConf"    # Landroid/net/wifi/WifiConfiguration;
    .param p2, "wifiPassword"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 94
    iget-object v0, p1, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 95
    iget-object v0, p1, Landroid/net/wifi/WifiConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    invoke-virtual {v0, v2}, Ljava/util/BitSet;->set(I)V

    .line 96
    iget-object v0, p1, Landroid/net/wifi/WifiConfiguration;->allowedProtocols:Ljava/util/BitSet;

    invoke-virtual {v0, v2}, Ljava/util/BitSet;->set(I)V

    .line 97
    iget-object v0, p1, Landroid/net/wifi/WifiConfiguration;->allowedProtocols:Ljava/util/BitSet;

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 98
    iget-object v0, p1, Landroid/net/wifi/WifiConfiguration;->allowedPairwiseCiphers:Ljava/util/BitSet;

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 99
    iget-object v0, p1, Landroid/net/wifi/WifiConfiguration;->allowedPairwiseCiphers:Ljava/util/BitSet;

    invoke-virtual {v0, v3}, Ljava/util/BitSet;->set(I)V

    .line 100
    iget-object v0, p1, Landroid/net/wifi/WifiConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    invoke-virtual {v0, v3}, Ljava/util/BitSet;->set(I)V

    .line 101
    iget-object v0, p1, Landroid/net/wifi/WifiConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 102
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 103
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    .line 105
    :cond_0
    return-void
.end method
