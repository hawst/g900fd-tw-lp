.class Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity$6;
.super Ljava/lang/Object;
.source "DeviceOwnerProvisioningActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->error(IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;


# direct methods
.method constructor <init>(Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;)V
    .locals 0

    .prologue
    .line 337
    iput-object p1, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity$6;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "id"    # I

    .prologue
    .line 341
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MASTER_CLEAR"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 342
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 343
    const-string v1, "android.intent.extra.REASON"

    const-string v2, "DeviceOwnerProvisioningActivity.error()"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 345
    iget-object v1, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity$6;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;

    invoke-virtual {v1, v0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 346
    iget-object v1, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity$6;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity$6;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;

    const-class v4, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v2}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->stopService(Landroid/content/Intent;)Z

    .line 348
    iget-object v1, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity$6;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;

    invoke-virtual {v1}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningActivity;->finish()V

    .line 349
    return-void
.end method
