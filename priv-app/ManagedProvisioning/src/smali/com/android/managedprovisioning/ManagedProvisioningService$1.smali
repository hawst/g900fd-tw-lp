.class Lcom/android/managedprovisioning/ManagedProvisioningService$1;
.super Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask$Callback;
.source "ManagedProvisioningService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/managedprovisioning/ManagedProvisioningService;->startManagedProfileProvisioning()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/managedprovisioning/ManagedProvisioningService;


# direct methods
.method constructor <init>(Lcom/android/managedprovisioning/ManagedProvisioningService;)V
    .locals 0

    .prologue
    .line 169
    iput-object p1, p0, Lcom/android/managedprovisioning/ManagedProvisioningService$1;->this$0:Lcom/android/managedprovisioning/ManagedProvisioningService;

    invoke-direct {p0}, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask$Callback;-><init>()V

    return-void
.end method


# virtual methods
.method public onError()V
    .locals 2

    .prologue
    .line 178
    iget-object v0, p0, Lcom/android/managedprovisioning/ManagedProvisioningService$1;->this$0:Lcom/android/managedprovisioning/ManagedProvisioningService;

    const-string v1, "Delete non required apps task failed."

    # invokes: Lcom/android/managedprovisioning/ManagedProvisioningService;->error(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/android/managedprovisioning/ManagedProvisioningService;->access$400(Lcom/android/managedprovisioning/ManagedProvisioningService;Ljava/lang/String;)V

    .line 179
    return-void
.end method

.method public onSuccess()V
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/android/managedprovisioning/ManagedProvisioningService$1;->this$0:Lcom/android/managedprovisioning/ManagedProvisioningService;

    # invokes: Lcom/android/managedprovisioning/ManagedProvisioningService;->setUpProfileAndFinish()V
    invoke-static {v0}, Lcom/android/managedprovisioning/ManagedProvisioningService;->access$300(Lcom/android/managedprovisioning/ManagedProvisioningService;)V

    .line 174
    return-void
.end method
