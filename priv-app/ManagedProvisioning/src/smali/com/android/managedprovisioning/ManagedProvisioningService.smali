.class public Lcom/android/managedprovisioning/ManagedProvisioningService;
.super Landroid/app/Service;
.source "ManagedProvisioningService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/managedprovisioning/ManagedProvisioningService$RunnerTask;
    }
.end annotation


# instance fields
.field private mActiveAdminComponentName:Landroid/content/ComponentName;

.field private mAdminExtrasBundle:Landroid/os/PersistableBundle;

.field private mIpm:Landroid/content/pm/IPackageManager;

.field private mManagedProfileUserInfo:Landroid/content/pm/UserInfo;

.field private mMdmPackageName:Ljava/lang/String;

.field private mUserManager:Landroid/os/UserManager;

.field private runnerTask:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Landroid/content/Intent;",
            "Ljava/lang/Object;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 86
    return-void
.end method

.method static synthetic access$000(Lcom/android/managedprovisioning/ManagedProvisioningService;Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/managedprovisioning/ManagedProvisioningService;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/android/managedprovisioning/ManagedProvisioningService;->initialize(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$100(Lcom/android/managedprovisioning/ManagedProvisioningService;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/managedprovisioning/ManagedProvisioningService;

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/android/managedprovisioning/ManagedProvisioningService;->startManagedProfileProvisioning()V

    return-void
.end method

.method static synthetic access$300(Lcom/android/managedprovisioning/ManagedProvisioningService;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/managedprovisioning/ManagedProvisioningService;

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/android/managedprovisioning/ManagedProvisioningService;->setUpProfileAndFinish()V

    return-void
.end method

.method static synthetic access$400(Lcom/android/managedprovisioning/ManagedProvisioningService;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/managedprovisioning/ManagedProvisioningService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/android/managedprovisioning/ManagedProvisioningService;->error(Ljava/lang/String;)V

    return-void
.end method

.method private cleanup()V
    .locals 2

    .prologue
    .line 342
    iget-object v0, p0, Lcom/android/managedprovisioning/ManagedProvisioningService;->mManagedProfileUserInfo:Landroid/content/pm/UserInfo;

    if-eqz v0, :cond_0

    .line 343
    const-string v0, "Removing managed profile"

    invoke-static {v0}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 344
    iget-object v0, p0, Lcom/android/managedprovisioning/ManagedProvisioningService;->mUserManager:Landroid/os/UserManager;

    iget-object v1, p0, Lcom/android/managedprovisioning/ManagedProvisioningService;->mManagedProfileUserInfo:Landroid/content/pm/UserInfo;

    iget v1, v1, Landroid/content/pm/UserInfo;->id:I

    invoke-virtual {v0, v1}, Landroid/os/UserManager;->removeUser(I)Z

    .line 346
    :cond_0
    return-void
.end method

.method private createProfile(Ljava/lang/String;)V
    .locals 3
    .param p1, "profileName"    # Ljava/lang/String;

    .prologue
    .line 200
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Creating managed profile with name "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 202
    iget-object v0, p0, Lcom/android/managedprovisioning/ManagedProvisioningService;->mUserManager:Landroid/os/UserManager;

    const/16 v1, 0x60

    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v2

    invoke-virtual {v0, p1, v1, v2}, Landroid/os/UserManager;->createProfileForUser(Ljava/lang/String;II)Landroid/content/pm/UserInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/android/managedprovisioning/ManagedProvisioningService;->mManagedProfileUserInfo:Landroid/content/pm/UserInfo;

    .line 206
    iget-object v0, p0, Lcom/android/managedprovisioning/ManagedProvisioningService;->mManagedProfileUserInfo:Landroid/content/pm/UserInfo;

    if-nez v0, :cond_0

    .line 207
    invoke-static {}, Landroid/os/UserManager;->getMaxSupportedUsers()I

    move-result v0

    iget-object v1, p0, Lcom/android/managedprovisioning/ManagedProvisioningService;->mUserManager:Landroid/os/UserManager;

    invoke-virtual {v1}, Landroid/os/UserManager;->getUserCount()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 208
    const-string v0, "Profile creation failed, maximum number of users reached."

    invoke-direct {p0, v0}, Lcom/android/managedprovisioning/ManagedProvisioningService;->error(Ljava/lang/String;)V

    .line 213
    :cond_0
    :goto_0
    return-void

    .line 210
    :cond_1
    const-string v0, "Couldn\'t create profile. Reason unknown."

    invoke-direct {p0, v0}, Lcom/android/managedprovisioning/ManagedProvisioningService;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private error(Ljava/lang/String;)V
    .locals 2
    .param p1, "logMessage"    # Ljava/lang/String;

    .prologue
    .line 330
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.managedprovisioning.error"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 331
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "ProvisioingErrorLogMessage"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 332
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 333
    invoke-direct {p0}, Lcom/android/managedprovisioning/ManagedProvisioningService;->cleanup()V

    .line 334
    invoke-virtual {p0}, Lcom/android/managedprovisioning/ManagedProvisioningService;->stopSelf()V

    .line 335
    return-void
.end method

.method private getAdminReceiverComponent(Ljava/lang/String;)Landroid/content/ComponentName;
    .locals 10
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 134
    const/4 v0, 0x0

    .line 137
    .local v0, "adminReceiverComponent":Landroid/content/ComponentName;
    :try_start_0
    invoke-virtual {p0}, Lcom/android/managedprovisioning/ManagedProvisioningService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    const/4 v9, 0x2

    invoke-virtual {v8, p1, v9}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v7

    .line 139
    .local v7, "pi":Landroid/content/pm/PackageInfo;
    iget-object v3, v7, Landroid/content/pm/PackageInfo;->receivers:[Landroid/content/pm/ActivityInfo;

    .local v3, "arr$":[Landroid/content/pm/ActivityInfo;
    array-length v6, v3
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    move-object v1, v0

    .end local v0    # "adminReceiverComponent":Landroid/content/ComponentName;
    .local v1, "adminReceiverComponent":Landroid/content/ComponentName;
    :goto_0
    if-ge v5, v6, :cond_0

    :try_start_1
    aget-object v2, v3, v5

    .line 140
    .local v2, "ai":Landroid/content/pm/ActivityInfo;
    iget-object v8, v2, Landroid/content/pm/ActivityInfo;->permission:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    iget-object v8, v2, Landroid/content/pm/ActivityInfo;->permission:Ljava/lang/String;

    const-string v9, "android.permission.BIND_DEVICE_ADMIN"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 142
    new-instance v0, Landroid/content/ComponentName;

    iget-object v8, v2, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v0, p1, v8}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 139
    .end local v1    # "adminReceiverComponent":Landroid/content/ComponentName;
    .restart local v0    # "adminReceiverComponent":Landroid/content/ComponentName;
    :goto_1
    add-int/lit8 v5, v5, 0x1

    move-object v1, v0

    .end local v0    # "adminReceiverComponent":Landroid/content/ComponentName;
    .restart local v1    # "adminReceiverComponent":Landroid/content/ComponentName;
    goto :goto_0

    .end local v2    # "ai":Landroid/content/pm/ActivityInfo;
    :cond_0
    move-object v0, v1

    .line 150
    .end local v1    # "adminReceiverComponent":Landroid/content/ComponentName;
    .end local v3    # "arr$":[Landroid/content/pm/ActivityInfo;
    .end local v5    # "i$":I
    .end local v6    # "len$":I
    .end local v7    # "pi":Landroid/content/pm/PackageInfo;
    .restart local v0    # "adminReceiverComponent":Landroid/content/ComponentName;
    :goto_2
    return-object v0

    .line 146
    :catch_0
    move-exception v4

    .line 147
    .local v4, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :goto_3
    const-string v8, "Error: The provided mobile device management package does not define a deviceadmin receiver component in its manifest."

    invoke-direct {p0, v8}, Lcom/android/managedprovisioning/ManagedProvisioningService;->error(Ljava/lang/String;)V

    goto :goto_2

    .line 146
    .end local v0    # "adminReceiverComponent":Landroid/content/ComponentName;
    .end local v4    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v1    # "adminReceiverComponent":Landroid/content/ComponentName;
    .restart local v3    # "arr$":[Landroid/content/pm/ActivityInfo;
    .restart local v5    # "i$":I
    .restart local v6    # "len$":I
    .restart local v7    # "pi":Landroid/content/pm/PackageInfo;
    :catch_1
    move-exception v4

    move-object v0, v1

    .end local v1    # "adminReceiverComponent":Landroid/content/ComponentName;
    .restart local v0    # "adminReceiverComponent":Landroid/content/ComponentName;
    goto :goto_3

    .end local v0    # "adminReceiverComponent":Landroid/content/ComponentName;
    .restart local v1    # "adminReceiverComponent":Landroid/content/ComponentName;
    .restart local v2    # "ai":Landroid/content/pm/ActivityInfo;
    :cond_1
    move-object v0, v1

    .end local v1    # "adminReceiverComponent":Landroid/content/ComponentName;
    .restart local v0    # "adminReceiverComponent":Landroid/content/ComponentName;
    goto :goto_1
.end method

.method private initialize(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 121
    const-string v0, "android.app.extra.PROVISIONING_DEVICE_ADMIN_PACKAGE_NAME"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/managedprovisioning/ManagedProvisioningService;->mMdmPackageName:Ljava/lang/String;

    .line 124
    const-string v0, "android.app.extra.PROVISIONING_ADMIN_EXTRAS_BUNDLE"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/PersistableBundle;

    iput-object v0, p0, Lcom/android/managedprovisioning/ManagedProvisioningService;->mAdminExtrasBundle:Landroid/os/PersistableBundle;

    .line 127
    iget-object v0, p0, Lcom/android/managedprovisioning/ManagedProvisioningService;->mMdmPackageName:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/android/managedprovisioning/ManagedProvisioningService;->getAdminReceiverComponent(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v0

    iput-object v0, p0, Lcom/android/managedprovisioning/ManagedProvisioningService;->mActiveAdminComponentName:Landroid/content/ComponentName;

    .line 128
    return-void
.end method

.method private installMdmOnManagedProfile()V
    .locals 5

    .prologue
    .line 236
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Installing mobile device management app "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/managedprovisioning/ManagedProvisioningService;->mMdmPackageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " on managed profile"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 240
    :try_start_0
    iget-object v2, p0, Lcom/android/managedprovisioning/ManagedProvisioningService;->mIpm:Landroid/content/pm/IPackageManager;

    iget-object v3, p0, Lcom/android/managedprovisioning/ManagedProvisioningService;->mMdmPackageName:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/managedprovisioning/ManagedProvisioningService;->mManagedProfileUserInfo:Landroid/content/pm/UserInfo;

    iget v4, v4, Landroid/content/pm/UserInfo;->id:I

    invoke-interface {v2, v3, v4}, Landroid/content/pm/IPackageManager;->installExistingPackageAsUser(Ljava/lang/String;I)I

    move-result v1

    .line 242
    .local v1, "status":I
    sparse-switch v1, :sswitch_data_0

    .line 254
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not install mobile device management app on managed profile. Unknown status: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/managedprovisioning/ManagedProvisioningService;->error(Ljava/lang/String;)V

    .line 261
    .end local v1    # "status":I
    :goto_1
    :sswitch_0
    return-void

    .line 247
    .restart local v1    # "status":I
    :sswitch_1
    const-string v2, "Could not install mobile device management app on managed profile because the user is restricted"

    invoke-direct {p0, v2}, Lcom/android/managedprovisioning/ManagedProvisioningService;->error(Ljava/lang/String;)V

    .line 251
    :sswitch_2
    const-string v2, "Could not install mobile device management app on managed profile because the package could not be found"

    invoke-direct {p0, v2}, Lcom/android/managedprovisioning/ManagedProvisioningService;->error(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 257
    .end local v1    # "status":I
    :catch_0
    move-exception v0

    .line 259
    .local v0, "neverThrown":Landroid/os/RemoteException;
    const-string v2, "This should not happen."

    invoke-static {v2, v0}, Lcom/android/managedprovisioning/ProvisionLogger;->loge(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 242
    :sswitch_data_0
    .sparse-switch
        -0x6f -> :sswitch_1
        -0x3 -> :sswitch_2
        0x1 -> :sswitch_0
    .end sparse-switch
.end method

.method private onProvisioningSuccess(Landroid/content/ComponentName;)V
    .locals 10
    .param p1, "deviceAdminComponent"    # Landroid/content/ComponentName;

    .prologue
    const/4 v3, 0x0

    .line 293
    invoke-virtual {p0}, Lcom/android/managedprovisioning/ManagedProvisioningService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v5, "user_setup_complete"

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/android/managedprovisioning/ManagedProvisioningService;->mManagedProfileUserInfo:Landroid/content/pm/UserInfo;

    iget v7, v7, Landroid/content/pm/UserInfo;->id:I

    invoke-static {v0, v5, v6, v7}, Landroid/provider/Settings$Secure;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 296
    const-string v0, "user"

    invoke-virtual {p0, v0}, Lcom/android/managedprovisioning/ManagedProvisioningService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/os/UserManager;

    .line 297
    .local v9, "userManager":Landroid/os/UserManager;
    iget-object v0, p0, Lcom/android/managedprovisioning/ManagedProvisioningService;->mManagedProfileUserInfo:Landroid/content/pm/UserInfo;

    iget v0, v0, Landroid/content/pm/UserInfo;->serialNumber:I

    int-to-long v6, v0

    invoke-virtual {v9, v6, v7}, Landroid/os/UserManager;->getUserForSerialNumber(J)Landroid/os/UserHandle;

    move-result-object v2

    .line 300
    .local v2, "userHandle":Landroid/os/UserHandle;
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.app.action.PROFILE_PROVISIONING_COMPLETE"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 301
    .local v1, "completeIntent":Landroid/content/Intent;
    iget-object v0, p0, Lcom/android/managedprovisioning/ManagedProvisioningService;->mActiveAdminComponentName:Landroid/content/ComponentName;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 302
    const v0, 0x10000020

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 304
    iget-object v0, p0, Lcom/android/managedprovisioning/ManagedProvisioningService;->mAdminExtrasBundle:Landroid/os/PersistableBundle;

    if-eqz v0, :cond_0

    .line 305
    const-string v0, "android.app.extra.PROVISIONING_ADMIN_EXTRAS_BUNDLE"

    iget-object v5, p0, Lcom/android/managedprovisioning/ManagedProvisioningService;->mAdminExtrasBundle:Landroid/os/PersistableBundle;

    invoke-virtual {v1, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 310
    :cond_0
    new-instance v4, Lcom/android/managedprovisioning/ManagedProvisioningService$2;

    invoke-direct {v4, p0}, Lcom/android/managedprovisioning/ManagedProvisioningService$2;-><init>(Lcom/android/managedprovisioning/ManagedProvisioningService;)V

    .line 322
    .local v4, "mdmReceivedSuccessReceiver":Landroid/content/BroadcastReceiver;
    const/4 v6, -0x1

    move-object v0, p0

    move-object v5, v3

    move-object v7, v3

    move-object v8, v3

    invoke-virtual/range {v0 .. v8}, Lcom/android/managedprovisioning/ManagedProvisioningService;->sendOrderedBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V

    .line 325
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Provisioning complete broadcast has been sent to user "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 327
    return-void
.end method

.method private setMdmAsActiveAdmin()V
    .locals 4

    .prologue
    .line 278
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Setting package "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/managedprovisioning/ManagedProvisioningService;->mMdmPackageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " as active admin."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 280
    const-string v1, "device_policy"

    invoke-virtual {p0, v1}, Lcom/android/managedprovisioning/ManagedProvisioningService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    .line 282
    .local v0, "dpm":Landroid/app/admin/DevicePolicyManager;
    iget-object v1, p0, Lcom/android/managedprovisioning/ManagedProvisioningService;->mActiveAdminComponentName:Landroid/content/ComponentName;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/android/managedprovisioning/ManagedProvisioningService;->mManagedProfileUserInfo:Landroid/content/pm/UserInfo;

    iget v3, v3, Landroid/content/pm/UserInfo;->id:I

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/admin/DevicePolicyManager;->setActiveAdmin(Landroid/content/ComponentName;ZI)V

    .line 284
    return-void
.end method

.method private setMdmAsManagedProfileOwner()V
    .locals 4

    .prologue
    .line 265
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Setting package "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/managedprovisioning/ManagedProvisioningService;->mMdmPackageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " as managed profile owner."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 267
    const-string v1, "device_policy"

    invoke-virtual {p0, v1}, Lcom/android/managedprovisioning/ManagedProvisioningService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    .line 269
    .local v0, "dpm":Landroid/app/admin/DevicePolicyManager;
    iget-object v1, p0, Lcom/android/managedprovisioning/ManagedProvisioningService;->mActiveAdminComponentName:Landroid/content/ComponentName;

    iget-object v2, p0, Lcom/android/managedprovisioning/ManagedProvisioningService;->mMdmPackageName:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/managedprovisioning/ManagedProvisioningService;->mManagedProfileUserInfo:Landroid/content/pm/UserInfo;

    iget v3, v3, Landroid/content/pm/UserInfo;->id:I

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/admin/DevicePolicyManager;->setProfileOwner(Landroid/content/ComponentName;Ljava/lang/String;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 271
    const-string v1, "Could not set profile owner."

    invoke-static {v1}, Lcom/android/managedprovisioning/ProvisionLogger;->logw(Ljava/lang/String;)V

    .line 272
    const-string v1, "Could not set profile owner."

    invoke-direct {p0, v1}, Lcom/android/managedprovisioning/ManagedProvisioningService;->error(Ljava/lang/String;)V

    .line 274
    :cond_0
    return-void
.end method

.method private setUpProfileAndFinish()V
    .locals 3

    .prologue
    .line 189
    invoke-direct {p0}, Lcom/android/managedprovisioning/ManagedProvisioningService;->installMdmOnManagedProfile()V

    .line 190
    invoke-direct {p0}, Lcom/android/managedprovisioning/ManagedProvisioningService;->setMdmAsActiveAdmin()V

    .line 191
    invoke-direct {p0}, Lcom/android/managedprovisioning/ManagedProvisioningService;->setMdmAsManagedProfileOwner()V

    .line 192
    invoke-direct {p0}, Lcom/android/managedprovisioning/ManagedProvisioningService;->startManagedProfile()V

    .line 193
    invoke-virtual {p0}, Lcom/android/managedprovisioning/ManagedProvisioningService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/managedprovisioning/ManagedProvisioningService;->getUserId()I

    move-result v1

    iget-object v2, p0, Lcom/android/managedprovisioning/ManagedProvisioningService;->mManagedProfileUserInfo:Landroid/content/pm/UserInfo;

    iget v2, v2, Landroid/content/pm/UserInfo;->id:I

    invoke-static {v0, v1, v2}, Lcom/android/managedprovisioning/CrossProfileIntentFiltersHelper;->setFilters(Landroid/content/pm/PackageManager;II)V

    .line 195
    iget-object v0, p0, Lcom/android/managedprovisioning/ManagedProvisioningService;->mActiveAdminComponentName:Landroid/content/ComponentName;

    invoke-direct {p0, v0}, Lcom/android/managedprovisioning/ManagedProvisioningService;->onProvisioningSuccess(Landroid/content/ComponentName;)V

    .line 196
    return-void
.end method

.method private startManagedProfile()V
    .locals 4

    .prologue
    .line 221
    const-string v3, "Starting user in background"

    invoke-static {v3}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 222
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v0

    .line 224
    .local v0, "iActivityManager":Landroid/app/IActivityManager;
    :try_start_0
    iget-object v3, p0, Lcom/android/managedprovisioning/ManagedProvisioningService;->mManagedProfileUserInfo:Landroid/content/pm/UserInfo;

    iget v3, v3, Landroid/content/pm/UserInfo;->id:I

    invoke-interface {v0, v3}, Landroid/app/IActivityManager;->startUserInBackground(I)Z

    move-result v2

    .line 225
    .local v2, "success":Z
    if-nez v2, :cond_0

    .line 226
    const-string v3, "Could not start user in background"

    invoke-direct {p0, v3}, Lcom/android/managedprovisioning/ManagedProvisioningService;->error(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 232
    .end local v2    # "success":Z
    :cond_0
    :goto_0
    return-void

    .line 228
    :catch_0
    move-exception v1

    .line 230
    .local v1, "neverThrown":Landroid/os/RemoteException;
    const-string v3, "This should not happen."

    invoke-static {v3, v1}, Lcom/android/managedprovisioning/ProvisionLogger;->loge(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private startManagedProfileProvisioning()V
    .locals 9

    .prologue
    const/4 v6, 0x1

    .line 158
    const-string v0, "Starting managed profile provisioning"

    invoke-static {v0}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 161
    const v0, 0x7f07000d

    invoke-virtual {p0, v0}, Lcom/android/managedprovisioning/ManagedProvisioningService;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/managedprovisioning/ManagedProvisioningService;->createProfile(Ljava/lang/String;)V

    .line 162
    iget-object v0, p0, Lcom/android/managedprovisioning/ManagedProvisioningService;->mManagedProfileUserInfo:Landroid/content/pm/UserInfo;

    if-eqz v0, :cond_0

    .line 163
    new-instance v0, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;

    iget-object v2, p0, Lcom/android/managedprovisioning/ManagedProvisioningService;->mMdmPackageName:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/managedprovisioning/ManagedProvisioningService;->mManagedProfileUserInfo:Landroid/content/pm/UserInfo;

    iget v3, v1, Landroid/content/pm/UserInfo;->id:I

    const v4, 0x7f060001

    const v5, 0x7f060003

    new-instance v8, Lcom/android/managedprovisioning/ManagedProvisioningService$1;

    invoke-direct {v8, p0}, Lcom/android/managedprovisioning/ManagedProvisioningService$1;-><init>(Lcom/android/managedprovisioning/ManagedProvisioningService;)V

    move-object v1, p0

    move v7, v6

    invoke-direct/range {v0 .. v8}, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;-><init>(Landroid/content/Context;Ljava/lang/String;IIIZZLcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask$Callback;)V

    invoke-virtual {v0}, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->run()V

    .line 182
    :cond_0
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 350
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 97
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 99
    const-string v0, "Managed provisioning service ONCREATE"

    invoke-static {v0}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 101
    const-string v0, "package"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/content/pm/IPackageManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/pm/IPackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/managedprovisioning/ManagedProvisioningService;->mIpm:Landroid/content/pm/IPackageManager;

    .line 102
    const-string v0, "user"

    invoke-virtual {p0, v0}, Lcom/android/managedprovisioning/ManagedProvisioningService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    iput-object v0, p0, Lcom/android/managedprovisioning/ManagedProvisioningService;->mUserManager:Landroid/os/UserManager;

    .line 104
    new-instance v0, Lcom/android/managedprovisioning/ManagedProvisioningService$RunnerTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/managedprovisioning/ManagedProvisioningService$RunnerTask;-><init>(Lcom/android/managedprovisioning/ManagedProvisioningService;Lcom/android/managedprovisioning/ManagedProvisioningService$1;)V

    iput-object v0, p0, Lcom/android/managedprovisioning/ManagedProvisioningService;->runnerTask:Landroid/os/AsyncTask;

    .line 105
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 355
    const-string v0, "ManagedProvisioningService  ONDESTROY"

    invoke-static {v0}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 356
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 110
    const-string v1, "Starting managed provisioning service"

    invoke-static {v1}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 112
    :try_start_0
    iget-object v1, p0, Lcom/android/managedprovisioning/ManagedProvisioningService;->runnerTask:Landroid/os/AsyncTask;

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/content/Intent;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v1, v2}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 117
    :goto_0
    const/4 v1, 0x2

    return v1

    .line 113
    :catch_0
    move-exception v0

    .line 114
    .local v0, "ex":Ljava/lang/IllegalStateException;
    const-string v1, "ManagedProvisioningService: Provisioning already in progress, second provisioning intent not being processed"

    invoke-static {v1}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    goto :goto_0
.end method
