.class public Lcom/android/managedprovisioning/PreBootListener;
.super Landroid/content/BroadcastReceiver;
.source "PreBootListener.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private deleteNonRequiredApps(Landroid/content/Context;I)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "userId"    # I

    .prologue
    .line 73
    const-string v0, "device_policy"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/app/admin/DevicePolicyManager;

    .line 75
    .local v9, "dpm":Landroid/app/admin/DevicePolicyManager;
    invoke-virtual {v9, p2}, Landroid/app/admin/DevicePolicyManager;->getProfileOwnerAsUser(I)Landroid/content/ComponentName;

    move-result-object v10

    .line 76
    .local v10, "profileOwner":Landroid/content/ComponentName;
    if-nez v10, :cond_0

    .line 77
    const-string v0, "There is no profile owner on a managed profile."

    invoke-static {v0}, Lcom/android/managedprovisioning/ProvisionLogger;->loge(Ljava/lang/String;)V

    .line 80
    :cond_0
    new-instance v0, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;

    invoke-virtual {v10}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const v4, 0x7f060001

    const v5, 0x7f060003

    const/4 v6, 0x0

    const/4 v7, 0x1

    new-instance v8, Lcom/android/managedprovisioning/PreBootListener$1;

    invoke-direct {v8, p0}, Lcom/android/managedprovisioning/PreBootListener$1;-><init>(Lcom/android/managedprovisioning/PreBootListener;)V

    move-object v1, p1

    move v3, p2

    invoke-direct/range {v0 .. v8}, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;-><init>(Landroid/content/Context;Ljava/lang/String;IIIZZLcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask$Callback;)V

    invoke-virtual {v0}, Lcom/android/managedprovisioning/task/DeleteNonRequiredAppsTask;->run()V

    .line 96
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 40
    invoke-virtual {p1}, Landroid/content/Context;->getUserId()I

    move-result v0

    .line 41
    .local v0, "currentUserId":I
    if-nez v0, :cond_2

    .line 44
    const-string v7, "user"

    invoke-virtual {p1, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/os/UserManager;

    .line 46
    .local v5, "um":Landroid/os/UserManager;
    invoke-virtual {v5, v0}, Landroid/os/UserManager;->getProfiles(I)Ljava/util/List;

    move-result-object v4

    .line 47
    .local v4, "profiles":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/UserInfo;>;"
    const/4 v1, 0x0

    .line 48
    .local v1, "hasClearedParent":Z
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v7

    const/4 v8, 0x1

    if-le v7, v8, :cond_2

    .line 49
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 50
    .local v3, "pm":Landroid/content/pm/PackageManager;
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/pm/UserInfo;

    .line 52
    .local v6, "userInfo":Landroid/content/pm/UserInfo;
    invoke-virtual {v6}, Landroid/content/pm/UserInfo;->isManagedProfile()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {v6}, Landroid/content/pm/UserInfo;->isKnoxWorkspace()Z

    move-result v7

    if-nez v7, :cond_0

    .line 53
    if-nez v1, :cond_1

    .line 55
    invoke-virtual {v3, v0}, Landroid/content/pm/PackageManager;->clearCrossProfileIntentFilters(I)V

    .line 56
    const/4 v1, 0x1

    .line 58
    :cond_1
    iget v7, v6, Landroid/content/pm/UserInfo;->id:I

    invoke-virtual {v3, v7}, Landroid/content/pm/PackageManager;->clearCrossProfileIntentFilters(I)V

    .line 59
    iget v7, v6, Landroid/content/pm/UserInfo;->id:I

    invoke-static {v3, v0, v7}, Lcom/android/managedprovisioning/CrossProfileIntentFiltersHelper;->setFilters(Landroid/content/pm/PackageManager;II)V

    .line 61
    iget v7, v6, Landroid/content/pm/UserInfo;->id:I

    invoke-direct {p0, p1, v7}, Lcom/android/managedprovisioning/PreBootListener;->deleteNonRequiredApps(Landroid/content/Context;I)V

    goto :goto_0

    .line 66
    .end local v1    # "hasClearedParent":Z
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "pm":Landroid/content/pm/PackageManager;
    .end local v4    # "profiles":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/UserInfo;>;"
    .end local v5    # "um":Landroid/os/UserManager;
    .end local v6    # "userInfo":Landroid/content/pm/UserInfo;
    :cond_2
    return-void
.end method
