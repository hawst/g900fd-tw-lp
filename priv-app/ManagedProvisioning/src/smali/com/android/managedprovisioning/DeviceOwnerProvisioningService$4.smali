.class Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$4;
.super Lcom/android/managedprovisioning/task/InstallPackageTask$Callback;
.source "DeviceOwnerProvisioningService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->startDeviceOwnerProvisioning(Lcom/android/managedprovisioning/ProvisioningParams;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;


# direct methods
.method constructor <init>(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;)V
    .locals 0

    .prologue
    .line 251
    iput-object p1, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$4;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    invoke-direct {p0}, Lcom/android/managedprovisioning/task/InstallPackageTask$Callback;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(I)V
    .locals 2
    .param p1, "errorCode"    # I

    .prologue
    .line 260
    packed-switch p1, :pswitch_data_0

    .line 268
    iget-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$4;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    const v1, 0x7f070029

    # invokes: Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->error(I)V
    invoke-static {v0, v1}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->access$700(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;I)V

    .line 271
    :goto_0
    return-void

    .line 262
    :pswitch_0
    iget-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$4;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    const v1, 0x7f07002e

    # invokes: Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->error(I)V
    invoke-static {v0, v1}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->access$700(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;I)V

    goto :goto_0

    .line 265
    :pswitch_1
    iget-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$4;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    const v1, 0x7f07002f

    # invokes: Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->error(I)V
    invoke-static {v0, v1}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->access$700(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;I)V

    goto :goto_0

    .line 260
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onSuccess()V
    .locals 2

    .prologue
    .line 254
    iget-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$4;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    const v1, 0x7f070023

    # invokes: Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->progressUpdate(I)V
    invoke-static {v0, v1}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->access$400(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;I)V

    .line 255
    iget-object v0, p0, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService$4;->this$0:Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;

    # getter for: Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->mSetDevicePolicyTask:Lcom/android/managedprovisioning/task/SetDevicePolicyTask;
    invoke-static {v0}, Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;->access$600(Lcom/android/managedprovisioning/DeviceOwnerProvisioningService;)Lcom/android/managedprovisioning/task/SetDevicePolicyTask;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/managedprovisioning/task/SetDevicePolicyTask;->run()V

    .line 256
    return-void
.end method
