.class Lcom/android/managedprovisioning/NetworkMonitor$1;
.super Landroid/content/BroadcastReceiver;
.source "NetworkMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/managedprovisioning/NetworkMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/managedprovisioning/NetworkMonitor;


# direct methods
.method constructor <init>(Lcom/android/managedprovisioning/NetworkMonitor;)V
    .locals 0

    .prologue
    .line 89
    iput-object p1, p0, Lcom/android/managedprovisioning/NetworkMonitor$1;->this$0:Lcom/android/managedprovisioning/NetworkMonitor;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 92
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onReceive "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/managedprovisioning/ProvisionLogger;->logd(Ljava/lang/String;)V

    .line 94
    iget-object v0, p0, Lcom/android/managedprovisioning/NetworkMonitor$1;->this$0:Lcom/android/managedprovisioning/NetworkMonitor;

    invoke-static {p1}, Lcom/android/managedprovisioning/NetworkMonitor;->isConnectedToWifi(Landroid/content/Context;)Z

    move-result v1

    # setter for: Lcom/android/managedprovisioning/NetworkMonitor;->mNetworkConnected:Z
    invoke-static {v0, v1}, Lcom/android/managedprovisioning/NetworkMonitor;->access$002(Lcom/android/managedprovisioning/NetworkMonitor;Z)Z

    .line 96
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.net.conn.INET_CONDITION_ACTION"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/android/managedprovisioning/NetworkMonitor$1;->this$0:Lcom/android/managedprovisioning/NetworkMonitor;

    # getter for: Lcom/android/managedprovisioning/NetworkMonitor;->mNetworkConnected:Z
    invoke-static {v0}, Lcom/android/managedprovisioning/NetworkMonitor;->access$000(Lcom/android/managedprovisioning/NetworkMonitor;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 99
    iget-object v0, p0, Lcom/android/managedprovisioning/NetworkMonitor$1;->this$0:Lcom/android/managedprovisioning/NetworkMonitor;

    # getter for: Lcom/android/managedprovisioning/NetworkMonitor;->mCallback:Lcom/android/managedprovisioning/NetworkMonitor$Callback;
    invoke-static {v0}, Lcom/android/managedprovisioning/NetworkMonitor;->access$100(Lcom/android/managedprovisioning/NetworkMonitor;)Lcom/android/managedprovisioning/NetworkMonitor$Callback;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/managedprovisioning/NetworkMonitor$Callback;->onNetworkConnected()V

    .line 104
    :cond_1
    :goto_0
    return-void

    .line 101
    :cond_2
    iget-object v0, p0, Lcom/android/managedprovisioning/NetworkMonitor$1;->this$0:Lcom/android/managedprovisioning/NetworkMonitor;

    # getter for: Lcom/android/managedprovisioning/NetworkMonitor;->mCallback:Lcom/android/managedprovisioning/NetworkMonitor$Callback;
    invoke-static {v0}, Lcom/android/managedprovisioning/NetworkMonitor;->access$100(Lcom/android/managedprovisioning/NetworkMonitor;)Lcom/android/managedprovisioning/NetworkMonitor$Callback;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/managedprovisioning/NetworkMonitor$Callback;->onNetworkDisconnected()V

    goto :goto_0
.end method
