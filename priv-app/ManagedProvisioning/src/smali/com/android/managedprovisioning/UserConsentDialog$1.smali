.class Lcom/android/managedprovisioning/UserConsentDialog$1;
.super Ljava/lang/Object;
.source "UserConsentDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/managedprovisioning/UserConsentDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/managedprovisioning/UserConsentDialog;


# direct methods
.method constructor <init>(Lcom/android/managedprovisioning/UserConsentDialog;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/android/managedprovisioning/UserConsentDialog$1;->this$0:Lcom/android/managedprovisioning/UserConsentDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 82
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    iget-object v2, p0, Lcom/android/managedprovisioning/UserConsentDialog$1;->this$0:Lcom/android/managedprovisioning/UserConsentDialog;

    # getter for: Lcom/android/managedprovisioning/UserConsentDialog;->mLearnMoreUri:Landroid/net/Uri;
    invoke-static {v2}, Lcom/android/managedprovisioning/UserConsentDialog;->access$000(Lcom/android/managedprovisioning/UserConsentDialog;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 83
    .local v0, "browserIntent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/android/managedprovisioning/UserConsentDialog$1;->this$0:Lcom/android/managedprovisioning/UserConsentDialog;

    # getter for: Lcom/android/managedprovisioning/UserConsentDialog;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/android/managedprovisioning/UserConsentDialog;->access$100(Lcom/android/managedprovisioning/UserConsentDialog;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 84
    return-void
.end method
