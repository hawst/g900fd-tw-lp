.class public interface abstract Lcom/samsung/android/scloud/quota/QuotaConstants$DIALOG;
.super Ljava/lang/Object;
.source "QuotaConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/quota/QuotaConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "DIALOG"
.end annotation


# static fields
.field public static final DATA_DELETION_DIALOG:I = 0xc

.field public static final DATA_EXCEEDED_DIALOG:I = 0x9

.field public static final DOWNGRADE_FAILED_DIALOG:I = 0x8

.field public static final NONE:I = -0x1

.field public static final PLAN_DOWNGRADED_DIALOG:I = 0x6

.field public static final PLAN_DOWNGRADE_NOTICE_DIALOG:I = 0x5

.field public static final PLAN_RENEWED_DIALOG:I = 0x2

.field public static final PLAN_RENEW_NOTICE_DIALOG:I = 0x1

.field public static final PLAN_UPGRADED_DIALOG:I = 0x4

.field public static final PLAN_UPGRADE_NOTICE_DIALOG:I = 0x3

.field public static final QUOTA_FULL_DIALOG:I = 0xe

.field public static final QUOTA_WARNING_DIALOG:I = 0xd

.field public static final RENEWAL_FAILED_DIALOG:I = 0x7

.field public static final SUSBSCRIPTION_CANCELLED_DIALOG:I = 0xb

.field public static final SUSBSCRIPTION_CANCEL_NOTICE_DIALOG:I = 0xa
