.class Lcom/samsung/android/scloud/quota/BackupList$9;
.super Ljava/lang/Thread;
.source "BackupList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/quota/BackupList;->callEndService()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/quota/BackupList;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/quota/BackupList;)V
    .locals 0

    .prologue
    .line 1140
    iput-object p1, p0, Lcom/samsung/android/scloud/quota/BackupList$9;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 1146
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList$9;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v2, p0, Lcom/samsung/android/scloud/quota/BackupList$9;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    # getter for: Lcom/samsung/android/scloud/quota/BackupList;->quotaServiceManager:Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;
    invoke-static {v2}, Lcom/samsung/android/scloud/quota/BackupList;->access$1100(Lcom/samsung/android/scloud/quota/BackupList;)Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/quota/BackupList$9;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    # getter for: Lcom/samsung/android/scloud/quota/BackupList;->quotaServiceManager:Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;
    invoke-static {v3}, Lcom/samsung/android/scloud/quota/BackupList;->access$1100(Lcom/samsung/android/scloud/quota/BackupList;)Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->getApiResult()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/quota/BackupList$9;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v4, v4, Lcom/samsung/android/scloud/quota/BackupList;->mCtid:Ljava/lang/String;

    iget-object v5, p0, Lcom/samsung/android/scloud/quota/BackupList$9;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    # getter for: Lcom/samsung/android/scloud/quota/BackupList;->quotaServiceManager:Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;
    invoke-static {v5}, Lcom/samsung/android/scloud/quota/BackupList;->access$1100(Lcom/samsung/android/scloud/quota/BackupList;)Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->getApiSuccess()Z

    move-result v5

    iget-object v6, p0, Lcom/samsung/android/scloud/quota/BackupList$9;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    # getter for: Lcom/samsung/android/scloud/quota/BackupList;->quotaServiceManager:Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;
    invoke-static {v6}, Lcom/samsung/android/scloud/quota/BackupList;->access$1100(Lcom/samsung/android/scloud/quota/BackupList;)Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->getLastApi()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->serviceEnd(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Lcom/samsung/android/scloud/quota/response/KVSResponse;

    move-result-object v2

    iput-object v2, v1, Lcom/samsung/android/scloud/quota/BackupList;->result:Lcom/samsung/android/scloud/quota/response/KVSResponse;
    :try_end_0
    .catch Lcom/samsung/android/scloud/quota/QuotaException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1164
    :goto_0
    return-void

    .line 1154
    :catch_0
    move-exception v0

    .line 1156
    .local v0, "e":Lcom/samsung/android/scloud/quota/QuotaException;
    const-string v1, "BackupList"

    const-string v2, "Error while Service End call"

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1158
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList$9;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    invoke-virtual {v1}, Lcom/samsung/android/scloud/quota/BackupList;->finish()V

    .line 1160
    invoke-virtual {v0}, Lcom/samsung/android/scloud/quota/QuotaException;->printStackTrace()V

    goto :goto_0
.end method
