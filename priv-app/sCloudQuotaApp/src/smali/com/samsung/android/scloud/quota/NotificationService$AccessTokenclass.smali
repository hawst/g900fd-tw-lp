.class Lcom/samsung/android/scloud/quota/NotificationService$AccessTokenclass;
.super Landroid/os/AsyncTask;
.source "NotificationService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/quota/NotificationService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AccessTokenclass"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/quota/NotificationService;


# direct methods
.method private constructor <init>(Lcom/samsung/android/scloud/quota/NotificationService;)V
    .locals 0

    .prologue
    .line 152
    iput-object p1, p0, Lcom/samsung/android/scloud/quota/NotificationService$AccessTokenclass;->this$0:Lcom/samsung/android/scloud/quota/NotificationService;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/scloud/quota/NotificationService;Lcom/samsung/android/scloud/quota/NotificationService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/scloud/quota/NotificationService;
    .param p2, "x1"    # Lcom/samsung/android/scloud/quota/NotificationService$1;

    .prologue
    .line 152
    invoke-direct {p0, p1}, Lcom/samsung/android/scloud/quota/NotificationService$AccessTokenclass;-><init>(Lcom/samsung/android/scloud/quota/NotificationService;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 152
    check-cast p1, [Ljava/lang/String;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/android/scloud/quota/NotificationService$AccessTokenclass;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "params"    # [Ljava/lang/String;

    .prologue
    .line 156
    const-string v1, "NotificationService"

    const-string v2, "QUOTA_5102 : [START]"

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/NotificationService$AccessTokenclass;->this$0:Lcom/samsung/android/scloud/quota/NotificationService;

    new-instance v2, Lcom/samsung/android/scloud/quota/AuthControlForNewDataRelay;

    invoke-direct {v2}, Lcom/samsung/android/scloud/quota/AuthControlForNewDataRelay;-><init>()V

    # setter for: Lcom/samsung/android/scloud/quota/NotificationService;->mAuthControlNew:Lcom/samsung/android/scloud/quota/AuthControlForNewDataRelay;
    invoke-static {v1, v2}, Lcom/samsung/android/scloud/quota/NotificationService;->access$402(Lcom/samsung/android/scloud/quota/NotificationService;Lcom/samsung/android/scloud/quota/AuthControlForNewDataRelay;)Lcom/samsung/android/scloud/quota/AuthControlForNewDataRelay;

    .line 160
    :try_start_0
    const-string v1, "NotificationService"

    const-string v2, "QUOTA_5102 : [TRY]"

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/NotificationService$AccessTokenclass;->this$0:Lcom/samsung/android/scloud/quota/NotificationService;

    iget-object v2, p0, Lcom/samsung/android/scloud/quota/NotificationService$AccessTokenclass;->this$0:Lcom/samsung/android/scloud/quota/NotificationService;

    # getter for: Lcom/samsung/android/scloud/quota/NotificationService;->mAuthControlNew:Lcom/samsung/android/scloud/quota/AuthControlForNewDataRelay;
    invoke-static {v2}, Lcom/samsung/android/scloud/quota/NotificationService;->access$400(Lcom/samsung/android/scloud/quota/NotificationService;)Lcom/samsung/android/scloud/quota/AuthControlForNewDataRelay;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/quota/NotificationService$AccessTokenclass;->this$0:Lcom/samsung/android/scloud/quota/NotificationService;

    # getter for: Lcom/samsung/android/scloud/quota/NotificationService;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/scloud/quota/NotificationService;->access$500(Lcom/samsung/android/scloud/quota/NotificationService;)Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/quota/NotificationService$AccessTokenclass;->this$0:Lcom/samsung/android/scloud/quota/NotificationService;

    # getter for: Lcom/samsung/android/scloud/quota/NotificationService;->mCtid:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/scloud/quota/NotificationService;->access$600(Lcom/samsung/android/scloud/quota/NotificationService;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/samsung/android/scloud/quota/AuthControlForNewDataRelay;->getAuthInformation(Landroid/content/Context;Ljava/lang/String;)Lcom/samsung/android/scloud/quota/AuthManager;

    move-result-object v2

    iput-object v2, v1, Lcom/samsung/android/scloud/quota/NotificationService;->mAuthManager:Lcom/samsung/android/scloud/quota/AuthManager;

    .line 164
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/NotificationService$AccessTokenclass;->this$0:Lcom/samsung/android/scloud/quota/NotificationService;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/samsung/android/scloud/quota/NotificationService;->isAccessToken:Ljava/lang/Boolean;
    :try_end_0
    .catch Lcom/samsung/android/scloud/quota/QuotaException; {:try_start_0 .. :try_end_0} :catch_0

    .line 171
    :goto_0
    const/4 v1, 0x0

    return-object v1

    .line 165
    :catch_0
    move-exception v0

    .line 167
    .local v0, "e":Lcom/samsung/android/scloud/quota/QuotaException;
    const-string v1, "NotificationService"

    const-string v2, "QUOTA_5102 : [QuotaException]"

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/NotificationService$AccessTokenclass;->this$0:Lcom/samsung/android/scloud/quota/NotificationService;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/samsung/android/scloud/quota/NotificationService;->isAccessToken:Ljava/lang/Boolean;

    .line 169
    invoke-virtual {v0}, Lcom/samsung/android/scloud/quota/QuotaException;->printStackTrace()V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 152
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/android/scloud/quota/NotificationService$AccessTokenclass;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 2
    .param p1, "result"    # Ljava/lang/String;

    .prologue
    .line 180
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/NotificationService$AccessTokenclass;->this$0:Lcom/samsung/android/scloud/quota/NotificationService;

    iget-object v0, v0, Lcom/samsung/android/scloud/quota/NotificationService;->isAccessToken:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/NotificationService$AccessTokenclass;->this$0:Lcom/samsung/android/scloud/quota/NotificationService;

    iget-object v0, v0, Lcom/samsung/android/scloud/quota/NotificationService;->mAuthManager:Lcom/samsung/android/scloud/quota/AuthManager;

    if-eqz v0, :cond_0

    .line 182
    const-string v0, "NotificationService"

    const-string v1, "Success to get Authinfo"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/NotificationService$AccessTokenclass;->this$0:Lcom/samsung/android/scloud/quota/NotificationService;

    # invokes: Lcom/samsung/android/scloud/quota/NotificationService;->startQuotaThread()V
    invoke-static {v0}, Lcom/samsung/android/scloud/quota/NotificationService;->access$700(Lcom/samsung/android/scloud/quota/NotificationService;)V

    .line 187
    :cond_0
    return-void
.end method
