.class Lcom/samsung/android/scloud/quota/AuthControl$1;
.super Ljava/lang/Object;
.source "AuthControl.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/quota/AuthControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/quota/AuthControl;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/quota/AuthControl;)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/samsung/android/scloud/quota/AuthControl$1;->this$0:Lcom/samsung/android/scloud/quota/AuthControl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "binder"    # Landroid/os/IBinder;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/AuthControl$1;->this$0:Lcom/samsung/android/scloud/quota/AuthControl;

    invoke-static {p2}, Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;

    move-result-object v1

    # setter for: Lcom/samsung/android/scloud/quota/AuthControl;->mService:Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;
    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/AuthControl;->access$002(Lcom/samsung/android/scloud/quota/AuthControl;Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;)Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;

    .line 83
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/AuthControl$1;->this$0:Lcom/samsung/android/scloud/quota/AuthControl;

    # getter for: Lcom/samsung/android/scloud/quota/AuthControl;->mService:Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;
    invoke-static {v0}, Lcom/samsung/android/scloud/quota/AuthControl;->access$000(Lcom/samsung/android/scloud/quota/AuthControl;)Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;

    move-result-object v0

    if-nez v0, :cond_0

    .line 85
    const-string v0, "AuthControl"

    const-string v1, "onServiceConnected : There is BindingService Error."

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/AuthControl$1;->this$0:Lcom/samsung/android/scloud/quota/AuthControl;

    # getter for: Lcom/samsung/android/scloud/quota/AuthControl;->syncLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/samsung/android/scloud/quota/AuthControl;->access$100(Lcom/samsung/android/scloud/quota/AuthControl;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 98
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/AuthControl$1;->this$0:Lcom/samsung/android/scloud/quota/AuthControl;

    const/4 v2, 0x0

    # setter for: Lcom/samsung/android/scloud/quota/AuthControl;->syncWait:Z
    invoke-static {v0, v2}, Lcom/samsung/android/scloud/quota/AuthControl;->access$202(Lcom/samsung/android/scloud/quota/AuthControl;Z)Z

    .line 100
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/AuthControl$1;->this$0:Lcom/samsung/android/scloud/quota/AuthControl;

    # getter for: Lcom/samsung/android/scloud/quota/AuthControl;->syncLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/samsung/android/scloud/quota/AuthControl;->access$100(Lcom/samsung/android/scloud/quota/AuthControl;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 102
    const-string v0, "AuthControl"

    const-string v2, "onServiceConnected : notify to startGetAuthDetails"

    invoke-static {v0, v2}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 106
    return-void

    .line 89
    :cond_0
    const-string v0, "AuthControl"

    const-string v1, "onServiceConnected : Binded."

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 104
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/AuthControl$1;->this$0:Lcom/samsung/android/scloud/quota/AuthControl;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/scloud/quota/AuthControl;->mService:Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;
    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/AuthControl;->access$002(Lcom/samsung/android/scloud/quota/AuthControl;Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;)Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;

    .line 113
    const-string v0, "AuthControl"

    const-string v1, "onServiceDisconnected : unBinded."

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    return-void
.end method
