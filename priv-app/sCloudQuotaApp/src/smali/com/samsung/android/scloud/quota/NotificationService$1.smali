.class Lcom/samsung/android/scloud/quota/NotificationService$1;
.super Landroid/os/Handler;
.source "NotificationService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/quota/NotificationService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/quota/NotificationService;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/quota/NotificationService;)V
    .locals 0

    .prologue
    .line 104
    iput-object p1, p0, Lcom/samsung/android/scloud/quota/NotificationService$1;->this$0:Lcom/samsung/android/scloud/quota/NotificationService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v4, 0x1

    .line 107
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 125
    :goto_0
    return-void

    .line 111
    :sswitch_0
    const-string v0, "NotificationService"

    const-string v1, "Handler, Total Usage"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    const-string v0, "NotificationService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "TOTAL_USAGE"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/NotificationService$1;->this$0:Lcom/samsung/android/scloud/quota/NotificationService;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "TOTAL_USAGE"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    # setter for: Lcom/samsung/android/scloud/quota/NotificationService;->mTotalUsage:J
    invoke-static {v0, v2, v3}, Lcom/samsung/android/scloud/quota/NotificationService;->access$102(Lcom/samsung/android/scloud/quota/NotificationService;J)J

    .line 114
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/NotificationService$1;->this$0:Lcom/samsung/android/scloud/quota/NotificationService;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "TOTAL_QUOTA"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    # setter for: Lcom/samsung/android/scloud/quota/NotificationService;->mTotalQuota:J
    invoke-static {v0, v2, v3}, Lcom/samsung/android/scloud/quota/NotificationService;->access$202(Lcom/samsung/android/scloud/quota/NotificationService;J)J

    .line 115
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/NotificationService$1;->this$0:Lcom/samsung/android/scloud/quota/NotificationService;

    # invokes: Lcom/samsung/android/scloud/quota/NotificationService;->showNotification(I)V
    invoke-static {v0, v4}, Lcom/samsung/android/scloud/quota/NotificationService;->access$300(Lcom/samsung/android/scloud/quota/NotificationService;I)V

    goto :goto_0

    .line 118
    :sswitch_1
    const-string v0, "NotificationService"

    const-string v1, "Handler, Bad Access Token"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    new-instance v0, Lcom/samsung/android/scloud/quota/NotificationService$AccessTokenclass;

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/NotificationService$1;->this$0:Lcom/samsung/android/scloud/quota/NotificationService;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/scloud/quota/NotificationService$AccessTokenclass;-><init>(Lcom/samsung/android/scloud/quota/NotificationService;Lcom/samsung/android/scloud/quota/NotificationService$1;)V

    new-array v1, v4, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, ""

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/quota/NotificationService$AccessTokenclass;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 107
    :sswitch_data_0
    .sparse-switch
        -0x3 -> :sswitch_1
        0x4 -> :sswitch_0
    .end sparse-switch
.end method
