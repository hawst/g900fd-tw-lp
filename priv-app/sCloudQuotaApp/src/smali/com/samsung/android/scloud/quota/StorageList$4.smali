.class Lcom/samsung/android/scloud/quota/StorageList$4;
.super Landroid/os/Handler;
.source "StorageList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/quota/StorageList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/quota/StorageList;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/quota/StorageList;)V
    .locals 0

    .prologue
    .line 947
    iput-object p1, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v6, 0x1

    const v4, 0x7f07007e

    const/4 v3, 0x0

    .line 950
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 1113
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 952
    :pswitch_1
    const-string v0, "StorageList"

    const-string v1, "Handle Quota Usage and total BNR usage"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 953
    const-string v0, "StorageList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TOTAL_BACKEDUP_SIZE : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "BACKEDUP_SIZE"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 957
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "BACKEDUP_SIZE"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    # setter for: Lcom/samsung/android/scloud/quota/StorageList;->mBNRUSage:J
    invoke-static {v0, v2, v3}, Lcom/samsung/android/scloud/quota/StorageList;->access$402(Lcom/samsung/android/scloud/quota/StorageList;J)J

    .line 959
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mUsageofApp:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/samsung/android/scloud/quota/StorageList;->access$500(Lcom/samsung/android/scloud/quota/StorageList;)Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "pref_backedupdata"

    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mBNRUSage:J
    invoke-static {v2}, Lcom/samsung/android/scloud/quota/StorageList;->access$400(Lcom/samsung/android/scloud/quota/StorageList;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 960
    const-string v0, "StorageList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CID_USAGE_CONTACT : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "CID_USAGE_CONTACT"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 964
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "CID_USAGE_CONTACT"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    # setter for: Lcom/samsung/android/scloud/quota/StorageList;->mContactUsage:J
    invoke-static {v0, v2, v3}, Lcom/samsung/android/scloud/quota/StorageList;->access$602(Lcom/samsung/android/scloud/quota/StorageList;J)J

    .line 966
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mUsageofApp:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/samsung/android/scloud/quota/StorageList;->access$500(Lcom/samsung/android/scloud/quota/StorageList;)Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "pref_contacts"

    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mContactUsage:J
    invoke-static {v2}, Lcom/samsung/android/scloud/quota/StorageList;->access$600(Lcom/samsung/android/scloud/quota/StorageList;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 968
    const-string v0, "StorageList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CID_USAGE_SNOTE : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "CID_USAGE_SNOTE"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 972
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "CID_USAGE_SNOTE"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    # setter for: Lcom/samsung/android/scloud/quota/StorageList;->mSnoteUsage:J
    invoke-static {v0, v2, v3}, Lcom/samsung/android/scloud/quota/StorageList;->access$702(Lcom/samsung/android/scloud/quota/StorageList;J)J

    .line 974
    const-string v0, "StorageList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CID_USAGE_SMEMO : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "CID_USAGE_SMEMO"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 978
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "CID_USAGE_SMEMO"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    # setter for: Lcom/samsung/android/scloud/quota/StorageList;->mSMemoUsage:J
    invoke-static {v0, v2, v3}, Lcom/samsung/android/scloud/quota/StorageList;->access$802(Lcom/samsung/android/scloud/quota/StorageList;J)J

    .line 980
    const-string v0, "StorageList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CID_USAGE_TMEMO : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "CID_USAGE_TMEMO"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 984
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "CID_USAGE_TMEMO"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    # setter for: Lcom/samsung/android/scloud/quota/StorageList;->mTmemoUsage:J
    invoke-static {v0, v2, v3}, Lcom/samsung/android/scloud/quota/StorageList;->access$902(Lcom/samsung/android/scloud/quota/StorageList;J)J

    .line 986
    const-string v0, "StorageList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CID_USAGE_KMEMO : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "CID_USAGE_KMEMO"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 991
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "CID_USAGE_KMEMO"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    # setter for: Lcom/samsung/android/scloud/quota/StorageList;->mKmemoUsage:J
    invoke-static {v0, v2, v3}, Lcom/samsung/android/scloud/quota/StorageList;->access$1002(Lcom/samsung/android/scloud/quota/StorageList;J)J

    .line 994
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mUsageofApp:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/samsung/android/scloud/quota/StorageList;->access$500(Lcom/samsung/android/scloud/quota/StorageList;)Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "pref_snb"

    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mSnoteUsage:J
    invoke-static {v2}, Lcom/samsung/android/scloud/quota/StorageList;->access$700(Lcom/samsung/android/scloud/quota/StorageList;)J

    move-result-wide v2

    iget-object v4, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mSMemoUsage:J
    invoke-static {v4}, Lcom/samsung/android/scloud/quota/StorageList;->access$800(Lcom/samsung/android/scloud/quota/StorageList;)J

    move-result-wide v4

    add-long/2addr v2, v4

    iget-object v4, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mTmemoUsage:J
    invoke-static {v4}, Lcom/samsung/android/scloud/quota/StorageList;->access$900(Lcom/samsung/android/scloud/quota/StorageList;)J

    move-result-wide v4

    add-long/2addr v2, v4

    iget-object v4, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mKmemoUsage:J
    invoke-static {v4}, Lcom/samsung/android/scloud/quota/StorageList;->access$1000(Lcom/samsung/android/scloud/quota/StorageList;)J

    move-result-wide v4

    add-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 997
    const-string v0, "StorageList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CID_USAGE_SNOTE3 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "CID_USAGE_SNOTE3"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1001
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "CID_USAGE_SNOTE3"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    # setter for: Lcom/samsung/android/scloud/quota/StorageList;->mSnote3Usage:J
    invoke-static {v0, v2, v3}, Lcom/samsung/android/scloud/quota/StorageList;->access$1102(Lcom/samsung/android/scloud/quota/StorageList;J)J

    .line 1003
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mUsageofApp:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/samsung/android/scloud/quota/StorageList;->access$500(Lcom/samsung/android/scloud/quota/StorageList;)Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "pref_spd"

    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mSnote3Usage:J
    invoke-static {v2}, Lcom/samsung/android/scloud/quota/StorageList;->access$1100(Lcom/samsung/android/scloud/quota/StorageList;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1007
    const-string v0, "StorageList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CID_USAGE_CALENDAR : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "CID_USAGE_CALENDAR"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1011
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "CID_USAGE_CALENDAR"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    # setter for: Lcom/samsung/android/scloud/quota/StorageList;->mCalendarUsage:J
    invoke-static {v0, v2, v3}, Lcom/samsung/android/scloud/quota/StorageList;->access$1202(Lcom/samsung/android/scloud/quota/StorageList;J)J

    .line 1013
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mUsageofApp:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/samsung/android/scloud/quota/StorageList;->access$500(Lcom/samsung/android/scloud/quota/StorageList;)Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "pref_splanner"

    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mCalendarUsage:J
    invoke-static {v2}, Lcom/samsung/android/scloud/quota/StorageList;->access$1200(Lcom/samsung/android/scloud/quota/StorageList;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1015
    const-string v0, "StorageList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CID_USAGE_BROWSER : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "CID_USAGE_BROWSER"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1019
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "CID_USAGE_BROWSER"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    # setter for: Lcom/samsung/android/scloud/quota/StorageList;->mBrowserUsage:J
    invoke-static {v0, v2, v3}, Lcom/samsung/android/scloud/quota/StorageList;->access$1302(Lcom/samsung/android/scloud/quota/StorageList;J)J

    .line 1021
    const-string v0, "StorageList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CID_USAGE_SBROWSER : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "CID_USAGE_SBROWSER"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1025
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "CID_USAGE_SBROWSER"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    # setter for: Lcom/samsung/android/scloud/quota/StorageList;->mSBrowserUsage:J
    invoke-static {v0, v2, v3}, Lcom/samsung/android/scloud/quota/StorageList;->access$1402(Lcom/samsung/android/scloud/quota/StorageList;J)J

    .line 1027
    const-string v0, "StorageList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CID_USAGE_SBROWSERSCRAP : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "CID_USAGE_SBROWSER_SCRAP"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1031
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "CID_USAGE_SBROWSER_SCRAP"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    # setter for: Lcom/samsung/android/scloud/quota/StorageList;->mSBrowserScrapUsage:J
    invoke-static {v0, v2, v3}, Lcom/samsung/android/scloud/quota/StorageList;->access$1502(Lcom/samsung/android/scloud/quota/StorageList;J)J

    .line 1034
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mUsageofApp:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/samsung/android/scloud/quota/StorageList;->access$500(Lcom/samsung/android/scloud/quota/StorageList;)Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "pref_bookmarks"

    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mSBrowserUsage:J
    invoke-static {v2}, Lcom/samsung/android/scloud/quota/StorageList;->access$1400(Lcom/samsung/android/scloud/quota/StorageList;)J

    move-result-wide v2

    iget-object v4, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mBrowserUsage:J
    invoke-static {v4}, Lcom/samsung/android/scloud/quota/StorageList;->access$1300(Lcom/samsung/android/scloud/quota/StorageList;)J

    move-result-wide v4

    add-long/2addr v2, v4

    iget-object v4, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mSBrowserScrapUsage:J
    invoke-static {v4}, Lcom/samsung/android/scloud/quota/StorageList;->access$1500(Lcom/samsung/android/scloud/quota/StorageList;)J

    move-result-wide v4

    add-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1037
    const-string v0, "StorageList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CID_USAGE_PINBOARD: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "CID_USAGE_PINBOARD"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1041
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "CID_USAGE_PINBOARD"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    # setter for: Lcom/samsung/android/scloud/quota/StorageList;->mPinboardUsage:J
    invoke-static {v0, v2, v3}, Lcom/samsung/android/scloud/quota/StorageList;->access$1602(Lcom/samsung/android/scloud/quota/StorageList;J)J

    .line 1043
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mUsageofApp:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/samsung/android/scloud/quota/StorageList;->access$500(Lcom/samsung/android/scloud/quota/StorageList;)Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "pref_pinboard"

    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mPinboardUsage:J
    invoke-static {v2}, Lcom/samsung/android/scloud/quota/StorageList;->access$1600(Lcom/samsung/android/scloud/quota/StorageList;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1045
    const-string v0, "StorageList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " TOTAl_USAGE : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "TOTAL_USAGE"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1049
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "TOTAL_USAGE"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    # setter for: Lcom/samsung/android/scloud/quota/StorageList;->mTotalUsage:J
    invoke-static {v0, v2, v3}, Lcom/samsung/android/scloud/quota/StorageList;->access$1702(Lcom/samsung/android/scloud/quota/StorageList;J)J

    .line 1050
    const-string v0, "StorageList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " TOTAL_QUOTA : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "TOTAL_QUOTA"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1055
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    # setter for: Lcom/samsung/android/scloud/quota/StorageList;->mIsBackedupData:Ljava/lang/Boolean;
    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/StorageList;->access$1802(Lcom/samsung/android/scloud/quota/StorageList;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    .line 1056
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # invokes: Lcom/samsung/android/scloud/quota/StorageList;->initPref()V
    invoke-static {v0}, Lcom/samsung/android/scloud/quota/StorageList;->access$1900(Lcom/samsung/android/scloud/quota/StorageList;)V

    .line 1057
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # invokes: Lcom/samsung/android/scloud/quota/StorageList;->setQuotaSummary()V
    invoke-static {v0}, Lcom/samsung/android/scloud/quota/StorageList;->access$2000(Lcom/samsung/android/scloud/quota/StorageList;)V

    goto/16 :goto_0

    .line 1060
    :pswitch_2
    const-string v0, "StorageList"

    const-string v1, "Handle Quota end"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1061
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mIsBackedupData:Ljava/lang/Boolean;
    invoke-static {v0}, Lcom/samsung/android/scloud/quota/StorageList;->access$1800(Lcom/samsung/android/scloud/quota/StorageList;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1062
    const-string v0, "StorageList"

    const-string v1, "Call setSummary()"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1063
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # invokes: Lcom/samsung/android/scloud/quota/StorageList;->setSummary()V
    invoke-static {v0}, Lcom/samsung/android/scloud/quota/StorageList;->access$2100(Lcom/samsung/android/scloud/quota/StorageList;)V

    goto/16 :goto_0

    .line 1069
    :pswitch_3
    const-string v0, "StorageList"

    const-string v1, "No Response from Quota Server"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1070
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mIsBackedupData:Ljava/lang/Boolean;
    invoke-static {v0}, Lcom/samsung/android/scloud/quota/StorageList;->access$1800(Lcom/samsung/android/scloud/quota/StorageList;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1071
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->isActivityRunning:Ljava/lang/Boolean;
    invoke-static {v0}, Lcom/samsung/android/scloud/quota/StorageList;->access$2200(Lcom/samsung/android/scloud/quota/StorageList;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1072
    const-string v0, "StorageList"

    const-string v1, "Disabled all the prefrences and setsummary to unknown"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1073
    sget-object v0, Lcom/samsung/android/scloud/quota/StorageList;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    const v2, 0x7f070065

    invoke-virtual {v1, v2}, Lcom/samsung/android/scloud/quota/StorageList;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1076
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mUsageBarPreference:Lcom/samsung/android/scloud/quota/UsageBarPreference;
    invoke-static {v0}, Lcom/samsung/android/scloud/quota/StorageList;->access$2300(Lcom/samsung/android/scloud/quota/StorageList;)Lcom/samsung/android/scloud/quota/UsageBarPreference;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    invoke-virtual {v1, v4}, Lcom/samsung/android/scloud/quota/StorageList;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/quota/UsageBarPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1078
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mPrefBackupData:Landroid/preference/Preference;
    invoke-static {v0}, Lcom/samsung/android/scloud/quota/StorageList;->access$2400(Lcom/samsung/android/scloud/quota/StorageList;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 1079
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mPrefPlan:Landroid/preference/Preference;
    invoke-static {v0}, Lcom/samsung/android/scloud/quota/StorageList;->access$2500(Lcom/samsung/android/scloud/quota/StorageList;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 1080
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mPrefSNote:Landroid/preference/Preference;
    invoke-static {v0}, Lcom/samsung/android/scloud/quota/StorageList;->access$2600(Lcom/samsung/android/scloud/quota/StorageList;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 1081
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mPrefSNote3:Landroid/preference/Preference;
    invoke-static {v0}, Lcom/samsung/android/scloud/quota/StorageList;->access$2700(Lcom/samsung/android/scloud/quota/StorageList;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 1082
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mPrefBookmarks:Landroid/preference/Preference;
    invoke-static {v0}, Lcom/samsung/android/scloud/quota/StorageList;->access$2800(Lcom/samsung/android/scloud/quota/StorageList;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 1083
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mPrefContacts:Landroid/preference/Preference;
    invoke-static {v0}, Lcom/samsung/android/scloud/quota/StorageList;->access$2900(Lcom/samsung/android/scloud/quota/StorageList;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 1084
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mPrefSPlanner:Landroid/preference/Preference;
    invoke-static {v0}, Lcom/samsung/android/scloud/quota/StorageList;->access$3000(Lcom/samsung/android/scloud/quota/StorageList;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 1085
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mPrefPinboard:Landroid/preference/Preference;
    invoke-static {v0}, Lcom/samsung/android/scloud/quota/StorageList;->access$3100(Lcom/samsung/android/scloud/quota/StorageList;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 1087
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mPrefPlan:Landroid/preference/Preference;
    invoke-static {v0}, Lcom/samsung/android/scloud/quota/StorageList;->access$2500(Lcom/samsung/android/scloud/quota/StorageList;)Landroid/preference/Preference;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    invoke-virtual {v1, v4}, Lcom/samsung/android/scloud/quota/StorageList;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1088
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mPrefBackupData:Landroid/preference/Preference;
    invoke-static {v0}, Lcom/samsung/android/scloud/quota/StorageList;->access$2400(Lcom/samsung/android/scloud/quota/StorageList;)Landroid/preference/Preference;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    invoke-virtual {v1, v4}, Lcom/samsung/android/scloud/quota/StorageList;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1090
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mPrefSNote:Landroid/preference/Preference;
    invoke-static {v0}, Lcom/samsung/android/scloud/quota/StorageList;->access$2600(Lcom/samsung/android/scloud/quota/StorageList;)Landroid/preference/Preference;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    invoke-virtual {v1, v4}, Lcom/samsung/android/scloud/quota/StorageList;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1092
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mPrefSNote3:Landroid/preference/Preference;
    invoke-static {v0}, Lcom/samsung/android/scloud/quota/StorageList;->access$2700(Lcom/samsung/android/scloud/quota/StorageList;)Landroid/preference/Preference;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    invoke-virtual {v1, v4}, Lcom/samsung/android/scloud/quota/StorageList;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1094
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mPrefBookmarks:Landroid/preference/Preference;
    invoke-static {v0}, Lcom/samsung/android/scloud/quota/StorageList;->access$2800(Lcom/samsung/android/scloud/quota/StorageList;)Landroid/preference/Preference;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    invoke-virtual {v1, v4}, Lcom/samsung/android/scloud/quota/StorageList;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1096
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mPrefContacts:Landroid/preference/Preference;
    invoke-static {v0}, Lcom/samsung/android/scloud/quota/StorageList;->access$2900(Lcom/samsung/android/scloud/quota/StorageList;)Landroid/preference/Preference;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    invoke-virtual {v1, v4}, Lcom/samsung/android/scloud/quota/StorageList;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1098
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mPrefPinboard:Landroid/preference/Preference;
    invoke-static {v0}, Lcom/samsung/android/scloud/quota/StorageList;->access$3100(Lcom/samsung/android/scloud/quota/StorageList;)Landroid/preference/Preference;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    invoke-virtual {v1, v4}, Lcom/samsung/android/scloud/quota/StorageList;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1100
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mPrefSPlanner:Landroid/preference/Preference;
    invoke-static {v0}, Lcom/samsung/android/scloud/quota/StorageList;->access$3000(Lcom/samsung/android/scloud/quota/StorageList;)Landroid/preference/Preference;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    invoke-virtual {v1, v4}, Lcom/samsung/android/scloud/quota/StorageList;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 1107
    :pswitch_4
    const-string v0, "StorageList"

    const-string v1, "Bad Access Token , Again call for Auth inforamtion"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1108
    new-instance v0, Lcom/samsung/android/scloud/quota/StorageList$AccessTokenclass;

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/StorageList$4;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/scloud/quota/StorageList$AccessTokenclass;-><init>(Lcom/samsung/android/scloud/quota/StorageList;Lcom/samsung/android/scloud/quota/StorageList$1;)V

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, ""

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/quota/StorageList$AccessTokenclass;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 950
    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
