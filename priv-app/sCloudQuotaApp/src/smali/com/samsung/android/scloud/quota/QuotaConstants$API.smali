.class public interface abstract Lcom/samsung/android/scloud/quota/QuotaConstants$API;
.super Ljava/lang/Object;
.source "QuotaConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/quota/QuotaConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "API"
.end annotation


# static fields
.field public static final AUTH_OAUTH2_GETUSERINFO:Ljava/lang/String; = "410"

.field public static final AUTH_OAUTH2_GETUSERINFOWEB:Ljava/lang/String; = "411_"

.field public static final BNR_BACKUPREADY:Ljava/lang/String; = "610"

.field public static final BNR_CLEAR:Ljava/lang/String; = "620"

.field public static final BNR_CLEARALL:Ljava/lang/String; = "621"

.field public static final BNR_COMMIT:Ljava/lang/String; = "613"

.field public static final BNR_DELETE:Ljava/lang/String; = "612"

.field public static final BNR_DETAILS:Ljava/lang/String; = "615"

.field public static final BNR_RELEASECLEARED:Ljava/lang/String; = "622"

.field public static final BNR_RESTOREITEM:Ljava/lang/String; = "618"

.field public static final BNR_RESTOREITEMS:Ljava/lang/String; = "619"

.field public static final BNR_RESTOREKEYS:Ljava/lang/String; = "617"

.field public static final BNR_RESTOREREADY:Ljava/lang/String; = "616"

.field public static final BNR_ROLLBACK:Ljava/lang/String; = "614"

.field public static final BNR_SET:Ljava/lang/String; = "611"

.field public static final CLOUD_END:Ljava/lang/String; = "812"

.field public static final CLOUD_PREPARE:Ljava/lang/String; = "810"

.field public static final CLOUD_START:Ljava/lang/String; = "811"

.field public static final KVS_DELETE:Ljava/lang/String; = "217"

.field public static final KVS_ITEM:Ljava/lang/String; = "213"

.field public static final KVS_ITEMS:Ljava/lang/String; = "214"

.field public static final KVS_KEY:Ljava/lang/String; = "210"

.field public static final KVS_KEYS:Ljava/lang/String; = "211"

.field public static final KVS_SET:Ljava/lang/String; = "216"

.field public static final KVS_TIMESTAMP:Ljava/lang/String; = "215"

.field public static final KVS_UPDATES:Ljava/lang/String; = "212"

.field public static final ORS_COPY:Ljava/lang/String; = "317"

.field public static final ORS_CREATE:Ljava/lang/String; = "310"

.field public static final ORS_DELETE:Ljava/lang/String; = "311"

.field public static final ORS_DOWNLOAD:Ljava/lang/String; = "316"

.field public static final ORS_GET:Ljava/lang/String; = "318"

.field public static final ORS_LIST:Ljava/lang/String; = "314"

.field public static final ORS_MOVE:Ljava/lang/String; = "313"

.field public static final ORS_RENAME:Ljava/lang/String; = "312"

.field public static final ORS_SET:Ljava/lang/String; = "319"

.field public static final ORS_UPLOAD:Ljava/lang/String; = "315"

.field public static final SERVICE_ACTIVATE:Ljava/lang/String; = "510"

.field public static final SERVICE_ACTIVATEON:Ljava/lang/String; = "511"

.field public static final SERVICE_DEACTIVATE:Ljava/lang/String; = "512"

.field public static final SERVICE_GETDEVICELIST:Ljava/lang/String; = "513"

.field public static final USER_QUOTA_ALL:Ljava/lang/String; = "715"

.field public static final USER_QUOTA_APPID:Ljava/lang/String; = "711"

.field public static final USER_QUOTA_CID:Ljava/lang/String; = "713"

.field public static final USER_QUOTA_DEVICE:Ljava/lang/String; = "714"

.field public static final USER_QUOTA_TOTAL:Ljava/lang/String; = "710"

.field public static final USER_QUOTA_USER:Ljava/lang/String; = "712"
