.class public Lcom/samsung/android/scloud/quota/NotificationActivity;
.super Landroid/app/Activity;
.source "NotificationActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "NotificationActivity"


# instance fields
.field private mDialog:Landroid/app/Dialog;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/NotificationActivity;->mDialog:Landroid/app/Dialog;

    return-void
.end method

.method static synthetic access$002(Lcom/samsung/android/scloud/quota/NotificationActivity;Landroid/app/Dialog;)Landroid/app/Dialog;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/NotificationActivity;
    .param p1, "x1"    # Landroid/app/Dialog;

    .prologue
    .line 16
    iput-object p1, p0, Lcom/samsung/android/scloud/quota/NotificationActivity;->mDialog:Landroid/app/Dialog;

    return-object p1
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 25
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 28
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 157
    const-string v0, "NotificationActivity"

    const-string v1, "onDestroy() called"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 159
    return-void
.end method

.method protected onResume()V
    .locals 8

    .prologue
    const/4 v7, -0x1

    .line 33
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 34
    const-string v0, "NotificationActivity"

    const-string v6, "onResume()"

    invoke-static {v0, v6}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/NotificationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/NotificationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v6, "android.intent.action.VIEW"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/NotificationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v6, "DIALOG"

    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 40
    .local v1, "dialog":I
    if-eq v1, v7, :cond_0

    .line 42
    const-string v0, "NotificationActivity"

    const-string v6, "dialog loaded from onResume"

    invoke-static {v0, v6}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/NotificationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v6, "current_plan_message"

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 44
    .local v2, "currentPlan":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/NotificationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v6, "next_plan_message"

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 45
    .local v3, "nextPlan":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/NotificationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v6, "date_field"

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 46
    .local v4, "date":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/NotificationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v6, "commerce_target"

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 47
    .local v5, "commerceTarget":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/NotificationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v6, "DIALOG"

    invoke-virtual {v0, v6}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    move-object v0, p0

    .line 48
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/scloud/quota/NotificationActivity;->showDialogbyID(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    .end local v1    # "dialog":I
    .end local v2    # "currentPlan":Ljava/lang/String;
    .end local v3    # "nextPlan":Ljava/lang/String;
    .end local v4    # "date":Ljava/lang/String;
    .end local v5    # "commerceTarget":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public showDialogbyID(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p1, "id"    # I
    .param p2, "currentPlan"    # Ljava/lang/String;
    .param p3, "nextPlan"    # Ljava/lang/String;
    .param p4, "date"    # Ljava/lang/String;
    .param p5, "commerceTarget"    # Ljava/lang/String;

    .prologue
    .line 57
    const-string v5, "NotificationActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "showDialogbyID :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    packed-switch p1, :pswitch_data_0

    .line 132
    :goto_0
    iget-object v5, p0, Lcom/samsung/android/scloud/quota/NotificationActivity;->mDialog:Landroid/app/Dialog;

    new-instance v6, Lcom/samsung/android/scloud/quota/NotificationActivity$5;

    invoke-direct {v6, p0}, Lcom/samsung/android/scloud/quota/NotificationActivity$5;-><init>(Lcom/samsung/android/scloud/quota/NotificationActivity;)V

    invoke-virtual {v5, v6}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 142
    iget-object v5, p0, Lcom/samsung/android/scloud/quota/NotificationActivity;->mDialog:Landroid/app/Dialog;

    new-instance v6, Lcom/samsung/android/scloud/quota/NotificationActivity$6;

    invoke-direct {v6, p0}, Lcom/samsung/android/scloud/quota/NotificationActivity$6;-><init>(Lcom/samsung/android/scloud/quota/NotificationActivity;)V

    invoke-virtual {v5, v6}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 152
    iget-object v5, p0, Lcom/samsung/android/scloud/quota/NotificationActivity;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v5}, Landroid/app/Dialog;->show()V

    .line 153
    return-void

    .line 61
    :pswitch_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 62
    .local v2, "sb12":Ljava/lang/StringBuilder;
    invoke-static {p4}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/32 v8, 0x100000

    div-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 63
    .local v4, "value":Ljava/lang/Long;
    invoke-virtual {v4}, Ljava/lang/Long;->doubleValue()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    .line 64
    .local v0, "d":Ljava/lang/Double;
    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Math;->floor(D)D

    move-result-wide v6

    double-to-int v1, v6

    .line 65
    .local v1, "quotaVal":I
    const-string v5, "Y"

    invoke-virtual {v5, p5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 66
    const v5, 0x7f070001

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {p0, v5, v6}, Lcom/samsung/android/scloud/quota/NotificationActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    :goto_1
    const-string v5, "\n"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    const-string v5, "\n"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    new-instance v5, Landroid/app/AlertDialog$Builder;

    invoke-direct {v5, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v6, 0x7f070003

    invoke-virtual {p0, v6}, Lcom/samsung/android/scloud/quota/NotificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    const v6, 0x104000a

    invoke-virtual {p0, v6}, Lcom/samsung/android/scloud/quota/NotificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/samsung/android/scloud/quota/NotificationActivity$2;

    invoke-direct {v7, p0}, Lcom/samsung/android/scloud/quota/NotificationActivity$2;-><init>(Lcom/samsung/android/scloud/quota/NotificationActivity;)V

    invoke-virtual {v5, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    const v6, 0x7f070074

    invoke-virtual {p0, v6}, Lcom/samsung/android/scloud/quota/NotificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/samsung/android/scloud/quota/NotificationActivity$1;

    invoke-direct {v7, p0}, Lcom/samsung/android/scloud/quota/NotificationActivity$1;-><init>(Lcom/samsung/android/scloud/quota/NotificationActivity;)V

    invoke-virtual {v5, v6, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/scloud/quota/NotificationActivity;->mDialog:Landroid/app/Dialog;

    goto/16 :goto_0

    .line 68
    :cond_0
    const v5, 0x7f070002

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {p0, v5, v6}, Lcom/samsung/android/scloud/quota/NotificationActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 98
    .end local v0    # "d":Ljava/lang/Double;
    .end local v1    # "quotaVal":I
    .end local v2    # "sb12":Ljava/lang/StringBuilder;
    .end local v4    # "value":Ljava/lang/Long;
    :pswitch_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 99
    .local v3, "sb13":Ljava/lang/StringBuilder;
    const v5, 0x7f070057

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/quota/NotificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 100
    const-string v5, "\n"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    const-string v5, "\n"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 102
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    new-instance v5, Landroid/app/AlertDialog$Builder;

    invoke-direct {v5, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v6, 0x7f070059

    invoke-virtual {p0, v6}, Lcom/samsung/android/scloud/quota/NotificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    const v6, 0x104000a

    invoke-virtual {p0, v6}, Lcom/samsung/android/scloud/quota/NotificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/samsung/android/scloud/quota/NotificationActivity$4;

    invoke-direct {v7, p0}, Lcom/samsung/android/scloud/quota/NotificationActivity$4;-><init>(Lcom/samsung/android/scloud/quota/NotificationActivity;)V

    invoke-virtual {v5, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    const v6, 0x7f070074

    invoke-virtual {p0, v6}, Lcom/samsung/android/scloud/quota/NotificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/samsung/android/scloud/quota/NotificationActivity$3;

    invoke-direct {v7, p0}, Lcom/samsung/android/scloud/quota/NotificationActivity$3;-><init>(Lcom/samsung/android/scloud/quota/NotificationActivity;)V

    invoke-virtual {v5, v6, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/scloud/quota/NotificationActivity;->mDialog:Landroid/app/Dialog;

    goto/16 :goto_0

    .line 58
    nop

    :pswitch_data_0
    .packed-switch 0xd
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
