.class public interface abstract Lcom/samsung/android/scloud/quota/QuotaConstants$REQUEST_TYPE;
.super Ljava/lang/Object;
.source "QuotaConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/quota/QuotaConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "REQUEST_TYPE"
.end annotation


# static fields
.field public static final REQUEST_GET:I = 0x1

.field public static final REQUEST_GET_FILE:I = 0x4

.field public static final REQUEST_GET_META:I = 0x5

.field public static final REQUEST_GET_STORAGE:I = 0x3

.field public static final REQUEST_POST:I = 0x0

.field public static final REQUEST_POST_MULTIPART:I = 0x2
