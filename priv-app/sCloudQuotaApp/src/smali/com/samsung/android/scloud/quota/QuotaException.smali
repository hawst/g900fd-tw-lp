.class public Lcom/samsung/android/scloud/quota/QuotaException;
.super Ljava/lang/Exception;
.source "QuotaException.java"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private mExceptionCode:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "code"    # I

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    .line 59
    iput p1, p0, Lcom/samsung/android/scloud/quota/QuotaException;->mExceptionCode:I

    .line 63
    return-void
.end method


# virtual methods
.method public getmExceptionCode()I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/samsung/android/scloud/quota/QuotaException;->mExceptionCode:I

    return v0
.end method
