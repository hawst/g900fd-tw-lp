.class Lcom/samsung/android/scloud/quota/BackupList$BackupListAdapter;
.super Landroid/widget/BaseAdapter;
.source "BackupList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/quota/BackupList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "BackupListAdapter"
.end annotation


# instance fields
.field mBackupList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/scloud/quota/BackupDetails;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/samsung/android/scloud/quota/BackupList;


# direct methods
.method public constructor <init>(Lcom/samsung/android/scloud/quota/BackupList;Landroid/content/Context;Ljava/util/List;)V
    .locals 0
    .param p2, "c"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/scloud/quota/BackupDetails;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 630
    .local p3, "backupList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/scloud/quota/BackupDetails;>;"
    iput-object p1, p0, Lcom/samsung/android/scloud/quota/BackupList$BackupListAdapter;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 631
    iput-object p2, p0, Lcom/samsung/android/scloud/quota/BackupList$BackupListAdapter;->mContext:Landroid/content/Context;

    .line 632
    iput-object p3, p0, Lcom/samsung/android/scloud/quota/BackupList$BackupListAdapter;->mBackupList:Ljava/util/List;

    .line 633
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 640
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList$BackupListAdapter;->mBackupList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 649
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 658
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 670
    move v2, p1

    .line 672
    .local v2, "posn":I
    move-object v3, p2

    .line 674
    .local v3, "view":Landroid/view/View;
    if-nez v3, :cond_2

    .line 675
    iget-object v4, p0, Lcom/samsung/android/scloud/quota/BackupList$BackupListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 676
    .local v1, "layoutInflator":Landroid/view/LayoutInflater;
    invoke-static {}, Lcom/samsung/android/scloud/quota/QuotaUtils;->isViewType_light()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 677
    const v4, 0x7f030001

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 679
    :cond_0
    new-instance v0, Lcom/samsung/android/scloud/quota/BackupList$ViewHolder;

    invoke-direct {v0}, Lcom/samsung/android/scloud/quota/BackupList$ViewHolder;-><init>()V

    .line 680
    .local v0, "holder":Lcom/samsung/android/scloud/quota/BackupList$ViewHolder;
    const v4, 0x7f0a0001

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    iput-object v4, v0, Lcom/samsung/android/scloud/quota/BackupList$ViewHolder;->cb:Landroid/widget/CheckBox;

    .line 681
    iget-object v4, p0, Lcom/samsung/android/scloud/quota/BackupList$BackupListAdapter;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-boolean v4, v4, Lcom/samsung/android/scloud/quota/BackupList;->flag:Z

    if-eqz v4, :cond_1

    .line 683
    iget-object v4, v0, Lcom/samsung/android/scloud/quota/BackupList$ViewHolder;->cb:Landroid/widget/CheckBox;

    invoke-virtual {v4, v6}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 694
    :goto_0
    const v4, 0x7f0a0003

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v0, Lcom/samsung/android/scloud/quota/BackupList$ViewHolder;->deviceName:Landroid/widget/TextView;

    .line 696
    const v4, 0x7f0a0005

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v0, Lcom/samsung/android/scloud/quota/BackupList$ViewHolder;->latestBackupDate:Landroid/widget/TextView;

    .line 698
    const v4, 0x7f0a0006

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v0, Lcom/samsung/android/scloud/quota/BackupList$ViewHolder;->size:Landroid/widget/TextView;

    .line 699
    invoke-virtual {v3, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 719
    .end local v1    # "layoutInflator":Landroid/view/LayoutInflater;
    :goto_1
    iget-object v4, v0, Lcom/samsung/android/scloud/quota/BackupList$ViewHolder;->cb:Landroid/widget/CheckBox;

    iget-object v5, p0, Lcom/samsung/android/scloud/quota/BackupList$BackupListAdapter;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v5, v5, Lcom/samsung/android/scloud/quota/BackupList;->mListSelected:[Z

    aget-boolean v5, v5, p1

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 720
    iget-object v5, v0, Lcom/samsung/android/scloud/quota/BackupList$ViewHolder;->deviceName:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/samsung/android/scloud/quota/BackupList$BackupListAdapter;->mBackupList:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/scloud/quota/BackupDetails;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/quota/BackupDetails;->getModelName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 721
    iget-object v5, v0, Lcom/samsung/android/scloud/quota/BackupList$ViewHolder;->latestBackupDate:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/samsung/android/scloud/quota/BackupList$BackupListAdapter;->mBackupList:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/scloud/quota/BackupDetails;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/quota/BackupDetails;->latestBackupDate()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 723
    iget-object v5, v0, Lcom/samsung/android/scloud/quota/BackupList$ViewHolder;->size:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/samsung/android/scloud/quota/BackupList$BackupListAdapter;->mBackupList:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/scloud/quota/BackupDetails;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/quota/BackupDetails;->size()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 725
    return-object v3

    .line 687
    .restart local v1    # "layoutInflator":Landroid/view/LayoutInflater;
    :cond_1
    iget-object v4, v0, Lcom/samsung/android/scloud/quota/BackupList$ViewHolder;->cb:Landroid/widget/CheckBox;

    invoke-virtual {v4, v7}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 688
    invoke-virtual {v3, v6}, Landroid/view/View;->setClickable(Z)V

    .line 689
    invoke-virtual {v3, v6}, Landroid/view/View;->setSelected(Z)V

    goto :goto_0

    .line 703
    .end local v0    # "holder":Lcom/samsung/android/scloud/quota/BackupList$ViewHolder;
    .end local v1    # "layoutInflator":Landroid/view/LayoutInflater;
    :cond_2
    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/quota/BackupList$ViewHolder;

    .line 704
    .restart local v0    # "holder":Lcom/samsung/android/scloud/quota/BackupList$ViewHolder;
    iget-object v4, p0, Lcom/samsung/android/scloud/quota/BackupList$BackupListAdapter;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-boolean v4, v4, Lcom/samsung/android/scloud/quota/BackupList;->flag:Z

    if-eqz v4, :cond_3

    .line 706
    iget-object v4, v0, Lcom/samsung/android/scloud/quota/BackupList$ViewHolder;->cb:Landroid/widget/CheckBox;

    invoke-virtual {v4, v6}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_1

    .line 710
    :cond_3
    iget-object v4, v0, Lcom/samsung/android/scloud/quota/BackupList$ViewHolder;->cb:Landroid/widget/CheckBox;

    invoke-virtual {v4, v7}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 711
    invoke-virtual {v3, v6}, Landroid/view/View;->setClickable(Z)V

    .line 712
    invoke-virtual {v3, v6}, Landroid/view/View;->setSelected(Z)V

    .line 713
    invoke-virtual {v3, v6}, Landroid/view/View;->setFocusable(Z)V

    goto :goto_1
.end method
