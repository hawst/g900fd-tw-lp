.class public interface abstract Lcom/samsung/android/scloud/quota/ServerCommonConstants;
.super Ljava/lang/Object;
.source "ServerCommonConstants.java"


# static fields
.field public static final ACCESS_TOKEN_PARM:Ljava/lang/String; = "&access_token="

.field public static final ACCESS_TOKEN_PARM_STORAGE:Ljava/lang/String; = "&access_token="

.field public static final BAD_ACCESS_TOKEN:I = 0x4a40

.field public static final BNR_SERVER_MIGRATION:I = 0x4e27

.field public static final BNR_SERVER_NOT_READY:I = 0x4e26

.field public static final CID_PARM:Ljava/lang/String; = "&cid="

.field public static final CONTENT_TYPE_URI:Ljava/lang/String; = "Application/JSON;charset=UTF-8"

.field public static final DEVICE_ID_PARM:Ljava/lang/String; = "&did="

.field public static final KEY:Ljava/lang/String; = "&key="

.field public static final KVS_SERVER_STORAGE_FULL:I = 0x4e23

.field public static final ORS_SERVER_STORAGE_FULL:I = 0x4e24

.field public static final USER_ID_PARM:Ljava/lang/String; = "&uid="

.field public static final USER_ID_PARM_STORAGE:Ljava/lang/String; = "&uid="
