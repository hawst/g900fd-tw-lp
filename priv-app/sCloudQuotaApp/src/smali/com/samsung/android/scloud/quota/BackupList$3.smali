.class Lcom/samsung/android/scloud/quota/BackupList$3;
.super Ljava/lang/Object;
.source "BackupList.java"

# interfaces
.implements Landroid/widget/AdapterView$OnTwMultiSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/quota/BackupList;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/quota/BackupList;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/quota/BackupList;)V
    .locals 0

    .prologue
    .line 171
    iput-object p1, p0, Lcom/samsung/android/scloud/quota/BackupList$3;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnTwMultiSelectStart(II)V
    .locals 3
    .param p1, "startX"    # I
    .param p2, "startY"    # I

    .prologue
    .line 178
    const-string v0, "BackupList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MultiSelect StartX = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "MultiSelect StartY = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    return-void
.end method

.method public OnTwMultiSelectStop(II)V
    .locals 7
    .param p1, "endX"    # I
    .param p2, "endY"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 184
    const-string v2, "BackupList"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MultiSelect endX = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "MultiSelect endY = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/BackupList$3;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    # invokes: Lcom/samsung/android/scloud/quota/BackupList;->registerSpinner()V
    invoke-static {v2}, Lcom/samsung/android/scloud/quota/BackupList;->access$200(Lcom/samsung/android/scloud/quota/BackupList;)V

    .line 186
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/BackupList$3;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-boolean v2, v2, Lcom/samsung/android/scloud/quota/BackupList;->flag:Z

    if-nez v2, :cond_0

    .line 188
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/BackupList$3;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iput-boolean v6, v2, Lcom/samsung/android/scloud/quota/BackupList;->flag:Z

    .line 189
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/BackupList$3;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iput-boolean v5, v2, Lcom/samsung/android/scloud/quota/BackupList;->longFlag:Z

    .line 195
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/BackupList$3;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    # getter for: Lcom/samsung/android/scloud/quota/BackupList;->mTwDragSelectedItemArray:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/scloud/quota/BackupList;->access$300(Lcom/samsung/android/scloud/quota/BackupList;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 197
    .local v1, "mTwDragSelectedViewPosition":Ljava/lang/Integer;
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/BackupList$3;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v2, v2, Lcom/samsung/android/scloud/quota/BackupList;->mListSelected:[Z

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aget-boolean v2, v2, v3

    if-eqz v2, :cond_1

    .line 198
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/BackupList$3;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v2, v2, Lcom/samsung/android/scloud/quota/BackupList;->mListSelected:[Z

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aput-boolean v5, v2, v3

    .line 199
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/BackupList$3;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v2, v2, Lcom/samsung/android/scloud/quota/BackupList;->list:Landroid/widget/ListView;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v2, v3, v5}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 200
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/BackupList$3;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    # operator-- for: Lcom/samsung/android/scloud/quota/BackupList;->count:I
    invoke-static {v2}, Lcom/samsung/android/scloud/quota/BackupList;->access$410(Lcom/samsung/android/scloud/quota/BackupList;)I

    goto :goto_0

    .line 202
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/BackupList$3;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v2, v2, Lcom/samsung/android/scloud/quota/BackupList;->mListSelected:[Z

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aput-boolean v6, v2, v3

    .line 203
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/BackupList$3;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v2, v2, Lcom/samsung/android/scloud/quota/BackupList;->list:Landroid/widget/ListView;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v2, v3, v5}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 204
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/BackupList$3;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    # operator++ for: Lcom/samsung/android/scloud/quota/BackupList;->count:I
    invoke-static {v2}, Lcom/samsung/android/scloud/quota/BackupList;->access$408(Lcom/samsung/android/scloud/quota/BackupList;)I

    goto :goto_0

    .line 208
    .end local v1    # "mTwDragSelectedViewPosition":Ljava/lang/Integer;
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/BackupList$3;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v2, v2, Lcom/samsung/android/scloud/quota/BackupList;->backupListAdapter:Lcom/samsung/android/scloud/quota/BackupList$BackupListAdapter;

    invoke-virtual {v2}, Lcom/samsung/android/scloud/quota/BackupList$BackupListAdapter;->notifyDataSetChanged()V

    .line 209
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/BackupList$3;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    # getter for: Lcom/samsung/android/scloud/quota/BackupList;->mTwDragSelectedItemArray:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/scloud/quota/BackupList;->access$300(Lcom/samsung/android/scloud/quota/BackupList;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 210
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/BackupList$3;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    invoke-virtual {v2}, Lcom/samsung/android/scloud/quota/BackupList;->setDropDownList()V

    .line 211
    return-void
.end method

.method public onTwMultiSelected(Landroid/widget/AdapterView;Landroid/view/View;IJZZZ)V
    .locals 3
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .param p6, "isShiftpress"    # Z
    .param p7, "isCtrlpress"    # Z
    .param p8, "isPenpress"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJZZZ)V"
        }
    .end annotation

    .prologue
    .line 173
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const-string v0, "BackupList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onTwMultiSelected call position = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  PenPress is = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList$3;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    invoke-virtual {v0, p2, p3}, Lcom/samsung/android/scloud/quota/BackupList;->addDragItemToListArray(Landroid/view/View;I)V

    .line 175
    return-void
.end method
