.class public final Lcom/samsung/android/scloud/quota/response/KVSResponse;
.super Ljava/lang/Object;
.source "KVSResponse.java"


# static fields
.field private static final BNR:Ljava/lang/String; = "bnr"

.field private static final CDID:Ljava/lang/String; = "cdid"

.field private static final CID:Ljava/lang/String; = "cid"

.field private static final CID_USAGE:Ljava/lang/String; = "usage"

.field private static final DELETED:Ljava/lang/String; = "deleted"

.field private static final DEVICENAME:Ljava/lang/String; = "name"

.field private static final DID:Ljava/lang/String; = "did"

.field private static final FREE_QUOTA:Ljava/lang/String; = "free_quota"

.field private static final KEY:Ljava/lang/String; = "key"

.field private static final LIST:Ljava/lang/String; = "list"

.field private static final MAX_TIMESTAMP:Ljava/lang/String; = "maxTimestamp"

.field private static final NEXTKEY:Ljava/lang/String; = "nextKey"

.field private static final RCODE:Ljava/lang/String; = "rcode"

.field private static final SERVER_TIMESTAMP:Ljava/lang/String; = "serverTimestamp"

.field private static final SIZE:Ljava/lang/String; = "size"

.field private static final SYNC:Ljava/lang/String; = "sync"

.field public static final TAG:Ljava/lang/String; = "KVSResponse"

.field private static final TIMESTAMP:Ljava/lang/String; = "date"

.field private static final TOTAL_QUOTA:Ljava/lang/String; = "total_quota"

.field private static final TOTAL_USAGE:Ljava/lang/String; = "total_usage"

.field private static final USAGE:Ljava/lang/String; = "usage"

.field private static final VALUE:Ljava/lang/String; = "value"


# instance fields
.field private jsonBody:Ljava/lang/String;

.field private mBNRListSize:I

.field private final mBackupDetailsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/scloud/quota/BackupDetailsList;",
            ">;"
        }
    .end annotation
.end field

.field private mCid:Ljava/lang/String;

.field private mCidUSage:Ljava/lang/Long;

.field private mFreeQuota:Ljava/lang/Long;

.field private mMapCIDUsage:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mMaxTimeStamp:Ljava/lang/String;

.field private mNextKey:Ljava/lang/String;

.field private mResponseCode:I

.field private mServerTimeStamp:Ljava/lang/Long;

.field private mTotalQuota:Ljava/lang/Long;

.field private mTotalUsage:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    iput-object v1, p0, Lcom/samsung/android/scloud/quota/response/KVSResponse;->mMaxTimeStamp:Ljava/lang/String;

    .line 112
    iput-object v1, p0, Lcom/samsung/android/scloud/quota/response/KVSResponse;->mNextKey:Ljava/lang/String;

    .line 131
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/response/KVSResponse;->mBackupDetailsList:Ljava/util/ArrayList;

    .line 133
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/scloud/quota/response/KVSResponse;->mBNRListSize:I

    .line 135
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/response/KVSResponse;->mMapCIDUsage:Ljava/util/HashMap;

    .line 137
    iput-object v1, p0, Lcom/samsung/android/scloud/quota/response/KVSResponse;->mTotalUsage:Ljava/lang/Long;

    .line 139
    iput-object v1, p0, Lcom/samsung/android/scloud/quota/response/KVSResponse;->mTotalQuota:Ljava/lang/Long;

    .line 141
    iput-object v1, p0, Lcom/samsung/android/scloud/quota/response/KVSResponse;->mCidUSage:Ljava/lang/Long;

    .line 143
    iput-object v1, p0, Lcom/samsung/android/scloud/quota/response/KVSResponse;->mCid:Ljava/lang/String;

    .line 145
    iput-object v1, p0, Lcom/samsung/android/scloud/quota/response/KVSResponse;->mFreeQuota:Ljava/lang/Long;

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    .line 155
    iput-object v1, p0, Lcom/samsung/android/scloud/quota/response/KVSResponse;->mMaxTimeStamp:Ljava/lang/String;

    .line 157
    iput-object v1, p0, Lcom/samsung/android/scloud/quota/response/KVSResponse;->mNextKey:Ljava/lang/String;

    .line 159
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/scloud/quota/response/KVSResponse;->mResponseCode:I

    .line 161
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/response/KVSResponse;->mTotalUsage:Ljava/lang/Long;

    .line 163
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/response/KVSResponse;->mTotalQuota:Ljava/lang/Long;

    .line 165
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/response/KVSResponse;->mCidUSage:Ljava/lang/Long;

    .line 167
    iput-object v1, p0, Lcom/samsung/android/scloud/quota/response/KVSResponse;->mCid:Ljava/lang/String;

    .line 171
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/response/KVSResponse;->mServerTimeStamp:Ljava/lang/Long;

    .line 173
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/response/KVSResponse;->mBackupDetailsList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 175
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/response/KVSResponse;->mMapCIDUsage:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 177
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/response/KVSResponse;->mFreeQuota:Ljava/lang/Long;

    .line 185
    return-void
.end method

.method public fromJSON(Ljava/lang/Object;)V
    .locals 18
    .param p1, "json"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 320
    move-object/from16 v2, p1

    check-cast v2, Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/scloud/quota/response/KVSResponse;->jsonBody:Ljava/lang/String;

    .line 322
    new-instance v11, Lorg/json/JSONObject;

    check-cast p1, Ljava/lang/String;

    .end local p1    # "json":Ljava/lang/Object;
    move-object/from16 v0, p1

    invoke-direct {v11, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 324
    .local v11, "jsonObj":Lorg/json/JSONObject;
    const-string v2, "rcode"

    invoke-virtual {v11, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 326
    const-string v2, "rcode"

    invoke-virtual {v11, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/scloud/quota/response/KVSResponse;->mResponseCode:I

    .line 338
    :cond_0
    const-string v2, "maxTimestamp"

    invoke-virtual {v11, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 340
    const-string v2, "maxTimestamp"

    invoke-virtual {v11, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/scloud/quota/response/KVSResponse;->mMaxTimeStamp:Ljava/lang/String;

    .line 342
    :cond_1
    const-string v2, "nextKey"

    invoke-virtual {v11, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 344
    const-string v2, "nextKey"

    invoke-virtual {v11, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/scloud/quota/response/KVSResponse;->mNextKey:Ljava/lang/String;

    .line 346
    :cond_2
    const-string v2, "serverTimestamp"

    invoke-virtual {v11, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 348
    const-string v2, "serverTimestamp"

    invoke-virtual {v11, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/scloud/quota/response/KVSResponse;->mServerTimeStamp:Ljava/lang/Long;

    .line 351
    :cond_3
    const-string v2, "total_usage"

    invoke-virtual {v11, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 353
    const-string v2, "total_usage"

    invoke-virtual {v11, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/scloud/quota/response/KVSResponse;->mTotalUsage:Ljava/lang/Long;

    .line 355
    :cond_4
    const-string v2, "total_quota"

    invoke-virtual {v11, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 357
    const-string v2, "total_quota"

    invoke-virtual {v11, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/scloud/quota/response/KVSResponse;->mTotalQuota:Ljava/lang/Long;

    .line 359
    :cond_5
    const-string v2, "usage"

    invoke-virtual {v11, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 361
    const-string v2, "usage"

    invoke-virtual {v11, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/scloud/quota/response/KVSResponse;->mCidUSage:Ljava/lang/Long;

    .line 363
    :cond_6
    const-string v2, "cid"

    invoke-virtual {v11, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 365
    const-string v2, "cid"

    invoke-virtual {v11, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/scloud/quota/response/KVSResponse;->mCid:Ljava/lang/String;

    .line 367
    :cond_7
    const-string v2, "list"

    invoke-virtual {v11, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 373
    const-string v2, "list"

    invoke-virtual {v11, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v12

    .line 375
    .local v12, "listitemarray":Lorg/json/JSONArray;
    const-string v2, "Size of listitemarray"

    invoke-virtual {v12}, Lorg/json/JSONArray;->length()I

    move-result v14

    invoke-static {v14}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v14

    invoke-static {v2, v14}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    const/4 v2, 0x0

    invoke-virtual {v12, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v8

    .line 382
    .local v8, "bnrJSONobj":Lorg/json/JSONObject;
    const-string v2, "free_quota"

    invoke-virtual {v8, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 384
    const-string v2, "free_quota"

    invoke-virtual {v8, v2}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/scloud/quota/response/KVSResponse;->mFreeQuota:Ljava/lang/Long;

    .line 387
    :cond_8
    const-string v2, "bnr"

    invoke-virtual {v8, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 389
    const-string v2, "bnr"

    invoke-virtual {v8, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v9

    .line 392
    .local v9, "dataitemarray":Lorg/json/JSONArray;
    invoke-virtual {v9}, Lorg/json/JSONArray;->length()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/scloud/quota/response/KVSResponse;->mBNRListSize:I

    .line 394
    invoke-virtual {v9}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-lez v2, :cond_e

    .line 396
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    invoke-virtual {v9}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v10, v2, :cond_e

    .line 398
    invoke-virtual {v9, v10}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v13

    .line 401
    .local v13, "obj":Lorg/json/JSONObject;
    const/4 v3, 0x0

    .line 403
    .local v3, "deviceid":Ljava/lang/String;
    const/4 v4, 0x0

    .line 405
    .local v4, "cdeviceid":Ljava/lang/String;
    const-wide/16 v14, 0x0

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    .line 407
    .local v5, "usage":Ljava/lang/Long;
    const-wide/16 v14, 0x0

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    .line 409
    .local v7, "timestamp":Ljava/lang/Long;
    const/4 v6, 0x0

    .line 411
    .local v6, "name":Ljava/lang/String;
    const-string v2, "did"

    invoke-virtual {v13, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 412
    const-string v2, "did"

    invoke-virtual {v13, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 413
    :cond_9
    const-string v2, "cdid"

    invoke-virtual {v13, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 414
    const-string v2, "cdid"

    invoke-virtual {v13, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 415
    :cond_a
    const-string v2, "usage"

    invoke-virtual {v13, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 416
    const-string v2, "usage"

    invoke-virtual {v13, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    .line 417
    :cond_b
    const-string v2, "date"

    invoke-virtual {v13, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 418
    const-string v2, "date"

    invoke-virtual {v13, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    .line 422
    :goto_1
    const-string v2, "name"

    invoke-virtual {v13, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 423
    const-string v2, "name"

    invoke-virtual {v13, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 427
    :goto_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/scloud/quota/response/KVSResponse;->mBackupDetailsList:Ljava/util/ArrayList;

    new-instance v2, Lcom/samsung/android/scloud/quota/BackupDetailsList;

    invoke-direct/range {v2 .. v7}, Lcom/samsung/android/scloud/quota/BackupDetailsList;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Long;)V

    invoke-virtual {v14, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 396
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 421
    :cond_c
    const-wide v14, 0x13c710e4150L

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    goto :goto_1

    .line 425
    :cond_d
    move-object v6, v4

    goto :goto_2

    .line 437
    .end local v3    # "deviceid":Ljava/lang/String;
    .end local v4    # "cdeviceid":Ljava/lang/String;
    .end local v5    # "usage":Ljava/lang/Long;
    .end local v6    # "name":Ljava/lang/String;
    .end local v7    # "timestamp":Ljava/lang/Long;
    .end local v9    # "dataitemarray":Lorg/json/JSONArray;
    .end local v10    # "i":I
    .end local v13    # "obj":Lorg/json/JSONObject;
    :cond_e
    const-string v2, "KVSResponse"

    const-string v14, "Check whether it has sync : ?"

    invoke-static {v2, v14}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 438
    const-string v2, "sync"

    invoke-virtual {v8, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 440
    const-string v2, "KVSResponse"

    const-string v14, "Has sync"

    invoke-static {v2, v14}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 442
    const-string v2, "sync"

    invoke-virtual {v8, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v9

    .line 445
    .restart local v9    # "dataitemarray":Lorg/json/JSONArray;
    invoke-virtual {v9}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-lez v2, :cond_10

    .line 446
    const-string v2, "KVSResponse"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Sync length :"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v9}, Lorg/json/JSONArray;->length()I

    move-result v15

    invoke-static {v15}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v2, v14}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 450
    const/4 v10, 0x0

    .restart local v10    # "i":I
    :goto_3
    invoke-virtual {v9}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v10, v2, :cond_f

    .line 452
    invoke-virtual {v9, v10}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v13

    .line 455
    .restart local v13    # "obj":Lorg/json/JSONObject;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/quota/response/KVSResponse;->mMapCIDUsage:Ljava/util/HashMap;

    const-string v14, "cid"

    invoke-virtual {v13, v14}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    const-string v15, "usage"

    invoke-virtual {v13, v15}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    invoke-virtual {v2, v14, v15}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 450
    add-int/lit8 v10, v10, 0x1

    goto :goto_3

    .line 460
    .end local v13    # "obj":Lorg/json/JSONObject;
    :cond_f
    const-string v2, "KVSResponse"

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/scloud/quota/response/KVSResponse;->mMapCIDUsage:Ljava/util/HashMap;

    invoke-virtual {v14}, Ljava/util/HashMap;->size()I

    move-result v14

    invoke-static {v14}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v14

    invoke-static {v2, v14}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 468
    .end local v8    # "bnrJSONobj":Lorg/json/JSONObject;
    .end local v9    # "dataitemarray":Lorg/json/JSONArray;
    .end local v10    # "i":I
    .end local v12    # "listitemarray":Lorg/json/JSONArray;
    :cond_10
    return-void
.end method

.method public getBNRListSize()I
    .locals 1

    .prologue
    .line 310
    iget v0, p0, Lcom/samsung/android/scloud/quota/response/KVSResponse;->mBNRListSize:I

    return v0
.end method

.method public getBnrDetailsList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/scloud/quota/BackupDetailsList;",
            ">;"
        }
    .end annotation

    .prologue
    .line 288
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/response/KVSResponse;->mBackupDetailsList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getCIDUsageMap()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 315
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/response/KVSResponse;->mMapCIDUsage:Ljava/util/HashMap;

    return-object v0
.end method

.method public getCid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/response/KVSResponse;->mCid:Ljava/lang/String;

    return-object v0
.end method

.method public getCidUsage()J
    .locals 2

    .prologue
    .line 272
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/response/KVSResponse;->mCidUSage:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getFreeQuota()J
    .locals 2

    .prologue
    .line 257
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/response/KVSResponse;->mFreeQuota:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getJSONBody()Ljava/lang/String;
    .locals 1

    .prologue
    .line 302
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/response/KVSResponse;->jsonBody:Ljava/lang/String;

    return-object v0
.end method

.method public getMaxTimeStamp()Ljava/lang/String;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/response/KVSResponse;->mMaxTimeStamp:Ljava/lang/String;

    return-object v0
.end method

.method public getNextKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/response/KVSResponse;->mNextKey:Ljava/lang/String;

    return-object v0
.end method

.method public getResponseCode()I
    .locals 1

    .prologue
    .line 189
    iget v0, p0, Lcom/samsung/android/scloud/quota/response/KVSResponse;->mResponseCode:I

    return v0
.end method

.method public getServerTimeStamp()J
    .locals 2

    .prologue
    .line 195
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/response/KVSResponse;->mServerTimeStamp:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getTotalQuota()J
    .locals 2

    .prologue
    .line 264
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/response/KVSResponse;->mTotalQuota:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getTotalUsage()J
    .locals 2

    .prologue
    .line 251
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/response/KVSResponse;->mTotalUsage:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public toJSON()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 472
    const/4 v0, 0x0

    return-object v0
.end method
