.class public Lcom/samsung/android/scloud/quota/BackupDetailsList;
.super Ljava/lang/Object;
.source "BackupDetailsList.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/scloud/quota/BackupDetailsList$BackupTimeComparator;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/scloud/quota/BackupDetailsList;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mBackupSize:Ljava/lang/Long;

.field private mClientDeviceID:Ljava/lang/String;

.field private mDeviceId:Ljava/lang/String;

.field private mModelNumber:Ljava/lang/String;

.field private mTimeStamp:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 72
    new-instance v0, Lcom/samsung/android/scloud/quota/BackupDetailsList$1;

    invoke-direct {v0}, Lcom/samsung/android/scloud/quota/BackupDetailsList$1;-><init>()V

    sput-object v0, Lcom/samsung/android/scloud/quota/BackupDetailsList;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/BackupDetailsList;->mDeviceId:Ljava/lang/String;

    .line 33
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/BackupDetailsList;->mClientDeviceID:Ljava/lang/String;

    .line 34
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/BackupDetailsList;->mBackupSize:Ljava/lang/Long;

    .line 36
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/BackupDetailsList;->mModelNumber:Ljava/lang/String;

    .line 38
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/BackupDetailsList;->mTimeStamp:Ljava/lang/Long;

    .line 39
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Long;)V
    .locals 0
    .param p1, "deviceId"    # Ljava/lang/String;
    .param p2, "clientDeviceID"    # Ljava/lang/String;
    .param p3, "size"    # Ljava/lang/Long;
    .param p4, "model"    # Ljava/lang/String;
    .param p5, "timeStamp"    # Ljava/lang/Long;

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/samsung/android/scloud/quota/BackupDetailsList;->mDeviceId:Ljava/lang/String;

    .line 22
    iput-object p2, p0, Lcom/samsung/android/scloud/quota/BackupDetailsList;->mClientDeviceID:Ljava/lang/String;

    .line 23
    iput-object p3, p0, Lcom/samsung/android/scloud/quota/BackupDetailsList;->mBackupSize:Ljava/lang/Long;

    .line 25
    iput-object p4, p0, Lcom/samsung/android/scloud/quota/BackupDetailsList;->mModelNumber:Ljava/lang/String;

    .line 27
    iput-object p5, p0, Lcom/samsung/android/scloud/quota/BackupDetailsList;->mTimeStamp:Ljava/lang/Long;

    .line 28
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x0

    return v0
.end method

.method public getBackupSize()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupDetailsList;->mBackupSize:Ljava/lang/Long;

    return-object v0
.end method

.method public getClientDeviceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupDetailsList;->mClientDeviceID:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupDetailsList;->mDeviceId:Ljava/lang/String;

    return-object v0
.end method

.method public getLastBackupTime()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupDetailsList;->mTimeStamp:Ljava/lang/Long;

    return-object v0
.end method

.method public getModelNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupDetailsList;->mModelNumber:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 94
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupDetailsList;->mDeviceId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 95
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupDetailsList;->mClientDeviceID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 96
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupDetailsList;->mBackupSize:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 98
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupDetailsList;->mModelNumber:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 100
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupDetailsList;->mTimeStamp:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 102
    return-void
.end method
