.class public Lcom/samsung/android/scloud/quota/UsageBarPreference;
.super Landroid/preference/Preference;
.source "UsageBarPreference.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "UsageBarPreference"


# instance fields
.field private mChart:Lcom/samsung/android/scloud/quota/PercentageBarChart;

.field private final mEntries:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/samsung/android/scloud/quota/PercentageBarChart$Entry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/UsageBarPreference;->mChart:Lcom/samsung/android/scloud/quota/PercentageBarChart;

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/UsageBarPreference;->mEntries:Ljava/util/Collection;

    .line 35
    const v0, 0x7f030008

    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/quota/UsageBarPreference;->setLayoutResource(I)V

    .line 36
    return-void
.end method


# virtual methods
.method public addEntry(FI)V
    .locals 3
    .param p1, "percentage"    # F
    .param p2, "color"    # I

    .prologue
    .line 40
    const-string v0, "UsageBarPreference"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Inside addEntry,  percentage : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " color :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mEntries :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/quota/UsageBarPreference;->mEntries:Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    const-string v0, "UsageBarPreference"

    const-string v1, "Add entries in the usage bar"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/UsageBarPreference;->mEntries:Ljava/util/Collection;

    invoke-static {p1, p2}, Lcom/samsung/android/scloud/quota/PercentageBarChart;->createEntry(FI)Lcom/samsung/android/scloud/quota/PercentageBarChart$Entry;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 43
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/UsageBarPreference;->mEntries:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    .line 62
    return-void
.end method

.method public commit()V
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/UsageBarPreference;->mChart:Lcom/samsung/android/scloud/quota/PercentageBarChart;

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/UsageBarPreference;->mChart:Lcom/samsung/android/scloud/quota/PercentageBarChart;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/quota/PercentageBarChart;->invalidate()V

    .line 58
    :cond_0
    return-void
.end method

.method protected onBindView(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 47
    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    .line 49
    const v0, 0x7f0a000d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/quota/PercentageBarChart;

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/UsageBarPreference;->mChart:Lcom/samsung/android/scloud/quota/PercentageBarChart;

    .line 51
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/UsageBarPreference;->mChart:Lcom/samsung/android/scloud/quota/PercentageBarChart;

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/UsageBarPreference;->mEntries:Ljava/util/Collection;

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/quota/PercentageBarChart;->setEntries(Ljava/util/Collection;)V

    .line 52
    return-void
.end method
