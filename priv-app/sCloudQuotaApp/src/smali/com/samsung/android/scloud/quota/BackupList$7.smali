.class Lcom/samsung/android/scloud/quota/BackupList$7;
.super Ljava/lang/Object;
.source "BackupList.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/quota/BackupList;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/quota/BackupList;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/quota/BackupList;)V
    .locals 0

    .prologue
    .line 475
    iput-object p1, p0, Lcom/samsung/android/scloud/quota/BackupList$7;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 8
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 479
    iget-object v3, p0, Lcom/samsung/android/scloud/quota/BackupList$7;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v3, v3, Lcom/samsung/android/scloud/quota/BackupList;->list:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->getCount()I

    move-result v2

    .line 480
    .local v2, "len":I
    iget-object v3, p0, Lcom/samsung/android/scloud/quota/BackupList$7;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v3, v3, Lcom/samsung/android/scloud/quota/BackupList;->list:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v0

    .line 482
    .local v0, "checked":Landroid/util/SparseBooleanArray;
    iget-object v3, p0, Lcom/samsung/android/scloud/quota/BackupList$7;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v3, v3, Lcom/samsung/android/scloud/quota/BackupList;->mDeleteList:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/scloud/quota/BackupList$7;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v3, v3, Lcom/samsung/android/scloud/quota/BackupList;->mDeleteList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 484
    const-string v3, "BackupList"

    const-string v4, "DeleteList should be cleared before creating it"

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 486
    iget-object v3, p0, Lcom/samsung/android/scloud/quota/BackupList$7;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v3, v3, Lcom/samsung/android/scloud/quota/BackupList;->mDeleteList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 489
    :cond_0
    const-string v3, "BackupList"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Length of List :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 490
    const-string v3, "BackupList"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Length of SparseBoolenArray :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 492
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 493
    invoke-virtual {v0, v1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 494
    const-string v3, "BackupList"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Item Checked :  true"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 492
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 497
    :cond_1
    const-string v3, "BackupList"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Item Checked :  false"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 501
    :cond_2
    add-int/lit8 v1, v2, -0x1

    :goto_2
    if-ltz v1, :cond_4

    .line 502
    invoke-virtual {v0, v1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 503
    const-string v3, "BackupList"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Posiiton is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 504
    iget-object v3, p0, Lcom/samsung/android/scloud/quota/BackupList$7;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v4, v3, Lcom/samsung/android/scloud/quota/BackupList;->mDeleteList:Ljava/util/ArrayList;

    new-instance v5, Lcom/samsung/android/scloud/quota/BackupDetails;

    iget-object v3, p0, Lcom/samsung/android/scloud/quota/BackupList$7;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v3, v3, Lcom/samsung/android/scloud/quota/BackupList;->backupList:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/scloud/quota/BackupDetails;

    invoke-virtual {v3}, Lcom/samsung/android/scloud/quota/BackupDetails;->deviceName()Ljava/lang/String;

    move-result-object v6

    iget-object v3, p0, Lcom/samsung/android/scloud/quota/BackupList$7;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v3, v3, Lcom/samsung/android/scloud/quota/BackupList;->backupList:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/scloud/quota/BackupDetails;

    invoke-virtual {v3}, Lcom/samsung/android/scloud/quota/BackupDetails;->getClientDeviceID()Ljava/lang/String;

    move-result-object v3

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-direct {v5, v6, v3, v7}, Lcom/samsung/android/scloud/quota/BackupDetails;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 501
    :cond_3
    add-int/lit8 v1, v1, -0x1

    goto :goto_2

    .line 513
    :cond_4
    const-string v3, "BackupList"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Delete List Size : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/scloud/quota/BackupList$7;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v5, v5, Lcom/samsung/android/scloud/quota/BackupList;->mDeleteList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 514
    iget-object v3, p0, Lcom/samsung/android/scloud/quota/BackupList$7;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v4, p0, Lcom/samsung/android/scloud/quota/BackupList$7;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v4, v4, Lcom/samsung/android/scloud/quota/BackupList;->mDeleteList:Ljava/util/ArrayList;

    # invokes: Lcom/samsung/android/scloud/quota/BackupList;->startDeleteThread(Ljava/util/ArrayList;)V
    invoke-static {v3, v4}, Lcom/samsung/android/scloud/quota/BackupList;->access$600(Lcom/samsung/android/scloud/quota/BackupList;Ljava/util/ArrayList;)V

    .line 516
    iget-object v3, p0, Lcom/samsung/android/scloud/quota/BackupList$7;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    # invokes: Lcom/samsung/android/scloud/quota/BackupList;->backTolist()V
    invoke-static {v3}, Lcom/samsung/android/scloud/quota/BackupList;->access$700(Lcom/samsung/android/scloud/quota/BackupList;)V

    .line 517
    return-void
.end method
