.class public Lcom/samsung/android/scloud/quota/QuotaUtils;
.super Ljava/lang/Object;
.source "QuotaUtils.java"


# static fields
.field public static final ACCOUNT_AUTHORITY_URI:Landroid/net/Uri;

.field public static final ACCOUNT_CONTENT_URI:Landroid/net/Uri;

.field public static final FIVEGIGABYTE:J = 0x140000000L

.field public static final GIGABYTE:J = 0x40000000L

.field public static final KILOBYTE:J = 0x400L

.field public static final MEGABYTE:J = 0x100000L

.field static final NEW_FORMAT:Ljava/lang/String; = "dd/MM/yyyy"

.field static final OLD_FORMAT:Ljava/lang/String; = "yyyyMMdd"

.field public static final ONLY_SUPPORT_AIDL_OF_SAMSUNGACCOUNT_VERSION:I = 0x30d40

.field private static final REG_JP_DCM:Ljava/lang/String; = "DCM"

.field private static final REG_US_COUNTRY_ISO:Ljava/lang/String; = "US"

.field public static final TAG:Ljava/lang/String; = "QuotaUtils"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 38
    const-string v0, "content://com.msc.openprovider.openContentProvider"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/scloud/quota/QuotaUtils;->ACCOUNT_AUTHORITY_URI:Landroid/net/Uri;

    .line 40
    sget-object v0, Lcom/samsung/android/scloud/quota/QuotaUtils;->ACCOUNT_AUTHORITY_URI:Landroid/net/Uri;

    const-string v1, "tncRequest"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/scloud/quota/QuotaUtils;->ACCOUNT_CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static changeDateFormat(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p0, "inputDate"    # Ljava/lang/String;

    .prologue
    .line 45
    const-string v4, "QuotaUtils"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "changeDateFormat : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    const/4 v1, 0x0

    .line 48
    .local v1, "convertedDate":Ljava/util/Date;
    const/4 v0, 0x0

    .line 50
    .local v0, "ConvertedPaymentDate":Ljava/lang/String;
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v4, "yyyyMMdd"

    invoke-direct {v2, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 52
    .local v2, "dateFormat":Ljava/text/SimpleDateFormat;
    :try_start_0
    invoke-virtual {v2, p0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    .line 53
    const-string v4, "dd/MM/yyyy"

    invoke-virtual {v2, v4}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    .line 54
    invoke-virtual {v2, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 60
    :goto_0
    return-object v0

    .line 55
    :catch_0
    move-exception v3

    .line 57
    .local v3, "e":Ljava/text/ParseException;
    invoke-virtual {v3}, Ljava/text/ParseException;->printStackTrace()V

    goto :goto_0
.end method

.method protected static checkAccountValidation(Landroid/content/Context;)Z
    .locals 15
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v13, 0x1

    const/4 v14, 0x0

    const/4 v2, 0x0

    .line 326
    const/4 v12, 0x0

    .line 327
    .local v12, "tncState":I
    const/4 v11, 0x0

    .line 328
    .local v11, "nameCheckState":I
    const/4 v9, 0x0

    .line 329
    .local v9, "emailValidationState":I
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/scloud/quota/QuotaUtils;->ACCOUNT_CONTENT_URI:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 331
    .local v7, "cursor":Landroid/database/Cursor;
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v10

    .line 333
    .local v10, "manager":Landroid/accounts/AccountManager;
    if-eqz v10, :cond_3

    .line 335
    const-string v0, "com.osp.app.signin"

    invoke-virtual {v10, v0}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v6

    .line 338
    .local v6, "accountArray":[Landroid/accounts/Account;
    array-length v0, v6

    if-lez v0, :cond_4

    .line 340
    if-eqz v7, :cond_1

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 342
    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 343
    invoke-interface {v7, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 344
    .local v8, "emailID":Ljava/lang/String;
    invoke-interface {v7, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 345
    const/4 v0, 0x2

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 346
    const/4 v0, 0x3

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 347
    const-string v0, "QuotaUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "emailID : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " tncState :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " nameCheckState : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " emailValidationState : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 350
    add-int v0, v12, v11

    add-int/2addr v0, v9

    if-nez v0, :cond_0

    .line 351
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    move v0, v13

    .line 365
    .end local v6    # "accountArray":[Landroid/accounts/Account;
    .end local v8    # "emailID":Ljava/lang/String;
    :goto_0
    return v0

    .line 356
    .restart local v6    # "accountArray":[Landroid/accounts/Account;
    :cond_1
    const-string v0, "QuotaUtils"

    const-string v1, "Fail To Obtain Cursor"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    :cond_2
    :goto_1
    if-eqz v7, :cond_3

    .line 363
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .end local v6    # "accountArray":[Landroid/accounts/Account;
    :cond_3
    move v0, v14

    .line 365
    goto :goto_0

    .line 360
    .restart local v6    # "accountArray":[Landroid/accounts/Account;
    :cond_4
    const-string v0, "QuotaUtils"

    const-string v1, "Samsung Account Not Logged in"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static daysBetween(Ljava/util/Date;Ljava/util/Date;)I
    .locals 4
    .param p0, "d1"    # Ljava/util/Date;
    .param p1, "d2"    # Ljava/util/Date;

    .prologue
    .line 115
    const-string v0, "QuotaUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "daysBetween :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-virtual {p0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x5265c00

    div-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public static generateCTID()Ljava/lang/String;
    .locals 5

    .prologue
    const/16 v4, 0xa

    .line 277
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 279
    .local v2, "sb":Ljava/lang/StringBuilder;
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    .line 281
    .local v0, "generator":Ljava/util/Random;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v4, :cond_0

    .line 283
    invoke-virtual {v0, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 281
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 287
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public static getClientDeviceId(Landroid/content/Context;)Ljava/lang/String;
    .locals 5
    .param p0, "mContext"    # Landroid/content/Context;

    .prologue
    .line 299
    const/4 v0, 0x0

    .line 300
    .local v0, "clientDeviceId":Ljava/lang/String;
    const-string v3, "phone"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 302
    .local v2, "telephonyManager":Landroid/telephony/TelephonyManager;
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v1

    .line 303
    .local v1, "phoneType":I
    if-eqz v1, :cond_2

    .line 306
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    .line 315
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    const-string v3, "0"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 318
    :cond_0
    const/4 v0, 0x0

    .line 321
    :cond_1
    return-object v0

    .line 311
    :cond_2
    sget-object v0, Landroid/os/Build;->SERIAL:Ljava/lang/String;

    goto :goto_0
.end method

.method private static getCountryIsoCode(Landroid/content/Context;)Ljava/lang/String;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 179
    const-string v7, "xx"

    .line 182
    .local v7, "ret":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v8

    const-string v9, "android.os.SystemProperties"

    invoke-virtual {v8, v9}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 184
    .local v1, "SystemProperties":Ljava/lang/Class;
    const/4 v8, 0x1

    new-array v5, v8, [Ljava/lang/Class;

    .line 185
    .local v5, "paramTypes":[Ljava/lang/Class;
    const/4 v8, 0x0

    const-class v9, Ljava/lang/String;

    aput-object v9, v5, v8

    .line 187
    const-string v8, "get"

    invoke-virtual {v1, v8, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 190
    .local v3, "get":Ljava/lang/reflect/Method;
    const/4 v8, 0x1

    new-array v6, v8, [Ljava/lang/Object;

    .line 192
    .local v6, "params":[Ljava/lang/Object;
    const/4 v8, 0x0

    const-string v9, "ro.csc.countryiso_code"

    aput-object v9, v6, v8

    .line 194
    invoke-virtual {v3, v1, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    move-object v0, v8

    check-cast v0, Ljava/lang/String;

    move-object v7, v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_4

    .line 213
    .end local v1    # "SystemProperties":Ljava/lang/Class;
    .end local v3    # "get":Ljava/lang/reflect/Method;
    .end local v5    # "paramTypes":[Ljava/lang/Class;
    .end local v6    # "params":[Ljava/lang/Object;
    :goto_0
    return-object v7

    .line 196
    :catch_0
    move-exception v4

    .line 197
    .local v4, "iAE":Ljava/lang/IllegalArgumentException;
    const-string v7, "xx"

    .line 212
    goto :goto_0

    .line 200
    .end local v4    # "iAE":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v2

    .line 201
    .local v2, "e":Ljava/lang/IllegalAccessException;
    const-string v7, "xx"

    .line 202
    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 203
    .end local v2    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v2

    .line 204
    .local v2, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v7, "xx"

    .line 205
    invoke-virtual {v2}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0

    .line 206
    .end local v2    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_3
    move-exception v2

    .line 208
    .local v2, "e":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v2}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    .line 209
    .end local v2    # "e":Ljava/lang/NoSuchMethodException;
    :catch_4
    move-exception v2

    .line 210
    .local v2, "e":Ljava/lang/ClassNotFoundException;
    const-string v7, "xx"

    .line 211
    invoke-virtual {v2}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getDateDifference(Ljava/lang/String;)I
    .locals 11
    .param p0, "purchaseDate"    # Ljava/lang/String;

    .prologue
    .line 68
    const-string v8, "QuotaUtils"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "changeDateFormat : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    const/4 v6, 0x0

    .line 71
    .local v6, "tday":Ljava/lang/String;
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string v8, "dd/MM/yyyy"

    invoke-direct {v3, v8}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 73
    .local v3, "dateFormat":Ljava/text/SimpleDateFormat;
    new-instance v8, Ljava/util/Date;

    invoke-direct {v8}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3, v8}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    .line 75
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    .line 77
    .local v0, "cal1":Ljava/util/Calendar;
    new-instance v1, Ljava/util/GregorianCalendar;

    invoke-direct {v1}, Ljava/util/GregorianCalendar;-><init>()V

    .line 79
    .local v1, "cal2":Ljava/util/Calendar;
    const/4 v2, 0x0

    .line 81
    .local v2, "convertedDate":Ljava/util/Date;
    const/4 v7, 0x0

    .line 85
    .local v7, "today":Ljava/util/Date;
    :try_start_0
    invoke-static {p0}, Lcom/samsung/android/scloud/quota/QuotaUtils;->changeDateFormat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_0

    .line 87
    invoke-static {p0}, Lcom/samsung/android/scloud/quota/QuotaUtils;->changeDateFormat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    .line 88
    invoke-virtual {v2}, Ljava/util/Date;->getYear()I

    move-result v8

    invoke-virtual {v2}, Ljava/util/Date;->getMonth()I

    move-result v9

    invoke-virtual {v2}, Ljava/util/Date;->getDate()I

    move-result v10

    invoke-virtual {v0, v8, v9, v10}, Ljava/util/Calendar;->set(III)V

    .line 91
    :cond_0
    invoke-virtual {v3, v6}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v7

    .line 92
    invoke-virtual {v7}, Ljava/util/Date;->getYear()I

    move-result v8

    invoke-virtual {v7}, Ljava/util/Date;->getMonth()I

    move-result v9

    invoke-virtual {v7}, Ljava/util/Date;->getDate()I

    move-result v10

    invoke-virtual {v1, v8, v9, v10}, Ljava/util/Calendar;->set(III)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 101
    :goto_0
    const/4 v5, 0x0

    .line 103
    .local v5, "gap":I
    if-eqz v2, :cond_1

    .line 104
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v8

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/android/scloud/quota/QuotaUtils;->daysBetween(Ljava/util/Date;Ljava/util/Date;)I

    move-result v5

    .line 106
    :cond_1
    return v5

    .line 94
    .end local v5    # "gap":I
    :catch_0
    move-exception v4

    .line 98
    .local v4, "e":Ljava/text/ParseException;
    invoke-virtual {v4}, Ljava/text/ParseException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getDateStringFromTimeSpan(JLandroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p0, "time"    # J
    .param p2, "mContext"    # Landroid/content/Context;

    .prologue
    .line 173
    invoke-static {p2}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    .line 174
    .local v0, "dateFormat":Ljava/text/DateFormat;
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, p0, p1}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getIsTablet(Landroid/content/res/Configuration;)Z
    .locals 6
    .param p0, "config"    # Landroid/content/res/Configuration;

    .prologue
    .line 124
    const/4 v0, 0x0

    .line 126
    .local v0, "isTablet":Z
    const-string v3, "QuotaUtils"

    const-string v4, "getIsTablet()"

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xb

    if-ge v3, v4, :cond_0

    .line 131
    const-string v3, "QuotaUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "1. getIsTablet() : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v0

    .line 168
    .end local v0    # "isTablet":Z
    .local v1, "isTablet":I
    :goto_0
    return v1

    .line 139
    .end local v1    # "isTablet":I
    .restart local v0    # "isTablet":Z
    :cond_0
    const-string v3, "GT-I9200"

    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "GT-I9205"

    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v4, "SHV-E310"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v4, "SGH-I527"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v4, "SCH-R960"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v4, "SPH-L600"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v4, "SGH-M819N"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 140
    :cond_1
    const-string v3, "QuotaUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "2. getIsTablet() : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v0

    .line 141
    .restart local v1    # "isTablet":I
    goto :goto_0

    .line 146
    .end local v1    # "isTablet":I
    :cond_2
    iget v3, p0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v2, v3, 0xf

    .line 148
    .local v2, "size":I
    packed-switch v2, :pswitch_data_0

    .line 166
    :goto_1
    const-string v3, "QuotaUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "3. getIsTablet() : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v0

    .line 168
    .restart local v1    # "isTablet":I
    goto/16 :goto_0

    .line 156
    .end local v1    # "isTablet":I
    :pswitch_0
    const/4 v0, 0x1

    .line 158
    goto :goto_1

    .line 148
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private static getSalesInfo(Landroid/content/Context;)Ljava/lang/String;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 251
    const-string v6, "xx"

    .line 254
    .local v6, "ret":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v7

    const-string v8, "android.os.SystemProperties"

    invoke-virtual {v7, v8}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 256
    .local v0, "SystemProperties":Ljava/lang/Class;
    const/4 v7, 0x1

    new-array v4, v7, [Ljava/lang/Class;

    .line 257
    .local v4, "paramTypes":[Ljava/lang/Class;
    const/4 v7, 0x0

    const-class v8, Ljava/lang/String;

    aput-object v8, v4, v7

    .line 259
    const-string v7, "get"

    invoke-virtual {v0, v7, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 262
    .local v2, "get":Ljava/lang/reflect/Method;
    const/4 v7, 0x1

    new-array v5, v7, [Ljava/lang/Object;

    .line 263
    .local v5, "params":[Ljava/lang/Object;
    const/4 v7, 0x0

    new-instance v8, Ljava/lang/String;

    const-string v9, "ro.csc.sales_code"

    invoke-direct {v8, v9}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v8, v5, v7

    .line 265
    invoke-virtual {v2, v0, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "ret":Ljava/lang/String;
    check-cast v6, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 272
    .end local v0    # "SystemProperties":Ljava/lang/Class;
    .end local v2    # "get":Ljava/lang/reflect/Method;
    .end local v4    # "paramTypes":[Ljava/lang/Class;
    .end local v5    # "params":[Ljava/lang/Object;
    .restart local v6    # "ret":Ljava/lang/String;
    :goto_0
    return-object v6

    .line 267
    .end local v6    # "ret":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 268
    .local v3, "iAE":Ljava/lang/IllegalArgumentException;
    const-string v6, "xx"

    .line 271
    .restart local v6    # "ret":Ljava/lang/String;
    goto :goto_0

    .line 269
    .end local v3    # "iAE":Ljava/lang/IllegalArgumentException;
    .end local v6    # "ret":Ljava/lang/String;
    :catch_1
    move-exception v1

    .line 270
    .local v1, "e":Ljava/lang/Exception;
    const-string v6, "xx"

    .restart local v6    # "ret":Ljava/lang/String;
    goto :goto_0
.end method

.method private static getVersionOfSamsungAccount(Landroid/content/Context;)I
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 371
    const/4 v3, 0x0

    .line 374
    .local v3, "version":I
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 375
    .local v2, "pm":Landroid/content/pm/PackageManager;
    const-string v4, "com.osp.app.signin"

    const/16 v5, 0x80

    invoke-virtual {v2, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 376
    .local v1, "pi":Landroid/content/pm/PackageInfo;
    iget v3, v1, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 381
    .end local v1    # "pi":Landroid/content/pm/PackageInfo;
    .end local v2    # "pm":Landroid/content/pm/PackageManager;
    :goto_0
    return v3

    .line 377
    :catch_0
    move-exception v0

    .line 379
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static hideStatusBar(Landroid/app/Activity;)V
    .locals 2
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    const/16 v1, 0x400

    .line 292
    sget-boolean v0, Lcom/samsung/android/scloud/quota/QuotaConstants$DEVICE;->IS_DEVICE_CHAGALL:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/samsung/android/scloud/quota/QuotaConstants$DEVICE;->IS_DEVICE_KLIMT:Z

    if-eqz v0, :cond_1

    .line 293
    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setFlags(II)V

    .line 294
    :cond_1
    return-void
.end method

.method public static isJapanDevice(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 230
    const/4 v0, 0x0

    .line 231
    .local v0, "ret":Z
    invoke-static {p0}, Lcom/samsung/android/scloud/quota/QuotaUtils;->getSalesInfo(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 232
    .local v1, "salesCode":Ljava/lang/String;
    if-eqz v1, :cond_0

    const-string v2, "DCM"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 233
    const/4 v0, 0x1

    .line 236
    :cond_0
    return v0
.end method

.method public static isJapanPreviousModel()Z
    .locals 2

    .prologue
    .line 242
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SC-02E"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SC-03E"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SC-04E"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SC-01F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SC-02F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 244
    :cond_0
    const/4 v0, 0x1

    .line 246
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected static isSupportAidlOnly(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 385
    const/4 v0, 0x0

    .line 386
    .local v0, "result":Z
    const-string v1, "QuotaUtils"

    const-string v2, "isSupportAidlOnly : ? "

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    invoke-static {p0}, Lcom/samsung/android/scloud/quota/QuotaUtils;->getVersionOfSamsungAccount(Landroid/content/Context;)I

    move-result v1

    const v2, 0x30d40

    if-lt v1, v2, :cond_0

    .line 389
    const/4 v0, 0x1

    .line 391
    :cond_0
    const-string v1, "QuotaUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isSupportAidlOnly : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    return v0
.end method

.method public static isUSDevice(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 217
    const/4 v1, 0x0

    .line 219
    .local v1, "ret":Z
    invoke-static {p0}, Lcom/samsung/android/scloud/quota/QuotaUtils;->getCountryIsoCode(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 220
    .local v0, "countryIso":Ljava/lang/String;
    const-string v2, "US"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 221
    const-string v2, "QuotaUtils"

    const-string v3, "USA Device"

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    const/4 v1, 0x1

    .line 225
    :cond_0
    return v1
.end method

.method public static isViewType_light()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 397
    sget-boolean v1, Lcom/samsung/android/scloud/quota/QuotaConstants$DEVICE;->IS_DEVICE_T:Z

    if-nez v1, :cond_0

    sget-boolean v1, Lcom/samsung/android/scloud/quota/QuotaConstants$DEVICE;->IS_DEVICE_A:Z

    if-nez v1, :cond_0

    sget-boolean v1, Lcom/samsung/android/scloud/quota/QuotaConstants$DEVICE;->IS_DEVICE_K:Z

    if-nez v1, :cond_0

    sget-boolean v1, Lcom/samsung/android/scloud/quota/QuotaConstants$DEVICE;->IS_DEVICE_H:Z

    if-nez v1, :cond_0

    sget-boolean v1, Lcom/samsung/android/scloud/quota/QuotaConstants$DEVICE;->IS_DEVICE_J:Z

    if-nez v1, :cond_0

    sget-boolean v1, Lcom/samsung/android/scloud/quota/QuotaConstants$DEVICE;->IS_DEVICE_ZERO:Z

    if-nez v1, :cond_0

    sget-boolean v1, Lcom/samsung/android/scloud/quota/QuotaConstants$DEVICE;->IS_DEVICE_SLTE:Z

    if-eq v1, v0, :cond_0

    sget-boolean v1, Lcom/samsung/android/scloud/quota/QuotaConstants$DEVICE;->IS_DEVICE_PACIFIC:Z

    if-nez v1, :cond_0

    sget-boolean v1, Lcom/samsung/android/scloud/quota/QuotaConstants$DEVICE;->IS_DEVICE_CHAGALL:Z

    if-nez v1, :cond_0

    sget-boolean v1, Lcom/samsung/android/scloud/quota/QuotaConstants$DEVICE;->IS_DEVICE_KLIMT:Z

    if-eqz v1, :cond_1

    .line 401
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
