.class Lcom/samsung/android/scloud/quota/BackupList$AccessTokenclass;
.super Landroid/os/AsyncTask;
.source "BackupList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/quota/BackupList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AccessTokenclass"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/quota/BackupList;


# direct methods
.method private constructor <init>(Lcom/samsung/android/scloud/quota/BackupList;)V
    .locals 0

    .prologue
    .line 1176
    iput-object p1, p0, Lcom/samsung/android/scloud/quota/BackupList$AccessTokenclass;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/scloud/quota/BackupList;Lcom/samsung/android/scloud/quota/BackupList$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/scloud/quota/BackupList;
    .param p2, "x1"    # Lcom/samsung/android/scloud/quota/BackupList$1;

    .prologue
    .line 1176
    invoke-direct {p0, p1}, Lcom/samsung/android/scloud/quota/BackupList$AccessTokenclass;-><init>(Lcom/samsung/android/scloud/quota/BackupList;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 1176
    check-cast p1, [Ljava/lang/String;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/android/scloud/quota/BackupList$AccessTokenclass;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "params"    # [Ljava/lang/String;

    .prologue
    .line 1181
    const-string v1, "BackupList"

    const-string v2, "QUOTA_5102 : [START]"

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1184
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList$AccessTokenclass;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    new-instance v2, Lcom/samsung/android/scloud/quota/AuthControlForNewDataRelay;

    invoke-direct {v2}, Lcom/samsung/android/scloud/quota/AuthControlForNewDataRelay;-><init>()V

    # setter for: Lcom/samsung/android/scloud/quota/BackupList;->mAuthControlNew:Lcom/samsung/android/scloud/quota/AuthControlForNewDataRelay;
    invoke-static {v1, v2}, Lcom/samsung/android/scloud/quota/BackupList;->access$1202(Lcom/samsung/android/scloud/quota/BackupList;Lcom/samsung/android/scloud/quota/AuthControlForNewDataRelay;)Lcom/samsung/android/scloud/quota/AuthControlForNewDataRelay;

    .line 1188
    :try_start_0
    const-string v1, "BackupList"

    const-string v2, "QUOTA_5102 : [TRY]"

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1192
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList$AccessTokenclass;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v2, p0, Lcom/samsung/android/scloud/quota/BackupList$AccessTokenclass;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    # getter for: Lcom/samsung/android/scloud/quota/BackupList;->mAuthControlNew:Lcom/samsung/android/scloud/quota/AuthControlForNewDataRelay;
    invoke-static {v2}, Lcom/samsung/android/scloud/quota/BackupList;->access$1200(Lcom/samsung/android/scloud/quota/BackupList;)Lcom/samsung/android/scloud/quota/AuthControlForNewDataRelay;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/scloud/quota/BackupList;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/samsung/android/scloud/quota/BackupList$AccessTokenclass;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v4, v4, Lcom/samsung/android/scloud/quota/BackupList;->mCtid:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/samsung/android/scloud/quota/AuthControlForNewDataRelay;->getAuthInformation(Landroid/content/Context;Ljava/lang/String;)Lcom/samsung/android/scloud/quota/AuthManager;

    move-result-object v2

    iput-object v2, v1, Lcom/samsung/android/scloud/quota/BackupList;->mAuthManager:Lcom/samsung/android/scloud/quota/AuthManager;

    .line 1196
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList$AccessTokenclass;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    const/4 v2, 0x1

    # setter for: Lcom/samsung/android/scloud/quota/BackupList;->isAccessToken:Z
    invoke-static {v1, v2}, Lcom/samsung/android/scloud/quota/BackupList;->access$1302(Lcom/samsung/android/scloud/quota/BackupList;Z)Z
    :try_end_0
    .catch Lcom/samsung/android/scloud/quota/QuotaException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1208
    :goto_0
    const/4 v1, 0x0

    return-object v1

    .line 1198
    :catch_0
    move-exception v0

    .line 1200
    .local v0, "e":Lcom/samsung/android/scloud/quota/QuotaException;
    const-string v1, "BackupList"

    const-string v2, "QUOTA_5102 : [QuotaException]"

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1203
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList$AccessTokenclass;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    const/4 v2, 0x0

    # setter for: Lcom/samsung/android/scloud/quota/BackupList;->isAccessToken:Z
    invoke-static {v1, v2}, Lcom/samsung/android/scloud/quota/BackupList;->access$1302(Lcom/samsung/android/scloud/quota/BackupList;Z)Z

    .line 1205
    invoke-virtual {v0}, Lcom/samsung/android/scloud/quota/QuotaException;->printStackTrace()V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 1176
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/android/scloud/quota/BackupList$AccessTokenclass;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 4
    .param p1, "result"    # Ljava/lang/String;

    .prologue
    .line 1216
    const-string v0, "BackupList"

    const-string v1, "Success to get Authinfo"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1218
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 1219
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList$AccessTokenclass;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    # getter for: Lcom/samsung/android/scloud/quota/BackupList;->isAccessToken:Z
    invoke-static {v0}, Lcom/samsung/android/scloud/quota/BackupList;->access$1300(Lcom/samsung/android/scloud/quota/BackupList;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1220
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList$AccessTokenclass;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v0, v0, Lcom/samsung/android/scloud/quota/BackupList;->mAuthManager:Lcom/samsung/android/scloud/quota/AuthManager;

    if-eqz v0, :cond_1

    .line 1221
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList$AccessTokenclass;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    new-instance v1, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;

    sget-object v2, Lcom/samsung/android/scloud/quota/BackupList;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/scloud/quota/BackupList$AccessTokenclass;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v3, v3, Lcom/samsung/android/scloud/quota/BackupList;->mAuthManager:Lcom/samsung/android/scloud/quota/AuthManager;

    invoke-direct {v1, v2, v3}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;-><init>(Landroid/content/Context;Lcom/samsung/android/scloud/quota/AuthManager;)V

    # setter for: Lcom/samsung/android/scloud/quota/BackupList;->quotaServiceManager:Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;
    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/BackupList;->access$1102(Lcom/samsung/android/scloud/quota/BackupList;Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;)Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;

    .line 1225
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList$AccessTokenclass;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    # invokes: Lcom/samsung/android/scloud/quota/BackupList;->startQuotaThread()V
    invoke-static {v0}, Lcom/samsung/android/scloud/quota/BackupList;->access$1400(Lcom/samsung/android/scloud/quota/BackupList;)V

    .line 1233
    :cond_0
    :goto_0
    return-void

    .line 1227
    :cond_1
    sget-object v0, Lcom/samsung/android/scloud/quota/BackupList;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList$AccessTokenclass;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    const v2, 0x7f07000a

    invoke-virtual {v1, v2}, Lcom/samsung/android/scloud/quota/BackupList;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1230
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList$AccessTokenclass;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/quota/BackupList;->finish()V

    goto :goto_0
.end method
