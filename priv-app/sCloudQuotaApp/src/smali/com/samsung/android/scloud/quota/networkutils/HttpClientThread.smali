.class public Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;
.super Ljava/lang/Thread;
.source "HttpClientThread.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread$RESPONSE;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "HttpClientThread"

.field public static baseString:Ljava/lang/String;

.field public static signature:Ljava/lang/String;

.field public static storeRequestId:Ljava/lang/String;

.field public static timeStamp:Ljava/lang/String;


# instance fields
.field private mHttpClient:Lorg/apache/http/client/HttpClient;

.field private mHttpClientResponseListener:Lcom/samsung/android/scloud/quota/networkutils/HttpClientResponseListener;

.field private mHttpResponse:Lorg/apache/http/HttpResponse;

.field private mHttpUriRequest:Lorg/apache/http/client/methods/HttpUriRequest;

.field private mRequestID:Ljava/lang/String;

.field private mThreadLock:Ljava/lang/Object;

.field private mbClosed:Z


# direct methods
.method public constructor <init>(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/HttpClient;Lcom/samsung/android/scloud/quota/networkutils/HttpClientResponseListener;Ljava/lang/String;)V
    .locals 1
    .param p1, "httpUriRequest"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .param p2, "httpClient"    # Lorg/apache/http/client/HttpClient;
    .param p3, "listener"    # Lcom/samsung/android/scloud/quota/networkutils/HttpClientResponseListener;
    .param p4, "requestID"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 57
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 36
    iput-object v0, p0, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->mHttpClientResponseListener:Lcom/samsung/android/scloud/quota/networkutils/HttpClientResponseListener;

    .line 37
    iput-object v0, p0, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->mHttpUriRequest:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 38
    iput-object v0, p0, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->mHttpClient:Lorg/apache/http/client/HttpClient;

    .line 39
    iput-object v0, p0, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->mRequestID:Ljava/lang/String;

    .line 40
    iput-object v0, p0, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->mHttpResponse:Lorg/apache/http/HttpResponse;

    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->mbClosed:Z

    .line 42
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->mThreadLock:Ljava/lang/Object;

    .line 58
    iput-object p3, p0, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->mHttpClientResponseListener:Lcom/samsung/android/scloud/quota/networkutils/HttpClientResponseListener;

    .line 59
    iput-object p1, p0, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->mHttpUriRequest:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 60
    iput-object p2, p0, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->mHttpClient:Lorg/apache/http/client/HttpClient;

    .line 61
    iput-object p4, p0, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->mRequestID:Ljava/lang/String;

    .line 63
    return-void
.end method

.method private removeThis(I)V
    .locals 2
    .param p1, "reason"    # I

    .prologue
    .line 127
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->mHttpClientResponseListener:Lcom/samsung/android/scloud/quota/networkutils/HttpClientResponseListener;

    if-eqz v1, :cond_0

    .line 128
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 129
    .local v0, "msg":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 130
    iput-object p0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 131
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->mHttpClientResponseListener:Lcom/samsung/android/scloud/quota/networkutils/HttpClientResponseListener;

    invoke-interface {v1, v0}, Lcom/samsung/android/scloud/quota/networkutils/HttpClientResponseListener;->onResponse(Landroid/os/Message;)V

    .line 133
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    return-void
.end method

.method private responseOK()V
    .locals 2

    .prologue
    .line 137
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->mHttpClientResponseListener:Lcom/samsung/android/scloud/quota/networkutils/HttpClientResponseListener;

    if-eqz v1, :cond_0

    .line 138
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 139
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 140
    iput-object p0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 141
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->mHttpClientResponseListener:Lcom/samsung/android/scloud/quota/networkutils/HttpClientResponseListener;

    invoke-interface {v1, v0}, Lcom/samsung/android/scloud/quota/networkutils/HttpClientResponseListener;->onResponse(Landroid/os/Message;)V

    .line 143
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 147
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->mbClosed:Z

    .line 148
    return-void
.end method

.method public getHttpResponse()Lorg/apache/http/HttpResponse;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->mHttpResponse:Lorg/apache/http/HttpResponse;

    return-object v0
.end method

.method public getRequestID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->mRequestID:Ljava/lang/String;

    return-object v0
.end method

.method public getThreadLock()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->mThreadLock:Ljava/lang/Object;

    return-object v0
.end method

.method public isClosed()Z
    .locals 1

    .prologue
    .line 152
    iget-boolean v0, p0, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->mbClosed:Z

    return v0
.end method

.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    .line 77
    iget-object v3, p0, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->mHttpUriRequest:Lorg/apache/http/client/methods/HttpUriRequest;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->mHttpClient:Lorg/apache/http/client/HttpClient;

    if-eqz v3, :cond_5

    .line 80
    :try_start_0
    const-string v3, "HttpClientThread"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mHttpUriRequest : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->mHttpUriRequest:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    iget-object v3, p0, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->mHttpClient:Lorg/apache/http/client/HttpClient;

    iget-object v4, p0, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->mHttpUriRequest:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v3, v4}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->mHttpResponse:Lorg/apache/http/HttpResponse;

    .line 82
    const-string v3, "HttpClientThread"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Response : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->mHttpResponse:Lorg/apache/http/HttpResponse;

    invoke-interface {v5}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    iget-object v3, p0, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->mHttpResponse:Lorg/apache/http/HttpResponse;

    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v1

    .line 85
    .local v1, "entity":[Lorg/apache/http/Header;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v3, v1

    if-ge v2, v3, :cond_4

    .line 87
    const-string v3, "HttpClientThread"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " name =  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v1, v2

    invoke-interface {v5}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " value = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v1, v2

    invoke-interface {v5}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    aget-object v3, v1, v2

    invoke-interface {v3}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "storeRequestId"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 89
    aget-object v3, v1, v2

    invoke-interface {v3}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->storeRequestId:Ljava/lang/String;

    .line 90
    :cond_0
    aget-object v3, v1, v2

    invoke-interface {v3}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "baseString"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 91
    aget-object v3, v1, v2

    invoke-interface {v3}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->baseString:Ljava/lang/String;

    .line 92
    :cond_1
    aget-object v3, v1, v2

    invoke-interface {v3}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "signature"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 93
    aget-object v3, v1, v2

    invoke-interface {v3}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->signature:Ljava/lang/String;

    .line 95
    :cond_2
    aget-object v3, v1, v2

    invoke-interface {v3}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "timeStamp"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 97
    aget-object v3, v1, v2

    invoke-interface {v3}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->timeStamp:Ljava/lang/String;
    :try_end_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 85
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 102
    :cond_4
    const-wide/16 v4, 0xa

    :try_start_1
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 108
    :try_start_2
    iget-boolean v3, p0, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->mbClosed:Z

    if-nez v3, :cond_6

    .line 109
    invoke-direct {p0}, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->responseOK()V

    .line 123
    .end local v1    # "entity":[Lorg/apache/http/Header;
    .end local v2    # "i":I
    :cond_5
    :goto_1
    return-void

    .line 103
    .restart local v1    # "entity":[Lorg/apache/http/Header;
    .restart local v2    # "i":I
    :catch_0
    move-exception v0

    .line 104
    .local v0, "e":Ljava/lang/InterruptedException;
    iget-object v3, p0, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->mHttpUriRequest:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v3}, Lorg/apache/http/client/methods/HttpUriRequest;->abort()V

    .line 105
    const/4 v3, 0x2

    invoke-direct {p0, v3}, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->removeThis(I)V
    :try_end_2
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    .line 113
    .end local v0    # "e":Ljava/lang/InterruptedException;
    .end local v1    # "entity":[Lorg/apache/http/Header;
    .end local v2    # "i":I
    :catch_1
    move-exception v0

    .line 114
    .local v0, "e":Lorg/apache/http/client/ClientProtocolException;
    iget-object v3, p0, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->mHttpUriRequest:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v3}, Lorg/apache/http/client/methods/HttpUriRequest;->abort()V

    .line 115
    const-string v3, "HttpClientThread"

    const-string v4, "This request is cancelled because of ClientProtocolException"

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    invoke-direct {p0, v6}, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->removeThis(I)V

    goto :goto_1

    .line 111
    .end local v0    # "e":Lorg/apache/http/client/ClientProtocolException;
    .restart local v1    # "entity":[Lorg/apache/http/Header;
    .restart local v2    # "i":I
    :cond_6
    const/4 v3, 0x2

    :try_start_3
    invoke-direct {p0, v3}, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->removeThis(I)V
    :try_end_3
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    .line 117
    .end local v1    # "entity":[Lorg/apache/http/Header;
    .end local v2    # "i":I
    :catch_2
    move-exception v0

    .line 118
    .local v0, "e":Ljava/io/IOException;
    iget-object v3, p0, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->mHttpUriRequest:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v3}, Lorg/apache/http/client/methods/HttpUriRequest;->abort()V

    .line 119
    const-string v3, "HttpClientThread"

    const-string v4, "This request is cancelled because of IOException"

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    invoke-direct {p0, v6}, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->removeThis(I)V

    goto :goto_1
.end method
