.class public Lcom/samsung/android/scloud/quota/NotificationService;
.super Landroid/app/Service;
.source "NotificationService.java"

# interfaces
.implements Lcom/samsung/android/scloud/quota/QuotaResponseThreadListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/scloud/quota/NotificationService$AccessTokenclass;
    }
.end annotation


# static fields
.field private static final DELETION_NOTICE:I = 0x0

.field private static final OTHER_NOTICE:I = 0x1

.field public static final TAG:Ljava/lang/String; = "NotificationService"


# instance fields
.field private code:Ljava/lang/String;

.field private extra:Ljava/lang/String;

.field isAccessToken:Ljava/lang/Boolean;

.field private mAuthControlNew:Lcom/samsung/android/scloud/quota/AuthControlForNewDataRelay;

.field protected mAuthManager:Lcom/samsung/android/scloud/quota/AuthManager;

.field private mCommerceTargetYN:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mCtid:Ljava/lang/String;

.field private mCurrencySymbol:Ljava/lang/String;

.field private mCurrentPlan:Ljava/lang/String;

.field private mCurrentPlanAmt:Ljava/lang/String;

.field private mCurrentPlanType:Ljava/lang/String;

.field private final mHandler:Landroid/os/Handler;

.field private mNextCurrencySymbol:Ljava/lang/String;

.field private mNextPlan:Ljava/lang/String;

.field private mNextPlanAmt:Ljava/lang/String;

.field private mNextPlanType:Ljava/lang/String;

.field private mNotifationFlags:I

.field private mNotification:Landroid/app/Notification;

.field private mNotificationIntent:Landroid/content/Intent;

.field private mNotificationManager:Landroid/app/NotificationManager;

.field private mPendingIntent:Landroid/app/PendingIntent;

.field private mRenewalDate:Ljava/lang/String;

.field private mTotalQuota:J

.field private mTotalUsage:J

.field notificationContent:Ljava/lang/CharSequence;

.field private notificationID:I

.field notificationTitle:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 18
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 22
    iput-object v1, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mNotification:Landroid/app/Notification;

    .line 23
    iput-object v1, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mNotificationManager:Landroid/app/NotificationManager;

    .line 24
    iput-object v1, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mPendingIntent:Landroid/app/PendingIntent;

    .line 25
    iput-object v1, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mNotificationIntent:Landroid/content/Intent;

    .line 26
    iput v0, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mNotifationFlags:I

    .line 27
    iput v0, p0, Lcom/samsung/android/scloud/quota/NotificationService;->notificationID:I

    .line 31
    iput-object v1, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mAuthManager:Lcom/samsung/android/scloud/quota/AuthManager;

    .line 32
    iput-object v1, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mAuthControlNew:Lcom/samsung/android/scloud/quota/AuthControlForNewDataRelay;

    .line 33
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/NotificationService;->isAccessToken:Ljava/lang/Boolean;

    .line 42
    const-string v0, "N"

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mCommerceTargetYN:Ljava/lang/String;

    .line 46
    iput-wide v2, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mTotalUsage:J

    .line 47
    iput-wide v2, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mTotalQuota:J

    .line 50
    iput-object v1, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mCtid:Ljava/lang/String;

    .line 51
    iput-object v1, p0, Lcom/samsung/android/scloud/quota/NotificationService;->notificationTitle:Ljava/lang/CharSequence;

    .line 52
    iput-object v1, p0, Lcom/samsung/android/scloud/quota/NotificationService;->notificationContent:Ljava/lang/CharSequence;

    .line 104
    new-instance v0, Lcom/samsung/android/scloud/quota/NotificationService$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/scloud/quota/NotificationService$1;-><init>(Lcom/samsung/android/scloud/quota/NotificationService;)V

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mHandler:Landroid/os/Handler;

    .line 152
    return-void
.end method

.method static synthetic access$102(Lcom/samsung/android/scloud/quota/NotificationService;J)J
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/NotificationService;
    .param p1, "x1"    # J

    .prologue
    .line 18
    iput-wide p1, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mTotalUsage:J

    return-wide p1
.end method

.method static synthetic access$202(Lcom/samsung/android/scloud/quota/NotificationService;J)J
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/NotificationService;
    .param p1, "x1"    # J

    .prologue
    .line 18
    iput-wide p1, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mTotalQuota:J

    return-wide p1
.end method

.method static synthetic access$300(Lcom/samsung/android/scloud/quota/NotificationService;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/NotificationService;
    .param p1, "x1"    # I

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/samsung/android/scloud/quota/NotificationService;->showNotification(I)V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/android/scloud/quota/NotificationService;)Lcom/samsung/android/scloud/quota/AuthControlForNewDataRelay;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/NotificationService;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mAuthControlNew:Lcom/samsung/android/scloud/quota/AuthControlForNewDataRelay;

    return-object v0
.end method

.method static synthetic access$402(Lcom/samsung/android/scloud/quota/NotificationService;Lcom/samsung/android/scloud/quota/AuthControlForNewDataRelay;)Lcom/samsung/android/scloud/quota/AuthControlForNewDataRelay;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/NotificationService;
    .param p1, "x1"    # Lcom/samsung/android/scloud/quota/AuthControlForNewDataRelay;

    .prologue
    .line 18
    iput-object p1, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mAuthControlNew:Lcom/samsung/android/scloud/quota/AuthControlForNewDataRelay;

    return-object p1
.end method

.method static synthetic access$500(Lcom/samsung/android/scloud/quota/NotificationService;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/NotificationService;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/android/scloud/quota/NotificationService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/NotificationService;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mCtid:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/android/scloud/quota/NotificationService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/NotificationService;

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/samsung/android/scloud/quota/NotificationService;->startQuotaThread()V

    return-void
.end method

.method private showNotification(I)V
    .locals 13
    .param p1, "id"    # I

    .prologue
    const v12, 0x7f070036

    const v9, 0x7f07002d

    const v8, 0x7f070019

    const/high16 v11, 0x10000000

    const/4 v10, 0x0

    .line 193
    const-string v5, "NotificationService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "showNotification() : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    packed-switch p1, :pswitch_data_0

    .line 386
    :goto_0
    iget-object v5, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mNotificationManager:Landroid/app/NotificationManager;

    iget v6, p0, Lcom/samsung/android/scloud/quota/NotificationService;->notificationID:I

    iget-object v7, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mNotification:Landroid/app/Notification;

    invoke-virtual {v5, v6, v7}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 387
    const-string v5, "NotificationService"

    const-string v6, "Stop Notification Service"

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 388
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/NotificationService;->stopSelf()V

    .line 390
    return-void

    .line 198
    :pswitch_0
    const-string v5, "NotificationService"

    const-string v6, "OTHER_NOTICE"

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    iget-object v5, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mNotification:Landroid/app/Notification;

    iget v6, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mNotifationFlags:I

    or-int/lit8 v6, v6, 0x10

    iput v6, v5, Landroid/app/Notification;->flags:I

    .line 200
    const/4 v0, 0x0

    .line 201
    .local v0, "currentPlanMessage":Ljava/lang/String;
    const/4 v2, 0x0

    .line 203
    .local v2, "nextPlanMessage":Ljava/lang/String;
    const-string v5, "Y"

    iget-object v6, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mCommerceTargetYN:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 205
    const-string v5, "0.0"

    iget-object v6, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mCurrentPlanAmt:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 206
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v8}, Lcom/samsung/android/scloud/quota/NotificationService;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ": "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mCurrentPlan:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0, v9}, Lcom/samsung/android/scloud/quota/NotificationService;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 210
    :goto_1
    const-string v5, "0.0"

    iget-object v6, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mNextPlanAmt:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 211
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v12}, Lcom/samsung/android/scloud/quota/NotificationService;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mNextPlan:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0, v9}, Lcom/samsung/android/scloud/quota/NotificationService;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 215
    :goto_2
    const-string v5, "NotificationService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Current Plan Message is : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    const-string v5, "NotificationService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Next Plan Message is : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    const-string v5, "NotificationService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Next Plan Message is : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mRenewalDate:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    :cond_0
    :goto_3
    const-string v5, "QUOTAFULL"

    iget-object v6, p0, Lcom/samsung/android/scloud/quota/NotificationService;->code:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 344
    const-string v5, "NotificationService"

    const-string v6, "QUOTAFULL"

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    const v5, 0x7f070059

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/quota/NotificationService;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/scloud/quota/NotificationService;->notificationTitle:Ljava/lang/CharSequence;

    .line 346
    const v5, 0x7f070058

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/quota/NotificationService;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/scloud/quota/NotificationService;->notificationContent:Ljava/lang/CharSequence;

    .line 347
    iget-object v5, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mNotificationIntent:Landroid/content/Intent;

    const-string v6, "DIALOG"

    const/16 v7, 0xe

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 348
    iget-object v5, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mNotificationIntent:Landroid/content/Intent;

    const-string v6, "current_plan_message"

    invoke-virtual {v5, v6, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 366
    :cond_1
    :goto_4
    iget-object v5, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mNotificationIntent:Landroid/content/Intent;

    invoke-static {p0, v10, v5, v11}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mPendingIntent:Landroid/app/PendingIntent;

    .line 367
    iget-object v5, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mNotification:Landroid/app/Notification;

    iget-object v6, p0, Lcom/samsung/android/scloud/quota/NotificationService;->notificationTitle:Ljava/lang/CharSequence;

    iget-object v7, p0, Lcom/samsung/android/scloud/quota/NotificationService;->notificationContent:Ljava/lang/CharSequence;

    iget-object v8, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mPendingIntent:Landroid/app/PendingIntent;

    invoke-virtual {v5, p0, v6, v7, v8}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 368
    iget-object v5, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mNotification:Landroid/app/Notification;

    iget-object v6, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mPendingIntent:Landroid/app/PendingIntent;

    iput-object v6, v5, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    goto/16 :goto_0

    .line 208
    :cond_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v8}, Lcom/samsung/android/scloud/quota/NotificationService;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ": "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mCurrentPlan:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mCurrencySymbol:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mCurrentPlanAmt:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mCurrentPlanType:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 213
    :cond_3
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v12}, Lcom/samsung/android/scloud/quota/NotificationService;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mNextPlan:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mNextCurrencySymbol:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mNextPlanAmt:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mNextPlanType:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_2

    .line 219
    :cond_4
    const-string v5, "N"

    iget-object v6, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mCommerceTargetYN:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 221
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const v6, 0x7f070077

    invoke-virtual {p0, v6}, Lcom/samsung/android/scloud/quota/NotificationService;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ": "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mContext:Landroid/content/Context;

    iget-wide v8, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mTotalQuota:J

    invoke-static {v6, v8, v9}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    .line 351
    :cond_5
    const-string v5, "QUOTA_WARNING"

    iget-object v6, p0, Lcom/samsung/android/scloud/quota/NotificationService;->code:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 353
    const-string v5, "NotificationService"

    const-string v6, "QUOTA_WARNING"

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    iget-object v5, p0, Lcom/samsung/android/scloud/quota/NotificationService;->extra:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/32 v8, 0x100000

    div-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 355
    .local v4, "value":Ljava/lang/Long;
    invoke-virtual {v4}, Ljava/lang/Long;->doubleValue()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    .line 356
    .local v1, "d":Ljava/lang/Double;
    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Math;->floor(D)D

    move-result-wide v6

    double-to-int v3, v6

    .line 357
    .local v3, "quotaVal":I
    const v5, 0x7f070003

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/quota/NotificationService;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/scloud/quota/NotificationService;->notificationTitle:Ljava/lang/CharSequence;

    .line 358
    const v5, 0x7f070004

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-virtual {p0, v5, v6}, Lcom/samsung/android/scloud/quota/NotificationService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/scloud/quota/NotificationService;->notificationContent:Ljava/lang/CharSequence;

    .line 359
    iget-object v5, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mNotificationIntent:Landroid/content/Intent;

    const-string v6, "DIALOG"

    const/16 v7, 0xd

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 360
    iget-object v5, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mNotificationIntent:Landroid/content/Intent;

    const-string v6, "date_field"

    iget-object v7, p0, Lcom/samsung/android/scloud/quota/NotificationService;->extra:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 361
    iget-object v5, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mNotificationIntent:Landroid/content/Intent;

    const-string v6, "commerce_target"

    iget-object v7, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mCommerceTargetYN:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 362
    iget-object v5, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mNotificationIntent:Landroid/content/Intent;

    const-string v6, "current_plan_message"

    invoke-virtual {v5, v6, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_4

    .line 373
    .end local v0    # "currentPlanMessage":Ljava/lang/String;
    .end local v1    # "d":Ljava/lang/Double;
    .end local v2    # "nextPlanMessage":Ljava/lang/String;
    .end local v3    # "quotaVal":I
    .end local v4    # "value":Ljava/lang/Long;
    :pswitch_1
    const-string v5, "NotificationService"

    const-string v6, "DELETION_NOTICE"

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    iget-object v5, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mNotification:Landroid/app/Notification;

    iget v6, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mNotifationFlags:I

    or-int/lit8 v6, v6, 0x10

    iput v6, v5, Landroid/app/Notification;->flags:I

    .line 375
    const v5, 0x7f07001d

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/quota/NotificationService;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/scloud/quota/NotificationService;->notificationTitle:Ljava/lang/CharSequence;

    .line 376
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const v6, 0x7f07001c

    invoke-virtual {p0, v6}, Lcom/samsung/android/scloud/quota/NotificationService;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ": "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/scloud/quota/NotificationService;->extra:Ljava/lang/String;

    invoke-static {v6}, Lcom/samsung/android/scloud/quota/QuotaUtils;->changeDateFormat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/scloud/quota/NotificationService;->notificationContent:Ljava/lang/CharSequence;

    .line 377
    iget-object v5, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mNotificationIntent:Landroid/content/Intent;

    const-string v6, "DIALOG"

    const/16 v7, 0xc

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 378
    iget-object v5, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mNotificationIntent:Landroid/content/Intent;

    const-string v6, "date_field"

    iget-object v7, p0, Lcom/samsung/android/scloud/quota/NotificationService;->extra:Ljava/lang/String;

    invoke-static {v7}, Lcom/samsung/android/scloud/quota/QuotaUtils;->changeDateFormat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 379
    iget-object v5, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mNotificationIntent:Landroid/content/Intent;

    invoke-static {p0, v10, v5, v11}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mPendingIntent:Landroid/app/PendingIntent;

    .line 380
    iget-object v5, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mNotification:Landroid/app/Notification;

    iget-object v6, p0, Lcom/samsung/android/scloud/quota/NotificationService;->notificationTitle:Ljava/lang/CharSequence;

    iget-object v7, p0, Lcom/samsung/android/scloud/quota/NotificationService;->notificationContent:Ljava/lang/CharSequence;

    iget-object v8, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mPendingIntent:Landroid/app/PendingIntent;

    invoke-virtual {v5, p0, v6, v7, v8}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 381
    iget-object v5, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mNotification:Landroid/app/Notification;

    iget-object v6, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mPendingIntent:Landroid/app/PendingIntent;

    iput-object v6, v5, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    goto/16 :goto_0

    .line 194
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private startQuotaThread()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 139
    const-string v1, "NotificationService"

    const-string v2, "QUOTA_5101 : [START]"

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    new-instance v0, Lcom/samsung/android/scloud/quota/QuotaThread;

    invoke-direct {v0}, Lcom/samsung/android/scloud/quota/QuotaThread;-><init>()V

    .line 141
    .local v0, "mQuotaThread":Lcom/samsung/android/scloud/quota/QuotaThread;
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/quota/QuotaThread;->setContext(Landroid/content/Context;)V

    .line 142
    invoke-virtual {v0, p0}, Lcom/samsung/android/scloud/quota/QuotaThread;->setListener(Lcom/samsung/android/scloud/quota/QuotaResponseThreadListener;)V

    .line 143
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/quota/QuotaThread;->setIsBackupDetails(Ljava/lang/Boolean;)V

    .line 144
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/quota/QuotaThread;->setIsBackedupSize(Ljava/lang/Boolean;)V

    .line 145
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/quota/QuotaThread;->setISDeleteTrue(Ljava/lang/Boolean;)V

    .line 146
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mAuthManager:Lcom/samsung/android/scloud/quota/AuthManager;

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/quota/QuotaThread;->setAuthManager(Lcom/samsung/android/scloud/quota/AuthManager;)V

    .line 147
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/quota/QuotaThread;->setCtid(Ljava/lang/String;)V

    .line 148
    invoke-virtual {v0}, Lcom/samsung/android/scloud/quota/QuotaThread;->start()V

    .line 149
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 76
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 6

    .prologue
    .line 85
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 86
    const-string v0, "NotificationService"

    const-string v1, "Service OnCreate()"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/NotificationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mContext:Landroid/content/Context;

    .line 88
    new-instance v0, Landroid/app/Notification;

    const v1, 0x7f020008

    const v2, 0x7f070074

    invoke-virtual {p0, v2}, Lcom/samsung/android/scloud/quota/NotificationService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v0, v1, v2, v4, v5}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mNotification:Landroid/app/Notification;

    .line 91
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/quota/NotificationService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mNotificationManager:Landroid/app/NotificationManager;

    .line 92
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mNotification:Landroid/app/Notification;

    iget v0, v0, Landroid/app/Notification;->flags:I

    iput v0, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mNotifationFlags:I

    .line 93
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/android/scloud/quota/NotificationActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mNotificationIntent:Landroid/content/Intent;

    .line 94
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mNotificationIntent:Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 95
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mNotificationIntent:Landroid/content/Intent;

    const-string v1, "android.intent.category.DEFAULT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 96
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mNotificationIntent:Landroid/content/Intent;

    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 97
    const-string v0, "NotificationService"

    const-string v1, "Service onCreate() end"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    invoke-static {}, Lcom/samsung/android/scloud/quota/QuotaUtils;->generateCTID()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mCtid:Ljava/lang/String;

    .line 99
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 398
    const-string v0, "NotificationService"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 399
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 400
    return-void
.end method

.method public onResponse(Landroid/os/Message;)V
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/NotificationService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 135
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v3, 0x0

    .line 60
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    .line 61
    const-string v0, "NotificationService"

    const-string v1, "Service onStartCommand()"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    const-string v0, "CODE"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/NotificationService;->code:Ljava/lang/String;

    .line 63
    const-string v0, "Extra"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/NotificationService;->extra:Ljava/lang/String;

    .line 64
    const-string v0, "NotificationService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CODE And Extra vaue is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/quota/NotificationService;->code:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/quota/NotificationService;->extra:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    const-string v0, "CMRC007"

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/NotificationService;->code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 67
    new-instance v0, Lcom/samsung/android/scloud/quota/NotificationService$AccessTokenclass;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/scloud/quota/NotificationService$AccessTokenclass;-><init>(Lcom/samsung/android/scloud/quota/NotificationService;Lcom/samsung/android/scloud/quota/NotificationService$1;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, ""

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/quota/NotificationService$AccessTokenclass;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 70
    :goto_0
    const/4 v0, 0x2

    return v0

    .line 69
    :cond_0
    invoke-direct {p0, v3}, Lcom/samsung/android/scloud/quota/NotificationService;->showNotification(I)V

    goto :goto_0
.end method
