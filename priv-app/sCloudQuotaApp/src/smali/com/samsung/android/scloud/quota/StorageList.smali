.class public Lcom/samsung/android/scloud/quota/StorageList;
.super Landroid/preference/PreferenceActivity;
.source "StorageList.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;
.implements Lcom/samsung/android/scloud/quota/QuotaResponseThreadListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/scloud/quota/StorageList$CheckSamsungAccountValidation;,
        Lcom/samsung/android/scloud/quota/StorageList$AccessTokenclass;
    }
.end annotation


# static fields
.field private static final ACTION_SAMSUNG_ACCOUNT_VALIDATION:Ljava/lang/String; = "com.msc.action.VALIDATION_CHECK_RESPONSE"

.field private static final LRE:C = '\u202a'

.field public static final ONLY_SUPPORT_AIDL_OF_SAMSUNGACCOUNT_VERSION:I = 0x30d40

.field private static final PDF:C = '\u202c'

.field private static final PREF_BACKEDUPDATA:Ljava/lang/String; = "pref_backedupdata"

.field private static final PREF_BARCHART:Ljava/lang/String; = "pref_barchart"

.field private static final PREF_BOOKMARKS:Ljava/lang/String; = "pref_bookmarks"

.field private static final PREF_CONTACTS:Ljava/lang/String; = "pref_contacts"

.field private static final PREF_PINBOARD:Ljava/lang/String; = "pref_pinboard"

.field private static final PREF_PLAN:Ljava/lang/String; = "pref_current_plan"

.field private static final PREF_SNB:Ljava/lang/String; = "pref_snb"

.field private static final PREF_SPD:Ljava/lang/String; = "pref_spd"

.field private static final PREF_SPLANNER:Ljava/lang/String; = "pref_splanner"

.field private static final REQUEST_CODE_WIFI_SETUP_ACTIVITY:I = 0x3

.field private static final RLM:C = '\u200f'

.field private static final RLM_STRING:Ljava/lang/String;

.field private static final SA_REQUEST_ID_VALIDATION:I = 0x1

.field private static final TAG:Ljava/lang/String; = "StorageList"

.field private static final VERIFICATION_OK:Ljava/lang/String; = "verificationOk"

.field public static mContext:Landroid/content/Context;

.field private static mProgressDialog:Landroid/app/ProgressDialog;

.field private static mmemoNotePackage:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field backupColor:I

.field bookmarksColor:I

.field contactsColor:I

.field familyColor:I

.field isAccessToken:Ljava/lang/Boolean;

.field private isActivityRunning:Ljava/lang/Boolean;

.field private isMemoOrNote:I

.field isPlan:Ljava/lang/Boolean;

.field private isSupportAidlOnly:Z

.field private mActivity:Landroid/app/Activity;

.field private mAuthControlNew:Lcom/samsung/android/scloud/quota/AuthControlForNewDataRelay;

.field protected mAuthManager:Lcom/samsung/android/scloud/quota/AuthManager;

.field private mBNRUSage:J

.field private mBrowserUsage:J

.field private mCalendarUsage:J

.field private mContactUsage:J

.field private mCtid:Ljava/lang/String;

.field public mDialog:Landroid/app/Dialog;

.field private final mHandler:Landroid/os/Handler;

.field private mIntentToStartActivity:Landroid/content/Intent;

.field private mIsBackedupData:Ljava/lang/Boolean;

.field private mIsConnected:Ljava/lang/Boolean;

.field private mKmemoUsage:J

.field private mPackageName:Ljava/lang/String;

.field private mPinboardUsage:J

.field private mPrefBackupData:Landroid/preference/Preference;

.field private mPrefBookmarks:Landroid/preference/Preference;

.field private mPrefContacts:Landroid/preference/Preference;

.field private mPrefPinboard:Landroid/preference/Preference;

.field private mPrefPlan:Landroid/preference/Preference;

.field private mPrefSNote:Landroid/preference/Preference;

.field private mPrefSNote3:Landroid/preference/Preference;

.field private mPrefSPlanner:Landroid/preference/Preference;

.field mResources:Landroid/content/res/Resources;

.field private mSBrowserScrapUsage:J

.field private mSBrowserUsage:J

.field private mSMemoUsage:J

.field private mSamsungAccountReceiver:Landroid/content/BroadcastReceiver;

.field private mSnote3Usage:J

.field private mSnoteUsage:J

.field private mSourceKeyPreference:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/preference/Preference;",
            ">;"
        }
    .end annotation
.end field

.field private mTmemoUsage:J

.field private mTotalUsage:J

.field private mUsageBarPreference:Lcom/samsung/android/scloud/quota/UsageBarPreference;

.field private mUsageofApp:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mValidAppList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mValidationStatus:I

.field pinboardColor:I

.field quotaSumary:Landroid/widget/TextView;

.field smemoColor:I

.field snote3Color:I

.field snoteColor:I

.field splannerColor:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 90
    const/16 v0, 0x200f

    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/scloud/quota/StorageList;->RLM_STRING:Ljava/lang/String;

    .line 98
    sput-object v1, Lcom/samsung/android/scloud/quota/StorageList;->mContext:Landroid/content/Context;

    .line 131
    sput-object v1, Lcom/samsung/android/scloud/quota/StorageList;->mProgressDialog:Landroid/app/ProgressDialog;

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    .line 43
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 56
    iput v4, p0, Lcom/samsung/android/scloud/quota/StorageList;->isMemoOrNote:I

    .line 69
    iput-object v1, p0, Lcom/samsung/android/scloud/quota/StorageList;->mAuthManager:Lcom/samsung/android/scloud/quota/AuthManager;

    .line 70
    iput-object v1, p0, Lcom/samsung/android/scloud/quota/StorageList;->mAuthControlNew:Lcom/samsung/android/scloud/quota/AuthControlForNewDataRelay;

    .line 72
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->isAccessToken:Ljava/lang/Boolean;

    .line 73
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->isPlan:Ljava/lang/Boolean;

    .line 74
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mIsBackedupData:Ljava/lang/Boolean;

    .line 99
    iput-wide v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mTotalUsage:J

    .line 100
    iput-wide v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mContactUsage:J

    .line 101
    iput-wide v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mSnoteUsage:J

    .line 102
    iput-wide v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mSnote3Usage:J

    .line 103
    iput-wide v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mSMemoUsage:J

    .line 104
    iput-wide v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mTmemoUsage:J

    .line 105
    iput-wide v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mKmemoUsage:J

    .line 106
    iput-wide v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mCalendarUsage:J

    .line 107
    iput-wide v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mBrowserUsage:J

    .line 108
    iput-wide v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mBNRUSage:J

    .line 109
    iput-wide v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mSBrowserUsage:J

    .line 110
    iput-wide v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPinboardUsage:J

    .line 111
    iput-wide v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mSBrowserScrapUsage:J

    .line 114
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mSourceKeyPreference:Ljava/util/HashMap;

    .line 115
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mValidAppList:Ljava/util/HashMap;

    .line 116
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mUsageofApp:Ljava/util/HashMap;

    .line 120
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mIsConnected:Ljava/lang/Boolean;

    .line 122
    iput-object v1, p0, Lcom/samsung/android/scloud/quota/StorageList;->mDialog:Landroid/app/Dialog;

    .line 123
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->isActivityRunning:Ljava/lang/Boolean;

    .line 124
    iput-object v1, p0, Lcom/samsung/android/scloud/quota/StorageList;->mCtid:Ljava/lang/String;

    .line 126
    iput-object v1, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPackageName:Ljava/lang/String;

    .line 128
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mValidationStatus:I

    .line 130
    iput-object v1, p0, Lcom/samsung/android/scloud/quota/StorageList;->mIntentToStartActivity:Landroid/content/Intent;

    .line 132
    iput-object v1, p0, Lcom/samsung/android/scloud/quota/StorageList;->mActivity:Landroid/app/Activity;

    .line 134
    iput-object v1, p0, Lcom/samsung/android/scloud/quota/StorageList;->mSamsungAccountReceiver:Landroid/content/BroadcastReceiver;

    .line 135
    iput-boolean v4, p0, Lcom/samsung/android/scloud/quota/StorageList;->isSupportAidlOnly:Z

    .line 947
    new-instance v0, Lcom/samsung/android/scloud/quota/StorageList$4;

    invoke-direct {v0, p0}, Lcom/samsung/android/scloud/quota/StorageList$4;-><init>(Lcom/samsung/android/scloud/quota/StorageList;)V

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mHandler:Landroid/os/Handler;

    .line 1291
    return-void
.end method

.method static synthetic access$1000(Lcom/samsung/android/scloud/quota/StorageList;)J
    .locals 2
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;

    .prologue
    .line 43
    iget-wide v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mKmemoUsage:J

    return-wide v0
.end method

.method static synthetic access$1002(Lcom/samsung/android/scloud/quota/StorageList;J)J
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;
    .param p1, "x1"    # J

    .prologue
    .line 43
    iput-wide p1, p0, Lcom/samsung/android/scloud/quota/StorageList;->mKmemoUsage:J

    return-wide p1
.end method

.method static synthetic access$1100(Lcom/samsung/android/scloud/quota/StorageList;)J
    .locals 2
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;

    .prologue
    .line 43
    iget-wide v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mSnote3Usage:J

    return-wide v0
.end method

.method static synthetic access$1102(Lcom/samsung/android/scloud/quota/StorageList;J)J
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;
    .param p1, "x1"    # J

    .prologue
    .line 43
    iput-wide p1, p0, Lcom/samsung/android/scloud/quota/StorageList;->mSnote3Usage:J

    return-wide p1
.end method

.method static synthetic access$1200(Lcom/samsung/android/scloud/quota/StorageList;)J
    .locals 2
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;

    .prologue
    .line 43
    iget-wide v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mCalendarUsage:J

    return-wide v0
.end method

.method static synthetic access$1202(Lcom/samsung/android/scloud/quota/StorageList;J)J
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;
    .param p1, "x1"    # J

    .prologue
    .line 43
    iput-wide p1, p0, Lcom/samsung/android/scloud/quota/StorageList;->mCalendarUsage:J

    return-wide p1
.end method

.method static synthetic access$1300(Lcom/samsung/android/scloud/quota/StorageList;)J
    .locals 2
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;

    .prologue
    .line 43
    iget-wide v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mBrowserUsage:J

    return-wide v0
.end method

.method static synthetic access$1302(Lcom/samsung/android/scloud/quota/StorageList;J)J
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;
    .param p1, "x1"    # J

    .prologue
    .line 43
    iput-wide p1, p0, Lcom/samsung/android/scloud/quota/StorageList;->mBrowserUsage:J

    return-wide p1
.end method

.method static synthetic access$1400(Lcom/samsung/android/scloud/quota/StorageList;)J
    .locals 2
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;

    .prologue
    .line 43
    iget-wide v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mSBrowserUsage:J

    return-wide v0
.end method

.method static synthetic access$1402(Lcom/samsung/android/scloud/quota/StorageList;J)J
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;
    .param p1, "x1"    # J

    .prologue
    .line 43
    iput-wide p1, p0, Lcom/samsung/android/scloud/quota/StorageList;->mSBrowserUsage:J

    return-wide p1
.end method

.method static synthetic access$1500(Lcom/samsung/android/scloud/quota/StorageList;)J
    .locals 2
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;

    .prologue
    .line 43
    iget-wide v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mSBrowserScrapUsage:J

    return-wide v0
.end method

.method static synthetic access$1502(Lcom/samsung/android/scloud/quota/StorageList;J)J
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;
    .param p1, "x1"    # J

    .prologue
    .line 43
    iput-wide p1, p0, Lcom/samsung/android/scloud/quota/StorageList;->mSBrowserScrapUsage:J

    return-wide p1
.end method

.method static synthetic access$1600(Lcom/samsung/android/scloud/quota/StorageList;)J
    .locals 2
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;

    .prologue
    .line 43
    iget-wide v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPinboardUsage:J

    return-wide v0
.end method

.method static synthetic access$1602(Lcom/samsung/android/scloud/quota/StorageList;J)J
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;
    .param p1, "x1"    # J

    .prologue
    .line 43
    iput-wide p1, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPinboardUsage:J

    return-wide p1
.end method

.method static synthetic access$1702(Lcom/samsung/android/scloud/quota/StorageList;J)J
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;
    .param p1, "x1"    # J

    .prologue
    .line 43
    iput-wide p1, p0, Lcom/samsung/android/scloud/quota/StorageList;->mTotalUsage:J

    return-wide p1
.end method

.method static synthetic access$1800(Lcom/samsung/android/scloud/quota/StorageList;)Ljava/lang/Boolean;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mIsBackedupData:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic access$1802(Lcom/samsung/android/scloud/quota/StorageList;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;
    .param p1, "x1"    # Ljava/lang/Boolean;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/samsung/android/scloud/quota/StorageList;->mIsBackedupData:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic access$1900(Lcom/samsung/android/scloud/quota/StorageList;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/samsung/android/scloud/quota/StorageList;->initPref()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/android/scloud/quota/StorageList;Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/Boolean;

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/scloud/quota/StorageList;->setSharedPref(Ljava/lang/String;Ljava/lang/Boolean;)V

    return-void
.end method

.method static synthetic access$2000(Lcom/samsung/android/scloud/quota/StorageList;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/samsung/android/scloud/quota/StorageList;->setQuotaSummary()V

    return-void
.end method

.method static synthetic access$2100(Lcom/samsung/android/scloud/quota/StorageList;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/samsung/android/scloud/quota/StorageList;->setSummary()V

    return-void
.end method

.method static synthetic access$2200(Lcom/samsung/android/scloud/quota/StorageList;)Ljava/lang/Boolean;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->isActivityRunning:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/samsung/android/scloud/quota/StorageList;)Lcom/samsung/android/scloud/quota/UsageBarPreference;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mUsageBarPreference:Lcom/samsung/android/scloud/quota/UsageBarPreference;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/samsung/android/scloud/quota/StorageList;)Landroid/preference/Preference;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefBackupData:Landroid/preference/Preference;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/samsung/android/scloud/quota/StorageList;)Landroid/preference/Preference;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefPlan:Landroid/preference/Preference;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/samsung/android/scloud/quota/StorageList;)Landroid/preference/Preference;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefSNote:Landroid/preference/Preference;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/samsung/android/scloud/quota/StorageList;)Landroid/preference/Preference;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefSNote3:Landroid/preference/Preference;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/samsung/android/scloud/quota/StorageList;)Landroid/preference/Preference;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefBookmarks:Landroid/preference/Preference;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/samsung/android/scloud/quota/StorageList;)Landroid/preference/Preference;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefContacts:Landroid/preference/Preference;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/scloud/quota/StorageList;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;
    .param p1, "x1"    # I

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/samsung/android/scloud/quota/StorageList;->startApplication(I)V

    return-void
.end method

.method static synthetic access$3000(Lcom/samsung/android/scloud/quota/StorageList;)Landroid/preference/Preference;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefSPlanner:Landroid/preference/Preference;

    return-object v0
.end method

.method static synthetic access$3100(Lcom/samsung/android/scloud/quota/StorageList;)Landroid/preference/Preference;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefPinboard:Landroid/preference/Preference;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/samsung/android/scloud/quota/StorageList;)Lcom/samsung/android/scloud/quota/AuthControlForNewDataRelay;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mAuthControlNew:Lcom/samsung/android/scloud/quota/AuthControlForNewDataRelay;

    return-object v0
.end method

.method static synthetic access$3202(Lcom/samsung/android/scloud/quota/StorageList;Lcom/samsung/android/scloud/quota/AuthControlForNewDataRelay;)Lcom/samsung/android/scloud/quota/AuthControlForNewDataRelay;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;
    .param p1, "x1"    # Lcom/samsung/android/scloud/quota/AuthControlForNewDataRelay;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/samsung/android/scloud/quota/StorageList;->mAuthControlNew:Lcom/samsung/android/scloud/quota/AuthControlForNewDataRelay;

    return-object p1
.end method

.method static synthetic access$3300(Lcom/samsung/android/scloud/quota/StorageList;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mCtid:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/samsung/android/scloud/quota/StorageList;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/samsung/android/scloud/quota/StorageList;->startQuotaThread()V

    return-void
.end method

.method static synthetic access$3500(Lcom/samsung/android/scloud/quota/StorageList;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$3600()Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/samsung/android/scloud/quota/StorageList;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$3602(Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0
    .param p0, "x0"    # Landroid/app/ProgressDialog;

    .prologue
    .line 43
    sput-object p0, Lcom/samsung/android/scloud/quota/StorageList;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object p0
.end method

.method static synthetic access$3700(Lcom/samsung/android/scloud/quota/StorageList;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->isSupportAidlOnly:Z

    return v0
.end method

.method static synthetic access$3702(Lcom/samsung/android/scloud/quota/StorageList;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;
    .param p1, "x1"    # Z

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/samsung/android/scloud/quota/StorageList;->isSupportAidlOnly:Z

    return p1
.end method

.method static synthetic access$3800(Lcom/samsung/android/scloud/quota/StorageList;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;

    .prologue
    .line 43
    iget v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mValidationStatus:I

    return v0
.end method

.method static synthetic access$3802(Lcom/samsung/android/scloud/quota/StorageList;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;
    .param p1, "x1"    # I

    .prologue
    .line 43
    iput p1, p0, Lcom/samsung/android/scloud/quota/StorageList;->mValidationStatus:I

    return p1
.end method

.method static synthetic access$3900(Lcom/samsung/android/scloud/quota/StorageList;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mIntentToStartActivity:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$3902(Lcom/samsung/android/scloud/quota/StorageList;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/samsung/android/scloud/quota/StorageList;->mIntentToStartActivity:Landroid/content/Intent;

    return-object p1
.end method

.method static synthetic access$400(Lcom/samsung/android/scloud/quota/StorageList;)J
    .locals 2
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;

    .prologue
    .line 43
    iget-wide v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mBNRUSage:J

    return-wide v0
.end method

.method static synthetic access$4000(Lcom/samsung/android/scloud/quota/StorageList;)Landroid/content/BroadcastReceiver;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mSamsungAccountReceiver:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method static synthetic access$4002(Lcom/samsung/android/scloud/quota/StorageList;Landroid/content/BroadcastReceiver;)Landroid/content/BroadcastReceiver;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;
    .param p1, "x1"    # Landroid/content/BroadcastReceiver;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/samsung/android/scloud/quota/StorageList;->mSamsungAccountReceiver:Landroid/content/BroadcastReceiver;

    return-object p1
.end method

.method static synthetic access$402(Lcom/samsung/android/scloud/quota/StorageList;J)J
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;
    .param p1, "x1"    # J

    .prologue
    .line 43
    iput-wide p1, p0, Lcom/samsung/android/scloud/quota/StorageList;->mBNRUSage:J

    return-wide p1
.end method

.method static synthetic access$500(Lcom/samsung/android/scloud/quota/StorageList;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mUsageofApp:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/android/scloud/quota/StorageList;)J
    .locals 2
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;

    .prologue
    .line 43
    iget-wide v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mContactUsage:J

    return-wide v0
.end method

.method static synthetic access$602(Lcom/samsung/android/scloud/quota/StorageList;J)J
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;
    .param p1, "x1"    # J

    .prologue
    .line 43
    iput-wide p1, p0, Lcom/samsung/android/scloud/quota/StorageList;->mContactUsage:J

    return-wide p1
.end method

.method static synthetic access$700(Lcom/samsung/android/scloud/quota/StorageList;)J
    .locals 2
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;

    .prologue
    .line 43
    iget-wide v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mSnoteUsage:J

    return-wide v0
.end method

.method static synthetic access$702(Lcom/samsung/android/scloud/quota/StorageList;J)J
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;
    .param p1, "x1"    # J

    .prologue
    .line 43
    iput-wide p1, p0, Lcom/samsung/android/scloud/quota/StorageList;->mSnoteUsage:J

    return-wide p1
.end method

.method static synthetic access$800(Lcom/samsung/android/scloud/quota/StorageList;)J
    .locals 2
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;

    .prologue
    .line 43
    iget-wide v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mSMemoUsage:J

    return-wide v0
.end method

.method static synthetic access$802(Lcom/samsung/android/scloud/quota/StorageList;J)J
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;
    .param p1, "x1"    # J

    .prologue
    .line 43
    iput-wide p1, p0, Lcom/samsung/android/scloud/quota/StorageList;->mSMemoUsage:J

    return-wide p1
.end method

.method static synthetic access$900(Lcom/samsung/android/scloud/quota/StorageList;)J
    .locals 2
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;

    .prologue
    .line 43
    iget-wide v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mTmemoUsage:J

    return-wide v0
.end method

.method static synthetic access$902(Lcom/samsung/android/scloud/quota/StorageList;J)J
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;
    .param p1, "x1"    # J

    .prologue
    .line 43
    iput-wide p1, p0, Lcom/samsung/android/scloud/quota/StorageList;->mTmemoUsage:J

    return-wide p1
.end method

.method private static createRectShape(III)Landroid/graphics/drawable/ShapeDrawable;
    .locals 2
    .param p0, "width"    # I
    .param p1, "height"    # I
    .param p2, "color"    # I

    .prologue
    .line 1252
    new-instance v0, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v1, Landroid/graphics/drawable/shapes/RectShape;

    invoke-direct {v1}, Landroid/graphics/drawable/shapes/RectShape;-><init>()V

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    .line 1253
    .local v0, "shape":Landroid/graphics/drawable/ShapeDrawable;
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/ShapeDrawable;->setIntrinsicHeight(I)V

    .line 1254
    invoke-virtual {v0, p0}, Landroid/graphics/drawable/ShapeDrawable;->setIntrinsicWidth(I)V

    .line 1255
    invoke-virtual {v0}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1256
    return-object v0
.end method

.method private displayAlert()V
    .locals 5

    .prologue
    .line 769
    const v2, 0x7f07007d

    invoke-virtual {p0, v2}, Lcom/samsung/android/scloud/quota/StorageList;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 770
    .local v1, "title":Ljava/lang/String;
    const v2, 0x7f07007b

    invoke-virtual {p0, v2}, Lcom/samsung/android/scloud/quota/StorageList;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 772
    .local v0, "description":Ljava/lang/String;
    const-string v2, "StorageList"

    const-string v3, "displayAlert - unable to open file"

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 773
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x104000a

    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/quota/StorageList;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/samsung/android/scloud/quota/StorageList$3;

    invoke-direct {v4, p0}, Lcom/samsung/android/scloud/quota/StorageList$3;-><init>(Lcom/samsung/android/scloud/quota/StorageList;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mDialog:Landroid/app/Dialog;

    .line 785
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v2}, Landroid/app/Dialog;->show()V

    .line 787
    return-void
.end method

.method private displayDialougBox(Ljava/lang/String;I)V
    .locals 10
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "appNum"    # I

    .prologue
    const/4 v9, 0x0

    .line 711
    invoke-direct {p0, p1}, Lcom/samsung/android/scloud/quota/StorageList;->getSharedPref(Ljava/lang/String;)Z

    move-result v4

    .line 713
    .local v4, "isChecked":Z
    const-string v7, "StorageList"

    const-string v8, "displayDialougBox - unable to delete dailog"

    invoke-static {v7, v8}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 715
    if-nez v4, :cond_0

    .line 717
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/StorageList;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    .line 719
    .local v3, "inflater":Landroid/view/LayoutInflater;
    const v7, 0x7f03000c

    const/4 v8, 0x0

    invoke-virtual {v3, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 722
    .local v1, "checkboxLayout":Landroid/view/View;
    invoke-virtual {v1, v9}, Landroid/view/View;->playSoundEffect(I)V

    .line 723
    const v7, 0x7f0a0010

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 725
    .local v0, "cb":Landroid/widget/CheckBox;
    new-instance v7, Lcom/samsung/android/scloud/quota/StorageList$1;

    invoke-direct {v7, p0}, Lcom/samsung/android/scloud/quota/StorageList$1;-><init>(Lcom/samsung/android/scloud/quota/StorageList;)V

    invoke-virtual {v0, v7}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 733
    const v7, 0x7f0a000f

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 735
    .local v6, "tv":Landroid/widget/TextView;
    invoke-virtual {v6, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 737
    const v7, 0x7f07007a

    invoke-virtual {p0, v7}, Lcom/samsung/android/scloud/quota/StorageList;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 738
    .local v5, "title":Ljava/lang/String;
    const v7, 0x7f070006

    invoke-virtual {p0, v7}, Lcom/samsung/android/scloud/quota/StorageList;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 741
    .local v2, "description":Ljava/lang/String;
    invoke-virtual {v6, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 742
    new-instance v7, Landroid/app/AlertDialog$Builder;

    invoke-direct {v7, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v7, v5}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    invoke-virtual {v7, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    const v8, 0x104000a

    new-instance v9, Lcom/samsung/android/scloud/quota/StorageList$2;

    invoke-direct {v9, p0, v0, p1, p2}, Lcom/samsung/android/scloud/quota/StorageList$2;-><init>(Lcom/samsung/android/scloud/quota/StorageList;Landroid/widget/CheckBox;Ljava/lang/String;I)V

    invoke-virtual {v7, v8, v9}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/android/scloud/quota/StorageList;->mDialog:Landroid/app/Dialog;

    .line 761
    iget-object v7, p0, Lcom/samsung/android/scloud/quota/StorageList;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v7}, Landroid/app/Dialog;->show()V

    .line 766
    .end local v0    # "cb":Landroid/widget/CheckBox;
    .end local v1    # "checkboxLayout":Landroid/view/View;
    .end local v2    # "description":Ljava/lang/String;
    .end local v3    # "inflater":Landroid/view/LayoutInflater;
    .end local v5    # "title":Ljava/lang/String;
    .end local v6    # "tv":Landroid/widget/TextView;
    :goto_0
    return-void

    .line 763
    :cond_0
    invoke-direct {p0, p2}, Lcom/samsung/android/scloud/quota/StorageList;->startApplication(I)V

    goto :goto_0
.end method

.method private getInstalledAppList()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 406
    const-string v0, "StorageList"

    const-string v1, "[getInstalledAppList]: START"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mSourceKeyPreference:Ljava/util/HashMap;

    const-string v1, "pref_snb"

    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefSNote:Landroid/preference/Preference;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 408
    iget v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->isMemoOrNote:I

    if-eqz v0, :cond_3

    .line 409
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mValidAppList:Ljava/util/HashMap;

    const-string v1, "pref_snb"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 413
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mSourceKeyPreference:Ljava/util/HashMap;

    const-string v1, "pref_spd"

    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefSNote3:Landroid/preference/Preference;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 414
    const-string v0, "com.samsung.android.snote"

    invoke-direct {p0, v0}, Lcom/samsung/android/scloud/quota/StorageList;->isAppInstalled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 415
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mValidAppList:Ljava/util/HashMap;

    const-string v1, "pref_spd"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 419
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mSourceKeyPreference:Ljava/util/HashMap;

    const-string v1, "pref_splanner"

    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefSPlanner:Landroid/preference/Preference;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 420
    const-string v0, "com.android.calendar"

    invoke-direct {p0, v0}, Lcom/samsung/android/scloud/quota/StorageList;->isAppInstalled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 421
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mValidAppList:Ljava/util/HashMap;

    const-string v1, "pref_splanner"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 425
    :goto_2
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mSourceKeyPreference:Ljava/util/HashMap;

    const-string v1, "pref_bookmarks"

    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefBookmarks:Landroid/preference/Preference;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 426
    const-string v0, "com.sec.android.app.sbrowser"

    invoke-direct {p0, v0}, Lcom/samsung/android/scloud/quota/StorageList;->isAppInstalled(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "com.android.browser"

    invoke-direct {p0, v0}, Lcom/samsung/android/scloud/quota/StorageList;->isAppInstalled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 428
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mValidAppList:Ljava/util/HashMap;

    const-string v1, "pref_bookmarks"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 432
    :goto_3
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mSourceKeyPreference:Ljava/util/HashMap;

    const-string v1, "pref_contacts"

    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefContacts:Landroid/preference/Preference;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 433
    const-string v0, "com.android.contacts"

    invoke-direct {p0, v0}, Lcom/samsung/android/scloud/quota/StorageList;->isAppInstalled(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "com.samsung.contacts"

    invoke-direct {p0, v0}, Lcom/samsung/android/scloud/quota/StorageList;->isAppInstalled(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "com.android.jcontact"

    invoke-direct {p0, v0}, Lcom/samsung/android/scloud/quota/StorageList;->isAppInstalled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 434
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mValidAppList:Ljava/util/HashMap;

    const-string v1, "pref_contacts"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 438
    :goto_4
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mSourceKeyPreference:Ljava/util/HashMap;

    const-string v1, "pref_pinboard"

    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefPinboard:Landroid/preference/Preference;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 439
    const-string v0, "com.samsung.android.app.pinboard"

    invoke-direct {p0, v0}, Lcom/samsung/android/scloud/quota/StorageList;->isAppInstalled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 440
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mValidAppList:Ljava/util/HashMap;

    const-string v1, "pref_pinboard"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 444
    :goto_5
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mSourceKeyPreference:Ljava/util/HashMap;

    const-string v1, "pref_backedupdata"

    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefBackupData:Landroid/preference/Preference;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 445
    const-string v0, "com.sec.android.sCloudBackupApp"

    invoke-direct {p0, v0}, Lcom/samsung/android/scloud/quota/StorageList;->isAppInstalled(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "com.samsung.android.scloud.backup"

    invoke-direct {p0, v0}, Lcom/samsung/android/scloud/quota/StorageList;->isAppInstalled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 446
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mValidAppList:Ljava/util/HashMap;

    const-string v1, "pref_backedupdata"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 450
    :goto_6
    const-string v0, "StorageList"

    const-string v1, "[getInstalledAppList] : END"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 452
    return-void

    .line 411
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mValidAppList:Ljava/util/HashMap;

    const-string v1, "pref_snb"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 417
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mValidAppList:Ljava/util/HashMap;

    const-string v1, "pref_spd"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 423
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mValidAppList:Ljava/util/HashMap;

    const-string v1, "pref_splanner"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 430
    :cond_6
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mValidAppList:Ljava/util/HashMap;

    const-string v1, "pref_bookmarks"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_3

    .line 436
    :cond_7
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mValidAppList:Ljava/util/HashMap;

    const-string v1, "pref_contacts"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_4

    .line 442
    :cond_8
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mValidAppList:Ljava/util/HashMap;

    const-string v1, "pref_pinboard"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    .line 448
    :cond_9
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mValidAppList:Ljava/util/HashMap;

    const-string v1, "pref_backedupdata"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_6
.end method

.method private getMemoOrNotePackage()Ljava/lang/String;
    .locals 6

    .prologue
    .line 832
    const-string v3, "StorageList"

    const-string v4, "whichAppexist"

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 833
    sget-object v3, Lcom/samsung/android/scloud/quota/StorageList;->mmemoNotePackage:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 835
    .local v2, "key":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/StorageList;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const/4 v4, 0x5

    invoke-virtual {v3, v2, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    .line 839
    const-string v3, "StorageList"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Package exist"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 846
    .end local v2    # "key":Ljava/lang/String;
    :goto_1
    return-object v2

    .line 842
    .restart local v2    # "key":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 843
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v3, "StorageList"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Package not exist"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 846
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v2    # "key":Ljava/lang/String;
    :cond_0
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private getPreferencesReference()V
    .locals 10

    .prologue
    const v9, 0x7f070068

    const/4 v8, 0x2

    const/4 v7, 0x1

    const v6, 0x7f070012

    const/4 v5, 0x0

    .line 301
    const-string v2, "StorageList"

    const-string v3, "QUOTA_5100 : [START]"

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f06001c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v1, v2

    .line 305
    .local v1, "width":I
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f06001b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v0, v2

    .line 307
    .local v0, "height":I
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f05000d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->snoteColor:I

    .line 308
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f05000c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->snote3Color:I

    .line 309
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f05000e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->splannerColor:I

    .line 310
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f050003

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->bookmarksColor:I

    .line 311
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f050004

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->contactsColor:I

    .line 312
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mResources:Landroid/content/res/Resources;

    const/high16 v3, 0x7f050000

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->backupColor:I

    .line 313
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f05000b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->smemoColor:I

    .line 314
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f050008

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->pinboardColor:I

    .line 316
    const-string v2, "pref_current_plan"

    invoke-virtual {p0, v2}, Lcom/samsung/android/scloud/quota/StorageList;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefPlan:Landroid/preference/Preference;

    .line 317
    const-string v2, "pref_snb"

    invoke-virtual {p0, v2}, Lcom/samsung/android/scloud/quota/StorageList;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/scloud/quota/QuotaContentPreference;

    iput-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefSNote:Landroid/preference/Preference;

    .line 318
    const-string v2, "pref_spd"

    invoke-virtual {p0, v2}, Lcom/samsung/android/scloud/quota/StorageList;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/scloud/quota/QuotaContentPreference;

    iput-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefSNote3:Landroid/preference/Preference;

    .line 319
    const-string v2, "pref_splanner"

    invoke-virtual {p0, v2}, Lcom/samsung/android/scloud/quota/StorageList;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/scloud/quota/QuotaContentPreference;

    iput-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefSPlanner:Landroid/preference/Preference;

    .line 320
    const-string v2, "pref_bookmarks"

    invoke-virtual {p0, v2}, Lcom/samsung/android/scloud/quota/StorageList;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/scloud/quota/QuotaContentPreference;

    iput-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefBookmarks:Landroid/preference/Preference;

    .line 321
    const-string v2, "pref_contacts"

    invoke-virtual {p0, v2}, Lcom/samsung/android/scloud/quota/StorageList;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/scloud/quota/QuotaContentPreference;

    iput-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefContacts:Landroid/preference/Preference;

    .line 322
    const-string v2, "pref_pinboard"

    invoke-virtual {p0, v2}, Lcom/samsung/android/scloud/quota/StorageList;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/scloud/quota/QuotaContentPreference;

    iput-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefPinboard:Landroid/preference/Preference;

    .line 323
    const-string v2, "pref_backedupdata"

    invoke-virtual {p0, v2}, Lcom/samsung/android/scloud/quota/StorageList;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/scloud/quota/QuotaContentPreference;

    iput-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefBackupData:Landroid/preference/Preference;

    .line 324
    const-string v2, "pref_barchart"

    invoke-virtual {p0, v2}, Lcom/samsung/android/scloud/quota/StorageList;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/scloud/quota/UsageBarPreference;

    iput-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mUsageBarPreference:Lcom/samsung/android/scloud/quota/UsageBarPreference;

    .line 330
    invoke-direct {p0}, Lcom/samsung/android/scloud/quota/StorageList;->getMemoOrNotePackage()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPackageName:Ljava/lang/String;

    .line 331
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPackageName:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 332
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPackageName:Ljava/lang/String;

    const-string v3, "memo"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 333
    const-string v2, "StorageList"

    const-string v3, "Some Memo is present"

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    iput v7, p0, Lcom/samsung/android/scloud/quota/StorageList;->isMemoOrNote:I

    .line 336
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPackageName:Ljava/lang/String;

    const-string v3, "snote"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 337
    const-string v2, "StorageList"

    const-string v3, "Some Note is present"

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 338
    iput v8, p0, Lcom/samsung/android/scloud/quota/StorageList;->isMemoOrNote:I

    .line 342
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/scloud/quota/StorageList;->getInstalledAppList()V

    .line 344
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefSNote:Landroid/preference/Preference;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/high16 v4, 0x7f070000

    invoke-virtual {p0, v4}, Lcom/samsung/android/scloud/quota/StorageList;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const v4, 0x7f070067

    invoke-virtual {p0, v4}, Lcom/samsung/android/scloud/quota/StorageList;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0, v9}, Lcom/samsung/android/scloud/quota/StorageList;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "(.snb)"

    invoke-virtual {p0, v4}, Lcom/samsung/android/scloud/quota/StorageList;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 349
    sget-object v2, Lcom/samsung/android/scloud/quota/StorageList;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/android/scloud/quota/QuotaUtils;->isUSDevice(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 350
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefSPlanner:Landroid/preference/Preference;

    const v3, 0x7f070013

    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/quota/StorageList;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 351
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefSNote:Landroid/preference/Preference;

    invoke-virtual {v2, v6}, Landroid/preference/Preference;->setSummary(I)V

    .line 352
    iget v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->isMemoOrNote:I

    if-eq v2, v8, :cond_3

    iget v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->isMemoOrNote:I

    if-nez v2, :cond_5

    .line 354
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefSNote:Landroid/preference/Preference;

    iget v3, p0, Lcom/samsung/android/scloud/quota/StorageList;->snoteColor:I

    invoke-static {v1, v0, v3}, Lcom/samsung/android/scloud/quota/StorageList;->createRectShape(III)Landroid/graphics/drawable/ShapeDrawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 360
    :cond_4
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefSNote3:Landroid/preference/Preference;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v9}, Lcom/samsung/android/scloud/quota/StorageList;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "(.spd)"

    invoke-virtual {p0, v4}, Lcom/samsung/android/scloud/quota/StorageList;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 361
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefSNote3:Landroid/preference/Preference;

    iget v3, p0, Lcom/samsung/android/scloud/quota/StorageList;->snote3Color:I

    invoke-static {v1, v0, v3}, Lcom/samsung/android/scloud/quota/StorageList;->createRectShape(III)Landroid/graphics/drawable/ShapeDrawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 362
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefSNote3:Landroid/preference/Preference;

    invoke-virtual {v2, v6}, Landroid/preference/Preference;->setSummary(I)V

    .line 363
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefSPlanner:Landroid/preference/Preference;

    invoke-virtual {v2, v6}, Landroid/preference/Preference;->setSummary(I)V

    .line 364
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefSPlanner:Landroid/preference/Preference;

    iget v3, p0, Lcom/samsung/android/scloud/quota/StorageList;->splannerColor:I

    invoke-static {v1, v0, v3}, Lcom/samsung/android/scloud/quota/StorageList;->createRectShape(III)Landroid/graphics/drawable/ShapeDrawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 365
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefBookmarks:Landroid/preference/Preference;

    invoke-virtual {v2, v6}, Landroid/preference/Preference;->setSummary(I)V

    .line 366
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefBookmarks:Landroid/preference/Preference;

    iget v3, p0, Lcom/samsung/android/scloud/quota/StorageList;->bookmarksColor:I

    invoke-static {v1, v0, v3}, Lcom/samsung/android/scloud/quota/StorageList;->createRectShape(III)Landroid/graphics/drawable/ShapeDrawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 367
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefContacts:Landroid/preference/Preference;

    invoke-virtual {v2, v6}, Landroid/preference/Preference;->setSummary(I)V

    .line 368
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefContacts:Landroid/preference/Preference;

    iget v3, p0, Lcom/samsung/android/scloud/quota/StorageList;->contactsColor:I

    invoke-static {v1, v0, v3}, Lcom/samsung/android/scloud/quota/StorageList;->createRectShape(III)Landroid/graphics/drawable/ShapeDrawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 369
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefPinboard:Landroid/preference/Preference;

    invoke-virtual {v2, v6}, Landroid/preference/Preference;->setSummary(I)V

    .line 370
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefPinboard:Landroid/preference/Preference;

    iget v3, p0, Lcom/samsung/android/scloud/quota/StorageList;->pinboardColor:I

    invoke-static {v1, v0, v3}, Lcom/samsung/android/scloud/quota/StorageList;->createRectShape(III)Landroid/graphics/drawable/ShapeDrawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 371
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefBackupData:Landroid/preference/Preference;

    invoke-virtual {v2, v6}, Landroid/preference/Preference;->setSummary(I)V

    .line 372
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mUsageBarPreference:Lcom/samsung/android/scloud/quota/UsageBarPreference;

    invoke-virtual {v2, v6}, Lcom/samsung/android/scloud/quota/UsageBarPreference;->setSummary(I)V

    .line 373
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefPlan:Landroid/preference/Preference;

    invoke-virtual {v2, v6}, Landroid/preference/Preference;->setSummary(I)V

    .line 374
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefBackupData:Landroid/preference/Preference;

    iget v3, p0, Lcom/samsung/android/scloud/quota/StorageList;->backupColor:I

    invoke-static {v1, v0, v3}, Lcom/samsung/android/scloud/quota/StorageList;->createRectShape(III)Landroid/graphics/drawable/ShapeDrawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 376
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mUsageBarPreference:Lcom/samsung/android/scloud/quota/UsageBarPreference;

    invoke-virtual {v2, v5}, Lcom/samsung/android/scloud/quota/UsageBarPreference;->setEnabled(Z)V

    .line 377
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefPlan:Landroid/preference/Preference;

    invoke-virtual {v2, v5}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 378
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefSNote:Landroid/preference/Preference;

    invoke-virtual {v2, v5}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 379
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefSNote3:Landroid/preference/Preference;

    invoke-virtual {v2, v5}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 380
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefSPlanner:Landroid/preference/Preference;

    invoke-virtual {v2, v5}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 381
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefBookmarks:Landroid/preference/Preference;

    invoke-virtual {v2, v5}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 382
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefContacts:Landroid/preference/Preference;

    invoke-virtual {v2, v5}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 383
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefPinboard:Landroid/preference/Preference;

    invoke-virtual {v2, v5}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 384
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefBackupData:Landroid/preference/Preference;

    invoke-virtual {v2, v5}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 386
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefPlan:Landroid/preference/Preference;

    invoke-virtual {v2, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 387
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefSNote:Landroid/preference/Preference;

    invoke-virtual {v2, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 388
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefSNote3:Landroid/preference/Preference;

    invoke-virtual {v2, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 389
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefSPlanner:Landroid/preference/Preference;

    invoke-virtual {v2, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 390
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefBookmarks:Landroid/preference/Preference;

    invoke-virtual {v2, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 391
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefContacts:Landroid/preference/Preference;

    invoke-virtual {v2, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 392
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefPinboard:Landroid/preference/Preference;

    invoke-virtual {v2, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 393
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefBackupData:Landroid/preference/Preference;

    invoke-virtual {v2, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 394
    const-string v2, "StorageList"

    const-string v3, "QUOTA_5100 : [END]"

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    return-void

    .line 356
    :cond_5
    iget v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->isMemoOrNote:I

    if-ne v2, v7, :cond_4

    .line 357
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefSNote:Landroid/preference/Preference;

    iget v3, p0, Lcom/samsung/android/scloud/quota/StorageList;->smemoColor:I

    invoke-static {v1, v0, v3}, Lcom/samsung/android/scloud/quota/StorageList;->createRectShape(III)Landroid/graphics/drawable/ShapeDrawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0
.end method

.method private getSharedPref(Ljava/lang/String;)Z
    .locals 5
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 701
    sget-object v2, Lcom/samsung/android/scloud/quota/StorageList;->mContext:Landroid/content/Context;

    const-string v3, "QuotaWarningPreference"

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 703
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1, p1, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 705
    .local v0, "isChecked":Z
    return v0
.end method

.method private initPref()V
    .locals 8

    .prologue
    .line 851
    const-string v3, "personal_data_management_usage"

    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/quota/StorageList;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Landroid/preference/PreferenceCategory;

    .line 853
    .local v2, "screen":Landroid/preference/PreferenceCategory;
    iget-object v3, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefSNote:Landroid/preference/Preference;

    check-cast v3, Lcom/samsung/android/scloud/quota/QuotaContentPreference;

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 854
    iget-object v3, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefSNote3:Landroid/preference/Preference;

    check-cast v3, Lcom/samsung/android/scloud/quota/QuotaContentPreference;

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 855
    iget-object v3, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefSPlanner:Landroid/preference/Preference;

    check-cast v3, Lcom/samsung/android/scloud/quota/QuotaContentPreference;

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 856
    iget-object v3, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefBookmarks:Landroid/preference/Preference;

    check-cast v3, Lcom/samsung/android/scloud/quota/QuotaContentPreference;

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 857
    iget-object v3, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefContacts:Landroid/preference/Preference;

    check-cast v3, Lcom/samsung/android/scloud/quota/QuotaContentPreference;

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 858
    iget-object v3, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefPinboard:Landroid/preference/Preference;

    check-cast v3, Lcom/samsung/android/scloud/quota/QuotaContentPreference;

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 859
    iget-object v3, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefBackupData:Landroid/preference/Preference;

    check-cast v3, Lcom/samsung/android/scloud/quota/QuotaContentPreference;

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 861
    iget-object v3, p0, Lcom/samsung/android/scloud/quota/StorageList;->mValidAppList:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 862
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Boolean;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/samsung/android/scloud/quota/StorageList;->mUsageofApp:Ljava/util/HashMap;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    .line 864
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/scloud/quota/StorageList;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/scloud/quota/QuotaContentPreference;

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 865
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map$Entry;->setValue(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 870
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Boolean;>;"
    :cond_2
    return-void
.end method

.method private insertPackageInMap()V
    .locals 3

    .prologue
    .line 818
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/scloud/quota/StorageList;->mmemoNotePackage:Ljava/util/HashMap;

    .line 819
    sget-object v0, Lcom/samsung/android/scloud/quota/StorageList;->mmemoNotePackage:Ljava/util/HashMap;

    const-string v1, "com.sec.android.widgetapp.diotek.smemo"

    const-string v2, "com.sec.android.widgetapp.q1_penmemo.MemoListActivity"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 822
    sget-object v0, Lcom/samsung/android/scloud/quota/StorageList;->mmemoNotePackage:Ljava/util/HashMap;

    const-string v1, "com.samsung.android.app.memo"

    const-string v2, "com.samsung.android.app.memo.Main"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 824
    sget-object v0, Lcom/samsung/android/scloud/quota/StorageList;->mmemoNotePackage:Ljava/util/HashMap;

    const-string v1, "com.sec.android.app.snotebook"

    const-string v2, "com.infraware.filemanager.FmFileTreeListActivity"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 826
    sget-object v0, Lcom/samsung/android/scloud/quota/StorageList;->mmemoNotePackage:Ljava/util/HashMap;

    const-string v1, "com.sec.android.app.memo"

    const-string v2, "com.sec.android.app.memo.MemoRoot"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 829
    return-void
.end method

.method private isAppInstalled(Ljava/lang/String;)Z
    .locals 3
    .param p1, "packagename"    # Ljava/lang/String;

    .prologue
    .line 456
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/StorageList;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 463
    const/4 v1, 0x1

    .line 468
    :goto_0
    return v1

    .line 464
    :catch_0
    move-exception v0

    .line 465
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 468
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isConnected(Landroid/content/Context;)Z
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1133
    const-string v6, "connectivity"

    invoke-virtual {p1, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 1136
    .local v1, "connectivityManager":Landroid/net/ConnectivityManager;
    if-eqz v1, :cond_2

    .line 1137
    invoke-virtual {v1, v4}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v3

    .line 1139
    .local v3, "wifiNetwork":Landroid/net/NetworkInfo;
    invoke-virtual {v1, v5}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    .line 1141
    .local v2, "mobileNetwork":Landroid/net/NetworkInfo;
    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 1144
    .local v0, "activeNetwork":Landroid/net/NetworkInfo;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1145
    const-string v5, "StorageList"

    const-string v6, "WIFI Connected"

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1158
    .end local v0    # "activeNetwork":Landroid/net/NetworkInfo;
    .end local v2    # "mobileNetwork":Landroid/net/NetworkInfo;
    .end local v3    # "wifiNetwork":Landroid/net/NetworkInfo;
    :goto_0
    return v4

    .line 1147
    .restart local v0    # "activeNetwork":Landroid/net/NetworkInfo;
    .restart local v2    # "mobileNetwork":Landroid/net/NetworkInfo;
    .restart local v3    # "wifiNetwork":Landroid/net/NetworkInfo;
    :cond_0
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1149
    const-string v5, "StorageList"

    const-string v6, "MOBILE NETWORK Connected"

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1151
    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1153
    const-string v5, "StorageList"

    const-string v6, "ACTIVE NETWORK Connected"

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .end local v0    # "activeNetwork":Landroid/net/NetworkInfo;
    .end local v2    # "mobileNetwork":Landroid/net/NetworkInfo;
    .end local v3    # "wifiNetwork":Landroid/net/NetworkInfo;
    :cond_2
    move v4, v5

    .line 1158
    goto :goto_0
.end method

.method private setActivityTheme()V
    .locals 2

    .prologue
    const v1, 0x103012b

    .line 1263
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/StorageList;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/scloud/quota/QuotaUtils;->getIsTablet(Landroid/content/res/Configuration;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1265
    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/quota/StorageList;->setTheme(I)V

    .line 1276
    :goto_0
    return-void

    .line 1267
    :cond_0
    invoke-static {}, Lcom/samsung/android/scloud/quota/QuotaUtils;->isViewType_light()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1269
    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/quota/StorageList;->setTheme(I)V

    goto :goto_0

    .line 1274
    :cond_1
    const v0, 0x1030128

    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/quota/StorageList;->setTheme(I)V

    goto :goto_0
.end method

.method private setQuotaSummary()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 921
    const-string v3, "StorageList"

    const-string v4, "setQuotaSummary - set the sumary for all the Quota preferences "

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 924
    iget-object v3, p0, Lcom/samsung/android/scloud/quota/StorageList;->mUsageBarPreference:Lcom/samsung/android/scloud/quota/UsageBarPreference;

    invoke-virtual {v3, v8}, Lcom/samsung/android/scloud/quota/UsageBarPreference;->setEnabled(Z)V

    .line 925
    iget-object v3, p0, Lcom/samsung/android/scloud/quota/StorageList;->mValidAppList:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 927
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Boolean;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 929
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 930
    .local v2, "key":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/scloud/quota/StorageList;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v3, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/preference/Preference;

    sget-object v5, Lcom/samsung/android/scloud/quota/StorageList;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/samsung/android/scloud/quota/StorageList;->mUsageofApp:Ljava/util/HashMap;

    invoke-virtual {v4, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v5, v6, v7}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 933
    iget-object v3, p0, Lcom/samsung/android/scloud/quota/StorageList;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v3, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/preference/Preference;

    invoke-virtual {v3, v8}, Landroid/preference/Preference;->setEnabled(Z)V

    goto :goto_0

    .line 938
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Boolean;>;"
    .end local v2    # "key":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private setSharedPref(Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 5
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Boolean;

    .prologue
    .line 693
    sget-object v2, Lcom/samsung/android/scloud/quota/StorageList;->mContext:Landroid/content/Context;

    const-string v3, "QuotaWarningPreference"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 695
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 696
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-interface {v0, p1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 697
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 698
    return-void
.end method

.method private setSharedPrefAccountValidation(Ljava/lang/String;I)V
    .locals 5
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # I

    .prologue
    .line 685
    sget-object v2, Lcom/samsung/android/scloud/quota/StorageList;->mContext:Landroid/content/Context;

    const-string v3, "QuotaWarningPreference"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 687
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 688
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 690
    return-void
.end method

.method private setSummary()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 876
    const-wide/16 v0, 0x0

    .line 877
    .local v0, "maxPercent":J
    const-string v2, "StorageList"

    const-string v3, "setSummary() - set the summary of all application usage"

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 879
    const-string v2, "StorageList"

    const-string v3, "setSummary() - set the summary for current plan if the country is a non commerce target country"

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 881
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefPlan:Landroid/preference/Preference;

    invoke-virtual {v2, v6}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 882
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefPlan:Landroid/preference/Preference;

    const v3, 0x7f070079

    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/quota/StorageList;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 883
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefPlan:Landroid/preference/Preference;

    sget-object v3, Lcom/samsung/android/scloud/quota/StorageList;->mContext:Landroid/content/Context;

    iget-wide v4, p0, Lcom/samsung/android/scloud/quota/StorageList;->mTotalUsage:J

    invoke-static {v3, v4, v5}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 884
    iget-wide v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mTotalUsage:J

    .line 885
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mUsageBarPreference:Lcom/samsung/android/scloud/quota/UsageBarPreference;

    const-string v3, ""

    invoke-virtual {v2, v3}, Lcom/samsung/android/scloud/quota/UsageBarPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 886
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mUsageBarPreference:Lcom/samsung/android/scloud/quota/UsageBarPreference;

    invoke-virtual {v2}, Lcom/samsung/android/scloud/quota/UsageBarPreference;->clear()V

    .line 888
    iget v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->isMemoOrNote:I

    if-ne v2, v6, :cond_1

    .line 890
    iget-object v3, p0, Lcom/samsung/android/scloud/quota/StorageList;->mUsageBarPreference:Lcom/samsung/android/scloud/quota/UsageBarPreference;

    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mUsageofApp:Ljava/util/HashMap;

    const-string v4, "pref_snb"

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    long-to-float v2, v4

    long-to-float v4, v0

    div-float/2addr v2, v4

    iget v4, p0, Lcom/samsung/android/scloud/quota/StorageList;->smemoColor:I

    invoke-virtual {v3, v2, v4}, Lcom/samsung/android/scloud/quota/UsageBarPreference;->addEntry(FI)V

    .line 899
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/scloud/quota/StorageList;->mUsageBarPreference:Lcom/samsung/android/scloud/quota/UsageBarPreference;

    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mUsageofApp:Ljava/util/HashMap;

    const-string v4, "pref_spd"

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    long-to-float v2, v4

    long-to-float v4, v0

    div-float/2addr v2, v4

    iget v4, p0, Lcom/samsung/android/scloud/quota/StorageList;->snote3Color:I

    invoke-virtual {v3, v2, v4}, Lcom/samsung/android/scloud/quota/UsageBarPreference;->addEntry(FI)V

    .line 902
    iget-object v3, p0, Lcom/samsung/android/scloud/quota/StorageList;->mUsageBarPreference:Lcom/samsung/android/scloud/quota/UsageBarPreference;

    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mUsageofApp:Ljava/util/HashMap;

    const-string v4, "pref_splanner"

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    long-to-float v2, v4

    long-to-float v4, v0

    div-float/2addr v2, v4

    iget v4, p0, Lcom/samsung/android/scloud/quota/StorageList;->splannerColor:I

    invoke-virtual {v3, v2, v4}, Lcom/samsung/android/scloud/quota/UsageBarPreference;->addEntry(FI)V

    .line 904
    iget-object v3, p0, Lcom/samsung/android/scloud/quota/StorageList;->mUsageBarPreference:Lcom/samsung/android/scloud/quota/UsageBarPreference;

    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mUsageofApp:Ljava/util/HashMap;

    const-string v4, "pref_bookmarks"

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    long-to-float v2, v4

    long-to-float v4, v0

    div-float/2addr v2, v4

    iget v4, p0, Lcom/samsung/android/scloud/quota/StorageList;->bookmarksColor:I

    invoke-virtual {v3, v2, v4}, Lcom/samsung/android/scloud/quota/UsageBarPreference;->addEntry(FI)V

    .line 906
    iget-object v3, p0, Lcom/samsung/android/scloud/quota/StorageList;->mUsageBarPreference:Lcom/samsung/android/scloud/quota/UsageBarPreference;

    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mUsageofApp:Ljava/util/HashMap;

    const-string v4, "pref_contacts"

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    long-to-float v2, v4

    long-to-float v4, v0

    div-float/2addr v2, v4

    iget v4, p0, Lcom/samsung/android/scloud/quota/StorageList;->contactsColor:I

    invoke-virtual {v3, v2, v4}, Lcom/samsung/android/scloud/quota/UsageBarPreference;->addEntry(FI)V

    .line 908
    iget-object v3, p0, Lcom/samsung/android/scloud/quota/StorageList;->mUsageBarPreference:Lcom/samsung/android/scloud/quota/UsageBarPreference;

    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mUsageofApp:Ljava/util/HashMap;

    const-string v4, "pref_pinboard"

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    long-to-float v2, v4

    long-to-float v4, v0

    div-float/2addr v2, v4

    iget v4, p0, Lcom/samsung/android/scloud/quota/StorageList;->pinboardColor:I

    invoke-virtual {v3, v2, v4}, Lcom/samsung/android/scloud/quota/UsageBarPreference;->addEntry(FI)V

    .line 910
    iget-object v3, p0, Lcom/samsung/android/scloud/quota/StorageList;->mUsageBarPreference:Lcom/samsung/android/scloud/quota/UsageBarPreference;

    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mUsageofApp:Ljava/util/HashMap;

    const-string v4, "pref_backedupdata"

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    long-to-float v2, v4

    long-to-float v4, v0

    div-float/2addr v2, v4

    iget v4, p0, Lcom/samsung/android/scloud/quota/StorageList;->backupColor:I

    invoke-virtual {v3, v2, v4}, Lcom/samsung/android/scloud/quota/UsageBarPreference;->addEntry(FI)V

    .line 914
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mUsageBarPreference:Lcom/samsung/android/scloud/quota/UsageBarPreference;

    invoke-virtual {v2}, Lcom/samsung/android/scloud/quota/UsageBarPreference;->commit()V

    .line 916
    return-void

    .line 893
    :cond_1
    iget v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->isMemoOrNote:I

    const/4 v3, 0x2

    if-eq v2, v3, :cond_2

    iget v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->isMemoOrNote:I

    if-nez v2, :cond_0

    .line 895
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/scloud/quota/StorageList;->mUsageBarPreference:Lcom/samsung/android/scloud/quota/UsageBarPreference;

    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mUsageofApp:Ljava/util/HashMap;

    const-string v4, "pref_snb"

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    long-to-float v2, v4

    long-to-float v4, v0

    div-float/2addr v2, v4

    iget v4, p0, Lcom/samsung/android/scloud/quota/StorageList;->snoteColor:I

    invoke-virtual {v3, v2, v4}, Lcom/samsung/android/scloud/quota/UsageBarPreference;->addEntry(FI)V

    goto/16 :goto_0
.end method

.method private startApp(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "pckgName"    # Ljava/lang/String;
    .param p2, "className"    # Ljava/lang/String;

    .prologue
    .line 805
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/StorageList;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    new-instance v3, Landroid/content/ComponentName;

    invoke-direct {v3, p1, p2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v4, 0x80

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    .line 808
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 809
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {v1, p1, p2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 810
    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/quota/StorageList;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 814
    .end local v1    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 811
    :catch_0
    move-exception v0

    .line 812
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private startApplication(I)V
    .locals 2
    .param p1, "appNum"    # I

    .prologue
    .line 790
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 791
    invoke-direct {p0}, Lcom/samsung/android/scloud/quota/StorageList;->startSmemoOrSnote()V

    .line 801
    :cond_0
    :goto_0
    return-void

    .line 792
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 793
    const-string v0, "com.samsung.android.snote"

    const-string v1, "com.samsung.android.snote.control.ui.filemanager.MainHomeActivity"

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/scloud/quota/StorageList;->startApp(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 796
    :cond_2
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 797
    const-string v0, "com.samsung.android.app.pinboard"

    const-string v1, "com.samsung.android.app.pinboard.ui.PinboardActivity"

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/scloud/quota/StorageList;->startApp(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private startQuotaThread()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1118
    const-string v1, "StorageList"

    const-string v2, "QUOTA_5101 : [START]"

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1119
    new-instance v0, Lcom/samsung/android/scloud/quota/QuotaThread;

    invoke-direct {v0}, Lcom/samsung/android/scloud/quota/QuotaThread;-><init>()V

    .line 1120
    .local v0, "mQuotaThread":Lcom/samsung/android/scloud/quota/QuotaThread;
    sget-object v1, Lcom/samsung/android/scloud/quota/StorageList;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/quota/QuotaThread;->setContext(Landroid/content/Context;)V

    .line 1121
    invoke-virtual {v0, p0}, Lcom/samsung/android/scloud/quota/QuotaThread;->setListener(Lcom/samsung/android/scloud/quota/QuotaResponseThreadListener;)V

    .line 1122
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/quota/QuotaThread;->setIsBackupDetails(Ljava/lang/Boolean;)V

    .line 1123
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/quota/QuotaThread;->setIsBackedupSize(Ljava/lang/Boolean;)V

    .line 1124
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/quota/QuotaThread;->setISDeleteTrue(Ljava/lang/Boolean;)V

    .line 1125
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/StorageList;->mAuthManager:Lcom/samsung/android/scloud/quota/AuthManager;

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/quota/QuotaThread;->setAuthManager(Lcom/samsung/android/scloud/quota/AuthManager;)V

    .line 1127
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/quota/QuotaThread;->setCtid(Ljava/lang/String;)V

    .line 1128
    invoke-virtual {v0}, Lcom/samsung/android/scloud/quota/QuotaThread;->start()V

    .line 1129
    return-void
.end method

.method private startSmemoOrSnote()V
    .locals 9

    .prologue
    .line 639
    iget-object v4, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPackageName:Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 640
    iget v4, p0, Lcom/samsung/android/scloud/quota/StorageList;->isMemoOrNote:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_3

    .line 642
    const/4 v1, 0x0

    .line 646
    .local v1, "classNameofMemo":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/StorageList;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    new-instance v6, Landroid/content/ComponentName;

    iget-object v7, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPackageName:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/scloud/quota/StorageList;->mmemoNotePackage:Ljava/util/HashMap;

    iget-object v8, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPackageName:Ljava/lang/String;

    invoke-virtual {v4, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-direct {v6, v7, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v4, 0x80

    invoke-virtual {v5, v6, v4}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    .line 650
    sget-object v4, Lcom/samsung/android/scloud/quota/StorageList;->mmemoNotePackage:Ljava/util/HashMap;

    iget-object v5, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPackageName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    .line 651
    const-string v4, "StorageList"

    const-string v5, "J device"

    invoke-static {v4, v5}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 667
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 669
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 670
    .local v3, "intent":Landroid/content/Intent;
    iget-object v4, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPackageName:Ljava/lang/String;

    invoke-virtual {v3, v4, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 671
    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/quota/StorageList;->startActivity(Landroid/content/Intent;)V

    .line 682
    .end local v1    # "classNameofMemo":Ljava/lang/String;
    .end local v3    # "intent":Landroid/content/Intent;
    :cond_1
    :goto_1
    return-void

    .line 652
    .restart local v1    # "classNameofMemo":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 653
    .local v2, "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    iget-object v4, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPackageName:Ljava/lang/String;

    const-string v5, "com.sec.android.widgetapp.diotek.smemo"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 655
    const-string v4, "StorageList"

    const-string v5, "Melius device and S MEMO"

    invoke-static {v4, v5}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 656
    const-string v1, "com.sec.android.widgetapp.q1_penmemo.MemoListActivityNB"

    .line 658
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPackageName:Ljava/lang/String;

    const-string v5, "com.sec.android.app.memo"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 660
    const-string v4, "StorageList"

    const-string v5, "TextMemo_ESS application "

    invoke-static {v4, v5}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 661
    const-string v1, "com.sec.android.app.memo.Memo"

    goto :goto_0

    .line 674
    .end local v1    # "classNameofMemo":Ljava/lang/String;
    .end local v2    # "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_3
    iget v4, p0, Lcom/samsung/android/scloud/quota/StorageList;->isMemoOrNote:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    .line 675
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 676
    .restart local v3    # "intent":Landroid/content/Intent;
    iget-object v5, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPackageName:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/scloud/quota/StorageList;->mmemoNotePackage:Ljava/util/HashMap;

    iget-object v6, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPackageName:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v3, v5, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 678
    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/quota/StorageList;->startActivity(Landroid/content/Intent;)V

    goto :goto_1
.end method

.method private startWifiActivity()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1162
    const-string v1, "StorageList"

    const-string v2, "start wifi activity"

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1163
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.net.wifi.PICK_WIFI_NETWORK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1164
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "extra_prefs_show_button_bar"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1165
    const-string v1, "extra_prefs_set_back_text"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1166
    const-string v1, "wifi_enable_next_on_connect"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1168
    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/scloud/quota/StorageList;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1169
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v2, 0x1

    .line 1173
    const-string v0, "StorageList"

    const-string v1, "onActivityResult"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1174
    invoke-super {p0, p1, p2, p3}, Landroid/preference/PreferenceActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 1175
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 1176
    const-string v0, "StorageList"

    const-string v1, "onActivityResult for WIFI Setup Activity"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1177
    sget-object v0, Lcom/samsung/android/scloud/quota/StorageList;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/samsung/android/scloud/quota/StorageList;->isConnected(Landroid/content/Context;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mIsConnected:Ljava/lang/Boolean;

    .line 1178
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mIsConnected:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1179
    sget-object v0, Lcom/samsung/android/scloud/quota/StorageList;->mContext:Landroid/content/Context;

    const v1, 0x7f07005e

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/quota/StorageList;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1182
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/StorageList;->finish()V

    .line 1186
    :cond_0
    if-ne p1, v2, :cond_1

    .line 1187
    const/4 v0, -0x1

    if-ne p2, v0, :cond_2

    .line 1188
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mValidationStatus:I

    .line 1195
    :cond_1
    :goto_0
    return-void

    .line 1192
    :cond_2
    iput v2, p0, Lcom/samsung/android/scloud/quota/StorageList;->mValidationStatus:I

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 5
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const v4, 0x7f03000b

    const v3, 0x7f03000a

    const/4 v2, 0x1

    .line 229
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 230
    invoke-static {p0}, Lcom/samsung/android/scloud/quota/QuotaUtils;->hideStatusBar(Landroid/app/Activity;)V

    .line 231
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    packed-switch v0, :pswitch_data_0

    .line 254
    :cond_0
    :goto_0
    return-void

    .line 234
    :pswitch_0
    const-string v0, "StorageList"

    const-string v1, "onConfigurationChanged : ORIENTATION_PORTRAIT"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    sget-boolean v0, Lcom/samsung/android/scloud/quota/QuotaConstants$DEVICE;->IS_DEVICE_VIENNA:Z

    if-ne v0, v2, :cond_1

    .line 236
    invoke-virtual {p0, v4}, Lcom/samsung/android/scloud/quota/StorageList;->setContentView(I)V

    goto :goto_0

    .line 238
    :cond_1
    sget-boolean v0, Lcom/samsung/android/scloud/quota/QuotaConstants$DEVICE;->IS_DEVICE_CHAGALL:Z

    if-ne v0, v2, :cond_0

    .line 239
    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/quota/StorageList;->setContentView(I)V

    goto :goto_0

    .line 243
    :pswitch_1
    const-string v0, "StorageList"

    const-string v1, "onConfigurationChanged : ORIENTATION_LANDSCAPE"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    sget-boolean v0, Lcom/samsung/android/scloud/quota/QuotaConstants$DEVICE;->IS_DEVICE_VIENNA:Z

    if-ne v0, v2, :cond_2

    .line 245
    invoke-virtual {p0, v4}, Lcom/samsung/android/scloud/quota/StorageList;->setContentView(I)V

    goto :goto_0

    .line 247
    :cond_2
    sget-boolean v0, Lcom/samsung/android/scloud/quota/QuotaConstants$DEVICE;->IS_DEVICE_CHAGALL:Z

    if-ne v0, v2, :cond_0

    .line 248
    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/quota/StorageList;->setContentView(I)V

    goto :goto_0

    .line 231
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 142
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/StorageList;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    sput-object v3, Lcom/samsung/android/scloud/quota/StorageList;->mContext:Landroid/content/Context;

    .line 143
    invoke-direct {p0}, Lcom/samsung/android/scloud/quota/StorageList;->setActivityTheme()V

    .line 146
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 147
    const-string v3, "StorageList"

    const-string v4, "onCreate"

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    const-string v3, "StorageList"

    const-string v4, "Quota App Version 1.7.01"

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/StorageList;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    const/16 v4, 0xc

    invoke-virtual {v3, v4}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 152
    invoke-static {p0}, Lcom/samsung/android/scloud/quota/QuotaUtils;->hideStatusBar(Landroid/app/Activity;)V

    .line 153
    const v3, 0x7f040001

    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/quota/StorageList;->addPreferencesFromResource(I)V

    .line 154
    sget-boolean v3, Lcom/samsung/android/scloud/quota/QuotaConstants$DEVICE;->IS_DEVICE_VIENNA:Z

    if-ne v3, v7, :cond_0

    .line 155
    const v3, 0x7f03000b

    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/quota/StorageList;->setContentView(I)V

    .line 157
    :cond_0
    sget-boolean v3, Lcom/samsung/android/scloud/quota/QuotaConstants$DEVICE;->IS_DEVICE_CHAGALL:Z

    if-ne v3, v7, :cond_1

    .line 158
    const v3, 0x7f03000a

    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/quota/StorageList;->setContentView(I)V

    .line 161
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/StorageList;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/scloud/quota/StorageList;->mResources:Landroid/content/res/Resources;

    .line 162
    invoke-direct {p0}, Lcom/samsung/android/scloud/quota/StorageList;->insertPackageInMap()V

    .line 163
    invoke-direct {p0}, Lcom/samsung/android/scloud/quota/StorageList;->getPreferencesReference()V

    .line 164
    invoke-static {}, Lcom/samsung/android/scloud/quota/QuotaUtils;->generateCTID()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/scloud/quota/StorageList;->mCtid:Ljava/lang/String;

    .line 165
    sget-object v3, Lcom/samsung/android/scloud/quota/StorageList;->mContext:Landroid/content/Context;

    const-string v4, "QuotaWarningPreference"

    invoke-virtual {v3, v4, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 167
    .local v2, "quotaMeta":Landroid/content/SharedPreferences;
    const-string v3, "verificationOk"

    const/4 v4, -0x1

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 168
    .local v1, "isValid":I
    const-string v3, "StorageList"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isVerificationDone = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/samsung/android/scloud/quota/StorageList;->mValidationStatus:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    if-eqz v1, :cond_3

    .line 172
    const-string v3, "StorageList"

    const-string v4, "Samsung Account : Not valid"

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    const-string v3, "StorageList"

    const-string v4, "Not Valid Account : Check through Provider call"

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    sget-object v3, Lcom/samsung/android/scloud/quota/StorageList;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/samsung/android/scloud/quota/QuotaUtils;->checkAccountValidation(Landroid/content/Context;)Z

    move-result v0

    .line 175
    .local v0, "accountValid":Z
    if-ne v0, v7, :cond_2

    .line 177
    const-string v3, "StorageList"

    const-string v4, "Account validated : Using Account validation call"

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    const-string v3, "verificationOk"

    invoke-direct {p0, v3, v6}, Lcom/samsung/android/scloud/quota/StorageList;->setSharedPrefAccountValidation(Ljava/lang/String;I)V

    .line 180
    new-instance v3, Lcom/samsung/android/scloud/quota/StorageList$AccessTokenclass;

    invoke-direct {v3, p0, v8}, Lcom/samsung/android/scloud/quota/StorageList$AccessTokenclass;-><init>(Lcom/samsung/android/scloud/quota/StorageList;Lcom/samsung/android/scloud/quota/StorageList$1;)V

    new-array v4, v7, [Ljava/lang/String;

    const-string v5, ""

    aput-object v5, v4, v6

    invoke-virtual {v3, v4}, Lcom/samsung/android/scloud/quota/StorageList$AccessTokenclass;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 194
    .end local v0    # "accountValid":Z
    :goto_0
    return-void

    .line 184
    .restart local v0    # "accountValid":Z
    :cond_2
    const-string v3, "StorageList"

    const-string v4, "Account not validated : Using Account validation call"

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    iput-object p0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mActivity:Landroid/app/Activity;

    .line 186
    new-instance v3, Lcom/samsung/android/scloud/quota/StorageList$CheckSamsungAccountValidation;

    invoke-direct {v3, p0, v8}, Lcom/samsung/android/scloud/quota/StorageList$CheckSamsungAccountValidation;-><init>(Lcom/samsung/android/scloud/quota/StorageList;Lcom/samsung/android/scloud/quota/StorageList$1;)V

    new-array v4, v6, [Ljava/lang/Void;

    invoke-virtual {v3, v4}, Lcom/samsung/android/scloud/quota/StorageList$CheckSamsungAccountValidation;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 190
    .end local v0    # "accountValid":Z
    :cond_3
    const-string v3, "StorageList"

    const-string v4, "Samsung Account :Valid"

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    new-instance v3, Lcom/samsung/android/scloud/quota/StorageList$AccessTokenclass;

    invoke-direct {v3, p0, v8}, Lcom/samsung/android/scloud/quota/StorageList$AccessTokenclass;-><init>(Lcom/samsung/android/scloud/quota/StorageList;Lcom/samsung/android/scloud/quota/StorageList$1;)V

    new-array v4, v7, [Ljava/lang/String;

    const-string v5, ""

    aput-object v5, v4, v6

    invoke-virtual {v3, v4}, Lcom/samsung/android/scloud/quota/StorageList$AccessTokenclass;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 207
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 208
    const-string v1, "StorageList"

    const-string v2, "onNewIntentCalled"

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 210
    .local v0, "action":Ljava/lang/String;
    const-string v1, "StorageList"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onNewIntentCalled "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    const-string v1, "StorageList"

    invoke-static {v1, v0}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 216
    const-string v0, "StorageList"

    const-string v1, "onOptionsItemSelected(MenuItem)"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 219
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/StorageList;->finish()V

    .line 220
    const/4 v0, 0x1

    .line 222
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 291
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onPause()V

    .line 292
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mIsConnected:Ljava/lang/Boolean;

    .line 293
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mIsBackedupData:Ljava/lang/Boolean;

    .line 294
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->isActivityRunning:Ljava/lang/Boolean;

    .line 295
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mUsageBarPreference:Lcom/samsung/android/scloud/quota/UsageBarPreference;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/quota/UsageBarPreference;->clear()V

    .line 297
    return-void
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 10
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    const/4 v9, 0x1

    .line 473
    const-string v5, "StorageList"

    const-string v6, "onPreferenceClick"

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    iget-object v5, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefContacts:Landroid/preference/Preference;

    invoke-virtual {p1, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 475
    const-string v5, "StorageList"

    const-string v6, "onPreferenceClick - Pref Contacts"

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 479
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 480
    .local v3, "intent":Landroid/content/Intent;
    sget-object v5, Lcom/samsung/android/scloud/quota/StorageList;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/samsung/android/scloud/quota/QuotaUtils;->isJapanDevice(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 482
    invoke-static {}, Lcom/samsung/android/scloud/quota/QuotaUtils;->isJapanPreviousModel()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 483
    new-instance v5, Landroid/content/ComponentName;

    const-string v6, "com.android.jcontacts"

    const-string v7, "com.android.contacts.activities.PeopleActivity"

    invoke-direct {v5, v6, v7}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 491
    :goto_0
    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/quota/StorageList;->startActivity(Landroid/content/Intent;)V

    .line 494
    .end local v3    # "intent":Landroid/content/Intent;
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefBackupData:Landroid/preference/Preference;

    invoke-virtual {p1, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 495
    const-string v5, "StorageList"

    const-string v6, "onPreferenceClick - Pref Backedup Data"

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 497
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 498
    .restart local v3    # "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/StorageList;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const-class v6, Lcom/samsung/android/scloud/quota/BackupList;

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 499
    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/quota/StorageList;->startActivity(Landroid/content/Intent;)V

    .line 502
    .end local v3    # "intent":Landroid/content/Intent;
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefSNote:Landroid/preference/Preference;

    invoke-virtual {p1, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 504
    const-string v5, "StorageList"

    const-string v6, "onPreferenceClick - Pref Snote"

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 505
    iget v5, p0, Lcom/samsung/android/scloud/quota/StorageList;->isMemoOrNote:I

    if-nez v5, :cond_9

    .line 507
    invoke-direct {p0}, Lcom/samsung/android/scloud/quota/StorageList;->displayAlert()V

    .line 513
    :cond_2
    :goto_1
    iget-object v5, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefSNote3:Landroid/preference/Preference;

    invoke-virtual {p1, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 515
    const-string v5, "StorageList"

    const-string v6, "onPreferenceClick - Pref Snote3"

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 517
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/StorageList;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    new-instance v6, Landroid/content/ComponentName;

    const-string v7, "com.samsung.android.snote"

    const-string v8, "com.samsung.android.snote.control.ui.filemanager.MainHomeActivity"

    invoke-direct {v6, v7, v8}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v7, 0x80

    invoke-virtual {v5, v6, v7}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    .line 523
    const-string v5, "ISSNOTECHECKBOXCHECKED"

    const/4 v6, 0x2

    invoke-direct {p0, v5, v6}, Lcom/samsung/android/scloud/quota/StorageList;->displayDialougBox(Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 534
    :cond_3
    :goto_2
    iget-object v5, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefBookmarks:Landroid/preference/Preference;

    invoke-virtual {p1, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 535
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 536
    .local v4, "isSBrowser":Ljava/lang/Boolean;
    const-string v5, "StorageList"

    const-string v6, "onPreferenceClick - Pref bookmarks"

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 539
    :try_start_1
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/StorageList;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const-string v6, "com.sec.android.app.sbrowser"

    const/4 v7, 0x5

    invoke-virtual {v5, v6, v7}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 546
    :goto_3
    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_a

    .line 548
    const-string v5, "StorageList"

    const-string v6, "Device contain sBrowser"

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 550
    :try_start_2
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/StorageList;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    new-instance v6, Landroid/content/ComponentName;

    const-string v7, "com.sec.android.app.sbrowser"

    const-string v8, "com.sec.android.app.sbrowser.SBrowserMainActivity"

    invoke-direct {v6, v7, v8}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v7, 0x80

    invoke-virtual {v5, v6, v7}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    .line 556
    const-string v0, "com.sec.android.app.sbrowser.SBrowserMainActivity"

    .line 557
    .local v0, "classNameofSBrowser":Ljava/lang/String;
    const-string v5, "StorageList"

    const-string v6, "J device"

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    .line 564
    :goto_4
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 565
    .restart local v3    # "intent":Landroid/content/Intent;
    const-string v5, "com.sec.android.app.sbrowser"

    invoke-virtual {v3, v5, v0}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 567
    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/quota/StorageList;->startActivity(Landroid/content/Intent;)V

    .line 590
    .end local v0    # "classNameofSBrowser":Ljava/lang/String;
    .end local v3    # "intent":Landroid/content/Intent;
    .end local v4    # "isSBrowser":Ljava/lang/Boolean;
    :cond_4
    :goto_5
    iget-object v5, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefSPlanner:Landroid/preference/Preference;

    invoke-virtual {p1, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 591
    const-string v5, "StorageList"

    const-string v6, "onPreferenceClick - Pref planner"

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 592
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 593
    .restart local v3    # "intent":Landroid/content/Intent;
    const-string v5, "com.android.calendar"

    const-string v6, "com.android.calendar.AllInOneActivity"

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 595
    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/quota/StorageList;->startActivity(Landroid/content/Intent;)V

    .line 600
    .end local v3    # "intent":Landroid/content/Intent;
    :cond_5
    iget-object v5, p0, Lcom/samsung/android/scloud/quota/StorageList;->mPrefPinboard:Landroid/preference/Preference;

    invoke-virtual {p1, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 601
    const-string v5, "StorageList"

    const-string v6, "onPreferenceClick - Pref pinBoard"

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 603
    :try_start_3
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/StorageList;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    new-instance v6, Landroid/content/ComponentName;

    const-string v7, "com.samsung.android.app.pinboard"

    const-string v8, "com.samsung.android.app.pinboard.ui.PinboardActivity"

    invoke-direct {v6, v7, v8}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v7, 0x80

    invoke-virtual {v5, v6, v7}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    .line 609
    const-string v5, "ISSCRAPCHECKBOXCHECKED"

    const/4 v6, 0x3

    invoke-direct {p0, v5, v6}, Lcom/samsung/android/scloud/quota/StorageList;->displayDialougBox(Ljava/lang/String;I)V
    :try_end_3
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_3 .. :try_end_3} :catch_4

    .line 634
    :cond_6
    :goto_6
    return v9

    .line 485
    .restart local v3    # "intent":Landroid/content/Intent;
    :cond_7
    new-instance v5, Landroid/content/ComponentName;

    const-string v6, "com.samsung.contacts"

    const-string v7, "com.android.contacts.activities.PeopleActivity"

    invoke-direct {v5, v6, v7}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    goto/16 :goto_0

    .line 489
    :cond_8
    new-instance v5, Landroid/content/ComponentName;

    const-string v6, "com.android.contacts"

    const-string v7, "com.android.contacts.activities.PeopleActivity"

    invoke-direct {v5, v6, v7}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    goto/16 :goto_0

    .line 509
    .end local v3    # "intent":Landroid/content/Intent;
    :cond_9
    const-string v5, "ISCHECKBOXCHECKED"

    invoke-direct {p0, v5, v9}, Lcom/samsung/android/scloud/quota/StorageList;->displayDialougBox(Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 524
    :catch_0
    move-exception v1

    .line 525
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 526
    invoke-direct {p0}, Lcom/samsung/android/scloud/quota/StorageList;->displayAlert()V

    goto/16 :goto_2

    .line 543
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v4    # "isSBrowser":Ljava/lang/Boolean;
    :catch_1
    move-exception v2

    .line 544
    .local v2, "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    goto/16 :goto_3

    .line 558
    .end local v2    # "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_2
    move-exception v2

    .line 560
    .restart local v2    # "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v5, "StorageList"

    const-string v6, "H device"

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 561
    const-string v0, "com.sec.android.app.sbrowser.mainactivity.ui.SBrowserMainActivity"

    .restart local v0    # "classNameofSBrowser":Ljava/lang/String;
    goto/16 :goto_4

    .line 570
    .end local v0    # "classNameofSBrowser":Ljava/lang/String;
    .end local v2    # "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_a
    :try_start_4
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/StorageList;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const-string v6, "com.android.browser"

    const/4 v7, 0x5

    invoke-virtual {v5, v6, v7}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    .line 574
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 575
    .restart local v3    # "intent":Landroid/content/Intent;
    const-string v5, "com.android.browser"

    const-string v6, "com.android.browser.BrowserActivity"

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 577
    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/quota/StorageList;->startActivity(Landroid/content/Intent;)V

    .line 578
    const-string v5, "StorageList"

    const-string v6, "Device contain secBrowser"

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4 .. :try_end_4} :catch_3

    goto/16 :goto_5

    .line 579
    .end local v3    # "intent":Landroid/content/Intent;
    :catch_3
    move-exception v2

    .line 581
    .restart local v2    # "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v5, "StorageList"

    const-string v6, "Device don\'t contian sBrowser or secBrowser"

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 582
    invoke-direct {p0}, Lcom/samsung/android/scloud/quota/StorageList;->displayAlert()V

    goto/16 :goto_5

    .line 610
    .end local v2    # "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v4    # "isSBrowser":Ljava/lang/Boolean;
    :catch_4
    move-exception v1

    .line 612
    .restart local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 613
    invoke-direct {p0}, Lcom/samsung/android/scloud/quota/StorageList;->displayAlert()V

    goto :goto_6
.end method

.method public onResponse(Landroid/os/Message;)V
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 943
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 945
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 264
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    .line 265
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->isActivityRunning:Ljava/lang/Boolean;

    .line 267
    const-string v0, "StorageList"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    sget-object v0, Lcom/samsung/android/scloud/quota/StorageList;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/samsung/android/scloud/quota/StorageList;->isConnected(Landroid/content/Context;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mIsConnected:Ljava/lang/Boolean;

    .line 269
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->mIsConnected:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 270
    const-string v0, "StorageList"

    const-string v1, "Not connected to internet"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    sget-object v0, Lcom/samsung/android/scloud/quota/StorageList;->mContext:Landroid/content/Context;

    const v1, 0x7f07005d

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/quota/StorageList;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 273
    invoke-direct {p0}, Lcom/samsung/android/scloud/quota/StorageList;->startWifiActivity()V

    .line 275
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList;->isAccessToken:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 276
    const-string v0, "StorageList"

    const-string v1, "onResume : Call for Quota thread "

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    invoke-direct {p0}, Lcom/samsung/android/scloud/quota/StorageList;->startQuotaThread()V

    .line 280
    :cond_1
    const-string v0, "StorageList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onResume() "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/StorageList;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    return-void
.end method

.method public unicodeWrap(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 1279
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1281
    .local v1, "result":Ljava/lang/StringBuilder;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 1282
    .local v0, "name":Ljava/lang/String;
    const-string v2, "ar"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1283
    sget-object v2, Lcom/samsung/android/scloud/quota/StorageList;->RLM_STRING:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1284
    :cond_0
    const/16 v2, 0x202a

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1285
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1286
    const/16 v2, 0x202c

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1287
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
