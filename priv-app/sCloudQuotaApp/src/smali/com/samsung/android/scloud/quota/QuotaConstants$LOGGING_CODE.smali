.class public interface abstract Lcom/samsung/android/scloud/quota/QuotaConstants$LOGGING_CODE;
.super Ljava/lang/Object;
.source "QuotaConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/quota/QuotaConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "LOGGING_CODE"
.end annotation


# static fields
.field public static final DELETE_BACKUP_FROM_SERVER:Ljava/lang/String; = "QUOTA_5115"

.field public static final DELETE_BACKUP_FROM_SERVER_API:Ljava/lang/String; = "QUOTA_5104"

.field public static final DO_IN_BACKGROUND_ACCESTOKEN:Ljava/lang/String; = "QUOTA_5102"

.field public static final GET_ALL_DETAIL_USAGE_API:Ljava/lang/String; = "QUOTA_5110"

.field public static final GET_BACKUP_DETAIL_API:Ljava/lang/String; = "QUOTA_5111"

.field public static final GET_PREFRENCE_REFRENCE:Ljava/lang/String; = "QUOTA_5100"

.field public static final GET_QUOTA_DETAILS:Ljava/lang/String; = "QUOTA_5103"

.field public static final GET_TOTAL_USAGE_API:Ljava/lang/String; = "QUOTA_5105"

.field public static final GET_TOTAL_USAGE_BY_APP_API:Ljava/lang/String; = "QUOTA_5106"

.field public static final GET_TOTAL_USAGE_BY_CID_API:Ljava/lang/String; = "QUOTA_5108"

.field public static final GET_TOTAL_USAGE_BY_DID_API:Ljava/lang/String; = "QUOTA_5109"

.field public static final GET_TOTAL_USAGE_BY_USER_API:Ljava/lang/String; = "QUOTA_5107"

.field public static final INTENT_COMMERCE:Ljava/lang/String; = "QUOTA_5112"

.field public static final INTENT_QUOTA_FULL:Ljava/lang/String; = "QUOTA_5113"

.field public static final INTENT_QUOTA_WARNING:Ljava/lang/String; = "QUOTA_5114"

.field public static final START_QUOTA_THREAD:Ljava/lang/String; = "QUOTA_5101"
