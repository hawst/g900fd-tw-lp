.class Lcom/samsung/android/scloud/quota/StorageList$CheckSamsungAccountValidation;
.super Landroid/os/AsyncTask;
.source "StorageList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/quota/StorageList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CheckSamsungAccountValidation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/quota/StorageList;


# direct methods
.method private constructor <init>(Lcom/samsung/android/scloud/quota/StorageList;)V
    .locals 0

    .prologue
    .line 1291
    iput-object p1, p0, Lcom/samsung/android/scloud/quota/StorageList$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/scloud/quota/StorageList;Lcom/samsung/android/scloud/quota/StorageList$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/scloud/quota/StorageList;
    .param p2, "x1"    # Lcom/samsung/android/scloud/quota/StorageList$1;

    .prologue
    .line 1291
    invoke-direct {p0, p1}, Lcom/samsung/android/scloud/quota/StorageList$CheckSamsungAccountValidation;-><init>(Lcom/samsung/android/scloud/quota/StorageList;)V

    return-void
.end method

.method private registerSamsungAccountReceiver()V
    .locals 3

    .prologue
    .line 1402
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/StorageList$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mSamsungAccountReceiver:Landroid/content/BroadcastReceiver;
    invoke-static {v1}, Lcom/samsung/android/scloud/quota/StorageList;->access$4000(Lcom/samsung/android/scloud/quota/StorageList;)Landroid/content/BroadcastReceiver;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1403
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/StorageList$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    new-instance v2, Lcom/samsung/android/scloud/quota/StorageList$CheckSamsungAccountValidation$1;

    invoke-direct {v2, p0}, Lcom/samsung/android/scloud/quota/StorageList$CheckSamsungAccountValidation$1;-><init>(Lcom/samsung/android/scloud/quota/StorageList$CheckSamsungAccountValidation;)V

    # setter for: Lcom/samsung/android/scloud/quota/StorageList;->mSamsungAccountReceiver:Landroid/content/BroadcastReceiver;
    invoke-static {v1, v2}, Lcom/samsung/android/scloud/quota/StorageList;->access$4002(Lcom/samsung/android/scloud/quota/StorageList;Landroid/content/BroadcastReceiver;)Landroid/content/BroadcastReceiver;

    .line 1470
    :cond_0
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1471
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.msc.action.VALIDATION_CHECK_RESPONSE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1472
    sget-object v1, Lcom/samsung/android/scloud/quota/StorageList;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_1

    .line 1473
    sget-object v1, Lcom/samsung/android/scloud/quota/StorageList;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mSamsungAccountReceiver:Landroid/content/BroadcastReceiver;
    invoke-static {v2}, Lcom/samsung/android/scloud/quota/StorageList;->access$4000(Lcom/samsung/android/scloud/quota/StorageList;)Landroid/content/BroadcastReceiver;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1474
    :cond_1
    return-void
.end method

.method private unregisterSamsungAccountReceiver()V
    .locals 2

    .prologue
    .line 1477
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mSamsungAccountReceiver:Landroid/content/BroadcastReceiver;
    invoke-static {v0}, Lcom/samsung/android/scloud/quota/StorageList;->access$4000(Lcom/samsung/android/scloud/quota/StorageList;)Landroid/content/BroadcastReceiver;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1478
    sget-object v0, Lcom/samsung/android/scloud/quota/StorageList;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 1479
    sget-object v0, Lcom/samsung/android/scloud/quota/StorageList;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/StorageList$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mSamsungAccountReceiver:Landroid/content/BroadcastReceiver;
    invoke-static {v1}, Lcom/samsung/android/scloud/quota/StorageList;->access$4000(Lcom/samsung/android/scloud/quota/StorageList;)Landroid/content/BroadcastReceiver;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1480
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/scloud/quota/StorageList;->mSamsungAccountReceiver:Landroid/content/BroadcastReceiver;
    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/StorageList;->access$4002(Lcom/samsung/android/scloud/quota/StorageList;Landroid/content/BroadcastReceiver;)Landroid/content/BroadcastReceiver;

    .line 1482
    :cond_1
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 11
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 1306
    const-string v6, "StorageList"

    const-string v7, "doInBackground...SamsungAccount Validation"

    invoke-static {v6, v7}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1307
    const-string v0, "tj9u972o46"

    .line 1308
    .local v0, "clientId":Ljava/lang/String;
    const-string v1, ""

    .line 1309
    .local v1, "clientSecret":Ljava/lang/String;
    const/4 v4, 0x0

    .line 1310
    .local v4, "intent":Landroid/content/Intent;
    sget-object v6, Lcom/samsung/android/scloud/quota/StorageList;->mContext:Landroid/content/Context;

    if-eqz v6, :cond_0

    .line 1312
    iget-object v6, p0, Lcom/samsung/android/scloud/quota/StorageList$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    sget-object v7, Lcom/samsung/android/scloud/quota/StorageList;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/samsung/android/scloud/quota/QuotaUtils;->isSupportAidlOnly(Landroid/content/Context;)Z

    move-result v7

    # setter for: Lcom/samsung/android/scloud/quota/StorageList;->isSupportAidlOnly:Z
    invoke-static {v6, v7}, Lcom/samsung/android/scloud/quota/StorageList;->access$3702(Lcom/samsung/android/scloud/quota/StorageList;Z)Z

    .line 1313
    iget-object v6, p0, Lcom/samsung/android/scloud/quota/StorageList$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->isSupportAidlOnly:Z
    invoke-static {v6}, Lcom/samsung/android/scloud/quota/StorageList;->access$3700(Lcom/samsung/android/scloud/quota/StorageList;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1315
    new-instance v4, Landroid/content/Intent;

    .end local v4    # "intent":Landroid/content/Intent;
    const-string v6, "com.msc.action.samsungaccount.REQUEST_CHECKLIST_VALIDATION"

    invoke-direct {v4, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1316
    .restart local v4    # "intent":Landroid/content/Intent;
    const-string v6, "client_id"

    const-string v7, "tj9u972o46"

    invoke-virtual {v4, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1317
    const-string v6, "client_secret"

    const-string v7, ""

    invoke-virtual {v4, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1318
    const-string v6, "progress_theme"

    const-string v7, "invisible"

    invoke-virtual {v4, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1319
    iget-object v6, p0, Lcom/samsung/android/scloud/quota/StorageList$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    invoke-virtual {v6, v4, v10}, Lcom/samsung/android/scloud/quota/StorageList;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1337
    :cond_0
    :goto_0
    const/4 v2, 0x0

    .line 1338
    .local v2, "count":I
    :goto_1
    iget-object v6, p0, Lcom/samsung/android/scloud/quota/StorageList$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mValidationStatus:I
    invoke-static {v6}, Lcom/samsung/android/scloud/quota/StorageList;->access$3800(Lcom/samsung/android/scloud/quota/StorageList;)I

    move-result v6

    const/4 v7, -0x1

    if-ne v6, v7, :cond_1

    .line 1340
    const/16 v6, 0x3c

    if-le v2, v6, :cond_5

    .line 1341
    :try_start_0
    const-string v6, "StorageList"

    const-string v7, "no response from Samsung account"

    invoke-static {v6, v7}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1350
    :cond_1
    iget-object v6, p0, Lcom/samsung/android/scloud/quota/StorageList$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->isSupportAidlOnly:Z
    invoke-static {v6}, Lcom/samsung/android/scloud/quota/StorageList;->access$3700(Lcom/samsung/android/scloud/quota/StorageList;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 1351
    invoke-direct {p0}, Lcom/samsung/android/scloud/quota/StorageList$CheckSamsungAccountValidation;->unregisterSamsungAccountReceiver()V

    .line 1353
    :cond_2
    sget-object v6, Lcom/samsung/android/scloud/quota/StorageList;->mContext:Landroid/content/Context;

    if-eqz v6, :cond_3

    .line 1354
    sget-object v6, Lcom/samsung/android/scloud/quota/StorageList;->mContext:Landroid/content/Context;

    const-string v7, "QuotaWarningPreference"

    invoke-virtual {v6, v7, v9}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    .line 1356
    .local v5, "quotaMeta":Landroid/content/SharedPreferences;
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "verificationOk"

    iget-object v8, p0, Lcom/samsung/android/scloud/quota/StorageList$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mValidationStatus:I
    invoke-static {v8}, Lcom/samsung/android/scloud/quota/StorageList;->access$3800(Lcom/samsung/android/scloud/quota/StorageList;)I

    move-result v8

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1359
    .end local v5    # "quotaMeta":Landroid/content/SharedPreferences;
    :cond_3
    iget-object v6, p0, Lcom/samsung/android/scloud/quota/StorageList$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mValidationStatus:I
    invoke-static {v6}, Lcom/samsung/android/scloud/quota/StorageList;->access$3800(Lcom/samsung/android/scloud/quota/StorageList;)I

    move-result v6

    if-nez v6, :cond_6

    .line 1360
    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    .line 1362
    :goto_2
    return-object v6

    .line 1323
    .end local v2    # "count":I
    :cond_4
    new-instance v4, Landroid/content/Intent;

    .end local v4    # "intent":Landroid/content/Intent;
    const-string v6, "com.msc.action.VALIDATION_CHECK_REQUEST"

    invoke-direct {v4, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1324
    .restart local v4    # "intent":Landroid/content/Intent;
    const-string v6, "client_id"

    const-string v7, "tj9u972o46"

    invoke-virtual {v4, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1325
    const-string v6, "client_secret"

    const-string v7, ""

    invoke-virtual {v4, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1326
    const-string v6, "mypackage"

    const-string v7, "com.samsung.android.scloud.quota"

    invoke-virtual {v4, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1327
    const-string v6, "OSP_VER"

    const-string v7, "OSP_02"

    invoke-virtual {v4, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1328
    const-string v6, "MODE"

    const-string v7, "VALIDATION_CHECK"

    invoke-virtual {v4, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1329
    invoke-direct {p0}, Lcom/samsung/android/scloud/quota/StorageList$CheckSamsungAccountValidation;->registerSamsungAccountReceiver()V

    .line 1330
    sget-object v6, Lcom/samsung/android/scloud/quota/StorageList;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 1344
    .restart local v2    # "count":I
    :cond_5
    const-wide/16 v6, 0x3e8

    :try_start_1
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1345
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    .line 1346
    :catch_0
    move-exception v3

    .line 1347
    .local v3, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v3}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto/16 :goto_1

    .line 1362
    .end local v3    # "e":Ljava/lang/InterruptedException;
    :cond_6
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    goto :goto_2
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 1291
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/android/scloud/quota/StorageList$CheckSamsungAccountValidation;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelled()V
    .locals 1

    .prologue
    .line 1395
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->isSupportAidlOnly:Z
    invoke-static {v0}, Lcom/samsung/android/scloud/quota/StorageList;->access$3700(Lcom/samsung/android/scloud/quota/StorageList;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1396
    invoke-direct {p0}, Lcom/samsung/android/scloud/quota/StorageList$CheckSamsungAccountValidation;->unregisterSamsungAccountReceiver()V

    .line 1397
    :cond_0
    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    .line 1398
    return-void
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 5
    .param p1, "validated"    # Ljava/lang/Boolean;

    .prologue
    .line 1367
    const-string v1, "StorageList"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Samsung account Validated :? "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1369
    :try_start_0
    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {}, Lcom/samsung/android/scloud/quota/StorageList;->access$3600()Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1370
    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {}, Lcom/samsung/android/scloud/quota/StorageList;->access$3600()Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1371
    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/scloud/quota/StorageList;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/samsung/android/scloud/quota/StorageList;->access$3602(Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 1373
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1374
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/StorageList$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mIntentToStartActivity:Landroid/content/Intent;
    invoke-static {v1}, Lcom/samsung/android/scloud/quota/StorageList;->access$3900(Lcom/samsung/android/scloud/quota/StorageList;)Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1375
    sget-object v1, Lcom/samsung/android/scloud/quota/StorageList;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mIntentToStartActivity:Landroid/content/Intent;
    invoke-static {v2}, Lcom/samsung/android/scloud/quota/StorageList;->access$3900(Lcom/samsung/android/scloud/quota/StorageList;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1379
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/StorageList$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    invoke-virtual {v1}, Lcom/samsung/android/scloud/quota/StorageList;->finish()V

    .line 1392
    :goto_1
    return-void

    .line 1377
    :cond_1
    sget-object v1, Lcom/samsung/android/scloud/quota/StorageList;->mContext:Landroid/content/Context;

    sget-object v2, Lcom/samsung/android/scloud/quota/StorageList;->mContext:Landroid/content/Context;

    const v3, 0x7f070065

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 1385
    :catch_0
    move-exception v0

    .line 1386
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 1387
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/StorageList$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    invoke-virtual {v1}, Lcom/samsung/android/scloud/quota/StorageList;->finish()V

    goto :goto_1

    .line 1382
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :cond_2
    :try_start_1
    const-string v1, "StorageList"

    const-string v2, "Verified!!!Get AccessToken"

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1383
    new-instance v1, Lcom/samsung/android/scloud/quota/StorageList$AccessTokenclass;

    iget-object v2, p0, Lcom/samsung/android/scloud/quota/StorageList$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/samsung/android/scloud/quota/StorageList$AccessTokenclass;-><init>(Lcom/samsung/android/scloud/quota/StorageList;Lcom/samsung/android/scloud/quota/StorageList$1;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, ""

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/samsung/android/scloud/quota/StorageList$AccessTokenclass;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 1388
    :catch_1
    move-exception v0

    .line 1389
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    .line 1390
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/StorageList$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    invoke-virtual {v1}, Lcom/samsung/android/scloud/quota/StorageList;->finish()V

    goto :goto_1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 1291
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/android/scloud/quota/StorageList$CheckSamsungAccountValidation;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    .line 1295
    const-string v0, "StorageList"

    const-string v1, "onPreExecute...SamsungAccount Validation"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1296
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/StorageList$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/samsung/android/scloud/quota/StorageList;->access$3500(Lcom/samsung/android/scloud/quota/StorageList;)Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1297
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/StorageList$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mActivity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/samsung/android/scloud/quota/StorageList;->access$3500(Lcom/samsung/android/scloud/quota/StorageList;)Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/samsung/android/scloud/quota/StorageList;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/samsung/android/scloud/quota/StorageList;->access$3602(Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 1298
    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {}, Lcom/samsung/android/scloud/quota/StorageList;->access$3600()Landroid/app/ProgressDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/StorageList$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/quota/StorageList;

    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mActivity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/samsung/android/scloud/quota/StorageList;->access$3500(Lcom/samsung/android/scloud/quota/StorageList;)Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f07000f

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 1299
    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {}, Lcom/samsung/android/scloud/quota/StorageList;->access$3600()Landroid/app/ProgressDialog;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 1300
    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {}, Lcom/samsung/android/scloud/quota/StorageList;->access$3600()Landroid/app/ProgressDialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 1301
    # getter for: Lcom/samsung/android/scloud/quota/StorageList;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {}, Lcom/samsung/android/scloud/quota/StorageList;->access$3600()Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 1303
    :cond_0
    return-void
.end method
