.class public Lcom/samsung/android/scloud/quota/PercentageBarChart$Entry;
.super Ljava/lang/Object;
.source "PercentageBarChart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/quota/PercentageBarChart;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Entry"
.end annotation


# instance fields
.field public final paint:Landroid/graphics/Paint;

.field public final percentage:F


# direct methods
.method protected constructor <init>(FLandroid/graphics/Paint;)V
    .locals 0
    .param p1, "percentage"    # F
    .param p2, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput p1, p0, Lcom/samsung/android/scloud/quota/PercentageBarChart$Entry;->percentage:F

    .line 46
    iput-object p2, p0, Lcom/samsung/android/scloud/quota/PercentageBarChart$Entry;->paint:Landroid/graphics/Paint;

    .line 47
    return-void
.end method
