.class public Lcom/samsung/android/scloud/quota/AccountRemovalReceiver;
.super Landroid/content/BroadcastReceiver;
.source "AccountRemovalReceiver.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "AccountRemovalReceiver"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v8, 0x0

    .line 18
    if-eqz p2, :cond_0

    .line 19
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 20
    .local v1, "action":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 21
    const-string v6, "AccountRemovalReceiver"

    const-string v7, "Account removal Broadcast received"

    invoke-static {v6, v7}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    const-string v6, "android.intent.action.SAMSUNGACCOUNT_SIGNOUT_COMPLETED"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 23
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v5

    .line 24
    .local v5, "manager":Landroid/accounts/AccountManager;
    if-eqz v5, :cond_0

    .line 25
    const-string v6, "com.osp.app.signin"

    invoke-virtual {v5, v6}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 27
    .local v0, "accounts":[Landroid/accounts/Account;
    array-length v6, v0

    if-nez v6, :cond_0

    .line 28
    const-string v6, "AccountRemovalReceiver"

    const-string v7, "Samsung Account Removed"

    invoke-static {v6, v7}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    const-string v6, "notification"

    invoke-virtual {p1, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/NotificationManager;

    .line 31
    .local v3, "mNotificationManager":Landroid/app/NotificationManager;
    invoke-virtual {v3, v8}, Landroid/app/NotificationManager;->cancel(I)V

    .line 32
    const-string v6, "QuotaWarningPreference"

    invoke-virtual {p1, v6, v8}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 35
    .local v4, "mSharedPreferences":Landroid/content/SharedPreferences;
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 37
    .local v2, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v6, "100MWARNING"

    invoke-interface {v2, v6, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 38
    const-string v6, "10MWARNING"

    invoke-interface {v2, v6, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 39
    const-string v6, "ISCHECKBOXCHECKED"

    invoke-interface {v2, v6, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 40
    const-string v6, "ISSCRAPCHECKBOXCHECKED"

    invoke-interface {v2, v6, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 41
    const-string v6, "verificationOk"

    const/4 v7, -0x1

    invoke-interface {v2, v6, v7}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 42
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 48
    .end local v0    # "accounts":[Landroid/accounts/Account;
    .end local v1    # "action":Ljava/lang/String;
    .end local v2    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v3    # "mNotificationManager":Landroid/app/NotificationManager;
    .end local v4    # "mSharedPreferences":Landroid/content/SharedPreferences;
    .end local v5    # "manager":Landroid/accounts/AccountManager;
    :cond_0
    return-void
.end method
