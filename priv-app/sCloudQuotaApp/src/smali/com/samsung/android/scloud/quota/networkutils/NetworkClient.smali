.class public final Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;
.super Ljava/lang/Object;
.source "NetworkClient.java"

# interfaces
.implements Lcom/samsung/android/scloud/quota/networkutils/HttpClientResponseListener;


# static fields
.field private static final APP_ID:Ljava/lang/String; = "tj9u972o46"

.field private static final APP_ID_HEADER:Ljava/lang/String; = "x-sc-appid"

.field private static final APP_ID_HEADER_STORAGE:Ljava/lang/String; = "appId"

.field private static final CLIENT_VERSION_HEADER:Ljava/lang/String; = "clientVersion"

.field private static final CLIENT_VERSION_HEADER_VALUE:Ljava/lang/String; = "1"

.field private static final CONNECTION_TIMEOUT_DURATION:I = 0xea60

.field private static final DEFAULT_PORT:I = 0x50

.field private static final DID_HEADER:Ljava/lang/String; = "did"

.field private static final JSON_CONTENT:Ljava/lang/String;

.field private static final MCC_HEADER:Ljava/lang/String; = "mcc"

.field private static final MNC_HEADER:Ljava/lang/String; = "mnc"

.field private static final OCTET_STREAM:Ljava/lang/String;

.field private static final SECURE_PORT:I = 0x1bb

.field private static final SOCKET_BUFFER_SIZE:I = 0x80000

.field private static final TAG:Ljava/lang/String; = "NetworkClient"

.field private static final USER_AGENT:Ljava/lang/String;


# instance fields
.field private mHttpClient:Lorg/apache/http/client/HttpClient;

.field private mNetworkResponse:Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;

.field private mThreadMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;",
            ">;"
        }
    .end annotation
.end field

.field private threadWait:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 124
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Landroid/os/Build;->DISPLAY:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->USER_AGENT:Ljava/lang/String;

    .line 142
    const-string v0, "application/octet-stream"

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->OCTET_STREAM:Ljava/lang/String;

    .line 144
    const-string v0, "application/json;charset=utf-8"

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->JSON_CONTENT:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 316
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 148
    new-instance v0, Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;

    invoke-direct {v0}, Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->mNetworkResponse:Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;

    .line 150
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->mHttpClient:Lorg/apache/http/client/HttpClient;

    .line 152
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->threadWait:Z

    .line 208
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->mThreadMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 320
    return-void
.end method

.method private execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;
    .locals 10
    .param p1, "request"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 538
    const-string v6, "x-sc-appid"

    const-string v8, "tj9u972o46"

    invoke-interface {p1, v6, v8}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 545
    const-string v6, "NetworkClient"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Request : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {p1}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 547
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    .line 549
    .local v4, "requestID":Ljava/lang/String;
    new-instance v2, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;

    invoke-direct {p0}, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->getHttpClient()Lorg/apache/http/client/HttpClient;

    move-result-object v6

    invoke-direct {v2, p1, v6, p0, v4}, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;-><init>(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/HttpClient;Lcom/samsung/android/scloud/quota/networkutils/HttpClientResponseListener;Ljava/lang/String;)V

    .line 555
    .local v2, "httpClientThread":Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;
    const/4 v3, 0x0

    .line 559
    .local v3, "httpResponse":Lorg/apache/http/HttpResponse;
    :try_start_0
    iget-object v6, p0, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->mThreadMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v6, v4, v2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 561
    invoke-virtual {v2}, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->start()V

    .line 563
    invoke-virtual {v2}, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->getThreadLock()Ljava/lang/Object;

    move-result-object v5

    .line 565
    .local v5, "threadLock":Ljava/lang/Object;
    monitor-enter v5
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 566
    const/4 v6, 0x1

    :try_start_1
    iput-boolean v6, p0, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->threadWait:Z

    .line 567
    :goto_0
    iget-boolean v6, p0, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->threadWait:Z

    if-eqz v6, :cond_0

    .line 568
    invoke-virtual {v5}, Ljava/lang/Object;->wait()V

    goto :goto_0

    .line 570
    :catchall_0
    move-exception v6

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v6
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    .line 588
    .end local v5    # "threadLock":Ljava/lang/Object;
    :catch_0
    move-exception v1

    .line 590
    .local v1, "e":Ljava/lang/InterruptedException;
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->close()V

    move-object v6, v7

    .line 620
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :goto_1
    return-object v6

    .line 570
    .restart local v5    # "threadLock":Ljava/lang/Object;
    :cond_0
    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 574
    :try_start_4
    iget-object v6, p0, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->mThreadMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v6, v4}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;

    move-object v2, v0

    .line 576
    if-nez v2, :cond_1

    move-object v6, v7

    .line 578
    goto :goto_1

    .line 582
    :cond_1
    invoke-virtual {v2}, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->getHttpResponse()Lorg/apache/http/HttpResponse;

    move-result-object v3

    .line 584
    iget-object v6, p0, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->mThreadMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v6, v4}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0

    .line 598
    if-nez v3, :cond_2

    .line 602
    new-instance v6, Ljava/io/IOException;

    const-string v7, "Response is NULL."

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 608
    :cond_2
    iget-object v6, p0, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->mNetworkResponse:Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;

    invoke-direct {p0, v3, v6}, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->handleResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;)V

    .line 612
    invoke-virtual {v2}, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->isClosed()Z

    move-result v6

    if-eqz v6, :cond_3

    move-object v6, v7

    .line 614
    goto :goto_1

    .line 620
    :cond_3
    iget-object v6, p0, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->mNetworkResponse:Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;

    goto :goto_1
.end method

.method private execute(Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/String;)Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;
    .locals 10
    .param p1, "request"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .param p2, "payReceipt"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 630
    const-string v6, "x-ups-signature"

    invoke-interface {p1, v6, p2}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 632
    const-string v6, "x-ups-apikey"

    const-string v8, "apikey"

    invoke-interface {p1, v6, v8}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 634
    const-string v6, "x-ups-retry"

    const-string v8, "12"

    invoke-interface {p1, v6, v8}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 644
    const-string v6, "NetworkClient"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Request : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {p1}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 647
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    .line 650
    .local v4, "requestID":Ljava/lang/String;
    new-instance v2, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;

    invoke-direct {p0}, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->getHttpClient()Lorg/apache/http/client/HttpClient;

    move-result-object v6

    invoke-direct {v2, p1, v6, p0, v4}, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;-><init>(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/HttpClient;Lcom/samsung/android/scloud/quota/networkutils/HttpClientResponseListener;Ljava/lang/String;)V

    .line 659
    .local v2, "httpClientThread":Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;
    const/4 v3, 0x0

    .line 665
    .local v3, "httpResponse":Lorg/apache/http/HttpResponse;
    :try_start_0
    iget-object v6, p0, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->mThreadMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v6, v4, v2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 668
    invoke-virtual {v2}, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->start()V

    .line 671
    invoke-virtual {v2}, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->getThreadLock()Ljava/lang/Object;

    move-result-object v5

    .line 674
    .local v5, "threadLock":Ljava/lang/Object;
    monitor-enter v5
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 675
    const/4 v6, 0x1

    :try_start_1
    iput-boolean v6, p0, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->threadWait:Z

    .line 677
    :goto_0
    iget-boolean v6, p0, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->threadWait:Z

    if-eqz v6, :cond_0

    .line 678
    invoke-virtual {v5}, Ljava/lang/Object;->wait()V

    goto :goto_0

    .line 681
    :catchall_0
    move-exception v6

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v6
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    .line 708
    .end local v5    # "threadLock":Ljava/lang/Object;
    :catch_0
    move-exception v1

    .line 711
    .local v1, "e":Ljava/lang/InterruptedException;
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->close()V

    move-object v6, v7

    .line 756
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :goto_1
    return-object v6

    .line 681
    .restart local v5    # "threadLock":Ljava/lang/Object;
    :cond_0
    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 687
    :try_start_4
    iget-object v6, p0, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->mThreadMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v6, v4}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;

    move-object v2, v0

    .line 690
    if-nez v2, :cond_1

    move-object v6, v7

    .line 693
    goto :goto_1

    .line 699
    :cond_1
    invoke-virtual {v2}, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->getHttpResponse()Lorg/apache/http/HttpResponse;

    move-result-object v3

    .line 702
    iget-object v6, p0, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->mThreadMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v6, v4}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0

    .line 723
    if-nez v3, :cond_2

    .line 729
    new-instance v6, Ljava/io/IOException;

    const-string v7, "Response is NULL."

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 738
    :cond_2
    iget-object v6, p0, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->mNetworkResponse:Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;

    invoke-direct {p0, v3, v6}, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->handleResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;)V

    .line 744
    invoke-virtual {v2}, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->isClosed()Z

    move-result v6

    if-eqz v6, :cond_3

    move-object v6, v7

    .line 747
    goto :goto_1

    .line 756
    :cond_3
    iget-object v6, p0, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->mNetworkResponse:Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;

    goto :goto_1
.end method

.method private execute(Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;
    .locals 10
    .param p1, "request"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .param p2, "did"    # Ljava/lang/String;
    .param p3, "countryCode"    # Ljava/lang/String;
    .param p4, "mnc"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 763
    const-string v6, "x-sc-appid"

    const-string v8, "tj9u972o46"

    invoke-interface {p1, v6, v8}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 765
    const-string v6, "did"

    invoke-interface {p1, v6, p2}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 767
    const-string v6, "mcc"

    invoke-interface {p1, v6, p3}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 769
    const-string v6, "mnc"

    invoke-interface {p1, v6, p4}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 771
    const-string v6, "clientVersion"

    const-string v8, "1"

    invoke-interface {p1, v6, v8}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 773
    const-string v6, "appId"

    const-string v8, "tj9u972o46"

    invoke-interface {p1, v6, v8}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 780
    const-string v6, "NetworkClient"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Request : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {p1}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 782
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    .line 784
    .local v4, "requestID":Ljava/lang/String;
    new-instance v2, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;

    invoke-direct {p0}, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->getHttpClient()Lorg/apache/http/client/HttpClient;

    move-result-object v6

    invoke-direct {v2, p1, v6, p0, v4}, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;-><init>(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/HttpClient;Lcom/samsung/android/scloud/quota/networkutils/HttpClientResponseListener;Ljava/lang/String;)V

    .line 790
    .local v2, "httpClientThread":Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;
    const/4 v3, 0x0

    .line 794
    .local v3, "httpResponse":Lorg/apache/http/HttpResponse;
    :try_start_0
    iget-object v6, p0, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->mThreadMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v6, v4, v2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 796
    invoke-virtual {v2}, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->start()V

    .line 798
    invoke-virtual {v2}, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->getThreadLock()Ljava/lang/Object;

    move-result-object v5

    .line 800
    .local v5, "threadLock":Ljava/lang/Object;
    monitor-enter v5
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 801
    const/4 v6, 0x1

    :try_start_1
    iput-boolean v6, p0, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->threadWait:Z

    .line 802
    :goto_0
    iget-boolean v6, p0, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->threadWait:Z

    if-eqz v6, :cond_0

    .line 803
    invoke-virtual {v5}, Ljava/lang/Object;->wait()V

    goto :goto_0

    .line 805
    :catchall_0
    move-exception v6

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v6
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    .line 823
    .end local v5    # "threadLock":Ljava/lang/Object;
    :catch_0
    move-exception v1

    .line 825
    .local v1, "e":Ljava/lang/InterruptedException;
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->close()V

    move-object v6, v7

    .line 855
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :goto_1
    return-object v6

    .line 805
    .restart local v5    # "threadLock":Ljava/lang/Object;
    :cond_0
    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 809
    :try_start_4
    iget-object v6, p0, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->mThreadMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v6, v4}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;

    move-object v2, v0

    .line 811
    if-nez v2, :cond_1

    move-object v6, v7

    .line 813
    goto :goto_1

    .line 817
    :cond_1
    invoke-virtual {v2}, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->getHttpResponse()Lorg/apache/http/HttpResponse;

    move-result-object v3

    .line 819
    iget-object v6, p0, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->mThreadMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v6, v4}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0

    .line 833
    if-nez v3, :cond_2

    .line 837
    new-instance v6, Ljava/io/IOException;

    const-string v7, "Response is NULL."

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 843
    :cond_2
    iget-object v6, p0, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->mNetworkResponse:Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;

    invoke-direct {p0, v3, v6}, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->handleResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;)V

    .line 847
    invoke-virtual {v2}, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->isClosed()Z

    move-result v6

    if-eqz v6, :cond_3

    move-object v6, v7

    .line 849
    goto :goto_1

    .line 855
    :cond_3
    iget-object v6, p0, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->mNetworkResponse:Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;

    goto :goto_1
.end method

.method private getHttpClient()Lorg/apache/http/client/HttpClient;
    .locals 8

    .prologue
    .line 262
    iget-object v4, p0, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->mHttpClient:Lorg/apache/http/client/HttpClient;

    if-eqz v4, :cond_0

    .line 264
    iget-object v4, p0, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->mHttpClient:Lorg/apache/http/client/HttpClient;

    .line 312
    :goto_0
    return-object v4

    .line 268
    :cond_0
    :try_start_0
    new-instance v2, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v2}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 270
    .local v2, "params":Lorg/apache/http/params/HttpParams;
    sget-object v4, Lorg/apache/http/HttpVersion;->HTTP_1_1:Lorg/apache/http/HttpVersion;

    invoke-static {v2, v4}, Lorg/apache/http/params/HttpProtocolParams;->setVersion(Lorg/apache/http/params/HttpParams;Lorg/apache/http/ProtocolVersion;)V

    .line 272
    const-string v4, "UTF-8"

    invoke-static {v2, v4}, Lorg/apache/http/params/HttpProtocolParams;->setContentCharset(Lorg/apache/http/params/HttpParams;Ljava/lang/String;)V

    .line 274
    sget-object v4, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->USER_AGENT:Ljava/lang/String;

    invoke-static {v2, v4}, Lorg/apache/http/params/HttpProtocolParams;->setUserAgent(Lorg/apache/http/params/HttpParams;Ljava/lang/String;)V

    .line 276
    const v4, 0xea60

    invoke-static {v2, v4}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 278
    const v4, 0xea60

    invoke-static {v2, v4}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 280
    const/high16 v4, 0x80000

    invoke-static {v2, v4}, Lorg/apache/http/params/HttpConnectionParams;->setSocketBufferSize(Lorg/apache/http/params/HttpParams;I)V

    .line 282
    const/4 v4, 0x0

    invoke-static {v2, v4}, Lorg/apache/http/params/HttpConnectionParams;->setStaleCheckingEnabled(Lorg/apache/http/params/HttpParams;Z)V

    .line 284
    const/4 v4, 0x0

    invoke-static {v2, v4}, Lorg/apache/http/client/params/HttpClientParams;->setRedirecting(Lorg/apache/http/params/HttpParams;Z)V

    .line 286
    const-string v4, "http.protocol.expect-continue"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-interface {v2, v4, v5}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 290
    new-instance v3, Lorg/apache/http/conn/scheme/SchemeRegistry;

    invoke-direct {v3}, Lorg/apache/http/conn/scheme/SchemeRegistry;-><init>()V

    .line 292
    .local v3, "registry":Lorg/apache/http/conn/scheme/SchemeRegistry;
    new-instance v4, Lorg/apache/http/conn/scheme/Scheme;

    const-string v5, "http"

    invoke-static {}, Lorg/apache/http/conn/scheme/PlainSocketFactory;->getSocketFactory()Lorg/apache/http/conn/scheme/PlainSocketFactory;

    move-result-object v6

    const/16 v7, 0x50

    invoke-direct {v4, v5, v6, v7}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v3, v4}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 294
    new-instance v4, Lorg/apache/http/conn/scheme/Scheme;

    const-string v5, "https"

    invoke-static {}, Lorg/apache/http/conn/ssl/SSLSocketFactory;->getSocketFactory()Lorg/apache/http/conn/ssl/SSLSocketFactory;

    move-result-object v6

    const/16 v7, 0x1bb

    invoke-direct {v4, v5, v6, v7}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v3, v4}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 298
    new-instance v0, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;

    invoke-direct {v0, v2, v3}, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;-><init>(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/scheme/SchemeRegistry;)V

    .line 302
    .local v0, "ccm":Lorg/apache/http/conn/ClientConnectionManager;
    new-instance v4, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v4, v0, v2}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V

    iput-object v4, p0, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->mHttpClient:Lorg/apache/http/client/HttpClient;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 312
    .end local v0    # "ccm":Lorg/apache/http/conn/ClientConnectionManager;
    .end local v2    # "params":Lorg/apache/http/params/HttpParams;
    .end local v3    # "registry":Lorg/apache/http/conn/scheme/SchemeRegistry;
    :goto_1
    iget-object v4, p0, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->mHttpClient:Lorg/apache/http/client/HttpClient;

    goto :goto_0

    .line 304
    :catch_0
    move-exception v1

    .line 306
    .local v1, "e":Ljava/lang/Exception;
    new-instance v4, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v4}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->mHttpClient:Lorg/apache/http/client/HttpClient;

    goto :goto_1
.end method

.method private handleResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;)V
    .locals 10
    .param p1, "httpResponse"    # Lorg/apache/http/HttpResponse;
    .param p2, "networkResponse"    # Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 457
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v6

    invoke-interface {v6}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v5

    .line 459
    .local v5, "status":I
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 465
    .local v1, "entity":Lorg/apache/http/HttpEntity;
    invoke-virtual {p2}, Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;->clear()V

    .line 467
    invoke-virtual {p2, v5}, Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;->setStatus(I)V

    .line 471
    const-string v6, "Content-Type"

    invoke-interface {p1, v6}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v2

    .line 475
    .local v2, "header":Lorg/apache/http/Header;
    if-eqz v2, :cond_4

    .line 477
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    .line 481
    .local v3, "headerString":Ljava/lang/String;
    sget-object v6, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->OCTET_STREAM:Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 483
    if-eqz v1, :cond_0

    .line 485
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v6

    invoke-virtual {p2, v6}, Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;->setFileInStream(Ljava/io/InputStream;)V

    .line 528
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    const/16 v6, 0x194

    if-ne v5, v6, :cond_1

    .line 530
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 534
    :cond_1
    return-void

    .line 487
    :cond_2
    sget-object v6, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->JSON_CONTENT:Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 493
    if-eqz v1, :cond_0

    .line 495
    :try_start_0
    const-string v6, "NetworkClient"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "SIZE :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 497
    invoke-static {v1}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v4

    .line 499
    .local v4, "response":Ljava/lang/String;
    invoke-virtual {p2, v4}, Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;->setBody(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 502
    .end local v4    # "response":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 504
    .local v0, "e":Ljava/lang/OutOfMemoryError;
    const-string v6, "NetworkClient"

    const-string v7, "Converting HTTPEntity to String returns out of Memory"

    invoke-static {v6, v7}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 506
    new-instance v6, Ljava/io/IOException;

    const-string v7, "Converting HTTPEntity to String returns out of Memory"

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 512
    .end local v0    # "e":Ljava/lang/OutOfMemoryError;
    :cond_3
    const-string v6, "NetworkClient"

    const-string v7, "Incorrect Header"

    invoke-static {v6, v7}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 514
    new-instance v6, Ljava/io/IOException;

    const-string v7, "Incorrect Header"

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 520
    .end local v3    # "headerString":Ljava/lang/String;
    :cond_4
    const-string v6, "NetworkClient"

    const-string v7, "Header is Empty"

    invoke-static {v6, v7}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 522
    new-instance v6, Ljava/io/IOException;

    const-string v7, "Header is Empty"

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6
.end method

.method private isUnsafe(C)Z
    .locals 2
    .param p1, "ch"    # C

    .prologue
    const/4 v0, 0x0

    .line 898
    const/16 v1, 0x30

    if-lt p1, v1, :cond_0

    const/16 v1, 0x39

    if-le p1, v1, :cond_2

    :cond_0
    const/16 v1, 0x41

    if-lt p1, v1, :cond_1

    const/16 v1, 0x5a

    if-le p1, v1, :cond_2

    :cond_1
    const/16 v1, 0x61

    if-lt p1, v1, :cond_3

    const/16 v1, 0x7a

    if-gt p1, v1, :cond_3

    .line 904
    :cond_2
    :goto_0
    return v0

    :cond_3
    const-string v1, " $+,;@[]"

    invoke-virtual {v1, p1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-ltz v1, :cond_2

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private toHex(I)C
    .locals 1
    .param p1, "ch"    # I

    .prologue
    .line 916
    const/16 v0, 0xa

    if-ge p1, v0, :cond_0

    add-int/lit8 v0, p1, 0x30

    :goto_0
    int-to-char v0, v0

    return v0

    :cond_0
    add-int/lit8 v0, p1, 0x41

    add-int/lit8 v0, v0, -0xa

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 6

    .prologue
    .line 216
    iget-object v5, p0, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->mThreadMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v5}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 218
    .local v0, "HttpClientThreadRequests":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 222
    .local v3, "requestID":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->mThreadMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v5, v3}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;

    .line 224
    .local v1, "httpClientThread":Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;
    if-eqz v1, :cond_0

    .line 228
    invoke-virtual {v1}, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->close()V

    .line 230
    invoke-virtual {v1}, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->isAlive()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 232
    invoke-virtual {v1}, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->interrupt()V

    .line 236
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->mThreadMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v5, v3}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 238
    invoke-virtual {v1}, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->getThreadLock()Ljava/lang/Object;

    move-result-object v4

    .line 240
    .local v4, "threadLock":Ljava/lang/Object;
    monitor-enter v4

    .line 244
    const/4 v5, 0x0

    :try_start_0
    iput-boolean v5, p0, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->threadWait:Z

    .line 245
    invoke-virtual {v4}, Ljava/lang/Object;->notify()V

    .line 246
    monitor-exit v4

    goto :goto_0

    :catchall_0
    move-exception v5

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    .line 254
    .end local v1    # "httpClientThread":Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;
    .end local v3    # "requestID":Ljava/lang/String;
    .end local v4    # "threadLock":Ljava/lang/Object;
    :cond_2
    return-void
.end method

.method public encodeURL(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 866
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 868
    .local v2, "encodedUrl":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    .local v0, "arr$":[C
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-char v1, v0, v3

    .line 872
    .local v1, "ch":C
    invoke-direct {p0, v1}, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->isUnsafe(C)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 874
    const-string v5, "%"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 876
    div-int/lit8 v5, v1, 0x10

    invoke-direct {p0, v5}, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->toHex(I)C

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 878
    rem-int/lit8 v5, v1, 0x10

    invoke-direct {p0, v5}, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->toHex(I)C

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 868
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 882
    :cond_0
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 888
    .end local v1    # "ch":C
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method

.method public get(Ljava/lang/String;)Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;
    .locals 2
    .param p1, "Url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 330
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {p0, p1}, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->encodeURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 332
    .local v0, "get":Lorg/apache/http/client/methods/HttpGet;
    invoke-direct {p0, v0}, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;

    move-result-object v1

    return-object v1
.end method

.method public get(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;
    .locals 2
    .param p1, "Url"    # Ljava/lang/String;
    .param p2, "did"    # Ljava/lang/String;
    .param p3, "countryCode"    # Ljava/lang/String;
    .param p4, "mnc"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 341
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {p0, p1}, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->encodeURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 343
    .local v0, "get":Lorg/apache/http/client/methods/HttpGet;
    invoke-direct {p0, v0, p2, p3, p4}, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;

    move-result-object v1

    return-object v1
.end method

.method public onResponse(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v5, 0x3

    .line 160
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;

    .line 164
    .local v0, "httpClientThread":Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;
    const-string v2, "NetworkClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "handleMessage - reason = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    iget v2, p1, Landroid/os/Message;->arg1:I

    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    iget v2, p1, Landroid/os/Message;->arg1:I

    if-ne v2, v5, :cond_1

    .line 172
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->mThreadMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->getRequestID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    :cond_1
    iget v2, p1, Landroid/os/Message;->arg1:I

    if-ne v2, v5, :cond_2

    .line 182
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->mHttpClient:Lorg/apache/http/client/HttpClient;

    invoke-interface {v2}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 184
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->mHttpClient:Lorg/apache/http/client/HttpClient;

    .line 190
    :cond_2
    invoke-virtual {v0}, Lcom/samsung/android/scloud/quota/networkutils/HttpClientThread;->getThreadLock()Ljava/lang/Object;

    move-result-object v1

    .line 192
    .local v1, "threadLock":Ljava/lang/Object;
    monitor-enter v1

    .line 196
    const/4 v2, 0x0

    :try_start_0
    iput-boolean v2, p0, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->threadWait:Z

    .line 197
    invoke-virtual {v1}, Ljava/lang/Object;->notify()V

    .line 200
    monitor-exit v1

    .line 202
    return-void

    .line 200
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public post(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;
    .locals 5
    .param p1, "Url"    # Ljava/lang/String;
    .param p2, "json"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 355
    new-instance v1, Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {p0, p1}, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->encodeURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 359
    .local v1, "post":Lorg/apache/http/client/methods/HttpPost;
    if-eqz p2, :cond_0

    .line 361
    new-instance v0, Lorg/apache/http/entity/StringEntity;

    const-string v2, "UTF-8"

    invoke-direct {v0, p2, v2}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 363
    .local v0, "entity":Lorg/apache/http/entity/StringEntity;
    new-instance v2, Lorg/apache/http/message/BasicHeader;

    const-string v3, "Content-Type"

    const-string v4, "Application/JSON;charset=UTF-8"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lorg/apache/http/entity/StringEntity;->setContentType(Lorg/apache/http/Header;)V

    .line 365
    invoke-virtual {v1, v0}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 371
    .end local v0    # "entity":Lorg/apache/http/entity/StringEntity;
    :cond_0
    invoke-direct {p0, v1}, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;

    move-result-object v2

    return-object v2
.end method

.method public post(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;
    .locals 5
    .param p1, "Url"    # Ljava/lang/String;
    .param p2, "json"    # Ljava/lang/String;
    .param p3, "payReceipt"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 382
    new-instance v1, Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {p0, p1}, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->encodeURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 388
    .local v1, "post":Lorg/apache/http/client/methods/HttpPost;
    if-eqz p2, :cond_0

    .line 391
    new-instance v0, Lorg/apache/http/entity/StringEntity;

    const-string v2, "UTF-8"

    invoke-direct {v0, p2, v2}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    .local v0, "entity":Lorg/apache/http/entity/StringEntity;
    new-instance v2, Lorg/apache/http/message/BasicHeader;

    const-string v3, "Content-Type"

    const-string v4, "Application/JSON;charset=UTF-8"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lorg/apache/http/entity/StringEntity;->setContentType(Lorg/apache/http/Header;)V

    .line 397
    invoke-virtual {v1, v0}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 406
    .end local v0    # "entity":Lorg/apache/http/entity/StringEntity;
    :cond_0
    invoke-direct {p0, v1, p3}, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/String;)Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;

    move-result-object v2

    return-object v2
.end method

.method public post(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;
    .locals 5
    .param p1, "Url"    # Ljava/lang/String;
    .param p2, "json"    # Ljava/lang/String;
    .param p3, "did"    # Ljava/lang/String;
    .param p4, "countryCode"    # Ljava/lang/String;
    .param p5, "mnc"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 418
    new-instance v1, Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {p0, p1}, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->encodeURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 424
    .local v1, "post":Lorg/apache/http/client/methods/HttpPost;
    if-eqz p2, :cond_0

    .line 427
    new-instance v0, Lorg/apache/http/entity/StringEntity;

    const-string v2, "UTF-8"

    invoke-direct {v0, p2, v2}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    .local v0, "entity":Lorg/apache/http/entity/StringEntity;
    new-instance v2, Lorg/apache/http/message/BasicHeader;

    const-string v3, "Content-Type"

    const-string v4, "Application/JSON;charset=UTF-8"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lorg/apache/http/entity/StringEntity;->setContentType(Lorg/apache/http/Header;)V

    .line 433
    invoke-virtual {v1, v0}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 442
    .end local v0    # "entity":Lorg/apache/http/entity/StringEntity;
    :cond_0
    invoke-direct {p0, v1, p3, p4, p5}, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;

    move-result-object v2

    return-object v2
.end method
