.class public Lcom/samsung/android/scloud/quota/AuthControl;
.super Ljava/lang/Object;
.source "AuthControl.java"


# static fields
.field private static final ACCESS_TOKEN:Ljava/lang/String; = "com.sec.android.samsungcloudsync.AccessToken"

.field private static final TAG:Ljava/lang/String; = "AuthControl"


# instance fields
.field private mAuthManager:Lcom/samsung/android/scloud/quota/AuthManager;

.field private mPushClientConnection:Landroid/content/ServiceConnection;

.field private mService:Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;

.field private final syncLock:Ljava/lang/Object;

.field private syncWait:Z


# direct methods
.method public constructor <init>(Lcom/samsung/android/scloud/quota/AuthManager;)V
    .locals 2
    .param p1, "authManager"    # Lcom/samsung/android/scloud/quota/AuthManager;

    .prologue
    const/4 v1, 0x0

    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object v1, p0, Lcom/samsung/android/scloud/quota/AuthControl;->mService:Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;

    .line 68
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/AuthControl;->syncLock:Ljava/lang/Object;

    .line 70
    iput-object v1, p0, Lcom/samsung/android/scloud/quota/AuthControl;->mAuthManager:Lcom/samsung/android/scloud/quota/AuthManager;

    .line 72
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/scloud/quota/AuthControl;->syncWait:Z

    .line 74
    new-instance v0, Lcom/samsung/android/scloud/quota/AuthControl$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/scloud/quota/AuthControl$1;-><init>(Lcom/samsung/android/scloud/quota/AuthControl;)V

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/AuthControl;->mPushClientConnection:Landroid/content/ServiceConnection;

    .line 121
    iput-object p1, p0, Lcom/samsung/android/scloud/quota/AuthControl;->mAuthManager:Lcom/samsung/android/scloud/quota/AuthManager;

    .line 123
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/scloud/quota/AuthControl;)Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/AuthControl;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/AuthControl;->mService:Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;

    return-object v0
.end method

.method static synthetic access$002(Lcom/samsung/android/scloud/quota/AuthControl;Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;)Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/AuthControl;
    .param p1, "x1"    # Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/samsung/android/scloud/quota/AuthControl;->mService:Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;

    return-object p1
.end method

.method static synthetic access$100(Lcom/samsung/android/scloud/quota/AuthControl;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/AuthControl;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/AuthControl;->syncLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$202(Lcom/samsung/android/scloud/quota/AuthControl;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/AuthControl;
    .param p1, "x1"    # Z

    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/samsung/android/scloud/quota/AuthControl;->syncWait:Z

    return p1
.end method


# virtual methods
.method public getAuthInformation(Landroid/content/Context;Z)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "bFresh"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/scloud/quota/QuotaException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v8, 0x0

    .line 128
    new-instance v3, Landroid/content/Intent;

    const-string v5, "com.sec.android.samsungcloudsync.AccessToken"

    invoke-direct {v3, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 130
    .local v3, "intent":Landroid/content/Intent;
    iget-object v5, p0, Lcom/samsung/android/scloud/quota/AuthControl;->mPushClientConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p1, v3, v5, v6}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    .line 132
    .local v0, "bResult":Z
    if-eqz v0, :cond_6

    .line 134
    iget-object v5, p0, Lcom/samsung/android/scloud/quota/AuthControl;->mService:Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;

    if-nez v5, :cond_2

    .line 138
    :try_start_0
    iget-object v6, p0, Lcom/samsung/android/scloud/quota/AuthControl;->syncLock:Ljava/lang/Object;

    monitor-enter v6
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 139
    const/4 v5, 0x1

    :try_start_1
    iput-boolean v5, p0, Lcom/samsung/android/scloud/quota/AuthControl;->syncWait:Z

    .line 140
    :goto_0
    iget-boolean v5, p0, Lcom/samsung/android/scloud/quota/AuthControl;->syncWait:Z

    if-eqz v5, :cond_1

    .line 141
    iget-object v5, p0, Lcom/samsung/android/scloud/quota/AuthControl;->syncLock:Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->wait()V

    goto :goto_0

    .line 143
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v5
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    .line 145
    :catch_0
    move-exception v2

    .line 147
    .local v2, "e1":Ljava/lang/InterruptedException;
    const-string v5, "AuthControl"

    const-string v6, "getAuthInformation: INTERRUPTED - wait()."

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    iget-object v5, p0, Lcom/samsung/android/scloud/quota/AuthControl;->mService:Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;

    if-eqz v5, :cond_0

    .line 153
    iget-object v5, p0, Lcom/samsung/android/scloud/quota/AuthControl;->mPushClientConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p1, v5}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 155
    iput-object v8, p0, Lcom/samsung/android/scloud/quota/AuthControl;->mService:Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;

    .line 159
    :cond_0
    new-instance v5, Lcom/samsung/android/scloud/quota/QuotaException;

    const/4 v6, 0x7

    invoke-direct {v5, v6}, Lcom/samsung/android/scloud/quota/QuotaException;-><init>(I)V

    throw v5

    .line 143
    .end local v2    # "e1":Ljava/lang/InterruptedException;
    :cond_1
    :try_start_3
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 167
    :cond_2
    iget-object v5, p0, Lcom/samsung/android/scloud/quota/AuthControl;->mService:Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;

    if-nez v5, :cond_3

    .line 171
    new-instance v5, Lcom/samsung/android/scloud/quota/QuotaException;

    const/4 v6, 0x5

    invoke-direct {v5, v6}, Lcom/samsung/android/scloud/quota/QuotaException;-><init>(I)V

    throw v5

    .line 177
    :cond_3
    sget-object v5, Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, v8}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;

    .line 183
    .local v4, "mAuth":Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;
    :try_start_4
    iget-object v5, p0, Lcom/samsung/android/scloud/quota/AuthControl;->mService:Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;

    invoke-interface {v5, p2}, Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;->getAuthInformation(Z)Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;

    move-result-object v4

    .line 184
    if-eqz v4, :cond_4

    .line 186
    const-string v5, "AuthControl"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getAuthInformation : STATUS = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v4, Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;->status:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_1

    .line 202
    :cond_4
    :goto_1
    iget-object v5, p0, Lcom/samsung/android/scloud/quota/AuthControl;->mService:Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;

    if-eqz v5, :cond_5

    .line 206
    iget-object v5, p0, Lcom/samsung/android/scloud/quota/AuthControl;->mPushClientConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p1, v5}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 208
    iput-object v8, p0, Lcom/samsung/android/scloud/quota/AuthControl;->mService:Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;

    .line 213
    :cond_5
    if-eqz v4, :cond_6

    .line 214
    iget-object v5, p0, Lcom/samsung/android/scloud/quota/AuthControl;->mAuthManager:Lcom/samsung/android/scloud/quota/AuthManager;

    invoke-virtual {v5, v4}, Lcom/samsung/android/scloud/quota/AuthManager;->updateAuthInformation(Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;)V

    .line 218
    .end local v4    # "mAuth":Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;
    :cond_6
    return-void

    .line 188
    .restart local v4    # "mAuth":Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;
    :catch_1
    move-exception v1

    .line 190
    .local v1, "e":Landroid/os/RemoteException;
    const-string v5, "AuthControl"

    const-string v6, "getAuthInformation: FAILED - RemoteService."

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
