.class public interface abstract Lcom/samsung/android/scloud/quota/QuotaConstants$KEY;
.super Ljava/lang/Object;
.source "QuotaConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/quota/QuotaConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "KEY"
.end annotation


# static fields
.field public static final COMMERCE_TARGET_FIELD:Ljava/lang/String; = "commerce_target"

.field public static final CURRENT_PLAN_MESSAGE:Ljava/lang/String; = "current_plan_message"

.field public static final DATE_FIELD:Ljava/lang/String; = "date_field"

.field public static final DIALOG_ID:Ljava/lang/String; = "DIALOG"

.field public static final NEXT_PLAN_MESSAGE:Ljava/lang/String; = "next_plan_message"
