.class public interface abstract Lcom/samsung/android/scloud/quota/QuotaConstants$QuotaCons;
.super Ljava/lang/Object;
.source "QuotaConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/quota/QuotaConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "QuotaCons"
.end annotation


# static fields
.field public static final BASE_URL:Ljava/lang/String; = "/user/quota"

.field public static final BNR_CLEAR:Ljava/lang/String; = "clear"

.field public static final BNR_CLEAR_ALL:Ljava/lang/String; = "clearall"

.field public static final CID_PARAM:Ljava/lang/String; = "&cid="

.field public static final CLOUD_END:Ljava/lang/String; = "/cloud/end?"

.field public static final CLOUD_START:Ljava/lang/String; = "/cloud/start?"

.field public static final CTID_PARAM:Ljava/lang/String; = "&ctid="

.field public static final DELETE_BNR_ACTION:Ljava/lang/String; = "/bnr/?action="

.field public static final GET_ALL_USAGE:Ljava/lang/String; = "?view=all"

.field public static final GET_DETAILS:Ljava/lang/String; = "/bnr/details?"

.field public static final PARAM_CDID:Ljava/lang/String; = "client_did="

.field public static final PARAM_CID:Ljava/lang/String; = "cid"

.field public static final PARAM_DID:Ljava/lang/String; = "did="

.field public static final RESULT_DATA:Ljava/lang/String; = "result_data"

.field public static final RESULT_ERROR:Ljava/lang/String; = "e"

.field public static final RESULT_PARAM:Ljava/lang/String; = "&result="

.field public static final RESULT_RCODE:Ljava/lang/String; = "r"

.field public static final RESULT_RCODE_SUC:Ljava/lang/String; = "0"

.field public static final TOTAL_USAGE:Ljava/lang/String; = "?view=total"

.field public static final TOTAL_USAGE_BY_APP:Ljava/lang/String; = "?view=appid"

.field public static final TRIGGER_PARAM:Ljava/lang/String; = "&trigger=quota"

.field public static final USAGE_BY_CID:Ljava/lang/String; = "?view=cid&cid="

.field public static final USAGE_BY_DEVICE:Ljava/lang/String; = "?view=device&did="

.field public static final USAGE_BY_USER:Ljava/lang/String; = "?view=user"
