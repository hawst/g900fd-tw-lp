.class public Lcom/samsung/android/scloud/quota/BackupList;
.super Landroid/app/Activity;
.source "BackupList.java"

# interfaces
.implements Landroid/app/ActionBar$OnNavigationListener;
.implements Lcom/samsung/android/scloud/quota/QuotaResponseThreadListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/scloud/quota/BackupList$AccessTokenclass;,
        Lcom/samsung/android/scloud/quota/BackupList$ViewHolder;,
        Lcom/samsung/android/scloud/quota/BackupList$BackupListAdapter;
    }
.end annotation


# static fields
.field private static final MENU_ID_DELETE:I = 0x1

.field private static final MENU_ID_DONE:I = 0x2

.field private static final TAG:Ljava/lang/String; = "BackupList"

.field public static mContext:Landroid/content/Context;

.field private static mProgressDialog:Landroid/app/ProgressDialog;


# instance fields
.field private final DELETE_LIST:I

.field public final GET_UPDATED_LIST:I

.field backupList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/scloud/quota/BackupDetails;",
            ">;"
        }
    .end annotation
.end field

.field backupListAdapter:Lcom/samsung/android/scloud/quota/BackupList$BackupListAdapter;

.field ck:Landroid/widget/CheckBox;

.field private count:I

.field emptyText:Landroid/widget/TextView;

.field flag:Z

.field private isAccessToken:Z

.field list:Landroid/widget/ListView;

.field longFlag:Z

.field private mAuthControlNew:Lcom/samsung/android/scloud/quota/AuthControlForNewDataRelay;

.field protected mAuthManager:Lcom/samsung/android/scloud/quota/AuthManager;

.field mBackupDetaiList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/scloud/quota/BackupDetailsList;",
            ">;"
        }
    .end annotation
.end field

.field mCheckbox:Landroid/widget/CheckBox;

.field mCtid:Ljava/lang/String;

.field mDeleteList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/scloud/quota/BackupDetails;",
            ">;"
        }
    .end annotation
.end field

.field private mDialogFlag:Z

.field private mDropDownList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mHandler:Landroid/os/Handler;

.field mListSelected:[Z

.field mQuotaThread:Lcom/samsung/android/scloud/quota/QuotaThread;

.field private mTwDragSelectedItemArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field mnu1:Landroid/view/MenuItem;

.field mnu2:Landroid/view/MenuItem;

.field navigationAdapter:Lcom/samsung/android/scloud/quota/DropDownNavigationAdapter;

.field private quotaServiceManager:Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;

.field result:Lcom/samsung/android/scloud/quota/response/KVSResponse;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/scloud/quota/BackupList;->mContext:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 45
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 51
    iput-boolean v2, p0, Lcom/samsung/android/scloud/quota/BackupList;->flag:Z

    .line 52
    iput-boolean v3, p0, Lcom/samsung/android/scloud/quota/BackupList;->longFlag:Z

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mBackupDetaiList:Ljava/util/ArrayList;

    .line 60
    iput-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList;->mAuthManager:Lcom/samsung/android/scloud/quota/AuthManager;

    .line 61
    iput-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList;->mAuthControlNew:Lcom/samsung/android/scloud/quota/AuthControlForNewDataRelay;

    .line 62
    iput v3, p0, Lcom/samsung/android/scloud/quota/BackupList;->DELETE_LIST:I

    .line 63
    iput v3, p0, Lcom/samsung/android/scloud/quota/BackupList;->GET_UPDATED_LIST:I

    .line 64
    iput-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList;->mListSelected:[Z

    .line 65
    iput v2, p0, Lcom/samsung/android/scloud/quota/BackupList;->count:I

    .line 67
    iput-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList;->mDropDownList:Ljava/util/ArrayList;

    .line 70
    iput-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList;->mCheckbox:Landroid/widget/CheckBox;

    .line 71
    iput-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList;->mCtid:Ljava/lang/String;

    .line 72
    iput-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList;->emptyText:Landroid/widget/TextView;

    .line 73
    iput-boolean v2, p0, Lcom/samsung/android/scloud/quota/BackupList;->isAccessToken:Z

    .line 74
    iput-boolean v2, p0, Lcom/samsung/android/scloud/quota/BackupList;->mDialogFlag:Z

    .line 77
    iput-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList;->quotaServiceManager:Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;

    .line 78
    iput-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList;->result:Lcom/samsung/android/scloud/quota/response/KVSResponse;

    .line 79
    iput-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList;->mQuotaThread:Lcom/samsung/android/scloud/quota/QuotaThread;

    .line 749
    new-instance v0, Lcom/samsung/android/scloud/quota/BackupList$8;

    invoke-direct {v0, p0}, Lcom/samsung/android/scloud/quota/BackupList$8;-><init>(Lcom/samsung/android/scloud/quota/BackupList;)V

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mHandler:Landroid/os/Handler;

    .line 1176
    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/scloud/quota/BackupList;Ljava/lang/Boolean;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/BackupList;
    .param p1, "x1"    # Ljava/lang/Boolean;

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/samsung/android/scloud/quota/BackupList;->actionSelectallclicked(Ljava/lang/Boolean;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/samsung/android/scloud/quota/BackupList;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/BackupList;

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mDialogFlag:Z

    return v0
.end method

.method static synthetic access$1002(Lcom/samsung/android/scloud/quota/BackupList;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/BackupList;
    .param p1, "x1"    # Z

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/samsung/android/scloud/quota/BackupList;->mDialogFlag:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/samsung/android/scloud/quota/BackupList;)Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/BackupList;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->quotaServiceManager:Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/samsung/android/scloud/quota/BackupList;Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;)Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/BackupList;
    .param p1, "x1"    # Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/samsung/android/scloud/quota/BackupList;->quotaServiceManager:Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/samsung/android/scloud/quota/BackupList;)Lcom/samsung/android/scloud/quota/AuthControlForNewDataRelay;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/BackupList;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mAuthControlNew:Lcom/samsung/android/scloud/quota/AuthControlForNewDataRelay;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/samsung/android/scloud/quota/BackupList;Lcom/samsung/android/scloud/quota/AuthControlForNewDataRelay;)Lcom/samsung/android/scloud/quota/AuthControlForNewDataRelay;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/BackupList;
    .param p1, "x1"    # Lcom/samsung/android/scloud/quota/AuthControlForNewDataRelay;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/samsung/android/scloud/quota/BackupList;->mAuthControlNew:Lcom/samsung/android/scloud/quota/AuthControlForNewDataRelay;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/samsung/android/scloud/quota/BackupList;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/BackupList;

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->isAccessToken:Z

    return v0
.end method

.method static synthetic access$1302(Lcom/samsung/android/scloud/quota/BackupList;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/BackupList;
    .param p1, "x1"    # Z

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/samsung/android/scloud/quota/BackupList;->isAccessToken:Z

    return p1
.end method

.method static synthetic access$1400(Lcom/samsung/android/scloud/quota/BackupList;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/BackupList;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/samsung/android/scloud/quota/BackupList;->startQuotaThread()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/android/scloud/quota/BackupList;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/BackupList;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/samsung/android/scloud/quota/BackupList;->registerSpinner()V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/android/scloud/quota/BackupList;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/BackupList;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mTwDragSelectedItemArray:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/scloud/quota/BackupList;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/BackupList;

    .prologue
    .line 45
    iget v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->count:I

    return v0
.end method

.method static synthetic access$402(Lcom/samsung/android/scloud/quota/BackupList;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/BackupList;
    .param p1, "x1"    # I

    .prologue
    .line 45
    iput p1, p0, Lcom/samsung/android/scloud/quota/BackupList;->count:I

    return p1
.end method

.method static synthetic access$408(Lcom/samsung/android/scloud/quota/BackupList;)I
    .locals 2
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/BackupList;

    .prologue
    .line 45
    iget v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->count:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/samsung/android/scloud/quota/BackupList;->count:I

    return v0
.end method

.method static synthetic access$410(Lcom/samsung/android/scloud/quota/BackupList;)I
    .locals 2
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/BackupList;

    .prologue
    .line 45
    iget v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->count:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/samsung/android/scloud/quota/BackupList;->count:I

    return v0
.end method

.method static synthetic access$500(Lcom/samsung/android/scloud/quota/BackupList;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/BackupList;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/samsung/android/scloud/quota/BackupList;->isDeviceUseDropdownMenu()Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/samsung/android/scloud/quota/BackupList;Ljava/util/ArrayList;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/BackupList;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/samsung/android/scloud/quota/BackupList;->startDeleteThread(Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic access$700(Lcom/samsung/android/scloud/quota/BackupList;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/BackupList;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/samsung/android/scloud/quota/BackupList;->backTolist()V

    return-void
.end method

.method static synthetic access$800(Lcom/samsung/android/scloud/quota/BackupList;J)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/quota/BackupList;
    .param p1, "x1"    # J

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/scloud/quota/BackupList;->getDateStringFromTimeSpan(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900()Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/samsung/android/scloud/quota/BackupList;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method private actionSelectallclicked(Ljava/lang/Boolean;)V
    .locals 5
    .param p1, "isChecked"    # Ljava/lang/Boolean;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 604
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/BackupList;->list:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getCount()I

    move-result v1

    .line 605
    .local v1, "len":I
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 607
    iput v1, p0, Lcom/samsung/android/scloud/quota/BackupList;->count:I

    .line 608
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 609
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/BackupList;->mListSelected:[Z

    aput-boolean v4, v2, v0

    .line 610
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/BackupList;->list:Landroid/widget/ListView;

    invoke-virtual {v2, v0, v4}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 608
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 615
    .end local v0    # "i":I
    :cond_0
    iput v3, p0, Lcom/samsung/android/scloud/quota/BackupList;->count:I

    .line 616
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_1
    if-ge v0, v1, :cond_1

    .line 617
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/BackupList;->mListSelected:[Z

    aput-boolean v3, v2, v0

    .line 618
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/BackupList;->list:Landroid/widget/ListView;

    invoke-virtual {v2, v0, v3}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 616
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 621
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/BackupList;->invalidateOptionsMenu()V

    .line 624
    return-void
.end method

.method private backPressed()V
    .locals 4

    .prologue
    .line 1060
    const-string v1, "BackupList"

    const-string v2, "backPressed called "

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1062
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList;->mQuotaThread:Lcom/samsung/android/scloud/quota/QuotaThread;

    if-eqz v1, :cond_0

    .line 1064
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList;->mQuotaThread:Lcom/samsung/android/scloud/quota/QuotaThread;

    invoke-virtual {v1}, Lcom/samsung/android/scloud/quota/QuotaThread;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1066
    const-string v1, "BackupList"

    const-string v2, "  mQuotaThread Thread is not Null and Alive"

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1070
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList;->mQuotaThread:Lcom/samsung/android/scloud/quota/QuotaThread;

    invoke-virtual {v1}, Lcom/samsung/android/scloud/quota/QuotaThread;->interrupt()V

    .line 1074
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList;->quotaServiceManager:Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;

    if-nez v1, :cond_2

    .line 1076
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList;->mAuthManager:Lcom/samsung/android/scloud/quota/AuthManager;

    if-eqz v1, :cond_1

    .line 1078
    new-instance v1, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;

    sget-object v2, Lcom/samsung/android/scloud/quota/BackupList;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/scloud/quota/BackupList;->mAuthManager:Lcom/samsung/android/scloud/quota/AuthManager;

    invoke-direct {v1, v2, v3}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;-><init>(Landroid/content/Context;Lcom/samsung/android/scloud/quota/AuthManager;)V

    iput-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList;->quotaServiceManager:Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;

    .line 1084
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/android/scloud/quota/BackupList;->callEndService()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1124
    :cond_0
    :goto_0
    return-void

    .line 1086
    :catch_0
    move-exception v0

    .line 1090
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 1096
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_1
    const-string v1, "BackupList"

    const-string v2, "mAuthManager is null"

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1098
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/BackupList;->finish()V

    goto :goto_0

    .line 1106
    :cond_2
    :try_start_1
    invoke-direct {p0}, Lcom/samsung/android/scloud/quota/BackupList;->callEndService()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 1108
    :catch_1
    move-exception v0

    .line 1112
    .restart local v0    # "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method private backTolist()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 581
    iput-boolean v2, p0, Lcom/samsung/android/scloud/quota/BackupList;->flag:Z

    .line 582
    iput-boolean v3, p0, Lcom/samsung/android/scloud/quota/BackupList;->longFlag:Z

    .line 585
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList;->mListSelected:[Z

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 586
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList;->mListSelected:[Z

    aget-boolean v1, v1, v0

    if-ne v1, v3, :cond_0

    .line 587
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList;->mListSelected:[Z

    aput-boolean v2, v1, v0

    .line 588
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList;->list:Landroid/widget/ListView;

    invoke-virtual {v1, v0, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 589
    iget v1, p0, Lcom/samsung/android/scloud/quota/BackupList;->count:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/samsung/android/scloud/quota/BackupList;->count:I

    .line 585
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 592
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList;->mCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 594
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/BackupList;->invalidateOptionsMenu()V

    .line 595
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList;->backupListAdapter:Lcom/samsung/android/scloud/quota/BackupList$BackupListAdapter;

    invoke-virtual {v1}, Lcom/samsung/android/scloud/quota/BackupList$BackupListAdapter;->notifyDataSetChanged()V

    .line 596
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList;->list:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/samsung/android/scloud/quota/BackupList;->backupListAdapter:Lcom/samsung/android/scloud/quota/BackupList$BackupListAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 597
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList;->list:Landroid/widget/ListView;

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/quota/BackupList;->registerForContextMenu(Landroid/view/View;)V

    .line 599
    return-void
.end method

.method private callEndService()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 1132
    const-string v1, "BackupList"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "LASTAPI Get "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/quota/BackupList;->quotaServiceManager:Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;

    invoke-virtual {v3}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->getLastApi()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1134
    const-string v1, "BackupList"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "LASTAPI Success "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/quota/BackupList;->quotaServiceManager:Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;

    invoke-virtual {v3}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->getApiSuccess()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1136
    const-string v1, "BackupList"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "LASTAPI Result "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/quota/BackupList;->quotaServiceManager:Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;

    invoke-virtual {v3}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->getApiResult()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1140
    new-instance v0, Lcom/samsung/android/scloud/quota/BackupList$9;

    invoke-direct {v0, p0}, Lcom/samsung/android/scloud/quota/BackupList$9;-><init>(Lcom/samsung/android/scloud/quota/BackupList;)V

    .line 1168
    .local v0, "endThread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 1170
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/BackupList;->finish()V

    .line 1172
    return-void
.end method

.method private createMenu(Landroid/view/Menu;)V
    .locals 10
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v9, 0x7f070020

    const v8, 0x7f02000a

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 391
    const-string v0, "BackupList"

    const-string v1, "createMenu()"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    iget-boolean v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->flag:Z

    if-nez v0, :cond_1

    .line 395
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/BackupList;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 396
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/BackupList;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 397
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/BackupList;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 398
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/BackupList;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p0}, Landroid/app/ActionBar;->setListNavigationCallbacks(Landroid/widget/SpinnerAdapter;Landroid/app/ActionBar$OnNavigationListener;)V

    .line 399
    const v0, 0x7f070010

    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/quota/BackupList;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/quota/BackupList;->setTitle(Ljava/lang/CharSequence;)V

    .line 400
    invoke-virtual {p0, v9}, Lcom/samsung/android/scloud/quota/BackupList;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v5, v6, v5, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mnu1:Landroid/view/MenuItem;

    .line 401
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mnu1:Landroid/view/MenuItem;

    invoke-interface {v0, v5}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 402
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mnu1:Landroid/view/MenuItem;

    invoke-interface {v0, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 403
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mnu1:Landroid/view/MenuItem;

    const v1, 0x7f020009

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 405
    const v0, 0x7f070025

    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/quota/BackupList;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v5, v7, v5, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mnu2:Landroid/view/MenuItem;

    .line 406
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mnu2:Landroid/view/MenuItem;

    invoke-interface {v0, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 408
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->list:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->list:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 409
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mnu1:Landroid/view/MenuItem;

    invoke-interface {v0, v6}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 410
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mnu1:Landroid/view/MenuItem;

    invoke-interface {v0, v8}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 438
    :cond_0
    :goto_0
    return-void

    .line 413
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/BackupList;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070063

    new-array v3, v6, [Ljava/lang/Object;

    iget v4, p0, Lcom/samsung/android/scloud/quota/BackupList;->count:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 414
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mnu1:Landroid/view/MenuItem;

    invoke-interface {v0, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 415
    const v0, 0x7f070025

    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/quota/BackupList;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v5, v7, v5, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mnu2:Landroid/view/MenuItem;

    .line 417
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mnu2:Landroid/view/MenuItem;

    invoke-interface {v0, v7}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 420
    iget-boolean v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->longFlag:Z

    if-nez v0, :cond_2

    .line 422
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mnu2:Landroid/view/MenuItem;

    invoke-virtual {p0, v9}, Lcom/samsung/android/scloud/quota/BackupList;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 423
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mnu2:Landroid/view/MenuItem;

    invoke-interface {v0, v8}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 427
    :cond_2
    iget v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->count:I

    if-nez v0, :cond_3

    .line 428
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mnu2:Landroid/view/MenuItem;

    invoke-interface {v0, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 429
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mnu2:Landroid/view/MenuItem;

    invoke-interface {v0, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 431
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mnu2:Landroid/view/MenuItem;

    invoke-interface {v0, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 432
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mnu2:Landroid/view/MenuItem;

    invoke-interface {v0, v6}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method private getDateStringFromTimeSpan(J)Ljava/lang/String;
    .locals 5
    .param p1, "time"    # J

    .prologue
    .line 1040
    sget-object v3, Lcom/samsung/android/scloud/quota/BackupList;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    .line 1044
    .local v0, "dateFormat":Ljava/text/DateFormat;
    sget-object v3, Lcom/samsung/android/scloud/quota/BackupList;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    .line 1048
    .local v1, "timeFormat":Ljava/text/DateFormat;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1052
    .local v2, "timeStr":Ljava/lang/String;
    return-object v2
.end method

.method private isDeviceUseDropdownMenu()Z
    .locals 2

    .prologue
    .line 1280
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ne v0, v1, :cond_0

    sget-boolean v0, Lcom/samsung/android/scloud/quota/QuotaConstants$DEVICE;->IS_DEVICE_T:Z

    if-eqz v0, :cond_0

    .line 1281
    const/4 v0, 0x1

    .line 1283
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private registerSpinner()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1288
    invoke-direct {p0}, Lcom/samsung/android/scloud/quota/BackupList;->isDeviceUseDropdownMenu()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1290
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/BackupList;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 1291
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/BackupList;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 1292
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/BackupList;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 1295
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/BackupList;->setListItem()V

    .line 1297
    new-instance v0, Lcom/samsung/android/scloud/quota/DropDownNavigationAdapter;

    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/BackupList;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/quota/BackupList;->mDropDownList:Ljava/util/ArrayList;

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/scloud/quota/DropDownNavigationAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->navigationAdapter:Lcom/samsung/android/scloud/quota/DropDownNavigationAdapter;

    .line 1299
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/BackupList;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList;->navigationAdapter:Lcom/samsung/android/scloud/quota/DropDownNavigationAdapter;

    invoke-virtual {v0, v1, p0}, Landroid/app/ActionBar;->setListNavigationCallbacks(Landroid/widget/SpinnerAdapter;Landroid/app/ActionBar$OnNavigationListener;)V

    .line 1301
    :cond_0
    return-void
.end method

.method private setActivityTheme()V
    .locals 2

    .prologue
    const v1, 0x103012b

    .line 332
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/BackupList;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/scloud/quota/QuotaUtils;->getIsTablet(Landroid/content/res/Configuration;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 335
    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/quota/BackupList;->setTheme(I)V

    .line 349
    :goto_0
    return-void

    .line 339
    :cond_0
    invoke-static {}, Lcom/samsung/android/scloud/quota/QuotaUtils;->isViewType_light()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 341
    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/quota/BackupList;->setTheme(I)V

    goto :goto_0

    .line 346
    :cond_1
    const v0, 0x1030128

    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/quota/BackupList;->setTheme(I)V

    goto :goto_0
.end method

.method private startDeleteThread(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/scloud/quota/BackupDetails;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "DeleteList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/scloud/quota/BackupDetails;>;"
    const/4 v4, 0x0

    .line 1000
    const-string v1, "BackupList"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "QUOTA_5115 : [START]   DeleteList Size :  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1002
    new-instance v0, Lcom/samsung/android/scloud/quota/QuotaThread;

    invoke-direct {v0}, Lcom/samsung/android/scloud/quota/QuotaThread;-><init>()V

    .line 1004
    .local v0, "mQuotaThread":Lcom/samsung/android/scloud/quota/QuotaThread;
    sget-object v1, Lcom/samsung/android/scloud/quota/BackupList;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/quota/QuotaThread;->setContext(Landroid/content/Context;)V

    .line 1006
    invoke-virtual {v0, p0}, Lcom/samsung/android/scloud/quota/QuotaThread;->setListener(Lcom/samsung/android/scloud/quota/QuotaResponseThreadListener;)V

    .line 1008
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/quota/QuotaThread;->setIsBackupDetails(Ljava/lang/Boolean;)V

    .line 1010
    invoke-virtual {v0, p1}, Lcom/samsung/android/scloud/quota/QuotaThread;->setDeleteDeviceList(Ljava/util/ArrayList;)V

    .line 1012
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/quota/QuotaThread;->setISDeleteTrue(Ljava/lang/Boolean;)V

    .line 1014
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList;->mAuthManager:Lcom/samsung/android/scloud/quota/AuthManager;

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/quota/QuotaThread;->setAuthManager(Lcom/samsung/android/scloud/quota/AuthManager;)V

    .line 1016
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList;->mCtid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/quota/QuotaThread;->setCtid(Ljava/lang/String;)V

    .line 1018
    invoke-virtual {v0}, Lcom/samsung/android/scloud/quota/QuotaThread;->start()V

    .line 1020
    sget-object v1, Lcom/samsung/android/scloud/quota/BackupList;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_0

    .line 1024
    sget-object v1, Lcom/samsung/android/scloud/quota/BackupList;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 1026
    sget-object v1, Lcom/samsung/android/scloud/quota/BackupList;->mProgressDialog:Landroid/app/ProgressDialog;

    const v2, 0x7f07004f

    invoke-virtual {p0, v2}, Lcom/samsung/android/scloud/quota/BackupList;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 1030
    sget-object v1, Lcom/samsung/android/scloud/quota/BackupList;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V

    .line 1034
    :cond_0
    return-void
.end method

.method private startQuotaThread()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 968
    const-string v0, "BackupList"

    const-string v1, "QUOTA_5101 : [START]"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 970
    new-instance v0, Lcom/samsung/android/scloud/quota/QuotaThread;

    invoke-direct {v0}, Lcom/samsung/android/scloud/quota/QuotaThread;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mQuotaThread:Lcom/samsung/android/scloud/quota/QuotaThread;

    .line 972
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mQuotaThread:Lcom/samsung/android/scloud/quota/QuotaThread;

    sget-object v1, Lcom/samsung/android/scloud/quota/BackupList;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/quota/QuotaThread;->setContext(Landroid/content/Context;)V

    .line 974
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mQuotaThread:Lcom/samsung/android/scloud/quota/QuotaThread;

    invoke-virtual {v0, p0}, Lcom/samsung/android/scloud/quota/QuotaThread;->setListener(Lcom/samsung/android/scloud/quota/QuotaResponseThreadListener;)V

    .line 976
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mQuotaThread:Lcom/samsung/android/scloud/quota/QuotaThread;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/quota/QuotaThread;->setIsBackupDetails(Ljava/lang/Boolean;)V

    .line 978
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mQuotaThread:Lcom/samsung/android/scloud/quota/QuotaThread;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/quota/QuotaThread;->setIsBackedupSize(Ljava/lang/Boolean;)V

    .line 980
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mQuotaThread:Lcom/samsung/android/scloud/quota/QuotaThread;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/quota/QuotaThread;->setISDeleteTrue(Ljava/lang/Boolean;)V

    .line 982
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mQuotaThread:Lcom/samsung/android/scloud/quota/QuotaThread;

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList;->mAuthManager:Lcom/samsung/android/scloud/quota/AuthManager;

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/quota/QuotaThread;->setAuthManager(Lcom/samsung/android/scloud/quota/AuthManager;)V

    .line 984
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mQuotaThread:Lcom/samsung/android/scloud/quota/QuotaThread;

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList;->mCtid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/quota/QuotaThread;->setCtid(Ljava/lang/String;)V

    .line 986
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mQuotaThread:Lcom/samsung/android/scloud/quota/QuotaThread;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/quota/QuotaThread;->start()V

    .line 988
    sget-object v0, Lcom/samsung/android/scloud/quota/BackupList;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 990
    sget-object v0, Lcom/samsung/android/scloud/quota/BackupList;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 994
    :cond_0
    return-void
.end method


# virtual methods
.method public addDragItemToListArray(Landroid/view/View;I)V
    .locals 2
    .param p1, "currentView"    # Landroid/view/View;
    .param p2, "position"    # I

    .prologue
    .line 307
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mTwDragSelectedItemArray:Ljava/util/ArrayList;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 308
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mTwDragSelectedItemArray:Ljava/util/ArrayList;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, p2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 312
    :goto_0
    return-void

    .line 310
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mTwDragSelectedItemArray:Ljava/util/ArrayList;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 456
    const-string v0, "BackupList"

    const-string v1, "onBackPressed ()"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 457
    iget-boolean v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->flag:Z

    if-eqz v0, :cond_0

    .line 458
    invoke-direct {p0}, Lcom/samsung/android/scloud/quota/BackupList;->backTolist()V

    .line 461
    :goto_0
    return-void

    .line 460
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 318
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 321
    iget-boolean v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->flag:Z

    if-eqz v0, :cond_0

    .line 323
    const-string v0, "BackupList"

    const-string v1, "flag is true"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    invoke-direct {p0}, Lcom/samsung/android/scloud/quota/BackupList;->registerSpinner()V

    .line 326
    :cond_0
    return-void
.end method

.method public onContentChanged()V
    .locals 3

    .prologue
    .line 365
    invoke-super {p0}, Landroid/app/Activity;->onContentChanged()V

    .line 366
    const v2, 0x1020004

    invoke-virtual {p0, v2}, Lcom/samsung/android/scloud/quota/BackupList;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 368
    .local v0, "empty":Landroid/view/View;
    const/4 v1, 0x0

    .line 369
    .local v1, "list":Landroid/widget/ListView;
    const v2, 0x7f0a0008

    invoke-virtual {p0, v2}, Lcom/samsung/android/scloud/quota/BackupList;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .end local v1    # "list":Landroid/widget/ListView;
    check-cast v1, Landroid/widget/ListView;

    .line 370
    .restart local v1    # "list":Landroid/widget/ListView;
    if-eqz v1, :cond_0

    .line 371
    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 373
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 89
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 91
    const-string v0, "BackupList"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/BackupList;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/scloud/quota/BackupList;->mContext:Landroid/content/Context;

    .line 95
    invoke-direct {p0}, Lcom/samsung/android/scloud/quota/BackupList;->setActivityTheme()V

    .line 97
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mTwDragSelectedItemArray:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 98
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mTwDragSelectedItemArray:Ljava/util/ArrayList;

    .line 101
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/BackupList;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 103
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/scloud/quota/BackupList;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 105
    sget-object v0, Lcom/samsung/android/scloud/quota/BackupList;->mProgressDialog:Landroid/app/ProgressDialog;

    const v1, 0x7f07000f

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/quota/BackupList;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 107
    sget-object v0, Lcom/samsung/android/scloud/quota/BackupList;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 109
    sget-object v0, Lcom/samsung/android/scloud/quota/BackupList;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 111
    sget-object v0, Lcom/samsung/android/scloud/quota/BackupList;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 113
    sget-object v0, Lcom/samsung/android/scloud/quota/BackupList;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 115
    sget-object v0, Lcom/samsung/android/scloud/quota/BackupList;->mProgressDialog:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/samsung/android/scloud/quota/BackupList$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/scloud/quota/BackupList$1;-><init>(Lcom/samsung/android/scloud/quota/BackupList;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 127
    invoke-static {p0}, Lcom/samsung/android/scloud/quota/QuotaUtils;->hideStatusBar(Landroid/app/Activity;)V

    .line 128
    invoke-static {}, Lcom/samsung/android/scloud/quota/QuotaUtils;->isViewType_light()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 129
    const v0, 0x7f030006

    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/quota/BackupList;->setContentView(I)V

    .line 132
    :goto_0
    const v0, 0x7f0a0009

    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/quota/BackupList;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->emptyText:Landroid/widget/TextView;

    .line 134
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->backupList:Ljava/util/List;

    .line 135
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mDeleteList:Ljava/util/ArrayList;

    .line 136
    invoke-static {}, Lcom/samsung/android/scloud/quota/QuotaUtils;->generateCTID()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mCtid:Ljava/lang/String;

    .line 137
    const-string v0, "BackupList"

    const-string v1, "call AccessTokenclass to get auth information"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    new-instance v0, Lcom/samsung/android/scloud/quota/BackupList$AccessTokenclass;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/scloud/quota/BackupList$AccessTokenclass;-><init>(Lcom/samsung/android/scloud/quota/BackupList;Lcom/samsung/android/scloud/quota/BackupList$1;)V

    new-array v1, v4, [Ljava/lang/String;

    const-string v2, ""

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/quota/BackupList$AccessTokenclass;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 139
    const v0, 0x7f0a0008

    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/quota/BackupList;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->list:Landroid/widget/ListView;

    .line 143
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->list:Landroid/widget/ListView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 144
    const v0, 0x7f070010

    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/quota/BackupList;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/quota/BackupList;->setTitle(Ljava/lang/CharSequence;)V

    .line 145
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/BackupList;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 146
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/BackupList;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/high16 v1, 0x7f030000

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setCustomView(I)V

    .line 147
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/BackupList;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v0

    const/high16 v1, 0x7f0a0000

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mCheckbox:Landroid/widget/CheckBox;

    .line 149
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mCheckbox:Landroid/widget/CheckBox;

    new-instance v1, Lcom/samsung/android/scloud/quota/BackupList$2;

    invoke-direct {v1, p0}, Lcom/samsung/android/scloud/quota/BackupList$2;-><init>(Lcom/samsung/android/scloud/quota/BackupList;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 164
    new-instance v0, Lcom/samsung/android/scloud/quota/BackupList$BackupListAdapter;

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList;->backupList:Ljava/util/List;

    invoke-direct {v0, p0, p0, v1}, Lcom/samsung/android/scloud/quota/BackupList$BackupListAdapter;-><init>(Lcom/samsung/android/scloud/quota/BackupList;Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->backupListAdapter:Lcom/samsung/android/scloud/quota/BackupList$BackupListAdapter;

    .line 165
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->list:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList;->backupListAdapter:Lcom/samsung/android/scloud/quota/BackupList$BackupListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 166
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->list:Landroid/widget/ListView;

    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/quota/BackupList;->registerForContextMenu(Landroid/view/View;)V

    .line 170
    sget-boolean v0, Lcom/samsung/android/scloud/quota/QuotaConstants$DEVICE;->IS_DEVICE_T:Z

    if-eqz v0, :cond_1

    .line 171
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->list:Landroid/widget/ListView;

    new-instance v1, Lcom/samsung/android/scloud/quota/BackupList$3;

    invoke-direct {v1, p0}, Lcom/samsung/android/scloud/quota/BackupList$3;-><init>(Lcom/samsung/android/scloud/quota/BackupList;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setTwMultiSelectedListener(Landroid/widget/AdapterView$OnTwMultiSelectedListener;)V

    .line 215
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->list:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setLongClickable(Z)V

    .line 216
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->list:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setClickable(Z)V

    .line 218
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->list:Landroid/widget/ListView;

    new-instance v1, Lcom/samsung/android/scloud/quota/BackupList$4;

    invoke-direct {v1, p0}, Lcom/samsung/android/scloud/quota/BackupList$4;-><init>(Lcom/samsung/android/scloud/quota/BackupList;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 269
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->list:Landroid/widget/ListView;

    new-instance v1, Lcom/samsung/android/scloud/quota/BackupList$5;

    invoke-direct {v1, p0}, Lcom/samsung/android/scloud/quota/BackupList$5;-><init>(Lcom/samsung/android/scloud/quota/BackupList;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 301
    return-void

    .line 131
    :cond_2
    const v0, 0x7f030005

    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/quota/BackupList;->setContentView(I)V

    goto/16 :goto_0
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 469
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 471
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f07001f

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/quota/BackupList;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f07001e

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/quota/BackupList;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f070020

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/quota/BackupList;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/scloud/quota/BackupList$7;

    invoke-direct {v2, p0}, Lcom/samsung/android/scloud/quota/BackupList$7;-><init>(Lcom/samsung/android/scloud/quota/BackupList;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000

    new-instance v2, Lcom/samsung/android/scloud/quota/BackupList$6;

    invoke-direct {v2, p0}, Lcom/samsung/android/scloud/quota/BackupList$6;-><init>(Lcom/samsung/android/scloud/quota/BackupList;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 528
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 380
    const-string v0, "BackupList"

    const-string v1, "onCreateOptionsMenu()"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 382
    invoke-direct {p0, p1}, Lcom/samsung/android/scloud/quota/BackupList;->createMenu(Landroid/view/Menu;)V

    .line 383
    const/4 v0, 0x1

    return v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 444
    const-string v0, "BackupList"

    const-string v1, "onDestroy ()"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    invoke-direct {p0}, Lcom/samsung/android/scloud/quota/BackupList;->backPressed()V

    .line 446
    sget-object v0, Lcom/samsung/android/scloud/quota/BackupList;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 447
    sget-object v0, Lcom/samsung/android/scloud/quota/BackupList;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 448
    :cond_0
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/scloud/quota/BackupList;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 449
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 451
    return-void
.end method

.method public onNavigationItemSelected(IJ)Z
    .locals 6
    .param p1, "itemPosition"    # I
    .param p2, "itemId"    # J

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1241
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/BackupList;->list:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getCount()I

    move-result v1

    .line 1243
    .local v1, "len":I
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/BackupList;->navigationAdapter:Lcom/samsung/android/scloud/quota/DropDownNavigationAdapter;

    invoke-virtual {v2, p1}, Lcom/samsung/android/scloud/quota/DropDownNavigationAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    const v3, 0x7f070060

    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/quota/BackupList;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1245
    iput v1, p0, Lcom/samsung/android/scloud/quota/BackupList;->count:I

    .line 1247
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 1249
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/BackupList;->mListSelected:[Z

    aput-boolean v5, v2, v0

    .line 1251
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/BackupList;->list:Landroid/widget/ListView;

    invoke-virtual {v2, v0, v5}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 1247
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1254
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/BackupList;->setDropDownList()V

    .line 1274
    .end local v0    # "i":I
    :cond_1
    :goto_1
    return v5

    .line 1258
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/BackupList;->navigationAdapter:Lcom/samsung/android/scloud/quota/DropDownNavigationAdapter;

    invoke-virtual {v2, p1}, Lcom/samsung/android/scloud/quota/DropDownNavigationAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    const v3, 0x7f070021

    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/quota/BackupList;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1261
    iput v4, p0, Lcom/samsung/android/scloud/quota/BackupList;->count:I

    .line 1263
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_2
    if-ge v0, v1, :cond_3

    .line 1265
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/BackupList;->mListSelected:[Z

    aput-boolean v4, v2, v0

    .line 1267
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/BackupList;->list:Landroid/widget/ListView;

    invoke-virtual {v2, v0, v4}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 1263
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1270
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/BackupList;->setDropDownList()V

    goto :goto_1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 7
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v6, 0x0

    const/4 v0, 0x1

    .line 537
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 571
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 542
    :sswitch_0
    iput-boolean v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->flag:Z

    .line 543
    invoke-direct {p0}, Lcom/samsung/android/scloud/quota/BackupList;->registerSpinner()V

    .line 544
    invoke-direct {p0}, Lcom/samsung/android/scloud/quota/BackupList;->isDeviceUseDropdownMenu()Z

    move-result v1

    if-nez v1, :cond_0

    .line 546
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/BackupList;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 547
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/BackupList;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 548
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/BackupList;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 549
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList;->mCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v1, v6}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 550
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList;->mCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 551
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList;->mCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/BackupList;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070063

    new-array v4, v0, [Ljava/lang/Object;

    iget v5, p0, Lcom/samsung/android/scloud/quota/BackupList;->count:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 553
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/BackupList;->invalidateOptionsMenu()V

    .line 554
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList;->backupListAdapter:Lcom/samsung/android/scloud/quota/BackupList$BackupListAdapter;

    invoke-virtual {v1}, Lcom/samsung/android/scloud/quota/BackupList$BackupListAdapter;->notifyDataSetChanged()V

    .line 555
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList;->list:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/samsung/android/scloud/quota/BackupList;->backupListAdapter:Lcom/samsung/android/scloud/quota/BackupList$BackupListAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 556
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList;->list:Landroid/widget/ListView;

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/quota/BackupList;->registerForContextMenu(Landroid/view/View;)V

    goto :goto_0

    .line 560
    :sswitch_1
    iget-boolean v1, p0, Lcom/samsung/android/scloud/quota/BackupList;->flag:Z

    if-eqz v1, :cond_1

    .line 561
    invoke-direct {p0}, Lcom/samsung/android/scloud/quota/BackupList;->backTolist()V

    goto :goto_0

    .line 563
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/BackupList;->finish()V

    goto :goto_0

    .line 567
    :sswitch_2
    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/quota/BackupList;->showDialog(I)V

    goto :goto_0

    .line 537
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_2
        0x102002c -> :sswitch_1
    .end sparse-switch
.end method

.method public onResponse(Landroid/os/Message;)V
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 744
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 745
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 356
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 357
    const-string v0, "BackupList"

    const-string v1, "OnResume "

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    return-void
.end method

.method public setDropDownList()V
    .locals 9

    .prologue
    const v8, 0x7f070063

    const v7, 0x7f070060

    const v6, 0x7f070021

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1330
    invoke-direct {p0}, Lcom/samsung/android/scloud/quota/BackupList;->isDeviceUseDropdownMenu()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1333
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mDropDownList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1334
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mDropDownList:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/BackupList;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    iget v3, p0, Lcom/samsung/android/scloud/quota/BackupList;->count:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v1, v8, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1335
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->navigationAdapter:Lcom/samsung/android/scloud/quota/DropDownNavigationAdapter;

    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/BackupList;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    iget v3, p0, Lcom/samsung/android/scloud/quota/BackupList;->count:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v1, v8, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Lcom/samsung/android/scloud/quota/DropDownNavigationAdapter;->setItemText(ILjava/lang/String;)V

    .line 1338
    iget v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->count:I

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList;->list:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getCount()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 1339
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mDropDownList:Ljava/util/ArrayList;

    invoke-virtual {p0, v6}, Lcom/samsung/android/scloud/quota/BackupList;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1350
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/BackupList;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    .line 1351
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->navigationAdapter:Lcom/samsung/android/scloud/quota/DropDownNavigationAdapter;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/quota/DropDownNavigationAdapter;->notifyDataSetChanged()V

    .line 1353
    :cond_0
    return-void

    .line 1341
    :cond_1
    iget v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->count:I

    if-nez v0, :cond_2

    .line 1343
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mDropDownList:Ljava/util/ArrayList;

    invoke-virtual {p0, v7}, Lcom/samsung/android/scloud/quota/BackupList;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1347
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mDropDownList:Ljava/util/ArrayList;

    invoke-virtual {p0, v7}, Lcom/samsung/android/scloud/quota/BackupList;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1348
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mDropDownList:Ljava/util/ArrayList;

    invoke-virtual {p0, v6}, Lcom/samsung/android/scloud/quota/BackupList;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public setListItem()V
    .locals 8

    .prologue
    const v7, 0x7f070060

    const v6, 0x7f070021

    .line 1305
    invoke-direct {p0}, Lcom/samsung/android/scloud/quota/BackupList;->isDeviceUseDropdownMenu()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1307
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mDropDownList:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 1308
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mDropDownList:Ljava/util/ArrayList;

    .line 1311
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mDropDownList:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/BackupList;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070063

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, p0, Lcom/samsung/android/scloud/quota/BackupList;->count:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1312
    iget v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->count:I

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList;->list:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getCount()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 1313
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mDropDownList:Ljava/util/ArrayList;

    invoke-virtual {p0, v6}, Lcom/samsung/android/scloud/quota/BackupList;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1325
    :cond_0
    :goto_1
    return-void

    .line 1310
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mDropDownList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    .line 1315
    :cond_2
    iget v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->count:I

    if-nez v0, :cond_3

    .line 1317
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mDropDownList:Ljava/util/ArrayList;

    invoke-virtual {p0, v7}, Lcom/samsung/android/scloud/quota/BackupList;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1321
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mDropDownList:Ljava/util/ArrayList;

    invoke-virtual {p0, v7}, Lcom/samsung/android/scloud/quota/BackupList;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1322
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/BackupList;->mDropDownList:Ljava/util/ArrayList;

    invoke-virtual {p0, v6}, Lcom/samsung/android/scloud/quota/BackupList;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method
