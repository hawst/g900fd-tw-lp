.class Lcom/samsung/android/scloud/quota/BackupList$8;
.super Landroid/os/Handler;
.source "BackupList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/quota/BackupList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/quota/BackupList;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/quota/BackupList;)V
    .locals 0

    .prologue
    .line 749
    iput-object p1, p0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 18
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 755
    move-object/from16 v0, p1

    iget v2, v0, Landroid/os/Message;->what:I

    sparse-switch v2, :sswitch_data_0

    .line 960
    :cond_0
    :goto_0
    return-void

    .line 759
    :sswitch_0
    const-string v2, "BackupList"

    const-string v6, "Handler, BNR details"

    invoke-static {v2, v6}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 761
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v6

    const-string v13, "BACKUP_DETAILS"

    invoke-virtual {v6, v13}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    iput-object v6, v2, Lcom/samsung/android/scloud/quota/BackupList;->mBackupDetaiList:Ljava/util/ArrayList;

    .line 765
    const-string v2, "BackupList"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Total Size : "

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v13, v13, Lcom/samsung/android/scloud/quota/BackupList;->mBackupDetaiList:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 766
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v2, v2, Lcom/samsung/android/scloud/quota/BackupList;->mBackupDetaiList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 768
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v2, v2, Lcom/samsung/android/scloud/quota/BackupList;->list:Landroid/widget/ListView;

    const/16 v6, 0x8

    invoke-virtual {v2, v6}, Landroid/widget/ListView;->setVisibility(I)V

    .line 769
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v2, v2, Lcom/samsung/android/scloud/quota/BackupList;->emptyText:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    const v13, 0x7f070038

    invoke-virtual {v6, v13}, Lcom/samsung/android/scloud/quota/BackupList;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 773
    :cond_1
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v2, v2, Lcom/samsung/android/scloud/quota/BackupList;->mBackupDetaiList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v10, v2, :cond_3

    .line 775
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v2, v2, Lcom/samsung/android/scloud/quota/BackupList;->mBackupDetaiList:Ljava/util/ArrayList;

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/scloud/quota/BackupDetailsList;

    invoke-virtual {v2}, Lcom/samsung/android/scloud/quota/BackupDetailsList;->getDeviceId()Ljava/lang/String;

    move-result-object v4

    .line 777
    .local v4, "Deviceid":Ljava/lang/String;
    const-string v2, "BackupList"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "DID : "

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 779
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v2, v2, Lcom/samsung/android/scloud/quota/BackupList;->mBackupDetaiList:Ljava/util/ArrayList;

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/scloud/quota/BackupDetailsList;

    invoke-virtual {v2}, Lcom/samsung/android/scloud/quota/BackupDetailsList;->getClientDeviceId()Ljava/lang/String;

    move-result-object v3

    .line 783
    .local v3, "CDeviceId":Ljava/lang/String;
    const-string v2, "BackupList"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "CDID : "

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 785
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v2, v2, Lcom/samsung/android/scloud/quota/BackupList;->mBackupDetaiList:Ljava/util/ArrayList;

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/scloud/quota/BackupDetailsList;

    invoke-virtual {v2}, Lcom/samsung/android/scloud/quota/BackupDetailsList;->getBackupSize()Ljava/lang/Long;

    move-result-object v9

    .line 787
    .local v9, "Usage":Ljava/lang/Long;
    const-string v2, "BackupList"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Usage : "

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 789
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v2, v2, Lcom/samsung/android/scloud/quota/BackupList;->mBackupDetaiList:Ljava/util/ArrayList;

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/scloud/quota/BackupDetailsList;

    invoke-virtual {v2}, Lcom/samsung/android/scloud/quota/BackupDetailsList;->getLastBackupTime()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    # invokes: Lcom/samsung/android/scloud/quota/BackupList;->getDateStringFromTimeSpan(J)Ljava/lang/String;
    invoke-static {v6, v14, v15}, Lcom/samsung/android/scloud/quota/BackupList;->access$800(Lcom/samsung/android/scloud/quota/BackupList;J)Ljava/lang/String;

    move-result-object v5

    .line 793
    .local v5, "lastBackupTime":Ljava/lang/String;
    const-string v2, "BackupList"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, " LAstBackupTime : "

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 795
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v2, v2, Lcom/samsung/android/scloud/quota/BackupList;->mBackupDetaiList:Ljava/util/ArrayList;

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/scloud/quota/BackupDetailsList;

    invoke-virtual {v2}, Lcom/samsung/android/scloud/quota/BackupDetailsList;->getModelNumber()Ljava/lang/String;

    move-result-object v7

    .line 799
    .local v7, "modelNumber":Ljava/lang/String;
    const-string v2, "BackupList"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, " LAstBackupTime : "

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 803
    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    const-wide/16 v16, 0x0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    cmp-long v2, v14, v16

    if-lez v2, :cond_2

    .line 805
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v13, v2, Lcom/samsung/android/scloud/quota/BackupList;->backupList:Ljava/util/List;

    new-instance v2, Lcom/samsung/android/scloud/quota/BackupDetails;

    sget-object v6, Lcom/samsung/android/scloud/quota/BackupList;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    invoke-static {v6, v14, v15}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v2 .. v7}, Lcom/samsung/android/scloud/quota/BackupDetails;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v13, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 773
    :cond_2
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_1

    .line 815
    .end local v3    # "CDeviceId":Ljava/lang/String;
    .end local v4    # "Deviceid":Ljava/lang/String;
    .end local v5    # "lastBackupTime":Ljava/lang/String;
    .end local v7    # "modelNumber":Ljava/lang/String;
    .end local v9    # "Usage":Ljava/lang/Long;
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v6, v6, Lcom/samsung/android/scloud/quota/BackupList;->backupList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    new-array v6, v6, [Z

    iput-object v6, v2, Lcom/samsung/android/scloud/quota/BackupList;->mListSelected:[Z

    .line 817
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v2, v2, Lcom/samsung/android/scloud/quota/BackupList;->backupListAdapter:Lcom/samsung/android/scloud/quota/BackupList$BackupListAdapter;

    invoke-virtual {v2}, Lcom/samsung/android/scloud/quota/BackupList$BackupListAdapter;->notifyDataSetChanged()V

    .line 819
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v2, v2, Lcom/samsung/android/scloud/quota/BackupList;->list:Landroid/widget/ListView;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v6, v6, Lcom/samsung/android/scloud/quota/BackupList;->backupListAdapter:Lcom/samsung/android/scloud/quota/BackupList$BackupListAdapter;

    invoke-virtual {v2, v6}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 821
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v6, v6, Lcom/samsung/android/scloud/quota/BackupList;->list:Landroid/widget/ListView;

    invoke-virtual {v2, v6}, Lcom/samsung/android/scloud/quota/BackupList;->registerForContextMenu(Landroid/view/View;)V

    .line 823
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v2, v2, Lcom/samsung/android/scloud/quota/BackupList;->list:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getCount()I

    move-result v2

    if-lez v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v2, v2, Lcom/samsung/android/scloud/quota/BackupList;->mnu1:Landroid/view/MenuItem;

    if-eqz v2, :cond_4

    .line 826
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v2, v2, Lcom/samsung/android/scloud/quota/BackupList;->mnu1:Landroid/view/MenuItem;

    const/4 v6, 0x1

    invoke-interface {v2, v6}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 829
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    invoke-virtual {v2}, Lcom/samsung/android/scloud/quota/BackupList;->invalidateOptionsMenu()V

    .line 831
    # getter for: Lcom/samsung/android/scloud/quota/BackupList;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {}, Lcom/samsung/android/scloud/quota/BackupList;->access$900()Landroid/app/ProgressDialog;

    move-result-object v2

    if-eqz v2, :cond_0

    # getter for: Lcom/samsung/android/scloud/quota/BackupList;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {}, Lcom/samsung/android/scloud/quota/BackupList;->access$900()Landroid/app/ProgressDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 833
    # getter for: Lcom/samsung/android/scloud/quota/BackupList;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {}, Lcom/samsung/android/scloud/quota/BackupList;->access$900()Landroid/app/ProgressDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->dismiss()V

    goto/16 :goto_0

    .line 839
    .end local v10    # "i":I
    :sswitch_1
    const-string v2, "BackupList"

    const-string v6, "Handler, Quota End Message"

    invoke-static {v2, v6}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 841
    # getter for: Lcom/samsung/android/scloud/quota/BackupList;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {}, Lcom/samsung/android/scloud/quota/BackupList;->access$900()Landroid/app/ProgressDialog;

    move-result-object v2

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    # getter for: Lcom/samsung/android/scloud/quota/BackupList;->mDialogFlag:Z
    invoke-static {v2}, Lcom/samsung/android/scloud/quota/BackupList;->access$1000(Lcom/samsung/android/scloud/quota/BackupList;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 843
    # getter for: Lcom/samsung/android/scloud/quota/BackupList;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {}, Lcom/samsung/android/scloud/quota/BackupList;->access$900()Landroid/app/ProgressDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->dismiss()V

    .line 847
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    const/4 v6, 0x0

    # setter for: Lcom/samsung/android/scloud/quota/BackupList;->mDialogFlag:Z
    invoke-static {v2, v6}, Lcom/samsung/android/scloud/quota/BackupList;->access$1002(Lcom/samsung/android/scloud/quota/BackupList;Z)Z

    goto/16 :goto_0

    .line 853
    :sswitch_2
    const-string v2, "BackupList"

    const-string v6, "Handler, Delete End"

    invoke-static {v2, v6}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 855
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    .line 857
    .local v8, "Status":Ljava/lang/Boolean;
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    .line 859
    .local v11, "isFailed":Ljava/lang/Boolean;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v6

    const-string v13, "DELETE_LIST"

    invoke-virtual {v6, v13}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    iput-object v6, v2, Lcom/samsung/android/scloud/quota/BackupList;->mDeleteList:Ljava/util/ArrayList;

    .line 863
    const-string v2, "BackupList"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Deleted Response, Size of list is : "

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v13, v13, Lcom/samsung/android/scloud/quota/BackupList;->mDeleteList:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 869
    const/4 v10, 0x0

    .restart local v10    # "i":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v2, v2, Lcom/samsung/android/scloud/quota/BackupList;->mDeleteList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v10, v2, :cond_9

    .line 871
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v2, v2, Lcom/samsung/android/scloud/quota/BackupList;->mDeleteList:Ljava/util/ArrayList;

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/scloud/quota/BackupDetails;

    invoke-virtual {v2}, Lcom/samsung/android/scloud/quota/BackupDetails;->getFlag()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 873
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    .line 875
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v2, v2, Lcom/samsung/android/scloud/quota/BackupList;->backupList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v12, v2, -0x1

    .local v12, "j":I
    :goto_3
    if-ltz v12, :cond_8

    .line 877
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v2, v2, Lcom/samsung/android/scloud/quota/BackupList;->mDeleteList:Ljava/util/ArrayList;

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/scloud/quota/BackupDetails;

    invoke-virtual {v2}, Lcom/samsung/android/scloud/quota/BackupDetails;->deviceName()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v2, v2, Lcom/samsung/android/scloud/quota/BackupList;->backupList:Ljava/util/List;

    invoke-interface {v2, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/scloud/quota/BackupDetails;

    invoke-virtual {v2}, Lcom/samsung/android/scloud/quota/BackupDetails;->deviceName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 881
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v2, v2, Lcom/samsung/android/scloud/quota/BackupList;->backupList:Ljava/util/List;

    invoke-interface {v2, v12}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 875
    :cond_6
    add-int/lit8 v12, v12, -0x1

    goto :goto_3

    .line 887
    .end local v12    # "j":I
    :cond_7
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    .line 869
    :cond_8
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 891
    :cond_9
    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 893
    const-string v2, "BackupList"

    const-string v6, "At least 1 item from the selected list is deleted"

    invoke-static {v2, v6}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 897
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v2, v2, Lcom/samsung/android/scloud/quota/BackupList;->mListSelected:[Z

    if-eqz v2, :cond_a

    .line 899
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    const/4 v6, 0x0

    iput-object v6, v2, Lcom/samsung/android/scloud/quota/BackupList;->mListSelected:[Z

    .line 901
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v2, v2, Lcom/samsung/android/scloud/quota/BackupList;->mCheckbox:Landroid/widget/CheckBox;

    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 903
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v6, v6, Lcom/samsung/android/scloud/quota/BackupList;->backupList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    new-array v6, v6, [Z

    iput-object v6, v2, Lcom/samsung/android/scloud/quota/BackupList;->mListSelected:[Z

    .line 905
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v2, v2, Lcom/samsung/android/scloud/quota/BackupList;->backupListAdapter:Lcom/samsung/android/scloud/quota/BackupList$BackupListAdapter;

    invoke-virtual {v2}, Lcom/samsung/android/scloud/quota/BackupList$BackupListAdapter;->notifyDataSetChanged()V

    .line 907
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v2, v2, Lcom/samsung/android/scloud/quota/BackupList;->list:Landroid/widget/ListView;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v6, v6, Lcom/samsung/android/scloud/quota/BackupList;->backupListAdapter:Lcom/samsung/android/scloud/quota/BackupList$BackupListAdapter;

    invoke-virtual {v2, v6}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 909
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    const/4 v6, 0x0

    # setter for: Lcom/samsung/android/scloud/quota/BackupList;->count:I
    invoke-static {v2, v6}, Lcom/samsung/android/scloud/quota/BackupList;->access$402(Lcom/samsung/android/scloud/quota/BackupList;I)I

    .line 911
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v2, v2, Lcom/samsung/android/scloud/quota/BackupList;->list:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getCount()I

    move-result v2

    if-nez v2, :cond_b

    .line 913
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v2, v2, Lcom/samsung/android/scloud/quota/BackupList;->mCheckbox:Landroid/widget/CheckBox;

    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 915
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v2, v2, Lcom/samsung/android/scloud/quota/BackupList;->list:Landroid/widget/ListView;

    const/16 v6, 0x8

    invoke-virtual {v2, v6}, Landroid/widget/ListView;->setVisibility(I)V

    .line 917
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v2, v2, Lcom/samsung/android/scloud/quota/BackupList;->emptyText:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    const v13, 0x7f070038

    invoke-virtual {v6, v13}, Lcom/samsung/android/scloud/quota/BackupList;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 922
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    invoke-virtual {v2}, Lcom/samsung/android/scloud/quota/BackupList;->invalidateOptionsMenu()V

    .line 926
    :cond_c
    # getter for: Lcom/samsung/android/scloud/quota/BackupList;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {}, Lcom/samsung/android/scloud/quota/BackupList;->access$900()Landroid/app/ProgressDialog;

    move-result-object v2

    if-eqz v2, :cond_d

    # getter for: Lcom/samsung/android/scloud/quota/BackupList;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {}, Lcom/samsung/android/scloud/quota/BackupList;->access$900()Landroid/app/ProgressDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 928
    # getter for: Lcom/samsung/android/scloud/quota/BackupList;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {}, Lcom/samsung/android/scloud/quota/BackupList;->access$900()Landroid/app/ProgressDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->dismiss()V

    .line 930
    :cond_d
    invoke-virtual {v11}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_0

    .line 934
    const-string v2, "BackupList"

    const-string v6, "Failed to delete at least 1 item from the selected list"

    invoke-static {v2, v6}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 936
    sget-object v2, Lcom/samsung/android/scloud/quota/BackupList;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    const v13, 0x7f07000c

    invoke-virtual {v6, v13}, Lcom/samsung/android/scloud/quota/BackupList;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v13, 0x1

    invoke-static {v2, v6, v13}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 946
    .end local v8    # "Status":Ljava/lang/Boolean;
    .end local v10    # "i":I
    .end local v11    # "isFailed":Ljava/lang/Boolean;
    :sswitch_3
    const-string v2, "BackupList"

    const-string v6, "Bad Access Token"

    invoke-static {v2, v6}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 948
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    const/4 v6, 0x1

    # setter for: Lcom/samsung/android/scloud/quota/BackupList;->mDialogFlag:Z
    invoke-static {v2, v6}, Lcom/samsung/android/scloud/quota/BackupList;->access$1002(Lcom/samsung/android/scloud/quota/BackupList;Z)Z

    .line 950
    new-instance v2, Lcom/samsung/android/scloud/quota/BackupList$AccessTokenclass;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/scloud/quota/BackupList$8;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    const/4 v13, 0x0

    invoke-direct {v2, v6, v13}, Lcom/samsung/android/scloud/quota/BackupList$AccessTokenclass;-><init>(Lcom/samsung/android/scloud/quota/BackupList;Lcom/samsung/android/scloud/quota/BackupList$1;)V

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v13, 0x0

    const-string v14, ""

    aput-object v14, v6, v13

    invoke-virtual {v2, v6}, Lcom/samsung/android/scloud/quota/BackupList$AccessTokenclass;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 755
    :sswitch_data_0
    .sparse-switch
        -0x3 -> :sswitch_3
        0x0 -> :sswitch_1
        0x5 -> :sswitch_0
        0x6 -> :sswitch_2
    .end sparse-switch
.end method
