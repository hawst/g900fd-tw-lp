.class public Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;
.super Ljava/lang/Object;
.source "QuotaServiceManager.java"


# static fields
.field private static final RCODE:Ljava/lang/String; = "rcode"

.field private static final TAG:Ljava/lang/String; = "QuotaServiceManager"

.field private static mApiResult:Ljava/lang/String;

.field private static mLastApiCode:Ljava/lang/String;

.field private static mSuccess:Z


# instance fields
.field private mAuthManager:Lcom/samsung/android/scloud/quota/AuthManager;

.field private mBaseUrl:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mKVSResponse:Lcom/samsung/android/scloud/quota/response/KVSResponse;

.field private mNetClient:Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;

.field private mUserParamStr:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mLastApiCode:Ljava/lang/String;

    .line 57
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mSuccess:Z

    .line 59
    const-string v0, "0"

    sput-object v0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mApiResult:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/scloud/quota/AuthManager;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "authManager"    # Lcom/samsung/android/scloud/quota/AuthManager;

    .prologue
    const/4 v1, 0x0

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    new-instance v0, Lcom/samsung/android/scloud/quota/response/KVSResponse;

    invoke-direct {v0}, Lcom/samsung/android/scloud/quota/response/KVSResponse;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mKVSResponse:Lcom/samsung/android/scloud/quota/response/KVSResponse;

    .line 46
    iput-object v1, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mNetClient:Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;

    .line 48
    iput-object v1, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mBaseUrl:Ljava/lang/String;

    .line 49
    iput-object v1, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mUserParamStr:Ljava/lang/String;

    .line 67
    invoke-virtual {p2}, Lcom/samsung/android/scloud/quota/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mBaseUrl:Ljava/lang/String;

    .line 69
    invoke-virtual {p2}, Lcom/samsung/android/scloud/quota/AuthManager;->getGetApiParamsWithoutCid()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mUserParamStr:Ljava/lang/String;

    .line 71
    new-instance v0, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;

    invoke-direct {v0, p1}, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mNetClient:Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;

    .line 72
    iput-object p2, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mAuthManager:Lcom/samsung/android/scloud/quota/AuthManager;

    .line 74
    iput-object p1, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mContext:Landroid/content/Context;

    .line 75
    return-void
.end method

.method private createMetaResponse(Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;Lcom/samsung/android/scloud/quota/response/KVSResponse;)V
    .locals 3
    .param p1, "networkResponse"    # Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;
    .param p2, "metaResponse"    # Lcom/samsung/android/scloud/quota/response/KVSResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/scloud/quota/QuotaException;
        }
    .end annotation

    .prologue
    .line 439
    invoke-virtual {p2}, Lcom/samsung/android/scloud/quota/response/KVSResponse;->clear()V

    .line 443
    :try_start_0
    invoke-virtual {p1}, Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;->getBody()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 445
    invoke-virtual {p1}, Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/samsung/android/scloud/quota/response/KVSResponse;->fromJSON(Ljava/lang/Object;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    .line 461
    :cond_0
    return-void

    .line 447
    :catch_0
    move-exception v0

    .line 449
    .local v0, "e":Lorg/json/JSONException;
    const-string v1, "QuotaServiceManager"

    const-string v2, "JSON PARSER Exception"

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 451
    new-instance v1, Lcom/samsung/android/scloud/quota/QuotaException;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Lcom/samsung/android/scloud/quota/QuotaException;-><init>(I)V

    throw v1

    .line 455
    .end local v0    # "e":Lorg/json/JSONException;
    :catch_1
    move-exception v0

    .line 457
    .local v0, "e":Ljava/lang/OutOfMemoryError;
    new-instance v1, Lcom/samsung/android/scloud/quota/QuotaException;

    const/4 v2, 0x4

    invoke-direct {v1, v2}, Lcom/samsung/android/scloud/quota/QuotaException;-><init>(I)V

    throw v1
.end method

.method private handleRequest(ILjava/lang/String;Lcom/samsung/android/scloud/quota/response/KVSResponse;)Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;
    .locals 6
    .param p1, "requestType"    # I
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "responseMeta"    # Lcom/samsung/android/scloud/quota/response/KVSResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/scloud/quota/QuotaException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 348
    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->handleRequest(ILjava/lang/String;Lcom/samsung/android/scloud/quota/response/KVSResponse;Ljava/lang/String;Lorg/apache/http/entity/mime/MultipartEntity;)Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;

    move-result-object v0

    return-object v0
.end method

.method private handleRequest(ILjava/lang/String;Lcom/samsung/android/scloud/quota/response/KVSResponse;Ljava/lang/String;Lorg/apache/http/entity/mime/MultipartEntity;)Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;
    .locals 10
    .param p1, "requestType"    # I
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "responseMeta"    # Lcom/samsung/android/scloud/quota/response/KVSResponse;
    .param p4, "Json"    # Ljava/lang/String;
    .param p5, "multipart"    # Lorg/apache/http/entity/mime/MultipartEntity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/scloud/quota/QuotaException;
        }
    .end annotation

    .prologue
    .line 368
    const/4 v2, 0x0

    .line 369
    .local v2, "networkResponse":Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;
    const-string v6, "QuotaServiceManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "URL : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/scloud/quota/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    iget-object v7, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mNetClient:Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;

    monitor-enter v7

    .line 373
    packed-switch p1, :pswitch_data_0

    .line 385
    :try_start_0
    const-string v6, "QuotaServiceManager"

    const-string v8, "Wrong Request."

    invoke-static {v6, v8}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    new-instance v6, Lcom/samsung/android/scloud/quota/QuotaException;

    const/4 v8, 0x5

    invoke-direct {v6, v8}, Lcom/samsung/android/scloud/quota/QuotaException;-><init>(I)V

    throw v6
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 390
    :catch_0
    move-exception v0

    .line 391
    .local v0, "e":Ljava/io/IOException;
    :try_start_1
    const-string v6, "QuotaServiceManager"

    const-string v8, "IOException occured while Http Request"

    invoke-static {v6, v8}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    new-instance v6, Lcom/samsung/android/scloud/quota/QuotaException;

    const/4 v8, 0x4

    invoke-direct {v6, v8}, Lcom/samsung/android/scloud/quota/QuotaException;-><init>(I)V

    throw v6

    .line 429
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v6

    .line 376
    :pswitch_0
    if-eqz p4, :cond_0

    .line 377
    :try_start_2
    const-string v6, "QuotaServiceManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Getting Headers : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Lcom/samsung/android/scloud/quota/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    :cond_0
    iget-object v6, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mNetClient:Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;

    invoke-virtual {v6, p2, p4}, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->post(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    .line 394
    :goto_0
    if-nez v2, :cond_1

    .line 396
    :try_start_3
    const-string v6, "QuotaServiceManager"

    const-string v8, "There is no Network Response."

    invoke-static {v6, v8}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 397
    new-instance v6, Lcom/samsung/android/scloud/quota/QuotaException;

    const/4 v8, 0x7

    invoke-direct {v6, v8}, Lcom/samsung/android/scloud/quota/QuotaException;-><init>(I)V

    throw v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 382
    :pswitch_1
    :try_start_4
    iget-object v6, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mNetClient:Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;

    invoke-virtual {v6, p2}, Lcom/samsung/android/scloud/quota/networkutils/NetworkClient;->get(Ljava/lang/String;)Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v2

    .line 383
    goto :goto_0

    .line 399
    :cond_1
    :try_start_5
    invoke-virtual {v2}, Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;->getBody()Ljava/lang/String;

    move-result-object v4

    .line 400
    .local v4, "responseBody":Ljava/lang/String;
    const-string v6, "QuotaServiceManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Response : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    invoke-virtual {v2}, Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;->getStatus()I

    move-result v5

    .line 402
    .local v5, "status":I
    sparse-switch v5, :sswitch_data_0

    .line 426
    const-string v6, "QuotaServiceManager"

    const-string v8, "HTTPEXCEPTION"

    invoke-static {v6, v8}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 427
    new-instance v6, Lcom/samsung/android/scloud/quota/QuotaException;

    const/4 v8, 0x1

    invoke-direct {v6, v8}, Lcom/samsung/android/scloud/quota/QuotaException;-><init>(I)V

    throw v6

    .line 404
    :sswitch_0
    const-string v6, "QuotaServiceManager"

    const-string v8, "REQUEST OK"

    invoke-static {v6, v8}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 429
    :cond_2
    monitor-exit v7
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 430
    invoke-direct {p0, v2, p3}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->createMetaResponse(Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;Lcom/samsung/android/scloud/quota/response/KVSResponse;)V

    .line 431
    return-object v2

    .line 407
    :sswitch_1
    :try_start_6
    const-string v6, "QuotaServiceManager"

    const-string v8, "BAD REQUEST"

    invoke-static {v6, v8}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 408
    if-eqz v4, :cond_2

    .line 410
    :try_start_7
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 411
    .local v1, "json":Lorg/json/JSONObject;
    const-string v6, "rcode"

    invoke-virtual {v1, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 412
    const-string v6, "rcode"

    invoke-virtual {v1, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 413
    .local v3, "rcode":I
    const-string v6, "QuotaServiceManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "RCODE is :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    const/16 v6, 0x4a40

    if-ne v6, v3, :cond_2

    .line 415
    const-string v6, "QuotaServiceManager"

    const-string v8, "BAD_ACCESS_TOKEN"

    invoke-static {v6, v8}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    new-instance v6, Lcom/samsung/android/scloud/quota/QuotaException;

    const/4 v8, 0x6

    invoke-direct {v6, v8}, Lcom/samsung/android/scloud/quota/QuotaException;-><init>(I)V

    throw v6
    :try_end_7
    .catch Lorg/json/JSONException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 419
    .end local v1    # "json":Lorg/json/JSONObject;
    .end local v3    # "rcode":I
    :catch_1
    move-exception v0

    .line 420
    .local v0, "e":Lorg/json/JSONException;
    :try_start_8
    const-string v6, "QuotaServiceManager"

    const-string v8, "JSON PARSER Exception"

    invoke-static {v6, v8}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    new-instance v6, Lcom/samsung/android/scloud/quota/QuotaException;

    const/4 v8, 0x2

    invoke-direct {v6, v8}, Lcom/samsung/android/scloud/quota/QuotaException;-><init>(I)V

    throw v6
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 373
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 402
    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_0
        0x190 -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public declared-synchronized deleteBackupFromServer(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/scloud/quota/response/KVSResponse;
    .locals 5
    .param p1, "did"    # Ljava/lang/String;
    .param p2, "cDid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/scloud/quota/QuotaException;
        }
    .end annotation

    .prologue
    .line 203
    monitor-enter p0

    :try_start_0
    const-string v1, "QuotaServiceManager"

    const-string v2, "QUOTA_5104 : [START]"

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    const-string v1, "QuotaServiceManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Client Device id = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " DID ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mBaseUrl:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 208
    .local v0, "url":Ljava/lang/StringBuilder;
    const-string v1, "/bnr/?action="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209
    const-string v1, "clearall"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 211
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mUserParamStr:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 213
    const-string v1, "&did="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 214
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 216
    const-string v1, "&client_did="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 217
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 219
    const-string v1, "621"

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->setLastApi(Ljava/lang/String;)V

    .line 221
    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mKVSResponse:Lcom/samsung/android/scloud/quota/response/KVSResponse;

    const/4 v4, 0x0

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->handleRequest(ILjava/lang/String;Lcom/samsung/android/scloud/quota/response/KVSResponse;Ljava/lang/String;)Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;

    .line 222
    const-string v1, "QuotaServiceManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "QUOTA_5104 RESPONSE CODE : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mKVSResponse:Lcom/samsung/android/scloud/quota/response/KVSResponse;

    invoke-virtual {v3}, Lcom/samsung/android/scloud/quota/response/KVSResponse;->getResponseCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mKVSResponse:Lcom/samsung/android/scloud/quota/response/KVSResponse;

    invoke-virtual {v1}, Lcom/samsung/android/scloud/quota/response/KVSResponse;->getResponseCode()I

    move-result v1

    const/16 v2, 0x4e28

    if-ne v1, v2, :cond_0

    .line 225
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->deleteBackupFromServer(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/scloud/quota/response/KVSResponse;

    .line 227
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mKVSResponse:Lcom/samsung/android/scloud/quota/response/KVSResponse;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v1

    .line 203
    .end local v0    # "url":Ljava/lang/StringBuilder;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized getAllDetailUsage()Lcom/samsung/android/scloud/quota/response/KVSResponse;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/scloud/quota/QuotaException;
        }
    .end annotation

    .prologue
    .line 181
    monitor-enter p0

    :try_start_0
    const-string v1, "QuotaServiceManager"

    const-string v2, "QUOTA_5110 : [START]"

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mBaseUrl:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 183
    .local v0, "url":Ljava/lang/StringBuilder;
    const-string v1, "/user/quota"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 184
    const-string v1, "?view=all"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mUserParamStr:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 188
    const-string v1, "715"

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->setLastApi(Ljava/lang/String;)V

    .line 190
    const/4 v1, 0x1

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mKVSResponse:Lcom/samsung/android/scloud/quota/response/KVSResponse;

    invoke-direct {p0, v1, v2, v3}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->handleRequest(ILjava/lang/String;Lcom/samsung/android/scloud/quota/response/KVSResponse;)Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;

    .line 191
    const-string v1, "QuotaServiceManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "QUOTA_5110 RESPONSE CODE : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mKVSResponse:Lcom/samsung/android/scloud/quota/response/KVSResponse;

    invoke-virtual {v3}, Lcom/samsung/android/scloud/quota/response/KVSResponse;->getResponseCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mKVSResponse:Lcom/samsung/android/scloud/quota/response/KVSResponse;

    invoke-virtual {v1}, Lcom/samsung/android/scloud/quota/response/KVSResponse;->getResponseCode()I

    move-result v1

    if-eqz v1, :cond_0

    .line 193
    new-instance v1, Lcom/samsung/android/scloud/quota/QuotaException;

    const/16 v2, 0x9

    invoke-direct {v1, v2}, Lcom/samsung/android/scloud/quota/QuotaException;-><init>(I)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 181
    .end local v0    # "url":Ljava/lang/StringBuilder;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 196
    .restart local v0    # "url":Ljava/lang/StringBuilder;
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mKVSResponse:Lcom/samsung/android/scloud/quota/response/KVSResponse;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v1
.end method

.method public getApiResult()Ljava/lang/String;
    .locals 1

    .prologue
    .line 257
    sget-object v0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mApiResult:Ljava/lang/String;

    return-object v0
.end method

.method public getApiSuccess()Z
    .locals 1

    .prologue
    .line 244
    sget-boolean v0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mSuccess:Z

    return v0
.end method

.method public declared-synchronized getBackupDetails()Lcom/samsung/android/scloud/quota/response/KVSResponse;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/scloud/quota/QuotaException;
        }
    .end annotation

    .prologue
    .line 324
    monitor-enter p0

    :try_start_0
    const-string v1, "QuotaServiceManager"

    const-string v2, "QUOTA_5111 : [START]"

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mBaseUrl:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 328
    .local v0, "url":Ljava/lang/StringBuilder;
    const-string v1, "/bnr/details?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 332
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mUserParamStr:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 334
    const-string v1, "615"

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->setLastApi(Ljava/lang/String;)V

    .line 337
    const/4 v1, 0x1

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mKVSResponse:Lcom/samsung/android/scloud/quota/response/KVSResponse;

    invoke-direct {p0, v1, v2, v3}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->handleRequest(ILjava/lang/String;Lcom/samsung/android/scloud/quota/response/KVSResponse;)Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;

    .line 340
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mKVSResponse:Lcom/samsung/android/scloud/quota/response/KVSResponse;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v1

    .line 324
    .end local v0    # "url":Ljava/lang/StringBuilder;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public getLastApi()Ljava/lang/String;
    .locals 1

    .prologue
    .line 231
    sget-object v0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mLastApiCode:Ljava/lang/String;

    return-object v0
.end method

.method public declared-synchronized getTotalUsage()Lcom/samsung/android/scloud/quota/response/KVSResponse;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/scloud/quota/QuotaException;
        }
    .end annotation

    .prologue
    .line 79
    monitor-enter p0

    :try_start_0
    const-string v1, "QuotaServiceManager"

    const-string v2, "QUOTA_5105 : [START]"

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mBaseUrl:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 81
    .local v0, "url":Ljava/lang/StringBuilder;
    const-string v1, "/user/quota"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    const-string v1, "?view=total"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mUserParamStr:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    const-string v1, "710"

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->setLastApi(Ljava/lang/String;)V

    .line 88
    const/4 v1, 0x1

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mKVSResponse:Lcom/samsung/android/scloud/quota/response/KVSResponse;

    invoke-direct {p0, v1, v2, v3}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->handleRequest(ILjava/lang/String;Lcom/samsung/android/scloud/quota/response/KVSResponse;)Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;

    .line 90
    const-string v1, "QuotaServiceManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "QUOTA_5105 RESPONSE CODE : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mKVSResponse:Lcom/samsung/android/scloud/quota/response/KVSResponse;

    invoke-virtual {v3}, Lcom/samsung/android/scloud/quota/response/KVSResponse;->getResponseCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mKVSResponse:Lcom/samsung/android/scloud/quota/response/KVSResponse;

    invoke-virtual {v1}, Lcom/samsung/android/scloud/quota/response/KVSResponse;->getResponseCode()I

    move-result v1

    if-eqz v1, :cond_0

    .line 92
    new-instance v1, Lcom/samsung/android/scloud/quota/QuotaException;

    const/16 v2, 0x9

    invoke-direct {v1, v2}, Lcom/samsung/android/scloud/quota/QuotaException;-><init>(I)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 79
    .end local v0    # "url":Ljava/lang/StringBuilder;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 95
    .restart local v0    # "url":Ljava/lang/StringBuilder;
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mKVSResponse:Lcom/samsung/android/scloud/quota/response/KVSResponse;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v1
.end method

.method public declared-synchronized getTotalUsageByApp()Lcom/samsung/android/scloud/quota/response/KVSResponse;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/scloud/quota/QuotaException;
        }
    .end annotation

    .prologue
    .line 100
    monitor-enter p0

    :try_start_0
    const-string v1, "QuotaServiceManager"

    const-string v2, "QUOTA_5106 : [START]"

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mBaseUrl:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 102
    .local v0, "url":Ljava/lang/StringBuilder;
    const-string v1, "/user/quota"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    const-string v1, "?view=appid"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mUserParamStr:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    const-string v1, "QuotaServiceManager"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    const-string v1, "711"

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->setLastApi(Ljava/lang/String;)V

    .line 108
    const/4 v1, 0x1

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mKVSResponse:Lcom/samsung/android/scloud/quota/response/KVSResponse;

    invoke-direct {p0, v1, v2, v3}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->handleRequest(ILjava/lang/String;Lcom/samsung/android/scloud/quota/response/KVSResponse;)Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;

    .line 109
    const-string v1, "QuotaServiceManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "QUOTA_5106 RESPONSE CODE : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mKVSResponse:Lcom/samsung/android/scloud/quota/response/KVSResponse;

    invoke-virtual {v3}, Lcom/samsung/android/scloud/quota/response/KVSResponse;->getResponseCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mKVSResponse:Lcom/samsung/android/scloud/quota/response/KVSResponse;

    invoke-virtual {v1}, Lcom/samsung/android/scloud/quota/response/KVSResponse;->getResponseCode()I

    move-result v1

    if-eqz v1, :cond_0

    .line 111
    new-instance v1, Lcom/samsung/android/scloud/quota/QuotaException;

    const/16 v2, 0x9

    invoke-direct {v1, v2}, Lcom/samsung/android/scloud/quota/QuotaException;-><init>(I)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 100
    .end local v0    # "url":Ljava/lang/StringBuilder;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 114
    .restart local v0    # "url":Ljava/lang/StringBuilder;
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mKVSResponse:Lcom/samsung/android/scloud/quota/response/KVSResponse;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v1
.end method

.method public declared-synchronized getTotalUsageByCID(Ljava/lang/String;)Lcom/samsung/android/scloud/quota/response/KVSResponse;
    .locals 4
    .param p1, "cid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/scloud/quota/QuotaException;
        }
    .end annotation

    .prologue
    .line 139
    monitor-enter p0

    :try_start_0
    const-string v1, "QuotaServiceManager"

    const-string v2, "QUOTA_5108 : [START]"

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mBaseUrl:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 141
    .local v0, "url":Ljava/lang/StringBuilder;
    const-string v1, "/user/quota"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 142
    const-string v1, "?view=cid&cid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 144
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 146
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mUserParamStr:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    const-string v1, "713"

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->setLastApi(Ljava/lang/String;)V

    .line 148
    const/4 v1, 0x1

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mKVSResponse:Lcom/samsung/android/scloud/quota/response/KVSResponse;

    invoke-direct {p0, v1, v2, v3}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->handleRequest(ILjava/lang/String;Lcom/samsung/android/scloud/quota/response/KVSResponse;)Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;

    .line 149
    const-string v1, "QuotaServiceManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "QUOTA_5108 RESPONSE CODE : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mKVSResponse:Lcom/samsung/android/scloud/quota/response/KVSResponse;

    invoke-virtual {v3}, Lcom/samsung/android/scloud/quota/response/KVSResponse;->getResponseCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mKVSResponse:Lcom/samsung/android/scloud/quota/response/KVSResponse;

    invoke-virtual {v1}, Lcom/samsung/android/scloud/quota/response/KVSResponse;->getResponseCode()I

    move-result v1

    if-eqz v1, :cond_0

    .line 151
    new-instance v1, Lcom/samsung/android/scloud/quota/QuotaException;

    const/16 v2, 0x9

    invoke-direct {v1, v2}, Lcom/samsung/android/scloud/quota/QuotaException;-><init>(I)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 139
    .end local v0    # "url":Ljava/lang/StringBuilder;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 154
    .restart local v0    # "url":Ljava/lang/StringBuilder;
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mKVSResponse:Lcom/samsung/android/scloud/quota/response/KVSResponse;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v1
.end method

.method public declared-synchronized getTotalUsageByDID(Ljava/lang/String;)Lcom/samsung/android/scloud/quota/response/KVSResponse;
    .locals 4
    .param p1, "did"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/scloud/quota/QuotaException;
        }
    .end annotation

    .prologue
    .line 160
    monitor-enter p0

    :try_start_0
    const-string v1, "QuotaServiceManager"

    const-string v2, "QUOTA_5109 : [START]"

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mBaseUrl:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 162
    .local v0, "url":Ljava/lang/StringBuilder;
    const-string v1, "/user/quota"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 163
    const-string v1, "?view=device&did="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 165
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 167
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mUserParamStr:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 168
    const-string v1, "714"

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->setLastApi(Ljava/lang/String;)V

    .line 170
    const/4 v1, 0x1

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mKVSResponse:Lcom/samsung/android/scloud/quota/response/KVSResponse;

    invoke-direct {p0, v1, v2, v3}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->handleRequest(ILjava/lang/String;Lcom/samsung/android/scloud/quota/response/KVSResponse;)Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;

    .line 171
    const-string v1, "QuotaServiceManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "QUOTA_5109 RESPONSE CODE : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mKVSResponse:Lcom/samsung/android/scloud/quota/response/KVSResponse;

    invoke-virtual {v3}, Lcom/samsung/android/scloud/quota/response/KVSResponse;->getResponseCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mKVSResponse:Lcom/samsung/android/scloud/quota/response/KVSResponse;

    invoke-virtual {v1}, Lcom/samsung/android/scloud/quota/response/KVSResponse;->getResponseCode()I

    move-result v1

    if-eqz v1, :cond_0

    .line 173
    new-instance v1, Lcom/samsung/android/scloud/quota/QuotaException;

    const/16 v2, 0x9

    invoke-direct {v1, v2}, Lcom/samsung/android/scloud/quota/QuotaException;-><init>(I)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 160
    .end local v0    # "url":Ljava/lang/StringBuilder;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 176
    .restart local v0    # "url":Ljava/lang/StringBuilder;
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mKVSResponse:Lcom/samsung/android/scloud/quota/response/KVSResponse;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v1
.end method

.method public declared-synchronized getTotalUsageByUser(Ljava/lang/String;)Lcom/samsung/android/scloud/quota/response/KVSResponse;
    .locals 4
    .param p1, "mCtid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/scloud/quota/QuotaException;
        }
    .end annotation

    .prologue
    .line 119
    monitor-enter p0

    :try_start_0
    const-string v1, "QuotaServiceManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "QUOTA_5107 : [START] , ctid : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mBaseUrl:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 122
    .local v0, "url":Ljava/lang/StringBuilder;
    const-string v1, "/user/quota"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 123
    const-string v1, "?view=user"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mUserParamStr:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126
    if-eqz p1, :cond_0

    .line 128
    const-string v1, "712"

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->setLastApi(Ljava/lang/String;)V

    .line 131
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mKVSResponse:Lcom/samsung/android/scloud/quota/response/KVSResponse;

    invoke-direct {p0, v1, v2, v3}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->handleRequest(ILjava/lang/String;Lcom/samsung/android/scloud/quota/response/KVSResponse;)Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;

    .line 132
    const-string v1, "QuotaServiceManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "QUOTA_5107 RESPONSE CODE : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mKVSResponse:Lcom/samsung/android/scloud/quota/response/KVSResponse;

    invoke-virtual {v3}, Lcom/samsung/android/scloud/quota/response/KVSResponse;->getResponseCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mKVSResponse:Lcom/samsung/android/scloud/quota/response/KVSResponse;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v1

    .line 119
    .end local v0    # "url":Ljava/lang/StringBuilder;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method protected handleRequest(ILjava/lang/String;Lcom/samsung/android/scloud/quota/response/KVSResponse;Ljava/lang/String;)Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;
    .locals 6
    .param p1, "requestType"    # I
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "responseMeta"    # Lcom/samsung/android/scloud/quota/response/KVSResponse;
    .param p4, "Json"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/scloud/quota/QuotaException;
        }
    .end annotation

    .prologue
    .line 356
    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->handleRequest(ILjava/lang/String;Lcom/samsung/android/scloud/quota/response/KVSResponse;Ljava/lang/String;Lorg/apache/http/entity/mime/MultipartEntity;)Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized serviceEnd(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Lcom/samsung/android/scloud/quota/response/KVSResponse;
    .locals 4
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "ctid"    # Ljava/lang/String;
    .param p3, "mSuccess"    # Z
    .param p4, "LastAPI"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/scloud/quota/QuotaException;
        }
    .end annotation

    .prologue
    .line 293
    monitor-enter p0

    :try_start_0
    const-string v1, "QuotaServiceManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "serviceEnd() - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mBaseUrl:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 295
    .local v0, "url":Ljava/lang/StringBuilder;
    const-string v1, "/cloud/end?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 296
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mUserParamStr:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 297
    const-string v1, "&cid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 298
    const-string v1, "tj9u972o46"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 299
    const-string v1, "&cdid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 300
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/android/scloud/quota/QuotaUtils;->getClientDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 301
    const-string v1, "&did="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 302
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mAuthManager:Lcom/samsung/android/scloud/quota/AuthManager;

    invoke-virtual {v1}, Lcom/samsung/android/scloud/quota/AuthManager;->getDeviceID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 303
    const-string v1, "&ctid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 304
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 305
    const-string v1, "&result="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 306
    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 307
    if-eqz p3, :cond_0

    const-string v1, "r"

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 308
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 309
    const/4 v1, 0x1

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mKVSResponse:Lcom/samsung/android/scloud/quota/response/KVSResponse;

    invoke-direct {p0, v1, v2, v3}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->handleRequest(ILjava/lang/String;Lcom/samsung/android/scloud/quota/response/KVSResponse;)Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;

    .line 311
    const-string v1, "QuotaServiceManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "serviceEnd , ctid : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", Response code : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mKVSResponse:Lcom/samsung/android/scloud/quota/response/KVSResponse;

    invoke-virtual {v3}, Lcom/samsung/android/scloud/quota/response/KVSResponse;->getResponseCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mKVSResponse:Lcom/samsung/android/scloud/quota/response/KVSResponse;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v1

    .line 307
    :cond_0
    :try_start_1
    const-string v1, "e"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 293
    .end local v0    # "url":Ljava/lang/StringBuilder;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized serviceStart(Ljava/lang/String;)Lcom/samsung/android/scloud/quota/response/KVSResponse;
    .locals 4
    .param p1, "ctid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/scloud/quota/QuotaException;
        }
    .end annotation

    .prologue
    .line 272
    monitor-enter p0

    :try_start_0
    const-string v1, "QuotaServiceManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "serviceStart() - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mBaseUrl:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 274
    .local v0, "url":Ljava/lang/StringBuilder;
    const-string v1, "/cloud/start?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 275
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mUserParamStr:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 276
    const-string v1, "&cid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 277
    const-string v1, "tj9u972o46"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 278
    const-string v1, "&cdid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 279
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/android/scloud/quota/QuotaUtils;->getClientDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 280
    const-string v1, "&did="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 281
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mAuthManager:Lcom/samsung/android/scloud/quota/AuthManager;

    invoke-virtual {v1}, Lcom/samsung/android/scloud/quota/AuthManager;->getDeviceID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 282
    const-string v1, "&ctid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 283
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 284
    const-string v1, "&trigger=quota"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 285
    const-string v1, "811"

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->setLastApi(Ljava/lang/String;)V

    .line 286
    const/4 v1, 0x1

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mKVSResponse:Lcom/samsung/android/scloud/quota/response/KVSResponse;

    invoke-direct {p0, v1, v2, v3}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->handleRequest(ILjava/lang/String;Lcom/samsung/android/scloud/quota/response/KVSResponse;)Lcom/samsung/android/scloud/quota/networkutils/NetworkResponse;

    .line 288
    const-string v1, "QuotaServiceManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "serviceStart , ctid : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", Response code : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mKVSResponse:Lcom/samsung/android/scloud/quota/response/KVSResponse;

    invoke-virtual {v3}, Lcom/samsung/android/scloud/quota/response/KVSResponse;->getResponseCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mKVSResponse:Lcom/samsung/android/scloud/quota/response/KVSResponse;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v1

    .line 272
    .end local v0    # "url":Ljava/lang/StringBuilder;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public setApiResult(Ljava/lang/String;)V
    .locals 3
    .param p1, "responseCode"    # Ljava/lang/String;

    .prologue
    .line 263
    const-string v0, "QuotaServiceManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LASTAPI Result "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    sput-object p1, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mApiResult:Ljava/lang/String;

    .line 266
    return-void
.end method

.method public setApiSuccess(Z)V
    .locals 3
    .param p1, "value"    # Z

    .prologue
    .line 250
    const-string v0, "QuotaServiceManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LASTAPI Success "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    sput-boolean p1, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mSuccess:Z

    .line 253
    return-void
.end method

.method public setLastApi(Ljava/lang/String;)V
    .locals 3
    .param p1, "userQuotaUser"    # Ljava/lang/String;

    .prologue
    .line 237
    const-string v0, "QuotaServiceManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LASTAPI Set: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    sput-object p1, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->mLastApiCode:Ljava/lang/String;

    .line 240
    return-void
.end method
