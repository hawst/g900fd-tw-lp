.class public Lcom/samsung/android/scloud/quota/QuotaThread;
.super Ljava/lang/Thread;
.source "QuotaThread.java"


# static fields
.field public static final BACKUP_DELETE_END:I = 0x6

.field public static final BACKUP_DELETE_LIST:Ljava/lang/String; = "DELETE_LIST"

.field public static final BACKUP_DELETE_STATUS:Ljava/lang/String; = "DELETE_STATUS"

.field public static final BACKUP_DETAILS:Ljava/lang/String; = "BACKUP_DETAILS"

.field public static final BAD_ACCESS_TOKEN:I = -0x3

.field public static final CID_CALENDAR:Ljava/lang/String; = "CID_CALENDAR"

.field public static final CID_CONTACT:Ljava/lang/String; = "CID_CONTACT"

.field public static final CID_PINBOARD:Ljava/lang/String; = "CID_PINBOARD"

.field public static final CID_SMEMO:Ljava/lang/String; = "CID_SMEMO"

.field public static final CID_SNOTE:Ljava/lang/String; = "CID_SNOTE"

.field public static final CID_USAGE_BROWSER:Ljava/lang/String; = "CID_USAGE_BROWSER"

.field public static final CID_USAGE_CALENDAR:Ljava/lang/String; = "CID_USAGE_CALENDAR"

.field public static final CID_USAGE_CONTACT:Ljava/lang/String; = "CID_USAGE_CONTACT"

.field public static final CID_USAGE_KMEMO:Ljava/lang/String; = "CID_USAGE_KMEMO"

.field public static final CID_USAGE_OTHERS:Ljava/lang/String; = "CID_USAGE_OTHERS"

.field public static final CID_USAGE_PINBOARD:Ljava/lang/String; = "CID_USAGE_PINBOARD"

.field public static final CID_USAGE_SBROWSER:Ljava/lang/String; = "CID_USAGE_SBROWSER"

.field public static final CID_USAGE_SBROWSER_SCRAP:Ljava/lang/String; = "CID_USAGE_SBROWSER_SCRAP"

.field public static final CID_USAGE_SBROWSER_TAB:Ljava/lang/String; = "CID_USAGE_SBROWSER_TAB"

.field public static final CID_USAGE_SMEMO:Ljava/lang/String; = "CID_USAGE_SMEMO"

.field public static final CID_USAGE_SNOTE:Ljava/lang/String; = "CID_USAGE_SNOTE"

.field public static final CID_USAGE_SNOTE3:Ljava/lang/String; = "CID_USAGE_SNOTE3"

.field public static final CID_USAGE_TMEMO:Ljava/lang/String; = "CID_USAGE_TMEMO"

.field public static final FREE_QUOTA:Ljava/lang/String; = "FREE_QUOTA"

.field public static final GET_APP_USAGE:I = 0x2

.field public static final GET_BNR_USAGE:I = 0x4

.field public static final GET_BNR_USAGE_DETAILS:I = 0x5

.field public static final GET_CID_USAGE:I = 0x3

.field public static final GET_QUOTA_END:I = 0x0

.field public static final GET_TOTAL_USAGE:I = 0x1

.field public static final NO_QUOTA_NETWORK_RESPONSE:I = -0x1

.field public static final PLAN_DETAILS:I = 0x7

.field private static final TAG:Ljava/lang/String; = "QuotaThread"

.field public static final TOTAL_BACKEDUP_SIZE:Ljava/lang/String; = "BACKEDUP_SIZE"

.field public static final TOTAL_QUOTA:Ljava/lang/String; = "TOTAL_QUOTA"

.field public static final TOTAL_USAGE:Ljava/lang/String; = "TOTAL_USAGE"


# instance fields
.field private isBackupDetails:Ljava/lang/Boolean;

.field private isBackupSize:Ljava/lang/Boolean;

.field private isDeleteTrue:Ljava/lang/Boolean;

.field private isTotalUsage:Ljava/lang/Boolean;

.field protected mAuthManager:Lcom/samsung/android/scloud/quota/AuthManager;

.field private mBackupDetailsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/scloud/quota/BackupDetailsList;",
            ">;"
        }
    .end annotation
.end field

.field private mCidBrowserUSage:J

.field private mCidCalendarUSage:J

.field private mCidContactUsage:J

.field private mCidKmemoUsage:J

.field private mCidPinboardUSage:J

.field private mCidSBowserUsage:J

.field private mCidSbrowserScrapUsage:J

.field private mCidSmemoUsage:J

.field private mCidSnote3Usage:J

.field private mCidSnoteUsage:J

.field private mCidTmemoUsage:J

.field private mContext:Landroid/content/Context;

.field private mCtid:Ljava/lang/String;

.field private mDeleteBackupList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/scloud/quota/BackupDetails;",
            ">;"
        }
    .end annotation
.end field

.field private mFreeQuota:J

.field private mTotalBackupSize:Ljava/lang/Long;

.field private mTotalQuota:J

.field private mTotalUsage:J

.field private quotaResponseThreadListener:Lcom/samsung/android/scloud/quota/QuotaResponseThreadListener;

.field private quotaServiceManager:Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;

.field result:Lcom/samsung/android/scloud/quota/response/KVSResponse;


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    .line 20
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 23
    iput-object v1, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mContext:Landroid/content/Context;

    .line 24
    iput-object v1, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->quotaResponseThreadListener:Lcom/samsung/android/scloud/quota/QuotaResponseThreadListener;

    .line 25
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->isBackupDetails:Ljava/lang/Boolean;

    .line 26
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->isBackupSize:Ljava/lang/Boolean;

    .line 27
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->isDeleteTrue:Ljava/lang/Boolean;

    .line 28
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->isTotalUsage:Ljava/lang/Boolean;

    .line 29
    iput-object v1, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mAuthManager:Lcom/samsung/android/scloud/quota/AuthManager;

    .line 30
    iput-object v1, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCtid:Ljava/lang/String;

    .line 32
    iput-object v1, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->quotaServiceManager:Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;

    .line 33
    iput-wide v2, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mTotalUsage:J

    .line 34
    iput-wide v2, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mTotalQuota:J

    .line 35
    iput-wide v2, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mFreeQuota:J

    .line 37
    iput-wide v2, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCidContactUsage:J

    .line 38
    iput-wide v2, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCidSnoteUsage:J

    .line 39
    iput-wide v2, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCidSnote3Usage:J

    .line 40
    iput-wide v2, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCidSmemoUsage:J

    .line 41
    iput-wide v2, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCidKmemoUsage:J

    .line 42
    iput-wide v2, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCidTmemoUsage:J

    .line 43
    iput-wide v2, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCidCalendarUSage:J

    .line 44
    iput-wide v2, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCidPinboardUSage:J

    .line 45
    iput-wide v2, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCidBrowserUSage:J

    .line 46
    iput-wide v2, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCidSBowserUsage:J

    .line 47
    iput-wide v2, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCidSbrowserScrapUsage:J

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mBackupDetailsList:Ljava/util/ArrayList;

    .line 51
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mTotalBackupSize:Ljava/lang/Long;

    .line 94
    iput-object v1, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->result:Lcom/samsung/android/scloud/quota/response/KVSResponse;

    return-void
.end method

.method private deleteBackupFromServer()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 173
    const-string v4, "QuotaThread"

    const-string v5, "QUOTA_5115 : [START]"

    invoke-static {v4, v5}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 177
    .local v1, "deleteStatus":Landroid/os/Bundle;
    const-string v0, "0"

    .line 178
    .local v0, "code":Ljava/lang/String;
    new-instance v4, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;

    iget-object v5, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mAuthManager:Lcom/samsung/android/scloud/quota/AuthManager;

    invoke-direct {v4, v5, v6}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;-><init>(Landroid/content/Context;Lcom/samsung/android/scloud/quota/AuthManager;)V

    iput-object v4, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->quotaServiceManager:Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;

    .line 180
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v4, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mDeleteBackupList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v3, v4, :cond_1

    .line 182
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->quotaServiceManager:Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;

    iget-object v4, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mDeleteBackupList:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/scloud/quota/BackupDetails;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/quota/BackupDetails;->getClientDeviceID()Ljava/lang/String;

    move-result-object v6

    iget-object v4, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mDeleteBackupList:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/scloud/quota/BackupDetails;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/quota/BackupDetails;->deviceName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v6, v4}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->deleteBackupFromServer(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/scloud/quota/response/KVSResponse;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->result:Lcom/samsung/android/scloud/quota/response/KVSResponse;

    .line 186
    iget-object v4, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->quotaServiceManager:Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;

    invoke-virtual {v4, v0}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->setApiResult(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/samsung/android/scloud/quota/QuotaException; {:try_start_0 .. :try_end_0} :catch_0

    .line 199
    const-string v5, "BackupDelete"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Clear Data Requested for : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v4, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mDeleteBackupList:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/scloud/quota/BackupDetails;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/quota/BackupDetails;->deviceName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v4, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mDeleteBackupList:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/scloud/quota/BackupDetails;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/quota/BackupDetails;->getClientDeviceID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    iget-object v4, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->result:Lcom/samsung/android/scloud/quota/response/KVSResponse;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/quota/response/KVSResponse;->getResponseCode()I

    move-result v4

    if-nez v4, :cond_0

    .line 204
    const-string v5, "BackupDelete"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Backup Cleared for : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v4, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mDeleteBackupList:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/scloud/quota/BackupDetails;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/quota/BackupDetails;->deviceName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v4, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mDeleteBackupList:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/scloud/quota/BackupDetails;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/quota/BackupDetails;->getClientDeviceID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    iget-object v4, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mDeleteBackupList:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/scloud/quota/BackupDetails;

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/android/scloud/quota/BackupDetails;->setFlag(Ljava/lang/Boolean;)V

    .line 208
    iget-object v4, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->quotaServiceManager:Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;

    invoke-virtual {v4, v8}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->setApiSuccess(Z)V

    .line 180
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 187
    :catch_0
    move-exception v2

    .line 188
    .local v2, "e":Lcom/samsung/android/scloud/quota/QuotaException;
    const-string v4, "QuotaThread"

    const-string v5, "QuotaException received while deleting the backup"

    invoke-static {v4, v5}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    const-string v5, "BackupDelete"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Backup Cleared failed for : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v4, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mDeleteBackupList:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/scloud/quota/BackupDetails;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/quota/BackupDetails;->deviceName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v4, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mDeleteBackupList:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/scloud/quota/BackupDetails;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/quota/BackupDetails;->getClientDeviceID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    iget-object v4, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mDeleteBackupList:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/scloud/quota/BackupDetails;

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/android/scloud/quota/BackupDetails;->setFlag(Ljava/lang/Boolean;)V

    .line 193
    iget-object v4, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->quotaServiceManager:Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;

    invoke-virtual {v4, v7}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->setApiSuccess(Z)V

    .line 194
    iget-object v4, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->quotaServiceManager:Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Lcom/samsung/android/scloud/quota/QuotaException;->getmExceptionCode()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->setApiResult(Ljava/lang/String;)V

    .line 195
    invoke-virtual {v2}, Lcom/samsung/android/scloud/quota/QuotaException;->printStackTrace()V

    goto :goto_1

    .line 213
    .end local v2    # "e":Lcom/samsung/android/scloud/quota/QuotaException;
    :cond_0
    const-string v5, "BackupDelete"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Backup Cleared failed for : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v4, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mDeleteBackupList:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/scloud/quota/BackupDetails;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/quota/BackupDetails;->deviceName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v4, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mDeleteBackupList:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/scloud/quota/BackupDetails;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/quota/BackupDetails;->getClientDeviceID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    iget-object v4, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mDeleteBackupList:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/scloud/quota/BackupDetails;

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/android/scloud/quota/BackupDetails;->setFlag(Ljava/lang/Boolean;)V

    .line 217
    iget-object v4, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->quotaServiceManager:Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;

    invoke-virtual {v4, v7}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->setApiSuccess(Z)V

    goto/16 :goto_1

    .line 222
    :cond_1
    const-string v4, "DELETE_LIST"

    iget-object v5, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mDeleteBackupList:Ljava/util/ArrayList;

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 224
    const/4 v4, 0x6

    invoke-direct {p0, v4, v1}, Lcom/samsung/android/scloud/quota/QuotaThread;->sendMessage(ILandroid/os/Bundle;)V

    .line 226
    return-void
.end method

.method private getAllUsage()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 232
    const-string v0, "QuotaThread"

    const-string v1, "[getAllUsage] : START"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    new-instance v0, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mAuthManager:Lcom/samsung/android/scloud/quota/AuthManager;

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;-><init>(Landroid/content/Context;Lcom/samsung/android/scloud/quota/AuthManager;)V

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->quotaServiceManager:Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;

    .line 240
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->isBackupSize:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->isBackupDetails:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 242
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/scloud/quota/QuotaThread;->getQuotaDetails()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 251
    :cond_1
    :goto_0
    const-string v0, "QuotaThread"

    const-string v1, "[getAllUsage] : END"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    invoke-direct {p0, v3, v4}, Lcom/samsung/android/scloud/quota/QuotaThread;->sendMessage(ILandroid/os/Bundle;)V

    .line 255
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/QuotaThread;->interrupt()V

    .line 259
    return-void

    .line 243
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->isTotalUsage:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 244
    invoke-direct {p0}, Lcom/samsung/android/scloud/quota/QuotaThread;->getTotalUsage()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 251
    :catchall_0
    move-exception v0

    const-string v1, "QuotaThread"

    const-string v2, "[getAllUsage] : END"

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    invoke-direct {p0, v3, v4}, Lcom/samsung/android/scloud/quota/QuotaThread;->sendMessage(ILandroid/os/Bundle;)V

    .line 255
    invoke-virtual {p0}, Lcom/samsung/android/scloud/quota/QuotaThread;->interrupt()V

    throw v0
.end method

.method private getQuotaDetails()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x0

    .line 297
    const-string v6, "QuotaThread"

    const-string v7, "QUOTA_5103 : [START]"

    invoke-static {v6, v7}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    const/4 v3, 0x0

    .line 300
    .local v3, "flag":Z
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 301
    .local v0, "bundBNRUsage":Landroid/os/Bundle;
    const/4 v5, 0x0

    .line 302
    .local v5, "mMapCIDUsage":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    const-string v1, "0"

    .line 305
    .local v1, "code":Ljava/lang/String;
    :try_start_0
    iget-object v6, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCtid:Ljava/lang/String;

    if-eqz v6, :cond_0

    .line 307
    const-string v6, "QuotaThread"

    const-string v7, "ServiceStart Command"

    invoke-static {v6, v7}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    iget-object v6, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->quotaServiceManager:Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;

    iget-object v7, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCtid:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->serviceStart(Ljava/lang/String;)Lcom/samsung/android/scloud/quota/response/KVSResponse;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->result:Lcom/samsung/android/scloud/quota/response/KVSResponse;

    .line 309
    iget-object v6, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->result:Lcom/samsung/android/scloud/quota/response/KVSResponse;

    invoke-virtual {v6}, Lcom/samsung/android/scloud/quota/response/KVSResponse;->getResponseCode()I

    move-result v6

    if-nez v6, :cond_3

    .line 310
    iget-object v6, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->quotaServiceManager:Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->setApiSuccess(Z)V

    .line 313
    :goto_0
    iget-object v6, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->quotaServiceManager:Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;

    invoke-virtual {v6, v1}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->setApiResult(Ljava/lang/String;)V

    .line 314
    const/4 v3, 0x1

    .line 317
    :cond_0
    iget-object v6, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->quotaServiceManager:Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;

    invoke-virtual {v6}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->getApiSuccess()Z

    move-result v6

    if-nez v6, :cond_1

    iget-object v6, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->result:Lcom/samsung/android/scloud/quota/response/KVSResponse;

    if-nez v6, :cond_7

    .line 321
    :cond_1
    iget-object v6, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->quotaServiceManager:Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;

    iget-object v7, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCtid:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->getTotalUsageByUser(Ljava/lang/String;)Lcom/samsung/android/scloud/quota/response/KVSResponse;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->result:Lcom/samsung/android/scloud/quota/response/KVSResponse;

    .line 322
    iget-object v6, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->result:Lcom/samsung/android/scloud/quota/response/KVSResponse;

    invoke-virtual {v6}, Lcom/samsung/android/scloud/quota/response/KVSResponse;->getResponseCode()I

    move-result v6

    if-nez v6, :cond_7

    .line 324
    if-eqz v3, :cond_2

    .line 326
    const-string v1, "0"

    .line 327
    iget-object v6, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->quotaServiceManager:Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->setApiSuccess(Z)V

    .line 328
    iget-object v6, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->quotaServiceManager:Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;

    invoke-virtual {v6, v1}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->setApiResult(Ljava/lang/String;)V

    .line 330
    :cond_2
    const-string v6, "QuotaThread"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getTotalUsageByUser Response code : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->result:Lcom/samsung/android/scloud/quota/response/KVSResponse;

    invoke-virtual {v8}, Lcom/samsung/android/scloud/quota/response/KVSResponse;->getResponseCode()I

    move-result v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    const-string v6, "QuotaThread"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Backup List size : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->result:Lcom/samsung/android/scloud/quota/response/KVSResponse;

    invoke-virtual {v8}, Lcom/samsung/android/scloud/quota/response/KVSResponse;->getBNRListSize()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    iget-object v6, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->result:Lcom/samsung/android/scloud/quota/response/KVSResponse;

    invoke-virtual {v6}, Lcom/samsung/android/scloud/quota/response/KVSResponse;->getBNRListSize()I

    move-result v6

    if-lez v6, :cond_9

    .line 334
    iget-object v6, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->result:Lcom/samsung/android/scloud/quota/response/KVSResponse;

    invoke-virtual {v6}, Lcom/samsung/android/scloud/quota/response/KVSResponse;->getBnrDetailsList()Ljava/util/ArrayList;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mBackupDetailsList:Ljava/util/ArrayList;

    .line 335
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    iget-object v6, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mBackupDetailsList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v4, v6, :cond_8

    .line 337
    iget-object v6, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mTotalBackupSize:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    iget-object v6, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mBackupDetailsList:Ljava/util/ArrayList;

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/scloud/quota/BackupDetailsList;

    invoke-virtual {v6}, Lcom/samsung/android/scloud/quota/BackupDetailsList;->getBackupSize()Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    add-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mTotalBackupSize:Ljava/lang/Long;

    .line 335
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 312
    .end local v4    # "i":I
    :cond_3
    iget-object v6, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->quotaServiceManager:Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->setApiSuccess(Z)V
    :try_end_0
    .catch Lcom/samsung/android/scloud/quota/QuotaException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 435
    :catch_0
    move-exception v2

    .line 437
    .local v2, "e":Lcom/samsung/android/scloud/quota/QuotaException;
    invoke-virtual {v2}, Lcom/samsung/android/scloud/quota/QuotaException;->getmExceptionCode()I

    move-result v6

    const/4 v7, 0x7

    if-ne v6, v7, :cond_4

    .line 439
    const-string v6, "QuotaThread"

    const-string v7, "Request failed due to Netork error"

    invoke-static {v6, v7}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    const/4 v6, -0x1

    invoke-direct {p0, v6, v11}, Lcom/samsung/android/scloud/quota/QuotaThread;->sendMessage(ILandroid/os/Bundle;)V

    .line 443
    :cond_4
    invoke-virtual {v2}, Lcom/samsung/android/scloud/quota/QuotaException;->getmExceptionCode()I

    move-result v6

    const/4 v7, 0x6

    if-ne v6, v7, :cond_5

    .line 444
    const-string v6, "QuotaThread"

    const-string v7, "Request failed due to Bad AccessToken"

    invoke-static {v6, v7}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    const/4 v6, -0x3

    invoke-direct {p0, v6, v11}, Lcom/samsung/android/scloud/quota/QuotaThread;->sendMessage(ILandroid/os/Bundle;)V

    .line 447
    :cond_5
    iget-object v6, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCtid:Ljava/lang/String;

    if-eqz v6, :cond_6

    .line 450
    iget-object v6, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->quotaServiceManager:Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;

    invoke-virtual {v6, v10}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->setApiSuccess(Z)V

    .line 451
    iget-object v6, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->quotaServiceManager:Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Lcom/samsung/android/scloud/quota/QuotaException;->getmExceptionCode()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->setApiResult(Ljava/lang/String;)V

    .line 456
    :cond_6
    invoke-virtual {v2}, Lcom/samsung/android/scloud/quota/QuotaException;->printStackTrace()V

    .line 460
    .end local v2    # "e":Lcom/samsung/android/scloud/quota/QuotaException;
    :cond_7
    :goto_2
    return-void

    .line 339
    .restart local v4    # "i":I
    :cond_8
    :try_start_1
    const-string v6, "QuotaThread"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Total Backup Size is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mTotalBackupSize:Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    .end local v4    # "i":I
    :cond_9
    iget-object v6, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->result:Lcom/samsung/android/scloud/quota/response/KVSResponse;

    invoke-virtual {v6}, Lcom/samsung/android/scloud/quota/response/KVSResponse;->getTotalUsage()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mTotalUsage:J

    .line 344
    iget-object v6, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->result:Lcom/samsung/android/scloud/quota/response/KVSResponse;

    invoke-virtual {v6}, Lcom/samsung/android/scloud/quota/response/KVSResponse;->getTotalQuota()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mTotalQuota:J

    .line 345
    iget-object v6, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->result:Lcom/samsung/android/scloud/quota/response/KVSResponse;

    invoke-virtual {v6}, Lcom/samsung/android/scloud/quota/response/KVSResponse;->getFreeQuota()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mFreeQuota:J

    .line 346
    const-string v6, "QuotaThread"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "TOTAl_USAGE : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-wide v8, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mTotalUsage:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    const-string v6, "QuotaThread"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "TOTAL_QUOTA : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-wide v8, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mTotalQuota:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    const-string v6, "QuotaThread"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "FREE_QUOTA : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-wide v8, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mFreeQuota:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 349
    const-string v6, "TOTAL_USAGE"

    iget-wide v8, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mTotalUsage:J

    invoke-virtual {v0, v6, v8, v9}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 350
    const-string v6, "TOTAL_QUOTA"

    iget-wide v8, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mTotalQuota:J

    invoke-virtual {v0, v6, v8, v9}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 351
    const-string v6, "FREE_QUOTA"

    iget-wide v8, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mFreeQuota:J

    invoke-virtual {v0, v6, v8, v9}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 353
    iget-object v6, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->result:Lcom/samsung/android/scloud/quota/response/KVSResponse;

    invoke-virtual {v6}, Lcom/samsung/android/scloud/quota/response/KVSResponse;->getCIDUsageMap()Ljava/util/HashMap;

    move-result-object v5

    .line 355
    const-string v6, "QuotaThread"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "CIDUSAGE MAP SIZE "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    const-string v6, "KEqLhXhtEP"

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCidContactUsage:J

    .line 357
    const-string v6, "QuotaThread"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "CIDUSAGE CONTACT  KEqLhXhtEP : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-wide v8, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCidContactUsage:J

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    const-string v6, "a1QGNqwu27"

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCidSnoteUsage:J

    .line 359
    const-string v6, "QuotaThread"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "CIDUSAGE SNOTE a1QGNqwu27 : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-wide v8, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCidSnoteUsage:J

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    const-string v6, "PM3HWwUYhP"

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_a

    .line 362
    const-string v6, "PM3HWwUYhP"

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCidSnote3Usage:J

    .line 363
    const-string v6, "QuotaThread"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "CIDUSAGE SNOTE3 PM3HWwUYhP : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-wide v8, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCidSnote3Usage:J

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    :cond_a
    const-string v6, "X08g96bD1C"

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCidSmemoUsage:J

    .line 366
    const-string v6, "QuotaThread"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "CIDUSAGE SMEMO X08g96bD1C : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-wide v8, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCidSmemoUsage:J

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    const-string v6, "8kLTKS0V1y"

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCidCalendarUSage:J

    .line 368
    const-string v6, "QuotaThread"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "CIDUSAGE  CALENDAR 8kLTKS0V1y : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-wide v8, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCidCalendarUSage:J

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    const-string v6, "LzftMiEuh2"

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_b

    .line 371
    const-string v6, "LzftMiEuh2"

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCidTmemoUsage:J

    .line 372
    const-string v6, "QuotaThread"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "CIDUSAGE  TMEMO LzftMiEuh2 : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-wide v8, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCidTmemoUsage:J

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    :cond_b
    const-string v6, "w8wcqZo4Uk"

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_c

    .line 376
    const-string v6, "w8wcqZo4Uk"

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCidKmemoUsage:J

    .line 377
    const-string v6, "QuotaThread"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "CIDUSAGE  KMEMO w8wcqZo4Uk : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-wide v8, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCidKmemoUsage:J

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    :cond_c
    const-string v6, "gr3k9outd1"

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_d

    .line 381
    const-string v6, "gr3k9outd1"

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCidPinboardUSage:J

    .line 382
    const-string v6, "QuotaThread"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "CIDUSAGE  PINBOARD gr3k9outd1 : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-wide v8, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCidPinboardUSage:J

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    :cond_d
    const-string v6, "4OuNBe4y9z"

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCidBrowserUSage:J

    .line 385
    const-string v6, "QuotaThread"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "CIDUSAGE  BROWSER 4OuNBe4y9z : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-wide v8, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCidBrowserUSage:J

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    const-string v6, "P56GWW8N4r"

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCidSBowserUsage:J

    .line 387
    const-string v6, "QuotaThread"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "CIDUSAGE  SBROWSER P56GWW8N4r : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-wide v8, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCidSBowserUsage:J

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    const-string v6, "QUVql3tKM8"

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_e

    .line 391
    const-string v6, "QUVql3tKM8"

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCidSbrowserScrapUsage:J

    .line 392
    const-string v6, "QuotaThread"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "CIDUSAGE  SBROWSERSCRAPS QUVql3tKM8 : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-wide v8, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCidSbrowserScrapUsage:J

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 397
    :cond_e
    const-string v6, "CID_USAGE_CONTACT"

    iget-wide v8, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCidContactUsage:J

    invoke-virtual {v0, v6, v8, v9}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 398
    const-string v6, "CID_USAGE_PINBOARD"

    iget-wide v8, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCidPinboardUSage:J

    invoke-virtual {v0, v6, v8, v9}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 399
    const-string v6, "CID_USAGE_SNOTE"

    iget-wide v8, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCidSnoteUsage:J

    invoke-virtual {v0, v6, v8, v9}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 400
    const-string v6, "CID_USAGE_SNOTE3"

    iget-wide v8, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCidSnote3Usage:J

    invoke-virtual {v0, v6, v8, v9}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 401
    const-string v6, "CID_USAGE_SMEMO"

    iget-wide v8, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCidSmemoUsage:J

    invoke-virtual {v0, v6, v8, v9}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 402
    const-string v6, "CID_USAGE_TMEMO"

    iget-wide v8, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCidTmemoUsage:J

    invoke-virtual {v0, v6, v8, v9}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 403
    const-string v6, "CID_USAGE_KMEMO"

    iget-wide v8, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCidKmemoUsage:J

    invoke-virtual {v0, v6, v8, v9}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 404
    const-string v6, "CID_USAGE_CALENDAR"

    iget-wide v8, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCidCalendarUSage:J

    invoke-virtual {v0, v6, v8, v9}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 405
    const-string v6, "CID_USAGE_BROWSER"

    iget-wide v8, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCidBrowserUSage:J

    invoke-virtual {v0, v6, v8, v9}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 406
    const-string v6, "CID_USAGE_SBROWSER"

    iget-wide v8, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCidSBowserUsage:J

    invoke-virtual {v0, v6, v8, v9}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 407
    const-string v6, "CID_USAGE_SBROWSER_SCRAP"

    iget-wide v8, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCidSbrowserScrapUsage:J

    invoke-virtual {v0, v6, v8, v9}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 409
    const-string v6, "QuotaThread"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "BACKEDUP_SIZE"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mTotalBackupSize:Ljava/lang/Long;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    iget-object v6, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->isBackupDetails:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-nez v6, :cond_f

    .line 412
    const-string v6, "QuotaThread"

    const-string v7, "QUOTA_5103 : [ONLY USAGE]"

    invoke-static {v6, v7}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    const-string v6, "BACKEDUP_SIZE"

    iget-object v7, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mTotalBackupSize:Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v0, v6, v8, v9}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 414
    const/4 v6, 0x4

    invoke-direct {p0, v6, v0}, Lcom/samsung/android/scloud/quota/QuotaThread;->sendMessage(ILandroid/os/Bundle;)V

    goto/16 :goto_2

    .line 418
    :cond_f
    const-string v6, "QuotaThread"

    const-string v7, "QUOTA_5103 : [DETAIL DISCRIPTION]"

    invoke-static {v6, v7}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    iget-object v6, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mBackupDetailsList:Ljava/util/ArrayList;

    if-eqz v6, :cond_10

    iget-object v6, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mBackupDetailsList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_10

    .line 421
    const-string v6, "QuotaThread"

    const-string v7, "Sort the BackupList according to BackupTime"

    invoke-static {v6, v7}, Lcom/samsung/android/scloud/quota/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    iget-object v6, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mBackupDetailsList:Ljava/util/ArrayList;

    new-instance v7, Lcom/samsung/android/scloud/quota/BackupDetailsList$BackupTimeComparator;

    invoke-direct {v7}, Lcom/samsung/android/scloud/quota/BackupDetailsList$BackupTimeComparator;-><init>()V

    invoke-static {v6, v7}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 424
    :cond_10
    const-string v6, "BACKUP_DETAILS"

    iget-object v7, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mBackupDetailsList:Ljava/util/ArrayList;

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 425
    const/4 v6, 0x5

    invoke-direct {p0, v6, v0}, Lcom/samsung/android/scloud/quota/QuotaThread;->sendMessage(ILandroid/os/Bundle;)V
    :try_end_1
    .catch Lcom/samsung/android/scloud/quota/QuotaException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_2
.end method

.method private getTotalUsage()V
    .locals 6

    .prologue
    .line 265
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 269
    .local v0, "bundTotalUsage":Landroid/os/Bundle;
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->quotaServiceManager:Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;

    invoke-virtual {v2}, Lcom/samsung/android/scloud/quota/servicemanager/QuotaServiceManager;->getTotalUsage()Lcom/samsung/android/scloud/quota/response/KVSResponse;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->result:Lcom/samsung/android/scloud/quota/response/KVSResponse;

    .line 271
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->result:Lcom/samsung/android/scloud/quota/response/KVSResponse;

    invoke-virtual {v2}, Lcom/samsung/android/scloud/quota/response/KVSResponse;->getTotalUsage()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mTotalUsage:J

    .line 273
    iget-object v2, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->result:Lcom/samsung/android/scloud/quota/response/KVSResponse;

    invoke-virtual {v2}, Lcom/samsung/android/scloud/quota/response/KVSResponse;->getTotalQuota()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mTotalQuota:J

    .line 275
    const-string v2, "TOTAL_USAGE"

    iget-wide v4, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mTotalUsage:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    const-string v2, "TOTAL_QUOTA"

    iget-wide v4, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mTotalQuota:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    const-string v2, "TOTAL_USAGE"

    iget-wide v4, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mTotalUsage:J

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 281
    const-string v2, "TOTAL_QUOTA"

    iget-wide v4, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mTotalQuota:J

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 283
    const/4 v2, 0x1

    invoke-direct {p0, v2, v0}, Lcom/samsung/android/scloud/quota/QuotaThread;->sendMessage(ILandroid/os/Bundle;)V
    :try_end_0
    .catch Lcom/samsung/android/scloud/quota/QuotaException; {:try_start_0 .. :try_end_0} :catch_0

    .line 293
    :goto_0
    return-void

    .line 285
    :catch_0
    move-exception v1

    .line 289
    .local v1, "e":Lcom/samsung/android/scloud/quota/QuotaException;
    invoke-virtual {v1}, Lcom/samsung/android/scloud/quota/QuotaException;->printStackTrace()V

    goto :goto_0
.end method

.method private sendMessage(ILandroid/os/Bundle;)V
    .locals 2
    .param p1, "what"    # I
    .param p2, "data"    # Landroid/os/Bundle;

    .prologue
    .line 466
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 468
    .local v0, "msg":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->what:I

    .line 470
    invoke-virtual {v0, p2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 472
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->quotaResponseThreadListener:Lcom/samsung/android/scloud/quota/QuotaResponseThreadListener;

    if-eqz v1, :cond_0

    .line 474
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->quotaResponseThreadListener:Lcom/samsung/android/scloud/quota/QuotaResponseThreadListener;

    invoke-interface {v1, v0}, Lcom/samsung/android/scloud/quota/QuotaResponseThreadListener;->onResponse(Landroid/os/Message;)V

    .line 476
    :cond_0
    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 160
    const-string v0, "QuotaThread"

    const-string v1, "QUOTA_5101 : [STARTED]"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->isDeleteTrue:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 163
    invoke-direct {p0}, Lcom/samsung/android/scloud/quota/QuotaThread;->getAllUsage()V

    .line 169
    :goto_0
    return-void

    .line 166
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/scloud/quota/QuotaThread;->deleteBackupFromServer()V

    goto :goto_0
.end method

.method public setAuthManager(Lcom/samsung/android/scloud/quota/AuthManager;)V
    .locals 0
    .param p1, "authManager"    # Lcom/samsung/android/scloud/quota/AuthManager;

    .prologue
    .line 148
    iput-object p1, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mAuthManager:Lcom/samsung/android/scloud/quota/AuthManager;

    .line 149
    return-void
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mContext:Landroid/content/Context;

    .line 107
    return-void
.end method

.method public setCtid(Ljava/lang/String;)V
    .locals 0
    .param p1, "ctid"    # Ljava/lang/String;

    .prologue
    .line 153
    iput-object p1, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mCtid:Ljava/lang/String;

    .line 155
    return-void
.end method

.method public setDeleteDeviceList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/scloud/quota/BackupDetails;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 143
    .local p1, "BackupList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/scloud/quota/BackupDetails;>;"
    iput-object p1, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->mDeleteBackupList:Ljava/util/ArrayList;

    .line 145
    return-void
.end method

.method public setISDeleteTrue(Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "flag"    # Ljava/lang/Boolean;

    .prologue
    .line 123
    iput-object p1, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->isDeleteTrue:Ljava/lang/Boolean;

    .line 125
    return-void
.end method

.method public setIsBackedupSize(Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "flag"    # Ljava/lang/Boolean;

    .prologue
    .line 131
    iput-object p1, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->isBackupSize:Ljava/lang/Boolean;

    .line 133
    return-void
.end method

.method public setIsBackupDetails(Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "flag"    # Ljava/lang/Boolean;

    .prologue
    .line 117
    iput-object p1, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->isBackupDetails:Ljava/lang/Boolean;

    .line 119
    return-void
.end method

.method public setListener(Lcom/samsung/android/scloud/quota/QuotaResponseThreadListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/scloud/quota/QuotaResponseThreadListener;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/samsung/android/scloud/quota/QuotaThread;->quotaResponseThreadListener:Lcom/samsung/android/scloud/quota/QuotaResponseThreadListener;

    .line 113
    return-void
.end method
