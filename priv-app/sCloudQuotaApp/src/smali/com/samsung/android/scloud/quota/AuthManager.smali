.class public final Lcom/samsung/android/scloud/quota/AuthManager;
.super Ljava/lang/Object;
.source "AuthManager.java"


# static fields
.field private static final INFO_OK:I = 0x0

.field private static final TAG:Ljava/lang/String; = "AuthManager"

.field private static sAuthManager:Lcom/samsung/android/scloud/quota/AuthManager;


# instance fields
.field private mAccessToken:Ljava/lang/String;

.field private mBackupBaseUrlPrimary:Ljava/lang/String;

.field private mBackupGetAPIparms:Ljava/lang/String;

.field private mBackupPutAPIparms:Ljava/lang/String;

.field private mBaseUrlPrimary:Ljava/lang/String;

.field private mGetAPIparms:Ljava/lang/String;

.field private mGetAPIparmsStorage:Ljava/lang/String;

.field private mPutAPIparms:Ljava/lang/String;

.field private mRegistrationID:Ljava/lang/String;

.field private mUserId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/scloud/quota/AuthManager;->sAuthManager:Lcom/samsung/android/scloud/quota/AuthManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object v0, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mUserId:Ljava/lang/String;

    .line 59
    iput-object v0, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mAccessToken:Ljava/lang/String;

    .line 61
    iput-object v0, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mRegistrationID:Ljava/lang/String;

    .line 63
    iput-object v0, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mBaseUrlPrimary:Ljava/lang/String;

    .line 65
    iput-object v0, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mGetAPIparms:Ljava/lang/String;

    .line 67
    iput-object v0, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mGetAPIparmsStorage:Ljava/lang/String;

    .line 69
    iput-object v0, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mPutAPIparms:Ljava/lang/String;

    .line 73
    iput-object v0, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mBackupBaseUrlPrimary:Ljava/lang/String;

    .line 75
    iput-object v0, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mBackupGetAPIparms:Ljava/lang/String;

    .line 77
    iput-object v0, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mBackupPutAPIparms:Ljava/lang/String;

    .line 88
    return-void
.end method

.method public static declared-synchronized create()Lcom/samsung/android/scloud/quota/AuthManager;
    .locals 2

    .prologue
    .line 96
    const-class v1, Lcom/samsung/android/scloud/quota/AuthManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/scloud/quota/AuthManager;->sAuthManager:Lcom/samsung/android/scloud/quota/AuthManager;

    if-nez v0, :cond_0

    .line 98
    new-instance v0, Lcom/samsung/android/scloud/quota/AuthManager;

    invoke-direct {v0}, Lcom/samsung/android/scloud/quota/AuthManager;-><init>()V

    sput-object v0, Lcom/samsung/android/scloud/quota/AuthManager;->sAuthManager:Lcom/samsung/android/scloud/quota/AuthManager;

    .line 102
    :cond_0
    sget-object v0, Lcom/samsung/android/scloud/quota/AuthManager;->sAuthManager:Lcom/samsung/android/scloud/quota/AuthManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 96
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private handleResult(I)V
    .locals 3
    .param p1, "status"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/scloud/quota/QuotaException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x3

    .line 320
    if-nez p1, :cond_1

    .line 322
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mUserId:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mRegistrationID:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mAccessToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mBaseUrlPrimary:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 326
    :cond_0
    const-string v0, "AuthManager"

    const-string v1, "Inside Handle Result : Auth information is empty"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    new-instance v0, Lcom/samsung/android/scloud/quota/QuotaException;

    invoke-direct {v0, v2}, Lcom/samsung/android/scloud/quota/QuotaException;-><init>(I)V

    throw v0

    .line 333
    :cond_1
    const-string v0, "AuthManager"

    const-string v1, "Inside Handle Result : Auth information status is not okay"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    new-instance v0, Lcom/samsung/android/scloud/quota/QuotaException;

    invoke-direct {v0, v2}, Lcom/samsung/android/scloud/quota/QuotaException;-><init>(I)V

    throw v0

    .line 338
    :cond_2
    return-void
.end method


# virtual methods
.method public getAccessToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mAccessToken:Ljava/lang/String;

    return-object v0
.end method

.method public getBackupBaseUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mBackupBaseUrlPrimary:Ljava/lang/String;

    return-object v0
.end method

.method public getBackupGetApiParams(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "cid"    # Ljava/lang/String;

    .prologue
    .line 161
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mBackupGetAPIparms:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&cid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBackupGetApiParamsWithoutCid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mBackupGetAPIparms:Ljava/lang/String;

    return-object v0
.end method

.method public getBackupPutAPIParamsWithoutCid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mBackupPutAPIparms:Ljava/lang/String;

    return-object v0
.end method

.method public getBackupPutApiParams(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "cid"    # Ljava/lang/String;

    .prologue
    .line 171
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mBackupPutAPIparms:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&cid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBaseUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mBaseUrlPrimary:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mRegistrationID:Ljava/lang/String;

    return-object v0
.end method

.method public getGetApiParams(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "cid"    # Ljava/lang/String;

    .prologue
    .line 112
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mGetAPIparms:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&cid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getGetApiParamsWithoutCid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mGetAPIparms:Ljava/lang/String;

    return-object v0
.end method

.method public getGetApiParamsWithoutCidStorage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mGetAPIparmsStorage:Ljava/lang/String;

    return-object v0
.end method

.method public getPutAPIParamsWithoutCid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mPutAPIparms:Ljava/lang/String;

    return-object v0
.end method

.method public getPutApiParams(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "cid"    # Ljava/lang/String;

    .prologue
    .line 123
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mPutAPIparms:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&cid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUserID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mUserId:Ljava/lang/String;

    return-object v0
.end method

.method public declared-synchronized updateAuthInformation(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "status"    # I
    .param p2, "userId"    # Ljava/lang/String;
    .param p3, "regId"    # Ljava/lang/String;
    .param p4, "accessToken"    # Ljava/lang/String;
    .param p5, "baseUrl"    # Ljava/lang/String;
    .param p6, "baseUrl2"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/scloud/quota/QuotaException;
        }
    .end annotation

    .prologue
    .line 245
    monitor-enter p0

    :try_start_0
    const-string v0, "AuthManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[updateAuthInformation] , getAuthInformation : STATUS = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    iput-object p5, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mBaseUrlPrimary:Ljava/lang/String;

    .line 251
    iput-object p4, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mAccessToken:Ljava/lang/String;

    .line 253
    iput-object p2, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mUserId:Ljava/lang/String;

    .line 255
    iput-object p3, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mRegistrationID:Ljava/lang/String;

    .line 257
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "&uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mUserId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&access_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mAccessToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mGetAPIparms:Ljava/lang/String;

    .line 259
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "&uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mUserId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&access_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mAccessToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mGetAPIparmsStorage:Ljava/lang/String;

    .line 261
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mGetAPIparms:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&did="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mRegistrationID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mPutAPIparms:Ljava/lang/String;

    .line 263
    iput-object p5, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mBackupBaseUrlPrimary:Ljava/lang/String;

    .line 265
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mGetAPIparms:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mBackupGetAPIparms:Ljava/lang/String;

    .line 267
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mPutAPIparms:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mBackupPutAPIparms:Ljava/lang/String;

    .line 270
    invoke-direct {p0, p1}, Lcom/samsung/android/scloud/quota/AuthManager;->handleResult(I)V

    .line 273
    const-string v0, "AuthManager"

    const-string v1, "[updateAuthInformation] : END"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 277
    monitor-exit p0

    return-void

    .line 245
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized updateAuthInformation(Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;)V
    .locals 3
    .param p1, "authInformation"    # Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/scloud/quota/QuotaException;
        }
    .end annotation

    .prologue
    .line 284
    monitor-enter p0

    :try_start_0
    const-string v0, "AuthManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[updateAuthInformation], getAuthInformation : STATUS = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;->status:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    iget-object v0, p1, Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;->baseUrl:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mBaseUrlPrimary:Ljava/lang/String;

    .line 289
    iget-object v0, p1, Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;->accessToken:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mAccessToken:Ljava/lang/String;

    .line 291
    iget-object v0, p1, Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;->userId:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mUserId:Ljava/lang/String;

    .line 293
    iget-object v0, p1, Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;->regId:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mRegistrationID:Ljava/lang/String;

    .line 296
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "&uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mUserId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&access_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mAccessToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mGetAPIparms:Ljava/lang/String;

    .line 298
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "&uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mUserId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&access_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mAccessToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mGetAPIparmsStorage:Ljava/lang/String;

    .line 300
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mGetAPIparms:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&did="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mRegistrationID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mPutAPIparms:Ljava/lang/String;

    .line 301
    iget-object v0, p1, Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;->baseUrl:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mBackupBaseUrlPrimary:Ljava/lang/String;

    .line 303
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mGetAPIparms:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mBackupGetAPIparms:Ljava/lang/String;

    .line 305
    iget-object v0, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mPutAPIparms:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/scloud/quota/AuthManager;->mBackupPutAPIparms:Ljava/lang/String;

    .line 307
    iget v0, p1, Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;->status:I

    invoke-direct {p0, v0}, Lcom/samsung/android/scloud/quota/AuthManager;->handleResult(I)V

    .line 309
    const-string v0, "AuthManager"

    const-string v1, "[updateAuthInformation] : END"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/quota/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 310
    monitor-exit p0

    return-void

    .line 284
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
