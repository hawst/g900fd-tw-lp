.class Lcom/samsung/android/scloud/quota/BackupList$4;
.super Ljava/lang/Object;
.source "BackupList.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/quota/BackupList;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/quota/BackupList;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/quota/BackupList;)V
    .locals 0

    .prologue
    .line 218
    iput-object p1, p0, Lcom/samsung/android/scloud/quota/BackupList$4;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 5
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 224
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList$4;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-boolean v1, v1, Lcom/samsung/android/scloud/quota/BackupList;->flag:Z

    if-nez v1, :cond_1

    if-eqz p2, :cond_1

    .line 227
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/quota/BackupList$ViewHolder;

    .line 228
    .local v0, "holder":Lcom/samsung/android/scloud/quota/BackupList$ViewHolder;
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList$4;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    # invokes: Lcom/samsung/android/scloud/quota/BackupList;->registerSpinner()V
    invoke-static {v1}, Lcom/samsung/android/scloud/quota/BackupList;->access$200(Lcom/samsung/android/scloud/quota/BackupList;)V

    .line 229
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList$4;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    # invokes: Lcom/samsung/android/scloud/quota/BackupList;->isDeviceUseDropdownMenu()Z
    invoke-static {v1}, Lcom/samsung/android/scloud/quota/BackupList;->access$500(Lcom/samsung/android/scloud/quota/BackupList;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 231
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList$4;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    invoke-virtual {v1}, Lcom/samsung/android/scloud/quota/BackupList;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 232
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList$4;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    invoke-virtual {v1}, Lcom/samsung/android/scloud/quota/BackupList;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 233
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList$4;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    invoke-virtual {v1}, Lcom/samsung/android/scloud/quota/BackupList;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 234
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList$4;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v1, v1, Lcom/samsung/android/scloud/quota/BackupList;->mCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 235
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList$4;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v1, v1, Lcom/samsung/android/scloud/quota/BackupList;->mCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v1, v4}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 237
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList$4;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iput-boolean v3, v1, Lcom/samsung/android/scloud/quota/BackupList;->longFlag:Z

    .line 238
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList$4;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iput-boolean v4, v1, Lcom/samsung/android/scloud/quota/BackupList;->flag:Z

    .line 241
    iget-object v1, v0, Lcom/samsung/android/scloud/quota/BackupList$ViewHolder;->cb:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-nez v1, :cond_2

    .line 242
    iget-object v1, v0, Lcom/samsung/android/scloud/quota/BackupList$ViewHolder;->cb:Landroid/widget/CheckBox;

    invoke-virtual {v1, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 243
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList$4;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v1, v1, Lcom/samsung/android/scloud/quota/BackupList;->list:Landroid/widget/ListView;

    invoke-virtual {v1, p3, v4}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 244
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList$4;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v1, v1, Lcom/samsung/android/scloud/quota/BackupList;->mListSelected:[Z

    aput-boolean v4, v1, p3

    .line 245
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList$4;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    # operator++ for: Lcom/samsung/android/scloud/quota/BackupList;->count:I
    invoke-static {v1}, Lcom/samsung/android/scloud/quota/BackupList;->access$408(Lcom/samsung/android/scloud/quota/BackupList;)I

    .line 253
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList$4;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    # getter for: Lcom/samsung/android/scloud/quota/BackupList;->count:I
    invoke-static {v1}, Lcom/samsung/android/scloud/quota/BackupList;->access$400(Lcom/samsung/android/scloud/quota/BackupList;)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/scloud/quota/BackupList$4;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v2, v2, Lcom/samsung/android/scloud/quota/BackupList;->list:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getCount()I

    move-result v2

    if-ne v1, v2, :cond_3

    .line 254
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList$4;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v1, v1, Lcom/samsung/android/scloud/quota/BackupList;->mCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v1, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 259
    .end local v0    # "holder":Lcom/samsung/android/scloud/quota/BackupList$ViewHolder;
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList$4;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    invoke-virtual {v1}, Lcom/samsung/android/scloud/quota/BackupList;->setDropDownList()V

    .line 260
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList$4;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    invoke-virtual {v1}, Lcom/samsung/android/scloud/quota/BackupList;->invalidateOptionsMenu()V

    .line 261
    return v4

    .line 247
    .restart local v0    # "holder":Lcom/samsung/android/scloud/quota/BackupList$ViewHolder;
    :cond_2
    iget-object v1, v0, Lcom/samsung/android/scloud/quota/BackupList$ViewHolder;->cb:Landroid/widget/CheckBox;

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 248
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList$4;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v1, v1, Lcom/samsung/android/scloud/quota/BackupList;->list:Landroid/widget/ListView;

    invoke-virtual {v1, p3, v3}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 249
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList$4;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v1, v1, Lcom/samsung/android/scloud/quota/BackupList;->mListSelected:[Z

    aput-boolean v3, v1, p3

    .line 250
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList$4;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    # operator-- for: Lcom/samsung/android/scloud/quota/BackupList;->count:I
    invoke-static {v1}, Lcom/samsung/android/scloud/quota/BackupList;->access$410(Lcom/samsung/android/scloud/quota/BackupList;)I

    goto :goto_0

    .line 256
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/scloud/quota/BackupList$4;->this$0:Lcom/samsung/android/scloud/quota/BackupList;

    iget-object v1, v1, Lcom/samsung/android/scloud/quota/BackupList;->mCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_1
.end method
