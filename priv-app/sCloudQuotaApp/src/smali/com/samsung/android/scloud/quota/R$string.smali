.class public final Lcom/samsung/android/scloud/quota/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/quota/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final Memo:I = 0x7f070000

.field public static final Quota_dialog_message:I = 0x7f070001

.field public static final Quota_dialog_message2:I = 0x7f070002

.field public static final Quota_warning:I = 0x7f070003

.field public static final Quota_warning_message:I = 0x7f070004

.field public static final Quota_warning_message_2:I = 0x7f070005

.field public static final alert_message:I = 0x7f070006

.field public static final alert_message_old:I = 0x7f070007

.field public static final alert_tile:I = 0x7f070008

.field public static final app_name:I = 0x7f070009

.field public static final auth_token_toast:I = 0x7f07000a

.field public static final backup_delete_dialog:I = 0x7f07000b

.field public static final backup_delete_failed_message:I = 0x7f07000c

.field public static final backup_delete_message:I = 0x7f07000d

.field public static final backup_detail_dialog:I = 0x7f07000e

.field public static final backup_detail_message:I = 0x7f07000f

.field public static final backupdata:I = 0x7f070010

.field public static final bookmarks:I = 0x7f070011

.field public static final calculating_size:I = 0x7f070012

.field public static final calendar_Preference:I = 0x7f070013

.field public static final cancel_reserve_plan_button:I = 0x7f070014

.field public static final cancel_reserve_plan_title:I = 0x7f070015

.field public static final change_plan:I = 0x7f070016

.field public static final contacts:I = 0x7f070017

.field public static final credit_card_reregister_msg:I = 0x7f070018

.field public static final current_plan:I = 0x7f070019

.field public static final current_plan_message:I = 0x7f07001a

.field public static final data_deletion_large_message:I = 0x7f07001b

.field public static final data_deletion_message:I = 0x7f07001c

.field public static final data_deletion_notice:I = 0x7f07001d

.field public static final delete_backup_dialog_message:I = 0x7f07001e

.field public static final delete_backup_dialog_title:I = 0x7f07001f

.field public static final delete_text:I = 0x7f070020

.field public static final deselect_all:I = 0x7f070021

.field public static final details_message_dialog:I = 0x7f070022

.field public static final dialog_mesage:I = 0x7f070023

.field public static final dialog_title:I = 0x7f070024

.field public static final done:I = 0x7f070025

.field public static final downgrade_failed_message:I = 0x7f070026

.field public static final downgrade_failed_notice:I = 0x7f070027

.field public static final downgrade_title:I = 0x7f070028

.field public static final excess_data_message:I = 0x7f070029

.field public static final excess_data_notice:I = 0x7f07002a

.field public static final familystory:I = 0x7f07002b

.field public static final free_plan_price:I = 0x7f07002c

.field public static final free_plan_value:I = 0x7f07002d

.field public static final free_user_reserve_error_msg:I = 0x7f07002e

.field public static final getting_plan_details:I = 0x7f07002f

.field public static final hello_world:I = 0x7f070030

.field public static final kmemo:I = 0x7f070031

.field public static final menu_settings:I = 0x7f070032

.field public static final new_plan_message:I = 0x7f070033

.field public static final next_payment_message:I = 0x7f070034

.field public static final next_plan_downgrade_message:I = 0x7f070035

.field public static final next_plan_message:I = 0x7f070036

.field public static final next_plan_upgrade_message:I = 0x7f070037

.field public static final no_backup_data:I = 0x7f070038

.field public static final no_purchase_history:I = 0x7f070039

.field public static final no_refund_after_free_quota:I = 0x7f07003a

.field public static final no_refund_after_validity:I = 0x7f07003b

.field public static final notification_message:I = 0x7f07003c

.field public static final other_data:I = 0x7f07003d

.field public static final payment_method:I = 0x7f07003e

.field public static final payment_validity_message:I = 0x7f07003f

.field public static final personal_data_management_usage:I = 0x7f070040

.field public static final personal_data_usage:I = 0x7f070041

.field public static final plan_automatic_renew_message:I = 0x7f070042

.field public static final plan_automatic_renew_message_dialog:I = 0x7f070043

.field public static final plan_downgrade_notice:I = 0x7f070044

.field public static final plan_downgraded_notice:I = 0x7f070045

.field public static final plan_ending_message:I = 0x7f070046

.field public static final plan_header:I = 0x7f070047

.field public static final plan_renew_notice:I = 0x7f070048

.field public static final plan_renewed_notice:I = 0x7f070049

.field public static final plan_ugrade_notice:I = 0x7f07004a

.field public static final plan_ugraded_notice:I = 0x7f07004b

.field public static final plan_validity_message:I = 0x7f07004c

.field public static final popup_using_checkbox:I = 0x7f07004d

.field public static final post_fail_message:I = 0x7f07004e

.field public static final processing_dialog_string:I = 0x7f07004f

.field public static final processing_request_message:I = 0x7f070050

.field public static final prorated_price_message:I = 0x7f070051

.field public static final purchase_button_string:I = 0x7f070052

.field public static final purchase_hist_dialog_message:I = 0x7f070053

.field public static final purchase_history:I = 0x7f070054

.field public static final purchase_notice_string:I = 0x7f070055

.field public static final purchase_notice_title:I = 0x7f070056

.field public static final quota_full_dialog_message:I = 0x7f070057

.field public static final quota_full_message:I = 0x7f070058

.field public static final quota_full_title:I = 0x7f070059

.field public static final refund_title:I = 0x7f07005a

.field public static final renew_failed_message:I = 0x7f07005b

.field public static final renewal_failed_notice:I = 0x7f07005c

.field public static final scloud_network_fail:I = 0x7f07005d

.field public static final scloud_network_retry_fail:I = 0x7f07005e

.field public static final scrapBook:I = 0x7f07005f

.field public static final select_all:I = 0x7f070060

.field public static final select_all_title:I = 0x7f070061

.field public static final select_plan_dialog_header:I = 0x7f070062

.field public static final selected:I = 0x7f070063

.field public static final selected_text:I = 0x7f070064

.field public static final server_connection_failed:I = 0x7f070065

.field public static final show_more_button:I = 0x7f070066

.field public static final smemo_title:I = 0x7f070067

.field public static final snote:I = 0x7f070068

.field public static final snote3:I = 0x7f070069

.field public static final splanner:I = 0x7f07006a

.field public static final stms_appgroup:I = 0x7f07006b

.field public static final stms_version:I = 0x7f07006c

.field public static final storage_purchase_title:I = 0x7f07006d

.field public static final subscription_cancel_large_message:I = 0x7f07006e

.field public static final subscription_cancel_message:I = 0x7f07006f

.field public static final subscription_cancel_notice:I = 0x7f070070

.field public static final subscription_cancelled_message:I = 0x7f070071

.field public static final subscription_cancelled_notice:I = 0x7f070072

.field public static final subscription_end_date_message:I = 0x7f070073

.field public static final title_activity_quota:I = 0x7f070074

.field public static final title_purchase_history:I = 0x7f070075

.field public static final title_purchase_storage:I = 0x7f070076

.field public static final total_space_title:I = 0x7f070077

.field public static final total_usage:I = 0x7f070078

.field public static final total_used:I = 0x7f070079

.field public static final unable_to_delete:I = 0x7f07007a

.field public static final unable_to_open_file:I = 0x7f07007b

.field public static final unable_to_open_file_old:I = 0x7f07007c

.field public static final unable_to_open_file_title:I = 0x7f07007d

.field public static final unknown_value:I = 0x7f07007e

.field public static final upgrade_now:I = 0x7f07007f

.field public static final upgrade_now_title:I = 0x7f070080

.field public static final upgrade_title:I = 0x7f070081

.field public static final usage:I = 0x7f070082

.field public static final used_title:I = 0x7f070083


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
