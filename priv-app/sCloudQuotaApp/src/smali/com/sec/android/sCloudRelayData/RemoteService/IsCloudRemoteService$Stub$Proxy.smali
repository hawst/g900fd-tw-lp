.class Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService$Stub$Proxy;
.super Ljava/lang/Object;
.source "IsCloudRemoteService.java"

# interfaces
.implements Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .locals 0
    .param p1, "remote"    # Landroid/os/IBinder;

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput-object p1, p0, Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    .line 70
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    return-object v0
.end method

.method public getAuthInformation(Z)Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;
    .locals 6
    .param p1, "resetData"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 81
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 82
    .local v0, "_data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 85
    .local v1, "_reply":Landroid/os/Parcel;
    :try_start_0
    const-string v5, "com.sec.android.sCloudRelayData.RemoteService.IsCloudRemoteService"

    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 86
    if-eqz p1, :cond_0

    :goto_0
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 87
    iget-object v3, p0, Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 88
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    .line 89
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_1

    .line 90
    sget-object v3, Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 97
    .local v2, "_result":Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;
    :goto_1
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 98
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 100
    return-object v2

    .end local v2    # "_result":Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;
    :cond_0
    move v3, v4

    .line 86
    goto :goto_0

    .line 93
    :cond_1
    const/4 v2, 0x0

    .restart local v2    # "_result":Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;
    goto :goto_1

    .line 96
    .end local v2    # "_result":Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;
    :catchall_0
    move-exception v3

    .line 97
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 98
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 99
    throw v3
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    const-string v0, "com.sec.android.sCloudRelayData.RemoteService.IsCloudRemoteService"

    return-object v0
.end method
