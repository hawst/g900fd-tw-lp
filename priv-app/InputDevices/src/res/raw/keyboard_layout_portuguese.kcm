# Copyright (C) 2012 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#
# Portuguese keyboard layout.
#

type OVERLAY

map key 12 SLASH
map key 53 MINUS
map key 86 PLUS

### ROW 1

key GRAVE {
    label:                              '\\'
    base:                               '\\'
    shift:                              '|'
}

key 1 {
    label:                              '1'
    base:                               '1'
    shift:                              '!'
}

key 2 {
    label:                              '2'
    base:                               '2'
    shift:                              '"'
    ralt:                               '@'
}

key 3 {
    label:                              '3'
    base:                               '3'
    shift:                              '#'
    ralt:                               '\u00a3'
}

key 4 {
    label:                              '4'
    base:                               '4'
    shift:                              '$'
    ralt:                               '\u00a7'
}

key 5 {
    label:                              '5'
    base:                               '5'
    shift:                              '%'
}

key 6 {
    label:                              '6'
    base:                               '6'
    shift:                              '&'
}

key 7 {
    label:                              '7'
    base:                               '7'
    shift:                              '/'
    ralt:                               '{'
}

key 8 {
    label:                              '8'
    base:                               '8'
    shift:                              '('
    ralt:                               '['
}

key 9 {
    label:                              '9'
    base:                               '9'
    shift:                              ')'
    ralt:                               ']'
}

key 0 {
    label:                              '0'
    base:                               '0'
    shift:                              '='
    ralt:                               '}'
}

key SLASH {
    label:                              '\''
    base:                               '\''
    shift:                              '?'
}

key EQUALS {
    label:                              '\u00ab'
    base:                               '\u00ab'
    shift:                              '\u00bb'
}

### ROW 2

key Q {
    label:                              'Q'
    base:                               'q'
    shift, capslock:                    'Q'
    capslock+shift:                     'q'   
}

key W {
    label:                              'W'
    base:                               'w'
    shift, capslock:                    'W'
    capslock+shift:                     'w'  
}

key E {
    label:                              'E'
    base:                               'e'
    shift, capslock:                    'E'
    ralt:                               '\u20ac'
    capslock+shift:                     'e'    	
}

key R {
    label:                              'R'
    base:                               'r'
    shift, capslock:                    'R'
    capslock+shift:                     'r'    	
}

key T {
    label:                              'T'
    base:                               't'
    shift, capslock:                    'T'
    capslock+shift:                     't'    	
}

key Y {
    label:                              'Y'
    base:                               'y'
    shift, capslock:                    'Y'
    capslock+shift:                     'y'    
}

key U {
    label:                              'U'
    base:                               'u'
    shift, capslock:                    'U'
    capslock+shift:                     'u'    	
}

key I {
    label:                              'I'
    base:                               'i'
    shift, capslock:                    'I'
    capslock+shift:                     'i'    	
}

key O {
    label:                              'O'
    base:                               'o'
    shift, capslock:                    'O'
    capslock+shift:                     'o'    	
}

key P {
    label:                              'P'
    base:                               'p'
    shift, capslock:                    'P'
    capslock+shift:                     'p'    	
}

key LEFT_BRACKET {
    label:                              '+'
    base:                               '+'
    shift:                              '*'
    ralt:                               '\u0308'
}

key RIGHT_BRACKET {
    label:                              '\u00b4'
    base:                               '\u0301'
    shift:                              '\u0300'
}

### ROW 3

key A {
    label:                              'A'
    base:                               'a'
    shift, capslock:                    'A'
    capslock+shift:                     'a'    
}

key S {
    label:                              'S'
    base:                               's'
    shift, capslock:                    'S'
    capslock+shift:                     's'    	
}

key D {
    label:                              'D'
    base:                               'd'
    shift, capslock:                    'D'
    capslock+shift:                     'd'    	
}

key F {
    label:                              'F'
    base:                               'f'
    shift, capslock:                    'F'
    capslock+shift:                     'f'  
}

key G {
    label:                              'G'
    base:                               'g'
    shift, capslock:                    'G'
    capslock+shift:                     'g'   
}

key H {
    label:                              'H'
    base:                               'h'
    shift, capslock:                    'H'
    capslock+shift:                     'h'    	
}

key J {
    label:                              'J'
    base:                               'j'
    shift, capslock:                    'J'
    capslock+shift:                     'j'    	
}

key K {
    label:                              'K'
    base:                               'k'
    shift, capslock:                    'K'
    capslock+shift:                     'k'    
}

key L {
    label:                              'L'
    base:                               'l'
    shift, capslock:                    'L'
    capslock+shift:                     'l'  
}

key SEMICOLON {
    label:                              '\u00c7'
    base:                               '\u00e7'
    shift, capslock:                    '\u00c7'
    capslock+shift:                     '\u00e7'
}

key APOSTROPHE {
    label:                              '\u00ba'
    base:                               '\u00ba'
    shift:                              '\u00aa'
}

key BACKSLASH {
    label:                              '\u02dc'
    base:                               '\u0303'
    shift:                              '\u0302'
}

### ROW 4

key PLUS {
    label:                              '<'
    base:                               '<'
    shift:                              '>'
    ralt:                               '\\'
}

key Z {
    label:                              'Z'
    base:                               'z'
    shift, capslock:                    'Z'
    capslock+shift:                     'z'    	
}

key X {
    label:                              'X'
    base:                               'x'
    shift, capslock:                    'X'
    capslock+shift:                     'x'    	
}

key C {
    label:                              'C'
    base:                               'c'
    shift, capslock:                    'C'
    capslock+shift:                     'c'    	
}

key V {
    label:                              'V'
    base:                               'v'
    shift, capslock:                    'V'
    capslock+shift:                     'v'  
}

key B {
    label:                              'B'
    base:                               'b'
    shift, capslock:                    'B'
    capslock+shift:                     'b'   
}

key N {
    label:                              'N'
    base:                               'n'
    shift, capslock:                    'N'
    capslock+shift:                     'n'  
}

key M {
    label:                              'M'
    base:                               'm'
    shift, capslock:                    'M'
    capslock+shift:                     'm'    	
}

key COMMA {
    label:                              ','
    base:                               ','
    shift:                              ';'
}

key PERIOD {
    label:                              '.'
    base:                               '.'
    shift:                              ':'
}

key MINUS {
    label:                              '-'
    base:                               '-'
    shift:                              '_'
}
