# Copyright (C) 2012 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#
# Slovak (EU - qwerty) keyboard layout.
#

type OVERLAY

map key 86 PLUS

### ROW 1

key GRAVE {
    label:                              ';'
    base:                               ';'
    shift:                              '\u00b0'
    ctrl+shift:                         'L'
}

key 1 {
    label:                              '1'
    base:                               '+'
    shift:                              '1'
    ralt:                               '~'
}

key 2 {
    label:                              '2'
    base:                               '\u013e'
    shift:                              '2'
    ralt:                               '\u02c7'
}

key 3 {
    label:                              '3'
    base:                               '\u0161'
    shift:                              '3'
    ralt:                               '\u0302'
#should be 0x005e
}

key 4 {
    label:                              '4'
    base:                               '\u010d'
    shift:                              '4'
    ralt:                               '\u02d8'
}

key 5 {
    label:                              '5'
    base:                               '\u0165'
    shift:                              '5'
    ralt:                               '\u00b0'
}

key 6 {
    label:                              '6'
    base:                               '\u017e'
    shift:                              '6'
    ralt:                               '\u02db'
}

key 7 {
    label:                              '7'
    base:                               '\u00fd'
    shift:                              '7'
    ralt:                               '\u0300'
#should be 0x0060
}

key 8 {
    label:                              '8'
    base:                               '\u00e1'
    shift:                              '8'
    ralt:                               '\u02d9'
}

key 9 {
    label:                              '9'
    base:                               '\u00ed'
    shift:                              '9'
    ralt:                               '\u0301'
#should be 0x00b4
}

key 0 {
    label:                              '0'
    base:                               '\u00e9'
    shift:                              '0'
    ralt:                               '\u02dd'
}

key MINUS {
    label:                              '='
    base:                               '='
    shift:                              '%'
    ralt:                               '\u0308'
#should be 0x00a8
}

key EQUALS {
    label:                              '\u00b4'
    base:                               '\u0301'
    shift:                              '\u030c'
    ralt:                               '\u00b8'
}

### ROW 2

key Q {
    label:                              'Q'
    base:                               'q'
    shift, capslock:                    'Q'
    ralt:                               '\\'
    capslock+shift:                     'q'  
}

key W {
    label:                              'W'
    base:                               'w'
    shift, capslock:                    'W'
    ralt:                               '|'
    capslock+shift:                     'w'  
}

key E {
    label:                              'E'
    base:                               'e'
    shift, capslock:                    'E'
    ralt:                               '\u20ac'
    capslock+shift:                     'e'    	
}

key R {
    label:                              'R'
    base:                               'r'
    shift, capslock:                    'R'
    capslock+shift:                     'r'    	
}

key T {
    label:                              'T'
    base:                               't'
    shift, capslock:                    'T'
    capslock+shift:                     't'    	
}

key Y {
    label:                              'Y'
    base:                               'y'
    shift, capslock:                    'Y'
    capslock+shift:                     'y'    
}

key U {
    label:                              'U'
    base:                               'u'
    shift, capslock:                    'U'
    capslock+shift:                     'u'    	
}

key I {
    label:                              'I'
    base:                               'i'
    shift, capslock:                    'I'
    capslock+shift:                     'i'    	
}

key O {
    label:                              'O'
    base:                               'o'
    shift, capslock:                    'O'
    capslock+shift:                     'o'    	
}

key P {
    label:                              'P'
    base:                               'p'
    shift, capslock:                    'P'
    ralt:                               '\''
    capslock+shift:                     'p'    	
}

key LEFT_BRACKET {
    label:                              '\u00fa'
    base:                               '\u00fa'
    shift:                              '/'
    ralt:                               '\u00f7'
}

key RIGHT_BRACKET {
    label:                              '\u00e4'
    base:                               '\u00e4'
    shift:                              '('
    ralt:                               '\u00d7'
}

### ROW 3

key A {
    label:                              'A'
    base:                               'a'
    shift, capslock:                    'A'
    capslock+shift:                     'a'    
}

key S {
    label:                              'S'
    base:                               's'
    shift, capslock:                    'S'
    ralt:                               '\u0111'
    capslock+shift:                     's' 
}

key D {
    label:                              'D'
    base:                               'd'
    shift, capslock:                    'D'
    ralt:                               '\u0110'
    capslock+shift:                     'd'  
}

key F {
    label:                              'F'
    base:                               'f'
    shift, capslock:                    'F'
    ralt:                               '['
    capslock+shift:                     'f'  
}

key G {
    label:                              'G'
    base:                               'g'
    shift, capslock:                    'G'
    ralt:                               ']'
    capslock+shift:                     'g'  
}

key H {
    label:                              'H'
    base:                               'h'
    shift, capslock:                    'H'
    capslock+shift:                     'h'    	
}

key J {
    label:                              'J'
    base:                               'j'
    shift, capslock:                    'J'
    capslock+shift:                     'j'    	
}

key K {
    label:                              'K'
    base:                               'k'
    shift, capslock:                    'K'
    ralt:                               '\u0142'
    capslock+shift:                     'k'    
}

key L {
    label:                              'L'
    base:                               'l'
    shift, capslock:                    'L'
    ralt:                               '\u0141'
    capslock+shift:                     'l'  
}

key SEMICOLON {
    label:                              '\u00f4'
    base:                               '\u00f4'
    shift:                              '"'
    ralt:                               '$'
}

key APOSTROPHE {
    label:                              '\u00a7'
    base:                               '\u00a7'
    shift:                              '!'
    ralt:                               '\u00df'
}

key BACKSLASH {
    label:                              '\u0148'
    base:                               '\u0148'
    shift:                              ')'
    ralt:                               '\u00a4'
}

### ROW 4

key PLUS {
    label:                              '&'
    base:                               '&'
    shift:                              '*'
    ralt:                               '<'
}

key Z {
    label:                              'Z'
    base:                               'z'
    shift, capslock:                    'Z'
    ralt:                               '>'
    capslock+shift:                     'z'    	
}

key X {
    label:                              'X'
    base:                               'x'
    shift, capslock:                    'X'
    ralt:                               '#'
    capslock+shift:                     'x'   
}

key C {
    label:                              'C'
    base:                               'c'
    shift, capslock:                    'C'
    ralt:                               '&'
    capslock+shift:                     'c'  
}

key V {
    label:                              'V'
    base:                               'v'
    shift, capslock:                    'V'
    ralt:                               '@'
    capslock+shift:                     'v' 
}

key B {
    label:                              'B'
    base:                               'b'
    shift, capslock:                    'B'
    ralt:                               '{'
    capslock+shift:                     'b'   
}

key N {
    label:                              'N'
    base:                               'n'
    shift, capslock:                    'N'
    ralt:                               '}'
    capslock+shift:                     'n'  
}

key M {
    label:                              'M'
    base:                               'm'
    shift, capslock:                    'M'
    capslock+shift:                     'm'    	
}

key COMMA {
    label:                              ','
    base:                               ','
    shift:                              '?'
    ralt:                               '<'
}

key PERIOD {
    label:                              '.'
    base:                               '.'
    shift:                              ':'
    ralt:                               '>'
}

key SLASH {
    label:                              '-'
    base:                               '-'
    shift:                              '_'
    ralt:                               '*'
}
