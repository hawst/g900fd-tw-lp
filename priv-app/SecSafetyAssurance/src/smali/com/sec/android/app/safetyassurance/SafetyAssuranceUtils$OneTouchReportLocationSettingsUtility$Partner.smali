.class public final Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils$OneTouchReportLocationSettingsUtility$Partner;
.super Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils$OneTouchReportLocationSettingsUtility$NameValueTable;
.source "SafetyAssuranceUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils$OneTouchReportLocationSettingsUtility;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Partner"
.end annotation


# static fields
.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final NETWORK_LOCATION_OPT_IN:Ljava/lang/String; = "network_location_opt_in"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 324
    const-string v0, "content://com.google.settings/partner"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils$OneTouchReportLocationSettingsUtility$Partner;->CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 323
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils$OneTouchReportLocationSettingsUtility$NameValueTable;-><init>()V

    return-void
.end method

.method public static putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    .locals 2
    .param p0, "cr"    # Landroid/content/ContentResolver;
    .param p1, "keyStr"    # Ljava/lang/String;
    .param p2, "value"    # I

    .prologue
    .line 328
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 329
    .local v0, "str":Ljava/lang/String;
    invoke-static {p0, p1, v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils$OneTouchReportLocationSettingsUtility$Partner;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method public static putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p0, "cr"    # Landroid/content/ContentResolver;
    .param p1, "keyStr"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 333
    sget-object v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils$OneTouchReportLocationSettingsUtility$Partner;->CONTENT_URI:Landroid/net/Uri;

    .line 334
    .local v0, "localUri":Landroid/net/Uri;
    invoke-static {p0, v0, p1, p2}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils$OneTouchReportLocationSettingsUtility$Partner;->putString(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    return v1
.end method
