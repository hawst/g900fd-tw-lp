.class Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$5;
.super Landroid/os/Handler;
.source "SafetyAssuranceFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)V
    .locals 0

    .prologue
    .line 889
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$5;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const v7, 0x7f090040

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 892
    const-string v1, "SafetyAssuranceFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mCompleteHandler = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 893
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$5;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->isAdded()Z

    move-result v1

    if-nez v1, :cond_0

    .line 943
    :goto_0
    return-void

    .line 897
    :cond_0
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 899
    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$5;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 900
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->featureSupportNewUX()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$5;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    # getter for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->isVZW:Z
    invoke-static {v1}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->access$1900(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)Z

    move-result v1

    if-eqz v1, :cond_1

    const v1, 0x7f090018

    :goto_1
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->featureSupportNewUX()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$5;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    # getter for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->isVZW:Z
    invoke-static {v1}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->access$1900(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$5;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    # getter for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->access$900(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f09000d

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$5;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    # getter for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->access$900(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v1, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :goto_2
    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/high16 v2, 0x1040000

    new-instance v3, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$5$3;

    invoke-direct {v3, p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$5$3;-><init>(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$5;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f09000b

    new-instance v3, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$5$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$5$2;-><init>(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$5;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$5$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$5$1;-><init>(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$5;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 937
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0

    .line 900
    :cond_1
    const v1, 0x7f090019

    goto :goto_1

    :cond_2
    const v1, 0x7f090027

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$5;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    # getter for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->access$900(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f09000e

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$5;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    # getter for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->access$900(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v1, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$5;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    # getter for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->access$900(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f090050

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 897
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
