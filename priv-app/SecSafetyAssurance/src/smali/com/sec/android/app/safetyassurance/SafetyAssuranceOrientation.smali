.class public Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;
.super Ljava/lang/Object;
.source "SafetyAssuranceOrientation.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SafetyAssuranceOrientation"


# instance fields
.field private mCameraInfo:[Landroid/hardware/Camera$CameraInfo;

.field private mContext:Landroid/content/Context;

.field private mLastOrientation:I

.field private mNumberOfCameras:I

.field private mOrientationListener:Landroid/view/OrientationEventListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;->mOrientationListener:Landroid/view/OrientationEventListener;

    .line 15
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;->mLastOrientation:I

    .line 18
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;->mContext:Landroid/content/Context;

    .line 19
    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;->mNumberOfCameras:I

    .line 20
    iget v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;->mNumberOfCameras:I

    new-array v1, v1, [Landroid/hardware/Camera$CameraInfo;

    iput-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;->mCameraInfo:[Landroid/hardware/Camera$CameraInfo;

    .line 21
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;->mNumberOfCameras:I

    if-ge v0, v1, :cond_0

    .line 22
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;->mCameraInfo:[Landroid/hardware/Camera$CameraInfo;

    new-instance v2, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v2}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    aput-object v2, v1, v0

    .line 23
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;->mCameraInfo:[Landroid/hardware/Camera$CameraInfo;

    aget-object v1, v1, v0

    invoke-static {v0, v1}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 21
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 25
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;I)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;
    .param p1, "x1"    # I

    .prologue
    .line 8
    invoke-direct {p0, p1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;->roundOrientation(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;
    .param p1, "x1"    # I

    .prologue
    .line 8
    invoke-direct {p0, p1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;->setLastOrientation(I)V

    return-void
.end method

.method private declared-synchronized getLastOrientation()I
    .locals 1

    .prologue
    .line 76
    monitor-enter p0

    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 77
    :try_start_1
    iget v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;->mLastOrientation:I

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return v0

    .line 78
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 76
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private roundOrientation(I)I
    .locals 1
    .param p1, "orientation"    # I

    .prologue
    .line 66
    add-int/lit8 v0, p1, 0x2d

    div-int/lit8 v0, v0, 0x5a

    mul-int/lit8 v0, v0, 0x5a

    rem-int/lit16 v0, v0, 0x168

    return v0
.end method

.method private setLastOrientation(I)V
    .locals 1
    .param p1, "lastOrientation"    # I

    .prologue
    .line 70
    monitor-enter p0

    .line 71
    :try_start_0
    iput p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;->mLastOrientation:I

    .line 72
    monitor-exit p0

    .line 73
    return-void

    .line 72
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private startOrientationListener()V
    .locals 2

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;->mOrientationListener:Landroid/view/OrientationEventListener;

    if-nez v0, :cond_0

    .line 39
    const-string v0, "SafetyAssuranceOrientation"

    const-string v1, "startOrientationListener - START!!!"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    new-instance v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation$1;

    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation$1;-><init>(Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;->mOrientationListener:Landroid/view/OrientationEventListener;

    .line 52
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;->mOrientationListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->enable()V

    .line 53
    return-void
.end method

.method private stopOrientationListener()V
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;->mOrientationListener:Landroid/view/OrientationEventListener;

    if-eqz v0, :cond_0

    .line 57
    const-string v0, "SafetyAssuranceOrientation"

    const-string v1, "stopOrientationListener - END!!!"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;->mOrientationListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->disable()V

    .line 59
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;->mOrientationListener:Landroid/view/OrientationEventListener;

    .line 63
    :goto_0
    return-void

    .line 61
    :cond_0
    const-string v0, "SafetyAssuranceOrientation"

    const-string v1, "stopOrientationListener is null"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method protected calculateOrientationForPicture(I)I
    .locals 6
    .param p1, "CameraNumber"    # I

    .prologue
    const/4 v5, 0x1

    .line 82
    const/4 v2, 0x0

    .line 83
    .local v2, "rotation":I
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;->getLastOrientation()I

    move-result v1

    .line 85
    .local v1, "orientation":I
    const/4 v3, -0x1

    if-eq v1, v3, :cond_1

    .line 86
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;->mCameraInfo:[Landroid/hardware/Camera$CameraInfo;

    aget-object v0, v3, p1

    .line 87
    .local v0, "info":Landroid/hardware/Camera$CameraInfo;
    iget v3, v0, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-ne v3, v5, :cond_0

    .line 88
    iget v3, v0, Landroid/hardware/Camera$CameraInfo;->orientation:I

    sub-int/2addr v3, v1

    add-int/lit16 v3, v3, 0x168

    rem-int/lit16 v2, v3, 0x168

    .line 104
    :goto_0
    const-string v3, "SafetyAssuranceOrientation"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "calculateOrientationForPicture : criteria for camera - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    return v2

    .line 90
    :cond_0
    iget v3, v0, Landroid/hardware/Camera$CameraInfo;->orientation:I

    add-int/2addr v3, v1

    rem-int/lit16 v2, v3, 0x168

    goto :goto_0

    .line 95
    .end local v0    # "info":Landroid/hardware/Camera$CameraInfo;
    :cond_1
    const-string v3, "SafetyAssuranceOrientation"

    const-string v4, "calculateOrientationForPicture : Final orientation is ORIENTATION_UNKNOWN"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;->mCameraInfo:[Landroid/hardware/Camera$CameraInfo;

    aget-object v0, v3, p1

    .line 97
    .restart local v0    # "info":Landroid/hardware/Camera$CameraInfo;
    iget v3, v0, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-ne v3, v5, :cond_2

    .line 98
    const/16 v2, 0x10e

    goto :goto_0

    .line 101
    :cond_2
    const/16 v2, 0x5a

    goto :goto_0
.end method

.method public close()V
    .locals 2

    .prologue
    .line 33
    const-string v0, "SafetyAssuranceOrientation"

    const-string v1, "close"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 34
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;->stopOrientationListener()V

    .line 35
    return-void
.end method

.method public getCameraInfo()[Landroid/hardware/Camera$CameraInfo;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;->mCameraInfo:[Landroid/hardware/Camera$CameraInfo;

    return-object v0
.end method

.method public open()V
    .locals 2

    .prologue
    .line 28
    const-string v0, "SafetyAssuranceOrientation"

    const-string v1, "open"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 29
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;->startOrientationListener()V

    .line 30
    return-void
.end method
