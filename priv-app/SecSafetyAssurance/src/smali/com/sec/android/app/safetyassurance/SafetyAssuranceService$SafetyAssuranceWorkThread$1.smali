.class Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;
.super Landroid/os/Handler;
.source "SafetyAssuranceService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;


# direct methods
.method constructor <init>(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;)V
    .locals 0

    .prologue
    .line 2236
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 12
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 2238
    const-string v7, "SafetyAssuranceWorkThread"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "handleMessage:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p1, Landroid/os/Message;->what:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2240
    iget v7, p1, Landroid/os/Message;->what:I

    packed-switch v7, :pswitch_data_0

    .line 2568
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 2242
    :pswitch_1
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$400(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Landroid/content/Context;

    move-result-object v7

    const/4 v8, 0x3

    invoke-static {v7, v8}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->setAppState(Landroid/content/Context;I)V

    .line 2244
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    # setter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mCurrentTime:J
    invoke-static {v7, v8, v9}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$3102(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;J)J

    .line 2245
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$400(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Landroid/content/Context;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v8, v8, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mCurrentTime:J
    invoke-static {v8}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$3100(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)J

    move-result-wide v8

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->setReminderStartedTime(Landroid/content/Context;J)V

    .line 2246
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    new-instance v8, Ljava/util/Date;

    iget-object v9, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v9, v9, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mCurrentTime:J
    invoke-static {v9}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$3100(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)J

    move-result-wide v10

    invoke-direct {v8, v10, v11}, Ljava/util/Date;-><init>(J)V

    # setter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mCurrentDate:Ljava/util/Date;
    invoke-static {v7, v8}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$3202(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Ljava/util/Date;)Ljava/util/Date;

    .line 2249
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$400(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Landroid/content/Context;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v8, v8, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mService:Landroid/app/Service;
    invoke-static {v8}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$3300(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Landroid/app/Service;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v9, v9, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mCurrentTime:J
    invoke-static {v9}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$3100(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)J

    move-result-wide v10

    iget-object v9, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v9, v9, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIntervalTime:I
    invoke-static {v9}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$2500(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)I

    move-result v9

    invoke-static {v7, v8, v10, v11, v9}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceNoti;->addNotification(Landroid/content/Context;Landroid/app/Service;JI)V

    .line 2252
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    new-instance v8, Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;

    iget-object v9, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v9, v9, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;
    invoke-static {v9}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$400(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Landroid/content/Context;

    move-result-object v9

    invoke-direct {v8, v9}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mOrientation:Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;
    invoke-static {v7, v8}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$3402(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;)Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;

    .line 2253
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mOrientation:Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$3400(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;->open()V

    .line 2254
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    const/4 v8, 0x1

    # invokes: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->controlLocationSensor(I)V
    invoke-static {v7, v8}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$3500(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;I)V

    .line 2256
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # invokes: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->startLocationListener()V
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$3600(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)V

    .line 2258
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mPhoneState:Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$2600(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->start()V

    .line 2260
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$400(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceApp;->ifForegroundActivityIsSASettingThenItIsResumed(Landroid/content/Context;)V

    .line 2263
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    const/4 v8, 0x0

    # setter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVoiceRecordingUri:Landroid/net/Uri;
    invoke-static {v7, v8}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$1802(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Landroid/net/Uri;)Landroid/net/Uri;

    .line 2264
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRemoveVocRecOption:Z
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$1600(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Z

    move-result v7

    if-nez v7, :cond_1

    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSendVocRec:I
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$1700(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_1

    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIsSosMode:Z
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$800(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 2265
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    new-instance v8, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;

    iget-object v9, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v9, v9, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;
    invoke-static {v9}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$400(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Landroid/content/Context;

    move-result-object v9

    invoke-direct {v8, v9}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVoiceRecording:Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;
    invoke-static {v7, v8}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$3702(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;)Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;

    .line 2266
    new-instance v7, Ljava/lang/Thread;

    new-instance v8, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1$1;

    invoke-direct {v8, p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1$1;-><init>(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;)V

    invoke-direct {v7, v8}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v7}, Ljava/lang/Thread;->start()V

    .line 2283
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    const/4 v8, 0x0

    # setter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mFrontPictureUri:Landroid/net/Uri;
    invoke-static {v7, v8}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$1102(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Landroid/net/Uri;)Landroid/net/Uri;

    .line 2284
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    const/4 v8, 0x0

    # setter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRearPictureUri:Landroid/net/Uri;
    invoke-static {v7, v8}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$1202(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Landroid/net/Uri;)Landroid/net/Uri;

    .line 2285
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIsSosMode:Z
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$800(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 2286
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRemoveCameraOption:Z
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$600(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Z

    move-result v7

    if-nez v7, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSendPictures:I
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$1000(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_0

    .line 2287
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    const/4 v8, 0x1

    # invokes: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->takePictures(Z)V
    invoke-static {v7, v8}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$3900(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Z)V

    goto/16 :goto_0

    .line 2290
    :cond_2
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    iget-object v8, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v8, v8, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mImageUri:Landroid/net/Uri;
    invoke-static {v8}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$4000(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Landroid/net/Uri;

    move-result-object v8

    # setter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mFrontPictureUri:Landroid/net/Uri;
    invoke-static {v7, v8}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$1102(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Landroid/net/Uri;)Landroid/net/Uri;

    goto/16 :goto_0

    .line 2296
    :pswitch_2
    const-class v8, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    monitor-enter v8

    .line 2297
    :try_start_0
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$400(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->getAppState(Landroid/content/Context;)I

    move-result v2

    .line 2298
    .local v2, "curState":I
    if-eqz v2, :cond_3

    const/4 v7, 0x1

    if-ne v2, v7, :cond_4

    .line 2299
    :cond_3
    const-string v7, "SafetyAssuranceWorkThread"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "curState is ["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "] skip WT_ALARMACTION"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2300
    monitor-exit v8

    goto/16 :goto_0

    .line 2302
    .end local v2    # "curState":I
    :catchall_0
    move-exception v7

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v7

    .restart local v2    # "curState":I
    :cond_4
    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2305
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # invokes: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->updateRecipient()Z
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$4100(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 2307
    const-string v7, "SafetyAssuranceWorkThread"

    const-string v8, "recieverList is empty!!!"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2308
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$400(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Landroid/content/Context;

    move-result-object v7

    const/4 v8, 0x7

    invoke-static {v7, v8}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->setAppState(Landroid/content/Context;I)V

    goto/16 :goto_0

    .line 2312
    :cond_5
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$400(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Landroid/content/Context;

    move-result-object v7

    const/4 v8, 0x6

    invoke-static {v7, v8}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->setAppState(Landroid/content/Context;I)V

    .line 2330
    const-string v7, "SafetyAssuranceWorkThread"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "First message ["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v9, v9, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mFirstMessage:Z
    invoke-static {v9}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$4200(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Z

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2333
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    iget-object v8, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v8, v8, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mPhoneState:Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;
    invoke-static {v8}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$2600(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->getPhoneState()I

    move-result v8

    # setter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mCurrentPhoneState:I
    invoke-static {v7, v8}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$4302(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;I)I

    .line 2337
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->isChinaDevice()Z

    move-result v7

    if-eqz v7, :cond_8

    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->featureEnableMultiSim()Z

    move-result v7

    if-eqz v7, :cond_8

    .line 2338
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mCurrentPhoneState:I
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$4300(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)I

    move-result v7

    if-eqz v7, :cond_8

    .line 2339
    const/4 v6, 0x0

    .line 2340
    .local v6, "slot2Service":I
    const-string v7, "ril.ICC_TYPE"

    const-string v8, "0"

    invoke-static {v7, v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2341
    .local v0, "SimState":Ljava/lang/String;
    const-string v7, "ril.ICC_TYPE_1"

    const-string v8, "0"

    invoke-static {v7, v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2342
    .local v1, "SimState2":Ljava/lang/String;
    const-string v7, "SafetyAssuranceWorkThread"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "SimState2(ril.ICC_TYPE_1)="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2345
    const-string v7, "0"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_6

    .line 2346
    const/4 v6, 0x0

    .line 2348
    :cond_6
    if-nez v6, :cond_7

    .line 2349
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # setter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mCurrentPhoneState:I
    invoke-static {v7, v6}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$4302(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;I)I

    .line 2352
    :cond_7
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->featureEnableDualMode()Z

    move-result v7

    if-eqz v7, :cond_8

    .line 2353
    const/4 v5, 0x0

    .line 2355
    .local v5, "secondaryService":I
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->featureEnableDualModeGsmGsm()Z

    move-result v7

    if-eqz v7, :cond_c

    .line 2356
    const-string v7, "service.ServiceState.gsmSecond"

    const/4 v8, 0x0

    invoke-static {v7, v8}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v5

    .line 2361
    :goto_1
    if-nez v5, :cond_8

    .line 2362
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # setter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mCurrentPhoneState:I
    invoke-static {v7, v5}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$4302(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;I)I

    .line 2369
    .end local v0    # "SimState":Ljava/lang/String;
    .end local v1    # "SimState2":Ljava/lang/String;
    .end local v5    # "secondaryService":I
    .end local v6    # "slot2Service":I
    :cond_8
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mCurrentPhoneState:I
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$4300(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)I

    move-result v7

    if-eqz v7, :cond_9

    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mFirstMessage:Z
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$4200(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Z

    move-result v7

    if-eqz v7, :cond_11

    .line 2371
    :cond_9
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mFirstMessage:Z
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$4200(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Z

    move-result v7

    if-nez v7, :cond_a

    .line 2372
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    const/4 v8, 0x0

    # setter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRunningCounter:I
    invoke-static {v7, v8}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$2702(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;I)I

    .line 2373
    const-string v7, "SafetyAssuranceWorkThread"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "mFirstMessage is false set mRunningCounter to "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v9, v9, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRunningCounter:I
    invoke-static {v9}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$2700(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2377
    :cond_a
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mFirstMessage:Z
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$4200(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Z

    move-result v7

    if-nez v7, :cond_b

    .line 2378
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRemoveVocRecOption:Z
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$1600(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Z

    move-result v7

    if-nez v7, :cond_b

    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSendVocRec:I
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$1700(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_b

    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIsSosMode:Z
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$800(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Z

    move-result v7

    if-nez v7, :cond_b

    .line 2379
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    new-instance v8, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;

    iget-object v9, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v9, v9, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;
    invoke-static {v9}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$400(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Landroid/content/Context;

    move-result-object v9

    invoke-direct {v8, v9}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVoiceRecording:Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;
    invoke-static {v7, v8}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$3702(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;)Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;

    .line 2380
    new-instance v7, Ljava/lang/Thread;

    new-instance v8, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1$2;

    invoke-direct {v8, p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1$2;-><init>(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;)V

    invoke-direct {v7, v8}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v7}, Ljava/lang/Thread;->start()V

    .line 2397
    :cond_b
    const-string v7, "SafetyAssuranceWorkThread"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "First Message? : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v9, v9, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mFirstMessage:Z
    invoke-static {v9}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$4200(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Z

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2398
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRemoveCameraOption:Z
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$600(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Z

    move-result v7

    if-nez v7, :cond_10

    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSendPictures:I
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$1000(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_10

    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIsSosMode:Z
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$800(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Z

    move-result v7

    if-nez v7, :cond_10

    .line 2400
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mFirstMessage:Z
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$4200(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Z

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_e

    .line 2404
    :goto_2
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mCameraCapturer:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$4400(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->isTakingPicturesOver()Z

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_d

    .line 2405
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->mMyWorkHandler:Landroid/os/Handler;

    invoke-virtual {v7}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v4

    .line 2406
    .local v4, "newMsg":Landroid/os/Message;
    const/4 v7, 0x2

    iput v7, v4, Landroid/os/Message;->what:I

    .line 2407
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mWorkHandler:Landroid/os/Handler;
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$2400(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Landroid/os/Handler;

    move-result-object v7

    invoke-virtual {v7, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 2358
    .end local v4    # "newMsg":Landroid/os/Message;
    .restart local v0    # "SimState":Ljava/lang/String;
    .restart local v1    # "SimState2":Ljava/lang/String;
    .restart local v5    # "secondaryService":I
    .restart local v6    # "slot2Service":I
    :cond_c
    const-string v7, "service.ServiceState.gsm"

    const/4 v8, 0x0

    invoke-static {v7, v8}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v5

    goto/16 :goto_1

    .line 2412
    .end local v0    # "SimState":Ljava/lang/String;
    .end local v1    # "SimState2":Ljava/lang/String;
    .end local v5    # "secondaryService":I
    .end local v6    # "slot2Service":I
    :cond_d
    :try_start_2
    const-string v7, "SafetyAssuranceWorkThread"

    const-string v8, "WORKTHREAD_ALARMACTION - Wait until all pictures are taken"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2413
    monitor-enter p0
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    .line 2414
    const-wide/16 v8, 0x64

    :try_start_3
    invoke-virtual {p0, v8, v9}, Ljava/lang/Object;->wait(J)V

    .line 2415
    monitor-exit p0

    goto :goto_2

    :catchall_1
    move-exception v7

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    throw v7
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0

    .line 2416
    :catch_0
    move-exception v3

    .line 2417
    .local v3, "e":Ljava/lang/InterruptedException;
    const-string v7, "SafetyAssuranceWorkThread"

    const-string v8, "InterruptedException!"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2418
    invoke-virtual {v3}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_2

    .line 2426
    .end local v3    # "e":Ljava/lang/InterruptedException;
    :cond_e
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mOrientation:Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$3400(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;

    move-result-object v7

    if-nez v7, :cond_f

    .line 2427
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    new-instance v8, Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;

    iget-object v9, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v9, v9, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;
    invoke-static {v9}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$400(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Landroid/content/Context;

    move-result-object v9

    invoke-direct {v8, v9}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mOrientation:Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;
    invoke-static {v7, v8}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$3402(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;)Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;

    .line 2428
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mOrientation:Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$3400(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;->open()V

    .line 2430
    :cond_f
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    const/4 v8, 0x0

    # invokes: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->takePictures(Z)V
    invoke-static {v7, v8}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$3900(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Z)V

    goto/16 :goto_0

    .line 2434
    :cond_10
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->mMyWorkHandler:Landroid/os/Handler;

    invoke-virtual {v7}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v4

    .line 2435
    .restart local v4    # "newMsg":Landroid/os/Message;
    const/4 v7, 0x2

    iput v7, v4, Landroid/os/Message;->what:I

    .line 2436
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mWorkHandler:Landroid/os/Handler;
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$2400(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Landroid/os/Handler;

    move-result-object v7

    invoke-virtual {v7, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 2439
    .end local v4    # "newMsg":Landroid/os/Message;
    :cond_11
    const-string v7, "SafetyAssuranceWorkThread"

    const-string v8, "Sending messages is skipped"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 2444
    .end local v2    # "curState":I
    :pswitch_3
    const-class v8, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    monitor-enter v8

    .line 2445
    :try_start_5
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$400(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->getAppState(Landroid/content/Context;)I

    move-result v2

    .line 2446
    .restart local v2    # "curState":I
    if-eqz v2, :cond_12

    const/4 v7, 0x1

    if-ne v2, v7, :cond_13

    .line 2447
    :cond_12
    const-string v7, "SafetyAssuranceWorkThread"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "curState is ["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "] skip WT_SENDMSG"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2448
    monitor-exit v8

    goto/16 :goto_0

    .line 2450
    .end local v2    # "curState":I
    :catchall_2
    move-exception v7

    monitor-exit v8
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v7

    .restart local v2    # "curState":I
    :cond_13
    :try_start_6
    monitor-exit v8
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 2453
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mOrientation:Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$3400(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;

    move-result-object v7

    if-eqz v7, :cond_14

    .line 2454
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mOrientation:Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$3400(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;->close()V

    .line 2455
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    const/4 v8, 0x0

    # setter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mOrientation:Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;
    invoke-static {v7, v8}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$3402(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;)Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;

    .line 2458
    :cond_14
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # invokes: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->startMessageBindService()V
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$4500(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)V

    goto/16 :goto_0

    .line 2462
    .end local v2    # "curState":I
    :pswitch_4
    const-class v8, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    monitor-enter v8

    .line 2463
    :try_start_7
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$400(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->getAppState(Landroid/content/Context;)I

    move-result v2

    .line 2464
    .restart local v2    # "curState":I
    if-eqz v2, :cond_15

    const/4 v7, 0x1

    if-ne v2, v7, :cond_16

    .line 2465
    :cond_15
    const-string v7, "SafetyAssuranceWorkThread"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "curState is ["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "] skip WT_SENDMSG_COMPLETE"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2466
    monitor-exit v8

    goto/16 :goto_0

    .line 2468
    .end local v2    # "curState":I
    :catchall_3
    move-exception v7

    monitor-exit v8
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    throw v7

    .restart local v2    # "curState":I
    :cond_16
    :try_start_8
    monitor-exit v8
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    .line 2471
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # invokes: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->stopMessageBindService()V
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$4600(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)V

    .line 2474
    monitor-enter p0

    .line 2475
    :try_start_9
    const-string v7, "SafetyAssuranceWorkThread"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "SENDMSG_COMPLETE mRunningCounter["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v9, v9, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRunningCounter:I
    invoke-static {v9}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$2700(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2476
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRunningCounter:I
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$2700(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)I

    move-result v7

    if-gtz v7, :cond_17

    .line 2477
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$400(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Landroid/content/Context;

    move-result-object v7

    const/4 v8, 0x7

    invoke-static {v7, v8}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->setAppState(Landroid/content/Context;I)V

    .line 2478
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$400(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Landroid/content/Context;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {v7, v8}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceApp;->setWakeLock(Landroid/content/Context;Z)V

    .line 2480
    :cond_17
    monitor-exit p0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    .line 2482
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->featureSupportNewUX()Z

    move-result v7

    if-nez v7, :cond_18

    .line 2483
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mNeedQuit:Z
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$4700(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Z

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_0

    .line 2484
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # invokes: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->quitService()V
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$100(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)V

    goto/16 :goto_0

    .line 2480
    :catchall_4
    move-exception v7

    :try_start_a
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    throw v7

    .line 2488
    :cond_18
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRunningCounter:I
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$2700(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)I

    move-result v7

    if-gtz v7, :cond_0

    .line 2489
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    const/4 v8, 0x1

    # invokes: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->quitServiceRightNow(Z)V
    invoke-static {v7, v8}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$200(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Z)V

    goto/16 :goto_0

    .line 2495
    .end local v2    # "curState":I
    :pswitch_5
    const-class v8, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    monitor-enter v8

    .line 2496
    :try_start_b
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$400(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->getAppState(Landroid/content/Context;)I

    move-result v2

    .line 2497
    .restart local v2    # "curState":I
    if-eqz v2, :cond_19

    const/4 v7, 0x1

    if-ne v2, v7, :cond_1a

    .line 2498
    :cond_19
    const-string v7, "SafetyAssuranceWorkThread"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "curState is ["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "] skip WT_SENDVOCMSG"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2499
    monitor-exit v8

    goto/16 :goto_0

    .line 2501
    .end local v2    # "curState":I
    :catchall_5
    move-exception v7

    monitor-exit v8
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_5

    throw v7

    .restart local v2    # "curState":I
    :cond_1a
    :try_start_c
    monitor-exit v8
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_5

    .line 2504
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    const/4 v8, 0x0

    # setter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVoiceRecording:Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;
    invoke-static {v7, v8}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$3702(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;)Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;

    .line 2507
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # invokes: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->startVocMessageBindService()V
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$4800(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)V

    goto/16 :goto_0

    .line 2511
    .end local v2    # "curState":I
    :pswitch_6
    const-class v8, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    monitor-enter v8

    .line 2512
    :try_start_d
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$400(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->getAppState(Landroid/content/Context;)I

    move-result v2

    .line 2513
    .restart local v2    # "curState":I
    if-eqz v2, :cond_1b

    const/4 v7, 0x1

    if-ne v2, v7, :cond_1c

    .line 2514
    :cond_1b
    const-string v7, "SafetyAssuranceWorkThread"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "curState is ["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "] skip WT_SENDVOCMSG_COMPLETE"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2515
    monitor-exit v8

    goto/16 :goto_0

    .line 2517
    .end local v2    # "curState":I
    :catchall_6
    move-exception v7

    monitor-exit v8
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_6

    throw v7

    .restart local v2    # "curState":I
    :cond_1c
    :try_start_e
    monitor-exit v8
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_6

    .line 2520
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # invokes: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->stopVocMessageBindService()V
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$4900(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)V

    .line 2523
    monitor-enter p0

    .line 2524
    :try_start_f
    const-string v7, "SafetyAssuranceWorkThread"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "SENDVOCMSG_COMPLETE mRunningCounter["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v9, v9, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRunningCounter:I
    invoke-static {v9}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$2700(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2525
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRunningCounter:I
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$2700(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)I

    move-result v7

    if-gtz v7, :cond_1d

    .line 2526
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$400(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Landroid/content/Context;

    move-result-object v7

    const/4 v8, 0x7

    invoke-static {v7, v8}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->setAppState(Landroid/content/Context;I)V

    .line 2527
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$400(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Landroid/content/Context;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {v7, v8}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceApp;->setWakeLock(Landroid/content/Context;Z)V

    .line 2529
    :cond_1d
    monitor-exit p0
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_7

    .line 2531
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->featureSupportNewUX()Z

    move-result v7

    if-nez v7, :cond_1e

    .line 2532
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mNeedQuit:Z
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$4700(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Z

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_0

    .line 2533
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # invokes: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->quitService()V
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$100(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)V

    goto/16 :goto_0

    .line 2529
    :catchall_7
    move-exception v7

    :try_start_10
    monitor-exit p0
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_7

    throw v7

    .line 2537
    :cond_1e
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRunningCounter:I
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$2700(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)I

    move-result v7

    if-gtz v7, :cond_0

    .line 2538
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    const/4 v8, 0x1

    # invokes: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->quitServiceRightNow(Z)V
    invoke-static {v7, v8}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$200(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Z)V

    goto/16 :goto_0

    .line 2544
    .end local v2    # "curState":I
    :pswitch_7
    const-class v8, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    monitor-enter v8

    .line 2545
    :try_start_11
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$400(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->getAppState(Landroid/content/Context;)I

    move-result v2

    .line 2546
    .restart local v2    # "curState":I
    if-eqz v2, :cond_1f

    const/4 v7, 0x1

    if-ne v2, v7, :cond_20

    .line 2547
    :cond_1f
    const-string v7, "SafetyAssuranceWorkThread"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "curState is ["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "] skip WT_RESTORELOCATIONSENSORNOW"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2548
    monitor-exit v8

    goto/16 :goto_0

    .line 2550
    .end local v2    # "curState":I
    :catchall_8
    move-exception v7

    monitor-exit v8
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_8

    throw v7

    .restart local v2    # "curState":I
    :cond_20
    :try_start_12
    monitor-exit v8
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_8

    .line 2553
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIntervalTime:I
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$2500(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)I

    move-result v7

    if-gtz v7, :cond_0

    .line 2559
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mFirstMessage:Z
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$4200(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Z

    move-result v7

    if-eqz v7, :cond_21

    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mFirstMessage:Z
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$4200(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Z

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mCurrentPhoneState:I
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$4300(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)I

    move-result v7

    if-nez v7, :cond_0

    .line 2560
    :cond_21
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # invokes: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->stopLocationListener()V
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$5000(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)V

    .line 2561
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v7, v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    const/4 v8, 0x0

    # invokes: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->controlLocationSensor(I)V
    invoke-static {v7, v8}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$3500(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;I)V

    goto/16 :goto_0

    .line 2240
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_7
    .end packed-switch
.end method
