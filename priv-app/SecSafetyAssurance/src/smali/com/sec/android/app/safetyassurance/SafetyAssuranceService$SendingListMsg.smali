.class public Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;
.super Ljava/lang/Object;
.source "SafetyAssuranceService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SendingListMsg"
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String; = "SendingListMsg"


# instance fields
.field private mMsg1:Z

.field private mMsg2:Z

.field private mMsg3:Z

.field private mPictureFront:Z

.field private mPictureRear:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2662
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2663
    const-string v0, "SendingListMsg"

    const-string v1, "Init - All false"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2664
    iput-boolean v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;->mMsg1:Z

    .line 2665
    iput-boolean v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;->mMsg2:Z

    .line 2666
    iput-boolean v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;->mMsg3:Z

    .line 2667
    iput-boolean v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;->mPictureFront:Z

    .line 2668
    iput-boolean v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;->mPictureRear:Z

    .line 2669
    return-void
.end method

.method public constructor <init>(ZZ)V
    .locals 3
    .param p1, "bMsg"    # Z
    .param p2, "bPic"    # Z

    .prologue
    .line 2645
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2646
    const-string v0, "SendingListMsg"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Init Msg["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] Pic["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2647
    iput-boolean p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;->mMsg1:Z

    .line 2648
    iput-boolean p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;->mMsg2:Z

    .line 2649
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->isJPNDevice()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->isJPNSecMessageDisable()Z

    move-result v0

    if-nez v0, :cond_0

    .end local p1    # "bMsg":Z
    :goto_0
    iput-boolean p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;->mMsg3:Z

    .line 2650
    iput-boolean p2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;->mPictureFront:Z

    .line 2651
    iput-boolean p2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;->mPictureRear:Z

    .line 2652
    return-void

    .line 2649
    .restart local p1    # "bMsg":Z
    :cond_0
    const/4 p1, 0x0

    goto :goto_0
.end method

.method public constructor <init>(ZZZ)V
    .locals 3
    .param p1, "bMsg"    # Z
    .param p2, "bPicFront"    # Z
    .param p3, "bPicRear"    # Z

    .prologue
    .line 2654
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2655
    const-string v0, "SendingListMsg"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Init Msg for SOS message ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] Picfront["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] PicRear ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2656
    iput-boolean p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;->mMsg1:Z

    .line 2657
    iput-boolean p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;->mMsg2:Z

    .line 2658
    iput-boolean p2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;->mPictureFront:Z

    .line 2659
    iput-boolean p3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;->mPictureRear:Z

    .line 2660
    return-void
.end method


# virtual methods
.method public isSendingMsgComplete()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2709
    monitor-enter p0

    .line 2710
    :try_start_0
    iget-boolean v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;->mMsg1:Z

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;->mMsg2:Z

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;->mMsg3:Z

    if-nez v2, :cond_0

    move v3, v0

    :goto_0
    iget-boolean v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;->mPictureFront:Z

    if-nez v2, :cond_1

    move v2, v0

    :goto_1
    and-int/2addr v2, v3

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;->mPictureRear:Z

    if-nez v2, :cond_2

    .line 2711
    monitor-exit p0

    .line 2713
    :goto_2
    return v0

    :cond_0
    move v3, v1

    .line 2710
    goto :goto_0

    :cond_1
    move v2, v1

    goto :goto_1

    .line 2713
    :cond_2
    monitor-exit p0

    move v0, v1

    goto :goto_2

    .line 2715
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setMsg0(Z)V
    .locals 1
    .param p1, "bMsg"    # Z

    .prologue
    .line 2672
    monitor-enter p0

    .line 2673
    :try_start_0
    iput-boolean p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;->mMsg1:Z

    .line 2674
    iput-boolean p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;->mMsg2:Z

    .line 2675
    monitor-exit p0

    .line 2676
    return-void

    .line 2675
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setMsg1(Z)V
    .locals 1
    .param p1, "bMsg"    # Z

    .prologue
    .line 2679
    monitor-enter p0

    .line 2680
    :try_start_0
    iput-boolean p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;->mMsg1:Z

    .line 2681
    monitor-exit p0

    .line 2682
    return-void

    .line 2681
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setMsg2(Z)V
    .locals 1
    .param p1, "bMsg"    # Z

    .prologue
    .line 2685
    monitor-enter p0

    .line 2686
    :try_start_0
    iput-boolean p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;->mMsg2:Z

    .line 2687
    monitor-exit p0

    .line 2688
    return-void

    .line 2687
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setMsg3(Z)V
    .locals 1
    .param p1, "bMsg"    # Z

    .prologue
    .line 2691
    monitor-enter p0

    .line 2692
    :try_start_0
    iput-boolean p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;->mMsg3:Z

    .line 2693
    monitor-exit p0

    .line 2694
    return-void

    .line 2693
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setPicFront(Z)V
    .locals 1
    .param p1, "bPic"    # Z

    .prologue
    .line 2697
    monitor-enter p0

    .line 2698
    :try_start_0
    iput-boolean p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;->mPictureFront:Z

    .line 2699
    monitor-exit p0

    .line 2700
    return-void

    .line 2699
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setPicRear(Z)V
    .locals 1
    .param p1, "bPic"    # Z

    .prologue
    .line 2703
    monitor-enter p0

    .line 2704
    :try_start_0
    iput-boolean p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;->mPictureRear:Z

    .line 2705
    monitor-exit p0

    .line 2706
    return-void

    .line 2705
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
