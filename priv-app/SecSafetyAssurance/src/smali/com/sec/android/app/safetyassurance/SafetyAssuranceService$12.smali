.class Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$12;
.super Lcom/android/mms/transaction/ISnsRemoteServiceCallback$Stub;
.source "SafetyAssuranceService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)V
    .locals 0

    .prologue
    .line 1239
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$12;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    invoke-direct {p0}, Lcom/android/mms/transaction/ISnsRemoteServiceCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(III)V
    .locals 5
    .param p1, "appID"    # I
    .param p2, "msgID"    # I
    .param p3, "value"    # I

    .prologue
    const/4 v4, 0x1

    .line 1241
    const/16 v1, 0xcae

    if-ne p1, v1, :cond_2

    .line 1242
    const-string v1, "SafetyAssuranceService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onResponse from VOC Message service: appid["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] msgID["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]   valuse["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 1243
    packed-switch p2, :pswitch_data_0

    .line 1251
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$12;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSendingListVoc:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListVoc;
    invoke-static {v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$1500(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListVoc;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListVoc;->isSendingVocComplete()Z

    move-result v1

    if-ne v1, v4, :cond_2

    .line 1252
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$12;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSendingListMsg:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;
    invoke-static {v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$900(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$12;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSendingListMsg:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;
    invoke-static {v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$900(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$12;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSendingListMsg:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;
    invoke-static {v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$900(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;->isSendingMsgComplete()Z

    move-result v1

    if-ne v1, v4, :cond_1

    .line 1254
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$12;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mWorkHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$2400(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 1255
    .local v0, "newMsg":Landroid/os/Message;
    const/16 v1, 0x8

    iput v1, v0, Landroid/os/Message;->what:I

    .line 1256
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$12;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mWorkHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$2400(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1259
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$12;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIntervalTime:I
    invoke-static {v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$2500(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)I

    move-result v1

    if-gtz v1, :cond_1

    .line 1260
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$12;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mPhoneState:Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;
    invoke-static {v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$2600(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1261
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$12;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mPhoneState:Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;
    invoke-static {v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$2600(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->close()V

    .line 1267
    .end local v0    # "newMsg":Landroid/os/Message;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$12;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # operator-- for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRunningCounter:I
    invoke-static {v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$2710(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)I

    .line 1268
    const-string v1, "SafetyAssuranceService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "voccallback onResponse mRunningCounter = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$12;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRunningCounter:I
    invoke-static {v3}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$2700(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1269
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$12;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mWorkHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$2400(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 1270
    .restart local v0    # "newMsg":Landroid/os/Message;
    const/4 v1, 0x5

    iput v1, v0, Landroid/os/Message;->what:I

    .line 1271
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$12;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mWorkHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$2400(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1275
    .end local v0    # "newMsg":Landroid/os/Message;
    :cond_2
    return-void

    .line 1245
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$12;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSendingListVoc:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListVoc;
    invoke-static {v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$1500(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListVoc;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListVoc;->setVocRec(Z)V

    goto/16 :goto_0

    .line 1243
    :pswitch_data_0
    .packed-switch 0x69
        :pswitch_0
    .end packed-switch
.end method
