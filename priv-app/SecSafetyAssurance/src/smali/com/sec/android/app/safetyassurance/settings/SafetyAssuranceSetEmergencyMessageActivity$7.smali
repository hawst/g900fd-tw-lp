.class Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$7;
.super Ljava/lang/Object;
.source "SafetyAssuranceSetEmergencyMessageActivity.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;

.field final synthetic val$okButton:Landroid/widget/Button;


# direct methods
.method constructor <init>(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;Landroid/widget/Button;)V
    .locals 0

    .prologue
    .line 227
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$7;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;

    iput-object p2, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$7;->val$okButton:Landroid/widget/Button;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 6
    .param p1, "arg0"    # Landroid/widget/CompoundButton;
    .param p2, "arg1"    # Z

    .prologue
    const v5, 0x7f070005

    const v4, 0x7f070004

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 230
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$7;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;

    iget-object v0, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->agreeToTurnOnMobileDataCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->playSoundEffect(I)V

    .line 231
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$7;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;

    # getter for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->newConcept:Z
    invoke-static {v0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->access$200(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 238
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->isChinaDevice()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 239
    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$7;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;

    iget-object v0, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->agreeToShareLocationCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$7;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;

    iget-object v0, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->agreeTermsAndConditionsCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 241
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$7;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;

    # getter for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->newConceptForPhone:Z
    invoke-static {v0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->access$100(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 242
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$7;->val$okButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$7;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 244
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$7;->val$okButton:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 273
    :goto_0
    return-void

    .line 246
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$7;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;

    # getter for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->newConceptForPhone:Z
    invoke-static {v0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->access$100(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 247
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$7;->val$okButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$7;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 249
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$7;->val$okButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 252
    :cond_3
    if-eqz p2, :cond_5

    .line 253
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$7;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;

    # getter for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->newConceptForPhone:Z
    invoke-static {v0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->access$100(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 254
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$7;->val$okButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$7;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 256
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$7;->val$okButton:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 258
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$7;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;

    # getter for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->newConceptForPhone:Z
    invoke-static {v0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->access$100(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 259
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$7;->val$okButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$7;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 261
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$7;->val$okButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 266
    :cond_7
    if-eqz p2, :cond_8

    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$7;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;

    iget-object v0, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->agreeToShareLocationCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$7;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;

    iget-object v0, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->agreeTermsAndConditionsCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 267
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$7;->val$okButton:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 269
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$7;->val$okButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method
