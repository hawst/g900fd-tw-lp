.class Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$8;
.super Ljava/lang/Object;
.source "SafetyAssuranceService.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)V
    .locals 0

    .prologue
    .line 1055
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$8;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 6
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1058
    const-string v2, "SafetyAssuranceService"

    const-string v3, "VOC MSG service onServiceConnected Called !!!! ==="

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1061
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$8;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIsSosMode:Z
    invoke-static {v2}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$800(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1062
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$8;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    new-instance v3, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListVoc;

    iget-object v4, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$8;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    iget-object v5, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$8;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRemoveVocRecOption:Z
    invoke-static {v5}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$1600(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Z

    move-result v5

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$8;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSendVocRec:I
    invoke-static {v5}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$1700(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)I

    move-result v5

    if-ne v5, v0, :cond_1

    :goto_0
    invoke-direct {v3, v4, v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListVoc;-><init>(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Z)V

    # setter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSendingListVoc:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListVoc;
    invoke-static {v2, v3}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$1502(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListVoc;)Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListVoc;

    .line 1069
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$8;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVoiceRecordingUri:Landroid/net/Uri;
    invoke-static {v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$1800(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1070
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$8;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSendingListVoc:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListVoc;
    invoke-static {v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$1500(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListVoc;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListVoc;->setVocRec(Z)V

    .line 1073
    :cond_0
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$8$1;

    invoke-direct {v1, p0, p2}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$8$1;-><init>(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$8;Landroid/os/IBinder;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 1094
    const-string v0, "SafetyAssuranceService"

    const-string v1, "VOC MSG service onServiceConnected Called END ==="

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 1095
    return-void

    :cond_1
    move v0, v1

    .line 1062
    goto :goto_0

    .line 1064
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$8;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    new-instance v3, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListVoc;

    iget-object v4, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$8;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    invoke-direct {v3, v4, v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListVoc;-><init>(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Z)V

    # setter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSendingListVoc:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListVoc;
    invoke-static {v2, v3}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$1502(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListVoc;)Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListVoc;

    goto :goto_1
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 1099
    const-string v1, "SafetyAssuranceService"

    const-string v2, "VOC MSG service onServiceDisconnected Called !!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1102
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$8;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVocServiceBinder:Lcom/android/mms/transaction/IMessageBackgroundSender;
    invoke-static {v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$1900(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Lcom/android/mms/transaction/IMessageBackgroundSender;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/mms/transaction/IMessageBackgroundSender;->stopListening()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1107
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$8;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVocServiceBinder:Lcom/android/mms/transaction/IMessageBackgroundSender;
    invoke-static {v1, v2}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$1902(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Lcom/android/mms/transaction/IMessageBackgroundSender;)Lcom/android/mms/transaction/IMessageBackgroundSender;

    .line 1108
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$8;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVocSvcConnected:Z

    .line 1109
    return-void

    .line 1103
    :catch_0
    move-exception v0

    .line 1104
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
