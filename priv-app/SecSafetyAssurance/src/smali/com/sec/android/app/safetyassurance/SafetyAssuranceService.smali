.class public Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;
.super Landroid/app/Service;
.source "SafetyAssuranceService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListVoc;,
        Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;,
        Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;,
        Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$LatLng;
    }
.end annotation


# static fields
.field private static final APP_ID_PICMSG:I = 0xcad

.field private static final APP_ID_VOCMSG:I = 0xcae

.field public static final BACK_GROUND_MSG_SENDING:Ljava/lang/String; = "com.android.mms.transaction.Send.BACKGROUND_MSG"

.field public static final B_SERVICEALARMACTION:Ljava/lang/String; = "android.intent.action.SAFETY_SERVICE_B_ALARM"

.field public static final COMMON_APP_NAME:Ljava/lang/String; = "Safety assistance"

.field public static final DEFAULT_HELP_MESSAGE:Ljava/lang/String; = "SOS!"

.field private static final FORCEMMS:Ljava/lang/String; = "forcemms"

.field public static final FORCESTOP:Ljava/lang/String; = "android.intent.action.SAFETY_SERVICE_FORCESTOP"

.field private static final LANGUAGE_KOR:Ljava/lang/String; = "ko"

.field private static final LOCATIONPICTUREMODE:I = 0x1

.field public static final LOCATIONSENSOR_RESTORE:I = 0x0

.field public static final LOCATIONSENSOR_TURNON:I = 0x1

.field private static final MAX_RETRY_COUNT:I = 0x3c

.field private static final MESSAGE:Ljava/lang/String; = "message"

.field public static final MESSAGE_MAX_GSM7_LENGTH:I = 0xa0

.field public static final MESSAGE_MAX_LENGTH:I = 0x46

.field private static final NONE:I = 0x0

.field private static final ONLYLOCATIONMODE:I = 0x2

.field private static final RECIPIENT:Ljava/lang/String; = "recipient"

.field private static final RECIPIENTS:Ljava/lang/String; = "recipients"

.field private static final REQUEST_APP:Ljava/lang/String; = "requestApp"

.field private static final REQUEST_CODE:Ljava/lang/String; = "requestCode"

.field public static final SENDING_LOCATION_JPN:I = 0x6

.field public static final SENDING_LOCATION_ONLY:I = 0x4

.field public static final SENDING_MESSAGE:I = 0x0

.field public static final SENDING_MESSAGE_JPN:I = 0x5

.field public static final SENDING_MESSAGE_ONLY:I = 0x3

.field public static final SENDING_PICTURE:I = 0x1

.field public static final SENDING_VOICE:I = 0x2

.field private static final SEND_MSG0:I = 0x64

.field private static final SEND_MSG1:I = 0x65

.field private static final SEND_MSG2:I = 0x66

.field private static final SEND_MSG3:I = 0x6a

.field private static final SEND_PICFRONT:I = 0x67

.field private static final SEND_PICREAR:I = 0x68

.field private static final SEND_VOCREC:I = 0x69

.field public static final SERVICEALARMACTION:Ljava/lang/String; = "android.intent.action.SAFETY_SERVICE_ALARM"

.field public static final SERVICESHOWSTOPDIALOGACTION:Ljava/lang/String; = "android.intent.action.SAFETY_SERVICE_SHOW_STOPDIALOG"

.field public static final SERVICESTARTACTION:Ljava/lang/String; = "android.intent.action.SAFETY_SERVICE_START"

.field public static final SERVICE_RESENDMSG:Ljava/lang/String; = "android.intent.action.SAFETY_SERVICE_RESENDMSG"

.field public static final SERVICE_SENDMSG_COMPLETE:Ljava/lang/String; = "android.intent.action.SAFETY_SERVICE_SENDMSG_COMPLETE"

.field private static final SLEEP_TIME:I = 0x1f4

.field private static final STOP_TIMER:I = 0x2

.field private static final TAG:Ljava/lang/String; = "SafetyAssuranceService"

.field public static final WORKTHREAD_ALARMACTION:I = 0x1

.field public static final WORKTHREAD_RESTORELOCATIONSENSORNOW:I = 0x8

.field public static final WORKTHREAD_SENDMSG:I = 0x2

.field public static final WORKTHREAD_SENDMSG_COMPLETE:I = 0x3

.field public static final WORKTHREAD_SENDVOCMSG:I = 0x4

.field public static final WORKTHREAD_SENDVOCMSG_COMPLETE:I = 0x5

.field public static final WORKTHREAD_SERVICESTART:I = 0x0

.field public static final WORKTHREAD_SHOWSTOPDIALOG:I = 0x9

.field private static mSMS:Landroid/telephony/SmsManager;


# instance fields
.field private isVZW:Z

.field mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mCallbackBinder:Lcom/android/mms/transaction/ISnsRemoteService;

.field public mCallbackConn:Landroid/content/ServiceConnection;

.field private mCallbackConnected:Z

.field private mCameraCapturer:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

.field private mContext:Landroid/content/Context;

.field private mCurrentDate:Ljava/util/Date;

.field private mCurrentPhoneState:I

.field private mCurrentTime:J

.field private mFirstMessage:Z

.field private mForceStopStart:Z

.field private mFrontPictureUri:Landroid/net/Uri;

.field mGPSLocationListener:Landroid/location/LocationListener;

.field private mImageUri:Landroid/net/Uri;

.field private mIntervalTime:I

.field private mIsActiveGPSProvider:Z

.field private mIsActiveLocationListener:Z

.field private mIsActiveLocationSensor:Z

.field private mIsChangingLocationSensor:Z

.field private mIsSosMode:Z

.field private mLocationManager:Landroid/location/LocationManager;

.field private mMessageText:Ljava/lang/String;

.field private mNeedQuit:Z

.field mNetworkLocationListener:Landroid/location/LocationListener;

.field private mOrientation:Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;

.field private mPhoneState:Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

.field private volatile mQuitDialog:Landroid/app/AlertDialog;

.field private mRearPictureUri:Landroid/net/Uri;

.field private mRecipients:[Ljava/lang/String;

.field private mRemoveCameraOption:Z

.field private mRemoveVocRecOption:Z

.field private mRequestApp:I

.field private mRunningCounter:I

.field private mSendPictures:I

.field private mSendVocRec:I

.field private mSendingListMsg:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;

.field private mSendingListVoc:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListVoc;

.field private mService:Landroid/app/Service;

.field private mServiceBinder:Lcom/android/mms/transaction/IMessageBackgroundSender;

.field mServiceConn:Landroid/content/ServiceConnection;

.field private mSnsCallback:Lcom/android/mms/transaction/ISnsRemoteServiceCallback;

.field private mSosMode:I

.field private mSvcConnected:Z

.field private mVocCallback:Lcom/android/mms/transaction/ISnsRemoteServiceCallback;

.field private mVocCallbackBinder:Lcom/android/mms/transaction/ISnsRemoteService;

.field public mVocCallbackConn:Landroid/content/ServiceConnection;

.field mVocCallbackConnected:Z

.field private mVocRecordingTime:I

.field private mVocServiceBinder:Lcom/android/mms/transaction/IMessageBackgroundSender;

.field mVocServiceConn:Landroid/content/ServiceConnection;

.field mVocSvcConnected:Z

.field private mVoiceRecording:Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;

.field private mVoiceRecordingUri:Landroid/net/Uri;

.field private volatile mWorkHandler:Landroid/os/Handler;

.field private volatile mWorkLooper:Landroid/os/Looper;

.field private volatile mWorkThread:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 206
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSMS:Landroid/telephony/SmsManager;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 80
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 209
    iput-boolean v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIsSosMode:Z

    .line 211
    iput v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSosMode:I

    .line 218
    iput-boolean v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->isVZW:Z

    .line 220
    new-instance v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$1;-><init>(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)V

    iput-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 982
    new-instance v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$7;-><init>(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)V

    iput-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mServiceConn:Landroid/content/ServiceConnection;

    .line 1055
    new-instance v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$8;-><init>(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)V

    iput-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVocServiceConn:Landroid/content/ServiceConnection;

    .line 1117
    new-instance v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$9;-><init>(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)V

    iput-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mCallbackConn:Landroid/content/ServiceConnection;

    .line 1147
    new-instance v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$10;

    invoke-direct {v0, p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$10;-><init>(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)V

    iput-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSnsCallback:Lcom/android/mms/transaction/ISnsRemoteServiceCallback;

    .line 1213
    new-instance v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$11;

    invoke-direct {v0, p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$11;-><init>(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)V

    iput-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVocCallbackConn:Landroid/content/ServiceConnection;

    .line 1239
    new-instance v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$12;

    invoke-direct {v0, p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$12;-><init>(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)V

    iput-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVocCallback:Lcom/android/mms/transaction/ISnsRemoteServiceCallback;

    .line 1909
    new-instance v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$13;

    invoke-direct {v0, p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$13;-><init>(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)V

    iput-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mGPSLocationListener:Landroid/location/LocationListener;

    .line 1971
    new-instance v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$14;

    invoke-direct {v0, p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$14;-><init>(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)V

    iput-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mNetworkLocationListener:Landroid/location/LocationListener;

    .line 2720
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->dismissQuitEmergencyDialog()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->quitService()V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    .prologue
    .line 80
    iget v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSendPictures:I

    return v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mFrontPictureUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;
    .param p1, "x1"    # Landroid/net/Uri;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mFrontPictureUri:Landroid/net/Uri;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRearPictureUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;
    .param p1, "x1"    # Landroid/net/Uri;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRearPictureUri:Landroid/net/Uri;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Lcom/android/mms/transaction/IMessageBackgroundSender;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mServiceBinder:Lcom/android/mms/transaction/IMessageBackgroundSender;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Lcom/android/mms/transaction/IMessageBackgroundSender;)Lcom/android/mms/transaction/IMessageBackgroundSender;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;
    .param p1, "x1"    # Lcom/android/mms/transaction/IMessageBackgroundSender;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mServiceBinder:Lcom/android/mms/transaction/IMessageBackgroundSender;

    return-object p1
.end method

.method static synthetic access$1402(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;
    .param p1, "x1"    # Z

    .prologue
    .line 80
    iput-boolean p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSvcConnected:Z

    return p1
.end method

.method static synthetic access$1500(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListVoc;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSendingListVoc:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListVoc;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListVoc;)Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListVoc;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;
    .param p1, "x1"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListVoc;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSendingListVoc:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListVoc;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRemoveVocRecOption:Z

    return v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    .prologue
    .line 80
    iget v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSendVocRec:I

    return v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVoiceRecordingUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$1802(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;
    .param p1, "x1"    # Landroid/net/Uri;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVoiceRecordingUri:Landroid/net/Uri;

    return-object p1
.end method

.method static synthetic access$1900(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Lcom/android/mms/transaction/IMessageBackgroundSender;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVocServiceBinder:Lcom/android/mms/transaction/IMessageBackgroundSender;

    return-object v0
.end method

.method static synthetic access$1902(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Lcom/android/mms/transaction/IMessageBackgroundSender;)Lcom/android/mms/transaction/IMessageBackgroundSender;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;
    .param p1, "x1"    # Lcom/android/mms/transaction/IMessageBackgroundSender;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVocServiceBinder:Lcom/android/mms/transaction/IMessageBackgroundSender;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;
    .param p1, "x1"    # Z

    .prologue
    .line 80
    invoke-direct {p0, p1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->quitServiceRightNow(Z)V

    return-void
.end method

.method static synthetic access$2000(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->sendVocMessage()V

    return-void
.end method

.method static synthetic access$2100(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Lcom/android/mms/transaction/ISnsRemoteService;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mCallbackBinder:Lcom/android/mms/transaction/ISnsRemoteService;

    return-object v0
.end method

.method static synthetic access$2102(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Lcom/android/mms/transaction/ISnsRemoteService;)Lcom/android/mms/transaction/ISnsRemoteService;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;
    .param p1, "x1"    # Lcom/android/mms/transaction/ISnsRemoteService;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mCallbackBinder:Lcom/android/mms/transaction/ISnsRemoteService;

    return-object p1
.end method

.method static synthetic access$2202(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;
    .param p1, "x1"    # Z

    .prologue
    .line 80
    iput-boolean p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mCallbackConnected:Z

    return p1
.end method

.method static synthetic access$2300(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Lcom/android/mms/transaction/ISnsRemoteServiceCallback;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSnsCallback:Lcom/android/mms/transaction/ISnsRemoteServiceCallback;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mWorkHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2402(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Landroid/os/Handler;)Landroid/os/Handler;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;
    .param p1, "x1"    # Landroid/os/Handler;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mWorkHandler:Landroid/os/Handler;

    return-object p1
.end method

.method static synthetic access$2500(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    .prologue
    .line 80
    iget v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIntervalTime:I

    return v0
.end method

.method static synthetic access$2600(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mPhoneState:Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    .prologue
    .line 80
    iget v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRunningCounter:I

    return v0
.end method

.method static synthetic access$2702(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;
    .param p1, "x1"    # I

    .prologue
    .line 80
    iput p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRunningCounter:I

    return p1
.end method

.method static synthetic access$2710(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    .prologue
    .line 80
    iget v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRunningCounter:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRunningCounter:I

    return v0
.end method

.method static synthetic access$2800(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Lcom/android/mms/transaction/ISnsRemoteService;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVocCallbackBinder:Lcom/android/mms/transaction/ISnsRemoteService;

    return-object v0
.end method

.method static synthetic access$2802(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Lcom/android/mms/transaction/ISnsRemoteService;)Lcom/android/mms/transaction/ISnsRemoteService;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;
    .param p1, "x1"    # Lcom/android/mms/transaction/ISnsRemoteService;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVocCallbackBinder:Lcom/android/mms/transaction/ISnsRemoteService;

    return-object p1
.end method

.method static synthetic access$2900(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Lcom/android/mms/transaction/ISnsRemoteServiceCallback;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVocCallback:Lcom/android/mms/transaction/ISnsRemoteServiceCallback;

    return-object v0
.end method

.method static synthetic access$3002(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;
    .param p1, "x1"    # Z

    .prologue
    .line 80
    iput-boolean p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIsActiveGPSProvider:Z

    return p1
.end method

.method static synthetic access$302(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mQuitDialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$3100(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    .prologue
    .line 80
    iget-wide v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mCurrentTime:J

    return-wide v0
.end method

.method static synthetic access$3102(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;
    .param p1, "x1"    # J

    .prologue
    .line 80
    iput-wide p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mCurrentTime:J

    return-wide p1
.end method

.method static synthetic access$3202(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Ljava/util/Date;)Ljava/util/Date;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;
    .param p1, "x1"    # Ljava/util/Date;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mCurrentDate:Ljava/util/Date;

    return-object p1
.end method

.method static synthetic access$3300(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Landroid/app/Service;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mService:Landroid/app/Service;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mOrientation:Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;

    return-object v0
.end method

.method static synthetic access$3402(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;)Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;
    .param p1, "x1"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mOrientation:Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;

    return-object p1
.end method

.method static synthetic access$3500(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;
    .param p1, "x1"    # I

    .prologue
    .line 80
    invoke-direct {p0, p1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->controlLocationSensor(I)V

    return-void
.end method

.method static synthetic access$3600(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->startLocationListener()V

    return-void
.end method

.method static synthetic access$3700(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVoiceRecording:Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;

    return-object v0
.end method

.method static synthetic access$3702(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;)Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;
    .param p1, "x1"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVoiceRecording:Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;

    return-object p1
.end method

.method static synthetic access$3800(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    .prologue
    .line 80
    iget v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVocRecordingTime:I

    return v0
.end method

.method static synthetic access$3900(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;
    .param p1, "x1"    # Z

    .prologue
    .line 80
    invoke-direct {p0, p1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->takePictures(Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$4000(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mImageUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$4100(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->updateRecipient()Z

    move-result v0

    return v0
.end method

.method static synthetic access$4200(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mFirstMessage:Z

    return v0
.end method

.method static synthetic access$4300(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    .prologue
    .line 80
    iget v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mCurrentPhoneState:I

    return v0
.end method

.method static synthetic access$4302(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;
    .param p1, "x1"    # I

    .prologue
    .line 80
    iput p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mCurrentPhoneState:I

    return p1
.end method

.method static synthetic access$4400(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mCameraCapturer:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    return-object v0
.end method

.method static synthetic access$4500(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->startMessageBindService()V

    return-void
.end method

.method static synthetic access$4600(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->stopMessageBindService()V

    return-void
.end method

.method static synthetic access$4700(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mNeedQuit:Z

    return v0
.end method

.method static synthetic access$4800(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->startVocMessageBindService()V

    return-void
.end method

.method static synthetic access$4900(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->stopVocMessageBindService()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->sendEmergencyMessage()V

    return-void
.end method

.method static synthetic access$5000(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->stopLocationListener()V

    return-void
.end method

.method static synthetic access$5102(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Landroid/os/Looper;)Landroid/os/Looper;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;
    .param p1, "x1"    # Landroid/os/Looper;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mWorkLooper:Landroid/os/Looper;

    return-object p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRemoveCameraOption:Z

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    .prologue
    .line 80
    iget v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSosMode:I

    return v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;
    .param p1, "x1"    # I

    .prologue
    .line 80
    iput p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSosMode:I

    return p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIsSosMode:Z

    return v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSendingListMsg:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;

    return-object v0
.end method

.method static synthetic access$902(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;)Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;
    .param p1, "x1"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSendingListMsg:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;

    return-object p1
.end method

.method public static checkMaxLength(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "text"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2761
    const-string v1, ""

    .line 2763
    .local v1, "returnString":Ljava/lang/String;
    if-nez p0, :cond_1

    .line 2788
    .end local p0    # "text":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p0

    .line 2773
    .restart local p0    # "text":Ljava/lang/String;
    :cond_1
    invoke-static {p0, v3}, Landroid/telephony/SmsMessage;->calculateLength(Ljava/lang/String;Z)[I

    move-result-object v0

    .line 2775
    .local v0, "params":[I
    if-eqz v0, :cond_0

    aget v2, v0, v3

    if-le v2, v4, :cond_0

    .line 2780
    const/4 v2, 0x3

    aget v2, v0, v2

    if-ne v2, v4, :cond_2

    .line 2782
    const/16 v2, 0xa0

    invoke-virtual {p0, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    :goto_1
    move-object p0, v1

    .line 2788
    goto :goto_0

    .line 2785
    :cond_2
    const/16 v2, 0x46

    invoke-virtual {p0, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method private declared-synchronized controlLocationSensor(I)V
    .locals 21
    .param p1, "nMode"    # I

    .prologue
    .line 2042
    monitor-enter p0

    :try_start_0
    const-string v18, "SafetyAssuranceService"

    const-string v19, "controlLocationSensor"

    invoke-static/range {v18 .. v19}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2043
    new-instance v12, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-direct {v12, v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;-><init>(Landroid/content/Context;)V

    .line 2044
    .local v12, "saUtils":Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;
    if-nez v12, :cond_0

    .line 2045
    const-string v18, "SafetyAssuranceService"

    const-string v19, "controlLocationSensor : saUtils is null"

    invoke-static/range {v18 .. v19}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2215
    :goto_0
    monitor-exit p0

    return-void

    .line 2049
    :cond_0
    const/16 v18, 0x1

    move/from16 v0, p1

    move/from16 v1, v18

    if-ne v0, v1, :cond_e

    .line 2050
    :try_start_1
    const-string v18, "SafetyAssuranceService"

    const-string v19, "LOCATIONSENSOR_TURNON : START"

    invoke-static/range {v18 .. v19}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2052
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIsChangingLocationSensor:Z

    move/from16 v18, v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v18, :cond_1

    .line 2054
    :try_start_2
    const-string v18, "SafetyAssuranceService"

    const-string v19, "controlLocationSensor is already being changed. Wait..."

    invoke-static/range {v18 .. v19}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2055
    const-wide/16 v18, 0x1f4

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Ljava/lang/Object;->wait(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 2056
    :catch_0
    move-exception v10

    .line 2057
    .local v10, "ex":Ljava/lang/InterruptedException;
    :try_start_3
    invoke-virtual {v10}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 2042
    .end local v10    # "ex":Ljava/lang/InterruptedException;
    .end local v12    # "saUtils":Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;
    :catchall_0
    move-exception v18

    monitor-exit p0

    throw v18

    .line 2061
    .restart local v12    # "saUtils":Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;
    :cond_1
    :try_start_4
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIsActiveLocationSensor:Z

    move/from16 v18, v0

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_2

    .line 2062
    const-string v18, "SafetyAssuranceService"

    const-string v19, "LOCATIONSENSOR_TURNON : Already activated. so END"

    invoke-static/range {v18 .. v19}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2066
    :cond_2
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIsChangingLocationSensor:Z

    .line 2074
    invoke-virtual {v12}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->getMobileDataEnabled()Z

    move-result v6

    .line 2075
    .local v6, "currentStateMobile":Z
    const-string v18, "SafetyAssuranceService"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "[Mobile] Current state["

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "]"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2076
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-static {v0, v6}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->setStateMobile(Landroid/content/Context;Z)V

    .line 2077
    if-nez v6, :cond_3

    .line 2078
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v12, v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->setMobile(Z)V

    .line 2080
    :cond_3
    invoke-virtual {v12}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->getMobileDataInRoamingEnabled()Z

    move-result v7

    .line 2081
    .local v7, "currentStateMobileInRoaming":Z
    const-string v18, "SafetyAssuranceService"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "[MobileInRomaing] Current state["

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "]"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2082
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-static {v0, v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->setStateMobileInRoaming(Landroid/content/Context;Z)V

    .line 2083
    if-nez v7, :cond_4

    .line 2084
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v12, v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->setMobileInRoaming(Z)V

    .line 2086
    :cond_4
    invoke-virtual {v12}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->getStateOfWifi()Z

    move-result v8

    .line 2087
    .local v8, "currentStateWIFI":Z
    const-string v18, "SafetyAssuranceService"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "[WIFI] Current state["

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "]"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2088
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-static {v0, v8}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->setStateWIFI(Landroid/content/Context;Z)V

    .line 2089
    if-nez v8, :cond_6

    .line 2090
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->isChinaDevice()Z

    move-result v18

    if-eqz v18, :cond_9

    .line 2091
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->getWlanPermissionAvailable()I

    move-result v11

    .line 2092
    .local v11, "originalValue":I
    const/16 v18, 0x1

    move/from16 v0, v18

    if-ne v11, v0, :cond_5

    .line 2093
    const-string v18, "SafetyAssuranceService"

    const-string v19, "[WIFI] setWlanPermissionAvailable()"

    invoke-static/range {v18 .. v19}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2094
    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->setWlanPermissionAvailable(I)V

    .line 2096
    :cond_5
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v12, v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->setWifi(Z)V

    .line 2097
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->setWlanPermissionAvailable(I)V

    .line 2103
    .end local v11    # "originalValue":I
    :cond_6
    :goto_2
    invoke-virtual {v12}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->getStateOfAgps()Z

    move-result v4

    .line 2104
    .local v4, "currentStateAGPS":Z
    const-string v18, "SafetyAssuranceService"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "[AGPS] Current state["

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "]"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2105
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-static {v0, v4}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->setStateAGPS(Landroid/content/Context;Z)V

    .line 2106
    if-nez v4, :cond_7

    .line 2107
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v12, v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->setAgps(Z)V

    .line 2109
    :cond_7
    invoke-virtual {v12}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->getStateOfLocationMode()I

    move-result v9

    .line 2110
    .local v9, "currrntStateLocationMode":I
    const-string v18, "SafetyAssuranceService"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "[Location Mode] Current state["

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "]"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2111
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-static {v0, v9}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->setStateLocationMode(Landroid/content/Context;I)V

    .line 2112
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->isChinaDevice()Z

    move-result v18

    if-eqz v18, :cond_a

    .line 2113
    const/16 v18, 0x3

    move/from16 v0, v18

    invoke-virtual {v12, v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->setLocationMode(I)V

    .line 2123
    :cond_8
    :goto_3
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIsActiveLocationSensor:Z

    .line 2124
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIsChangingLocationSensor:Z

    .line 2125
    const-string v18, "SafetyAssuranceService"

    const-string v19, "LOCATIONSENSOR_TURNON : END"

    invoke-static/range {v18 .. v19}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 2099
    .end local v4    # "currentStateAGPS":Z
    .end local v9    # "currrntStateLocationMode":I
    :cond_9
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v12, v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->setWifi(Z)V

    goto/16 :goto_2

    .line 2115
    .restart local v4    # "currentStateAGPS":Z
    .restart local v9    # "currrntStateLocationMode":I
    :cond_a
    if-eqz v9, :cond_b

    const/16 v18, 0x1

    move/from16 v0, v18

    if-ne v9, v0, :cond_c

    .line 2117
    :cond_b
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v12, v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->setLocationMode(I)V

    goto :goto_3

    .line 2118
    :cond_c
    const/16 v18, 0x2

    move/from16 v0, v18

    if-eq v9, v0, :cond_d

    const/16 v18, 0x3

    move/from16 v0, v18

    if-ne v9, v0, :cond_8

    .line 2120
    :cond_d
    const/16 v18, 0x3

    move/from16 v0, v18

    invoke-virtual {v12, v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->setLocationMode(I)V

    goto :goto_3

    .line 2127
    .end local v4    # "currentStateAGPS":Z
    .end local v6    # "currentStateMobile":Z
    .end local v7    # "currentStateMobileInRoaming":Z
    .end local v8    # "currentStateWIFI":Z
    .end local v9    # "currrntStateLocationMode":I
    :cond_e
    if-nez p1, :cond_18

    .line 2128
    const-string v18, "SafetyAssuranceService"

    const-string v19, "LOCATIONSENSOR_RESTORE : START"

    invoke-static/range {v18 .. v19}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2130
    :goto_4
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIsChangingLocationSensor:Z

    move/from16 v18, v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v18, :cond_f

    .line 2132
    :try_start_5
    const-string v18, "SafetyAssuranceService"

    const-string v19, "controlLocationSensor is already being changed. Wait..."

    invoke-static/range {v18 .. v19}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2133
    const-wide/16 v18, 0x1f4

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Ljava/lang/Object;->wait(J)V
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_4

    .line 2134
    :catch_1
    move-exception v10

    .line 2135
    .restart local v10    # "ex":Ljava/lang/InterruptedException;
    :try_start_6
    invoke-virtual {v10}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_4

    .line 2139
    .end local v10    # "ex":Ljava/lang/InterruptedException;
    :cond_f
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIsActiveLocationSensor:Z

    move/from16 v18, v0

    if-nez v18, :cond_10

    .line 2140
    const-string v18, "SafetyAssuranceService"

    const-string v19, "LOCATIONSENSOR_RESTORE : Sensor is not force activated. So END"

    invoke-static/range {v18 .. v19}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 2144
    :cond_10
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIsChangingLocationSensor:Z

    .line 2145
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->getStateMobile(Landroid/content/Context;)Z

    move-result v15

    .line 2146
    .local v15, "savedStateMobile":Z
    invoke-virtual {v12}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->getMobileDataEnabled()Z

    move-result v6

    .line 2147
    .restart local v6    # "currentStateMobile":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->getStateMobileInRoaming(Landroid/content/Context;)Z

    move-result v16

    .line 2148
    .local v16, "savedStateMobileInRoaming":Z
    invoke-virtual {v12}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->getMobileDataInRoamingEnabled()Z

    move-result v7

    .line 2149
    .restart local v7    # "currentStateMobileInRoaming":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->getStateWIFI(Landroid/content/Context;)Z

    move-result v17

    .line 2150
    .local v17, "savedStateWIFI":Z
    invoke-virtual {v12}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->getStateOfWifi()Z

    move-result v8

    .line 2151
    .restart local v8    # "currentStateWIFI":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->getStateAGPS(Landroid/content/Context;)Z

    move-result v13

    .line 2152
    .local v13, "savedStateAGPS":Z
    invoke-virtual {v12}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->getStateOfAgps()Z

    move-result v4

    .line 2153
    .restart local v4    # "currentStateAGPS":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->getStateLocationMode(Landroid/content/Context;)I

    move-result v14

    .line 2154
    .local v14, "savedStateLocationMode":I
    invoke-virtual {v12}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->getStateOfLocationMode()I

    move-result v5

    .line 2157
    .local v5, "currentStateLocationMode":I
    const-string v18, "SafetyAssuranceService"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "[Location Mode] Saved state["

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "] Current State["

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "]"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2159
    if-eq v14, v5, :cond_11

    .line 2160
    invoke-virtual {v12, v14}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->setLocationMode(I)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 2162
    :cond_11
    const-wide/16 v18, 0x1f4

    :try_start_7
    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Ljava/lang/Object;->wait(J)V
    :try_end_7
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 2167
    :goto_5
    :try_start_8
    const-string v18, "SafetyAssuranceService"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "[AGPS] Saved state["

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "] Current State["

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "]"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2168
    if-eq v13, v4, :cond_12

    .line 2169
    invoke-virtual {v12, v13}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->setAgps(Z)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 2171
    :cond_12
    const-wide/16 v18, 0x1f4

    :try_start_9
    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Ljava/lang/Object;->wait(J)V
    :try_end_9
    .catch Ljava/lang/InterruptedException; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 2176
    :goto_6
    :try_start_a
    const-string v18, "SafetyAssuranceService"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "[WIFI] Saved state["

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "] Current State["

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "]"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2177
    move/from16 v0, v17

    if-eq v0, v8, :cond_14

    .line 2178
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->isChinaDevice()Z

    move-result v18

    if-eqz v18, :cond_17

    .line 2179
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->getWlanPermissionAvailable()I

    move-result v11

    .line 2180
    .restart local v11    # "originalValue":I
    const/16 v18, 0x1

    move/from16 v0, v18

    if-ne v11, v0, :cond_13

    .line 2181
    const-string v18, "SafetyAssuranceService"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "[WIFI] setWlanPermissionAvailable(), savedStateWIFI = "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2182
    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->setWlanPermissionAvailable(I)V

    .line 2184
    :cond_13
    move/from16 v0, v17

    invoke-virtual {v12, v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->setWifi(Z)V

    .line 2185
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->setWlanPermissionAvailable(I)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 2191
    .end local v11    # "originalValue":I
    :cond_14
    :goto_7
    const-wide/16 v18, 0x1f4

    :try_start_b
    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Ljava/lang/Object;->wait(J)V
    :try_end_b
    .catch Ljava/lang/InterruptedException; {:try_start_b .. :try_end_b} :catch_4
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 2196
    :goto_8
    :try_start_c
    const-string v18, "SafetyAssuranceService"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "[MobileInRoaming] Saved state["

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "] Current State["

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "]"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2197
    move/from16 v0, v16

    if-eq v0, v7, :cond_15

    .line 2198
    move/from16 v0, v16

    invoke-virtual {v12, v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->setMobileInRoaming(Z)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 2200
    :cond_15
    const-wide/16 v18, 0x1f4

    :try_start_d
    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Ljava/lang/Object;->wait(J)V
    :try_end_d
    .catch Ljava/lang/InterruptedException; {:try_start_d .. :try_end_d} :catch_5
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 2205
    :goto_9
    :try_start_e
    const-string v18, "SafetyAssuranceService"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "[Mobile] Saved state["

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "] Current State["

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "]"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2206
    if-eq v15, v6, :cond_16

    .line 2207
    invoke-virtual {v12, v15}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->setMobile(Z)V

    .line 2209
    :cond_16
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIsActiveLocationSensor:Z

    .line 2210
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIsChangingLocationSensor:Z

    .line 2211
    const-string v18, "SafetyAssuranceService"

    const-string v19, "LOCATIONSENSOR_RESTORE : END"

    invoke-static/range {v18 .. v19}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 2163
    :catch_2
    move-exception v10

    .line 2164
    .restart local v10    # "ex":Ljava/lang/InterruptedException;
    invoke-virtual {v10}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto/16 :goto_5

    .line 2172
    .end local v10    # "ex":Ljava/lang/InterruptedException;
    :catch_3
    move-exception v10

    .line 2173
    .restart local v10    # "ex":Ljava/lang/InterruptedException;
    invoke-virtual {v10}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto/16 :goto_6

    .line 2187
    .end local v10    # "ex":Ljava/lang/InterruptedException;
    :cond_17
    move/from16 v0, v17

    invoke-virtual {v12, v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->setWifi(Z)V

    goto/16 :goto_7

    .line 2192
    :catch_4
    move-exception v10

    .line 2193
    .restart local v10    # "ex":Ljava/lang/InterruptedException;
    invoke-virtual {v10}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto/16 :goto_8

    .line 2201
    .end local v10    # "ex":Ljava/lang/InterruptedException;
    :catch_5
    move-exception v10

    .line 2202
    .restart local v10    # "ex":Ljava/lang/InterruptedException;
    invoke-virtual {v10}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_9

    .line 2213
    .end local v4    # "currentStateAGPS":Z
    .end local v5    # "currentStateLocationMode":I
    .end local v6    # "currentStateMobile":Z
    .end local v7    # "currentStateMobileInRoaming":Z
    .end local v8    # "currentStateWIFI":Z
    .end local v10    # "ex":Ljava/lang/InterruptedException;
    .end local v13    # "savedStateAGPS":Z
    .end local v14    # "savedStateLocationMode":I
    .end local v15    # "savedStateMobile":Z
    .end local v16    # "savedStateMobileInRoaming":Z
    .end local v17    # "savedStateWIFI":Z
    :cond_18
    const-string v18, "SafetyAssuranceService"

    const-string v19, "controlLocationSensor : invalid mode"

    invoke-static/range {v18 .. v19}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    goto/16 :goto_0
.end method

.method private convToDouble(I)D
    .locals 4
    .param p1, "i"    # I

    .prologue
    .line 1906
    int-to-double v0, p1

    const-wide v2, 0x412e848000000000L    # 1000000.0

    div-double/2addr v0, v2

    return-wide v0
.end method

.method private dismissQuitEmergencyDialog()V
    .locals 2

    .prologue
    .line 690
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mQuitDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mQuitDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 691
    const-string v0, "SafetyAssuranceService"

    const-string v1, "dismissQuitEmergencyDialog was called"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 692
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mQuitDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 694
    :cond_0
    return-void
.end method

.method private doStopService()V
    .locals 3

    .prologue
    .line 795
    const-string v1, "SafetyAssuranceService"

    const-string v2, "doStopService"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 796
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->setAppState(Landroid/content/Context;I)V

    .line 798
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVoiceRecording:Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;

    if-eqz v1, :cond_0

    .line 799
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVoiceRecording:Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;

    invoke-virtual {v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->getMediaRecorderState()I

    move-result v1

    const/16 v2, 0x3ec

    if-eq v1, v2, :cond_0

    .line 801
    const-string v1, "SafetyAssuranceService"

    const-string v2, "quitService - Stop Recording"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 802
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVoiceRecording:Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;

    invoke-virtual {v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->cancelRecording()Z

    .line 803
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVoiceRecording:Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;

    .line 806
    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const-class v2, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 807
    .local v0, "saService":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 808
    return-void
.end method

.method private getAddress(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$LatLng;)Ljava/lang/String;
    .locals 12
    .param p1, "point"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$LatLng;

    .prologue
    .line 1866
    const-string v2, "SafetyAssuranceService"

    const-string v3, "getAddress"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1867
    const-string v10, ""

    .line 1868
    .local v10, "returnAddress":Ljava/lang/String;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 1869
    .local v11, "sb":Ljava/lang/StringBuilder;
    const/4 v7, 0x0

    .line 1870
    .local v7, "data":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    new-instance v1, Landroid/location/Geocoder;

    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/location/Geocoder;-><init>(Landroid/content/Context;Ljava/util/Locale;)V

    .line 1873
    .local v1, "geoCoder":Landroid/location/Geocoder;
    :try_start_0
    iget-wide v2, p1, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$LatLng;->mLatitude:D

    iget-wide v4, p1, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$LatLng;->mLongitude:D

    const/4 v6, 0x1

    invoke-virtual/range {v1 .. v6}, Landroid/location/Geocoder;->getFromLocation(DDI)Ljava/util/List;

    move-result-object v7

    .line 1874
    if-eqz v7, :cond_1

    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1875
    const/4 v2, 0x0

    invoke-interface {v7, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Address;

    .line 1876
    .local v0, "address":Landroid/location/Address;
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    invoke-virtual {v0}, Landroid/location/Address;->getMaxAddressLineIndex()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    if-ge v9, v2, :cond_1

    .line 1877
    invoke-virtual {v0, v9}, Landroid/location/Address;->getAddressLine(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1878
    invoke-virtual {v0, v9}, Landroid/location/Address;->getAddressLine(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x20

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1876
    :cond_0
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 1882
    .end local v0    # "address":Landroid/location/Address;
    .end local v9    # "i":I
    :cond_1
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v10

    .line 1887
    :goto_1
    return-object v10

    .line 1884
    :catch_0
    move-exception v8

    .line 1885
    .local v8, "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method private getCurrentTime(JZ)Ljava/lang/String;
    .locals 3
    .param p1, "currentTime"    # J
    .param p3, "setLocale"    # Z

    .prologue
    .line 2590
    const/4 v0, 0x0

    .line 2591
    .local v0, "sdf":Ljava/text/SimpleDateFormat;
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2593
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->isChinaDevice()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2594
    if-eqz p3, :cond_0

    .line 2595
    new-instance v0, Ljava/text/SimpleDateFormat;

    .end local v0    # "sdf":Ljava/text/SimpleDateFormat;
    const-string v1, " (kk:mm:ss)"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 2624
    .restart local v0    # "sdf":Ljava/text/SimpleDateFormat;
    :goto_0
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 2597
    :cond_0
    new-instance v0, Ljava/text/SimpleDateFormat;

    .end local v0    # "sdf":Ljava/text/SimpleDateFormat;
    const-string v1, " (kk:mm:ss)"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .restart local v0    # "sdf":Ljava/text/SimpleDateFormat;
    goto :goto_0

    .line 2600
    :cond_1
    if-eqz p3, :cond_2

    .line 2601
    new-instance v0, Ljava/text/SimpleDateFormat;

    .end local v0    # "sdf":Ljava/text/SimpleDateFormat;
    const-string v1, " (HH:mm:ss)"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .restart local v0    # "sdf":Ljava/text/SimpleDateFormat;
    goto :goto_0

    .line 2603
    :cond_2
    new-instance v0, Ljava/text/SimpleDateFormat;

    .end local v0    # "sdf":Ljava/text/SimpleDateFormat;
    const-string v1, " (HH:mm:ss)"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .restart local v0    # "sdf":Ljava/text/SimpleDateFormat;
    goto :goto_0

    .line 2608
    :cond_3
    const-string v1, "ko"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2610
    if-eqz p3, :cond_4

    .line 2611
    new-instance v0, Ljava/text/SimpleDateFormat;

    .end local v0    # "sdf":Ljava/text/SimpleDateFormat;
    const-string v1, " (a h:mm:ss)"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .restart local v0    # "sdf":Ljava/text/SimpleDateFormat;
    goto :goto_0

    .line 2613
    :cond_4
    new-instance v0, Ljava/text/SimpleDateFormat;

    .end local v0    # "sdf":Ljava/text/SimpleDateFormat;
    const-string v1, " (a h:mm:ss)"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .restart local v0    # "sdf":Ljava/text/SimpleDateFormat;
    goto :goto_0

    .line 2617
    :cond_5
    if-eqz p3, :cond_6

    .line 2618
    new-instance v0, Ljava/text/SimpleDateFormat;

    .end local v0    # "sdf":Ljava/text/SimpleDateFormat;
    const-string v1, " (h:mm:ss a)"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .restart local v0    # "sdf":Ljava/text/SimpleDateFormat;
    goto :goto_0

    .line 2620
    :cond_6
    new-instance v0, Ljava/text/SimpleDateFormat;

    .end local v0    # "sdf":Ljava/text/SimpleDateFormat;
    const-string v1, " (h:mm:ss a)"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .restart local v0    # "sdf":Ljava/text/SimpleDateFormat;
    goto :goto_0
.end method

.method private getEmergencyContact()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2032
    new-instance v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;

    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;-><init>(Landroid/content/Context;)V

    .line 2033
    .local v0, "saUtils":Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;
    if-nez v0, :cond_0

    .line 2034
    const-string v1, "SafetyAssuranceService"

    const-string v2, "getEmergencyContact : saUtils is null"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2035
    const/4 v1, 0x0

    .line 2037
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->getEmergencyContact()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private declared-synchronized getMyLocation()Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$LatLng;
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 1802
    monitor-enter p0

    :try_start_0
    const-string v2, "SafetyAssuranceService"

    const-string v3, "getMyLocation"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1803
    const/4 v0, 0x0

    .line 1804
    .local v0, "myLocation":Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$LatLng;
    const/4 v9, 0x0

    .line 1807
    .local v9, "location":Landroid/location/Location;
    const/4 v6, 0x0

    .line 1811
    .local v6, "cnt":I
    :goto_0
    const-class v2, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1812
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->getAppState(Landroid/content/Context;)I

    move-result v7

    .line 1813
    .local v7, "curState":I
    if-eqz v7, :cond_0

    const/4 v3, 0x1

    if-ne v7, v3, :cond_1

    .line 1814
    :cond_0
    const-string v3, "SafetyAssuranceService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "curState is ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] skip getMyLocation"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1815
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1860
    :goto_1
    monitor-exit p0

    return-object v1

    .line 1817
    :cond_1
    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1820
    :try_start_3
    iget-boolean v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIsActiveGPSProvider:Z

    if-eqz v2, :cond_2

    .line 1821
    const-string v2, "SafetyAssuranceService"

    const-string v3, "GPS location is used"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1822
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mLocationManager:Landroid/location/LocationManager;

    const-string v3, "gps"

    invoke-virtual {v2, v3}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v9

    .line 1835
    :goto_2
    if-nez v9, :cond_4

    .line 1837
    const-wide/16 v2, 0x3e8

    :try_start_4
    invoke-virtual {p0, v2, v3}, Ljava/lang/Object;->wait(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1841
    :goto_3
    const/16 v2, 0x3c

    if-ge v6, v2, :cond_3

    .line 1842
    add-int/lit8 v6, v6, 0x1

    .line 1843
    :try_start_5
    const-string v2, "SafetyAssuranceService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getMyLocation retry ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    .line 1802
    .end local v0    # "myLocation":Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$LatLng;
    .end local v6    # "cnt":I
    .end local v7    # "curState":I
    .end local v9    # "location":Landroid/location/Location;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 1817
    .restart local v0    # "myLocation":Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$LatLng;
    .restart local v6    # "cnt":I
    .restart local v9    # "location":Landroid/location/Location;
    :catchall_1
    move-exception v1

    :try_start_6
    monitor-exit v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    throw v1

    .line 1825
    .restart local v7    # "curState":I
    :cond_2
    const-string v2, "SafetyAssuranceService"

    const-string v3, "Network location is used"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1826
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mLocationManager:Landroid/location/LocationManager;

    const-string v3, "network"

    invoke-virtual {v2, v3}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v9

    goto :goto_2

    .line 1838
    :catch_0
    move-exception v8

    .line 1839
    .local v8, "ex":Ljava/lang/InterruptedException;
    invoke-virtual {v8}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_3

    .line 1845
    .end local v8    # "ex":Ljava/lang/InterruptedException;
    :cond_3
    const-string v2, "SafetyAssuranceService"

    const-string v3, "getMyLocation retry timeout"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1853
    :cond_4
    if-eqz v9, :cond_5

    .line 1854
    const-string v1, "SafetyAssuranceService"

    const-string v2, "I got my location"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1856
    new-instance v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$LatLng;

    .end local v0    # "myLocation":Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$LatLng;
    invoke-virtual {v9}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v9}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$LatLng;-><init>(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;DD)V

    .restart local v0    # "myLocation":Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$LatLng;
    move-object v1, v0

    .line 1857
    goto :goto_1

    .line 1859
    :cond_5
    const-string v2, "SafetyAssuranceService"

    const-string v3, "I couldn\'t get my location"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_1
.end method

.method private getURL(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$LatLng;)Ljava/lang/String;
    .locals 8
    .param p1, "point"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$LatLng;

    .prologue
    const/4 v3, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1891
    const-string v0, ""

    .line 1893
    .local v0, "returnURL":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->isChinaDevice()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1894
    const-string v1, "http://mo.amap.com/?q=%.6f,%.6f&name=Emergency_declared&dev=1"

    new-array v2, v3, [Ljava/lang/Object;

    iget-wide v4, p1, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$LatLng;->mLatitude:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v2, v6

    iget-wide v4, p1, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$LatLng;->mLongitude:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1902
    :goto_0
    return-object v0

    .line 1896
    :cond_0
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->isJPNDevice()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->isJPNSecMessageDisable()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1897
    const-string v1, "http://maps.google.com/maps?f=q&q=(%.6f,%.6f)&z=15"

    new-array v2, v3, [Ljava/lang/Object;

    iget-wide v4, p1, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$LatLng;->mLatitude:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v2, v6

    iget-wide v4, p1, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$LatLng;->mLongitude:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1899
    :cond_1
    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v2, "http://maps.google.com/maps?f=q&q=(%.6f,%.6f)"

    new-array v3, v3, [Ljava/lang/Object;

    iget-wide v4, p1, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$LatLng;->mLatitude:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v3, v6

    iget-wide v4, p1, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$LatLng;->mLongitude:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private init()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 247
    const-string v2, "SafetyAssuranceService"

    const-string v3, "init - START"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    .line 249
    iput-object p0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mService:Landroid/app/Service;

    .line 250
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const-string v3, "location"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/location/LocationManager;

    iput-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mLocationManager:Landroid/location/LocationManager;

    .line 251
    iput-boolean v4, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIsActiveLocationListener:Z

    .line 252
    iput-boolean v4, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIsActiveLocationSensor:Z

    .line 253
    iput-boolean v4, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIsChangingLocationSensor:Z

    .line 254
    iput-object v5, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mFrontPictureUri:Landroid/net/Uri;

    .line 255
    iput-object v5, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRearPictureUri:Landroid/net/Uri;

    .line 256
    iput-boolean v4, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mNeedQuit:Z

    .line 257
    iput-object v5, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mQuitDialog:Landroid/app/AlertDialog;

    .line 258
    iput-object v5, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mOrientation:Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;

    .line 259
    iput-object v5, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mServiceBinder:Lcom/android/mms/transaction/IMessageBackgroundSender;

    .line 260
    iput-object v5, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mCallbackBinder:Lcom/android/mms/transaction/ISnsRemoteService;

    .line 261
    iput-boolean v4, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSvcConnected:Z

    .line 262
    iput-boolean v4, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mCallbackConnected:Z

    .line 263
    iput-object v5, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVocServiceBinder:Lcom/android/mms/transaction/IMessageBackgroundSender;

    .line 264
    iput-object v5, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVocCallbackBinder:Lcom/android/mms/transaction/ISnsRemoteService;

    .line 265
    iput-boolean v4, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVocSvcConnected:Z

    .line 266
    iput-boolean v4, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVocCallbackConnected:Z

    .line 267
    iput-boolean v4, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIsActiveGPSProvider:Z

    .line 268
    iput-boolean v4, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mForceStopStart:Z

    .line 269
    iput-object v5, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mImageUri:Landroid/net/Uri;

    .line 270
    iput-boolean v4, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIsSosMode:Z

    .line 271
    iput v4, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSosMode:I

    .line 273
    invoke-static {}, Landroid/telephony/SmsManager;->getDefault()Landroid/telephony/SmsManager;

    move-result-object v2

    sput-object v2, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSMS:Landroid/telephony/SmsManager;

    .line 275
    iput v4, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRequestApp:I

    .line 276
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "send_dual_captured_image"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSendPictures:I

    .line 281
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->featureSupportNewUX()Z

    move-result v2

    if-nez v2, :cond_3

    .line 282
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "the_string_of_emergency_message"

    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 283
    .local v0, "messageText":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 284
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const v3, 0x7f09004f

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 290
    :cond_0
    :goto_0
    invoke-direct {p0, v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->setMessageText(Ljava/lang/String;)V

    .line 292
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->featureSupportNewUX()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 294
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "automatic_sending_interval"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 297
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "automatic_sending_interval"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIntervalTime:I

    .line 300
    new-instance v2, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    invoke-direct {v2, p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;-><init>(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)V

    iput-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mWorkThread:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    .line 301
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mWorkThread:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    invoke-virtual {v2, v6}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->setDaemon(Z)V

    .line 302
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mWorkThread:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    invoke-virtual {v2}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->start()V

    .line 305
    new-instance v2, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3, p0}, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;-><init>(Landroid/content/Context;Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)V

    iput-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mPhoneState:Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

    .line 306
    iput v6, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mCurrentPhoneState:I

    .line 307
    iput-boolean v4, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mFirstMessage:Z

    .line 311
    new-instance v1, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;

    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;-><init>(Landroid/content/Context;)V

    .line 312
    .local v1, "saUtils":Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;
    invoke-virtual {v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->isDisableCameraOption()Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRemoveCameraOption:Z

    .line 313
    invoke-virtual {v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->isVocRecHide()Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRemoveVocRecOption:Z

    .line 314
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "send_vocrec"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSendVocRec:I

    .line 315
    const/4 v2, 0x5

    iput v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVocRecordingTime:I

    .line 316
    iput-object v5, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSendingListMsg:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;

    .line 317
    iput-object v5, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSendingListVoc:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListVoc;

    .line 326
    iput v4, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRunningCounter:I

    .line 328
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->isVZWDevice()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 329
    const-string v2, "SafetyAssuranceService"

    const-string v3, "onCreate() : usa-vzw"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 330
    iput-boolean v6, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->isVZW:Z

    .line 333
    :cond_2
    const-string v2, "SafetyAssuranceService"

    const-string v3, "init - END"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 334
    return-void

    .line 288
    .end local v0    # "messageText":Ljava/lang/String;
    .end local v1    # "saUtils":Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;
    :cond_3
    const-string v0, "SOS!"

    .restart local v0    # "messageText":Ljava/lang/String;
    goto/16 :goto_0
.end method

.method private isCurrentNetworkChina()Z
    .locals 10

    .prologue
    const/4 v9, 0x3

    .line 1941
    const/16 v0, 0x1cc

    .line 1942
    .local v0, "CHINA_MCC":I
    const/4 v2, 0x0

    .local v2, "mcc":I
    const/4 v3, 0x0

    .line 1943
    .local v3, "mnc":I
    const/4 v5, 0x1

    .line 1945
    .local v5, "retVal":Z
    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const-string v8, "phone"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/telephony/TelephonyManager;

    .line 1946
    .local v6, "tel":Landroid/telephony/TelephonyManager;
    invoke-virtual {v6}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v4

    .line 1948
    .local v4, "networkOperator":Ljava/lang/String;
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    if-le v7, v9, :cond_0

    .line 1950
    const/4 v7, 0x0

    const/4 v8, 0x3

    :try_start_0
    invoke-virtual {v4, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 1951
    const-string v7, "SafetyAssuranceService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "[isCurrentNetworkChina]  isTestSimCard MCC : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1952
    const/4 v7, 0x3

    invoke-virtual {v4, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 1953
    const-string v7, "SafetyAssuranceService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "[isCurrentNetworkChina]  isTestSimCard MNC : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1959
    :cond_0
    :goto_0
    const/16 v7, 0x1cc

    if-ne v7, v2, :cond_1

    .line 1960
    const/4 v5, 0x1

    .line 1965
    :goto_1
    const-string v7, "SafetyAssuranceService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "[isCurrentNetworkChina] retVal : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1966
    return v5

    .line 1954
    :catch_0
    move-exception v1

    .line 1955
    .local v1, "e":Ljava/lang/NumberFormatException;
    const-string v7, "SafetyAssuranceService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "[isCurrentNetworkChina] Exception occured! : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1962
    .end local v1    # "e":Ljava/lang/NumberFormatException;
    :cond_1
    const/4 v5, 0x0

    goto :goto_1
.end method

.method private parseRecipient(Ljava/lang/String;)[Ljava/lang/String;
    .locals 8
    .param p1, "recipient"    # Ljava/lang/String;

    .prologue
    const/4 v7, -0x1

    .line 1732
    const/4 v3, 0x0

    .line 1733
    .local v3, "parIndex":I
    const/4 v0, 0x0

    .line 1734
    .local v0, "arrCount":I
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1737
    .local v2, "mTemp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1738
    const/4 v4, 0x0

    .line 1760
    :cond_0
    return-object v4

    .line 1748
    :cond_1
    const/4 v6, 0x0

    invoke-virtual {p1, v6, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 1749
    .local v5, "recs":Ljava/lang/String;
    add-int/lit8 v6, v3, 0x1

    invoke-virtual {p1, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    .line 1750
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1751
    add-int/lit8 v0, v0, 0x1

    .line 1741
    .end local v5    # "recs":Ljava/lang/String;
    :cond_2
    if-eq v3, v7, :cond_3

    .line 1742
    const/16 v6, 0x2c

    invoke-virtual {p1, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    .line 1743
    if-ne v3, v7, :cond_1

    .line 1744
    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1745
    add-int/lit8 v0, v0, 0x1

    .line 1754
    :cond_3
    new-array v4, v0, [Ljava/lang/String;

    .line 1756
    .local v4, "recipients":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 1757
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    aput-object v6, v4, v1

    .line 1756
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private quitService()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 697
    const-string v6, "SafetyAssuranceService"

    const-string v7, "quitService"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 699
    iget-object v6, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    invoke-static {v6, v9}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceApp;->setWakeLock(Landroid/content/Context;Z)V

    .line 702
    iget-boolean v6, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mNeedQuit:Z

    if-nez v6, :cond_1

    .line 704
    iget-object v6, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mPhoneState:Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

    if-eqz v6, :cond_0

    .line 705
    iget-object v6, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mPhoneState:Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

    invoke-virtual {v6, v8}, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->setSosMode(Z)V

    .line 706
    iget-object v6, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mPhoneState:Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

    invoke-virtual {v6}, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->close()V

    .line 708
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->removeAlarm()V

    .line 709
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "sos_mode"

    invoke-static {v6, v7, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 710
    iget-object v6, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    invoke-static {v6, p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceNoti;->hideNotification(Landroid/content/Context;Landroid/app/Service;)V

    .line 714
    :cond_1
    iput-boolean v9, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mNeedQuit:Z

    .line 716
    monitor-enter p0

    .line 717
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->getAppState(Landroid/content/Context;)I

    move-result v2

    .line 719
    .local v2, "nCurrentAppState":I
    const/4 v6, 0x7

    if-ne v2, v6, :cond_3

    .line 721
    iget-object v6, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const/4 v7, 0x0

    invoke-static {v6, v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->setAppState(Landroid/content/Context;I)V

    .line 723
    iget-boolean v6, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mForceStopStart:Z

    if-eqz v6, :cond_2

    .line 725
    iget-object v6, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const-string v7, "alarm"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/AlarmManager;

    .line 726
    .local v1, "am":Landroid/app/AlarmManager;
    new-instance v0, Landroid/content/Intent;

    iget-object v6, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const-class v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;

    invoke-direct {v0, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 727
    .local v0, "alarmintent":Landroid/content/Intent;
    const-string v6, "android.intent.action.QUIT_EMERGENCY_SITUATION_FORCESTOP"

    invoke-virtual {v0, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 729
    iget-object v6, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const/4 v7, 0x0

    const/high16 v8, 0x8000000

    invoke-static {v6, v7, v0, v8}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 730
    .local v3, "sender":Landroid/app/PendingIntent;
    invoke-virtual {v1, v3}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 731
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mForceStopStart:Z

    .line 734
    .end local v0    # "alarmintent":Landroid/content/Intent;
    .end local v1    # "am":Landroid/app/AlarmManager;
    .end local v3    # "sender":Landroid/app/PendingIntent;
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->doStopService()V

    .line 749
    :goto_0
    monitor-exit p0

    .line 750
    return-void

    .line 737
    :cond_3
    const-string v6, "SafetyAssuranceService"

    const-string v7, "quitService - Make alarm for force stop"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 739
    iget-object v6, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const-string v7, "alarm"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/AlarmManager;

    .line 740
    .restart local v1    # "am":Landroid/app/AlarmManager;
    new-instance v0, Landroid/content/Intent;

    iget-object v6, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const-class v7, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;

    invoke-direct {v0, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 741
    .restart local v0    # "alarmintent":Landroid/content/Intent;
    const-string v6, "android.intent.action.QUIT_EMERGENCY_SITUATION_FORCESTOP"

    invoke-virtual {v0, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 743
    iget-object v6, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const/4 v7, 0x0

    const/high16 v8, 0x8000000

    invoke-static {v6, v7, v0, v8}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 744
    .restart local v3    # "sender":Landroid/app/PendingIntent;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    const-wide/32 v8, 0x1d4c0

    add-long v4, v6, v8

    .line 745
    .local v4, "startTime":J
    const/4 v6, 0x0

    invoke-virtual {v1, v6, v4, v5, v3}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 746
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mForceStopStart:Z

    goto :goto_0

    .line 749
    .end local v0    # "alarmintent":Landroid/content/Intent;
    .end local v1    # "am":Landroid/app/AlarmManager;
    .end local v2    # "nCurrentAppState":I
    .end local v3    # "sender":Landroid/app/PendingIntent;
    .end local v4    # "startTime":J
    :catchall_0
    move-exception v6

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6
.end method

.method private quitServiceRightNow(Z)V
    .locals 9
    .param p1, "makeNotification"    # Z

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 754
    const-class v4, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    monitor-enter v4

    .line 755
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->getAppState(Landroid/content/Context;)I

    move-result v0

    .line 756
    .local v0, "curState":I
    const-string v1, "SafetyAssuranceService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "quitServiceRightNow curState["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 757
    if-eqz v0, :cond_0

    if-ne v0, v8, :cond_1

    .line 758
    :cond_0
    monitor-exit v4

    .line 791
    :goto_0
    return-void

    .line 760
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const/4 v5, 0x0

    invoke-static {v1, v5}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->setAppState(Landroid/content/Context;I)V

    .line 761
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const/4 v5, 0x1

    invoke-static {v1, v5}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceApp;->setWakeLock(Landroid/content/Context;Z)V

    .line 764
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mWorkHandler:Landroid/os/Handler;

    const/4 v5, 0x1

    invoke-virtual {v1, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 765
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mWorkHandler:Landroid/os/Handler;

    const/4 v5, 0x2

    invoke-virtual {v1, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 766
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mWorkHandler:Landroid/os/Handler;

    const/4 v5, 0x3

    invoke-virtual {v1, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 767
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mWorkHandler:Landroid/os/Handler;

    const/4 v5, 0x4

    invoke-virtual {v1, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 768
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mWorkHandler:Landroid/os/Handler;

    const/4 v5, 0x5

    invoke-virtual {v1, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 769
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mWorkHandler:Landroid/os/Handler;

    const/16 v5, 0x8

    invoke-virtual {v1, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 770
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 774
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mPhoneState:Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

    if-eqz v1, :cond_2

    .line 775
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mPhoneState:Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

    invoke-virtual {v1, v7}, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->setSosMode(Z)V

    .line 776
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mPhoneState:Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

    invoke-virtual {v1}, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->close()V

    .line 778
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->removeAlarm()V

    .line 779
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v4, "sos_mode"

    invoke-static {v1, v4, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 780
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    invoke-static {v1, p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceNoti;->hideNotification(Landroid/content/Context;Landroid/app/Service;)V

    .line 782
    if-eqz p1, :cond_3

    .line 783
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 784
    .local v2, "curTime":J
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceNoti;->addFinishNotification(Landroid/content/Context;J)V

    .line 787
    .end local v2    # "curTime":J
    :cond_3
    monitor-enter p0

    .line 789
    :try_start_1
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->doStopService()V

    .line 790
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 770
    .end local v0    # "curState":I
    :catchall_1
    move-exception v1

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v1
.end method

.method private reSendingEmergencyMessage(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 7
    .param p1, "recipient"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;
    .param p3, "requestCode"    # I

    .prologue
    const/4 v2, 0x0

    .line 494
    new-instance v6, Landroid/content/Intent;

    const-string v0, "com.sec.android.app.safetyassurance.SENT_MESSAGE"

    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const-class v3, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;

    invoke-direct {v6, v0, v2, v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 495
    .local v6, "intent":Landroid/content/Intent;
    const-string v0, "message"

    invoke-virtual {v6, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 496
    const-string v0, "recipient"

    invoke-virtual {v6, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 497
    const-string v0, "failed"

    const/4 v1, 0x1

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 499
    const-string v0, "SafetyAssuranceService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "reSendingEmergencyMessage  recipient = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " message = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " requestCode="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 501
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const/high16 v1, 0x8000000

    invoke-static {v0, p3, v6, v1}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    .line 502
    .local v4, "sentPI":Landroid/app/PendingIntent;
    sget-object v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSMS:Landroid/telephony/SmsManager;

    move-object v1, p1

    move-object v3, p2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V

    .line 503
    return-void
.end method

.method private restartInit()V
    .locals 4

    .prologue
    .line 337
    const-string v0, "SafetyAssuranceService"

    const-string v1, "restartInit - START"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 339
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->setAppState(Landroid/content/Context;I)V

    .line 341
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->getReminderStartedTime(Landroid/content/Context;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mCurrentTime:J

    .line 342
    new-instance v0, Ljava/util/Date;

    iget-wide v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mCurrentTime:J

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mCurrentDate:Ljava/util/Date;

    .line 345
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "automatic_sending_interval"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIntervalTime:I

    .line 346
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    iget-wide v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mCurrentTime:J

    iget v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIntervalTime:I

    invoke-static {v0, p0, v2, v3, v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceNoti;->addNotification(Landroid/content/Context;Landroid/app/Service;JI)V

    .line 350
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->stopLocationListener()V

    .line 351
    iget v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIntervalTime:I

    if-lez v0, :cond_0

    .line 352
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->startLocationListener()V

    .line 355
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mPhoneState:Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

    invoke-virtual {v0}, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->restart()V

    .line 357
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->isVZWDevice()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 358
    const-string v0, "SafetyAssuranceService"

    const-string v1, "onCreate() : usa-vzw"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 359
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->isVZW:Z

    .line 362
    :cond_1
    const-string v0, "SafetyAssuranceService"

    const-string v1, "restartInit - END"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 363
    return-void
.end method

.method private sendEmergencyMessage()V
    .locals 2

    .prologue
    .line 967
    const-string v0, "SafetyAssuranceService"

    const-string v1, "sendEmergencyMessage"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 968
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->isJPNDevice()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->isJPNSecMessageDisable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 970
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->sendingEmergencyMessage(I)V

    .line 971
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->sendingEmergencyMessage(I)V

    .line 980
    :goto_0
    return-void

    .line 972
    :cond_0
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->isJPNSecMessageDisable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 974
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->sendingEmergencyMessage(I)V

    .line 975
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->sendingEmergencyMessage(I)V

    goto :goto_0

    .line 977
    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->sendingEmergencyMessage(I)V

    .line 978
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->sendingEmergencyMessage(I)V

    goto :goto_0
.end method

.method private sendIntentToMsg(Landroid/content/Intent;II)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "nAppId"    # I
    .param p3, "nMsgType"    # I

    .prologue
    .line 1709
    packed-switch p2, :pswitch_data_0

    .line 1729
    :cond_0
    :goto_0
    return-void

    .line 1711
    :pswitch_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mServiceBinder:Lcom/android/mms/transaction/IMessageBackgroundSender;

    if-eqz v1, :cond_0

    .line 1712
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mServiceBinder:Lcom/android/mms/transaction/IMessageBackgroundSender;

    invoke-interface {v1, p2, p3, p1}, Lcom/android/mms/transaction/IMessageBackgroundSender;->sendMessage(IILandroid/content/Intent;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1726
    :catch_0
    move-exception v0

    .line 1727
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 1717
    .end local v0    # "e":Landroid/os/RemoteException;
    :pswitch_1
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVocServiceBinder:Lcom/android/mms/transaction/IMessageBackgroundSender;

    if-eqz v1, :cond_0

    .line 1718
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVocServiceBinder:Lcom/android/mms/transaction/IMessageBackgroundSender;

    invoke-interface {v1, p2, p3, p1}, Lcom/android/mms/transaction/IMessageBackgroundSender;->sendMessage(IILandroid/content/Intent;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1709
    nop

    :pswitch_data_0
    .packed-switch 0xcad
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private sendVocMessage()V
    .locals 2

    .prologue
    .line 1113
    const-string v0, "SafetyAssuranceService"

    const-string v1, "sendVocMessage"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1114
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->sendingEmergencyMessage(I)V

    .line 1115
    return-void
.end method

.method private sendingEmergencyMessage(I)V
    .locals 51
    .param p1, "SendType"    # I

    .prologue
    .line 1279
    const-string v4, "SafetyAssuranceService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "sendingEmergencyMessage SendType:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, p1

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1282
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRecipients:[Ljava/lang/String;

    if-nez v4, :cond_1

    .line 1283
    const-string v4, "SafetyAssuranceService"

    const-string v6, "Error!!! sendingEmergencyMessage mRecipients is empty!!! Update again"

    invoke-static {v4, v6}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1284
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->updateRecipient()Z

    move-result v4

    if-nez v4, :cond_1

    .line 1286
    const-string v4, "SafetyAssuranceService"

    const-string v6, "Error!!! sendingEmergencyMessage mRecipients is still empty!!!"

    invoke-static {v4, v6}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1288
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRunningCounter:I

    .line 1289
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mWorkHandler:Landroid/os/Handler;

    if-eqz v4, :cond_0

    .line 1290
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mWorkHandler:Landroid/os/Handler;

    invoke-virtual {v4}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v45

    .line 1291
    .local v45, "newMsg":Landroid/os/Message;
    const/16 v4, 0x8

    move-object/from16 v0, v45

    iput v4, v0, Landroid/os/Message;->what:I

    .line 1292
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mWorkHandler:Landroid/os/Handler;

    move-object/from16 v0, v45

    invoke-virtual {v4, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1294
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mWorkHandler:Landroid/os/Handler;

    invoke-virtual {v4}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v46

    .line 1295
    .local v46, "newMsg2":Landroid/os/Message;
    sparse-switch p1, :sswitch_data_0

    .line 1705
    .end local v45    # "newMsg":Landroid/os/Message;
    .end local v46    # "newMsg2":Landroid/os/Message;
    :cond_0
    :goto_0
    return-void

    .line 1302
    .restart local v45    # "newMsg":Landroid/os/Message;
    .restart local v46    # "newMsg2":Landroid/os/Message;
    :sswitch_0
    const/4 v4, 0x3

    move-object/from16 v0, v45

    iput v4, v0, Landroid/os/Message;->what:I

    .line 1303
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mWorkHandler:Landroid/os/Handler;

    move-object/from16 v0, v46

    invoke-virtual {v4, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 1306
    :sswitch_1
    const/4 v4, 0x5

    move-object/from16 v0, v45

    iput v4, v0, Landroid/os/Message;->what:I

    .line 1307
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mWorkHandler:Landroid/os/Handler;

    move-object/from16 v0, v46

    invoke-virtual {v4, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 1325
    .end local v45    # "newMsg":Landroid/os/Message;
    .end local v46    # "newMsg2":Landroid/os/Message;
    :cond_1
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRequestApp:I

    .line 1327
    if-nez p1, :cond_b

    .line 1328
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->getMyLocation()Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$LatLng;

    move-result-object v38

    .line 1329
    .local v38, "location":Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$LatLng;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    .line 1330
    .local v18, "cTime":J
    const/4 v4, 0x1

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-direct {v0, v1, v2, v4}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->getCurrentTime(JZ)Ljava/lang/String;

    move-result-object v21

    .line 1331
    .local v21, "cTimeStringWithLocale":Ljava/lang/String;
    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-direct {v0, v1, v2, v4}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->getCurrentTime(JZ)Ljava/lang/String;

    move-result-object v20

    .line 1332
    .local v20, "cTimeString":Ljava/lang/String;
    const-class v6, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    monitor-enter v6

    .line 1333
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->getAppState(Landroid/content/Context;)I

    move-result v22

    .line 1334
    .local v22, "curState":I
    if-eqz v22, :cond_2

    const/4 v4, 0x1

    move/from16 v0, v22

    if-ne v0, v4, :cond_3

    .line 1335
    :cond_2
    const-string v4, "SafetyAssuranceService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "curState is ["

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, "] skip SENDING_MESSAGE"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1336
    monitor-exit v6

    goto :goto_0

    .line 1338
    .end local v22    # "curState":I
    :catchall_0
    move-exception v4

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .restart local v22    # "curState":I
    :cond_3
    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1341
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->isChinaDevice()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1342
    if-nez v38, :cond_4

    .line 1343
    const/16 v40, 0x0

    .line 1344
    .local v40, "loopCount":I
    const/4 v15, 0x6

    .line 1345
    .local v15, "MAX_LOOP":I
    const/16 v16, 0x5dc

    .line 1346
    .local v16, "WAIT_INTERVAL":I
    :goto_1
    move/from16 v0, v40

    if-ge v0, v15, :cond_4

    if-nez v38, :cond_4

    .line 1347
    const-string v4, "SafetyAssuranceService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "location is null, retry for getting location loopCount # : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v40

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1349
    move/from16 v0, v16

    int-to-long v6, v0

    :try_start_2
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V

    .line 1350
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->getMyLocation()Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$LatLng;
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_3

    move-result-object v38

    .line 1353
    :goto_2
    add-int/lit8 v40, v40, 0x1

    goto :goto_1

    .line 1358
    .end local v15    # "MAX_LOOP":I
    .end local v16    # "WAIT_INTERVAL":I
    .end local v40    # "loopCount":I
    :cond_4
    if-nez v38, :cond_7

    .line 1359
    new-instance v26, Landroid/content/Intent;

    const-string v4, "com.android.mms.transaction.Send.BACKGROUND_MSG"

    move-object/from16 v0, v26

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1360
    .local v26, "intent":Landroid/content/Intent;
    monitor-enter v26

    .line 1362
    :try_start_3
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->featureSupportNewUX()Z

    move-result v4

    if-nez v4, :cond_6

    .line 1363
    new-instance v42, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mMessageText:Ljava/lang/String;

    move-object/from16 v0, v42

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1368
    .local v42, "msgBuilder":Ljava/lang/StringBuilder;
    :goto_3
    const-string v4, "\n"

    move-object/from16 v0, v42

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const v7, 0x7f090013

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "\n"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1371
    const-string v4, "recipients"

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRecipients:[Ljava/lang/String;

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 1372
    const-string v4, "requestApp"

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRequestApp:I

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1373
    const-string v4, "message"

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1374
    const-string v4, "forcemms"

    const/4 v6, 0x1

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1375
    const/16 v4, 0xcad

    const/16 v6, 0x64

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v0, v1, v4, v6}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->sendIntentToMsg(Landroid/content/Intent;II)V

    .line 1376
    monitor-exit v26
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1377
    const-string v4, "SafetyAssuranceService"

    const-string v6, "SENDING_SMS 0 (CFL)"

    invoke-static {v4, v6}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1701
    .end local v18    # "cTime":J
    .end local v20    # "cTimeString":Ljava/lang/String;
    .end local v21    # "cTimeStringWithLocale":Ljava/lang/String;
    .end local v22    # "curState":I
    .end local v26    # "intent":Landroid/content/Intent;
    .end local v38    # "location":Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$LatLng;
    .end local v42    # "msgBuilder":Ljava/lang/StringBuilder;
    :cond_5
    :goto_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRecipients:[Ljava/lang/String;

    array-length v0, v4

    move/from16 v44, v0

    .line 1702
    .local v44, "nSize":I
    const/16 v24, 0x0

    .local v24, "i":I
    :goto_5
    move/from16 v0, v24

    move/from16 v1, v44

    if-ge v0, v1, :cond_0

    .line 1703
    const-string v4, "SafetyAssuranceService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mRecipients["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v24

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRecipients:[Ljava/lang/String;

    aget-object v7, v7, v24

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 1702
    add-int/lit8 v24, v24, 0x1

    goto :goto_5

    .line 1366
    .end local v24    # "i":I
    .end local v44    # "nSize":I
    .restart local v18    # "cTime":J
    .restart local v20    # "cTimeString":Ljava/lang/String;
    .restart local v21    # "cTimeStringWithLocale":Ljava/lang/String;
    .restart local v22    # "curState":I
    .restart local v26    # "intent":Landroid/content/Intent;
    .restart local v38    # "location":Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$LatLng;
    :cond_6
    :try_start_4
    new-instance v42, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const v6, 0x7f09002f

    invoke-virtual {v4, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v42

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .restart local v42    # "msgBuilder":Ljava/lang/StringBuilder;
    goto/16 :goto_3

    .line 1376
    .end local v42    # "msgBuilder":Ljava/lang/StringBuilder;
    :catchall_1
    move-exception v4

    monitor-exit v26
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v4

    .line 1380
    .end local v26    # "intent":Landroid/content/Intent;
    :cond_7
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->featureSupportNewUX()Z

    move-result v4

    if-nez v4, :cond_a

    .line 1384
    new-instance v27, Landroid/content/Intent;

    const-string v4, "com.android.mms.transaction.Send.BACKGROUND_MSG"

    move-object/from16 v0, v27

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1385
    .local v27, "intent01":Landroid/content/Intent;
    monitor-enter v27

    .line 1386
    :try_start_5
    new-instance v42, Ljava/lang/StringBuilder;

    invoke-direct/range {v42 .. v42}, Ljava/lang/StringBuilder;-><init>()V

    .line 1387
    .restart local v42    # "msgBuilder":Ljava/lang/StringBuilder;
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->isChinaDevice()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 1388
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const v6, 0x7f090010

    invoke-virtual {v4, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v42

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1392
    :goto_6
    const-string v4, "\n"

    move-object/from16 v0, v42

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, v38

    invoke-direct {v0, v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->getURL(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$LatLng;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1393
    const-string v4, "recipients"

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRecipients:[Ljava/lang/String;

    move-object/from16 v0, v27

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 1394
    const-string v4, "requestApp"

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRequestApp:I

    move-object/from16 v0, v27

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1395
    const-string v4, "message"

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v27

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1396
    const/16 v4, 0xcad

    const/16 v6, 0x65

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-direct {v0, v1, v4, v6}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->sendIntentToMsg(Landroid/content/Intent;II)V

    .line 1397
    monitor-exit v27
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1398
    const-string v4, "SafetyAssuranceService"

    const-string v6, "SENDING_SMS 1"

    invoke-static {v4, v6}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1403
    new-instance v28, Landroid/content/Intent;

    const-string v4, "com.android.mms.transaction.Send.BACKGROUND_MSG"

    move-object/from16 v0, v28

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1404
    .local v28, "intent02":Landroid/content/Intent;
    monitor-enter v28

    .line 1405
    :try_start_6
    new-instance v42, Ljava/lang/StringBuilder;

    .end local v42    # "msgBuilder":Ljava/lang/StringBuilder;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mMessageText:Ljava/lang/String;

    move-object/from16 v0, v42

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1406
    .restart local v42    # "msgBuilder":Ljava/lang/StringBuilder;
    const-string v4, "\n"

    move-object/from16 v0, v42

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, v38

    invoke-direct {v0, v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->getURL(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$LatLng;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " \n"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1407
    move-object/from16 v0, p0

    move-object/from16 v1, v38

    invoke-direct {v0, v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->getAddress(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$LatLng;)Ljava/lang/String;

    move-result-object v41

    .line 1408
    .local v41, "message":Ljava/lang/String;
    invoke-static/range {v41 .. v41}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1409
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const v6, 0x7f090013

    invoke-virtual {v4, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v41

    .line 1411
    :cond_8
    move-object/from16 v0, v42

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1412
    const-string v4, "recipients"

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRecipients:[Ljava/lang/String;

    move-object/from16 v0, v28

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 1413
    const-string v4, "requestApp"

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRequestApp:I

    move-object/from16 v0, v28

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1414
    const-string v4, "message"

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v28

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1415
    const-string v4, "forcemms"

    const/4 v6, 0x1

    move-object/from16 v0, v28

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1416
    const/16 v4, 0xcad

    const/16 v6, 0x66

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-direct {v0, v1, v4, v6}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->sendIntentToMsg(Landroid/content/Intent;II)V

    .line 1417
    monitor-exit v28
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 1418
    const-string v4, "SafetyAssuranceService"

    const-string v6, "SENDING_SMS 2"

    invoke-static {v4, v6}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 1390
    .end local v28    # "intent02":Landroid/content/Intent;
    .end local v41    # "message":Ljava/lang/String;
    :cond_9
    :try_start_7
    const-string v4, "Safety assistance"

    move-object/from16 v0, v42

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 1397
    .end local v42    # "msgBuilder":Ljava/lang/StringBuilder;
    :catchall_2
    move-exception v4

    monitor-exit v27
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    throw v4

    .line 1417
    .restart local v28    # "intent02":Landroid/content/Intent;
    :catchall_3
    move-exception v4

    :try_start_8
    monitor-exit v28
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    throw v4

    .line 1425
    .end local v27    # "intent01":Landroid/content/Intent;
    .end local v28    # "intent02":Landroid/content/Intent;
    :cond_a
    new-instance v29, Landroid/content/Intent;

    const-string v4, "com.android.mms.transaction.Send.BACKGROUND_MSG"

    move-object/from16 v0, v29

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1426
    .local v29, "intent03":Landroid/content/Intent;
    monitor-enter v29

    .line 1427
    :try_start_9
    new-instance v42, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mMessageText:Ljava/lang/String;

    move-object/from16 v0, v42

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1428
    .restart local v42    # "msgBuilder":Ljava/lang/StringBuilder;
    const-string v4, "\n"

    move-object/from16 v0, v42

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, v38

    invoke-direct {v0, v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->getURL(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$LatLng;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1429
    const-string v4, "recipients"

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRecipients:[Ljava/lang/String;

    move-object/from16 v0, v29

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 1430
    const-string v4, "requestApp"

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRequestApp:I

    move-object/from16 v0, v29

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1431
    const-string v4, "message"

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v29

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1432
    const/16 v4, 0xcad

    const/16 v6, 0x65

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-direct {v0, v1, v4, v6}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->sendIntentToMsg(Landroid/content/Intent;II)V

    .line 1433
    monitor-exit v29
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    .line 1434
    const-string v4, "SafetyAssuranceService"

    const-string v6, "SENDING_SMS 1"

    invoke-static {v4, v6}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1439
    new-instance v30, Landroid/content/Intent;

    const-string v4, "com.android.mms.transaction.Send.BACKGROUND_MSG"

    move-object/from16 v0, v30

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1440
    .local v30, "intent04":Landroid/content/Intent;
    monitor-enter v30

    .line 1441
    :try_start_a
    new-instance v42, Ljava/lang/StringBuilder;

    .end local v42    # "msgBuilder":Ljava/lang/StringBuilder;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const v6, 0x7f09002f

    invoke-virtual {v4, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v42

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1442
    .restart local v42    # "msgBuilder":Ljava/lang/StringBuilder;
    const-string v4, "\n"

    move-object/from16 v0, v42

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, v38

    invoke-direct {v0, v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->getURL(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$LatLng;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1443
    const-string v4, "recipients"

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRecipients:[Ljava/lang/String;

    move-object/from16 v0, v30

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 1444
    const-string v4, "requestApp"

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRequestApp:I

    move-object/from16 v0, v30

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1445
    const-string v4, "message"

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v30

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1446
    const-string v4, "forcemms"

    const/4 v6, 0x1

    move-object/from16 v0, v30

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1447
    const/16 v4, 0xcad

    const/16 v6, 0x66

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-direct {v0, v1, v4, v6}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->sendIntentToMsg(Landroid/content/Intent;II)V

    .line 1448
    monitor-exit v30
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_5

    .line 1449
    const-string v4, "SafetyAssuranceService"

    const-string v6, "SENDING_SMS 2"

    invoke-static {v4, v6}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 1433
    .end local v30    # "intent04":Landroid/content/Intent;
    .end local v42    # "msgBuilder":Ljava/lang/StringBuilder;
    :catchall_4
    move-exception v4

    :try_start_b
    monitor-exit v29
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_4

    throw v4

    .line 1448
    .restart local v30    # "intent04":Landroid/content/Intent;
    :catchall_5
    move-exception v4

    :try_start_c
    monitor-exit v30
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_5

    throw v4

    .line 1452
    .end local v18    # "cTime":J
    .end local v20    # "cTimeString":Ljava/lang/String;
    .end local v21    # "cTimeStringWithLocale":Ljava/lang/String;
    .end local v22    # "curState":I
    .end local v29    # "intent03":Landroid/content/Intent;
    .end local v30    # "intent04":Landroid/content/Intent;
    .end local v38    # "location":Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$LatLng;
    :cond_b
    const/4 v4, 0x3

    move/from16 v0, p1

    if-ne v0, v4, :cond_e

    .line 1453
    const-class v6, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    monitor-enter v6

    .line 1454
    :try_start_d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->getAppState(Landroid/content/Context;)I

    move-result v22

    .line 1455
    .restart local v22    # "curState":I
    if-eqz v22, :cond_c

    const/4 v4, 0x1

    move/from16 v0, v22

    if-ne v0, v4, :cond_d

    .line 1456
    :cond_c
    const-string v4, "SafetyAssuranceService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "curState is ["

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, "] skip SENDING_MESSAGE_ONLY"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1457
    monitor-exit v6

    goto/16 :goto_0

    .line 1459
    .end local v22    # "curState":I
    :catchall_6
    move-exception v4

    monitor-exit v6
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_6

    throw v4

    .restart local v22    # "curState":I
    :cond_d
    :try_start_e
    monitor-exit v6
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_6

    .line 1460
    new-instance v33, Landroid/content/Intent;

    const-string v4, "com.android.mms.transaction.Send.BACKGROUND_MSG"

    move-object/from16 v0, v33

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1461
    .local v33, "intent07":Landroid/content/Intent;
    new-instance v42, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const v6, 0x7f09002f

    invoke-virtual {v4, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v42

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1463
    .restart local v42    # "msgBuilder":Ljava/lang/StringBuilder;
    const-string v4, "recipients"

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRecipients:[Ljava/lang/String;

    move-object/from16 v0, v33

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 1464
    const-string v4, "requestApp"

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRequestApp:I

    move-object/from16 v0, v33

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1465
    const-string v4, "message"

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v33

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1467
    const/16 v4, 0xcad

    const/16 v6, 0x6a

    move-object/from16 v0, p0

    move-object/from16 v1, v33

    invoke-direct {v0, v1, v4, v6}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->sendIntentToMsg(Landroid/content/Intent;II)V

    .line 1468
    const-string v4, "SafetyAssuranceService"

    const-string v6, "SENDING_SMS 3"

    invoke-static {v4, v6}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 1469
    .end local v22    # "curState":I
    .end local v33    # "intent07":Landroid/content/Intent;
    .end local v42    # "msgBuilder":Ljava/lang/StringBuilder;
    :cond_e
    const/4 v4, 0x4

    move/from16 v0, p1

    if-ne v0, v4, :cond_13

    .line 1470
    const-class v6, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    monitor-enter v6

    .line 1471
    :try_start_f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->getAppState(Landroid/content/Context;)I

    move-result v22

    .line 1472
    .restart local v22    # "curState":I
    if-eqz v22, :cond_f

    const/4 v4, 0x1

    move/from16 v0, v22

    if-ne v0, v4, :cond_10

    .line 1473
    :cond_f
    const-string v4, "SafetyAssuranceService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "curState is ["

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, "] skip SENDING_LOCATION_ONLY"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1474
    monitor-exit v6

    goto/16 :goto_0

    .line 1476
    .end local v22    # "curState":I
    :catchall_7
    move-exception v4

    monitor-exit v6
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_7

    throw v4

    .restart local v22    # "curState":I
    :cond_10
    :try_start_10
    monitor-exit v6
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_7

    .line 1477
    new-instance v36, Landroid/content/Intent;

    const-string v4, "com.android.mms.transaction.Send.BACKGROUND_MSG"

    move-object/from16 v0, v36

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1478
    .local v36, "intent10":Landroid/content/Intent;
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->getMyLocation()Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$LatLng;

    move-result-object v38

    .line 1480
    .restart local v38    # "location":Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$LatLng;
    const-string v4, "recipients"

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRecipients:[Ljava/lang/String;

    move-object/from16 v0, v36

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 1481
    const-string v4, "requestApp"

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRequestApp:I

    move-object/from16 v0, v36

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1483
    if-nez v38, :cond_11

    .line 1484
    new-instance v42, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const v6, 0x7f090013

    invoke-virtual {v4, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v42

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1486
    .restart local v42    # "msgBuilder":Ljava/lang/StringBuilder;
    const-string v4, "message"

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v36

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1487
    const/16 v4, 0xcad

    const/16 v6, 0x64

    move-object/from16 v0, p0

    move-object/from16 v1, v36

    invoke-direct {v0, v1, v4, v6}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->sendIntentToMsg(Landroid/content/Intent;II)V

    .line 1488
    const-string v4, "SafetyAssuranceService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SENDING_SMS 0 ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 1490
    .end local v42    # "msgBuilder":Ljava/lang/StringBuilder;
    :cond_11
    new-instance v42, Ljava/lang/StringBuilder;

    invoke-direct/range {v42 .. v42}, Ljava/lang/StringBuilder;-><init>()V

    .line 1491
    .restart local v42    # "msgBuilder":Ljava/lang/StringBuilder;
    move-object/from16 v0, p0

    move-object/from16 v1, v38

    invoke-direct {v0, v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->getURL(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$LatLng;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v42

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1493
    const-string v4, "message"

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v36

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1494
    const/16 v4, 0xcad

    const/16 v6, 0x65

    move-object/from16 v0, p0

    move-object/from16 v1, v36

    invoke-direct {v0, v1, v4, v6}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->sendIntentToMsg(Landroid/content/Intent;II)V

    .line 1495
    const-string v4, "SafetyAssuranceService"

    const-string v6, "SENDING_SMS 1"

    invoke-static {v4, v6}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1497
    new-instance v43, Ljava/lang/StringBuilder;

    invoke-direct/range {v43 .. v43}, Ljava/lang/StringBuilder;-><init>()V

    .line 1498
    .local v43, "msgBuilder2":Ljava/lang/StringBuilder;
    move-object/from16 v0, p0

    move-object/from16 v1, v38

    invoke-direct {v0, v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->getAddress(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$LatLng;)Ljava/lang/String;

    move-result-object v39

    .line 1499
    .local v39, "locationAddress":Ljava/lang/String;
    invoke-static/range {v39 .. v39}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_12

    .line 1500
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const v6, 0x7f090013

    invoke-virtual {v4, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v39

    .line 1502
    :cond_12
    move-object/from16 v0, v43

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1503
    const-string v4, "message"

    invoke-virtual/range {v43 .. v43}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v36

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1504
    const/16 v4, 0xcad

    const/16 v6, 0x66

    move-object/from16 v0, p0

    move-object/from16 v1, v36

    invoke-direct {v0, v1, v4, v6}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->sendIntentToMsg(Landroid/content/Intent;II)V

    .line 1505
    const-string v4, "SafetyAssuranceService"

    const-string v6, "SENDING_SMS 2"

    invoke-static {v4, v6}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 1508
    .end local v22    # "curState":I
    .end local v36    # "intent10":Landroid/content/Intent;
    .end local v38    # "location":Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$LatLng;
    .end local v39    # "locationAddress":Ljava/lang/String;
    .end local v42    # "msgBuilder":Ljava/lang/StringBuilder;
    .end local v43    # "msgBuilder2":Ljava/lang/StringBuilder;
    :cond_13
    const/4 v4, 0x1

    move/from16 v0, p1

    if-ne v0, v4, :cond_1b

    .line 1509
    const-class v6, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    monitor-enter v6

    .line 1510
    :try_start_11
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->getAppState(Landroid/content/Context;)I

    move-result v22

    .line 1511
    .restart local v22    # "curState":I
    if-eqz v22, :cond_14

    const/4 v4, 0x1

    move/from16 v0, v22

    if-ne v0, v4, :cond_15

    .line 1512
    :cond_14
    const-string v4, "SafetyAssuranceService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "curState is ["

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, "] skip SENDING_PICTURE"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1513
    monitor-exit v6

    goto/16 :goto_0

    .line 1515
    .end local v22    # "curState":I
    :catchall_8
    move-exception v4

    monitor-exit v6
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_8

    throw v4

    .restart local v22    # "curState":I
    :cond_15
    :try_start_12
    monitor-exit v6
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_8

    .line 1517
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRemoveCameraOption:Z

    if-nez v4, :cond_16

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSendPictures:I

    const/4 v6, 0x1

    if-eq v4, v6, :cond_17

    :cond_16
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIsSosMode:Z

    if-eqz v4, :cond_5

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSosMode:I

    if-ne v4, v6, :cond_5

    .line 1518
    :cond_17
    const-string v4, "SafetyAssuranceService"

    const-string v6, "SENDING_PICTURE"

    invoke-static {v4, v6}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1519
    const-string v4, "SafetyAssuranceService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "FrontPicture URI is : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mFrontPictureUri:Landroid/net/Uri;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 1520
    new-instance v31, Landroid/content/Intent;

    const-string v4, "com.android.mms.transaction.Send.BACKGROUND_MSG"

    move-object/from16 v0, v31

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1521
    .local v31, "intent05":Landroid/content/Intent;
    monitor-enter v31

    .line 1522
    :try_start_13
    const-string v4, "recipients"

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRecipients:[Ljava/lang/String;

    move-object/from16 v0, v31

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 1523
    const-string v4, "requestApp"

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRequestApp:I

    move-object/from16 v0, v31

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1524
    const-string v4, "message"

    const-string v6, ""

    move-object/from16 v0, v31

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1525
    const-string v4, "forcemms"

    const/4 v6, 0x1

    move-object/from16 v0, v31

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1526
    const-string v4, "android.intent.extra.STREAM"

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mFrontPictureUri:Landroid/net/Uri;

    move-object/from16 v0, v31

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1527
    const-string v4, "image/jpeg"

    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1528
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mFrontPictureUri:Landroid/net/Uri;

    if-eqz v4, :cond_19

    .line 1529
    const/16 v4, 0xcad

    const/16 v6, 0x67

    move-object/from16 v0, p0

    move-object/from16 v1, v31

    invoke-direct {v0, v1, v4, v6}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->sendIntentToMsg(Landroid/content/Intent;II)V

    .line 1533
    :goto_7
    monitor-exit v31
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_9

    .line 1535
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIsSosMode:Z

    if-nez v4, :cond_18

    .line 1536
    const-string v4, "SafetyAssuranceService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "RearPicture URI is : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRearPictureUri:Landroid/net/Uri;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 1537
    new-instance v32, Landroid/content/Intent;

    const-string v4, "com.android.mms.transaction.Send.BACKGROUND_MSG"

    move-object/from16 v0, v32

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1538
    .local v32, "intent06":Landroid/content/Intent;
    monitor-enter v32

    .line 1539
    :try_start_14
    const-string v4, "recipients"

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRecipients:[Ljava/lang/String;

    move-object/from16 v0, v32

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 1540
    const-string v4, "requestApp"

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRequestApp:I

    move-object/from16 v0, v32

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1541
    const-string v4, "message"

    const-string v6, ""

    move-object/from16 v0, v32

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1542
    const-string v4, "forcemms"

    const/4 v6, 0x1

    move-object/from16 v0, v32

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1543
    const-string v4, "android.intent.extra.STREAM"

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRearPictureUri:Landroid/net/Uri;

    move-object/from16 v0, v32

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1544
    const-string v4, "image/jpeg"

    move-object/from16 v0, v32

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1545
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRearPictureUri:Landroid/net/Uri;

    if-eqz v4, :cond_1a

    .line 1546
    const/16 v4, 0xcad

    const/16 v6, 0x68

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-direct {v0, v1, v4, v6}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->sendIntentToMsg(Landroid/content/Intent;II)V

    .line 1550
    :goto_8
    monitor-exit v32
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_a

    .line 1553
    .end local v32    # "intent06":Landroid/content/Intent;
    :cond_18
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mFrontPictureUri:Landroid/net/Uri;

    .line 1554
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRearPictureUri:Landroid/net/Uri;

    goto/16 :goto_4

    .line 1531
    :cond_19
    :try_start_15
    const-string v4, "SafetyAssuranceService"

    const-string v6, "FrontPicture URI is null"

    invoke-static {v4, v6}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_7

    .line 1533
    :catchall_9
    move-exception v4

    monitor-exit v31
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_9

    throw v4

    .line 1548
    .restart local v32    # "intent06":Landroid/content/Intent;
    :cond_1a
    :try_start_16
    const-string v4, "SafetyAssuranceService"

    const-string v6, "RearPicture URI is null"

    invoke-static {v4, v6}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_8

    .line 1550
    :catchall_a
    move-exception v4

    monitor-exit v32
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_a

    throw v4

    .line 1556
    .end local v22    # "curState":I
    .end local v31    # "intent05":Landroid/content/Intent;
    .end local v32    # "intent06":Landroid/content/Intent;
    :cond_1b
    const/4 v4, 0x5

    move/from16 v0, p1

    if-ne v0, v4, :cond_1e

    .line 1557
    const-class v6, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    monitor-enter v6

    .line 1558
    :try_start_17
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->getAppState(Landroid/content/Context;)I

    move-result v22

    .line 1559
    .restart local v22    # "curState":I
    if-eqz v22, :cond_1c

    const/4 v4, 0x1

    move/from16 v0, v22

    if-ne v0, v4, :cond_1d

    .line 1560
    :cond_1c
    const-string v4, "SafetyAssuranceService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "curState is ["

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, "] skip SENDING_MESSAGE_JPN"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1561
    monitor-exit v6

    goto/16 :goto_0

    .line 1563
    .end local v22    # "curState":I
    :catchall_b
    move-exception v4

    monitor-exit v6
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_b

    throw v4

    .restart local v22    # "curState":I
    :cond_1d
    :try_start_18
    monitor-exit v6
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_b

    .line 1564
    new-instance v42, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const v6, 0x7f09002f

    invoke-virtual {v4, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v42

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1567
    .restart local v42    # "msgBuilder":Ljava/lang/StringBuilder;
    new-instance v34, Landroid/content/Intent;

    const-string v4, "com.sec.android.app.safetyassurance.SENT_MESSAGE"

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const-class v9, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;

    move-object/from16 v0, v34

    invoke-direct {v0, v4, v6, v7, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 1568
    .local v34, "intent08":Landroid/content/Intent;
    const-string v4, "message"

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v34

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1570
    const/16 v47, 0x0

    .line 1572
    .local v47, "requestCode":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRecipients:[Ljava/lang/String;

    move-object/from16 v17, v0

    .local v17, "arr$":[Ljava/lang/String;
    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v37, v0

    .local v37, "len$":I
    const/16 v25, 0x0

    .local v25, "i$":I
    move/from16 v48, v47

    .end local v47    # "requestCode":I
    .local v48, "requestCode":I
    :goto_9
    move/from16 v0, v25

    move/from16 v1, v37

    if-ge v0, v1, :cond_5

    aget-object v5, v17, v25

    .line 1573
    .local v5, "recipient":Ljava/lang/String;
    const-string v4, "recipient"

    move-object/from16 v0, v34

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1574
    const-string v4, "requestCode"

    move-object/from16 v0, v34

    move/from16 v1, v48

    invoke-virtual {v0, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1575
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    add-int/lit8 v47, v48, 0x1

    .end local v48    # "requestCode":I
    .restart local v47    # "requestCode":I
    const/high16 v6, 0x8000000

    move/from16 v0, v48

    move-object/from16 v1, v34

    invoke-static {v4, v0, v1, v6}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v8

    .line 1577
    .local v8, "sentPI":Landroid/app/PendingIntent;
    :try_start_19
    sget-object v4, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSMS:Landroid/telephony/SmsManager;

    const/4 v6, 0x0

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/telephony/SmsManager;->sendTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V
    :try_end_19
    .catch Ljava/lang/IllegalArgumentException; {:try_start_19 .. :try_end_19} :catch_0

    .line 1572
    :goto_a
    add-int/lit8 v25, v25, 0x1

    move/from16 v48, v47

    .end local v47    # "requestCode":I
    .restart local v48    # "requestCode":I
    goto :goto_9

    .line 1578
    .end local v48    # "requestCode":I
    .restart local v47    # "requestCode":I
    :catch_0
    move-exception v23

    .line 1580
    .local v23, "e":Ljava/lang/IllegalArgumentException;
    const-string v4, "SafetyAssuranceService"

    const-string v6, "fail to send message"

    invoke-static {v4, v6}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1581
    const-string v4, "com.sec.android.app.safetyassurance.SENT_MESSAGE"

    move-object/from16 v0, v34

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1582
    const-string v4, "smsException"

    const/4 v6, 0x1

    move-object/from16 v0, v34

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1583
    move-object/from16 v0, p0

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_a

    .line 1588
    .end local v5    # "recipient":Ljava/lang/String;
    .end local v8    # "sentPI":Landroid/app/PendingIntent;
    .end local v17    # "arr$":[Ljava/lang/String;
    .end local v22    # "curState":I
    .end local v23    # "e":Ljava/lang/IllegalArgumentException;
    .end local v25    # "i$":I
    .end local v34    # "intent08":Landroid/content/Intent;
    .end local v37    # "len$":I
    .end local v42    # "msgBuilder":Ljava/lang/StringBuilder;
    .end local v47    # "requestCode":I
    :cond_1e
    const/4 v4, 0x6

    move/from16 v0, p1

    if-ne v0, v4, :cond_25

    .line 1589
    const-class v6, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    monitor-enter v6

    .line 1590
    :try_start_1a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->getAppState(Landroid/content/Context;)I

    move-result v22

    .line 1591
    .restart local v22    # "curState":I
    if-eqz v22, :cond_1f

    const/4 v4, 0x1

    move/from16 v0, v22

    if-ne v0, v4, :cond_20

    .line 1592
    :cond_1f
    const-string v4, "SafetyAssuranceService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "curState is ["

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, "] skip SENDING_LOCATION_JPN"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1593
    monitor-exit v6

    goto/16 :goto_0

    .line 1595
    .end local v22    # "curState":I
    :catchall_c
    move-exception v4

    monitor-exit v6
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_c

    throw v4

    .restart local v22    # "curState":I
    :cond_20
    :try_start_1b
    monitor-exit v6
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_c

    .line 1596
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->getMyLocation()Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$LatLng;

    move-result-object v38

    .line 1597
    .restart local v38    # "location":Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$LatLng;
    new-instance v35, Landroid/content/Intent;

    const-string v4, "com.sec.android.app.safetyassurance.SENT_MESSAGE"

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const-class v9, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;

    move-object/from16 v0, v35

    invoke-direct {v0, v4, v6, v7, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 1599
    .local v35, "intent09":Landroid/content/Intent;
    if-nez v38, :cond_22

    .line 1600
    new-instance v42, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const v6, 0x7f090013

    invoke-virtual {v4, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v42

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1602
    .restart local v42    # "msgBuilder":Ljava/lang/StringBuilder;
    const/16 v47, 0x4

    .line 1603
    .restart local v47    # "requestCode":I
    const-string v4, "message"

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v35

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1605
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRecipients:[Ljava/lang/String;

    move-object/from16 v17, v0

    .restart local v17    # "arr$":[Ljava/lang/String;
    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v37, v0

    .restart local v37    # "len$":I
    const/16 v25, 0x0

    .restart local v25    # "i$":I
    move/from16 v48, v47

    .end local v47    # "requestCode":I
    .restart local v48    # "requestCode":I
    :goto_b
    move/from16 v0, v25

    move/from16 v1, v37

    if-ge v0, v1, :cond_5

    aget-object v5, v17, v25

    .line 1606
    .restart local v5    # "recipient":Ljava/lang/String;
    const-string v4, "recipient"

    move-object/from16 v0, v35

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1607
    const-string v4, "requestCode"

    move-object/from16 v0, v35

    move/from16 v1, v48

    invoke-virtual {v0, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1608
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRecipients:[Ljava/lang/String;

    array-length v4, v4

    add-int/lit8 v6, v48, -0x3

    if-ne v4, v6, :cond_21

    .line 1609
    const-string v4, "lastsent"

    const/4 v6, 0x1

    move-object/from16 v0, v35

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1611
    :cond_21
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    add-int/lit8 v47, v48, 0x1

    .end local v48    # "requestCode":I
    .restart local v47    # "requestCode":I
    const/high16 v6, 0x8000000

    move/from16 v0, v48

    move-object/from16 v1, v35

    invoke-static {v4, v0, v1, v6}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v8

    .line 1613
    .restart local v8    # "sentPI":Landroid/app/PendingIntent;
    :try_start_1c
    sget-object v4, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSMS:Landroid/telephony/SmsManager;

    const/4 v6, 0x0

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/telephony/SmsManager;->sendTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V
    :try_end_1c
    .catch Ljava/lang/Exception; {:try_start_1c .. :try_end_1c} :catch_1

    .line 1605
    :goto_c
    add-int/lit8 v25, v25, 0x1

    move/from16 v48, v47

    .end local v47    # "requestCode":I
    .restart local v48    # "requestCode":I
    goto :goto_b

    .line 1614
    .end local v48    # "requestCode":I
    .restart local v47    # "requestCode":I
    :catch_1
    move-exception v23

    .line 1615
    .local v23, "e":Ljava/lang/Exception;
    const-string v4, "SafetyAssuranceService"

    const-string v6, "fail to send message"

    invoke-static {v4, v6}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1616
    const-string v4, "com.sec.android.app.safetyassurance.SENT_MESSAGE"

    move-object/from16 v0, v35

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1617
    const-string v4, "smsException"

    const/4 v6, 0x1

    move-object/from16 v0, v35

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1618
    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_c

    .line 1637
    .end local v5    # "recipient":Ljava/lang/String;
    .end local v8    # "sentPI":Landroid/app/PendingIntent;
    .end local v17    # "arr$":[Ljava/lang/String;
    .end local v23    # "e":Ljava/lang/Exception;
    .end local v25    # "i$":I
    .end local v37    # "len$":I
    .end local v42    # "msgBuilder":Ljava/lang/StringBuilder;
    .end local v47    # "requestCode":I
    :cond_22
    new-instance v42, Ljava/lang/StringBuilder;

    invoke-direct/range {v42 .. v42}, Ljava/lang/StringBuilder;-><init>()V

    .line 1638
    .restart local v42    # "msgBuilder":Ljava/lang/StringBuilder;
    move-object/from16 v0, p0

    move-object/from16 v1, v38

    invoke-direct {v0, v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->getAddress(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$LatLng;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->checkMaxLength(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v41

    .line 1639
    .restart local v41    # "message":Ljava/lang/String;
    invoke-static/range {v41 .. v41}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_23

    .line 1640
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const v6, 0x7f090013

    invoke-virtual {v4, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v41

    .line 1642
    :cond_23
    move-object/from16 v0, v42

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1644
    const-string v4, "message"

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v35

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1645
    const/16 v49, 0xc

    .line 1647
    .local v49, "requestCode_Address":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRecipients:[Ljava/lang/String;

    move-object/from16 v17, v0

    .restart local v17    # "arr$":[Ljava/lang/String;
    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v37, v0

    .restart local v37    # "len$":I
    const/16 v25, 0x0

    .restart local v25    # "i$":I
    move/from16 v50, v49

    .end local v49    # "requestCode_Address":I
    .local v50, "requestCode_Address":I
    :goto_d
    move/from16 v0, v25

    move/from16 v1, v37

    if-ge v0, v1, :cond_5

    aget-object v5, v17, v25

    .line 1648
    .restart local v5    # "recipient":Ljava/lang/String;
    const-string v4, "recipient"

    move-object/from16 v0, v35

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1649
    const-string v4, "requestCode"

    move-object/from16 v0, v35

    move/from16 v1, v50

    invoke-virtual {v0, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1650
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRecipients:[Ljava/lang/String;

    array-length v4, v4

    add-int/lit8 v6, v50, -0xb

    if-ne v4, v6, :cond_24

    .line 1651
    const-string v4, "lastsent"

    const/4 v6, 0x1

    move-object/from16 v0, v35

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1653
    :cond_24
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    add-int/lit8 v49, v50, 0x1

    .end local v50    # "requestCode_Address":I
    .restart local v49    # "requestCode_Address":I
    const/high16 v6, 0x8000000

    move/from16 v0, v50

    move-object/from16 v1, v35

    invoke-static {v4, v0, v1, v6}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v13

    .line 1655
    .local v13, "sentPI_Address":Landroid/app/PendingIntent;
    :try_start_1d
    sget-object v9, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSMS:Landroid/telephony/SmsManager;

    const/4 v11, 0x0

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    const/4 v14, 0x0

    move-object v10, v5

    invoke-virtual/range {v9 .. v14}, Landroid/telephony/SmsManager;->sendTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V
    :try_end_1d
    .catch Ljava/lang/Exception; {:try_start_1d .. :try_end_1d} :catch_2

    .line 1663
    :goto_e
    const-string v4, "SafetyAssuranceService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Send Address recipinet = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1647
    add-int/lit8 v25, v25, 0x1

    move/from16 v50, v49

    .end local v49    # "requestCode_Address":I
    .restart local v50    # "requestCode_Address":I
    goto :goto_d

    .line 1656
    .end local v50    # "requestCode_Address":I
    .restart local v49    # "requestCode_Address":I
    :catch_2
    move-exception v23

    .line 1657
    .restart local v23    # "e":Ljava/lang/Exception;
    const-string v4, "SafetyAssuranceService"

    const-string v6, "fail to send message"

    invoke-static {v4, v6}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1658
    const-string v4, "com.sec.android.app.safetyassurance.SENT_MESSAGE"

    move-object/from16 v0, v35

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1659
    const-string v4, "smsException"

    const/4 v6, 0x1

    move-object/from16 v0, v35

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1660
    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_e

    .line 1667
    .end local v5    # "recipient":Ljava/lang/String;
    .end local v13    # "sentPI_Address":Landroid/app/PendingIntent;
    .end local v17    # "arr$":[Ljava/lang/String;
    .end local v22    # "curState":I
    .end local v23    # "e":Ljava/lang/Exception;
    .end local v25    # "i$":I
    .end local v35    # "intent09":Landroid/content/Intent;
    .end local v37    # "len$":I
    .end local v38    # "location":Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$LatLng;
    .end local v41    # "message":Ljava/lang/String;
    .end local v42    # "msgBuilder":Ljava/lang/StringBuilder;
    .end local v49    # "requestCode_Address":I
    :cond_25
    const/4 v4, 0x2

    move/from16 v0, p1

    if-ne v0, v4, :cond_2b

    .line 1668
    const-class v6, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    monitor-enter v6

    .line 1669
    :try_start_1e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->getAppState(Landroid/content/Context;)I

    move-result v22

    .line 1670
    .restart local v22    # "curState":I
    if-eqz v22, :cond_26

    const/4 v4, 0x1

    move/from16 v0, v22

    if-ne v0, v4, :cond_27

    .line 1671
    :cond_26
    const-string v4, "SafetyAssuranceService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "curState is ["

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, "] skip SENDING_VOICE"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1672
    monitor-exit v6

    goto/16 :goto_0

    .line 1674
    .end local v22    # "curState":I
    :catchall_d
    move-exception v4

    monitor-exit v6
    :try_end_1e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_d

    throw v4

    .restart local v22    # "curState":I
    :cond_27
    :try_start_1f
    monitor-exit v6
    :try_end_1f
    .catchall {:try_start_1f .. :try_end_1f} :catchall_d

    .line 1676
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIsSosMode:Z

    if-nez v4, :cond_28

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRemoveVocRecOption:Z

    if-nez v4, :cond_29

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSendVocRec:I

    const/4 v6, 0x1

    if-ne v4, v6, :cond_29

    .line 1677
    :cond_28
    const-string v4, "SafetyAssuranceService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Voice URI is : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVoiceRecordingUri:Landroid/net/Uri;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 1678
    new-instance v33, Landroid/content/Intent;

    const-string v4, "com.android.mms.transaction.Send.BACKGROUND_MSG"

    move-object/from16 v0, v33

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1679
    .restart local v33    # "intent07":Landroid/content/Intent;
    monitor-enter v33

    .line 1680
    :try_start_20
    const-string v4, "recipients"

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRecipients:[Ljava/lang/String;

    move-object/from16 v0, v33

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 1681
    const-string v4, "requestApp"

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRequestApp:I

    move-object/from16 v0, v33

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1682
    const-string v4, "message"

    const-string v6, ""

    move-object/from16 v0, v33

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1683
    const-string v4, "forcemms"

    const/4 v6, 0x1

    move-object/from16 v0, v33

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1684
    const-string v4, "android.intent.extra.STREAM"

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVoiceRecordingUri:Landroid/net/Uri;

    move-object/from16 v0, v33

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1685
    const-string v4, "audio/amr"

    move-object/from16 v0, v33

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1686
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVoiceRecordingUri:Landroid/net/Uri;

    if-eqz v4, :cond_2a

    .line 1687
    const/16 v4, 0xcae

    const/16 v6, 0x69

    move-object/from16 v0, p0

    move-object/from16 v1, v33

    invoke-direct {v0, v1, v4, v6}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->sendIntentToMsg(Landroid/content/Intent;II)V

    .line 1691
    :goto_f
    monitor-exit v33
    :try_end_20
    .catchall {:try_start_20 .. :try_end_20} :catchall_e

    .line 1694
    .end local v33    # "intent07":Landroid/content/Intent;
    :cond_29
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVoiceRecordingUri:Landroid/net/Uri;

    goto/16 :goto_4

    .line 1689
    .restart local v33    # "intent07":Landroid/content/Intent;
    :cond_2a
    :try_start_21
    const-string v4, "SafetyAssuranceService"

    const-string v6, "FrontPicture URI is null"

    invoke-static {v4, v6}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_f

    .line 1691
    :catchall_e
    move-exception v4

    monitor-exit v33
    :try_end_21
    .catchall {:try_start_21 .. :try_end_21} :catchall_e

    throw v4

    .line 1697
    .end local v22    # "curState":I
    .end local v33    # "intent07":Landroid/content/Intent;
    :cond_2b
    const-string v4, "SafetyAssuranceService"

    const-string v6, "sendingEmergencyMessage : Unknown SendType"

    invoke-static {v4, v6}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 1351
    .restart local v15    # "MAX_LOOP":I
    .restart local v16    # "WAIT_INTERVAL":I
    .restart local v18    # "cTime":J
    .restart local v20    # "cTimeString":Ljava/lang/String;
    .restart local v21    # "cTimeStringWithLocale":Ljava/lang/String;
    .restart local v22    # "curState":I
    .restart local v38    # "location":Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$LatLng;
    .restart local v40    # "loopCount":I
    :catch_3
    move-exception v4

    goto/16 :goto_2

    .line 1295
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_0
        0x3 -> :sswitch_0
        0x4 -> :sswitch_0
        0x5 -> :sswitch_0
        0x6 -> :sswitch_0
        0x69 -> :sswitch_1
    .end sparse-switch
.end method

.method private setMessageText(Ljava/lang/String;)V
    .locals 0
    .param p1, "messagetext"    # Ljava/lang/String;

    .prologue
    .line 963
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mMessageText:Ljava/lang/String;

    .line 964
    return-void
.end method

.method private showQuitEmergencyDialog()V
    .locals 20

    .prologue
    .line 553
    const-string v13, "SafetyAssuranceService"

    const-string v14, "showQuitEmergencyDialog"

    invoke-static {v13, v14}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 555
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mQuitDialog:Landroid/app/AlertDialog;

    if-eqz v13, :cond_0

    .line 556
    const-string v13, "SafetyAssuranceService"

    const-string v14, "There is already Quit Dialog"

    invoke-static {v13, v14}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 687
    :goto_0
    return-void

    .line 560
    :cond_0
    const/4 v9, 0x0

    .line 562
    .local v9, "sdf":Ljava/text/SimpleDateFormat;
    new-instance v6, Landroid/content/IntentFilter;

    const-string v13, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-direct {v6, v13}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 563
    .local v6, "intentFilter":Landroid/content/IntentFilter;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v13, v14, v6}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 566
    const/4 v11, 0x4

    .line 567
    .local v11, "theme":I
    const-string v13, "ro.build.scafe.cream"

    const-string v14, "white"

    invoke-static {v13, v14}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 569
    .local v12, "themeValue":Ljava/lang/String;
    const-string v13, "white"

    invoke-virtual {v13, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 570
    const/4 v11, 0x5

    .line 572
    :cond_1
    new-instance v8, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    invoke-direct {v8, v13, v11}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 573
    .local v8, "quitDialog":Landroid/app/AlertDialog$Builder;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 575
    .local v3, "dlgBody":Ljava/lang/StringBuilder;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const-string v14, "layout_inflater"

    invoke-virtual {v13, v14}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/LayoutInflater;

    .line 576
    .local v5, "inflater":Landroid/view/LayoutInflater;
    const v13, 0x7f03000a

    const/4 v14, 0x0

    invoke-virtual {v5, v13, v14}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    .line 577
    .local v7, "layout":Landroid/view/View;
    const v13, 0x7f0b0024

    invoke-virtual {v7, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 578
    .local v10, "textView":Landroid/widget/TextView;
    const v13, 0x7f0b0022

    invoke-virtual {v7, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 579
    .local v4, "imageView":Landroid/widget/ImageView;
    const v13, 0x7f0b0023

    invoke-virtual {v7, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Chronometer;

    .line 580
    .local v2, "chronometer":Landroid/widget/Chronometer;
    const v13, 0x7f020006

    invoke-virtual {v4, v13}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 582
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v14

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    invoke-static {v13}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->getReminderStartedTime(Landroid/content/Context;)J

    move-result-wide v18

    sub-long v16, v16, v18

    sub-long v14, v14, v16

    invoke-virtual {v2, v14, v15}, Landroid/widget/Chronometer;->setBase(J)V

    .line 583
    invoke-virtual {v2}, Landroid/widget/Chronometer;->start()V

    .line 585
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    invoke-static {v13}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 587
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->isChinaDevice()Z

    move-result v13

    if-eqz v13, :cond_3

    .line 588
    new-instance v9, Ljava/text/SimpleDateFormat;

    .end local v9    # "sdf":Ljava/text/SimpleDateFormat;
    const-string v13, "HH:mm"

    invoke-direct {v9, v13}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 604
    .restart local v9    # "sdf":Ljava/text/SimpleDateFormat;
    :goto_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mCurrentDate:Ljava/util/Date;

    if-nez v13, :cond_2

    .line 606
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    invoke-static {v13}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->getReminderStartedTime(Landroid/content/Context;)J

    move-result-wide v14

    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mCurrentTime:J

    .line 607
    new-instance v13, Ljava/util/Date;

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mCurrentTime:J

    invoke-direct {v13, v14, v15}, Ljava/util/Date;-><init>(J)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mCurrentDate:Ljava/util/Date;

    .line 610
    :cond_2
    const-string v13, "\n"

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const v15, 0x7f09002e

    invoke-virtual {v14, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, " : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mCurrentDate:Ljava/util/Date;

    invoke-virtual {v9, v15}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 613
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIntervalTime:I

    if-lez v13, :cond_6

    .line 615
    const v13, 0x7f09004d

    invoke-virtual {v8, v13}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v13

    const/high16 v14, 0x1040000

    new-instance v15, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$3;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$3;-><init>(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)V

    invoke-virtual {v13, v14, v15}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v13

    const v14, 0x104000a

    new-instance v15, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$2;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$2;-><init>(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)V

    invoke-virtual {v13, v14, v15}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 635
    const-string v13, "\n"

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const v15, 0x7f09003f

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIntervalTime:I

    move/from16 v18, v0

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-virtual/range {v14 .. v16}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 670
    :goto_2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v10, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 671
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f070009

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getColor(I)I

    move-result v13

    invoke-virtual {v10, v13}, Landroid/widget/TextView;->setTextColor(I)V

    .line 672
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f070009

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getColor(I)I

    move-result v13

    invoke-virtual {v2, v13}, Landroid/widget/Chronometer;->setTextColor(I)V

    .line 673
    invoke-virtual {v8, v7}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 674
    new-instance v13, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$5;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$5;-><init>(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)V

    invoke-virtual {v8, v13}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 684
    invoke-virtual {v8}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mQuitDialog:Landroid/app/AlertDialog;

    .line 685
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mQuitDialog:Landroid/app/AlertDialog;

    invoke-virtual {v13}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v13

    const/16 v14, 0x7d8

    invoke-virtual {v13, v14}, Landroid/view/Window;->setType(I)V

    .line 686
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mQuitDialog:Landroid/app/AlertDialog;

    invoke-virtual {v13}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_0

    .line 590
    :cond_3
    new-instance v9, Ljava/text/SimpleDateFormat;

    .end local v9    # "sdf":Ljava/text/SimpleDateFormat;
    const-string v13, "kk:mm"

    invoke-direct {v9, v13}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .restart local v9    # "sdf":Ljava/text/SimpleDateFormat;
    goto/16 :goto_1

    .line 594
    :cond_4
    const-string v13, "ko"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v14

    invoke-virtual {v14}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 596
    new-instance v9, Ljava/text/SimpleDateFormat;

    .end local v9    # "sdf":Ljava/text/SimpleDateFormat;
    const-string v13, "a h:mm"

    invoke-direct {v9, v13}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .restart local v9    # "sdf":Ljava/text/SimpleDateFormat;
    goto/16 :goto_1

    .line 599
    :cond_5
    new-instance v9, Ljava/text/SimpleDateFormat;

    .end local v9    # "sdf":Ljava/text/SimpleDateFormat;
    const-string v13, "h:mm a"

    invoke-direct {v9, v13}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .restart local v9    # "sdf":Ljava/text/SimpleDateFormat;
    goto/16 :goto_1

    .line 639
    :cond_6
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->featureSupportNewUX()Z

    move-result v13

    if-nez v13, :cond_7

    .line 640
    const v13, 0x7f09002e

    invoke-virtual {v8, v13}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 649
    :goto_3
    const v13, 0x7f090015

    new-instance v14, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$4;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$4;-><init>(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)V

    invoke-virtual {v8, v13, v14}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_2

    .line 643
    :cond_7
    const v13, 0x7f090040

    invoke-virtual {v8, v13}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 644
    invoke-virtual {v2}, Landroid/widget/Chronometer;->stop()V

    .line 645
    const/16 v13, 0x8

    invoke-virtual {v2, v13}, Landroid/widget/Chronometer;->setVisibility(I)V

    .line 646
    new-instance v3, Ljava/lang/StringBuilder;

    .end local v3    # "dlgBody":Ljava/lang/StringBuilder;
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->isVZW:Z

    if-eqz v13, :cond_8

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const v14, 0x7f090030

    invoke-virtual {v13, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    :goto_4
    invoke-direct {v3, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .restart local v3    # "dlgBody":Ljava/lang/StringBuilder;
    goto :goto_3

    .end local v3    # "dlgBody":Ljava/lang/StringBuilder;
    :cond_8
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const v14, 0x7f090031

    invoke-virtual {v13, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    goto :goto_4
.end method

.method private declared-synchronized startLocationListener()V
    .locals 7

    .prologue
    .line 852
    monitor-enter p0

    :try_start_0
    const-string v0, "SafetyAssuranceService"

    const-string v1, "startLocationListener"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 853
    iget-boolean v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIsActiveLocationListener:Z

    if-nez v0, :cond_0

    .line 854
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIsActiveGPSProvider:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 856
    :try_start_1
    const-string v0, "SafetyAssuranceService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) = "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mLocationManager:Landroid/location/LocationManager;

    const-string v5, "gps"

    invoke-virtual {v4, v5}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 857
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mLocationManager:Landroid/location/LocationManager;

    const-string v1, "gps"

    const-wide/16 v2, 0x3e8

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mGPSLocationListener:Landroid/location/LocationListener;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 863
    :goto_0
    :try_start_2
    const-string v0, "SafetyAssuranceService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER) = "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mLocationManager:Landroid/location/LocationManager;

    const-string v5, "network"

    invoke-virtual {v4, v5}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 864
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->isChinaDevice()Z

    move-result v0

    if-eqz v0, :cond_1

    const-wide/16 v2, 0x4e20

    .line 865
    .local v2, "minTime":J
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mLocationManager:Landroid/location/LocationManager;

    const-string v1, "network"

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mNetworkLocationListener:Landroid/location/LocationListener;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 869
    .end local v2    # "minTime":J
    :goto_2
    const/4 v0, 0x1

    :try_start_3
    iput-boolean v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIsActiveLocationListener:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 871
    :cond_0
    monitor-exit p0

    return-void

    .line 858
    :catch_0
    move-exception v6

    .line 859
    .local v6, "e":Ljava/lang/Exception;
    :try_start_4
    const-string v0, "SafetyAssuranceService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "locationProvider request e : "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 852
    .end local v6    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 864
    :cond_1
    const-wide/16 v2, 0x2710

    goto :goto_1

    .line 866
    :catch_1
    move-exception v6

    .line 867
    .restart local v6    # "e":Ljava/lang/Exception;
    :try_start_5
    const-string v0, "SafetyAssuranceService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "locationProvider request e : "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2
.end method

.method private startMessageBindService()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 883
    monitor-enter p0

    .line 884
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->getAppState(Landroid/content/Context;)I

    move-result v0

    .line 885
    .local v0, "nAppState":I
    if-nez v0, :cond_0

    .line 886
    const-string v1, "SafetyAssuranceService"

    const-string v2, "startMessageBindService is skipped. Already ONEDESTORY mode"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 887
    monitor-exit p0

    .line 910
    :goto_0
    return-void

    .line 889
    :cond_0
    iget v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRunningCounter:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRunningCounter:I

    .line 890
    const-string v1, "SafetyAssuranceService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startMessageBindService mRunningCounter = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRunningCounter:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 891
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const/4 v2, 0x6

    invoke-static {v1, v2}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->setAppState(Landroid/content/Context;I)V

    .line 893
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 894
    const-string v1, "SafetyAssuranceService"

    const-string v2, "startMessageBindService"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 897
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->isJPNSecMessageDisable()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 898
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$6;

    invoke-direct {v2, p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$6;-><init>(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 893
    .end local v0    # "nAppState":I
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 905
    .restart local v0    # "nAppState":I
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->stopMessageBindService()V

    .line 907
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const-class v2, Lcom/android/mms/transaction/IMessageBackgroundSender;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    invoke-static {v2, v3}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->getExplicitIntent(Ljava/lang/String;Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mServiceConn:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 908
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const-class v2, Lcom/android/mms/transaction/ISnsRemoteService;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    invoke-static {v2, v3}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->getExplicitIntent(Ljava/lang/String;Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mCallbackConn:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    goto :goto_0
.end method

.method private startVocMessageBindService()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 927
    monitor-enter p0

    .line 928
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->getAppState(Landroid/content/Context;)I

    move-result v0

    .line 929
    .local v0, "nAppState":I
    if-nez v0, :cond_0

    .line 930
    const-string v1, "SafetyAssuranceService"

    const-string v2, "startVocMessageBindService is skipped. Already ONEDESTORY mode"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 931
    monitor-exit p0

    .line 945
    :goto_0
    return-void

    .line 933
    :cond_0
    iget v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRunningCounter:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRunningCounter:I

    .line 934
    const-string v1, "SafetyAssuranceService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startVocMessageBindService mRunningCounter = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRunningCounter:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 935
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const/4 v2, 0x6

    invoke-static {v1, v2}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->setAppState(Landroid/content/Context;I)V

    .line 937
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 938
    const-string v1, "SafetyAssuranceService"

    const-string v2, "startVocMessageBindService"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 940
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->stopVocMessageBindService()V

    .line 942
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const-class v2, Lcom/android/mms/transaction/IMessageBackgroundSender;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    invoke-static {v2, v3}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->getExplicitIntent(Ljava/lang/String;Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVocServiceConn:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 943
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const-class v2, Lcom/android/mms/transaction/ISnsRemoteService;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    invoke-static {v2, v3}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->getExplicitIntent(Ljava/lang/String;Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVocCallbackConn:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    goto :goto_0

    .line 937
    .end local v0    # "nAppState":I
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private declared-synchronized stopLocationListener()V
    .locals 2

    .prologue
    .line 874
    monitor-enter p0

    :try_start_0
    const-string v0, "SafetyAssuranceService"

    const-string v1, "stopLocationListener"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 875
    iget-boolean v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIsActiveLocationListener:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 876
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mLocationManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mGPSLocationListener:Landroid/location/LocationListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 877
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mLocationManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mNetworkLocationListener:Landroid/location/LocationListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 878
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIsActiveLocationListener:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 880
    :cond_0
    monitor-exit p0

    return-void

    .line 874
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private stopMessageBindService()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 913
    iget-boolean v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mCallbackConnected:Z

    if-eqz v0, :cond_0

    .line 914
    const-string v0, "SafetyAssuranceService"

    const-string v1, "stopMessageCallback"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 915
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mCallbackConn:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 916
    iput-boolean v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mCallbackConnected:Z

    .line 919
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSvcConnected:Z

    if-eqz v0, :cond_1

    .line 920
    const-string v0, "SafetyAssuranceService"

    const-string v1, "stopMessageBind"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 921
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mServiceConn:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 922
    iput-boolean v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSvcConnected:Z

    .line 924
    :cond_1
    return-void
.end method

.method private stopVocMessageBindService()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 948
    iget-boolean v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVocCallbackConnected:Z

    if-eqz v0, :cond_0

    .line 949
    const-string v0, "SafetyAssuranceService"

    const-string v1, "stopVocCallback"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 950
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVocCallbackConn:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 951
    iput-boolean v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVocCallbackConnected:Z

    .line 954
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVocSvcConnected:Z

    if-eqz v0, :cond_1

    .line 955
    const-string v0, "SafetyAssuranceService"

    const-string v1, "stopVocMessageBind"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 956
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVocServiceConn:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 957
    iput-boolean v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVocSvcConnected:Z

    .line 959
    :cond_1
    return-void
.end method

.method private takePictures(Z)V
    .locals 3
    .param p1, "bFirstPicture"    # Z

    .prologue
    .line 2218
    iget-boolean v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRemoveCameraOption:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSendPictures:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2219
    const-string v0, "SafetyAssuranceService"

    const-string v1, "takePicturesOnstart"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2220
    new-instance v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mOrientation:Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;

    invoke-direct {v0, v1, p0, v2}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;-><init>(Landroid/content/Context;Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;)V

    iput-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mCameraCapturer:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    .line 2221
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mCameraCapturer:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->isFirstPicures(Z)V

    .line 2222
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mCameraCapturer:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    invoke-virtual {v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->CaptureFrontAndRearImage()V

    .line 2224
    :cond_0
    return-void
.end method

.method private declared-synchronized updateRecipient()Z
    .locals 2

    .prologue
    .line 1765
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->getEmergencyContact()Ljava/lang/String;

    move-result-object v0

    .line 1766
    .local v0, "receiverList":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1767
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRecipients:[Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1768
    const/4 v1, 0x0

    .line 1771
    :goto_0
    monitor-exit p0

    return v1

    .line 1770
    :cond_0
    :try_start_1
    invoke-direct {p0, v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->parseRecipient(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRecipients:[Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1771
    const/4 v1, 0x1

    goto :goto_0

    .line 1765
    .end local v0    # "receiverList":Ljava/lang/String;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method


# virtual methods
.method public getSosModeOn()Z
    .locals 1

    .prologue
    .line 2747
    iget-boolean v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIsSosMode:Z

    return v0
.end method

.method public getWlanPermissionAvailable()I
    .locals 3

    .prologue
    .line 2755
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "wlan_permission_available"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getWorkHanler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 2634
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mWorkHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 367
    const-string v0, "SafetyAssuranceService"

    const-string v1, "onBind"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 368
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 241
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 242
    const-string v0, "SafetyAssuranceService"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->init()V

    .line 244
    return-void
.end method

.method public onDestroy()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 507
    const-string v1, "SafetyAssuranceService"

    const-string v2, "onDestroy - START"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 510
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mWorkLooper:Landroid/os/Looper;

    if-nez v1, :cond_0

    .line 511
    monitor-enter p0

    .line 513
    :try_start_0
    const-string v1, "SafetyAssuranceService"

    const-string v2, "onDestroy - Wait until thread has started"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 514
    const-wide/16 v2, 0x64

    invoke-virtual {p0, v2, v3}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 519
    :goto_1
    :try_start_1
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 515
    :catch_0
    move-exception v0

    .line 516
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_2
    const-string v1, "SafetyAssuranceService"

    const-string v2, "InterruptedException!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 517
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 521
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mWorkLooper:Landroid/os/Looper;

    invoke-virtual {v1}, Landroid/os/Looper;->quit()V

    .line 523
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->stopMessageBindService()V

    .line 524
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->stopVocMessageBindService()V

    .line 526
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const-wide/16 v2, 0x0

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->setReminderStartedTime(Landroid/content/Context;J)V

    .line 529
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mQuitDialog:Landroid/app/AlertDialog;

    if-eqz v1, :cond_1

    .line 530
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mQuitDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 531
    iput-object v5, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mQuitDialog:Landroid/app/AlertDialog;

    .line 534
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mOrientation:Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;

    if-eqz v1, :cond_2

    .line 535
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mOrientation:Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;

    invoke-virtual {v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;->close()V

    .line 536
    iput-object v5, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mOrientation:Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;

    .line 539
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->stopLocationListener()V

    .line 542
    invoke-direct {p0, v4}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->controlLocationSensor(I)V

    .line 544
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceApp;->ifForegroundActivityIsSASettingThenItIsResumed(Landroid/content/Context;)V

    .line 545
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->setAppState(Landroid/content/Context;I)V

    .line 547
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 548
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    invoke-static {v1, v4}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceApp;->setWakeLock(Landroid/content/Context;Z)V

    .line 549
    const-string v1, "SafetyAssuranceService"

    const-string v2, "onDestroy - END"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 550
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 13
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startid"    # I

    .prologue
    .line 373
    invoke-super/range {p0 .. p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    .line 376
    :goto_0
    iget-object v10, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mWorkHandler:Landroid/os/Handler;

    if-nez v10, :cond_0

    .line 377
    monitor-enter p0

    .line 379
    :try_start_0
    const-string v10, "SafetyAssuranceService"

    const-string v11, "Wait! System is making WorkThread ..."

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    const-wide/16 v10, 0x64

    invoke-virtual {p0, v10, v11}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 385
    :goto_1
    :try_start_1
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v10

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v10

    .line 381
    :catch_0
    move-exception v1

    .line 382
    .local v1, "e":Ljava/lang/InterruptedException;
    :try_start_2
    const-string v10, "SafetyAssuranceService"

    const-string v11, "InterruptedException!"

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 383
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 388
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :cond_0
    if-nez p1, :cond_1

    .line 389
    const-string v10, "SafetyAssuranceService"

    const-string v11, "onStartCommand [intent is null] RESTART!!!"

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 390
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->restartInit()V

    .line 391
    iget-object v10, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const/4 v11, 0x7

    invoke-static {v10, v11}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->setAppState(Landroid/content/Context;I)V

    .line 392
    iget-object v10, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const/4 v11, 0x0

    invoke-static {v10, v11}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceApp;->setWakeLock(Landroid/content/Context;Z)V

    .line 393
    const/4 v10, 0x1

    .line 489
    :goto_2
    return v10

    .line 396
    :cond_1
    iget-object v10, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const/4 v11, 0x1

    invoke-static {v10, v11}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceApp;->setWakeLock(Landroid/content/Context;Z)V

    .line 398
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 399
    .local v0, "action":Ljava/lang/String;
    const-string v10, "SafetyAssuranceService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "onStartCommand ["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "]"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 400
    iget-object v10, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mWorkHandler:Landroid/os/Handler;

    invoke-virtual {v10}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v4

    .line 402
    .local v4, "msg":Landroid/os/Message;
    const-string v10, "SafetyAssuranceService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "action = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    const-string v10, "SafetyAssuranceService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "interval = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIntervalTime:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 405
    const-string v10, "android.intent.action.SAFETY_SERVICE_START"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 406
    const-string v10, "voice"

    const/4 v11, 0x0

    invoke-virtual {p1, v10, v11}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 407
    .local v2, "isVoice":Z
    const-string v10, "data"

    invoke-virtual {p1, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 408
    .local v9, "tempStr":Ljava/lang/String;
    const-string v10, "sos_mode"

    const/4 v11, 0x0

    invoke-virtual {p1, v10, v11}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v10

    iput-boolean v10, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIsSosMode:Z

    .line 409
    if-nez v2, :cond_5

    .line 410
    if-eqz v9, :cond_2

    .line 411
    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    iput-object v10, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mImageUri:Landroid/net/Uri;

    .line 413
    :cond_2
    iget-object v10, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mImageUri:Landroid/net/Uri;

    if-eqz v10, :cond_4

    .line 414
    const/4 v10, 0x1

    iput v10, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSosMode:I

    .line 418
    :goto_3
    const-string v10, "SafetyAssuranceService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "mIsSosMode = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-boolean v12, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIsSosMode:Z

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " mSosMode = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSosMode:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 419
    const-string v10, "SafetyAssuranceService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "mIntervalTime = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIntervalTime:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 420
    const/4 v10, 0x0

    iput v10, v4, Landroid/os/Message;->what:I

    .line 421
    iget-object v10, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mWorkHandler:Landroid/os/Handler;

    invoke-virtual {v10, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 429
    :goto_4
    iget-boolean v10, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIsSosMode:Z

    if-eqz v10, :cond_7

    .line 430
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const-string v11, "sos_mode"

    const/4 v12, 0x1

    invoke-static {v10, v11, v12}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 434
    :goto_5
    iget-object v10, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mPhoneState:Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

    if-eqz v10, :cond_3

    .line 435
    iget-object v10, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mPhoneState:Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

    iget-boolean v11, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIsSosMode:Z

    invoke-virtual {v10, v11}, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->setSosMode(Z)V

    .line 489
    .end local v2    # "isVoice":Z
    .end local v9    # "tempStr":Ljava/lang/String;
    :cond_3
    :goto_6
    const/4 v10, 0x1

    goto/16 :goto_2

    .line 416
    .restart local v2    # "isVoice":Z
    .restart local v9    # "tempStr":Ljava/lang/String;
    :cond_4
    const/4 v10, 0x2

    iput v10, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSosMode:I

    goto :goto_3

    .line 423
    :cond_5
    if-eqz v9, :cond_6

    .line 424
    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    iput-object v10, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVoiceRecordingUri:Landroid/net/Uri;

    .line 426
    :cond_6
    const/4 v10, 0x4

    iput v10, v4, Landroid/os/Message;->what:I

    .line 427
    iget-object v10, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mWorkHandler:Landroid/os/Handler;

    invoke-virtual {v10, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_4

    .line 432
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const-string v11, "sos_mode"

    const/4 v12, 0x0

    invoke-static {v10, v11, v12}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_5

    .line 437
    .end local v2    # "isVoice":Z
    .end local v9    # "tempStr":Ljava/lang/String;
    :cond_8
    const-string v10, "android.intent.action.SAFETY_SERVICE_ALARM"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 439
    const-string v10, "SA_FIRSTALARM"

    const/4 v11, 0x0

    invoke-virtual {p1, v10, v11}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v10

    iput-boolean v10, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mFirstMessage:Z

    .line 440
    const/4 v10, 0x1

    iput v10, v4, Landroid/os/Message;->what:I

    .line 441
    iget-object v10, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mWorkHandler:Landroid/os/Handler;

    invoke-virtual {v10, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_6

    .line 442
    :cond_9
    const-string v10, "android.intent.action.SAFETY_SERVICE_B_ALARM"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_d

    .line 443
    const/4 v10, 0x0

    iput-object v10, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mImageUri:Landroid/net/Uri;

    .line 444
    const/4 v10, 0x0

    iput-object v10, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVoiceRecordingUri:Landroid/net/Uri;

    .line 445
    const-string v10, "SA_FIRSTALARM"

    const/4 v11, 0x0

    invoke-virtual {p1, v10, v11}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v10

    iput-boolean v10, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mFirstMessage:Z

    .line 446
    const-string v10, "data"

    invoke-virtual {p1, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 447
    .restart local v9    # "tempStr":Ljava/lang/String;
    const-string v10, "sos_mode"

    const/4 v11, 0x0

    invoke-virtual {p1, v10, v11}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v10

    iput-boolean v10, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIsSosMode:Z

    .line 448
    if-eqz v9, :cond_a

    .line 449
    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    iput-object v10, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mImageUri:Landroid/net/Uri;

    .line 451
    :cond_a
    iget-object v10, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mImageUri:Landroid/net/Uri;

    if-eqz v10, :cond_c

    .line 452
    const/4 v10, 0x1

    iput v10, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSosMode:I

    .line 456
    :goto_7
    iget-object v10, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mPhoneState:Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

    if-eqz v10, :cond_b

    .line 457
    iget-object v10, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mPhoneState:Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

    iget-boolean v11, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIsSosMode:Z

    invoke-virtual {v10, v11}, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->setSosMode(Z)V

    .line 459
    :cond_b
    const/4 v10, 0x1

    iput v10, v4, Landroid/os/Message;->what:I

    .line 460
    iget-object v10, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mWorkHandler:Landroid/os/Handler;

    invoke-virtual {v10, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_6

    .line 454
    :cond_c
    const/4 v10, 0x2

    iput v10, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSosMode:I

    goto :goto_7

    .line 461
    .end local v9    # "tempStr":Ljava/lang/String;
    :cond_d
    const-string v10, "android.intent.action.SAFETY_SERVICE_SHOW_STOPDIALOG"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_e

    .line 462
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->showQuitEmergencyDialog()V

    goto/16 :goto_6

    .line 464
    :cond_e
    const-string v10, "android.intent.action.SAFETY_SERVICE_FORCESTOP"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_f

    .line 465
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->doStopService()V

    goto/16 :goto_6

    .line 467
    :cond_f
    const-string v10, "android.intent.action.SAFETY_SERVICE_SENDMSG_COMPLETE"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_11

    .line 469
    iget-object v10, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mWorkHandler:Landroid/os/Handler;

    invoke-virtual {v10}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v5

    .line 470
    .local v5, "newMsg":Landroid/os/Message;
    const/16 v10, 0x8

    iput v10, v5, Landroid/os/Message;->what:I

    .line 471
    iget-object v10, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mWorkHandler:Landroid/os/Handler;

    invoke-virtual {v10, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 474
    iget v10, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRunningCounter:I

    if-lez v10, :cond_10

    .line 475
    iget v10, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRunningCounter:I

    add-int/lit8 v10, v10, -0x1

    iput v10, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRunningCounter:I

    .line 476
    :cond_10
    iget-object v10, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mWorkHandler:Landroid/os/Handler;

    invoke-virtual {v10}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v6

    .line 477
    .local v6, "newMsg2":Landroid/os/Message;
    const/4 v10, 0x3

    iput v10, v6, Landroid/os/Message;->what:I

    .line 478
    iget-object v10, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mWorkHandler:Landroid/os/Handler;

    invoke-virtual {v10, v6}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_6

    .line 479
    .end local v5    # "newMsg":Landroid/os/Message;
    .end local v6    # "newMsg2":Landroid/os/Message;
    :cond_11
    const-string v10, "android.intent.action.SAFETY_SERVICE_RESENDMSG"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_12

    .line 480
    const-string v10, "recipient"

    invoke-virtual {p1, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 481
    .local v7, "recipient":Ljava/lang/String;
    const-string v10, "message"

    invoke-virtual {p1, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 482
    .local v3, "message":Ljava/lang/String;
    const-string v10, "requestCode"

    const/4 v11, 0x0

    invoke-virtual {p1, v10, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    .line 484
    .local v8, "requestCode":I
    invoke-direct {p0, v7, v3, v8}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->reSendingEmergencyMessage(Ljava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_6

    .line 486
    .end local v3    # "message":Ljava/lang/String;
    .end local v7    # "recipient":Ljava/lang/String;
    .end local v8    # "requestCode":I
    :cond_12
    const-string v10, "SafetyAssuranceService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "ERROR!!! Unknown action["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "]"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_6
.end method

.method public removeAlarm()V
    .locals 7

    .prologue
    const/high16 v6, 0x8000000

    const/4 v5, 0x0

    .line 811
    const-string v3, "SafetyAssuranceService"

    const-string v4, "remove B Alarm"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 812
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const-string v4, "alarm"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/AlarmManager;

    .line 813
    .local v1, "am":Landroid/app/AlarmManager;
    new-instance v0, Landroid/content/Intent;

    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const-class v4, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 814
    .local v0, "alarmintent":Landroid/content/Intent;
    const-string v3, "android.intent.action.SAFETY_MESSAGE_B_ALARM_ACTION"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 815
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    invoke-static {v3, v5, v0, v6}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 816
    .local v2, "sender":Landroid/app/PendingIntent;
    invoke-virtual {v1, v2}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 818
    const-string v3, "SafetyAssuranceService"

    const-string v4, "remove Phone Alarm"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 819
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const-string v4, "alarm"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "am":Landroid/app/AlarmManager;
    check-cast v1, Landroid/app/AlarmManager;

    .line 820
    .restart local v1    # "am":Landroid/app/AlarmManager;
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "alarmintent":Landroid/content/Intent;
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const-class v4, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 821
    .restart local v0    # "alarmintent":Landroid/content/Intent;
    const-string v3, "android.intent.action.SAFETY_MESSAGE_ALARM_ACTION"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 822
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    invoke-static {v3, v5, v0, v6}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 823
    invoke-virtual {v1, v2}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 824
    return-void
.end method

.method public setAlarm()V
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 827
    const-string v8, "SafetyAssuranceService"

    const-string v9, "setAlarm"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 830
    iget v8, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIntervalTime:I

    if-gtz v8, :cond_0

    .line 831
    const-string v1, "SafetyAssuranceService"

    const-string v8, "Send once. Just ignore remind"

    invoke-static {v1, v8}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 849
    :goto_0
    return-void

    .line 834
    :cond_0
    const-string v8, "SafetyAssuranceService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Send message every ["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIntervalTime:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "] min"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 837
    iget-object v8, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const-string v9, "alarm"

    invoke-virtual {v8, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 839
    .local v0, "am":Landroid/app/AlarmManager;
    new-instance v7, Landroid/content/Intent;

    iget-object v8, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const-class v9, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;

    invoke-direct {v7, v8, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 840
    .local v7, "alarmintent":Landroid/content/Intent;
    const/4 v8, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string v10, "sos_mode"

    invoke-static {v9, v10, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v9

    if-ne v8, v9, :cond_1

    .line 841
    const-string v8, "android.intent.action.SAFETY_MESSAGE_B_ALARM_ACTION"

    invoke-virtual {v7, v8}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 845
    :goto_1
    iget-object v8, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    const/high16 v9, 0x8000000

    invoke-static {v8, v1, v7, v9}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 846
    .local v6, "sender":Landroid/app/PendingIntent;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    iget v10, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIntervalTime:I

    mul-int/lit8 v10, v10, 0x3c

    mul-int/lit16 v10, v10, 0x3e8

    int-to-long v10, v10

    add-long v2, v8, v10

    .line 847
    .local v2, "startTime":J
    iget v8, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIntervalTime:I

    mul-int/lit8 v8, v8, 0x3c

    mul-int/lit16 v8, v8, 0x3e8

    int-to-long v4, v8

    .line 848
    .local v4, "cycleTime":J
    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    goto :goto_0

    .line 843
    .end local v2    # "startTime":J
    .end local v4    # "cycleTime":J
    .end local v6    # "sender":Landroid/app/PendingIntent;
    :cond_1
    const-string v8, "android.intent.action.SAFETY_MESSAGE_ALARM_ACTION"

    invoke-virtual {v7, v8}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1
.end method

.method public setFrontPictureUri(Landroid/net/Uri;)V
    .locals 3
    .param p1, "fileUri"    # Landroid/net/Uri;

    .prologue
    .line 2579
    const-string v0, "SafetyAssuranceService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setFrontPictureUri : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2580
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mFrontPictureUri:Landroid/net/Uri;

    .line 2581
    return-void
.end method

.method public setRearPictureUri(Landroid/net/Uri;)V
    .locals 3
    .param p1, "fileUri"    # Landroid/net/Uri;

    .prologue
    .line 2584
    const-string v0, "SafetyAssuranceService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setRearPictureUri : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2585
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRearPictureUri:Landroid/net/Uri;

    .line 2586
    return-void
.end method

.method public setVocRecUri(Landroid/net/Uri;)V
    .locals 3
    .param p1, "fileUri"    # Landroid/net/Uri;

    .prologue
    .line 2629
    const-string v0, "SafetyAssuranceService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setVocRecUri : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2630
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVoiceRecordingUri:Landroid/net/Uri;

    .line 2631
    return-void
.end method

.method public setWlanPermissionAvailable(I)V
    .locals 2
    .param p1, "setValue"    # I

    .prologue
    .line 2751
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "wlan_permission_available"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 2752
    return-void
.end method
