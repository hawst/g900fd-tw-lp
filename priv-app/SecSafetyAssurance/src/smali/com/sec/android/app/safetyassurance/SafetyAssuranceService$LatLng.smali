.class Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$LatLng;
.super Ljava/lang/Object;
.source "SafetyAssuranceService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LatLng"
.end annotation


# instance fields
.field mLatitude:D

.field mLongitude:D

.field final synthetic this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;DD)V
    .locals 0
    .param p2, "lat"    # D
    .param p4, "lng"    # D

    .prologue
    .line 231
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$LatLng;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 232
    iput-wide p2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$LatLng;->mLatitude:D

    .line 233
    iput-wide p4, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$LatLng;->mLongitude:D

    .line 234
    return-void
.end method
