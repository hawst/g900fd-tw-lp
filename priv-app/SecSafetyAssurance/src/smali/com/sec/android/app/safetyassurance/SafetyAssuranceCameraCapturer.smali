.class public Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;
.super Ljava/lang/Object;
.source "SafetyAssuranceCameraCapturer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$ErrorCallback;,
        Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;
    }
.end annotation


# static fields
.field public static final BEST_PICTURE_SIZE:I = 0x5b7100

.field public static final CAMERASTATE_ERROR_AFTERTAKE:I = 0x2

.field public static final CAMERASTATE_ERROR_BEFORETAKE:I = 0x1

.field public static final CAMERASTATE_INIT:I = 0x0

.field public static final CAMERASTATE_SUCCESS:I = 0x3

.field public static final CAMERA_IMAGE_BUCKET_NAME_PHONE:Ljava/lang/String;

.field public static final DELAY_CAPTURE_TIME:J = 0x1f4L

.field private static final FOCUS_AUTO:Ljava/lang/String; = "auto"

.field private static final FOCUS_FIXED:Ljava/lang/String; = "fixed"

.field private static final FOCUS_INFINITY:Ljava/lang/String; = "infinity"

.field private static final FOCUS_MODE:Ljava/lang/String; = "focus-mode"

.field public static final FRONT_CAMERA:I = 0x1

.field public static final REAR_CAMERA:I = 0x0

.field private static final TAG:Ljava/lang/String; = "SafetyAssuranceCameraCapturer"

.field public static final WAIT_CAPTURE_SECOND:J = 0x8bb2c97000L


# instance fields
.field final CaptureLock:Ljava/util/concurrent/locks/Condition;

.field final lock:Ljava/util/concurrent/locks/Lock;

.field private mActivityContext:Landroid/content/Context;

.field private mCallbackThread:Landroid/os/HandlerThread;

.field private mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

.field private mCameraState:[I

.field private mContentResolver:Landroid/content/ContentResolver;

.field public mFinishCallbackHandler:Landroid/os/Handler;

.field private mFirstPictures:Z

.field protected mFrontUri:Landroid/net/Uri;

.field private mOrientation:Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;

.field private mOrientationOnTake:I

.field private mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

.field protected mRearUri:Landroid/net/Uri;

.field private mSAService:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 39
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/Emergency"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->CAMERA_IMAGE_BUCKET_NAME_PHONE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;)V
    .locals 4
    .param p1, "applicationContext"    # Landroid/content/Context;
    .param p2, "saService"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;
    .param p3, "orientation"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->lock:Ljava/util/concurrent/locks/Lock;

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->CaptureLock:Ljava/util/concurrent/locks/Condition;

    .line 71
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mCameraState:[I

    .line 77
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mOrientationOnTake:I

    .line 81
    const-string v0, "SafetyAssuranceCameraCapturer"

    const-string v1, "SafetyAssuranceCameraCapturer"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mActivityContext:Landroid/content/Context;

    .line 83
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mContentResolver:Landroid/content/ContentResolver;

    .line 84
    iput-object p3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mOrientation:Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;

    .line 85
    iput-object p2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mSAService:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    .line 86
    iput-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mFrontUri:Landroid/net/Uri;

    .line 87
    iput-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mRearUri:Landroid/net/Uri;

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mCameraState:[I

    aput v2, v0, v2

    .line 89
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mCameraState:[I

    const/4 v1, 0x1

    aput v2, v0, v1

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mSAService:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    invoke-virtual {v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->getWorkHanler()Landroid/os/Handler;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mFinishCallbackHandler:Landroid/os/Handler;

    .line 91
    iput-boolean v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mFirstPictures:Z

    .line 92
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->getDateTaken()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;J)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;
    .param p1, "x1"    # J

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->createName(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;)[I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mCameraState:[I

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->getOrientationOnTake()I

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;)Landroid/content/ContentResolver;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mContentResolver:Landroid/content/ContentResolver;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mActivityContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;)Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mSAService:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mFirstPictures:Z

    return v0
.end method

.method private closeCamera()V
    .locals 2

    .prologue
    .line 309
    const-string v0, "SafetyAssuranceCameraCapturer"

    const-string v1, "closeCamera"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 310
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-eqz v0, :cond_0

    .line 311
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    invoke-virtual {v0}, Lcom/sec/android/seccamera/SecCamera;->release()V

    .line 312
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    .line 314
    :cond_0
    return-void
.end method

.method private createName(J)Ljava/lang/String;
    .locals 3
    .param p1, "lTime"    # J

    .prologue
    .line 431
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Emergency_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "yyyyMMdd_kkmmss"

    invoke-static {v1, p1, p2}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getDateTaken()J
    .locals 6

    .prologue
    .line 459
    const-string v3, "SafetyAssuranceCameraCapturer"

    const-string v4, "getDateTaken"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 460
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    .line 461
    .local v0, "calendar":Ljava/util/GregorianCalendar;
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    .line 462
    .local v1, "time":Landroid/text/format/Time;
    iget-object v3, v1, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    invoke-static {v3}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v2

    .line 463
    .local v2, "timezone":Ljava/util/TimeZone;
    invoke-virtual {v0, v2}, Ljava/util/GregorianCalendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 465
    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v4

    return-wide v4
.end method

.method private getOrientationOnTake()I
    .locals 1

    .prologue
    .line 474
    iget v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mOrientationOnTake:I

    return v0
.end method

.method private getPreferFocusMode(Ljava/util/List;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .local p1, "focusMode":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .line 283
    if-nez p1, :cond_0

    .line 284
    const-string v3, "SafetyAssuranceCameraCapturer"

    const-string v4, "focusMode is null!!"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 304
    :goto_0
    return-object v2

    .line 288
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 289
    .local v1, "thisFocus":Ljava/lang/String;
    const-string v3, "SafetyAssuranceCameraCapturer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "supported focus mode ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 292
    .end local v1    # "thisFocus":Ljava/lang/String;
    :cond_1
    const-string v3, "infinity"

    invoke-interface {p1, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 293
    const-string v2, "SafetyAssuranceCameraCapturer"

    const-string v3, "INFINITY MODE"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    const-string v2, "infinity"

    goto :goto_0

    .line 295
    :cond_2
    const-string v3, "fixed"

    invoke-interface {p1, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 296
    const-string v2, "SafetyAssuranceCameraCapturer"

    const-string v3, "FIXED MODE"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    const-string v2, "fixed"

    goto :goto_0

    .line 298
    :cond_3
    const-string v3, "auto"

    invoke-interface {p1, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 299
    const-string v2, "SafetyAssuranceCameraCapturer"

    const-string v3, "AUTO MODE"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 300
    const-string v2, "auto"

    goto :goto_0

    .line 303
    :cond_4
    const-string v3, "SafetyAssuranceCameraCapturer"

    const-string v4, "None of support focus"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private getSizeRatio(II)I
    .locals 6
    .param p1, "nWidth"    # I
    .param p2, "nHeight"    # I

    .prologue
    .line 491
    const/4 v0, 0x0

    .line 492
    .local v0, "nResult":I
    int-to-double v2, p1

    int-to-double v4, p2

    div-double/2addr v2, v4

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v0, v2

    .line 493
    return v0
.end method

.method private openCamera(I)Z
    .locals 14
    .param p1, "CameraNumber"    # I

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 206
    const-string v11, "SafetyAssuranceCameraCapturer"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "openCamera:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    :try_start_0
    iget-object v11, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mCallbackThread:Landroid/os/HandlerThread;

    invoke-virtual {v11}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v11

    invoke-static {p1, v11}, Lcom/sec/android/seccamera/SecCamera;->open(ILandroid/os/Looper;)Lcom/sec/android/seccamera/SecCamera;

    move-result-object v11

    iput-object v11, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 216
    iget-object v11, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-eqz v11, :cond_5

    .line 217
    iget-object v11, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    new-instance v12, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$ErrorCallback;

    invoke-direct {v12, p0, p1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$ErrorCallback;-><init>(Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;I)V

    invoke-virtual {v11, v12}, Lcom/sec/android/seccamera/SecCamera;->setErrorCallback(Lcom/sec/android/seccamera/SecCamera$ErrorCallback;)V

    .line 218
    const-string v11, "SafetyAssuranceCameraCapturer"

    const-string v12, "camera device is opened."

    invoke-static {v11, v12}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    iget-object v11, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    invoke-virtual {v11}, Lcom/sec/android/seccamera/SecCamera;->getParameters()Lcom/sec/android/seccamera/SecCamera$Parameters;

    move-result-object v11

    iput-object v11, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    .line 220
    iget-object v11, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    if-nez v11, :cond_0

    .line 221
    const-string v10, "SafetyAssuranceCameraCapturer"

    const-string v11, "CameraParameters is null"

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    :goto_0
    return v9

    .line 209
    :catch_0
    move-exception v1

    .line 210
    .local v1, "e":Ljava/lang/Exception;
    const-string v10, "SafetyAssuranceCameraCapturer"

    const-string v11, "Can not open camera device"

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 233
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    iget-object v11, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-virtual {v11}, Lcom/sec/android/seccamera/SecCamera$Parameters;->getSupportedPictureSizes()Ljava/util/List;

    move-result-object v4

    .line 234
    .local v4, "listSize":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/seccamera/SecCamera$Size;>;"
    invoke-virtual {p0, v4}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->getBestSize(Ljava/util/List;)Lcom/sec/android/seccamera/SecCamera$Size;

    move-result-object v0

    .line 235
    .local v0, "curSize":Lcom/sec/android/seccamera/SecCamera$Size;
    if-nez v0, :cond_1

    .line 236
    const-string v10, "SafetyAssuranceCameraCapturer"

    const-string v11, "Error!!! getBestSize return null!!!"

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 240
    :cond_1
    iget v11, v0, Lcom/sec/android/seccamera/SecCamera$Size;->width:I

    iget v12, v0, Lcom/sec/android/seccamera/SecCamera$Size;->height:I

    invoke-direct {p0, v11, v12}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->getSizeRatio(II)I

    move-result v6

    .line 241
    .local v6, "ratioPicture":I
    iget-object v11, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    iget v12, v0, Lcom/sec/android/seccamera/SecCamera$Size;->width:I

    iget v13, v0, Lcom/sec/android/seccamera/SecCamera$Size;->height:I

    invoke-virtual {v11, v12, v13}, Lcom/sec/android/seccamera/SecCamera$Parameters;->setPictureSize(II)V

    .line 242
    const-string v11, "SafetyAssuranceCameraCapturer"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "set picture size - width:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget v13, v0, Lcom/sec/android/seccamera/SecCamera$Size;->width:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " height:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget v13, v0, Lcom/sec/android/seccamera/SecCamera$Size;->height:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " ratio:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 245
    iget-object v11, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-virtual {v11}, Lcom/sec/android/seccamera/SecCamera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v4

    .line 246
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/seccamera/SecCamera$Size;

    .line 247
    .local v5, "oneSize":Lcom/sec/android/seccamera/SecCamera$Size;
    iget v11, v5, Lcom/sec/android/seccamera/SecCamera$Size;->width:I

    iget v12, v5, Lcom/sec/android/seccamera/SecCamera$Size;->height:I

    invoke-direct {p0, v11, v12}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->getSizeRatio(II)I

    move-result v7

    .line 248
    .local v7, "ratioPreview":I
    if-ne v6, v7, :cond_2

    .line 249
    iget-object v11, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    iget v12, v5, Lcom/sec/android/seccamera/SecCamera$Size;->width:I

    iget v13, v5, Lcom/sec/android/seccamera/SecCamera$Size;->height:I

    invoke-virtual {v11, v12, v13}, Lcom/sec/android/seccamera/SecCamera$Parameters;->setPreviewSize(II)V

    .line 250
    const-string v11, "SafetyAssuranceCameraCapturer"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "set preview size - width:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget v13, v5, Lcom/sec/android/seccamera/SecCamera$Size;->width:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " height:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget v13, v5, Lcom/sec/android/seccamera/SecCamera$Size;->height:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " ratio:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    .end local v5    # "oneSize":Lcom/sec/android/seccamera/SecCamera$Size;
    .end local v7    # "ratioPreview":I
    :cond_3
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->featureSupportNewUX()Z

    move-result v11

    if-eqz v11, :cond_4

    .line 257
    iget-object v11, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-virtual {v11}, Lcom/sec/android/seccamera/SecCamera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v11

    invoke-direct {p0, v11}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->getPreferFocusMode(Ljava/util/List;)Ljava/lang/String;

    move-result-object v8

    .line 258
    .local v8, "selectedFocusMode":Ljava/lang/String;
    if-eqz v8, :cond_4

    .line 259
    iget-object v11, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    const-string v12, "focus-mode"

    invoke-virtual {v11, v12, v8}, Lcom/sec/android/seccamera/SecCamera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    .end local v8    # "selectedFocusMode":Ljava/lang/String;
    :cond_4
    iget-object v11, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    const-string v12, "no-display-mode"

    invoke-virtual {v11, v12, v10}, Lcom/sec/android/seccamera/SecCamera$Parameters;->set(Ljava/lang/String;I)V

    .line 266
    iget-object v11, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    iget-object v12, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-virtual {v11, v12}, Lcom/sec/android/seccamera/SecCamera;->setParameters(Lcom/sec/android/seccamera/SecCamera$Parameters;)V

    .line 269
    :try_start_1
    iget-object v11, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    invoke-virtual {v11}, Lcom/sec/android/seccamera/SecCamera;->startPreview()V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    .end local v0    # "curSize":Lcom/sec/android/seccamera/SecCamera$Size;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "listSize":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/seccamera/SecCamera$Size;>;"
    .end local v6    # "ratioPicture":I
    :cond_5
    move v9, v10

    .line 278
    goto/16 :goto_0

    .line 270
    .restart local v0    # "curSize":Lcom/sec/android/seccamera/SecCamera$Size;
    .restart local v3    # "i$":Ljava/util/Iterator;
    .restart local v4    # "listSize":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/seccamera/SecCamera$Size;>;"
    .restart local v6    # "ratioPicture":I
    :catch_1
    move-exception v2

    .line 271
    .local v2, "ex":Ljava/lang/RuntimeException;
    const-string v10, "SafetyAssuranceCameraCapturer"

    const-string v11, "mCameraDevice.startPreview() failed!!"

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    invoke-virtual {v2}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto/16 :goto_0
.end method

.method private setOrientationOnTake(I)V
    .locals 0
    .param p1, "orientationOnTake"    # I

    .prologue
    .line 470
    iput p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mOrientationOnTake:I

    .line 471
    return-void
.end method

.method private takePicture(I)Z
    .locals 5
    .param p1, "CameraNumber"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 317
    const-string v2, "SafetyAssuranceCameraCapturer"

    const-string v3, "takePicture"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 318
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-nez v2, :cond_1

    .line 319
    :cond_0
    const-string v2, "SafetyAssuranceCameraCapturer"

    const-string v3, "CameraParameters or CameraDevice is null"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 331
    :goto_0
    return v1

    .line 322
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    const-string v3, "off"

    invoke-virtual {v2, v3}, Lcom/sec/android/seccamera/SecCamera$Parameters;->setFlashMode(Ljava/lang/String;)V

    .line 324
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mOrientation:Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceOrientation;->calculateOrientationForPicture(I)I

    move-result v0

    .line 325
    .local v0, "rotation":I
    invoke-direct {p0, v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->setOrientationOnTake(I)V

    .line 326
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-virtual {v2, v0}, Lcom/sec/android/seccamera/SecCamera$Parameters;->setRotation(I)V

    .line 328
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mParameters:Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-virtual {v2, v3}, Lcom/sec/android/seccamera/SecCamera;->setParameters(Lcom/sec/android/seccamera/SecCamera$Parameters;)V

    .line 329
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    invoke-virtual {v2, v1}, Lcom/sec/android/seccamera/SecCamera;->setShutterSoundEnable(Z)V

    .line 330
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    new-instance v2, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;

    invoke-direct {v2, p0, p1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;-><init>(Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;I)V

    invoke-virtual {v1, v4, v4, v2}, Lcom/sec/android/seccamera/SecCamera;->takePicture(Lcom/sec/android/seccamera/SecCamera$ShutterCallback;Lcom/sec/android/seccamera/SecCamera$PictureCallback;Lcom/sec/android/seccamera/SecCamera$PictureCallback;)V

    .line 331
    const/4 v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public CaptureFrontAndRearImage()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 96
    const-string v2, "SafetyAssuranceCameraCapturer"

    const-string v3, "CaptureFrontAndRearImage"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    new-instance v2, Landroid/os/HandlerThread;

    const-string v3, "Callback Handler"

    invoke-direct {v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mCallbackThread:Landroid/os/HandlerThread;

    .line 98
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mCallbackThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->start()V

    .line 101
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 103
    const/4 v2, 0x1

    :try_start_0
    invoke-direct {p0, v2}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->openCamera(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 105
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mCameraState:[I

    const/4 v3, 0x1

    const/4 v4, 0x1

    aput v4, v2, v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 128
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 132
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 134
    const/4 v2, 0x0

    :try_start_1
    invoke-direct {p0, v2}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->openCamera(I)Z

    move-result v2

    if-nez v2, :cond_4

    .line 136
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mCameraState:[I

    const/4 v3, 0x0

    const/4 v4, 0x1

    aput v4, v2, v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 159
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 164
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mCameraState:[I

    aget v2, v2, v7

    if-ne v2, v6, :cond_0

    iget-boolean v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mFirstPictures:Z

    if-nez v2, :cond_0

    .line 165
    const-string v2, "SafetyAssuranceCameraCapturer"

    const-string v3, "CaptureFrontAndRearImage ERROR_BEFORETAKE : Send msg anyway"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mFinishCallbackHandler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 167
    .local v1, "msg":Landroid/os/Message;
    const/4 v2, 0x2

    iput v2, v1, Landroid/os/Message;->what:I

    .line 168
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mFinishCallbackHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 171
    .end local v1    # "msg":Landroid/os/Message;
    :cond_0
    return-void

    .line 109
    :cond_1
    const-wide/16 v2, 0x1f4

    :try_start_2
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V

    .line 110
    const-string v2, "SafetyAssuranceCameraCapturer"

    const-string v3, "SLEEP 500ms"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->takePicture(I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 112
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->CaptureLock:Ljava/util/concurrent/locks/Condition;

    const-wide v4, 0x8bb2c97000L

    invoke-interface {v2, v4, v5}, Ljava/util/concurrent/locks/Condition;->awaitNanos(J)J

    move-result-wide v2

    cmp-long v2, v2, v8

    if-gtz v2, :cond_2

    .line 113
    const-string v2, "SafetyAssuranceCameraCapturer"

    const-string v3, "Picture is not comming in time."

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 124
    :cond_2
    :goto_2
    :try_start_3
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->closeCamera()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 128
    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v2

    .line 116
    :cond_3
    :try_start_4
    const-string v2, "SafetyAssuranceCameraCapturer"

    const-string v3, "take picture call failed"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mCameraState:[I

    const/4 v3, 0x1

    const/4 v4, 0x1

    aput v4, v2, v3
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_2

    .line 119
    :catch_0
    move-exception v0

    .line 120
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_5
    const-string v2, "SafetyAssuranceCameraCapturer"

    const-string v3, "Exception on takePicture"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 122
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mCameraState:[I

    const/4 v3, 0x1

    const/4 v4, 0x1

    aput v4, v2, v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 124
    :try_start_6
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->closeCamera()V

    goto/16 :goto_0

    .end local v0    # "ex":Ljava/lang/Exception;
    :catchall_1
    move-exception v2

    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->closeCamera()V

    throw v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 140
    :cond_4
    const-wide/16 v2, 0x1f4

    :try_start_7
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V

    .line 141
    const-string v2, "SafetyAssuranceCameraCapturer"

    const-string v3, "SLEEP 500ms"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->takePicture(I)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 143
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->CaptureLock:Ljava/util/concurrent/locks/Condition;

    const-wide v4, 0x8bb2c97000L

    invoke-interface {v2, v4, v5}, Ljava/util/concurrent/locks/Condition;->awaitNanos(J)J

    move-result-wide v2

    cmp-long v2, v2, v8

    if-gtz v2, :cond_5

    .line 144
    const-string v2, "SafetyAssuranceCameraCapturer"

    const-string v3, "Picture is not comming in time."

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 155
    :cond_5
    :goto_3
    :try_start_8
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->closeCamera()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    goto/16 :goto_1

    .line 159
    :catchall_2
    move-exception v2

    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v2

    .line 147
    :cond_6
    :try_start_9
    const-string v2, "SafetyAssuranceCameraCapturer"

    const-string v3, "take picture call failed"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mCameraState:[I

    const/4 v3, 0x0

    const/4 v4, 0x1

    aput v4, v2, v3
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    goto :goto_3

    .line 150
    :catch_1
    move-exception v0

    .line 151
    .restart local v0    # "ex":Ljava/lang/Exception;
    :try_start_a
    const-string v2, "SafetyAssuranceCameraCapturer"

    const-string v3, "Exception on takePicture"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 153
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mCameraState:[I

    const/4 v3, 0x0

    const/4 v4, 0x1

    aput v4, v2, v3
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    .line 155
    :try_start_b
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->closeCamera()V

    goto/16 :goto_1

    .end local v0    # "ex":Ljava/lang/Exception;
    :catchall_3
    move-exception v2

    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->closeCamera()V

    throw v2
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2
.end method

.method getBestSize(Ljava/util/List;)Lcom/sec/android/seccamera/SecCamera$Size;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/seccamera/SecCamera$Size;",
            ">;)",
            "Lcom/sec/android/seccamera/SecCamera$Size;"
        }
    .end annotation

    .prologue
    .local p1, "listSize":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/seccamera/SecCamera$Size;>;"
    const v7, 0x5b7100

    .line 180
    const/4 v1, 0x0

    .line 181
    .local v1, "bestSize":Lcom/sec/android/seccamera/SecCamera$Size;
    const/4 v0, 0x0

    .line 183
    .local v0, "bestPictureSize":I
    if-nez p1, :cond_0

    .line 184
    const-string v4, "SafetyAssuranceCameraCapturer"

    const-string v5, "Error!!! List of SupportedPictureSizes is null!!"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    const/4 v4, 0x0

    .line 202
    :goto_0
    return-object v4

    .line 188
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/seccamera/SecCamera$Size;

    .line 189
    .local v3, "thisSize":Lcom/sec/android/seccamera/SecCamera$Size;
    const-string v4, "SafetyAssuranceCameraCapturer"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Support Picture size ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Lcom/sec/android/seccamera/SecCamera$Size;->width:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Lcom/sec/android/seccamera/SecCamera$Size;->height:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    if-nez v1, :cond_2

    .line 191
    move-object v1, v3

    .line 192
    iget v4, v3, Lcom/sec/android/seccamera/SecCamera$Size;->width:I

    iget v5, v3, Lcom/sec/android/seccamera/SecCamera$Size;->height:I

    mul-int v0, v4, v5

    .line 193
    goto :goto_1

    .line 196
    :cond_2
    sub-int v4, v7, v0

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    iget v5, v3, Lcom/sec/android/seccamera/SecCamera$Size;->width:I

    iget v6, v3, Lcom/sec/android/seccamera/SecCamera$Size;->height:I

    mul-int/2addr v5, v6

    sub-int v5, v7, v5

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v5

    if-le v4, v5, :cond_1

    .line 197
    move-object v1, v3

    .line 198
    iget v4, v3, Lcom/sec/android/seccamera/SecCamera$Size;->width:I

    iget v5, v3, Lcom/sec/android/seccamera/SecCamera$Size;->height:I

    mul-int v0, v4, v5

    goto :goto_1

    .end local v3    # "thisSize":Lcom/sec/android/seccamera/SecCamera$Size;
    :cond_3
    move-object v4, v1

    .line 202
    goto :goto_0
.end method

.method public isFirstPicures(Z)V
    .locals 3
    .param p1, "bFirstPictures"    # Z

    .prologue
    .line 174
    const-string v0, "SafetyAssuranceCameraCapturer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isFirstPicures:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    iput-boolean p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mFirstPictures:Z

    .line 176
    return-void
.end method

.method public isTakingPicturesOver()Z
    .locals 4

    .prologue
    .line 479
    const/4 v0, 0x0

    .line 480
    .local v0, "bResult":Z
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mCameraState:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mCameraState:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    if-eqz v1, :cond_0

    .line 481
    const/4 v0, 0x1

    .line 485
    :goto_0
    const-string v1, "SafetyAssuranceCameraCapturer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isTakingPicturesOver : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 486
    return v0

    .line 483
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
