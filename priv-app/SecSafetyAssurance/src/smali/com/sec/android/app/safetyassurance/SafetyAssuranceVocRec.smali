.class public Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;
.super Ljava/lang/Object;
.source "SafetyAssuranceVocRec.java"

# interfaces
.implements Lcom/sec/android/secmediarecorder/SecMediaRecorder$OnErrorListener;
.implements Lcom/sec/android/secmediarecorder/SecMediaRecorder$OnInfoListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec$SAMediaRecorderState;,
        Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec$SAVRFormat;
    }
.end annotation


# static fields
.field private static final MAX_WAIT_COUNT:I = 0xa

.field public static final SAVR_ACTION_ERROR:I = 0x270f

.field public static final SAVR_ACTION_SAVED:I = 0x4

.field private static final SLEEP_TIME:I = 0x1f4

.field private static final TAG:Ljava/lang/String; = "SafetyAssuranceVocRec"


# instance fields
.field private mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private mAudioFormat:Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec$SAVRFormat;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mBroadcastReceiverSDCard:Landroid/content/BroadcastReceiver;

.field private mContext:Landroid/content/Context;

.field private mFileName:Ljava/lang/String;

.field private mFileSaveHiddenPath:Ljava/lang/String;

.field private mFileSavePath:Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field private mLastSavedFileUri:Landroid/net/Uri;

.field private mMediaRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

.field private mMediaRecorderState:I

.field private mSAService:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mAudioManager:Landroid/media/AudioManager;

    .line 70
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mMediaRecorderState:I

    .line 466
    new-instance v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec$1;-><init>(Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;)V

    iput-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 78
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mContext:Landroid/content/Context;

    .line 79
    return-void
.end method

.method private createNewFileName()Ljava/lang/String;
    .locals 7

    .prologue
    .line 511
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    .line 512
    .local v0, "calendar":Ljava/util/GregorianCalendar;
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    .line 513
    .local v1, "time":Landroid/text/format/Time;
    iget-object v5, v1, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    invoke-static {v5}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v4

    .line 514
    .local v4, "timezone":Ljava/util/TimeZone;
    invoke-virtual {v0, v4}, Ljava/util/GregorianCalendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 515
    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v2

    .line 516
    .local v2, "lCurrentTime":J
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Emergency_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "yyyyMMdd_kkmmss"

    invoke-static {v6, v2, v3}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method

.method public static getSaveDirPathCreated()Ljava/lang/String;
    .locals 4

    .prologue
    .line 520
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/Emergency"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 522
    .local v1, "dirpath":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 523
    .local v0, "dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-nez v2, :cond_0

    .line 524
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v2

    if-nez v2, :cond_0

    .line 525
    const-string v2, "SafetyAssuranceVocRec"

    const-string v3, " getSaveDirPathCreated : mkdir() failure or if the directory already existed"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 528
    :cond_0
    return-object v1
.end method

.method public static getSaveHiddenDirPathCreated()Ljava/lang/String;
    .locals 4

    .prologue
    .line 532
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 534
    .local v1, "dirpath":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 535
    .local v0, "dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-nez v2, :cond_0

    .line 536
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v2

    if-nez v2, :cond_0

    .line 537
    const-string v2, "SafetyAssuranceVocRec"

    const-string v3, "getSaveHiddenDirPathCreated : mkdir() failure or if the directory already existed"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 540
    :cond_0
    return-object v1
.end method

.method private registerBroadcastReceiverSDCard(Z)V
    .locals 4
    .param p1, "register"    # Z

    .prologue
    .line 579
    const-string v1, "SafetyAssuranceVocRec"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "registerBroadcastReceiverSDCard: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 580
    if-eqz p1, :cond_1

    .line 581
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 582
    .local v0, "iFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 583
    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 584
    const-string v1, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 585
    const-string v1, "android.intent.action.MEDIA_BAD_REMOVAL"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 586
    const-string v1, "android.intent.action.MEDIA_SHARED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 587
    const-string v1, "android.intent.action.MEDIA_SCANNER_STARTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 588
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 590
    new-instance v1, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec$2;-><init>(Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;)V

    iput-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mBroadcastReceiverSDCard:Landroid/content/BroadcastReceiver;

    .line 602
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mBroadcastReceiverSDCard:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 609
    .end local v0    # "iFilter":Landroid/content/IntentFilter;
    :cond_0
    :goto_0
    return-void

    .line 604
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mBroadcastReceiverSDCard:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    .line 605
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mBroadcastReceiverSDCard:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 606
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mBroadcastReceiverSDCard:Landroid/content/BroadcastReceiver;

    goto :goto_0
.end method

.method private requestAudioFocus()Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 496
    const/4 v0, 0x0

    .line 497
    .local v0, "focusResult":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v3, 0x5

    if-ge v1, v3, :cond_0

    .line 498
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mAudioManager:Landroid/media/AudioManager;

    iget-object v4, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v5, 0x3

    invoke-virtual {v3, v4, v5, v2}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v0

    .line 500
    if-ne v0, v2, :cond_2

    .line 503
    :cond_0
    if-nez v0, :cond_1

    .line 504
    const/4 v2, 0x0

    .line 507
    :cond_1
    return v2

    .line 497
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private setState(I)V
    .locals 3
    .param p1, "recorderStateOrCommand"    # I

    .prologue
    .line 427
    const-string v0, "SafetyAssuranceVocRec"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setState: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 429
    packed-switch p1, :pswitch_data_0

    .line 439
    :goto_0
    :pswitch_0
    return-void

    .line 434
    :pswitch_1
    iput p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mMediaRecorderState:I

    goto :goto_0

    .line 429
    nop

    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public declared-synchronized cancelRecording()Z
    .locals 4

    .prologue
    .line 387
    monitor-enter p0

    :try_start_0
    const-string v2, "SafetyAssuranceVocRec"

    const-string v3, "cancelRecording: "

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 390
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mAudioManager:Landroid/media/AudioManager;

    const-string v3, "samsungrecord_ns=off"

    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 395
    :goto_0
    :try_start_2
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->releaseMediaRecorder()Z

    .line 397
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mFileName:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 398
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mFileName:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 399
    .local v1, "f":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v2

    if-nez v2, :cond_0

    .line 400
    const-string v2, "SafetyAssuranceVocRec"

    const-string v3, "cancelRecording: Failed to delete file"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 404
    .end local v1    # "f":Ljava/io/File;
    :cond_0
    const/16 v2, 0x3e8

    invoke-direct {p0, v2}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->setState(I)V

    .line 406
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->registerBroadcastReceiverSDCard(Z)V

    .line 408
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mAudioManager:Landroid/media/AudioManager;

    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 410
    const/4 v2, 0x1

    monitor-exit p0

    return v2

    .line 391
    :catch_0
    move-exception v0

    .line 392
    .local v0, "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 387
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public getMediaRecorderState()I
    .locals 3

    .prologue
    .line 82
    const-string v0, "SafetyAssuranceVocRec"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getMediaRecorderState: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mMediaRecorderState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    iget v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mMediaRecorderState:I

    return v0
.end method

.method public declared-synchronized initRecording(ILcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Z
    .locals 8
    .param p1, "maxRecordTime"    # I
    .param p2, "saService"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 104
    monitor-enter p0

    :try_start_0
    const-string v3, "SafetyAssuranceVocRec"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "initRecording:  maxRecordTime="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 105
    const/16 v3, 0x3e80

    if-le p1, v3, :cond_0

    move v3, v4

    .line 205
    :goto_0
    monitor-exit p0

    return v3

    .line 108
    :cond_0
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mAudioManager:Landroid/media/AudioManager;

    if-nez v3, :cond_1

    .line 109
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mContext:Landroid/content/Context;

    const-string v6, "audio"

    invoke-virtual {v3, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/media/AudioManager;

    iput-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mAudioManager:Landroid/media/AudioManager;

    .line 113
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v3}, Landroid/media/AudioManager;->isRecordActive()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 114
    const-string v3, "SafetyAssuranceVocRec"

    const-string v6, "initRecording : isRecordActive() is true"

    invoke-static {v3, v6}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mContext:Landroid/content/Context;

    new-instance v6, Landroid/content/Intent;

    const-string v7, "com.sec.android.app.voicenote.rec_save"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 118
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mContext:Landroid/content/Context;

    new-instance v6, Landroid/content/Intent;

    const-string v7, "com.sec.android.app.voicerecorder.rec_save"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 119
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mContext:Landroid/content/Context;

    new-instance v6, Landroid/content/Intent;

    const-string v7, "com.samsung.media.fmradio.rec_save"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 121
    const/16 v2, 0xa

    .line 124
    .local v2, "waitCount":I
    :cond_2
    const-wide/16 v6, 0x1f4

    :try_start_2
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 128
    :goto_1
    :try_start_3
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v3}, Landroid/media/AudioManager;->isRecordActive()Z

    move-result v3

    if-nez v3, :cond_4

    .line 129
    const-string v3, "SafetyAssuranceVocRec"

    const-string v6, "initRecording : isRecordActive() is false now. Go ahead."

    invoke-static {v3, v6}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    .end local v2    # "waitCount":I
    :cond_3
    :goto_2
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mLastSavedFileUri:Landroid/net/Uri;

    .line 146
    const/4 v3, 0x1

    invoke-direct {p0, v3}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->registerBroadcastReceiverSDCard(Z)V

    .line 148
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mAudioManager:Landroid/media/AudioManager;

    iget-object v6, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v3, v6}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 151
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->requestAudioFocus()Z

    move-result v3

    if-nez v3, :cond_6

    .line 152
    const-string v3, "SafetyAssuranceVocRec"

    const-string v5, "initRecording: requestAudioFocus() is failed"

    invoke-static {v3, v5}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v4

    .line 153
    goto :goto_0

    .line 125
    .restart local v2    # "waitCount":I
    :catch_0
    move-exception v1

    .line 126
    .local v1, "ex":Ljava/lang/InterruptedException;
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 104
    .end local v1    # "ex":Ljava/lang/InterruptedException;
    .end local v2    # "waitCount":I
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 132
    .restart local v2    # "waitCount":I
    :cond_4
    add-int/lit8 v2, v2, -0x1

    .line 133
    if-gtz v2, :cond_5

    .line 134
    :try_start_4
    const-string v3, "SafetyAssuranceVocRec"

    const-string v5, "initRecording : isRecordActive() is still true. Recording is skipped"

    invoke-static {v3, v5}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v4

    .line 135
    goto/16 :goto_0

    .line 137
    :cond_5
    const-string v3, "SafetyAssuranceVocRec"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "initRecording : isRecordActive() is still true. ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    if-gtz v2, :cond_2

    goto :goto_2

    .line 157
    .end local v2    # "waitCount":I
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->releaseMediaRecorder()Z

    .line 159
    new-instance v3, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec$SAVRFormat;

    invoke-direct {v3}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec$SAVRFormat;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mAudioFormat:Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec$SAVRFormat;

    .line 161
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->createNewFileName()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mFileName:Ljava/lang/String;

    .line 162
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->getSaveHiddenDirPathCreated()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "/"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ".voice"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mFileSaveHiddenPath:Ljava/lang/String;

    .line 163
    const-string v3, "SafetyAssuranceVocRec"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "initRecording: mFileName is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mFileName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " mFileSaveHiddenPath is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mFileSaveHiddenPath:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    new-instance v3, Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    invoke-direct {v3}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mMediaRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 169
    :try_start_5
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mAudioManager:Landroid/media/AudioManager;

    const-string v6, "samsungrecord_ns=off"

    invoke-virtual {v3, v6}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 170
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mMediaRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    const/4 v6, 0x1

    invoke-virtual {v3, v6}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setAudioSource(I)V

    .line 173
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mMediaRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    iget-object v6, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mAudioFormat:Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec$SAVRFormat;

    invoke-virtual {v6}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec$SAVRFormat;->getOutputFormat()I

    move-result v6

    invoke-virtual {v3, v6}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setOutputFormat(I)V

    .line 174
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mMediaRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    iget-object v6, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mAudioFormat:Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec$SAVRFormat;

    invoke-virtual {v6}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec$SAVRFormat;->getWarningSize()I

    move-result v6

    mul-int/lit16 v6, v6, 0x3e8

    int-to-long v6, v6

    invoke-virtual {v3, v6, v7}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setMaxFileSize(J)V

    .line 175
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mMediaRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    invoke-virtual {v3, p1}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setMaxDuration(I)V

    .line 177
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mMediaRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    iget-object v6, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mFileSaveHiddenPath:Ljava/lang/String;

    invoke-virtual {v3, v6}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setOutputFile(Ljava/lang/String;)V

    .line 178
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mMediaRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    iget-object v6, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mAudioFormat:Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec$SAVRFormat;

    invoke-virtual {v6}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec$SAVRFormat;->getAudioEncodingBitrate()I

    move-result v6

    invoke-virtual {v3, v6}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setAudioEncodingBitRate(I)V

    .line 179
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mMediaRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    const/4 v6, 0x1

    invoke-virtual {v3, v6}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setAudioChannels(I)V

    .line 180
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mMediaRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    iget-object v6, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mAudioFormat:Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec$SAVRFormat;

    invoke-virtual {v6}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec$SAVRFormat;->getAudioSamplingRate()I

    move-result v6

    invoke-virtual {v3, v6}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setAudioSamplingRate(I)V

    .line 181
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mMediaRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    iget-object v6, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mAudioFormat:Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec$SAVRFormat;

    invoke-virtual {v6}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec$SAVRFormat;->getAudioEncoder()I

    move-result v6

    invoke-virtual {v3, v6}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setAudioEncoder(I)V

    .line 182
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mMediaRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    const/16 v6, 0x3e8

    invoke-virtual {v3, v6}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setDurationInterval(I)V

    .line 183
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mMediaRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    const-wide/16 v6, 0x300

    invoke-virtual {v3, v6, v7}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setFileSizeInterval(J)V

    .line 184
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mMediaRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    const/4 v6, 0x1

    invoke-virtual {v3, v6}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setAuthor(I)V

    .line 186
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mMediaRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    invoke-virtual {v3}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->prepare()V

    .line 188
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mMediaRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    invoke-virtual {v3, p0}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setOnErrorListener(Lcom/sec/android/secmediarecorder/SecMediaRecorder$OnErrorListener;)V

    .line 189
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mMediaRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    invoke-virtual {v3, p0}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setOnInfoListener(Lcom/sec/android/secmediarecorder/SecMediaRecorder$OnInfoListener;)V
    :try_end_5
    .catch Ljava/lang/IllegalStateException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 200
    :try_start_6
    iput-object p2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mSAService:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    .line 201
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mSAService:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    invoke-virtual {v3}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->getWorkHanler()Landroid/os/Handler;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mHandler:Landroid/os/Handler;

    .line 203
    const/16 v3, 0x3e9

    invoke-direct {p0, v3}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->setState(I)V

    move v3, v5

    .line 205
    goto/16 :goto_0

    .line 190
    :catch_1
    move-exception v0

    .line 191
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    .line 192
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->cancelRecording()Z

    move v3, v4

    .line 193
    goto/16 :goto_0

    .line 194
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v0

    .line 195
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 196
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->cancelRecording()Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move v3, v4

    .line 197
    goto/16 :goto_0
.end method

.method public isRecording()Z
    .locals 3

    .prologue
    .line 87
    const-string v0, "SafetyAssuranceVocRec"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isRecording: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v2}, Landroid/media/AudioManager;->isRecordActive()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isRecordActive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    const/4 v0, 0x1

    .line 91
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onError(Lcom/sec/android/secmediarecorder/SecMediaRecorder;II)V
    .locals 4
    .param p1, "arg0"    # Lcom/sec/android/secmediarecorder/SecMediaRecorder;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I

    .prologue
    .line 443
    const-string v1, "SafetyAssuranceVocRec"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onError:  arg1 is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " arg2 is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 444
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 445
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 446
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0x270f

    iput v1, v0, Landroid/os/Message;->what:I

    .line 447
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 449
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->cancelRecording()Z

    .line 450
    return-void
.end method

.method public onInfo(Lcom/sec/android/secmediarecorder/SecMediaRecorder;II)V
    .locals 3
    .param p1, "mr"    # Lcom/sec/android/secmediarecorder/SecMediaRecorder;
    .param p2, "what"    # I
    .param p3, "extra"    # I

    .prologue
    .line 454
    packed-switch p2, :pswitch_data_0

    .line 465
    :goto_0
    return-void

    .line 456
    :pswitch_0
    const-string v0, "SafetyAssuranceVocRec"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onInfo:  what is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " extra is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 457
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->saveRecording()Z

    goto :goto_0

    .line 460
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->saveRecording()Z

    goto :goto_0

    .line 454
    :pswitch_data_0
    .packed-switch 0x320
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public releaseMediaRecorder()Z
    .locals 2

    .prologue
    .line 414
    const-string v0, "SafetyAssuranceVocRec"

    const-string v1, "releaseMediaRecorder: "

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 415
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mMediaRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    if-eqz v0, :cond_1

    .line 416
    iget v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mMediaRecorderState:I

    const/16 v1, 0x3e8

    if-eq v0, v1, :cond_0

    .line 417
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mMediaRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    invoke-virtual {v0}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->reset()V

    .line 419
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mMediaRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    invoke-virtual {v0}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->release()V

    .line 420
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mMediaRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    .line 423
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public declared-synchronized saveRecording()Z
    .locals 24

    .prologue
    .line 246
    monitor-enter p0

    :try_start_0
    const-string v2, "SafetyAssuranceVocRec"

    const-string v3, "saveRecording: "

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mMediaRecorderState:I

    const/16 v3, 0x3ea

    if-eq v2, v3, :cond_0

    .line 250
    const-string v2, "SafetyAssuranceVocRec"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "saveRecording:  mMediaRecorderState is "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mMediaRecorderState:I

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 251
    const/4 v2, 0x0

    .line 383
    :goto_0
    monitor-exit p0

    return v2

    .line 255
    :cond_0
    const/16 v2, 0x3e8

    :try_start_1
    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mMediaRecorderState:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 259
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mMediaRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    invoke-virtual {v2}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->stop()V

    .line 260
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->releaseMediaRecorder()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 267
    :try_start_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->getSaveDirPathCreated()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mFileName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mAudioFormat:Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec$SAVRFormat;

    invoke-virtual {v3}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec$SAVRFormat;->getExtension()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mFileSavePath:Ljava/lang/String;

    .line 268
    const-string v2, "SafetyAssuranceVocRec"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "saveRecording:  mFileSavePath is "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mFileSavePath:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 276
    :try_start_4
    new-instance v17, Landroid/media/MediaMetadataRetriever;

    invoke-direct/range {v17 .. v17}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 277
    .local v17, "retriever":Landroid/media/MediaMetadataRetriever;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mFileSaveHiddenPath:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 279
    const/16 v20, 0x0

    .line 280
    .local v20, "value":Ljava/lang/String;
    const/16 v2, 0x9

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v20

    .line 281
    invoke-virtual/range {v17 .. v17}, Landroid/media/MediaMetadataRetriever;->release()V

    .line 282
    const/16 v17, 0x0

    .line 283
    invoke-static/range {v20 .. v20}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v12

    .line 285
    .local v12, "fileDuration":J
    const-wide/16 v2, 0x0

    cmp-long v2, v12, v2

    if-ltz v2, :cond_1

    const-wide/16 v2, 0x3e8

    cmp-long v2, v12, v2

    if-gez v2, :cond_1

    .line 286
    const-string v2, "SafetyAssuranceVocRec"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "saveRecording:  fileDuration is "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->cancelRecording()Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 288
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 261
    .end local v12    # "fileDuration":J
    .end local v17    # "retriever":Landroid/media/MediaMetadataRetriever;
    .end local v20    # "value":Ljava/lang/String;
    :catch_0
    move-exception v10

    .line 262
    .local v10, "e":Ljava/lang/Exception;
    :try_start_5
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V

    .line 263
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->cancelRecording()Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 264
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 291
    .end local v10    # "e":Ljava/lang/Exception;
    .restart local v12    # "fileDuration":J
    .restart local v17    # "retriever":Landroid/media/MediaMetadataRetriever;
    .restart local v20    # "value":Ljava/lang/String;
    :cond_1
    :try_start_6
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->getSaveDirPathCreated()Ljava/lang/String;

    .line 292
    new-instance v14, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mFileSaveHiddenPath:Ljava/lang/String;

    invoke-direct {v14, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 293
    .local v14, "hiddenFile":Ljava/io/File;
    new-instance v18, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mFileSavePath:Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 294
    .local v18, "saveFile":Ljava/io/File;
    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v16

    .line 295
    .local v16, "resMove":Z
    if-nez v16, :cond_2

    .line 296
    const-string v2, "SafetyAssuranceVocRec"

    const-string v3, "saveRecording:  rename is failed"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->cancelRecording()Z
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 298
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 301
    .end local v12    # "fileDuration":J
    .end local v14    # "hiddenFile":Ljava/io/File;
    .end local v16    # "resMove":Z
    .end local v17    # "retriever":Landroid/media/MediaMetadataRetriever;
    .end local v18    # "saveFile":Ljava/io/File;
    .end local v20    # "value":Ljava/lang/String;
    :catch_1
    move-exception v10

    .line 302
    .restart local v10    # "e":Ljava/lang/Exception;
    :try_start_7
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V

    .line 303
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->cancelRecording()Z
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 304
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 309
    .end local v10    # "e":Ljava/lang/Exception;
    .restart local v12    # "fileDuration":J
    .restart local v14    # "hiddenFile":Ljava/io/File;
    .restart local v16    # "resMove":Z
    .restart local v17    # "retriever":Landroid/media/MediaMetadataRetriever;
    .restart local v18    # "saveFile":Ljava/io/File;
    .restart local v20    # "value":Ljava/lang/String;
    :cond_2
    :try_start_8
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 310
    .local v9, "contentValues":Landroid/content/ContentValues;
    const-string v2, "title"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mFileName:Ljava/lang/String;

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 311
    const-string v2, "mime_type"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mAudioFormat:Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec$SAVRFormat;

    invoke-virtual {v3}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec$SAVRFormat;->getMimeType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    const-string v2, "_data"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mFileSavePath:Ljava/lang/String;

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    const-string v2, "duration"

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 314
    const-string v2, "_size"

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->length()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 315
    const-string v2, "date_modified"

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->lastModified()J

    move-result-wide v6

    const-wide/16 v22, 0x3e8

    div-long v6, v6, v22

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 316
    const-string v2, "recordingtype"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 317
    const-string v2, "is_music"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 319
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    .line 320
    .local v19, "sb":Ljava/lang/StringBuilder;
    const-string v2, "_data"

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mFileSavePath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 321
    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v4, v2
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 325
    .local v4, "selection":[Ljava/lang/String;
    const/4 v8, 0x0

    .line 327
    .local v8, "c":Landroid/database/Cursor;
    :try_start_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 328
    if-eqz v8, :cond_3

    .line 329
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-nez v2, :cond_6

    .line 330
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3, v9}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mLastSavedFileUri:Landroid/net/Uri;
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 341
    :cond_3
    :goto_1
    if-eqz v8, :cond_4

    .line 342
    :try_start_a
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 343
    const/4 v8, 0x0

    .line 347
    :cond_4
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mLastSavedFileUri:Landroid/net/Uri;

    if-nez v2, :cond_9

    .line 348
    const-string v2, "SafetyAssuranceVocRec"

    const-string v3, "saveRecording:  mLastSavedFileUri is null "

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->cancelRecording()Z

    .line 350
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mFileSavePath:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 351
    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->delete()Z

    move-result v2

    if-nez v2, :cond_5

    .line 352
    const-string v2, "SafetyAssuranceVocRec"

    const-string v3, "saveRecording: Failed to delete savefile"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 355
    :cond_5
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 332
    :cond_6
    :try_start_b
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 333
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "content://media"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mLastSavedFileUri:Landroid/net/Uri;
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_2
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto :goto_1

    .line 338
    :catch_2
    move-exception v11

    .line 339
    .local v11, "ex":Ljava/lang/Exception;
    :try_start_c
    invoke-virtual {v11}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 341
    if-eqz v8, :cond_4

    .line 342
    :try_start_d
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 343
    const/4 v8, 0x0

    goto :goto_2

    .line 341
    .end local v11    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    if-eqz v8, :cond_7

    .line 342
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 343
    const/4 v8, 0x0

    :cond_7
    throw v2
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_3
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 358
    .end local v4    # "selection":[Ljava/lang/String;
    .end local v8    # "c":Landroid/database/Cursor;
    .end local v9    # "contentValues":Landroid/content/ContentValues;
    .end local v19    # "sb":Ljava/lang/StringBuilder;
    :catch_3
    move-exception v10

    .line 359
    .restart local v10    # "e":Ljava/lang/Exception;
    :try_start_e
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V

    .line 360
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->cancelRecording()Z

    .line 361
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mFileSavePath:Ljava/lang/String;

    if-eqz v2, :cond_8

    .line 362
    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->delete()Z

    .line 364
    :cond_8
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 367
    .end local v10    # "e":Ljava/lang/Exception;
    .restart local v4    # "selection":[Ljava/lang/String;
    .restart local v8    # "c":Landroid/database/Cursor;
    .restart local v9    # "contentValues":Landroid/content/ContentValues;
    .restart local v19    # "sb":Ljava/lang/StringBuilder;
    :cond_9
    const/16 v2, 0x3ec

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->setState(I)V

    .line 368
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->cancelRecording()Z
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    .line 371
    :try_start_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mAudioManager:Landroid/media/AudioManager;

    const-string v3, "samsungrecord_ns=off"

    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_4
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    .line 376
    :goto_3
    :try_start_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mHandler:Landroid/os/Handler;

    if-eqz v2, :cond_a

    .line 377
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mSAService:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mLastSavedFileUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->setVocRecUri(Landroid/net/Uri;)V

    .line 378
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v15

    .line 379
    .local v15, "msg":Landroid/os/Message;
    const/4 v2, 0x4

    iput v2, v15, Landroid/os/Message;->what:I

    .line 380
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v15}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 383
    .end local v15    # "msg":Landroid/os/Message;
    :cond_a
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 372
    :catch_4
    move-exception v10

    .line 373
    .restart local v10    # "e":Ljava/lang/Exception;
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    goto :goto_3

    .line 246
    .end local v4    # "selection":[Ljava/lang/String;
    .end local v8    # "c":Landroid/database/Cursor;
    .end local v9    # "contentValues":Landroid/content/ContentValues;
    .end local v10    # "e":Ljava/lang/Exception;
    .end local v12    # "fileDuration":J
    .end local v14    # "hiddenFile":Ljava/io/File;
    .end local v16    # "resMove":Z
    .end local v17    # "retriever":Landroid/media/MediaMetadataRetriever;
    .end local v18    # "saveFile":Ljava/io/File;
    .end local v19    # "sb":Ljava/lang/StringBuilder;
    .end local v20    # "value":Ljava/lang/String;
    :catchall_1
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized startRecording()Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 215
    monitor-enter p0

    :try_start_0
    const-string v3, "SafetyAssuranceVocRec"

    const-string v4, "startRecording: "

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    iget v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mMediaRecorderState:I

    const/16 v4, 0x3e9

    if-eq v3, v4, :cond_0

    .line 218
    const-string v3, "SafetyAssuranceVocRec"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "startRecording: mMediaRecorderState is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mMediaRecorderState:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 237
    :goto_0
    monitor-exit p0

    return v2

    .line 224
    :cond_0
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->mMediaRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    invoke-virtual {v3}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->start()V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 235
    const/16 v2, 0x3ea

    :try_start_2
    invoke-direct {p0, v2}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->setState(I)V

    .line 237
    const/4 v2, 0x1

    goto :goto_0

    .line 225
    :catch_0
    move-exception v0

    .line 226
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    .line 227
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->cancelRecording()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 215
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 229
    :catch_1
    move-exception v1

    .line 230
    .local v1, "re":Ljava/lang/RuntimeException;
    :try_start_3
    invoke-virtual {v1}, Ljava/lang/RuntimeException;->printStackTrace()V

    .line 231
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->cancelRecording()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method
