.class Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$7;
.super Ljava/lang/Object;
.source "SafetyAssuranceService.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)V
    .locals 0

    .prologue
    .line 982
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$7;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 5
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 985
    const-string v0, "SafetyAssuranceService"

    const-string v3, "MSG service onServiceConnected Called !!!! ==="

    invoke-static {v0, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 988
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$7;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRemoveCameraOption:Z
    invoke-static {v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$600(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 989
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$7;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # setter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSosMode:I
    invoke-static {v0, v4}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$702(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;I)I

    .line 993
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$7;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIsSosMode:Z
    invoke-static {v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$800(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 994
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$7;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    new-instance v4, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;

    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$7;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRemoveCameraOption:Z
    invoke-static {v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$600(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$7;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSendPictures:I
    invoke-static {v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$1000(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)I

    move-result v0

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_0
    invoke-direct {v4, v1, v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;-><init>(ZZ)V

    # setter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSendingListMsg:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;
    invoke-static {v3, v4}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$902(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;)Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;

    .line 1007
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$7;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIsSosMode:Z
    invoke-static {v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$800(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1008
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$7;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mFrontPictureUri:Landroid/net/Uri;
    invoke-static {v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$1100(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1009
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$7;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSendingListMsg:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;
    invoke-static {v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$900(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;->setPicFront(Z)V

    .line 1011
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$7;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mRearPictureUri:Landroid/net/Uri;
    invoke-static {v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$1200(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_2

    .line 1012
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$7;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSendingListMsg:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;
    invoke-static {v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$900(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;->setPicRear(Z)V

    .line 1016
    :cond_2
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$7$1;

    invoke-direct {v1, p0, p2}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$7$1;-><init>(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$7;Landroid/os/IBinder;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 1037
    const-string v0, "SafetyAssuranceService"

    const-string v1, "MSG service onServiceConnected Called END ==="

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 1038
    return-void

    :cond_3
    move v0, v2

    .line 994
    goto :goto_0

    .line 996
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$7;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSosMode:I
    invoke-static {v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$700(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)I

    move-result v0

    if-ne v1, v0, :cond_5

    .line 997
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$7;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    new-instance v3, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;

    invoke-direct {v3, v1, v1, v2}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;-><init>(ZZZ)V

    # setter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSendingListMsg:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;
    invoke-static {v0, v3}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$902(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;)Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;

    goto :goto_1

    .line 998
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$7;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSosMode:I
    invoke-static {v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$700(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)I

    move-result v0

    if-ne v4, v0, :cond_6

    .line 999
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$7;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    new-instance v3, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;

    invoke-direct {v3, v1, v2, v2}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;-><init>(ZZZ)V

    # setter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSendingListMsg:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;
    invoke-static {v0, v3}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$902(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;)Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;

    goto :goto_1

    .line 1002
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$7;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    new-instance v3, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;

    invoke-direct {v3, v1, v2, v2}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;-><init>(ZZZ)V

    # setter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSendingListMsg:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;
    invoke-static {v0, v3}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$902(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;)Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListMsg;

    goto :goto_1
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 1042
    const-string v1, "SafetyAssuranceService"

    const-string v2, "MSG service onServiceDisconnected Called !!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1045
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$7;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mServiceBinder:Lcom/android/mms/transaction/IMessageBackgroundSender;
    invoke-static {v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$1300(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Lcom/android/mms/transaction/IMessageBackgroundSender;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/mms/transaction/IMessageBackgroundSender;->stopListening()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1050
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$7;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mServiceBinder:Lcom/android/mms/transaction/IMessageBackgroundSender;
    invoke-static {v1, v2}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$1302(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Lcom/android/mms/transaction/IMessageBackgroundSender;)Lcom/android/mms/transaction/IMessageBackgroundSender;

    .line 1051
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$7;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSvcConnected:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$1402(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Z)Z

    .line 1052
    return-void

    .line 1046
    :catch_0
    move-exception v0

    .line 1047
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
