.class Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$3;
.super Ljava/lang/Object;
.source "SafetyAssuranceFragment.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->initSAActionBarSwitch()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)V
    .locals 0

    .prologue
    .line 627
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$3;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 8
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 630
    const/4 v2, 0x0

    .line 631
    .local v2, "state":I
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$3;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    # getter for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mStartFrom:I
    invoke-static {v3}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->access$800(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)I

    move-result v3

    if-nez v3, :cond_2

    .line 632
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$3;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "send_emergency_message"

    invoke-static {v3, v4, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    .line 637
    :cond_0
    :goto_0
    if-eqz p2, :cond_4

    if-nez v2, :cond_4

    .line 638
    const-string v3, "SafetyAssuranceFragment"

    const-string v4, "The SA switch is on"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 639
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->featureSupportNewUX()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 640
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$3;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    # getter for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->access$900(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->getFinishDisclaimer(Landroid/content/Context;)Z

    move-result v3

    if-ne v3, v7, :cond_3

    .line 642
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$3;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    # invokes: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->prepareActivate()V
    invoke-static {v3}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->access$1000(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)V

    .line 687
    :cond_1
    :goto_1
    return-void

    .line 633
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$3;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    # getter for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mStartFrom:I
    invoke-static {v3}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->access$800(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)I

    move-result v3

    if-ne v3, v7, :cond_0

    .line 634
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$3;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "send_b_emergency_message"

    invoke-static {v3, v4, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    goto :goto_0

    .line 647
    :cond_3
    const/4 v0, 0x0

    .line 648
    .local v0, "sendingIntervalTime":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$3;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    # getter for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mSendingTimerList:Landroid/preference/ListPreference;
    invoke-static {v3}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->access$1100(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)Landroid/preference/ListPreference;

    move-result-object v3

    invoke-virtual {v3}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 650
    const-string v3, "SafetyAssuranceFragment"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SAssurance - sendingIntervalTime="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 652
    new-instance v1, Landroid/content/Intent;

    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$3;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-class v4, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 653
    .local v1, "startEMActivityIntent":Landroid/content/Intent;
    const-string v3, "sendingIntervalTime"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 654
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$3;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    invoke-virtual {v3, v1, v6}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_1

    .line 655
    .end local v0    # "sendingIntervalTime":Ljava/lang/String;
    .end local v1    # "startEMActivityIntent":Landroid/content/Intent;
    :cond_4
    if-nez p2, :cond_1

    if-ne v2, v7, :cond_1

    .line 656
    const-string v3, "SafetyAssuranceFragment"

    const-string v4, "The SA switch is off"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 657
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$3;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    # getter for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mRemoveCameraOption:Z
    invoke-static {v3}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->access$1200(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 658
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$3;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    # getter for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mDualCapturedCheckBox:Landroid/preference/CheckBoxPreference;
    invoke-static {v3}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->access$1300(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)Landroid/preference/CheckBoxPreference;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 660
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$3;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    # getter for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mRemoveVoiceRecordingOption:Z
    invoke-static {v3}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->access$1400(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 661
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$3;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    # getter for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mSoundRecordingPreference:Landroid/preference/CheckBoxPreference;
    invoke-static {v3}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->access$1500(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)Landroid/preference/CheckBoxPreference;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 663
    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$3;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    # getter for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mSendingTimerList:Landroid/preference/ListPreference;
    invoke-static {v3}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->access$1100(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)Landroid/preference/ListPreference;

    move-result-object v3

    if-eqz v3, :cond_7

    .line 664
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$3;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    # getter for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mSendingTimerList:Landroid/preference/ListPreference;
    invoke-static {v3}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->access$1100(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)Landroid/preference/ListPreference;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/preference/ListPreference;->setEnabled(Z)V

    .line 667
    :cond_7
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->featureSupportNewUX()Z

    move-result v3

    if-nez v3, :cond_9

    .line 668
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$3;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    iget-object v4, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$3;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    const v5, 0x7f090001

    invoke-virtual {v4, v5}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    # invokes: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->displayToast(Ljava/lang/String;)V
    invoke-static {v3, v4}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->access$1600(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;Ljava/lang/String;)V

    .line 674
    :goto_2
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$3;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "user_agree_to_use_location_service"

    invoke-static {v3, v4, v6}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 675
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$3;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    # getter for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mStartFrom:I
    invoke-static {v3}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->access$800(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)I

    move-result v3

    if-nez v3, :cond_a

    .line 676
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$3;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "send_emergency_message"

    invoke-static {v3, v4, v6}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 682
    :cond_8
    :goto_3
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$3;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    # getter for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mStartFrom:I
    invoke-static {v3}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->access$800(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)I

    move-result v3

    if-nez v3, :cond_1

    .line 684
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$3;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    # getter for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->access$900(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v6}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->setWakeupKey(Landroid/content/Context;Z)V

    goto/16 :goto_1

    .line 671
    :cond_9
    const-string v3, "SafetyAssuranceFragment"

    const-string v4, "onCheckedChanged : NEWUX"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 677
    :cond_a
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$3;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    # getter for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mStartFrom:I
    invoke-static {v3}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->access$800(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)I

    move-result v3

    if-ne v3, v7, :cond_8

    .line 678
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$3;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "send_b_emergency_message"

    invoke-static {v3, v4, v6}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 679
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$3;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    # invokes: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->sendBroadcastToWatch(Z)V
    invoke-static {v3, v6}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->access$1700(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;Z)V

    goto :goto_3
.end method
