.class public Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;
.super Landroid/app/Activity;
.source "SafetyAssuranceSetEmergencyMessageActivity.java"


# static fields
.field private static final LANGUAGE_KOR:Ljava/lang/String; = "ko"

.field static final TAG:Ljava/lang/String; = "SafetyAssuranceSetEmergencyMessageActivity"


# instance fields
.field agreeTermsAndConditionsCheckBox:Landroid/widget/CheckBox;

.field agreeToShareLocationCheckBox:Landroid/widget/CheckBox;

.field agreeToTurnOnMobileDataCheckBox:Landroid/widget/CheckBox;

.field private newConcept:Z

.field private newConceptForPhone:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 36
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 39
    iput-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->agreeToShareLocationCheckBox:Landroid/widget/CheckBox;

    .line 40
    iput-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->agreeTermsAndConditionsCheckBox:Landroid/widget/CheckBox;

    .line 41
    iput-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->agreeToTurnOnMobileDataCheckBox:Landroid/widget/CheckBox;

    .line 43
    iput-boolean v1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->newConcept:Z

    .line 44
    iput-boolean v1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->newConceptForPhone:Z

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->prepareFinishActivity()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->newConceptForPhone:Z

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->newConcept:Z

    return v0
.end method

.method private prepareFinishActivity()V
    .locals 0

    .prologue
    .line 319
    return-void
.end method


# virtual methods
.method public finish()V
    .locals 0

    .prologue
    .line 349
    invoke-super {p0}, Landroid/app/Activity;->finish()V

    .line 350
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 17
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    .line 48
    invoke-super/range {p0 .. p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 49
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->featureSupportNewUX()Z

    move-result v14

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->newConcept:Z

    .line 50
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->newConcept:Z

    if-eqz v14, :cond_0

    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->isTabletDevice()Z

    move-result v14

    if-nez v14, :cond_0

    .line 51
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->newConceptForPhone:Z

    .line 54
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->newConcept:Z

    if-eqz v14, :cond_1

    .line 55
    const v14, 0x7f09004e

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->setTitle(I)V

    .line 56
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v14

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 60
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->newConcept:Z

    if-nez v14, :cond_a

    .line 61
    const v14, 0x7f030004

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->setContentView(I)V

    .line 71
    :goto_0
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->newConcept:Z

    if-eqz v14, :cond_2

    .line 78
    const v14, 0x7f0b000e

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 80
    .local v5, "disclaimer_01":Landroid/widget/TextView;
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->isKoreaDevice()Z

    move-result v14

    if-eqz v14, :cond_c

    .line 82
    const v14, 0x7f090045

    invoke-virtual {v5, v14}, Landroid/widget/TextView;->setText(I)V

    .line 83
    const v14, 0x7f0b000f

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 84
    .local v6, "disclaimer_02":Landroid/widget/TextView;
    const/4 v14, 0x0

    const/4 v15, 0x1

    invoke-virtual {v6, v14, v15}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 85
    const v14, 0x7f090046

    invoke-virtual {v6, v14}, Landroid/widget/TextView;->setText(I)V

    .line 86
    const/4 v14, 0x0

    invoke-virtual {v6, v14}, Landroid/widget/TextView;->setVisibility(I)V

    .line 87
    const v14, 0x7f0b0010

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 88
    .local v7, "disclaimer_03":Landroid/widget/TextView;
    const v14, 0x7f090047

    invoke-virtual {v7, v14}, Landroid/widget/TextView;->setText(I)V

    .line 89
    const/4 v14, 0x0

    invoke-virtual {v7, v14}, Landroid/widget/TextView;->setVisibility(I)V

    .line 111
    .end local v5    # "disclaimer_01":Landroid/widget/TextView;
    .end local v6    # "disclaimer_02":Landroid/widget/TextView;
    .end local v7    # "disclaimer_03":Landroid/widget/TextView;
    :cond_2
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v14

    const/4 v15, 0x1

    invoke-virtual {v14, v15}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 112
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v14

    const/4 v15, 0x1

    invoke-virtual {v14, v15}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 116
    const/4 v9, 0x0

    .line 117
    .local v9, "intervalTimeStr":Ljava/lang/String;
    const/4 v8, 0x0

    .line 118
    .local v8, "intent":Landroid/content/Intent;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    .line 119
    const-string v14, "sendingIntervalTime"

    invoke-virtual {v8, v14}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 120
    if-eqz v9, :cond_11

    .line 134
    const v14, 0x7f0b000c

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 135
    .local v2, "cancelButton":Landroid/widget/Button;
    new-instance v14, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$1;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$1;-><init>(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;)V

    invoke-virtual {v2, v14}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 144
    const v14, 0x7f0b000d

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/Button;

    .line 145
    .local v10, "okButton":Landroid/widget/Button;
    new-instance v14, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$2;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$2;-><init>(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;)V

    invoke-virtual {v10, v14}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 154
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->newConcept:Z

    if-nez v14, :cond_3

    .line 155
    const v14, 0x7f0b0009

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/CheckBox;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->agreeToShareLocationCheckBox:Landroid/widget/CheckBox;

    .line 156
    const v14, 0x7f0b000a

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/CheckBox;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->agreeTermsAndConditionsCheckBox:Landroid/widget/CheckBox;

    .line 158
    :cond_3
    const v14, 0x7f0b000b

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/CheckBox;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->agreeToTurnOnMobileDataCheckBox:Landroid/widget/CheckBox;

    .line 160
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->isChinaDevice()Z

    move-result v14

    if-eqz v14, :cond_4

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->newConcept:Z

    if-eqz v14, :cond_4

    .line 161
    const v14, 0x7f0b0011

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/CheckBox;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->agreeTermsAndConditionsCheckBox:Landroid/widget/CheckBox;

    .line 162
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->agreeTermsAndConditionsCheckBox:Landroid/widget/CheckBox;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 163
    const v14, 0x7f0b0012

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/CheckBox;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->agreeToShareLocationCheckBox:Landroid/widget/CheckBox;

    .line 164
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->agreeToShareLocationCheckBox:Landroid/widget/CheckBox;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 167
    :cond_4
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->newConcept:Z

    if-nez v14, :cond_5

    .line 168
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->agreeToShareLocationCheckBox:Landroid/widget/CheckBox;

    new-instance v15, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$3;

    move-object/from16 v0, p0

    invoke-direct {v15, v0, v10}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$3;-><init>(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;Landroid/widget/Button;)V

    invoke-virtual {v14, v15}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 180
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->agreeTermsAndConditionsCheckBox:Landroid/widget/CheckBox;

    new-instance v15, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$4;

    move-object/from16 v0, p0

    invoke-direct {v15, v0, v10}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$4;-><init>(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;Landroid/widget/Button;)V

    invoke-virtual {v14, v15}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 193
    :cond_5
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->isChinaDevice()Z

    move-result v14

    if-eqz v14, :cond_6

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->newConcept:Z

    if-eqz v14, :cond_6

    .line 194
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->agreeTermsAndConditionsCheckBox:Landroid/widget/CheckBox;

    new-instance v15, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$5;

    move-object/from16 v0, p0

    invoke-direct {v15, v0, v10}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$5;-><init>(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;Landroid/widget/Button;)V

    invoke-virtual {v14, v15}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 210
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->agreeToShareLocationCheckBox:Landroid/widget/CheckBox;

    new-instance v15, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$6;

    move-object/from16 v0, p0

    invoke-direct {v15, v0, v10}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$6;-><init>(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;Landroid/widget/Button;)V

    invoke-virtual {v14, v15}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 227
    :cond_6
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->agreeToTurnOnMobileDataCheckBox:Landroid/widget/CheckBox;

    new-instance v15, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$7;

    move-object/from16 v0, p0

    invoke-direct {v15, v0, v10}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$7;-><init>(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;Landroid/widget/Button;)V

    invoke-virtual {v14, v15}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 276
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->newConcept:Z

    if-eqz v14, :cond_7

    .line 277
    const v14, 0x7f0b0005

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v14

    const v15, 0x7f020020

    invoke-virtual {v14, v15}, Landroid/view/View;->setBackgroundResource(I)V

    .line 281
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    const-string v15, "user_agree_to_use_location_service"

    const/16 v16, 0x0

    invoke-static/range {v14 .. v16}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    .line 282
    .local v4, "didUserAgree":I
    const/4 v14, 0x1

    if-ne v4, v14, :cond_f

    .line 284
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->newConceptForPhone:Z

    if-eqz v14, :cond_8

    .line 285
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f070005

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getColor(I)I

    move-result v14

    invoke-virtual {v10, v14}, Landroid/widget/Button;->setTextColor(I)V

    .line 286
    :cond_8
    const/4 v14, 0x1

    invoke-virtual {v10, v14}, Landroid/widget/Button;->setEnabled(Z)V

    .line 301
    .end local v2    # "cancelButton":Landroid/widget/Button;
    .end local v4    # "didUserAgree":I
    .end local v10    # "okButton":Landroid/widget/Button;
    :goto_2
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->isChinaDevice()Z

    move-result v14

    if-eqz v14, :cond_9

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->newConcept:Z

    if-nez v14, :cond_9

    .line 302
    const v14, 0x7f0b0008

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    .line 303
    .local v13, "tvDescription3":Landroid/widget/TextView;
    invoke-virtual {v13}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v14

    invoke-interface {v14}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v11

    .line 304
    .local v11, "stringDescription3":Ljava/lang/String;
    const-string v14, "Wi-Fi"

    const-string v15, "WLAN"

    invoke-virtual {v11, v14, v15}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 305
    .local v12, "stringDescription3Wlan":Ljava/lang/String;
    invoke-virtual {v13, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 306
    const-string v14, "SafetyAssuranceSetEmergencyMessageActivity"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "replace description : Wi-Fi -> WLAN, stringDescription3="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 310
    .end local v11    # "stringDescription3":Ljava/lang/String;
    .end local v12    # "stringDescription3Wlan":Ljava/lang/String;
    .end local v13    # "tvDescription3":Landroid/widget/TextView;
    :cond_9
    return-void

    .line 64
    .end local v8    # "intent":Landroid/content/Intent;
    .end local v9    # "intervalTimeStr":Ljava/lang/String;
    :cond_a
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->isTabletDevice()Z

    move-result v14

    if-eqz v14, :cond_b

    .line 65
    const v14, 0x7f030006

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->setContentView(I)V

    goto/16 :goto_0

    .line 67
    :cond_b
    const v14, 0x7f030005

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->setContentView(I)V

    goto/16 :goto_0

    .line 90
    .restart local v5    # "disclaimer_01":Landroid/widget/TextView;
    :cond_c
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->isUSADevice()Z

    move-result v14

    if-eqz v14, :cond_d

    .line 92
    const v14, 0x7f090048

    invoke-virtual {v5, v14}, Landroid/widget/TextView;->setText(I)V

    .line 93
    const v14, 0x7f0b000b

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    .line 94
    .local v3, "confirmCheckBox":Landroid/widget/CheckBox;
    if-eqz v3, :cond_2

    .line 95
    const v14, 0x7f090009

    invoke-virtual {v3, v14}, Landroid/widget/CheckBox;->setText(I)V

    goto/16 :goto_1

    .line 99
    .end local v3    # "confirmCheckBox":Landroid/widget/CheckBox;
    :cond_d
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 100
    .local v1, "builder_01":Ljava/lang/StringBuilder;
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->isChinaDevice()Z

    move-result v14

    if-nez v14, :cond_e

    .line 101
    const v14, 0x7f090043

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    :goto_3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v5, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 103
    :cond_e
    const v14, 0x7f090044

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104
    const-string v14, "SafetyAssuranceSetEmergencyMessageActivity"

    const-string v15, "replace description for CHN"

    invoke-static {v14, v15}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 289
    .end local v1    # "builder_01":Ljava/lang/StringBuilder;
    .end local v5    # "disclaimer_01":Landroid/widget/TextView;
    .restart local v2    # "cancelButton":Landroid/widget/Button;
    .restart local v4    # "didUserAgree":I
    .restart local v8    # "intent":Landroid/content/Intent;
    .restart local v9    # "intervalTimeStr":Ljava/lang/String;
    .restart local v10    # "okButton":Landroid/widget/Button;
    :cond_f
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->newConceptForPhone:Z

    if-eqz v14, :cond_10

    .line 290
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f070004

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getColor(I)I

    move-result v14

    invoke-virtual {v10, v14}, Landroid/widget/Button;->setTextColor(I)V

    .line 291
    :cond_10
    const/4 v14, 0x0

    invoke-virtual {v10, v14}, Landroid/widget/Button;->setEnabled(Z)V

    goto/16 :goto_2

    .line 296
    .end local v2    # "cancelButton":Landroid/widget/Button;
    .end local v4    # "didUserAgree":I
    .end local v10    # "okButton":Landroid/widget/Button;
    :cond_11
    const-string v14, "SafetyAssuranceSetEmergencyMessageActivity"

    const-string v15, "SAssurance - There is no value of sendingIntervalTime"

    invoke-static {v14, v15}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->finish()V

    goto/16 :goto_2
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 357
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 358
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 323
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 324
    const-string v0, "SafetyAssuranceSetEmergencyMessageActivity"

    const-string v1, "SAssurance - The Back key is pressed."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->prepareFinishActivity()V

    .line 326
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->setResult(I)V

    .line 327
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->finish()V

    .line 330
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 363
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 367
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 365
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->finish()V

    goto :goto_0

    .line 363
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 336
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 342
    return-void
.end method
