.class public Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetSoundRecordingTime;
.super Landroid/app/Activity;
.source "SafetyAssuranceSetSoundRecordingTime.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SafetyAssistanceSetSoundRecordingTime"


# instance fields
.field private intervalTimeArr:[I

.field private listView:Landroid/widget/ListView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 21
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetSoundRecordingTime;->intervalTimeArr:[I

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetSoundRecordingTime;->listView:Landroid/widget/ListView;

    return-void

    .line 21
    nop

    :array_0
    .array-data 4
        0x5
        0xa
        0xf
    .end array-data
.end method

.method static synthetic access$000(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetSoundRecordingTime;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetSoundRecordingTime;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetSoundRecordingTime;->listView:Landroid/widget/ListView;

    return-object v0
.end method


# virtual methods
.method checkItemOfTheValueThatWasSetInSettings()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 100
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetSoundRecordingTime;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "voc_recording_time"

    const/4 v3, 0x5

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 101
    .local v0, "recordingSeconds":I
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetSoundRecordingTime;->intervalTimeArr:[I

    aget v1, v1, v5

    if-ne v0, v1, :cond_0

    .line 102
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetSoundRecordingTime;->listView:Landroid/widget/ListView;

    invoke-virtual {v1, v5, v4}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 109
    :goto_0
    return-void

    .line 103
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetSoundRecordingTime;->intervalTimeArr:[I

    aget v1, v1, v4

    if-ne v0, v1, :cond_1

    .line 104
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetSoundRecordingTime;->listView:Landroid/widget/ListView;

    invoke-virtual {v1, v4, v4}, Landroid/widget/ListView;->setItemChecked(IZ)V

    goto :goto_0

    .line 106
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetSoundRecordingTime;->listView:Landroid/widget/ListView;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, v4}, Landroid/widget/ListView;->setItemChecked(IZ)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v7, 0x7f090037

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 26
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 27
    const v2, 0x7f030003

    invoke-virtual {p0, v2}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetSoundRecordingTime;->setContentView(I)V

    .line 29
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetSoundRecordingTime;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 30
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetSoundRecordingTime;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 32
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 33
    .local v1, "arGeneral":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetSoundRecordingTime;->intervalTimeArr:[I

    aget v3, v3, v6

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {p0, v7, v2}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetSoundRecordingTime;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 34
    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetSoundRecordingTime;->intervalTimeArr:[I

    aget v3, v3, v5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {p0, v7, v2}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetSoundRecordingTime;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 35
    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetSoundRecordingTime;->intervalTimeArr:[I

    const/4 v4, 0x2

    aget v3, v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {p0, v7, v2}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetSoundRecordingTime;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 38
    new-instance v0, Landroid/widget/ArrayAdapter;

    const v2, 0x109000f

    invoke-direct {v0, p0, v2, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 40
    .local v0, "Adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    const v2, 0x7f0b0004

    invoke-virtual {p0, v2}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetSoundRecordingTime;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    iput-object v2, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetSoundRecordingTime;->listView:Landroid/widget/ListView;

    .line 41
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetSoundRecordingTime;->listView:Landroid/widget/ListView;

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 42
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetSoundRecordingTime;->listView:Landroid/widget/ListView;

    invoke-virtual {v2, v5}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 43
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetSoundRecordingTime;->listView:Landroid/widget/ListView;

    new-instance v3, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetSoundRecordingTime$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetSoundRecordingTime$1;-><init>(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetSoundRecordingTime;)V

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 53
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 57
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 61
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 59
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetSoundRecordingTime;->finish()V

    goto :goto_0

    .line 57
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 68
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 69
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetSoundRecordingTime;->storeTimeValueInSettings()V

    .line 70
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 75
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 77
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetSoundRecordingTime;->checkItemOfTheValueThatWasSetInSettings()V

    .line 78
    return-void
.end method

.method storeTimeValueInSettings()V
    .locals 4

    .prologue
    .line 81
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetSoundRecordingTime;->listView:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getCheckedItemPosition()I

    move-result v0

    .line 82
    .local v0, "checkedItemIdx":I
    const-string v1, "SafetyAssistanceSetSoundRecordingTime"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "selectedItemIdx="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetSoundRecordingTime;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "voc_recording_time"

    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetSoundRecordingTime;->intervalTimeArr:[I

    aget v3, v3, v0

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 84
    return-void
.end method
