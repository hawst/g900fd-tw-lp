.class Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver$1;
.super Ljava/lang/Object;
.source "SafetyAssuranceReceiver.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;


# direct methods
.method constructor <init>(Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;)V
    .locals 0

    .prologue
    .line 133
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver$1;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 136
    monitor-enter p0

    .line 137
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver$1;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->access$000(Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;)Landroid/content/Context;

    move-result-object v2

    const-string v3, "vibrator"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Vibrator;

    .line 138
    .local v1, "vib":Landroid/os/Vibrator;
    invoke-virtual {v1}, Landroid/os/Vibrator;->hasVibrator()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 139
    const-string v2, "SafetyAssuranceReceiver"

    const-string v3, "Run vibrator"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    const-wide/16 v2, 0x64

    invoke-virtual {v1, v2, v3}, Landroid/os/Vibrator;->vibrate(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 143
    const-wide/16 v2, 0x82

    :try_start_1
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 148
    :goto_0
    const-wide/16 v2, 0x64

    :try_start_2
    invoke-virtual {v1, v2, v3}, Landroid/os/Vibrator;->vibrate(J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 150
    const-wide/16 v2, 0x82

    :try_start_3
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 155
    :goto_1
    const-wide/16 v2, 0x64

    :try_start_4
    invoke-virtual {v1, v2, v3}, Landroid/os/Vibrator;->vibrate(J)V

    .line 157
    :cond_0
    monitor-exit p0

    .line 158
    return-void

    .line 144
    :catch_0
    move-exception v0

    .line 145
    .local v0, "ex":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 157
    .end local v0    # "ex":Ljava/lang/InterruptedException;
    .end local v1    # "vib":Landroid/os/Vibrator;
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v2

    .line 151
    .restart local v1    # "vib":Landroid/os/Vibrator;
    :catch_1
    move-exception v0

    .line 152
    .restart local v0    # "ex":Ljava/lang/InterruptedException;
    :try_start_5
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1
.end method
