.class Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$11;
.super Ljava/lang/Object;
.source "SafetyAssuranceService.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)V
    .locals 0

    .prologue
    .line 1213
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$11;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 1216
    const-string v1, "SafetyAssuranceService"

    const-string v2, "Voc callback onServiceConnected Called !!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1217
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$11;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    invoke-static {p2}, Lcom/android/mms/transaction/ISnsRemoteService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/mms/transaction/ISnsRemoteService;

    move-result-object v2

    # setter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVocCallbackBinder:Lcom/android/mms/transaction/ISnsRemoteService;
    invoke-static {v1, v2}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$2802(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Lcom/android/mms/transaction/ISnsRemoteService;)Lcom/android/mms/transaction/ISnsRemoteService;

    .line 1218
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$11;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVocCallbackConnected:Z

    .line 1220
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$11;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVocCallbackBinder:Lcom/android/mms/transaction/ISnsRemoteService;
    invoke-static {v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$2800(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Lcom/android/mms/transaction/ISnsRemoteService;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$11;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVocCallback:Lcom/android/mms/transaction/ISnsRemoteServiceCallback;
    invoke-static {v2}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$2900(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Lcom/android/mms/transaction/ISnsRemoteServiceCallback;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/android/mms/transaction/ISnsRemoteService;->registerCallback(Lcom/android/mms/transaction/ISnsRemoteServiceCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1224
    :goto_0
    return-void

    .line 1221
    :catch_0
    move-exception v0

    .line 1222
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 1228
    const-string v1, "SafetyAssuranceService"

    const-string v2, "Voc callback onServiceDisconnected Called !!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1230
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$11;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVocCallbackBinder:Lcom/android/mms/transaction/ISnsRemoteService;
    invoke-static {v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$2800(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Lcom/android/mms/transaction/ISnsRemoteService;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$11;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVocCallback:Lcom/android/mms/transaction/ISnsRemoteServiceCallback;
    invoke-static {v2}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$2900(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Lcom/android/mms/transaction/ISnsRemoteServiceCallback;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/android/mms/transaction/ISnsRemoteService;->unregisterCallback(Lcom/android/mms/transaction/ISnsRemoteServiceCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1234
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$11;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVocCallbackBinder:Lcom/android/mms/transaction/ISnsRemoteService;
    invoke-static {v1, v2}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$2802(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Lcom/android/mms/transaction/ISnsRemoteService;)Lcom/android/mms/transaction/ISnsRemoteService;

    .line 1235
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$11;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVocCallbackConnected:Z

    .line 1236
    return-void

    .line 1231
    :catch_0
    move-exception v0

    .line 1232
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
