.class Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$13;
.super Ljava/lang/Object;
.source "SafetyAssuranceService.java"

# interfaces
.implements Landroid/location/LocationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)V
    .locals 0

    .prologue
    .line 1909
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$13;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLocationChanged(Landroid/location/Location;)V
    .locals 2
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 1913
    if-eqz p1, :cond_0

    .line 1915
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$13;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIsActiveGPSProvider:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$3002(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Z)Z

    .line 1920
    :goto_0
    return-void

    .line 1917
    :cond_0
    const-string v0, "SafetyAssuranceService"

    const-string v1, "onLocationChanged GPS [Location is null]"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1918
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$13;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIsActiveGPSProvider:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$3002(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Z)Z

    goto :goto_0
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 2
    .param p1, "arg0"    # Ljava/lang/String;

    .prologue
    .line 1924
    const-string v0, "SafetyAssuranceService"

    const-string v1, "onProviderDisabled GPS"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1925
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$13;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mIsActiveGPSProvider:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$3002(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Z)Z

    .line 1926
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/String;

    .prologue
    .line 1931
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/String;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # Landroid/os/Bundle;

    .prologue
    .line 1936
    return-void
.end method
