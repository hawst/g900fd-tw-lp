.class public Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec$SAVRFormat;
.super Ljava/lang/Object;
.source "SafetyAssuranceVocRec.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SAVRFormat"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 543
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAudioEncoder()I
    .locals 1

    .prologue
    .line 558
    const/4 v0, 0x1

    return v0
.end method

.method public getAudioEncodingBitrate()I
    .locals 1

    .prologue
    .line 554
    const/16 v0, 0x2fa8

    return v0
.end method

.method public getAudioSamplingRate()I
    .locals 1

    .prologue
    .line 562
    const/16 v0, 0x1f40

    return v0
.end method

.method public getExtension()Ljava/lang/String;
    .locals 1

    .prologue
    .line 566
    const-string v0, ".amr"

    return-object v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 570
    const-string v0, "audio/amr"

    return-object v0
.end method

.method public getOutputFormat()I
    .locals 1

    .prologue
    .line 550
    const/4 v0, 0x3

    return v0
.end method

.method public getWarningSize()I
    .locals 1

    .prologue
    .line 574
    const/16 v0, 0x3e80

    return v0
.end method
