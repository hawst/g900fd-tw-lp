.class public Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;
.super Ljava/lang/Object;
.source "SafetyAssuranceAppFeatures.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SafetyAssuranceAppFeatures"

.field private static mATTDevice:Z

.field public static mChinaDevice:Z

.field private static mFeatureDisableCameraOption:Z

.field private static mFeatureEnableDsdaOneChip:Z

.field private static mFeatureEnableDualMode:Z

.field private static mFeatureEnableDualModeGsmGsm:Z

.field private static mFeatureEnableMultiSim:Z

.field private static mFeatureFindoSearchSettingAppEnable:Z

.field private static mFeatureRightTriggerKey:Z

.field private static mFeatureSupportNewUX:Z

.field private static mFeatureUpTriggerKey:Z

.field private static mJPNDevice:Z

.field private static mJPNSecMessageDisable:Z

.field private static mKoreaDevice:Z

.field private static mTabletDevice:Z

.field private static mUSADevice:Z

.field private static mVZWDevice:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 14
    sput-boolean v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mChinaDevice:Z

    .line 16
    sput-boolean v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mKoreaDevice:Z

    .line 17
    sput-boolean v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mUSADevice:Z

    .line 18
    sput-boolean v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mVZWDevice:Z

    .line 19
    sput-boolean v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mATTDevice:Z

    .line 20
    sput-boolean v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mJPNDevice:Z

    .line 21
    sput-boolean v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mJPNSecMessageDisable:Z

    .line 22
    sput-boolean v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mTabletDevice:Z

    .line 26
    sput-boolean v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mFeatureSupportNewUX:Z

    .line 27
    sput-boolean v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mFeatureRightTriggerKey:Z

    .line 28
    sput-boolean v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mFeatureUpTriggerKey:Z

    .line 29
    sput-boolean v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mFeatureDisableCameraOption:Z

    .line 30
    sput-boolean v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mFeatureFindoSearchSettingAppEnable:Z

    .line 31
    sput-boolean v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mFeatureEnableMultiSim:Z

    .line 32
    sput-boolean v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mFeatureEnableDualMode:Z

    .line 33
    sput-boolean v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mFeatureEnableDsdaOneChip:Z

    .line 34
    sput-boolean v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mFeatureEnableDualModeGsmGsm:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static featureDisableCameraOption()Z
    .locals 1

    .prologue
    .line 248
    sget-boolean v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mFeatureDisableCameraOption:Z

    return v0
.end method

.method public static featureEnableDsdaOnechip()Z
    .locals 1

    .prologue
    .line 261
    sget-boolean v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mFeatureEnableDsdaOneChip:Z

    return v0
.end method

.method public static featureEnableDualMode()Z
    .locals 1

    .prologue
    .line 258
    sget-boolean v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mFeatureEnableDualMode:Z

    return v0
.end method

.method public static featureEnableDualModeGsmGsm()Z
    .locals 1

    .prologue
    .line 264
    sget-boolean v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mFeatureEnableDualModeGsmGsm:Z

    return v0
.end method

.method public static featureEnableMultiSim()Z
    .locals 1

    .prologue
    .line 255
    sget-boolean v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mFeatureEnableMultiSim:Z

    return v0
.end method

.method public static featureFindoSearchSettingAppEnable()Z
    .locals 1

    .prologue
    .line 252
    sget-boolean v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mFeatureFindoSearchSettingAppEnable:Z

    return v0
.end method

.method public static featureRightTriggerKey()Z
    .locals 1

    .prologue
    .line 240
    sget-boolean v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mFeatureRightTriggerKey:Z

    return v0
.end method

.method public static featureSupportNewUX()Z
    .locals 1

    .prologue
    .line 236
    sget-boolean v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mFeatureSupportNewUX:Z

    return v0
.end method

.method public static featureUpTriggerKey()Z
    .locals 1

    .prologue
    .line 244
    sget-boolean v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mFeatureUpTriggerKey:Z

    return v0
.end method

.method public static init(Landroid/content/Context;)V
    .locals 14
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v13, 0x0

    const/4 v12, 0x1

    .line 40
    const-string v9, "SafetyAssuranceAppFeatures"

    const-string v10, "init start"

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 42
    const-string v9, "ro.csc.sales_code"

    invoke-static {v9}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 43
    .local v7, "salesCode":Ljava/lang/String;
    const-string v9, "CHN"

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    const-string v9, "CHM"

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    const-string v9, "CHU"

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    const-string v9, "CHC"

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    const-string v9, "LHS"

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    const-string v9, "CTC"

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_c

    .line 56
    :cond_0
    sput-boolean v12, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mChinaDevice:Z

    .line 71
    :goto_0
    const-string v9, "VZW"

    invoke-virtual {v9, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 73
    sput-boolean v12, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mVZWDevice:Z

    .line 78
    :cond_1
    const-string v9, "ATT"

    invoke-virtual {v9, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 80
    sput-boolean v12, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mATTDevice:Z

    .line 84
    :cond_2
    const-string v9, "SafetyAssuranceAppFeatures"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "mLoadAMap : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget-boolean v11, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mChinaDevice:Z

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    const-string v9, "ro.csc.country_code"

    const-string v10, "unknown"

    invoke-static {v9, v10}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 91
    .local v1, "country":Ljava/lang/String;
    const-string v9, "SafetyAssuranceAppFeatures"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "ro.csc.country_code ["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "]"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    const-string v9, "korea"

    invoke-virtual {v9, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_d

    .line 95
    sput-boolean v12, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mKoreaDevice:Z

    .line 124
    :cond_3
    :goto_1
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v4

    .line 126
    .local v4, "floatingFeature":Lcom/samsung/android/feature/FloatingFeature;
    const-string v9, "SEC_FLOATING_FEATURE_SAFETYASSISTANCE_CONFIG_KEY_POS_FOR_SENDHELPMESSAGE"

    const-string v10, "left"

    invoke-virtual {v4, v9, v10}, Lcom/samsung/android/feature/FloatingFeature;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 128
    .local v6, "posVolKey":Ljava/lang/String;
    const-string v9, "right"

    invoke-virtual {v9, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_f

    .line 129
    const-string v9, "SafetyAssuranceAppFeatures"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "VolumeKey pos ["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "]"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    sput-boolean v12, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mFeatureRightTriggerKey:Z

    .line 138
    :cond_4
    :goto_2
    const-string v9, "SEC_FLOATING_FEATURE_SAFETYASSISTANCE_CONFIG_FUNCTION_FOR_SENDHELPMESSAGE"

    const-string v10, "unknown"

    invoke-virtual {v4, v9, v10}, Lcom/samsung/android/feature/FloatingFeature;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 139
    .local v0, "configFeature":Ljava/lang/String;
    const-string v9, "-camera"

    invoke-virtual {v0, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 141
    const-string v9, "SafetyAssuranceAppFeatures"

    const-string v10, "Disable camera option by feature"

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    sput-boolean v12, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mFeatureDisableCameraOption:Z

    .line 144
    :cond_5
    const-string v9, "+oldUX"

    invoke-virtual {v0, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_10

    .line 146
    const-string v9, "SafetyAssuranceAppFeatures"

    const-string v10, "This model supports old UX"

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    sput-boolean v13, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mFeatureSupportNewUX:Z

    .line 155
    :goto_3
    sput-boolean v12, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mFeatureFindoSearchSettingAppEnable:Z

    .line 156
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 158
    .local v5, "pm":Landroid/content/pm/PackageManager;
    :try_start_0
    const-string v9, "com.samsung.android.app.galaxyfinder"

    const/4 v10, 0x0

    invoke-virtual {v5, v9, v10}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    .line 163
    :goto_4
    const-string v9, "ro.multisim.simslotcount"

    invoke-static {v9, v12}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v8

    .line 164
    .local v8, "simSlotCount":I
    if-le v8, v12, :cond_11

    .line 165
    const-string v9, "SafetyAssuranceAppFeatures"

    const-string v10, "mFeatureEnableMultiSim true"

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    sput-boolean v12, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mFeatureEnableMultiSim:Z

    .line 170
    :goto_5
    const-string v9, "SEC_FLOATING_FEATURE_RIL_MULTISIM_DUOS_CDMAGSM"

    invoke-virtual {v4, v9, v13}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;Z)Z

    move-result v9

    if-nez v9, :cond_6

    const-string v9, "SEC_FLOATING_FEATURE_RIL_MULTISIM_DUOS_GSMGSM"

    invoke-virtual {v4, v9, v13}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;Z)Z

    move-result v9

    if-nez v9, :cond_6

    const-string v9, "SEC_FLOATING_FEATURE_RIL_MULTISIM_DUOS_CGG"

    invoke-virtual {v4, v9, v13}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;Z)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 173
    :cond_6
    const-string v9, "SafetyAssuranceAppFeatures"

    const-string v10, "mFeatureEnableDualMode true"

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    sput-boolean v12, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mFeatureEnableDualMode:Z

    .line 176
    :cond_7
    const-string v9, "SEC_FLOATING_FEATURE_RIL_MULTISIM_DUOS_GSMGSM"

    invoke-virtual {v4, v9, v13}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;Z)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 177
    const-string v9, "SafetyAssuranceAppFeatures"

    const-string v10, "mFeatureEnableDualModeGsmGsm true"

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    sput-boolean v12, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mFeatureEnableDualModeGsmGsm:Z

    .line 180
    :cond_8
    const-string v9, "SEC_FLOATING_FEATURE_RIL_DSDA_ONECHIP_CDMAGSM"

    invoke-virtual {v4, v9, v13}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;Z)Z

    move-result v9

    if-nez v9, :cond_9

    const-string v9, "SEC_FLOATING_FEATURE_RIL_DSDA_ONECHIP_GSMGSM"

    invoke-virtual {v4, v9, v13}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;Z)Z

    move-result v9

    if-eqz v9, :cond_a

    .line 182
    :cond_9
    const-string v9, "SafetyAssuranceAppFeatures"

    const-string v10, "mFeatureEnableDsdaOneChip true"

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    sput-boolean v12, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mFeatureEnableDsdaOneChip:Z

    .line 186
    :cond_a
    const-string v9, "ro.build.characteristics"

    invoke-static {v9}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 187
    .local v2, "deviceType":Ljava/lang/String;
    if-eqz v2, :cond_b

    const-string v9, "tablet"

    invoke-virtual {v2, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_b

    .line 188
    sput-boolean v12, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mTabletDevice:Z

    .line 192
    :cond_b
    const-string v9, "SafetyAssuranceAppFeatures"

    const-string v10, "init end"

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    return-void

    .line 66
    .end local v0    # "configFeature":Ljava/lang/String;
    .end local v1    # "country":Ljava/lang/String;
    .end local v2    # "deviceType":Ljava/lang/String;
    .end local v4    # "floatingFeature":Lcom/samsung/android/feature/FloatingFeature;
    .end local v5    # "pm":Landroid/content/pm/PackageManager;
    .end local v6    # "posVolKey":Ljava/lang/String;
    .end local v8    # "simSlotCount":I
    :cond_c
    sput-boolean v13, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mChinaDevice:Z

    goto/16 :goto_0

    .line 97
    .restart local v1    # "country":Ljava/lang/String;
    :cond_d
    const-string v9, "JP"

    invoke-virtual {v9, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_e

    .line 99
    sput-boolean v12, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mJPNDevice:Z

    .line 101
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 105
    .restart local v5    # "pm":Landroid/content/pm/PackageManager;
    :try_start_1
    const-string v9, "com.android.mms"

    const/4 v10, 0x1

    invoke-virtual {v5, v9, v10}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    .line 106
    const-string v9, "SafetyAssuranceAppFeatures"

    const-string v10, "getApplicationInfo is installed"

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    const/4 v9, 0x0

    sput-boolean v9, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mJPNSecMessageDisable:Z
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    .line 109
    :catch_0
    move-exception v3

    .line 111
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v9, "SafetyAssuranceAppFeatures"

    const-string v10, "Send Message by Framework API"

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    sput-boolean v12, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mJPNSecMessageDisable:Z

    goto/16 :goto_1

    .line 116
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v5    # "pm":Landroid/content/pm/PackageManager;
    :cond_e
    const-string v9, "USA"

    invoke-virtual {v9, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 118
    sput-boolean v12, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mUSADevice:Z

    goto/16 :goto_1

    .line 131
    .restart local v4    # "floatingFeature":Lcom/samsung/android/feature/FloatingFeature;
    .restart local v6    # "posVolKey":Ljava/lang/String;
    :cond_f
    const-string v9, "up"

    invoke-virtual {v9, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 132
    const-string v9, "SafetyAssuranceAppFeatures"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "VolumeKey pos ["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "]"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    sput-boolean v12, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mFeatureUpTriggerKey:Z

    goto/16 :goto_2

    .line 150
    .restart local v0    # "configFeature":Ljava/lang/String;
    :cond_10
    const-string v9, "SafetyAssuranceAppFeatures"

    const-string v10, "This model supports new UX"

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    sput-boolean v12, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mFeatureSupportNewUX:Z

    goto/16 :goto_3

    .line 159
    .restart local v5    # "pm":Landroid/content/pm/PackageManager;
    :catch_1
    move-exception v3

    .line 160
    .restart local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v9, "SafetyAssuranceAppFeatures"

    const-string v10, "Disable FindoSearch"

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    sput-boolean v13, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mFeatureFindoSearchSettingAppEnable:Z

    goto/16 :goto_4

    .line 168
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v8    # "simSlotCount":I
    :cond_11
    sput-boolean v13, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mFeatureEnableMultiSim:Z

    goto/16 :goto_5
.end method

.method public static isATTDevice()Z
    .locals 1

    .prologue
    .line 215
    sget-boolean v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mATTDevice:Z

    return v0
.end method

.method public static isChinaDevice()Z
    .locals 1

    .prologue
    .line 198
    sget-boolean v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mChinaDevice:Z

    return v0
.end method

.method public static isJPNDevice()Z
    .locals 1

    .prologue
    .line 219
    sget-boolean v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mJPNDevice:Z

    return v0
.end method

.method public static isJPNSecMessageDisable()Z
    .locals 1

    .prologue
    .line 223
    sget-boolean v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mJPNSecMessageDisable:Z

    return v0
.end method

.method public static isKoreaDevice()Z
    .locals 1

    .prologue
    .line 205
    sget-boolean v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mKoreaDevice:Z

    return v0
.end method

.method public static isTabletDevice()Z
    .locals 1

    .prologue
    .line 231
    sget-boolean v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mTabletDevice:Z

    return v0
.end method

.method public static isUSADevice()Z
    .locals 1

    .prologue
    .line 227
    sget-boolean v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mUSADevice:Z

    return v0
.end method

.method public static isVZWDevice()Z
    .locals 1

    .prologue
    .line 211
    sget-boolean v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->mVZWDevice:Z

    return v0
.end method
