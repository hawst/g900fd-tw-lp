.class Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;
.super Ljava/lang/Thread;
.source "SafetyAssuranceService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SafetyAssuranceWorkThread"
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SafetyAssuranceWorkThread"


# instance fields
.field public mMyWorkHandler:Landroid/os/Handler;

.field final synthetic this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)V
    .locals 0

    .prologue
    .line 2231
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 2232
    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 2235
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 2236
    new-instance v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;-><init>(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;)V

    iput-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->mMyWorkHandler:Landroid/os/Handler;

    .line 2571
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->mMyWorkHandler:Landroid/os/Handler;

    # setter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mWorkHandler:Landroid/os/Handler;
    invoke-static {v0, v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$2402(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Landroid/os/Handler;)Landroid/os/Handler;

    .line 2572
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    # setter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mWorkLooper:Landroid/os/Looper;
    invoke-static {v0, v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$5102(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Landroid/os/Looper;)Landroid/os/Looper;

    .line 2574
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 2575
    return-void
.end method
