.class public Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;
.super Ljava/lang/Object;
.source "SafetyAssuranceUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils$OneTouchReportLocationSettingsUtility;
    }
.end annotation


# static fields
.field private static final CSC_COUNTRY_ISO:Ljava/lang/String; = "GeneralInfo.CountryISO"

.field private static final CSC_SHUTTERSOUND:Ljava/lang/String; = "Settings.Multimedia.Camera.ShutterSound"

.field private static final DEBUG:Z = false

.field public static final LOG_LIMIT:I = 0x24

.field private static final MAX_CONTACTS:I = 0x4

.field public static final OFF:Z = false

.field public static final ON:Z = true

.field private static final TAG:Ljava/lang/String; = "SafetyAssuranceUtils"

.field private static final VOLUME_UP_DOWN:Ljava/lang/String; = "114,115"

.field private static mSavedForceShutterSound:I


# instance fields
.field private final gCSCountryCodes:[Ljava/lang/String;

.field private final gEPSalesCodes:[Ljava/lang/String;

.field private mConnectivityManager:Landroid/net/ConnectivityManager;

.field private mContext:Landroid/content/Context;

.field private mLocationManager:Landroid/location/LocationManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const/4 v0, -0x1

    sput v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->mSavedForceShutterSound:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "XEF"

    aput-object v1, v0, v2

    const-string v1, "TFN"

    aput-object v1, v0, v3

    iput-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->gEPSalesCodes:[Ljava/lang/String;

    .line 69
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "WW"

    aput-object v1, v0, v2

    const-string v1, "AU"

    aput-object v1, v0, v3

    const-string v1, "CN"

    aput-object v1, v0, v4

    const/4 v1, 0x3

    const-string v2, "JP"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "KR"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->gCSCountryCodes:[Ljava/lang/String;

    .line 76
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->mContext:Landroid/content/Context;

    .line 77
    const-string v0, "connectivity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->mConnectivityManager:Landroid/net/ConnectivityManager;

    .line 78
    const-string v0, "location"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->mLocationManager:Landroid/location/LocationManager;

    .line 79
    return-void
.end method

.method private getCscIsoCode()Ljava/lang/String;
    .locals 2

    .prologue
    .line 500
    new-instance v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCscParser;

    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCscParser;->getCustomerPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCscParser;-><init>(Ljava/lang/String;)V

    .line 501
    .local v0, "parser":Lcom/sec/android/app/safetyassurance/SafetyAssuranceCscParser;
    const-string v1, "GeneralInfo.CountryISO"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getCurrentDateTime()J
    .locals 6

    .prologue
    .line 594
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    .line 595
    .local v0, "calendar":Ljava/util/GregorianCalendar;
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    .line 596
    .local v1, "time":Landroid/text/format/Time;
    iget-object v5, v1, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    invoke-static {v5}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v4

    .line 597
    .local v4, "timezone":Ljava/util/TimeZone;
    invoke-virtual {v0, v4}, Ljava/util/GregorianCalendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 598
    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v2

    .line 600
    .local v2, "lCurrentTime":J
    return-wide v2
.end method

.method public static getExplicitIntent(Ljava/lang/String;Landroid/content/Context;)Landroid/content/Intent;
    .locals 10
    .param p0, "action"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v9, 0x0

    .line 604
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3, p0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 605
    .local v3, "intent":Landroid/content/Intent;
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 606
    .local v5, "pm":Landroid/content/pm/PackageManager;
    invoke-virtual {v5, v3, v9}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v6

    .line 607
    .local v6, "resolveInfos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz v6, :cond_0

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v8

    if-lez v8, :cond_0

    .line 608
    invoke-interface {v6, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/pm/ResolveInfo;

    .line 609
    .local v7, "serviceInfo":Landroid/content/pm/ResolveInfo;
    iget-object v8, v7, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v4, v8, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    .line 610
    .local v4, "packageName":Ljava/lang/String;
    iget-object v8, v7, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v0, v8, Landroid/content/pm/ServiceInfo;->name:Ljava/lang/String;

    .line 611
    .local v0, "className":Ljava/lang/String;
    new-instance v1, Landroid/content/ComponentName;

    invoke-direct {v1, v4, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 612
    .local v1, "component":Landroid/content/ComponentName;
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 613
    .local v2, "iapIntent":Landroid/content/Intent;
    invoke-virtual {v2, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 614
    invoke-virtual {v2, p0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 618
    .end local v0    # "className":Ljava/lang/String;
    .end local v1    # "component":Landroid/content/ComponentName;
    .end local v2    # "iapIntent":Landroid/content/Intent;
    .end local v4    # "packageName":Ljava/lang/String;
    .end local v7    # "serviceInfo":Landroid/content/pm/ResolveInfo;
    :goto_0
    return-object v2

    .line 617
    :cond_0
    const-string v8, "SafetyAssuranceUtils"

    const-string v9, "bindService failed. No implicit intent."

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 618
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private getForceShutterSound()Ljava/lang/String;
    .locals 2

    .prologue
    .line 509
    new-instance v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCscParser;

    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCscParser;->getCustomerPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCscParser;-><init>(Ljava/lang/String;)V

    .line 510
    .local v0, "parser":Lcom/sec/android/app/safetyassurance/SafetyAssuranceCscParser;
    const-string v1, "Settings.Multimedia.Camera.ShutterSound"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private getSalesCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 505
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCscParser;->getSalesCode()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private isSimReady()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 141
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->mContext:Landroid/content/Context;

    const-string v4, "phone"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 142
    .local v1, "telephonyManager":Landroid/telephony/TelephonyManager;
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v0

    .line 143
    .local v0, "SimState":I
    if-eqz v0, :cond_0

    if-ne v0, v2, :cond_1

    .line 147
    :cond_0
    const/4 v2, 0x0

    .line 152
    :cond_1
    return v2
.end method

.method public static isUSIMAbsent()Z
    .locals 4

    .prologue
    .line 529
    const-string v1, "gsm.sim.state"

    const-string v2, "UNKNOWN"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 530
    .local v0, "SIMState":Ljava/lang/String;
    const-string v1, "SafetyAssuranceUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "gsm.sim.state = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 531
    const-string v1, "ABSENT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "ABSENT,ABSENT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 532
    :cond_0
    const/4 v1, 0x1

    .line 534
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static queryEmergencyContactSize(Landroid/content/Context;)I
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 401
    const-string v3, "SafetyAssuranceUtils"

    const-string v4, "getEmergencyContactSize"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    const/4 v8, -0x1

    .line 404
    .local v8, "nSize":I
    sget-object v3, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "title"

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "ICE"

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "contacts"

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "emergency"

    const-string v5, "1"

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "defaultId"

    const-string v5, "3"

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 407
    .local v1, "uri":Landroid/net/Uri;
    const/4 v3, 0x2

    new-array v2, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "data1"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "display_name"

    aput-object v4, v2, v3

    .line 411
    .local v2, "PROJECTION":[Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 413
    .local v0, "cr":Landroid/content/ContentResolver;
    const/4 v6, 0x0

    .line 415
    .local v6, "c":Landroid/database/Cursor;
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 416
    if-eqz v6, :cond_0

    .line 417
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v8

    .line 419
    :cond_0
    const-string v3, "SafetyAssuranceUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Selected emergency contact size ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 423
    if-eqz v6, :cond_1

    .line 424
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 427
    :cond_1
    :goto_0
    return v8

    .line 420
    :catch_0
    move-exception v7

    .line 421
    .local v7, "e":Ljava/lang/IllegalArgumentException;
    :try_start_1
    invoke-virtual {v7}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 423
    if-eqz v6, :cond_1

    .line 424
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 423
    .end local v7    # "e":Ljava/lang/IllegalArgumentException;
    :catchall_0
    move-exception v3

    if-eqz v6, :cond_2

    .line 424
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v3
.end method

.method public static queryFavoriteContactSize(Landroid/content/Context;)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 443
    const-string v0, "SafetyAssuranceUtils"

    const-string v1, "getFavoriteContactSize"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 444
    const/4 v0, 0x2

    invoke-static {p0, v0}, Lcom/sec/android/app/safetyassurance/EmergencyContactNumberUtils;->getContactNumbersSize(Landroid/content/Context;I)I

    move-result v0

    return v0
.end method

.method public static queryFreqeuntlyContactSize(Landroid/content/Context;)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 453
    const-string v0, "SafetyAssuranceUtils"

    const-string v1, "getFreqeuntlyContactSize"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 454
    const/4 v0, 0x3

    invoke-static {p0, v0}, Lcom/sec/android/app/safetyassurance/EmergencyContactNumberUtils;->getContactNumbersSize(Landroid/content/Context;I)I

    move-result v0

    return v0
.end method

.method public static saveEmergencyLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "recipient"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;
    .param p3, "success"    # I

    .prologue
    const/16 v8, 0x24

    const/4 v6, 0x1

    .line 569
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v5

    .line 570
    .local v5, "sharedPrefs":Landroid/content/SharedPreferences;
    const-string v7, "index_number"

    invoke-interface {v5, v7, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 571
    .local v3, "preIndex":I
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v7, "pref_key_log"

    invoke-direct {v2, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 572
    .local v2, "key_number":Ljava/lang/StringBuilder;
    if-le v3, v8, :cond_1

    :goto_0
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 573
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    .line 574
    .local v0, "LogArray":Lorg/json/JSONArray;
    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 575
    invoke-virtual {v0, p1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 576
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->getCurrentDateTime()J

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, Lorg/json/JSONArray;->put(J)Lorg/json/JSONArray;

    .line 577
    invoke-virtual {v0, p2}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 579
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 580
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    if-le v3, v8, :cond_0

    .line 581
    const/4 v3, 0x1

    .line 583
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v1, v6}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 584
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 585
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v1, v6, v7}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 586
    const-string v6, "index_number"

    invoke-interface {v1, v6, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 587
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 589
    return-void

    .line 572
    .end local v0    # "LogArray":Lorg/json/JSONArray;
    .end local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_1
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "preIndex":I
    .local v4, "preIndex":I
    move v6, v3

    move v3, v4

    .end local v4    # "preIndex":I
    .restart local v3    # "preIndex":I
    goto :goto_0
.end method

.method private setGoogleNetworkProviderStatus(Z)V
    .locals 3
    .param p1, "value"    # Z

    .prologue
    .line 309
    if-eqz p1, :cond_0

    .line 312
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "network_location_opt_in"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils$OneTouchReportLocationSettingsUtility$Partner;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 320
    :goto_0
    return-void

    .line 317
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "network_location_opt_in"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils$OneTouchReportLocationSettingsUtility$Partner;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0
.end method

.method public static setWakeupKey(Landroid/content/Context;Z)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "bSet"    # Z

    .prologue
    const/4 v4, 0x1

    .line 539
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->featureSupportNewUX()Z

    move-result v1

    if-nez v1, :cond_0

    .line 540
    const-string v1, "SafetyAssuranceUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setWakeupKey  set ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 542
    if-nez p0, :cond_1

    .line 543
    const-string v1, "SafetyAssuranceUtils"

    const-string v2, "setWakeupKey : context is null"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 565
    :cond_0
    :goto_0
    return-void

    .line 547
    :cond_1
    const-string v1, "input"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/input/InputManager;

    .line 548
    .local v0, "inputManager":Landroid/hardware/input/InputManager;
    if-nez v0, :cond_2

    .line 549
    const-string v1, "SafetyAssuranceUtils"

    const-string v2, "setWakeupKey : Getting inputManager was failed"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 553
    :cond_2
    if-ne p1, v4, :cond_3

    .line 555
    const-string v1, "SafetyAssuranceUtils"

    const-string v2, "setWakeupKey : Set wake up key"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 556
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "114,115"

    invoke-virtual {v0, v1, v4, v2}, Landroid/hardware/input/InputManager;->setWakeKeyDynamically(Ljava/lang/String;ZLjava/lang/String;)V

    goto :goto_0

    .line 559
    :cond_3
    const-string v1, "SafetyAssuranceUtils"

    const-string v2, "setWakeupKey : Remove wake up key"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 560
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const-string v3, "114,115"

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/input/InputManager;->setWakeKeyDynamically(Ljava/lang/String;ZLjava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public getEmergencyContact()Ljava/lang/String;
    .locals 9

    .prologue
    .line 360
    const-string v3, "SafetyAssuranceUtils"

    const-string v4, "getEmergencyContact"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 362
    sget-object v3, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "title"

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "ICE"

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "contacts"

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "emergency"

    const-string v5, "1"

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "defaultId"

    const-string v5, "3"

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 365
    .local v1, "uri":Landroid/net/Uri;
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v3, ""

    invoke-direct {v8, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 366
    .local v8, "str":Ljava/lang/StringBuilder;
    const/4 v3, 0x2

    new-array v2, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "data1"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "display_name"

    aput-object v4, v2, v3

    .line 370
    .local v2, "PROJECTION":[Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 372
    .local v0, "cr":Landroid/content/ContentResolver;
    const/4 v6, 0x0

    .line 374
    .local v6, "c":Landroid/database/Cursor;
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 375
    if-eqz v6, :cond_2

    .line 376
    const/4 v3, -0x1

    invoke-interface {v6, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 377
    :cond_0
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 378
    const-string v3, "data1"

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 379
    invoke-interface {v6}, Landroid/database/Cursor;->isLast()Z

    move-result v3

    if-nez v3, :cond_0

    .line 380
    const-string v3, ","

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 384
    :catch_0
    move-exception v7

    .line 385
    .local v7, "e":Ljava/lang/IllegalArgumentException;
    :try_start_1
    invoke-virtual {v7}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 387
    if-eqz v6, :cond_1

    .line 388
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 391
    .end local v7    # "e":Ljava/lang/IllegalArgumentException;
    :cond_1
    :goto_1
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 387
    :cond_2
    if-eqz v6, :cond_1

    .line 388
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 387
    :catchall_0
    move-exception v3

    if-eqz v6, :cond_3

    .line 388
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v3
.end method

.method public getFavoriteContact()Ljava/lang/String;
    .locals 2

    .prologue
    .line 438
    const-string v0, "SafetyAssuranceUtils"

    const-string v1, "getFavoriteContact"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 439
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->mContext:Landroid/content/Context;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/sec/android/app/safetyassurance/EmergencyContactNumberUtils;->getContactNumbers(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFreqeuntlyContact()Ljava/lang/String;
    .locals 2

    .prologue
    .line 448
    const-string v0, "SafetyAssuranceUtils"

    const-string v1, "getFreqeuntlyContact"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 449
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->mContext:Landroid/content/Context;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/sec/android/app/safetyassurance/EmergencyContactNumberUtils;->getContactNumbers(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMobileDataEnabled()Z
    .locals 2

    .prologue
    .line 107
    const/4 v0, 0x0

    .line 108
    .local v0, "bMobileData":Z
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getMobileDataEnabled()Z

    move-result v0

    .line 111
    return v0
.end method

.method public getMobileDataInRoamingEnabled()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 127
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "data_roaming"

    invoke-static {v3, v4, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 128
    .local v0, "isEnabled":I
    if-ne v0, v1, :cond_0

    .line 135
    :goto_0
    return v1

    :cond_0
    move v1, v2

    goto :goto_0
.end method

.method getStateOfAgps()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 205
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "assisted_gps_enabled"

    const/4 v4, 0x2

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 206
    .local v0, "isEnabled":I
    if-ne v0, v1, :cond_0

    .line 213
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method getStateOfGps()Z
    .locals 2

    .prologue
    .line 183
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->mLocationManager:Landroid/location/LocationManager;

    const-string v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 186
    const/4 v0, 0x1

    .line 190
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method getStateOfLocationMode()I
    .locals 4

    .prologue
    .line 257
    const/4 v1, 0x0

    .line 260
    .local v1, "result":I
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "location_mode"

    invoke-static {v2, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 265
    :goto_0
    return v1

    .line 261
    :catch_0
    move-exception v0

    .line 262
    .local v0, "ex":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method getStateOfMobile()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 83
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v2, v1}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    .line 84
    .local v0, "mInfo":Landroid/net/NetworkInfo;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 87
    const/4 v1, 0x1

    .line 91
    :cond_0
    return v1
.end method

.method getStateOfNetworkProvider()Z
    .locals 3

    .prologue
    .line 227
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->mContext:Landroid/content/Context;

    const-string v2, "location"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 229
    .local v0, "lm":Landroid/location/LocationManager;
    const-string v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 232
    const/4 v1, 0x1

    .line 236
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method getStateOfWifi()Z
    .locals 4

    .prologue
    .line 157
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->mContext:Landroid/content/Context;

    const-string v3, "wifi"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 159
    .local v0, "wifiManager":Landroid/net/wifi/WifiManager;
    if-eqz v0, :cond_0

    .line 160
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v1

    .line 165
    .local v1, "wifiState":Z
    if-eqz v1, :cond_0

    .line 166
    const/4 v2, 0x1

    .line 170
    .end local v1    # "wifiState":Z
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isCameraShutterSoundAlwaysOn()Z
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 470
    sget v4, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->mSavedForceShutterSound:I

    if-gez v4, :cond_3

    .line 471
    const-string v4, "SafetyAssuranceUtils"

    const-string v5, "isCameraShutterSoundAlwaysOn - first run"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 473
    new-instance v1, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v1}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    .line 474
    .local v1, "info0":Landroid/hardware/Camera$CameraInfo;
    invoke-static {v2, v1}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 475
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->getForceShutterSound()Ljava/lang/String;

    move-result-object v0

    .line 476
    .local v0, "forceShutterSound":Ljava/lang/String;
    const-string v4, "SafetyAssuranceUtils"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "can disable s-sound ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v6, v1, Landroid/hardware/Camera$CameraInfo;->canDisableShutterSound:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]  force s-sound ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 478
    iget-boolean v4, v1, Landroid/hardware/Camera$CameraInfo;->canDisableShutterSound:Z

    if-eqz v4, :cond_0

    const-string v4, "on"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 480
    :cond_0
    sput v3, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->mSavedForceShutterSound:I

    move v2, v3

    .line 493
    .end local v0    # "forceShutterSound":Ljava/lang/String;
    .end local v1    # "info0":Landroid/hardware/Camera$CameraInfo;
    :cond_1
    :goto_0
    return v2

    .line 484
    .restart local v0    # "forceShutterSound":Ljava/lang/String;
    .restart local v1    # "info0":Landroid/hardware/Camera$CameraInfo;
    :cond_2
    sput v2, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->mSavedForceShutterSound:I

    goto :goto_0

    .line 489
    .end local v0    # "forceShutterSound":Ljava/lang/String;
    .end local v1    # "info0":Landroid/hardware/Camera$CameraInfo;
    :cond_3
    const-string v4, "SafetyAssuranceUtils"

    const-string v5, "isCameraShutterSoundAlwaysOn - return saved value"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 490
    sget v4, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->mSavedForceShutterSound:I

    if-ne v4, v3, :cond_1

    move v2, v3

    .line 491
    goto :goto_0
.end method

.method public isDisableCameraOption()Z
    .locals 1

    .prologue
    .line 459
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->featureDisableCameraOption()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->isCameraShutterSoundAlwaysOn()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDisableCameraOptionForGear()Z
    .locals 1

    .prologue
    .line 463
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->isCameraShutterSoundAlwaysOn()Z

    move-result v0

    return v0
.end method

.method public isVocRecHide()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 515
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->featureSupportNewUX()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 516
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->isJPNDevice()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 524
    :cond_0
    :goto_0
    return v0

    .line 521
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method setAgps(Z)V
    .locals 3
    .param p1, "value"    # Z

    .prologue
    .line 221
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "assisted_gps_enabled"

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 222
    return-void

    .line 221
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method setGps(Z)V
    .locals 3
    .param p1, "value"    # Z

    .prologue
    .line 198
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "gps"

    invoke-static {v1, v2, p1}, Landroid/provider/Settings$Secure;->setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    .line 199
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.GPS_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 200
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 201
    return-void
.end method

.method declared-synchronized setLocationMode(I)V
    .locals 3
    .param p1, "mode"    # I

    .prologue
    .line 279
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->isChinaDevice()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 280
    if-eqz p1, :cond_0

    .line 281
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "network"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    .line 282
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "gps"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    .line 291
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "location_mode"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 294
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->isChinaDevice()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 295
    if-nez p1, :cond_1

    .line 296
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "network"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    .line 297
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "gps"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 305
    :cond_1
    :goto_1
    monitor-exit p0

    return-void

    .line 286
    :cond_2
    if-eqz p1, :cond_0

    .line 287
    const/4 v0, 0x1

    :try_start_1
    invoke-direct {p0, v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->setGoogleNetworkProviderStatus(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 279
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 301
    :cond_3
    if-nez p1, :cond_1

    .line 302
    const/4 v0, 0x0

    :try_start_2
    invoke-direct {p0, v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->setGoogleNetworkProviderStatus(Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method setMobile(Z)V
    .locals 1
    .param p1, "value"    # Z

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->isSimReady()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v0, p1}, Landroid/net/ConnectivityManager;->setMobileDataEnabled(Z)V

    .line 104
    :cond_0
    return-void
.end method

.method setMobileInRoaming(Z)V
    .locals 3
    .param p1, "value"    # Z

    .prologue
    .line 119
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->isSimReady()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "data_roaming"

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 124
    :cond_0
    return-void

    .line 120
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method setNetworkProvider(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 245
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->isChinaDevice()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 252
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "network"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$Secure;->setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    .line 253
    return-void

    .line 249
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->setGoogleNetworkProviderStatus(Z)V

    goto :goto_0
.end method

.method setWifi(Z)V
    .locals 3
    .param p1, "value"    # Z

    .prologue
    .line 177
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->mContext:Landroid/content/Context;

    const-string v2, "wifi"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 178
    .local v0, "wifiManager":Landroid/net/wifi/WifiManager;
    invoke-virtual {v0, p1}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    .line 179
    return-void
.end method
