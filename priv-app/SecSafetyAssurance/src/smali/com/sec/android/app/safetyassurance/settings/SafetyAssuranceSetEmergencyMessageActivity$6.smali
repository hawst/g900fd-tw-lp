.class Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$6;
.super Ljava/lang/Object;
.source "SafetyAssuranceSetEmergencyMessageActivity.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;

.field final synthetic val$okButton:Landroid/widget/Button;


# direct methods
.method constructor <init>(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;Landroid/widget/Button;)V
    .locals 0

    .prologue
    .line 210
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$6;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;

    iput-object p2, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$6;->val$okButton:Landroid/widget/Button;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 4
    .param p1, "arg0"    # Landroid/widget/CompoundButton;
    .param p2, "arg1"    # Z

    .prologue
    const/4 v3, 0x0

    .line 213
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$6;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;

    iget-object v0, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->agreeToShareLocationCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->playSoundEffect(I)V

    .line 214
    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$6;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;

    iget-object v0, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->agreeTermsAndConditionsCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$6;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;

    iget-object v0, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->agreeToTurnOnMobileDataCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 215
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$6;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;

    # getter for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->newConceptForPhone:Z
    invoke-static {v0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->access$100(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 216
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$6;->val$okButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$6;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070005

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 217
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$6;->val$okButton:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 223
    :goto_0
    return-void

    .line 219
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$6;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;

    # getter for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->newConceptForPhone:Z
    invoke-static {v0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->access$100(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 220
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$6;->val$okButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$6;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070004

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 221
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceSetEmergencyMessageActivity$6;->val$okButton:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method
