.class public Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;
.super Ljava/lang/Object;
.source "SafetyAssurancePhoneState.java"


# static fields
.field public static final STATE_ERROR:I = -0x1

.field public static final STATE_INIT:I = -0x63

.field private static final TAG:Ljava/lang/String; = "SafetyAssurancePhoneState"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCurrentState:I

.field private mPhoneStateListener:Landroid/telephony/PhoneStateListener;

.field private mPreviousState:I

.field private mService:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

.field private mSosMode:Z

.field private mTelephonyManager:Landroid/telephony/TelephonyManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "service"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    .prologue
    const/16 v2, -0x63

    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mSosMode:Z

    .line 29
    new-instance v0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState$1;-><init>(Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;)V

    iput-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    .line 107
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mContext:Landroid/content/Context;

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mContext:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 109
    iput v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mCurrentState:I

    .line 110
    iput v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mPreviousState:I

    .line 111
    iput-object p2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mService:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    .line 112
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

    .prologue
    .line 16
    iget v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mPreviousState:I

    return v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;
    .param p1, "x1"    # I

    .prologue
    .line 16
    iput p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mPreviousState:I

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

    .prologue
    .line 16
    iget v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mCurrentState:I

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;
    .param p1, "x1"    # I

    .prologue
    .line 16
    iput p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mCurrentState:I

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;)Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mService:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->retrySendAlarmAndMessage()V

    return-void
.end method

.method private retrySendAlarmAndMessage()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 147
    const-string v1, "SafetyAssurancePhoneState"

    const-string v2, "retrySendAlarmAndMessage"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    const/4 v0, 0x0

    .line 150
    .local v0, "startService":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "sos_mode"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-ne v4, v1, :cond_0

    .line 151
    const-string v1, "SafetyAssurancePhoneState"

    const-string v2, "SOS MODE ON"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "startService":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mContext:Landroid/content/Context;

    const-class v2, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 153
    .restart local v0    # "startService":Landroid/content/Intent;
    const-string v1, "android.intent.action.SAFETY_SERVICE_B_ALARM"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 154
    const-string v1, "SA_FIRSTALARM"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 155
    const-string v1, "sos_mode"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 156
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 164
    :goto_0
    return-void

    .line 158
    :cond_0
    const-string v1, "SafetyAssurancePhoneState"

    const-string v2, "SOS MODE OFF"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "startService":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mContext:Landroid/content/Context;

    const-class v2, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 160
    .restart local v0    # "startService":Landroid/content/Intent;
    const-string v1, "android.intent.action.SAFETY_SERVICE_ALARM"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 161
    const-string v1, "SA_FIRSTALARM"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 162
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 3

    .prologue
    .line 129
    const-string v0, "SafetyAssurancePhoneState"

    const-string v1, "close"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 132
    return-void
.end method

.method public getPhoneState()I
    .locals 5

    .prologue
    .line 135
    const/4 v1, -0x1

    .line 137
    .local v1, "state":I
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getServiceState()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 142
    :goto_0
    const-string v2, "SafetyAssurancePhoneState"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getPhoneState result ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    return v1

    .line 138
    :catch_0
    move-exception v0

    .line 139
    .local v0, "ex":Ljava/lang/Exception;
    const-string v2, "SafetyAssurancePhoneState"

    const-string v3, "Unavailable to get telephony"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public restart()V
    .locals 3

    .prologue
    .line 121
    const-string v0, "SafetyAssurancePhoneState"

    const-string v1, "restart"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 125
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/16 v2, 0x41

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 126
    return-void
.end method

.method public setSosMode(Z)V
    .locals 0
    .param p1, "isSosMode"    # Z

    .prologue
    .line 167
    iput-boolean p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mSosMode:Z

    .line 168
    return-void
.end method

.method public start()V
    .locals 3

    .prologue
    .line 115
    const-string v0, "SafetyAssurancePhoneState"

    const-string v1, "start"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/16 v2, 0x41

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 118
    return-void
.end method
