.class public final Lcom/sec/android/app/safetyassurance/BuildConfig;
.super Ljava/lang/Object;
.source "BuildConfig.java"


# static fields
.field public static final APPLICATION_ID:Ljava/lang/String; = "com.sec.android.app.safetyassurance"

.field public static final BUILD_TYPE:Ljava/lang/String; = "release"

.field public static final DEBUG:Z = false

.field public static final FLAVOR:Ljava/lang/String; = "lightOpenFHD"

.field public static final FLAVOR_dpi:Ljava/lang/String; = "FHD"

.field public static final FLAVOR_region:Ljava/lang/String; = "open"

.field public static final FLAVOR_theme:Ljava/lang/String; = "light"

.field public static final PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.app.safetyassurance"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final VERSION_CODE:I = 0x78782ad1

.field public static final VERSION_NAME:Ljava/lang/String; = "2.21.152750"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
