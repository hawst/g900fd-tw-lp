.class Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState$1;
.super Landroid/telephony/PhoneStateListener;
.source "SafetyAssurancePhoneState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;


# direct methods
.method constructor <init>(Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;)V
    .locals 0

    .prologue
    .line 29
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState$1;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDataConnectionStateChanged(II)V
    .locals 3
    .param p1, "state"    # I
    .param p2, "networkType"    # I

    .prologue
    .line 101
    const-string v0, "SafetyAssurancePhoneState"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDataConnectionStateChanged  state["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState$1;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mCurrentState:I
    invoke-static {v2}, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->access$100(Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]  networkType["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    return-void
.end method

.method public onServiceStateChanged(Landroid/telephony/ServiceState;)V
    .locals 8
    .param p1, "state"    # Landroid/telephony/ServiceState;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 32
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState$1;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

    iget-object v4, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState$1;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mCurrentState:I
    invoke-static {v4}, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->access$100(Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;)I

    move-result v4

    # setter for: Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mPreviousState:I
    invoke-static {v3, v4}, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->access$002(Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;I)I

    .line 33
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState$1;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getState()I

    move-result v4

    # setter for: Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mCurrentState:I
    invoke-static {v3, v4}, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->access$102(Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;I)I

    .line 34
    const-string v3, "SafetyAssurancePhoneState"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onServiceStateChanged  Service state["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState$1;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mCurrentState:I
    invoke-static {v5}, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->access$100(Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 36
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->featureEnableMultiSim()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 37
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState$1;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mCurrentState:I
    invoke-static {v3}, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->access$100(Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;)I

    move-result v3

    if-eqz v3, :cond_3

    .line 38
    const/4 v2, 0x0

    .line 40
    .local v2, "slot2Service":I
    const-string v3, "ril.ICC_TYPE_1"

    const-string v4, "0"

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 41
    .local v0, "SimState2":Ljava/lang/String;
    const-string v3, "SafetyAssurancePhoneState"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SimState2(ril.ICC_TYPE_1)="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    const-string v3, "0"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 45
    const/4 v2, 0x0

    .line 47
    :cond_0
    if-nez v2, :cond_1

    .line 48
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState$1;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

    # setter for: Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mCurrentState:I
    invoke-static {v3, v2}, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->access$102(Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;I)I

    .line 50
    :cond_1
    const-string v3, "SafetyAssurancePhoneState"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onServiceStateChanged  Service state (MultiSim) ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState$1;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mCurrentState:I
    invoke-static {v5}, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->access$100(Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->featureEnableDualMode()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 53
    const/4 v1, 0x0

    .line 55
    .local v1, "secondaryService":I
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->featureEnableDualModeGsmGsm()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 56
    const-string v3, "service.ServiceState.gsmSecond"

    invoke-static {v3, v6}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 61
    :goto_0
    if-nez v1, :cond_2

    .line 62
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState$1;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

    # setter for: Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mCurrentState:I
    invoke-static {v3, v1}, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->access$102(Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;I)I

    .line 64
    :cond_2
    const-string v3, "SafetyAssurancePhoneState"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onServiceStateChanged  Service state (DUALMODE)["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState$1;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mCurrentState:I
    invoke-static {v5}, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->access$100(Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    .end local v0    # "SimState2":Ljava/lang/String;
    .end local v1    # "secondaryService":I
    .end local v2    # "slot2Service":I
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState$1;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mCurrentState:I
    invoke-static {v3}, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->access$100(Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState$1;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mPreviousState:I
    invoke-static {v4}, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->access$000(Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;)I

    move-result v4

    if-eq v3, v4, :cond_4

    .line 70
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState$1;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mPreviousState:I
    invoke-static {v3}, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->access$000(Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;)I

    move-result v3

    const/16 v4, -0x63

    if-ne v3, v4, :cond_7

    .line 72
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState$1;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mCurrentState:I
    invoke-static {v3}, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->access$100(Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;)I

    move-result v3

    if-nez v3, :cond_6

    .line 73
    const-string v3, "SafetyAssurancePhoneState"

    const-string v4, "onServiceStateChanged  [First state] In Service"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState$1;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mService:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;
    invoke-static {v3}, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->access$200(Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;)Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->setAlarm()V

    .line 97
    :cond_4
    :goto_1
    return-void

    .line 58
    .restart local v0    # "SimState2":Ljava/lang/String;
    .restart local v1    # "secondaryService":I
    .restart local v2    # "slot2Service":I
    :cond_5
    const-string v3, "service.ServiceState.gsm"

    invoke-static {v3, v6}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v1

    goto :goto_0

    .line 76
    .end local v0    # "SimState2":Ljava/lang/String;
    .end local v1    # "secondaryService":I
    .end local v2    # "slot2Service":I
    :cond_6
    const-string v3, "SafetyAssurancePhoneState"

    const-string v4, "onServiceStateChanged  [First state] No Service"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 81
    :cond_7
    const-string v3, "SafetyAssurancePhoneState"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mPreviousState = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState$1;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mPreviousState:I
    invoke-static {v5}, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->access$000(Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    const-string v3, "SafetyAssurancePhoneState"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mCurrentState = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState$1;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mCurrentState:I
    invoke-static {v5}, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->access$100(Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState$1;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mPreviousState:I
    invoke-static {v3}, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->access$000(Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;)I

    move-result v3

    if-ne v3, v7, :cond_8

    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState$1;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mCurrentState:I
    invoke-static {v3}, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->access$100(Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;)I

    move-result v3

    if-nez v3, :cond_8

    .line 85
    const-string v3, "SafetyAssurancePhoneState"

    const-string v4, "onServiceStateChanged  [On going state] In Service"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState$1;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mService:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;
    invoke-static {v3}, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->access$200(Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;)Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->setAlarm()V

    .line 87
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState$1;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

    # invokes: Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->retrySendAlarmAndMessage()V
    invoke-static {v3}, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->access$300(Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;)V

    goto :goto_1

    .line 88
    :cond_8
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState$1;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mPreviousState:I
    invoke-static {v3}, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->access$000(Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;)I

    move-result v3

    if-nez v3, :cond_9

    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState$1;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mCurrentState:I
    invoke-static {v3}, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->access$100(Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;)I

    move-result v3

    if-eq v3, v7, :cond_a

    :cond_9
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState$1;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mPreviousState:I
    invoke-static {v3}, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->access$000(Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;)I

    move-result v3

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState$1;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mCurrentState:I
    invoke-static {v3}, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->access$100(Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;)I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_4

    .line 91
    :cond_a
    const-string v3, "SafetyAssurancePhoneState"

    const-string v4, "onServiceStateChanged  [On going state] No Service"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState$1;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->mService:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;
    invoke-static {v3}, Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;->access$200(Lcom/sec/android/app/safetyassurance/SafetyAssurancePhoneState;)Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->removeAlarm()V

    goto/16 :goto_1
.end method
