.class Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$5$2;
.super Ljava/lang/Object;
.source "SafetyAssuranceFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$5;->handleMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$5;


# direct methods
.method constructor <init>(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$5;)V
    .locals 0

    .prologue
    .line 915
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$5$2;->this$1:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 918
    const-string v2, "SafetyAssuranceFragment"

    const-string v3, "SHOW_NO_EMERGENCY_CONTACT_DIALOG - add"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 920
    :try_start_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.android.app.contacts.action.INTERACTION_EMERGENCY_MESSAGE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 921
    .local v1, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$5$2;->this$1:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$5;

    iget-object v2, v2, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$5;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    const/4 v3, 0x2

    invoke-virtual {v2, v1, v3}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 925
    .end local v1    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 922
    :catch_0
    move-exception v0

    .line 923
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
