.class public final Lcom/sec/android/app/safetyassurance/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/safetyassurance/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final Activated_Emergency_message:I = 0x7f090000

.field public static final Deactivated_Emergency_message:I = 0x7f090001

.field public static final Every_minutes:I = 0x7f090002

.field public static final SASEMActivity_DescriptionOfEergencyMessage_OnEmergencySituation:I = 0x7f090003

.field public static final SASEMActivity_DescriptionOfEergencyMessage_PressAndHold:I = 0x7f090004

.field public static final SASEMActivity_DescriptionOfEergencyMessage_WhenEmergencySituation:I = 0x7f090005

.field public static final SASEMActivity_I_agree_that_mobile_data:I = 0x7f090006

.field public static final SASEMActivity_share_my_location_checkbox_text:I = 0x7f090007

.field public static final SASEMActivity_terms_and_conditions_above_checkbox_text:I = 0x7f090008

.field public static final SASEMActivity_terms_and_conditions_above_checkbox_text_usa:I = 0x7f090009

.field public static final SASEMActivity_terms_and_conditions_checkbox_text:I = 0x7f09000a

.field public static final add:I = 0x7f09000b

.field public static final add_contacts_to_ec:I = 0x7f09000c

.field public static final add_contacts_to_ec_atlesatone:I = 0x7f09000d

.field public static final add_contacts_to_pc:I = 0x7f09000e

.field public static final adjust_the_interval_sending_message_and_images:I = 0x7f09000f

.field public static final app_name:I = 0x7f090010

.field public static final automatic_sending_interval:I = 0x7f090011

.field public static final b_captured_image_will_be_sent:I = 0x7f090012

.field public static final cannot_find_location:I = 0x7f090013

.field public static final cannot_save_message:I = 0x7f090014

.field public static final close:I = 0x7f090015

.field public static final confirm_delete_message:I = 0x7f090016

.field public static final confirm_dialog_title:I = 0x7f090017

.field public static final create_emergency_contact:I = 0x7f090018

.field public static final create_primary_contact:I = 0x7f090019

.field public static final descriptionOfSafetyAssurance_B_TextView:I = 0x7f09001a

.field public static final descriptionOfSafetyAssurance_TextView:I = 0x7f09001b

.field public static final descriptionWithoutTakingPicturesOfSafetyAssurance_TextView:I = 0x7f09001c

.field public static final description_sa_with_sc_textview:I = 0x7f09001d

.field public static final description_sa_with_sc_textview2:I = 0x7f09001e

.field public static final description_sa_with_sc_textview2_pc:I = 0x7f09001f

.field public static final description_sa_with_sc_textview3:I = 0x7f090020

.field public static final description_sa_with_sc_textview3_onlysendrecording:I = 0x7f090021

.field public static final description_sa_with_sc_textview_att:I = 0x7f090022

.field public static final description_sa_with_sc_textview_pc:I = 0x7f090023

.field public static final do_not_repeat:I = 0x7f090024

.field public static final dual_captured_image_will_be_sent:I = 0x7f090025

.field public static final edit_emergency_message:I = 0x7f090026

.field public static final emergency_contacts:I = 0x7f090027

.field public static final emergency_log:I = 0x7f090028

.field public static final emergency_log_empty:I = 0x7f090029

.field public static final emergency_log_menu_clear:I = 0x7f09002a

.field public static final emergency_message:I = 0x7f09002b

.field public static final emergency_message_sent:I = 0x7f09002c

.field public static final emergency_messages:I = 0x7f09002d

.field public static final emergency_situation:I = 0x7f09002e

.field public static final ineedhelp:I = 0x7f09002f

.field public static final messages_are_being_sent_emergency_contacts:I = 0x7f090030

.field public static final messages_are_being_sent_primary_contacts:I = 0x7f090031

.field public static final nosim:I = 0x7f090032

.field public static final nouim:I = 0x7f090033

.field public static final once:I = 0x7f090034

.field public static final permission_description:I = 0x7f090035

.field public static final save:I = 0x7f090036

.field public static final seconds:I = 0x7f090037

.field public static final send_b_captured_image:I = 0x7f090038

.field public static final send_dual_captured_image:I = 0x7f090039

.field public static final send_pictures:I = 0x7f09003a

.field public static final send_pictures_b_description:I = 0x7f09003b

.field public static final send_pictures_description:I = 0x7f09003c

.field public static final send_pictures_description_att:I = 0x7f09003d

.field public static final send_sound_recording:I = 0x7f09003e

.field public static final send_to_msg_in_time:I = 0x7f09003f

.field public static final sendhelpmessage:I = 0x7f090040

.field public static final sending_emergency_message:I = 0x7f090041

.field public static final shm_I_agree_that_mobile_data:I = 0x7f090042

.field public static final shm_new_disclaimer_msg_01:I = 0x7f090043

.field public static final shm_new_disclaimer_msg_chn_01:I = 0x7f090044

.field public static final shm_new_disclaimer_msg_kor_01:I = 0x7f090045

.field public static final shm_new_disclaimer_msg_kor_02:I = 0x7f090046

.field public static final shm_new_disclaimer_msg_kor_03:I = 0x7f090047

.field public static final shm_new_disclaimer_msg_usa_01:I = 0x7f090048

.field public static final shm_share_my_location_checkbox_text:I = 0x7f090049

.field public static final sound_recording_description:I = 0x7f09004a

.field public static final sound_recording_description_att:I = 0x7f09004b

.field public static final stms_appgroup:I = 0x7f09004c

.field public static final stop_emergency_situation:I = 0x7f09004d

.field public static final terms_and_conditions:I = 0x7f09004e

.field public static final urgent_situation:I = 0x7f09004f

.field public static final you_didnt_set_the_emergency_contacts:I = 0x7f090050


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
