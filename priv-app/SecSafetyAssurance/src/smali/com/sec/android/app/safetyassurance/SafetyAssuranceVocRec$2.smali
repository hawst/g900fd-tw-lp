.class Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec$2;
.super Landroid/content/BroadcastReceiver;
.source "SafetyAssuranceVocRec.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->registerBroadcastReceiverSDCard(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;


# direct methods
.method constructor <init>(Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;)V
    .locals 0

    .prologue
    .line 590
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec$2;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 593
    const-string v1, "SafetyAssuranceVocRec"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onReceive: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 594
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 595
    .local v0, "action":Ljava/lang/String;
    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 596
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec$2;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;

    invoke-virtual {v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->cancelRecording()Z

    .line 600
    :cond_0
    :goto_0
    return-void

    .line 597
    :cond_1
    const-string v1, "android.intent.action.MEDIA_BAD_REMOVAL"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 598
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec$2;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;

    invoke-virtual {v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->cancelRecording()Z

    goto :goto_0
.end method
