.class public Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceMessageLogs;
.super Landroid/preference/PreferenceActivity;
.source "SafetyAssuranceMessageLogs.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceMessageLogs$SafetyLogReceiver;
    }
.end annotation


# static fields
.field public static final LOG_RESUME:Ljava/lang/String; = "com.sec.android.app.safetyassurance.action.LOG_RESUME"

.field private static final MENU_CLEAR_LOG:I = 0x3e9

.field public static TAG:Ljava/lang/String; = null

.field private static final preLogKey:Ljava/lang/String; = "pref_key_log"


# instance fields
.field private mReceiver:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceMessageLogs$SafetyLogReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-string v0, "SafetyAssuranceMessageLogs"

    sput-object v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceMessageLogs;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceMessageLogs;->mReceiver:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceMessageLogs$SafetyLogReceiver;

    .line 111
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceMessageLogs;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceMessageLogs;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceMessageLogs;->clearMessageHistory()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceMessageLogs;)Landroid/preference/PreferenceScreen;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceMessageLogs;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceMessageLogs;->createPreference()Landroid/preference/PreferenceScreen;

    move-result-object v0

    return-object v0
.end method

.method private clearMessageHistory()V
    .locals 6

    .prologue
    .line 85
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 86
    .local v3, "sharedPrefs":Landroid/content/SharedPreferences;
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 87
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    const/4 v0, 0x1

    .local v0, "count":I
    :goto_0
    const/16 v4, 0x24

    if-gt v0, v4, :cond_0

    .line 88
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "pref_key_log"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 89
    .local v2, "prefString":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 90
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "pref_key_log"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 91
    const-string v4, "index_number"

    const/4 v5, 0x1

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 92
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 87
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 97
    .end local v2    # "prefString":Ljava/lang/String;
    :cond_0
    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceMessageLogs;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    .line 98
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceMessageLogs;->onResume()V

    .line 99
    return-void
.end method

.method private createPreference()Landroid/preference/PreferenceScreen;
    .locals 14

    .prologue
    .line 136
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v8

    .line 137
    .local v8, "sharedPrefs":Landroid/content/SharedPreferences;
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceMessageLogs;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v10

    invoke-virtual {v10, p0}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v7

    .line 140
    .local v7, "root":Landroid/preference/PreferenceScreen;
    const/16 v10, 0x25

    new-array v0, v10, [J

    .line 141
    .local v0, "SATime":[J
    const/4 v1, 0x1

    .local v1, "count":I
    :goto_0
    const/16 v10, 0x24

    if-gt v1, v10, :cond_0

    .line 149
    :try_start_0
    new-instance v4, Lorg/json/JSONArray;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "pref_key_log"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v11, ""

    invoke-interface {v8, v10, v11}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v4, v10}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 152
    .local v4, "jsonData":Lorg/json/JSONArray;
    const/4 v10, 0x2

    invoke-virtual {v4, v10}, Lorg/json/JSONArray;->getLong(I)J

    move-result-wide v10

    aput-wide v10, v0, v1
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    .line 141
    .end local v4    # "jsonData":Lorg/json/JSONArray;
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 158
    :cond_0
    invoke-direct {p0, v0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceMessageLogs;->recentMessageSorting([J)V

    .line 160
    const/4 v9, 0x1

    .local v9, "timeIndexCount":I
    :goto_2
    const/16 v10, 0x24

    if-gt v9, v10, :cond_4

    .line 161
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 162
    .local v5, "preSummary":Ljava/lang/StringBuilder;
    new-instance v6, Landroid/preference/Preference;

    invoke-direct {v6, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 165
    .local v6, "preference":Landroid/preference/Preference;
    const/4 v1, 0x1

    :goto_3
    const/16 v10, 0x24

    if-gt v1, v10, :cond_3

    .line 167
    :try_start_1
    new-instance v4, Lorg/json/JSONArray;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "pref_key_log"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v11, ""

    invoke-interface {v8, v10, v11}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v4, v10}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 169
    .restart local v4    # "jsonData":Lorg/json/JSONArray;
    aget-wide v10, v0, v9

    const/4 v12, 0x2

    invoke-virtual {v4, v12}, Lorg/json/JSONArray;->getLong(I)J

    move-result-wide v12

    cmp-long v10, v10, v12

    if-nez v10, :cond_1

    .line 171
    const/4 v10, 0x0

    invoke-virtual {v4, v10}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    const/4 v11, 0x1

    if-ne v10, v11, :cond_2

    .line 172
    const v10, 0x7f020009

    invoke-virtual {v6, v10}, Landroid/preference/Preference;->setIcon(I)V

    .line 176
    :goto_4
    const-string v10, "(yyyy-MM-dd kk:mm)"

    const/4 v11, 0x2

    invoke-virtual {v4, v11}, Lorg/json/JSONArray;->getLong(I)J

    move-result-wide v12

    invoke-static {v10, v12, v13}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v10

    invoke-interface {v10}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 178
    .local v2, "dateTime":Ljava/lang/String;
    const/4 v10, 0x1

    invoke-virtual {v4, v10}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const/16 v11, 0xa

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 179
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const/16 v11, 0xa

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 180
    const/4 v10, 0x3

    invoke-virtual {v4, v10}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 181
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "pref_key_log"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    .line 182
    invoke-virtual {v6, v5}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 186
    invoke-virtual {v7, v6}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 165
    .end local v2    # "dateTime":Ljava/lang/String;
    .end local v4    # "jsonData":Lorg/json/JSONArray;
    :cond_1
    :goto_5
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_3

    .line 174
    .restart local v4    # "jsonData":Lorg/json/JSONArray;
    :cond_2
    const v10, 0x7f020008

    invoke-virtual {v6, v10}, Landroid/preference/Preference;->setIcon(I)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_4

    .line 188
    .end local v4    # "jsonData":Lorg/json/JSONArray;
    :catch_0
    move-exception v3

    .line 190
    .local v3, "e":Lorg/json/JSONException;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "pref_key_log"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    goto :goto_5

    .line 160
    .end local v3    # "e":Lorg/json/JSONException;
    :cond_3
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_2

    .line 195
    .end local v5    # "preSummary":Ljava/lang/StringBuilder;
    .end local v6    # "preference":Landroid/preference/Preference;
    :cond_4
    return-object v7

    .line 154
    .end local v9    # "timeIndexCount":I
    :catch_1
    move-exception v10

    goto/16 :goto_1
.end method

.method private isEmptyLogs()Z
    .locals 5

    .prologue
    .line 225
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 226
    .local v2, "sharedPrefs":Landroid/content/SharedPreferences;
    const-string v3, "pref_key_log1"

    const-string v4, ""

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 227
    .local v0, "first_log":Ljava/lang/String;
    const/4 v1, 0x1

    .line 228
    .local v1, "isEmpty":Z
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 229
    const/4 v1, 0x0

    .line 231
    :cond_0
    return v1
.end method

.method private recentMessageSorting([J)V
    .locals 9
    .param p1, "time"    # [J

    .prologue
    const/16 v8, 0x24

    .line 123
    const/4 v0, 0x1

    .local v0, "fst":I
    :goto_0
    if-ge v0, v8, :cond_2

    .line 124
    add-int/lit8 v1, v0, 0x1

    .local v1, "snd":I
    :goto_1
    if-gt v1, v8, :cond_1

    .line 125
    aget-wide v4, p1, v0

    aget-wide v6, p1, v1

    cmp-long v4, v4, v6

    if-gez v4, :cond_0

    .line 126
    aget-wide v2, p1, v0

    .line 127
    .local v2, "temp":J
    aget-wide v4, p1, v1

    aput-wide v4, p1, v0

    .line 128
    aput-wide v2, p1, v1

    .line 124
    .end local v2    # "temp":J
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 123
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 132
    .end local v1    # "snd":I
    :cond_2
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x1

    .line 41
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 43
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceMessageLogs;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 44
    .local v2, "intent":Landroid/content/Intent;
    const-string v3, "fromSearch"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 45
    .local v1, "fromSearch":Ljava/lang/Boolean;
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 46
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceMessageLogs;->isEmptyLogs()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 47
    const v3, 0x7f090029

    invoke-static {p0, v3, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 48
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceMessageLogs;->finish()V

    .line 51
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceMessageLogs;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 52
    .local v0, "actionBar":Landroid/app/ActionBar;
    if-eqz v0, :cond_1

    .line 53
    invoke-virtual {v0, v5}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 54
    const v3, 0x7f090028

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setTitle(I)V

    .line 57
    :cond_1
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 60
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 80
    :goto_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    return v1

    .line 62
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceMessageLogs;->finish()V

    goto :goto_0

    .line 65
    :sswitch_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 66
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f090017

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f090016

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/high16 v2, 0x1040000

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x104000a

    new-instance v3, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceMessageLogs$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceMessageLogs$1;-><init>(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceMessageLogs;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 77
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0

    .line 60
    nop

    :sswitch_data_0
    .sparse-switch
        0x3e9 -> :sswitch_1
        0x102002c -> :sswitch_0
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceMessageLogs;->mReceiver:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceMessageLogs$SafetyLogReceiver;

    if-eqz v0, :cond_0

    .line 218
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceMessageLogs;->mReceiver:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceMessageLogs$SafetyLogReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceMessageLogs;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 219
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceMessageLogs;->mReceiver:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceMessageLogs$SafetyLogReceiver;

    .line 221
    :cond_0
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onPause()V

    .line 222
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v3, 0x0

    .line 201
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 202
    const/16 v1, 0x3e9

    const v2, 0x7f09002a

    invoke-interface {p1, v3, v1, v3, v2}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    .line 203
    .local v0, "deleteLog":Landroid/view/MenuItem;
    const v1, 0x108003c

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 205
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceMessageLogs;->isEmptyLogs()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 206
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 211
    :goto_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    return v1

    .line 208
    :cond_0
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 102
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    .line 103
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceMessageLogs;->createPreference()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceMessageLogs;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceMessageLogs;->mReceiver:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceMessageLogs$SafetyLogReceiver;

    if-nez v0, :cond_0

    .line 106
    new-instance v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceMessageLogs$SafetyLogReceiver;

    invoke-direct {v0, p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceMessageLogs$SafetyLogReceiver;-><init>(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceMessageLogs;)V

    iput-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceMessageLogs;->mReceiver:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceMessageLogs$SafetyLogReceiver;

    .line 107
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceMessageLogs;->mReceiver:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceMessageLogs$SafetyLogReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.sec.android.app.safetyassurance.action.LOG_RESUME"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceMessageLogs;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 109
    :cond_0
    return-void
.end method
