.class Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$5;
.super Ljava/lang/Object;
.source "SafetyAssuranceService.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->showQuitEmergencyDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)V
    .locals 0

    .prologue
    .line 674
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$5;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 677
    const-string v0, "SafetyAssuranceService"

    const-string v1, "quitDialog onDismiss"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 678
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$5;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mQuitDialog:Landroid/app/AlertDialog;
    invoke-static {v0, v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$302(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 679
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$5;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    iget-object v0, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 680
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$5;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$400(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$5;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    iget-object v1, v1, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 681
    :cond_0
    return-void
.end method
