.class Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity$2;
.super Ljava/lang/Object;
.source "SafetyAssuranceEditingEmergencyMessageActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;)V
    .locals 0

    .prologue
    .line 125
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity$2;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity$2;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;

    # getter for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;->imm:Landroid/view/inputmethod/InputMethodManager;
    invoke-static {v0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;->access$000(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isAccessoryKeyboardState()I

    move-result v0

    if-nez v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity$2;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;

    # getter for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;->imm:Landroid/view/inputmethod/InputMethodManager;
    invoke-static {v0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;->access$000(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity$2;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;

    iget-object v1, v1, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;->mCreateEdit:Landroid/widget/EditText;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 132
    :goto_0
    return-void

    .line 130
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity$2;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;

    # getter for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;->imm:Landroid/view/inputmethod/InputMethodManager;
    invoke-static {v0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;->access$000(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity$2;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;

    iget-object v1, v1, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;->mCreateEdit:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    goto :goto_0
.end method
