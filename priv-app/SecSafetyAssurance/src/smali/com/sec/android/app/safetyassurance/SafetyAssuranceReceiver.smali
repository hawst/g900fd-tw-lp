.class public Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SafetyAssuranceReceiver.java"


# static fields
.field public static final ALARMACTION:Ljava/lang/String; = "android.intent.action.SAFETY_MESSAGE_ALARM_ACTION"

.field public static final B_ALARMACTION:Ljava/lang/String; = "android.intent.action.SAFETY_MESSAGE_B_ALARM_ACTION"

.field public static final QUITEMERGENCYSITUATION:Ljava/lang/String; = "android.intent.action.QUIT_EMERGENCY_SITUATION"

.field public static final QUITEMERGENCYSITUATION_FORCESTOP:Ljava/lang/String; = "android.intent.action.QUIT_EMERGENCY_SITUATION_FORCESTOP"

.field public static final SA_FIRSTALARM:Ljava/lang/String; = "SA_FIRSTALARM"

.field public static final SENT_ACTION:Ljava/lang/String; = "com.sec.android.app.safetyassurance.SENT_MESSAGE"

.field public static final SHOWSETTINGACTION:Ljava/lang/String; = "android.intent.action.SAFETY_SHOW_SETTING"

.field private static final TAG:Ljava/lang/String; = "SafetyAssuranceReceiver"

.field public static final TRIGGERACTION:Ljava/lang/String; = "android.intent.action.SAFETY_MESSAGE_TRIGGER"

.field public static final TRIGGERACTION_WITH_MEIDA:Ljava/lang/String; = "android.intent.action.SAFETY_MESSAGE_TRIGGER_WITH_MEDIA"

.field static mSplashDialog:Landroid/app/AlertDialog;


# instance fields
.field private final VIBRATOR_SLEEP_TIME:I

.field private final VIBRATOR_TIME:I

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mSplashDialog:Landroid/app/AlertDialog;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 46
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->VIBRATOR_TIME:I

    .line 47
    const/16 v0, 0x82

    iput v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->VIBRATOR_SLEEP_TIME:I

    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private checkEmergencyContactIsEmpty()Z
    .locals 4

    .prologue
    .line 309
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->queryEmergencyContactSize(Landroid/content/Context;)I

    move-result v0

    .line 310
    .local v0, "nSize":I
    const-string v1, "SafetyAssuranceReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "checkEmergencyContactIsEmpty : EmergencyContactSize is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    if-gtz v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private checkUSIMAbsent()Z
    .locals 3

    .prologue
    .line 315
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->isUSIMAbsent()Z

    move-result v0

    .line 317
    .local v0, "result":Z
    if-eqz v0, :cond_0

    .line 318
    const-string v1, "SafetyAssuranceReceiver"

    const-string v2, "checkUSIMAbsent : USIM is absent"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    :cond_0
    return v0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 18
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 66
    const-string v13, "SafetyAssuranceReceiver"

    const-string v14, "onReceive"

    invoke-static {v13, v14}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    const/4 v13, 0x1

    move-object/from16 v0, p1

    invoke-static {v0, v13}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceApp;->setWakeLock(Landroid/content/Context;Z)V

    .line 68
    if-nez p2, :cond_1

    .line 69
    const-string v13, "SafetyAssuranceReceiver"

    const-string v14, "onReceive - intent is null"

    invoke-static {v13, v14}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v13}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceApp;->setWakeLock(Landroid/content/Context;Z)V

    .line 306
    :cond_0
    :goto_0
    return-void

    .line 74
    :cond_1
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    .line 76
    .local v2, "action":Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->getAppState(Landroid/content/Context;)I

    move-result v7

    .line 77
    .local v7, "nCurrentAppState":I
    const-string v13, "SafetyAssuranceReceiver"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "onReceive - action:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", currentAppState:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    .line 81
    const-string v13, "android.intent.action.SAFETY_MESSAGE_TRIGGER"

    invoke-virtual {v2, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_2

    const-string v13, "android.intent.action.SAFETY_MESSAGE_TRIGGER_WITH_MEDIA"

    invoke-virtual {v2, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_c

    .line 83
    :cond_2
    const/4 v13, 0x1

    if-eq v7, v13, :cond_3

    .line 84
    const-string v13, "SafetyAssuranceReceiver"

    const-string v14, "App state is already ON. Ignore trigger event"

    invoke-static {v13, v14}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    invoke-static {v7}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->isWorkingAppState(I)Z

    move-result v13

    if-nez v13, :cond_0

    .line 86
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    const/4 v14, 0x0

    invoke-static {v13, v14}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceApp;->setWakeLock(Landroid/content/Context;Z)V

    goto :goto_0

    .line 90
    :cond_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    const/4 v14, 0x2

    invoke-static {v13, v14}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->setAppState(Landroid/content/Context;I)V

    .line 93
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->checkUSIMAbsent()Z

    move-result v13

    if-eqz v13, :cond_5

    .line 94
    const-string v13, "SafetyAssuranceReceiver"

    const-string v14, "USIM is absent"

    invoke-static {v13, v14}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    const-string v13, "ro.csc.sales_code"

    invoke-static {v13}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 96
    .local v10, "salesCode":Ljava/lang/String;
    const-string v13, "CTC"

    invoke-virtual {v13, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 97
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    const v14, 0x7f090033

    const/4 v15, 0x1

    invoke-static {v13, v14, v15}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v13

    invoke-virtual {v13}, Landroid/widget/Toast;->show()V

    .line 101
    :goto_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    const/4 v14, 0x1

    invoke-static {v13, v14}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->setAppState(Landroid/content/Context;I)V

    .line 102
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    const/4 v14, 0x0

    invoke-static {v13, v14}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceApp;->setWakeLock(Landroid/content/Context;Z)V

    goto/16 :goto_0

    .line 99
    :cond_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    const v14, 0x7f090032

    const/4 v15, 0x1

    invoke-static {v13, v14, v15}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v13

    invoke-virtual {v13}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 107
    .end local v10    # "salesCode":Ljava/lang/String;
    :cond_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    const-string v14, "send_emergency_message"

    const/4 v15, 0x0

    invoke-static {v13, v14, v15}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v13

    if-nez v13, :cond_6

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    const-string v14, "send_b_emergency_message"

    const/4 v15, 0x0

    invoke-static {v13, v14, v15}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v13

    if-nez v13, :cond_6

    .line 109
    const-string v13, "SafetyAssuranceReceiver"

    const-string v14, "SafeyAssurance is not activated"

    invoke-static {v13, v14}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    const/4 v14, 0x1

    invoke-static {v13, v14}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->setAppState(Landroid/content/Context;I)V

    .line 111
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    const/4 v14, 0x0

    invoke-static {v13, v14}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceApp;->setWakeLock(Landroid/content/Context;Z)V

    goto/16 :goto_0

    .line 115
    :cond_6
    const-string v13, "android.intent.action.SAFETY_MESSAGE_TRIGGER_WITH_MEDIA"

    invoke-virtual {v2, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_7

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    const-string v14, "send_b_emergency_message"

    const/4 v15, 0x0

    invoke-static {v13, v14, v15}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v13

    if-nez v13, :cond_7

    .line 117
    const-string v13, "SafetyAssuranceReceiver"

    const-string v14, "TRIGGERACTION_WITH_MEIDA with SOS message off, just bail"

    invoke-static {v13, v14}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    const/4 v14, 0x1

    invoke-static {v13, v14}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->setAppState(Landroid/content/Context;I)V

    .line 119
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    const/4 v14, 0x0

    invoke-static {v13, v14}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceApp;->setWakeLock(Landroid/content/Context;Z)V

    goto/16 :goto_0

    .line 124
    :cond_7
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->checkEmergencyContactIsEmpty()Z

    move-result v13

    if-eqz v13, :cond_8

    .line 125
    const-string v13, "SafetyAssuranceReceiver"

    const-string v14, "Emergency Contact is empty. Ignore trigger event"

    invoke-static {v13, v14}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    const/4 v14, 0x1

    invoke-static {v13, v14}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->setAppState(Landroid/content/Context;I)V

    .line 127
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    const/4 v14, 0x0

    invoke-static {v13, v14}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceApp;->setWakeLock(Landroid/content/Context;Z)V

    goto/16 :goto_0

    .line 132
    :cond_8
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->featureSupportNewUX()Z

    move-result v13

    if-eqz v13, :cond_9

    .line 133
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceApp;->getApplicationHandler()Landroid/os/Handler;

    move-result-object v13

    new-instance v14, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver$1;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver$1;-><init>(Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;)V

    invoke-virtual {v13, v14}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 163
    :cond_9
    new-instance v11, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    const-class v14, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    invoke-direct {v11, v13, v14}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 164
    .local v11, "startService":Landroid/content/Intent;
    const-string v13, "android.intent.action.SAFETY_SERVICE_START"

    invoke-virtual {v11, v13}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 165
    const-string v13, "android.intent.action.SAFETY_MESSAGE_TRIGGER_WITH_MEDIA"

    invoke-virtual {v2, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_a

    .line 166
    const-string v13, "SafetyAssuranceReceiver"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "image file uri : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "data"

    move-object/from16 v0, p2

    invoke-virtual {v0, v15}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    const-string v13, "data"

    const-string v14, "data"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v11, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 168
    const-string v13, "sos_mode"

    const/4 v14, 0x1

    invoke-virtual {v11, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 170
    :cond_a
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v13, v11}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 172
    const-string v13, "voice"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 173
    .local v12, "voiceStr":Ljava/lang/String;
    const-string v13, "SafetyAssuranceReceiver"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "voice file uri : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "voice"

    move-object/from16 v0, p2

    invoke-virtual {v0, v15}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    const-string v13, "android.intent.action.SAFETY_MESSAGE_TRIGGER_WITH_MEDIA"

    invoke-virtual {v2, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_b

    if-eqz v12, :cond_b

    .line 175
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceApp;->getApplicationHandler()Landroid/os/Handler;

    move-result-object v13

    new-instance v14, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver$2;

    move-object/from16 v0, p0

    invoke-direct {v14, v0, v12}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver$2;-><init>(Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;Ljava/lang/String;)V

    const-wide/16 v16, 0x1770

    move-wide/from16 v0, v16

    invoke-virtual {v13, v14, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 189
    :cond_b
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceApp;->getApplicationHandler()Landroid/os/Handler;

    move-result-object v13

    new-instance v14, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver$3;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver$3;-><init>(Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;)V

    const-wide/16 v16, 0x1388

    move-wide/from16 v0, v16

    invoke-virtual {v13, v14, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 200
    .end local v11    # "startService":Landroid/content/Intent;
    .end local v12    # "voiceStr":Ljava/lang/String;
    :cond_c
    const-string v13, "android.intent.action.SAFETY_MESSAGE_ALARM_ACTION"

    invoke-virtual {v2, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_e

    .line 202
    const/4 v13, 0x1

    if-ge v13, v7, :cond_d

    .line 204
    new-instance v11, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    const-class v14, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    invoke-direct {v11, v13, v14}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 205
    .restart local v11    # "startService":Landroid/content/Intent;
    const-string v13, "android.intent.action.SAFETY_SERVICE_ALARM"

    invoke-virtual {v11, v13}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 206
    const-string v13, "SA_FIRSTALARM"

    const/4 v14, 0x0

    invoke-virtual {v11, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 207
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v13, v11}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_0

    .line 209
    .end local v11    # "startService":Landroid/content/Intent;
    :cond_d
    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v13}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceApp;->setWakeLock(Landroid/content/Context;Z)V

    goto/16 :goto_0

    .line 211
    :cond_e
    const-string v13, "android.intent.action.SAFETY_MESSAGE_B_ALARM_ACTION"

    invoke-virtual {v2, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_10

    .line 212
    const/4 v13, 0x1

    if-ge v13, v7, :cond_f

    .line 214
    new-instance v11, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    const-class v14, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    invoke-direct {v11, v13, v14}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 215
    .restart local v11    # "startService":Landroid/content/Intent;
    const-string v13, "android.intent.action.SAFETY_SERVICE_B_ALARM"

    invoke-virtual {v11, v13}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 216
    const-string v13, "SA_FIRSTALARM"

    const/4 v14, 0x0

    invoke-virtual {v11, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 217
    const-string v13, "sos_mode"

    const/4 v14, 0x1

    invoke-virtual {v11, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 218
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v13, v11}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_0

    .line 220
    .end local v11    # "startService":Landroid/content/Intent;
    :cond_f
    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v13}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceApp;->setWakeLock(Landroid/content/Context;Z)V

    goto/16 :goto_0

    .line 222
    :cond_10
    const-string v13, "android.intent.action.QUIT_EMERGENCY_SITUATION"

    invoke-virtual {v2, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_12

    .line 224
    const/4 v13, 0x1

    if-ge v13, v7, :cond_11

    .line 225
    new-instance v11, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    const-class v14, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    invoke-direct {v11, v13, v14}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 226
    .restart local v11    # "startService":Landroid/content/Intent;
    const-string v13, "android.intent.action.SAFETY_SERVICE_SHOW_STOPDIALOG"

    invoke-virtual {v11, v13}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 227
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v13, v11}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_0

    .line 229
    .end local v11    # "startService":Landroid/content/Intent;
    :cond_11
    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v13}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceApp;->setWakeLock(Landroid/content/Context;Z)V

    goto/16 :goto_0

    .line 231
    :cond_12
    const-string v13, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v2, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_14

    .line 233
    const-string v13, "SafetyAssuranceReceiver"

    const-string v14, "Init App state"

    invoke-static {v13, v14}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    const/4 v14, 0x1

    invoke-static {v13, v14}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->setAppState(Landroid/content/Context;I)V

    .line 236
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    invoke-static {v13}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceApp;->ifForegroundActivityIsSASettingThenItIsResumed(Landroid/content/Context;)V

    .line 238
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    const-string v14, "send_emergency_message"

    const/4 v15, 0x0

    invoke-static {v13, v14, v15}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v13

    if-eqz v13, :cond_13

    .line 239
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    const/4 v14, 0x1

    invoke-static {v13, v14}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->setWakeupKey(Landroid/content/Context;Z)V

    .line 241
    :cond_13
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    const/4 v14, 0x0

    invoke-static {v13, v14}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceApp;->setWakeLock(Landroid/content/Context;Z)V

    goto/16 :goto_0

    .line 243
    :cond_14
    const-string v13, "android.intent.action.QUIT_EMERGENCY_SITUATION_FORCESTOP"

    invoke-virtual {v2, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_16

    .line 244
    const/4 v13, 0x1

    if-ge v13, v7, :cond_15

    .line 245
    new-instance v11, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    const-class v14, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    invoke-direct {v11, v13, v14}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 246
    .restart local v11    # "startService":Landroid/content/Intent;
    const-string v13, "android.intent.action.SAFETY_SERVICE_FORCESTOP"

    invoke-virtual {v11, v13}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 247
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v13, v11}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_0

    .line 249
    .end local v11    # "startService":Landroid/content/Intent;
    :cond_15
    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v13}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceApp;->setWakeLock(Landroid/content/Context;Z)V

    goto/16 :goto_0

    .line 251
    :cond_16
    const-string v13, "android.intent.action.SAFETY_SHOW_SETTING"

    invoke-virtual {v2, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_17

    .line 252
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    invoke-static {v13}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceApp;->ifForegroundActivityIsSASettingThenDontStartItAgain(Landroid/content/Context;)V

    .line 253
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    const/4 v14, 0x0

    invoke-static {v13, v14}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceApp;->setWakeLock(Landroid/content/Context;Z)V

    goto/16 :goto_0

    .line 256
    :cond_17
    const-string v13, "com.sec.android.app.safetyassurance.SENT_MESSAGE"

    invoke-virtual {v2, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 257
    const-string v13, "recipient"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 258
    .local v8, "recipient":Ljava/lang/String;
    const-string v13, "message"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 259
    .local v6, "message":Ljava/lang/String;
    const-string v13, "requestCode"

    const/4 v14, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v14}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v9

    .line 260
    .local v9, "requestCode":I
    const-string v13, "lastsent"

    const/4 v14, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v14}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    .line 261
    .local v5, "lastSent":Z
    const-string v13, "failed"

    const/4 v14, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v14}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    .line 262
    .local v4, "lastFailed":Z
    const-string v13, "smsException"

    const/4 v14, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v14}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 263
    .local v3, "isSMSException":Z
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->getResultCode()I

    move-result v13

    const/4 v14, -0x1

    if-ne v13, v14, :cond_1b

    .line 264
    if-eqz v3, :cond_19

    .line 265
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    const/4 v14, 0x0

    invoke-static {v13, v8, v6, v14}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->saveEmergencyLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    .line 266
    const/4 v13, 0x1

    if-ge v13, v7, :cond_18

    .line 267
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    invoke-static {v13}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceApp;->ifForegroundActivityIsSALogThenItIsRefresh(Landroid/content/Context;)V

    .line 268
    new-instance v11, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    const-class v14, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    invoke-direct {v11, v13, v14}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 269
    .restart local v11    # "startService":Landroid/content/Intent;
    const-string v13, "android.intent.action.SAFETY_SERVICE_SENDMSG_COMPLETE"

    invoke-virtual {v11, v13}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 270
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v13, v11}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_0

    .line 272
    .end local v11    # "startService":Landroid/content/Intent;
    :cond_18
    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v13}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceApp;->setWakeLock(Landroid/content/Context;Z)V

    goto/16 :goto_0

    .line 275
    :cond_19
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    const/4 v14, 0x1

    invoke-static {v13, v8, v6, v14}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->saveEmergencyLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    .line 276
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    invoke-static {v13}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceApp;->ifForegroundActivityIsSALogThenItIsRefresh(Landroid/content/Context;)V

    .line 277
    if-eqz v5, :cond_1a

    .line 278
    new-instance v11, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    const-class v14, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    invoke-direct {v11, v13, v14}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 279
    .restart local v11    # "startService":Landroid/content/Intent;
    const-string v13, "android.intent.action.SAFETY_SERVICE_SENDMSG_COMPLETE"

    invoke-virtual {v11, v13}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 280
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v13, v11}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 282
    .end local v11    # "startService":Landroid/content/Intent;
    :cond_1a
    const-string v13, "SafetyAssuranceReceiver"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "onReceive - action: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " recipient = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " message = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 285
    :cond_1b
    if-eqz v4, :cond_1d

    .line 286
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    const/4 v14, 0x0

    invoke-static {v13, v8, v6, v14}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->saveEmergencyLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    .line 287
    const/4 v13, 0x1

    if-ge v13, v7, :cond_1c

    .line 288
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    invoke-static {v13}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceApp;->ifForegroundActivityIsSALogThenItIsRefresh(Landroid/content/Context;)V

    .line 289
    new-instance v11, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    const-class v14, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    invoke-direct {v11, v13, v14}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 290
    .restart local v11    # "startService":Landroid/content/Intent;
    const-string v13, "android.intent.action.SAFETY_SERVICE_SENDMSG_COMPLETE"

    invoke-virtual {v11, v13}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 291
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v13, v11}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 303
    .end local v11    # "startService":Landroid/content/Intent;
    :goto_2
    const-string v13, "SafetyAssuranceReceiver"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "resultCode = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->getResultCode()I

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 293
    :cond_1c
    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v13}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceApp;->setWakeLock(Landroid/content/Context;Z)V

    goto :goto_2

    .line 296
    :cond_1d
    new-instance v11, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    const-class v14, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    invoke-direct {v11, v13, v14}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 297
    .restart local v11    # "startService":Landroid/content/Intent;
    const-string v13, "android.intent.action.SAFETY_SERVICE_RESENDMSG"

    invoke-virtual {v11, v13}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 298
    const-string v13, "recipient"

    invoke-virtual {v11, v13, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 299
    const-string v13, "message"

    invoke-virtual {v11, v13, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 300
    const-string v13, "requestCode"

    invoke-virtual {v11, v13, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 301
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v13, v11}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_2
.end method
