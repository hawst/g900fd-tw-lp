.class Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$1;
.super Ljava/lang/Object;
.source "SafetyAssuranceFragment.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->onStart()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

.field final synthetic val$swImCnt00:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;

.field final synthetic val$swImCnt01:Landroid/widget/ImageView;

.field final synthetic val$swNavi01:Landroid/widget/ImageView;

.field final synthetic val$swNavi02:Landroid/widget/ImageView;

.field final synthetic val$swNavi03:Landroid/widget/ImageView;

.field final synthetic val$swTv:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;Landroid/widget/ImageView;Landroid/widget/TextView;Landroid/widget/ImageView;Landroid/widget/ImageView;Landroid/widget/ImageView;)V
    .locals 0

    .prologue
    .line 455
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$1;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    iput-object p2, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$1;->val$swImCnt00:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;

    iput-object p3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$1;->val$swImCnt01:Landroid/widget/ImageView;

    iput-object p4, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$1;->val$swTv:Landroid/widget/TextView;

    iput-object p5, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$1;->val$swNavi01:Landroid/widget/ImageView;

    iput-object p6, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$1;->val$swNavi02:Landroid/widget/ImageView;

    iput-object p7, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$1;->val$swNavi03:Landroid/widget/ImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 11
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v10, 0x1

    .line 458
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v7

    .line 460
    .local v7, "action":I
    if-nez v7, :cond_0

    .line 461
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$1;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    # setter for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->xAtDown:F
    invoke-static {v0, v1}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->access$002(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;F)F

    .line 462
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$1;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    # setter for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->yAtDown:F
    invoke-static {v0, v1}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->access$102(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;F)F

    .line 463
    const-string v0, "SafetyAssuranceFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onTouch DOWN x["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$1;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    # getter for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->xAtDown:F
    invoke-static {v2}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->access$000(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] y["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$1;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    # getter for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->yAtDown:F
    invoke-static {v2}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->access$100(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v10

    .line 499
    :goto_0
    return v0

    .line 466
    :cond_0
    if-eq v7, v10, :cond_1

    const/4 v0, 0x3

    if-ne v7, v0, :cond_6

    .line 467
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$1;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    # setter for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->xAtUp:F
    invoke-static {v0, v1}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->access$202(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;F)F

    .line 468
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$1;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    # setter for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->yAtUp:F
    invoke-static {v0, v1}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->access$302(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;F)F

    .line 469
    const-string v0, "SafetyAssuranceFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onTouch UPnCANCEL x["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$1;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    # getter for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->xAtUp:F
    invoke-static {v2}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->access$200(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] y["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$1;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    # getter for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->yAtUp:F
    invoke-static {v2}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->access$300(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 471
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$1;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    # getter for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->xAtDown:F
    invoke-static {v0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->access$000(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$1;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    # getter for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->xAtUp:F
    invoke-static {v1}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->access$200(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)F

    move-result v1

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v8

    .line 472
    .local v8, "xDelta":F
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$1;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    # getter for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->yAtDown:F
    invoke-static {v0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->access$100(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$1;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    # getter for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->yAtUp:F
    invoke-static {v1}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->access$300(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)F

    move-result v1

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v9

    .line 473
    .local v9, "yDelta":F
    const-string v0, "SafetyAssuranceFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onTouch Delta x["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] y["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 475
    cmpl-float v0, v9, v8

    if-gtz v0, :cond_2

    const/high16 v0, 0x41200000    # 10.0f

    cmpl-float v0, v0, v8

    if-lez v0, :cond_3

    :cond_2
    move v0, v10

    .line 476
    goto/16 :goto_0

    .line 479
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$1;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    # getter for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->xAtUp:F
    invoke-static {v0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->access$200(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$1;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    # getter for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->xAtDown:F
    invoke-static {v1}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->access$000(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_5

    .line 481
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$1;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    # getter for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->currentPage:I
    invoke-static {v0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->access$400(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$1;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    # getter for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->max_helpimage_page:I
    invoke-static {v1}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->access$500(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)I

    move-result v1

    if-ge v0, v1, :cond_4

    .line 482
    const-string v0, "SafetyAssuranceFragment"

    const-string v1, "onTouch Go Next page"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 483
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$1;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    # operator++ for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->currentPage:I
    invoke-static {v0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->access$408(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)I

    .line 484
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$1;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$1;->val$swImCnt00:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;

    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$1;->val$swImCnt01:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$1;->val$swTv:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$1;->val$swNavi01:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$1;->val$swNavi02:Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$1;->val$swNavi03:Landroid/widget/ImageView;

    # invokes: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->updateContent(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;Landroid/widget/ImageView;Landroid/widget/TextView;Landroid/widget/ImageView;Landroid/widget/ImageView;Landroid/widget/ImageView;)V
    invoke-static/range {v0 .. v6}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->access$600(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;Landroid/widget/ImageView;Landroid/widget/TextView;Landroid/widget/ImageView;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    :cond_4
    :goto_1
    move v0, v10

    .line 496
    goto/16 :goto_0

    .line 489
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$1;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    # getter for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->currentPage:I
    invoke-static {v0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->access$400(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)I

    move-result v0

    if-lez v0, :cond_4

    .line 490
    const-string v0, "SafetyAssuranceFragment"

    const-string v1, "onTouch Go prev page"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 491
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$1;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    # operator-- for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->currentPage:I
    invoke-static {v0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->access$410(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)I

    .line 492
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$1;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$1;->val$swImCnt00:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;

    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$1;->val$swImCnt01:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$1;->val$swTv:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$1;->val$swNavi01:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$1;->val$swNavi02:Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$1;->val$swNavi03:Landroid/widget/ImageView;

    # invokes: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->updateContent(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;Landroid/widget/ImageView;Landroid/widget/TextView;Landroid/widget/ImageView;Landroid/widget/ImageView;Landroid/widget/ImageView;)V
    invoke-static/range {v0 .. v6}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->access$600(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;Landroid/widget/ImageView;Landroid/widget/TextView;Landroid/widget/ImageView;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    goto :goto_1

    .line 499
    .end local v8    # "xDelta":F
    .end local v9    # "yDelta":F
    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_0
.end method
