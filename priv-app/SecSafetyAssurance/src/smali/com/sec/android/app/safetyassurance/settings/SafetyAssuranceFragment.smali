.class public Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;
.super Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceGeneralPreferenceFragment;
.source "SafetyAssuranceFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$SafetyAssuranceFragmentReceiver;
    }
.end annotation


# static fields
.field public static final ASSISTANCE_LAUNCH_PREF:Ljava/lang/String; = "FirstLaunchPreference"

.field public static final AUTOMATIC_SENDING_INTERVAL:Ljava/lang/String; = "automatic_sending_interval"

.field public static final BLOCK_EMERGENCY_MESSAGE:Ljava/lang/String; = "block_emergency_message"

.field public static final DISABLE_CAMERA:Ljava/lang/String; = "disable_camera"

.field public static final EMERGENCY_MESSAGE:Ljava/lang/String; = "the_string_of_emergency_message"

.field private static final FROM_B:I = 0x1

.field private static final FROM_SETTING:I = 0x0

.field public static final SAFRAGMENT_RESUME:Ljava/lang/String; = "com.sec.android.app.safetyassurance.action.SAFRAGMENT_RESUME"

.field public static final SEND_B_EMERGENCY_MESSAGE:Ljava/lang/String; = "send_b_emergency_message"

.field public static final SEND_B_OPTION_INFO:Ljava/lang/String; = "com.sec.android.app.safetyassurance.settings.SEND_B_OPTION_INFO"

.field public static final SEND_DUAL_CAPTURED_IMAGE:Ljava/lang/String; = "send_dual_captured_image"

.field public static final SEND_EMERGENCY_MESSAGE:Ljava/lang/String; = "send_emergency_message"

.field public static final SEND_FROM_B:Ljava/lang/String; = "sendFromB"

.field public static final SEND_MY_LOCATION:Ljava/lang/String; = "send_my_location"

.field public static final SEND_VOCREC:Ljava/lang/String; = "send_vocrec"

.field private static final SHOW_NO_EMERGENCY_CONTACT_DIALOG:I = 0x0

.field public static final SOS_MODE:Ljava/lang/String; = "sos_mode"

.field public static final TAG:Ljava/lang/String; = "SafetyAssuranceFragment"

.field public static final USER_AGREE_TO_USE_LOCATION_SERVICE:Ljava/lang/String; = "user_agree_to_use_location_service"

.field public static final VOC_RECORDING_TIME:Ljava/lang/String; = "voc_recording_time"


# instance fields
.field private final KEY_PRESS_TIME:I

.field private final MIN_DISTANCE:F

.field final REQUESTCODE_ADD_EMERGENCY_CONTACT_ACTIVITY:I

.field final REQUESTCODE_SET_EMERGENCY_MESSAGE_ACTIVITY:I

.field final REQUESTCODE_SET_RECORDING_TIME_ACTIVITY:I

.field private av:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;

.field private check_no_emcontact_onCreate:Z

.field private currentPage:I

.field private final entryValues:[Ljava/lang/CharSequence;

.field final handler:Landroid/os/Handler;

.field private hide_page:Z

.field private isATT:Z

.field private isVZW:Z

.field private layoutSwIm:Landroid/widget/LinearLayout;

.field private mActionBarLayout:Landroid/view/View;

.field private mActionBarSwitch:Landroid/widget/Switch;

.field private mCompleteHandler:Landroid/os/Handler;

.field private mContext:Landroid/content/Context;

.field private mDualCapturedCheckBox:Landroid/preference/CheckBoxPreference;

.field private mEditingEmergencyMessage:Landroid/preference/Preference;

.field private mEmergencyContact:Landroid/preference/Preference;

.field private mEmergencyMessageLog:Landroid/preference/Preference;

.field private mReceiver:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$SafetyAssuranceFragmentReceiver;

.field private mRemoveCameraOption:Z

.field private mRemoveCameraOptionForGear:Z

.field private mRemoveVoiceRecordingOption:Z

.field private mSendingTimerList:Landroid/preference/ListPreference;

.field private mSoundRecordingPreference:Landroid/preference/CheckBoxPreference;

.field private mStartFrom:I

.field private max_helpimage_page:I

.field private only_send_recording:Z

.field private xAtDown:F

.field private xAtUp:F

.field private yAtDown:F

.field private yAtUp:F


# direct methods
.method public constructor <init>()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 64
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceGeneralPreferenceFragment;-><init>()V

    .line 71
    iput-object v2, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mReceiver:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$SafetyAssuranceFragmentReceiver;

    .line 74
    iput-object v2, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mEditingEmergencyMessage:Landroid/preference/Preference;

    .line 76
    iput-object v2, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mEmergencyMessageLog:Landroid/preference/Preference;

    .line 77
    iput-object v2, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mDualCapturedCheckBox:Landroid/preference/CheckBoxPreference;

    .line 78
    iput-object v2, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mSendingTimerList:Landroid/preference/ListPreference;

    .line 81
    iput-object v2, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mEmergencyContact:Landroid/preference/Preference;

    .line 82
    iput-object v2, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mSoundRecordingPreference:Landroid/preference/CheckBoxPreference;

    .line 86
    iput-object v2, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mContext:Landroid/content/Context;

    .line 105
    iput v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->REQUESTCODE_SET_EMERGENCY_MESSAGE_ACTIVITY:I

    .line 106
    iput v6, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->REQUESTCODE_SET_RECORDING_TIME_ACTIVITY:I

    .line 107
    iput v5, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->REQUESTCODE_ADD_EMERGENCY_CONTACT_ACTIVITY:I

    .line 109
    iput-object v2, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mActionBarSwitch:Landroid/widget/Switch;

    .line 110
    iput-object v2, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mActionBarLayout:Landroid/view/View;

    .line 115
    iput v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mStartFrom:I

    .line 119
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->handler:Landroid/os/Handler;

    .line 120
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/CharSequence;

    const-string v1, "0"

    aput-object v1, v0, v3

    const-string v1, "10"

    aput-object v1, v0, v6

    const-string v1, "20"

    aput-object v1, v0, v5

    iput-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->entryValues:[Ljava/lang/CharSequence;

    .line 122
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->KEY_PRESS_TIME:I

    .line 123
    iput v4, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->xAtDown:F

    .line 124
    iput v4, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->xAtUp:F

    .line 125
    iput v4, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->yAtDown:F

    .line 126
    iput v4, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->yAtUp:F

    .line 127
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->MIN_DISTANCE:F

    .line 129
    iput-object v2, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->layoutSwIm:Landroid/widget/LinearLayout;

    .line 130
    iput-object v2, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->av:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;

    .line 131
    iput v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->currentPage:I

    .line 133
    iput v5, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->max_helpimage_page:I

    .line 134
    iput-boolean v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->hide_page:Z

    .line 135
    iput-boolean v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->only_send_recording:Z

    .line 137
    iput-boolean v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->check_no_emcontact_onCreate:Z

    .line 138
    iput-boolean v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->isVZW:Z

    .line 139
    iput-boolean v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->isATT:Z

    .line 889
    new-instance v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$5;-><init>(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mCompleteHandler:Landroid/os/Handler;

    .line 950
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    .prologue
    .line 64
    iget v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->xAtDown:F

    return v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;
    .param p1, "x1"    # F

    .prologue
    .line 64
    iput p1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->xAtDown:F

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    .prologue
    .line 64
    iget v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->yAtDown:F

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->prepareActivate()V

    return-void
.end method

.method static synthetic access$102(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;
    .param p1, "x1"    # F

    .prologue
    .line 64
    iput p1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->yAtDown:F

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)Landroid/preference/ListPreference;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mSendingTimerList:Landroid/preference/ListPreference;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mRemoveCameraOption:Z

    return v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)Landroid/preference/CheckBoxPreference;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mDualCapturedCheckBox:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mRemoveVoiceRecordingOption:Z

    return v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)Landroid/preference/CheckBoxPreference;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mSoundRecordingPreference:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->displayToast(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1700(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->sendBroadcastToWatch(Z)V

    return-void
.end method

.method static synthetic access$1800(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->discardActivate()V

    return-void
.end method

.method static synthetic access$1900(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->isVZW:Z

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    .prologue
    .line 64
    iget v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->xAtUp:F

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;
    .param p1, "x1"    # F

    .prologue
    .line 64
    iput p1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->xAtUp:F

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    .prologue
    .line 64
    iget v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->yAtUp:F

    return v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;
    .param p1, "x1"    # F

    .prologue
    .line 64
    iput p1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->yAtUp:F

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    .prologue
    .line 64
    iget v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->currentPage:I

    return v0
.end method

.method static synthetic access$408(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    .prologue
    .line 64
    iget v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->currentPage:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->currentPage:I

    return v0
.end method

.method static synthetic access$410(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    .prologue
    .line 64
    iget v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->currentPage:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->currentPage:I

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    .prologue
    .line 64
    iget v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->max_helpimage_page:I

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;Landroid/widget/ImageView;Landroid/widget/TextView;Landroid/widget/ImageView;Landroid/widget/ImageView;Landroid/widget/ImageView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;
    .param p1, "x1"    # Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;
    .param p2, "x2"    # Landroid/widget/ImageView;
    .param p3, "x3"    # Landroid/widget/TextView;
    .param p4, "x4"    # Landroid/widget/ImageView;
    .param p5, "x5"    # Landroid/widget/ImageView;
    .param p6, "x6"    # Landroid/widget/ImageView;

    .prologue
    .line 64
    invoke-direct/range {p0 .. p6}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->updateContent(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;Landroid/widget/ImageView;Landroid/widget/TextView;Landroid/widget/ImageView;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mCompleteHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    .prologue
    .line 64
    iget v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mStartFrom:I

    return v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private changeTextOfItervalTimeToSendMessage(Ljava/lang/Object;)V
    .locals 6
    .param p1, "newValue"    # Ljava/lang/Object;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 151
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 154
    .local v0, "intervalAndDescription":Ljava/lang/StringBuilder;
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mSendingTimerList:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v1

    .local v1, "intervalTimeStr":[Ljava/lang/CharSequence;
    move-object v2, p1

    .line 156
    check-cast v2, Ljava/lang/CharSequence;

    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->entryValues:[Ljava/lang/CharSequence;

    aget-object v3, v3, v4

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 157
    aget-object v2, v1, v4

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 164
    .end local p1    # "newValue":Ljava/lang/Object;
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mSendingTimerList:Landroid/preference/ListPreference;

    invoke-virtual {v2, v0}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 165
    return-void

    .line 158
    .restart local p1    # "newValue":Ljava/lang/Object;
    :cond_0
    check-cast p1, Ljava/lang/CharSequence;

    .end local p1    # "newValue":Ljava/lang/Object;
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->entryValues:[Ljava/lang/CharSequence;

    aget-object v2, v2, v5

    invoke-static {p1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 159
    aget-object v2, v1, v5

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 161
    :cond_1
    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private changeTextOfRecordingTime()V
    .locals 6

    .prologue
    .line 168
    iget-boolean v2, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mRemoveVoiceRecordingOption:Z

    if-nez v2, :cond_0

    .line 169
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->featureSupportNewUX()Z

    move-result v2

    if-nez v2, :cond_1

    .line 170
    const/4 v0, 0x5

    .line 171
    .local v0, "recordingSeconds":I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 172
    .local v1, "recordingTimeAndDescription":Ljava/lang/StringBuilder;
    const v2, 0x7f090037

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 176
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mSoundRecordingPreference:Landroid/preference/CheckBoxPreference;

    if-eqz v2, :cond_0

    .line 177
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mSoundRecordingPreference:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, v1}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 189
    .end local v0    # "recordingSeconds":I
    .end local v1    # "recordingTimeAndDescription":Ljava/lang/StringBuilder;
    :cond_0
    :goto_0
    return-void

    .line 180
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mSoundRecordingPreference:Landroid/preference/CheckBoxPreference;

    if-eqz v2, :cond_0

    .line 181
    iget-boolean v2, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->isATT:Z

    if-eqz v2, :cond_2

    .line 182
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mSoundRecordingPreference:Landroid/preference/CheckBoxPreference;

    const v3, 0x7f09004b

    invoke-virtual {v2, v3}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    goto :goto_0

    .line 185
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mSoundRecordingPreference:Landroid/preference/CheckBoxPreference;

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private discardActivate()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 812
    iget-boolean v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mRemoveCameraOption:Z

    if-nez v0, :cond_0

    .line 813
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mDualCapturedCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 815
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mRemoveVoiceRecordingOption:Z

    if-nez v0, :cond_1

    .line 816
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mSoundRecordingPreference:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 818
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mSendingTimerList:Landroid/preference/ListPreference;

    if-eqz v0, :cond_2

    .line 819
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mSendingTimerList:Landroid/preference/ListPreference;

    invoke-virtual {v0, v2}, Landroid/preference/ListPreference;->setEnabled(Z)V

    .line 822
    :cond_2
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->featureSupportNewUX()Z

    move-result v0

    if-nez v0, :cond_4

    .line 823
    const v0, 0x7f090001

    invoke-virtual {p0, v0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->displayToast(Ljava/lang/String;)V

    .line 829
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v0, v2}, Landroid/widget/Switch;->setChecked(Z)V

    .line 831
    iget v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mStartFrom:I

    if-nez v0, :cond_5

    .line 832
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "send_emergency_message"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 837
    :cond_3
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "user_agree_to_use_location_service"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 838
    return-void

    .line 826
    :cond_4
    const-string v0, "SafetyAssuranceFragment"

    const-string v1, "discardActivate : NEWUX"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 833
    :cond_5
    iget v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mStartFrom:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 834
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "send_b_emergency_message"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 835
    invoke-direct {p0, v2}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->sendBroadcastToWatch(Z)V

    goto :goto_1
.end method

.method private displayToast(Ljava/lang/String;)V
    .locals 2
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 947
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 948
    return-void
.end method

.method private getPreferenceOrder(Ljava/lang/String;)I
    .locals 5
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 961
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v3

    .line 962
    .local v3, "screen":Landroid/preference/PreferenceScreen;
    invoke-virtual {v3}, Landroid/preference/PreferenceScreen;->getRootAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    .line 964
    .local v1, "listAdapter":Landroid/widget/ListAdapter;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v4

    if-ge v0, v4, :cond_1

    .line 965
    invoke-interface {v1, v0}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/preference/Preference;

    invoke-virtual {v4}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    .line 966
    .local v2, "prefKey":Ljava/lang/String;
    if-eqz v2, :cond_0

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 970
    .end local v0    # "i":I
    .end local v2    # "prefKey":Ljava/lang/String;
    :goto_1
    return v0

    .line 964
    .restart local v0    # "i":I
    .restart local v2    # "prefKey":Ljava/lang/String;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 970
    .end local v2    # "prefKey":Ljava/lang/String;
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private isCameraDisabled()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 993
    iget-boolean v1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mRemoveCameraOption:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mRemoveCameraOptionForGear:Z

    if-eqz v1, :cond_1

    .line 1000
    :cond_0
    :goto_0
    return v0

    .line 997
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mDualCapturedCheckBox:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mDualCapturedCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 998
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private launchFromSettingSearch(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 354
    const/4 v4, 0x0

    .line 355
    .local v4, "openSubMenuKey":Ljava/lang/String;
    const/4 v3, 0x0

    .line 356
    .local v3, "mCheckValue":Z
    const-string v7, "extra_from_search"

    invoke-virtual {p1, v7, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 358
    .local v0, "bOpenSubMenu":Z
    if-eqz v0, :cond_0

    .line 359
    const-string v7, "extra_parent_preference_key"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 360
    const-string v7, "extra_setting_value"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    if-ne v7, v5, :cond_2

    move v3, v5

    .line 361
    :goto_0
    const-string v7, "SafetyAssuranceFragment"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "mOpenDetailMenuKey = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", mCheckValue = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    :cond_0
    if-eqz v4, :cond_1

    .line 365
    const-string v7, "automatic_sending_interval_key"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 366
    const-string v5, "automatic_sending_interval_key"

    invoke-direct {p0, v5}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->openPreference(Ljava/lang/String;)V

    .line 385
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v3, v6

    .line 360
    goto :goto_0

    .line 367
    :cond_3
    const-string v7, "edit_emergency_message_key"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 368
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    const-class v6, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;

    invoke-direct {v2, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 369
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v2}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 370
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_4
    const-string v7, "emergency_contacts_key"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 371
    new-instance v2, Landroid/content/Intent;

    const-string v5, "com.sec.android.app.contacts.action.INTERACTION_EMERGENCY_MESSAGE"

    invoke-direct {v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 372
    .restart local v2    # "intent":Landroid/content/Intent;
    invoke-virtual {p0, v2}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 373
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_5
    const-string v7, "emergency_log_key"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 375
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    const-class v7, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceMessageLogs;

    invoke-direct {v2, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 376
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v6, "fromSearch"

    invoke-virtual {v2, v6, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 377
    invoke-virtual {p0, v2}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 380
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_6
    const-string v7, "send_dual_captured_image_key"

    invoke-virtual {p0, v7}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    .line 381
    .local v1, "dualCapturedCheckBox":Landroid/preference/CheckBoxPreference;
    invoke-virtual {v1, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 382
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "send_dual_captured_image"

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v9

    if-eqz v9, :cond_7

    :goto_2
    invoke-static {v7, v8, v5}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_1

    :cond_7
    move v5, v6

    goto :goto_2
.end method

.method private openPreference(Ljava/lang/String;)V
    .locals 6
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 974
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    .line 975
    .local v0, "screen":Landroid/preference/PreferenceScreen;
    if-eqz v0, :cond_0

    .line 976
    invoke-direct {p0, p1}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getPreferenceOrder(Ljava/lang/String;)I

    move-result v3

    .line 977
    .local v3, "order":I
    const/4 v2, -0x1

    if-le v3, v2, :cond_0

    .line 978
    const-wide/16 v4, 0x0

    move-object v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/preference/PreferenceScreen;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 981
    .end local v3    # "order":I
    :cond_0
    return-void
.end method

.method private prepareActivate()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 841
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->featureSupportNewUX()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 842
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->getFinishDisclaimer(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 843
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mContext:Landroid/content/Context;

    invoke-static {v0, v2}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->setFinishDisclaimer(Landroid/content/Context;Z)V

    .line 847
    :cond_0
    iget v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mStartFrom:I

    if-nez v0, :cond_1

    .line 849
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mContext:Landroid/content/Context;

    invoke-static {v0, v2}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->setWakeupKey(Landroid/content/Context;Z)V

    .line 852
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mRemoveCameraOption:Z

    if-nez v0, :cond_2

    .line 853
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mDualCapturedCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 855
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mRemoveVoiceRecordingOption:Z

    if-nez v0, :cond_3

    .line 856
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mSoundRecordingPreference:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 858
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mSendingTimerList:Landroid/preference/ListPreference;

    if-eqz v0, :cond_4

    .line 859
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mSendingTimerList:Landroid/preference/ListPreference;

    invoke-virtual {v0, v2}, Landroid/preference/ListPreference;->setEnabled(Z)V

    .line 862
    :cond_4
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->featureSupportNewUX()Z

    move-result v0

    if-nez v0, :cond_6

    .line 863
    const/high16 v0, 0x7f090000

    invoke-virtual {p0, v0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->displayToast(Ljava/lang/String;)V

    .line 870
    :goto_0
    iget v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mStartFrom:I

    if-nez v0, :cond_7

    .line 871
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "send_emergency_message"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 877
    :cond_5
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "user_agree_to_use_location_service"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 879
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$4;-><init>(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 887
    return-void

    .line 866
    :cond_6
    const-string v0, "SafetyAssuranceFragment"

    const-string v1, "prepareActivate : NEWUX"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 872
    :cond_7
    iget v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mStartFrom:I

    if-ne v0, v2, :cond_5

    .line 873
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "send_b_emergency_message"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 874
    invoke-direct {p0, v2}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->sendBroadcastToWatch(Z)V

    goto :goto_1
.end method

.method private sendBroadcastToWatch(Z)V
    .locals 4
    .param p1, "enableSA"    # Z

    .prologue
    .line 984
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.safetyassurance.settings.SEND_B_OPTION_INFO"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 985
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "sendFromB"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 986
    const-string v1, "disable_camera"

    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->isCameraDisabled()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 987
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 988
    const-string v1, "SafetyAssuranceFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "broadcasting the SEND_B_EMERGENCY_MESSAGE SA enable value : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 989
    const-string v1, "SafetyAssuranceFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "broadcasting the SEND_B_EMERGENCY_MESSAGE Camera disable value : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->isCameraDisabled()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 990
    return-void
.end method

.method private updateContent(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;Landroid/widget/ImageView;Landroid/widget/TextView;Landroid/widget/ImageView;Landroid/widget/ImageView;Landroid/widget/ImageView;)V
    .locals 7
    .param p1, "im00"    # Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;
    .param p2, "im01"    # Landroid/widget/ImageView;
    .param p3, "tv"    # Landroid/widget/TextView;
    .param p4, "navi01"    # Landroid/widget/ImageView;
    .param p5, "navi02"    # Landroid/widget/ImageView;
    .param p6, "navi03"    # Landroid/widget/ImageView;

    .prologue
    const/4 v2, 0x1

    const v6, 0x7f020013

    const/16 v1, 0x8

    const/4 v5, 0x0

    const v4, 0x7f020012

    .line 1005
    iget v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->currentPage:I

    packed-switch v0, :pswitch_data_0

    .line 1041
    :goto_0
    return-void

    .line 1007
    :pswitch_0
    invoke-virtual {p1, v5}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;->setVisibility(I)V

    .line 1008
    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1009
    iget-boolean v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->isVZW:Z

    if-eqz v0, :cond_0

    .line 1010
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mContext:Landroid/content/Context;

    const v1, 0x7f09001d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1016
    :goto_1
    invoke-virtual {p4, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1017
    invoke-virtual {p5, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1018
    invoke-virtual {p6, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 1011
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->isATT:Z

    if-eqz v0, :cond_1

    .line 1012
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mContext:Landroid/content/Context;

    const v1, 0x7f090022

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1014
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mContext:Landroid/content/Context;

    const v1, 0x7f090023

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1021
    :pswitch_1
    invoke-virtual {p1, v1}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;->setVisibility(I)V

    .line 1022
    const v0, 0x7f020018

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1023
    invoke-virtual {p2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1024
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mContext:Landroid/content/Context;

    iget-boolean v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->isVZW:Z

    if-eqz v0, :cond_2

    const v0, 0x7f09001e

    :goto_2
    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1025
    invoke-virtual {p4, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1026
    invoke-virtual {p5, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1027
    invoke-virtual {p6, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 1024
    :cond_2
    const v0, 0x7f09001f

    goto :goto_2

    .line 1030
    :pswitch_2
    invoke-virtual {p1, v1}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;->setVisibility(I)V

    .line 1031
    const v0, 0x7f020019

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1032
    invoke-virtual {p2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1033
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mContext:Landroid/content/Context;

    iget-boolean v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->only_send_recording:Z

    if-eqz v0, :cond_3

    const v0, 0x7f090021

    :goto_3
    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1034
    invoke-virtual {p4, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1035
    invoke-virtual {p5, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1036
    invoke-virtual {p6, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 1033
    :cond_3
    const v0, 0x7f090020

    goto :goto_3

    .line 1005
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method initSAActionBarSwitch()V
    .locals 11

    .prologue
    const/16 v6, 0x10

    const/4 v10, -0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 624
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 625
    .local v0, "activity":Landroid/app/Activity;
    new-instance v4, Landroid/widget/Switch;

    invoke-direct {v4, v0}, Landroid/widget/Switch;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mActionBarSwitch:Landroid/widget/Switch;

    .line 627
    iget-object v4, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mActionBarSwitch:Landroid/widget/Switch;

    new-instance v5, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$3;

    invoke-direct {v5, p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$3;-><init>(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)V

    invoke-virtual {v4, v5}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 690
    instance-of v4, v0, Landroid/preference/PreferenceActivity;

    if-eqz v4, :cond_1

    move-object v2, v0

    .line 691
    check-cast v2, Landroid/preference/PreferenceActivity;

    .line 692
    .local v2, "preferenceActivity":Landroid/preference/PreferenceActivity;
    invoke-virtual {v2}, Landroid/preference/PreferenceActivity;->onIsHidingHeaders()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v2}, Landroid/preference/PreferenceActivity;->onIsMultiPane()Z

    move-result v4

    if-nez v4, :cond_1

    .line 693
    :cond_0
    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const/high16 v5, 0x7f080000

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 694
    .local v1, "padding":I
    iget-object v4, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v4, v9, v9, v1, v9}, Landroid/widget/Switch;->setPaddingRelative(IIII)V

    .line 695
    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v6, v6}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 696
    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mActionBarSwitch:Landroid/widget/Switch;

    new-instance v6, Landroid/app/ActionBar$LayoutParams;

    const v7, 0x800015

    invoke-direct {v6, v10, v10, v7}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    invoke-virtual {v4, v5, v6}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    .line 699
    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 700
    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 701
    invoke-virtual {p0, v8}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->setHasOptionsMenu(Z)V

    .line 703
    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mActionBarLayout:Landroid/view/View;

    .line 707
    .end local v1    # "padding":I
    .end local v2    # "preferenceActivity":Landroid/preference/PreferenceActivity;
    :cond_1
    const/4 v3, 0x0

    .line 709
    .local v3, "switchState":I
    iget v4, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mStartFrom:I

    if-nez v4, :cond_4

    .line 710
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "send_emergency_message"

    invoke-static {v4, v5, v9}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    .line 715
    :cond_2
    :goto_0
    if-ne v3, v8, :cond_3

    .line 716
    iget-object v4, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v4, v8}, Landroid/widget/Switch;->setChecked(Z)V

    .line 717
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->featureSupportNewUX()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 719
    iput-boolean v8, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->check_no_emcontact_onCreate:Z

    .line 722
    :cond_3
    return-void

    .line 711
    :cond_4
    iget v4, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mStartFrom:I

    if-ne v4, v8, :cond_2

    .line 712
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "send_b_emergency_message"

    invoke-static {v4, v5, v9}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v2, -0x1

    .line 778
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceGeneralPreferenceFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 780
    if-nez p1, :cond_2

    .line 781
    if-ne p2, v2, :cond_1

    .line 782
    const-string v1, "SafetyAssuranceFragment"

    const-string v2, "Result SEMA OK"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 783
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->prepareActivate()V

    .line 809
    :cond_0
    :goto_0
    return-void

    .line 784
    :cond_1
    if-nez p2, :cond_0

    .line 785
    const-string v1, "SafetyAssuranceFragment"

    const-string v2, "Result SEMA CANCEL"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 786
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->discardActivate()V

    goto :goto_0

    .line 789
    :cond_2
    const/4 v1, 0x1

    if-ne p1, v1, :cond_4

    .line 790
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getActivity()Landroid/app/Activity;

    if-ne p2, v2, :cond_3

    .line 791
    const-string v1, "SafetyAssuranceFragment"

    const-string v2, "onActivityResult : REQUESTCODE_SET_RECORDING_TIME_ACTIVITY - RESULT_OK"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 793
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getActivity()Landroid/app/Activity;

    if-nez p2, :cond_0

    .line 794
    const-string v1, "SafetyAssuranceFragment"

    const-string v2, "onActivityResult : REQUESTCODE_SET_RECORDING_TIME_ACTIVITY - RESULT_CANCELED"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 798
    :cond_4
    const/4 v1, 0x2

    if-ne p1, v1, :cond_0

    .line 799
    const-string v1, "SafetyAssuranceFragment"

    const-string v2, "Result AECA"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 800
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->featureSupportNewUX()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 802
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->queryEmergencyContactSize(Landroid/content/Context;)I

    move-result v0

    .line 803
    .local v0, "nSize":I
    if-gtz v0, :cond_0

    .line 804
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->discardActivate()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 21
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 193
    invoke-super/range {p0 .. p1}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceGeneralPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 195
    new-instance v15, Landroid/view/ContextThemeWrapper;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v16

    const v17, 0x7f0a0003

    invoke-direct/range {v15 .. v17}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mContext:Landroid/content/Context;

    .line 197
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->isVZWDevice()Z

    move-result v15

    if-eqz v15, :cond_0

    .line 198
    const-string v15, "SafetyAssuranceFragment"

    const-string v16, "onCreate() : usa-vzw"

    invoke-static/range {v15 .. v16}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->isVZW:Z

    .line 202
    :cond_0
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->isATTDevice()Z

    move-result v15

    if-eqz v15, :cond_1

    .line 203
    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->isATT:Z

    .line 206
    :cond_1
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->featureSupportNewUX()Z

    move-result v15

    if-eqz v15, :cond_2

    .line 207
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v15

    invoke-virtual {v15}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v15

    const/16 v16, 0x0

    invoke-virtual/range {v15 .. v16}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 210
    :cond_2
    new-instance v10, Landroid/content/Intent;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v15

    invoke-virtual {v15}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v15

    invoke-direct {v10, v15}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 211
    .local v10, "modIntent":Landroid/content/Intent;
    const/high16 v15, 0x7f050000

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->addPreferencesFromResource(I)V

    .line 213
    const-string v15, "edit_emergency_message_key"

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mEditingEmergencyMessage:Landroid/preference/Preference;

    .line 216
    const-string v15, "emergency_log_key"

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mEmergencyMessageLog:Landroid/preference/Preference;

    .line 217
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mEmergencyMessageLog:Landroid/preference/Preference;

    if-eqz v15, :cond_3

    .line 218
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->isJPNSecMessageDisable()Z

    move-result v15

    if-nez v15, :cond_3

    .line 219
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mEmergencyMessageLog:Landroid/preference/Preference;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 223
    :cond_3
    const-string v15, "launchFrom"

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v10, v15, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v15

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v15, v0, :cond_4

    .line 224
    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mStartFrom:I

    .line 231
    :cond_4
    const-string v15, "send_dual_captured_image_key"

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v15

    check-cast v15, Landroid/preference/CheckBoxPreference;

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mDualCapturedCheckBox:Landroid/preference/CheckBoxPreference;

    .line 233
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mContext:Landroid/content/Context;

    const-string v16, "FirstLaunchPreference"

    const/16 v17, 0x0

    invoke-virtual/range {v15 .. v17}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v11

    .line 234
    .local v11, "sPref":Landroid/content/SharedPreferences;
    const-string v15, "FirstLaunch"

    const/16 v16, 0x1

    move/from16 v0, v16

    invoke-interface {v11, v15, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    .line 235
    .local v9, "firstLaunch":Z
    const-string v15, "SafetyAssuranceFragment"

    const-string v16, "SAssurance - First launch of SAssurance in settings menu"

    invoke-static/range {v15 .. v16}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->featureSupportNewUX()Z

    move-result v15

    if-eqz v15, :cond_5

    .line 239
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v15}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v15

    const-string v16, "automatic_sending_interval"

    const/16 v17, 0x0

    invoke-static/range {v15 .. v17}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 243
    :cond_5
    new-instance v12, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v15

    invoke-direct {v12, v15}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;-><init>(Landroid/content/Context;)V

    .line 244
    .local v12, "saUtils":Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;
    invoke-virtual {v12}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->isDisableCameraOption()Z

    move-result v15

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mRemoveCameraOption:Z

    .line 245
    invoke-virtual {v12}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->isDisableCameraOptionForGear()Z

    move-result v15

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mRemoveCameraOptionForGear:Z

    .line 246
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mDualCapturedCheckBox:Landroid/preference/CheckBoxPreference;

    if-eqz v15, :cond_9

    .line 247
    const/4 v15, 0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mStartFrom:I

    move/from16 v16, v0

    move/from16 v0, v16

    if-ne v15, v0, :cond_13

    .line 248
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mDualCapturedCheckBox:Landroid/preference/CheckBoxPreference;

    const v16, 0x7f090038

    invoke-virtual/range {v15 .. v16}, Landroid/preference/CheckBoxPreference;->setTitle(I)V

    .line 249
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mDualCapturedCheckBox:Landroid/preference/CheckBoxPreference;

    const v16, 0x7f09003b

    invoke-virtual/range {v15 .. v16}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    .line 250
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mRemoveCameraOptionForGear:Z

    if-eqz v15, :cond_6

    .line 251
    const-string v15, "SafetyAssuranceFragment"

    const-string v16, "Remove Camera option"

    invoke-static/range {v15 .. v16}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mDualCapturedCheckBox:Landroid/preference/CheckBoxPreference;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 268
    :cond_6
    :goto_0
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mRemoveCameraOption:Z

    if-nez v15, :cond_8

    if-eqz v9, :cond_8

    .line 269
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v2

    .line 270
    .local v2, "csc":Lcom/sec/android/app/CscFeature;
    const-string v15, "CscFeature_Setting_EnableEmergencyPicturesSending"

    invoke-virtual {v2, v15}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v15

    if-nez v15, :cond_7

    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->featureSupportNewUX()Z

    move-result v15

    if-eqz v15, :cond_17

    .line 272
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v15

    const-string v16, "send_dual_captured_image"

    const/16 v17, 0x1

    invoke-static/range {v15 .. v17}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 277
    .end local v2    # "csc":Lcom/sec/android/app/CscFeature;
    :cond_8
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mDualCapturedCheckBox:Landroid/preference/CheckBoxPreference;

    move-object/from16 v16, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v15

    const-string v17, "send_dual_captured_image"

    const/16 v18, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-static {v15, v0, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v15

    if-eqz v15, :cond_18

    const/4 v15, 0x1

    :goto_2
    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 280
    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v16

    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->featureSupportNewUX()Z

    move-result v15

    if-eqz v15, :cond_19

    const v15, 0x7f090024

    :goto_3
    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 281
    .local v8, "firstItem":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v15

    const v16, 0x7f090002

    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->entryValues:[Ljava/lang/CharSequence;

    move-object/from16 v19, v0

    const/16 v20, 0x1

    aget-object v19, v19, v20

    invoke-interface/range {v19 .. v19}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v19

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    aput-object v19, v17, v18

    invoke-virtual/range {v15 .. v17}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    .line 282
    .local v13, "secondItem":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v15

    const v16, 0x7f090002

    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->entryValues:[Ljava/lang/CharSequence;

    move-object/from16 v19, v0

    const/16 v20, 0x2

    aget-object v19, v19, v20

    invoke-interface/range {v19 .. v19}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v19

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    aput-object v19, v17, v18

    invoke-virtual/range {v15 .. v17}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    .line 283
    .local v14, "thirdItem":Ljava/lang/String;
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_a

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_a

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_a

    .line 284
    const/4 v15, 0x3

    new-array v6, v15, [Ljava/lang/CharSequence;

    const/4 v15, 0x0

    aput-object v8, v6, v15

    const/4 v15, 0x1

    aput-object v13, v6, v15

    const/4 v15, 0x2

    aput-object v14, v6, v15

    .line 286
    .local v6, "entries":[Ljava/lang/CharSequence;
    const-string v15, "automatic_sending_interval_key"

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v15

    check-cast v15, Landroid/preference/ListPreference;

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mSendingTimerList:Landroid/preference/ListPreference;

    .line 287
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mSendingTimerList:Landroid/preference/ListPreference;

    if-eqz v15, :cond_a

    .line 288
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mSendingTimerList:Landroid/preference/ListPreference;

    const/16 v16, 0x0

    invoke-virtual/range {v15 .. v16}, Landroid/preference/ListPreference;->setWidgetLayoutResource(I)V

    .line 289
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mSendingTimerList:Landroid/preference/ListPreference;

    invoke-virtual {v15, v6}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 290
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mSendingTimerList:Landroid/preference/ListPreference;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->entryValues:[Ljava/lang/CharSequence;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 291
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mSendingTimerList:Landroid/preference/ListPreference;

    move-object/from16 v0, p0

    invoke-virtual {v15, v0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 293
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->featureSupportNewUX()Z

    move-result v15

    if-eqz v15, :cond_a

    .line 295
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mSendingTimerList:Landroid/preference/ListPreference;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 299
    .end local v6    # "entries":[Ljava/lang/CharSequence;
    :cond_a
    const-string v15, "emergency_contacts_key"

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mEmergencyContact:Landroid/preference/Preference;

    .line 301
    const-string v15, "send_send_sound_recording_key"

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v15

    check-cast v15, Landroid/preference/CheckBoxPreference;

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mSoundRecordingPreference:Landroid/preference/CheckBoxPreference;

    .line 304
    invoke-virtual {v12}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->isVocRecHide()Z

    move-result v15

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mRemoveVoiceRecordingOption:Z

    .line 305
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mSoundRecordingPreference:Landroid/preference/CheckBoxPreference;

    if-eqz v15, :cond_d

    .line 306
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mRemoveVoiceRecordingOption:Z

    if-eqz v15, :cond_b

    .line 307
    const-string v15, "SafetyAssuranceFragment"

    const-string v16, "Remove Voice Recording option"

    invoke-static/range {v15 .. v16}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 308
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mSoundRecordingPreference:Landroid/preference/CheckBoxPreference;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 310
    :cond_b
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mRemoveVoiceRecordingOption:Z

    if-nez v15, :cond_c

    if-eqz v9, :cond_c

    .line 312
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v15

    const-string v16, "send_vocrec"

    const/16 v17, 0x1

    invoke-static/range {v15 .. v17}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 314
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mSoundRecordingPreference:Landroid/preference/CheckBoxPreference;

    move-object/from16 v16, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v15

    const-string v17, "send_vocrec"

    const/16 v18, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-static {v15, v0, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v15

    if-eqz v15, :cond_1a

    const/4 v15, 0x1

    :goto_4
    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 318
    :cond_d
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->featureSupportNewUX()Z

    move-result v15

    if-eqz v15, :cond_f

    .line 319
    const-string v15, "edit_emergency_message_key"

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    .line 320
    .local v3, "editEmergencyMessage":Landroid/preference/Preference;
    if-eqz v3, :cond_e

    .line 321
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v15

    invoke-virtual {v15, v3}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 323
    :cond_e
    const-string v15, "emergency_contacts_key"

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    .line 324
    .local v5, "emergencyContacts":Landroid/preference/Preference;
    if-eqz v5, :cond_f

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mStartFrom:I

    const/16 v16, 0x1

    move/from16 v0, v16

    if-eq v15, v0, :cond_f

    .line 325
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v15

    invoke-virtual {v15, v5}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 330
    .end local v3    # "editEmergencyMessage":Landroid/preference/Preference;
    .end local v5    # "emergencyContacts":Landroid/preference/Preference;
    :cond_f
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->initSAActionBarSwitch()V

    .line 332
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v7

    .line 333
    .local v7, "extra_bundle":Landroid/os/Bundle;
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->featureFindoSearchSettingAppEnable()Z

    move-result v15

    if-eqz v15, :cond_10

    if-eqz v7, :cond_10

    .line 334
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->launchFromSettingSearch(Landroid/os/Bundle;)V

    .line 336
    :cond_10
    const-string v15, "SafetyAssuranceFragment"

    const-string v16, "SAssurance - end of onCreate()"

    invoke-static/range {v15 .. v16}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 338
    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mStartFrom:I

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v15, v0, :cond_11

    .line 339
    const-string v15, "send_send_sound_recording_key"

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v15

    check-cast v15, Landroid/preference/CheckBoxPreference;

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mSoundRecordingPreference:Landroid/preference/CheckBoxPreference;

    .line 340
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mSoundRecordingPreference:Landroid/preference/CheckBoxPreference;

    if-eqz v15, :cond_11

    .line 341
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mSoundRecordingPreference:Landroid/preference/CheckBoxPreference;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 346
    :cond_11
    const/4 v15, 0x1

    if-ne v9, v15, :cond_12

    .line 347
    invoke-interface {v11}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    .line 348
    .local v4, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v15, "FirstLaunch"

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-interface {v4, v15, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 349
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 351
    .end local v4    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_12
    return-void

    .line 255
    .end local v7    # "extra_bundle":Landroid/os/Bundle;
    .end local v8    # "firstItem":Ljava/lang/String;
    .end local v13    # "secondItem":Ljava/lang/String;
    .end local v14    # "thirdItem":Ljava/lang/String;
    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mDualCapturedCheckBox:Landroid/preference/CheckBoxPreference;

    move-object/from16 v16, v0

    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->featureSupportNewUX()Z

    move-result v15

    if-eqz v15, :cond_16

    const v15, 0x7f09003a

    :goto_5
    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Landroid/preference/CheckBoxPreference;->setTitle(I)V

    .line 256
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->featureSupportNewUX()Z

    move-result v15

    if-nez v15, :cond_14

    .line 257
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mDualCapturedCheckBox:Landroid/preference/CheckBoxPreference;

    const v16, 0x7f09003c

    invoke-virtual/range {v15 .. v16}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    .line 259
    :cond_14
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->isATT:Z

    if-eqz v15, :cond_15

    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->featureSupportNewUX()Z

    move-result v15

    if-eqz v15, :cond_15

    .line 260
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mDualCapturedCheckBox:Landroid/preference/CheckBoxPreference;

    const v16, 0x7f09003d

    invoke-virtual/range {v15 .. v16}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    .line 262
    :cond_15
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mRemoveCameraOption:Z

    if-eqz v15, :cond_6

    .line 263
    const-string v15, "SafetyAssuranceFragment"

    const-string v16, "Remove Camera option"

    invoke-static/range {v15 .. v16}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mDualCapturedCheckBox:Landroid/preference/CheckBoxPreference;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_0

    .line 255
    :cond_16
    const v15, 0x7f090039

    goto :goto_5

    .line 274
    .restart local v2    # "csc":Lcom/sec/android/app/CscFeature;
    :cond_17
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v15

    const-string v16, "send_dual_captured_image"

    const/16 v17, 0x0

    invoke-static/range {v15 .. v17}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_1

    .line 277
    .end local v2    # "csc":Lcom/sec/android/app/CscFeature;
    :cond_18
    const/4 v15, 0x0

    goto/16 :goto_2

    .line 280
    :cond_19
    const v15, 0x7f090034

    goto/16 :goto_3

    .line 314
    .restart local v8    # "firstItem":Ljava/lang/String;
    .restart local v13    # "secondItem":Ljava/lang/String;
    .restart local v14    # "thirdItem":Ljava/lang/String;
    :cond_1a
    const/4 v15, 0x0

    goto/16 :goto_4
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 726
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 730
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceGeneralPreferenceFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 728
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->finish()V

    goto :goto_0

    .line 726
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 606
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mReceiver:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$SafetyAssuranceFragmentReceiver;

    if-eqz v0, :cond_0

    .line 607
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mReceiver:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$SafetyAssuranceFragmentReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 608
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mReceiver:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$SafetyAssuranceFragmentReceiver;

    .line 610
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceGeneralPreferenceFragment;->onPause()V

    .line 611
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mSendingTimerList:Landroid/preference/ListPreference;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 144
    invoke-direct {p0, p2}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->changeTextOfItervalTimeToSendMessage(Ljava/lang/Object;)V

    .line 145
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "automatic_sending_interval"

    check-cast p2, Ljava/lang/String;

    .end local p2    # "newValue":Ljava/lang/Object;
    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 147
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 8
    .param p1, "preferenceScreen"    # Landroid/preference/PreferenceScreen;
    .param p2, "preference"    # Landroid/preference/Preference;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 736
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mEditingEmergencyMessage:Landroid/preference/Preference;

    invoke-virtual {v3, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 737
    const-string v3, "SafetyAssuranceFragment"

    const-string v5, "SAssurance - preferenceTreeClick mEmergencyMessageSwitch"

    invoke-static {v3, v5}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 738
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-class v5, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;

    invoke-direct {v2, v3, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 739
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v2}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->startActivity(Landroid/content/Intent;)V

    .line 773
    .end local v2    # "intent":Landroid/content/Intent;
    :goto_0
    return v4

    .line 741
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mDualCapturedCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 742
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mDualCapturedCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 743
    iget v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mStartFrom:I

    if-ne v4, v3, :cond_3

    .line 744
    const v3, 0x7f090012

    invoke-virtual {p0, v3}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->displayToast(Ljava/lang/String;)V

    .line 749
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "send_dual_captured_image"

    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mDualCapturedCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_4

    move v3, v4

    :goto_2
    invoke-static {v6, v7, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 751
    iget v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mStartFrom:I

    if-ne v3, v4, :cond_2

    .line 752
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v6, "send_b_emergency_message"

    invoke-static {v3, v6, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 753
    .local v1, "enabledSA":I
    if-lez v1, :cond_5

    :goto_3
    invoke-direct {p0, v4}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->sendBroadcastToWatch(Z)V

    .line 773
    .end local v1    # "enabledSA":I
    :cond_2
    :goto_4
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceGeneralPreferenceFragment;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v4

    goto :goto_0

    .line 746
    :cond_3
    const v3, 0x7f090025

    invoke-virtual {p0, v3}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->displayToast(Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    move v3, v5

    .line 749
    goto :goto_2

    .restart local v1    # "enabledSA":I
    :cond_5
    move v4, v5

    .line 753
    goto :goto_3

    .line 755
    .end local v1    # "enabledSA":I
    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mSendingTimerList:Landroid/preference/ListPreference;

    invoke-virtual {v3, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 756
    const-string v3, "SafetyAssuranceFragment"

    const-string v4, "onPreferenceTreeClick : mSendingTimerList"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 758
    :cond_7
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mEmergencyMessageLog:Landroid/preference/Preference;

    invoke-virtual {v3, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 759
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-class v4, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceMessageLogs;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 760
    .restart local v2    # "intent":Landroid/content/Intent;
    invoke-virtual {p0, v2}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_4

    .line 761
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_8
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mEmergencyContact:Landroid/preference/Preference;

    invoke-virtual {v3, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 763
    :try_start_0
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.sec.android.app.contacts.action.INTERACTION_EMERGENCY_MESSAGE"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 764
    .restart local v2    # "intent":Landroid/content/Intent;
    invoke-virtual {p0, v2}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_4

    .line 765
    .end local v2    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 766
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_4

    .line 768
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_9
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mSoundRecordingPreference:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 769
    const-string v3, "SafetyAssuranceFragment"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SoundRecording isChecked["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mSoundRecordingPreference:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v7}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 770
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v6, "send_vocrec"

    iget-object v7, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mSoundRecordingPreference:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v7}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v7

    if-eqz v7, :cond_a

    :goto_5
    invoke-static {v3, v6, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_4

    :cond_a
    move v4, v5

    goto :goto_5
.end method

.method public onResume()V
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 524
    invoke-super {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceGeneralPreferenceFragment;->onResume()V

    .line 525
    const-string v3, "SafetyAssuranceFragment"

    const-string v6, "SAssurance - onResume()"

    invoke-static {v3, v6}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 527
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mReceiver:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$SafetyAssuranceFragmentReceiver;

    if-nez v3, :cond_0

    .line 528
    new-instance v3, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$SafetyAssuranceFragmentReceiver;

    invoke-direct {v3, p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$SafetyAssuranceFragmentReceiver;-><init>(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)V

    iput-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mReceiver:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$SafetyAssuranceFragmentReceiver;

    .line 529
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    iget-object v6, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mReceiver:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$SafetyAssuranceFragmentReceiver;

    new-instance v7, Landroid/content/IntentFilter;

    const-string v8, "com.sec.android.app.safetyassurance.action.SAFRAGMENT_RESUME"

    invoke-direct {v7, v8}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v6, v7}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 532
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mActionBarLayout:Landroid/view/View;

    if-eqz v3, :cond_7

    .line 533
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mActionBarLayout:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 538
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mDualCapturedCheckBox:Landroid/preference/CheckBoxPreference;

    if-eqz v3, :cond_1

    .line 539
    iget-object v6, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mDualCapturedCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v7, "send_dual_captured_image"

    invoke-static {v3, v7, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-eqz v3, :cond_8

    move v3, v4

    :goto_1
    invoke-virtual {v6, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 541
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mSoundRecordingPreference:Landroid/preference/CheckBoxPreference;

    if-eqz v3, :cond_2

    .line 542
    iget-object v6, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mSoundRecordingPreference:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v7, "send_vocrec"

    invoke-static {v3, v7, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-eqz v3, :cond_9

    move v3, v4

    :goto_2
    invoke-virtual {v6, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 546
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceConfig;->getAppState(Landroid/content/Context;)I

    move-result v1

    .line 548
    .local v1, "nAppState":I
    if-gt v1, v4, :cond_3

    if-nez v1, :cond_a

    .line 549
    :cond_3
    const-string v3, "SafetyAssuranceFragment"

    const-string v4, "SAssurance - the return value from getTriggerMode() is true."

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 550
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v3, v5}, Landroid/widget/Switch;->setEnabled(Z)V

    .line 551
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/preference/PreferenceScreen;->setEnabled(Z)V

    .line 580
    :cond_4
    :goto_3
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mSendingTimerList:Landroid/preference/ListPreference;

    if-eqz v3, :cond_f

    .line 581
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mSendingTimerList:Landroid/preference/ListPreference;

    invoke-virtual {v3}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->changeTextOfItervalTimeToSendMessage(Ljava/lang/Object;)V

    .line 585
    :goto_4
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->changeTextOfRecordingTime()V

    .line 587
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->featureSupportNewUX()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 589
    iget-boolean v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->check_no_emcontact_onCreate:Z

    if-eqz v3, :cond_6

    .line 590
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v3}, Landroid/widget/Switch;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v3}, Landroid/widget/Switch;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->queryEmergencyContactSize(Landroid/content/Context;)I

    move-result v3

    if-gtz v3, :cond_5

    .line 591
    new-instance v3, Ljava/lang/Thread;

    new-instance v4, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$2;

    invoke-direct {v4, p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$2;-><init>(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)V

    invoke-direct {v3, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    .line 598
    :cond_5
    iput-boolean v5, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->check_no_emcontact_onCreate:Z

    .line 602
    :cond_6
    return-void

    .line 535
    .end local v1    # "nAppState":I
    :cond_7
    const-string v3, "SafetyAssuranceFragment"

    const-string v6, "SAssurance - mActionBarLayout is null"

    invoke-static {v3, v6}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_8
    move v3, v5

    .line 539
    goto/16 :goto_1

    :cond_9
    move v3, v5

    .line 542
    goto :goto_2

    .line 553
    .restart local v1    # "nAppState":I
    :cond_a
    const-string v3, "SafetyAssuranceFragment"

    const-string v6, "SAssurance - the return value from getTriggerMode() is false."

    invoke-static {v3, v6}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 554
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v3, v4}, Landroid/widget/Switch;->setEnabled(Z)V

    .line 555
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->setEnabled(Z)V

    .line 557
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v3}, Landroid/widget/Switch;->isChecked()Z

    move-result v3

    if-nez v3, :cond_d

    .line 558
    iget-boolean v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mRemoveCameraOption:Z

    if-nez v3, :cond_b

    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mDualCapturedCheckBox:Landroid/preference/CheckBoxPreference;

    if-eqz v3, :cond_b

    .line 559
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mDualCapturedCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v5}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 561
    :cond_b
    iget-boolean v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mRemoveVoiceRecordingOption:Z

    if-nez v3, :cond_c

    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mSoundRecordingPreference:Landroid/preference/CheckBoxPreference;

    if-eqz v3, :cond_c

    .line 562
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mSoundRecordingPreference:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v5}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 564
    :cond_c
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mSendingTimerList:Landroid/preference/ListPreference;

    if-eqz v3, :cond_d

    .line 565
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mSendingTimerList:Landroid/preference/ListPreference;

    invoke-virtual {v3, v5}, Landroid/preference/ListPreference;->setEnabled(Z)V

    .line 570
    :cond_d
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->isJPNSecMessageDisable()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 571
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 572
    .local v2, "sharedPrefs":Landroid/content/SharedPreferences;
    const-string v3, "pref_key_log1"

    const-string v6, ""

    invoke-interface {v2, v3, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 573
    .local v0, "first_log":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 574
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mEmergencyMessageLog:Landroid/preference/Preference;

    invoke-virtual {v3, v5}, Landroid/preference/Preference;->setEnabled(Z)V

    goto/16 :goto_3

    .line 576
    :cond_e
    iget-object v3, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mEmergencyMessageLog:Landroid/preference/Preference;

    invoke-virtual {v3, v4}, Landroid/preference/Preference;->setEnabled(Z)V

    goto/16 :goto_3

    .line 583
    .end local v0    # "first_log":Ljava/lang/String;
    .end local v2    # "sharedPrefs":Landroid/content/SharedPreferences;
    :cond_f
    const-string v3, "SafetyAssuranceFragment"

    const-string v4, "mSendingTimerList is null"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4
.end method

.method public onStart()V
    .locals 24

    .prologue
    .line 389
    invoke-super/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceGeneralPreferenceFragment;->onStart()V

    .line 391
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v16

    .line 392
    .local v16, "countHeaderView":I
    if-lez v16, :cond_1

    .line 520
    :cond_0
    :goto_0
    return-void

    .line 396
    :cond_1
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mStartFrom:I

    const/4 v8, 0x1

    if-ne v1, v8, :cond_4

    .line 397
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mContext:Landroid/content/Context;

    const v8, 0x7f030009

    const/4 v9, 0x0

    invoke-static {v1, v8, v9}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v17

    .line 398
    .local v17, "header":Landroid/view/View;
    if-eqz v17, :cond_0

    .line 399
    const v1, 0x7f0b0019

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v23

    check-cast v23, Landroid/widget/TextView;

    .line 400
    .local v23, "textView":Landroid/widget/TextView;
    if-eqz v23, :cond_2

    .line 401
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mContext:Landroid/content/Context;

    const v8, 0x7f09001a

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    const/4 v11, 0x3

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v1, v8, v9}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, v23

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 403
    :cond_2
    const v1, 0x7f0b0017

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/LinearLayout;

    .line 404
    .local v18, "linerLayout":Landroid/widget/LinearLayout;
    if-eqz v18, :cond_3

    .line 405
    const/4 v1, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 407
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getListView()Landroid/widget/ListView;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    goto :goto_0

    .line 410
    .end local v17    # "header":Landroid/view/View;
    .end local v18    # "linerLayout":Landroid/widget/LinearLayout;
    .end local v23    # "textView":Landroid/widget/TextView;
    :cond_4
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mStartFrom:I

    if-nez v1, :cond_0

    .line 411
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mContext:Landroid/content/Context;

    const v8, 0x7f030008

    const/4 v9, 0x0

    invoke-static {v1, v8, v9}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v17

    .line 412
    .restart local v17    # "header":Landroid/view/View;
    if-eqz v17, :cond_0

    .line 413
    const v1, 0x7f0b0017

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/LinearLayout;

    .line 414
    .restart local v18    # "linerLayout":Landroid/widget/LinearLayout;
    if-eqz v18, :cond_5

    .line 415
    const/4 v1, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 417
    :cond_5
    new-instance v21, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mContext:Landroid/content/Context;

    move-object/from16 v0, v21

    invoke-direct {v0, v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;-><init>(Landroid/content/Context;)V

    .line 418
    .local v21, "saUtils":Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;
    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->isDisableCameraOption()Z

    move-result v20

    .line 419
    .local v20, "removeCameraOption":Z
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->isJPNDevice()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 421
    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->max_helpimage_page:I

    .line 422
    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->hide_page:Z

    .line 428
    :cond_6
    :goto_1
    const v1, 0x7f0b0019

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/TextView;

    .line 430
    .local v22, "summary":Landroid/widget/TextView;
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->featureSupportNewUX()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 432
    const v1, 0x7f0b001b

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;

    .line 433
    .local v2, "swImCnt00":Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;
    const v1, 0x7f0b001c

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 434
    .local v3, "swImCnt01":Landroid/widget/ImageView;
    const v1, 0x7f0b0021

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 435
    .local v4, "swTv":Landroid/widget/TextView;
    const v1, 0x7f0b001e

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 436
    .local v5, "swNavi01":Landroid/widget/ImageView;
    const v1, 0x7f0b001f

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 437
    .local v6, "swNavi02":Landroid/widget/ImageView;
    const v1, 0x7f0b0020

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    .line 440
    .local v7, "swNavi03":Landroid/widget/ImageView;
    const v1, 0x7f0b0018

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->av:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;

    .line 441
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->av:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;

    const/16 v8, 0x8

    invoke-virtual {v1, v8}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;->setVisibility(I)V

    .line 442
    if-eqz v22, :cond_7

    .line 443
    const/16 v1, 0x8

    move-object/from16 v0, v22

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 446
    :cond_7
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->hide_page:Z

    if-eqz v1, :cond_8

    .line 448
    const/16 v1, 0x8

    invoke-virtual {v7, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_8
    move-object/from16 v1, p0

    .line 451
    invoke-direct/range {v1 .. v7}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->updateContent(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;Landroid/widget/ImageView;Landroid/widget/TextView;Landroid/widget/ImageView;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    .line 453
    const v1, 0x7f0b001a

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->layoutSwIm:Landroid/widget/LinearLayout;

    .line 454
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->layoutSwIm:Landroid/widget/LinearLayout;

    const/4 v8, 0x0

    invoke-virtual {v1, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 455
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->layoutSwIm:Landroid/widget/LinearLayout;

    new-instance v8, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$1;

    move-object/from16 v9, p0

    move-object v10, v2

    move-object v11, v3

    move-object v12, v4

    move-object v13, v5

    move-object v14, v6

    move-object v15, v7

    invoke-direct/range {v8 .. v15}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$1;-><init>(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;Landroid/widget/ImageView;Landroid/widget/TextView;Landroid/widget/ImageView;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    invoke-virtual {v1, v8}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 515
    .end local v2    # "swImCnt00":Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;
    .end local v3    # "swImCnt01":Landroid/widget/ImageView;
    .end local v4    # "swTv":Landroid/widget/TextView;
    .end local v5    # "swNavi01":Landroid/widget/ImageView;
    .end local v6    # "swNavi02":Landroid/widget/ImageView;
    .end local v7    # "swNavi03":Landroid/widget/ImageView;
    :goto_2
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->getListView()Landroid/widget/ListView;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    goto/16 :goto_0

    .line 423
    .end local v22    # "summary":Landroid/widget/TextView;
    :cond_9
    if-eqz v20, :cond_6

    .line 425
    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->only_send_recording:Z

    goto/16 :goto_1

    .line 507
    .restart local v22    # "summary":Landroid/widget/TextView;
    :cond_a
    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->isDisableCameraOption()Z

    move-result v19

    .line 508
    .local v19, "mRemoveCameraOption":Z
    if-eqz v19, :cond_b

    .line 509
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mContext:Landroid/content/Context;

    const v8, 0x7f09001c

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    const/4 v11, 0x3

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v1, v8, v9}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, v22

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 511
    :cond_b
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mContext:Landroid/content/Context;

    const v8, 0x7f09001b

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    const/4 v11, 0x3

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v1, v8, v9}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, v22

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 615
    invoke-super {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceGeneralPreferenceFragment;->onStop()V

    .line 616
    const-string v0, "SafetyAssuranceFragment"

    const-string v1, "SAssurance - onStop() "

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 618
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mActionBarLayout:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 619
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mActionBarLayout:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 621
    :cond_0
    return-void
.end method
