.class Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$5$1;
.super Ljava/lang/Object;
.source "SafetyAssuranceFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$5;->handleMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$5;


# direct methods
.method constructor <init>(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$5;)V
    .locals 0

    .prologue
    .line 926
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$5$1;->this$1:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 928
    const-string v0, "SafetyAssuranceFragment"

    const-string v1, "SHOW_NO_EMERGENCY_CONTACT_DIALOG - cancel"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 929
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->featureSupportNewUX()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 931
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$5$1;->this$1:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$5;

    iget-object v0, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$5;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    # getter for: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->access$900(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceUtils;->queryEmergencyContactSize(Landroid/content/Context;)I

    move-result v0

    if-gtz v0, :cond_0

    .line 932
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$5$1;->this$1:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$5;

    iget-object v0, v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment$5;->this$0:Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;

    # invokes: Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->discardActivate()V
    invoke-static {v0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;->access$1800(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceFragment;)V

    .line 935
    :cond_0
    return-void
.end method
