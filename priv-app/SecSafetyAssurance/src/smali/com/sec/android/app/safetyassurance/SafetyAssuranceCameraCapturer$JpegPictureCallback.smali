.class final Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;
.super Ljava/lang/Object;
.source "SafetyAssuranceCameraCapturer.java"

# interfaces
.implements Lcom/sec/android/seccamera/SecCamera$PictureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "JpegPictureCallback"
.end annotation


# instance fields
.field mWhichCamera:I

.field outputStream:Ljava/io/FileOutputStream;

.field final synthetic this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;I)V
    .locals 0
    .param p2, "whichCamera"    # I

    .prologue
    .line 338
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 339
    iput p2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->mWhichCamera:I

    .line 340
    return-void
.end method


# virtual methods
.method public onPictureTaken([BLcom/sec/android/seccamera/SecCamera;)V
    .locals 18
    .param p1, "jpegData"    # [B
    .param p2, "camera"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 343
    const-string v14, "SafetyAssuranceCameraCapturer"

    const-string v15, "onPictureTaken"

    invoke-static {v14, v15}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 344
    const/4 v12, 0x0

    .line 345
    .local v12, "savedUri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    # invokes: Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->getDateTaken()J
    invoke-static {v14}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->access$000(Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;)J

    move-result-wide v10

    .line 346
    .local v10, "lTakenTime":J
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    # invokes: Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->createName(J)Ljava/lang/String;
    invoke-static {v15, v10, v11}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->access$100(Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;J)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ".jpg"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 347
    .local v7, "filename":Ljava/lang/String;
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v15, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->CAMERA_IMAGE_BUCKET_NAME_PHONE:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "/"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 350
    .local v6, "filePath":Ljava/lang/String;
    :try_start_0
    new-instance v2, Ljava/io/File;

    sget-object v14, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->CAMERA_IMAGE_BUCKET_NAME_PHONE:Ljava/lang/String;

    invoke-direct {v2, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 351
    .local v2, "dir":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v14

    if-nez v14, :cond_0

    .line 352
    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    .line 353
    :cond_0
    new-instance v5, Ljava/io/File;

    sget-object v14, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->CAMERA_IMAGE_BUCKET_NAME_PHONE:Ljava/lang/String;

    invoke-direct {v5, v14, v7}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    .local v5, "file":Ljava/io/File;
    new-instance v14, Ljava/io/FileOutputStream;

    invoke-direct {v14, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->outputStream:Ljava/io/FileOutputStream;

    .line 355
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->outputStream:Ljava/io/FileOutputStream;

    if-eqz v14, :cond_1

    if-eqz p1, :cond_1

    .line 356
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->outputStream:Ljava/io/FileOutputStream;

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 370
    :cond_1
    :try_start_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->outputStream:Ljava/io/FileOutputStream;

    if-eqz v14, :cond_2

    .line 371
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->outputStream:Ljava/io/FileOutputStream;

    invoke-virtual {v14}, Ljava/io/FileOutputStream;->close()V

    .line 373
    :cond_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mCameraState:[I
    invoke-static {v14}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->access$200(Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;)[I

    move-result-object v14

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->mWhichCamera:I

    aget v14, v14, v15

    const/4 v15, 0x2

    if-eq v14, v15, :cond_3

    .line 374
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mCameraState:[I
    invoke-static {v14}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->access$200(Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;)[I

    move-result-object v14

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->mWhichCamera:I

    const/16 v16, 0x3

    aput v16, v14, v15
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 386
    .end local v2    # "dir":Ljava/io/File;
    .end local v5    # "file":Ljava/io/File;
    :cond_3
    :goto_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mCameraState:[I
    invoke-static {v14}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->access$200(Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;)[I

    move-result-object v14

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->mWhichCamera:I

    aget v14, v14, v15

    const/4 v15, 0x3

    if-ne v14, v15, :cond_5

    .line 387
    new-instance v13, Landroid/content/ContentValues;

    const/4 v14, 0x5

    invoke-direct {v13, v14}, Landroid/content/ContentValues;-><init>(I)V

    .line 388
    .local v13, "values":Landroid/content/ContentValues;
    const-string v14, "_display_name"

    invoke-virtual {v13, v14, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    const-string v14, "datetaken"

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    invoke-virtual {v13, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 390
    const-string v14, "mime_type"

    const-string v15, "image/jpeg"

    invoke-virtual {v13, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    const-string v14, "_data"

    invoke-virtual {v13, v14, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    const-string v14, "orientation"

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    # invokes: Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->getOrientationOnTake()I
    invoke-static {v15}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->access$300(Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;)I

    move-result v15

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v13, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 394
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mContentResolver:Landroid/content/ContentResolver;
    invoke-static {v14}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->access$400(Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;)Landroid/content/ContentResolver;

    move-result-object v14

    sget-object v15, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v14, v15, v13}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v12

    .line 396
    if-nez v12, :cond_b

    .line 397
    const-string v14, "SafetyAssuranceCameraCapturer"

    const-string v15, "onPictureTaken - Insert was failed!"

    invoke-static {v14, v15}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 405
    :cond_4
    :goto_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    iget-object v14, v14, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v14}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 406
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    iget-object v14, v14, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->CaptureLock:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v14}, Ljava/util/concurrent/locks/Condition;->signal()V

    .line 407
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    iget-object v14, v14, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v14}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 410
    .end local v13    # "values":Landroid/content/ContentValues;
    :cond_5
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->mWhichCamera:I

    const/4 v15, 0x1

    if-ne v14, v15, :cond_c

    .line 412
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mSAService:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;
    invoke-static {v14}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->access$600(Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;)Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    move-result-object v14

    invoke-virtual {v14, v12}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->setFrontPictureUri(Landroid/net/Uri;)V

    .line 420
    :goto_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    invoke-virtual {v14}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->isTakingPicturesOver()Z

    move-result v14

    const/4 v15, 0x1

    if-ne v14, v15, :cond_6

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mFirstPictures:Z
    invoke-static {v14}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->access$700(Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;)Z

    move-result v14

    if-nez v14, :cond_6

    .line 421
    const-string v14, "SafetyAssuranceCameraCapturer"

    const-string v15, "onPictureTaken - Send msg"

    invoke-static {v14, v15}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 422
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    iget-object v14, v14, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mFinishCallbackHandler:Landroid/os/Handler;

    invoke-virtual {v14}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v9

    .line 423
    .local v9, "msg":Landroid/os/Message;
    const/4 v14, 0x2

    iput v14, v9, Landroid/os/Message;->what:I

    .line 424
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    iget-object v14, v14, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mFinishCallbackHandler:Landroid/os/Handler;

    invoke-virtual {v14, v9}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 427
    .end local v9    # "msg":Landroid/os/Message;
    :cond_6
    return-void

    .line 376
    .restart local v2    # "dir":Ljava/io/File;
    .restart local v5    # "file":Ljava/io/File;
    :catch_0
    move-exception v3

    .line 378
    .local v3, "e":Ljava/io/IOException;
    const-string v14, "SafetyAssuranceCameraCapturer"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "IOException"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 379
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    iget-object v14, v14, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v14}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 380
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    iget-object v14, v14, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->CaptureLock:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v14}, Ljava/util/concurrent/locks/Condition;->signal()V

    .line 381
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    iget-object v14, v14, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v14}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 382
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mCameraState:[I
    invoke-static {v14}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->access$200(Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;)[I

    move-result-object v14

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->mWhichCamera:I

    const/16 v16, 0x2

    aput v16, v14, v15

    goto/16 :goto_0

    .line 357
    .end local v2    # "dir":Ljava/io/File;
    .end local v3    # "e":Ljava/io/IOException;
    .end local v5    # "file":Ljava/io/File;
    :catch_1
    move-exception v4

    .line 358
    .local v4, "ex":Ljava/io/IOException;
    :try_start_2
    const-string v14, "No space left on device"

    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 359
    const-string v14, "SafetyAssuranceCameraCapturer"

    const-string v15, "IOException - Out of primary external storage!!"

    invoke-static {v14, v15}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 363
    :goto_3
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    .line 364
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    iget-object v14, v14, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v14}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 365
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    iget-object v14, v14, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->CaptureLock:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v14}, Ljava/util/concurrent/locks/Condition;->signal()V

    .line 366
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    iget-object v14, v14, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v14}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 367
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mCameraState:[I
    invoke-static {v14}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->access$200(Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;)[I

    move-result-object v14

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->mWhichCamera:I

    const/16 v16, 0x2

    aput v16, v14, v15
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 370
    :try_start_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->outputStream:Ljava/io/FileOutputStream;

    if-eqz v14, :cond_7

    .line 371
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->outputStream:Ljava/io/FileOutputStream;

    invoke-virtual {v14}, Ljava/io/FileOutputStream;->close()V

    .line 373
    :cond_7
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mCameraState:[I
    invoke-static {v14}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->access$200(Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;)[I

    move-result-object v14

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->mWhichCamera:I

    aget v14, v14, v15

    const/4 v15, 0x2

    if-eq v14, v15, :cond_3

    .line 374
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mCameraState:[I
    invoke-static {v14}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->access$200(Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;)[I

    move-result-object v14

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->mWhichCamera:I

    const/16 v16, 0x3

    aput v16, v14, v15
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_0

    .line 376
    :catch_2
    move-exception v3

    .line 378
    .restart local v3    # "e":Ljava/io/IOException;
    const-string v14, "SafetyAssuranceCameraCapturer"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "IOException"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 379
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    iget-object v14, v14, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v14}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 380
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    iget-object v14, v14, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->CaptureLock:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v14}, Ljava/util/concurrent/locks/Condition;->signal()V

    .line 381
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    iget-object v14, v14, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v14}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 382
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mCameraState:[I
    invoke-static {v14}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->access$200(Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;)[I

    move-result-object v14

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->mWhichCamera:I

    const/16 v16, 0x2

    aput v16, v14, v15

    goto/16 :goto_0

    .line 361
    .end local v3    # "e":Ljava/io/IOException;
    :cond_8
    :try_start_4
    const-string v14, "SafetyAssuranceCameraCapturer"

    const-string v15, "IOException occur"

    invoke-static {v14, v15}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_3

    .line 369
    .end local v4    # "ex":Ljava/io/IOException;
    :catchall_0
    move-exception v14

    .line 370
    :try_start_5
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->outputStream:Ljava/io/FileOutputStream;

    if-eqz v15, :cond_9

    .line 371
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->outputStream:Ljava/io/FileOutputStream;

    invoke-virtual {v15}, Ljava/io/FileOutputStream;->close()V

    .line 373
    :cond_9
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mCameraState:[I
    invoke-static {v15}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->access$200(Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;)[I

    move-result-object v15

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->mWhichCamera:I

    move/from16 v16, v0

    aget v15, v15, v16

    const/16 v16, 0x2

    move/from16 v0, v16

    if-eq v15, v0, :cond_a

    .line 374
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mCameraState:[I
    invoke-static {v15}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->access$200(Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;)[I

    move-result-object v15

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->mWhichCamera:I

    move/from16 v16, v0

    const/16 v17, 0x3

    aput v17, v15, v16
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 383
    :cond_a
    :goto_4
    throw v14

    .line 376
    :catch_3
    move-exception v3

    .line 378
    .restart local v3    # "e":Ljava/io/IOException;
    const-string v15, "SafetyAssuranceCameraCapturer"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "IOException"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 379
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    iget-object v15, v15, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v15}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 380
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    iget-object v15, v15, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->CaptureLock:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v15}, Ljava/util/concurrent/locks/Condition;->signal()V

    .line 381
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    iget-object v15, v15, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v15}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 382
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mCameraState:[I
    invoke-static {v15}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->access$200(Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;)[I

    move-result-object v15

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->mWhichCamera:I

    move/from16 v16, v0

    const/16 v17, 0x2

    aput v17, v15, v16

    goto :goto_4

    .line 399
    .end local v3    # "e":Ljava/io/IOException;
    .restart local v13    # "values":Landroid/content/ContentValues;
    :cond_b
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mActivityContext:Landroid/content/Context;
    invoke-static {v14}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->access$500(Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;)Landroid/content/Context;

    move-result-object v14

    if-eqz v14, :cond_4

    .line 400
    new-instance v8, Landroid/content/Intent;

    const-string v14, "com.android.camera.NEW_PICTURE"

    invoke-direct {v8, v14}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 401
    .local v8, "intent":Landroid/content/Intent;
    invoke-virtual {v8, v12}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 402
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mActivityContext:Landroid/content/Context;
    invoke-static {v14}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->access$500(Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;)Landroid/content/Context;

    move-result-object v14

    invoke-virtual {v14, v8}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 415
    .end local v8    # "intent":Landroid/content/Intent;
    .end local v13    # "values":Landroid/content/ContentValues;
    :cond_c
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$JpegPictureCallback;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mSAService:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;
    invoke-static {v14}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->access$600(Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;)Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    move-result-object v14

    invoke-virtual {v14, v12}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->setRearPictureUri(Landroid/net/Uri;)V

    goto/16 :goto_2
.end method
