.class final Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$ErrorCallback;
.super Ljava/lang/Object;
.source "SafetyAssuranceCameraCapturer.java"

# interfaces
.implements Lcom/sec/android/seccamera/SecCamera$ErrorCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ErrorCallback"
.end annotation


# static fields
.field private static final CAMERA_ERROR_MSG_NO_ERROR:I


# instance fields
.field private nWhichCamera:I

.field final synthetic this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;I)V
    .locals 0
    .param p2, "CameraNumber"    # I

    .prologue
    .line 438
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$ErrorCallback;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 439
    iput p2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$ErrorCallback;->nWhichCamera:I

    .line 440
    return-void
.end method


# virtual methods
.method public onError(ILcom/sec/android/seccamera/SecCamera;)V
    .locals 3
    .param p1, "error"    # I
    .param p2, "camera"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 443
    const-string v0, "SafetyAssuranceCameraCapturer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ErrorCallback.onError ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 444
    packed-switch p1, :pswitch_data_0

    .line 448
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$ErrorCallback;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    iget-object v0, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 449
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$ErrorCallback;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    iget-object v0, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->CaptureLock:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signal()V

    .line 450
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$ErrorCallback;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    iget-object v0, v0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 451
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$ErrorCallback;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->mCameraState:[I
    invoke-static {v0}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;->access$200(Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer;)[I

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceCameraCapturer$ErrorCallback;->nWhichCamera:I

    const/4 v2, 0x1

    aput v2, v0, v1

    .line 455
    :pswitch_0
    return-void

    .line 444
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
