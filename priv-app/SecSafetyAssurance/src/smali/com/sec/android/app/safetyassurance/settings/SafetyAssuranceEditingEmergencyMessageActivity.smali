.class public Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;
.super Landroid/app/Activity;
.source "SafetyAssuranceEditingEmergencyMessageActivity.java"


# static fields
.field private static final CANCEL:I = 0x3

.field private static final CANCEL_NO_ICON:I = 0x4

.field private static final LOG_TAG:Ljava/lang/String; = "SafetyAssuaranceEditingEmergencyMessageActivity"

.field private static final MAX_LENGTH:I = 0x64

.field private static final MAX_LENGTH_SKT:I = 0x50

.field public static final REJECT_MESSAGE_RESULT:I = 0x3

.field private static final SAVE:I = 0x1

.field private static final SAVE_NO_ICON:I = 0x2


# instance fields
.field bytesText:Landroid/widget/TextView;

.field private handler:Landroid/os/Handler;

.field private imm:Landroid/view/inputmethod/InputMethodManager;

.field istoastshowing:Z

.field mCreateEdit:Landroid/widget/EditText;

.field private mToast:Landroid/widget/Toast;

.field private prevString:Ljava/lang/String;

.field selectedItem:I

.field selectedMessage:Ljava/lang/String;

.field titleString:Ljava/lang/CharSequence;

.field updateMODE:Z

.field update_ID:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 58
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;->istoastshowing:Z

    .line 80
    new-instance v0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity$1;-><init>(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;->handler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;)Landroid/view/inputmethod/InputMethodManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;->imm:Landroid/view/inputmethod/InputMethodManager;

    return-object v0
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 242
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;->invalidateOptionsMenu()V

    .line 243
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 244
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x1

    .line 93
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 94
    const-string v2, "SafetyAssuaranceEditingEmergencyMessageActivity"

    const-string v3, "OnCreate ============== :"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    const v2, 0x7f030001

    invoke-virtual {p0, v2}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;->setContentView(I)V

    .line 97
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 98
    .local v0, "actionBar":Landroid/app/ActionBar;
    if-eqz v0, :cond_0

    .line 100
    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 101
    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 102
    const v2, 0x7f090026

    invoke-virtual {p0, v2}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 109
    :cond_0
    const v2, 0x7f0b0001

    invoke-virtual {p0, v2}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;->mCreateEdit:Landroid/widget/EditText;

    .line 111
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "the_string_of_emergency_message"

    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 115
    .local v1, "messageText":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;->mCreateEdit:Landroid/widget/EditText;

    if-eqz v2, :cond_1

    .line 116
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 117
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;->mCreateEdit:Landroid/widget/EditText;

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 121
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;->mCreateEdit:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->requestFocus()Z

    .line 124
    :cond_1
    const-string v2, "input_method"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/inputmethod/InputMethodManager;

    iput-object v2, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;->imm:Landroid/view/inputmethod/InputMethodManager;

    .line 125
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;->handler:Landroid/os/Handler;

    new-instance v3, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity$2;-><init>(Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;)V

    const-wide/16 v4, 0xc8

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 138
    return-void

    .line 119
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;->mCreateEdit:Landroid/widget/EditText;

    const v3, 0x7f09004f

    invoke-virtual {p0, v3}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 8
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v7, 0x7f090036

    const/high16 v3, 0x1040000

    const/4 v6, 0x6

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 198
    const/4 v0, 0x0

    .line 199
    .local v0, "cancelOrder":I
    const/4 v1, 0x1

    .line 201
    .local v1, "saveOrder":I
    const/4 v2, 0x4

    invoke-interface {p1, v4, v2, v0, v3}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v5}, Landroid/view/MenuItem;->setShowAsActionFlags(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 203
    const/4 v2, 0x3

    invoke-interface {p1, v4, v2, v0, v3}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v2

    const v3, 0x7f020008

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v6}, Landroid/view/MenuItem;->setShowAsActionFlags(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 206
    const/4 v2, 0x2

    invoke-interface {p1, v4, v2, v1, v7}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v5}, Landroid/view/MenuItem;->setShowAsActionFlags(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 208
    invoke-interface {p1, v4, v5, v1, v7}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v2

    const v3, 0x7f020009

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v6}, Landroid/view/MenuItem;->setShowAsActionFlags(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 212
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 213
    return v5
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v1, 0x0

    .line 143
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 165
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 145
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;->finish()V

    goto :goto_0

    .line 149
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;->mCreateEdit:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 150
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;->mToast:Landroid/widget/Toast;

    if-nez v0, :cond_1

    .line 151
    const v0, 0x7f090014

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;->mToast:Landroid/widget/Toast;

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 153
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;->mToast:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-nez v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 157
    :cond_2
    invoke-virtual {p0, v1}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;->softkeyLeftRun(Landroid/view/View;)V

    goto :goto_0

    .line 162
    :sswitch_2
    invoke-virtual {p0, v1}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;->softkeyRightRun(Landroid/view/View;)V

    goto :goto_0

    .line 143
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x4 -> :sswitch_2
        0x102002c -> :sswitch_0
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 6
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v5, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 220
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    if-ne v3, v5, :cond_1

    move v0, v1

    .line 229
    .local v0, "isLand":Z
    :goto_0
    const/4 v3, 0x4

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    if-nez v0, :cond_2

    move v3, v1

    :goto_1
    invoke-interface {v4, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 230
    const/4 v3, 0x3

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 231
    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    if-nez v0, :cond_0

    move v2, v1

    :cond_0
    invoke-interface {v3, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 232
    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 235
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    .line 236
    return v1

    .end local v0    # "isLand":Z
    :cond_1
    move v0, v2

    .line 220
    goto :goto_0

    .restart local v0    # "isLand":Z
    :cond_2
    move v3, v2

    .line 229
    goto :goto_1
.end method

.method public softkeyLeftRun(Landroid/view/View;)V
    .locals 3
    .param p1, "target"    # Landroid/view/View;

    .prologue
    .line 175
    const-string v1, "SafetyAssuaranceEditingEmergencyMessageActivity"

    const-string v2, "softkeyLeftRun - softkeyLeftRun()"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;->mCreateEdit:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 177
    .local v0, "tempStr":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "the_string_of_emergency_message"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 178
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;->finish()V

    .line 179
    return-void
.end method

.method public softkeyRightRun(Landroid/view/View;)V
    .locals 0
    .param p1, "target"    # Landroid/view/View;

    .prologue
    .line 186
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceEditingEmergencyMessageActivity;->finish()V

    .line 187
    return-void
.end method
