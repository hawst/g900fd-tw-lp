.class Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$7$1;
.super Ljava/lang/Object;
.source "SafetyAssuranceService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$7;->onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$7;

.field final synthetic val$service:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$7;Landroid/os/IBinder;)V
    .locals 0

    .prologue
    .line 1016
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$7$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$7;

    iput-object p2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$7$1;->val$service:Landroid/os/IBinder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 1019
    const-string v1, "SafetyAssuranceService"

    const-string v2, "Send Emergency Message - START"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1021
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$7$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$7;

    iget-object v1, v1, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$7;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$7$1;->val$service:Landroid/os/IBinder;

    invoke-static {v2}, Lcom/android/mms/transaction/IMessageBackgroundSender$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/mms/transaction/IMessageBackgroundSender;

    move-result-object v2

    # setter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mServiceBinder:Lcom/android/mms/transaction/IMessageBackgroundSender;
    invoke-static {v1, v2}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$1302(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Lcom/android/mms/transaction/IMessageBackgroundSender;)Lcom/android/mms/transaction/IMessageBackgroundSender;

    .line 1022
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$7$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$7;

    iget-object v1, v1, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$7;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    const/4 v2, 0x1

    # setter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mSvcConnected:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$1402(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Z)Z

    .line 1025
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$7$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$7;

    iget-object v1, v1, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$7;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mServiceBinder:Lcom/android/mms/transaction/IMessageBackgroundSender;
    invoke-static {v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$1300(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Lcom/android/mms/transaction/IMessageBackgroundSender;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1026
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$7$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$7;

    iget-object v1, v1, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$7;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mServiceBinder:Lcom/android/mms/transaction/IMessageBackgroundSender;
    invoke-static {v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$1300(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Lcom/android/mms/transaction/IMessageBackgroundSender;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/mms/transaction/IMessageBackgroundSender;->startListening()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1031
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$7$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$7;

    iget-object v1, v1, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$7;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # invokes: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->sendEmergencyMessage()V
    invoke-static {v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$500(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)V

    .line 1033
    const-string v1, "SafetyAssuranceService"

    const-string v2, "Send Emergency Message - END"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1034
    return-void

    .line 1028
    :catch_0
    move-exception v0

    .line 1029
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
