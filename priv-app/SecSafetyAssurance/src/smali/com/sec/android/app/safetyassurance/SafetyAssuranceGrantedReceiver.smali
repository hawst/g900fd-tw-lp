.class public Lcom/sec/android/app/safetyassurance/SafetyAssuranceGrantedReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SafetyAssuranceGrantedReceiver.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SafetyAssuranceGrantedReceiver"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 32
    const-string v0, "SafetyAssuranceGrantedReceiver"

    const-string v1, "onReceive"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 33
    const-string v0, "SafetyAssuranceGrantedReceiver"

    const-string v1, "Granted receiver do nothing"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 34
    return-void
.end method
