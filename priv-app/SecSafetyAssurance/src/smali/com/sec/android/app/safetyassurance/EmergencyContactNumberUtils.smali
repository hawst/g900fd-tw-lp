.class public Lcom/sec/android/app/safetyassurance/EmergencyContactNumberUtils;
.super Ljava/lang/Object;
.source "EmergencyContactNumberUtils.java"


# static fields
.field private static final MAX_PERSON:I = 0xa

.field public static final TYPE_EMERGENCY_CONTACT:I = 0x1

.field public static final TYPE_FAVORITE_CONTACT:I = 0x2

.field public static final TYPE_FREQUENT_CONTACT:I = 0x3


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getContactNumbers(Landroid/content/Context;I)Ljava/lang/String;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "type"    # I

    .prologue
    .line 21
    const/4 v3, 0x0

    .line 22
    .local v3, "lastUsedContactIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    const/4 v4, 0x0

    .line 23
    .local v4, "number":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, ""

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 24
    .local v5, "str":Ljava/lang/StringBuilder;
    invoke-static {p0, p1}, Lcom/sec/android/app/safetyassurance/EmergencyContactNumberUtils;->getLastUsedContactIds(Landroid/content/Context;I)Ljava/util/List;

    move-result-object v3

    .line 26
    if-eqz v3, :cond_0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    if-gtz v6, :cond_1

    .line 27
    :cond_0
    const/4 v6, 0x0

    .line 39
    :goto_0
    return-object v6

    .line 30
    :cond_1
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 31
    .local v0, "contactId":J
    invoke-static {p0, v0, v1}, Lcom/sec/android/app/safetyassurance/EmergencyContactNumberUtils;->getDefaultOrLastUsedNumber(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v4

    .line 32
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 33
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/16 v7, 0x2c

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 37
    .end local v0    # "contactId":J
    :cond_3
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 39
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_0
.end method

.method public static getContactNumbersSize(Landroid/content/Context;I)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "type"    # I

    .prologue
    .line 43
    const/4 v0, 0x0

    .line 44
    .local v0, "lastUsedContactIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-static {p0, p1}, Lcom/sec/android/app/safetyassurance/EmergencyContactNumberUtils;->getLastUsedContactIds(Landroid/content/Context;I)Ljava/util/List;

    move-result-object v0

    .line 46
    if-nez v0, :cond_0

    .line 47
    const/4 v1, -0x1

    .line 50
    :goto_0
    return v1

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    goto :goto_0
.end method

.method private static getDefaultOrLastUsedNumber(Landroid/content/Context;J)Ljava/lang/String;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "contactId"    # J

    .prologue
    const/4 v1, 0x0

    .line 144
    const/4 v8, 0x0

    .line 146
    .local v8, "number":Ljava/lang/String;
    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "data1"

    aput-object v0, v2, v1

    const/4 v0, 0x1

    const-string v1, "display_name"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string v1, "last_time_used"

    aput-object v1, v2, v0

    .line 149
    .local v2, "projection":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mimetype=\'vnd.android.cursor.item/phone_v2\' AND contact_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 151
    .local v3, "selection":Ljava/lang/String;
    const-string v5, "is_super_primary DESC, last_time_used DESC"

    .line 152
    .local v5, "sortOrder":Ljava/lang/String;
    const/4 v6, 0x0

    .line 155
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 156
    if-eqz v6, :cond_0

    .line 157
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 158
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    .line 164
    :cond_0
    if-eqz v6, :cond_1

    .line 165
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 169
    :cond_1
    :goto_0
    return-object v8

    .line 161
    :catch_0
    move-exception v7

    .line 162
    .local v7, "e":Ljava/lang/IllegalArgumentException;
    :try_start_1
    invoke-virtual {v7}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 164
    if-eqz v6, :cond_1

    .line 165
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 164
    .end local v7    # "e":Ljava/lang/IllegalArgumentException;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 165
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method private static getLastUsedContactIds(Landroid/content/Context;I)Ljava/util/List;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 54
    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "contact_id"

    aput-object v0, v2, v1

    const/4 v0, 0x1

    const-string v1, "last_time_used"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string v1, "display_name"

    aput-object v1, v2, v0

    .line 57
    .local v2, "projection":[Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/app/safetyassurance/EmergencyContactNumberUtils;->getSelection(I)Ljava/lang/String;

    move-result-object v3

    .line 58
    .local v3, "selection":Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/app/safetyassurance/EmergencyContactNumberUtils;->getSortOrder(I)Ljava/lang/String;

    move-result-object v5

    .line 60
    .local v5, "sortOrder":Ljava/lang/String;
    const/4 v9, 0x0

    .line 61
    .local v9, "cursor":Landroid/database/Cursor;
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 64
    .local v8, "contactIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 65
    if-eqz v9, :cond_2

    .line 66
    :cond_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 67
    const/4 v0, 0x0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 68
    .local v6, "contactId":J
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 69
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 71
    :cond_1
    invoke-interface {v8}, Ljava/util/List;->size()I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    const/16 v1, 0xa

    if-lt v0, v1, :cond_0

    .line 79
    .end local v6    # "contactId":J
    :cond_2
    if-eqz v9, :cond_3

    .line 80
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 84
    :cond_3
    :goto_0
    return-object v8

    .line 76
    :catch_0
    move-exception v10

    .line 77
    .local v10, "e":Ljava/lang/IllegalArgumentException;
    :try_start_1
    invoke-virtual {v10}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 79
    if-eqz v9, :cond_3

    .line 80
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 79
    .end local v10    # "e":Ljava/lang/IllegalArgumentException;
    :catchall_0
    move-exception v0

    if-eqz v9, :cond_4

    .line 80
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method private static getSelection(I)Ljava/lang/String;
    .locals 4
    .param p0, "type"    # I

    .prologue
    .line 90
    packed-switch p0, :pswitch_data_0

    .line 114
    const/4 v1, 0x0

    .line 118
    .local v1, "selection":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 92
    .end local v1    # "selection":Ljava/lang/String;
    :pswitch_0
    const-string v0, "( SELECT contact_id FROM view_data_restricted WHERE mimetype=\'vnd.android.cursor.item/group_membership\' AND title=\'ICE\')"

    .line 99
    .local v0, "emergencyContactIdSelection":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "contact_id in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mimetype"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "vnd.android.cursor.item/phone_v2"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 101
    .restart local v1    # "selection":Ljava/lang/String;
    goto :goto_0

    .line 104
    .end local v0    # "emergencyContactIdSelection":Ljava/lang/String;
    .end local v1    # "selection":Ljava/lang/String;
    :pswitch_1
    const-string v1, "starred!=0 AND mimetype=\'vnd.android.cursor.item/phone_v2\'"

    .line 106
    .restart local v1    # "selection":Ljava/lang/String;
    goto :goto_0

    .line 109
    .end local v1    # "selection":Ljava/lang/String;
    :pswitch_2
    const-string v1, "times_used>0 AND mimetype=\'vnd.android.cursor.item/phone_v2\'"

    .line 111
    .restart local v1    # "selection":Ljava/lang/String;
    goto :goto_0

    .line 90
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static getSortOrder(I)Ljava/lang/String;
    .locals 1
    .param p0, "type"    # I

    .prologue
    .line 124
    packed-switch p0, :pswitch_data_0

    .line 136
    const/4 v0, 0x0

    .line 140
    .local v0, "sortOrder":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 127
    .end local v0    # "sortOrder":Ljava/lang/String;
    :pswitch_0
    const-string v0, "last_time_used DESC, sort_key"

    .line 128
    .restart local v0    # "sortOrder":Ljava/lang/String;
    goto :goto_0

    .line 131
    .end local v0    # "sortOrder":Ljava/lang/String;
    :pswitch_1
    const-string v0, "times_used DESC, last_time_used DESC, sort_key"

    .line 133
    .restart local v0    # "sortOrder":Ljava/lang/String;
    goto :goto_0

    .line 124
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
