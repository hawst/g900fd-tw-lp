.class Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver$2;
.super Ljava/lang/Object;
.source "SafetyAssuranceReceiver.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;

.field final synthetic val$voiceStr:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 175
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver$2;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;

    iput-object p2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver$2;->val$voiceStr:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 178
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver$2;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->access$000(Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;)Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 179
    .local v0, "startService":Landroid/content/Intent;
    const-string v1, "android.intent.action.SAFETY_SERVICE_START"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 180
    const-string v1, "data"

    iget-object v2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver$2;->val$voiceStr:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 181
    const-string v1, "voice"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 182
    const-string v1, "sos_mode"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 183
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver$2;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;->access$000(Lcom/sec/android/app/safetyassurance/SafetyAssuranceReceiver;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 184
    return-void
.end method
