.class Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1$2;
.super Ljava/lang/Object;
.source "SafetyAssuranceService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->handleMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;


# direct methods
.method constructor <init>(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;)V
    .locals 0

    .prologue
    .line 2380
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1$2;->this$2:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 2384
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1$2;->this$2:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;

    iget-object v1, v1, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v1, v1, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVoiceRecording:Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;
    invoke-static {v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$3700(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;

    move-result-object v2

    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1$2;->this$2:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;

    iget-object v1, v1, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v1, v1, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVocRecordingTime:I
    invoke-static {v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$3800(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)I

    move-result v1

    mul-int/lit16 v1, v1, 0x3e8

    add-int/lit16 v3, v1, 0x1f4

    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1$2;->this$2:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;

    iget-object v1, v1, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v1, v1, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mService:Landroid/app/Service;
    invoke-static {v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$3300(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Landroid/app/Service;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    invoke-virtual {v2, v3, v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->initRecording(ILcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Z

    move-result v0

    .line 2385
    .local v0, "bResult":Z
    if-eqz v0, :cond_0

    .line 2386
    const-string v1, "SafetyAssuranceWorkThread"

    const-string v2, "Voice Recording : Start"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2387
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1$2;->this$2:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;

    iget-object v1, v1, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v1, v1, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    # getter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVoiceRecording:Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;
    invoke-static {v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$3700(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;)Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;->startRecording()Z

    .line 2392
    :goto_0
    return-void

    .line 2389
    :cond_0
    const-string v1, "SafetyAssuranceWorkThread"

    const-string v2, "Voice Recording : Init is failed"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2390
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1$2;->this$2:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;

    iget-object v1, v1, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread$1;->this$1:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;

    iget-object v1, v1, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SafetyAssuranceWorkThread;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->mVoiceRecording:Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;
    invoke-static {v1, v2}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;->access$3702(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;)Lcom/sec/android/app/safetyassurance/SafetyAssuranceVocRec;

    goto :goto_0
.end method
