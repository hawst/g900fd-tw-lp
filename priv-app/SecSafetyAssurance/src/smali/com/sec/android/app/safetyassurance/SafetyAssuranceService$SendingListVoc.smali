.class public Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListVoc;
.super Ljava/lang/Object;
.source "SafetyAssuranceService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SendingListVoc"
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String; = "SendingListVoc"


# instance fields
.field private mVocRec:Z

.field final synthetic this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;Z)V
    .locals 3
    .param p2, "bRec"    # Z

    .prologue
    .line 2724
    iput-object p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListVoc;->this$0:Lcom/sec/android/app/safetyassurance/SafetyAssuranceService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2725
    const-string v0, "SendingListVoc"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Init VocRec["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2726
    iput-boolean p2, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListVoc;->mVocRec:Z

    .line 2727
    return-void
.end method


# virtual methods
.method public isSendingVocComplete()Z
    .locals 1

    .prologue
    .line 2736
    monitor-enter p0

    .line 2737
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListVoc;->mVocRec:Z

    if-nez v0, :cond_0

    .line 2738
    const/4 v0, 0x1

    monitor-exit p0

    .line 2740
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    monitor-exit p0

    goto :goto_0

    .line 2742
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setVocRec(Z)V
    .locals 1
    .param p1, "bRec"    # Z

    .prologue
    .line 2730
    monitor-enter p0

    .line 2731
    :try_start_0
    iput-boolean p1, p0, Lcom/sec/android/app/safetyassurance/SafetyAssuranceService$SendingListVoc;->mVocRec:Z

    .line 2732
    monitor-exit p0

    .line 2733
    return-void

    .line 2732
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
