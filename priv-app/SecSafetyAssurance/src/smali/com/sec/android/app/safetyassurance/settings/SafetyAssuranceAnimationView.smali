.class public Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;
.super Landroid/widget/ImageView;
.source "SafetyAssuranceAnimationView.java"


# annotations
.annotation runtime Landroid/widget/RemoteViews$RemoteView;
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SafetyAssuranceAnimationView"


# instance fields
.field private isOnWindow:Z

.field private isThisEnabled:Z

.field private mADrawable:Landroid/graphics/drawable/AnimationDrawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 38
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 40
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->featureSupportNewUX()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 42
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->featureUpTriggerKey()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43
    const v0, 0x7f040003

    invoke-virtual {p0, v0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;->setImageResource(I)V

    .line 55
    :goto_0
    return-void

    .line 45
    :cond_0
    const v0, 0x7f040002

    invoke-virtual {p0, v0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;->setImageResource(I)V

    goto :goto_0

    .line 49
    :cond_1
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->featureRightTriggerKey()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 50
    const v0, 0x7f040001

    invoke-virtual {p0, v0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;->setImageResource(I)V

    goto :goto_0

    .line 52
    :cond_2
    const/high16 v0, 0x7f040000

    invoke-virtual {p0, v0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;->setImageResource(I)V

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 60
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->featureSupportNewUX()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 61
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->featureUpTriggerKey()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    const v0, 0x7f040003

    invoke-virtual {p0, v0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;->setImageResource(I)V

    .line 75
    :goto_0
    return-void

    .line 65
    :cond_0
    const v0, 0x7f040002

    invoke-virtual {p0, v0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;->setImageResource(I)V

    goto :goto_0

    .line 69
    :cond_1
    invoke-static {}, Lcom/sec/android/app/safetyassurance/SafetyAssuranceAppFeatures;->featureRightTriggerKey()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 70
    const v0, 0x7f040001

    invoke-virtual {p0, v0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;->setImageResource(I)V

    goto :goto_0

    .line 72
    :cond_2
    const/high16 v0, 0x7f040000

    invoke-virtual {p0, v0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;->setImageResource(I)V

    goto :goto_0
.end method

.method private startAnimation()V
    .locals 2

    .prologue
    .line 130
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 131
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    iget-boolean v1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;->isOnWindow:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;->mADrawable:Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v1, :cond_0

    .line 132
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;->mADrawable:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    .line 134
    :cond_0
    instance-of v1, v0, Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v1, :cond_3

    .line 135
    check-cast v0, Landroid/graphics/drawable/AnimationDrawable;

    .end local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    iput-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;->mADrawable:Landroid/graphics/drawable/AnimationDrawable;

    .line 137
    iget-boolean v1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;->isOnWindow:Z

    if-eqz v1, :cond_1

    .line 138
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;->clearColorFilter()V

    .line 139
    iget-object v1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;->mADrawable:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 141
    :cond_1
    iget-boolean v1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;->isThisEnabled:Z

    if-nez v1, :cond_2

    .line 142
    const v1, -0x777778

    invoke-virtual {p0, v1}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;->setColorFilter(I)V

    .line 147
    :cond_2
    :goto_0
    return-void

    .line 145
    .restart local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_3
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;->mADrawable:Landroid/graphics/drawable/AnimationDrawable;

    goto :goto_0
.end method


# virtual methods
.method protected finalize()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 111
    invoke-virtual {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;->clearAnimation()V

    .line 112
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 113
    return-void
.end method

.method public onAttachedToWindow()V
    .locals 1

    .prologue
    .line 93
    invoke-super {p0}, Landroid/widget/ImageView;->onAttachedToWindow()V

    .line 94
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;->mADrawable:Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;->mADrawable:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 97
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;->isOnWindow:Z

    .line 98
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 102
    invoke-super {p0}, Landroid/widget/ImageView;->onDetachedFromWindow()V

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;->mADrawable:Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;->mADrawable:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    .line 106
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;->isOnWindow:Z

    .line 107
    return-void
.end method

.method public setEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 119
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 121
    if-eqz p1, :cond_0

    .line 122
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;->isThisEnabled:Z

    .line 126
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;->startAnimation()V

    .line 127
    return-void

    .line 124
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/safetyassurance/settings/SafetyAssuranceAnimationView;->isThisEnabled:Z

    goto :goto_0
.end method

.method public setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 80
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 82
    return-void
.end method

.method public setImageResource(I)V
    .locals 1
    .param p1, "resid"    # I

    .prologue
    .line 86
    const/4 v0, 0x0

    invoke-super {p0, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 87
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 89
    return-void
.end method
