.class public Lcom/android/apps/tag/record/MimeRecord;
.super Lcom/android/apps/tag/record/ParsedNdefRecord;
.source "MimeRecord.java"


# instance fields
.field private final mContent:[B

.field private final mType:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;[B)V
    .locals 1
    .param p1, "mimeType"    # Ljava/lang/String;
    .param p2, "content"    # [B

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/android/apps/tag/record/ParsedNdefRecord;-><init>()V

    .line 43
    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/android/apps/tag/record/MimeRecord;->mType:Ljava/lang/String;

    .line 44
    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    array-length v0, p2

    invoke-static {p2, v0}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v0

    iput-object v0, p0, Lcom/android/apps/tag/record/MimeRecord;->mContent:[B

    .line 46
    return-void
.end method

.method public static isMime(Landroid/nfc/NdefRecord;)Z
    .locals 1
    .param p0, "record"    # Landroid/nfc/NdefRecord;

    .prologue
    .line 76
    invoke-virtual {p0}, Landroid/nfc/NdefRecord;->toMimeType()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parse(Landroid/nfc/NdefRecord;)Lcom/android/apps/tag/record/MimeRecord;
    .locals 3
    .param p0, "record"    # Landroid/nfc/NdefRecord;

    .prologue
    .line 71
    invoke-virtual {p0}, Landroid/nfc/NdefRecord;->toMimeType()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    .line 72
    new-instance v0, Lcom/android/apps/tag/record/MimeRecord;

    invoke-virtual {p0}, Landroid/nfc/NdefRecord;->toMimeType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/android/apps/tag/record/MimeRecord;-><init>(Ljava/lang/String;[B)V

    return-object v0

    .line 71
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getContent()[B
    .locals 2

    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/apps/tag/record/MimeRecord;->mContent:[B

    iget-object v1, p0, Lcom/android/apps/tag/record/MimeRecord;->mContent:[B

    array-length v1, v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v0

    return-object v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/android/apps/tag/record/MimeRecord;->mType:Ljava/lang/String;

    return-object v0
.end method

.method public getView(Landroid/app/Activity;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "inflater"    # Landroid/view/LayoutInflater;
    .param p3, "parent"    # Landroid/view/ViewGroup;
    .param p4, "offset"    # I

    .prologue
    .line 60
    const v1, 0x7f030002

    const/4 v2, 0x0

    invoke-virtual {p2, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 61
    .local v0, "text":Landroid/widget/TextView;
    iget-object v1, p0, Lcom/android/apps/tag/record/MimeRecord;->mType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 62
    return-object v0
.end method
