.class public Lcom/android/apps/tag/message/NdefMessageParser;
.super Ljava/lang/Object;
.source "NdefMessageParser.java"


# direct methods
.method public static getRecords(Landroid/nfc/NdefMessage;)Ljava/util/List;
    .locals 1
    .param p0, "message"    # Landroid/nfc/NdefMessage;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/nfc/NdefMessage;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/android/apps/tag/record/ParsedNdefRecord;",
            ">;"
        }
    .end annotation

    .prologue
    .line 48
    invoke-virtual {p0}, Landroid/nfc/NdefMessage;->getRecords()[Landroid/nfc/NdefRecord;

    move-result-object v0

    invoke-static {v0}, Lcom/android/apps/tag/message/NdefMessageParser;->getRecords([Landroid/nfc/NdefRecord;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static getRecords([Landroid/nfc/NdefRecord;)Ljava/util/List;
    .locals 6
    .param p0, "records"    # [Landroid/nfc/NdefRecord;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Landroid/nfc/NdefRecord;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/android/apps/tag/record/ParsedNdefRecord;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 53
    .local v1, "elements":Ljava/util/List;, "Ljava/util/List<Lcom/android/apps/tag/record/ParsedNdefRecord;>;"
    move-object v0, p0

    .local v0, "arr$":[Landroid/nfc/NdefRecord;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_6

    aget-object v4, v0, v2

    .line 54
    .local v4, "record":Landroid/nfc/NdefRecord;
    invoke-static {v4}, Lcom/android/apps/tag/record/SmartPoster;->isPoster(Landroid/nfc/NdefRecord;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 55
    invoke-static {v4}, Lcom/android/apps/tag/record/SmartPoster;->parse(Landroid/nfc/NdefRecord;)Lcom/android/apps/tag/record/SmartPoster;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 53
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 56
    :cond_0
    invoke-static {v4}, Lcom/android/apps/tag/record/UriRecord;->isUri(Landroid/nfc/NdefRecord;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 57
    invoke-static {v4}, Lcom/android/apps/tag/record/UriRecord;->parse(Landroid/nfc/NdefRecord;)Lcom/android/apps/tag/record/UriRecord;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 58
    :cond_1
    invoke-static {v4}, Lcom/android/apps/tag/record/TextRecord;->isText(Landroid/nfc/NdefRecord;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 59
    invoke-static {v4}, Lcom/android/apps/tag/record/TextRecord;->parse(Landroid/nfc/NdefRecord;)Lcom/android/apps/tag/record/TextRecord;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 60
    :cond_2
    invoke-static {v4}, Lcom/android/apps/tag/record/ImageRecord;->isImage(Landroid/nfc/NdefRecord;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 61
    invoke-static {v4}, Lcom/android/apps/tag/record/ImageRecord;->parse(Landroid/nfc/NdefRecord;)Lcom/android/apps/tag/record/ImageRecord;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 62
    :cond_3
    invoke-static {v4}, Lcom/android/apps/tag/record/VCardRecord;->isVCard(Landroid/nfc/NdefRecord;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 63
    invoke-static {v4}, Lcom/android/apps/tag/record/VCardRecord;->parse(Landroid/nfc/NdefRecord;)Lcom/android/apps/tag/record/VCardRecord;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 64
    :cond_4
    invoke-static {v4}, Lcom/android/apps/tag/record/MimeRecord;->isMime(Landroid/nfc/NdefRecord;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 65
    invoke-static {v4}, Lcom/android/apps/tag/record/MimeRecord;->parse(Landroid/nfc/NdefRecord;)Lcom/android/apps/tag/record/MimeRecord;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 67
    :cond_5
    new-instance v5, Lcom/android/apps/tag/record/UnknownRecord;

    invoke-direct {v5}, Lcom/android/apps/tag/record/UnknownRecord;-><init>()V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 70
    .end local v4    # "record":Landroid/nfc/NdefRecord;
    :cond_6
    return-object v1
.end method

.method public static parse(Landroid/nfc/NdefMessage;)Lcom/android/apps/tag/message/ParsedNdefMessage;
    .locals 2
    .param p0, "message"    # Landroid/nfc/NdefMessage;

    .prologue
    .line 44
    new-instance v0, Lcom/android/apps/tag/message/ParsedNdefMessage;

    invoke-static {p0}, Lcom/android/apps/tag/message/NdefMessageParser;->getRecords(Landroid/nfc/NdefMessage;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/apps/tag/message/ParsedNdefMessage;-><init>(Ljava/util/List;)V

    return-object v0
.end method
