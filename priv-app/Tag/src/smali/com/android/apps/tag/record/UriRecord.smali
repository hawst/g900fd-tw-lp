.class public Lcom/android/apps/tag/record/UriRecord;
.super Lcom/android/apps/tag/record/ParsedNdefRecord;
.source "UriRecord.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private mDialWaringDialog:Landroid/app/AlertDialog;

.field private mSmsWaringDialog:Landroid/app/AlertDialog;

.field private final mUri:Landroid/net/Uri;


# direct methods
.method private constructor <init>(Landroid/net/Uri;)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/android/apps/tag/record/ParsedNdefRecord;-><init>()V

    .line 75
    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/android/apps/tag/record/UriRecord;->mUri:Landroid/net/Uri;

    .line 76
    return-void
.end method

.method public static isUri(Landroid/nfc/NdefRecord;)Z
    .locals 1
    .param p0, "record"    # Landroid/nfc/NdefRecord;

    .prologue
    .line 242
    invoke-virtual {p0}, Landroid/nfc/NdefRecord;->toUri()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parse(Landroid/nfc/NdefRecord;)Lcom/android/apps/tag/record/UriRecord;
    .locals 3
    .param p0, "record"    # Landroid/nfc/NdefRecord;

    .prologue
    .line 236
    invoke-virtual {p0}, Landroid/nfc/NdefRecord;->toUri()Landroid/net/Uri;

    move-result-object v0

    .line 237
    .local v0, "uri":Landroid/net/Uri;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "not a uri"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 238
    :cond_0
    new-instance v1, Lcom/android/apps/tag/record/UriRecord;

    invoke-direct {v1, v0}, Lcom/android/apps/tag/record/UriRecord;-><init>(Landroid/net/Uri;)V

    return-object v1
.end method


# virtual methods
.method public getIntentForUri()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 79
    iget-object v1, p0, Lcom/android/apps/tag/record/UriRecord;->mUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    .line 80
    .local v0, "scheme":Ljava/lang/String;
    const-string v1, "tel"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 81
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.CALL"

    iget-object v3, p0, Lcom/android/apps/tag/record/UriRecord;->mUri:Landroid/net/Uri;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 85
    :goto_0
    return-object v1

    .line 82
    :cond_0
    const-string v1, "sms"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "smsto"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 83
    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.SENDTO"

    iget-object v3, p0, Lcom/android/apps/tag/record/UriRecord;->mUri:Landroid/net/Uri;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    goto :goto_0

    .line 85
    :cond_2
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    iget-object v3, p0, Lcom/android/apps/tag/record/UriRecord;->mUri:Landroid/net/Uri;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public getPrettyUriString(Landroid/content/Context;)Ljava/lang/String;
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 90
    iget-object v7, p0, Lcom/android/apps/tag/record/UriRecord;->mUri:Landroid/net/Uri;

    invoke-virtual {v7}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    .line 91
    .local v1, "scheme":Ljava/lang/String;
    const-string v7, "tel"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 92
    .local v4, "tel":Z
    const-string v7, "sms"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    const-string v7, "smsto"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    :cond_0
    move v2, v6

    .line 93
    .local v2, "sms":Z
    :goto_0
    if-nez v4, :cond_1

    if-eqz v2, :cond_5

    .line 94
    :cond_1
    iget-object v7, p0, Lcom/android/apps/tag/record/UriRecord;->mUri:Landroid/net/Uri;

    invoke-virtual {v7}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v3

    .line 95
    .local v3, "ssp":Ljava/lang/String;
    const/16 v7, 0x3f

    invoke-virtual {v3, v7}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 96
    .local v0, "offset":I
    if-ltz v0, :cond_2

    .line 97
    invoke-virtual {v3, v5, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 99
    :cond_2
    if-eqz v4, :cond_4

    .line 100
    const v7, 0x7f060007

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v3}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v5

    invoke-virtual {p1, v7, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 105
    .end local v0    # "offset":I
    .end local v3    # "ssp":Ljava/lang/String;
    :goto_1
    return-object v5

    .end local v2    # "sms":Z
    :cond_3
    move v2, v5

    .line 92
    goto :goto_0

    .line 102
    .restart local v0    # "offset":I
    .restart local v2    # "sms":Z
    .restart local v3    # "ssp":Ljava/lang/String;
    :cond_4
    const v7, 0x7f060006

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v3}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v5

    invoke-virtual {p1, v7, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 105
    .end local v0    # "offset":I
    .end local v3    # "ssp":Ljava/lang/String;
    :cond_5
    iget-object v5, p0, Lcom/android/apps/tag/record/UriRecord;->mUri:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_1
.end method

.method public getPrettyUriStringForPopUp(Landroid/content/Context;)Ljava/lang/String;
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    .line 111
    iget-object v6, p0, Lcom/android/apps/tag/record/UriRecord;->mUri:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    .line 112
    .local v1, "scheme":Ljava/lang/String;
    const-string v6, "tel"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 113
    .local v4, "tel":Z
    const-string v6, "sms"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "smsto"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    :cond_0
    const/4 v2, 0x1

    .line 114
    .local v2, "sms":Z
    :goto_0
    if-nez v4, :cond_1

    if-eqz v2, :cond_5

    .line 115
    :cond_1
    iget-object v6, p0, Lcom/android/apps/tag/record/UriRecord;->mUri:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v3

    .line 116
    .local v3, "ssp":Ljava/lang/String;
    const/16 v6, 0x3f

    invoke-virtual {v3, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 117
    .local v0, "offset":I
    if-ltz v0, :cond_2

    .line 118
    invoke-virtual {v3, v5, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 120
    :cond_2
    if-eqz v4, :cond_4

    .line 121
    invoke-static {v3}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 126
    .end local v0    # "offset":I
    .end local v3    # "ssp":Ljava/lang/String;
    :goto_1
    return-object v5

    .end local v2    # "sms":Z
    :cond_3
    move v2, v5

    .line 113
    goto :goto_0

    .line 123
    .restart local v0    # "offset":I
    .restart local v2    # "sms":Z
    .restart local v3    # "ssp":Ljava/lang/String;
    :cond_4
    invoke-static {v3}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 126
    .end local v0    # "offset":I
    .end local v3    # "ssp":Ljava/lang/String;
    :cond_5
    iget-object v5, p0, Lcom/android/apps/tag/record/UriRecord;->mUri:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_1
.end method

.method public getUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/android/apps/tag/record/UriRecord;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getView(Landroid/app/Activity;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 6
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "inflater"    # Landroid/view/LayoutInflater;
    .param p3, "parent"    # Landroid/view/ViewGroup;
    .param p4, "offset"    # I

    .prologue
    .line 133
    invoke-virtual {p0}, Lcom/android/apps/tag/record/UriRecord;->getIntentForUri()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {p0, p1}, Lcom/android/apps/tag/record/UriRecord;->getPrettyUriString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move-object v3, p0

    invoke-static/range {v0 .. v5}, Lcom/android/apps/tag/record/RecordUtils;->getViewsForIntent(Landroid/app/Activity;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/view/View$OnClickListener;Landroid/content/Intent;Ljava/lang/String;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 12
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 144
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v7

    const-string v8, "CscFeature_NFC_EnableSecurityPromptPopup"

    invoke-virtual {v7, v8}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 145
    .local v2, "popup":Ljava/lang/String;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    if-nez v7, :cond_1

    .line 146
    :cond_0
    const-string v2, "none"

    .line 148
    :cond_1
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    const-string v8, "all"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    const-string v8, "call"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    const-string v8, "sms"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 149
    :cond_2
    iget-object v7, p0, Lcom/android/apps/tag/record/UriRecord;->mUri:Landroid/net/Uri;

    invoke-virtual {v7}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    .line 150
    .local v3, "scheme":Ljava/lang/String;
    const-string v7, "tel"

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    .line 151
    .local v5, "tel":Z
    const-string v7, "sms"

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_3

    const-string v7, "smsto"

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    :cond_3
    move v4, v6

    .line 153
    .local v4, "sms":Z
    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/apps/tag/record/RecordUtils$ClickInfo;

    .line 156
    .local v1, "info":Lcom/android/apps/tag/record/RecordUtils$ClickInfo;
    if-eqz v4, :cond_7

    .line 157
    :try_start_0
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    const-string v7, "all"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_5

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    const-string v7, "sms"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 158
    :cond_5
    new-instance v6, Landroid/app/AlertDialog$Builder;

    iget-object v7, v1, Lcom/android/apps/tag/record/RecordUtils$ClickInfo;->activity:Landroid/app/Activity;

    invoke-direct {v6, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v7, 0x7f060010

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x1080027

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    iget-object v7, v1, Lcom/android/apps/tag/record/RecordUtils$ClickInfo;->activity:Landroid/app/Activity;

    const v8, 0x7f060011

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, v1, Lcom/android/apps/tag/record/RecordUtils$ClickInfo;->activity:Landroid/app/Activity;

    invoke-virtual {p0, v11}, Lcom/android/apps/tag/record/UriRecord;->getPrettyUriStringForPopUp(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v7, v8, v9}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f06000e

    new-instance v8, Lcom/android/apps/tag/record/UriRecord$2;

    invoke-direct {v8, p0, v1}, Lcom/android/apps/tag/record/UriRecord$2;-><init>(Lcom/android/apps/tag/record/UriRecord;Lcom/android/apps/tag/record/RecordUtils$ClickInfo;)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f06000d

    new-instance v8, Lcom/android/apps/tag/record/UriRecord$1;

    invoke-direct {v8, p0}, Lcom/android/apps/tag/record/UriRecord$1;-><init>(Lcom/android/apps/tag/record/UriRecord;)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v6

    iput-object v6, p0, Lcom/android/apps/tag/record/UriRecord;->mSmsWaringDialog:Landroid/app/AlertDialog;

    .line 221
    .end local v3    # "scheme":Ljava/lang/String;
    .end local v4    # "sms":Z
    .end local v5    # "tel":Z
    :goto_0
    return-void

    .line 175
    .restart local v3    # "scheme":Ljava/lang/String;
    .restart local v4    # "sms":Z
    .restart local v5    # "tel":Z
    :cond_6
    iget-object v6, v1, Lcom/android/apps/tag/record/RecordUtils$ClickInfo;->activity:Landroid/app/Activity;

    iget-object v7, v1, Lcom/android/apps/tag/record/RecordUtils$ClickInfo;->intent:Landroid/content/Intent;

    invoke-virtual {v6, v7}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 176
    iget-object v6, v1, Lcom/android/apps/tag/record/RecordUtils$ClickInfo;->activity:Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->finish()V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 206
    :catch_0
    move-exception v0

    .line 208
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v6, "UriRecord"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Failed to launch activity for intent "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v1, Lcom/android/apps/tag/record/RecordUtils$ClickInfo;->intent:Landroid/content/Intent;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 178
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    :cond_7
    if-eqz v5, :cond_a

    .line 179
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    const-string v7, "all"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_8

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    const-string v7, "call"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 180
    :cond_8
    new-instance v6, Landroid/app/AlertDialog$Builder;

    iget-object v7, v1, Lcom/android/apps/tag/record/RecordUtils$ClickInfo;->activity:Landroid/app/Activity;

    invoke-direct {v6, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v7, 0x7f06000c

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x1080027

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    iget-object v7, v1, Lcom/android/apps/tag/record/RecordUtils$ClickInfo;->activity:Landroid/app/Activity;

    const v8, 0x7f06000f

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, v1, Lcom/android/apps/tag/record/RecordUtils$ClickInfo;->activity:Landroid/app/Activity;

    invoke-virtual {p0, v11}, Lcom/android/apps/tag/record/UriRecord;->getPrettyUriStringForPopUp(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v7, v8, v9}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f06000e

    new-instance v8, Lcom/android/apps/tag/record/UriRecord$4;

    invoke-direct {v8, p0, v1}, Lcom/android/apps/tag/record/UriRecord$4;-><init>(Lcom/android/apps/tag/record/UriRecord;Lcom/android/apps/tag/record/RecordUtils$ClickInfo;)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f06000d

    new-instance v8, Lcom/android/apps/tag/record/UriRecord$3;

    invoke-direct {v8, p0}, Lcom/android/apps/tag/record/UriRecord$3;-><init>(Lcom/android/apps/tag/record/UriRecord;)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v6

    iput-object v6, p0, Lcom/android/apps/tag/record/UriRecord;->mDialWaringDialog:Landroid/app/AlertDialog;

    goto/16 :goto_0

    .line 197
    :cond_9
    iget-object v6, v1, Lcom/android/apps/tag/record/RecordUtils$ClickInfo;->activity:Landroid/app/Activity;

    iget-object v7, v1, Lcom/android/apps/tag/record/RecordUtils$ClickInfo;->intent:Landroid/content/Intent;

    invoke-virtual {v6, v7}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 198
    iget-object v6, v1, Lcom/android/apps/tag/record/RecordUtils$ClickInfo;->activity:Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    .line 203
    :cond_a
    iget-object v6, v1, Lcom/android/apps/tag/record/RecordUtils$ClickInfo;->activity:Landroid/app/Activity;

    iget-object v7, v1, Lcom/android/apps/tag/record/RecordUtils$ClickInfo;->intent:Landroid/content/Intent;

    invoke-virtual {v6, v7}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 204
    iget-object v6, v1, Lcom/android/apps/tag/record/RecordUtils$ClickInfo;->activity:Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->finish()V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 212
    .end local v1    # "info":Lcom/android/apps/tag/record/RecordUtils$ClickInfo;
    .end local v3    # "scheme":Ljava/lang/String;
    .end local v4    # "sms":Z
    .end local v5    # "tel":Z
    :cond_b
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/apps/tag/record/RecordUtils$ClickInfo;

    .line 214
    .restart local v1    # "info":Lcom/android/apps/tag/record/RecordUtils$ClickInfo;
    :try_start_2
    iget-object v6, v1, Lcom/android/apps/tag/record/RecordUtils$ClickInfo;->activity:Landroid/app/Activity;

    iget-object v7, v1, Lcom/android/apps/tag/record/RecordUtils$ClickInfo;->intent:Landroid/content/Intent;

    invoke-virtual {v6, v7}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 215
    iget-object v6, v1, Lcom/android/apps/tag/record/RecordUtils$ClickInfo;->activity:Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->finish()V
    :try_end_2
    .catch Landroid/content/ActivityNotFoundException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 216
    :catch_1
    move-exception v0

    .line 218
    .restart local v0    # "e":Landroid/content/ActivityNotFoundException;
    const-string v6, "UriRecord"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Failed to launch activity for intent "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v1, Lcom/android/apps/tag/record/RecordUtils$ClickInfo;->intent:Landroid/content/Intent;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0
.end method
