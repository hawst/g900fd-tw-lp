.class public Lcom/android/apps/tag/record/VCardRecord;
.super Lcom/android/apps/tag/record/ParsedNdefRecord;
.source "VCardRecord.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mVCard:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 73
    const-class v0, Lcom/android/apps/tag/record/VCardRecord;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/apps/tag/record/VCardRecord;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>([B)V
    .locals 0
    .param p1, "content"    # [B

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/android/apps/tag/record/ParsedNdefRecord;-><init>()V

    .line 80
    iput-object p1, p0, Lcom/android/apps/tag/record/VCardRecord;->mVCard:[B

    .line 81
    return-void
.end method

.method private getVCardEntries()Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/vcard/VCardEntry;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/vcard/exception/VCardException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 118
    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 120
    .local v2, "entries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vcard/VCardEntry;>;"
    const/4 v5, 0x0

    .line 121
    .local v5, "type":I
    new-instance v0, Lcom/android/vcard/VCardEntryConstructor;

    invoke-direct {v0, v7}, Lcom/android/vcard/VCardEntryConstructor;-><init>(I)V

    .line 122
    .local v0, "constructor":Lcom/android/vcard/VCardEntryConstructor;
    new-instance v6, Lcom/android/apps/tag/record/VCardRecord$1;

    invoke-direct {v6, p0, v2}, Lcom/android/apps/tag/record/VCardRecord$1;-><init>(Lcom/android/apps/tag/record/VCardRecord;Ljava/util/ArrayList;)V

    invoke-virtual {v0, v6}, Lcom/android/vcard/VCardEntryConstructor;->addEntryHandler(Lcom/android/vcard/VCardEntryHandler;)V

    .line 132
    new-instance v3, Lcom/android/vcard/VCardParser_V21;

    invoke-direct {v3, v7}, Lcom/android/vcard/VCardParser_V21;-><init>(I)V

    .line 134
    .local v3, "parser":Lcom/android/vcard/VCardParser;
    :try_start_0
    new-instance v6, Ljava/io/ByteArrayInputStream;

    iget-object v7, p0, Lcom/android/apps/tag/record/VCardRecord;->mVCard:[B

    invoke-direct {v6, v7}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v3, v6, v0}, Lcom/android/vcard/VCardParser;->parse(Ljava/io/InputStream;Lcom/android/vcard/VCardInterpreter;)V
    :try_end_0
    .catch Lcom/android/vcard/exception/VCardVersionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 143
    :goto_0
    return-object v2

    .line 135
    :catch_0
    move-exception v1

    .line 137
    .local v1, "e":Lcom/android/vcard/exception/VCardVersionException;
    :try_start_1
    new-instance v4, Lcom/android/vcard/VCardParser_V30;

    const/4 v6, 0x0

    invoke-direct {v4, v6}, Lcom/android/vcard/VCardParser_V30;-><init>(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 138
    .end local v3    # "parser":Lcom/android/vcard/VCardParser;
    .local v4, "parser":Lcom/android/vcard/VCardParser;
    :try_start_2
    new-instance v6, Ljava/io/ByteArrayInputStream;

    iget-object v7, p0, Lcom/android/apps/tag/record/VCardRecord;->mVCard:[B

    invoke-direct {v6, v7}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v4, v6, v0}, Lcom/android/vcard/VCardParser;->parse(Ljava/io/InputStream;Lcom/android/vcard/VCardInterpreter;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-object v3, v4

    .line 140
    .end local v4    # "parser":Lcom/android/vcard/VCardParser;
    .restart local v3    # "parser":Lcom/android/vcard/VCardParser;
    goto :goto_0

    .line 139
    :catchall_0
    move-exception v6

    :goto_1
    throw v6

    .end local v3    # "parser":Lcom/android/vcard/VCardParser;
    .restart local v4    # "parser":Lcom/android/vcard/VCardParser;
    :catchall_1
    move-exception v6

    move-object v3, v4

    .end local v4    # "parser":Lcom/android/vcard/VCardParser;
    .restart local v3    # "parser":Lcom/android/vcard/VCardParser;
    goto :goto_1
.end method

.method public static isVCard(Landroid/nfc/NdefRecord;)Z
    .locals 2
    .param p0, "record"    # Landroid/nfc/NdefRecord;

    .prologue
    .line 178
    :try_start_0
    invoke-static {p0}, Lcom/android/apps/tag/record/VCardRecord;->parse(Landroid/nfc/NdefRecord;)Lcom/android/apps/tag/record/VCardRecord;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 179
    const/4 v1, 0x1

    .line 181
    :goto_0
    return v1

    .line 180
    :catch_0
    move-exception v0

    .line 181
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static parse(Landroid/nfc/NdefRecord;)Lcom/android/apps/tag/record/VCardRecord;
    .locals 3
    .param p0, "record"    # Landroid/nfc/NdefRecord;

    .prologue
    .line 153
    invoke-static {p0}, Lcom/android/apps/tag/record/MimeRecord;->parse(Landroid/nfc/NdefRecord;)Lcom/android/apps/tag/record/MimeRecord;

    move-result-object v0

    .line 156
    .local v0, "underlyingRecord":Lcom/android/apps/tag/record/MimeRecord;
    const-string v1, "text/x-vcard"

    invoke-virtual {v0}, Lcom/android/apps/tag/record/MimeRecord;->getMimeType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    .line 157
    new-instance v1, Lcom/android/apps/tag/record/VCardRecord;

    invoke-virtual {v0}, Lcom/android/apps/tag/record/MimeRecord;->getContent()[B

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/android/apps/tag/record/VCardRecord;-><init>([B)V

    return-object v1
.end method


# virtual methods
.method public getDisplayName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 107
    :try_start_0
    invoke-direct {p0}, Lcom/android/apps/tag/record/VCardRecord;->getVCardEntries()Ljava/util/ArrayList;

    move-result-object v0

    .line 108
    .local v0, "entries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vcard/VCardEntry;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 109
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/vcard/VCardEntry;

    invoke-virtual {v1}, Lcom/android/vcard/VCardEntry;->getDisplayName()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 114
    .end local v0    # "entries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vcard/VCardEntry;>;"
    :goto_0
    return-object v1

    .line 111
    :catch_0
    move-exception v1

    .line 114
    :cond_0
    const-string v1, "vCard"

    goto :goto_0
.end method

.method public getView(Landroid/app/Activity;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 8
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "inflater"    # Landroid/view/LayoutInflater;
    .param p3, "parent"    # Landroid/view/ViewGroup;
    .param p4, "offset"    # I

    .prologue
    .line 86
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v7

    .line 87
    .local v7, "uri":Landroid/net/Uri;
    invoke-static {p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 88
    const-string v0, "mime"

    invoke-static {v7, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 91
    new-instance v4, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v4, v0, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 93
    .local v4, "intent":Landroid/content/Intent;
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f060009

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    .line 94
    .local v6, "template":Ljava/lang/CharSequence;
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/CharSequence;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/android/apps/tag/record/VCardRecord;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v6, v0}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    .local v5, "description":Ljava/lang/String;
    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move-object v3, p0

    .line 96
    invoke-static/range {v0 .. v5}, Lcom/android/apps/tag/record/RecordUtils;->getViewsForIntent(Landroid/app/Activity;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/view/View$OnClickListener;Landroid/content/Intent;Ljava/lang/String;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 166
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/apps/tag/record/RecordUtils$ClickInfo;

    .line 168
    .local v1, "info":Lcom/android/apps/tag/record/RecordUtils$ClickInfo;
    :try_start_0
    iget-object v2, v1, Lcom/android/apps/tag/record/RecordUtils$ClickInfo;->activity:Landroid/app/Activity;

    iget-object v3, v1, Lcom/android/apps/tag/record/RecordUtils$ClickInfo;->intent:Landroid/content/Intent;

    invoke-virtual {v2, v3}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 169
    iget-object v2, v1, Lcom/android/apps/tag/record/RecordUtils$ClickInfo;->activity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->finish()V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 174
    :goto_0
    return-void

    .line 170
    :catch_0
    move-exception v0

    .line 172
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    sget-object v2, Lcom/android/apps/tag/record/VCardRecord;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to launch activity for intent "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Lcom/android/apps/tag/record/RecordUtils$ClickInfo;->intent:Landroid/content/Intent;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
