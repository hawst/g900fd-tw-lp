.class public Lcom/android/apps/tag/record/SmartPoster;
.super Lcom/android/apps/tag/record/ParsedNdefRecord;
.source "SmartPoster.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;
    }
.end annotation


# static fields
.field private static final ACTION_RECORD_TYPE:[B

.field private static final TYPE_TYPE:[B


# instance fields
.field private final mAction:Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;

.field private final mImageRecord:Lcom/android/apps/tag/record/ImageRecord;

.field private final mTitleRecord:Lcom/android/apps/tag/record/TextRecord;

.field private final mType:Ljava/lang/String;

.field private final mUriRecord:Lcom/android/apps/tag/record/UriRecord;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 233
    const/4 v0, 0x3

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/apps/tag/record/SmartPoster;->ACTION_RECORD_TYPE:[B

    .line 247
    const/4 v0, 0x1

    new-array v0, v0, [B

    const/4 v1, 0x0

    const/16 v2, 0x74

    aput-byte v2, v0, v1

    sput-object v0, Lcom/android/apps/tag/record/SmartPoster;->TYPE_TYPE:[B

    return-void

    .line 233
    nop

    :array_0
    .array-data 1
        0x61t
        0x63t
        0x74t
    .end array-data
.end method

.method private constructor <init>(Lcom/android/apps/tag/record/UriRecord;Lcom/android/apps/tag/record/TextRecord;Lcom/android/apps/tag/record/ImageRecord;Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;Ljava/lang/String;)V
    .locals 1
    .param p1, "uri"    # Lcom/android/apps/tag/record/UriRecord;
    .param p2, "title"    # Lcom/android/apps/tag/record/TextRecord;
    .param p3, "image"    # Lcom/android/apps/tag/record/ImageRecord;
    .param p4, "action"    # Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;
    .param p5, "type"    # Ljava/lang/String;

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/android/apps/tag/record/ParsedNdefRecord;-><init>()V

    .line 106
    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/apps/tag/record/UriRecord;

    iput-object v0, p0, Lcom/android/apps/tag/record/SmartPoster;->mUriRecord:Lcom/android/apps/tag/record/UriRecord;

    .line 107
    iput-object p2, p0, Lcom/android/apps/tag/record/SmartPoster;->mTitleRecord:Lcom/android/apps/tag/record/TextRecord;

    .line 108
    iput-object p3, p0, Lcom/android/apps/tag/record/SmartPoster;->mImageRecord:Lcom/android/apps/tag/record/ImageRecord;

    .line 109
    invoke-static {p4}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;

    iput-object v0, p0, Lcom/android/apps/tag/record/SmartPoster;->mAction:Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;

    .line 110
    iput-object p5, p0, Lcom/android/apps/tag/record/SmartPoster;->mType:Ljava/lang/String;

    .line 111
    return-void
.end method

.method private static getByType([B[Landroid/nfc/NdefRecord;)Landroid/nfc/NdefRecord;
    .locals 5
    .param p0, "type"    # [B
    .param p1, "records"    # [Landroid/nfc/NdefRecord;

    .prologue
    .line 225
    move-object v0, p1

    .local v0, "arr$":[Landroid/nfc/NdefRecord;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 226
    .local v3, "record":Landroid/nfc/NdefRecord;
    invoke-virtual {v3}, Landroid/nfc/NdefRecord;->getType()[B

    move-result-object v4

    invoke-static {p0, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 230
    .end local v3    # "record":Landroid/nfc/NdefRecord;
    :goto_1
    return-object v3

    .line 225
    .restart local v3    # "record":Landroid/nfc/NdefRecord;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 230
    .end local v3    # "record":Landroid/nfc/NdefRecord;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private static getFirstIfExists(Ljava/lang/Iterable;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Iterable",
            "<*>;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 193
    .local p0, "elements":Ljava/lang/Iterable;, "Ljava/lang/Iterable<*>;"
    .local p1, "type":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-static {p0, p1}, Lcom/google/common/collect/Iterables;->filter(Ljava/lang/Iterable;Ljava/lang/Class;)Ljava/lang/Iterable;

    move-result-object v0

    .line 194
    .local v0, "filtered":Ljava/lang/Iterable;, "Ljava/lang/Iterable<TT;>;"
    const/4 v1, 0x0

    .line 195
    .local v1, "instance":Ljava/lang/Object;, "TT;"
    invoke-static {v0}, Lcom/google/common/collect/Iterables;->isEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 196
    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/google/common/collect/Iterables;->get(Ljava/lang/Iterable;I)Ljava/lang/Object;

    move-result-object v1

    .line 198
    .end local v1    # "instance":Ljava/lang/Object;, "TT;"
    :cond_0
    return-object v1
.end method

.method public static isPoster(Landroid/nfc/NdefRecord;)Z
    .locals 2
    .param p0, "record"    # Landroid/nfc/NdefRecord;

    .prologue
    .line 152
    :try_start_0
    invoke-static {p0}, Lcom/android/apps/tag/record/SmartPoster;->parse(Landroid/nfc/NdefRecord;)Lcom/android/apps/tag/record/SmartPoster;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 153
    const/4 v1, 0x1

    .line 155
    :goto_0
    return v1

    .line 154
    :catch_0
    move-exception v0

    .line 155
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static parse(Landroid/nfc/NdefRecord;)Lcom/android/apps/tag/record/SmartPoster;
    .locals 4
    .param p0, "record"    # Landroid/nfc/NdefRecord;

    .prologue
    const/4 v2, 0x1

    .line 125
    invoke-virtual {p0}, Landroid/nfc/NdefRecord;->getTnf()S

    move-result v3

    if-ne v3, v2, :cond_0

    :goto_0
    invoke-static {v2}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    .line 126
    invoke-virtual {p0}, Landroid/nfc/NdefRecord;->getType()[B

    move-result-object v2

    sget-object v3, Landroid/nfc/NdefRecord;->RTD_SMART_POSTER:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    invoke-static {v2}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    .line 128
    :try_start_0
    new-instance v1, Landroid/nfc/NdefMessage;

    invoke-virtual {p0}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/nfc/NdefMessage;-><init>([B)V

    .line 129
    .local v1, "subRecords":Landroid/nfc/NdefMessage;
    invoke-virtual {v1}, Landroid/nfc/NdefMessage;->getRecords()[Landroid/nfc/NdefRecord;

    move-result-object v2

    invoke-static {v2}, Lcom/android/apps/tag/record/SmartPoster;->parse([Landroid/nfc/NdefRecord;)Lcom/android/apps/tag/record/SmartPoster;
    :try_end_0
    .catch Landroid/nfc/FormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    return-object v2

    .line 125
    .end local v1    # "subRecords":Landroid/nfc/NdefMessage;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 130
    :catch_0
    move-exception v0

    .line 131
    .local v0, "e":Landroid/nfc/FormatException;
    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public static parse([Landroid/nfc/NdefRecord;)Lcom/android/apps/tag/record/SmartPoster;
    .locals 8
    .param p0, "recordsRaw"    # [Landroid/nfc/NdefRecord;

    .prologue
    .line 137
    :try_start_0
    invoke-static {p0}, Lcom/android/apps/tag/message/NdefMessageParser;->getRecords([Landroid/nfc/NdefRecord;)Ljava/util/List;

    move-result-object v7

    .line 138
    .local v7, "records":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lcom/android/apps/tag/record/ParsedNdefRecord;>;"
    const-class v0, Lcom/android/apps/tag/record/UriRecord;

    invoke-static {v7, v0}, Lcom/google/common/collect/Iterables;->filter(Ljava/lang/Iterable;Ljava/lang/Class;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/Iterables;->getOnlyElement(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/apps/tag/record/UriRecord;

    .line 139
    .local v1, "uri":Lcom/android/apps/tag/record/UriRecord;
    const-class v0, Lcom/android/apps/tag/record/TextRecord;

    invoke-static {v7, v0}, Lcom/android/apps/tag/record/SmartPoster;->getFirstIfExists(Ljava/lang/Iterable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/apps/tag/record/TextRecord;

    .line 140
    .local v2, "title":Lcom/android/apps/tag/record/TextRecord;
    const-class v0, Lcom/android/apps/tag/record/ImageRecord;

    invoke-static {v7, v0}, Lcom/android/apps/tag/record/SmartPoster;->getFirstIfExists(Ljava/lang/Iterable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/apps/tag/record/ImageRecord;

    .line 141
    .local v3, "image":Lcom/android/apps/tag/record/ImageRecord;
    invoke-static {p0}, Lcom/android/apps/tag/record/SmartPoster;->parseRecommendedAction([Landroid/nfc/NdefRecord;)Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;

    move-result-object v4

    .line 142
    .local v4, "action":Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;
    invoke-static {p0}, Lcom/android/apps/tag/record/SmartPoster;->parseType([Landroid/nfc/NdefRecord;)Ljava/lang/String;

    move-result-object v5

    .line 144
    .local v5, "type":Ljava/lang/String;
    new-instance v0, Lcom/android/apps/tag/record/SmartPoster;

    invoke-direct/range {v0 .. v5}, Lcom/android/apps/tag/record/SmartPoster;-><init>(Lcom/android/apps/tag/record/UriRecord;Lcom/android/apps/tag/record/TextRecord;Lcom/android/apps/tag/record/ImageRecord;Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 145
    .end local v1    # "uri":Lcom/android/apps/tag/record/UriRecord;
    .end local v2    # "title":Lcom/android/apps/tag/record/TextRecord;
    .end local v3    # "image":Lcom/android/apps/tag/record/ImageRecord;
    .end local v4    # "action":Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;
    .end local v5    # "type":Ljava/lang/String;
    .end local v7    # "records":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lcom/android/apps/tag/record/ParsedNdefRecord;>;"
    :catch_0
    move-exception v6

    .line 146
    .local v6, "e":Ljava/util/NoSuchElementException;
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method private static parseRecommendedAction([Landroid/nfc/NdefRecord;)Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;
    .locals 4
    .param p0, "records"    # [Landroid/nfc/NdefRecord;

    .prologue
    .line 236
    sget-object v2, Lcom/android/apps/tag/record/SmartPoster;->ACTION_RECORD_TYPE:[B

    invoke-static {v2, p0}, Lcom/android/apps/tag/record/SmartPoster;->getByType([B[Landroid/nfc/NdefRecord;)Landroid/nfc/NdefRecord;

    move-result-object v1

    .line 237
    .local v1, "record":Landroid/nfc/NdefRecord;
    if-nez v1, :cond_0

    .line 238
    sget-object v2, Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;->UNKNOWN:Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;

    .line 244
    :goto_0
    return-object v2

    .line 240
    :cond_0
    invoke-virtual {v1}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v2

    const/4 v3, 0x0

    aget-byte v0, v2, v3

    .line 241
    .local v0, "action":B
    # getter for: Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;->LOOKUP:Lcom/google/common/collect/ImmutableMap;
    invoke-static {}, Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;->access$000()Lcom/google/common/collect/ImmutableMap;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/common/collect/ImmutableMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 242
    # getter for: Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;->LOOKUP:Lcom/google/common/collect/ImmutableMap;
    invoke-static {}, Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;->access$000()Lcom/google/common/collect/ImmutableMap;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/common/collect/ImmutableMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;

    goto :goto_0

    .line 244
    :cond_1
    sget-object v2, Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;->UNKNOWN:Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;

    goto :goto_0
.end method

.method private static parseType([Landroid/nfc/NdefRecord;)Ljava/lang/String;
    .locals 4
    .param p0, "records"    # [Landroid/nfc/NdefRecord;

    .prologue
    .line 250
    sget-object v1, Lcom/android/apps/tag/record/SmartPoster;->TYPE_TYPE:[B

    invoke-static {v1, p0}, Lcom/android/apps/tag/record/SmartPoster;->getByType([B[Landroid/nfc/NdefRecord;)Landroid/nfc/NdefRecord;

    move-result-object v0

    .line 251
    .local v0, "type":Landroid/nfc/NdefRecord;
    if-nez v0, :cond_0

    .line 252
    const/4 v1, 0x0

    .line 254
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Ljava/lang/String;

    invoke-virtual {v0}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v2

    sget-object v3, Lcom/google/common/base/Charsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-direct {v1, v2, v3}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    goto :goto_0
.end method


# virtual methods
.method public getView(Landroid/app/Activity;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 4
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "inflater"    # Landroid/view/LayoutInflater;
    .param p3, "parent"    # Landroid/view/ViewGroup;
    .param p4, "offset"    # I

    .prologue
    .line 161
    iget-object v1, p0, Lcom/android/apps/tag/record/SmartPoster;->mTitleRecord:Lcom/android/apps/tag/record/TextRecord;

    if-eqz v1, :cond_0

    .line 163
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 164
    .local v0, "container":Landroid/widget/LinearLayout;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 165
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 168
    iget-object v1, p0, Lcom/android/apps/tag/record/SmartPoster;->mTitleRecord:Lcom/android/apps/tag/record/TextRecord;

    invoke-virtual {v1, p1, p2, v0, p4}, Lcom/android/apps/tag/record/TextRecord;->getView(Landroid/app/Activity;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 169
    const/high16 v1, 0x7f030000

    invoke-virtual {p2, v1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 170
    iget-object v1, p0, Lcom/android/apps/tag/record/SmartPoster;->mUriRecord:Lcom/android/apps/tag/record/UriRecord;

    invoke-virtual {v1, p1, p2, v0, p4}, Lcom/android/apps/tag/record/UriRecord;->getView(Landroid/app/Activity;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 174
    .end local v0    # "container":Landroid/widget/LinearLayout;
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/android/apps/tag/record/SmartPoster;->mUriRecord:Lcom/android/apps/tag/record/UriRecord;

    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/android/apps/tag/record/UriRecord;->getView(Landroid/app/Activity;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method
