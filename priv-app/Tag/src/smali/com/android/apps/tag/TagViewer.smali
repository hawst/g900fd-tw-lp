.class public Lcom/android/apps/tag/TagViewer;
.super Landroid/app/Activity;
.source "TagViewer.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field mTagContent:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method buildTagViews(Landroid/nfc/NdefMessage;)V
    .locals 11
    .param p1, "msg"    # Landroid/nfc/NdefMessage;

    .prologue
    const v10, 0x7f060004

    const v9, 0x7f030002

    const/4 v8, 0x0

    .line 77
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 78
    .local v3, "inflater":Landroid/view/LayoutInflater;
    iget-object v0, p0, Lcom/android/apps/tag/TagViewer;->mTagContent:Landroid/widget/LinearLayout;

    .line 81
    .local v0, "content":Landroid/widget/LinearLayout;
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 84
    if-nez p1, :cond_1

    .line 85
    invoke-virtual {v3, v9, v0, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 86
    .local v1, "empty":Landroid/widget/TextView;
    invoke-virtual {v1, v10}, Landroid/widget/TextView;->setText(I)V

    .line 87
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 107
    .end local v1    # "empty":Landroid/widget/TextView;
    :cond_0
    :goto_0
    return-void

    .line 91
    :cond_1
    invoke-static {p1}, Lcom/android/apps/tag/message/NdefMessageParser;->parse(Landroid/nfc/NdefMessage;)Lcom/android/apps/tag/message/ParsedNdefMessage;

    move-result-object v4

    .line 93
    .local v4, "parsedMsg":Lcom/android/apps/tag/message/ParsedNdefMessage;
    invoke-virtual {v4}, Lcom/android/apps/tag/message/ParsedNdefMessage;->getRecords()Ljava/util/List;

    move-result-object v6

    .line 94
    .local v6, "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/apps/tag/record/ParsedNdefRecord;>;"
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    .line 95
    .local v7, "size":I
    if-nez v7, :cond_2

    .line 96
    invoke-virtual {v3, v9, v0, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 97
    .restart local v1    # "empty":Landroid/widget/TextView;
    invoke-virtual {v1, v10}, Landroid/widget/TextView;->setText(I)V

    .line 98
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 100
    .end local v1    # "empty":Landroid/widget/TextView;
    :cond_2
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v7, :cond_0

    .line 101
    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/apps/tag/record/ParsedNdefRecord;

    .line 102
    .local v5, "record":Lcom/android/apps/tag/record/ParsedNdefRecord;
    invoke-virtual {v5, p0, v3, v0, v2}, Lcom/android/apps/tag/record/ParsedNdefRecord;->getView(Landroid/app/Activity;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 103
    const/high16 v8, 0x7f030000

    const/4 v9, 0x1

    invoke-virtual {v3, v8, v0, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 100
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 117
    invoke-virtual {p0}, Lcom/android/apps/tag/TagViewer;->finish()V

    .line 118
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 48
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 50
    const v0, 0x7f030004

    invoke-virtual {p0, v0}, Lcom/android/apps/tag/TagViewer;->setContentView(I)V

    .line 52
    const v0, 0x7f080005

    invoke-virtual {p0, v0}, Lcom/android/apps/tag/TagViewer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/apps/tag/TagViewer;->mTagContent:Landroid/widget/LinearLayout;

    .line 54
    invoke-virtual {p0}, Lcom/android/apps/tag/TagViewer;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/apps/tag/TagViewer;->resolveIntent(Landroid/content/Intent;)V

    .line 55
    return-void
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 111
    invoke-virtual {p0, p1}, Lcom/android/apps/tag/TagViewer;->setIntent(Landroid/content/Intent;)V

    .line 112
    invoke-virtual {p0, p1}, Lcom/android/apps/tag/TagViewer;->resolveIntent(Landroid/content/Intent;)V

    .line 113
    return-void
.end method

.method resolveIntent(Landroid/content/Intent;)V
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 59
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 60
    .local v0, "action":Ljava/lang/String;
    const-string v3, "android.nfc.action.TAG_DISCOVERED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "android.nfc.action.TECH_DISCOVERED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 62
    :cond_0
    const-string v3, "android.nfc.extra.NDEF_MESSAGES"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v2

    .line 63
    .local v2, "rawMsgs":[Landroid/os/Parcelable;
    const/4 v1, 0x0

    .line 64
    .local v1, "msg":Landroid/nfc/NdefMessage;
    if-eqz v2, :cond_1

    array-length v3, v2

    if-lez v3, :cond_1

    .line 65
    const/4 v3, 0x0

    aget-object v1, v2, v3

    .end local v1    # "msg":Landroid/nfc/NdefMessage;
    check-cast v1, Landroid/nfc/NdefMessage;

    .line 68
    .restart local v1    # "msg":Landroid/nfc/NdefMessage;
    :cond_1
    invoke-virtual {p0, v1}, Lcom/android/apps/tag/TagViewer;->buildTagViews(Landroid/nfc/NdefMessage;)V

    .line 74
    .end local v1    # "msg":Landroid/nfc/NdefMessage;
    .end local v2    # "rawMsgs":[Landroid/os/Parcelable;
    :goto_0
    return-void

    .line 70
    :cond_2
    const-string v3, "TagViewer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unknown intent "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    invoke-virtual {p0}, Lcom/android/apps/tag/TagViewer;->finish()V

    goto :goto_0
.end method
