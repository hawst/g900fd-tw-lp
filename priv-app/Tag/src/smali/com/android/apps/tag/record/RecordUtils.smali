.class public Lcom/android/apps/tag/record/RecordUtils;
.super Ljava/lang/Object;
.source "RecordUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/apps/tag/record/RecordUtils$ClickInfo;
    }
.end annotation


# direct methods
.method private static buildActivityView(Landroid/app/Activity;Landroid/content/pm/ResolveInfo;Landroid/content/pm/PackageManager;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/view/View$OnClickListener;Landroid/content/Intent;Ljava/lang/String;)Landroid/view/View;
    .locals 8
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "resolveInfo"    # Landroid/content/pm/ResolveInfo;
    .param p2, "pm"    # Landroid/content/pm/PackageManager;
    .param p3, "inflater"    # Landroid/view/LayoutInflater;
    .param p4, "parent"    # Landroid/view/ViewGroup;
    .param p5, "listener"    # Landroid/view/View$OnClickListener;
    .param p6, "intent"    # Landroid/content/Intent;
    .param p7, "defaultText"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 108
    iget-object v0, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 110
    .local v0, "activityInfo":Landroid/content/pm/ActivityInfo;
    iget-object v4, p1, Landroid/content/pm/ResolveInfo;->filter:Landroid/content/IntentFilter;

    invoke-virtual {v4, v7}, Landroid/content/IntentFilter;->getAction(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p6, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 111
    new-instance v4, Landroid/content/ComponentName;

    iget-object v5, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v6, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v4, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p6, v4}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 113
    const v4, 0x7f030003

    invoke-virtual {p3, v4, p4, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 114
    .local v2, "item":Landroid/view/View;
    invoke-virtual {v2, p5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 115
    new-instance v4, Lcom/android/apps/tag/record/RecordUtils$ClickInfo;

    invoke-direct {v4, p0, p6}, Lcom/android/apps/tag/record/RecordUtils$ClickInfo;-><init>(Landroid/app/Activity;Landroid/content/Intent;)V

    invoke-virtual {v2, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 117
    const v4, 0x7f080002

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 118
    .local v1, "icon":Landroid/widget/ImageView;
    invoke-virtual {p1, p2}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 120
    const v4, 0x7f080004

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 121
    .local v3, "text":Landroid/widget/TextView;
    invoke-virtual {p1, p2}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 123
    const v4, 0x7f080003

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .end local v3    # "text":Landroid/widget/TextView;
    check-cast v3, Landroid/widget/TextView;

    .line 124
    .restart local v3    # "text":Landroid/widget/TextView;
    invoke-virtual {v3, p7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    return-object v2
.end method

.method public static getViewsForIntent(Landroid/app/Activity;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/view/View$OnClickListener;Landroid/content/Intent;Ljava/lang/String;)Landroid/view/View;
    .locals 14
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "parent"    # Landroid/view/ViewGroup;
    .param p3, "listener"    # Landroid/view/View$OnClickListener;
    .param p4, "intent"    # Landroid/content/Intent;
    .param p5, "description"    # Ljava/lang/String;

    .prologue
    .line 66
    invoke-virtual {p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 67
    .local v3, "pm":Landroid/content/pm/PackageManager;
    const v10, 0x10040

    .line 68
    .local v10, "flags":I
    move-object/from16 v0, p4

    invoke-virtual {v3, v0, v10}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v9

    .line 69
    .local v9, "activities":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v12

    .line 70
    .local v12, "numActivities":I
    if-eqz v12, :cond_0

    const/4 v1, 0x1

    if-ne v12, v1, :cond_2

    const/4 v1, 0x0

    invoke-interface {v9, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/ResolveInfo;

    iget-object v1, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-boolean v1, v1, Landroid/content/pm/ActivityInfo;->enabled:Z

    if-nez v1, :cond_2

    .line 71
    :cond_0
    const v1, 0x7f030002

    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-virtual {p1, v1, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    .line 72
    .local v13, "text":Landroid/widget/TextView;
    move-object/from16 v0, p5

    invoke-virtual {v13, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v5, v13

    .line 98
    .end local v13    # "text":Landroid/widget/TextView;
    :cond_1
    :goto_0
    return-object v5

    .line 74
    :cond_2
    const/4 v1, 0x1

    if-ne v12, v1, :cond_3

    .line 75
    const/4 v1, 0x0

    invoke-interface {v9, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/ResolveInfo;

    move-object v1, p0

    move-object v4, p1

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    invoke-static/range {v1 .. v8}, Lcom/android/apps/tag/record/RecordUtils;->buildActivityView(Landroid/app/Activity;Landroid/content/pm/ResolveInfo;Landroid/content/pm/PackageManager;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/view/View$OnClickListener;Landroid/content/Intent;Ljava/lang/String;)Landroid/view/View;

    move-result-object v5

    goto :goto_0

    .line 79
    :cond_3
    new-instance v5, Landroid/widget/LinearLayout;

    invoke-direct {v5, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 80
    .local v5, "container":Landroid/widget/LinearLayout;
    const/4 v1, 0x1

    invoke-virtual {v5, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 81
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v4, -0x1

    const/4 v6, -0x2

    invoke-direct {v1, v4, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v5, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 85
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "i$":Ljava/util/Iterator;
    :cond_4
    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/ResolveInfo;

    .line 86
    .local v2, "resolveInfo":Landroid/content/pm/ResolveInfo;
    iget-object v1, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-boolean v1, v1, Landroid/content/pm/ActivityInfo;->enabled:Z

    if-eqz v1, :cond_4

    .line 90
    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    if-lez v1, :cond_5

    .line 91
    const/high16 v1, 0x7f030000

    invoke-virtual {p1, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 94
    :cond_5
    new-instance v7, Landroid/content/Intent;

    move-object/from16 v0, p4

    invoke-direct {v7, v0}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .local v7, "clone":Landroid/content/Intent;
    move-object v1, p0

    move-object v4, p1

    move-object/from16 v6, p3

    move-object/from16 v8, p5

    .line 95
    invoke-static/range {v1 .. v8}, Lcom/android/apps/tag/record/RecordUtils;->buildActivityView(Landroid/app/Activity;Landroid/content/pm/ResolveInfo;Landroid/content/pm/PackageManager;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/view/View$OnClickListener;Landroid/content/Intent;Ljava/lang/String;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v5, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_1
.end method
