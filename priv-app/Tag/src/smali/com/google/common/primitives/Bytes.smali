.class public final Lcom/google/common/primitives/Bytes;
.super Ljava/lang/Object;
.source "Bytes.java"


# direct methods
.method public static varargs concat([[B)[B
    .locals 9
    .param p0, "arrays"    # [[B

    .prologue
    .line 167
    const/4 v4, 0x0

    .line 168
    .local v4, "length":I
    move-object v0, p0

    .local v0, "arr$":[[B
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 169
    .local v1, "array":[B
    array-length v7, v1

    add-int/2addr v4, v7

    .line 168
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 171
    .end local v1    # "array":[B
    :cond_0
    new-array v6, v4, [B

    .line 172
    .local v6, "result":[B
    const/4 v5, 0x0

    .line 173
    .local v5, "pos":I
    move-object v0, p0

    array-length v3, v0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 174
    .restart local v1    # "array":[B
    const/4 v7, 0x0

    array-length v8, v1

    invoke-static {v1, v7, v6, v5, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 175
    array-length v7, v1

    add-int/2addr v5, v7

    .line 173
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 177
    .end local v1    # "array":[B
    :cond_1
    return-object v6
.end method
