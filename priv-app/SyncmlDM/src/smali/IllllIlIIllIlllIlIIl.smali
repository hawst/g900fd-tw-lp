.class public LIllllIlIIllIlllIlIIl;
.super Ljava/lang/Object;
.source "llIIIIlllllIIllIIllI"


# static fields
.field private static final llIIIIlllllIIllIIllI:[C


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7
    const/16 v0, 0x10

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, LIllllIlIIllIlllIlIIl;->llIIIIlllllIIllIIllI:[C

    .line 8
    return-void

    .line 7
    :array_0
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x41s
        0x42s
        0x43s
        0x44s
        0x45s
        0x46s
    .end array-data
.end method

.method static llIIIIlllllIIllIIllI(C)I
    .locals 3

    .prologue
    .line 202
    const/16 v0, 0x30

    if-lt p0, v0, :cond_0

    const/16 v0, 0x39

    if-gt p0, v0, :cond_0

    .line 203
    add-int/lit8 v0, p0, -0x30

    .line 207
    :goto_0
    return v0

    .line 204
    :cond_0
    const/16 v0, 0x41

    if-lt p0, v0, :cond_1

    const/16 v0, 0x46

    if-gt p0, v0, :cond_1

    .line 205
    add-int/lit8 v0, p0, -0x41

    add-int/lit8 v0, v0, 0xa

    goto :goto_0

    .line 206
    :cond_1
    const/16 v0, 0x61

    if-lt p0, v0, :cond_2

    const/16 v0, 0x66

    if-gt p0, v0, :cond_2

    .line 207
    add-int/lit8 v0, p0, -0x61

    add-int/lit8 v0, v0, 0xa

    goto :goto_0

    .line 209
    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "invalid hex char \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static llIIIIlllllIIllIIllI(I)I
    .locals 1

    .prologue
    .line 140
    if-ltz p0, :cond_0

    const/16 v0, 0x9

    if-gt p0, v0, :cond_0

    .line 141
    add-int/lit8 v0, p0, 0x30

    .line 145
    :goto_0
    return v0

    .line 142
    :cond_0
    const/16 v0, 0xa

    if-gt v0, p0, :cond_1

    const/16 v0, 0xf

    if-gt p0, v0, :cond_1

    .line 143
    add-int/lit8 v0, p0, 0x41

    add-int/lit8 v0, v0, -0xa

    goto :goto_0

    .line 145
    :cond_1
    const/16 v0, 0x3f

    goto :goto_0
.end method

.method public static llIIIIlllllIIllIIllI(Ljava/lang/String;Ljava/lang/String;I)I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 99
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-ge p2, v1, :cond_0

    .line 100
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 104
    :cond_0
    invoke-virtual {p0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1

    .line 109
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static llIIIIlllllIIllIIllI(Ljava/lang/String;C)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 75
    .line 77
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 85
    :cond_0
    :goto_0
    return-object v0

    .line 80
    :cond_1
    invoke-virtual {p0, p1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 82
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 85
    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static llIIIIlllllIIllIIllI(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 54
    .line 56
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 64
    :cond_0
    :goto_0
    return-object v0

    .line 59
    :cond_1
    invoke-virtual {p0, p1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 61
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 64
    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static llIIIIlllllIIllIIllI([B)Ljava/lang/String;
    .locals 4

    .prologue
    .line 220
    if-nez p0, :cond_0

    .line 221
    const/4 v0, 0x0

    .line 238
    :goto_0
    return-object v0

    .line 223
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    array-length v0, p0

    mul-int/lit8 v0, v0, 0x2

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 225
    const/4 v0, 0x0

    :goto_1
    array-length v2, p0

    if-lt v0, v2, :cond_1

    .line 238
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 229
    :cond_1
    aget-byte v2, p0, v0

    shr-int/lit8 v2, v2, 0x4

    and-int/lit8 v2, v2, 0xf

    .line 231
    const-string v3, "0123456789abcdef"

    invoke-virtual {v3, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 233
    aget-byte v2, p0, v0

    and-int/lit8 v2, v2, 0xf

    .line 235
    const-string v3, "0123456789abcdef"

    invoke-virtual {v3, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 225
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public static llIIIIlllllIIllIIllI([BII)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 243
    mul-int/lit8 v1, p2, 0x2

    new-array v3, v1, [C

    move v1, p1

    move v2, v0

    .line 248
    :goto_0
    add-int v4, p1, p2

    if-lt v1, v4, :cond_1

    .line 256
    :goto_1
    array-length v1, v3

    if-lt v0, v1, :cond_2

    .line 263
    :cond_0
    array-length v1, v3

    if-ne v0, v1, :cond_3

    .line 265
    new-instance v0, Ljava/lang/String;

    const-string v1, "0"

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 271
    :goto_2
    return-object v0

    .line 250
    :cond_1
    aget-byte v4, p0, v1

    .line 251
    add-int/lit8 v5, v2, 0x1

    sget-object v6, LIllllIlIIllIlllIlIIl;->llIIIIlllllIIllIIllI:[C

    ushr-int/lit8 v7, v4, 0x4

    and-int/lit8 v7, v7, 0xf

    aget-char v6, v6, v7

    aput-char v6, v3, v2

    .line 252
    add-int/lit8 v2, v5, 0x1

    sget-object v6, LIllllIlIIllIlllIlIIl;->llIIIIlllllIIllIIllI:[C

    and-int/lit8 v4, v4, 0xf

    aget-char v4, v6, v4

    aput-char v4, v3, v5

    .line 248
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 258
    :cond_2
    aget-char v1, v3, v0

    const/16 v2, 0x30

    if-gt v1, v2, :cond_0

    .line 256
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 269
    :cond_3
    array-length v1, v3

    sub-int/2addr v1, v0

    invoke-static {v3, v0, v1}, Ljava/lang/String;->valueOf([CII)Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method public static llIIIIlllllIIllIIllI([C)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 155
    .line 157
    array-length v0, p0

    if-gtz v0, :cond_3

    .line 158
    const/4 v0, 0x0

    .line 171
    :goto_0
    return-object v0

    .line 161
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 159
    :goto_1
    aget-char v2, p0, v0

    if-eqz v2, :cond_1

    array-length v2, p0

    if-gt v2, v0, :cond_0

    .line 164
    :cond_1
    new-array v2, v0, [C

    .line 166
    :goto_2
    if-lt v1, v0, :cond_2

    .line 171
    invoke-static {v2}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 168
    :cond_2
    aget-char v3, p0, v1

    aput-char v3, v2, v1

    .line 166
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public static llIIIIlllllIIllIIllI([CC[C)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 19
    .line 21
    if-nez p0, :cond_0

    move-object v0, v2

    .line 43
    :goto_0
    return-object v0

    .line 25
    :cond_0
    array-length v0, p0

    if-nez v0, :cond_4

    move-object v0, v2

    .line 27
    goto :goto_0

    .line 32
    :cond_1
    aget-char v3, p0, v0

    if-ne v3, p1, :cond_3

    .line 34
    aput-char v1, p2, v0

    .line 35
    array-length v2, p0

    add-int/lit8 v3, v0, 0x1

    sub-int/2addr v2, v3

    new-array v2, v2, [C

    .line 36
    :goto_1
    array-length v3, p0

    add-int/lit8 v4, v0, 0x1

    sub-int/2addr v3, v4

    if-lt v1, v3, :cond_2

    .line 38
    invoke-static {v2}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 37
    :cond_2
    add-int v3, v1, v0

    add-int/lit8 v3, v3, 0x1

    aget-char v3, p0, v3

    aput-char v3, v2, v1

    .line 36
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 40
    :cond_3
    aget-char v3, p0, v0

    aput-char v3, p2, v0

    .line 41
    add-int/lit8 v0, v0, 0x1

    .line 30
    :goto_2
    array-length v3, p0

    if-lt v0, v3, :cond_1

    move-object v0, v2

    .line 43
    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method public static llIIIIlllllIIllIIllI(Ljava/lang/String;)[B
    .locals 6

    .prologue
    .line 185
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 186
    const/4 v0, 0x0

    .line 197
    :cond_0
    return-object v0

    .line 188
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    .line 190
    div-int/lit8 v0, v2, 0x2

    new-array v0, v0, [B

    .line 192
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 194
    div-int/lit8 v3, v1, 0x2

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {v4}, LIllllIlIIllIlllIlIIl;->llIIIIlllllIIllIIllI(C)I

    move-result v4

    shl-int/lit8 v4, v4, 0x4

    add-int/lit8 v5, v1, 0x1

    invoke-virtual {p0, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5}, LIllllIlIIllIlllIlIIl;->llIIIIlllllIIllIIllI(C)I

    move-result v5

    or-int/2addr v4, v5

    int-to-byte v4, v4

    aput-byte v4, v0, v3

    .line 192
    add-int/lit8 v1, v1, 0x2

    goto :goto_0
.end method

.method public static llllIIIllIlIIIIllllI(Ljava/lang/String;C)Ljava/lang/String;
    .locals 2

    .prologue
    .line 120
    .line 122
    invoke-virtual {p0, p1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 124
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
