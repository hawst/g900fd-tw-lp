.class public LIlIIlIIlIllllIlllIII;
.super Ljava/lang/Object;
.source "llIIIIlllllIIllIIllI"

# interfaces
.implements Lcom/wssyncmldm/interfaces/XDMInterface;


# direct methods
.method public static llIIIIlllllIIllIIllI(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 36
    const-string v0, "syncml:auth-basic"

    invoke-virtual {v0, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 37
    const/4 v0, 0x0

    .line 43
    :goto_0
    return v0

    .line 38
    :cond_0
    const-string v0, "syncml:auth-md5"

    invoke-virtual {v0, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1

    .line 39
    const/4 v0, 0x1

    goto :goto_0

    .line 40
    :cond_1
    const-string v0, "syncml:auth-MAC"

    invoke-virtual {v0, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_2

    .line 41
    const/4 v0, 0x2

    goto :goto_0

    .line 43
    :cond_2
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static llIIIIlllllIIllIIllI(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 20
    packed-switch p0, :pswitch_data_0

    .line 29
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v1, "Not Support Auth Type"

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 30
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 23
    :pswitch_0
    const-string v0, "syncml:auth-basic"

    goto :goto_0

    .line 25
    :pswitch_1
    const-string v0, "syncml:auth-md5"

    goto :goto_0

    .line 27
    :pswitch_2
    const-string v0, "syncml:auth-MAC"

    goto :goto_0

    .line 20
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static llIIIIlllllIIllIIllI(ILjava/lang/String;Ljava/lang/String;[BI[BI)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 75
    .line 78
    const/16 v1, 0x10

    new-array v1, v1, [B

    .line 80
    packed-switch p0, :pswitch_data_0

    .line 112
    sget-object v1, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v2, "Not Support Auth Type"

    invoke-virtual {v1, v2}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 193
    :cond_0
    :goto_0
    return-object v0

    .line 84
    :pswitch_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 86
    :cond_1
    sget-object v1, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v2, "userName or passWord is NULL"

    invoke-virtual {v1, v2}, LIIIIllIlIIlIIlIlIllI;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    goto :goto_0

    .line 93
    :pswitch_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    if-eqz p3, :cond_2

    if-gtz p4, :cond_4

    .line 95
    :cond_2
    sget-object v1, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v2, "userName or passWord or nonce or nonceLength is NULL"

    invoke-virtual {v1, v2}, LIIIIllIlIIlIIlIlIllI;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    goto :goto_0

    .line 103
    :pswitch_2
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    if-eqz p3, :cond_3

    if-lez p4, :cond_3

    if-eqz p5, :cond_3

    if-gtz p6, :cond_4

    .line 105
    :cond_3
    sget-object v1, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v2, "userName or passWord or nonce or nonceLength or packetBody or bodyLength is NULL"

    invoke-virtual {v1, v2}, LIIIIllIlIIlIIlIlIllI;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    goto :goto_0

    .line 117
    :cond_4
    packed-switch p0, :pswitch_data_1

    goto :goto_0

    .line 122
    :pswitch_3
    const-string v0, ":"

    invoke-virtual {p1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 123
    invoke-virtual {v0, p2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 124
    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-static {v0}, LIIlIIlIIlIlIllIIIIlI;->llIIIIlllllIIllIIllI([B)[B

    move-result-object v2

    .line 125
    new-instance v0, Ljava/lang/String;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 127
    sget-object v2, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "XDM_CRED_TYPE_BASIC cred:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " ret:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LIIIIllIlIIlIIlIlIllI;->llIIIIlllllIIllIIllI(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 134
    :pswitch_4
    new-instance v0, LlIllIlIlIIIIIIlIlIII;

    invoke-direct {v0}, LlIllIlIlIIIIIIlIlIII;-><init>()V

    .line 135
    invoke-virtual {v0, p1, p2, p3}, LlIllIlIlIIIIIIlIlIII;->llIIIIlllllIIllIIllI(Ljava/lang/String;Ljava/lang/String;[B)[B

    move-result-object v1

    .line 136
    new-instance v0, Ljava/lang/String;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 138
    new-instance v1, Ljava/lang/String;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v2

    invoke-direct {v1, p3, v2}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 139
    sget-object v2, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "XDM_CRED_TYPE_MD5 nonce= "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " ret= "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LIIIIllIlIIlIIlIlIllI;->llIIIIlllllIIllIIllI(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 149
    :pswitch_5
    :try_start_0
    const-string v1, "MD5"

    invoke-static {v1}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 157
    :goto_1
    if-eqz v1, :cond_0

    .line 161
    const-string v0, ":"

    invoke-virtual {p1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 162
    invoke-virtual {v0, p2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 163
    invoke-virtual {v1}, Ljava/security/MessageDigest;->reset()V

    .line 165
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v0

    .line 166
    invoke-static {v0}, LIIlIIlIIlIlIllIIIIlI;->llIIIIlllllIIllIIllI([B)[B

    move-result-object v0

    .line 168
    new-instance v2, Ljava/lang/String;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 169
    invoke-virtual {v1, p5}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v0

    .line 170
    invoke-static {v0}, LIIlIIlIIlIlIllIIIIlI;->llIIIIlllllIIllIIllI([B)[B

    move-result-object v0

    .line 171
    new-instance v3, Ljava/lang/String;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v4

    invoke-direct {v3, v0, v4}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 173
    const-string v0, ":"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 174
    new-instance v2, Ljava/lang/String;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v4

    invoke-direct {v2, p3, v4}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 175
    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 176
    invoke-virtual {v0, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 177
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v1

    .line 178
    const/4 v0, 0x2

    if-ne p0, v0, :cond_5

    .line 180
    invoke-static {v1}, LIIlIIlIIlIlIllIIIIlI;->llIIIIlllllIIllIIllI([B)[B

    move-result-object v1

    .line 181
    new-instance v0, Ljava/lang/String;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    goto/16 :goto_0

    .line 151
    :catch_0
    move-exception v1

    .line 153
    sget-object v2, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    invoke-virtual {v2, v1}, LIIIIllIlIIlIIlIlIllI;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V

    move-object v1, v0

    goto :goto_1

    .line 186
    :cond_5
    new-instance v0, Ljava/lang/String;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    goto/16 :goto_0

    .line 80
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch

    .line 117
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method

.method public static llIIIIlllllIIllIIllI(I[BI[B)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    const/16 v6, 0x40

    .line 198
    .line 199
    new-array v3, v6, [B

    .line 200
    new-array v4, v6, [B

    .line 203
    const-string v2, ""

    .line 205
    packed-switch p0, :pswitch_data_0

    .line 214
    sget-object v1, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v2, "Not Support Auth Type."

    invoke-virtual {v1, v2}, LIIIIllIlIIlIIlIlIllI;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 267
    :cond_0
    :goto_0
    return-object v0

    .line 208
    :pswitch_0
    if-eqz p1, :cond_0

    if-eqz p3, :cond_0

    .line 218
    if-le p2, v6, :cond_2

    :cond_1
    move v2, v1

    .line 230
    :goto_1
    if-lt v2, v6, :cond_3

    .line 235
    :goto_2
    if-lt v1, v6, :cond_4

    .line 244
    :try_start_0
    const-string v1, "SHA-1"

    invoke-static {v1}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 252
    :goto_3
    if-eqz v1, :cond_0

    .line 255
    invoke-virtual {v1, v3}, Ljava/security/MessageDigest;->update([B)V

    .line 256
    invoke-virtual {v1, p3}, Ljava/security/MessageDigest;->update([B)V

    .line 257
    invoke-virtual {v1}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v0

    .line 259
    invoke-virtual {v1, v4}, Ljava/security/MessageDigest;->update([B)V

    .line 260
    invoke-virtual {v1, v0}, Ljava/security/MessageDigest;->update([B)V

    .line 261
    invoke-virtual {v1}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v0

    .line 263
    invoke-static {v0}, LIllllIlIIllIlllIlIIl;->llIIIIlllllIIllIIllI([B)Ljava/lang/String;

    move-result-object v0

    .line 264
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 265
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    move v2, v1

    .line 224
    :goto_4
    if-ge v2, p2, :cond_1

    .line 226
    aget-byte v5, p1, v2

    aput-byte v5, v3, v2

    .line 224
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 232
    :cond_3
    aget-byte v5, v3, v2

    aput-byte v5, v4, v2

    .line 230
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 237
    :cond_4
    aget-byte v2, v3, v1

    xor-int/lit8 v2, v2, 0x36

    int-to-byte v2, v2

    aput-byte v2, v3, v1

    .line 238
    aget-byte v2, v4, v1

    xor-int/lit8 v2, v2, 0x5c

    int-to-byte v2, v2

    aput-byte v2, v4, v1

    .line 235
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 246
    :catch_0
    move-exception v1

    .line 248
    sget-object v2, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    invoke-virtual {v2, v1}, LIIIIllIlIIlIIlIlIllI;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V

    move-object v1, v0

    goto :goto_3

    .line 205
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public static llllIIIllIlIIIIllllI(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 63
    const-string v0, "BASIC"

    invoke-virtual {v0, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 64
    const/4 v0, 0x0

    .line 70
    :goto_0
    return v0

    .line 65
    :cond_0
    const-string v0, "DIGEST"

    invoke-virtual {v0, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1

    .line 66
    const/4 v0, 0x1

    goto :goto_0

    .line 67
    :cond_1
    const-string v0, "HMAC"

    invoke-virtual {v0, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_2

    .line 68
    const/4 v0, 0x2

    goto :goto_0

    .line 70
    :cond_2
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static llllIIIllIlIIIIllllI(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    packed-switch p0, :pswitch_data_0

    .line 57
    const-string v0, "NONE"

    :goto_0
    return-object v0

    .line 51
    :pswitch_0
    const-string v0, "BASIC"

    goto :goto_0

    .line 53
    :pswitch_1
    const-string v0, "DIGEST"

    goto :goto_0

    .line 55
    :pswitch_2
    const-string v0, "HMAC"

    goto :goto_0

    .line 48
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
