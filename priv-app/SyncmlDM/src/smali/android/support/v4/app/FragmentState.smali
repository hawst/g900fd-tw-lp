.class final Landroid/support/v4/app/FragmentState;
.super Ljava/lang/Object;
.source "llIIIIlllllIIllIIllI"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final IIIIllIlIIlIIIIlllIl:Z

.field IIIlIIllIlIIllIlllII:Landroid/os/Bundle;

.field IlIlIlIlIlIIlllllIlI:Landroid/support/v4/app/Fragment;

.field final IllIlIIIIlIIlIIIllIl:I

.field final llIIIIlllllIIllIIllI:Ljava/lang/String;

.field final llIIllllIIlllIIIIlll:Ljava/lang/String;

.field final llIlIIIIlIIIIIlIlIII:I

.field final llIlIllllllllllllllI:Landroid/os/Bundle;

.field final lllIlIlIIIllIIlIllIl:Z

.field final llllIIIllIlIIIIllllI:I

.field final llllllIllIlIlllIIlIl:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 140
    new-instance v0, Landroid/support/v4/app/IIlIlIIIlIIlIlIIIIII;

    invoke-direct {v0}, Landroid/support/v4/app/IIlIlIIIlIIlIlIIIIII;-><init>()V

    sput-object v0, Landroid/support/v4/app/FragmentState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/FragmentState;->llIIIIlllllIIllIIllI:Ljava/lang/String;

    .line 81
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/support/v4/app/FragmentState;->llllIIIllIlIIIIllllI:I

    .line 82
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Landroid/support/v4/app/FragmentState;->lllIlIlIIIllIIlIllIl:Z

    .line 83
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/support/v4/app/FragmentState;->llIlIIIIlIIIIIlIlIII:I

    .line 84
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/support/v4/app/FragmentState;->IllIlIIIIlIIlIIIllIl:I

    .line 85
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/FragmentState;->llIIllllIIlllIIIIlll:Ljava/lang/String;

    .line 86
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Landroid/support/v4/app/FragmentState;->IIIIllIlIIlIIIIlllIl:Z

    .line 87
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2

    :goto_2
    iput-boolean v1, p0, Landroid/support/v4/app/FragmentState;->llllllIllIlIlllIIlIl:Z

    .line 88
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/FragmentState;->llIlIllllllllllllllI:Landroid/os/Bundle;

    .line 89
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/FragmentState;->IIIlIIllIlIIllIlllII:Landroid/os/Bundle;

    .line 90
    return-void

    :cond_0
    move v0, v2

    .line 82
    goto :goto_0

    :cond_1
    move v0, v2

    .line 86
    goto :goto_1

    :cond_2
    move v1, v2

    .line 87
    goto :goto_2
.end method

.method public constructor <init>(Landroid/support/v4/app/Fragment;)V
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/FragmentState;->llIIIIlllllIIllIIllI:Ljava/lang/String;

    .line 69
    iget v0, p1, Landroid/support/v4/app/Fragment;->IIIIllIlIIlIIIIlllIl:I

    iput v0, p0, Landroid/support/v4/app/FragmentState;->llllIIIllIlIIIIllllI:I

    .line 70
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->IIlIlIIlIlIIlIlllIIl:Z

    iput-boolean v0, p0, Landroid/support/v4/app/FragmentState;->lllIlIlIIIllIIlIllIl:Z

    .line 71
    iget v0, p1, Landroid/support/v4/app/Fragment;->lIIIIIIlIlllIIlllIIl:I

    iput v0, p0, Landroid/support/v4/app/FragmentState;->llIlIIIIlIIIIIlIlIII:I

    .line 72
    iget v0, p1, Landroid/support/v4/app/Fragment;->lIIIIIIllIIllIlIllII:I

    iput v0, p0, Landroid/support/v4/app/FragmentState;->IllIlIIIIlIIlIIIllIl:I

    .line 73
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->llllIllIIIIIlIIllIII:Ljava/lang/String;

    iput-object v0, p0, Landroid/support/v4/app/FragmentState;->llIIllllIIlllIIIIlll:Ljava/lang/String;

    .line 74
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->llIIlIlIlIlIIIIIIlIl:Z

    iput-boolean v0, p0, Landroid/support/v4/app/FragmentState;->IIIIllIlIIlIIIIlllIl:Z

    .line 75
    iget-boolean v0, p1, Landroid/support/v4/app/Fragment;->lllllllIlllIIlllIlIl:Z

    iput-boolean v0, p0, Landroid/support/v4/app/FragmentState;->llllllIllIlIlllIIlIl:Z

    .line 76
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->llIlIllllllllllllllI:Landroid/os/Bundle;

    iput-object v0, p0, Landroid/support/v4/app/FragmentState;->llIlIllllllllllllllI:Landroid/os/Bundle;

    .line 77
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 124
    const/4 v0, 0x0

    return v0
.end method

.method public llIIIIlllllIIllIIllI(Landroid/support/v4/app/IIIIIIlIIIllllllIlII;Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/Fragment;
    .locals 3

    .prologue
    .line 93
    iget-object v0, p0, Landroid/support/v4/app/FragmentState;->IlIlIlIlIlIIlllllIlI:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Landroid/support/v4/app/FragmentState;->IlIlIlIlIlIIlllllIlI:Landroid/support/v4/app/Fragment;

    .line 120
    :goto_0
    return-object v0

    .line 97
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/FragmentState;->llIlIllllllllllllllI:Landroid/os/Bundle;

    if-eqz v0, :cond_1

    .line 98
    iget-object v0, p0, Landroid/support/v4/app/FragmentState;->llIlIllllllllllllllI:Landroid/os/Bundle;

    invoke-virtual {p1}, Landroid/support/v4/app/IIIIIIlIIIllllllIlII;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 101
    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/FragmentState;->llIIIIlllllIIllIIllI:Ljava/lang/String;

    iget-object v1, p0, Landroid/support/v4/app/FragmentState;->llIlIllllllllllllllI:Landroid/os/Bundle;

    invoke-static {p1, v0, v1}, Landroid/support/v4/app/Fragment;->llIIIIlllllIIllIIllI(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/FragmentState;->IlIlIlIlIlIIlllllIlI:Landroid/support/v4/app/Fragment;

    .line 103
    iget-object v0, p0, Landroid/support/v4/app/FragmentState;->IIIlIIllIlIIllIlllII:Landroid/os/Bundle;

    if-eqz v0, :cond_2

    .line 104
    iget-object v0, p0, Landroid/support/v4/app/FragmentState;->IIIlIIllIlIIllIlllII:Landroid/os/Bundle;

    invoke-virtual {p1}, Landroid/support/v4/app/IIIIIIlIIIllllllIlII;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 105
    iget-object v0, p0, Landroid/support/v4/app/FragmentState;->IlIlIlIlIlIIlllllIlI:Landroid/support/v4/app/Fragment;

    iget-object v1, p0, Landroid/support/v4/app/FragmentState;->IIIlIIllIlIIllIlllII:Landroid/os/Bundle;

    iput-object v1, v0, Landroid/support/v4/app/Fragment;->IllIlIIIIlIIlIIIllIl:Landroid/os/Bundle;

    .line 107
    :cond_2
    iget-object v0, p0, Landroid/support/v4/app/FragmentState;->IlIlIlIlIlIIlllllIlI:Landroid/support/v4/app/Fragment;

    iget v1, p0, Landroid/support/v4/app/FragmentState;->llllIIIllIlIIIIllllI:I

    invoke-virtual {v0, v1, p2}, Landroid/support/v4/app/Fragment;->llIIIIlllllIIllIIllI(ILandroid/support/v4/app/Fragment;)V

    .line 108
    iget-object v0, p0, Landroid/support/v4/app/FragmentState;->IlIlIlIlIlIIlllllIlI:Landroid/support/v4/app/Fragment;

    iget-boolean v1, p0, Landroid/support/v4/app/FragmentState;->lllIlIlIIIllIIlIllIl:Z

    iput-boolean v1, v0, Landroid/support/v4/app/Fragment;->IIlIlIIlIlIIlIlllIIl:Z

    .line 109
    iget-object v0, p0, Landroid/support/v4/app/FragmentState;->IlIlIlIlIlIIlllllIlI:Landroid/support/v4/app/Fragment;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v4/app/Fragment;->lIlIIlIlIIlIlIIIIlll:Z

    .line 110
    iget-object v0, p0, Landroid/support/v4/app/FragmentState;->IlIlIlIlIlIIlllllIlI:Landroid/support/v4/app/Fragment;

    iget v1, p0, Landroid/support/v4/app/FragmentState;->llIlIIIIlIIIIIlIlIII:I

    iput v1, v0, Landroid/support/v4/app/Fragment;->lIIIIIIlIlllIIlllIIl:I

    .line 111
    iget-object v0, p0, Landroid/support/v4/app/FragmentState;->IlIlIlIlIlIIlllllIlI:Landroid/support/v4/app/Fragment;

    iget v1, p0, Landroid/support/v4/app/FragmentState;->IllIlIIIIlIIlIIIllIl:I

    iput v1, v0, Landroid/support/v4/app/Fragment;->lIIIIIIllIIllIlIllII:I

    .line 112
    iget-object v0, p0, Landroid/support/v4/app/FragmentState;->IlIlIlIlIlIIlllllIlI:Landroid/support/v4/app/Fragment;

    iget-object v1, p0, Landroid/support/v4/app/FragmentState;->llIIllllIIlllIIIIlll:Ljava/lang/String;

    iput-object v1, v0, Landroid/support/v4/app/Fragment;->llllIllIIIIIlIIllIII:Ljava/lang/String;

    .line 113
    iget-object v0, p0, Landroid/support/v4/app/FragmentState;->IlIlIlIlIlIIlllllIlI:Landroid/support/v4/app/Fragment;

    iget-boolean v1, p0, Landroid/support/v4/app/FragmentState;->IIIIllIlIIlIIIIlllIl:Z

    iput-boolean v1, v0, Landroid/support/v4/app/Fragment;->llIIlIlIlIlIIIIIIlIl:Z

    .line 114
    iget-object v0, p0, Landroid/support/v4/app/FragmentState;->IlIlIlIlIlIIlllllIlI:Landroid/support/v4/app/Fragment;

    iget-boolean v1, p0, Landroid/support/v4/app/FragmentState;->llllllIllIlIlllIIlIl:Z

    iput-boolean v1, v0, Landroid/support/v4/app/Fragment;->lllllllIlllIIlllIlIl:Z

    .line 115
    iget-object v0, p0, Landroid/support/v4/app/FragmentState;->IlIlIlIlIlIIlllllIlI:Landroid/support/v4/app/Fragment;

    iget-object v1, p1, Landroid/support/v4/app/IIIIIIlIIIllllllIlII;->llllIIIllIlIIIIllllI:Landroid/support/v4/app/lIlIIlIlIIlIlIIIIlll;

    iput-object v1, v0, Landroid/support/v4/app/Fragment;->IIIIlllIIIlIlIlllIII:Landroid/support/v4/app/lIlIIlIlIIlIlIIIIlll;

    .line 117
    sget-boolean v0, Landroid/support/v4/app/lIlIIlIlIIlIlIIIIlll;->llIIIIlllllIIllIIllI:Z

    if-eqz v0, :cond_3

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Instantiated fragment "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v4/app/FragmentState;->IlIlIlIlIlIIlllllIlI:Landroid/support/v4/app/Fragment;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    :cond_3
    iget-object v0, p0, Landroid/support/v4/app/FragmentState;->IlIlIlIlIlIIlllllIlI:Landroid/support/v4/app/Fragment;

    goto/16 :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 128
    iget-object v0, p0, Landroid/support/v4/app/FragmentState;->llIIIIlllllIIllIIllI:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 129
    iget v0, p0, Landroid/support/v4/app/FragmentState;->llllIIIllIlIIIIllllI:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 130
    iget-boolean v0, p0, Landroid/support/v4/app/FragmentState;->lllIlIlIIIllIIlIllIl:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 131
    iget v0, p0, Landroid/support/v4/app/FragmentState;->llIlIIIIlIIIIIlIlIII:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 132
    iget v0, p0, Landroid/support/v4/app/FragmentState;->IllIlIIIIlIIlIIIllIl:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 133
    iget-object v0, p0, Landroid/support/v4/app/FragmentState;->llIIllllIIlllIIIIlll:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 134
    iget-boolean v0, p0, Landroid/support/v4/app/FragmentState;->IIIIllIlIIlIIIIlllIl:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 135
    iget-boolean v0, p0, Landroid/support/v4/app/FragmentState;->llllllIllIlIlllIIlIl:Z

    if-eqz v0, :cond_2

    :goto_2
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 136
    iget-object v0, p0, Landroid/support/v4/app/FragmentState;->llIlIllllllllllllllI:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 137
    iget-object v0, p0, Landroid/support/v4/app/FragmentState;->IIIlIIllIlIIllIlllII:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 138
    return-void

    :cond_0
    move v0, v2

    .line 130
    goto :goto_0

    :cond_1
    move v0, v2

    .line 134
    goto :goto_1

    :cond_2
    move v1, v2

    .line 135
    goto :goto_2
.end method
