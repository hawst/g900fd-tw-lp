.class final Landroid/support/v4/app/BackStackState;
.super Ljava/lang/Object;
.source "llIIIIlllllIIllIIllI"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final IIIIllIlIIlIIIIlllIl:Ljava/lang/CharSequence;

.field final IIIlIIllIlIIllIlllII:Ljava/util/ArrayList;

.field final IlIlIlIlIlIIlllllIlI:Ljava/util/ArrayList;

.field final IllIlIIIIlIIlIIIllIl:I

.field final llIIIIlllllIIllIIllI:[I

.field final llIIllllIIlllIIIIlll:I

.field final llIlIIIIlIIIIIlIlIII:Ljava/lang/String;

.field final llIlIllllllllllllllI:Ljava/lang/CharSequence;

.field final lllIlIlIIIllIIlIllIl:I

.field final llllIIIllIlIIIIllllI:I

.field final llllllIllIlIlllIIlIl:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 176
    new-instance v0, Landroid/support/v4/app/IIIlIIllIlIIllIlllII;

    invoke-direct {v0}, Landroid/support/v4/app/IIIlIIllIlIIllIlllII;-><init>()V

    sput-object v0, Landroid/support/v4/app/BackStackState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    invoke-virtual {p1}, Landroid/os/Parcel;->createIntArray()[I

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/BackStackState;->llIIIIlllllIIllIIllI:[I

    .line 98
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/support/v4/app/BackStackState;->llllIIIllIlIIIIllllI:I

    .line 99
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/support/v4/app/BackStackState;->lllIlIlIIIllIIlIllIl:I

    .line 100
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/BackStackState;->llIlIIIIlIIIIIlIlIII:Ljava/lang/String;

    .line 101
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/support/v4/app/BackStackState;->IllIlIIIIlIIlIIIllIl:I

    .line 102
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/support/v4/app/BackStackState;->llIIllllIIlllIIIIlll:I

    .line 103
    sget-object v0, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    iput-object v0, p0, Landroid/support/v4/app/BackStackState;->IIIIllIlIIlIIIIlllIl:Ljava/lang/CharSequence;

    .line 104
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/support/v4/app/BackStackState;->llllllIllIlIlllIIlIl:I

    .line 105
    sget-object v0, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    iput-object v0, p0, Landroid/support/v4/app/BackStackState;->llIlIllllllllllllllI:Ljava/lang/CharSequence;

    .line 106
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/BackStackState;->IIIlIIllIlIIllIlllII:Ljava/util/ArrayList;

    .line 107
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/BackStackState;->IlIlIlIlIlIIlllllIlI:Ljava/util/ArrayList;

    .line 108
    return-void
.end method

.method public constructor <init>(Landroid/support/v4/app/lIlIIlIlIIlIlIIIIlll;Landroid/support/v4/app/llIlIIIIlIIIIIlIlIII;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iget-object v0, p2, Landroid/support/v4/app/llIlIIIIlIIIIIlIlIII;->llllIIIllIlIIIIllllI:Landroid/support/v4/app/llllllIllIlIlllIIlIl;

    move-object v1, v0

    move v0, v3

    .line 54
    :goto_0
    if-eqz v1, :cond_1

    .line 55
    iget-object v2, v1, Landroid/support/v4/app/llllllIllIlIlllIIlIl;->llIlIllllllllllllllI:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    iget-object v2, v1, Landroid/support/v4/app/llllllIllIlIlllIIlIl;->llIlIllllllllllllllI:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/2addr v0, v2

    .line 56
    :cond_0
    iget-object v1, v1, Landroid/support/v4/app/llllllIllIlIlllIIlIl;->llIIIIlllllIIllIIllI:Landroid/support/v4/app/llllllIllIlIlllIIlIl;

    goto :goto_0

    .line 58
    :cond_1
    iget v1, p2, Landroid/support/v4/app/llIlIIIIlIIIIIlIlIII;->llIlIIIIlIIIIIlIlIII:I

    mul-int/lit8 v1, v1, 0x7

    add-int/2addr v0, v1

    new-array v0, v0, [I

    iput-object v0, p0, Landroid/support/v4/app/BackStackState;->llIIIIlllllIIllIIllI:[I

    .line 60
    iget-boolean v0, p2, Landroid/support/v4/app/llIlIIIIlIIIIIlIlIII;->IlIlIlIlIlIIlllllIlI:Z

    if-nez v0, :cond_2

    .line 61
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not on back stack"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 64
    :cond_2
    iget-object v0, p2, Landroid/support/v4/app/llIlIIIIlIIIIIlIlIII;->llllIIIllIlIIIIllllI:Landroid/support/v4/app/llllllIllIlIlllIIlIl;

    move-object v5, v0

    move v0, v3

    .line 66
    :goto_1
    if-eqz v5, :cond_6

    .line 67
    iget-object v1, p0, Landroid/support/v4/app/BackStackState;->llIIIIlllllIIllIIllI:[I

    add-int/lit8 v2, v0, 0x1

    iget v4, v5, Landroid/support/v4/app/llllllIllIlIlllIIlIl;->lllIlIlIIIllIIlIllIl:I

    aput v4, v1, v0

    .line 68
    iget-object v1, p0, Landroid/support/v4/app/BackStackState;->llIIIIlllllIIllIIllI:[I

    add-int/lit8 v4, v2, 0x1

    iget-object v0, v5, Landroid/support/v4/app/llllllIllIlIlllIIlIl;->llIlIIIIlIIIIIlIlIII:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_3

    iget-object v0, v5, Landroid/support/v4/app/llllllIllIlIlllIIlIl;->llIlIIIIlIIIIIlIlIII:Landroid/support/v4/app/Fragment;

    iget v0, v0, Landroid/support/v4/app/Fragment;->IIIIllIlIIlIIIIlllIl:I

    :goto_2
    aput v0, v1, v2

    .line 69
    iget-object v0, p0, Landroid/support/v4/app/BackStackState;->llIIIIlllllIIllIIllI:[I

    add-int/lit8 v1, v4, 0x1

    iget v2, v5, Landroid/support/v4/app/llllllIllIlIlllIIlIl;->IllIlIIIIlIIlIIIllIl:I

    aput v2, v0, v4

    .line 70
    iget-object v0, p0, Landroid/support/v4/app/BackStackState;->llIIIIlllllIIllIIllI:[I

    add-int/lit8 v2, v1, 0x1

    iget v4, v5, Landroid/support/v4/app/llllllIllIlIlllIIlIl;->llIIllllIIlllIIIIlll:I

    aput v4, v0, v1

    .line 71
    iget-object v0, p0, Landroid/support/v4/app/BackStackState;->llIIIIlllllIIllIIllI:[I

    add-int/lit8 v1, v2, 0x1

    iget v4, v5, Landroid/support/v4/app/llllllIllIlIlllIIlIl;->IIIIllIlIIlIIIIlllIl:I

    aput v4, v0, v2

    .line 72
    iget-object v0, p0, Landroid/support/v4/app/BackStackState;->llIIIIlllllIIllIIllI:[I

    add-int/lit8 v2, v1, 0x1

    iget v4, v5, Landroid/support/v4/app/llllllIllIlIlllIIlIl;->llllllIllIlIlllIIlIl:I

    aput v4, v0, v1

    .line 73
    iget-object v0, v5, Landroid/support/v4/app/llllllIllIlIlllIIlIl;->llIlIllllllllllllllI:Ljava/util/ArrayList;

    if-eqz v0, :cond_5

    .line 74
    iget-object v0, v5, Landroid/support/v4/app/llllllIllIlIlllIIlIl;->llIlIllllllllllllllI:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 75
    iget-object v0, p0, Landroid/support/v4/app/BackStackState;->llIIIIlllllIIllIIllI:[I

    add-int/lit8 v1, v2, 0x1

    aput v6, v0, v2

    move v2, v3

    .line 76
    :goto_3
    if-ge v2, v6, :cond_4

    .line 77
    iget-object v7, p0, Landroid/support/v4/app/BackStackState;->llIIIIlllllIIllIIllI:[I

    add-int/lit8 v4, v1, 0x1

    iget-object v0, v5, Landroid/support/v4/app/llllllIllIlIlllIIlIl;->llIlIllllllllllllllI:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    iget v0, v0, Landroid/support/v4/app/Fragment;->IIIIllIlIIlIIIIlllIl:I

    aput v0, v7, v1

    .line 76
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v1, v4

    goto :goto_3

    .line 68
    :cond_3
    const/4 v0, -0x1

    goto :goto_2

    :cond_4
    move v0, v1

    .line 82
    :goto_4
    iget-object v1, v5, Landroid/support/v4/app/llllllIllIlIlllIIlIl;->llIIIIlllllIIllIIllI:Landroid/support/v4/app/llllllIllIlIlllIIlIl;

    move-object v5, v1

    goto :goto_1

    .line 80
    :cond_5
    iget-object v1, p0, Landroid/support/v4/app/BackStackState;->llIIIIlllllIIllIIllI:[I

    add-int/lit8 v0, v2, 0x1

    aput v3, v1, v2

    goto :goto_4

    .line 84
    :cond_6
    iget v0, p2, Landroid/support/v4/app/llIlIIIIlIIIIIlIlIII;->llIlIllllllllllllllI:I

    iput v0, p0, Landroid/support/v4/app/BackStackState;->llllIIIllIlIIIIllllI:I

    .line 85
    iget v0, p2, Landroid/support/v4/app/llIlIIIIlIIIIIlIlIII;->IIIlIIllIlIIllIlllII:I

    iput v0, p0, Landroid/support/v4/app/BackStackState;->lllIlIlIIIllIIlIllIl:I

    .line 86
    iget-object v0, p2, Landroid/support/v4/app/llIlIIIIlIIIIIlIlIII;->IIIIIIlIIIllllllIlII:Ljava/lang/String;

    iput-object v0, p0, Landroid/support/v4/app/BackStackState;->llIlIIIIlIIIIIlIlIII:Ljava/lang/String;

    .line 87
    iget v0, p2, Landroid/support/v4/app/llIlIIIIlIIIIIlIlIII;->IlIlllIIlIlIIIlIlIll:I

    iput v0, p0, Landroid/support/v4/app/BackStackState;->IllIlIIIIlIIlIIIllIl:I

    .line 88
    iget v0, p2, Landroid/support/v4/app/llIlIIIIlIIIIIlIlIII;->IIlIlIIlIlIIlIlllIIl:I

    iput v0, p0, Landroid/support/v4/app/BackStackState;->llIIllllIIlllIIIIlll:I

    .line 89
    iget-object v0, p2, Landroid/support/v4/app/llIlIIIIlIIIIIlIlIII;->lllllIlIlIIIIIIIIlIl:Ljava/lang/CharSequence;

    iput-object v0, p0, Landroid/support/v4/app/BackStackState;->IIIIllIlIIlIIIIlllIl:Ljava/lang/CharSequence;

    .line 90
    iget v0, p2, Landroid/support/v4/app/llIlIIIIlIIIIIlIlIII;->lIlIIlIlIIlIlIIIIlll:I

    iput v0, p0, Landroid/support/v4/app/BackStackState;->llllllIllIlIlllIIlIl:I

    .line 91
    iget-object v0, p2, Landroid/support/v4/app/llIlIIIIlIIIIIlIlIII;->IIllIlIIIIlllIIlIIll:Ljava/lang/CharSequence;

    iput-object v0, p0, Landroid/support/v4/app/BackStackState;->llIlIllllllllllllllI:Ljava/lang/CharSequence;

    .line 92
    iget-object v0, p2, Landroid/support/v4/app/llIlIIIIlIIIIIlIlIII;->IIIIlllIIIlIlIlllIII:Ljava/util/ArrayList;

    iput-object v0, p0, Landroid/support/v4/app/BackStackState;->IIIlIIllIlIIllIlllII:Ljava/util/ArrayList;

    .line 93
    iget-object v0, p2, Landroid/support/v4/app/llIlIIIIlIIIIIlIlIII;->IIIIlIlIIIIIIIlIIlll:Ljava/util/ArrayList;

    iput-object v0, p0, Landroid/support/v4/app/BackStackState;->IlIlIlIlIlIIlllllIlI:Ljava/util/ArrayList;

    .line 94
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 159
    const/4 v0, 0x0

    return v0
.end method

.method public llIIIIlllllIIllIIllI(Landroid/support/v4/app/lIlIIlIlIIlIlIIIIlll;)Landroid/support/v4/app/llIlIIIIlIIIIIlIlIII;
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v2, 0x0

    .line 111
    new-instance v6, Landroid/support/v4/app/llIlIIIIlIIIIIlIlIII;

    invoke-direct {v6, p1}, Landroid/support/v4/app/llIlIIIIlIIIIIlIlIII;-><init>(Landroid/support/v4/app/lIlIIlIlIIlIlIIIIlll;)V

    move v1, v2

    move v0, v2

    .line 114
    :goto_0
    iget-object v3, p0, Landroid/support/v4/app/BackStackState;->llIIIIlllllIIllIIllI:[I

    array-length v3, v3

    if-ge v0, v3, :cond_4

    .line 115
    new-instance v7, Landroid/support/v4/app/llllllIllIlIlllIIlIl;

    invoke-direct {v7}, Landroid/support/v4/app/llllllIllIlIlllIIlIl;-><init>()V

    .line 116
    iget-object v3, p0, Landroid/support/v4/app/BackStackState;->llIIIIlllllIIllIIllI:[I

    add-int/lit8 v4, v0, 0x1

    aget v0, v3, v0

    iput v0, v7, Landroid/support/v4/app/llllllIllIlIlllIIlIl;->lllIlIlIIIllIIlIllIl:I

    .line 117
    sget-boolean v0, Landroid/support/v4/app/lIlIIlIlIIlIlIIIIlll;->llIIIIlllllIIllIIllI:Z

    if-eqz v0, :cond_0

    const-string v0, "FragmentManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Instantiate "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " op #"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " base fragment #"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Landroid/support/v4/app/BackStackState;->llIIIIlllllIIllIIllI:[I

    aget v5, v5, v4

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/BackStackState;->llIIIIlllllIIllIIllI:[I

    add-int/lit8 v3, v4, 0x1

    aget v0, v0, v4

    .line 120
    if-ltz v0, :cond_2

    .line 121
    iget-object v4, p1, Landroid/support/v4/app/lIlIIlIlIIlIlIIIIlll;->llIIllllIIlllIIIIlll:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 122
    iput-object v0, v7, Landroid/support/v4/app/llllllIllIlIlllIIlIl;->llIlIIIIlIIIIIlIlIII:Landroid/support/v4/app/Fragment;

    .line 126
    :goto_1
    iget-object v0, p0, Landroid/support/v4/app/BackStackState;->llIIIIlllllIIllIIllI:[I

    add-int/lit8 v4, v3, 0x1

    aget v0, v0, v3

    iput v0, v7, Landroid/support/v4/app/llllllIllIlIlllIIlIl;->IllIlIIIIlIIlIIIllIl:I

    .line 127
    iget-object v0, p0, Landroid/support/v4/app/BackStackState;->llIIIIlllllIIllIIllI:[I

    add-int/lit8 v3, v4, 0x1

    aget v0, v0, v4

    iput v0, v7, Landroid/support/v4/app/llllllIllIlIlllIIlIl;->llIIllllIIlllIIIIlll:I

    .line 128
    iget-object v0, p0, Landroid/support/v4/app/BackStackState;->llIIIIlllllIIllIIllI:[I

    add-int/lit8 v4, v3, 0x1

    aget v0, v0, v3

    iput v0, v7, Landroid/support/v4/app/llllllIllIlIlllIIlIl;->IIIIllIlIIlIIIIlllIl:I

    .line 129
    iget-object v0, p0, Landroid/support/v4/app/BackStackState;->llIIIIlllllIIllIIllI:[I

    add-int/lit8 v5, v4, 0x1

    aget v0, v0, v4

    iput v0, v7, Landroid/support/v4/app/llllllIllIlIlllIIlIl;->llllllIllIlIlllIIlIl:I

    .line 130
    iget-object v0, p0, Landroid/support/v4/app/BackStackState;->llIIIIlllllIIllIIllI:[I

    add-int/lit8 v3, v5, 0x1

    aget v8, v0, v5

    .line 131
    if-lez v8, :cond_3

    .line 132
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v8}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, v7, Landroid/support/v4/app/llllllIllIlIlllIIlIl;->llIlIllllllllllllllI:Ljava/util/ArrayList;

    move v4, v2

    .line 133
    :goto_2
    if-ge v4, v8, :cond_3

    .line 134
    sget-boolean v0, Landroid/support/v4/app/lIlIIlIlIIlIlIIIIlll;->llIIIIlllllIIllIIllI:Z

    if-eqz v0, :cond_1

    const-string v0, "FragmentManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Instantiate "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, " set remove fragment #"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v9, p0, Landroid/support/v4/app/BackStackState;->llIIIIlllllIIllIIllI:[I

    aget v9, v9, v3

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    :cond_1
    iget-object v0, p1, Landroid/support/v4/app/lIlIIlIlIIlIlIIIIlll;->llIIllllIIlllIIIIlll:Ljava/util/ArrayList;

    iget-object v9, p0, Landroid/support/v4/app/BackStackState;->llIIIIlllllIIllIIllI:[I

    add-int/lit8 v5, v3, 0x1

    aget v3, v9, v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 137
    iget-object v3, v7, Landroid/support/v4/app/llllllIllIlIlllIIlIl;->llIlIllllllllllllllI:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 133
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    move v3, v5

    goto :goto_2

    .line 124
    :cond_2
    const/4 v0, 0x0

    iput-object v0, v7, Landroid/support/v4/app/llllllIllIlIlllIIlIl;->llIlIIIIlIIIIIlIlIII:Landroid/support/v4/app/Fragment;

    goto :goto_1

    .line 140
    :cond_3
    invoke-virtual {v6, v7}, Landroid/support/v4/app/llIlIIIIlIIIIIlIlIII;->llIIIIlllllIIllIIllI(Landroid/support/v4/app/llllllIllIlIlllIIlIl;)V

    .line 141
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v3

    .line 142
    goto/16 :goto_0

    .line 143
    :cond_4
    iget v0, p0, Landroid/support/v4/app/BackStackState;->llllIIIllIlIIIIllllI:I

    iput v0, v6, Landroid/support/v4/app/llIlIIIIlIIIIIlIlIII;->llIlIllllllllllllllI:I

    .line 144
    iget v0, p0, Landroid/support/v4/app/BackStackState;->lllIlIlIIIllIIlIllIl:I

    iput v0, v6, Landroid/support/v4/app/llIlIIIIlIIIIIlIlIII;->IIIlIIllIlIIllIlllII:I

    .line 145
    iget-object v0, p0, Landroid/support/v4/app/BackStackState;->llIlIIIIlIIIIIlIlIII:Ljava/lang/String;

    iput-object v0, v6, Landroid/support/v4/app/llIlIIIIlIIIIIlIlIII;->IIIIIIlIIIllllllIlII:Ljava/lang/String;

    .line 146
    iget v0, p0, Landroid/support/v4/app/BackStackState;->IllIlIIIIlIIlIIIllIl:I

    iput v0, v6, Landroid/support/v4/app/llIlIIIIlIIIIIlIlIII;->IlIlllIIlIlIIIlIlIll:I

    .line 147
    iput-boolean v10, v6, Landroid/support/v4/app/llIlIIIIlIIIIIlIlIII;->IlIlIlIlIlIIlllllIlI:Z

    .line 148
    iget v0, p0, Landroid/support/v4/app/BackStackState;->llIIllllIIlllIIIIlll:I

    iput v0, v6, Landroid/support/v4/app/llIlIIIIlIIIIIlIlIII;->IIlIlIIlIlIIlIlllIIl:I

    .line 149
    iget-object v0, p0, Landroid/support/v4/app/BackStackState;->IIIIllIlIIlIIIIlllIl:Ljava/lang/CharSequence;

    iput-object v0, v6, Landroid/support/v4/app/llIlIIIIlIIIIIlIlIII;->lllllIlIlIIIIIIIIlIl:Ljava/lang/CharSequence;

    .line 150
    iget v0, p0, Landroid/support/v4/app/BackStackState;->llllllIllIlIlllIIlIl:I

    iput v0, v6, Landroid/support/v4/app/llIlIIIIlIIIIIlIlIII;->lIlIIlIlIIlIlIIIIlll:I

    .line 151
    iget-object v0, p0, Landroid/support/v4/app/BackStackState;->llIlIllllllllllllllI:Ljava/lang/CharSequence;

    iput-object v0, v6, Landroid/support/v4/app/llIlIIIIlIIIIIlIlIII;->IIllIlIIIIlllIIlIIll:Ljava/lang/CharSequence;

    .line 152
    iget-object v0, p0, Landroid/support/v4/app/BackStackState;->IIIlIIllIlIIllIlllII:Ljava/util/ArrayList;

    iput-object v0, v6, Landroid/support/v4/app/llIlIIIIlIIIIIlIlIII;->IIIIlllIIIlIlIlllIII:Ljava/util/ArrayList;

    .line 153
    iget-object v0, p0, Landroid/support/v4/app/BackStackState;->IlIlIlIlIlIIlllllIlI:Ljava/util/ArrayList;

    iput-object v0, v6, Landroid/support/v4/app/llIlIIIIlIIIIIlIlIII;->IIIIlIlIIIIIIIlIIlll:Ljava/util/ArrayList;

    .line 154
    invoke-virtual {v6, v10}, Landroid/support/v4/app/llIlIIIIlIIIIIlIlIII;->llIIIIlllllIIllIIllI(I)V

    .line 155
    return-object v6
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 163
    iget-object v0, p0, Landroid/support/v4/app/BackStackState;->llIIIIlllllIIllIIllI:[I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeIntArray([I)V

    .line 164
    iget v0, p0, Landroid/support/v4/app/BackStackState;->llllIIIllIlIIIIllllI:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 165
    iget v0, p0, Landroid/support/v4/app/BackStackState;->lllIlIlIIIllIIlIllIl:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 166
    iget-object v0, p0, Landroid/support/v4/app/BackStackState;->llIlIIIIlIIIIIlIlIII:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 167
    iget v0, p0, Landroid/support/v4/app/BackStackState;->IllIlIIIIlIIlIIIllIl:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 168
    iget v0, p0, Landroid/support/v4/app/BackStackState;->llIIllllIIlllIIIIlll:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 169
    iget-object v0, p0, Landroid/support/v4/app/BackStackState;->IIIIllIlIIlIIIIlllIl:Ljava/lang/CharSequence;

    invoke-static {v0, p1, v1}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    .line 170
    iget v0, p0, Landroid/support/v4/app/BackStackState;->llllllIllIlIlllIIlIl:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 171
    iget-object v0, p0, Landroid/support/v4/app/BackStackState;->llIlIllllllllllllllI:Ljava/lang/CharSequence;

    invoke-static {v0, p1, v1}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    .line 172
    iget-object v0, p0, Landroid/support/v4/app/BackStackState;->IIIlIIllIlIIllIlllII:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 173
    iget-object v0, p0, Landroid/support/v4/app/BackStackState;->IlIlIlIlIlIIlllllIlI:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 174
    return-void
.end method
