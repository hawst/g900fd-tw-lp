.class public final enum Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;
.super Ljava/lang/Enum;
.source "llIIIIlllllIIllIIllI"


# static fields
.field public static final enum IIIIllIlIIlIIIIlllIl:Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;

.field public static final enum IllIlIIIIlIIlIIIllIl:Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;

.field public static final enum llIIIIlllllIIllIIllI:Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;

.field public static final enum llIIllllIIlllIIIIlll:Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;

.field public static final enum llIlIIIIlIIIIIlIlIII:Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;

.field private static final synthetic llIlIllllllllllllllI:[Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;

.field public static final enum lllIlIlIIIllIIlIllIl:Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;

.field public static final enum llllIIIllIlIIIIllllI:Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;

.field public static final enum llllllIllIlIlllIIlIl:Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 153
    new-instance v0, Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;

    const-string v1, "XDM_STATE_NONE"

    invoke-direct {v0, v1, v3}, Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;->llIIIIlllllIIllIIllI:Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;

    .line 154
    new-instance v0, Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;

    const-string v1, "XDM_STATE_INIT"

    invoke-direct {v0, v1, v4}, Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;->llllIIIllIlIIIIllllI:Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;

    .line 155
    new-instance v0, Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;

    const-string v1, "XDM_STATE_CLIENT_INIT_MGMT"

    invoke-direct {v0, v1, v5}, Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;->lllIlIlIIIllIIlIllIl:Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;

    .line 156
    new-instance v0, Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;

    const-string v1, "XDM_STATE_PROCESSING"

    invoke-direct {v0, v1, v6}, Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;->llIlIIIIlIIIIIlIlIII:Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;

    .line 157
    new-instance v0, Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;

    const-string v1, "XDM_STATE_GENERIC_ALERT"

    invoke-direct {v0, v1, v7}, Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;->IllIlIIIIlIIlIIIllIl:Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;

    .line 158
    new-instance v0, Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;

    const-string v1, "XDM_STATE_GENERIC_ALERT_REPORT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;->llIIllllIIlllIIIIlll:Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;

    .line 159
    new-instance v0, Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;

    const-string v1, "XDM_STATE_ABORT_ALERT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;->IIIIllIlIIlIIIIlllIl:Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;

    .line 160
    new-instance v0, Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;

    const-string v1, "XDM_STATE_FINISH"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;->llllllIllIlIlllIIlIl:Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;

    .line 151
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;

    sget-object v1, Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;->llIIIIlllllIIllIIllI:Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;->llllIIIllIlIIIIllllI:Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;->lllIlIlIIIllIIlIllIl:Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;->llIlIIIIlIIIIIlIlIII:Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;->IllIlIIIIlIIlIIIllIl:Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;->llIIllllIIlllIIIIlll:Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;->IIIIllIlIIlIIIIlllIl:Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;->llllllIllIlIlllIIlIl:Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;->llIlIllllllllllllllI:[Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 151
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;
    .locals 1

    .prologue
    .line 151
    const-class v0, Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;

    return-object v0
.end method

.method public static values()[Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;
    .locals 1

    .prologue
    .line 151
    sget-object v0, Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;->llIlIllllllllllllllI:[Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;

    invoke-virtual {v0}, [Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/wssyncmldm/interfaces/XDMInterface$XDMSyncMLState;

    return-object v0
.end method
