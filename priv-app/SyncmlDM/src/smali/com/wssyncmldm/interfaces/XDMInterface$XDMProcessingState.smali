.class public final enum Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;
.super Ljava/lang/Enum;
.source "llIIIIlllllIIllIIllI"


# static fields
.field public static final enum IIIIllIlIIlIIIIlllIl:Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

.field public static final enum IIIlIIllIlIIllIlllII:Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

.field public static final enum IlIlIlIlIlIIlllllIlI:Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

.field public static final enum IllIlIIIIlIIlIIIllIl:Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

.field public static final enum llIIIIlllllIIllIIllI:Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

.field public static final enum llIIllllIIlllIIIIlll:Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

.field public static final enum llIlIIIIlIIIIIlIlIII:Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

.field public static final enum llIlIllllllllllllllI:Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

.field public static final enum lllIlIlIIIllIIlIllIl:Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

.field public static final enum llllIIIllIlIIIIllllI:Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

.field private static final synthetic lllllIIlIIIlIlIIIllI:[Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

.field public static final enum llllllIllIlIlllIIlIl:Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 131
    new-instance v0, Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

    const-string v1, "XDM_PROC_NONE"

    invoke-direct {v0, v1, v3}, Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;->llIIIIlllllIIllIIllI:Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

    .line 132
    new-instance v0, Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

    const-string v1, "XDM_PROC_SYNCHDR"

    invoke-direct {v0, v1, v4}, Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;->llllIIIllIlIIIIllllI:Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

    .line 133
    new-instance v0, Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

    const-string v1, "XDM_PROC_ALERT"

    invoke-direct {v0, v1, v5}, Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;->lllIlIlIIIllIIlIllIl:Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

    .line 134
    new-instance v0, Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

    const-string v1, "XDM_PROC_STATUS"

    invoke-direct {v0, v1, v6}, Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;->llIlIIIIlIIIIIlIlIII:Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

    .line 135
    new-instance v0, Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

    const-string v1, "XDM_PROC_RESULTS"

    invoke-direct {v0, v1, v7}, Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;->IllIlIIIIlIIlIIIllIl:Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

    .line 136
    new-instance v0, Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

    const-string v1, "XDM_PROC_GET"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;->llIIllllIIlllIIIIlll:Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

    .line 137
    new-instance v0, Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

    const-string v1, "XDM_PROC_EXEC"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;->IIIIllIlIIlIIIIlllIl:Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

    .line 138
    new-instance v0, Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

    const-string v1, "XDM_PROC_ADD"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;->llllllIllIlIlllIIlIl:Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

    .line 139
    new-instance v0, Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

    const-string v1, "XDM_PROC_REPLACE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;->llIlIllllllllllllllI:Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

    .line 140
    new-instance v0, Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

    const-string v1, "XDM_PROC_DELETE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;->IIIlIIllIlIIllIlllII:Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

    .line 141
    new-instance v0, Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

    const-string v1, "XDM_PROC_COPY"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;->IlIlIlIlIlIIlllllIlI:Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

    .line 129
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

    sget-object v1, Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;->llIIIIlllllIIllIIllI:Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;->llllIIIllIlIIIIllllI:Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;->lllIlIlIIIllIIlIllIl:Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;->llIlIIIIlIIIIIlIlIII:Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;->IllIlIIIIlIIlIIIllIl:Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;->llIIllllIIlllIIIIlll:Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;->IIIIllIlIIlIIIIlllIl:Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;->llllllIllIlIlllIIlIl:Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;->llIlIllllllllllllllI:Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;->IIIlIIllIlIIllIlllII:Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;->IlIlIlIlIlIIlllllIlI:Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;->lllllIIlIIIlIlIIIllI:[Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 129
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;
    .locals 1

    .prologue
    .line 129
    const-class v0, Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

    return-object v0
.end method

.method public static values()[Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;
    .locals 1

    .prologue
    .line 129
    sget-object v0, Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;->lllllIIlIIIlIlIIIllI:[Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

    invoke-virtual {v0}, [Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/wssyncmldm/interfaces/XDMInterface$XDMProcessingState;

    return-object v0
.end method
