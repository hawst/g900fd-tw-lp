.class public Lcom/wssyncmldm/DMSecBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "llIIIIlllllIIllIIllI"

# interfaces
.implements Lcom/wssyncmldm/interfaces/XDMInterface;


# static fields
.field public static llIIIIlllllIIllIIllI:Z

.field private static llIlIIIIlIIIIIlIlIII:Landroid/content/Context;

.field private static lllIlIlIIIllIIlIllIl:Landroid/content/Intent;

.field public static llllIIIllIlIIIIllllI:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 65
    sput-boolean v0, Lcom/wssyncmldm/DMSecBroadcastReceiver;->llIIIIlllllIIllIIllI:Z

    .line 66
    sput v0, Lcom/wssyncmldm/DMSecBroadcastReceiver;->llllIIIllIlIIIIllllI:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method public static llIIIIlllllIIllIIllI(Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 471
    .line 472
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "type is ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->llIIIIlllllIIllIIllI(Ljava/lang/String;)V

    .line 474
    const-string v0, "BASIC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "DIGEST,BASIC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 476
    :cond_0
    const/4 v0, 0x0

    .line 490
    :goto_0
    return v0

    .line 478
    :cond_1
    const-string v0, "DIGEST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "DIGEST,MD5"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 480
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    .line 482
    :cond_3
    const-string v0, "HMAC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "DIGEST,HMAC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 484
    :cond_4
    const/4 v0, 0x2

    goto :goto_0

    .line 488
    :cond_5
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static llIIIIlllllIIllIIllI(I)V
    .locals 0

    .prologue
    .line 465
    invoke-static {p0}, Lcom/wssyncmldm/ui/llllIIIllIlIIIIllllI;->lllIlIlIIIllIIlIllIl(I)V

    .line 467
    return-void
.end method

.method private llIIIIlllllIIllIIllI(Landroid/content/Context;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 495
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v1, "DM Not Init"

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 497
    packed-switch p2, :pswitch_data_0

    .line 549
    invoke-static {p2}, LllIlIIIIlllIlllllIll;->llIIIIlllllIIllIIllI(I)V

    .line 552
    :cond_0
    :goto_0
    return-void

    .line 500
    :pswitch_0
    invoke-static {}, LIIIlllIlIIlllIIIIllI;->llIIIIlllllIIllllllI()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 502
    invoke-static {p2}, LllIlIIIIlllIlllllIll;->llIIIIlllllIIllIIllI(I)V

    goto :goto_0

    .line 506
    :cond_1
    invoke-static {p2}, LllIlIIIIlllIlllllIll;->llIIIIlllllIIllIIllI(I)V

    goto :goto_0

    .line 510
    :pswitch_1
    invoke-static {}, Lcom/wssyncmldm/XDMService;->IlIIIllllIIlIIIIIlII()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {}, Lcom/wssyncmldm/XDMService;->IIlIlIIlIlIIlIlllIIl()Z

    move-result v0

    if-nez v0, :cond_3

    .line 512
    invoke-static {}, LIIIlllIlIIlllIIIIllI;->llIIIIlllllIIllllllI()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 513
    const/16 v0, 0xeb

    invoke-static {v2, v0}, LIIllIIIIlIlIlIlIllII;->llIIIIlllllIIllIIllI(Ljava/lang/Object;I)V

    goto :goto_0

    .line 517
    :cond_2
    new-instance v0, Landroid/view/ContextThemeWrapper;

    const/high16 v1, 0x7f0a0000

    invoke-direct {v0, p1, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 518
    const/4 v1, 0x0

    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 519
    const v1, 0x7f090031

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    .line 520
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 524
    :cond_3
    const/16 v0, 0x193

    invoke-static {v2, v0}, LIIllIIIIlIlIlIlIllII;->llIIIIlllllIIllIIllI(Ljava/lang/Object;I)V

    .line 525
    sget-boolean v0, LllIlIIIIlllIlllllIll;->llllIIIllIlIIIIllllI:Z

    if-eqz v0, :cond_4

    sget-boolean v0, LllIlIIIIlllIlllllIll;->llIlIIIIlIIIIIlIlIII:Z

    if-nez v0, :cond_4

    sget-boolean v0, Lcom/wssyncmldm/XDMService;->llIlIIIIlIIIIIlIlIII:Z

    if-nez v0, :cond_4

    .line 527
    const/4 v0, 0x1

    sput-boolean v0, Lcom/wssyncmldm/XDMService;->llIlIIIIlIIIIIlIlIII:Z

    .line 528
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v1, "----------- FOTACLIENT_PULL XEVENT_DM_INIT "

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 529
    const/16 v0, 0xc

    invoke-static {v0, v2, v2}, LlIIIlllIlIIIIllllIII;->llIIIIlllllIIllIIllI(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 530
    invoke-static {}, Lcom/wssyncmldm/db/sql/llllIIIllIlIIIIllllI;->lllIlIlIIIllIIlIllIl()I

    move-result v0

    if-nez v0, :cond_0

    .line 532
    invoke-static {}, LllIlIIIIlllIlllllIll;->llIlIIIIlIIIIIlIlIII()I

    move-result v0

    if-nez v0, :cond_0

    .line 534
    invoke-static {p2}, LllIlIIIIlllIlllllIll;->llIIIIlllllIIllIIllI(I)V

    goto :goto_0

    .line 540
    :cond_4
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v1, "----------- Is initializing."

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 541
    invoke-static {}, LllIlIIIIlllIlllllIll;->llIlIIIIlIIIIIlIlIII()I

    move-result v0

    if-nez v0, :cond_0

    .line 543
    invoke-static {p2}, LllIlIIIIlllIlllllIll;->llIIIIlllllIIllIIllI(I)V

    goto/16 :goto_0

    .line 497
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private llIIIIlllllIIllIIllI(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 243
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v1, ""

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 245
    invoke-static {}, LllIlIIIIlllIlllllIll;->llIlIIIIlIIIIIlIlIII()I

    move-result v0

    if-nez v0, :cond_0

    .line 247
    const-string v0, "pdus"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    .line 249
    if-nez v0, :cond_1

    .line 251
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v1, "PushData is null."

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 276
    :cond_0
    :goto_0
    return-void

    .line 255
    :cond_1
    invoke-static {v0}, LIIlIIlIIlIlIllIIIIlI;->llllIIIllIlIIIIllllI([B)[B

    move-result-object v0

    .line 256
    if-eqz v0, :cond_0

    .line 258
    const/4 v1, 0x2

    invoke-static {v1, v0}, Lcom/wssyncmldm/noti/XNOTIAdapter;->llllIIIllIlIIIIllllI(I[B)V

    .line 260
    sget-boolean v0, LllIlIIIIlllIlllllIll;->llIlIIIIlIIIIIlIlIII:Z

    if-nez v0, :cond_2

    .line 262
    const/4 v0, 0x6

    invoke-direct {p0, p1, v0}, Lcom/wssyncmldm/DMSecBroadcastReceiver;->llIIIIlllllIIllIIllI(Landroid/content/Context;I)V

    goto :goto_0

    .line 266
    :cond_2
    invoke-static {}, Lcom/wssyncmldm/noti/XNOTIAdapter;->llIIlIlIlIlIIIIIIlIl()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 268
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v1, "Noti Processing..."

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    goto :goto_0

    .line 272
    :cond_3
    invoke-static {}, Lcom/wssyncmldm/noti/XNOTIAdapter;->llIllIIllIIlIIlIIIII()V

    goto :goto_0
.end method

.method public static llIIIIlllllIIllIIllI(Landroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, -0x1

    .line 294
    .line 296
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v1, "Receive Intent name: android.intent.action.OMACP_DM_SET"

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 298
    if-nez p0, :cond_0

    .line 300
    sget-object p0, Lcom/wssyncmldm/DMSecBroadcastReceiver;->lllIlIlIIIllIIlIllIl:Landroid/content/Intent;

    .line 303
    :cond_0
    new-instance v0, Lcom/wssyncmldm/db/file/XDBProfileInfo;

    invoke-direct {v0}, Lcom/wssyncmldm/db/file/XDBProfileInfo;-><init>()V

    .line 304
    sget-object v1, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v2, "My W7 PDU"

    invoke-virtual {v1, v2}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 306
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 307
    if-nez v1, :cond_1

    .line 309
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v1, "extras is null"

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 441
    :goto_0
    return-void

    .line 313
    :cond_1
    const-string v2, "ProfileName"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/wssyncmldm/db/file/XDBProfileInfo;->ProfileName:Ljava/lang/String;

    .line 314
    iget-object v2, v0, Lcom/wssyncmldm/db/file/XDBProfileInfo;->ProfileName:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 315
    sget-object v2, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ProfileName :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/wssyncmldm/db/file/XDBProfileInfo;->ProfileName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LIIIIllIlIIlIIlIlIllI;->llIIIIlllllIIllIIllI(Ljava/lang/String;)V

    .line 317
    :cond_2
    const-string v2, "ServerUrl"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/wssyncmldm/db/file/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    .line 319
    iget-object v2, v0, Lcom/wssyncmldm/db/file/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 322
    const-string v2, ""

    .line 323
    const-string v2, ""

    .line 325
    const-string v2, ""

    .line 326
    iget-object v2, v0, Lcom/wssyncmldm/db/file/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    invoke-static {v2}, LlllllllIlIllIIlllIIl;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)Lcom/wssyncmldm/db/file/XDBUrlInfo;

    move-result-object v2

    .line 327
    iget-object v3, v2, Lcom/wssyncmldm/db/file/XDBUrlInfo;->pURL:Ljava/lang/String;

    .line 328
    iget-object v4, v2, Lcom/wssyncmldm/db/file/XDBUrlInfo;->pAddress:Ljava/lang/String;

    .line 329
    iget v5, v2, Lcom/wssyncmldm/db/file/XDBUrlInfo;->nPort:I

    .line 330
    iget-object v2, v2, Lcom/wssyncmldm/db/file/XDBUrlInfo;->pProtocol:Ljava/lang/String;

    .line 332
    iput-object v3, v0, Lcom/wssyncmldm/db/file/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    .line 333
    iput-object v4, v0, Lcom/wssyncmldm/db/file/XDBProfileInfo;->ServerIP:Ljava/lang/String;

    .line 334
    iput v5, v0, Lcom/wssyncmldm/db/file/XDBProfileInfo;->ServerPort:I

    .line 335
    iput-object v2, v0, Lcom/wssyncmldm/db/file/XDBProfileInfo;->Protocol:Ljava/lang/String;

    .line 337
    iget-object v2, v0, Lcom/wssyncmldm/db/file/XDBProfileInfo;->Protocol:Ljava/lang/String;

    iput-object v2, v0, Lcom/wssyncmldm/db/file/XDBProfileInfo;->Protocol_Org:Ljava/lang/String;

    .line 338
    iget v2, v0, Lcom/wssyncmldm/db/file/XDBProfileInfo;->ServerPort:I

    iput v2, v0, Lcom/wssyncmldm/db/file/XDBProfileInfo;->ServerPort_Org:I

    .line 339
    iget-object v2, v0, Lcom/wssyncmldm/db/file/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    iput-object v2, v0, Lcom/wssyncmldm/db/file/XDBProfileInfo;->ServerUrl_Org:Ljava/lang/String;

    .line 340
    iget-object v2, v0, Lcom/wssyncmldm/db/file/XDBProfileInfo;->ServerIP:Ljava/lang/String;

    iput-object v2, v0, Lcom/wssyncmldm/db/file/XDBProfileInfo;->ServerIP_Org:Ljava/lang/String;

    .line 348
    :cond_3
    const-string v2, "ServerId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/wssyncmldm/db/file/XDBProfileInfo;->ServerID:Ljava/lang/String;

    .line 352
    const-string v2, "ServerPwd"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/wssyncmldm/db/file/XDBProfileInfo;->ServerPwd:Ljava/lang/String;

    .line 356
    const-string v2, "ServerNonce"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/wssyncmldm/db/file/XDBProfileInfo;->ServerNonce:Ljava/lang/String;

    .line 357
    iget-object v2, v0, Lcom/wssyncmldm/db/file/XDBProfileInfo;->ServerNonce:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 358
    sget-object v2, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ServerNonce :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/wssyncmldm/db/file/XDBProfileInfo;->ServerNonce:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LIIIIllIlIIlIIlIlIllI;->llIIIIlllllIIllIIllI(Ljava/lang/String;)V

    .line 360
    :cond_4
    const-string v2, "UserId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/wssyncmldm/db/file/XDBProfileInfo;->UserName:Ljava/lang/String;

    .line 364
    const-string v2, "UserPwd"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/wssyncmldm/db/file/XDBProfileInfo;->Password:Ljava/lang/String;

    .line 368
    const-string v2, "ClientNonce"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/wssyncmldm/db/file/XDBProfileInfo;->ClientNonce:Ljava/lang/String;

    .line 369
    iget-object v2, v0, Lcom/wssyncmldm/db/file/XDBProfileInfo;->ClientNonce:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 370
    sget-object v2, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ClientNonce :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/wssyncmldm/db/file/XDBProfileInfo;->ClientNonce:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LIIIIllIlIIlIIlIlIllI;->llIIIIlllllIIllIIllI(Ljava/lang/String;)V

    .line 372
    :cond_5
    const-string v2, "ServerAuthType"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 373
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 375
    sget-object v3, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "nServerAuthType: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LIIIIllIlIIlIIlIlIllI;->llIIIIlllllIIllIIllI(Ljava/lang/String;)V

    .line 376
    invoke-static {v2}, Lcom/wssyncmldm/DMSecBroadcastReceiver;->llIIIIlllllIIllIIllI(Ljava/lang/String;)I

    move-result v2

    .line 378
    if-eq v2, v6, :cond_a

    .line 380
    iput v2, v0, Lcom/wssyncmldm/db/file/XDBProfileInfo;->nServerAuthType:I

    .line 381
    sget-object v2, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ServerAuthType :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v0, Lcom/wssyncmldm/db/file/XDBProfileInfo;->nServerAuthType:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LIIIIllIlIIlIIlIlIllI;->llIIIIlllllIIllIIllI(Ljava/lang/String;)V

    .line 389
    :cond_6
    :goto_1
    const-string v2, "ClientAuthType"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 390
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 392
    sget-object v2, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ClientAuthType: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LIIIIllIlIIlIIlIlIllI;->llIIIIlllllIIllIIllI(Ljava/lang/String;)V

    .line 393
    invoke-static {v1}, Lcom/wssyncmldm/DMSecBroadcastReceiver;->llIIIIlllllIIllIIllI(Ljava/lang/String;)I

    move-result v1

    .line 395
    if-eq v1, v6, :cond_b

    .line 397
    iput v1, v0, Lcom/wssyncmldm/db/file/XDBProfileInfo;->AuthType:I

    .line 398
    sget-object v1, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ClientAuthType :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Lcom/wssyncmldm/db/file/XDBProfileInfo;->AuthType:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LIIIIllIlIIlIIlIlIllI;->llIIIIlllllIIllIIllI(Ljava/lang/String;)V

    .line 407
    :cond_7
    :goto_2
    iget-object v1, v0, Lcom/wssyncmldm/db/file/XDBProfileInfo;->ServerID:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_d

    .line 409
    iget-object v1, v0, Lcom/wssyncmldm/db/file/XDBProfileInfo;->ServerID:Ljava/lang/String;

    invoke-static {v1}, Lcom/wssyncmldm/db/file/XDB;->lllllIlIlIIIIIIIIlIl(Ljava/lang/String;)I

    move-result v1

    .line 413
    iget-object v2, v0, Lcom/wssyncmldm/db/file/XDBProfileInfo;->ProfileName:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 415
    iget-object v2, v0, Lcom/wssyncmldm/db/file/XDBProfileInfo;->ProfileName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/wssyncmldm/db/file/XDB;->llIIIIlllllIIllIIllI(ILjava/lang/String;)V

    .line 418
    :cond_8
    invoke-static {v0}, Lcom/wssyncmldm/db/file/XDB;->llIIIIlllllIIllIIllI(Lcom/wssyncmldm/db/file/XDBProfileInfo;)Z

    .line 419
    invoke-static {v1}, Lcom/wssyncmldm/db/file/XDB;->lllllIIlIIIlIlIIIllI(I)V

    .line 422
    invoke-static {}, LlIIllllllIlllllIlIIl;->llllIIIllIlIIIIllllI()I

    move-result v0

    if-gtz v0, :cond_9

    invoke-static {}, Lcom/wssyncmldm/db/file/XDB;->lllIIIIllIlIlllllllI()I

    move-result v0

    if-eqz v0, :cond_c

    .line 424
    :cond_9
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v1, "DM is running. don\'t install cp bootstrap!"

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 385
    :cond_a
    sget-object v2, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v3, "ServerAuthType didn\'t receve from Server"

    invoke-virtual {v2, v3}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 402
    :cond_b
    sget-object v1, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v2, "ClientAuthType didn\'t receve from Server"

    invoke-virtual {v1, v2}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    goto :goto_2

    .line 429
    :cond_c
    const-wide/16 v0, 0x1388

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 435
    :goto_3
    const/16 v0, 0xb

    invoke-static {v0, v7, v7}, LlIIIlllIlIIIIllllIII;->llIIIIlllllIIllIIllI(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 431
    :catch_0
    move-exception v0

    .line 433
    sget-object v1, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    invoke-virtual {v1, v0}, LIIIIllIlIIlIIlIlIllI;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V

    goto :goto_3

    .line 439
    :cond_d
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v1, "Not Exist ServerID"

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public static llIIIIlllllIIllIIllI(Z)V
    .locals 3

    .prologue
    .line 579
    sput-boolean p0, Lcom/wssyncmldm/DMSecBroadcastReceiver;->llIIIIlllllIIllIIllI:Z

    .line 580
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "m_IsSavedNfcMode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/wssyncmldm/DMSecBroadcastReceiver;->llIIIIlllllIIllIIllI:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 581
    return-void
.end method

.method public static lllIlIlIIIllIIlIllIl(I)V
    .locals 3

    .prologue
    .line 591
    sput p0, Lcom/wssyncmldm/DMSecBroadcastReceiver;->llllIIIllIlIIIIllllI:I

    .line 592
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "m_nSavedNfcMode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/wssyncmldm/DMSecBroadcastReceiver;->llllIIIllIlIIIIllllI:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 593
    return-void
.end method

.method public static llllIIIllIlIIIIllllI(I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 556
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "nMode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 557
    sget-boolean v0, LllIlIIIIlllIlllllIll;->llIlIIIIlIIIIIlIlIII:Z

    if-nez v0, :cond_1

    .line 559
    invoke-static {v3}, Lcom/wssyncmldm/DMSecBroadcastReceiver;->llIIIIlllllIIllIIllI(Z)V

    .line 560
    invoke-static {p0}, Lcom/wssyncmldm/DMSecBroadcastReceiver;->lllIlIlIIIllIIlIllIl(I)V

    .line 575
    :cond_0
    :goto_0
    return-void

    .line 564
    :cond_1
    invoke-static {}, Lcom/wssyncmldm/db/file/XDB;->lllIIIIllIlIlllllllI()I

    move-result v0

    .line 565
    if-nez v0, :cond_0

    .line 567
    if-ne p0, v3, :cond_2

    .line 568
    const-string v0, "NFC"

    invoke-static {v0}, Lcom/wssyncmldm/db/file/XDB;->IIIlIIllIlIIllIlllII(Ljava/lang/String;)V

    .line 572
    :goto_1
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/wssyncmldm/DMSecBroadcastReceiver;->llIIIIlllllIIllIIllI(Z)V

    goto :goto_0

    .line 570
    :cond_2
    const-string v0, ""

    invoke-static {v0}, Lcom/wssyncmldm/db/file/XDB;->IIIlIIllIlIIllIlllII(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private llllIIIllIlIIIIllllI(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 280
    sget-boolean v0, LllIlIIIIlllIlllllIll;->llIlIIIIlIIIIIlIlIII:Z

    if-nez v0, :cond_0

    .line 282
    const/4 v0, 0x3

    invoke-direct {p0, p1, v0}, Lcom/wssyncmldm/DMSecBroadcastReceiver;->llIIIIlllllIIllIIllI(Landroid/content/Context;I)V

    .line 283
    sput-object p2, Lcom/wssyncmldm/DMSecBroadcastReceiver;->lllIlIlIIIllIIlIllIl:Landroid/content/Intent;

    .line 284
    sput-object p1, Lcom/wssyncmldm/DMSecBroadcastReceiver;->llIlIIIIlIIIIIlIlIII:Landroid/content/Context;

    .line 289
    :goto_0
    return-void

    .line 288
    :cond_0
    invoke-static {p2}, Lcom/wssyncmldm/DMSecBroadcastReceiver;->llIIIIlllllIIllIIllI(Landroid/content/Intent;)V

    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 71
    if-nez p1, :cond_1

    .line 73
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v1, "context is null"

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 239
    :cond_0
    :goto_0
    return-void

    .line 77
    :cond_1
    if-nez p2, :cond_2

    .line 79
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v1, "intent is null"

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    goto :goto_0

    .line 83
    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 84
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 86
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v1, "intent.getAction() is null!!"

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    goto :goto_0

    .line 90
    :cond_3
    const-string v1, "sec.fota.push.intent.RECEIVE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "sec.fota.pull.intent.RECEIVE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "sec.fota.polling.intent.RECEIVE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "sec.fota.terms.intent.REGISTRATION"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 92
    :cond_4
    invoke-static {}, Lcom/wssyncmldm/db/sql/llllIIIllIlIIIIllllI;->lllIlIlIIIllIIlIllIl()I

    move-result v1

    .line 93
    invoke-static {}, LlIIllllllIlllllIlIIl;->llllIIIllIlIIIIllllI()I

    move-result v2

    if-nez v2, :cond_5

    invoke-static {}, LIlIlIlllIIlllIlIlIIl;->llIlIIIIlIIIIIlIlIII()Z

    move-result v2

    if-nez v2, :cond_5

    const/16 v2, 0x3c

    if-eq v1, v2, :cond_5

    const/16 v2, 0x41

    if-eq v1, v2, :cond_5

    const/16 v2, 0x46

    if-eq v1, v2, :cond_5

    const/16 v2, 0x50

    if-eq v1, v2, :cond_5

    const/16 v2, 0x5a

    if-eq v1, v2, :cond_5

    const/16 v2, 0x64

    if-ne v1, v2, :cond_7

    .line 103
    :cond_5
    :goto_1
    invoke-static {}, Lcom/wssyncmldm/db/file/XDB;->IlIlIlIIIIIllllIlIIl()I

    move-result v1

    if-nez v1, :cond_8

    .line 105
    invoke-static {}, Lcom/wssyncmldm/db/file/XDB;->lIlIIIlllIIlIIIlIlII()V

    .line 106
    sget-boolean v1, Lcom/wssyncmldm/XDMService;->IIIlIIllIlIIllIlllII:Z

    if-nez v1, :cond_6

    .line 108
    sget-object v1, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v2, "DM service start!"

    invoke-virtual {v1, v2}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 109
    new-instance v1, Landroid/content/ComponentName;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-class v3, Lcom/wssyncmldm/XDMService;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v2, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 121
    :cond_6
    :goto_2
    const-string v1, "android.intent.action.OMACP_DM_SET"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 123
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v1, "android.intent.action.OMACP_DM_SET"

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 124
    invoke-direct {p0, p1, p2}, Lcom/wssyncmldm/DMSecBroadcastReceiver;->llllIIIllIlIIIIllllI(Landroid/content/Context;Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 100
    :cond_7
    invoke-static {p1}, LIlIlllIlllllIlIllllI;->llIIIIlllllIIllIIllI(Landroid/content/Context;)V

    goto :goto_1

    .line 113
    :cond_8
    sget-boolean v1, Lcom/wssyncmldm/XDMService;->IIIlIIllIlIIllIlllII:Z

    if-nez v1, :cond_6

    .line 115
    sget-object v1, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v2, "DM service start!"

    invoke-virtual {v1, v2}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 116
    new-instance v1, Landroid/content/ComponentName;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-class v3, Lcom/wssyncmldm/XDMService;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v2, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_2

    .line 126
    :cond_9
    const-string v1, "sec.fota.push.intent.RECEIVE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 128
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v1, "sec.fota.push.intent.RECEIVE"

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 130
    invoke-static {v4}, Lcom/wssyncmldm/DMSecBroadcastReceiver;->llllIIIllIlIIIIllllI(I)V

    .line 131
    invoke-direct {p0, p1, p2}, Lcom/wssyncmldm/DMSecBroadcastReceiver;->llIIIIlllllIIllIIllI(Landroid/content/Context;Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 133
    :cond_a
    const-string v1, "sec.fota.terms.intent.REGISTRATION"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 135
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sec.fota.terms.intent.REGISTRATION : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "mode"

    invoke-virtual {p2, v2, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 136
    const-string v0, "mode"

    invoke-virtual {p2, v0, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 138
    const-string v1, "NFC_MODE"

    invoke-virtual {p2, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 139
    invoke-static {v1}, Lcom/wssyncmldm/DMSecBroadcastReceiver;->llllIIIllIlIIIIllllI(I)V

    .line 141
    if-nez v0, :cond_c

    .line 143
    sget-boolean v0, LllIlIIIIlllIlllllIll;->llIlIIIIlIIIIIlIlIII:Z

    if-nez v0, :cond_b

    .line 145
    invoke-direct {p0, p1, v7}, Lcom/wssyncmldm/DMSecBroadcastReceiver;->llIIIIlllllIIllIIllI(Landroid/content/Context;I)V

    goto/16 :goto_0

    .line 148
    :cond_b
    invoke-static {v6}, Lcom/wssyncmldm/DMSecBroadcastReceiver;->llIIIIlllllIIllIIllI(I)V

    goto/16 :goto_0

    .line 150
    :cond_c
    if-ne v0, v5, :cond_e

    .line 152
    sget-boolean v0, LllIlIIIIlllIlllllIll;->llIlIIIIlIIIIIlIlIII:Z

    if-nez v0, :cond_d

    .line 154
    const/4 v0, 0x7

    invoke-direct {p0, p1, v0}, Lcom/wssyncmldm/DMSecBroadcastReceiver;->llIIIIlllllIIllIIllI(Landroid/content/Context;I)V

    goto/16 :goto_0

    .line 157
    :cond_d
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/wssyncmldm/ui/llllIIIllIlIIIIllllI;->lllIlIlIIIllIIlIllIl(I)V

    goto/16 :goto_0

    .line 159
    :cond_e
    if-ne v0, v6, :cond_0

    .line 161
    sget-boolean v0, LllIlIIIIlllIlllllIll;->llIlIIIIlIIIIIlIlIII:Z

    if-nez v0, :cond_f

    .line 163
    const/16 v0, 0x8

    invoke-direct {p0, p1, v0}, Lcom/wssyncmldm/DMSecBroadcastReceiver;->llIIIIlllllIIllIIllI(Landroid/content/Context;I)V

    goto/16 :goto_0

    .line 166
    :cond_f
    invoke-static {v7}, Lcom/wssyncmldm/ui/llllIIIllIlIIIIllllI;->lllIlIlIIIllIIlIllIl(I)V

    goto/16 :goto_0

    .line 170
    :cond_10
    const-string v1, "sec.fota.pull.intent.RECEIVE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 172
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v1, "sec.fota.pull.intent.RECEIVE"

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 174
    const-string v0, "NFC_MODE"

    invoke-virtual {p2, v0, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 175
    invoke-static {v0}, Lcom/wssyncmldm/DMSecBroadcastReceiver;->llllIIIllIlIIIIllllI(I)V

    .line 177
    sget-boolean v0, LllIlIIIIlllIlllllIll;->llIlIIIIlIIIIIlIlIII:Z

    if-nez v0, :cond_12

    .line 179
    invoke-static {}, LllIlIIIIIIIIlllIllII;->IIlIlIIlIlIIlIlllIIl()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 181
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v1, "Roaming, UnableNetwork, return"

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 182
    const/16 v0, 0x191

    invoke-static {v8, v0}, LIIllIIIIlIlIlIlIllII;->llIIIIlllllIIllIIllI(Ljava/lang/Object;I)V

    goto/16 :goto_0

    .line 185
    :cond_11
    const/4 v0, 0x5

    invoke-direct {p0, p1, v0}, Lcom/wssyncmldm/DMSecBroadcastReceiver;->llIIIIlllllIIllIIllI(Landroid/content/Context;I)V

    goto/16 :goto_0

    .line 189
    :cond_12
    invoke-static {v5}, Lcom/wssyncmldm/ui/llllIIIllIlIIIIllllI;->lllIlIlIIIllIIlIllIl(I)V

    goto/16 :goto_0

    .line 192
    :cond_13
    const-string v1, "sec.fota.polling.intent.RECEIVE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 194
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v1, "sec.fota.polling.intent.RECEIVE"

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 196
    invoke-static {v4}, Lcom/wssyncmldm/DMSecBroadcastReceiver;->llllIIIllIlIIIIllllI(I)V

    .line 197
    sget-boolean v0, LllIlIIIIlllIlllllIll;->llIlIIIIlIIIIIlIlIII:Z

    if-nez v0, :cond_14

    .line 199
    invoke-direct {p0, p1, v7}, Lcom/wssyncmldm/DMSecBroadcastReceiver;->llIIIIlllllIIllIIllI(Landroid/content/Context;I)V

    goto/16 :goto_0

    .line 202
    :cond_14
    invoke-static {v6}, Lcom/wssyncmldm/DMSecBroadcastReceiver;->llIIIIlllllIIllIIllI(I)V

    goto/16 :goto_0

    .line 204
    :cond_15
    const-string v1, "android.intent.action.ADMIN_SETTING"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_17

    .line 206
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v1, "android.intent.action.ADMIN_SETTING"

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 207
    sget-boolean v0, Lcom/wssyncmldm/XDMService;->IIIlIIllIlIIllIlllII:Z

    if-nez v0, :cond_16

    .line 209
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v1, "DM service start!"

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 210
    new-instance v0, Landroid/content/ComponentName;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/wssyncmldm/XDMService;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 214
    :cond_16
    new-instance v0, Landroid/content/Intent;

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/wssyncmldm/ui/XUIMainActivity;

    invoke-direct {v0, v1, v8, p1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 215
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 216
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 218
    :cond_17
    const-string v1, "com.xdm.intent.EXECUTE_UPDATE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1a

    .line 220
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v1, "com.xdm.intent.EXECUTE_UPDATE"

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 221
    invoke-static {}, Lcom/wssyncmldm/db/file/XDB;->lllIIIIllIlIlllllllI()I

    move-result v0

    const/16 v1, 0x32

    if-eq v0, v1, :cond_18

    invoke-static {}, Lcom/wssyncmldm/db/file/XDB;->lllIIIIllIlIlllllllI()I

    move-result v0

    const/16 v1, 0xdc

    if-ne v0, v1, :cond_19

    .line 223
    :cond_18
    invoke-static {}, Lcom/wssyncmldm/ui/XUIInstallConfirmActivity;->llllIIIllIlIIIIllllI()V

    .line 224
    invoke-static {}, LlIlIIIlllIlIlIlIIIll;->IIIIllIlIIlIIIIlllIl()V

    goto/16 :goto_0

    .line 228
    :cond_19
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v1, "FUMOStatus not READY_TO_UPDATE"

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 231
    :cond_1a
    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 233
    sput-boolean v4, LIIlllIlIIlIllIlllIll;->IIIIllIlIIlIIIIlllIl:Z

    goto/16 :goto_0

    .line 237
    :cond_1b
    sget-object v1, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    invoke-virtual {v1, v0}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
