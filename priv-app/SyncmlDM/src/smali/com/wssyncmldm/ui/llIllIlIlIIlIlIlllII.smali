.class public Lcom/wssyncmldm/ui/llIllIlIlIIlIlIlllII;
.super Landroid/preference/PreferenceFragment;
.source "llIIIIlllllIIllIIllI"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 105
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 111
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 113
    const v0, 0x7f040001

    invoke-virtual {p0, v0}, Lcom/wssyncmldm/ui/llIllIlIlIIlIlIlllII;->addPreferencesFromResource(I)V

    .line 115
    const-string v0, "serverurl"

    invoke-virtual {p0, v0}, Lcom/wssyncmldm/ui/llIllIlIlIIlIlIlllII;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 116
    const v3, 0x7f090092

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setTitle(I)V

    .line 117
    const-string v3, "serverip"

    invoke-virtual {p0, v3}, Lcom/wssyncmldm/ui/llIllIlIlIIlIlIlllII;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    .line 118
    const v4, 0x7f090095

    invoke-virtual {v3, v4}, Landroid/preference/Preference;->setTitle(I)V

    .line 119
    const-string v4, "authtype"

    invoke-virtual {p0, v4}, Lcom/wssyncmldm/ui/llIllIlIlIIlIlIlllII;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    .line 120
    const v5, 0x7f090087

    invoke-virtual {v4, v5}, Landroid/preference/Preference;->setTitle(I)V

    .line 121
    const-string v5, "objectsize"

    invoke-virtual {p0, v5}, Lcom/wssyncmldm/ui/llIllIlIlIIlIlIlllII;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    .line 122
    const v6, 0x7f090089

    invoke-virtual {v5, v6}, Landroid/preference/Preference;->setTitle(I)V

    .line 123
    const-string v6, "correlator"

    invoke-virtual {p0, v6}, Lcom/wssyncmldm/ui/llIllIlIlIIlIlIlllII;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v6

    .line 124
    const v7, 0x7f090088

    invoke-virtual {v6, v7}, Landroid/preference/Preference;->setTitle(I)V

    .line 125
    const-string v7, "result"

    invoke-virtual {p0, v7}, Lcom/wssyncmldm/ui/llIllIlIlIIlIlIlllII;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    .line 126
    const v8, 0x7f09001d

    invoke-virtual {v7, v8}, Landroid/preference/Preference;->setTitle(I)V

    .line 129
    invoke-static {}, Lcom/wssyncmldm/ui/XUIResultActivity;->llIIIIlllllIIllIIllI()Lcom/wssyncmldm/db/file/XDBFumoInfo;

    move-result-object v8

    .line 133
    :try_start_0
    iget-object v9, v8, Lcom/wssyncmldm/db/file/XDBFumoInfo;->ServerUrl:Ljava/lang/String;

    const-string v10, "http://"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    iget-object v9, v8, Lcom/wssyncmldm/db/file/XDBFumoInfo;->ServerUrl:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    const/16 v10, 0x8

    if-ge v9, v10, :cond_3

    .line 136
    :cond_0
    const v9, 0x7f09001a

    invoke-virtual {v0, v9}, Landroid/preference/Preference;->setSummary(I)V

    .line 137
    const v0, 0x7f09001a

    invoke-virtual {v3, v0}, Landroid/preference/Preference;->setSummary(I)V

    .line 138
    invoke-static {}, Lcom/wssyncmldm/ui/XUIResultActivity;->llllIIIllIlIIIIllllI()[Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    aget-object v0, v0, v3

    invoke-virtual {v4, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 139
    const-string v0, "0"

    invoke-virtual {v5, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    move v0, v1

    .line 148
    :goto_0
    iget-object v3, v8, Lcom/wssyncmldm/db/file/XDBFumoInfo;->Correlator:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 149
    const v3, 0x7f09001a

    invoke-virtual {v6, v3}, Landroid/preference/Preference;->setSummary(I)V

    .line 153
    :goto_1
    iget-object v3, v8, Lcom/wssyncmldm/db/file/XDBFumoInfo;->ResultCode:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    if-eqz v0, :cond_5

    .line 154
    :cond_1
    const v2, 0x7f09001a

    invoke-virtual {v7, v2}, Landroid/preference/Preference;->setSummary(I)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 182
    :goto_2
    if-eqz v0, :cond_2

    .line 184
    invoke-static {v1}, Lcom/wssyncmldm/ui/llIIIIlllIIlIIIlllIl;->llIIIIlllllIIllIIllI(I)Lcom/wssyncmldm/ui/llIIIIlllIIlIIIlllIl;

    move-result-object v0

    invoke-static {v0}, Lcom/wssyncmldm/ui/XUIResultActivity;->llIIIIlllllIIllIIllI(Lcom/wssyncmldm/ui/llIIIIlllIIlIIIlllIl;)Lcom/wssyncmldm/ui/llIIIIlllIIlIIIlllIl;

    .line 185
    invoke-static {}, Lcom/wssyncmldm/ui/XUIResultActivity;->lllIlIlIIIllIIlIllIl()Lcom/wssyncmldm/ui/llIIIIlllIIlIIIlllIl;

    move-result-object v0

    invoke-virtual {p0}, Lcom/wssyncmldm/ui/llIllIlIlIIlIlIlllII;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "DIALOG_NO_DATA_RECEIVED"

    invoke-virtual {v0, v1, v2}, Lcom/wssyncmldm/ui/llIIIIlllIIlIIIlllIl;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 187
    :cond_2
    return-void

    .line 143
    :cond_3
    :try_start_1
    iget-object v9, v8, Lcom/wssyncmldm/db/file/XDBFumoInfo;->ServerUrl:Ljava/lang/String;

    invoke-virtual {v0, v9}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 144
    iget-object v0, v8, Lcom/wssyncmldm/db/file/XDBFumoInfo;->ServerIP:Ljava/lang/String;

    invoke-virtual {v3, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 145
    invoke-static {}, Lcom/wssyncmldm/ui/XUIResultActivity;->llllIIIllIlIIIIllllI()[Ljava/lang/String;

    move-result-object v0

    iget v3, v8, Lcom/wssyncmldm/db/file/XDBFumoInfo;->AuthType:I

    aget-object v0, v0, v3

    invoke-virtual {v4, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 146
    iget v0, v8, Lcom/wssyncmldm/db/file/XDBFumoInfo;->nObjectSize:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    move v0, v2

    goto :goto_0

    .line 151
    :cond_4
    iget-object v3, v8, Lcom/wssyncmldm/db/file/XDBFumoInfo;->Correlator:Ljava/lang/String;

    invoke-virtual {v6, v3}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 176
    :catch_0
    move-exception v0

    .line 179
    sget-object v2, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LIIIIllIlIIlIIlIlIllI;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    move v0, v1

    goto :goto_2

    .line 158
    :cond_5
    :try_start_2
    invoke-static {}, Lcom/wssyncmldm/ui/XUIResultActivity$eResult;->values()[Lcom/wssyncmldm/ui/XUIResultActivity$eResult;

    move-result-object v3

    .line 159
    sget-object v4, Lcom/wssyncmldm/ui/XUIResultActivity$eResult;->IIIIlllIIIlIlIlllIII:Lcom/wssyncmldm/ui/XUIResultActivity$eResult;

    invoke-virtual {v4}, Lcom/wssyncmldm/ui/XUIResultActivity$eResult;->llIIIIlllllIIllIIllI()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    .line 161
    array-length v5, v3

    :goto_3
    if-ge v2, v5, :cond_6

    aget-object v6, v3, v2

    .line 163
    invoke-virtual {v6}, Lcom/wssyncmldm/ui/XUIResultActivity$eResult;->llIIIIlllllIIllIIllI()I

    move-result v9

    sget-object v10, Lcom/wssyncmldm/ui/XUIResultActivity$eResult;->IIIIlllIIIlIlIlllIII:Lcom/wssyncmldm/ui/XUIResultActivity$eResult;

    invoke-virtual {v10}, Lcom/wssyncmldm/ui/XUIResultActivity$eResult;->llIIIIlllllIIllIIllI()I

    move-result v10

    if-ne v9, v10, :cond_7

    .line 173
    :cond_6
    iget-object v2, v8, Lcom/wssyncmldm/db/file/XDBFumoInfo;->ResultCode:Ljava/lang/String;

    invoke-virtual {v7, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 166
    :cond_7
    invoke-virtual {v6}, Lcom/wssyncmldm/ui/XUIResultActivity$eResult;->llIIIIlllllIIllIIllI()I

    move-result v9

    invoke-virtual {v6}, Lcom/wssyncmldm/ui/XUIResultActivity$eResult;->llllIIIllIlIIIIllllI()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v4, v9

    .line 167
    invoke-virtual {v6}, Lcom/wssyncmldm/ui/XUIResultActivity$eResult;->llllIIIllIlIIIIllllI()Ljava/lang/String;

    move-result-object v9

    iget-object v10, v8, Lcom/wssyncmldm/db/file/XDBFumoInfo;->ResultCode:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 169
    invoke-virtual {v6}, Lcom/wssyncmldm/ui/XUIResultActivity$eResult;->lllIlIlIIIllIIlIllIl()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0

    .line 161
    :cond_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_3
.end method
