.class public Lcom/wssyncmldm/ui/XUINotificationBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "llIIIIlllllIIllIIllI"

# interfaces
.implements Lcom/wssyncmldm/interfaces/XDMInterface;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7

    .prologue
    const/16 v6, 0xdf

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 23
    if-nez p1, :cond_1

    .line 25
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v1, "context is null"

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 98
    :cond_0
    :goto_0
    return-void

    .line 28
    :cond_1
    if-nez p2, :cond_2

    .line 30
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v1, "intent is null"

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    goto :goto_0

    .line 33
    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    .line 35
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v1, "intent.getAction() is null"

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    goto :goto_0

    .line 39
    :cond_3
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 40
    sget-object v1, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    invoke-virtual {v1, v0}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 42
    const-string v1, "android.intent.action.DM.NOTIFICATION"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    const-string v0, "notiid"

    invoke-virtual {p2, v0, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 45
    sget-object v1, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Notification ID : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 46
    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 86
    :sswitch_0
    sput-boolean v4, Lcom/wssyncmldm/ui/XUIInstallConfirmActivity;->llIIIIlllllIIllIIllI:Z

    .line 87
    invoke-static {v6}, Lcom/wssyncmldm/XDMService;->llIlIIIIlIIIIIlIlIII(I)V

    goto :goto_0

    .line 49
    :sswitch_1
    sput-boolean v4, Lcom/wssyncmldm/ui/XUIWifiConfirmActivity;->llIIIIlllllIIllIIllI:Z

    .line 50
    invoke-static {v5}, Lcom/wssyncmldm/db/file/XDB;->IIIIIlIlIlIllIlIIllI(I)V

    .line 51
    invoke-static {}, Lcom/wssyncmldm/XDMService;->IIIlIIllIlIIllIlllII()V

    .line 52
    invoke-static {}, LllIlIIIIIIIIlllIllII;->lllllIlIlIIIIIIIIlIl()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 54
    const/16 v0, 0xea

    invoke-static {v0}, Lcom/wssyncmldm/XDMService;->llIlIIIIlIIIIIlIlIII(I)V

    goto :goto_0

    .line 58
    :cond_4
    sput v5, Lcom/wssyncmldm/XDMService;->IllIlIIIIlIIlIIIllIl:I

    .line 59
    const/16 v0, 0x18

    invoke-static {v0}, Lcom/wssyncmldm/ui/llIlIllIIIlIIlllIlII;->llIIIIlllllIIllIIllI(I)V

    .line 60
    invoke-static {v4}, LlIlIIIlllIlIlIlIIIll;->llIIIIlllllIIllIIllI(I)I

    goto :goto_0

    .line 64
    :sswitch_2
    sput-boolean v4, Lcom/wssyncmldm/ui/XUIDownloadConfirmActivity;->lllIlIlIIIllIIlIllIl:Z

    .line 65
    sput-boolean v4, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->llIIIIlllllIIllIIllI:Z

    .line 66
    invoke-static {v5}, Lcom/wssyncmldm/db/file/XDB;->IIIIIlIlIlIllIlIIllI(I)V

    .line 67
    const-string v0, "USC"

    invoke-static {}, LIIIlllIlIIlllIIIIllI;->lllllIIlIIIlIlIIIllI()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "CSP"

    invoke-static {}, LIIIlllIlIIlllIIIIllI;->lllllIIlIIIlIlIIIllI()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "LRA"

    invoke-static {}, LIIIlllIlIIlllIIIIllI;->lllllIIlIIIlIlIIIllI()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 69
    :cond_5
    invoke-static {}, Lcom/wssyncmldm/XDMService;->IlIIIllllIIlIIIIIlII()Z

    move-result v0

    if-nez v0, :cond_6

    .line 71
    invoke-static {}, Lcom/wssyncmldm/XDMService;->llIlIllllllllllllllI()V

    goto/16 :goto_0

    .line 75
    :cond_6
    sput v5, Lcom/wssyncmldm/XDMService;->IllIlIIIIlIIlIIIllIl:I

    .line 76
    const/16 v0, 0x10

    invoke-static {v0}, Lcom/wssyncmldm/ui/llIlIllIIIlIIlllIlII;->llIIIIlllllIIllIIllI(I)V

    .line 77
    invoke-static {v4}, LlIlIIIlllIlIlIlIIIll;->llIIIIlllllIIllIIllI(I)I

    goto/16 :goto_0

    .line 82
    :cond_7
    invoke-static {}, Lcom/wssyncmldm/XDMService;->llIlIllllllllllllllI()V

    goto/16 :goto_0

    .line 90
    :sswitch_3
    invoke-static {}, Lcom/wssyncmldm/XDMService;->IIIlIIllIlIIllIlllII()V

    .line 91
    const/16 v0, 0x267

    invoke-static {v0}, Lcom/wssyncmldm/XDMService;->llIlIIIIlIIIIIlIlIII(I)V

    .line 92
    sput-boolean v4, Lcom/wssyncmldm/ui/XUIInstallConfirmActivity;->llIIIIlllllIIllIIllI:Z

    .line 93
    invoke-static {v6}, Lcom/wssyncmldm/XDMService;->llIlIIIIlIIIIIlIlIII(I)V

    goto/16 :goto_0

    .line 46
    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0x7 -> :sswitch_3
        0xf -> :sswitch_2
        0x17 -> :sswitch_1
    .end sparse-switch
.end method
