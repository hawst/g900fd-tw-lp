.class public Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;
.super Landroid/app/Activity;
.source "llIIIIlllllIIllIIllI"

# interfaces
.implements Lcom/wssyncmldm/interfaces/XDMInterface;


# static fields
.field private static IIIIllIlIIlIIIIlllIl:I

.field private static IllIlIIIIlIIlIIIllIl:I

.field public static llIIIIlllllIIllIIllI:Z

.field private static llIIllllIIlllIIIIlll:I

.field private static llllllIllIlIlllIIlIl:I


# instance fields
.field private llIlIIIIlIIIIIlIlIII:Landroid/app/Dialog;

.field private llIlIllllllllllllllI:I

.field private lllIlIlIIIllIIlIllIl:Ljava/util/ArrayList;

.field private llllIIIllIlIIIIllllI:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 48
    sput-boolean v0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->llIIIIlllllIIllIIllI:Z

    .line 50
    sput v0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->IllIlIIIIlIIlIIIllIl:I

    .line 52
    const/4 v0, 0x1

    sput v0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->llIIllllIIlllIIIIlll:I

    .line 54
    const/4 v0, 0x2

    sput v0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->IIIIllIlIIlIIIIlllIl:I

    .line 56
    const/4 v0, 0x3

    sput v0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->llllllIllIlIlllIIlIl:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 40
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 44
    iput-object v0, p0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->lllIlIlIIIllIIlIllIl:Ljava/util/ArrayList;

    .line 46
    iput-object v0, p0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->llIlIIIIlIIIIIlIlIII:Landroid/app/Dialog;

    .line 58
    sget v0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->IllIlIIIIlIIlIIIllIl:I

    iput v0, p0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->llIlIllllllllllllllI:I

    .line 258
    return-void
.end method

.method static synthetic llIIIIlllllIIllIIllI(Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->llIlIIIIlIIIIIlIlIII:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic llIIIIlllllIIllIIllI(Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;Landroid/app/Dialog;)Landroid/app/Dialog;
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->llIlIIIIlIIIIIlIlIII:Landroid/app/Dialog;

    return-object p1
.end method

.method public static llIIIIlllllIIllIIllI(Landroid/content/Context;)V
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/16 v8, 0xdc

    const/4 v7, 0x3

    const/4 v6, 0x1

    .line 490
    invoke-static {}, Lcom/wssyncmldm/db/file/XDB;->lllIIIIllIlIlllllllI()I

    move-result v0

    .line 493
    sget-object v1, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v2, ""

    invoke-virtual {v1, v2}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 495
    invoke-static {}, Lcom/wssyncmldm/db/file/XDB;->IIIIlIIlIIIlllIllIIl()Lcom/wssyncmldm/db/file/XDBPostPoneInfo;

    move-result-object v1

    .line 496
    if-nez v1, :cond_1

    .line 498
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v1, "postpone data from database is null. return without any action"

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 556
    :cond_0
    :goto_0
    return-void

    .line 503
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 504
    iget-wide v4, v1, Lcom/wssyncmldm/db/file/XDBPostPoneInfo;->nPostPoneTime:J

    cmp-long v4, v4, v10

    if-nez v4, :cond_3

    iget-wide v4, v1, Lcom/wssyncmldm/db/file/XDBPostPoneInfo;->tEndTime:J

    cmp-long v4, v4, v10

    if-nez v4, :cond_3

    .line 506
    sget-object v4, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v5, "pPostUpdate not set or postpone time is passed by"

    invoke-virtual {v4, v5}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 507
    iput-wide v2, v1, Lcom/wssyncmldm/db/file/XDBPostPoneInfo;->tCurrentTime:J

    .line 508
    const-wide/32 v2, 0x36ee80

    iput-wide v2, v1, Lcom/wssyncmldm/db/file/XDBPostPoneInfo;->nPostPoneTime:J

    .line 509
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, v1, Lcom/wssyncmldm/db/file/XDBPostPoneInfo;->nPostPoneTime:J

    add-long/2addr v2, v4

    iput-wide v2, v1, Lcom/wssyncmldm/db/file/XDBPostPoneInfo;->tEndTime:J

    .line 511
    invoke-static {v1}, Lcom/wssyncmldm/db/file/XDB;->llIlIIIIlIIIIIlIlIII(Ljava/lang/Object;)V

    .line 513
    if-eq v0, v8, :cond_2

    const/16 v2, 0x32

    if-ne v0, v2, :cond_3

    .line 515
    :cond_2
    invoke-static {v8}, Lcom/wssyncmldm/db/file/XDB;->llIIIIlllllIIllllllI(I)V

    .line 516
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v2, "xdbSetFUMOStatus  to XDL_STATE_POSTPONE_TO_UPDATE"

    invoke-virtual {v0, v2}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 520
    :cond_3
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 521
    iget-wide v2, v1, Lcom/wssyncmldm/db/file/XDBPostPoneInfo;->tEndTime:J

    invoke-virtual {v0, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 522
    sget-object v2, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "PostPoneTime :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/text/format/Time;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LIIIIllIlIIlIIlIlIllI;->llIIIIlllllIIllIIllI(Ljava/lang/String;)V

    .line 524
    iget v0, v1, Lcom/wssyncmldm/db/file/XDBPostPoneInfo;->nPostPoneDownload:I

    if-eqz v0, :cond_7

    .line 526
    iget-wide v2, v1, Lcom/wssyncmldm/db/file/XDBPostPoneInfo;->tEndTime:J

    invoke-static {p0, v2, v3}, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->llIIIIlllllIIllIIllI(Landroid/content/Context;J)V

    .line 527
    iget v0, v1, Lcom/wssyncmldm/db/file/XDBPostPoneInfo;->nPostPoneDownload:I

    if-ne v0, v6, :cond_4

    .line 529
    invoke-static {v6}, Lcom/wssyncmldm/ui/llIlIllIIIlIIlllIlII;->llIIIIlllllIIllIIllI(I)V

    .line 530
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/wssyncmldm/ui/llIlIllIIIlIIlllIlII;->llIIIIlllllIIllIIllI(I)V

    goto :goto_0

    .line 532
    :cond_4
    iget v0, v1, Lcom/wssyncmldm/db/file/XDBPostPoneInfo;->nPostPoneDownload:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_5

    .line 534
    invoke-static {}, LIIIlllIlIIlllIIIIllI;->IlIlllIIlIlIIIlIlIll()Z

    move-result v0

    if-nez v0, :cond_0

    .line 536
    invoke-static {v6}, Lcom/wssyncmldm/ui/llIlIllIIIlIIlllIlII;->llIIIIlllllIIllIIllI(I)V

    .line 537
    const/4 v0, 0x5

    invoke-static {v0}, Lcom/wssyncmldm/ui/llIlIllIIIlIIlllIlII;->llIIIIlllllIIllIIllI(I)V

    goto/16 :goto_0

    .line 540
    :cond_5
    iget v0, v1, Lcom/wssyncmldm/db/file/XDBPostPoneInfo;->nPostPoneDownload:I

    if-ne v0, v7, :cond_6

    .line 542
    invoke-static {v6}, Lcom/wssyncmldm/ui/llIlIllIIIlIIlllIlII;->llIIIIlllllIIllIIllI(I)V

    .line 543
    invoke-static {v7}, Lcom/wssyncmldm/ui/llIlIllIIIlIIlllIlII;->llIIIIlllllIIllIIllI(I)V

    goto/16 :goto_0

    .line 545
    :cond_6
    iget v0, v1, Lcom/wssyncmldm/db/file/XDBPostPoneInfo;->nPostPoneDownload:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 547
    invoke-static {v6}, Lcom/wssyncmldm/ui/llIlIllIIIlIIlllIlII;->llIIIIlllllIIllIIllI(I)V

    .line 548
    const/16 v0, 0x1b

    invoke-static {v0}, Lcom/wssyncmldm/ui/llIlIllIIIlIIlllIlII;->llIIIIlllllIIllIIllI(I)V

    goto/16 :goto_0

    .line 553
    :cond_7
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v1, "nPostPone Status is none"

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 554
    invoke-static {v6}, Lcom/wssyncmldm/ui/llIlIllIIIlIIlllIlII;->llIIIIlllllIIllIIllI(I)V

    goto/16 :goto_0
.end method

.method public static llIIIIlllllIIllIIllI(Landroid/content/Context;J)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 466
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v1, ""

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 467
    if-nez p0, :cond_0

    .line 469
    invoke-static {}, Lcom/wssyncmldm/XDMApplication;->llIIIIlllllIIllIIllI()Landroid/content/Context;

    move-result-object p0

    .line 470
    if-nez p0, :cond_0

    .line 472
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v1, "context is null"

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 486
    :goto_0
    return-void

    .line 477
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.POSTPONED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 478
    invoke-static {p0, v2, v0, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 479
    const-string v0, "alarm"

    invoke-static {v0}, Lcom/wssyncmldm/XDMService;->llllIIIllIlIIIIllllI(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 480
    if-nez v0, :cond_1

    .line 482
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v1, "AlarmManager is null!!"

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    goto :goto_0

    .line 485
    :cond_1
    invoke-virtual {v0, v2, p1, p2, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    goto :goto_0
.end method

.method private lllIlIlIIIllIIlIllIl()V
    .locals 3

    .prologue
    .line 237
    :try_start_0
    invoke-virtual {p0}, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 238
    invoke-virtual {p0}, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "dialog"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    .line 239
    if-eqz v1, :cond_0

    .line 241
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 243
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 245
    invoke-static {}, Lcom/wssyncmldm/ui/IllIIlIllllIIllIIlII;->llIIIIlllllIIllIIllI()Lcom/wssyncmldm/ui/IllIIlIllllIIllIIlII;

    move-result-object v1

    .line 246
    const-string v2, "dialog"

    invoke-virtual {v1, v0, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentTransaction;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 253
    :goto_0
    return-void

    .line 248
    :catch_0
    move-exception v0

    .line 250
    sget-object v1, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    invoke-virtual {v1, v0}, LIIIIllIlIIlIIlIlIllI;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V

    .line 251
    invoke-virtual {p0}, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->finish()V

    goto :goto_0
.end method

.method public static lllIlIlIIIllIIlIllIl(I)V
    .locals 11

    .prologue
    const/16 v10, 0xdc

    const/16 v9, 0x8

    const/4 v8, 0x3

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 329
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v1, ""

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 330
    invoke-static {}, Lcom/wssyncmldm/db/file/XDB;->lllIIIIllIlIlllllllI()I

    move-result v0

    .line 331
    invoke-static {}, Lcom/wssyncmldm/db/file/XDB;->IIIIlIIlIIIlllIllIIl()Lcom/wssyncmldm/db/file/XDBPostPoneInfo;

    move-result-object v1

    .line 333
    invoke-static {v6}, Lcom/wssyncmldm/XDMService;->lllIlIlIIIllIIlIllIl(I)V

    .line 334
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 336
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    iput-wide v4, v1, Lcom/wssyncmldm/db/file/XDBPostPoneInfo;->tCurrentTime:J

    .line 337
    const-wide/32 v4, 0xa4cb80

    iput-wide v4, v1, Lcom/wssyncmldm/db/file/XDBPostPoneInfo;->nPostPoneTime:J

    .line 338
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    iget-wide v4, v1, Lcom/wssyncmldm/db/file/XDBPostPoneInfo;->nPostPoneTime:J

    add-long/2addr v2, v4

    iput-wide v2, v1, Lcom/wssyncmldm/db/file/XDBPostPoneInfo;->tEndTime:J

    .line 339
    if-eq p0, v6, :cond_0

    .line 340
    iget v2, v1, Lcom/wssyncmldm/db/file/XDBPostPoneInfo;->nPostPoneCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lcom/wssyncmldm/db/file/XDBPostPoneInfo;->nPostPoneCount:I

    .line 341
    :cond_0
    invoke-static {}, LllIlIIIIIIIIlllIllII;->llllllIllIlIlllIIlIl()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/wssyncmldm/db/file/XDBPostPoneInfo;->CurrentVersion:Ljava/lang/String;

    .line 343
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    .line 344
    iget-wide v4, v1, Lcom/wssyncmldm/db/file/XDBPostPoneInfo;->tEndTime:J

    invoke-virtual {v2, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 345
    sget-object v3, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "PostPoneTime :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Landroid/text/format/Time;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, LIIIIllIlIIlIIlIlIllI;->llIIIIlllllIIllIIllI(Ljava/lang/String;)V

    .line 347
    invoke-static {v1}, Lcom/wssyncmldm/db/file/XDB;->llIlIIIIlIIIIIlIlIII(Ljava/lang/Object;)V

    .line 349
    if-nez v0, :cond_1

    .line 352
    invoke-static {v7}, Lcom/wssyncmldm/noti/XNOTIAdapter;->IlIlIlIlIlIIlllllIlI(I)V

    .line 353
    invoke-static {v6}, Lcom/wssyncmldm/db/file/XDB;->IIIIIlIlIlIllIlIIllI(I)V

    .line 354
    const/16 v0, 0xe

    invoke-static {v0}, Lcom/wssyncmldm/ui/llIlIllIIIlIIlllIlII;->llIIIIlllllIIllIIllI(I)V

    .line 355
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/wssyncmldm/ui/llIlIllIIIlIIlllIlII;->llIIIIlllllIIllIIllI(I)V

    .line 385
    :goto_0
    invoke-static {}, Lcom/wssyncmldm/XDMApplication;->llIIIIlllllIIllIIllI()Landroid/content/Context;

    move-result-object v0

    iget-wide v2, v1, Lcom/wssyncmldm/db/file/XDBPostPoneInfo;->tEndTime:J

    invoke-static {v0, v2, v3}, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->llIIIIlllllIIllIIllI(Landroid/content/Context;J)V

    .line 386
    return-void

    .line 357
    :cond_1
    const/16 v2, 0xc8

    if-ne v0, v2, :cond_2

    .line 359
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/wssyncmldm/db/file/XDB;->IIIIIlIlIlIllIlIIllI(I)V

    .line 360
    const/16 v0, 0x10

    invoke-static {v0}, Lcom/wssyncmldm/ui/llIlIllIIIlIIlllIlII;->llIIIIlllllIIllIIllI(I)V

    .line 361
    const/4 v0, 0x5

    invoke-static {v0}, Lcom/wssyncmldm/ui/llIlIllIIIlIIlllIlII;->llIIIIlllllIIllIIllI(I)V

    .line 362
    const/16 v0, 0x113

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, LlIIIlllIlIIIIllllIII;->llIIIIlllllIIllIIllI(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 364
    :cond_2
    if-eq v0, v10, :cond_3

    const/16 v2, 0x32

    if-ne v0, v2, :cond_5

    .line 366
    :cond_3
    if-eq p0, v6, :cond_4

    .line 368
    invoke-static {v8}, Lcom/wssyncmldm/db/file/XDB;->IIIIIlIlIlIllIlIIllI(I)V

    .line 369
    invoke-static {v9}, Lcom/wssyncmldm/ui/llIlIllIIIlIIlllIlII;->llIIIIlllllIIllIIllI(I)V

    .line 370
    const/16 v0, 0x1c

    invoke-static {v0}, Lcom/wssyncmldm/ui/llIlIllIIIlIIlllIlII;->llIIIIlllllIIllIIllI(I)V

    .line 371
    invoke-static {v8}, Lcom/wssyncmldm/ui/llIlIllIIIlIIlllIlII;->llIIIIlllllIIllIIllI(I)V

    .line 372
    invoke-static {v10}, Lcom/wssyncmldm/db/file/XDB;->llIIIIlllllIIllllllI(I)V

    .line 373
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v2, "xdbSetFUMOStatus  to XDL_STATE_POSTPONE_TO_UPDATE"

    invoke-virtual {v0, v2}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    goto :goto_0

    .line 377
    :cond_4
    invoke-static {v9}, Lcom/wssyncmldm/db/file/XDB;->IIIIIlIlIlIllIlIIllI(I)V

    .line 378
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v2, "No idle Postpone"

    invoke-virtual {v0, v2}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    goto :goto_0

    .line 383
    :cond_5
    invoke-static {v7}, Lcom/wssyncmldm/db/file/XDB;->IIIIIlIlIlIllIlIIllI(I)V

    goto :goto_0
.end method

.method private llllIIIllIlIIIIllllI()V
    .locals 12

    .prologue
    const v3, 0x7f0900a9

    const-wide/32 v10, 0xa4cb80

    const-wide/32 v8, 0x36ee80

    const v7, 0x7f0900ac

    const v6, 0x7f0900aa

    .line 139
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->lllIlIlIIIllIIlIllIl:Ljava/util/ArrayList;

    .line 140
    iget v0, p0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->llIlIllllllllllllllI:I

    sget v1, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->llllllIllIlIlllIIlIl:I

    if-ne v0, v1, :cond_1

    .line 142
    const-string v0, "TMB"

    invoke-static {}, LIIIlllIlIIlllIIIIllI;->lllllIIlIIIlIlIIIllI()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->lllIlIlIIIllIIlIllIl:Ljava/util/ArrayList;

    new-instance v1, Lcom/wssyncmldm/ui/llIIIIlllllIIllIIllI;

    invoke-virtual {p0, v6}, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v8, v9}, Lcom/wssyncmldm/ui/llIIIIlllllIIllIIllI;-><init>(Ljava/lang/String;J)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 145
    iget-object v0, p0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->lllIlIlIIIllIIlIllIl:Ljava/util/ArrayList;

    new-instance v1, Lcom/wssyncmldm/ui/llIIIIlllllIIllIIllI;

    invoke-virtual {p0, v7}, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v10, v11}, Lcom/wssyncmldm/ui/llIIIIlllllIIllIIllI;-><init>(Ljava/lang/String;J)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 146
    iget-object v0, p0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->lllIlIlIIIllIIlIllIl:Ljava/util/ArrayList;

    new-instance v1, Lcom/wssyncmldm/ui/llIIIIlllllIIllIIllI;

    const v2, 0x7f0900ab

    invoke-virtual {p0, v2}, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-wide/32 v4, 0x1499700

    invoke-direct {v1, v2, v4, v5}, Lcom/wssyncmldm/ui/llIIIIlllllIIllIIllI;-><init>(Ljava/lang/String;J)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 147
    iget-object v0, p0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->lllIlIlIIIllIIlIllIl:Ljava/util/ArrayList;

    new-instance v1, Lcom/wssyncmldm/ui/llIIIIlllllIIllIIllI;

    const v2, 0x7f0900ae

    invoke-virtual {p0, v2}, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-wide/32 v4, 0x2932e00

    invoke-direct {v1, v2, v4, v5}, Lcom/wssyncmldm/ui/llIIIIlllllIIllIIllI;-><init>(Ljava/lang/String;J)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 148
    iget-object v0, p0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->lllIlIlIIIllIIlIllIl:Ljava/util/ArrayList;

    new-instance v1, Lcom/wssyncmldm/ui/llIIIIlllllIIllIIllI;

    invoke-virtual {p0, v3}, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-wide/32 v4, 0x5265c00

    invoke-direct {v1, v2, v4, v5}, Lcom/wssyncmldm/ui/llIIIIlllllIIllIIllI;-><init>(Ljava/lang/String;J)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 166
    :goto_0
    return-void

    .line 152
    :cond_0
    iget-object v0, p0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->lllIlIlIIIllIIlIllIl:Ljava/util/ArrayList;

    new-instance v1, Lcom/wssyncmldm/ui/llIIIIlllllIIllIIllI;

    const v2, 0x7f0900a6

    invoke-virtual {p0, v2}, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const/16 v5, 0x1e

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-wide/32 v4, 0x1b7740

    invoke-direct {v1, v2, v4, v5}, Lcom/wssyncmldm/ui/llIIIIlllllIIllIIllI;-><init>(Ljava/lang/String;J)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 153
    iget-object v0, p0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->lllIlIlIIIllIIlIllIl:Ljava/util/ArrayList;

    new-instance v1, Lcom/wssyncmldm/ui/llIIIIlllllIIllIIllI;

    invoke-virtual {p0, v6}, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v8, v9}, Lcom/wssyncmldm/ui/llIIIIlllllIIllIIllI;-><init>(Ljava/lang/String;J)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 154
    iget-object v0, p0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->lllIlIlIIIllIIlIllIl:Ljava/util/ArrayList;

    new-instance v1, Lcom/wssyncmldm/ui/llIIIIlllllIIllIIllI;

    invoke-virtual {p0, v7}, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v10, v11}, Lcom/wssyncmldm/ui/llIIIIlllllIIllIIllI;-><init>(Ljava/lang/String;J)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 159
    :cond_1
    iget-object v0, p0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->lllIlIlIIIllIIlIllIl:Ljava/util/ArrayList;

    new-instance v1, Lcom/wssyncmldm/ui/llIIIIlllllIIllIIllI;

    invoke-virtual {p0, v6}, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v8, v9}, Lcom/wssyncmldm/ui/llIIIIlllllIIllIIllI;-><init>(Ljava/lang/String;J)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 160
    iget-object v0, p0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->lllIlIlIIIllIIlIllIl:Ljava/util/ArrayList;

    new-instance v1, Lcom/wssyncmldm/ui/llIIIIlllllIIllIIllI;

    invoke-virtual {p0, v7}, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v10, v11}, Lcom/wssyncmldm/ui/llIIIIlllllIIllIIllI;-><init>(Ljava/lang/String;J)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 161
    iget-object v0, p0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->lllIlIlIIIllIIlIllIl:Ljava/util/ArrayList;

    new-instance v1, Lcom/wssyncmldm/ui/llIIIIlllllIIllIIllI;

    const v2, 0x7f0900ab

    invoke-virtual {p0, v2}, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-wide/32 v4, 0x1499700

    invoke-direct {v1, v2, v4, v5}, Lcom/wssyncmldm/ui/llIIIIlllllIIllIIllI;-><init>(Ljava/lang/String;J)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 162
    iget-object v0, p0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->lllIlIlIIIllIIlIllIl:Ljava/util/ArrayList;

    new-instance v1, Lcom/wssyncmldm/ui/llIIIIlllllIIllIIllI;

    const v2, 0x7f0900ae

    invoke-virtual {p0, v2}, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-wide/32 v4, 0x2932e00

    invoke-direct {v1, v2, v4, v5}, Lcom/wssyncmldm/ui/llIIIIlllllIIllIIllI;-><init>(Ljava/lang/String;J)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 163
    iget-object v0, p0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->lllIlIlIIIllIIlIllIl:Ljava/util/ArrayList;

    new-instance v1, Lcom/wssyncmldm/ui/llIIIIlllllIIllIIllI;

    invoke-virtual {p0, v3}, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-wide/32 v4, 0x5265c00

    invoke-direct {v1, v2, v4, v5}, Lcom/wssyncmldm/ui/llIIIIlllllIIllIIllI;-><init>(Ljava/lang/String;J)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 164
    iget-object v0, p0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->lllIlIlIIIllIIlIllIl:Ljava/util/ArrayList;

    new-instance v1, Lcom/wssyncmldm/ui/llIIIIlllllIIllIIllI;

    const v2, 0x7f0900a5

    invoke-virtual {p0, v2}, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-wide/16 v4, 0x0

    invoke-direct {v1, v2, v4, v5}, Lcom/wssyncmldm/ui/llIIIIlllllIIllIIllI;-><init>(Ljava/lang/String;J)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method public static llllIIIllIlIIIIllllI(J)V
    .locals 6

    .prologue
    .line 443
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v1, ""

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 445
    invoke-static {}, Lcom/wssyncmldm/db/file/XDB;->IIIIlIIlIIIlllIllIIl()Lcom/wssyncmldm/db/file/XDBPostPoneInfo;

    move-result-object v0

    .line 446
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 448
    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/wssyncmldm/db/file/XDBPostPoneInfo;->tCurrentTime:J

    .line 449
    const-wide/16 v2, 0x0

    iput-wide v2, v0, Lcom/wssyncmldm/db/file/XDBPostPoneInfo;->nPostPoneTime:J

    .line 450
    iput-wide p0, v0, Lcom/wssyncmldm/db/file/XDBPostPoneInfo;->tEndTime:J

    .line 451
    const/4 v1, 0x2

    iput v1, v0, Lcom/wssyncmldm/db/file/XDBPostPoneInfo;->nPostPoneDownload:I

    .line 452
    invoke-static {}, LllIlIIIIIIIIlllIllII;->llllllIllIlIlllIIlIl()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/wssyncmldm/db/file/XDBPostPoneInfo;->CurrentVersion:Ljava/lang/String;

    .line 454
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    .line 455
    iget-wide v2, v0, Lcom/wssyncmldm/db/file/XDBPostPoneInfo;->tEndTime:J

    invoke-virtual {v1, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 456
    sget-object v2, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "PostPoneTime :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Landroid/text/format/Time;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LIIIIllIlIIlIIlIlIllI;->llIIIIlllllIIllIIllI(Ljava/lang/String;)V

    .line 459
    invoke-static {v0}, Lcom/wssyncmldm/db/file/XDB;->llIlIIIIlIIIIIlIlIII(Ljava/lang/Object;)V

    .line 461
    invoke-static {}, Lcom/wssyncmldm/XDMApplication;->llIIIIlllllIIllIIllI()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p0, p1}, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->llIIIIlllllIIllIIllI(Landroid/content/Context;J)V

    .line 462
    return-void
.end method


# virtual methods
.method public llIIIIlllllIIllIIllI(I)J
    .locals 2

    .prologue
    .line 180
    iget-object v0, p0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->lllIlIlIIIllIIlIllIl:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssyncmldm/ui/llIIIIlllllIIllIIllI;

    invoke-virtual {v0}, Lcom/wssyncmldm/ui/llIIIIlllllIIllIIllI;->llllIIIllIlIIIIllllI()J

    move-result-wide v0

    return-wide v0
.end method

.method public llIIIIlllllIIllIIllI(J)V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/16 v9, 0xdc

    const/4 v8, 0x3

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 390
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v1, ""

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 391
    invoke-static {}, Lcom/wssyncmldm/db/file/XDB;->lllIIIIllIlIlllllllI()I

    move-result v0

    .line 392
    invoke-static {}, Lcom/wssyncmldm/db/file/XDB;->IIIIlIIlIIIlllIllIIl()Lcom/wssyncmldm/db/file/XDBPostPoneInfo;

    move-result-object v1

    .line 394
    invoke-static {v7}, Lcom/wssyncmldm/XDMService;->lllIlIlIIIllIIlIllIl(I)V

    .line 396
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 397
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    iput-wide v4, v1, Lcom/wssyncmldm/db/file/XDBPostPoneInfo;->tCurrentTime:J

    .line 398
    iput-wide p1, v1, Lcom/wssyncmldm/db/file/XDBPostPoneInfo;->nPostPoneTime:J

    .line 399
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    add-long/2addr v2, p1

    iput-wide v2, v1, Lcom/wssyncmldm/db/file/XDBPostPoneInfo;->tEndTime:J

    .line 400
    iget v2, p0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->llllIIIllIlIIIIllllI:I

    iput v2, v1, Lcom/wssyncmldm/db/file/XDBPostPoneInfo;->nPostPoneCount:I

    .line 401
    invoke-static {}, LllIlIIIIIIIIlllIllII;->llllllIllIlIlllIIlIl()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/wssyncmldm/db/file/XDBPostPoneInfo;->CurrentVersion:Ljava/lang/String;

    .line 403
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    .line 404
    iget-wide v4, v1, Lcom/wssyncmldm/db/file/XDBPostPoneInfo;->tEndTime:J

    invoke-virtual {v2, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 405
    sget-object v3, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "PostPoneTime :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Landroid/text/format/Time;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, LIIIIllIlIIlIIlIlIllI;->llIIIIlllllIIllIIllI(Ljava/lang/String;)V

    .line 408
    invoke-static {v1}, Lcom/wssyncmldm/db/file/XDB;->llIlIIIIlIIIIIlIlIII(Ljava/lang/Object;)V

    .line 410
    if-nez v0, :cond_0

    .line 413
    invoke-static {v6}, Lcom/wssyncmldm/noti/XNOTIAdapter;->IlIlIlIlIlIIlllllIlI(I)V

    .line 414
    invoke-static {v7}, Lcom/wssyncmldm/db/file/XDB;->IIIIIlIlIlIllIlIIllI(I)V

    .line 415
    const/16 v0, 0xe

    invoke-static {v0}, Lcom/wssyncmldm/ui/llIlIllIIIlIIlllIlII;->llIIIIlllllIIllIIllI(I)V

    .line 416
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/wssyncmldm/ui/llIlIllIIIlIIlllIlII;->llIIIIlllllIIllIIllI(I)V

    .line 438
    :goto_0
    iget-wide v0, v1, Lcom/wssyncmldm/db/file/XDBPostPoneInfo;->tEndTime:J

    invoke-static {p0, v0, v1}, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->llIIIIlllllIIllIIllI(Landroid/content/Context;J)V

    .line 439
    return-void

    .line 418
    :cond_0
    const/16 v2, 0xc8

    if-ne v0, v2, :cond_1

    .line 420
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/wssyncmldm/db/file/XDB;->IIIIIlIlIlIllIlIIllI(I)V

    .line 421
    const/16 v0, 0x10

    invoke-static {v0}, Lcom/wssyncmldm/ui/llIlIllIIIlIIlllIlII;->llIIIIlllllIIllIIllI(I)V

    .line 422
    const/4 v0, 0x5

    invoke-static {v0}, Lcom/wssyncmldm/ui/llIlIllIIIlIIlllIlII;->llIIIIlllllIIllIIllI(I)V

    .line 423
    const/16 v0, 0x113

    invoke-static {v0, v10, v10}, LlIIIlllIlIIIIllllIII;->llIIIIlllllIIllIIllI(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 425
    :cond_1
    if-eq v0, v9, :cond_2

    const/16 v2, 0x32

    if-ne v0, v2, :cond_3

    .line 427
    :cond_2
    invoke-static {v8}, Lcom/wssyncmldm/db/file/XDB;->IIIIIlIlIlIllIlIIllI(I)V

    .line 428
    const/16 v0, 0x8

    invoke-static {v0}, Lcom/wssyncmldm/ui/llIlIllIIIlIIlllIlII;->llIIIIlllllIIllIIllI(I)V

    .line 429
    const/16 v0, 0x1c

    invoke-static {v0}, Lcom/wssyncmldm/ui/llIlIllIIIlIIlllIlII;->llIIIIlllllIIllIIllI(I)V

    .line 430
    invoke-static {v8}, Lcom/wssyncmldm/ui/llIlIllIIIlIIlllIlII;->llIIIIlllllIIllIIllI(I)V

    .line 431
    invoke-static {v9}, Lcom/wssyncmldm/db/file/XDB;->llIIIIlllllIIllllllI(I)V

    .line 432
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v2, "xdbSetFUMOStatus  to XDL_STATE_POSTPONE_TO_UPDATE"

    invoke-virtual {v0, v2}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    goto :goto_0

    .line 436
    :cond_3
    invoke-static {v6}, Lcom/wssyncmldm/db/file/XDB;->IIIIIlIlIlIllIlIIllI(I)V

    goto :goto_0
.end method

.method public llIIIIlllllIIllIIllI()[Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 170
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move v1, v2

    .line 171
    :goto_0
    iget-object v0, p0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->lllIlIlIIIllIIlIllIl:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->lllIlIlIIIllIIlIllIl:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssyncmldm/ui/llIIIIlllllIIllIIllI;

    invoke-virtual {v0}, Lcom/wssyncmldm/ui/llIIIIlllllIIllIIllI;->llIIIIlllllIIllIIllI()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 171
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 175
    :cond_0
    new-array v0, v2, [Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method public llllIIIllIlIIIIllllI(I)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/16 v8, 0xc8

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 185
    const/16 v0, 0x8

    invoke-static {v0}, Lcom/wssyncmldm/ui/llIlIllIIIlIIlllIlII;->llIIIIlllllIIllIIllI(I)V

    .line 186
    const/16 v0, 0xe

    invoke-static {v0}, Lcom/wssyncmldm/ui/llIlIllIIIlIIlllIlII;->llIIIIlllllIIllIIllI(I)V

    .line 187
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "position: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 188
    invoke-static {}, Lcom/wssyncmldm/db/file/XDB;->lllIIIIllIlIlllllllI()I

    move-result v0

    .line 189
    invoke-virtual {p0, p1}, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->llIIIIlllllIIllIIllI(I)J

    move-result-wide v2

    .line 190
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    .line 192
    if-eqz v0, :cond_0

    if-eq v0, v8, :cond_0

    .line 194
    sget-object v1, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Postpone Count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->llllIIIllIlIIIIllllI:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 195
    iget v1, p0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->llllIIIllIlIIIIllllI:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->llllIIIllIlIIIIllllI:I

    .line 196
    iget v1, p0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->llllIIIllIlIIIIllllI:I

    invoke-static {v1}, Lcom/wssyncmldm/db/file/XDB;->llllIllIIIIIlIIllIII(I)V

    .line 198
    :cond_0
    invoke-virtual {p0, v2, v3}, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->llIIIIlllllIIllIIllI(J)V

    .line 222
    :goto_0
    const/16 v1, 0x32

    if-eq v0, v1, :cond_1

    const/16 v1, 0xdc

    if-ne v0, v1, :cond_2

    .line 223
    :cond_1
    invoke-static {}, Lcom/wssyncmldm/ui/XUIInstallConfirmActivity;->llllIIIllIlIIIIllllI()V

    .line 225
    :cond_2
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->setResult(I)V

    .line 226
    invoke-virtual {p0}, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->finish()V

    .line 227
    return-void

    .line 202
    :cond_3
    const/16 v1, 0x10

    invoke-static {v1}, Lcom/wssyncmldm/ui/llIlIllIIIlIIlllIlII;->llIIIIlllllIIllIIllI(I)V

    .line 203
    const/16 v1, 0x16

    invoke-static {v1}, Lcom/wssyncmldm/ui/llIlIllIIIlIIlllIlII;->llIIIIlllllIIllIIllI(I)V

    .line 204
    if-nez v0, :cond_5

    .line 207
    invoke-static {v6}, Lcom/wssyncmldm/noti/XNOTIAdapter;->IlIlIlIlIlIIlllllIlI(I)V

    .line 208
    invoke-static {}, Lcom/wssyncmldm/noti/XNOTIAdapter;->llllIlIIIllllIIlIlll()V

    .line 218
    :cond_4
    :goto_1
    invoke-static {v7}, Lcom/wssyncmldm/XDMService;->lllIlIlIIIllIIlIllIl(I)V

    .line 219
    invoke-static {v6}, Lcom/wssyncmldm/db/file/XDB;->IIIIIlIlIlIllIlIIllI(I)V

    goto :goto_0

    .line 210
    :cond_5
    if-ne v0, v8, :cond_4

    .line 212
    invoke-static {}, Lcom/wssyncmldm/db/file/XDB;->IIIllIIllIllIlIIlIIl()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-static {}, Lcom/wssyncmldm/db/file/XDB;->IIIlIllIlIIIIIlIIIll()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_6

    .line 214
    invoke-static {v7}, Lcom/wssyncmldm/db/file/XDB;->lllIlIlIIIllIIlIllIl(Z)V

    .line 216
    :cond_6
    const/16 v1, 0x113

    invoke-static {v1, v9, v9}, LlIIIlllIlIIIIllllIII;->llIIIIlllllIIllIIllI(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 63
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 64
    sput-boolean v1, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->llIIIIlllllIIllIIllI:Z

    .line 66
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/wssyncmldm/XDMService;->lllIlIlIIIllIIlIllIl(I)V

    .line 67
    invoke-static {v1}, Lcom/wssyncmldm/db/file/XDB;->IIIIIlIlIlIllIlIIllI(I)V

    .line 70
    invoke-static {}, Lcom/wssyncmldm/db/file/XDB;->IllIIIIllIllIIllllll()I

    move-result v0

    iput v0, p0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->llllIIIllIlIIIIllllI:I

    .line 71
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "nPostponeCount : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->llllIIIllIlIIIIllllI:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 73
    invoke-static {}, Lcom/wssyncmldm/db/file/XDB;->lllIIIIllIlIlllllllI()I

    move-result v0

    .line 74
    if-nez v0, :cond_1

    .line 76
    sget v0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->llIIllllIIlllIIIIlll:I

    iput v0, p0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->llIlIllllllllllllllI:I

    .line 86
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->llllIIIllIlIIIIllllI()V

    .line 87
    invoke-direct {p0}, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->lllIlIlIIIllIIlIllIl()V

    .line 88
    return-void

    .line 78
    :cond_1
    const/16 v1, 0xc8

    if-ne v0, v1, :cond_2

    .line 80
    sget v0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->IIIIllIlIIlIIIIlllIl:I

    iput v0, p0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->llIlIllllllllllllllI:I

    goto :goto_0

    .line 82
    :cond_2
    const/16 v1, 0xdc

    if-eq v0, v1, :cond_3

    const/16 v1, 0x32

    if-ne v0, v1, :cond_0

    .line 84
    :cond_3
    sget v0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->llllllIllIlIlllIIlIl:I

    iput v0, p0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->llIlIllllllllllllllI:I

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 95
    :try_start_0
    iget-object v0, p0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->llIlIIIIlIIIIIlIlIII:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->llIlIIIIlIIIIIlIlIII:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->llIlIIIIlIIIIIlIlIII:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 106
    :cond_0
    iput-object v2, p0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->llIlIIIIlIIIIIlIlIII:Landroid/app/Dialog;

    .line 108
    :goto_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 109
    return-void

    .line 100
    :catch_0
    move-exception v0

    .line 102
    :try_start_1
    sget-object v1, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    invoke-virtual {v1, v0}, LIIIIllIlIIlIIlIlIllI;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 106
    iput-object v2, p0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->llIlIIIIlIIIIIlIlIII:Landroid/app/Dialog;

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-object v2, p0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->llIlIIIIlIIIIIlIlIII:Landroid/app/Dialog;

    throw v0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 114
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v1, ""

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 115
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 116
    invoke-virtual {p0}, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->finish()V

    .line 117
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 122
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v1, ""

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 123
    sget-boolean v0, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->llIIIIlllllIIllIIllI:Z

    if-nez v0, :cond_1

    .line 125
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v1, "Home Key!!"

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 126
    invoke-static {}, Lcom/wssyncmldm/db/file/XDB;->lllIIIIllIlIlllllllI()I

    move-result v0

    const/16 v1, 0xc8

    if-ne v0, v1, :cond_0

    .line 128
    invoke-static {v2}, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->lllIlIlIIIllIIlIllIl(I)V

    .line 129
    const/16 v0, 0x113

    invoke-static {v0, v3, v3}, LlIIIlllIlIIIIllllIII;->llIIIIlllllIIllIIllI(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 131
    :cond_0
    invoke-virtual {p0}, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->finish()V

    .line 133
    :cond_1
    sput-boolean v2, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->llIIIIlllllIIllIIllI:Z

    .line 134
    invoke-super {p0}, Landroid/app/Activity;->onUserLeaveHint()V

    .line 135
    return-void
.end method
