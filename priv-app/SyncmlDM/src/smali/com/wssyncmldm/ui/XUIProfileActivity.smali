.class public Lcom/wssyncmldm/ui/XUIProfileActivity;
.super Landroid/app/Activity;
.source "llIIIIlllllIIllIIllI"

# interfaces
.implements Lcom/wssyncmldm/interfaces/XDMInterface;


# static fields
.field private static IllIlIIIIlIIlIIIllIl:Landroid/content/Context;

.field public static llIIIIlllllIIllIIllI:I

.field private static llIIllllIIlllIIIIlll:Landroid/app/Activity;

.field public static llIlIIIIlIIIIIlIlIII:[Lcom/wssyncmldm/db/file/XDBProfileInfo;

.field public static lllIlIlIIIllIIlIllIl:Z

.field public static llllIIIllIlIIIIllllI:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 34
    sput v0, Lcom/wssyncmldm/ui/XUIProfileActivity;->llIIIIlllllIIllIIllI:I

    .line 36
    sput-boolean v0, Lcom/wssyncmldm/ui/XUIProfileActivity;->lllIlIlIIIllIIlIllIl:Z

    .line 39
    sput-object v1, Lcom/wssyncmldm/ui/XUIProfileActivity;->IllIlIIIIlIIlIIIllIl:Landroid/content/Context;

    .line 40
    sput-object v1, Lcom/wssyncmldm/ui/XUIProfileActivity;->llIIllllIIlllIIIIlll:Landroid/app/Activity;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 248
    return-void
.end method

.method private static IllIlIIIIlIIlIIIllIl()V
    .locals 3

    .prologue
    .line 139
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/wssyncmldm/ui/XUIProfileActivity;->IllIlIIIIlIIlIIIllIl:Landroid/content/Context;

    const-class v2, Lcom/wssyncmldm/ui/XUINetProfileActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 140
    const-string v1, "profileIndex"

    sget v2, Lcom/wssyncmldm/ui/XUIProfileActivity;->llIIIIlllllIIllIIllI:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 141
    sget-object v1, Lcom/wssyncmldm/ui/XUIProfileActivity;->IllIlIIIIlIIlIIIllIl:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 142
    sget-object v0, Lcom/wssyncmldm/ui/XUIProfileActivity;->llIIllllIIlllIIIIlll:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 143
    return-void
.end method

.method private llIIIIlllllIIllIIllI(I)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const v5, 0x7f0b0004

    .line 207
    invoke-virtual {p0}, Lcom/wssyncmldm/ui/XUIProfileActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 208
    invoke-virtual {v0, v7}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 210
    invoke-virtual {v0}, Landroid/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v1

    .line 212
    sget-object v2, Lcom/wssyncmldm/ui/XUIProfileActivity;->llIlIIIIlIIIIIlIlIII:[Lcom/wssyncmldm/db/file/XDBProfileInfo;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    iget-object v2, v2, Lcom/wssyncmldm/db/file/XDBProfileInfo;->ProfileName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/app/ActionBar$Tab;->setText(Ljava/lang/CharSequence;)Landroid/app/ActionBar$Tab;

    .line 213
    invoke-virtual {v1, v5}, Landroid/app/ActionBar$Tab;->setContentDescription(I)Landroid/app/ActionBar$Tab;

    .line 214
    const-string v2, "tab1"

    invoke-virtual {v1, v2}, Landroid/app/ActionBar$Tab;->setTag(Ljava/lang/Object;)Landroid/app/ActionBar$Tab;

    .line 215
    new-instance v2, Lcom/wssyncmldm/ui/IIllIllllIIlIlIIlIII;

    const-class v3, Lcom/wssyncmldm/ui/IIllllIllIlIIIIIllll;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, p0, p0, v3}, Lcom/wssyncmldm/ui/IIllIllllIIlIlIIlIII;-><init>(Lcom/wssyncmldm/ui/XUIProfileActivity;Landroid/app/Activity;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/app/ActionBar$Tab;->setTabListener(Landroid/app/ActionBar$TabListener;)Landroid/app/ActionBar$Tab;

    .line 216
    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->addTab(Landroid/app/ActionBar$Tab;)V

    .line 218
    invoke-virtual {v0}, Landroid/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v2

    .line 220
    sget-object v3, Lcom/wssyncmldm/ui/XUIProfileActivity;->llIlIIIIlIIIIIlIlIII:[Lcom/wssyncmldm/db/file/XDBProfileInfo;

    aget-object v3, v3, v6

    iget-object v3, v3, Lcom/wssyncmldm/db/file/XDBProfileInfo;->ProfileName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/app/ActionBar$Tab;->setText(Ljava/lang/CharSequence;)Landroid/app/ActionBar$Tab;

    .line 221
    invoke-virtual {v2, v5}, Landroid/app/ActionBar$Tab;->setContentDescription(I)Landroid/app/ActionBar$Tab;

    .line 222
    const-string v3, "tab2"

    invoke-virtual {v2, v3}, Landroid/app/ActionBar$Tab;->setTag(Ljava/lang/Object;)Landroid/app/ActionBar$Tab;

    .line 223
    new-instance v3, Lcom/wssyncmldm/ui/IIllIllllIIlIlIIlIII;

    const-class v4, Lcom/wssyncmldm/ui/IIllllIllIlIIIIIllll;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, p0, p0, v4}, Lcom/wssyncmldm/ui/IIllIllllIIlIlIIlIII;-><init>(Lcom/wssyncmldm/ui/XUIProfileActivity;Landroid/app/Activity;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/app/ActionBar$Tab;->setTabListener(Landroid/app/ActionBar$TabListener;)Landroid/app/ActionBar$Tab;

    .line 224
    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->addTab(Landroid/app/ActionBar$Tab;)V

    .line 226
    invoke-virtual {v0}, Landroid/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v3

    .line 228
    sget-object v4, Lcom/wssyncmldm/ui/XUIProfileActivity;->llIlIIIIlIIIIIlIlIII:[Lcom/wssyncmldm/db/file/XDBProfileInfo;

    aget-object v4, v4, v7

    iget-object v4, v4, Lcom/wssyncmldm/db/file/XDBProfileInfo;->ProfileName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/app/ActionBar$Tab;->setText(Ljava/lang/CharSequence;)Landroid/app/ActionBar$Tab;

    .line 229
    invoke-virtual {v3, v5}, Landroid/app/ActionBar$Tab;->setContentDescription(I)Landroid/app/ActionBar$Tab;

    .line 230
    const-string v4, "tab3"

    invoke-virtual {v3, v4}, Landroid/app/ActionBar$Tab;->setTag(Ljava/lang/Object;)Landroid/app/ActionBar$Tab;

    .line 231
    new-instance v4, Lcom/wssyncmldm/ui/IIllIllllIIlIlIIlIII;

    const-class v5, Lcom/wssyncmldm/ui/IIllllIllIlIIIIIllll;

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, p0, p0, v5}, Lcom/wssyncmldm/ui/IIllIllllIIlIlIIlIII;-><init>(Lcom/wssyncmldm/ui/XUIProfileActivity;Landroid/app/Activity;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Landroid/app/ActionBar$Tab;->setTabListener(Landroid/app/ActionBar$TabListener;)Landroid/app/ActionBar$Tab;

    .line 232
    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->addTab(Landroid/app/ActionBar$Tab;)V

    .line 234
    if-nez p1, :cond_0

    .line 236
    invoke-virtual {v1}, Landroid/app/ActionBar$Tab;->select()V

    .line 246
    :goto_0
    return-void

    .line 238
    :cond_0
    if-ne p1, v6, :cond_1

    .line 240
    invoke-virtual {v2}, Landroid/app/ActionBar$Tab;->select()V

    goto :goto_0

    .line 244
    :cond_1
    invoke-virtual {v3}, Landroid/app/ActionBar$Tab;->select()V

    goto :goto_0
.end method

.method public static llIIIIlllllIIllIIllI()Z
    .locals 1

    .prologue
    .line 44
    sget-boolean v0, Lcom/wssyncmldm/ui/XUIProfileActivity;->lllIlIlIIIllIIlIllIl:Z

    return v0
.end method

.method static synthetic llIlIIIIlIIIIIlIlIII()V
    .locals 0

    .prologue
    .line 30
    invoke-static {}, Lcom/wssyncmldm/ui/XUIProfileActivity;->IllIlIIIIlIIlIIIllIl()V

    return-void
.end method

.method public static llllIIIllIlIIIIllllI()I
    .locals 1

    .prologue
    .line 55
    sget v0, Lcom/wssyncmldm/ui/XUIProfileActivity;->llllIIIllIlIIIIllllI:I

    return v0
.end method


# virtual methods
.method protected lllIlIlIIIllIIlIllIl()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 298
    move v0, v1

    :goto_0
    const/4 v2, 0x3

    if-ge v0, v2, :cond_1

    .line 300
    const-string v2, ""

    .line 302
    const-string v2, ""

    .line 305
    sget-object v2, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Tab : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LIIIIllIlIIlIIlIlIllI;->llIIIIlllllIIllIIllI(Ljava/lang/String;)V

    .line 308
    sget-object v2, Lcom/wssyncmldm/ui/XUIProfileActivity;->llIlIIIIlIIIIIlIlIII:[Lcom/wssyncmldm/db/file/XDBProfileInfo;

    aget-object v2, v2, v0

    iget-object v2, v2, Lcom/wssyncmldm/db/file/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    invoke-static {v2}, LlllllllIlIllIIlllIIl;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)Lcom/wssyncmldm/db/file/XDBUrlInfo;

    move-result-object v2

    .line 309
    iget-object v3, v2, Lcom/wssyncmldm/db/file/XDBUrlInfo;->pAddress:Ljava/lang/String;

    .line 310
    iget v4, v2, Lcom/wssyncmldm/db/file/XDBUrlInfo;->nPort:I

    .line 311
    iget-object v5, v2, Lcom/wssyncmldm/db/file/XDBUrlInfo;->pProtocol:Ljava/lang/String;

    .line 312
    sget-object v6, Lcom/wssyncmldm/ui/XUIProfileActivity;->llIlIIIIlIIIIIlIlIII:[Lcom/wssyncmldm/db/file/XDBProfileInfo;

    aget-object v6, v6, v0

    iget-object v2, v2, Lcom/wssyncmldm/db/file/XDBUrlInfo;->pURL:Ljava/lang/String;

    iput-object v2, v6, Lcom/wssyncmldm/db/file/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    .line 313
    sget-object v2, Lcom/wssyncmldm/ui/XUIProfileActivity;->llIlIIIIlIIIIIlIlIII:[Lcom/wssyncmldm/db/file/XDBProfileInfo;

    aget-object v2, v2, v0

    iput-object v3, v2, Lcom/wssyncmldm/db/file/XDBProfileInfo;->ServerIP:Ljava/lang/String;

    .line 314
    sget-object v2, Lcom/wssyncmldm/ui/XUIProfileActivity;->llIlIIIIlIIIIIlIlIII:[Lcom/wssyncmldm/db/file/XDBProfileInfo;

    aget-object v2, v2, v0

    iput v4, v2, Lcom/wssyncmldm/db/file/XDBProfileInfo;->ServerPort:I

    .line 315
    sget-object v2, Lcom/wssyncmldm/ui/XUIProfileActivity;->llIlIIIIlIIIIIlIlIII:[Lcom/wssyncmldm/db/file/XDBProfileInfo;

    aget-object v2, v2, v0

    iput-object v5, v2, Lcom/wssyncmldm/db/file/XDBProfileInfo;->Protocol:Ljava/lang/String;

    .line 317
    sget-object v2, Lcom/wssyncmldm/ui/XUIProfileActivity;->llIlIIIIlIIIIIlIlIII:[Lcom/wssyncmldm/db/file/XDBProfileInfo;

    aget-object v2, v2, v0

    sget-object v3, Lcom/wssyncmldm/ui/XUIProfileActivity;->llIlIIIIlIIIIIlIlIII:[Lcom/wssyncmldm/db/file/XDBProfileInfo;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/wssyncmldm/db/file/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    iput-object v3, v2, Lcom/wssyncmldm/db/file/XDBProfileInfo;->ServerUrl_Org:Ljava/lang/String;

    .line 318
    sget-object v2, Lcom/wssyncmldm/ui/XUIProfileActivity;->llIlIIIIlIIIIIlIlIII:[Lcom/wssyncmldm/db/file/XDBProfileInfo;

    aget-object v2, v2, v0

    sget-object v3, Lcom/wssyncmldm/ui/XUIProfileActivity;->llIlIIIIlIIIIIlIlIII:[Lcom/wssyncmldm/db/file/XDBProfileInfo;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/wssyncmldm/db/file/XDBProfileInfo;->ServerIP:Ljava/lang/String;

    iput-object v3, v2, Lcom/wssyncmldm/db/file/XDBProfileInfo;->ServerIP_Org:Ljava/lang/String;

    .line 319
    sget-object v2, Lcom/wssyncmldm/ui/XUIProfileActivity;->llIlIIIIlIIIIIlIlIII:[Lcom/wssyncmldm/db/file/XDBProfileInfo;

    aget-object v2, v2, v0

    sget-object v3, Lcom/wssyncmldm/ui/XUIProfileActivity;->llIlIIIIlIIIIIlIlIII:[Lcom/wssyncmldm/db/file/XDBProfileInfo;

    aget-object v3, v3, v0

    iget v3, v3, Lcom/wssyncmldm/db/file/XDBProfileInfo;->ServerPort:I

    iput v3, v2, Lcom/wssyncmldm/db/file/XDBProfileInfo;->ServerPort_Org:I

    .line 320
    sget-object v2, Lcom/wssyncmldm/ui/XUIProfileActivity;->llIlIIIIlIIIIIlIlIII:[Lcom/wssyncmldm/db/file/XDBProfileInfo;

    aget-object v2, v2, v0

    sget-object v3, Lcom/wssyncmldm/ui/XUIProfileActivity;->llIlIIIIlIIIIIlIlIII:[Lcom/wssyncmldm/db/file/XDBProfileInfo;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/wssyncmldm/db/file/XDBProfileInfo;->Protocol:Ljava/lang/String;

    iput-object v3, v2, Lcom/wssyncmldm/db/file/XDBProfileInfo;->Protocol_Org:Ljava/lang/String;

    .line 322
    const/4 v2, 0x1

    sput-boolean v2, Lcom/wssyncmldm/ui/XUIProfileActivity;->lllIlIlIIIllIIlIllIl:Z

    .line 323
    sput v0, Lcom/wssyncmldm/ui/XUIProfileActivity;->llllIIIllIlIIIIllllI:I

    .line 325
    sget-object v2, Lcom/wssyncmldm/ui/XUIProfileActivity;->llIlIIIIlIIIIIlIlIII:[Lcom/wssyncmldm/db/file/XDBProfileInfo;

    aget-object v2, v2, v0

    invoke-static {v2}, Lcom/wssyncmldm/db/file/XDB;->llIIIIlllllIIllIIllI(Lcom/wssyncmldm/db/file/XDBProfileInfo;)Z

    .line 327
    sget-object v2, Lcom/wssyncmldm/db/file/XDB;->llIlIIIIlIIIIIlIlIII:Lcom/wssyncmldm/db/file/XDBNvm;

    if-eqz v2, :cond_0

    sget-object v2, Lcom/wssyncmldm/db/file/XDB;->llIlIIIIlIIIIIlIlIII:Lcom/wssyncmldm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/wssyncmldm/db/file/XDBNvm;->tProfileList:Lcom/wssyncmldm/db/file/XDBProflieListInfo;

    if-eqz v2, :cond_0

    .line 328
    sget-object v2, Lcom/wssyncmldm/db/file/XDB;->llIlIIIIlIIIIIlIlIII:Lcom/wssyncmldm/db/file/XDBNvm;

    iget-object v2, v2, Lcom/wssyncmldm/db/file/XDBNvm;->tProfileList:Lcom/wssyncmldm/db/file/XDBProflieListInfo;

    iget-object v2, v2, Lcom/wssyncmldm/db/file/XDBProflieListInfo;->ProfileName:[Ljava/lang/String;

    sget-object v3, Lcom/wssyncmldm/ui/XUIProfileActivity;->llIlIIIIlIIIIIlIlIII:[Lcom/wssyncmldm/db/file/XDBProfileInfo;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/wssyncmldm/db/file/XDBProfileInfo;->ProfileName:Ljava/lang/String;

    aput-object v3, v2, v0

    .line 298
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 334
    :cond_1
    sput-boolean v1, Lcom/wssyncmldm/ui/XUIProfileActivity;->lllIlIlIIIllIIlIllIl:Z

    .line 335
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "iTapIndex="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/wssyncmldm/ui/XUIProfileActivity;->llIIIIlllllIIllIIllI:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LIIIIllIlIIlIIlIlIllI;->llIIIIlllllIIllIIllI(Ljava/lang/String;)V

    .line 337
    sget-object v0, Lcom/wssyncmldm/db/file/XDB;->llIlIIIIlIIIIIlIlIII:Lcom/wssyncmldm/db/file/XDBNvm;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/wssyncmldm/db/file/XDB;->llIlIIIIlIIIIIlIlIII:Lcom/wssyncmldm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/wssyncmldm/db/file/XDBNvm;->tProfileList:Lcom/wssyncmldm/db/file/XDBProflieListInfo;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/wssyncmldm/db/file/XDB;->llIlIIIIlIIIIIlIlIII:Lcom/wssyncmldm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/wssyncmldm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/wssyncmldm/db/file/XDBProfileInfo;

    if-nez v0, :cond_3

    .line 352
    :cond_2
    :goto_1
    return-void

    .line 341
    :cond_3
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v2, "Save to database..."

    invoke-virtual {v0, v2}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 342
    sget-object v0, Lcom/wssyncmldm/db/file/XDB;->llIlIIIIlIIIIIlIlIII:Lcom/wssyncmldm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/wssyncmldm/db/file/XDBNvm;->tProfileList:Lcom/wssyncmldm/db/file/XDBProflieListInfo;

    sget v2, Lcom/wssyncmldm/ui/XUIProfileActivity;->llIIIIlllllIIllIIllI:I

    iput v2, v0, Lcom/wssyncmldm/db/file/XDBProflieListInfo;->Profileindex:I

    .line 344
    sget-object v0, Lcom/wssyncmldm/db/file/XDB;->llIlIIIIlIIIIIlIlIII:Lcom/wssyncmldm/db/file/XDBNvm;

    invoke-static {}, Lcom/wssyncmldm/db/file/XDB;->IIlIlIIlIlIIlIlllIIl()Lcom/wssyncmldm/db/file/XDBProfileInfo;

    move-result-object v2

    iput-object v2, v0, Lcom/wssyncmldm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/wssyncmldm/db/file/XDBProfileInfo;

    .line 345
    sget-object v0, Lcom/wssyncmldm/db/file/XDB;->llIlIIIIlIIIIIlIlIII:Lcom/wssyncmldm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/wssyncmldm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/wssyncmldm/db/file/XDBProfileInfo;

    if-eqz v0, :cond_4

    .line 346
    sget-object v0, Lcom/wssyncmldm/db/file/XDB;->llIlIIIIlIIIIIlIlIII:Lcom/wssyncmldm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/wssyncmldm/db/file/XDBNvm;->NVMSyncMLDMInfo:Lcom/wssyncmldm/db/file/XDBProfileInfo;

    invoke-static {}, Lcom/wssyncmldm/db/file/XDB;->lIllIllllIIIlllIlllI()Lcom/wssyncmldm/db/file/XDBInfoConRef;

    move-result-object v2

    iput-object v2, v0, Lcom/wssyncmldm/db/file/XDBProfileInfo;->ConRef:Lcom/wssyncmldm/db/file/XDBInfoConRef;

    .line 348
    :cond_4
    sget-object v0, Lcom/wssyncmldm/db/file/XDB;->llIlIIIIlIIIIIlIlIII:Lcom/wssyncmldm/db/file/XDBNvm;

    iget-object v0, v0, Lcom/wssyncmldm/db/file/XDBNvm;->tProfileList:Lcom/wssyncmldm/db/file/XDBProflieListInfo;

    invoke-static {v0}, Lcom/wssyncmldm/db/file/XDB;->llllIIIllIlIIIIllllI(Ljava/lang/Object;)V

    .line 349
    sget v0, Lcom/wssyncmldm/ui/XUIProfileActivity;->llIIIIlllllIIllIIllI:I

    invoke-static {v0}, Lcom/wssyncmldm/db/file/XDB;->lllllIIlIIIlIlIIIllI(I)V

    .line 350
    const-string v0, "Saved"

    invoke-static {p0, v0, v1}, Lcom/wssyncmldm/XDMService;->llIIIIlllllIIllIIllI(Landroid/content/Context;Ljava/lang/String;I)V

    goto :goto_1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 190
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 191
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v1, ""

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 193
    sput-object p0, Lcom/wssyncmldm/ui/XUIProfileActivity;->IllIlIIIIlIIlIIIllIl:Landroid/content/Context;

    .line 194
    sput-object p0, Lcom/wssyncmldm/ui/XUIProfileActivity;->llIIllllIIlllIIIIlll:Landroid/app/Activity;

    .line 196
    invoke-virtual {p0}, Lcom/wssyncmldm/ui/XUIProfileActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "profileIndex"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/wssyncmldm/ui/XUIProfileActivity;->llIIIIlllllIIllIIllI:I

    .line 198
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/wssyncmldm/db/file/XDBProfileInfo;

    sput-object v0, Lcom/wssyncmldm/ui/XUIProfileActivity;->llIlIIIIlIIIIIlIlIII:[Lcom/wssyncmldm/db/file/XDBProfileInfo;

    .line 199
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/wssyncmldm/db/file/XDB;->lllIlIlIIIllIIlIllIl(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/wssyncmldm/db/file/XDBProfileInfo;

    check-cast v0, [Lcom/wssyncmldm/db/file/XDBProfileInfo;

    sput-object v0, Lcom/wssyncmldm/ui/XUIProfileActivity;->llIlIIIIlIIIIIlIlIII:[Lcom/wssyncmldm/db/file/XDBProfileInfo;

    .line 201
    sget-object v0, Lcom/wssyncmldm/ui/XUIProfileActivity;->llIlIIIIlIIIIIlIlIII:[Lcom/wssyncmldm/db/file/XDBProfileInfo;

    if-eqz v0, :cond_0

    .line 202
    sget v0, Lcom/wssyncmldm/ui/XUIProfileActivity;->llIIIIlllllIIllIIllI:I

    invoke-direct {p0, v0}, Lcom/wssyncmldm/ui/XUIProfileActivity;->llIIIIlllllIIllIIllI(I)V

    .line 203
    :cond_0
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 175
    const-string v0, "SAVE"

    invoke-interface {p1, v2, v2, v2, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x108004e

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 176
    const/4 v0, 0x1

    const-string v1, "revert"

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x108004c

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 177
    const/4 v0, 0x2

    const-string v1, "Edit Network Info"

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x108003e

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 178
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 357
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 366
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0

    .line 360
    :pswitch_0
    invoke-virtual {p0}, Lcom/wssyncmldm/ui/XUIProfileActivity;->lllIlIlIIIllIIlIllIl()V

    .line 361
    invoke-virtual {p0}, Lcom/wssyncmldm/ui/XUIProfileActivity;->finish()V

    goto :goto_0

    .line 357
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 148
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 169
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 151
    :pswitch_0
    invoke-virtual {p0}, Lcom/wssyncmldm/ui/XUIProfileActivity;->lllIlIlIIIllIIlIllIl()V

    .line 152
    invoke-virtual {p0}, Lcom/wssyncmldm/ui/XUIProfileActivity;->finish()V

    goto :goto_0

    .line 156
    :pswitch_1
    invoke-virtual {p0}, Lcom/wssyncmldm/ui/XUIProfileActivity;->finish()V

    goto :goto_0

    .line 161
    :pswitch_2
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/wssyncmldm/ui/lIlIIIllIllIlllllIIl;->llIIIIlllllIIllIIllI(I)Lcom/wssyncmldm/ui/lIlIIIllIllIlllllIIl;

    move-result-object v0

    .line 162
    invoke-virtual {p0}, Lcom/wssyncmldm/ui/XUIProfileActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "DIALOG_NETWORK_PROFILE_EDIT_YES_NO"

    invoke-virtual {v0, v1, v2}, Lcom/wssyncmldm/ui/lIlIIIllIllIlllllIIl;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 148
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onStart()V
    .locals 0

    .prologue
    .line 184
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 185
    return-void
.end method
