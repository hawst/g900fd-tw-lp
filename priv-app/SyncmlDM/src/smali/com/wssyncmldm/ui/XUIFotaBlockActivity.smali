.class public Lcom/wssyncmldm/ui/XUIFotaBlockActivity;
.super Landroid/app/Activity;
.source "llIIIIlllllIIllIIllI"


# static fields
.field private static llIIIIlllllIIllIIllI:I

.field private static llllIIIllIlIIIIllllI:Landroid/app/Activity;


# instance fields
.field private llIlIIIIlIIIIIlIlIII:Landroid/app/ProgressDialog;

.field private lllIlIlIIIllIIlIllIl:Landroid/app/Dialog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    sput v0, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->llIIIIlllllIIllIIllI:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 25
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 34
    iput-object v0, p0, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->lllIlIlIIIllIIlIllIl:Landroid/app/Dialog;

    .line 37
    iput-object v0, p0, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->llIlIIIIlIIIIIlIlIII:Landroid/app/ProgressDialog;

    .line 283
    return-void
.end method

.method private IllIlIIIIlIIlIIIllIl()V
    .locals 3

    .prologue
    .line 261
    :try_start_0
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v1, "show dialog: XUI_CANNOT_USE_DEVICE"

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 262
    invoke-virtual {p0}, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 263
    invoke-virtual {p0}, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "rooting_block_dialog"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    .line 264
    if-eqz v1, :cond_0

    .line 266
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 268
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 270
    invoke-static {}, Lcom/wssyncmldm/ui/lIIIllIIIIlIIIIlIIII;->llIIIIlllllIIllIIllI()Lcom/wssyncmldm/ui/lIIIllIIIIlIIIIlIIII;

    move-result-object v1

    .line 271
    const-string v2, "rooting_block_dialog"

    invoke-virtual {v1, v0, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentTransaction;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 278
    :goto_0
    return-void

    .line 273
    :catch_0
    move-exception v0

    .line 275
    sget-object v1, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    invoke-virtual {v1, v0}, LIIIIllIlIIlIIlIlIllI;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V

    .line 276
    invoke-virtual {p0}, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->finish()V

    goto :goto_0
.end method

.method static synthetic llIIIIlllllIIllIIllI(Lcom/wssyncmldm/ui/XUIFotaBlockActivity;)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->lllIlIlIIIllIIlIllIl:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic llIIIIlllllIIllIIllI(Lcom/wssyncmldm/ui/XUIFotaBlockActivity;Landroid/app/Dialog;)Landroid/app/Dialog;
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->lllIlIlIIIllIIlIllIl:Landroid/app/Dialog;

    return-object p1
.end method

.method public static llIIIIlllllIIllIIllI()V
    .locals 2

    .prologue
    .line 413
    :try_start_0
    sget-object v0, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->llllIIIllIlIIIIllllI:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 415
    const/4 v0, 0x0

    sput v0, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->llIIIIlllllIIllIIllI:I

    .line 416
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v1, "XUIFotaBlockActivity Remove and reset m_nBlockDialogId"

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 417
    sget-object v0, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->llllIIIllIlIIIIllllI:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 424
    :cond_0
    :goto_0
    return-void

    .line 420
    :catch_0
    move-exception v0

    .line 422
    sget-object v1, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LIIIIllIlIIlIIlIlIllI;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static llIIIIlllllIIllIIllI(I)Z
    .locals 3

    .prologue
    .line 428
    const/4 v0, 0x0

    .line 429
    sget-object v1, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->llllIIIllIlIIIIllllI:Landroid/app/Activity;

    if-eqz v1, :cond_0

    sget v1, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->llIIIIlllllIIllIIllI:I

    if-ne v1, p0, :cond_0

    .line 431
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "XUIFotaBlockActivity show dialog : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 432
    const/4 v0, 0x1

    .line 434
    :cond_0
    return v0
.end method

.method private llIIllllIIlllIIIIlll()V
    .locals 4

    .prologue
    .line 347
    :try_start_0
    iget-object v0, p0, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->llIlIIIIlIIIIIlIlIII:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    .line 349
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->llIlIIIIlIIIIIlIlIII:Landroid/app/ProgressDialog;

    .line 351
    :cond_0
    iget-object v0, p0, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->llIlIIIIlIIIIIlIlIII:Landroid/app/ProgressDialog;

    const v1, 0x7f0900b8

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setTitle(I)V

    .line 352
    iget-object v0, p0, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->llIlIIIIlIIIIIlIlIII:Landroid/app/ProgressDialog;

    const v1, 0x7f09003a

    invoke-virtual {p0, v1}, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 353
    iget-object v0, p0, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->llIlIIIIlIIIIIlIlIII:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/wssyncmldm/ui/lIIIlllIIIlllIIlllll;

    invoke-direct {v1, p0}, Lcom/wssyncmldm/ui/lIIIlllIIIlllIIlllll;-><init>(Lcom/wssyncmldm/ui/XUIFotaBlockActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 361
    sget-boolean v0, LllIlIIIIlllIlllllIll;->llIlIIIIlIIIIIlIlIII:Z

    if-eqz v0, :cond_1

    .line 363
    iget-object v0, p0, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->llIlIIIIlIIIIIlIlIII:Landroid/app/ProgressDialog;

    const/4 v1, -0x2

    const v2, 0x7f09000c

    invoke-virtual {p0, v2}, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/wssyncmldm/ui/llIlIIlIIIlllIIlIllI;

    invoke-direct {v3, p0}, Lcom/wssyncmldm/ui/llIlIIlIIIlllIIlIllI;-><init>(Lcom/wssyncmldm/ui/XUIFotaBlockActivity;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/ProgressDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 372
    iget-object v0, p0, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->llIlIIIIlIIIIIlIlIII:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/wssyncmldm/ui/IlIIllIllIIlllIlllll;

    invoke-direct {v1, p0}, Lcom/wssyncmldm/ui/IlIIllIllIIlllIlllll;-><init>(Lcom/wssyncmldm/ui/XUIFotaBlockActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 396
    :goto_0
    iget-object v0, p0, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->llIlIIIIlIIIIIlIlIII:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 403
    :goto_1
    return-void

    .line 394
    :cond_1
    iget-object v0, p0, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->llIlIIIIlIIIIIlIlIII:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 398
    :catch_0
    move-exception v0

    .line 400
    sget-object v1, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    invoke-virtual {v1, v0}, LIIIIllIlIIlIIlIlIllI;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V

    .line 401
    invoke-virtual {p0}, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->finish()V

    goto :goto_1
.end method

.method private llIlIIIIlIIIIIlIlIII()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 247
    const-string v0, "450"

    invoke-static {v0}, Lcom/wssyncmldm/db/file/XDB;->lllllIIlIIIlIlIIIllI(Ljava/lang/String;)V

    .line 248
    const/16 v0, 0xf0

    invoke-static {v0}, Lcom/wssyncmldm/db/file/XDB;->llIIIIlllllIIllllllI(I)V

    .line 249
    const/16 v0, 0xb

    invoke-static {v0, v1, v1}, LlIIIlllIlIIIIllllIII;->llIIIIlllllIIllIIllI(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 250
    return-void
.end method

.method private lllIlIlIIIllIIlIllIl()Z
    .locals 2

    .prologue
    .line 239
    invoke-static {}, Lcom/wssyncmldm/db/file/XDB;->lllIIIIllIlIlllllllI()I

    move-result v0

    const/16 v1, 0xdc

    if-eq v0, v1, :cond_0

    invoke-static {}, Lcom/wssyncmldm/db/file/XDB;->lllIIIIllIlIlllllllI()I

    move-result v0

    const/16 v1, 0x32

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private llllIIIllIlIIIIllllI()V
    .locals 3

    .prologue
    .line 141
    :try_start_0
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v1, "show dialog: XUI_ROAMING_WIFI_DISCONNECTED"

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 142
    invoke-virtual {p0}, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 143
    invoke-virtual {p0}, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "roaming_block_dialog"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    .line 144
    if-eqz v1, :cond_0

    .line 146
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 148
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 150
    invoke-static {}, Lcom/wssyncmldm/ui/llIlIIlIlllIllllllII;->llIIIIlllllIIllIIllI()Lcom/wssyncmldm/ui/llIlIIlIlllIllllllII;

    move-result-object v1

    .line 151
    const-string v2, "roaming_block_dialog"

    invoke-virtual {v1, v0, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentTransaction;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 158
    :goto_0
    return-void

    .line 153
    :catch_0
    move-exception v0

    .line 155
    sget-object v1, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    invoke-virtual {v1, v0}, LIIIIllIlIIlIIlIlIllI;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V

    .line 156
    invoke-virtual {p0}, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->finish()V

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 42
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 44
    sput-object p0, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->llllIIIllIlIIIIllllI:Landroid/app/Activity;

    .line 45
    invoke-virtual {p0}, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 48
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->llIIIIlllllIIllIIllI:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 55
    :goto_0
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->llIIIIlllllIIllIIllI:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 56
    sget v0, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->llIIIIlllllIIllIIllI:I

    packed-switch v0, :pswitch_data_0

    .line 72
    invoke-virtual {p0}, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->finish()V

    .line 75
    :goto_1
    return-void

    .line 50
    :catch_0
    move-exception v0

    .line 52
    sget-object v1, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    invoke-virtual {v1, v0}, LIIIIllIlIIlIIlIlIllI;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 59
    :pswitch_0
    invoke-direct {p0}, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->llllIIIllIlIIIIllllI()V

    goto :goto_1

    .line 62
    :pswitch_1
    invoke-direct {p0}, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->lllIlIlIIIllIIlIllIl()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    invoke-direct {p0}, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->llIlIIIIlIIIIIlIlIII()V

    .line 66
    :cond_0
    invoke-direct {p0}, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->IllIlIIIIlIIlIIIllIl()V

    goto :goto_1

    .line 69
    :pswitch_2
    invoke-direct {p0}, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->llIIllllIIlllIIIIlll()V

    goto :goto_1

    .line 56
    :pswitch_data_0
    .packed-switch 0x191
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 82
    :try_start_0
    iget-object v0, p0, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->lllIlIlIIIllIIlIllIl:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->lllIlIlIIIllIIlIllIl:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->lllIlIlIIIllIIlIllIl:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 93
    :cond_0
    iput-object v2, p0, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->lllIlIlIIIllIIlIllIl:Landroid/app/Dialog;

    .line 98
    :goto_0
    :try_start_1
    iget-object v0, p0, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->llIlIIIIlIIIIIlIlIII:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    .line 100
    iget-object v0, p0, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->llIlIIIIlIIIIIlIlIII:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 110
    :cond_1
    iput-object v2, p0, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->llIlIIIIlIIIIIlIlIII:Landroid/app/ProgressDialog;

    .line 112
    :goto_1
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 113
    return-void

    .line 87
    :catch_0
    move-exception v0

    .line 89
    :try_start_2
    sget-object v1, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    invoke-virtual {v1, v0}, LIIIIllIlIIlIIlIlIllI;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 93
    iput-object v2, p0, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->lllIlIlIIIllIIlIllIl:Landroid/app/Dialog;

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-object v2, p0, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->lllIlIlIIIllIIlIllIl:Landroid/app/Dialog;

    throw v0

    .line 104
    :catch_1
    move-exception v0

    .line 106
    :try_start_3
    sget-object v1, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    invoke-virtual {v1, v0}, LIIIIllIlIIlIIlIlIllI;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 110
    iput-object v2, p0, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->llIlIIIIlIIIIIlIlIII:Landroid/app/ProgressDialog;

    goto :goto_1

    :catchall_1
    move-exception v0

    iput-object v2, p0, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->llIlIIIIlIIIIIlIlIII:Landroid/app/ProgressDialog;

    throw v0
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 119
    sget v0, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->llIIIIlllllIIllIIllI:I

    const/16 v1, 0x194

    if-ne v0, v1, :cond_0

    .line 121
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->llIIIIlllllIIllIIllI:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 122
    invoke-virtual {p0}, Lcom/wssyncmldm/ui/XUIFotaBlockActivity;->finish()V

    .line 125
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 126
    return-void
.end method
