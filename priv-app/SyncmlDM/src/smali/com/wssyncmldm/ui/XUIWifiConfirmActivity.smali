.class public Lcom/wssyncmldm/ui/XUIWifiConfirmActivity;
.super Landroid/app/Activity;
.source "llIIIIlllllIIllIIllI"

# interfaces
.implements Lcom/wssyncmldm/interfaces/XDMInterface;
.implements Lcom/wssyncmldm/interfaces/XUICInterface;


# static fields
.field public static llIIIIlllllIIllIIllI:Z

.field private static llIlIIIIlIIIIIlIlIII:Landroid/app/AlertDialog;

.field private static lllIlIlIIIllIIlIllIl:Landroid/content/Context;

.field private static llllIIIllIlIIIIllllI:Landroid/app/Activity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    sput-boolean v0, Lcom/wssyncmldm/ui/XUIWifiConfirmActivity;->llIIIIlllllIIllIIllI:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 46
    return-void
.end method

.method static synthetic llIIIIlllllIIllIIllI(Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0

    .prologue
    .line 35
    sput-object p0, Lcom/wssyncmldm/ui/XUIWifiConfirmActivity;->llIlIIIIlIIIIIlIlIII:Landroid/app/AlertDialog;

    return-object p0
.end method

.method protected static llIIIIlllllIIllIIllI()V
    .locals 2

    .prologue
    .line 198
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.WIFI_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 199
    const v1, 0x10008000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 200
    sget-object v1, Lcom/wssyncmldm/ui/XUIWifiConfirmActivity;->lllIlIlIIIllIIlIllIl:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 201
    return-void
.end method

.method public static llIIIIlllllIIllIIllI(J)V
    .locals 6

    .prologue
    .line 237
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v1, ""

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 239
    invoke-static {}, Lcom/wssyncmldm/db/file/XDB;->IIIIlIIlIIIlllIllIIl()Lcom/wssyncmldm/db/file/XDBPostPoneInfo;

    move-result-object v0

    .line 240
    invoke-static {}, Lcom/wssyncmldm/db/file/XDB;->IIlllIlIIlIllIlllIll()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/wssyncmldm/db/file/XDBPostPoneInfo;->nWifiPostPoneCount:I

    .line 241
    iget v1, v0, Lcom/wssyncmldm/db/file/XDBPostPoneInfo;->nWifiPostPoneCount:I

    invoke-static {v1}, Lcom/wssyncmldm/db/file/XDB;->llIIlIlIlIlIIIIIIlIl(I)V

    .line 242
    sget-object v1, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "nWifiPostPoneCount : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Lcom/wssyncmldm/db/file/XDBPostPoneInfo;->nWifiPostPoneCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 244
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 245
    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/wssyncmldm/db/file/XDBPostPoneInfo;->tCurrentTime:J

    .line 246
    const-wide/16 v2, 0x0

    iput-wide v2, v0, Lcom/wssyncmldm/db/file/XDBPostPoneInfo;->nPostPoneTime:J

    .line 247
    iput-wide p0, v0, Lcom/wssyncmldm/db/file/XDBPostPoneInfo;->tEndTime:J

    .line 248
    const/4 v1, 0x7

    iput v1, v0, Lcom/wssyncmldm/db/file/XDBPostPoneInfo;->nPostPoneDownload:I

    .line 249
    invoke-static {}, LllIlIIIIIIIIlllIllII;->llllllIllIlIlllIIlIl()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/wssyncmldm/db/file/XDBPostPoneInfo;->CurrentVersion:Ljava/lang/String;

    .line 251
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    .line 252
    iget-wide v2, v0, Lcom/wssyncmldm/db/file/XDBPostPoneInfo;->tEndTime:J

    invoke-virtual {v1, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 253
    sget-object v2, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "PostPoneTime :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Landroid/text/format/Time;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LIIIIllIlIIlIIlIlIllI;->llIIIIlllllIIllIIllI(Ljava/lang/String;)V

    .line 256
    invoke-static {v0}, Lcom/wssyncmldm/db/file/XDB;->llIlIIIIlIIIIIlIlIII(Ljava/lang/Object;)V

    .line 258
    invoke-static {}, Lcom/wssyncmldm/XDMApplication;->llIIIIlllllIIllIIllI()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p0, p1}, Lcom/wssyncmldm/ui/XUIFotaPostponeActivity;->llIIIIlllllIIllIIllI(Landroid/content/Context;J)V

    .line 259
    return-void
.end method

.method static synthetic llIlIIIIlIIIIIlIlIII()Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/wssyncmldm/ui/XUIWifiConfirmActivity;->llIlIIIIlIIIIIlIlIII:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic lllIlIlIIIllIIlIllIl()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/wssyncmldm/ui/XUIWifiConfirmActivity;->llllIIIllIlIIIIllllI:Landroid/app/Activity;

    return-object v0
.end method

.method protected static llllIIIllIlIIIIllllI()V
    .locals 6

    .prologue
    .line 205
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 206
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    const-wide/32 v4, 0xa4cb80

    add-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 207
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/wssyncmldm/ui/XUIWifiConfirmActivity;->llIIIIlllllIIllIIllI(J)V

    .line 208
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 158
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 160
    sput-object p0, Lcom/wssyncmldm/ui/XUIWifiConfirmActivity;->lllIlIlIIIllIIlIllIl:Landroid/content/Context;

    .line 161
    sput-object p0, Lcom/wssyncmldm/ui/XUIWifiConfirmActivity;->llllIIIllIlIIIIllllI:Landroid/app/Activity;

    .line 162
    const/4 v0, 0x0

    sput-boolean v0, Lcom/wssyncmldm/ui/XUIWifiConfirmActivity;->llIIIIlllllIIllIIllI:Z

    .line 164
    const/16 v0, 0xea

    invoke-static {v0}, Lcom/wssyncmldm/ui/IllllIlllllIIllllIIl;->llIIIIlllllIIllIIllI(I)Lcom/wssyncmldm/ui/IllllIlllllIIllllIIl;

    move-result-object v0

    .line 165
    invoke-virtual {p0}, Lcom/wssyncmldm/ui/XUIWifiConfirmActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "XUI_DL_DELTA_OVER_SIZE_WIFI_DOWNLOAD"

    invoke-virtual {v0, v1, v2}, Lcom/wssyncmldm/ui/IllllIlllllIIllllIIl;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 166
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 171
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v1, ""

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 172
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 173
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 178
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v1, ""

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 179
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 180
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 2

    .prologue
    .line 185
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v1, ""

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 186
    sget-boolean v0, Lcom/wssyncmldm/ui/XUIWifiConfirmActivity;->llIIIIlllllIIllIIllI:Z

    if-nez v0, :cond_0

    .line 188
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v1, "Home Key!!"

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 189
    invoke-static {}, Lcom/wssyncmldm/ui/XUIWifiConfirmActivity;->llllIIIllIlIIIIllllI()V

    .line 190
    invoke-virtual {p0}, Lcom/wssyncmldm/ui/XUIWifiConfirmActivity;->finish()V

    .line 192
    :cond_0
    const/4 v0, 0x0

    sput-boolean v0, Lcom/wssyncmldm/ui/XUIWifiConfirmActivity;->llIIIIlllllIIllIIllI:Z

    .line 193
    invoke-super {p0}, Landroid/app/Activity;->onUserLeaveHint()V

    .line 194
    return-void
.end method
