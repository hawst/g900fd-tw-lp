.class public Lcom/wssyncmldm/ui/XUIResultActivity;
.super Landroid/preference/PreferenceActivity;
.source "llIIIIlllllIIllIIllI"

# interfaces
.implements Lcom/wssyncmldm/interfaces/XDMInterface;


# static fields
.field private static IllIlIIIIlIIlIIIllIl:Lcom/wssyncmldm/ui/llIIIIlllIIlIIIlllIl;

.field private static final llIIIIlllllIIllIIllI:[Ljava/lang/String;

.field private static llIlIIIIlIIIIIlIlIII:Landroid/app/Activity;

.field private static lllIlIlIIIllIIlIllIl:Landroid/content/Context;

.field private static llllIIIllIlIIIIllllI:Lcom/wssyncmldm/db/file/XDBFumoInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 23
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "NONE"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "BASIC"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "MD5"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "HMAC"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "MD5 Not BASE64"

    aput-object v2, v0, v1

    sput-object v0, Lcom/wssyncmldm/ui/XUIResultActivity;->llIIIIlllllIIllIIllI:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 190
    return-void
.end method

.method static synthetic IllIlIIIIlIIlIIIllIl()Landroid/content/Context;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/wssyncmldm/ui/XUIResultActivity;->lllIlIlIIIllIIlIllIl:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic llIIIIlllllIIllIIllI()Lcom/wssyncmldm/db/file/XDBFumoInfo;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/wssyncmldm/ui/XUIResultActivity;->llllIIIllIlIIIIllllI:Lcom/wssyncmldm/db/file/XDBFumoInfo;

    return-object v0
.end method

.method static synthetic llIIIIlllllIIllIIllI(Lcom/wssyncmldm/ui/llIIIIlllIIlIIIlllIl;)Lcom/wssyncmldm/ui/llIIIIlllIIlIIIlllIl;
    .locals 0

    .prologue
    .line 21
    sput-object p0, Lcom/wssyncmldm/ui/XUIResultActivity;->IllIlIIIIlIIlIIIllIl:Lcom/wssyncmldm/ui/llIIIIlllIIlIIIlllIl;

    return-object p0
.end method

.method static synthetic llIlIIIIlIIIIIlIlIII()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/wssyncmldm/ui/XUIResultActivity;->llIlIIIIlIIIIIlIlIII:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic lllIlIlIIIllIIlIllIl()Lcom/wssyncmldm/ui/llIIIIlllIIlIIIlllIl;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/wssyncmldm/ui/XUIResultActivity;->IllIlIIIIlIIlIIIllIl:Lcom/wssyncmldm/ui/llIIIIlllIIlIIIlllIl;

    return-object v0
.end method

.method static synthetic llllIIIllIlIIIIllllI()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/wssyncmldm/ui/XUIResultActivity;->llIIIIlllllIIllIIllI:[Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 93
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 95
    sput-object p0, Lcom/wssyncmldm/ui/XUIResultActivity;->lllIlIlIIIllIIlIllIl:Landroid/content/Context;

    .line 96
    sput-object p0, Lcom/wssyncmldm/ui/XUIResultActivity;->llIlIIIIlIIIIIlIlIII:Landroid/app/Activity;

    .line 98
    invoke-virtual {p0}, Lcom/wssyncmldm/ui/XUIResultActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x1020002

    new-instance v2, Lcom/wssyncmldm/ui/llIllIlIlIIlIlIlllII;

    invoke-direct {v2}, Lcom/wssyncmldm/ui/llIllIlIlIIlIlIlllII;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 100
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/wssyncmldm/ui/llllIIIllIlIIIIllllI;->llIIIIlllllIIllIIllI(I)V

    .line 101
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v1, "UI MODE SESSION RESET"

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 103
    return-void
.end method
