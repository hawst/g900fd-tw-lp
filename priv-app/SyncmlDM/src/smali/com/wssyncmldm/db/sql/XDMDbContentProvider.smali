.class public Lcom/wssyncmldm/db/sql/XDMDbContentProvider;
.super Landroid/content/ContentProvider;
.source "llIIIIlllllIIllIIllI"


# static fields
.field private static llIIIIlllllIIllIIllI:Landroid/content/Context;

.field private static final llllIIIllIlIIIIllllI:Landroid/content/UriMatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 28
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/wssyncmldm/db/sql/XDMDbContentProvider;->llllIIIllIlIIIIllllI:Landroid/content/UriMatcher;

    .line 29
    sget-object v0, Lcom/wssyncmldm/db/sql/XDMDbContentProvider;->llllIIIllIlIIIIllllI:Landroid/content/UriMatcher;

    const-string v1, "com.wssyncmldm.ContentProvider"

    const-string v2, "polling"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 30
    sget-object v0, Lcom/wssyncmldm/db/sql/XDMDbContentProvider;->llllIIIllIlIIIIllllI:Landroid/content/UriMatcher;

    const-string v1, "com.wssyncmldm.ContentProvider"

    const-string v2, "dmcycle"

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 32
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method

.method public static llIIIIlllllIIllIIllI()Landroid/database/sqlite/SQLiteDatabase;
    .locals 3

    .prologue
    .line 37
    const/4 v0, 0x0

    .line 40
    :try_start_0
    new-instance v1, Lcom/wssyncmldm/db/sql/llIIIIlllllIIllIIllI;

    sget-object v2, Lcom/wssyncmldm/db/sql/XDMDbContentProvider;->llIIIIlllllIIllIIllI:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/wssyncmldm/db/sql/llIIIIlllllIIllIIllI;-><init>(Landroid/content/Context;)V

    .line 41
    invoke-virtual {v1}, Lcom/wssyncmldm/db/sql/llIIIIlllllIIllIIllI;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 48
    :goto_0
    return-object v0

    .line 43
    :catch_0
    move-exception v1

    .line 45
    sget-object v2, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    invoke-virtual {v2, v1}, LIIIIllIlIIlIIlIlIllI;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static llllIIIllIlIIIIllllI()Landroid/database/sqlite/SQLiteDatabase;
    .locals 3

    .prologue
    .line 54
    const/4 v0, 0x0

    .line 57
    :try_start_0
    new-instance v1, Lcom/wssyncmldm/db/sql/llIIIIlllllIIllIIllI;

    sget-object v2, Lcom/wssyncmldm/db/sql/XDMDbContentProvider;->llIIIIlllllIIllIIllI:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/wssyncmldm/db/sql/llIIIIlllllIIllIIllI;-><init>(Landroid/content/Context;)V

    .line 58
    invoke-virtual {v1}, Lcom/wssyncmldm/db/sql/llIIIIlllllIIllIIllI;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 65
    :goto_0
    return-object v0

    .line 60
    :catch_0
    move-exception v1

    .line 62
    sget-object v2, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    invoke-virtual {v2, v1}, LIIIIllIlIIlIIlIlIllI;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x0

    return v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()Z
    .locals 3

    .prologue
    .line 92
    const/4 v1, 0x0

    .line 95
    :try_start_0
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v2, ""

    invoke-virtual {v0, v2}, LIIIIllIlIIlIIlIlIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 96
    invoke-virtual {p0}, Lcom/wssyncmldm/db/sql/XDMDbContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/wssyncmldm/db/sql/XDMDbContentProvider;->llIIIIlllllIIllIIllI:Landroid/content/Context;

    .line 100
    invoke-static {}, Lcom/wssyncmldm/db/sql/XDMDbContentProvider;->llIIIIlllllIIllIIllI()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 108
    if-eqz v0, :cond_0

    .line 109
    invoke-static {v0}, Lcom/wssyncmldm/db/sql/llIIIIlllllIIllIIllI;->llIIIIlllllIIllIIllI(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 111
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 102
    :catch_0
    move-exception v0

    .line 104
    :try_start_1
    sget-object v2, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    invoke-virtual {v2, v0}, LIIIIllIlIIlIIlIlIllI;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 108
    if-eqz v1, :cond_0

    .line 109
    invoke-static {v1}, Lcom/wssyncmldm/db/sql/llIIIIlllllIIllIIllI;->llIIIIlllllIIllIIllI(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 108
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_1

    .line 109
    invoke-static {v1}, Lcom/wssyncmldm/db/sql/llIIIIlllllIIllIIllI;->llIIIIlllllIIllIIllI(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 117
    .line 118
    invoke-static {}, Lcom/wssyncmldm/db/sql/XDMDbContentProvider;->llIIIIlllllIIllIIllI()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 120
    if-nez v1, :cond_0

    .line 122
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v1, "Database doesn\'t exist"

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 159
    :goto_0
    return-object v8

    .line 129
    :cond_0
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 130
    const-string v2, ""

    .line 132
    sget-object v2, Lcom/wssyncmldm/db/sql/XDMDbContentProvider;->llllIIIllIlIIIIllllI:Landroid/content/UriMatcher;

    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 145
    sget-object v0, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v1, "URI is wrong"

    invoke-virtual {v0, v1}, LIIIIllIlIIlIIlIlIllI;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    goto :goto_0

    .line 135
    :pswitch_0
    const-string v2, "polling"

    .line 151
    :goto_1
    :try_start_0
    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 152
    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v7, p5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_2
    move-object v8, v0

    .line 159
    goto :goto_0

    .line 139
    :pswitch_1
    const-string v2, "polling"

    .line 140
    invoke-static {}, LlIlIIIlllIlIlIlIIIll;->llIlIIIIlIIIIIlIlIII()Z

    move-result v3

    invoke-static {v3}, Lcom/wssyncmldm/db/file/XDB;->llllllIllIlIlllIIlIl(Z)V

    goto :goto_1

    .line 154
    :catch_0
    move-exception v0

    .line 156
    sget-object v1, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    invoke-virtual {v1, v0}, LIIIIllIlIIlIIlIlIllI;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V

    move-object v0, v8

    goto :goto_2

    .line 132
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 4

    .prologue
    .line 165
    const/4 v0, -0x1

    .line 166
    invoke-static {}, Lcom/wssyncmldm/db/sql/XDMDbContentProvider;->llllIIIllIlIIIIllllI()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 168
    if-nez v2, :cond_1

    .line 170
    sget-object v1, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v2, "Database doesn\'t exist"

    invoke-virtual {v1, v2}, LIIIIllIlIIlIIlIlIllI;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 205
    :cond_0
    :goto_0
    return v0

    .line 179
    :cond_1
    sget-object v1, Lcom/wssyncmldm/db/sql/XDMDbContentProvider;->llllIIIllIlIIIIllllI:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 187
    sget-object v1, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    const-string v2, "URI is wrong"

    invoke-virtual {v1, v2}, LIIIIllIlIIlIIlIlIllI;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    goto :goto_0

    .line 182
    :pswitch_0
    const-string v1, "polling"

    .line 193
    :try_start_0
    invoke-virtual {v2, v1, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 194
    invoke-virtual {p0}, Lcom/wssyncmldm/db/sql/XDMDbContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v1, p1, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 202
    if-eqz v2, :cond_0

    .line 203
    invoke-static {v2}, Lcom/wssyncmldm/db/sql/llIIIIlllllIIllIIllI;->llIIIIlllllIIllIIllI(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 196
    :catch_0
    move-exception v1

    .line 198
    :try_start_1
    sget-object v3, Lcom/sec/android/syncmldm/log/llIIIIlllllIIllIIllI;->IllIlIIIIlIIlIIIllIl:LIIIIllIlIIlIIlIlIllI;

    invoke-virtual {v3, v1}, LIIIIllIlIIlIIlIlIllI;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 202
    if-eqz v2, :cond_0

    .line 203
    invoke-static {v2}, Lcom/wssyncmldm/db/sql/llIIIIlllllIIllIIllI;->llIIIIlllllIIllIIllI(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 202
    :catchall_0
    move-exception v0

    if-eqz v2, :cond_2

    .line 203
    invoke-static {v2}, Lcom/wssyncmldm/db/sql/llIIIIlllllIIllIIllI;->llIIIIlllllIIllIIllI(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_2
    throw v0

    .line 179
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
