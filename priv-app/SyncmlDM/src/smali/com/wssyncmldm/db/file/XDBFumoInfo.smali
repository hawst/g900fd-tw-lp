.class public Lcom/wssyncmldm/db/file/XDBFumoInfo;
.super Ljava/lang/Object;
.source "llIIIIlllllIIllIIllI"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public AuthType:I

.field public Correlator:Ljava/lang/String;

.field public ObexType:I

.field public ResultCode:Ljava/lang/String;

.field public ServerIP:Ljava/lang/String;

.field public ServerPort:I

.field public ServerUrl:Ljava/lang/String;

.field public m_bBigDeltaDownload:Z

.field public m_bCheckRooting:Z

.field public m_bOptionalCancel:Z

.field public m_bOptionalUpdate:Z

.field public m_bWifiOnlyDownload:Z

.field public m_szAcceptType:Ljava/lang/String;

.field public m_szContentType:Ljava/lang/String;

.field public m_szObjectDownloadIP:Ljava/lang/String;

.field public m_szObjectDownloadProtocol:Ljava/lang/String;

.field public m_szObjectDownloadUrl:Ljava/lang/String;

.field public m_szProtocol:Ljava/lang/String;

.field public m_szReportURI:Ljava/lang/String;

.field public m_szStatusNodeName:Ljava/lang/String;

.field public m_szStatusNotifyIP:Ljava/lang/String;

.field public m_szStatusNotifyProtocol:Ljava/lang/String;

.field public m_szStatusNotifyUrl:Ljava/lang/String;

.field public nCacheMargin:I

.field public nCurrentDownloadMode:I

.field public nDataMargin:I

.field public nDownloadMode:Z

.field public nFFSWriteSize:I

.field public nInitiatedType:I

.field public nObjectDownloadPort:I

.field public nObjectSize:I

.field public nPriority:I

.field public nStatus:I

.field public nStatusNotifyPort:I

.field public nUpdateMechanism:I

.field public nUpdateWait:I

.field public szDescription:Ljava/lang/String;

.field public szDownloadResultCode:Ljava/lang/String;

.field public szNetworkConnType:Ljava/lang/String;

.field public szObjectHash:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/16 v2, -0x63

    const/4 v1, 0x0

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    const-string v0, ""

    iput-object v0, p0, Lcom/wssyncmldm/db/file/XDBFumoInfo;->m_szProtocol:Ljava/lang/String;

    .line 58
    const-string v0, ""

    iput-object v0, p0, Lcom/wssyncmldm/db/file/XDBFumoInfo;->ServerUrl:Ljava/lang/String;

    .line 59
    const-string v0, ""

    iput-object v0, p0, Lcom/wssyncmldm/db/file/XDBFumoInfo;->ServerIP:Ljava/lang/String;

    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/wssyncmldm/db/file/XDBFumoInfo;->m_szObjectDownloadProtocol:Ljava/lang/String;

    .line 61
    const-string v0, ""

    iput-object v0, p0, Lcom/wssyncmldm/db/file/XDBFumoInfo;->m_szObjectDownloadUrl:Ljava/lang/String;

    .line 62
    const-string v0, ""

    iput-object v0, p0, Lcom/wssyncmldm/db/file/XDBFumoInfo;->m_szObjectDownloadIP:Ljava/lang/String;

    .line 64
    const-string v0, ""

    iput-object v0, p0, Lcom/wssyncmldm/db/file/XDBFumoInfo;->m_szStatusNotifyProtocol:Ljava/lang/String;

    .line 65
    const-string v0, ""

    iput-object v0, p0, Lcom/wssyncmldm/db/file/XDBFumoInfo;->m_szStatusNotifyUrl:Ljava/lang/String;

    .line 66
    const-string v0, ""

    iput-object v0, p0, Lcom/wssyncmldm/db/file/XDBFumoInfo;->m_szStatusNotifyIP:Ljava/lang/String;

    .line 68
    const-string v0, ""

    iput-object v0, p0, Lcom/wssyncmldm/db/file/XDBFumoInfo;->m_szReportURI:Ljava/lang/String;

    .line 70
    const-string v0, ""

    iput-object v0, p0, Lcom/wssyncmldm/db/file/XDBFumoInfo;->m_szStatusNodeName:Ljava/lang/String;

    .line 71
    const-string v0, ""

    iput-object v0, p0, Lcom/wssyncmldm/db/file/XDBFumoInfo;->ResultCode:Ljava/lang/String;

    .line 73
    const-string v0, ""

    iput-object v0, p0, Lcom/wssyncmldm/db/file/XDBFumoInfo;->Correlator:Ljava/lang/String;

    .line 74
    const-string v0, ""

    iput-object v0, p0, Lcom/wssyncmldm/db/file/XDBFumoInfo;->m_szContentType:Ljava/lang/String;

    .line 75
    const-string v0, ""

    iput-object v0, p0, Lcom/wssyncmldm/db/file/XDBFumoInfo;->m_szAcceptType:Ljava/lang/String;

    .line 77
    iput v2, p0, Lcom/wssyncmldm/db/file/XDBFumoInfo;->ObexType:I

    .line 78
    iput v2, p0, Lcom/wssyncmldm/db/file/XDBFumoInfo;->AuthType:I

    .line 79
    iput v2, p0, Lcom/wssyncmldm/db/file/XDBFumoInfo;->nFFSWriteSize:I

    .line 80
    iput v2, p0, Lcom/wssyncmldm/db/file/XDBFumoInfo;->nUpdateWait:I

    .line 82
    iput-boolean v1, p0, Lcom/wssyncmldm/db/file/XDBFumoInfo;->nDownloadMode:Z

    .line 83
    iput-boolean v1, p0, Lcom/wssyncmldm/db/file/XDBFumoInfo;->m_bOptionalUpdate:Z

    .line 84
    iput-boolean v1, p0, Lcom/wssyncmldm/db/file/XDBFumoInfo;->m_bOptionalCancel:Z

    .line 85
    iput-boolean v1, p0, Lcom/wssyncmldm/db/file/XDBFumoInfo;->m_bWifiOnlyDownload:Z

    .line 86
    iput-boolean v1, p0, Lcom/wssyncmldm/db/file/XDBFumoInfo;->m_bCheckRooting:Z

    .line 87
    iput-boolean v1, p0, Lcom/wssyncmldm/db/file/XDBFumoInfo;->m_bBigDeltaDownload:Z

    .line 89
    const-string v0, ""

    iput-object v0, p0, Lcom/wssyncmldm/db/file/XDBFumoInfo;->szDownloadResultCode:Ljava/lang/String;

    .line 91
    const-string v0, ""

    iput-object v0, p0, Lcom/wssyncmldm/db/file/XDBFumoInfo;->szNetworkConnType:Ljava/lang/String;

    .line 92
    const-string v0, ""

    iput-object v0, p0, Lcom/wssyncmldm/db/file/XDBFumoInfo;->szObjectHash:Ljava/lang/String;

    .line 93
    const/16 v0, 0x3c

    iput v0, p0, Lcom/wssyncmldm/db/file/XDBFumoInfo;->nCacheMargin:I

    .line 94
    const/16 v0, 0xb4

    iput v0, p0, Lcom/wssyncmldm/db/file/XDBFumoInfo;->nDataMargin:I

    .line 95
    return-void
.end method
