.class public Lcom/wssyncmldm/db/file/XDBPollingInfo;
.super Ljava/lang/Object;
.source "llIIIIlllllIIllIIllI"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public bDmCycle:Z

.field public m_szOrgVersionUrl:Ljava/lang/String;

.field public m_szPeriodUnit:Ljava/lang/String;

.field public m_szVersionUrl:Ljava/lang/String;

.field public nPeriod:I

.field public nRange:I

.field public nTime:I

.field public nWifiActivated:I

.field public nWifiCycle:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const-string v0, "http://org-fota-dn.ospserver.net/firmware/"

    iput-object v0, p0, Lcom/wssyncmldm/db/file/XDBPollingInfo;->m_szOrgVersionUrl:Ljava/lang/String;

    .line 24
    const-string v0, "http://fota-cloud-dn.ospserver.net/firmware/"

    iput-object v0, p0, Lcom/wssyncmldm/db/file/XDBPollingInfo;->m_szVersionUrl:Ljava/lang/String;

    .line 25
    const/4 v0, 0x7

    iput v0, p0, Lcom/wssyncmldm/db/file/XDBPollingInfo;->nPeriod:I

    .line 26
    const/16 v0, 0xf

    iput v0, p0, Lcom/wssyncmldm/db/file/XDBPollingInfo;->nTime:I

    .line 27
    const/4 v0, 0x3

    iput v0, p0, Lcom/wssyncmldm/db/file/XDBPollingInfo;->nRange:I

    .line 28
    const-string v0, "day"

    iput-object v0, p0, Lcom/wssyncmldm/db/file/XDBPollingInfo;->m_szPeriodUnit:Ljava/lang/String;

    .line 29
    iput-boolean v1, p0, Lcom/wssyncmldm/db/file/XDBPollingInfo;->bDmCycle:Z

    .line 30
    const/4 v0, -0x1

    iput v0, p0, Lcom/wssyncmldm/db/file/XDBPollingInfo;->nWifiCycle:I

    .line 31
    iput v1, p0, Lcom/wssyncmldm/db/file/XDBPollingInfo;->nWifiActivated:I

    .line 32
    return-void
.end method
