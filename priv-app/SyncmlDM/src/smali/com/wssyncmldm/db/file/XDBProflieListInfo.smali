.class public Lcom/wssyncmldm/db/file/XDBProflieListInfo;
.super Ljava/lang/Object;
.source "llIIIIlllllIIllIIllI"

# interfaces
.implements Lcom/wssyncmldm/interfaces/XDMInterface;
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public MagicNumber:I

.field public NotiResumeState:Lcom/wssyncmldm/db/file/XDBSessionSaveInfo;

.field public ProfileName:[Ljava/lang/String;

.field public Profileindex:I

.field public bAutoUpdate:Z

.field public bPushMessage:Z

.field public bWifiOnly:Z

.field public m_szNetworkConnName:Ljava/lang/String;

.field public m_szSessionID:Ljava/lang/String;

.field public nDDFParserNodeIndex:I

.field public nDestoryNotiTime:J

.field public nNotiEvent:I

.field public nNotiReSyncMode:I

.field public nProxyIndex:I

.field public nSaveDeltaFileIndex:I

.field public nSkipDevDiscovery:I

.field public tUicResultKeep:Lcom/wssyncmldm/db/file/XDBUICResultKeepInfo;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/16 v3, -0x63

    const/4 v2, 0x0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/wssyncmldm/db/file/XDBProflieListInfo;->m_szNetworkConnName:Ljava/lang/String;

    .line 34
    iput v3, p0, Lcom/wssyncmldm/db/file/XDBProflieListInfo;->nProxyIndex:I

    .line 35
    const-wide/16 v0, -0x63

    iput-wide v0, p0, Lcom/wssyncmldm/db/file/XDBProflieListInfo;->nDestoryNotiTime:J

    .line 36
    iput v2, p0, Lcom/wssyncmldm/db/file/XDBProflieListInfo;->Profileindex:I

    .line 37
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/wssyncmldm/db/file/XDBProflieListInfo;->ProfileName:[Ljava/lang/String;

    .line 38
    iput v3, p0, Lcom/wssyncmldm/db/file/XDBProflieListInfo;->nSkipDevDiscovery:I

    .line 39
    new-instance v0, Lcom/wssyncmldm/db/file/XDBSessionSaveInfo;

    invoke-direct {v0}, Lcom/wssyncmldm/db/file/XDBSessionSaveInfo;-><init>()V

    iput-object v0, p0, Lcom/wssyncmldm/db/file/XDBProflieListInfo;->NotiResumeState:Lcom/wssyncmldm/db/file/XDBSessionSaveInfo;

    .line 40
    new-instance v0, Lcom/wssyncmldm/db/file/XDBUICResultKeepInfo;

    invoke-direct {v0}, Lcom/wssyncmldm/db/file/XDBUICResultKeepInfo;-><init>()V

    iput-object v0, p0, Lcom/wssyncmldm/db/file/XDBProflieListInfo;->tUicResultKeep:Lcom/wssyncmldm/db/file/XDBUICResultKeepInfo;

    .line 41
    iput v3, p0, Lcom/wssyncmldm/db/file/XDBProflieListInfo;->nDDFParserNodeIndex:I

    .line 42
    iput-boolean v2, p0, Lcom/wssyncmldm/db/file/XDBProflieListInfo;->bWifiOnly:Z

    .line 43
    iput-boolean v2, p0, Lcom/wssyncmldm/db/file/XDBProflieListInfo;->bAutoUpdate:Z

    .line 44
    iput-boolean v2, p0, Lcom/wssyncmldm/db/file/XDBProflieListInfo;->bPushMessage:Z

    .line 45
    iput v2, p0, Lcom/wssyncmldm/db/file/XDBProflieListInfo;->nSaveDeltaFileIndex:I

    .line 46
    return-void
.end method
