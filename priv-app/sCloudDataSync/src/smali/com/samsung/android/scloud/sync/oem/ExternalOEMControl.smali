.class public Lcom/samsung/android/scloud/sync/oem/ExternalOEMControl;
.super Ljava/lang/Object;
.source "ExternalOEMControl.java"

# interfaces
.implements Lcom/samsung/android/scloud/sync/oem/IOEMControl;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/scloud/sync/oem/ExternalOEMControl$Key;,
        Lcom/samsung/android/scloud/sync/oem/ExternalOEMControl$METHOD;
    }
.end annotation


# static fields
.field private static INSTANCE:Lcom/samsung/android/scloud/sync/oem/IOEMControl; = null

.field private static final TAG:Ljava/lang/String; = "ExternalIOEMControl"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    new-instance v0, Lcom/samsung/android/scloud/sync/oem/ExternalOEMControl;

    invoke-direct {v0}, Lcom/samsung/android/scloud/sync/oem/ExternalOEMControl;-><init>()V

    sput-object v0, Lcom/samsung/android/scloud/sync/oem/ExternalOEMControl;->INSTANCE:Lcom/samsung/android/scloud/sync/oem/IOEMControl;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    return-void
.end method

.method public static getInstance()Lcom/samsung/android/scloud/sync/oem/IOEMControl;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/samsung/android/scloud/sync/oem/ExternalOEMControl;->INSTANCE:Lcom/samsung/android/scloud/sync/oem/IOEMControl;

    return-object v0
.end method


# virtual methods
.method public complete(Landroid/content/Context;Lcom/samsung/android/scloud/sync/model/IModel;Lcom/samsung/android/scloud/sync/data/SyncItem;I)Z
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "model"    # Lcom/samsung/android/scloud/sync/model/IModel;
    .param p3, "item"    # Lcom/samsung/android/scloud/sync/data/SyncItem;
    .param p4, "resultCode"    # I

    .prologue
    .line 214
    const-string v3, "ExternalIOEMControl"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "complete : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", rcode : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 216
    .local v0, "input":Landroid/os/Bundle;
    const-string v3, "local_id"

    invoke-virtual {p3}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getLocalId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    const-string v4, "sync_key"

    invoke-virtual {p3}, Lcom/samsung/android/scloud/sync/data/SyncItem;->isNew()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p3}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getSyncKey()Ljava/lang/String;

    move-result-object v3

    :goto_0
    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    const-string v3, "rcode"

    invoke-virtual {v0, v3, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 219
    const-string v3, "timestamp"

    invoke-virtual {p3}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getTimeStamp()J

    move-result-wide v4

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 221
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-interface {p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getOemContentUri()Landroid/net/Uri;

    move-result-object v4

    const-string v5, "complete"

    invoke-interface {p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6, v0}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v2

    .line 223
    .local v2, "result":Landroid/os/Bundle;
    const-string v3, "is_success"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 225
    .local v1, "oemResult":Z
    if-nez v1, :cond_1

    .line 226
    new-instance v3, Lcom/samsung/android/scloud/framework/PDMException;

    const/16 v4, 0x13d

    invoke-direct {v3, v4}, Lcom/samsung/android/scloud/framework/PDMException;-><init>(I)V

    throw v3

    .line 217
    .end local v1    # "oemResult":Z
    .end local v2    # "result":Landroid/os/Bundle;
    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    .line 227
    .restart local v1    # "oemResult":Z
    .restart local v2    # "result":Landroid/os/Bundle;
    :cond_1
    return v1
.end method

.method public deleteLocal(Landroid/content/Context;Lcom/samsung/android/scloud/sync/model/IModel;Ljava/lang/String;)Z
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "model"    # Lcom/samsung/android/scloud/sync/model/IModel;
    .param p3, "localId"    # Ljava/lang/String;

    .prologue
    .line 198
    const-string v3, "ExternalIOEMControl"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "deleteLocal : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 200
    .local v0, "input":Landroid/os/Bundle;
    const-string v3, "local_id"

    invoke-virtual {v0, v3, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-interface {p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getOemContentUri()Landroid/net/Uri;

    move-result-object v4

    const-string v5, "deleteItem"

    invoke-interface {p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6, v0}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v2

    .line 204
    .local v2, "result":Landroid/os/Bundle;
    const-string v3, "is_success"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 206
    .local v1, "oemResult":Z
    if-nez v1, :cond_0

    .line 207
    new-instance v3, Lcom/samsung/android/scloud/framework/PDMException;

    const/16 v4, 0x13d

    invoke-direct {v3, v4}, Lcom/samsung/android/scloud/framework/PDMException;-><init>(I)V

    throw v3

    .line 208
    :cond_0
    return v1
.end method

.method public getAttachmentFileInfo(Landroid/content/Context;Lcom/samsung/android/scloud/sync/model/IModel;Ljava/lang/String;)Lcom/samsung/android/scloud/sync/data/Attachments;
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "model"    # Lcom/samsung/android/scloud/sync/model/IModel;
    .param p3, "localId"    # Ljava/lang/String;

    .prologue
    .line 119
    const-string v5, "ExternalIOEMControl"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getAttachmentFileInfo : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 121
    .local v2, "input":Landroid/os/Bundle;
    const-string v5, "local_id"

    invoke-virtual {v2, v5, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-interface {p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getOemContentUri()Landroid/net/Uri;

    move-result-object v6

    const-string v7, "getAttachmentInfo"

    invoke-interface {p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v6, v7, v8, v2}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v3

    .line 125
    .local v3, "result":Landroid/os/Bundle;
    const/4 v1, 0x0

    .line 126
    .local v1, "fileList":[Ljava/lang/String;
    const/4 v4, 0x0

    .line 127
    .local v4, "timestampList":[J
    const-string v5, "file_list"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 128
    const-string v5, "file_list"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 129
    const-string v5, "timestamp_list"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v4

    .line 139
    :cond_0
    new-instance v0, Lcom/samsung/android/scloud/sync/data/Attachments;

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/scloud/sync/data/Attachments;-><init>([Ljava/lang/String;[J)V

    .line 141
    .local v0, "attachments":Lcom/samsung/android/scloud/sync/data/Attachments;
    return-object v0
.end method

.method public getLocalChange(Landroid/content/Context;Lcom/samsung/android/scloud/sync/model/IModel;Lcom/samsung/android/scloud/sync/data/SyncItem;Lcom/samsung/android/scloud/sync/data/Attachments;)Z
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "model"    # Lcom/samsung/android/scloud/sync/model/IModel;
    .param p3, "item"    # Lcom/samsung/android/scloud/sync/data/SyncItem;
    .param p4, "attachements"    # Lcom/samsung/android/scloud/sync/data/Attachments;

    .prologue
    .line 147
    const-string v3, "ExternalIOEMControl"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getLocalChange : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 150
    .local v0, "input":Landroid/os/Bundle;
    const-string v3, "local_id"

    invoke-virtual {p3}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getLocalId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    if-eqz p4, :cond_0

    invoke-virtual {p4}, Lcom/samsung/android/scloud/sync/data/Attachments;->hasFile()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 154
    const-string v3, "upload_file_list"

    invoke-virtual {p4}, Lcom/samsung/android/scloud/sync/data/Attachments;->getFileArray()[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 157
    :cond_0
    const-string v3, "to_upload_file_uri"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "content://com.samsung.android.scloud.sync/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p2, p3}, Lcom/samsung/android/scloud/sync/model/IModel;->getLocalFileKeyHader(Lcom/samsung/android/scloud/sync/data/SyncItem;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-interface {p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getOemContentUri()Landroid/net/Uri;

    move-result-object v4

    const-string v5, "upload"

    invoke-interface {p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6, v0}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v2

    .line 161
    .local v2, "result":Landroid/os/Bundle;
    const-string v3, "is_success"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 163
    .local v1, "oemResult":Z
    if-nez v1, :cond_1

    .line 164
    new-instance v3, Lcom/samsung/android/scloud/framework/PDMException;

    const/16 v4, 0x13d

    invoke-direct {v3, v4}, Lcom/samsung/android/scloud/framework/PDMException;-><init>(I)V

    throw v3

    .line 166
    :cond_1
    return v1
.end method

.method public prepareToSync(Landroid/content/Context;Lcom/samsung/android/scloud/sync/model/IModel;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 22
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "model"    # Lcom/samsung/android/scloud/sync/model/IModel;
    .param p4, "accountType"    # Ljava/lang/String;
    .param p5, "accountName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/samsung/android/scloud/sync/model/IModel;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/scloud/sync/data/SyncItem;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/scloud/sync/data/SyncItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 65
    .local p3, "serverChanges":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/samsung/android/scloud/sync/data/SyncItem;>;"
    const-string v2, "ExternalIOEMControl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "prepareToSync : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface/range {p2 .. p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    new-instance v11, Landroid/os/Bundle;

    invoke-direct {v11}, Landroid/os/Bundle;-><init>()V

    .line 68
    .local v11, "input":Landroid/os/Bundle;
    if-eqz p3, :cond_1

    invoke-interface/range {p3 .. p3}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 69
    invoke-interface/range {p3 .. p3}, Ljava/util/Map;->size()I

    move-result v2

    new-array v14, v2, [Ljava/lang/String;

    .line 70
    .local v14, "keyArr":[Ljava/lang/String;
    invoke-interface/range {p3 .. p3}, Ljava/util/Map;->size()I

    move-result v2

    new-array v0, v2, [J

    move-object/from16 v21, v0

    .line 71
    .local v21, "timestampArr":[J
    invoke-interface/range {p3 .. p3}, Ljava/util/Map;->size()I

    move-result v2

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v19, v0

    .line 72
    .local v19, "tagArr":[Ljava/lang/String;
    const/4 v9, 0x0

    .line 73
    .local v9, "i":I
    invoke-interface/range {p3 .. p3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Map$Entry;

    .line 74
    .local v7, "changeSet":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/scloud/sync/data/SyncItem;>;"
    invoke-interface {v7}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    aput-object v2, v14, v9

    .line 75
    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/scloud/sync/data/SyncItem;

    invoke-virtual {v2}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getTimeStamp()J

    move-result-wide v2

    aput-wide v2, v21, v9

    .line 76
    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/scloud/sync/data/SyncItem;

    invoke-virtual {v2}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getTag()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v19, v9

    .line 77
    const-string v2, "ExternalIOEMControl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "prepareToSync - key : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v14, v9

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", timestamp : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-wide v4, v21, v9

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", tag : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v19, v9

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    add-int/lit8 v9, v9, 0x1

    .line 79
    goto :goto_0

    .line 81
    .end local v7    # "changeSet":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/scloud/sync/data/SyncItem;>;"
    :cond_0
    const-string v2, "sync_key"

    invoke-virtual {v11, v2, v14}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 82
    const-string v2, "timestamp"

    move-object/from16 v0, v21

    invoke-virtual {v11, v2, v0}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    .line 83
    const-string v2, "tag"

    move-object/from16 v0, v19

    invoke-virtual {v11, v2, v0}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 86
    .end local v9    # "i":I
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v14    # "keyArr":[Ljava/lang/String;
    .end local v19    # "tagArr":[Ljava/lang/String;
    .end local v21    # "timestampArr":[J
    :cond_1
    const-string v2, "account_type"

    const-string v3, "com.osp.app.signin"

    invoke-virtual {v11, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    const-string v2, "account_name"

    move-object/from16 v0, p5

    invoke-virtual {v11, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-interface/range {p2 .. p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getOemContentUri()Landroid/net/Uri;

    move-result-object v3

    const-string v4, "prepare"

    invoke-interface/range {p2 .. p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5, v11}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v16

    .line 91
    .local v16, "result":Landroid/os/Bundle;
    const-string v2, "is_success"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v12

    .line 93
    .local v12, "isSuccess":Z
    if-nez v12, :cond_2

    .line 94
    new-instance v2, Lcom/samsung/android/scloud/framework/PDMException;

    const/16 v3, 0x13d

    invoke-direct {v2, v3}, Lcom/samsung/android/scloud/framework/PDMException;-><init>(I)V

    throw v2

    .line 96
    :cond_2
    const-string v2, "local_id"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v15

    .line 97
    .local v15, "localId":[Ljava/lang/String;
    const-string v2, "sync_key"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v17

    .line 98
    .local v17, "syncKey":[Ljava/lang/String;
    const-string v2, "timestamp"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v20

    .line 99
    .local v20, "timestamp":[J
    const-string v2, "deleted"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBooleanArray(Ljava/lang/String;)[Z

    move-result-object v8

    .line 101
    .local v8, "deleted":[Z
    const-string v2, "tag"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v18

    .line 103
    .local v18, "tag":[Ljava/lang/String;
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 104
    .local v13, "items":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/scloud/sync/data/SyncItem;>;"
    if-eqz v15, :cond_4

    if-eqz v17, :cond_4

    .line 105
    const/4 v9, 0x0

    .restart local v9    # "i":I
    :goto_1
    array-length v2, v15

    if-ge v9, v2, :cond_4

    .line 106
    new-instance v1, Lcom/samsung/android/scloud/sync/data/SyncItem;

    aget-object v2, v15, v9

    aget-object v3, v17, v9

    aget-wide v4, v20, v9

    aget-boolean v6, v8, v9

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/scloud/sync/data/SyncItem;-><init>(Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 107
    .local v1, "item":Lcom/samsung/android/scloud/sync/data/SyncItem;
    if-eqz v18, :cond_3

    aget-object v2, v18, v9

    invoke-virtual {v1, v2}, Lcom/samsung/android/scloud/sync/data/SyncItem;->setTag(Ljava/lang/String;)V

    .line 108
    :cond_3
    invoke-interface {v13, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 109
    const-string v2, "ExternalIOEMControl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface/range {p2 .. p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] : LocalItem - key : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getSyncKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", timestamp : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getTimeStamp()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", deleted : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/samsung/android/scloud/sync/data/SyncItem;->isDeleted()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", tag : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getTag()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 113
    .end local v1    # "item":Lcom/samsung/android/scloud/sync/data/SyncItem;
    .end local v9    # "i":I
    :cond_4
    return-object v13
.end method

.method public updateLocal(Landroid/content/Context;Lcom/samsung/android/scloud/sync/model/IModel;Lcom/samsung/android/scloud/sync/data/SyncItem;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "model"    # Lcom/samsung/android/scloud/sync/model/IModel;
    .param p3, "item"    # Lcom/samsung/android/scloud/sync/data/SyncItem;
    .param p6, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/samsung/android/scloud/sync/model/IModel;",
            "Lcom/samsung/android/scloud/sync/data/SyncItem;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 173
    .local p4, "toDownloadAttFileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p5, "toDeleteAttFileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v3, "ExternalIOEMControl"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "updateLocal : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 175
    .local v0, "input":Landroid/os/Bundle;
    const-string v3, "local_id"

    invoke-virtual {p3}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getLocalId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    const-string v3, "sync_key"

    invoke-virtual {p3}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getSyncKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    const-string v3, "timestamp"

    invoke-virtual {p3}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getTimeStamp()J

    move-result-wide v4

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 179
    if-eqz p4, :cond_0

    invoke-interface {p4}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 180
    const-string v4, "download_file_list"

    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-interface {p4, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 181
    :cond_0
    if-eqz p5, :cond_1

    invoke-interface {p5}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 182
    const-string v4, "deleted_file_list"

    invoke-interface {p5}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-interface {p5, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 184
    :cond_1
    const-string v3, "to_download_file_uri"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "content://com.samsung.android.scloud.sync"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {p6, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-interface {p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getOemContentUri()Landroid/net/Uri;

    move-result-object v4

    const-string v5, "download"

    invoke-interface {p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6, v0}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v2

    .line 188
    .local v2, "result":Landroid/os/Bundle;
    const-string v3, "local_id"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 190
    .local v1, "oemResult":Ljava/lang/String;
    if-nez v1, :cond_2

    .line 191
    new-instance v3, Lcom/samsung/android/scloud/framework/PDMException;

    const/16 v4, 0x13d

    const-string v5, "Local Id is null"

    invoke-direct {v3, v4, v5}, Lcom/samsung/android/scloud/framework/PDMException;-><init>(ILjava/lang/String;)V

    throw v3

    .line 192
    :cond_2
    return-object v1
.end method
