.class public interface abstract Lcom/samsung/android/scloud/sync/oem/IOEMControl;
.super Ljava/lang/Object;
.source "IOEMControl.java"


# virtual methods
.method public abstract complete(Landroid/content/Context;Lcom/samsung/android/scloud/sync/model/IModel;Lcom/samsung/android/scloud/sync/data/SyncItem;I)Z
.end method

.method public abstract deleteLocal(Landroid/content/Context;Lcom/samsung/android/scloud/sync/model/IModel;Ljava/lang/String;)Z
.end method

.method public abstract getAttachmentFileInfo(Landroid/content/Context;Lcom/samsung/android/scloud/sync/model/IModel;Ljava/lang/String;)Lcom/samsung/android/scloud/sync/data/Attachments;
.end method

.method public abstract getLocalChange(Landroid/content/Context;Lcom/samsung/android/scloud/sync/model/IModel;Lcom/samsung/android/scloud/sync/data/SyncItem;Lcom/samsung/android/scloud/sync/data/Attachments;)Z
.end method

.method public abstract prepareToSync(Landroid/content/Context;Lcom/samsung/android/scloud/sync/model/IModel;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/samsung/android/scloud/sync/model/IModel;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/scloud/sync/data/SyncItem;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/scloud/sync/data/SyncItem;",
            ">;"
        }
    .end annotation
.end method

.method public abstract updateLocal(Landroid/content/Context;Lcom/samsung/android/scloud/sync/model/IModel;Lcom/samsung/android/scloud/sync/data/SyncItem;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/samsung/android/scloud/sync/model/IModel;",
            "Lcom/samsung/android/scloud/sync/data/SyncItem;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation
.end method
