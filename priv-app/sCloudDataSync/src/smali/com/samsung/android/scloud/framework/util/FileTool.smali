.class public final Lcom/samsung/android/scloud/framework/util/FileTool;
.super Ljava/lang/Object;
.source "FileTool.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "FileTool"

.field private static mMessageDigest:Ljava/security/MessageDigest;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/scloud/framework/util/FileTool;->mMessageDigest:Ljava/security/MessageDigest;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static encode(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "message"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 195
    :try_start_0
    const-string v3, "SHA1"

    invoke-static {v3}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    .line 196
    .local v1, "md":Ljava/security/MessageDigest;
    const-string v3, "UTF-8"

    invoke-virtual {p0, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v3, v4}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 202
    .end local v1    # "md":Ljava/security/MessageDigest;
    :goto_0
    return-object v2

    .line 197
    :catch_0
    move-exception v0

    .line 198
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    goto :goto_0

    .line 200
    .end local v0    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_1
    move-exception v0

    .line 201
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getByteArr(Ljava/io/InputStream;)[B
    .locals 6
    .param p0, "inputStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v5, 0x400

    const/4 v4, 0x0

    .line 184
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 185
    .local v1, "byteOpStream":Ljava/io/ByteArrayOutputStream;
    new-array v0, v5, [B

    .line 186
    .local v0, "buff":[B
    :goto_0
    invoke-virtual {p0, v0, v4, v5}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    .local v2, "len":I
    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 187
    invoke-virtual {v1, v0, v4, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0

    .line 188
    :cond_0
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 189
    return-object v0
.end method

.method public static declared-synchronized getMessageDigest(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 9
    .param p0, "fis"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/NoSuchAlgorithmException;
        }
    .end annotation

    .prologue
    .line 59
    const-class v7, Lcom/samsung/android/scloud/framework/util/FileTool;

    monitor-enter v7

    const/4 v0, 0x0

    .line 60
    .local v0, "bHex":I
    const/16 v6, 0x2000

    :try_start_0
    new-array v1, v6, [B

    .line 61
    .local v1, "buff":[B
    sget-object v6, Lcom/samsung/android/scloud/framework/util/FileTool;->mMessageDigest:Ljava/security/MessageDigest;

    if-nez v6, :cond_0

    .line 62
    const-string v6, "MD5"

    invoke-static {v6}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v6

    sput-object v6, Lcom/samsung/android/scloud/framework/util/FileTool;->mMessageDigest:Ljava/security/MessageDigest;

    .line 66
    :goto_0
    const/4 v4, 0x0

    .line 67
    .local v4, "len":I
    :goto_1
    invoke-virtual {p0, v1}, Ljava/io/InputStream;->read([B)I

    move-result v4

    if-lez v4, :cond_1

    .line 68
    sget-object v6, Lcom/samsung/android/scloud/framework/util/FileTool;->mMessageDigest:Ljava/security/MessageDigest;

    const/4 v8, 0x0

    invoke-virtual {v6, v1, v8, v4}, Ljava/security/MessageDigest;->update([BII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 59
    .end local v1    # "buff":[B
    .end local v4    # "len":I
    :catchall_0
    move-exception v6

    monitor-exit v7

    throw v6

    .line 64
    .restart local v1    # "buff":[B
    :cond_0
    :try_start_1
    sget-object v6, Lcom/samsung/android/scloud/framework/util/FileTool;->mMessageDigest:Ljava/security/MessageDigest;

    invoke-virtual {v6}, Ljava/security/MessageDigest;->reset()V

    goto :goto_0

    .line 70
    .restart local v4    # "len":I
    :cond_1
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    .line 71
    sget-object v6, Lcom/samsung/android/scloud/framework/util/FileTool;->mMessageDigest:Ljava/security/MessageDigest;

    invoke-virtual {v6}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v5

    .line 72
    .local v5, "md5Data":[B
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 74
    .local v2, "checksum":Ljava/lang/StringBuilder;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    array-length v6, v5

    if-ge v3, v6, :cond_3

    .line 75
    aget-byte v6, v5, v3

    and-int/lit16 v0, v6, 0xff

    .line 76
    const/16 v6, 0xf

    if-gt v0, v6, :cond_2

    .line 78
    const-string v6, "0"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 82
    :cond_3
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v6

    monitor-exit v7

    return-object v6
.end method

.method public static declared-synchronized getMessageDigest(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "filepath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/NoSuchAlgorithmException;
        }
    .end annotation

    .prologue
    .line 52
    const-class v2, Lcom/samsung/android/scloud/framework/util/FileTool;

    monitor-enter v2

    :try_start_0
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 54
    .local v0, "fis":Ljava/io/FileInputStream;
    invoke-static {v0}, Lcom/samsung/android/scloud/framework/util/FileTool;->getMessageDigest(Ljava/io/InputStream;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    monitor-exit v2

    return-object v1

    .line 52
    .end local v0    # "fis":Ljava/io/FileInputStream;
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method public static declared-synchronized getMessageDigestFromString(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "msg"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/NoSuchAlgorithmException;
        }
    .end annotation

    .prologue
    .line 44
    const-class v2, Lcom/samsung/android/scloud/framework/util/FileTool;

    monitor-enter v2

    :try_start_0
    new-instance v0, Ljava/io/ByteArrayInputStream;

    const-string v1, "UTF-8"

    invoke-virtual {p0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 46
    .local v0, "bis":Ljava/io/ByteArrayInputStream;
    invoke-static {v0}, Lcom/samsung/android/scloud/framework/util/FileTool;->getMessageDigest(Ljava/io/InputStream;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    monitor-exit v2

    return-object v1

    .line 44
    .end local v0    # "bis":Ljava/io/ByteArrayInputStream;
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method public static isSameFile(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p0, "filepath"    # Ljava/lang/String;
    .param p1, "checkSum"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 104
    :try_start_0
    invoke-static {p0}, Lcom/samsung/android/scloud/framework/util/FileTool;->getMessageDigest(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 111
    .local v1, "fileChecksum":Ljava/lang/String;
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 112
    const/4 v2, 0x1

    .line 114
    .end local v1    # "fileChecksum":Ljava/lang/String;
    :cond_0
    :goto_0
    return v2

    .line 105
    :catch_0
    move-exception v0

    .line 106
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    goto :goto_0

    .line 107
    .end local v0    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_1
    move-exception v0

    .line 108
    .local v0, "e":Ljava/io/IOException;
    goto :goto_0
.end method

.method public static isSameFileInputStream(Ljava/io/FileInputStream;Ljava/lang/String;)Z
    .locals 4
    .param p0, "fis"    # Ljava/io/FileInputStream;
    .param p1, "checkSum"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 88
    :try_start_0
    invoke-static {p0}, Lcom/samsung/android/scloud/framework/util/FileTool;->getMessageDigest(Ljava/io/InputStream;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 95
    .local v1, "fileChecksum":Ljava/lang/String;
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 96
    const/4 v2, 0x1

    .line 98
    .end local v1    # "fileChecksum":Ljava/lang/String;
    :cond_0
    :goto_0
    return v2

    .line 89
    :catch_0
    move-exception v0

    .line 90
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    goto :goto_0

    .line 91
    .end local v0    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_1
    move-exception v0

    .line 92
    .local v0, "e":Ljava/io/IOException;
    goto :goto_0
.end method

.method public static writeToFile(Ljava/io/InputStream;JLjava/io/FileOutputStream;Lcom/samsung/android/scloud/framework/network/NetworkUtil$PDMProgressListener;)V
    .locals 8
    .param p0, "inputStream"    # Ljava/io/InputStream;
    .param p1, "size"    # J
    .param p3, "fileOpStream"    # Ljava/io/FileOutputStream;
    .param p4, "handler"    # Lcom/samsung/android/scloud/framework/network/NetworkUtil$PDMProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 120
    :try_start_0
    const-string v5, "FileTool"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "writeToFile - start Write with stream : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/framework/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    const/high16 v5, 0x20000

    new-array v0, v5, [B

    .line 124
    .local v0, "buffer":[B
    const/4 v2, 0x0

    .line 125
    .local v2, "len":I
    const-wide/16 v3, 0x0

    .line 126
    .local v3, "sum":J
    :goto_0
    invoke-virtual {p0, v0}, Ljava/io/InputStream;->read([B)I

    move-result v2

    if-lez v2, :cond_3

    .line 127
    int-to-long v5, v2

    add-long/2addr v3, v5

    .line 129
    if-eqz p4, :cond_0

    .line 130
    invoke-interface {p4, v3, v4, p1, p2}, Lcom/samsung/android/scloud/framework/network/NetworkUtil$PDMProgressListener;->transferred(JJ)V

    .line 131
    :cond_0
    const/4 v5, 0x0

    invoke-virtual {p3, v0, v5, v2}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 133
    .end local v0    # "buffer":[B
    .end local v2    # "len":I
    .end local v3    # "sum":J
    :catch_0
    move-exception v1

    .line 134
    .local v1, "ioe":Ljava/io/IOException;
    :try_start_1
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 136
    .end local v1    # "ioe":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    if-eqz p0, :cond_1

    .line 137
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    .line 138
    :cond_1
    if-eqz p3, :cond_2

    .line 139
    invoke-virtual {p3}, Ljava/io/FileOutputStream;->close()V

    :cond_2
    throw v5

    .line 136
    .restart local v0    # "buffer":[B
    .restart local v2    # "len":I
    .restart local v3    # "sum":J
    :cond_3
    if-eqz p0, :cond_4

    .line 137
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    .line 138
    :cond_4
    if-eqz p3, :cond_5

    .line 139
    invoke-virtual {p3}, Ljava/io/FileOutputStream;->close()V

    .line 141
    :cond_5
    return-void
.end method

.method public static writeToFile(Ljava/io/InputStream;JLjava/lang/String;Lcom/samsung/android/scloud/framework/network/NetworkUtil$PDMProgressListener;)V
    .locals 10
    .param p0, "inputStream"    # Ljava/io/InputStream;
    .param p1, "size"    # J
    .param p3, "filepath"    # Ljava/lang/String;
    .param p4, "handler"    # Lcom/samsung/android/scloud/framework/network/NetworkUtil$PDMProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 145
    const-string v6, "FileTool"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "writeToFile - start Write with stream : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/scloud/framework/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    const-string v6, "/"

    invoke-virtual {p3, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 149
    .local v5, "split":[Ljava/lang/String;
    array-length v6, v5

    add-int/lit8 v6, v6, -0x1

    aget-object v1, v5, v6

    .line 150
    .local v1, "fileName":Ljava/lang/String;
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v7

    sub-int/2addr v6, v7

    invoke-virtual {p3, v9, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 152
    .local v3, "folderPath":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 153
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_0

    .line 154
    const-string v6, "FileTool"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Creating folder : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v4

    .line 157
    .local v4, "result":Z
    if-nez v4, :cond_0

    .line 159
    const-string v6, "FileTool"

    const-string v7, "ORSMetaResponse.fromBinaryFile(): Can not create directory. "

    invoke-static {v6, v7}, Lcom/samsung/android/scloud/framework/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    new-instance v6, Ljava/io/IOException;

    invoke-direct {v6}, Ljava/io/IOException;-><init>()V

    throw v6

    .line 164
    .end local v4    # "result":Z
    :cond_0
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, p3, v9}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;Z)V

    .line 166
    .local v2, "fileOpStream":Ljava/io/FileOutputStream;
    invoke-static {p0, p1, p2, v2, p4}, Lcom/samsung/android/scloud/framework/util/FileTool;->writeToFile(Ljava/io/InputStream;JLjava/io/FileOutputStream;Lcom/samsung/android/scloud/framework/network/NetworkUtil$PDMProgressListener;)V

    .line 168
    return-void
.end method

.method public static writeToFile(Ljava/lang/String;JLjava/io/FileOutputStream;Lcom/samsung/android/scloud/framework/network/NetworkUtil$PDMProgressListener;)V
    .locals 3
    .param p0, "inputFile"    # Ljava/lang/String;
    .param p1, "size"    # J
    .param p3, "outputStream"    # Ljava/io/FileOutputStream;
    .param p4, "handler"    # Lcom/samsung/android/scloud/framework/network/NetworkUtil$PDMProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 172
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 173
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 174
    new-instance v2, Ljava/io/IOException;

    invoke-direct {v2}, Ljava/io/IOException;-><init>()V

    throw v2

    .line 176
    :cond_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 178
    .local v1, "inputStream":Ljava/io/FileInputStream;
    invoke-static {v1, p1, p2, p3, p4}, Lcom/samsung/android/scloud/framework/util/FileTool;->writeToFile(Ljava/io/InputStream;JLjava/io/FileOutputStream;Lcom/samsung/android/scloud/framework/network/NetworkUtil$PDMProgressListener;)V

    .line 179
    return-void
.end method
