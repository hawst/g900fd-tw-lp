.class public Lcom/samsung/android/scloud/framework/PDMConstants;
.super Ljava/lang/Object;
.source "PDMConstants.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/scloud/framework/PDMConstants$ResultCode;,
        Lcom/samsung/android/scloud/framework/PDMConstants$Status;,
        Lcom/samsung/android/scloud/framework/PDMConstants$ServiceType;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    return-void
.end method

.method public static getRCode(I)I
    .locals 1
    .param p0, "msgCode"    # I

    .prologue
    .line 57
    rem-int/lit16 v0, p0, 0x3e8

    return v0
.end method

.method public static getServiceType(I)I
    .locals 1
    .param p0, "msgCode"    # I

    .prologue
    .line 49
    const v0, 0xf4240

    div-int v0, p0, v0

    return v0
.end method

.method public static getStatus(I)I
    .locals 1
    .param p0, "msgCode"    # I

    .prologue
    .line 53
    const v0, 0xf4240

    rem-int v0, p0, v0

    div-int/lit16 v0, v0, 0x3e8

    return v0
.end method

.method public static makeMassageCode(III)I
    .locals 2
    .param p0, "serviceType"    # I
    .param p1, "status"    # I
    .param p2, "rCode"    # I

    .prologue
    .line 43
    const v0, 0xf4240

    mul-int/2addr v0, p0

    mul-int/lit16 v1, p1, 0x3e8

    add-int/2addr v0, v1

    mul-int/lit8 v1, p2, 0x1

    add-int/2addr v0, v1

    return v0
.end method
