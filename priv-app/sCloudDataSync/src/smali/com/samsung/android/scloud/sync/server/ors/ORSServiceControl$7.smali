.class Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$7;
.super Lcom/samsung/android/scloud/framework/network/NetworkUtil$JSONResponseHandler;
.source "ORSServiceControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->uploadItem(Lcom/samsung/android/scloud/sync/data/SyncItem;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

.field final synthetic val$file:Ljava/lang/String;

.field final synthetic val$serverFileMap:Ljava/util/Map;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;Ljava/util/Map;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 300
    iput-object p1, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$7;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    iput-object p2, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$7;->val$serverFileMap:Ljava/util/Map;

    iput-object p3, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$7;->val$file:Ljava/lang/String;

    invoke-direct {p0}, Lcom/samsung/android/scloud/framework/network/NetworkUtil$JSONResponseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleJSONResponse(Lorg/json/JSONObject;)V
    .locals 7
    .param p1, "json"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 304
    iget-object v1, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$7;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    # getter for: Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mListener:Lcom/samsung/android/scloud/framework/IStatusListener;
    invoke-static {v1}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->access$100(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Lcom/samsung/android/scloud/framework/IStatusListener;

    move-result-object v1

    const/4 v2, 0x4

    const-string v3, "ORSServiceControl"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "deleteFile Finished - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$7;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    # getter for: Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mCtid:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->access$000(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v2, v3, v4}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 305
    const-string v1, "revision"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    .line 306
    .local v0, "revision":I
    iget-object v1, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$7;->val$serverFileMap:Ljava/util/Map;

    iget-object v2, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$7;->val$file:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [J

    aget-wide v1, v1, v6

    int-to-long v3, v0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    .line 307
    new-instance v2, Lcom/samsung/android/scloud/framework/PDMException;

    const/16 v3, 0x13e

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Revision Conflict on : "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$7;->val$file:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", transaction rev : "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v1, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$7;->val$serverFileMap:Ljava/util/Map;

    iget-object v5, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$7;->val$file:Ljava/lang/String;

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [J

    aget-wide v5, v1, v6

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", server rev : "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Lcom/samsung/android/scloud/framework/PDMException;-><init>(ILjava/lang/String;)V

    throw v2

    .line 308
    :cond_0
    return-void
.end method
