.class public interface abstract Lcom/samsung/android/scloud/framework/PDMConstants$ResultCode;
.super Ljava/lang/Object;
.source "PDMConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/framework/PDMConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ResultCode"
.end annotation


# static fields
.field public static final DO_NOTHING:I = 0x12e

.field public static final EXTRACT_KEY:I = 0x1

.field public static final FAIL_AUTHENTICATION:I = 0x131

.field public static final FAIL_DUPLICATED_SYNC_KEY:I = 0x140

.field public static final FAIL_FILE_IO:I = 0x138

.field public static final FAIL_FILE_WRITE_ERR:I = 0x133

.field public static final FAIL_FROM_OEM:I = 0x13d

.field public static final FAIL_HTTP:I = 0x12f

.field public static final FAIL_IN_PROCESS:I = 0x13c

.field public static final FAIL_IN_TRANSACTION:I = 0x13e

.field public static final FAIL_JSON:I = 0x130

.field public static final FAIL_OEM_GET:I = 0x13a

.field public static final FAIL_OEM_INSERT:I = 0x139

.field public static final FAIL_OTHER:I = 0x135

.field public static final FAIL_OUT_OF_MEMORY:I = 0x13f

.field public static final FAIL_RESTORE_READY:I = 0x136

.field public static final FAIL_SERVER_ERR:I = 0x13b

.field public static final FAIL_SERVER_STORAGE_FULL:I = 0x137

.field public static final FAIL_THREAD:I = 0x134

.field public static final FAIL_TIME_DIFFERENCE:I = 0x141

.field public static final FAIL_USER_CANCELED:I = 0x132

.field public static final SUCCESS:I = 0x12d
