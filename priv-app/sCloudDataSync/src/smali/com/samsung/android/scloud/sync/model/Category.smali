.class public Lcom/samsung/android/scloud/sync/model/Category;
.super Lcom/samsung/android/scloud/sync/model/CommonORSModel;
.source "Category.java"

# interfaces
.implements Lcom/samsung/android/scloud/sync/model/IInternalModel;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/scloud/sync/model/Category$sync_state;,
        Lcom/samsung/android/scloud/sync/model/Category$CategoryTable;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/samsung/android/scloud/sync/model/CommonORSModel;-><init>()V

    .line 94
    return-void
.end method


# virtual methods
.method public getAccountNameFieldName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    const-string v0, "accountName"

    return-object v0
.end method

.method public getAccountTypeFieldName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    const-string v0, "accountType"

    return-object v0
.end method

.method public getCid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    const-string v0, "w8wcqZo4Uk"

    return-object v0
.end method

.method public getDeletedFieldName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    const-string v0, "isDeleted"

    return-object v0
.end method

.method public getDeletedValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    const-string v0, "1"

    return-object v0
.end method

.method public getDirtyFieldName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    const-string v0, "isDirty"

    return-object v0
.end method

.method public getDirtyValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    const-string v0, "1"

    return-object v0
.end method

.method public getLocalIdFieldName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    const-string v0, "UUID"

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18
    const-string v0, "MEMO_CATE"

    return-object v0
.end method

.method public getOEMControl()Lcom/samsung/android/scloud/sync/oem/IOEMControl;
    .locals 1

    .prologue
    .line 13
    invoke-static {}, Lcom/samsung/android/scloud/sync/oem/InternalOEMControl;->getInstance()Lcom/samsung/android/scloud/sync/oem/IOEMControl;

    move-result-object v0

    return-object v0
.end method

.method public getOemContentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/samsung/android/scloud/sync/common/SyncSources$OEMAuthorities;->CATEGORY_OEM_CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method public getSyncKeyFieldName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    const-string v0, "sync2"

    return-object v0
.end method

.method public getTimestampFieldName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    const-string v0, "sync1"

    return-object v0
.end method
