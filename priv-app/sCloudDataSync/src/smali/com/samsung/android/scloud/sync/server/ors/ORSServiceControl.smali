.class public Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;
.super Ljava/lang/Object;
.source "ORSServiceControl.java"

# interfaces
.implements Lcom/samsung/android/scloud/sync/server/ICloudServiceControl;


# static fields
.field private static final CONTENT_REVISION_IDX:I = 0x1

.field private static CONTROL_MAP:Ljava/util/concurrent/ConcurrentHashMap; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;",
            ">;"
        }
    .end annotation
.end field

.field private static final FOLDER_REVISION_IDX:I = 0x0

.field private static final MAX_KEY_GET_COUNT:I = 0x7d0

.field private static final TAG:Ljava/lang/String; = "ORSServiceControl"


# instance fields
.field private mAuth:Lcom/samsung/android/scloud/auth/AuthManager;

.field private mContext:Landroid/content/Context;

.field private mCtid:Ljava/lang/String;

.field private mListener:Lcom/samsung/android/scloud/framework/IStatusListener;

.field private mModel:Lcom/samsung/android/scloud/sync/model/IModel;

.field private mRevisionMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[I>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->CONTROL_MAP:Ljava/util/concurrent/ConcurrentHashMap;

    return-void
.end method

.method private constructor <init>(Lcom/samsung/android/scloud/sync/model/IModel;)V
    .locals 0
    .param p1, "model"    # Lcom/samsung/android/scloud/sync/model/IModel;

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    .line 47
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mCtid:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Lcom/samsung/android/scloud/framework/IStatusListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mListener:Lcom/samsung/android/scloud/framework/IStatusListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Lcom/samsung/android/scloud/sync/model/IModel;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mRevisionMap:Ljava/util/Map;

    return-object v0
.end method

.method public static declared-synchronized getInstance(Lcom/samsung/android/scloud/sync/model/IModel;)Lcom/samsung/android/scloud/sync/server/ICloudServiceControl;
    .locals 4
    .param p0, "model"    # Lcom/samsung/android/scloud/sync/model/IModel;

    .prologue
    .line 50
    const-class v1, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->CONTROL_MAP:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-interface {p0}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 51
    sget-object v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->CONTROL_MAP:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-interface {p0}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    invoke-direct {v3, p0}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;-><init>(Lcom/samsung/android/scloud/sync/model/IModel;)V

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    :cond_0
    sget-object v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->CONTROL_MAP:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-interface {p0}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/sync/server/ICloudServiceControl;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 50
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public deleteItem(Lcom/samsung/android/scloud/sync/data/SyncItem;)Z
    .locals 22
    .param p1, "item"    # Lcom/samsung/android/scloud/sync/data/SyncItem;

    .prologue
    .line 516
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mRevisionMap:Ljava/util/Map;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getSyncKey()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 517
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mRevisionMap:Ljava/util/Map;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getSyncKey()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [I

    fill-array-data v4, :array_0

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 519
    :cond_0
    const/4 v2, 0x1

    new-array v0, v2, [I

    move-object/from16 v21, v0

    const/4 v2, 0x0

    const/4 v3, 0x1

    aput v3, v21, v2

    .line 520
    .local v21, "txSeq":[I
    const/4 v2, 0x1

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v20, v0

    const/4 v2, 0x0

    const-string v3, ""

    aput-object v3, v20, v2

    .line 522
    .local v20, "txKey":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mAuth:Lcom/samsung/android/scloud/auth/AuthManager;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mCtid:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    invoke-interface {v5}, Lcom/samsung/android/scloud/sync/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$15;

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v6, v0, v1}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$15;-><init>(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;[Ljava/lang/String;)V

    invoke-static {v2, v3, v4, v5, v6}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceManager;->transactionStart(Landroid/content/Context;Lcom/samsung/android/scloud/auth/AuthManager;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/scloud/framework/network/NetworkUtil$JSONResponseHandler;)V

    .line 531
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mRevisionMap:Ljava/util/Map;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getSyncKey()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [I

    const/4 v3, 0x0

    aget v9, v2, v3

    .line 533
    .local v9, "folderRevision":I
    if-nez v9, :cond_1

    .line 534
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mListener:Lcom/samsung/android/scloud/framework/IStatusListener;

    const/4 v3, 0x0

    const-string v4, "ORSServiceControl"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Should find folder revision to delete safty!!! key : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getSyncKey()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v3, v4, v5}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 537
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getTimeStamp()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    .line 538
    .local v8, "tag":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mAuth:Lcom/samsung/android/scloud/auth/AuthManager;

    const/4 v4, 0x0

    aget-object v4, v20, v4

    const/4 v6, 0x0

    aget v5, v21, v6

    add-int/lit8 v7, v5, 0x1

    aput v7, v21, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mCtid:Ljava/lang/String;

    new-instance v11, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$16;

    move-object/from16 v0, p0

    invoke-direct {v11, v0}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$16;-><init>(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)V

    move-object/from16 v7, p1

    invoke-static/range {v2 .. v11}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceManager;->updateTag(Landroid/content/Context;Lcom/samsung/android/scloud/auth/AuthManager;Ljava/lang/String;ILcom/samsung/android/scloud/sync/model/IModel;Lcom/samsung/android/scloud/sync/data/SyncItem;Ljava/lang/String;ILjava/lang/String;Lcom/samsung/android/scloud/framework/network/NetworkUtil$StringResponseHandler;)V

    .line 547
    :try_start_0
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mAuth:Lcom/samsung/android/scloud/auth/AuthManager;

    const/4 v2, 0x0

    aget-object v12, v20, v2

    const/4 v2, 0x0

    aget v13, v21, v2

    add-int/lit8 v3, v13, 0x1

    aput v3, v21, v2

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    move-object/from16 v0, p1

    invoke-interface {v2, v0}, Lcom/samsung/android/scloud/sync/model/IModel;->getServerFilePathPrefix(Lcom/samsung/android/scloud/sync/data/SyncItem;)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mCtid:Ljava/lang/String;

    move-object/from16 v17, v0

    new-instance v18, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$17;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$17;-><init>(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)V

    move/from16 v16, v9

    invoke-static/range {v10 .. v18}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceManager;->deleteFolder(Landroid/content/Context;Lcom/samsung/android/scloud/auth/AuthManager;Ljava/lang/String;ILcom/samsung/android/scloud/sync/model/IModel;Ljava/lang/String;ILjava/lang/String;Lcom/samsung/android/scloud/framework/network/NetworkUtil$StringResponseHandler;)V
    :try_end_0
    .catch Lcom/samsung/android/scloud/framework/PDMException; {:try_start_0 .. :try_end_0} :catch_0

    .line 566
    :goto_0
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mAuth:Lcom/samsung/android/scloud/auth/AuthManager;

    const/4 v2, 0x0

    aget-object v12, v20, v2

    const/4 v2, 0x0

    aget v2, v21, v2

    add-int/lit8 v13, v2, -0x1

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mCtid:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    invoke-interface {v2}, Lcom/samsung/android/scloud/sync/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v15

    new-instance v16, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$18;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$18;-><init>(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)V

    invoke-static/range {v10 .. v16}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceManager;->transactionEnd(Landroid/content/Context;Lcom/samsung/android/scloud/auth/AuthManager;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/samsung/android/scloud/framework/network/NetworkUtil$JSONResponseHandler;)V

    .line 576
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mListener:Lcom/samsung/android/scloud/framework/IStatusListener;

    const/4 v3, 0x4

    const-string v4, "ORSServiceControl"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "delete from server complete !! key : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getSyncKey()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v3, v4, v5}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 577
    const/4 v2, 0x1

    return v2

    .line 557
    :catch_0
    move-exception v19

    .line 558
    .local v19, "e":Lcom/samsung/android/scloud/framework/PDMException;
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/scloud/framework/PDMException;->getMessage()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/scloud/framework/PDMException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const-string v3, "\"rcode\":31002"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/scloud/framework/PDMException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const-string v3, "\"rcode\":32006"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 559
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mListener:Lcom/samsung/android/scloud/framework/IStatusListener;

    const/4 v3, 0x0

    const-string v4, "ORSServiceControl"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    invoke-interface {v6}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mCtid:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") : Already deleted folder. key : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getSyncKey()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v3, v4, v5}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 560
    const/4 v2, 0x0

    aget v3, v21, v2

    add-int/lit8 v3, v3, -0x1

    aput v3, v21, v2

    goto/16 :goto_0

    .line 562
    :cond_3
    throw v19

    .line 517
    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method public downloadItem(Lcom/samsung/android/scloud/sync/data/SyncItem;)Z
    .locals 25
    .param p1, "item"    # Lcom/samsung/android/scloud/sync/data/SyncItem;

    .prologue
    .line 367
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mRevisionMap:Ljava/util/Map;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getSyncKey()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 368
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mRevisionMap:Ljava/util/Map;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getSyncKey()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x2

    new-array v5, v5, [I

    fill-array-data v5, :array_0

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 370
    :cond_0
    new-instance v21, Ljava/util/HashMap;

    invoke-direct/range {v21 .. v21}, Ljava/util/HashMap;-><init>()V

    .line 371
    .local v21, "serverFileMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;[J>;"
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mAuth:Lcom/samsung/android/scloud/auth/AuthManager;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mCtid:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    move-object/from16 v0, p1

    invoke-interface {v8, v0}, Lcom/samsung/android/scloud/sync/model/IModel;->getServerFilePathPrefix(Lcom/samsung/android/scloud/sync/data/SyncItem;)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mRevisionMap:Ljava/util/Map;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getSyncKey()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [I

    const/4 v9, 0x0

    aget v8, v8, v9

    const-string v9, ""

    const/16 v10, 0x7d0

    const-wide/16 v11, 0x0

    new-instance v13, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$12;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v21

    invoke-direct {v13, v0, v1, v2}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$12;-><init>(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;Lcom/samsung/android/scloud/sync/data/SyncItem;Ljava/util/Map;)V

    invoke-static/range {v3 .. v13}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceManager;->listDirectory(Landroid/content/Context;Lcom/samsung/android/scloud/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/sync/model/IModel;Ljava/lang/String;ILjava/lang/String;IJLcom/samsung/android/scloud/framework/network/NetworkUtil$JSONResponseHandler;)V

    .line 418
    const/16 v16, 0x0

    .line 419
    .local v16, "attachments":Lcom/samsung/android/scloud/sync/data/Attachments;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getLocalId()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 420
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    invoke-interface {v3}, Lcom/samsung/android/scloud/sync/model/IModel;->getOEMControl()Lcom/samsung/android/scloud/sync/oem/IOEMControl;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getLocalId()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v4, v5, v6}, Lcom/samsung/android/scloud/sync/oem/IOEMControl;->getAttachmentFileInfo(Landroid/content/Context;Lcom/samsung/android/scloud/sync/model/IModel;Ljava/lang/String;)Lcom/samsung/android/scloud/sync/data/Attachments;

    move-result-object v16

    .line 422
    :cond_1
    new-instance v24, Ljava/util/ArrayList;

    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    .line 423
    .local v24, "toDownloadFileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    .line 425
    .local v23, "toDeleteFileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v16, :cond_6

    .line 426
    invoke-interface/range {v21 .. v21}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v19

    .local v19, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/util/Map$Entry;

    .line 427
    .local v17, "file":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;[J>;"
    invoke-interface/range {v17 .. v17}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lcom/samsung/android/scloud/sync/data/Attachments;->hasFile(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface/range {v17 .. v17}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lcom/samsung/android/scloud/sync/data/Attachments;->getTimeStampOf(Ljava/lang/String;)J

    move-result-wide v4

    invoke-interface/range {v17 .. v17}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [J

    const/4 v6, 0x0

    aget-wide v8, v3, v6

    cmp-long v3, v4, v8

    if-eqz v3, :cond_2

    .line 428
    :cond_3
    invoke-interface/range {v17 .. v17}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    move-object/from16 v0, v24

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 431
    .end local v17    # "file":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;[J>;"
    :cond_4
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/scloud/sync/data/Attachments;->howManyFiles()I

    move-result v22

    .line 433
    .local v22, "size":I
    const/16 v18, 0x0

    .local v18, "i":I
    :goto_1
    move/from16 v0, v18

    move/from16 v1, v22

    if-ge v0, v1, :cond_7

    .line 434
    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/sync/data/Attachments;->getFileNameAt(I)Ljava/lang/String;

    move-result-object v7

    .line 435
    .local v7, "fileName":Ljava/lang/String;
    move-object/from16 v0, v21

    invoke-interface {v0, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 436
    move-object/from16 v0, v23

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 433
    :cond_5
    add-int/lit8 v18, v18, 0x1

    goto :goto_1

    .line 440
    .end local v7    # "fileName":Ljava/lang/String;
    .end local v18    # "i":I
    .end local v19    # "i$":Ljava/util/Iterator;
    .end local v22    # "size":I
    :cond_6
    invoke-interface/range {v21 .. v21}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v19

    .restart local v19    # "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/util/Map$Entry;

    .line 441
    .restart local v17    # "file":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;[J>;"
    invoke-interface/range {v17 .. v17}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    move-object/from16 v0, v24

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 447
    .end local v17    # "file":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;[J>;"
    :cond_7
    invoke-interface/range {v24 .. v24}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :goto_3
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 448
    .restart local v7    # "fileName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mAuth:Lcom/samsung/android/scloud/auth/AuthManager;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    move-object/from16 v0, v21

    invoke-interface {v0, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [J

    const/4 v8, 0x1

    aget-wide v8, v6, v8

    long-to-int v8, v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mCtid:Ljava/lang/String;

    new-instance v10, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$13;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v10, v0, v7, v1}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$13;-><init>(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;Ljava/lang/String;Lcom/samsung/android/scloud/sync/data/SyncItem;)V

    move-object/from16 v6, p1

    invoke-static/range {v3 .. v10}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceManager;->downloadFile(Landroid/content/Context;Lcom/samsung/android/scloud/auth/AuthManager;Lcom/samsung/android/scloud/sync/model/IModel;Lcom/samsung/android/scloud/sync/data/SyncItem;Ljava/lang/String;ILjava/lang/String;Lcom/samsung/android/scloud/framework/network/NetworkUtil$FileResponseHandler;)V

    goto :goto_3

    .line 475
    .end local v7    # "fileName":Ljava/lang/String;
    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mListener:Lcom/samsung/android/scloud/framework/IStatusListener;

    const/4 v4, 0x4

    const-string v5, "ORSServiceControl"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "downsync attachment file download finished - "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mCtid:Ljava/lang/String;

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " , cnt : "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface/range {v24 .. v24}, Ljava/util/List;->size()I

    move-result v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v4, v5, v6}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 477
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mAuth:Lcom/samsung/android/scloud/auth/AuthManager;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    const-string v12, "content.sync"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mRevisionMap:Ljava/util/Map;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getSyncKey()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [I

    const/4 v4, 0x1

    aget v13, v3, v4

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mCtid:Ljava/lang/String;

    new-instance v15, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$14;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v15, v0, v1}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$14;-><init>(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;Lcom/samsung/android/scloud/sync/data/SyncItem;)V

    move-object/from16 v11, p1

    invoke-static/range {v8 .. v15}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceManager;->downloadFile(Landroid/content/Context;Lcom/samsung/android/scloud/auth/AuthManager;Lcom/samsung/android/scloud/sync/model/IModel;Lcom/samsung/android/scloud/sync/data/SyncItem;Ljava/lang/String;ILjava/lang/String;Lcom/samsung/android/scloud/framework/network/NetworkUtil$FileResponseHandler;)V

    .line 503
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mListener:Lcom/samsung/android/scloud/framework/IStatusListener;

    const/4 v4, 0x4

    const-string v5, "ORSServiceControl"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "downsync content data file download finished - "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mCtid:Ljava/lang/String;

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " , key : "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getSyncKey()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v4, v5, v6}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 505
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    invoke-interface {v3}, Lcom/samsung/android/scloud/sync/model/IModel;->getOEMControl()Lcom/samsung/android/scloud/sync/oem/IOEMControl;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mContext:Landroid/content/Context;

    move-object/from16 v0, p1

    invoke-interface {v3, v4, v0}, Lcom/samsung/android/scloud/sync/model/IModel;->getLocalFilePathPrefix(Landroid/content/Context;Lcom/samsung/android/scloud/sync/data/SyncItem;)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v11, p1

    move-object/from16 v12, v24

    move-object/from16 v13, v23

    invoke-interface/range {v8 .. v14}, Lcom/samsung/android/scloud/sync/oem/IOEMControl;->updateLocal(Landroid/content/Context;Lcom/samsung/android/scloud/sync/model/IModel;Lcom/samsung/android/scloud/sync/data/SyncItem;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 507
    .local v20, "localId":Ljava/lang/String;
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/sync/data/SyncItem;->setLocalId(Ljava/lang/String;)V

    .line 508
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mListener:Lcom/samsung/android/scloud/framework/IStatusListener;

    const/4 v4, 0x4

    const-string v5, "ORSServiceControl"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "set localId - "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v4, v5, v6}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 510
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    invoke-interface {v3}, Lcom/samsung/android/scloud/sync/model/IModel;->getOEMControl()Lcom/samsung/android/scloud/sync/oem/IOEMControl;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    const/16 v6, 0x12d

    move-object/from16 v0, p1

    invoke-interface {v3, v4, v5, v0, v6}, Lcom/samsung/android/scloud/sync/oem/IOEMControl;->complete(Landroid/content/Context;Lcom/samsung/android/scloud/sync/model/IModel;Lcom/samsung/android/scloud/sync/data/SyncItem;I)Z

    move-result v3

    return v3

    .line 368
    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method public getKeys(Ljava/lang/String;JLjava/util/HashMap;)Ljava/lang/String;
    .locals 14
    .param p1, "startKey"    # Ljava/lang/String;
    .param p2, "lastSyncTime"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/scloud/sync/data/SyncItem;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 100
    .local p4, "outServerChanges":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/samsung/android/scloud/sync/data/SyncItem;>;"
    const/4 v1, 0x1

    :try_start_0
    new-array v13, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, ""

    aput-object v2, v13, v1

    .line 101
    .local v13, "nextKey":[Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mAuth:Lcom/samsung/android/scloud/auth/AuthManager;

    iget-object v3, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mCtid:Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    invoke-interface {v6}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/16 v8, 0x7d0

    new-instance v11, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$2;

    move-object/from16 v0, p4

    invoke-direct {v11, p0, v13, v0}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$2;-><init>(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;[Ljava/lang/String;Ljava/util/HashMap;)V

    move-object v7, p1

    move-wide/from16 v9, p2

    invoke-static/range {v1 .. v11}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceManager;->listDirectory(Landroid/content/Context;Lcom/samsung/android/scloud/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/sync/model/IModel;Ljava/lang/String;ILjava/lang/String;IJLcom/samsung/android/scloud/framework/network/NetworkUtil$JSONResponseHandler;)V

    .line 136
    const/4 v1, 0x0

    aget-object v1, v13, v1
    :try_end_0
    .catch Lcom/samsung/android/scloud/framework/PDMException; {:try_start_0 .. :try_end_0} :catch_0

    .line 140
    .end local v13    # "nextKey":[Ljava/lang/String;
    :goto_0
    return-object v1

    .line 137
    :catch_0
    move-exception v12

    .line 138
    .local v12, "e":Lcom/samsung/android/scloud/framework/PDMException;
    invoke-virtual {v12}, Lcom/samsung/android/scloud/framework/PDMException;->getMessage()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v12}, Lcom/samsung/android/scloud/framework/PDMException;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "\"rcode\":31002"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v12}, Lcom/samsung/android/scloud/framework/PDMException;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "\"rcode\":32006"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 139
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mListener:Lcom/samsung/android/scloud/framework/IStatusListener;

    const/4 v2, 0x4

    const-string v3, "ORSServiceControl"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    invoke-interface {v5}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mCtid:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") : First sync on this account"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v2, v3, v4}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 140
    const/4 v1, 0x0

    goto :goto_0

    .line 141
    :cond_1
    throw v12
.end method

.method public getServerTimestamp()J
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 73
    iget-object v1, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mListener:Lcom/samsung/android/scloud/framework/IStatusListener;

    const/4 v2, 0x4

    const-string v3, "ORSServiceControl"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "checkServerTimestamp - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mCtid:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v2, v3, v4}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 75
    const/4 v1, 0x1

    new-array v0, v1, [J

    const-wide/16 v1, 0x0

    aput-wide v1, v0, v6

    .line 76
    .local v0, "serverTime":[J
    iget-object v1, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mAuth:Lcom/samsung/android/scloud/auth/AuthManager;

    iget-object v3, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    iget-object v4, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mCtid:Ljava/lang/String;

    new-instance v5, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$1;

    invoke-direct {v5, p0, v0}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$1;-><init>(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;[J)V

    invoke-static {v1, v2, v3, v4, v5}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceManager;->getTimestamp(Landroid/content/Context;Lcom/samsung/android/scloud/auth/AuthManager;Lcom/samsung/android/scloud/sync/model/IModel;Ljava/lang/String;Lcom/samsung/android/scloud/framework/network/NetworkUtil$JSONResponseHandler;)V

    .line 92
    aget-wide v1, v0, v6

    return-wide v1
.end method

.method public init(Landroid/content/Context;Lcom/samsung/android/scloud/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/framework/IStatusListener;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "auth"    # Lcom/samsung/android/scloud/auth/AuthManager;
    .param p3, "ctid"    # Ljava/lang/String;
    .param p4, "listener"    # Lcom/samsung/android/scloud/framework/IStatusListener;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mContext:Landroid/content/Context;

    .line 65
    iput-object p2, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mAuth:Lcom/samsung/android/scloud/auth/AuthManager;

    .line 66
    iput-object p3, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mCtid:Ljava/lang/String;

    .line 67
    iput-object p4, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mListener:Lcom/samsung/android/scloud/framework/IStatusListener;

    .line 68
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mRevisionMap:Ljava/util/Map;

    .line 69
    return-void
.end method

.method public uploadItem(Lcom/samsung/android/scloud/sync/data/SyncItem;)Z
    .locals 35
    .param p1, "item"    # Lcom/samsung/android/scloud/sync/data/SyncItem;

    .prologue
    .line 149
    const/4 v3, 0x1

    new-array v0, v3, [I

    move-object/from16 v34, v0

    const/4 v3, 0x0

    const/4 v4, 0x1

    aput v4, v34, v3

    .line 150
    .local v34, "txSeq":[I
    const/4 v3, 0x1

    new-array v0, v3, [Ljava/lang/String;

    move-object/from16 v33, v0

    const/4 v3, 0x0

    const-string v4, ""

    aput-object v4, v33, v3

    .line 152
    .local v33, "txKey":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mRevisionMap:Ljava/util/Map;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getSyncKey()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 153
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mRevisionMap:Ljava/util/Map;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getSyncKey()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x2

    new-array v5, v5, [I

    fill-array-data v5, :array_0

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 156
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mAuth:Lcom/samsung/android/scloud/auth/AuthManager;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mCtid:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    invoke-interface {v6}, Lcom/samsung/android/scloud/sync/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$3;

    move-object/from16 v0, p0

    move-object/from16 v1, v33

    invoke-direct {v7, v0, v1}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$3;-><init>(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;[Ljava/lang/String;)V

    invoke-static {v3, v4, v5, v6, v7}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceManager;->transactionStart(Landroid/content/Context;Lcom/samsung/android/scloud/auth/AuthManager;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/scloud/framework/network/NetworkUtil$JSONResponseHandler;)V

    .line 168
    new-instance v29, Ljava/util/HashMap;

    invoke-direct/range {v29 .. v29}, Ljava/util/HashMap;-><init>()V

    .line 169
    .local v29, "serverFileMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;[J>;"
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/scloud/sync/data/SyncItem;->isNew()Z

    move-result v3

    if-nez v3, :cond_6

    .line 171
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mAuth:Lcom/samsung/android/scloud/auth/AuthManager;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mCtid:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    move-object/from16 v0, p1

    invoke-interface {v7, v0}, Lcom/samsung/android/scloud/sync/model/IModel;->getServerFilePathPrefix(Lcom/samsung/android/scloud/sync/data/SyncItem;)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mRevisionMap:Ljava/util/Map;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getSyncKey()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v8, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [I

    const/4 v10, 0x0

    aget v8, v8, v10

    const-string v9, ""

    const/16 v10, 0x7d0

    const-wide/16 v11, 0x0

    new-instance v13, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$4;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v29

    invoke-direct {v13, v0, v1, v2}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$4;-><init>(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;Lcom/samsung/android/scloud/sync/data/SyncItem;Ljava/util/Map;)V

    invoke-static/range {v3 .. v13}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceManager;->listDirectory(Landroid/content/Context;Lcom/samsung/android/scloud/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/sync/model/IModel;Ljava/lang/String;ILjava/lang/String;IJLcom/samsung/android/scloud/framework/network/NetworkUtil$JSONResponseHandler;)V
    :try_end_0
    .catch Lcom/samsung/android/scloud/framework/PDMException; {:try_start_0 .. :try_end_0} :catch_0

    .line 229
    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    invoke-interface {v3}, Lcom/samsung/android/scloud/sync/model/IModel;->getOEMControl()Lcom/samsung/android/scloud/sync/oem/IOEMControl;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getLocalId()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v4, v5, v6}, Lcom/samsung/android/scloud/sync/oem/IOEMControl;->getAttachmentFileInfo(Landroid/content/Context;Lcom/samsung/android/scloud/sync/model/IModel;Ljava/lang/String;)Lcom/samsung/android/scloud/sync/data/Attachments;

    move-result-object v22

    .line 230
    .local v22, "attachments":Lcom/samsung/android/scloud/sync/data/Attachments;
    new-instance v24, Ljava/util/HashMap;

    invoke-direct/range {v24 .. v24}, Ljava/util/HashMap;-><init>()V

    .line 232
    .local v24, "fileMetaResultMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;[J>;"
    new-instance v32, Ljava/util/ArrayList;

    invoke-direct/range {v32 .. v32}, Ljava/util/ArrayList;-><init>()V

    .line 233
    .local v32, "toUploadFileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v31, Ljava/util/ArrayList;

    invoke-direct/range {v31 .. v31}, Ljava/util/ArrayList;-><init>()V

    .line 235
    .local v31, "toDeleteFileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v22, :cond_9

    .line 236
    invoke-virtual/range {v22 .. v22}, Lcom/samsung/android/scloud/sync/data/Attachments;->howManyFiles()I

    move-result v30

    .line 238
    .local v30, "size":I
    const/16 v26, 0x0

    .local v26, "i":I
    :goto_1
    move/from16 v0, v26

    move/from16 v1, v30

    if-ge v0, v1, :cond_7

    .line 239
    move-object/from16 v0, v22

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/sync/data/Attachments;->getFileNameAt(I)Ljava/lang/String;

    move-result-object v25

    .line 240
    .local v25, "fileName":Ljava/lang/String;
    const/16 v28, 0x0

    .line 241
    .local v28, "revision":I
    move-object/from16 v0, v29

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    move-object/from16 v0, v29

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [J

    const/4 v4, 0x1

    aget-wide v3, v3, v4

    long-to-int v0, v3

    move/from16 v28, v0

    .line 243
    :cond_1
    const/4 v3, 0x2

    new-array v3, v3, [J

    const/4 v4, 0x0

    move-object/from16 v0, v22

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/sync/data/Attachments;->getTimeStampAt(I)J

    move-result-wide v5

    aput-wide v5, v3, v4

    const/4 v4, 0x1

    move/from16 v0, v28

    int-to-long v5, v0

    aput-wide v5, v3, v4

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 245
    move-object/from16 v0, v29

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    move-object/from16 v0, v22

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/sync/data/Attachments;->getTimeStampOf(Ljava/lang/String;)J

    move-result-wide v4

    move-object/from16 v0, v29

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [J

    const/4 v6, 0x0

    aget-wide v6, v3, v6

    cmp-long v3, v4, v6

    if-eqz v3, :cond_3

    .line 246
    :cond_2
    move-object/from16 v0, v32

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 238
    :cond_3
    add-int/lit8 v26, v26, 0x1

    goto :goto_1

    .line 218
    .end local v22    # "attachments":Lcom/samsung/android/scloud/sync/data/Attachments;
    .end local v24    # "fileMetaResultMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;[J>;"
    .end local v25    # "fileName":Ljava/lang/String;
    .end local v26    # "i":I
    .end local v28    # "revision":I
    .end local v30    # "size":I
    .end local v31    # "toDeleteFileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v32    # "toUploadFileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catch_0
    move-exception v23

    .line 219
    .local v23, "e":Lcom/samsung/android/scloud/framework/PDMException;
    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/scloud/framework/PDMException;->getMessage()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_5

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/scloud/framework/PDMException;->getMessage()Ljava/lang/String;

    move-result-object v3

    const-string v4, "\"rcode\":31002"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/scloud/framework/PDMException;->getMessage()Ljava/lang/String;

    move-result-object v3

    const-string v4, "\"rcode\":32006"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 220
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mListener:Lcom/samsung/android/scloud/framework/IStatusListener;

    const/4 v4, 0x0

    const-string v5, "ORSServiceControl"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    invoke-interface {v7}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mCtid:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ") : No server item, it\'s new : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v4, v5, v6}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 222
    :cond_5
    throw v23

    .line 226
    .end local v23    # "e":Lcom/samsung/android/scloud/framework/PDMException;
    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mListener:Lcom/samsung/android/scloud/framework/IStatusListener;

    const/4 v4, 0x4

    const-string v5, "ORSServiceControl"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    invoke-interface {v7}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mCtid:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ") : New item from local. "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v4, v5, v6}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 249
    .restart local v22    # "attachments":Lcom/samsung/android/scloud/sync/data/Attachments;
    .restart local v24    # "fileMetaResultMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;[J>;"
    .restart local v26    # "i":I
    .restart local v30    # "size":I
    .restart local v31    # "toDeleteFileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v32    # "toUploadFileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_7
    invoke-interface/range {v29 .. v29}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v27

    .local v27, "i$":Ljava/util/Iterator;
    :cond_8
    :goto_2
    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/lang/String;

    .line 250
    .restart local v25    # "fileName":Ljava/lang/String;
    move-object/from16 v0, v22

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/sync/data/Attachments;->hasFile(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 251
    move-object/from16 v0, v31

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 254
    .end local v25    # "fileName":Ljava/lang/String;
    .end local v26    # "i":I
    .end local v27    # "i$":Ljava/util/Iterator;
    .end local v30    # "size":I
    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mListener:Lcom/samsung/android/scloud/framework/IStatusListener;

    const/4 v4, 0x4

    const-string v5, "ORSServiceControl"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    invoke-interface {v7}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mCtid:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ") : There is no attachement file in local. delete all files on server. cnt :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface/range {v29 .. v29}, Ljava/util/Map;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v4, v5, v6}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 255
    invoke-interface/range {v29 .. v29}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v27

    .restart local v27    # "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/lang/String;

    .line 256
    .restart local v25    # "fileName":Ljava/lang/String;
    move-object/from16 v0, v31

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 260
    .end local v25    # "fileName":Ljava/lang/String;
    :cond_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mListener:Lcom/samsung/android/scloud/framework/IStatusListener;

    const/4 v4, 0x4

    const-string v5, "ORSServiceControl"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    invoke-interface {v7}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mCtid:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ") : Attachment files - toUpload : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface/range {v32 .. v32}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", toDelete : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface/range {v31 .. v31}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v4, v5, v6}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 262
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    invoke-interface {v3}, Lcom/samsung/android/scloud/sync/model/IModel;->getOEMControl()Lcom/samsung/android/scloud/sync/oem/IOEMControl;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-interface {v3, v4, v5, v0, v1}, Lcom/samsung/android/scloud/sync/oem/IOEMControl;->getLocalChange(Landroid/content/Context;Lcom/samsung/android/scloud/sync/model/IModel;Lcom/samsung/android/scloud/sync/data/SyncItem;Lcom/samsung/android/scloud/sync/data/Attachments;)Z

    .line 264
    invoke-interface/range {v32 .. v32}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v27

    :goto_4
    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_b

    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 265
    .local v9, "file":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mListener:Lcom/samsung/android/scloud/framework/IStatusListener;

    const/4 v4, 0x4

    const-string v5, "ORSServiceControl"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "uploadFile attachment : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v4, v5, v6}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 267
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mAuth:Lcom/samsung/android/scloud/auth/AuthManager;

    const/4 v5, 0x0

    aget-object v5, v33, v5

    const/4 v7, 0x0

    aget v6, v34, v7

    add-int/lit8 v8, v6, 0x1

    aput v8, v34, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    invoke-virtual {v0, v9}, Lcom/samsung/android/scloud/sync/data/Attachments;->getTimeStampOf(Ljava/lang/String;)J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, ""

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, v24

    invoke-interface {v0, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [J

    const/4 v11, 0x1

    aget-wide v11, v8, v11

    long-to-int v11, v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mCtid:Ljava/lang/String;

    new-instance v13, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$5;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$5;-><init>(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)V

    new-instance v14, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$6;

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-direct {v14, v0, v9, v1}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$6;-><init>(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;Ljava/lang/String;Ljava/util/Map;)V

    move-object/from16 v8, p1

    invoke-static/range {v3 .. v14}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceManager;->uploadFile(Landroid/content/Context;Lcom/samsung/android/scloud/auth/AuthManager;Ljava/lang/String;ILcom/samsung/android/scloud/sync/model/IModel;Lcom/samsung/android/scloud/sync/data/SyncItem;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/samsung/android/scloud/framework/network/NetworkUtil$PDMProgressListener;Lcom/samsung/android/scloud/framework/network/NetworkUtil$JSONResponseHandler;)V

    goto :goto_4

    .line 293
    .end local v9    # "file":Ljava/lang/String;
    :cond_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mListener:Lcom/samsung/android/scloud/framework/IStatusListener;

    const/4 v4, 0x4

    const-string v5, "ORSServiceControl"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "uploadFile attachment file finished - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", cnt : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface/range {v32 .. v32}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v4, v5, v6}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 296
    invoke-interface/range {v31 .. v31}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v27

    :goto_5
    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 297
    .restart local v9    # "file":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mAuth:Lcom/samsung/android/scloud/auth/AuthManager;

    const/4 v5, 0x0

    aget-object v5, v33, v5

    const/4 v7, 0x0

    aget v6, v34, v7

    add-int/lit8 v8, v6, 0x1

    aput v8, v34, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    move-object/from16 v0, v29

    invoke-interface {v0, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [J

    const/4 v10, 0x1

    aget-wide v10, v8, v10

    long-to-int v10, v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mCtid:Ljava/lang/String;

    new-instance v12, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$7;

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-direct {v12, v0, v1, v9}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$7;-><init>(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;Ljava/util/Map;Ljava/lang/String;)V

    move-object/from16 v8, p1

    invoke-static/range {v3 .. v12}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceManager;->deleteFile(Landroid/content/Context;Lcom/samsung/android/scloud/auth/AuthManager;Ljava/lang/String;ILcom/samsung/android/scloud/sync/model/IModel;Lcom/samsung/android/scloud/sync/data/SyncItem;Ljava/lang/String;ILjava/lang/String;Lcom/samsung/android/scloud/framework/network/NetworkUtil$StringResponseHandler;)V

    goto :goto_5

    .line 313
    .end local v9    # "file":Ljava/lang/String;
    :cond_c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mListener:Lcom/samsung/android/scloud/framework/IStatusListener;

    const/4 v4, 0x4

    const-string v5, "ORSServiceControl"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "deleteFile attachment file finished - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", cnt : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface/range {v31 .. v31}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v4, v5, v6}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 316
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mAuth:Lcom/samsung/android/scloud/auth/AuthManager;

    const/4 v3, 0x0

    aget-object v12, v33, v3

    const/4 v3, 0x0

    aget v13, v34, v3

    add-int/lit8 v4, v13, 0x1

    aput v4, v34, v3

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    const-string v16, "content.sync"

    const/16 v17, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mRevisionMap:Ljava/util/Map;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getSyncKey()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [I

    const/4 v4, 0x1

    aget v18, v3, v4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mCtid:Ljava/lang/String;

    move-object/from16 v19, v0

    new-instance v20, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$8;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$8;-><init>(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)V

    new-instance v21, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$9;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$9;-><init>(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;Lcom/samsung/android/scloud/sync/data/SyncItem;)V

    move-object/from16 v15, p1

    invoke-static/range {v10 .. v21}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceManager;->uploadFile(Landroid/content/Context;Lcom/samsung/android/scloud/auth/AuthManager;Ljava/lang/String;ILcom/samsung/android/scloud/sync/model/IModel;Lcom/samsung/android/scloud/sync/data/SyncItem;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/samsung/android/scloud/framework/network/NetworkUtil$PDMProgressListener;Lcom/samsung/android/scloud/framework/network/NetworkUtil$JSONResponseHandler;)V

    .line 339
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mListener:Lcom/samsung/android/scloud/framework/IStatusListener;

    const/4 v4, 0x4

    const-string v5, "ORSServiceControl"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "uploadFile content data file finished - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v4, v5, v6}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 341
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mAuth:Lcom/samsung/android/scloud/auth/AuthManager;

    const/4 v3, 0x0

    aget-object v12, v33, v3

    const/4 v3, 0x0

    aget v13, v34, v3

    add-int/lit8 v4, v13, 0x1

    aput v4, v34, v3

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getTimeStamp()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mRevisionMap:Ljava/util/Map;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getSyncKey()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [I

    const/4 v4, 0x0

    aget v17, v3, v4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mCtid:Ljava/lang/String;

    move-object/from16 v18, v0

    new-instance v19, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$10;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$10;-><init>(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)V

    move-object/from16 v15, p1

    invoke-static/range {v10 .. v19}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceManager;->updateTag(Landroid/content/Context;Lcom/samsung/android/scloud/auth/AuthManager;Ljava/lang/String;ILcom/samsung/android/scloud/sync/model/IModel;Lcom/samsung/android/scloud/sync/data/SyncItem;Ljava/lang/String;ILjava/lang/String;Lcom/samsung/android/scloud/framework/network/NetworkUtil$StringResponseHandler;)V

    .line 352
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mAuth:Lcom/samsung/android/scloud/auth/AuthManager;

    const/4 v3, 0x0

    aget-object v12, v33, v3

    const/4 v3, 0x0

    aget v3, v34, v3

    add-int/lit8 v13, v3, -0x1

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mCtid:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    invoke-interface {v3}, Lcom/samsung/android/scloud/sync/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v15

    new-instance v16, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$11;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$11;-><init>(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)V

    invoke-static/range {v10 .. v16}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceManager;->transactionEnd(Landroid/content/Context;Lcom/samsung/android/scloud/auth/AuthManager;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/samsung/android/scloud/framework/network/NetworkUtil$JSONResponseHandler;)V

    .line 362
    const/4 v3, 0x1

    return v3

    .line 153
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method
