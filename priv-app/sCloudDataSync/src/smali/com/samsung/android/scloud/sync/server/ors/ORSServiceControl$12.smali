.class Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$12;
.super Lcom/samsung/android/scloud/framework/network/NetworkUtil$JSONResponseHandler;
.source "ORSServiceControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->downloadItem(Lcom/samsung/android/scloud/sync/data/SyncItem;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

.field final synthetic val$item:Lcom/samsung/android/scloud/sync/data/SyncItem;

.field final synthetic val$serverFileMap:Ljava/util/Map;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;Lcom/samsung/android/scloud/sync/data/SyncItem;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 373
    iput-object p1, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$12;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    iput-object p2, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$12;->val$item:Lcom/samsung/android/scloud/sync/data/SyncItem;

    iput-object p3, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$12;->val$serverFileMap:Ljava/util/Map;

    invoke-direct {p0}, Lcom/samsung/android/scloud/framework/network/NetworkUtil$JSONResponseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleJSONResponse(Lorg/json/JSONObject;)V
    .locals 23
    .param p1, "data"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 376
    const-string v17, "path"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 377
    .local v5, "folder":Ljava/lang/String;
    const-string v17, "revision"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v7

    .line 378
    .local v7, "folderRevision":I
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    .line 380
    .local v6, "folderLen":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$12;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    move-object/from16 v17, v0

    # getter for: Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mRevisionMap:Ljava/util/Map;
    invoke-static/range {v17 .. v17}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->access$400(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Ljava/util/Map;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$12;->val$item:Lcom/samsung/android/scloud/sync/data/SyncItem;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getSyncKey()Ljava/lang/String;

    move-result-object v18

    invoke-interface/range {v17 .. v18}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, [I

    const/16 v18, 0x0

    aget v17, v17, v18

    move/from16 v0, v17

    if-eq v0, v7, :cond_0

    .line 381
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$12;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    move-object/from16 v17, v0

    # getter for: Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mListener:Lcom/samsung/android/scloud/framework/IStatusListener;
    invoke-static/range {v17 .. v17}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->access$100(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Lcom/samsung/android/scloud/framework/IStatusListener;

    move-result-object v18

    const/16 v19, 0x0

    const-string v20, "ORSServiceControl"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "["

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$12;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->access$300(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Lcom/samsung/android/scloud/sync/model/IModel;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v21, "]("

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$12;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mCtid:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->access$000(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v21, ") : ServerItem , revision update "

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$12;->val$item:Lcom/samsung/android/scloud/sync/data/SyncItem;

    move-object/from16 v21, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v21, ", rev : "

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$12;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    move-object/from16 v17, v0

    # getter for: Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mRevisionMap:Ljava/util/Map;
    invoke-static/range {v17 .. v17}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->access$400(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Ljava/util/Map;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$12;->val$item:Lcom/samsung/android/scloud/sync/data/SyncItem;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getSyncKey()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, [I

    const/16 v22, 0x0

    aget v17, v17, v22

    move-object/from16 v0, v21

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v21, "->"

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v18

    move/from16 v1, v19

    move-object/from16 v2, v20

    move-object/from16 v3, v17

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 382
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$12;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    move-object/from16 v17, v0

    # getter for: Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mRevisionMap:Ljava/util/Map;
    invoke-static/range {v17 .. v17}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->access$400(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Ljava/util/Map;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$12;->val$item:Lcom/samsung/android/scloud/sync/data/SyncItem;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getSyncKey()Ljava/lang/String;

    move-result-object v18

    invoke-interface/range {v17 .. v18}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, [I

    const/16 v18, 0x0

    aput v7, v17, v18

    .line 385
    :cond_0
    const-string v17, "children"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v11

    .line 387
    .local v11, "list":Lorg/json/JSONArray;
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    invoke-virtual {v11}, Lorg/json/JSONArray;->length()I

    move-result v17

    move/from16 v0, v17

    if-ge v8, v0, :cond_3

    .line 388
    invoke-virtual {v11, v8}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v9

    .line 390
    .local v9, "json":Lorg/json/JSONObject;
    :try_start_0
    const-string v17, "path"

    move-object/from16 v0, v17

    invoke-virtual {v9, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 391
    .local v12, "name":Ljava/lang/String;
    invoke-virtual {v5, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_1

    .line 392
    add-int/lit8 v17, v6, 0x1

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v12

    .line 393
    const-string v17, "revision"

    move-object/from16 v0, v17

    invoke-virtual {v9, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v14

    .line 395
    .local v14, "revision":I
    const-string v17, "content.sync"

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_2

    .line 396
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$12;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    move-object/from16 v17, v0

    # getter for: Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mListener:Lcom/samsung/android/scloud/framework/IStatusListener;
    invoke-static/range {v17 .. v17}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->access$100(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Lcom/samsung/android/scloud/framework/IStatusListener;

    move-result-object v17

    const/16 v18, 0x4

    const-string v19, "ORSServiceControl"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "["

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$12;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->access$300(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Lcom/samsung/android/scloud/sync/model/IModel;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "]("

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$12;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mCtid:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->access$000(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ") : ServerItem , Content file : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-interface/range {v17 .. v20}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 397
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$12;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    move-object/from16 v17, v0

    # getter for: Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mRevisionMap:Ljava/util/Map;
    invoke-static/range {v17 .. v17}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->access$400(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Ljava/util/Map;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$12;->val$item:Lcom/samsung/android/scloud/sync/data/SyncItem;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getSyncKey()Ljava/lang/String;

    move-result-object v18

    invoke-interface/range {v17 .. v18}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, [I

    const/16 v18, 0x1

    aput v14, v17, v18

    .line 387
    .end local v12    # "name":Ljava/lang/String;
    .end local v14    # "revision":I
    :cond_1
    :goto_1
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_0

    .line 399
    .restart local v12    # "name":Ljava/lang/String;
    .restart local v14    # "revision":I
    :cond_2
    const-string v17, "tag"

    move-object/from16 v0, v17

    invoke-virtual {v9, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v15

    .line 400
    .local v15, "timestamp":J
    const-string v17, "deleted"

    move-object/from16 v0, v17

    invoke-virtual {v9, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v4

    .line 402
    .local v4, "deleted":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$12;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    move-object/from16 v17, v0

    # getter for: Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mListener:Lcom/samsung/android/scloud/framework/IStatusListener;
    invoke-static/range {v17 .. v17}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->access$100(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Lcom/samsung/android/scloud/framework/IStatusListener;

    move-result-object v17

    const/16 v18, 0x4

    const-string v19, "ORSServiceControl"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "["

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$12;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->access$300(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Lcom/samsung/android/scloud/sync/model/IModel;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "]("

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$12;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mCtid:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->access$000(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ") : ServerItem , Attachment file : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-interface/range {v17 .. v20}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 403
    if-nez v4, :cond_1

    .line 404
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$12;->val$serverFileMap:Ljava/util/Map;

    move-object/from16 v17, v0

    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v0, v0, [J

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aput-wide v15, v18, v19

    const/16 v19, 0x1

    int-to-long v0, v14

    move-wide/from16 v20, v0

    aput-wide v20, v18, v19

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-interface {v0, v12, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_1

    .line 408
    .end local v4    # "deleted":Z
    .end local v12    # "name":Ljava/lang/String;
    .end local v14    # "revision":I
    .end local v15    # "timestamp":J
    :catch_0
    move-exception v10

    .line 409
    .local v10, "jsone":Lorg/json/JSONException;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$12;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    move-object/from16 v17, v0

    # getter for: Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mListener:Lcom/samsung/android/scloud/framework/IStatusListener;
    invoke-static/range {v17 .. v17}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->access$100(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Lcom/samsung/android/scloud/framework/IStatusListener;

    move-result-object v17

    const/16 v18, 0x0

    const-string v19, "ORSServiceControl"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "["

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$12;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->access$300(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Lcom/samsung/android/scloud/sync/model/IModel;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "]("

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$12;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mCtid:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->access$000(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ") : ServerItem , invalid value : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-interface/range {v17 .. v20}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 410
    .end local v10    # "jsone":Lorg/json/JSONException;
    :catch_1
    move-exception v13

    .line 411
    .local v13, "nfe":Ljava/lang/NumberFormatException;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$12;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    move-object/from16 v17, v0

    # getter for: Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mListener:Lcom/samsung/android/scloud/framework/IStatusListener;
    invoke-static/range {v17 .. v17}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->access$100(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Lcom/samsung/android/scloud/framework/IStatusListener;

    move-result-object v17

    const/16 v18, 0x0

    const-string v19, "ORSServiceControl"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "["

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$12;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->access$300(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Lcom/samsung/android/scloud/sync/model/IModel;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "]("

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$12;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mCtid:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->access$000(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ") : ServerItem , invalid value : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-interface/range {v17 .. v20}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 414
    .end local v9    # "json":Lorg/json/JSONObject;
    .end local v13    # "nfe":Ljava/lang/NumberFormatException;
    :cond_3
    return-void
.end method
