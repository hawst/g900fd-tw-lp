.class Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$3;
.super Ljava/lang/Object;
.source "SNoteServiceControl.java"

# interfaces
.implements Lcom/samsung/android/scloud/framework/network/NetworkUtil$PDMProgressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->uploadItem(Lcom/samsung/android/scloud/sync/data/SyncItem;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;)V
    .locals 0

    .prologue
    .line 141
    iput-object p1, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$3;->this$0:Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public transferred(JJ)V
    .locals 5
    .param p1, "now"    # J
    .param p3, "total"    # J

    .prologue
    .line 145
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$3;->this$0:Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;

    # getter for: Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mListener:Lcom/samsung/android/scloud/framework/IStatusListener;
    invoke-static {v0}, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->access$100(Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;)Lcom/samsung/android/scloud/framework/IStatusListener;

    move-result-object v0

    const/4 v1, 0x4

    const-string v2, "SNoteServiceControl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "uploadFile attachment transferred : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 146
    return-void
.end method
