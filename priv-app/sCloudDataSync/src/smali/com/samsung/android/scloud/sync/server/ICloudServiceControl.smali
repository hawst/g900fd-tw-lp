.class public interface abstract Lcom/samsung/android/scloud/sync/server/ICloudServiceControl;
.super Ljava/lang/Object;
.source "ICloudServiceControl.java"


# virtual methods
.method public abstract deleteItem(Lcom/samsung/android/scloud/sync/data/SyncItem;)Z
.end method

.method public abstract downloadItem(Lcom/samsung/android/scloud/sync/data/SyncItem;)Z
.end method

.method public abstract getKeys(Ljava/lang/String;JLjava/util/HashMap;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/scloud/sync/data/SyncItem;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation
.end method

.method public abstract getServerTimestamp()J
.end method

.method public abstract init(Landroid/content/Context;Lcom/samsung/android/scloud/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/framework/IStatusListener;)V
.end method

.method public abstract uploadItem(Lcom/samsung/android/scloud/sync/data/SyncItem;)Z
.end method
