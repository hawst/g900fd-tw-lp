.class public interface abstract Lcom/samsung/android/scloud/sync/common/SyncConstants$Status;
.super Ljava/lang/Object;
.source "SyncConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/sync/common/SyncConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Status"
.end annotation


# static fields
.field public static final OPERATION_FINISHED:I = 0xcd

.field public static final OPERATION_PAUSED:I = 0xcb

.field public static final OPERATION_PROGRESS:I = 0xca

.field public static final OPERATION_RESUMED:I = 0xcc

.field public static final OPERATION_START:I = 0xc9
