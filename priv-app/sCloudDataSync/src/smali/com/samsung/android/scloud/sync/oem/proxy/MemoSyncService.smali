.class public Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;
.super Landroid/app/Service;
.source "MemoSyncService.java"


# static fields
.field private static final AUTHORITY:Ljava/lang/String; = "com.samsung.android.memo"

.field private static final CATEGORY_CONTENT_URI:Landroid/net/Uri;

.field private static final MEMO_CONTENT_URI:Landroid/net/Uri;

.field private static final TAG:Ljava/lang/String; = "MEMO-SYNC"


# instance fields
.field private isCanceled:Z

.field private final mBinder:Lcom/sec/android/sCloudSync/IDataSync/IDataSync$Stub;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 42
    const-string v0, "content://com.samsung.android.memo/memo"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;->MEMO_CONTENT_URI:Landroid/net/Uri;

    .line 44
    const-string v0, "content://com.samsung.android.memo/category"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;->CATEGORY_CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 65
    new-instance v0, Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService$1;-><init>(Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;)V

    iput-object v0, p0, Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;->mBinder:Lcom/sec/android/sCloudSync/IDataSync/IDataSync$Stub;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;->isCanceled:Z

    return v0
.end method

.method static synthetic access$002(Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;
    .param p1, "x1"    # Z

    .prologue
    .line 38
    iput-boolean p1, p0, Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;->isCanceled:Z

    return p1
.end method

.method static synthetic access$100()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;->MEMO_CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;Landroid/content/Context;Landroid/net/Uri;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Landroid/net/Uri;

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;->isFirstSync(Landroid/content/Context;Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$300()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;->CATEGORY_CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method private isFirstSync(Landroid/content/Context;Landroid/net/Uri;)Z
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 119
    const/4 v6, 0x0

    .line 121
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    const-string v3, "sync2 is not null"

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 122
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 123
    const-string v0, "MEMO-SYNC"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isFirstSync false, uri : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", count : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 124
    const/4 v0, 0x0

    .line 129
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 131
    :cond_0
    :goto_0
    return v0

    .line 129
    :cond_1
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 131
    :cond_2
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 126
    :catch_0
    move-exception v7

    .line 127
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v0, "MEMO-SYNC"

    const-string v1, "isFirstSync err"

    invoke-static {v0, v1, v7}, Lcom/samsung/android/scloud/framework/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 129
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;->mBinder:Lcom/sec/android/sCloudSync/IDataSync/IDataSync$Stub;

    return-object v0
.end method

.method public onCreate()V
    .locals 0

    .prologue
    .line 50
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 51
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 62
    const-string v0, "MEMO-SYNC"

    const-string v1, "SYNC SERVICE END"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/framework/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    return-void
.end method
