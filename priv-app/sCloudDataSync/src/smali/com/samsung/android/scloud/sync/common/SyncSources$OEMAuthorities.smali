.class public interface abstract Lcom/samsung/android/scloud/sync/common/SyncSources$OEMAuthorities;
.super Ljava/lang/Object;
.source "SyncSources.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/sync/common/SyncSources;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OEMAuthorities"
.end annotation


# static fields
.field public static final CATEGORY_OEM_CONTENT_URI:Landroid/net/Uri;

.field public static final MEMO_OEM_AUTHORITY:Ljava/lang/String; = "com.samsung.android.memo"

.field public static final MEMO_OEM_CONTENT_URI:Landroid/net/Uri;

.field public static final READINGLIST_OEM_AUTHORITY:Ljava/lang/String; = "com.sec.android.app.sbrowser"

.field public static final READINGLIST_OEM_CONTENT_URI:Landroid/net/Uri;

.field public static final SNOTE4_AUTHORITY:Ljava/lang/String; = "com.samsung.android.snoteprovider4"

.field public static final SNOTE4_CONTENT_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-string v0, "content://com.samsung.android.memo/memo"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/scloud/sync/common/SyncSources$OEMAuthorities;->MEMO_OEM_CONTENT_URI:Landroid/net/Uri;

    .line 19
    const-string v0, "content://com.samsung.android.memo/category"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/scloud/sync/common/SyncSources$OEMAuthorities;->CATEGORY_OEM_CONTENT_URI:Landroid/net/Uri;

    .line 22
    const-string v0, "content://com.sec.android.app.sbrowser/readinglist"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/scloud/sync/common/SyncSources$OEMAuthorities;->READINGLIST_OEM_CONTENT_URI:Landroid/net/Uri;

    .line 25
    const-string v0, "content://com.samsung.android.snoteprovider4"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/scloud/sync/common/SyncSources$OEMAuthorities;->SNOTE4_CONTENT_URI:Landroid/net/Uri;

    return-void
.end method
