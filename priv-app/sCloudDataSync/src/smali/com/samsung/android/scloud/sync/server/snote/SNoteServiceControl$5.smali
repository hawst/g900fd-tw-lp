.class Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$5;
.super Ljava/lang/Object;
.source "SNoteServiceControl.java"

# interfaces
.implements Lcom/samsung/android/scloud/framework/network/NetworkUtil$FileResponseHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->downloadItem(Lcom/samsung/android/scloud/sync/data/SyncItem;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;

.field final synthetic val$item:Lcom/samsung/android/scloud/sync/data/SyncItem;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;Lcom/samsung/android/scloud/sync/data/SyncItem;)V
    .locals 0

    .prologue
    .line 193
    iput-object p1, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$5;->this$0:Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;

    iput-object p2, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$5;->val$item:Lcom/samsung/android/scloud/sync/data/SyncItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleResponse(Lorg/apache/http/Header;Ljava/lang/String;JLjava/io/InputStream;)V
    .locals 12
    .param p1, "header"    # Lorg/apache/http/Header;
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "size"    # J
    .param p5, "stream"    # Ljava/io/InputStream;

    .prologue
    .line 197
    iget-object v1, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$5;->this$0:Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;

    # getter for: Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mListener:Lcom/samsung/android/scloud/framework/IStatusListener;
    invoke-static {v1}, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->access$100(Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;)Lcom/samsung/android/scloud/framework/IStatusListener;

    move-result-object v1

    const/4 v2, 0x4

    const-string v3, "SNoteServiceControl"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "downloadFile Finished - "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v6, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$5;->this$0:Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;

    # getter for: Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mCtid:Ljava/lang/String;
    invoke-static {v6}, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->access$000(Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v6, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$5;->val$item:Lcom/samsung/android/scloud/sync/data/SyncItem;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v2, v3, v4}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 200
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$5;->this$0:Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;

    # getter for: Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;
    invoke-static {v1}, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->access$300(Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;)Lcom/samsung/android/scloud/sync/model/IModel;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$5;->this$0:Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;

    # getter for: Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->access$200(Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$5;->val$item:Lcom/samsung/android/scloud/sync/data/SyncItem;

    invoke-interface {v1, v2, v3}, Lcom/samsung/android/scloud/sync/model/IModel;->getLocalFilePathPrefix(Landroid/content/Context;Lcom/samsung/android/scloud/sync/data/SyncItem;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p5

    invoke-static {p1, v0, v1}, Lcom/samsung/android/scloud/sync/server/snote/MFDownloadParser;->parseAndDownload(Lorg/apache/http/Header;Ljava/io/InputStream;Ljava/lang/String;)Ljava/util/List;

    move-result-object v5

    .line 201
    .local v5, "toDownloadFileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$5;->this$0:Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;

    # getter for: Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;
    invoke-static {v1}, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->access$300(Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;)Lcom/samsung/android/scloud/sync/model/IModel;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/android/scloud/sync/model/IModel;->getOEMControl()Lcom/samsung/android/scloud/sync/oem/IOEMControl;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$5;->this$0:Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;

    # getter for: Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->access$200(Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$5;->this$0:Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;

    # getter for: Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;
    invoke-static {v3}, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->access$300(Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;)Lcom/samsung/android/scloud/sync/model/IModel;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$5;->val$item:Lcom/samsung/android/scloud/sync/data/SyncItem;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$5;->this$0:Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;

    # getter for: Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;
    invoke-static {v7}, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->access$300(Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;)Lcom/samsung/android/scloud/sync/model/IModel;

    move-result-object v7

    iget-object v10, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$5;->this$0:Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;

    # getter for: Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mContext:Landroid/content/Context;
    invoke-static {v10}, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->access$200(Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;)Landroid/content/Context;

    move-result-object v10

    iget-object v11, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$5;->val$item:Lcom/samsung/android/scloud/sync/data/SyncItem;

    invoke-interface {v7, v10, v11}, Lcom/samsung/android/scloud/sync/model/IModel;->getLocalFilePathPrefix(Landroid/content/Context;Lcom/samsung/android/scloud/sync/data/SyncItem;)Ljava/lang/String;

    move-result-object v7

    invoke-interface/range {v1 .. v7}, Lcom/samsung/android/scloud/sync/oem/IOEMControl;->updateLocal(Landroid/content/Context;Lcom/samsung/android/scloud/sync/model/IModel;Lcom/samsung/android/scloud/sync/data/SyncItem;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 203
    .local v9, "localId":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$5;->val$item:Lcom/samsung/android/scloud/sync/data/SyncItem;

    invoke-virtual {v1, v9}, Lcom/samsung/android/scloud/sync/data/SyncItem;->setLocalId(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 208
    return-void

    .line 205
    .end local v5    # "toDownloadFileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v9    # "localId":Ljava/lang/String;
    :catch_0
    move-exception v8

    .line 206
    .local v8, "e":Ljava/io/IOException;
    new-instance v1, Lcom/samsung/android/scloud/framework/PDMException;

    const/16 v2, 0x12f

    const-string v3, "Download failed"

    invoke-direct {v1, v2, v3, v8}, Lcom/samsung/android/scloud/framework/PDMException;-><init>(ILjava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
