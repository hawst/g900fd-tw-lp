.class public interface abstract Lcom/samsung/android/scloud/sync/model/IModel;
.super Ljava/lang/Object;
.source "IModel.java"


# virtual methods
.method public abstract generateSyncKey(Lcom/samsung/android/scloud/sync/data/SyncItem;)Ljava/lang/String;
.end method

.method public abstract getCid()Ljava/lang/String;
.end method

.method public abstract getCloudServiceControl()Lcom/samsung/android/scloud/sync/server/ICloudServiceControl;
.end method

.method public abstract getLocalFileKeyHader(Lcom/samsung/android/scloud/sync/data/SyncItem;)Ljava/lang/String;
.end method

.method public abstract getLocalFilePathPrefix(Landroid/content/Context;Lcom/samsung/android/scloud/sync/data/SyncItem;)Ljava/lang/String;
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getOEMControl()Lcom/samsung/android/scloud/sync/oem/IOEMControl;
.end method

.method public abstract getOemContentUri()Landroid/net/Uri;
.end method

.method public abstract getServerFilePathPrefix(Lcom/samsung/android/scloud/sync/data/SyncItem;)Ljava/lang/String;
.end method
