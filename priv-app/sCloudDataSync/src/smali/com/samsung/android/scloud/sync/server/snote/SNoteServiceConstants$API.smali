.class public interface abstract Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceConstants$API;
.super Ljava/lang/Object;
.source "SNoteServiceConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "API"
.end annotation


# static fields
.field public static final DELETE:Ljava/lang/String; = "delete"

.field public static final DOWNLOAD:Ljava/lang/String; = "download"

.field public static final LIST:Ljava/lang/String; = "list"

.field public static final SNOTE_API:Ljava/lang/String; = "/snote/"

.field public static final TIMESTAMP:Ljava/lang/String; = "timestamp"

.field public static final UPLOAD:Ljava/lang/String; = "upload"
