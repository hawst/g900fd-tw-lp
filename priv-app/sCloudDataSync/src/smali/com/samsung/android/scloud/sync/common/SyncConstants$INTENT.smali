.class public interface abstract Lcom/samsung/android/scloud/sync/common/SyncConstants$INTENT;
.super Ljava/lang/Object;
.source "SyncConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/sync/common/SyncConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "INTENT"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/scloud/sync/common/SyncConstants$INTENT$VALUE;,
        Lcom/samsung/android/scloud/sync/common/SyncConstants$INTENT$KEY;
    }
.end annotation


# static fields
.field public static final ACCOUNT_SIGNED_IN:Ljava/lang/String; = "android.intent.action.SAMSUNGACCOUNT_SIGNIN_COMPLETED"

.field public static final ACCOUNT_SIGNED_OUT:Ljava/lang/String; = "android.intent.action.SAMSUNGACCOUNT_SIGNOUT_COMPLETED"

.field public static final REQUEST_CANCEL:Ljava/lang/String; = "com.samsung.android.scloud.sync.REQUEST_CANCEL"

.field public static final REQUEST_SYNC:Ljava/lang/String; = "com.samsung.android.scloud.sync.REQUEST_SYNC"
