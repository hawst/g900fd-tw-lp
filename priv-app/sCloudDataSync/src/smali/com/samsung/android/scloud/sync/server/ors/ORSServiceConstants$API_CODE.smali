.class public interface abstract Lcom/samsung/android/scloud/sync/server/ors/ORSServiceConstants$API_CODE;
.super Ljava/lang/Object;
.source "ORSServiceConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/sync/server/ors/ORSServiceConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "API_CODE"
.end annotation


# static fields
.field public static final DELETE_FILE:I = 0x38d

.field public static final DELETE_FOLDER:I = 0x38c

.field public static final DOWNLOAD_FILE:I = 0x38b

.field public static final GET_TIMESTAMP:I = 0x385

.field public static final LIST_DIRECTORY:I = 0x386

.field public static final TRANSACTION_CANCEL:I = 0x389

.field public static final TRANSACTION_END:I = 0x388

.field public static final TRANSACTION_START:I = 0x387

.field public static final UPDATE_TAG:I = 0x38e

.field public static final UPLOAD_FILE:I = 0x38a
