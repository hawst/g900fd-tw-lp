.class public Lcom/samsung/android/scloud/sync/common/MetaManager;
.super Ljava/lang/Object;
.source "MetaManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/scloud/sync/common/MetaManager$META;
    }
.end annotation


# static fields
.field private static final DISABLED_OPERATOR_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private static final MIN_MEMORY:J = 0xc0000L

.field private static final TAG:Ljava/lang/String; = "MetaManager"

.field private static sMetaManager:Lcom/samsung/android/scloud/sync/common/MetaManager;


# instance fields
.field private lastServerSyncTimeMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mSyncMeta:Landroid/content/SharedPreferences;

.field private progressMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 40
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/scloud/sync/common/MetaManager;->sMetaManager:Lcom/samsung/android/scloud/sync/common/MetaManager;

    .line 58
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/scloud/sync/common/MetaManager;->DISABLED_OPERATOR_MAP:Ljava/util/Map;

    .line 60
    sget-object v0, Lcom/samsung/android/scloud/sync/common/MetaManager;->DISABLED_OPERATOR_MAP:Ljava/util/Map;

    const-string v1, "DCM"

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    sget-object v0, Lcom/samsung/android/scloud/sync/common/MetaManager;->DISABLED_OPERATOR_MAP:Ljava/util/Map;

    const-string v1, "VZW"

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    sget-object v0, Lcom/samsung/android/scloud/sync/common/MetaManager;->DISABLED_OPERATOR_MAP:Ljava/util/Map;

    const-string v1, "LRA"

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    sget-object v0, Lcom/samsung/android/scloud/sync/common/MetaManager;->DISABLED_OPERATOR_MAP:Ljava/util/Map;

    const-string v1, "CTC"

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    sget-object v0, Lcom/samsung/android/scloud/sync/common/MetaManager;->DISABLED_OPERATOR_MAP:Ljava/util/Map;

    const-string v1, "CHC"

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    sget-object v0, Lcom/samsung/android/scloud/sync/common/MetaManager;->DISABLED_OPERATOR_MAP:Ljava/util/Map;

    const-string v1, "CHM"

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    sget-object v0, Lcom/samsung/android/scloud/sync/common/MetaManager;->DISABLED_OPERATOR_MAP:Ljava/util/Map;

    const-string v1, "CHU"

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/scloud/sync/common/MetaManager;->mSyncMeta:Landroid/content/SharedPreferences;

    .line 73
    const-string v0, "SyncMeta"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/sync/common/MetaManager;->mSyncMeta:Landroid/content/SharedPreferences;

    .line 74
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/sync/common/MetaManager;->progressMap:Ljava/util/Map;

    .line 76
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/sync/common/MetaManager;->lastServerSyncTimeMap:Ljava/util/Map;

    .line 79
    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/sync/common/MetaManager;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 82
    const-class v1, Lcom/samsung/android/scloud/sync/common/MetaManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/scloud/sync/common/MetaManager;->sMetaManager:Lcom/samsung/android/scloud/sync/common/MetaManager;

    if-nez v0, :cond_0

    .line 83
    new-instance v0, Lcom/samsung/android/scloud/sync/common/MetaManager;

    invoke-direct {v0, p0}, Lcom/samsung/android/scloud/sync/common/MetaManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/scloud/sync/common/MetaManager;->sMetaManager:Lcom/samsung/android/scloud/sync/common/MetaManager;

    .line 85
    :cond_0
    sget-object v0, Lcom/samsung/android/scloud/sync/common/MetaManager;->sMetaManager:Lcom/samsung/android/scloud/sync/common/MetaManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 82
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private isDisabledDevice(Landroid/content/Context;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 186
    sget-object v1, Lcom/samsung/android/scloud/sync/common/MetaManager;->DISABLED_OPERATOR_MAP:Ljava/util/Map;

    invoke-static {p1}, Lcom/samsung/android/scloud/framework/util/PDMUtil;->getSalesInfo(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 187
    .local v0, "disabled":Ljava/lang/Boolean;
    if-eqz v0, :cond_0

    .line 189
    const/4 v1, 0x1

    .line 191
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public accountRemoved()V
    .locals 2

    .prologue
    .line 181
    const-string v0, "MetaManager"

    const-string v1, "accountRemoved... "

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/framework/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    invoke-virtual {p0}, Lcom/samsung/android/scloud/sync/common/MetaManager;->clear()V

    .line 183
    return-void
.end method

.method public accountSignedIn(Landroid/content/Context;Landroid/accounts/Account;)V
    .locals 17
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "account"    # Landroid/accounts/Account;

    .prologue
    .line 151
    const-string v14, "MetaManager"

    const-string v15, "accountSignedIn.. "

    invoke-static {v14, v15}, Lcom/samsung/android/scloud/framework/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    invoke-static {}, Landroid/content/ContentResolver;->getSyncAdapterTypes()[Landroid/content/SyncAdapterType;

    move-result-object v12

    .line 154
    .local v12, "syncAdapterTypes":[Landroid/content/SyncAdapterType;
    const-wide/32 v14, 0xc0000

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/samsung/android/scloud/framework/util/PDMUtil;->getTotalMemory(Landroid/content/Context;J)J

    move-result-wide v7

    .line 155
    .local v7, "mem":J
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/scloud/framework/util/PDMUtil;->getSalesInfo(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v10

    .line 156
    .local v10, "sales":Ljava/lang/String;
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/scloud/sync/common/MetaManager;->isDisabledDevice(Landroid/content/Context;)Z

    move-result v14

    if-nez v14, :cond_2

    const-wide/32 v14, 0xc0000

    cmp-long v14, v7, v14

    if-lez v14, :cond_2

    const/4 v5, 0x1

    .line 157
    .local v5, "isAuto":Z
    :goto_0
    const-string v14, "MetaManager"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Sync Auto Enabled : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", Sales : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", Mem : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/samsung/android/scloud/framework/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    move-object v2, v12

    .local v2, "arr$":[Landroid/content/SyncAdapterType;
    array-length v6, v2

    .local v6, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_1
    if-ge v4, v6, :cond_3

    aget-object v11, v2, v4

    .line 161
    .local v11, "syncAdapterType":Landroid/content/SyncAdapterType;
    iget-object v1, v11, Landroid/content/SyncAdapterType;->accountType:Ljava/lang/String;

    .line 162
    .local v1, "accountType":Ljava/lang/String;
    const-string v14, "com.osp.app.signin"

    invoke-virtual {v1, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_1

    .line 164
    iget-object v3, v11, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    .line 165
    .local v3, "authority":Ljava/lang/String;
    invoke-virtual {v11}, Landroid/content/SyncAdapterType;->isUserVisible()Z

    move-result v13

    .line 166
    .local v13, "visible":Z
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v14

    const/4 v15, 0x0

    invoke-virtual {v14, v3, v15}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v9

    .line 168
    .local v9, "providerInfo":Landroid/content/pm/ProviderInfo;
    if-eqz v9, :cond_1

    if-eqz v13, :cond_1

    .line 170
    if-nez v5, :cond_0

    .line 171
    move-object/from16 v0, p2

    invoke-static {v0, v3}, Landroid/content/ContentResolver;->cancelSync(Landroid/accounts/Account;Ljava/lang/String;)V

    .line 173
    :cond_0
    move-object/from16 v0, p2

    invoke-static {v0, v3, v5}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 174
    const-string v14, "MetaManager"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Sync Auto Enabled set : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", authority : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/samsung/android/scloud/framework/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    .end local v3    # "authority":Ljava/lang/String;
    .end local v9    # "providerInfo":Landroid/content/pm/ProviderInfo;
    .end local v13    # "visible":Z
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 156
    .end local v1    # "accountType":Ljava/lang/String;
    .end local v2    # "arr$":[Landroid/content/SyncAdapterType;
    .end local v4    # "i$":I
    .end local v5    # "isAuto":Z
    .end local v6    # "len$":I
    .end local v11    # "syncAdapterType":Landroid/content/SyncAdapterType;
    :cond_2
    const/4 v5, 0x0

    goto/16 :goto_0

    .line 178
    .restart local v2    # "arr$":[Landroid/content/SyncAdapterType;
    .restart local v4    # "i$":I
    .restart local v5    # "isAuto":Z
    .restart local v6    # "len$":I
    :cond_3
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/common/MetaManager;->mSyncMeta:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 199
    return-void
.end method

.method public commitLastSyncTime(Ljava/lang/String;)V
    .locals 5
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 137
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/common/MetaManager;->mSyncMeta:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LastSyncTime_Server_"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/samsung/android/scloud/sync/common/MetaManager;->lastServerSyncTimeMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-interface {v1, v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 138
    return-void
.end method

.method public getDateStringFromTimeSpan(Landroid/content/Context;J)Ljava/lang/String;
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "time"    # J

    .prologue
    .line 142
    invoke-static {p1}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    .line 143
    .local v0, "dateFormat":Ljava/text/DateFormat;
    invoke-static {p1}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    .line 144
    .local v1, "timeFormat":Ljava/text/DateFormat;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4, p2, p3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4, p2, p3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 147
    .local v2, "timeStr":Ljava/lang/String;
    return-object v2
.end method

.method public getLastClientSyncTime(Ljava/lang/String;)J
    .locals 4
    .param p1, "sourceKey"    # Ljava/lang/String;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/common/MetaManager;->mSyncMeta:Landroid/content/SharedPreferences;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LastSyncTime_Client_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getLastServerSyncTime(Ljava/lang/String;)J
    .locals 4
    .param p1, "sourceKey"    # Ljava/lang/String;

    .prologue
    .line 127
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/common/MetaManager;->mSyncMeta:Landroid/content/SharedPreferences;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LastSyncTime_Server_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getProgress(Ljava/lang/String;)I
    .locals 1
    .param p1, "sourceKey"    # Ljava/lang/String;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/common/MetaManager;->progressMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getTotalProgress()I
    .locals 4

    .prologue
    .line 105
    const/4 v2, 0x0

    .line 107
    .local v2, "total":I
    iget-object v3, p0, Lcom/samsung/android/scloud/sync/common/MetaManager;->progressMap:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 108
    .local v1, "proc":I
    add-int/2addr v2, v1

    goto :goto_0

    .line 109
    .end local v1    # "proc":I
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/scloud/sync/common/MetaManager;->progressMap:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    div-int v3, v2, v3

    return v3
.end method

.method public initLastSyncTime(Ljava/lang/String;)V
    .locals 2
    .param p1, "sourceKey"    # Ljava/lang/String;

    .prologue
    .line 132
    invoke-virtual {p0, p1}, Lcom/samsung/android/scloud/sync/common/MetaManager;->getLastServerSyncTime(Ljava/lang/String;)J

    move-result-wide v0

    invoke-virtual {p0, p1, v0, v1}, Lcom/samsung/android/scloud/sync/common/MetaManager;->setLastServerSyncTime(Ljava/lang/String;J)V

    .line 133
    return-void
.end method

.method public initProgress(Ljava/lang/String;)V
    .locals 2
    .param p1, "src"    # Ljava/lang/String;

    .prologue
    .line 101
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/common/MetaManager;->progressMap:Ljava/util/Map;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    return-void
.end method

.method public initProgress([Ljava/lang/String;)V
    .locals 6
    .param p1, "sources"    # [Ljava/lang/String;

    .prologue
    .line 95
    iget-object v4, p0, Lcom/samsung/android/scloud/sync/common/MetaManager;->progressMap:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->clear()V

    .line 96
    move-object v0, p1

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 97
    .local v3, "src":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/scloud/sync/common/MetaManager;->progressMap:Ljava/util/Map;

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 98
    .end local v3    # "src":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public setLastServerSyncTime(Ljava/lang/String;J)V
    .locals 2
    .param p1, "sourceKey"    # Ljava/lang/String;
    .param p2, "time"    # J

    .prologue
    .line 123
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/common/MetaManager;->lastServerSyncTimeMap:Ljava/util/Map;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    return-void
.end method

.method public setProgress(Ljava/lang/String;I)V
    .locals 2
    .param p1, "sourceKey"    # Ljava/lang/String;
    .param p2, "progress"    # I

    .prologue
    .line 92
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/common/MetaManager;->progressMap:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    return-void
.end method
