.class public interface abstract Lcom/samsung/android/scloud/sync/server/ors/ORSServiceConstants;
.super Ljava/lang/Object;
.source "ORSServiceConstants.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/scloud/sync/server/ors/ORSServiceConstants$ApiCode;,
        Lcom/samsung/android/scloud/sync/server/ors/ORSServiceConstants$API_CODE;,
        Lcom/samsung/android/scloud/sync/server/ors/ORSServiceConstants$API;
    }
.end annotation


# static fields
.field public static final ACCESS_TOKEN_PARM:Ljava/lang/String; = "access_token"

.field public static final CID_PARM:Ljava/lang/String; = "cid"

.field public static final CTID_PARM:Ljava/lang/String; = "ctid"

.field public static final DEVICE_ID_PARM:Ljava/lang/String; = "did"

.field public static final KEY:Ljava/lang/String; = "key"

.field public static final MODIFIED_AFTER_PARM:Ljava/lang/String; = "modified_after"

.field public static final REVISION_PARM:Ljava/lang/String; = "revision"

.field public static final START:Ljava/lang/String; = "start"

.field public static final TAG_PARM:Ljava/lang/String; = "tag"

.field public static final TX_COUNT_PARM:Ljava/lang/String; = "tx_count"

.field public static final TX_KEY_PARM:Ljava/lang/String; = "tx_key"

.field public static final TX_SEQ_PARM:Ljava/lang/String; = "tx_seq"

.field public static final USER_ID_PARM:Ljava/lang/String; = "uid"
