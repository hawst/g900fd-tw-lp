.class public abstract Lcom/samsung/android/scloud/framework/PDMService;
.super Landroid/app/Service;
.source "PDMService.java"

# interfaces
.implements Lcom/samsung/android/scloud/framework/IPDMContext;


# instance fields
.field protected ActivityHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 47
    new-instance v0, Lcom/samsung/android/scloud/framework/PDMService$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/scloud/framework/PDMService$1;-><init>(Lcom/samsung/android/scloud/framework/PDMService;)V

    iput-object v0, p0, Lcom/samsung/android/scloud/framework/PDMService;->ActivityHandler:Landroid/os/Handler;

    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 3

    .prologue
    .line 33
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 34
    invoke-virtual {p0}, Lcom/samsung/android/scloud/framework/PDMService;->getTag()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    invoke-virtual {p0}, Lcom/samsung/android/scloud/framework/PDMService;->getTag()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/android/scloud/framework/PDMService;->getTargetEventServiceCodes()[I

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/framework/PDMService;->ActivityHandler:Landroid/os/Handler;

    invoke-static {v0, v1, v2}, Lcom/samsung/android/scloud/framework/PDMApp;->setHandlerActivity(Ljava/lang/String;[ILandroid/os/Handler;)V

    .line 37
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 41
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 42
    invoke-virtual {p0}, Lcom/samsung/android/scloud/framework/PDMService;->getTag()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onDestory()"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    invoke-virtual {p0}, Lcom/samsung/android/scloud/framework/PDMService;->getTag()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/scloud/framework/PDMApp;->removeHandlerActivity(Ljava/lang/String;)V

    .line 45
    return-void
.end method
