.class public abstract Lcom/samsung/android/scloud/sync/model/builder/IBuilder;
.super Ljava/lang/Object;
.source "IBuilder.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "IBuilder"


# instance fields
.field protected myModel:Lcom/samsung/android/scloud/sync/model/IInternalModel;


# direct methods
.method public constructor <init>(Lcom/samsung/android/scloud/sync/model/IInternalModel;)V
    .locals 1
    .param p1, "mModel"    # Lcom/samsung/android/scloud/sync/model/IInternalModel;

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/scloud/sync/model/builder/IBuilder;->myModel:Lcom/samsung/android/scloud/sync/model/IInternalModel;

    .line 41
    iput-object p1, p0, Lcom/samsung/android/scloud/sync/model/builder/IBuilder;->myModel:Lcom/samsung/android/scloud/sync/model/IInternalModel;

    .line 42
    return-void
.end method


# virtual methods
.method public abstract complete(Landroid/content/Context;Lcom/samsung/android/scloud/sync/data/SyncItem;I)Z
.end method

.method public abstract deleteLocal(Landroid/content/Context;Ljava/lang/String;)Z
.end method

.method public abstract getAttachmentFileInfo(Landroid/content/Context;Ljava/lang/String;)Lcom/samsung/android/scloud/sync/data/Attachments;
.end method

.method public abstract getLocalChange(Landroid/content/Context;Ljava/lang/String;Lcom/samsung/android/scloud/sync/data/Attachments;Ljava/lang/String;)Z
.end method

.method public prepareToSync(Landroid/content/Context;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 23
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "accountType"    # Ljava/lang/String;
    .param p4, "accountName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/scloud/sync/data/SyncItem;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/scloud/sync/data/SyncItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47
    .local p2, "serverChanges":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/samsung/android/scloud/sync/data/SyncItem;>;"
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 48
    .local v17, "result":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/scloud/sync/data/SyncItem;>;"
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/sync/model/builder/IBuilder;->myModel:Lcom/samsung/android/scloud/sync/model/IInternalModel;

    invoke-interface {v2}, Lcom/samsung/android/scloud/sync/model/IInternalModel;->getAccountTypeFieldName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " = \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\" and "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/sync/model/builder/IBuilder;->myModel:Lcom/samsung/android/scloud/sync/model/IInternalModel;

    invoke-interface {v2}, Lcom/samsung/android/scloud/sync/model/IInternalModel;->getAccountNameFieldName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " = \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p4

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    .line 51
    .local v21, "whereAccount":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/sync/model/builder/IBuilder;->myModel:Lcom/samsung/android/scloud/sync/model/IInternalModel;

    invoke-interface {v2}, Lcom/samsung/android/scloud/sync/model/IInternalModel;->getDirtyFieldName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/sync/model/builder/IBuilder;->myModel:Lcom/samsung/android/scloud/sync/model/IInternalModel;

    invoke-interface {v2}, Lcom/samsung/android/scloud/sync/model/IInternalModel;->getDirtyValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    .line 52
    .local v22, "whereDirty":Ljava/lang/String;
    if-eqz p2, :cond_1

    invoke-interface/range {p2 .. p2}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 53
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    .line 54
    .local v18, "sb":Ljava/lang/StringBuilder;
    invoke-interface/range {p2 .. p2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .local v14, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    .line 55
    .local v15, "key":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 56
    .end local v15    # "key":Ljava/lang/String;
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " or "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/sync/model/builder/IBuilder;->myModel:Lcom/samsung/android/scloud/sync/model/IInternalModel;

    invoke-interface {v2}, Lcom/samsung/android/scloud/sync/model/IInternalModel;->getSyncKeyFieldName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " in ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    .line 59
    .end local v14    # "i$":Ljava/util/Iterator;
    .end local v18    # "sb":Ljava/lang/StringBuilder;
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, v22

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 61
    .local v4, "where":Ljava/lang/String;
    const-string v1, "IBuilder"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "prepareToSync - where : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/sync/model/builder/IBuilder;->myModel:Lcom/samsung/android/scloud/sync/model/IInternalModel;

    invoke-interface {v2}, Lcom/samsung/android/scloud/sync/model/IInternalModel;->getOemContentUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "caller_is_syncadapter"

    const-string v5, "true"

    invoke-virtual {v2, v3, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/String;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/scloud/sync/model/builder/IBuilder;->myModel:Lcom/samsung/android/scloud/sync/model/IInternalModel;

    invoke-interface {v10}, Lcom/samsung/android/scloud/sync/model/IInternalModel;->getLocalIdFieldName()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v3, v5

    const/4 v5, 0x1

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/scloud/sync/model/builder/IBuilder;->myModel:Lcom/samsung/android/scloud/sync/model/IInternalModel;

    invoke-interface {v10}, Lcom/samsung/android/scloud/sync/model/IInternalModel;->getSyncKeyFieldName()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v3, v5

    const/4 v5, 0x2

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/scloud/sync/model/builder/IBuilder;->myModel:Lcom/samsung/android/scloud/sync/model/IInternalModel;

    invoke-interface {v10}, Lcom/samsung/android/scloud/sync/model/IInternalModel;->getDeletedFieldName()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v3, v5

    const/4 v5, 0x3

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/scloud/sync/model/builder/IBuilder;->myModel:Lcom/samsung/android/scloud/sync/model/IInternalModel;

    invoke-interface {v10}, Lcom/samsung/android/scloud/sync/model/IInternalModel;->getTimestampFieldName()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v3, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 68
    .local v11, "cursor":Landroid/database/Cursor;
    if-nez v11, :cond_2

    .line 96
    :goto_1
    return-object v17

    .line 71
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/scloud/sync/model/builder/IBuilder;->myModel:Lcom/samsung/android/scloud/sync/model/IInternalModel;

    invoke-interface {v1}, Lcom/samsung/android/scloud/sync/model/IInternalModel;->getLocalIdFieldName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    .line 72
    .local v16, "localIdColIdx":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/scloud/sync/model/builder/IBuilder;->myModel:Lcom/samsung/android/scloud/sync/model/IInternalModel;

    invoke-interface {v1}, Lcom/samsung/android/scloud/sync/model/IInternalModel;->getSyncKeyFieldName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    .line 73
    .local v19, "syncKeyColIdx":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/scloud/sync/model/builder/IBuilder;->myModel:Lcom/samsung/android/scloud/sync/model/IInternalModel;

    invoke-interface {v1}, Lcom/samsung/android/scloud/sync/model/IInternalModel;->getDeletedFieldName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    .line 76
    .local v13, "deletedColIdx":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/scloud/sync/model/builder/IBuilder;->myModel:Lcom/samsung/android/scloud/sync/model/IInternalModel;

    invoke-interface {v1}, Lcom/samsung/android/scloud/sync/model/IInternalModel;->getTimestampFieldName()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_3

    .line 77
    const/16 v20, -0x1

    .line 81
    .local v20, "timestampColIdx":I
    :goto_2
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 82
    move/from16 v0, v16

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 83
    .local v6, "localId":Ljava/lang/String;
    move/from16 v0, v19

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 84
    .local v7, "syncKey":Ljava/lang/String;
    invoke-interface {v11, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 88
    .local v12, "deleted":I
    const/4 v1, -0x1

    move/from16 v0, v20

    if-ne v1, v0, :cond_4

    .line 89
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 92
    .local v8, "timestamp":J
    :goto_3
    const-string v1, "IBuilder"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Local Item - localId : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", key : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", timestamp : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", deleted : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    new-instance v5, Lcom/samsung/android/scloud/sync/data/SyncItem;

    const/4 v1, 0x1

    if-ne v12, v1, :cond_5

    const/4 v10, 0x1

    :goto_4
    invoke-direct/range {v5 .. v10}, Lcom/samsung/android/scloud/sync/data/SyncItem;-><init>(Ljava/lang/String;Ljava/lang/String;JZ)V

    move-object/from16 v0, v17

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 79
    .end local v6    # "localId":Ljava/lang/String;
    .end local v7    # "syncKey":Ljava/lang/String;
    .end local v8    # "timestamp":J
    .end local v12    # "deleted":I
    .end local v20    # "timestampColIdx":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/scloud/sync/model/builder/IBuilder;->myModel:Lcom/samsung/android/scloud/sync/model/IInternalModel;

    invoke-interface {v1}, Lcom/samsung/android/scloud/sync/model/IInternalModel;->getTimestampFieldName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    .restart local v20    # "timestampColIdx":I
    goto :goto_2

    .line 91
    .restart local v6    # "localId":Ljava/lang/String;
    .restart local v7    # "syncKey":Ljava/lang/String;
    .restart local v12    # "deleted":I
    :cond_4
    move/from16 v0, v20

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .restart local v8    # "timestamp":J
    goto :goto_3

    .line 93
    :cond_5
    const/4 v10, 0x0

    goto :goto_4

    .line 95
    .end local v6    # "localId":Ljava/lang/String;
    .end local v7    # "syncKey":Ljava/lang/String;
    .end local v8    # "timestamp":J
    .end local v12    # "deleted":I
    :cond_6
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1
.end method

.method public abstract updateLocal(Landroid/content/Context;Lcom/samsung/android/scloud/sync/data/SyncItem;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/samsung/android/scloud/sync/data/SyncItem;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation
.end method
