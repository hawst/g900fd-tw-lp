.class Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceManager;
.super Ljava/lang/Object;
.source "SNoteServiceManager.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SNoteServiceManager"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static deleteNote(Landroid/content/Context;Lcom/samsung/android/scloud/auth/AuthManager;Lcom/samsung/android/scloud/sync/model/IModel;Lcom/samsung/android/scloud/sync/data/SyncItem;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/scloud/framework/network/NetworkUtil$StringResponseHandler;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "auth"    # Lcom/samsung/android/scloud/auth/AuthManager;
    .param p2, "model"    # Lcom/samsung/android/scloud/sync/model/IModel;
    .param p3, "item"    # Lcom/samsung/android/scloud/sync/data/SyncItem;
    .param p4, "clientTimestamp"    # Ljava/lang/String;
    .param p5, "ctid"    # Ljava/lang/String;
    .param p6, "handler"    # Lcom/samsung/android/scloud/framework/network/NetworkUtil$StringResponseHandler;

    .prologue
    const/4 v5, 0x0

    .line 211
    const-string v2, "SNoteServiceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "deleteNote!!!!!!!!!! - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", key : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p3}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getSyncKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/samsung/android/scloud/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 216
    .local v1, "url":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Lcom/samsung/android/scloud/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    .line 217
    const-string v2, "SNoteServiceManager"

    const-string v3, "There is NO Base URL."

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    new-instance v2, Lcom/samsung/android/scloud/framework/PDMException;

    const/16 v3, 0x131

    invoke-direct {v2, v3}, Lcom/samsung/android/scloud/framework/PDMException;-><init>(I)V

    throw v2

    .line 221
    :cond_0
    const-string v2, "/snote/delete"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 222
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p3}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getSyncKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 224
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/samsung/android/scloud/auth/AuthManager;->getPutApiParamsWithCid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 226
    if-eqz p4, :cond_1

    .line 227
    const-string v2, "clientTimestamp"

    invoke-static {v1, v2, p4, v5}, Lcom/samsung/android/scloud/framework/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 229
    :cond_1
    const-string v2, "ctid"

    invoke-static {v1, v2, p5, v5}, Lcom/samsung/android/scloud/framework/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 231
    const-string v2, "SNoteServiceManager"

    const-string v3, "API Code is 805"

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    invoke-interface {p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/network/NetworkUtil;->post(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 233
    .local v0, "httpResponse":Lorg/apache/http/HttpResponse;
    invoke-static {v0, p6}, Lcom/samsung/android/scloud/framework/network/NetworkUtil;->extractResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/framework/network/NetworkUtil$StringResponseHandler;)V

    .line 236
    return-void
.end method

.method public static downloadNote(Landroid/content/Context;Lcom/samsung/android/scloud/auth/AuthManager;Lcom/samsung/android/scloud/sync/model/IModel;Lcom/samsung/android/scloud/sync/data/SyncItem;Ljava/lang/String;Lcom/samsung/android/scloud/framework/network/NetworkUtil$FileResponseHandler;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "auth"    # Lcom/samsung/android/scloud/auth/AuthManager;
    .param p2, "model"    # Lcom/samsung/android/scloud/sync/model/IModel;
    .param p3, "item"    # Lcom/samsung/android/scloud/sync/data/SyncItem;
    .param p4, "ctid"    # Ljava/lang/String;
    .param p5, "handler"    # Lcom/samsung/android/scloud/framework/network/NetworkUtil$FileResponseHandler;

    .prologue
    .line 179
    const-string v2, "SNoteServiceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "downloadNote!!!!!!!!!! - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", key : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p3}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getSyncKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/samsung/android/scloud/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 184
    .local v1, "url":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Lcom/samsung/android/scloud/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    .line 185
    const-string v2, "SNoteServiceManager"

    const-string v3, "There is NO Base URL."

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    new-instance v2, Lcom/samsung/android/scloud/framework/PDMException;

    const/16 v3, 0x131

    invoke-direct {v2, v3}, Lcom/samsung/android/scloud/framework/PDMException;-><init>(I)V

    throw v2

    .line 189
    :cond_0
    const-string v2, "/snote/download"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 190
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p3}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getSyncKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 192
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/samsung/android/scloud/auth/AuthManager;->getPutApiParamsWithCid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 194
    const-string v2, "ctid"

    const/4 v3, 0x0

    invoke-static {v1, v2, p4, v3}, Lcom/samsung/android/scloud/framework/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 196
    const-string v2, "SNoteServiceManager"

    const-string v3, "API Code is 804"

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    invoke-interface {p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/samsung/android/scloud/framework/network/NetworkUtil;->get(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 199
    .local v0, "httpResponse":Lorg/apache/http/HttpResponse;
    invoke-virtual {p3}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getSyncKey()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, p5}, Lcom/samsung/android/scloud/framework/network/NetworkUtil;->extractResponse4(Lorg/apache/http/HttpResponse;Ljava/lang/String;Lcom/samsung/android/scloud/framework/network/NetworkUtil$FileResponseHandler;)V

    .line 200
    return-void
.end method

.method public static getTimestamp(Landroid/content/Context;Lcom/samsung/android/scloud/auth/AuthManager;Lcom/samsung/android/scloud/sync/model/IModel;Ljava/lang/String;Lcom/samsung/android/scloud/framework/network/NetworkUtil$JSONResponseHandler;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "auth"    # Lcom/samsung/android/scloud/auth/AuthManager;
    .param p2, "model"    # Lcom/samsung/android/scloud/sync/model/IModel;
    .param p3, "ctid"    # Ljava/lang/String;
    .param p4, "handler"    # Lcom/samsung/android/scloud/framework/network/NetworkUtil$JSONResponseHandler;

    .prologue
    .line 39
    const-string v2, "SNoteServiceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getTimestamp!!!!!!!!!! - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/samsung/android/scloud/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 44
    .local v1, "url":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Lcom/samsung/android/scloud/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    .line 45
    const-string v2, "SNoteServiceManager"

    const-string v3, "There is NO Base URL."

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    new-instance v2, Lcom/samsung/android/scloud/framework/PDMException;

    const/16 v3, 0x131

    invoke-direct {v2, v3}, Lcom/samsung/android/scloud/framework/PDMException;-><init>(I)V

    throw v2

    .line 49
    :cond_0
    const-string v2, "/timestamp"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 50
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/samsung/android/scloud/auth/AuthManager;->getPutApiParamsWithCid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    const-string v2, "ctid"

    const/4 v3, 0x0

    invoke-static {v1, v2, p3, v3}, Lcom/samsung/android/scloud/framework/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 53
    const-string v2, "SNoteServiceManager"

    const-string v3, "API Code is 801"

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    invoke-interface {p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/samsung/android/scloud/framework/network/NetworkUtil;->get(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 55
    .local v0, "httpResponse":Lorg/apache/http/HttpResponse;
    invoke-static {v0, p4}, Lcom/samsung/android/scloud/framework/network/NetworkUtil;->extractResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/framework/network/NetworkUtil$StringResponseHandler;)V

    .line 56
    return-void
.end method

.method public static listNote(Landroid/content/Context;Lcom/samsung/android/scloud/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/sync/model/IModel;Ljava/lang/String;Ljava/lang/String;JLcom/samsung/android/scloud/framework/network/NetworkUtil$JSONResponseHandler;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "auth"    # Lcom/samsung/android/scloud/auth/AuthManager;
    .param p2, "ctid"    # Ljava/lang/String;
    .param p3, "model"    # Lcom/samsung/android/scloud/sync/model/IModel;
    .param p4, "directoryPath"    # Ljava/lang/String;
    .param p5, "startKey"    # Ljava/lang/String;
    .param p6, "modifiedAfter"    # J
    .param p8, "handler"    # Lcom/samsung/android/scloud/framework/network/NetworkUtil$JSONResponseHandler;

    .prologue
    const/4 v5, 0x0

    .line 62
    const-string v2, "SNoteServiceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "listNote!!!!!!!!!! - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/samsung/android/scloud/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 67
    .local v1, "url":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Lcom/samsung/android/scloud/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    .line 68
    const-string v2, "SNoteServiceManager"

    const-string v3, "There is NO Base URL."

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    new-instance v2, Lcom/samsung/android/scloud/framework/PDMException;

    const/16 v3, 0x131

    invoke-direct {v2, v3}, Lcom/samsung/android/scloud/framework/PDMException;-><init>(I)V

    throw v2

    .line 72
    :cond_0
    const-string v2, "/snote/list"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p3}, Lcom/samsung/android/scloud/sync/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/samsung/android/scloud/auth/AuthManager;->getPutApiParamsWithCid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    const-wide/16 v2, 0x0

    cmp-long v2, p6, v2

    if-lez v2, :cond_1

    .line 75
    const-string v2, "modified_after"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p6, p7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3, v5}, Lcom/samsung/android/scloud/framework/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 77
    :cond_1
    if-eqz p5, :cond_2

    const-string v2, ""

    invoke-virtual {v2, p5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 78
    const-string v2, "start_key"

    invoke-static {v1, v2, p5, v5}, Lcom/samsung/android/scloud/framework/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 80
    :cond_2
    const-string v2, "ctid"

    invoke-static {v1, v2, p2, v5}, Lcom/samsung/android/scloud/framework/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 82
    const-string v2, "SNoteServiceManager"

    const-string v3, "API Code is 802"

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    invoke-interface {p3}, Lcom/samsung/android/scloud/sync/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/samsung/android/scloud/framework/network/NetworkUtil;->get(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 84
    .local v0, "httpResponse":Lorg/apache/http/HttpResponse;
    invoke-static {v0, p8}, Lcom/samsung/android/scloud/framework/network/NetworkUtil;->extractResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/framework/network/NetworkUtil$StringResponseHandler;)V

    .line 85
    return-void
.end method

.method public static uploadNote(Landroid/content/Context;Lcom/samsung/android/scloud/auth/AuthManager;Lcom/samsung/android/scloud/sync/model/IModel;Lcom/samsung/android/scloud/sync/data/SyncItem;Lcom/samsung/android/scloud/sync/data/Attachments;Ljava/lang/String;Lcom/samsung/android/scloud/framework/network/NetworkUtil$PDMProgressListener;Lcom/samsung/android/scloud/framework/network/NetworkUtil$JSONResponseHandler;)V
    .locals 22
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "auth"    # Lcom/samsung/android/scloud/auth/AuthManager;
    .param p2, "model"    # Lcom/samsung/android/scloud/sync/model/IModel;
    .param p3, "item"    # Lcom/samsung/android/scloud/sync/data/SyncItem;
    .param p4, "attachments"    # Lcom/samsung/android/scloud/sync/data/Attachments;
    .param p5, "ctid"    # Ljava/lang/String;
    .param p6, "progressListener"    # Lcom/samsung/android/scloud/framework/network/NetworkUtil$PDMProgressListener;
    .param p7, "handler"    # Lcom/samsung/android/scloud/framework/network/NetworkUtil$JSONResponseHandler;

    .prologue
    .line 95
    const/4 v6, 0x0

    .line 96
    .local v6, "contentFileInputStream":Ljava/io/FileInputStream;
    const/4 v9, 0x0

    .line 98
    .local v9, "fileInputStream":Ljava/io/FileInputStream;
    new-instance v14, Lcom/samsung/android/scloud/framework/network/CustomMultiPartEntity;

    const/16 v18, 0x0

    const-string v19, "AaB03x"

    const/16 v20, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    move-object/from16 v3, p6

    invoke-direct {v14, v0, v1, v2, v3}, Lcom/samsung/android/scloud/framework/network/CustomMultiPartEntity;-><init>(Lorg/apache/http/entity/mime/HttpMultipartMode;Ljava/lang/String;Ljava/nio/charset/Charset;Lcom/samsung/android/scloud/framework/network/NetworkUtil$PDMProgressListener;)V

    .line 101
    .local v14, "requestEntity":Lorg/apache/http/entity/mime/MultipartEntity;
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    move-object/from16 v2, p3

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/scloud/sync/model/IModel;->getLocalFilePathPrefix(Landroid/content/Context;Lcom/samsung/android/scloud/sync/data/SyncItem;)Ljava/lang/String;

    move-result-object v10

    .line 102
    .local v10, "filePrefix":Ljava/lang/String;
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "content.sync"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 104
    .local v13, "metaFile":Ljava/lang/String;
    new-instance v12, Ljava/io/File;

    invoke-direct {v12, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 106
    .local v12, "metaF":Ljava/io/File;
    invoke-virtual {v12}, Ljava/io/File;->exists()Z

    move-result v18

    if-nez v18, :cond_0

    .line 107
    new-instance v18, Lcom/samsung/android/scloud/framework/PDMException;

    const/16 v19, 0x138

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "File not exists : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v18 .. v20}, Lcom/samsung/android/scloud/framework/PDMException;-><init>(ILjava/lang/String;)V

    throw v18

    .line 110
    :cond_0
    :try_start_0
    new-instance v6, Ljava/io/FileInputStream;

    .end local v6    # "contentFileInputStream":Ljava/io/FileInputStream;
    invoke-direct {v6, v12}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 115
    .restart local v6    # "contentFileInputStream":Ljava/io/FileInputStream;
    const-string v18, "snote_detail"

    new-instance v19, Lorg/apache/http/entity/mime/content/InputStreamBody;

    const-string v20, "application/json;charset=utf-8"

    const-string v21, "snote_detail"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-direct {v0, v6, v1, v2}, Lorg/apache/http/entity/mime/content/InputStreamBody;-><init>(Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Lorg/apache/http/entity/mime/MultipartEntity;->addPart(Ljava/lang/String;Lorg/apache/http/entity/mime/content/ContentBody;)V

    .line 117
    if-eqz p4, :cond_2

    invoke-virtual/range {p4 .. p4}, Lcom/samsung/android/scloud/sync/data/Attachments;->hasFile()Z

    move-result v18

    if-eqz v18, :cond_2

    .line 118
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const/16 v19, 0x0

    move-object/from16 v0, p4

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/sync/data/Attachments;->getFileNameAt(I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 119
    .local v16, "spdFile":Ljava/lang/String;
    const-string v18, "SNoteServiceManager"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "uploadNote!!!!!!!!!! - "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p5

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ", key : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {p3 .. p3}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getSyncKey()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ", file : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    new-instance v15, Ljava/io/File;

    invoke-direct/range {v15 .. v16}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 123
    .local v15, "spdF":Ljava/io/File;
    invoke-virtual {v15}, Ljava/io/File;->exists()Z

    move-result v18

    if-nez v18, :cond_1

    .line 124
    new-instance v18, Lcom/samsung/android/scloud/framework/PDMException;

    const/16 v19, 0x138

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "File not exists :"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v18 .. v20}, Lcom/samsung/android/scloud/framework/PDMException;-><init>(ILjava/lang/String;)V

    throw v18

    .line 111
    .end local v6    # "contentFileInputStream":Ljava/io/FileInputStream;
    .end local v15    # "spdF":Ljava/io/File;
    .end local v16    # "spdFile":Ljava/lang/String;
    :catch_0
    move-exception v8

    .line 113
    .local v8, "e1":Ljava/io/FileNotFoundException;
    new-instance v18, Lcom/samsung/android/scloud/framework/PDMException;

    const/16 v19, 0x138

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "File not exists : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v18 .. v20}, Lcom/samsung/android/scloud/framework/PDMException;-><init>(ILjava/lang/String;)V

    throw v18

    .line 126
    .end local v8    # "e1":Ljava/io/FileNotFoundException;
    .restart local v6    # "contentFileInputStream":Ljava/io/FileInputStream;
    .restart local v15    # "spdF":Ljava/io/File;
    .restart local v16    # "spdFile":Ljava/lang/String;
    :cond_1
    :try_start_1
    new-instance v9, Ljava/io/FileInputStream;

    .end local v9    # "fileInputStream":Ljava/io/FileInputStream;
    invoke-direct {v9, v15}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 131
    .restart local v9    # "fileInputStream":Ljava/io/FileInputStream;
    const-string v18, "file"

    new-instance v19, Lorg/apache/http/entity/mime/content/InputStreamBody;

    const-string v20, "application/octet-stream"

    const/16 v21, 0x0

    move-object/from16 v0, p4

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/sync/data/Attachments;->getFileNameAt(I)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-direct {v0, v9, v1, v2}, Lorg/apache/http/entity/mime/content/InputStreamBody;-><init>(Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Lorg/apache/http/entity/mime/MultipartEntity;->addPart(Ljava/lang/String;Lorg/apache/http/entity/mime/content/ContentBody;)V

    .line 134
    .end local v15    # "spdF":Ljava/io/File;
    .end local v16    # "spdFile":Ljava/lang/String;
    :cond_2
    const-string v18, "SNoteServiceManager"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "uploadNote!!!!!!!!!! - "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p5

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ", key : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {p3 .. p3}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getSyncKey()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ", metaFile:"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    invoke-virtual/range {p3 .. p3}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getTimeStamp()J

    move-result-wide v4

    .line 138
    .local v4, "clientTimestamp":J
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/scloud/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 141
    .local v17, "url":Ljava/lang/StringBuilder;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/scloud/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v18

    if-nez v18, :cond_3

    .line 142
    const-string v18, "SNoteServiceManager"

    const-string v19, "There is NO Base URL."

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/scloud/framework/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    new-instance v18, Lcom/samsung/android/scloud/framework/PDMException;

    const/16 v19, 0x131

    invoke-direct/range {v18 .. v19}, Lcom/samsung/android/scloud/framework/PDMException;-><init>(I)V

    throw v18

    .line 127
    .end local v4    # "clientTimestamp":J
    .end local v9    # "fileInputStream":Ljava/io/FileInputStream;
    .end local v17    # "url":Ljava/lang/StringBuilder;
    .restart local v15    # "spdF":Ljava/io/File;
    .restart local v16    # "spdFile":Ljava/lang/String;
    :catch_1
    move-exception v7

    .line 129
    .local v7, "e":Ljava/io/FileNotFoundException;
    new-instance v18, Lcom/samsung/android/scloud/framework/PDMException;

    const/16 v19, 0x138

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "File not exists :"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v18 .. v20}, Lcom/samsung/android/scloud/framework/PDMException;-><init>(ILjava/lang/String;)V

    throw v18

    .line 146
    .end local v7    # "e":Ljava/io/FileNotFoundException;
    .end local v15    # "spdF":Ljava/io/File;
    .end local v16    # "spdFile":Ljava/lang/String;
    .restart local v4    # "clientTimestamp":J
    .restart local v9    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v17    # "url":Ljava/lang/StringBuilder;
    :cond_3
    const-string v18, "/snote/upload"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "/"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {p3 .. p3}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getSyncKey()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 149
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "?"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-interface/range {p2 .. p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/auth/AuthManager;->getPutApiParamsWithCid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    const-wide/16 v18, 0x0

    cmp-long v18, v4, v18

    if-lez v18, :cond_4

    .line 152
    const-string v18, "clientTimestamp"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ""

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    const/16 v20, 0x0

    invoke-static/range {v17 .. v20}, Lcom/samsung/android/scloud/framework/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 153
    :cond_4
    const-string v18, "ctid"

    const/16 v19, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, p5

    move/from16 v3, v19

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/android/scloud/framework/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 156
    :try_start_2
    const-string v18, "SNoteServiceManager"

    const-string v19, "API Code is 803"

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/scloud/framework/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    invoke-interface/range {p2 .. p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-static {v0, v1, v14}, Lcom/samsung/android/scloud/framework/network/NetworkUtil;->postMultiPart(Ljava/lang/String;Ljava/lang/String;Lorg/apache/http/entity/mime/MultipartEntity;)Lorg/apache/http/HttpResponse;

    move-result-object v11

    .line 159
    .local v11, "httpResponse":Lorg/apache/http/HttpResponse;
    move-object/from16 v0, p7

    invoke-static {v11, v0}, Lcom/samsung/android/scloud/framework/network/NetworkUtil;->extractResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/framework/network/NetworkUtil$StringResponseHandler;)V

    .line 160
    if-eqz v6, :cond_5

    .line 161
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V

    .line 162
    :cond_5
    if-eqz v9, :cond_6

    .line 163
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Lcom/samsung/android/scloud/framework/PDMException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    .line 170
    :cond_6
    return-void

    .line 164
    .end local v11    # "httpResponse":Lorg/apache/http/HttpResponse;
    :catch_2
    move-exception v7

    .line 165
    .local v7, "e":Lcom/samsung/android/scloud/framework/PDMException;
    throw v7

    .line 166
    .end local v7    # "e":Lcom/samsung/android/scloud/framework/PDMException;
    :catch_3
    move-exception v7

    .line 167
    .local v7, "e":Ljava/lang/Exception;
    new-instance v18, Lcom/samsung/android/scloud/framework/PDMException;

    const/16 v19, 0x138

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-direct {v0, v1, v7}, Lcom/samsung/android/scloud/framework/PDMException;-><init>(ILjava/lang/Throwable;)V

    throw v18
.end method
