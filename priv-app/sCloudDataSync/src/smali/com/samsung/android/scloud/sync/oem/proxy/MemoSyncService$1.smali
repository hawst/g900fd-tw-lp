.class Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService$1;
.super Lcom/sec/android/sCloudSync/IDataSync/IDataSync$Stub;
.source "MemoSyncService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;)V
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService$1;->this$0:Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;

    invoke-direct {p0}, Lcom/sec/android/sCloudSync/IDataSync/IDataSync$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public cancelSync()V
    .locals 3

    .prologue
    .line 110
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService$1;->this$0:Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;

    const/4 v1, 0x1

    # setter for: Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;->isCanceled:Z
    invoke-static {v0, v1}, Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;->access$002(Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;Z)Z

    .line 112
    invoke-static {}, Lcom/samsung/android/scloud/sync/model/ModelManager;->getInstance()Lcom/samsung/android/scloud/sync/model/ModelManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService$1;->this$0:Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;

    const-string v2, "MEMO_CATE"

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/scloud/sync/model/ModelManager;->getSyncManager(Landroid/content/Context;Ljava/lang/String;)Lcom/samsung/android/scloud/sync/core/SyncManager;

    move-result-object v0

    invoke-static {}, Lcom/samsung/android/scloud/sync/model/ModelManager;->getInstance()Lcom/samsung/android/scloud/sync/model/ModelManager;

    move-result-object v1

    const-string v2, "MEMO_CATE"

    invoke-virtual {v1, v2}, Lcom/samsung/android/scloud/sync/model/ModelManager;->getModel(Ljava/lang/String;)Lcom/samsung/android/scloud/sync/model/IModel;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/android/scloud/sync/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/sync/core/SyncManager;->requestCancel(Ljava/lang/String;)V

    .line 113
    invoke-static {}, Lcom/samsung/android/scloud/sync/model/ModelManager;->getInstance()Lcom/samsung/android/scloud/sync/model/ModelManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService$1;->this$0:Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;

    const-string v2, "MEMO_DATA"

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/scloud/sync/model/ModelManager;->getSyncManager(Landroid/content/Context;Ljava/lang/String;)Lcom/samsung/android/scloud/sync/core/SyncManager;

    move-result-object v0

    invoke-static {}, Lcom/samsung/android/scloud/sync/model/ModelManager;->getInstance()Lcom/samsung/android/scloud/sync/model/ModelManager;

    move-result-object v1

    const-string v2, "MEMO_DATA"

    invoke-virtual {v1, v2}, Lcom/samsung/android/scloud/sync/model/ModelManager;->getModel(Ljava/lang/String;)Lcom/samsung/android/scloud/sync/model/IModel;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/android/scloud/sync/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/sync/core/SyncManager;->requestCancel(Ljava/lang/String;)V

    .line 114
    return-void
.end method

.method public performSync(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;)Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;
    .locals 12
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "authority"    # Ljava/lang/String;
    .param p3, "dataSyncResult"    # Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService$1;->this$0:Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;->isCanceled:Z
    invoke-static {v0, v1}, Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;->access$002(Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;Z)Z

    .line 71
    invoke-virtual {p3}, Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;->getSyncResult()Landroid/content/SyncResult;

    move-result-object v6

    .line 73
    .local v6, "syncResult":Landroid/content/SyncResult;
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService$1;->this$0:Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;

    invoke-static {v0}, Lcom/samsung/android/scloud/sync/common/MetaManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/sync/common/MetaManager;

    move-result-object v7

    .line 76
    .local v7, "meta":Lcom/samsung/android/scloud/sync/common/MetaManager;
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService$1;->this$0:Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;

    iget-object v1, p0, Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService$1;->this$0:Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;

    # getter for: Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;->MEMO_CONTENT_URI:Landroid/net/Uri;
    invoke-static {}, Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;->access$100()Landroid/net/Uri;

    move-result-object v2

    # invokes: Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;->isFirstSync(Landroid/content/Context;Landroid/net/Uri;)Z
    invoke-static {v0, v1, v2}, Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;->access$200(Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;Landroid/content/Context;Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService$1;->this$0:Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;

    iget-object v1, p0, Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService$1;->this$0:Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;

    # getter for: Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;->CATEGORY_CONTENT_URI:Landroid/net/Uri;
    invoke-static {}, Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;->access$300()Landroid/net/Uri;

    move-result-object v2

    # invokes: Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;->isFirstSync(Landroid/content/Context;Landroid/net/Uri;)Z
    invoke-static {v0, v1, v2}, Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;->access$200(Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;Landroid/content/Context;Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    const-string v0, "MEMO-SYNC"

    const-string v1, "First Sync.. initialize lastSyncTime to 0"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/framework/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    const-string v0, "MEMO_CATE"

    const-wide/16 v1, 0x0

    invoke-virtual {v7, v0, v1, v2}, Lcom/samsung/android/scloud/sync/common/MetaManager;->setLastServerSyncTime(Ljava/lang/String;J)V

    .line 79
    const-string v0, "MEMO_DATA"

    const-wide/16 v1, 0x0

    invoke-virtual {v7, v0, v1, v2}, Lcom/samsung/android/scloud/sync/common/MetaManager;->setLastServerSyncTime(Ljava/lang/String;J)V

    .line 80
    const-string v0, "MEMO_CATE"

    invoke-virtual {v7, v0}, Lcom/samsung/android/scloud/sync/common/MetaManager;->commitLastSyncTime(Ljava/lang/String;)V

    .line 81
    const-string v0, "MEMO_DATA"

    invoke-virtual {v7, v0}, Lcom/samsung/android/scloud/sync/common/MetaManager;->commitLastSyncTime(Ljava/lang/String;)V

    .line 84
    :cond_0
    const-string v0, "MEMO_CATE"

    invoke-virtual {v7, v0}, Lcom/samsung/android/scloud/sync/common/MetaManager;->initLastSyncTime(Ljava/lang/String;)V

    .line 85
    const-string v0, "MEMO_DATA"

    invoke-virtual {v7, v0}, Lcom/samsung/android/scloud/sync/common/MetaManager;->initLastSyncTime(Ljava/lang/String;)V

    .line 87
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService$1;->this$0:Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;

    # getter for: Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;->isCanceled:Z
    invoke-static {v0}, Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;->access$000(Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 88
    invoke-static {}, Lcom/samsung/android/scloud/sync/model/ModelManager;->getInstance()Lcom/samsung/android/scloud/sync/model/ModelManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService$1;->this$0:Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;

    const-string v2, "MEMO_CATE"

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/scloud/sync/model/ModelManager;->getSyncManager(Landroid/content/Context;Ljava/lang/String;)Lcom/samsung/android/scloud/sync/core/SyncManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService$1;->this$0:Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;

    const/4 v2, 0x0

    const-string v3, "MEMO_CATE"

    invoke-virtual {v7, v3}, Lcom/samsung/android/scloud/sync/common/MetaManager;->getLastServerSyncTime(Ljava/lang/String;)J

    move-result-wide v4

    move-object v3, p1

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/scloud/sync/core/SyncManager;->performSync(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLandroid/content/SyncResult;)J

    move-result-wide v8

    .line 89
    .local v8, "nextLastSyncTimeCate":J
    const-wide/16 v0, 0x0

    cmp-long v0, v8, v0

    if-lez v0, :cond_1

    .line 90
    const-string v0, "MEMO_CATE"

    invoke-virtual {v7, v0, v8, v9}, Lcom/samsung/android/scloud/sync/common/MetaManager;->setLastServerSyncTime(Ljava/lang/String;J)V

    .line 91
    const-string v0, "MEMO_CATE"

    invoke-virtual {v7, v0}, Lcom/samsung/android/scloud/sync/common/MetaManager;->commitLastSyncTime(Ljava/lang/String;)V

    .line 96
    .end local v8    # "nextLastSyncTimeCate":J
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService$1;->this$0:Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;

    # getter for: Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;->isCanceled:Z
    invoke-static {v0}, Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;->access$000(Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {v6}, Landroid/content/SyncResult;->hasError()Z

    move-result v0

    if-nez v0, :cond_2

    .line 97
    invoke-static {}, Lcom/samsung/android/scloud/sync/model/ModelManager;->getInstance()Lcom/samsung/android/scloud/sync/model/ModelManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService$1;->this$0:Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;

    const-string v2, "MEMO_DATA"

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/scloud/sync/model/ModelManager;->getSyncManager(Landroid/content/Context;Ljava/lang/String;)Lcom/samsung/android/scloud/sync/core/SyncManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService$1;->this$0:Lcom/samsung/android/scloud/sync/oem/proxy/MemoSyncService;

    const/4 v2, 0x0

    const-string v3, "MEMO_DATA"

    invoke-virtual {v7, v3}, Lcom/samsung/android/scloud/sync/common/MetaManager;->getLastServerSyncTime(Ljava/lang/String;)J

    move-result-wide v4

    move-object v3, p1

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/scloud/sync/core/SyncManager;->performSync(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLandroid/content/SyncResult;)J

    move-result-wide v10

    .line 98
    .local v10, "nextLastSyncTimeMemo":J
    const-wide/16 v0, 0x0

    cmp-long v0, v10, v0

    if-lez v0, :cond_2

    .line 99
    const-string v0, "MEMO_DATA"

    invoke-virtual {v7, v0, v10, v11}, Lcom/samsung/android/scloud/sync/common/MetaManager;->setLastServerSyncTime(Ljava/lang/String;J)V

    .line 100
    const-string v0, "MEMO_DATA"

    invoke-virtual {v7, v0}, Lcom/samsung/android/scloud/sync/common/MetaManager;->commitLastSyncTime(Ljava/lang/String;)V

    .line 105
    .end local v10    # "nextLastSyncTimeMemo":J
    :cond_2
    invoke-virtual {p3, v6}, Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;->setSyncResult(Landroid/content/SyncResult;)V

    .line 106
    return-object p3
.end method
