.class public Lcom/samsung/android/scloud/sync/oem/InternalOEMControl;
.super Ljava/lang/Object;
.source "InternalOEMControl.java"

# interfaces
.implements Lcom/samsung/android/scloud/sync/oem/IOEMControl;


# static fields
.field private static INSTANCE:Lcom/samsung/android/scloud/sync/oem/IOEMControl;

.field private static TAG:Ljava/lang/String;


# instance fields
.field private mBuilderMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/scloud/sync/model/builder/IBuilder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-string v0, "InternalOEMControl"

    sput-object v0, Lcom/samsung/android/scloud/sync/oem/InternalOEMControl;->TAG:Ljava/lang/String;

    .line 41
    new-instance v0, Lcom/samsung/android/scloud/sync/oem/InternalOEMControl;

    invoke-direct {v0}, Lcom/samsung/android/scloud/sync/oem/InternalOEMControl;-><init>()V

    sput-object v0, Lcom/samsung/android/scloud/sync/oem/InternalOEMControl;->INSTANCE:Lcom/samsung/android/scloud/sync/oem/IOEMControl;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/sync/oem/InternalOEMControl;->mBuilderMap:Ljava/util/HashMap;

    .line 47
    iget-object v1, p0, Lcom/samsung/android/scloud/sync/oem/InternalOEMControl;->mBuilderMap:Ljava/util/HashMap;

    const-string v2, "MEMO_DATA"

    new-instance v3, Lcom/samsung/android/scloud/sync/model/builder/MemoBuilder;

    invoke-static {}, Lcom/samsung/android/scloud/sync/model/ModelManager;->getInstance()Lcom/samsung/android/scloud/sync/model/ModelManager;

    move-result-object v0

    const-string v4, "MEMO_DATA"

    invoke-virtual {v0, v4}, Lcom/samsung/android/scloud/sync/model/ModelManager;->getModel(Ljava/lang/String;)Lcom/samsung/android/scloud/sync/model/IModel;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/sync/model/IInternalModel;

    invoke-direct {v3, v0}, Lcom/samsung/android/scloud/sync/model/builder/MemoBuilder;-><init>(Lcom/samsung/android/scloud/sync/model/IInternalModel;)V

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    iget-object v1, p0, Lcom/samsung/android/scloud/sync/oem/InternalOEMControl;->mBuilderMap:Ljava/util/HashMap;

    const-string v2, "MEMO_CATE"

    new-instance v3, Lcom/samsung/android/scloud/sync/model/builder/CategoryBuilder;

    invoke-static {}, Lcom/samsung/android/scloud/sync/model/ModelManager;->getInstance()Lcom/samsung/android/scloud/sync/model/ModelManager;

    move-result-object v0

    const-string v4, "MEMO_CATE"

    invoke-virtual {v0, v4}, Lcom/samsung/android/scloud/sync/model/ModelManager;->getModel(Ljava/lang/String;)Lcom/samsung/android/scloud/sync/model/IModel;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/sync/model/IInternalModel;

    invoke-direct {v3, v0}, Lcom/samsung/android/scloud/sync/model/builder/CategoryBuilder;-><init>(Lcom/samsung/android/scloud/sync/model/IInternalModel;)V

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    return-void
.end method

.method public static getInstance()Lcom/samsung/android/scloud/sync/oem/IOEMControl;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/samsung/android/scloud/sync/oem/InternalOEMControl;->INSTANCE:Lcom/samsung/android/scloud/sync/oem/IOEMControl;

    return-object v0
.end method


# virtual methods
.method public complete(Landroid/content/Context;Lcom/samsung/android/scloud/sync/model/IModel;Lcom/samsung/android/scloud/sync/data/SyncItem;I)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "model"    # Lcom/samsung/android/scloud/sync/model/IModel;
    .param p3, "item"    # Lcom/samsung/android/scloud/sync/data/SyncItem;
    .param p4, "rcode"    # I

    .prologue
    .line 88
    sget-object v0, Lcom/samsung/android/scloud/sync/oem/InternalOEMControl;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "complete : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/oem/InternalOEMControl;->mBuilderMap:Ljava/util/HashMap;

    invoke-interface {p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/sync/model/builder/IBuilder;

    invoke-virtual {v0, p1, p3, p4}, Lcom/samsung/android/scloud/sync/model/builder/IBuilder;->complete(Landroid/content/Context;Lcom/samsung/android/scloud/sync/data/SyncItem;I)Z

    move-result v0

    return v0
.end method

.method public deleteLocal(Landroid/content/Context;Lcom/samsung/android/scloud/sync/model/IModel;Ljava/lang/String;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "model"    # Lcom/samsung/android/scloud/sync/model/IModel;
    .param p3, "localId"    # Ljava/lang/String;

    .prologue
    .line 82
    sget-object v0, Lcom/samsung/android/scloud/sync/oem/InternalOEMControl;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "deleteLocal : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/oem/InternalOEMControl;->mBuilderMap:Ljava/util/HashMap;

    invoke-interface {p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/sync/model/builder/IBuilder;

    invoke-virtual {v0, p1, p3}, Lcom/samsung/android/scloud/sync/model/builder/IBuilder;->deleteLocal(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public getAttachmentFileInfo(Landroid/content/Context;Lcom/samsung/android/scloud/sync/model/IModel;Ljava/lang/String;)Lcom/samsung/android/scloud/sync/data/Attachments;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "model"    # Lcom/samsung/android/scloud/sync/model/IModel;
    .param p3, "localId"    # Ljava/lang/String;

    .prologue
    .line 64
    sget-object v0, Lcom/samsung/android/scloud/sync/oem/InternalOEMControl;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getAttachmentFileInfo : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/oem/InternalOEMControl;->mBuilderMap:Ljava/util/HashMap;

    invoke-interface {p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/sync/model/builder/IBuilder;

    invoke-virtual {v0, p1, p3}, Lcom/samsung/android/scloud/sync/model/builder/IBuilder;->getAttachmentFileInfo(Landroid/content/Context;Ljava/lang/String;)Lcom/samsung/android/scloud/sync/data/Attachments;

    move-result-object v0

    return-object v0
.end method

.method public getLocalChange(Landroid/content/Context;Lcom/samsung/android/scloud/sync/model/IModel;Lcom/samsung/android/scloud/sync/data/SyncItem;Lcom/samsung/android/scloud/sync/data/Attachments;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "model"    # Lcom/samsung/android/scloud/sync/model/IModel;
    .param p3, "item"    # Lcom/samsung/android/scloud/sync/data/SyncItem;
    .param p4, "attachments"    # Lcom/samsung/android/scloud/sync/data/Attachments;

    .prologue
    .line 70
    sget-object v0, Lcom/samsung/android/scloud/sync/oem/InternalOEMControl;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getLocalChange : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/oem/InternalOEMControl;->mBuilderMap:Ljava/util/HashMap;

    invoke-interface {p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/sync/model/builder/IBuilder;

    invoke-virtual {p3}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getLocalId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, p1, p3}, Lcom/samsung/android/scloud/sync/model/IModel;->getLocalFilePathPrefix(Landroid/content/Context;Lcom/samsung/android/scloud/sync/data/SyncItem;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p1, v1, p4, v2}, Lcom/samsung/android/scloud/sync/model/builder/IBuilder;->getLocalChange(Landroid/content/Context;Ljava/lang/String;Lcom/samsung/android/scloud/sync/data/Attachments;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public prepareToSync(Landroid/content/Context;Lcom/samsung/android/scloud/sync/model/IModel;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "model"    # Lcom/samsung/android/scloud/sync/model/IModel;
    .param p4, "accountType"    # Ljava/lang/String;
    .param p5, "accountName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/samsung/android/scloud/sync/model/IModel;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/scloud/sync/data/SyncItem;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/scloud/sync/data/SyncItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58
    .local p3, "serverChanges":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/samsung/android/scloud/sync/data/SyncItem;>;"
    sget-object v0, Lcom/samsung/android/scloud/sync/oem/InternalOEMControl;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "prepareToSync : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/oem/InternalOEMControl;->mBuilderMap:Ljava/util/HashMap;

    invoke-interface {p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/sync/model/builder/IBuilder;

    invoke-virtual {v0, p1, p3, p4, p5}, Lcom/samsung/android/scloud/sync/model/builder/IBuilder;->prepareToSync(Landroid/content/Context;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public updateLocal(Landroid/content/Context;Lcom/samsung/android/scloud/sync/model/IModel;Lcom/samsung/android/scloud/sync/data/SyncItem;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "model"    # Lcom/samsung/android/scloud/sync/model/IModel;
    .param p3, "item"    # Lcom/samsung/android/scloud/sync/data/SyncItem;
    .param p6, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/samsung/android/scloud/sync/model/IModel;",
            "Lcom/samsung/android/scloud/sync/data/SyncItem;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 77
    .local p4, "toDownloadAttFileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p5, "toDeleteAttFileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/oem/InternalOEMControl;->mBuilderMap:Ljava/util/HashMap;

    invoke-interface {p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/sync/model/builder/IBuilder;

    move-object v1, p1

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/scloud/sync/model/builder/IBuilder;->updateLocal(Landroid/content/Context;Lcom/samsung/android/scloud/sync/data/SyncItem;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
