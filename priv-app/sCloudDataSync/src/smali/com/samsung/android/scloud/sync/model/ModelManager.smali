.class public Lcom/samsung/android/scloud/sync/model/ModelManager;
.super Ljava/lang/Object;
.source "ModelManager.java"


# static fields
.field private static INSTANCE:Lcom/samsung/android/scloud/sync/model/ModelManager;


# instance fields
.field private mModelMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/scloud/sync/model/IModel;",
            ">;"
        }
    .end annotation
.end field

.field private mSyncManagerMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/scloud/sync/core/SyncManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 3

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/sync/model/ModelManager;->mModelMap:Ljava/util/Map;

    .line 27
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/model/ModelManager;->mModelMap:Ljava/util/Map;

    const-string v1, "MEMO_DATA"

    new-instance v2, Lcom/samsung/android/scloud/sync/model/Memo;

    invoke-direct {v2}, Lcom/samsung/android/scloud/sync/model/Memo;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/model/ModelManager;->mModelMap:Ljava/util/Map;

    const-string v1, "MEMO_CATE"

    new-instance v2, Lcom/samsung/android/scloud/sync/model/Category;

    invoke-direct {v2}, Lcom/samsung/android/scloud/sync/model/Category;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/model/ModelManager;->mModelMap:Ljava/util/Map;

    const-string v1, "S-NOTE3"

    new-instance v2, Lcom/samsung/android/scloud/sync/model/SNote4;

    invoke-direct {v2}, Lcom/samsung/android/scloud/sync/model/SNote4;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/sync/model/ModelManager;->mSyncManagerMap:Ljava/util/Map;

    .line 32
    return-void
.end method

.method public static getInstance()Lcom/samsung/android/scloud/sync/model/ModelManager;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/samsung/android/scloud/sync/model/ModelManager;->INSTANCE:Lcom/samsung/android/scloud/sync/model/ModelManager;

    if-nez v0, :cond_0

    .line 18
    new-instance v0, Lcom/samsung/android/scloud/sync/model/ModelManager;

    invoke-direct {v0}, Lcom/samsung/android/scloud/sync/model/ModelManager;-><init>()V

    sput-object v0, Lcom/samsung/android/scloud/sync/model/ModelManager;->INSTANCE:Lcom/samsung/android/scloud/sync/model/ModelManager;

    .line 20
    :cond_0
    sget-object v0, Lcom/samsung/android/scloud/sync/model/ModelManager;->INSTANCE:Lcom/samsung/android/scloud/sync/model/ModelManager;

    return-object v0
.end method


# virtual methods
.method public getModel(Ljava/lang/String;)Lcom/samsung/android/scloud/sync/model/IModel;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/model/ModelManager;->mModelMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/sync/model/IModel;

    return-object v0
.end method

.method public getSyncManager(Landroid/content/Context;Ljava/lang/String;)Lcom/samsung/android/scloud/sync/core/SyncManager;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/model/ModelManager;->mSyncManagerMap:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 40
    iget-object v1, p0, Lcom/samsung/android/scloud/sync/model/ModelManager;->mSyncManagerMap:Ljava/util/Map;

    new-instance v2, Lcom/samsung/android/scloud/sync/core/SyncManager;

    iget-object v0, p0, Lcom/samsung/android/scloud/sync/model/ModelManager;->mModelMap:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/sync/model/IModel;

    invoke-direct {v2, p1, v0}, Lcom/samsung/android/scloud/sync/core/SyncManager;-><init>(Landroid/content/Context;Lcom/samsung/android/scloud/sync/model/IModel;)V

    invoke-interface {v1, p2, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/model/ModelManager;->mSyncManagerMap:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/sync/core/SyncManager;

    return-object v0
.end method

.method public initSyncManager(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/model/ModelManager;->mSyncManagerMap:Ljava/util/Map;

    const-string v1, "MEMO_DATA"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 48
    iget-object v1, p0, Lcom/samsung/android/scloud/sync/model/ModelManager;->mSyncManagerMap:Ljava/util/Map;

    const-string v2, "MEMO_DATA"

    new-instance v3, Lcom/samsung/android/scloud/sync/core/SyncManager;

    iget-object v0, p0, Lcom/samsung/android/scloud/sync/model/ModelManager;->mModelMap:Ljava/util/Map;

    const-string v4, "MEMO_DATA"

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/sync/model/IModel;

    invoke-direct {v3, p1, v0}, Lcom/samsung/android/scloud/sync/core/SyncManager;-><init>(Landroid/content/Context;Lcom/samsung/android/scloud/sync/model/IModel;)V

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/model/ModelManager;->mSyncManagerMap:Ljava/util/Map;

    const-string v1, "MEMO_CATE"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 51
    iget-object v1, p0, Lcom/samsung/android/scloud/sync/model/ModelManager;->mSyncManagerMap:Ljava/util/Map;

    const-string v2, "MEMO_CATE"

    new-instance v3, Lcom/samsung/android/scloud/sync/core/SyncManager;

    iget-object v0, p0, Lcom/samsung/android/scloud/sync/model/ModelManager;->mModelMap:Ljava/util/Map;

    const-string v4, "MEMO_CATE"

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/sync/model/IModel;

    invoke-direct {v3, p1, v0}, Lcom/samsung/android/scloud/sync/core/SyncManager;-><init>(Landroid/content/Context;Lcom/samsung/android/scloud/sync/model/IModel;)V

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/model/ModelManager;->mSyncManagerMap:Ljava/util/Map;

    const-string v1, "S-NOTE3"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 54
    iget-object v1, p0, Lcom/samsung/android/scloud/sync/model/ModelManager;->mSyncManagerMap:Ljava/util/Map;

    const-string v2, "S-NOTE3"

    new-instance v3, Lcom/samsung/android/scloud/sync/core/SyncManager;

    iget-object v0, p0, Lcom/samsung/android/scloud/sync/model/ModelManager;->mModelMap:Ljava/util/Map;

    const-string v4, "S-NOTE3"

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/sync/model/IModel;

    invoke-direct {v3, p1, v0}, Lcom/samsung/android/scloud/sync/core/SyncManager;-><init>(Landroid/content/Context;Lcom/samsung/android/scloud/sync/model/IModel;)V

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    :cond_2
    return-void
.end method

.method public registerModel(Lcom/samsung/android/scloud/sync/model/IModel;)V
    .locals 2
    .param p1, "model"    # Lcom/samsung/android/scloud/sync/model/IModel;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/model/ModelManager;->mModelMap:Ljava/util/Map;

    invoke-interface {p1}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/model/ModelManager;->mModelMap:Ljava/util/Map;

    invoke-interface {p1}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    :cond_0
    return-void
.end method
