.class public final Lcom/samsung/android/scloud/framework/util/TimeManager;
.super Ljava/lang/Object;
.source "TimeManager.java"


# static fields
.field private static final PREVIOUS_ELAPSED_TIME:Ljava/lang/String; = "PREVIOUS_ELAPSED_TIME"

.field private static final PREVIOUS_SYS_TIME:Ljava/lang/String; = "PREVIOUS_SYS_TIME"

.field private static final TAG:Ljava/lang/String; = " Time Manager "

.field private static final TIME_DIFFERENCE:Ljava/lang/String; = "TIME_DIFFERENCE"

.field private static sTimeManager:Lcom/samsung/android/scloud/framework/util/TimeManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/scloud/framework/util/TimeManager;->sTimeManager:Lcom/samsung/android/scloud/framework/util/TimeManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    return-void
.end method

.method public static declared-synchronized create()Lcom/samsung/android/scloud/framework/util/TimeManager;
    .locals 2

    .prologue
    .line 47
    const-class v1, Lcom/samsung/android/scloud/framework/util/TimeManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/scloud/framework/util/TimeManager;->sTimeManager:Lcom/samsung/android/scloud/framework/util/TimeManager;

    if-nez v0, :cond_0

    .line 48
    new-instance v0, Lcom/samsung/android/scloud/framework/util/TimeManager;

    invoke-direct {v0}, Lcom/samsung/android/scloud/framework/util/TimeManager;-><init>()V

    sput-object v0, Lcom/samsung/android/scloud/framework/util/TimeManager;->sTimeManager:Lcom/samsung/android/scloud/framework/util/TimeManager;

    .line 50
    :cond_0
    sget-object v0, Lcom/samsung/android/scloud/framework/util/TimeManager;->sTimeManager:Lcom/samsung/android/scloud/framework/util/TimeManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 47
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static getCurrentTime(Landroid/content/Context;)J
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 136
    const-wide/16 v1, 0x0

    .line 138
    .local v1, "uTimeDifference":J
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "TIME_DIFFERENCE"

    invoke-static {v3, v4}, Landroid/provider/Settings$System;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v1

    .line 148
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    sub-long/2addr v3, v1

    return-wide v3

    .line 139
    :catch_0
    move-exception v0

    .line 140
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    const-string v3, " Time Manager "

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Time Difference not stored. "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/framework/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    const-wide/16 v1, 0x0

    goto :goto_0
.end method

.method private declared-synchronized updateSettings(Landroid/content/Context;JJJ)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "time_difference"    # J
    .param p4, "currentSystemTime"    # J
    .param p6, "initialElapsedTime"    # J

    .prologue
    .line 61
    monitor-enter p0

    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.samsungcloudsync.DELTA_TIME_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 62
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "TimeDifference"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 63
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 65
    const-string v1, "com.samsung.android.scloud.sync.DELTA_TIME_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 66
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 68
    const-string v1, " Time Manager "

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Time Difference stored. TIME_DIFFERENCE : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", PREVIOUS_SYS_TIME : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", PREVIOUS_ELAPSED_TIME"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p6, p7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/framework/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "TIME_DIFFERENCE"

    invoke-static {v1, v2, p2, p3}, Landroid/provider/Settings$System;->putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    .line 70
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "PREVIOUS_SYS_TIME"

    invoke-static {v1, v2, p4, p5}, Landroid/provider/Settings$System;->putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    .line 71
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "PREVIOUS_ELAPSED_TIME"

    invoke-static {v1, v2, p6, p7}, Landroid/provider/Settings$System;->putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 72
    monitor-exit p0

    return-void

    .line 61
    .end local v0    # "intent":Landroid/content/Intent;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method


# virtual methods
.method public declared-synchronized updateSettings(Landroid/content/Context;JJ)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "currentSystemTime"    # J
    .param p4, "initialElapsedTime"    # J

    .prologue
    .line 54
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "PREVIOUS_SYS_TIME"

    invoke-static {v0, v1, p2, p3}, Landroid/provider/Settings$System;->putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    .line 55
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "PREVIOUS_ELAPSED_TIME"

    invoke-static {v0, v1, p4, p5}, Landroid/provider/Settings$System;->putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 56
    monitor-exit p0

    return-void

    .line 54
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized updateSettingsUsingServer(Landroid/content/Context;J)V
    .locals 15
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "serverTimeStamp"    # J

    .prologue
    .line 111
    monitor-enter p0

    :try_start_0
    const-string v2, " Time Manager "

    const-string v3, " Inside the updateSettingsUsingServer method"

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 115
    .local v10, "currentTimeStamp_system":J
    const-string v2, " Time Manager "

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "The received server time is "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, p2

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 118
    :try_start_1
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "TIME_DIFFERENCE"

    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;)J
    :try_end_1
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v13

    .line 124
    .local v13, "timeDifference":J
    :goto_0
    const-wide/16 v4, 0x0

    .line 125
    .local v4, "newTimeDifference":J
    const-wide/16 v2, 0x0

    cmp-long v2, p2, v2

    if-lez v2, :cond_0

    .line 126
    sub-long v4, v10, p2

    .line 128
    :cond_0
    cmp-long v2, v13, v4

    if-eqz v2, :cond_1

    .line 129
    :try_start_2
    const-string v2, " Time Manager "

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "The new TIME_DIFFERENCE is "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    move-object v2, p0

    move-object/from16 v3, p1

    invoke-direct/range {v2 .. v9}, Lcom/samsung/android/scloud/framework/util/TimeManager;->updateSettings(Landroid/content/Context;JJJ)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 133
    :cond_1
    monitor-exit p0

    return-void

    .line 119
    .end local v4    # "newTimeDifference":J
    .end local v13    # "timeDifference":J
    :catch_0
    move-exception v12

    .line 120
    .local v12, "e":Landroid/provider/Settings$SettingNotFoundException;
    :try_start_3
    const-string v2, " Time Manager "

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Time Difference not stored. "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v12}, Landroid/provider/Settings$SettingNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 121
    const-wide/16 v13, 0x0

    .restart local v13    # "timeDifference":J
    goto :goto_0

    .line 111
    .end local v10    # "currentTimeStamp_system":J
    .end local v12    # "e":Landroid/provider/Settings$SettingNotFoundException;
    .end local v13    # "timeDifference":J
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized updateTimeSettings(Landroid/content/Context;JJ)V
    .locals 17
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "newSystemTime"    # J
    .param p4, "newElapsedTime"    # J

    .prologue
    .line 82
    monitor-enter p0

    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "PREVIOUS_SYS_TIME"

    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;)J

    move-result-wide v13

    .line 83
    .local v13, "startSystemTime":J
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "PREVIOUS_ELAPSED_TIME"

    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;)J

    move-result-wide v9

    .line 84
    .local v9, "oldElapsedTime":J
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "TIME_DIFFERENCE"

    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v11

    .line 89
    .local v11, "oldTimeDifference":J
    sub-long v0, p2, v13

    sub-long v4, p4, v9

    sub-long v15, v0, v4

    .line 90
    .local v15, "temp":J
    const-wide/16 v0, 0x0

    cmp-long v0, v15, v0

    if-nez v0, :cond_0

    .line 104
    .end local v9    # "oldElapsedTime":J
    .end local v11    # "oldTimeDifference":J
    .end local v13    # "startSystemTime":J
    .end local v15    # "temp":J
    :goto_0
    monitor-exit p0

    return-void

    .line 93
    .restart local v9    # "oldElapsedTime":J
    .restart local v11    # "oldTimeDifference":J
    .restart local v13    # "startSystemTime":J
    .restart local v15    # "temp":J
    :cond_0
    add-long v2, v11, v15

    .line 99
    .local v2, "newTimeDifference":J
    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/samsung/android/scloud/framework/util/TimeManager;->updateSettings(Landroid/content/Context;JJJ)V
    :try_end_1
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 101
    .end local v2    # "newTimeDifference":J
    .end local v9    # "oldElapsedTime":J
    .end local v11    # "oldTimeDifference":J
    .end local v13    # "startSystemTime":J
    .end local v15    # "temp":J
    :catch_0
    move-exception v8

    .line 102
    .local v8, "e":Landroid/provider/Settings$SettingNotFoundException;
    :try_start_2
    const-string v0, " Time Manager "

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Time Difference not stored. "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v8}, Landroid/provider/Settings$SettingNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/framework/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 82
    .end local v8    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
