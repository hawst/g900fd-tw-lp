.class Lcom/samsung/android/scloud/sync/model/builder/MemoBuilder$1;
.super Ljava/lang/Object;
.source "MemoBuilder.java"

# interfaces
.implements Lcom/samsung/android/scloud/framework/network/NetworkUtil$PDMProgressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/sync/model/builder/MemoBuilder;->getLocalChange(Landroid/content/Context;Ljava/lang/String;Lcom/samsung/android/scloud/sync/data/Attachments;Ljava/lang/String;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/sync/model/builder/MemoBuilder;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/sync/model/builder/MemoBuilder;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/samsung/android/scloud/sync/model/builder/MemoBuilder$1;->this$0:Lcom/samsung/android/scloud/sync/model/builder/MemoBuilder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public transferred(JJ)V
    .locals 3
    .param p1, "now"    # J
    .param p3, "total"    # J

    .prologue
    .line 113
    const-string v0, "MemoBuilder"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "transferred : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    return-void
.end method
