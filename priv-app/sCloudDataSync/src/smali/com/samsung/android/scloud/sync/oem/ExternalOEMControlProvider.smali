.class public Lcom/samsung/android/scloud/sync/oem/ExternalOEMControlProvider;
.super Landroid/content/ContentProvider;
.source "ExternalOEMControlProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/scloud/sync/oem/ExternalOEMControlProvider$Key;
    }
.end annotation


# static fields
.field static final AUTHORITY:Ljava/lang/String; = "com.samsung.android.scloud.sync"

.field static final FILE_URI:Ljava/lang/String; = "content://com.samsung.android.scloud.sync"

.field static final REGISTER:Ljava/lang/String; = "register"

.field static final REQUEST_CANCEL:Ljava/lang/String; = "request_cancel"

.field static final REQUEST_SYNC:Ljava/lang/String; = "request_sync"

.field private static TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-string v0, "ExternalOEMControlProvider"

    sput-object v0, Lcom/samsung/android/scloud/sync/oem/ExternalOEMControlProvider;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 30
    return-void
.end method


# virtual methods
.method public call(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 12
    .param p1, "method"    # Ljava/lang/String;
    .param p2, "arg"    # Ljava/lang/String;
    .param p3, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 80
    sget-object v0, Lcom/samsung/android/scloud/sync/oem/ExternalOEMControlProvider;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "call !!  method : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    const-string v0, "request_sync"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 83
    const-string v0, "name"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 84
    .local v8, "modelName":Ljava/lang/String;
    const-string v0, "account_name"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 85
    .local v3, "accountName":Ljava/lang/String;
    const-string v0, "last_sync_time"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 86
    .local v4, "lastSyncTime":J
    const-string v0, "sync_result"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v6

    check-cast v6, Landroid/content/SyncResult;

    .line 88
    .local v6, "syncResult":Landroid/content/SyncResult;
    sget-object v0, Lcom/samsung/android/scloud/sync/oem/ExternalOEMControlProvider;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "call !!  REQUEST_SYNC : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    invoke-static {}, Lcom/samsung/android/scloud/sync/model/ModelManager;->getInstance()Lcom/samsung/android/scloud/sync/model/ModelManager;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/samsung/android/scloud/sync/model/ModelManager;->getModel(Ljava/lang/String;)Lcom/samsung/android/scloud/sync/model/IModel;

    move-result-object v0

    if-nez v0, :cond_0

    .line 92
    new-instance v7, Lcom/samsung/android/scloud/sync/oem/ExternalOEMControlProvider$1;

    invoke-direct {v7, p0, p3, v8}, Lcom/samsung/android/scloud/sync/oem/ExternalOEMControlProvider$1;-><init>(Lcom/samsung/android/scloud/sync/oem/ExternalOEMControlProvider;Landroid/os/Bundle;Ljava/lang/String;)V

    .line 124
    .local v7, "model":Lcom/samsung/android/scloud/sync/model/IModel;
    sget-object v0, Lcom/samsung/android/scloud/sync/oem/ExternalOEMControlProvider;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "register : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    invoke-static {}, Lcom/samsung/android/scloud/sync/model/ModelManager;->getInstance()Lcom/samsung/android/scloud/sync/model/ModelManager;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/samsung/android/scloud/sync/model/ModelManager;->registerModel(Lcom/samsung/android/scloud/sync/model/IModel;)V

    .line 129
    .end local v7    # "model":Lcom/samsung/android/scloud/sync/model/IModel;
    :cond_0
    invoke-static {}, Lcom/samsung/android/scloud/sync/model/ModelManager;->getInstance()Lcom/samsung/android/scloud/sync/model/ModelManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/android/scloud/sync/oem/ExternalOEMControlProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, v8}, Lcom/samsung/android/scloud/sync/model/ModelManager;->getSyncManager(Landroid/content/Context;Ljava/lang/String;)Lcom/samsung/android/scloud/sync/core/SyncManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/android/scloud/sync/oem/ExternalOEMControlProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/scloud/sync/core/SyncManager;->performSync(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLandroid/content/SyncResult;)J

    move-result-wide v10

    .line 131
    .local v10, "nextLastSyncTime":J
    const-string v0, "sync_result"

    invoke-virtual {p3, v0, v6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 132
    const-string v0, "last_sync_time"

    invoke-virtual {p3, v0, v10, v11}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 139
    .end local v3    # "accountName":Ljava/lang/String;
    .end local v4    # "lastSyncTime":J
    .end local v6    # "syncResult":Landroid/content/SyncResult;
    .end local v8    # "modelName":Ljava/lang/String;
    .end local v10    # "nextLastSyncTime":J
    :cond_1
    :goto_0
    return-object p3

    .line 134
    :cond_2
    const-string v0, "request_cancel"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 135
    const-string v0, "name"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 137
    .local v9, "name":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/scloud/sync/model/ModelManager;->getInstance()Lcom/samsung/android/scloud/sync/model/ModelManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/android/scloud/sync/oem/ExternalOEMControlProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, v9}, Lcom/samsung/android/scloud/sync/model/ModelManager;->getSyncManager(Landroid/content/Context;Ljava/lang/String;)Lcom/samsung/android/scloud/sync/core/SyncManager;

    move-result-object v0

    invoke-static {}, Lcom/samsung/android/scloud/sync/model/ModelManager;->getInstance()Lcom/samsung/android/scloud/sync/model/ModelManager;

    move-result-object v1

    invoke-virtual {v1, v9}, Lcom/samsung/android/scloud/sync/model/ModelManager;->getModel(Ljava/lang/String;)Lcom/samsung/android/scloud/sync/model/IModel;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/android/scloud/sync/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/sync/core/SyncManager;->requestCancel(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 43
    const/4 v0, 0x0

    return v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 49
    const/4 v0, 0x0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 55
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()Z
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    return v0
.end method

.method public openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 10
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "mode"    # Ljava/lang/String;

    .prologue
    .line 144
    sget-object v7, Lcom/samsung/android/scloud/sync/oem/ExternalOEMControlProvider;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "openFile !!  uri : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", mode : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    const/4 v2, 0x0

    .line 146
    .local v2, "fd":Landroid/os/ParcelFileDescriptor;
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    .line 147
    .local v4, "filename":Ljava/lang/String;
    sget-object v7, Lcom/samsung/android/scloud/sync/oem/ExternalOEMControlProvider;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "filename !!  uri : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 150
    :cond_0
    new-instance v7, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v7}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v7

    .line 153
    :cond_1
    invoke-static {v4}, Lcom/samsung/android/scloud/framework/util/PDMUtil;->ensureValidFileName(Ljava/lang/String;)V

    .line 155
    invoke-virtual {p0}, Lcom/samsung/android/scloud/sync/oem/ExternalOEMControlProvider;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    .line 156
    .local v0, "dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_2

    .line 157
    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    move-result v6

    .line 158
    .local v6, "isDirectoryMade":Z
    if-nez v6, :cond_2

    .line 159
    sget-object v7, Lcom/samsung/android/scloud/sync/oem/ExternalOEMControlProvider;->TAG:Ljava/lang/String;

    const-string v8, "mkdir() failed "

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/samsung/android/scloud/framework/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 161
    .end local v6    # "isDirectoryMade":Z
    :cond_2
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 162
    .local v5, "filepath":Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 165
    .local v3, "file":Ljava/io/File;
    sget-object v7, Lcom/samsung/android/scloud/sync/oem/ExternalOEMControlProvider;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "openFile result local file : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    const/high16 v7, 0x38000000

    :try_start_0
    invoke-static {v3, v7}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 172
    :goto_0
    return-object v2

    .line 169
    :catch_0
    move-exception v1

    .line 170
    .local v1, "e":Ljava/io/FileNotFoundException;
    sget-object v7, Lcom/samsung/android/scloud/sync/oem/ExternalOEMControlProvider;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Unable to open file "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v1}, Lcom/samsung/android/scloud/framework/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 68
    const/4 v0, 0x0

    return-object v0
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 75
    const/4 v0, 0x0

    return v0
.end method
