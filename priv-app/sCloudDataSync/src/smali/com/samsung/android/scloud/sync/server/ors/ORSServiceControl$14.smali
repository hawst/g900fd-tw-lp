.class Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$14;
.super Ljava/lang/Object;
.source "ORSServiceControl.java"

# interfaces
.implements Lcom/samsung/android/scloud/framework/network/NetworkUtil$FileResponseHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->downloadItem(Lcom/samsung/android/scloud/sync/data/SyncItem;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

.field final synthetic val$item:Lcom/samsung/android/scloud/sync/data/SyncItem;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;Lcom/samsung/android/scloud/sync/data/SyncItem;)V
    .locals 0

    .prologue
    .line 480
    iput-object p1, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$14;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    iput-object p2, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$14;->val$item:Lcom/samsung/android/scloud/sync/data/SyncItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleResponse(Lorg/apache/http/Header;Ljava/lang/String;JLjava/io/InputStream;)V
    .locals 8
    .param p1, "header"    # Lorg/apache/http/Header;
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "size"    # J
    .param p5, "stream"    # Ljava/io/InputStream;

    .prologue
    const/4 v7, 0x4

    .line 483
    iget-object v1, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$14;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    # getter for: Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mListener:Lcom/samsung/android/scloud/framework/IStatusListener;
    invoke-static {v1}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->access$100(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Lcom/samsung/android/scloud/framework/IStatusListener;

    move-result-object v1

    const-string v2, "ORSServiceControl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "handleFileStringResponse Finished - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$14;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    # getter for: Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mCtid:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->access$000(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v7, v2, v3}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 485
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$14;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    # getter for: Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;
    invoke-static {v2}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->access$300(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Lcom/samsung/android/scloud/sync/model/IModel;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$14;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    # getter for: Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->access$200(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$14;->val$item:Lcom/samsung/android/scloud/sync/data/SyncItem;

    invoke-interface {v2, v3, v4}, Lcom/samsung/android/scloud/sync/model/IModel;->getLocalFilePathPrefix(Landroid/content/Context;Lcom/samsung/android/scloud/sync/data/SyncItem;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "content.sync"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$14$1;

    invoke-direct {v2, p0}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$14$1;-><init>(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$14;)V

    invoke-static {p5, p3, p4, v1, v2}, Lcom/samsung/android/scloud/framework/util/FileTool;->writeToFile(Ljava/io/InputStream;JLjava/lang/String;Lcom/samsung/android/scloud/framework/network/NetworkUtil$PDMProgressListener;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 497
    iget-object v1, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$14;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    # getter for: Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mListener:Lcom/samsung/android/scloud/framework/IStatusListener;
    invoke-static {v1}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->access$100(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Lcom/samsung/android/scloud/framework/IStatusListener;

    move-result-object v1

    const-string v2, "ORSServiceControl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "downloadFile fileWrite Finished - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$14;->val$item:Lcom/samsung/android/scloud/sync/data/SyncItem;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", file : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$14;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    # getter for: Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;
    invoke-static {v4}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->access$300(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Lcom/samsung/android/scloud/sync/model/IModel;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$14;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    # getter for: Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->access$200(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$14;->val$item:Lcom/samsung/android/scloud/sync/data/SyncItem;

    invoke-interface {v4, v5, v6}, Lcom/samsung/android/scloud/sync/model/IModel;->getLocalFilePathPrefix(Landroid/content/Context;Lcom/samsung/android/scloud/sync/data/SyncItem;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "content.sync"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v7, v2, v3}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 500
    return-void

    .line 493
    :catch_0
    move-exception v0

    .line 494
    .local v0, "e":Ljava/io/IOException;
    iget-object v1, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$14;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    # getter for: Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mListener:Lcom/samsung/android/scloud/framework/IStatusListener;
    invoke-static {v1}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->access$100(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Lcom/samsung/android/scloud/framework/IStatusListener;

    move-result-object v1

    const/4 v2, 0x6

    const-string v3, "ORSServiceControl"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "downloadFile fileWrite err - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$14;->val$item:Lcom/samsung/android/scloud/sync/data/SyncItem;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", file : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$14;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    # getter for: Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;
    invoke-static {v5}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->access$300(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Lcom/samsung/android/scloud/sync/model/IModel;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$14;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    # getter for: Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->access$200(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Landroid/content/Context;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$14;->val$item:Lcom/samsung/android/scloud/sync/data/SyncItem;

    invoke-interface {v5, v6, v7}, Lcom/samsung/android/scloud/sync/model/IModel;->getLocalFilePathPrefix(Landroid/content/Context;Lcom/samsung/android/scloud/sync/data/SyncItem;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "content.sync"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v2, v3, v4}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 495
    new-instance v1, Lcom/samsung/android/scloud/framework/PDMException;

    const/16 v2, 0x133

    invoke-direct {v1, v2, v0}, Lcom/samsung/android/scloud/framework/PDMException;-><init>(ILjava/lang/Throwable;)V

    throw v1
.end method
