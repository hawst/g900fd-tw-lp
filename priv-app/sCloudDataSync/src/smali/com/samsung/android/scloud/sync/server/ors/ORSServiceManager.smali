.class Lcom/samsung/android/scloud/sync/server/ors/ORSServiceManager;
.super Ljava/lang/Object;
.source "ORSServiceManager.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ORSServiceManager"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static deleteFile(Landroid/content/Context;Lcom/samsung/android/scloud/auth/AuthManager;Ljava/lang/String;ILcom/samsung/android/scloud/sync/model/IModel;Lcom/samsung/android/scloud/sync/data/SyncItem;Ljava/lang/String;ILjava/lang/String;Lcom/samsung/android/scloud/framework/network/NetworkUtil$StringResponseHandler;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "auth"    # Lcom/samsung/android/scloud/auth/AuthManager;
    .param p2, "txKey"    # Ljava/lang/String;
    .param p3, "txSeq"    # I
    .param p4, "model"    # Lcom/samsung/android/scloud/sync/model/IModel;
    .param p5, "item"    # Lcom/samsung/android/scloud/sync/data/SyncItem;
    .param p6, "fileName"    # Ljava/lang/String;
    .param p7, "revision"    # I
    .param p8, "ctid"    # Ljava/lang/String;
    .param p9, "handler"    # Lcom/samsung/android/scloud/framework/network/NetworkUtil$StringResponseHandler;

    .prologue
    const/4 v5, 0x0

    .line 296
    const-string v2, "ORSServiceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "deleteFile!!!!!!!!!! - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", key : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p5}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getSyncKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/samsung/android/scloud/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 301
    .local v1, "url":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Lcom/samsung/android/scloud/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    .line 302
    const-string v2, "ORSServiceManager"

    const-string v3, "There is NO Base URL."

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    new-instance v2, Lcom/samsung/android/scloud/framework/PDMException;

    const/16 v3, 0x131

    invoke-direct {v2, v3}, Lcom/samsung/android/scloud/framework/PDMException;-><init>(I)V

    throw v2

    .line 306
    :cond_0
    const-string v2, "/ors/v2/tx/rm/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 307
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 308
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p4, p5}, Lcom/samsung/android/scloud/sync/model/IModel;->getServerFilePathPrefix(Lcom/samsung/android/scloud/sync/data/SyncItem;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 310
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p4}, Lcom/samsung/android/scloud/sync/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/samsung/android/scloud/auth/AuthManager;->getPutApiParamsWithCid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 311
    if-lez p7, :cond_1

    .line 312
    const-string v2, "revision"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3, v5}, Lcom/samsung/android/scloud/framework/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 313
    :cond_1
    const-string v2, "ctid"

    invoke-static {v1, v2, p8, v5}, Lcom/samsung/android/scloud/framework/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 315
    const-string v2, "ORSServiceManager"

    const-string v3, "API Code is 909"

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    invoke-interface {p4}, Lcom/samsung/android/scloud/sync/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/samsung/android/scloud/framework/network/NetworkUtil;->delete(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 317
    .local v0, "httpResponse":Lorg/apache/http/HttpResponse;
    invoke-static {v0, p9}, Lcom/samsung/android/scloud/framework/network/NetworkUtil;->extractResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/framework/network/NetworkUtil$StringResponseHandler;)V

    .line 319
    return-void
.end method

.method public static deleteFolder(Landroid/content/Context;Lcom/samsung/android/scloud/auth/AuthManager;Ljava/lang/String;ILcom/samsung/android/scloud/sync/model/IModel;Ljava/lang/String;ILjava/lang/String;Lcom/samsung/android/scloud/framework/network/NetworkUtil$StringResponseHandler;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "auth"    # Lcom/samsung/android/scloud/auth/AuthManager;
    .param p2, "txKey"    # Ljava/lang/String;
    .param p3, "txSeq"    # I
    .param p4, "model"    # Lcom/samsung/android/scloud/sync/model/IModel;
    .param p5, "folderPath"    # Ljava/lang/String;
    .param p6, "revision"    # I
    .param p7, "ctid"    # Ljava/lang/String;
    .param p8, "handler"    # Lcom/samsung/android/scloud/framework/network/NetworkUtil$StringResponseHandler;

    .prologue
    const/4 v5, 0x0

    .line 259
    const-string v2, "ORSServiceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "deleteFolder!!!!!!!!!! - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", path : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/samsung/android/scloud/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 264
    .local v1, "url":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Lcom/samsung/android/scloud/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    .line 265
    const-string v2, "ORSServiceManager"

    const-string v3, "There is NO Base URL."

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    new-instance v2, Lcom/samsung/android/scloud/framework/PDMException;

    const/16 v3, 0x131

    invoke-direct {v2, v3}, Lcom/samsung/android/scloud/framework/PDMException;-><init>(I)V

    throw v2

    .line 269
    :cond_0
    const-string v2, "/ors/v2/tx/rmdir/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 270
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 271
    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 273
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p4}, Lcom/samsung/android/scloud/sync/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/samsung/android/scloud/auth/AuthManager;->getPutApiParamsWithCid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 274
    if-lez p6, :cond_1

    .line 275
    const-string v2, "revision"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3, v5}, Lcom/samsung/android/scloud/framework/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 276
    :cond_1
    const-string v2, "ctid"

    invoke-static {v1, v2, p7, v5}, Lcom/samsung/android/scloud/framework/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 278
    const-string v2, "ORSServiceManager"

    const-string v3, "API Code is 908"

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    invoke-interface {p4}, Lcom/samsung/android/scloud/sync/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/samsung/android/scloud/framework/network/NetworkUtil;->delete(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 280
    .local v0, "httpResponse":Lorg/apache/http/HttpResponse;
    invoke-static {v0, p8}, Lcom/samsung/android/scloud/framework/network/NetworkUtil;->extractResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/framework/network/NetworkUtil$StringResponseHandler;)V

    .line 283
    return-void
.end method

.method public static downloadFile(Landroid/content/Context;Lcom/samsung/android/scloud/auth/AuthManager;Lcom/samsung/android/scloud/sync/model/IModel;Lcom/samsung/android/scloud/sync/data/SyncItem;Ljava/lang/String;ILjava/lang/String;Lcom/samsung/android/scloud/framework/network/NetworkUtil$FileResponseHandler;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "auth"    # Lcom/samsung/android/scloud/auth/AuthManager;
    .param p2, "model"    # Lcom/samsung/android/scloud/sync/model/IModel;
    .param p3, "item"    # Lcom/samsung/android/scloud/sync/data/SyncItem;
    .param p4, "fileName"    # Ljava/lang/String;
    .param p5, "revision"    # I
    .param p6, "ctid"    # Ljava/lang/String;
    .param p7, "handler"    # Lcom/samsung/android/scloud/framework/network/NetworkUtil$FileResponseHandler;

    .prologue
    const/4 v5, 0x0

    .line 225
    const-string v2, "ORSServiceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "downloadFile!!!!!!!!!! - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", key : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p3}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getSyncKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", file : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p2, p3}, Lcom/samsung/android/scloud/sync/model/IModel;->getServerFilePathPrefix(Lcom/samsung/android/scloud/sync/data/SyncItem;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/samsung/android/scloud/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 230
    .local v1, "url":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Lcom/samsung/android/scloud/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    .line 231
    const-string v2, "ORSServiceManager"

    const-string v3, "There is NO Base URL."

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    new-instance v2, Lcom/samsung/android/scloud/framework/PDMException;

    const/16 v3, 0x131

    invoke-direct {v2, v3}, Lcom/samsung/android/scloud/framework/PDMException;-><init>(I)V

    throw v2

    .line 235
    :cond_0
    const-string v2, "/ors/v2/download"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 236
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p2, p3}, Lcom/samsung/android/scloud/sync/model/IModel;->getServerFilePathPrefix(Lcom/samsung/android/scloud/sync/data/SyncItem;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 238
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/samsung/android/scloud/auth/AuthManager;->getPutApiParamsWithCid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 239
    if-lez p5, :cond_1

    .line 240
    const-string v2, "revision"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3, v5}, Lcom/samsung/android/scloud/framework/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 241
    :cond_1
    const-string v2, "ctid"

    invoke-static {v1, v2, p6, v5}, Lcom/samsung/android/scloud/framework/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 243
    const-string v2, "ORSServiceManager"

    const-string v3, "API Code is 907"

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    invoke-interface {p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/samsung/android/scloud/framework/network/NetworkUtil;->get(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 245
    .local v0, "httpResponse":Lorg/apache/http/HttpResponse;
    invoke-static {v0, p4, p7}, Lcom/samsung/android/scloud/framework/network/NetworkUtil;->extractResponse(Lorg/apache/http/HttpResponse;Ljava/lang/String;Lcom/samsung/android/scloud/framework/network/NetworkUtil$FileResponseHandler;)V

    .line 246
    return-void
.end method

.method public static getTimestamp(Landroid/content/Context;Lcom/samsung/android/scloud/auth/AuthManager;Lcom/samsung/android/scloud/sync/model/IModel;Ljava/lang/String;Lcom/samsung/android/scloud/framework/network/NetworkUtil$JSONResponseHandler;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "auth"    # Lcom/samsung/android/scloud/auth/AuthManager;
    .param p2, "model"    # Lcom/samsung/android/scloud/sync/model/IModel;
    .param p3, "ctid"    # Ljava/lang/String;
    .param p4, "handler"    # Lcom/samsung/android/scloud/framework/network/NetworkUtil$JSONResponseHandler;

    .prologue
    .line 35
    const-string v2, "ORSServiceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getTimestamp!!!!!!!!!! - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/samsung/android/scloud/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 40
    .local v1, "url":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Lcom/samsung/android/scloud/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    .line 41
    const-string v2, "ORSServiceManager"

    const-string v3, "There is NO Base URL."

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    new-instance v2, Lcom/samsung/android/scloud/framework/PDMException;

    const/16 v3, 0x131

    invoke-direct {v2, v3}, Lcom/samsung/android/scloud/framework/PDMException;-><init>(I)V

    throw v2

    .line 45
    :cond_0
    const-string v2, "/ors/v2/timestamp"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 46
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/samsung/android/scloud/auth/AuthManager;->getPutApiParamsWithCid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    const-string v2, "ctid"

    const/4 v3, 0x0

    invoke-static {v1, v2, p3, v3}, Lcom/samsung/android/scloud/framework/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 49
    const-string v2, "ORSServiceManager"

    const-string v3, "API Code is 901"

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    invoke-interface {p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/samsung/android/scloud/framework/network/NetworkUtil;->get(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 51
    .local v0, "httpResponse":Lorg/apache/http/HttpResponse;
    invoke-static {v0, p4}, Lcom/samsung/android/scloud/framework/network/NetworkUtil;->extractResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/framework/network/NetworkUtil$StringResponseHandler;)V

    .line 52
    return-void
.end method

.method public static listDirectory(Landroid/content/Context;Lcom/samsung/android/scloud/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/sync/model/IModel;Ljava/lang/String;ILjava/lang/String;IJLcom/samsung/android/scloud/framework/network/NetworkUtil$JSONResponseHandler;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "auth"    # Lcom/samsung/android/scloud/auth/AuthManager;
    .param p2, "ctid"    # Ljava/lang/String;
    .param p3, "model"    # Lcom/samsung/android/scloud/sync/model/IModel;
    .param p4, "directoryPath"    # Ljava/lang/String;
    .param p5, "revision"    # I
    .param p6, "startKey"    # Ljava/lang/String;
    .param p7, "count"    # I
    .param p8, "modifiedAfter"    # J
    .param p10, "handler"    # Lcom/samsung/android/scloud/framework/network/NetworkUtil$JSONResponseHandler;

    .prologue
    .line 58
    const-string v2, "ORSServiceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "listDirectory!!!!!!!!!! - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/samsung/android/scloud/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 63
    .local v1, "url":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Lcom/samsung/android/scloud/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    .line 64
    const-string v2, "ORSServiceManager"

    const-string v3, "There is NO Base URL."

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    new-instance v2, Lcom/samsung/android/scloud/framework/PDMException;

    const/16 v3, 0x131

    invoke-direct {v2, v3}, Lcom/samsung/android/scloud/framework/PDMException;-><init>(I)V

    throw v2

    .line 68
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "/ors/v2/ls"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p3}, Lcom/samsung/android/scloud/sync/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/samsung/android/scloud/auth/AuthManager;->getPutApiParamsWithCid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    const-wide/16 v2, 0x0

    cmp-long v2, p8, v2

    if-lez v2, :cond_1

    .line 71
    const-string v2, "modified_after"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p8, p9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Lcom/samsung/android/scloud/framework/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 72
    :cond_1
    if-lez p5, :cond_2

    .line 73
    const-string v2, "revision"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Lcom/samsung/android/scloud/framework/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 75
    :cond_2
    if-eqz p6, :cond_3

    const-string v2, ""

    invoke-virtual {v2, p6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 76
    const-string v2, "start"

    const/4 v3, 0x0

    invoke-static {v1, v2, p6, v3}, Lcom/samsung/android/scloud/framework/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 78
    :cond_3
    const-string v2, "ctid"

    const/4 v3, 0x0

    invoke-static {v1, v2, p2, v3}, Lcom/samsung/android/scloud/framework/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 80
    const-string v2, "ORSServiceManager"

    const-string v3, "API Code is 902"

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    invoke-interface {p3}, Lcom/samsung/android/scloud/sync/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/samsung/android/scloud/framework/network/NetworkUtil;->get(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 82
    .local v0, "httpResponse":Lorg/apache/http/HttpResponse;
    invoke-static {v0, p10}, Lcom/samsung/android/scloud/framework/network/NetworkUtil;->extractResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/framework/network/NetworkUtil$StringResponseHandler;)V

    .line 83
    return-void
.end method

.method public static transactionCancel(Landroid/content/Context;Lcom/samsung/android/scloud/auth/AuthManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/scloud/framework/network/NetworkUtil$JSONResponseHandler;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "auth"    # Lcom/samsung/android/scloud/auth/AuthManager;
    .param p2, "cid"    # Ljava/lang/String;
    .param p3, "ctid"    # Ljava/lang/String;
    .param p4, "txKey"    # Ljava/lang/String;
    .param p5, "handler"    # Lcom/samsung/android/scloud/framework/network/NetworkUtil$JSONResponseHandler;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 139
    const-string v2, "ORSServiceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "transactionCancel!!!!!!!!!! - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/samsung/android/scloud/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 144
    .local v1, "url":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Lcom/samsung/android/scloud/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    .line 145
    const-string v2, "ORSServiceManager"

    const-string v3, "There is NO Base URL."

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    new-instance v2, Lcom/samsung/android/scloud/framework/PDMException;

    const/16 v3, 0x131

    invoke-direct {v2, v3}, Lcom/samsung/android/scloud/framework/PDMException;-><init>(I)V

    throw v2

    .line 149
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "/ors/v2/tx/cancel/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1, p2}, Lcom/samsung/android/scloud/auth/AuthManager;->getPutApiParamsWithCid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    const-string v2, "ctid"

    const/4 v3, 0x0

    invoke-static {v1, v2, p3, v3}, Lcom/samsung/android/scloud/framework/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 153
    const-string v2, "ORSServiceManager"

    const-string v3, "API Code is 905"

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {p2, v2, v3}, Lcom/samsung/android/scloud/framework/network/NetworkUtil;->delete(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 155
    .local v0, "httpResponse":Lorg/apache/http/HttpResponse;
    invoke-static {v0, p5}, Lcom/samsung/android/scloud/framework/network/NetworkUtil;->extractResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/framework/network/NetworkUtil$StringResponseHandler;)V

    .line 157
    return-void
.end method

.method public static transactionEnd(Landroid/content/Context;Lcom/samsung/android/scloud/auth/AuthManager;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/samsung/android/scloud/framework/network/NetworkUtil$JSONResponseHandler;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "auth"    # Lcom/samsung/android/scloud/auth/AuthManager;
    .param p2, "txKey"    # Ljava/lang/String;
    .param p3, "txCnt"    # I
    .param p4, "ctid"    # Ljava/lang/String;
    .param p5, "cid"    # Ljava/lang/String;
    .param p6, "handler"    # Lcom/samsung/android/scloud/framework/network/NetworkUtil$JSONResponseHandler;

    .prologue
    const/4 v5, 0x0

    .line 114
    const-string v2, "ORSServiceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "transactionEnd!!!!!!!!!! - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/samsung/android/scloud/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 119
    .local v1, "url":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Lcom/samsung/android/scloud/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    .line 120
    const-string v2, "ORSServiceManager"

    const-string v3, "There is NO Base URL."

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    new-instance v2, Lcom/samsung/android/scloud/framework/PDMException;

    const/16 v3, 0x131

    invoke-direct {v2, v3}, Lcom/samsung/android/scloud/framework/PDMException;-><init>(I)V

    throw v2

    .line 124
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "/ors/v2/tx/end/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1, p5}, Lcom/samsung/android/scloud/auth/AuthManager;->getPutApiParamsWithCid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126
    const-string v2, "tx_count"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3, v5}, Lcom/samsung/android/scloud/framework/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 127
    const-string v2, "ctid"

    invoke-static {v1, v2, p4, v5}, Lcom/samsung/android/scloud/framework/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 129
    const-string v2, "ORSServiceManager"

    const-string v3, "API Code is 904"

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p5, v2}, Lcom/samsung/android/scloud/framework/network/NetworkUtil;->post(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 131
    .local v0, "httpResponse":Lorg/apache/http/HttpResponse;
    invoke-static {v0, p6}, Lcom/samsung/android/scloud/framework/network/NetworkUtil;->extractResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/framework/network/NetworkUtil$StringResponseHandler;)V

    .line 133
    return-void
.end method

.method public static transactionStart(Landroid/content/Context;Lcom/samsung/android/scloud/auth/AuthManager;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/scloud/framework/network/NetworkUtil$JSONResponseHandler;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "auth"    # Lcom/samsung/android/scloud/auth/AuthManager;
    .param p2, "ctid"    # Ljava/lang/String;
    .param p3, "cid"    # Ljava/lang/String;
    .param p4, "handler"    # Lcom/samsung/android/scloud/framework/network/NetworkUtil$JSONResponseHandler;

    .prologue
    .line 87
    const-string v2, "ORSServiceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "transactionStart!!!!!!!!!! - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/samsung/android/scloud/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 92
    .local v1, "url":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Lcom/samsung/android/scloud/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    .line 93
    const-string v2, "ORSServiceManager"

    const-string v3, "There is NO Base URL."

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    new-instance v2, Lcom/samsung/android/scloud/framework/PDMException;

    const/16 v3, 0x131

    invoke-direct {v2, v3}, Lcom/samsung/android/scloud/framework/PDMException;-><init>(I)V

    throw v2

    .line 97
    :cond_0
    const-string v2, "/ors/v2/tx/start"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1, p3}, Lcom/samsung/android/scloud/auth/AuthManager;->getPutApiParamsWithCid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 99
    const-string v2, "ctid"

    const/4 v3, 0x0

    invoke-static {v1, v2, p2, v3}, Lcom/samsung/android/scloud/framework/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 101
    const-string v2, "ORSServiceManager"

    const-string v3, "API Code is 903"

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p3, v2}, Lcom/samsung/android/scloud/framework/network/NetworkUtil;->post(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 103
    .local v0, "httpResponse":Lorg/apache/http/HttpResponse;
    invoke-static {v0, p4}, Lcom/samsung/android/scloud/framework/network/NetworkUtil;->extractResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/framework/network/NetworkUtil$StringResponseHandler;)V

    .line 105
    return-void
.end method

.method public static updateTag(Landroid/content/Context;Lcom/samsung/android/scloud/auth/AuthManager;Ljava/lang/String;ILcom/samsung/android/scloud/sync/model/IModel;Lcom/samsung/android/scloud/sync/data/SyncItem;Ljava/lang/String;ILjava/lang/String;Lcom/samsung/android/scloud/framework/network/NetworkUtil$StringResponseHandler;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "auth"    # Lcom/samsung/android/scloud/auth/AuthManager;
    .param p2, "txKey"    # Ljava/lang/String;
    .param p3, "txSeq"    # I
    .param p4, "model"    # Lcom/samsung/android/scloud/sync/model/IModel;
    .param p5, "item"    # Lcom/samsung/android/scloud/sync/data/SyncItem;
    .param p6, "tag"    # Ljava/lang/String;
    .param p7, "revision"    # I
    .param p8, "ctid"    # Ljava/lang/String;
    .param p9, "handler"    # Lcom/samsung/android/scloud/framework/network/NetworkUtil$StringResponseHandler;

    .prologue
    const/4 v5, 0x0

    .line 330
    const-string v2, "ORSServiceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "updateTimestamp!!!!!!!!!! - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", key : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p5}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getSyncKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", tag : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/samsung/android/scloud/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 335
    .local v1, "url":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Lcom/samsung/android/scloud/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    .line 336
    const-string v2, "ORSServiceManager"

    const-string v3, "There is NO Base URL."

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    new-instance v2, Lcom/samsung/android/scloud/framework/PDMException;

    const/16 v3, 0x131

    invoke-direct {v2, v3}, Lcom/samsung/android/scloud/framework/PDMException;-><init>(I)V

    throw v2

    .line 340
    :cond_0
    const-string v2, "/ors/v2/tx/chdir/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 341
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 342
    invoke-interface {p4, p5}, Lcom/samsung/android/scloud/sync/model/IModel;->getServerFilePathPrefix(Lcom/samsung/android/scloud/sync/data/SyncItem;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 344
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p4}, Lcom/samsung/android/scloud/sync/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/samsung/android/scloud/auth/AuthManager;->getPutApiParamsWithCid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 345
    if-lez p7, :cond_1

    .line 346
    const-string v2, "revision"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3, v5}, Lcom/samsung/android/scloud/framework/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 347
    :cond_1
    const-string v2, "tag"

    invoke-static {v1, v2, p6, v5}, Lcom/samsung/android/scloud/framework/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 348
    const-string v2, "ctid"

    invoke-static {v1, v2, p8, v5}, Lcom/samsung/android/scloud/framework/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 350
    const-string v2, "ORSServiceManager"

    const-string v3, "API Code is 910"

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    invoke-interface {p4}, Lcom/samsung/android/scloud/sync/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/network/NetworkUtil;->post(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 352
    .local v0, "httpResponse":Lorg/apache/http/HttpResponse;
    invoke-static {v0, p9}, Lcom/samsung/android/scloud/framework/network/NetworkUtil;->extractResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/framework/network/NetworkUtil$StringResponseHandler;)V

    .line 354
    return-void
.end method

.method public static uploadFile(Landroid/content/Context;Lcom/samsung/android/scloud/auth/AuthManager;Ljava/lang/String;ILcom/samsung/android/scloud/sync/model/IModel;Lcom/samsung/android/scloud/sync/data/SyncItem;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/samsung/android/scloud/framework/network/NetworkUtil$PDMProgressListener;Lcom/samsung/android/scloud/framework/network/NetworkUtil$JSONResponseHandler;)V
    .locals 13
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "auth"    # Lcom/samsung/android/scloud/auth/AuthManager;
    .param p2, "txKey"    # Ljava/lang/String;
    .param p3, "txSeq"    # I
    .param p4, "model"    # Lcom/samsung/android/scloud/sync/model/IModel;
    .param p5, "item"    # Lcom/samsung/android/scloud/sync/data/SyncItem;
    .param p6, "fileName"    # Ljava/lang/String;
    .param p7, "tag"    # Ljava/lang/String;
    .param p8, "revision"    # I
    .param p9, "ctid"    # Ljava/lang/String;
    .param p10, "progressListener"    # Lcom/samsung/android/scloud/framework/network/NetworkUtil$PDMProgressListener;
    .param p11, "handler"    # Lcom/samsung/android/scloud/framework/network/NetworkUtil$JSONResponseHandler;

    .prologue
    .line 173
    const-string v2, "ORSServiceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "uploadFile!!!!!!!!!! - "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p9

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", key : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p5 .. p5}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getSyncKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", file : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p6

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/samsung/android/scloud/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v12, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 178
    .local v12, "url":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Lcom/samsung/android/scloud/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    .line 179
    const-string v2, "ORSServiceManager"

    const-string v3, "There is NO Base URL."

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    new-instance v2, Lcom/samsung/android/scloud/framework/PDMException;

    const/16 v3, 0x131

    invoke-direct {v2, v3}, Lcom/samsung/android/scloud/framework/PDMException;-><init>(I)V

    throw v2

    .line 183
    :cond_0
    const-string v2, "/ors/v2/tx/upload/"

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 184
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 185
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface/range {p4 .. p5}, Lcom/samsung/android/scloud/sync/model/IModel;->getServerFilePathPrefix(Lcom/samsung/android/scloud/sync/data/SyncItem;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p6

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 187
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface/range {p4 .. p4}, Lcom/samsung/android/scloud/sync/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/samsung/android/scloud/auth/AuthManager;->getPutApiParamsWithCid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 188
    if-lez p8, :cond_1

    .line 189
    const-string v2, "revision"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v0, p8

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ""

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    invoke-static {v12, v2, v3, v5}, Lcom/samsung/android/scloud/framework/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 190
    :cond_1
    if-eqz p7, :cond_2

    .line 191
    const-string v2, "tag"

    const/4 v3, 0x0

    move-object/from16 v0, p7

    invoke-static {v12, v2, v0, v3}, Lcom/samsung/android/scloud/framework/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 192
    :cond_2
    const-string v2, "ctid"

    const/4 v3, 0x0

    move-object/from16 v0, p9

    invoke-static {v12, v2, v0, v3}, Lcom/samsung/android/scloud/framework/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 195
    new-instance v9, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p4

    move-object/from16 v1, p5

    invoke-interface {v0, p0, v1}, Lcom/samsung/android/scloud/sync/model/IModel;->getLocalFilePathPrefix(Landroid/content/Context;Lcom/samsung/android/scloud/sync/data/SyncItem;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p6

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v9, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 197
    .local v9, "file":Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_3

    .line 198
    new-instance v2, Lcom/samsung/android/scloud/framework/PDMException;

    const/16 v3, 0x138

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "File not exists : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p4

    move-object/from16 v1, p5

    invoke-interface {v0, p0, v1}, Lcom/samsung/android/scloud/sync/model/IModel;->getLocalFilePathPrefix(Landroid/content/Context;Lcom/samsung/android/scloud/sync/data/SyncItem;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p6

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v3, v5}, Lcom/samsung/android/scloud/framework/PDMException;-><init>(ILjava/lang/String;)V

    throw v2

    .line 201
    :cond_3
    :try_start_0
    new-instance v11, Ljava/io/FileInputStream;

    invoke-direct {v11, v9}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 202
    .local v11, "stream":Ljava/io/InputStream;
    new-instance v4, Lcom/samsung/android/scloud/framework/network/CountingInputStreamEntity;

    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v2

    move-object/from16 v0, p10

    invoke-direct {v4, v11, v2, v3, v0}, Lcom/samsung/android/scloud/framework/network/CountingInputStreamEntity;-><init>(Ljava/io/InputStream;JLcom/samsung/android/scloud/framework/network/NetworkUtil$PDMProgressListener;)V

    .line 203
    .local v4, "inputEntity":Lcom/samsung/android/scloud/framework/network/CountingInputStreamEntity;
    const-string v2, "application/octet-stream"

    invoke-virtual {v4, v2}, Lcom/samsung/android/scloud/framework/network/CountingInputStreamEntity;->setContentType(Ljava/lang/String;)V

    .line 204
    const-string v2, "ORSServiceManager"

    const-string v3, "API Code is 906"

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    invoke-interface/range {p4 .. p4}, Lcom/samsung/android/scloud/sync/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v5

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/samsung/android/scloud/framework/network/NetworkUtil;->put(Ljava/lang/String;Ljava/lang/String;Lorg/apache/http/entity/InputStreamEntity;JLjava/util/Map;)Lorg/apache/http/HttpResponse;

    move-result-object v10

    .line 206
    .local v10, "httpResponse":Lorg/apache/http/HttpResponse;
    move-object/from16 v0, p11

    invoke-static {v10, v0}, Lcom/samsung/android/scloud/framework/network/NetworkUtil;->extractResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/framework/network/NetworkUtil$StringResponseHandler;)V

    .line 207
    invoke-virtual {v11}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Lcom/samsung/android/scloud/framework/PDMException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 214
    return-void

    .line 208
    .end local v4    # "inputEntity":Lcom/samsung/android/scloud/framework/network/CountingInputStreamEntity;
    .end local v10    # "httpResponse":Lorg/apache/http/HttpResponse;
    .end local v11    # "stream":Ljava/io/InputStream;
    :catch_0
    move-exception v8

    .line 209
    .local v8, "e":Lcom/samsung/android/scloud/framework/PDMException;
    throw v8

    .line 210
    .end local v8    # "e":Lcom/samsung/android/scloud/framework/PDMException;
    :catch_1
    move-exception v8

    .line 211
    .local v8, "e":Ljava/lang/Exception;
    new-instance v2, Lcom/samsung/android/scloud/framework/PDMException;

    const/16 v3, 0x138

    invoke-direct {v2, v3, v8}, Lcom/samsung/android/scloud/framework/PDMException;-><init>(ILjava/lang/Throwable;)V

    throw v2
.end method
