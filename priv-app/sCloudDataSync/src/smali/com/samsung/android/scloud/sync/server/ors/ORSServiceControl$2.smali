.class Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$2;
.super Lcom/samsung/android/scloud/framework/network/NetworkUtil$JSONResponseHandler;
.source "ORSServiceControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->getKeys(Ljava/lang/String;JLjava/util/HashMap;)Ljava/lang/String;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

.field final synthetic val$nextKey:[Ljava/lang/String;

.field final synthetic val$outServerChanges:Ljava/util/HashMap;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;[Ljava/lang/String;Ljava/util/HashMap;)V
    .locals 0

    .prologue
    .line 102
    iput-object p1, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$2;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    iput-object p2, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$2;->val$nextKey:[Ljava/lang/String;

    iput-object p3, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$2;->val$outServerChanges:Ljava/util/HashMap;

    invoke-direct {p0}, Lcom/samsung/android/scloud/framework/network/NetworkUtil$JSONResponseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleJSONResponse(Lorg/json/JSONObject;)V
    .locals 21
    .param p1, "data"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 105
    const-string v15, "next"

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_1

    .line 106
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$2;->val$nextKey:[Ljava/lang/String;

    const/16 v16, 0x0

    const-string v17, "next"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    .line 110
    :goto_0
    const-string v15, "path"

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 111
    .local v3, "folder":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    .line 112
    .local v4, "folderLen":I
    const-string v15, "children"

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v11

    .line 114
    .local v11, "list":Lorg/json/JSONArray;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    invoke-virtual {v11}, Lorg/json/JSONArray;->length()I

    move-result v15

    if-ge v6, v15, :cond_2

    .line 115
    invoke-virtual {v11, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v8

    .line 117
    .local v8, "json":Lorg/json/JSONObject;
    :try_start_0
    const-string v15, "path"

    invoke-virtual {v8, v15}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 118
    .local v10, "key":Ljava/lang/String;
    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_0

    .line 119
    add-int/lit8 v15, v4, 0x1

    invoke-virtual {v10, v15}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    .line 120
    const-string v15, "revision"

    invoke-virtual {v8, v15}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 121
    .local v5, "folderRevision":I
    const-string v15, "tag"

    invoke-virtual {v8, v15}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v13

    .line 122
    .local v13, "timestamp":J
    const-string v15, "deleted"

    invoke-virtual {v8, v15}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 123
    .local v2, "deleted":Z
    new-instance v7, Lcom/samsung/android/scloud/sync/data/SyncItem;

    invoke-direct {v7, v10, v13, v14, v2}, Lcom/samsung/android/scloud/sync/data/SyncItem;-><init>(Ljava/lang/String;JZ)V

    .line 124
    .local v7, "item":Lcom/samsung/android/scloud/sync/data/SyncItem;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$2;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    # getter for: Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mListener:Lcom/samsung/android/scloud/framework/IStatusListener;
    invoke-static {v15}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->access$100(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Lcom/samsung/android/scloud/framework/IStatusListener;

    move-result-object v15

    const/16 v16, 0x4

    const-string v17, "ORSServiceControl"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "["

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$2;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;
    invoke-static/range {v19 .. v19}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->access$300(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Lcom/samsung/android/scloud/sync/model/IModel;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "]("

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$2;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mCtid:Ljava/lang/String;
    invoke-static/range {v19 .. v19}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->access$000(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ") : ServerItem - key : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v7}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getSyncKey()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", timestamp : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v7}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getTimeStamp()J

    move-result-wide v19

    invoke-virtual/range {v18 .. v20}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", deleted : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v7}, Lcom/samsung/android/scloud/sync/data/SyncItem;->isDeleted()Z

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-interface/range {v15 .. v18}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 125
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$2;->val$outServerChanges:Ljava/util/HashMap;

    invoke-virtual {v15, v10, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$2;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    # getter for: Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mRevisionMap:Ljava/util/Map;
    invoke-static {v15}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->access$400(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Ljava/util/Map;

    move-result-object v15

    const/16 v16, 0x2

    move/from16 v0, v16

    new-array v0, v0, [I

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aput v5, v16, v17

    const/16 v17, 0x1

    const/16 v18, 0x0

    aput v18, v16, v17

    move-object/from16 v0, v16

    invoke-interface {v15, v10, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    .line 114
    .end local v2    # "deleted":Z
    .end local v5    # "folderRevision":I
    .end local v7    # "item":Lcom/samsung/android/scloud/sync/data/SyncItem;
    .end local v10    # "key":Ljava/lang/String;
    .end local v13    # "timestamp":J
    :cond_0
    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_1

    .line 108
    .end local v3    # "folder":Ljava/lang/String;
    .end local v4    # "folderLen":I
    .end local v6    # "i":I
    .end local v8    # "json":Lorg/json/JSONObject;
    .end local v11    # "list":Lorg/json/JSONArray;
    :cond_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$2;->val$nextKey:[Ljava/lang/String;

    const/16 v16, 0x0

    const/16 v17, 0x0

    aput-object v17, v15, v16

    goto/16 :goto_0

    .line 128
    .restart local v3    # "folder":Ljava/lang/String;
    .restart local v4    # "folderLen":I
    .restart local v6    # "i":I
    .restart local v8    # "json":Lorg/json/JSONObject;
    .restart local v11    # "list":Lorg/json/JSONArray;
    :catch_0
    move-exception v9

    .line 129
    .local v9, "jsone":Lorg/json/JSONException;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$2;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    # getter for: Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mListener:Lcom/samsung/android/scloud/framework/IStatusListener;
    invoke-static {v15}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->access$100(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Lcom/samsung/android/scloud/framework/IStatusListener;

    move-result-object v15

    const/16 v16, 0x0

    const-string v17, "ORSServiceControl"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "["

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$2;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;
    invoke-static/range {v19 .. v19}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->access$300(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Lcom/samsung/android/scloud/sync/model/IModel;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "]("

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$2;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mCtid:Ljava/lang/String;
    invoke-static/range {v19 .. v19}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->access$000(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ") : ServerItem , invalid value : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-interface/range {v15 .. v18}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 130
    .end local v9    # "jsone":Lorg/json/JSONException;
    :catch_1
    move-exception v12

    .line 131
    .local v12, "nfe":Ljava/lang/NumberFormatException;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$2;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    # getter for: Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mListener:Lcom/samsung/android/scloud/framework/IStatusListener;
    invoke-static {v15}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->access$100(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Lcom/samsung/android/scloud/framework/IStatusListener;

    move-result-object v15

    const/16 v16, 0x0

    const-string v17, "ORSServiceControl"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "["

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$2;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;
    invoke-static/range {v19 .. v19}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->access$300(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Lcom/samsung/android/scloud/sync/model/IModel;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "]("

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$2;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mCtid:Ljava/lang/String;
    invoke-static/range {v19 .. v19}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->access$000(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ") : ServerItem , invalid value : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-interface/range {v15 .. v18}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 134
    .end local v8    # "json":Lorg/json/JSONObject;
    .end local v12    # "nfe":Ljava/lang/NumberFormatException;
    :cond_2
    return-void
.end method
