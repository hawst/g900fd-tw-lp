.class public Lcom/samsung/android/scloud/sync/data/SyncItem;
.super Ljava/lang/Object;
.source "SyncItem.java"


# instance fields
.field private deleted:Z

.field private isNew:Z

.field private localId:Ljava/lang/String;

.field private syncKey:Ljava/lang/String;

.field private tag:Ljava/lang/String;

.field private timeStamp:J


# direct methods
.method public constructor <init>(Ljava/lang/String;JZ)V
    .locals 1
    .param p1, "syncKey"    # Ljava/lang/String;
    .param p2, "timestamp"    # J
    .param p4, "deleted"    # Z

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/samsung/android/scloud/sync/data/SyncItem;->syncKey:Ljava/lang/String;

    .line 22
    iput-wide p2, p0, Lcom/samsung/android/scloud/sync/data/SyncItem;->timeStamp:J

    .line 23
    iput-boolean p4, p0, Lcom/samsung/android/scloud/sync/data/SyncItem;->deleted:Z

    .line 24
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/scloud/sync/data/SyncItem;->isNew:Z

    .line 25
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;JZ)V
    .locals 1
    .param p1, "localId"    # Ljava/lang/String;
    .param p2, "syncKey"    # Ljava/lang/String;
    .param p3, "timestamp"    # J
    .param p5, "deleted"    # Z

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/samsung/android/scloud/sync/data/SyncItem;->localId:Ljava/lang/String;

    .line 29
    iput-object p2, p0, Lcom/samsung/android/scloud/sync/data/SyncItem;->syncKey:Ljava/lang/String;

    .line 30
    iput-wide p3, p0, Lcom/samsung/android/scloud/sync/data/SyncItem;->timeStamp:J

    .line 31
    iput-boolean p5, p0, Lcom/samsung/android/scloud/sync/data/SyncItem;->deleted:Z

    .line 33
    if-nez p2, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/scloud/sync/data/SyncItem;->isNew:Z

    .line 34
    :cond_0
    return-void
.end method


# virtual methods
.method public getLocalId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/data/SyncItem;->localId:Ljava/lang/String;

    return-object v0
.end method

.method public getSyncKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/data/SyncItem;->syncKey:Ljava/lang/String;

    return-object v0
.end method

.method public getTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/data/SyncItem;->tag:Ljava/lang/String;

    return-object v0
.end method

.method public getTimeStamp()J
    .locals 2

    .prologue
    .line 47
    iget-wide v0, p0, Lcom/samsung/android/scloud/sync/data/SyncItem;->timeStamp:J

    return-wide v0
.end method

.method public isDeleted()Z
    .locals 1

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/samsung/android/scloud/sync/data/SyncItem;->deleted:Z

    return v0
.end method

.method public isNew()Z
    .locals 1

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/samsung/android/scloud/sync/data/SyncItem;->isNew:Z

    return v0
.end method

.method public setIsNew(Z)V
    .locals 0
    .param p1, "isNew"    # Z

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/samsung/android/scloud/sync/data/SyncItem;->isNew:Z

    .line 38
    return-void
.end method

.method public setLocalId(Ljava/lang/String;)V
    .locals 0
    .param p1, "localId"    # Ljava/lang/String;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/samsung/android/scloud/sync/data/SyncItem;->localId:Ljava/lang/String;

    .line 71
    return-void
.end method

.method public setSyncKey(Ljava/lang/String;)V
    .locals 0
    .param p1, "syncKey"    # Ljava/lang/String;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/samsung/android/scloud/sync/data/SyncItem;->syncKey:Ljava/lang/String;

    .line 67
    return-void
.end method

.method public setTag(Ljava/lang/String;)V
    .locals 0
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/samsung/android/scloud/sync/data/SyncItem;->tag:Ljava/lang/String;

    .line 82
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 96
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SyncItem - localId : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/sync/data/SyncItem;->localId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", syncKey : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/sync/data/SyncItem;->syncKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", timeStamp : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/samsung/android/scloud/sync/data/SyncItem;->timeStamp:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", deleted : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/samsung/android/scloud/sync/data/SyncItem;->deleted:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isNew : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/samsung/android/scloud/sync/data/SyncItem;->isNew:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
