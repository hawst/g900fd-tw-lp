.class public Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;
.super Ljava/lang/Object;
.source "SNoteServiceControl.java"

# interfaces
.implements Lcom/samsung/android/scloud/sync/server/ICloudServiceControl;


# static fields
.field private static CONTROL_MAP:Ljava/util/concurrent/ConcurrentHashMap; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "SNoteServiceControl"


# instance fields
.field private mAuth:Lcom/samsung/android/scloud/auth/AuthManager;

.field private mContext:Landroid/content/Context;

.field private mCtid:Ljava/lang/String;

.field private mListener:Lcom/samsung/android/scloud/framework/IStatusListener;

.field private mModel:Lcom/samsung/android/scloud/sync/model/IModel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->CONTROL_MAP:Ljava/util/concurrent/ConcurrentHashMap;

    return-void
.end method

.method private constructor <init>(Lcom/samsung/android/scloud/sync/model/IModel;)V
    .locals 0
    .param p1, "model"    # Lcom/samsung/android/scloud/sync/model/IModel;

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    .line 38
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mCtid:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;)Lcom/samsung/android/scloud/framework/IStatusListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mListener:Lcom/samsung/android/scloud/framework/IStatusListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;)Lcom/samsung/android/scloud/sync/model/IModel;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    return-object v0
.end method

.method public static declared-synchronized getInstance(Lcom/samsung/android/scloud/sync/model/IModel;)Lcom/samsung/android/scloud/sync/server/ICloudServiceControl;
    .locals 4
    .param p0, "model"    # Lcom/samsung/android/scloud/sync/model/IModel;

    .prologue
    .line 41
    const-class v1, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->CONTROL_MAP:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-interface {p0}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 42
    sget-object v0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->CONTROL_MAP:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-interface {p0}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;

    invoke-direct {v3, p0}, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;-><init>(Lcom/samsung/android/scloud/sync/model/IModel;)V

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    :cond_0
    sget-object v0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->CONTROL_MAP:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-interface {p0}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/sync/server/ICloudServiceControl;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 41
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public deleteItem(Lcom/samsung/android/scloud/sync/data/SyncItem;)Z
    .locals 9
    .param p1, "item"    # Lcom/samsung/android/scloud/sync/data/SyncItem;

    .prologue
    const/4 v8, 0x0

    .line 220
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mAuth:Lcom/samsung/android/scloud/auth/AuthManager;

    iget-object v2, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getTimeStamp()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mCtid:Ljava/lang/String;

    new-instance v6, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$6;

    invoke-direct {v6, p0, p1}, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$6;-><init>(Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;Lcom/samsung/android/scloud/sync/data/SyncItem;)V

    move-object v3, p1

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceManager;->deleteNote(Landroid/content/Context;Lcom/samsung/android/scloud/auth/AuthManager;Lcom/samsung/android/scloud/sync/model/IModel;Lcom/samsung/android/scloud/sync/data/SyncItem;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/scloud/framework/network/NetworkUtil$StringResponseHandler;)V
    :try_end_0
    .catch Lcom/samsung/android/scloud/framework/PDMException; {:try_start_0 .. :try_end_0} :catch_0

    .line 227
    const/4 v0, 0x1

    .line 237
    :goto_0
    return v0

    .line 228
    :catch_0
    move-exception v7

    .line 229
    .local v7, "e":Lcom/samsung/android/scloud/framework/PDMException;
    const-string v0, "SNoteServiceControl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception in transaction - ctid : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mCtid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v7}, Lcom/samsung/android/scloud/framework/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 231
    const/16 v0, 0x132

    invoke-virtual {v7}, Lcom/samsung/android/scloud/framework/PDMException;->getExceptionCode()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 232
    throw v7

    .line 233
    :cond_0
    invoke-virtual {v7}, Lcom/samsung/android/scloud/framework/PDMException;->getMessage()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v7}, Lcom/samsung/android/scloud/framework/PDMException;->getMessage()Ljava/lang/String;

    move-result-object v0

    const-string v1, "\"rcode\":20000"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 234
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mListener:Lcom/samsung/android/scloud/framework/IStatusListener;

    const-string v1, "SNoteServiceControl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    invoke-interface {v3}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mCtid:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") : Timestamp of the key on server is bigger. item : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", e: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v7}, Lcom/samsung/android/scloud/framework/PDMException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v8, v1, v2}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 235
    new-instance v0, Lcom/samsung/android/scloud/framework/PDMException;

    const/16 v1, 0x141

    invoke-virtual {v7}, Lcom/samsung/android/scloud/framework/PDMException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/scloud/framework/PDMException;-><init>(ILjava/lang/String;)V

    throw v0

    :cond_1
    move v0, v8

    .line 237
    goto/16 :goto_0
.end method

.method public downloadItem(Lcom/samsung/android/scloud/sync/data/SyncItem;)Z
    .locals 6
    .param p1, "item"    # Lcom/samsung/android/scloud/sync/data/SyncItem;

    .prologue
    .line 193
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mAuth:Lcom/samsung/android/scloud/auth/AuthManager;

    iget-object v2, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    iget-object v4, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mCtid:Ljava/lang/String;

    new-instance v5, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$5;

    invoke-direct {v5, p0, p1}, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$5;-><init>(Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;Lcom/samsung/android/scloud/sync/data/SyncItem;)V

    move-object v3, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceManager;->downloadNote(Landroid/content/Context;Lcom/samsung/android/scloud/auth/AuthManager;Lcom/samsung/android/scloud/sync/model/IModel;Lcom/samsung/android/scloud/sync/data/SyncItem;Ljava/lang/String;Lcom/samsung/android/scloud/framework/network/NetworkUtil$FileResponseHandler;)V

    .line 211
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mListener:Lcom/samsung/android/scloud/framework/IStatusListener;

    const/4 v1, 0x4

    const-string v2, "SNoteServiceControl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "downsync content data file download finished - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mCtid:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " , key : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getSyncKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 213
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    invoke-interface {v0}, Lcom/samsung/android/scloud/sync/model/IModel;->getOEMControl()Lcom/samsung/android/scloud/sync/oem/IOEMControl;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    const/16 v3, 0x12d

    invoke-interface {v0, v1, v2, p1, v3}, Lcom/samsung/android/scloud/sync/oem/IOEMControl;->complete(Landroid/content/Context;Lcom/samsung/android/scloud/sync/model/IModel;Lcom/samsung/android/scloud/sync/data/SyncItem;I)Z

    move-result v0

    return v0
.end method

.method public getKeys(Ljava/lang/String;JLjava/util/HashMap;)Ljava/lang/String;
    .locals 11
    .param p1, "startKey"    # Ljava/lang/String;
    .param p2, "lastSyncTime"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/scloud/sync/data/SyncItem;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .local p4, "outServerChanges":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/samsung/android/scloud/sync/data/SyncItem;>;"
    const/4 v10, 0x0

    .line 88
    const/4 v0, 0x1

    new-array v9, v0, [Ljava/lang/String;

    const-string v0, ""

    aput-object v0, v9, v10

    .line 89
    .local v9, "nextKey":[Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mAuth:Lcom/samsung/android/scloud/auth/AuthManager;

    iget-object v2, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mCtid:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    invoke-interface {v5}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v8, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$2;

    invoke-direct {v8, p0, v9, p4}, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$2;-><init>(Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;[Ljava/lang/String;Ljava/util/HashMap;)V

    move-object v5, p1

    move-wide v6, p2

    invoke-static/range {v0 .. v8}, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceManager;->listNote(Landroid/content/Context;Lcom/samsung/android/scloud/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/sync/model/IModel;Ljava/lang/String;Ljava/lang/String;JLcom/samsung/android/scloud/framework/network/NetworkUtil$JSONResponseHandler;)V

    .line 130
    aget-object v0, v9, v10

    return-object v0
.end method

.method public getServerTimestamp()J
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 62
    iget-object v1, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mListener:Lcom/samsung/android/scloud/framework/IStatusListener;

    const/4 v2, 0x4

    const-string v3, "SNoteServiceControl"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "checkServerTimestamp - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mCtid:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v2, v3, v4}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 64
    const/4 v1, 0x1

    new-array v0, v1, [J

    const-wide/16 v1, 0x0

    aput-wide v1, v0, v6

    .line 65
    .local v0, "serverTime":[J
    iget-object v1, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mAuth:Lcom/samsung/android/scloud/auth/AuthManager;

    iget-object v3, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    iget-object v4, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mCtid:Ljava/lang/String;

    new-instance v5, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$1;

    invoke-direct {v5, p0, v0}, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$1;-><init>(Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;[J)V

    invoke-static {v1, v2, v3, v4, v5}, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceManager;->getTimestamp(Landroid/content/Context;Lcom/samsung/android/scloud/auth/AuthManager;Lcom/samsung/android/scloud/sync/model/IModel;Ljava/lang/String;Lcom/samsung/android/scloud/framework/network/NetworkUtil$JSONResponseHandler;)V

    .line 81
    aget-wide v1, v0, v6

    return-wide v1
.end method

.method public init(Landroid/content/Context;Lcom/samsung/android/scloud/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/framework/IStatusListener;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "auth"    # Lcom/samsung/android/scloud/auth/AuthManager;
    .param p3, "ctid"    # Ljava/lang/String;
    .param p4, "listener"    # Lcom/samsung/android/scloud/framework/IStatusListener;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mContext:Landroid/content/Context;

    .line 55
    iput-object p2, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mAuth:Lcom/samsung/android/scloud/auth/AuthManager;

    .line 56
    iput-object p3, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mCtid:Ljava/lang/String;

    .line 57
    iput-object p4, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mListener:Lcom/samsung/android/scloud/framework/IStatusListener;

    .line 58
    return-void
.end method

.method public uploadItem(Lcom/samsung/android/scloud/sync/data/SyncItem;)Z
    .locals 13
    .param p1, "item"    # Lcom/samsung/android/scloud/sync/data/SyncItem;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    invoke-interface {v0}, Lcom/samsung/android/scloud/sync/model/IModel;->getOEMControl()Lcom/samsung/android/scloud/sync/oem/IOEMControl;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    invoke-virtual {p1}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getLocalId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/scloud/sync/oem/IOEMControl;->getAttachmentFileInfo(Landroid/content/Context;Lcom/samsung/android/scloud/sync/model/IModel;Ljava/lang/String;)Lcom/samsung/android/scloud/sync/data/Attachments;

    move-result-object v4

    .line 139
    .local v4, "attachments":Lcom/samsung/android/scloud/sync/data/Attachments;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    invoke-interface {v0}, Lcom/samsung/android/scloud/sync/model/IModel;->getOEMControl()Lcom/samsung/android/scloud/sync/oem/IOEMControl;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    invoke-interface {v0, v1, v2, p1, v4}, Lcom/samsung/android/scloud/sync/oem/IOEMControl;->getLocalChange(Landroid/content/Context;Lcom/samsung/android/scloud/sync/model/IModel;Lcom/samsung/android/scloud/sync/data/SyncItem;Lcom/samsung/android/scloud/sync/data/Attachments;)Z

    .line 141
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mAuth:Lcom/samsung/android/scloud/auth/AuthManager;

    iget-object v2, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    iget-object v5, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mCtid:Ljava/lang/String;

    new-instance v6, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$3;

    invoke-direct {v6, p0}, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$3;-><init>(Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;)V

    new-instance v7, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$4;

    invoke-direct {v7, p0, p1}, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$4;-><init>(Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;Lcom/samsung/android/scloud/sync/data/SyncItem;)V

    move-object v3, p1

    invoke-static/range {v0 .. v7}, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceManager;->uploadNote(Landroid/content/Context;Lcom/samsung/android/scloud/auth/AuthManager;Lcom/samsung/android/scloud/sync/model/IModel;Lcom/samsung/android/scloud/sync/data/SyncItem;Lcom/samsung/android/scloud/sync/data/Attachments;Ljava/lang/String;Lcom/samsung/android/scloud/framework/network/NetworkUtil$PDMProgressListener;Lcom/samsung/android/scloud/framework/network/NetworkUtil$JSONResponseHandler;)V
    :try_end_0
    .catch Lcom/samsung/android/scloud/framework/PDMException; {:try_start_0 .. :try_end_0} :catch_0

    .line 159
    const/4 v0, 0x1

    return v0

    .line 161
    :catch_0
    move-exception v8

    .line 162
    .local v8, "e":Lcom/samsung/android/scloud/framework/PDMException;
    invoke-virtual {v8}, Lcom/samsung/android/scloud/framework/PDMException;->getMessage()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v8}, Lcom/samsung/android/scloud/framework/PDMException;->getMessage()Ljava/lang/String;

    move-result-object v0

    const-string v1, "\"rcode\":79902"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mListener:Lcom/samsung/android/scloud/framework/IStatusListener;

    const/4 v1, 0x0

    const-string v2, "SNoteServiceControl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "["

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    invoke-interface {v5}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "]("

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mCtid:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ") : Snote syncpath already exists. fileName : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getTag()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", e: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v8}, Lcom/samsung/android/scloud/framework/PDMException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 164
    const/16 v0, 0x140

    invoke-virtual {v8, v0}, Lcom/samsung/android/scloud/framework/PDMException;->setCode(I)V

    .line 167
    :try_start_1
    invoke-virtual {v8}, Lcom/samsung/android/scloud/framework/PDMException;->getTag()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    .line 168
    .local v12, "response":Ljava/lang/String;
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, v12}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v1, "datakey_list"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v10

    .line 169
    .local v10, "json":Lorg/json/JSONArray;
    const/4 v0, 0x0

    invoke-virtual {v10, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 171
    .local v11, "key":Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mListener:Lcom/samsung/android/scloud/framework/IStatusListener;

    const/4 v1, 0x0

    const-string v2, "SNoteServiceControl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "["

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    invoke-interface {v5}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "]("

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mCtid:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ") : change item sync key to server key and set isNew as true to set changed syncKey in local."

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 172
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mListener:Lcom/samsung/android/scloud/framework/IStatusListener;

    const/4 v1, 0x0

    const-string v2, "SNoteServiceControl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "["

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    invoke-interface {v5}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "]("

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mCtid:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ") : before change. "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 173
    invoke-virtual {p1, v11}, Lcom/samsung/android/scloud/sync/data/SyncItem;->setSyncKey(Ljava/lang/String;)V

    .line 174
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/samsung/android/scloud/sync/data/SyncItem;->setIsNew(Z)V

    .line 175
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mListener:Lcom/samsung/android/scloud/framework/IStatusListener;

    const/4 v1, 0x0

    const-string v2, "SNoteServiceControl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "["

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    invoke-interface {v5}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "]("

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mCtid:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ") : changed. "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 181
    .end local v10    # "json":Lorg/json/JSONArray;
    .end local v11    # "key":Ljava/lang/String;
    .end local v12    # "response":Ljava/lang/String;
    :goto_0
    throw v8

    .line 177
    :catch_1
    move-exception v9

    .line 178
    .local v9, "ee":Ljava/lang/Exception;
    const-string v0, "SNoteServiceControl"

    const-string v1, "parsing error"

    invoke-static {v0, v1, v9}, Lcom/samsung/android/scloud/framework/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 182
    .end local v9    # "ee":Ljava/lang/Exception;
    :cond_0
    invoke-virtual {v8}, Lcom/samsung/android/scloud/framework/PDMException;->getMessage()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v8}, Lcom/samsung/android/scloud/framework/PDMException;->getMessage()Ljava/lang/String;

    move-result-object v0

    const-string v1, "\"rcode\":20000"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 183
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mListener:Lcom/samsung/android/scloud/framework/IStatusListener;

    const/4 v1, 0x0

    const-string v2, "SNoteServiceControl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "["

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    invoke-interface {v5}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "]("

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mCtid:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ") : Timestamp of the key on server is bigger. item : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", e: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v8}, Lcom/samsung/android/scloud/framework/PDMException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 184
    const/16 v0, 0x141

    invoke-virtual {v8, v0}, Lcom/samsung/android/scloud/framework/PDMException;->setCode(I)V

    .line 185
    throw v8

    .line 187
    :cond_1
    throw v8
.end method
