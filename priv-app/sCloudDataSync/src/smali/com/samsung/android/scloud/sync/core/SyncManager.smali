.class public Lcom/samsung/android/scloud/sync/core/SyncManager;
.super Ljava/lang/Object;
.source "SyncManager.java"

# interfaces
.implements Lcom/samsung/android/scloud/framework/IStatusListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "SyncManager_"


# instance fields
.field private final PAUSE_LOCK:Ljava/lang/Object;

.field private isCanceled:Z

.field private isFailed:Z

.field private isInProcess:Z

.field private isPaused:Z

.field private mAuthManager:Lcom/samsung/android/scloud/auth/AuthManager;

.field private mCtid:Ljava/lang/String;

.field private mMetaManager:Lcom/samsung/android/scloud/sync/common/MetaManager;

.field private mModel:Lcom/samsung/android/scloud/sync/model/IModel;

.field private mServiceType:I

.field private final mTAG:Ljava/lang/String;

.field private mWaitFlag:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/scloud/sync/model/IModel;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "model"    # Lcom/samsung/android/scloud/sync/model/IModel;

    .prologue
    const/4 v1, 0x0

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mMetaManager:Lcom/samsung/android/scloud/sync/common/MetaManager;

    .line 53
    iput-boolean v1, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->isPaused:Z

    .line 54
    iput-boolean v1, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->isCanceled:Z

    .line 55
    iput-boolean v1, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->isFailed:Z

    .line 57
    iput-boolean v1, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->isInProcess:Z

    .line 61
    iput-boolean v1, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mWaitFlag:Z

    .line 62
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->PAUSE_LOCK:Ljava/lang/Object;

    .line 65
    invoke-static {p1}, Lcom/samsung/android/scloud/sync/common/MetaManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/sync/common/MetaManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mMetaManager:Lcom/samsung/android/scloud/sync/common/MetaManager;

    .line 66
    iput-object p2, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    .line 67
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SyncManager_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mTAG:Ljava/lang/String;

    .line 68
    return-void
.end method

.method private postOperation(Landroid/content/Context;IILjava/lang/String;Ljava/lang/Object;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "serviceType"    # I
    .param p3, "rCode"    # I
    .param p4, "msg"    # Ljava/lang/String;
    .param p5, "obj"    # Ljava/lang/Object;

    .prologue
    const/16 v8, 0xcd

    .line 90
    iget-object v5, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mTAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "postOperation "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    const/16 v5, 0x13c

    if-eq p3, v5, :cond_2

    .line 94
    iget-object v6, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->PAUSE_LOCK:Ljava/lang/Object;

    monitor-enter v6

    .line 95
    :try_start_0
    iget-boolean v5, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->isCanceled:Z

    if-eqz v5, :cond_0

    .line 96
    const/16 p3, 0x132

    .line 98
    :cond_0
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->isInProcess:Z

    .line 100
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->isCanceled:Z

    .line 102
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->isPaused:Z

    .line 103
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->isFailed:Z

    .line 104
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 106
    const-string v5, ""

    iput-object v5, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mCtid:Ljava/lang/String;

    .line 108
    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    .line 109
    .local v1, "dir":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 110
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .local v0, "arr$":[Ljava/io/File;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_2

    aget-object v4, v0, v2

    .line 111
    .local v4, "tempFile":Ljava/io/File;
    if-eqz v4, :cond_1

    .line 112
    iget-object v5, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mTAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Files : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v4}, Ljava/io/File;->isFile()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    invoke-interface {v6}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 114
    iget-object v5, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mTAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Delete temp File : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 104
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v1    # "dir":Ljava/io/File;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "tempFile":Ljava/io/File;
    :catchall_0
    move-exception v5

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v5

    .line 122
    :cond_2
    if-eqz p5, :cond_3

    .line 123
    invoke-virtual {p0, p2, v8, p3, p5}, Lcom/samsung/android/scloud/sync/core/SyncManager;->sendMessageToActivity(IIILjava/lang/Object;)V

    .line 127
    :goto_1
    return-void

    .line 125
    :cond_3
    invoke-virtual {p0, p2, v8, p3}, Lcom/samsung/android/scloud/sync/core/SyncManager;->sendMessageToActivity(III)V

    goto :goto_1
.end method

.method private preOperation(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "trigger"    # Ljava/lang/String;

    .prologue
    .line 71
    iget-object v1, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->PAUSE_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 72
    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->isInProcess:Z

    if-eqz v0, :cond_0

    .line 73
    new-instance v0, Lcom/samsung/android/scloud/framework/PDMException;

    const/16 v2, 0x13c

    invoke-direct {v0, v2}, Lcom/samsung/android/scloud/framework/PDMException;-><init>(I)V

    throw v0

    .line 76
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 75
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->isInProcess:Z

    .line 76
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 77
    const/16 v0, 0xa

    invoke-static {v0}, Lcom/samsung/android/scloud/framework/util/PDMUtil;->generateCTID(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mCtid:Ljava/lang/String;

    .line 78
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mTAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "preOperation.. SYNC VERSION - 5.6.5, CTID : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mCtid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/framework/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mMetaManager:Lcom/samsung/android/scloud/sync/common/MetaManager;

    iget-object v1, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    invoke-interface {v1}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/sync/common/MetaManager;->initProgress(Ljava/lang/String;)V

    .line 82
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mTAG:Ljava/lang/String;

    const-string v1, "Request auth information to dataRelay... "

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    new-instance v0, Lcom/samsung/android/scloud/auth/AuthControl;

    invoke-direct {v0}, Lcom/samsung/android/scloud/auth/AuthControl;-><init>()V

    iget-object v1, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mCtid:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, Lcom/samsung/android/scloud/auth/AuthControl;->getAuthInformation(Landroid/content/Context;Ljava/lang/String;)Lcom/samsung/android/scloud/auth/AuthManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mAuthManager:Lcom/samsung/android/scloud/auth/AuthManager;

    .line 85
    return-void
.end method


# virtual methods
.method public checkAndLog(ILjava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "logLevel"    # I
    .param p2, "tag"    # Ljava/lang/String;
    .param p3, "msg"    # Ljava/lang/String;

    .prologue
    .line 285
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "[TID : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1, p3}, Lcom/samsung/android/scloud/framework/util/LOG;->log(ILjava/lang/String;Ljava/lang/String;)V

    .line 286
    iget-boolean v1, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->isPaused:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->isCanceled:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->isFailed:Z

    if-eqz v1, :cond_5

    .line 287
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->PAUSE_LOCK:Ljava/lang/Object;

    monitor-enter v2

    .line 288
    :try_start_0
    iget-boolean v1, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->isPaused:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_2

    .line 290
    :try_start_1
    iget-object v1, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mTAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isPaused : wait!! [TID : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Thread;->getId()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]- paused : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->isPaused:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mWaitFlag:Z

    .line 292
    :goto_0
    iget-boolean v1, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mWaitFlag:Z

    if-eqz v1, :cond_1

    .line 293
    iget-object v1, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->PAUSE_LOCK:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 295
    :catch_0
    move-exception v0

    .line 296
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_2
    iget-object v1, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mTAG:Ljava/lang/String;

    invoke-static {v1, p3, v0}, Lcom/samsung/android/scloud/framework/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 297
    new-instance v1, Lcom/samsung/android/scloud/framework/PDMException;

    const/16 v3, 0x134

    invoke-direct {v1, v3, v0}, Lcom/samsung/android/scloud/framework/PDMException;-><init>(ILjava/lang/Throwable;)V

    throw v1

    .line 306
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 294
    :cond_1
    :try_start_3
    iget-object v1, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mTAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isPaused : notified!! [TID : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Thread;->getId()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]- paused : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->isPaused:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 300
    :cond_2
    :try_start_4
    iget-boolean v1, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->isCanceled:Z

    if-eqz v1, :cond_3

    .line 301
    new-instance v1, Lcom/samsung/android/scloud/framework/PDMException;

    const/16 v3, 0x132

    invoke-direct {v1, v3}, Lcom/samsung/android/scloud/framework/PDMException;-><init>(I)V

    throw v1

    .line 303
    :cond_3
    iget-boolean v1, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->isFailed:Z

    if-eqz v1, :cond_4

    .line 304
    new-instance v1, Lcom/samsung/android/scloud/framework/PDMException;

    const/16 v3, 0x134

    invoke-direct {v1, v3}, Lcom/samsung/android/scloud/framework/PDMException;-><init>(I)V

    throw v1

    .line 306
    :cond_4
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 308
    :cond_5
    return-void
.end method

.method public onFinished(Landroid/content/Context;Ljava/lang/String;IILjava/lang/Object;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "sourceKey"    # Ljava/lang/String;
    .param p3, "serviceType"    # I
    .param p4, "rCode"    # I
    .param p5, "obj"    # Ljava/lang/Object;

    .prologue
    .line 220
    iget-object v1, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mTAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onFinished : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p4

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p5

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/framework/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    const/4 v9, 0x1

    .line 227
    .local v9, "isFinish":Z
    const/4 v10, 0x1

    .line 228
    .local v10, "isSuc":Z
    const/4 v8, 0x1

    .line 229
    .local v8, "isDoNothing":Z
    const/4 v11, 0x0

    .line 230
    .local v11, "isUserCanceled":Z
    const/4 v1, -0x1

    move/from16 v0, p4

    if-eq v0, v1, :cond_3

    const/4 v1, 0x1

    :goto_0
    and-int/2addr v9, v1

    .line 231
    const/16 v1, 0x12d

    move/from16 v0, p4

    if-ne v0, v1, :cond_4

    const/4 v1, 0x1

    :goto_1
    and-int/2addr v10, v1

    .line 232
    const/16 v1, 0x12e

    move/from16 v0, p4

    if-ne v0, v1, :cond_5

    const/4 v1, 0x1

    :goto_2
    and-int/2addr v8, v1

    .line 233
    const/16 v1, 0x132

    move/from16 v0, p4

    if-ne v0, v1, :cond_6

    const/4 v1, 0x1

    :goto_3
    or-int/2addr v11, v1

    .line 234
    const/16 v1, 0x12d

    move/from16 v0, p4

    if-eq v0, v1, :cond_0

    const/16 v1, 0x12e

    move/from16 v0, p4

    if-eq v0, v1, :cond_0

    const/16 v1, 0x132

    move/from16 v0, p4

    if-eq v0, v1, :cond_0

    .line 238
    if-eqz v11, :cond_0

    .line 239
    const/16 p4, 0x132

    .line 241
    :cond_0
    if-eqz v8, :cond_1

    .line 242
    const/16 p4, 0x12e

    .line 244
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mTAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onFinished : isFinish-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", isSuc-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", isDoNothing-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", isUserCanceled-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/framework/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    if-eqz v9, :cond_2

    .line 247
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0xcd

    move/from16 v0, p4

    invoke-static {p3, v2, v0}, Lcom/samsung/android/scloud/framework/PDMConstants;->makeMassageCode(III)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v1, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    invoke-interface {v1}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v6

    move-object v1, p0

    move-object v2, p1

    move v3, p3

    move/from16 v4, p4

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/scloud/sync/core/SyncManager;->postOperation(Landroid/content/Context;IILjava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/samsung/android/scloud/framework/PDMException; {:try_start_0 .. :try_end_0} :catch_0

    .line 253
    :cond_2
    :goto_4
    return-void

    .line 230
    :cond_3
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 231
    :cond_4
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 232
    :cond_5
    const/4 v1, 0x0

    goto/16 :goto_2

    .line 233
    :cond_6
    const/4 v1, 0x0

    goto/16 :goto_3

    .line 250
    :catch_0
    move-exception v7

    .line 251
    .local v7, "bnrE":Lcom/samsung/android/scloud/framework/PDMException;
    invoke-virtual {v7}, Lcom/samsung/android/scloud/framework/PDMException;->getExceptionCode()I

    move-result v4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7}, Lcom/samsung/android/scloud/framework/PDMException;->getExceptionCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Lcom/samsung/android/scloud/framework/PDMException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v1, p0

    move-object v2, p1

    move v3, p3

    move-object/from16 v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/scloud/sync/core/SyncManager;->postOperation(Landroid/content/Context;IILjava/lang/String;Ljava/lang/Object;)V

    goto :goto_4
.end method

.method public onProgress(Ljava/lang/String;II)V
    .locals 7
    .param p1, "sourceKey"    # Ljava/lang/String;
    .param p2, "serviceType"    # I
    .param p3, "progress"    # I

    .prologue
    .line 199
    iget-object v3, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mMetaManager:Lcom/samsung/android/scloud/sync/common/MetaManager;

    invoke-virtual {v3}, Lcom/samsung/android/scloud/sync/common/MetaManager;->getTotalProgress()I

    move-result v2

    .line 200
    .local v2, "preTotal":I
    iget-object v3, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mMetaManager:Lcom/samsung/android/scloud/sync/common/MetaManager;

    invoke-virtual {v3, p1}, Lcom/samsung/android/scloud/sync/common/MetaManager;->getProgress(Ljava/lang/String;)I

    move-result v1

    .line 201
    .local v1, "preProc":I
    iget-object v3, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mMetaManager:Lcom/samsung/android/scloud/sync/common/MetaManager;

    invoke-virtual {v3, p1, p3}, Lcom/samsung/android/scloud/sync/common/MetaManager;->setProgress(Ljava/lang/String;I)V

    .line 202
    iget-object v3, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mMetaManager:Lcom/samsung/android/scloud/sync/common/MetaManager;

    invoke-virtual {v3}, Lcom/samsung/android/scloud/sync/common/MetaManager;->getTotalProgress()I

    move-result v0

    .line 203
    .local v0, "nowTotal":I
    if-eq v1, p3, :cond_0

    .line 204
    const/4 v3, 0x4

    iget-object v4, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mTAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onProgress : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", Total : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v3, v4, v5}, Lcom/samsung/android/scloud/sync/core/SyncManager;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 205
    :cond_0
    if-eq v2, v0, :cond_1

    .line 206
    const/16 v3, 0xca

    const/16 v4, 0x12d

    invoke-virtual {p0, p2, v3, v4, p3}, Lcom/samsung/android/scloud/sync/core/SyncManager;->sendMessageToActivity(IIII)V

    .line 210
    :cond_1
    return-void
.end method

.method public performSync(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLandroid/content/SyncResult;)J
    .locals 14
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "trigger"    # Ljava/lang/String;
    .param p3, "accountName"    # Ljava/lang/String;
    .param p4, "lastSyncTime"    # J
    .param p6, "syncResult"    # Landroid/content/SyncResult;

    .prologue
    .line 131
    const-wide/16 v12, 0x0

    .line 132
    .local v12, "nextLastSyncTime":J
    const/16 v1, 0x65

    iput v1, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mServiceType:I

    .line 134
    :try_start_0
    invoke-direct/range {p0 .. p2}, Lcom/samsung/android/scloud/sync/core/SyncManager;->preOperation(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/samsung/android/scloud/framework/PDMException; {:try_start_0 .. :try_end_0} :catch_1

    .line 137
    :try_start_1
    iget-object v3, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    iget-object v4, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mAuthManager:Lcom/samsung/android/scloud/auth/AuthManager;

    iget-object v5, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mCtid:Ljava/lang/String;

    move-object v1, p1

    move-object/from16 v2, p3

    move-wide/from16 v6, p4

    move-object/from16 v8, p6

    move-object v9, p0

    invoke-static/range {v1 .. v9}, Lcom/samsung/android/scloud/sync/core/SyncTask;->performSync(Landroid/content/Context;Ljava/lang/String;Lcom/samsung/android/scloud/sync/model/IModel;Lcom/samsung/android/scloud/auth/AuthManager;Ljava/lang/String;JLandroid/content/SyncResult;Lcom/samsung/android/scloud/framework/IStatusListener;)J
    :try_end_1
    .catch Lcom/samsung/android/scloud/framework/PDMException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    move-result-wide v12

    .line 153
    :goto_0
    return-wide v12

    .line 138
    :catch_0
    move-exception v11

    .line 139
    .local v11, "e":Lcom/samsung/android/scloud/framework/PDMException;
    :try_start_2
    iget-object v1, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mTAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "PDMException in performSync : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v11}, Lcom/samsung/android/scloud/framework/PDMException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v11}, Lcom/samsung/android/scloud/framework/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 140
    move-object/from16 v0, p6

    iget-object v1, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v1, Landroid/content/SyncStats;->numAuthExceptions:J

    .line 141
    iget-object v1, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    invoke-interface {v1}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x65

    invoke-virtual {p0, p1, v1, v2, v11}, Lcom/samsung/android/scloud/sync/core/SyncManager;->throwException(Landroid/content/Context;Ljava/lang/String;ILcom/samsung/android/scloud/framework/PDMException;)V
    :try_end_2
    .catch Lcom/samsung/android/scloud/framework/PDMException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 148
    .end local v11    # "e":Lcom/samsung/android/scloud/framework/PDMException;
    :catch_1
    move-exception v10

    .line 149
    .local v10, "bnrE":Lcom/samsung/android/scloud/framework/PDMException;
    iget-object v1, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mTAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "PDMException in performSync : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v10}, Lcom/samsung/android/scloud/framework/PDMException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v10}, Lcom/samsung/android/scloud/framework/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 150
    move-object/from16 v0, p6

    iget-object v1, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v1, Landroid/content/SyncStats;->numAuthExceptions:J

    .line 151
    const/16 v3, 0x65

    invoke-virtual {v10}, Lcom/samsung/android/scloud/framework/PDMException;->getExceptionCode()I

    move-result v4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10}, Lcom/samsung/android/scloud/framework/PDMException;->getExceptionCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v10}, Lcom/samsung/android/scloud/framework/PDMException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/scloud/sync/core/SyncManager;->postOperation(Landroid/content/Context;IILjava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 142
    .end local v10    # "bnrE":Lcom/samsung/android/scloud/framework/PDMException;
    :catch_2
    move-exception v11

    .line 143
    .local v11, "e":Ljava/lang/Exception;
    :try_start_3
    iget-object v1, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mTAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception in performSync : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v11}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v11}, Lcom/samsung/android/scloud/framework/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 144
    move-object/from16 v0, p6

    iget-object v1, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v1, Landroid/content/SyncStats;->numAuthExceptions:J

    .line 145
    iget-object v1, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    invoke-interface {v1}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x65

    new-instance v3, Lcom/samsung/android/scloud/framework/PDMException;

    const/16 v4, 0x135

    invoke-direct {v3, v4, v11}, Lcom/samsung/android/scloud/framework/PDMException;-><init>(ILjava/lang/Throwable;)V

    invoke-virtual {p0, p1, v1, v2, v3}, Lcom/samsung/android/scloud/sync/core/SyncManager;->throwException(Landroid/content/Context;Ljava/lang/String;ILcom/samsung/android/scloud/framework/PDMException;)V
    :try_end_3
    .catch Lcom/samsung/android/scloud/framework/PDMException; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_0
.end method

.method public requestCancel(Ljava/lang/String;)V
    .locals 4
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 172
    iget-object v1, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->PAUSE_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 173
    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->isInProcess:Z

    if-eqz v0, :cond_0

    .line 174
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->isCanceled:Z

    .line 175
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->isPaused:Z

    .line 177
    invoke-static {p1}, Lcom/samsung/android/scloud/framework/network/NetworkUtil;->cancelRequestes(Ljava/lang/String;)V

    .line 179
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mWaitFlag:Z

    .line 180
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->PAUSE_LOCK:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 181
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mTAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "requestCancel: isPaused - notify!!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->isPaused:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    :goto_0
    monitor-exit v1

    .line 185
    return-void

    .line 183
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mTAG:Ljava/lang/String;

    const-string v2, "requestCancel : already finished!!"

    invoke-static {v0, v2}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 184
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public requestPause()V
    .locals 4

    .prologue
    .line 161
    iget-object v1, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->PAUSE_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 162
    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->isInProcess:Z

    if-eqz v0, :cond_0

    .line 163
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->isPaused:Z

    .line 165
    iget v0, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mServiceType:I

    const/16 v2, 0xcb

    const/16 v3, 0x12d

    invoke-virtual {p0, v0, v2, v3}, Lcom/samsung/android/scloud/sync/core/SyncManager;->sendMessageToActivity(III)V

    .line 168
    :goto_0
    monitor-exit v1

    .line 169
    return-void

    .line 167
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mTAG:Ljava/lang/String;

    const-string v2, "requestPause : already finished!!"

    invoke-static {v0, v2}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 168
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public requestResume()V
    .locals 4

    .prologue
    .line 188
    iget-object v1, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->PAUSE_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 189
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->isPaused:Z

    .line 190
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mWaitFlag:Z

    .line 191
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->PAUSE_LOCK:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 192
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mTAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "requestResume : isPaused - notify!!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->isPaused:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    iget v0, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mServiceType:I

    const/16 v2, 0xcc

    const/16 v3, 0x12d

    invoke-virtual {p0, v0, v2, v3}, Lcom/samsung/android/scloud/sync/core/SyncManager;->sendMessageToActivity(III)V

    .line 194
    monitor-exit v1

    .line 195
    return-void

    .line 194
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public sendMessageToActivity(III)V
    .locals 5
    .param p1, "serviceType"    # I
    .param p2, "status"    # I
    .param p3, "rCode"    # I

    .prologue
    .line 256
    invoke-static {p1, p2, p3}, Lcom/samsung/android/scloud/framework/PDMConstants;->makeMassageCode(III)I

    move-result v1

    .line 257
    .local v1, "msgCode":I
    iget-object v2, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mTAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sendMessageToActivity : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 259
    .local v0, "msg":Landroid/os/Message;
    iput v1, v0, Landroid/os/Message;->what:I

    .line 260
    iget-object v2, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    invoke-interface {v2}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 261
    invoke-static {v0}, Lcom/samsung/android/scloud/framework/PDMApp;->sendMessageToActivities(Landroid/os/Message;)V

    .line 262
    return-void
.end method

.method public sendMessageToActivity(IIII)V
    .locals 5
    .param p1, "serviceType"    # I
    .param p2, "status"    # I
    .param p3, "rCode"    # I
    .param p4, "progress"    # I

    .prologue
    .line 274
    invoke-static {p1, p2, p3}, Lcom/samsung/android/scloud/framework/PDMConstants;->makeMassageCode(III)I

    move-result v1

    .line 275
    .local v1, "msgCode":I
    iget-object v2, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mTAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sendMessageToActivity : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 277
    .local v0, "msg":Landroid/os/Message;
    iput v1, v0, Landroid/os/Message;->what:I

    .line 278
    iput p4, v0, Landroid/os/Message;->arg1:I

    .line 279
    iget-object v2, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    invoke-interface {v2}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 280
    invoke-static {v0}, Lcom/samsung/android/scloud/framework/PDMApp;->sendMessageToActivities(Landroid/os/Message;)V

    .line 281
    return-void
.end method

.method public sendMessageToActivity(IIILjava/lang/Object;)V
    .locals 5
    .param p1, "serviceType"    # I
    .param p2, "status"    # I
    .param p3, "rCode"    # I
    .param p4, "data"    # Ljava/lang/Object;

    .prologue
    .line 265
    invoke-static {p1, p2, p3}, Lcom/samsung/android/scloud/framework/PDMConstants;->makeMassageCode(III)I

    move-result v1

    .line 266
    .local v1, "msgCode":I
    iget-object v2, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mTAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sendMessageToActivity : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 268
    .local v0, "msg":Landroid/os/Message;
    iput v1, v0, Landroid/os/Message;->what:I

    .line 269
    iput-object p4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 270
    invoke-static {v0}, Lcom/samsung/android/scloud/framework/PDMApp;->sendMessageToActivities(Landroid/os/Message;)V

    .line 271
    return-void
.end method

.method public throwException(Landroid/content/Context;Ljava/lang/String;ILcom/samsung/android/scloud/framework/PDMException;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "sourceKey"    # Ljava/lang/String;
    .param p3, "serviceType"    # I
    .param p4, "e"    # Lcom/samsung/android/scloud/framework/PDMException;

    .prologue
    .line 313
    iget-boolean v0, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->isFailed:Z

    if-nez v0, :cond_0

    .line 314
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->isFailed:Z

    .line 315
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/core/SyncManager;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;

    invoke-interface {v0}, Lcom/samsung/android/scloud/sync/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/scloud/framework/network/NetworkUtil;->cancelRequestes(Ljava/lang/String;)V

    .line 317
    :cond_0
    invoke-virtual {p4}, Lcom/samsung/android/scloud/framework/PDMException;->getExceptionCode()I

    move-result v4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p4}, Lcom/samsung/android/scloud/framework/PDMException;->getExceptionCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p4}, Lcom/samsung/android/scloud/framework/PDMException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/scloud/sync/core/SyncManager;->onFinished(Landroid/content/Context;Ljava/lang/String;IILjava/lang/Object;)V

    .line 318
    return-void
.end method
