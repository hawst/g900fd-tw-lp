.class public Lcom/samsung/android/scloud/framework/util/PDMUtil;
.super Ljava/lang/Object;
.source "PDMUtil.java"


# static fields
.field public static final ACCOUNT_AUTHORITY_URI:Landroid/net/Uri;

.field public static final ACCOUNT_CONTENT_URI:Landroid/net/Uri;

.field private static TAG:Ljava/lang/String;

.field private static clientDeviceId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 39
    const-string v0, "PDMUtil"

    sput-object v0, Lcom/samsung/android/scloud/framework/util/PDMUtil;->TAG:Ljava/lang/String;

    .line 40
    const-string v0, "content://com.msc.openprovider.openContentProvider"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/scloud/framework/util/PDMUtil;->ACCOUNT_AUTHORITY_URI:Landroid/net/Uri;

    .line 42
    sget-object v0, Lcom/samsung/android/scloud/framework/util/PDMUtil;->ACCOUNT_AUTHORITY_URI:Landroid/net/Uri;

    const-string v1, "tncRequest"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/scloud/framework/util/PDMUtil;->ACCOUNT_CONTENT_URI:Landroid/net/Uri;

    .line 54
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/scloud/framework/util/PDMUtil;->clientDeviceId:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static checkAccountValidation(Landroid/content/Context;)Z
    .locals 15
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v13, 0x1

    const/4 v14, 0x0

    const/4 v2, 0x0

    .line 134
    const/4 v12, 0x0

    .line 135
    .local v12, "tncState":I
    const/4 v11, 0x0

    .line 136
    .local v11, "nameCheckState":I
    const/4 v9, 0x0

    .line 137
    .local v9, "emailValidationState":I
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/scloud/framework/util/PDMUtil;->ACCOUNT_CONTENT_URI:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 139
    .local v7, "cursor":Landroid/database/Cursor;
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v10

    .line 141
    .local v10, "manager":Landroid/accounts/AccountManager;
    if-eqz v10, :cond_2

    .line 142
    const-string v0, "com.osp.app.signin"

    invoke-virtual {v10, v0}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v6

    .line 145
    .local v6, "accountArray":[Landroid/accounts/Account;
    array-length v0, v6

    if-lez v0, :cond_4

    .line 147
    if-eqz v7, :cond_1

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 149
    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 150
    invoke-interface {v7, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 151
    .local v8, "emailID":Ljava/lang/String;
    invoke-interface {v7, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 152
    const/4 v0, 0x2

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 153
    const/4 v0, 0x3

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 154
    sget-object v0, Lcom/samsung/android/scloud/framework/util/PDMUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "emailID : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " tncState :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " nameCheckState : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " emailValidationState : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    add-int v0, v12, v11

    add-int/2addr v0, v9

    if-nez v0, :cond_0

    .line 158
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    move v0, v13

    .line 172
    .end local v6    # "accountArray":[Landroid/accounts/Account;
    .end local v8    # "emailID":Ljava/lang/String;
    :goto_0
    return v0

    .line 163
    .restart local v6    # "accountArray":[Landroid/accounts/Account;
    :cond_1
    sget-object v0, Lcom/samsung/android/scloud/framework/util/PDMUtil;->TAG:Ljava/lang/String;

    const-string v1, "Fail To Obtain Cursor"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/framework/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    .end local v6    # "accountArray":[Landroid/accounts/Account;
    :cond_2
    :goto_1
    if-eqz v7, :cond_3

    .line 171
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_3
    move v0, v14

    .line 172
    goto :goto_0

    .line 167
    .restart local v6    # "accountArray":[Landroid/accounts/Account;
    :cond_4
    sget-object v0, Lcom/samsung/android/scloud/framework/util/PDMUtil;->TAG:Ljava/lang/String;

    const-string v1, "Samsung Account Not Logged in"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/framework/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static ensureValidFileName(Ljava/lang/String;)V
    .locals 3
    .param p0, "fileName"    # Ljava/lang/String;

    .prologue
    .line 265
    const-string v0, ".."

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "../"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "/.."

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 266
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ".. path specifier not allowed. Bad file name: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 269
    :cond_1
    return-void
.end method

.method public static generateCTID(I)Ljava/lang/String;
    .locals 6
    .param p0, "length"    # I

    .prologue
    .line 46
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 47
    .local v1, "sb":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p0, :cond_0

    .line 48
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    mul-double/2addr v2, v4

    double-to-int v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 47
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 50
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static declared-synchronized getClientDeviceId(Landroid/content/Context;)Ljava/lang/String;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 56
    const-class v3, Lcom/samsung/android/scloud/framework/util/PDMUtil;

    monitor-enter v3

    :try_start_0
    sget-object v2, Lcom/samsung/android/scloud/framework/util/PDMUtil;->clientDeviceId:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 57
    sget-object v2, Lcom/samsung/android/scloud/framework/util/PDMUtil;->clientDeviceId:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 82
    :goto_0
    monitor-exit v3

    return-object v2

    .line 59
    :cond_0
    const/4 v2, 0x0

    :try_start_1
    sput-object v2, Lcom/samsung/android/scloud/framework/util/PDMUtil;->clientDeviceId:Ljava/lang/String;

    .line 60
    const-string v2, "phone"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 62
    .local v1, "telephonyManager":Landroid/telephony/TelephonyManager;
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v0

    .line 63
    .local v0, "phoneType":I
    if-eqz v0, :cond_3

    .line 66
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/samsung/android/scloud/framework/util/PDMUtil;->clientDeviceId:Ljava/lang/String;

    .line 75
    :goto_1
    sget-object v2, Lcom/samsung/android/scloud/framework/util/PDMUtil;->clientDeviceId:Ljava/lang/String;

    if-eqz v2, :cond_1

    sget-object v2, Lcom/samsung/android/scloud/framework/util/PDMUtil;->clientDeviceId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    const/4 v4, 0x1

    if-eq v2, v4, :cond_1

    sget-object v2, Lcom/samsung/android/scloud/framework/util/PDMUtil;->clientDeviceId:Ljava/lang/String;

    const-string v4, "0"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 78
    :cond_1
    const/4 v2, 0x0

    sput-object v2, Lcom/samsung/android/scloud/framework/util/PDMUtil;->clientDeviceId:Ljava/lang/String;

    .line 82
    :cond_2
    sget-object v2, Lcom/samsung/android/scloud/framework/util/PDMUtil;->clientDeviceId:Ljava/lang/String;

    goto :goto_0

    .line 71
    :cond_3
    sget-object v2, Landroid/os/Build;->SERIAL:Ljava/lang/String;

    sput-object v2, Lcom/samsung/android/scloud/framework/util/PDMUtil;->clientDeviceId:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 56
    .end local v0    # "phoneType":I
    .end local v1    # "telephonyManager":Landroid/telephony/TelephonyManager;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method public static getIsGT_I9300()Z
    .locals 3

    .prologue
    .line 123
    const/4 v0, 0x0

    .line 124
    .local v0, "ret":Z
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "GT-I9300"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 125
    const/4 v0, 0x1

    .line 127
    :cond_0
    return v0
.end method

.method public static getIsTablet(Landroid/content/res/Configuration;)Z
    .locals 5
    .param p0, "config"    # Landroid/content/res/Configuration;

    .prologue
    .line 86
    const/4 v0, 0x0

    .line 87
    .local v0, "isTablet":Z
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xb

    if-ge v3, v4, :cond_0

    move v1, v0

    .line 111
    .end local v0    # "isTablet":Z
    .local v1, "isTablet":I
    :goto_0
    return v1

    .line 92
    .end local v1    # "isTablet":I
    .restart local v0    # "isTablet":Z
    :cond_0
    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v4, "GT-I9200"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v4, "GT-I9205"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v4, "SHV-E310"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v4, "SGH-I527"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v4, "SCH-R960"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v4, "SCH-P729"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v4, "DCH-P729"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_1
    move v1, v0

    .line 96
    .restart local v1    # "isTablet":I
    goto :goto_0

    .line 99
    .end local v1    # "isTablet":I
    :cond_2
    iget v3, p0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v2, v3, 0xf

    .line 102
    .local v2, "size":I
    packed-switch v2, :pswitch_data_0

    :goto_1
    move v1, v0

    .line 111
    .restart local v1    # "isTablet":I
    goto :goto_0

    .line 106
    .end local v1    # "isTablet":I
    :pswitch_0
    const/4 v0, 0x1

    .line 107
    goto :goto_1

    .line 102
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static getSalesInfo(Landroid/content/Context;)Ljava/lang/String;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 223
    const-string v6, ""

    .line 226
    .local v6, "ret":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v7

    const-string v8, "android.os.SystemProperties"

    invoke-virtual {v7, v8}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 228
    .local v0, "SystemProperties":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v7, 0x1

    new-array v4, v7, [Ljava/lang/Class;

    .line 229
    .local v4, "paramTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    const/4 v7, 0x0

    const-class v8, Ljava/lang/String;

    aput-object v8, v4, v7

    .line 231
    const-string v7, "get"

    invoke-virtual {v0, v7, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 234
    .local v2, "get":Ljava/lang/reflect/Method;
    const/4 v7, 0x1

    new-array v5, v7, [Ljava/lang/Object;

    .line 236
    .local v5, "params":[Ljava/lang/Object;
    const/4 v7, 0x0

    const-string v8, "ro.csc.sales_code"

    aput-object v8, v5, v7

    .line 238
    invoke-virtual {v2, v0, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "ret":Ljava/lang/String;
    check-cast v6, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 247
    .end local v0    # "SystemProperties":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v2    # "get":Ljava/lang/reflect/Method;
    .end local v4    # "paramTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    .end local v5    # "params":[Ljava/lang/Object;
    .restart local v6    # "ret":Ljava/lang/String;
    :goto_0
    return-object v6

    .line 240
    .end local v6    # "ret":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 241
    .local v3, "iAE":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v3}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 242
    const-string v6, ""

    .line 246
    .restart local v6    # "ret":Ljava/lang/String;
    goto :goto_0

    .line 243
    .end local v3    # "iAE":Ljava/lang/IllegalArgumentException;
    .end local v6    # "ret":Ljava/lang/String;
    :catch_1
    move-exception v1

    .line 244
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 245
    const-string v6, ""

    .restart local v6    # "ret":Ljava/lang/String;
    goto :goto_0
.end method

.method public static getTotalMemory(Landroid/content/Context;J)J
    .locals 22
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "initial"    # J

    .prologue
    .line 176
    move-wide/from16 v17, p1

    .line 177
    .local v17, "totalMemory":J
    const-string v15, "/proc/meminfo"

    .line 178
    .local v15, "str1":Ljava/lang/String;
    const-string v16, ""

    .line 180
    .local v16, "str2":Ljava/lang/String;
    const/4 v12, 0x0

    .line 181
    .local v12, "localFileReader":Ljava/io/FileReader;
    const/4 v10, 0x0

    .line 184
    .local v10, "localBufferedReader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v13, Ljava/io/FileReader;

    invoke-direct {v13, v15}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 185
    .end local v12    # "localFileReader":Ljava/io/FileReader;
    .local v13, "localFileReader":Ljava/io/FileReader;
    :try_start_1
    new-instance v11, Ljava/io/BufferedReader;

    const/16 v19, 0x2000

    move/from16 v0, v19

    invoke-direct {v11, v13, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 186
    .end local v10    # "localBufferedReader":Ljava/io/BufferedReader;
    .local v11, "localBufferedReader":Ljava/io/BufferedReader;
    :try_start_2
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    .line 187
    .local v5, "buf":Ljava/lang/StringBuffer;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    const/16 v19, 0x2

    move/from16 v0, v19

    if-ge v7, v0, :cond_0

    .line 188
    const-string v19, " "

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 189
    invoke-virtual {v11}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 187
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 192
    :cond_0
    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v16

    .line 193
    const-string v19, "\\s+"

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 194
    .local v4, "arrayOfString":[Ljava/lang/String;
    move-object v3, v4

    .local v3, "arr$":[Ljava/lang/String;
    array-length v9, v3

    .local v9, "len$":I
    const/4 v8, 0x0

    .local v8, "i$":I
    :goto_1
    if-ge v8, v9, :cond_1

    aget-object v14, v3, v8

    .line 195
    .local v14, "num":Ljava/lang/String;
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\t"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 198
    .end local v14    # "num":Ljava/lang/String;
    :cond_1
    const/16 v19, 0x2

    aget-object v19, v4, v19

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result v19

    move/from16 v0, v19

    int-to-long v0, v0

    move-wide/from16 v17, v0

    .line 207
    if-eqz v11, :cond_2

    .line 208
    :try_start_3
    invoke-virtual {v11}, Ljava/io/BufferedReader;->close()V

    .line 209
    :cond_2
    if-eqz v13, :cond_3

    .line 210
    invoke-virtual {v13}, Ljava/io/FileReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :cond_3
    move-object v10, v11

    .end local v11    # "localBufferedReader":Ljava/io/BufferedReader;
    .restart local v10    # "localBufferedReader":Ljava/io/BufferedReader;
    move-object v12, v13

    .line 218
    .end local v3    # "arr$":[Ljava/lang/String;
    .end local v4    # "arrayOfString":[Ljava/lang/String;
    .end local v5    # "buf":Ljava/lang/StringBuffer;
    .end local v7    # "i":I
    .end local v8    # "i$":I
    .end local v9    # "len$":I
    .end local v13    # "localFileReader":Ljava/io/FileReader;
    .restart local v12    # "localFileReader":Ljava/io/FileReader;
    :cond_4
    :goto_2
    sget-object v19, Lcom/samsung/android/scloud/framework/util/PDMUtil;->TAG:Ljava/lang/String;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "memory : size ="

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-wide/from16 v1, v17

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    return-wide v17

    .line 211
    .end local v10    # "localBufferedReader":Ljava/io/BufferedReader;
    .end local v12    # "localFileReader":Ljava/io/FileReader;
    .restart local v3    # "arr$":[Ljava/lang/String;
    .restart local v4    # "arrayOfString":[Ljava/lang/String;
    .restart local v5    # "buf":Ljava/lang/StringBuffer;
    .restart local v7    # "i":I
    .restart local v8    # "i$":I
    .restart local v9    # "len$":I
    .restart local v11    # "localBufferedReader":Ljava/io/BufferedReader;
    .restart local v13    # "localFileReader":Ljava/io/FileReader;
    :catch_0
    move-exception v6

    .line 212
    .local v6, "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    .line 213
    move-wide/from16 v17, p1

    move-object v10, v11

    .end local v11    # "localBufferedReader":Ljava/io/BufferedReader;
    .restart local v10    # "localBufferedReader":Ljava/io/BufferedReader;
    move-object v12, v13

    .line 215
    .end local v13    # "localFileReader":Ljava/io/FileReader;
    .restart local v12    # "localFileReader":Ljava/io/FileReader;
    goto :goto_2

    .line 202
    .end local v3    # "arr$":[Ljava/lang/String;
    .end local v4    # "arrayOfString":[Ljava/lang/String;
    .end local v5    # "buf":Ljava/lang/StringBuffer;
    .end local v6    # "e":Ljava/io/IOException;
    .end local v7    # "i":I
    .end local v8    # "i$":I
    .end local v9    # "len$":I
    :catch_1
    move-exception v6

    .line 203
    .restart local v6    # "e":Ljava/io/IOException;
    :goto_3
    :try_start_4
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 204
    move-wide/from16 v17, p1

    .line 207
    if-eqz v10, :cond_5

    .line 208
    :try_start_5
    invoke-virtual {v10}, Ljava/io/BufferedReader;->close()V

    .line 209
    :cond_5
    if-eqz v12, :cond_4

    .line 210
    invoke-virtual {v12}, Ljava/io/FileReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_2

    .line 211
    :catch_2
    move-exception v6

    .line 212
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    .line 213
    move-wide/from16 v17, p1

    .line 215
    goto :goto_2

    .line 206
    .end local v6    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v19

    .line 207
    :goto_4
    if-eqz v10, :cond_6

    .line 208
    :try_start_6
    invoke-virtual {v10}, Ljava/io/BufferedReader;->close()V

    .line 209
    :cond_6
    if-eqz v12, :cond_7

    .line 210
    invoke-virtual {v12}, Ljava/io/FileReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 214
    :cond_7
    :goto_5
    throw v19

    .line 211
    :catch_3
    move-exception v6

    .line 212
    .restart local v6    # "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    .line 213
    move-wide/from16 v17, p1

    goto :goto_5

    .line 206
    .end local v6    # "e":Ljava/io/IOException;
    .end local v12    # "localFileReader":Ljava/io/FileReader;
    .restart local v13    # "localFileReader":Ljava/io/FileReader;
    :catchall_1
    move-exception v19

    move-object v12, v13

    .end local v13    # "localFileReader":Ljava/io/FileReader;
    .restart local v12    # "localFileReader":Ljava/io/FileReader;
    goto :goto_4

    .end local v10    # "localBufferedReader":Ljava/io/BufferedReader;
    .end local v12    # "localFileReader":Ljava/io/FileReader;
    .restart local v11    # "localBufferedReader":Ljava/io/BufferedReader;
    .restart local v13    # "localFileReader":Ljava/io/FileReader;
    :catchall_2
    move-exception v19

    move-object v10, v11

    .end local v11    # "localBufferedReader":Ljava/io/BufferedReader;
    .restart local v10    # "localBufferedReader":Ljava/io/BufferedReader;
    move-object v12, v13

    .end local v13    # "localFileReader":Ljava/io/FileReader;
    .restart local v12    # "localFileReader":Ljava/io/FileReader;
    goto :goto_4

    .line 202
    .end local v12    # "localFileReader":Ljava/io/FileReader;
    .restart local v13    # "localFileReader":Ljava/io/FileReader;
    :catch_4
    move-exception v6

    move-object v12, v13

    .end local v13    # "localFileReader":Ljava/io/FileReader;
    .restart local v12    # "localFileReader":Ljava/io/FileReader;
    goto :goto_3

    .end local v10    # "localBufferedReader":Ljava/io/BufferedReader;
    .end local v12    # "localFileReader":Ljava/io/FileReader;
    .restart local v11    # "localBufferedReader":Ljava/io/BufferedReader;
    .restart local v13    # "localFileReader":Ljava/io/FileReader;
    :catch_5
    move-exception v6

    move-object v10, v11

    .end local v11    # "localBufferedReader":Ljava/io/BufferedReader;
    .restart local v10    # "localBufferedReader":Ljava/io/BufferedReader;
    move-object v12, v13

    .end local v13    # "localFileReader":Ljava/io/FileReader;
    .restart local v12    # "localFileReader":Ljava/io/FileReader;
    goto :goto_3
.end method

.method public static isInstalledApplication(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 251
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 253
    .local v1, "pm":Landroid/content/pm/PackageManager;
    const/16 v2, 0x80

    :try_start_0
    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    .line 254
    sget-object v2, Lcom/samsung/android/scloud/framework/util/PDMUtil;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Package Name is: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is installed."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 259
    const/4 v2, 0x1

    :goto_0
    return v2

    .line 255
    :catch_0
    move-exception v0

    .line 256
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-object v2, Lcom/samsung/android/scloud/framework/util/PDMUtil;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Package Name is: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " in NOT installed."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static isNullOrEmpty(Ljava/lang/String;)Z
    .locals 1
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 273
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static supportHeterogeneousDevice()Z
    .locals 3

    .prologue
    .line 115
    const/4 v0, 0x0

    .line 116
    .local v0, "ret":Z
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "GT-N7100"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "GT-N7105"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 117
    :cond_0
    const/4 v0, 0x1

    .line 119
    :cond_1
    return v0
.end method
