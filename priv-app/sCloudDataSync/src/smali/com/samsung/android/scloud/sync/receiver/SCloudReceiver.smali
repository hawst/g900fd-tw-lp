.class public Lcom/samsung/android/scloud/sync/receiver/SCloudReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SCloudReceiver.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SCloudReceiver"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 22
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 23
    .local v1, "action":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 45
    :cond_0
    :goto_0
    return-void

    .line 25
    :cond_1
    const-string v3, "SCloudReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onReceive : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/framework/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    const-string v3, "android.intent.action.SAMSUNGACCOUNT_SIGNIN_COMPLETED"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 27
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    .line 28
    .local v2, "am":Landroid/accounts/AccountManager;
    const/4 v0, 0x0

    .line 29
    .local v0, "accounts":[Landroid/accounts/Account;
    if-eqz v2, :cond_2

    .line 30
    const-string v3, "com.osp.app.signin"

    invoke-virtual {v2, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 31
    :cond_2
    if-eqz v0, :cond_0

    array-length v3, v0

    const/4 v4, 0x1

    if-lt v3, v4, :cond_0

    .line 34
    invoke-static {p1}, Lcom/samsung/android/scloud/sync/common/MetaManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/sync/common/MetaManager;

    move-result-object v3

    const/4 v4, 0x0

    aget-object v4, v0, v4

    invoke-virtual {v3, p1, v4}, Lcom/samsung/android/scloud/sync/common/MetaManager;->accountSignedIn(Landroid/content/Context;Landroid/accounts/Account;)V

    goto :goto_0

    .line 36
    .end local v0    # "accounts":[Landroid/accounts/Account;
    .end local v2    # "am":Landroid/accounts/AccountManager;
    :cond_3
    const-string v3, "android.intent.action.SAMSUNGACCOUNT_SIGNOUT_COMPLETED"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 37
    invoke-static {p1}, Lcom/samsung/android/scloud/sync/common/MetaManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/sync/common/MetaManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/scloud/sync/common/MetaManager;->accountRemoved()V

    goto :goto_0

    .line 42
    :cond_4
    const-string v3, "SCloudReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unknown intent received: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
