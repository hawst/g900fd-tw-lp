.class public Lcom/samsung/android/scloud/sync/core/SyncTask;
.super Ljava/lang/Object;
.source "SyncTask.java"


# static fields
.field private static final INIT_TIMESTAMP:J = 0xe8d4a51000L

.field private static final TAG:Ljava/lang/String; = "SyncTask-"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static performSync(Landroid/content/Context;Ljava/lang/String;Lcom/samsung/android/scloud/sync/model/IModel;Lcom/samsung/android/scloud/auth/AuthManager;Ljava/lang/String;JLandroid/content/SyncResult;Lcom/samsung/android/scloud/framework/IStatusListener;)J
    .locals 32
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "model"    # Lcom/samsung/android/scloud/sync/model/IModel;
    .param p3, "auth"    # Lcom/samsung/android/scloud/auth/AuthManager;
    .param p4, "ctid"    # Ljava/lang/String;
    .param p5, "lastSyncTime"    # J
    .param p7, "syncResult"    # Landroid/content/SyncResult;
    .param p8, "listener"    # Lcom/samsung/android/scloud/framework/IStatusListener;

    .prologue
    .line 56
    const/4 v4, 0x0

    const-string v5, "SyncTask-"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "["

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface/range {p2 .. p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "]("

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p4

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ") : PERFORM SYNC !!, LastSyncTime : "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, p5

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p8

    invoke-interface {v0, v4, v5, v6}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 65
    invoke-interface/range {p2 .. p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getCloudServiceControl()Lcom/samsung/android/scloud/sync/server/ICloudServiceControl;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    move-object/from16 v3, p8

    invoke-interface {v4, v0, v1, v2, v3}, Lcom/samsung/android/scloud/sync/server/ICloudServiceControl;->init(Landroid/content/Context;Lcom/samsung/android/scloud/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/framework/IStatusListener;)V

    .line 67
    const-wide/16 v21, 0x0

    .line 68
    .local v21, "result":J
    invoke-interface/range {p2 .. p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getCloudServiceControl()Lcom/samsung/android/scloud/sync/server/ICloudServiceControl;

    move-result-object v4

    invoke-interface {v4}, Lcom/samsung/android/scloud/sync/server/ICloudServiceControl;->getServerTimestamp()J

    move-result-wide v19

    .line 74
    .local v19, "nextLastSyncTime":J
    const/16 v25, 0x0

    .line 75
    .local v25, "startKey":Ljava/lang/String;
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 79
    .local v7, "serverChanges":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/samsung/android/scloud/sync/data/SyncItem;>;"
    :cond_0
    :try_start_0
    invoke-interface/range {p2 .. p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getCloudServiceControl()Lcom/samsung/android/scloud/sync/server/ICloudServiceControl;

    move-result-object v4

    move-object/from16 v0, v25

    move-wide/from16 v1, p5

    invoke-interface {v4, v0, v1, v2, v7}, Lcom/samsung/android/scloud/sync/server/ICloudServiceControl;->getKeys(Ljava/lang/String;JLjava/util/HashMap;)Ljava/lang/String;

    move-result-object v25

    .line 80
    const/4 v4, 0x4

    const-string v5, "SyncTask-"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "["

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface/range {p2 .. p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "]("

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p4

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ") : get server keys - count : "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v7}, Ljava/util/HashMap;->size()I

    move-result v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p8

    invoke-interface {v0, v4, v5, v6}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 81
    if-eqz v25, :cond_1

    const-string v4, ""

    move-object/from16 v0, v25

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/samsung/android/scloud/framework/PDMException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-eqz v4, :cond_0

    .line 87
    :cond_1
    const/4 v4, 0x4

    const-string v5, "SyncTask-"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "["

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface/range {p2 .. p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "]("

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p4

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ") : get server keys end!! serverKeys : "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v7}, Ljava/util/HashMap;->size()I

    move-result v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p8

    invoke-interface {v0, v4, v5, v6}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 89
    invoke-interface/range {p2 .. p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getOEMControl()Lcom/samsung/android/scloud/sync/oem/IOEMControl;

    move-result-object v4

    const-string v8, "com.osp.app.signin"

    move-object/from16 v5, p0

    move-object/from16 v6, p2

    move-object/from16 v9, p1

    invoke-interface/range {v4 .. v9}, Lcom/samsung/android/scloud/sync/oem/IOEMControl;->prepareToSync(Landroid/content/Context;Lcom/samsung/android/scloud/sync/model/IModel;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v18

    .line 91
    .local v18, "localKeys":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/scloud/sync/data/SyncItem;>;"
    const/4 v4, 0x4

    const-string v5, "SyncTask-"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "["

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface/range {p2 .. p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "]("

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p4

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ") : get local keys end!! cnt : "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p8

    invoke-interface {v0, v4, v5, v6}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 93
    new-instance v30, Ljava/util/ArrayList;

    invoke-direct/range {v30 .. v30}, Ljava/util/ArrayList;-><init>()V

    .line 94
    .local v30, "toUploadList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/scloud/sync/data/SyncItem;>;"
    new-instance v29, Ljava/util/ArrayList;

    invoke-direct/range {v29 .. v29}, Ljava/util/ArrayList;-><init>()V

    .line 95
    .local v29, "toDownloadList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/scloud/sync/data/SyncItem;>;"
    new-instance v27, Ljava/util/ArrayList;

    invoke-direct/range {v27 .. v27}, Ljava/util/ArrayList;-><init>()V

    .line 96
    .local v27, "toDeleteLocalList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/scloud/sync/data/SyncItem;>;"
    new-instance v28, Ljava/util/ArrayList;

    invoke-direct/range {v28 .. v28}, Ljava/util/ArrayList;-><init>()V

    .line 100
    .local v28, "toDeleteServerList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/scloud/sync/data/SyncItem;>;"
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .local v15, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_f

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/samsung/android/scloud/sync/data/SyncItem;

    .line 105
    .local v17, "localItem":Lcom/samsung/android/scloud/sync/data/SyncItem;
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/android/scloud/sync/data/SyncItem;->isNew()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 106
    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Lcom/samsung/android/scloud/sync/model/IModel;->generateSyncKey(Lcom/samsung/android/scloud/sync/data/SyncItem;)Ljava/lang/String;

    move-result-object v26

    .line 107
    .local v26, "syncKey":Ljava/lang/String;
    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/sync/data/SyncItem;->setSyncKey(Ljava/lang/String;)V

    .line 108
    const/4 v4, 0x4

    const-string v5, "SyncTask-"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "["

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface/range {p2 .. p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "]("

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p4

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ") : Generate syncKey to new local item : "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p8

    invoke-interface {v0, v4, v5, v6}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 111
    .end local v26    # "syncKey":Ljava/lang/String;
    :cond_3
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getSyncKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Lcom/samsung/android/scloud/sync/data/SyncItem;

    .line 112
    .local v23, "serverItem":Lcom/samsung/android/scloud/sync/data/SyncItem;
    if-eqz v23, :cond_c

    .line 113
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/android/scloud/sync/data/SyncItem;->isNew()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 114
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/android/scloud/sync/data/SyncItem;->isDeleted()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 115
    move-object/from16 v0, v27

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 82
    .end local v15    # "i$":Ljava/util/Iterator;
    .end local v17    # "localItem":Lcom/samsung/android/scloud/sync/data/SyncItem;
    .end local v18    # "localKeys":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/scloud/sync/data/SyncItem;>;"
    .end local v23    # "serverItem":Lcom/samsung/android/scloud/sync/data/SyncItem;
    .end local v27    # "toDeleteLocalList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/scloud/sync/data/SyncItem;>;"
    .end local v28    # "toDeleteServerList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/scloud/sync/data/SyncItem;>;"
    .end local v29    # "toDownloadList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/scloud/sync/data/SyncItem;>;"
    .end local v30    # "toUploadList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/scloud/sync/data/SyncItem;>;"
    :catch_0
    move-exception v14

    .line 83
    .local v14, "e":Lcom/samsung/android/scloud/framework/PDMException;
    move-object/from16 v0, p7

    iget-object v4, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v5, v4, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v8, 0x1

    add-long/2addr v5, v8

    iput-wide v5, v4, Landroid/content/SyncStats;->numAuthExceptions:J

    .line 84
    throw v14

    .line 116
    .end local v14    # "e":Lcom/samsung/android/scloud/framework/PDMException;
    .restart local v15    # "i$":Ljava/util/Iterator;
    .restart local v17    # "localItem":Lcom/samsung/android/scloud/sync/data/SyncItem;
    .restart local v18    # "localKeys":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/scloud/sync/data/SyncItem;>;"
    .restart local v23    # "serverItem":Lcom/samsung/android/scloud/sync/data/SyncItem;
    .restart local v27    # "toDeleteLocalList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/scloud/sync/data/SyncItem;>;"
    .restart local v28    # "toDeleteServerList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/scloud/sync/data/SyncItem;>;"
    .restart local v29    # "toDownloadList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/scloud/sync/data/SyncItem;>;"
    .restart local v30    # "toUploadList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/scloud/sync/data/SyncItem;>;"
    :cond_4
    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/scloud/sync/data/SyncItem;->isDeleted()Z

    move-result v4

    if-nez v4, :cond_5

    .line 117
    const/4 v4, 0x0

    const-string v5, "SyncTask-"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "["

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface/range {p2 .. p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "]("

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p4

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ") : duplicated sync_key on new local item : "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getSyncKey()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p8

    invoke-interface {v0, v4, v5, v6}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 118
    invoke-interface/range {p2 .. p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getOEMControl()Lcom/samsung/android/scloud/sync/oem/IOEMControl;

    move-result-object v4

    const/16 v5, 0x140

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, v17

    invoke-interface {v4, v0, v1, v2, v5}, Lcom/samsung/android/scloud/sync/oem/IOEMControl;->complete(Landroid/content/Context;Lcom/samsung/android/scloud/sync/model/IModel;Lcom/samsung/android/scloud/sync/data/SyncItem;I)Z

    .line 119
    move-object/from16 v0, p7

    iget-object v4, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v5, v4, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v8, 0x1

    add-long/2addr v5, v8

    iput-wide v5, v4, Landroid/content/SyncStats;->numAuthExceptions:J

    goto/16 :goto_0

    .line 122
    :cond_5
    move-object/from16 v0, v30

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 126
    :cond_6
    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getSyncKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getTimeStamp()J

    move-result-wide v4

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getTimeStamp()J

    move-result-wide v8

    cmp-long v4, v4, v8

    if-ltz v4, :cond_9

    .line 128
    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/scloud/sync/data/SyncItem;->isDeleted()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 129
    move-object/from16 v0, v27

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 131
    :cond_7
    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getTimeStamp()J

    move-result-wide v4

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getTimeStamp()J

    move-result-wide v8

    cmp-long v4, v4, v8

    if-eqz v4, :cond_8

    .line 132
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/android/scloud/sync/data/SyncItem;->isNew()Z

    move-result v4

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Lcom/samsung/android/scloud/sync/data/SyncItem;->setIsNew(Z)V

    .line 133
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getLocalId()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Lcom/samsung/android/scloud/sync/data/SyncItem;->setLocalId(Ljava/lang/String;)V

    .line 135
    move-object/from16 v0, v29

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 136
    :cond_8
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/android/scloud/sync/data/SyncItem;->isDeleted()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 137
    move-object/from16 v0, v28

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 140
    :cond_9
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/android/scloud/sync/data/SyncItem;->isDeleted()Z

    move-result v4

    if-eqz v4, :cond_b

    .line 141
    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/scloud/sync/data/SyncItem;->isDeleted()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 142
    move-object/from16 v0, v27

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 144
    :cond_a
    move-object/from16 v0, v28

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 146
    :cond_b
    move-object/from16 v0, v30

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 153
    :cond_c
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/android/scloud/sync/data/SyncItem;->isDeleted()Z

    move-result v4

    if-eqz v4, :cond_e

    .line 154
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/android/scloud/sync/data/SyncItem;->isNew()Z

    move-result v4

    if-nez v4, :cond_d

    .line 155
    move-object/from16 v0, v28

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 157
    :cond_d
    move-object/from16 v0, v27

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 160
    :cond_e
    move-object/from16 v0, v30

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 165
    .end local v17    # "localItem":Lcom/samsung/android/scloud/sync/data/SyncItem;
    .end local v23    # "serverItem":Lcom/samsung/android/scloud/sync/data/SyncItem;
    :cond_f
    invoke-virtual {v7}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_1
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_11

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/util/Map$Entry;

    .line 166
    .local v24, "serverSet":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/scloud/sync/data/SyncItem;>;"
    invoke-interface/range {v24 .. v24}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Lcom/samsung/android/scloud/sync/data/SyncItem;

    .line 171
    .restart local v23    # "serverItem":Lcom/samsung/android/scloud/sync/data/SyncItem;
    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/scloud/sync/data/SyncItem;->isDeleted()Z

    move-result v4

    if-eqz v4, :cond_10

    .line 172
    const/4 v5, 0x4

    const-string v6, "SyncTask-"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "["

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface/range {p2 .. p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, "]("

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, ") : Already Deleted from server and local - ServerKey : "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface/range {v24 .. v24}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p8

    invoke-interface {v0, v5, v6, v4}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 174
    :cond_10
    move-object/from16 v0, v29

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 180
    .end local v23    # "serverItem":Lcom/samsung/android/scloud/sync/data/SyncItem;
    .end local v24    # "serverSet":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/scloud/sync/data/SyncItem;>;"
    :cond_11
    const/4 v4, 0x0

    const-string v5, "SyncTask-"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "["

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface/range {p2 .. p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "]("

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p4

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ") : Compare end - toUploadList : "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface/range {v30 .. v30}, Ljava/util/List;->size()I

    move-result v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ", toDownloadList : "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface/range {v29 .. v29}, Ljava/util/List;->size()I

    move-result v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ", toDeleteLocalList : "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface/range {v27 .. v27}, Ljava/util/List;->size()I

    move-result v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ", toDeleteServerList : "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface/range {v28 .. v28}, Ljava/util/List;->size()I

    move-result v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p8

    invoke-interface {v0, v4, v5, v6}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 183
    invoke-interface/range {v30 .. v30}, Ljava/util/List;->size()I

    move-result v4

    invoke-interface/range {v29 .. v29}, Ljava/util/List;->size()I

    move-result v5

    add-int/2addr v4, v5

    invoke-interface/range {v27 .. v27}, Ljava/util/List;->size()I

    move-result v5

    add-int/2addr v4, v5

    invoke-interface/range {v28 .. v28}, Ljava/util/List;->size()I

    move-result v5

    add-int v31, v4, v5

    .line 185
    .local v31, "totalProc":I
    if-gtz v31, :cond_12

    .line 186
    const/4 v4, 0x0

    const-string v5, "SyncTask-"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "["

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface/range {p2 .. p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "]("

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p4

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ") : there is no items to sync !!"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p8

    invoke-interface {v0, v4, v5, v6}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 192
    :cond_12
    const/4 v4, 0x4

    const-string v5, "SyncTask-"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "["

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface/range {p2 .. p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "]("

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p4

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ") : Delete to server start !!"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p8

    invoke-interface {v0, v4, v5, v6}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 193
    invoke-interface/range {v28 .. v28}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_13
    :goto_2
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_17

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/samsung/android/scloud/sync/data/SyncItem;

    .line 194
    .local v16, "item":Lcom/samsung/android/scloud/sync/data/SyncItem;
    const/4 v4, 0x4

    const-string v5, "SyncTask-"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "["

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface/range {p2 .. p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "]("

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p4

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ") : Delete to server - item : "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p8

    invoke-interface {v0, v4, v5, v6}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 196
    :try_start_1
    invoke-interface/range {p2 .. p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getCloudServiceControl()Lcom/samsung/android/scloud/sync/server/ICloudServiceControl;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-interface {v4, v0}, Lcom/samsung/android/scloud/sync/server/ICloudServiceControl;->deleteItem(Lcom/samsung/android/scloud/sync/data/SyncItem;)Z

    move-result v4

    if-eqz v4, :cond_14

    .line 197
    invoke-interface/range {p2 .. p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getOEMControl()Lcom/samsung/android/scloud/sync/oem/IOEMControl;

    move-result-object v4

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getLocalId()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-interface {v4, v0, v1, v5}, Lcom/samsung/android/scloud/sync/oem/IOEMControl;->deleteLocal(Landroid/content/Context;Lcom/samsung/android/scloud/sync/model/IModel;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_13

    .line 198
    move-object/from16 v0, p7

    iget-object v4, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v5, v4, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v8, 0x1

    add-long/2addr v5, v8

    iput-wide v5, v4, Landroid/content/SyncStats;->numAuthExceptions:J
    :try_end_1
    .catch Lcom/samsung/android/scloud/framework/PDMException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 200
    :catch_1
    move-exception v14

    .line 201
    .restart local v14    # "e":Lcom/samsung/android/scloud/framework/PDMException;
    const-string v4, "SyncTask-"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Exception in deleting - ctid : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p4

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", item : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v14}, Lcom/samsung/android/scloud/framework/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 202
    const/16 v4, 0x132

    invoke-virtual {v14}, Lcom/samsung/android/scloud/framework/PDMException;->getExceptionCode()I

    move-result v5

    if-ne v4, v5, :cond_15

    .line 203
    throw v14

    .line 199
    .end local v14    # "e":Lcom/samsung/android/scloud/framework/PDMException;
    :cond_14
    :try_start_2
    move-object/from16 v0, p7

    iget-object v4, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v5, v4, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v8, 0x1

    add-long/2addr v5, v8

    iput-wide v5, v4, Landroid/content/SyncStats;->numAuthExceptions:J
    :try_end_2
    .catch Lcom/samsung/android/scloud/framework/PDMException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_2

    .line 205
    .restart local v14    # "e":Lcom/samsung/android/scloud/framework/PDMException;
    :cond_15
    const/16 v4, 0x141

    invoke-virtual {v14}, Lcom/samsung/android/scloud/framework/PDMException;->getExceptionCode()I

    move-result v5

    if-ne v4, v5, :cond_16

    .line 206
    const-wide v21, 0xe8d4a51000L

    .line 207
    move-object/from16 v0, p7

    iget-object v4, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v5, v4, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v8, 0x1

    add-long/2addr v5, v8

    iput-wide v5, v4, Landroid/content/SyncStats;->numIoExceptions:J

    goto/16 :goto_2

    .line 209
    :cond_16
    move-object/from16 v0, p7

    iget-object v4, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v5, v4, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v8, 0x1

    add-long/2addr v5, v8

    iput-wide v5, v4, Landroid/content/SyncStats;->numAuthExceptions:J

    goto/16 :goto_2

    .line 213
    .end local v14    # "e":Lcom/samsung/android/scloud/framework/PDMException;
    .end local v16    # "item":Lcom/samsung/android/scloud/sync/data/SyncItem;
    :cond_17
    const/4 v4, 0x0

    const-string v5, "SyncTask-"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Delete to server finished !! cnt : "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface/range {v28 .. v28}, Ljava/util/List;->size()I

    move-result v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p8

    invoke-interface {v0, v4, v5, v6}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 216
    const/4 v4, 0x4

    const-string v5, "SyncTask-"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "["

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface/range {p2 .. p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "]("

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p4

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ") : Upsync start !!"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p8

    invoke-interface {v0, v4, v5, v6}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 217
    invoke-interface/range {v30 .. v30}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_3
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1b

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/samsung/android/scloud/sync/data/SyncItem;

    .line 218
    .restart local v16    # "item":Lcom/samsung/android/scloud/sync/data/SyncItem;
    const/4 v4, 0x4

    const-string v5, "SyncTask-"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "["

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface/range {p2 .. p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "]("

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p4

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ") : Upsync - item : "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p8

    invoke-interface {v0, v4, v5, v6}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 220
    :try_start_3
    invoke-interface/range {p2 .. p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getCloudServiceControl()Lcom/samsung/android/scloud/sync/server/ICloudServiceControl;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-interface {v4, v0}, Lcom/samsung/android/scloud/sync/server/ICloudServiceControl;->uploadItem(Lcom/samsung/android/scloud/sync/data/SyncItem;)Z

    .line 221
    invoke-interface/range {p2 .. p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getOEMControl()Lcom/samsung/android/scloud/sync/oem/IOEMControl;

    move-result-object v4

    const/16 v5, 0x12d

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, v16

    invoke-interface {v4, v0, v1, v2, v5}, Lcom/samsung/android/scloud/sync/oem/IOEMControl;->complete(Landroid/content/Context;Lcom/samsung/android/scloud/sync/model/IModel;Lcom/samsung/android/scloud/sync/data/SyncItem;I)Z

    move-result v4

    if-nez v4, :cond_18

    .line 222
    move-object/from16 v0, p7

    iget-object v4, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v5, v4, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v8, 0x1

    add-long/2addr v5, v8

    iput-wide v5, v4, Landroid/content/SyncStats;->numAuthExceptions:J

    .line 223
    :cond_18
    const/4 v4, 0x4

    const-string v5, "SyncTask-"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "upsync complete !! "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ", "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v21

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p8

    invoke-interface {v0, v4, v5, v6}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Lcom/samsung/android/scloud/framework/PDMException; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_3

    .line 224
    :catch_2
    move-exception v14

    .line 225
    .restart local v14    # "e":Lcom/samsung/android/scloud/framework/PDMException;
    const-string v4, "SyncTask-"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Exception in uploading - ctid : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p4

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", item : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v14}, Lcom/samsung/android/scloud/framework/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 226
    invoke-interface/range {p2 .. p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getOEMControl()Lcom/samsung/android/scloud/sync/oem/IOEMControl;

    move-result-object v4

    invoke-virtual {v14}, Lcom/samsung/android/scloud/framework/PDMException;->getExceptionCode()I

    move-result v5

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, v16

    invoke-interface {v4, v0, v1, v2, v5}, Lcom/samsung/android/scloud/sync/oem/IOEMControl;->complete(Landroid/content/Context;Lcom/samsung/android/scloud/sync/model/IModel;Lcom/samsung/android/scloud/sync/data/SyncItem;I)Z

    .line 227
    const/16 v4, 0x132

    invoke-virtual {v14}, Lcom/samsung/android/scloud/framework/PDMException;->getExceptionCode()I

    move-result v5

    if-ne v4, v5, :cond_19

    .line 228
    throw v14

    .line 230
    :cond_19
    const/16 v4, 0x141

    invoke-virtual {v14}, Lcom/samsung/android/scloud/framework/PDMException;->getExceptionCode()I

    move-result v5

    if-ne v4, v5, :cond_1a

    .line 231
    const-wide v21, 0xe8d4a51000L

    .line 232
    move-object/from16 v0, p7

    iget-object v4, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v5, v4, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v8, 0x1

    add-long/2addr v5, v8

    iput-wide v5, v4, Landroid/content/SyncStats;->numIoExceptions:J

    goto/16 :goto_3

    .line 234
    :cond_1a
    move-object/from16 v0, p7

    iget-object v4, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v5, v4, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v8, 0x1

    add-long/2addr v5, v8

    iput-wide v5, v4, Landroid/content/SyncStats;->numAuthExceptions:J

    goto/16 :goto_3

    .line 237
    .end local v14    # "e":Lcom/samsung/android/scloud/framework/PDMException;
    .end local v16    # "item":Lcom/samsung/android/scloud/sync/data/SyncItem;
    :cond_1b
    const/4 v4, 0x0

    const-string v5, "SyncTask-"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Upsync finished !! cnt : "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface/range {v30 .. v30}, Ljava/util/List;->size()I

    move-result v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p8

    invoke-interface {v0, v4, v5, v6}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 240
    const/4 v4, 0x4

    const-string v5, "SyncTask-"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "["

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface/range {p2 .. p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "]("

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p4

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ") : Downsync start !!"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p8

    invoke-interface {v0, v4, v5, v6}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 241
    invoke-interface/range {v29 .. v29}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_4
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1e

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/samsung/android/scloud/sync/data/SyncItem;

    .line 242
    .restart local v16    # "item":Lcom/samsung/android/scloud/sync/data/SyncItem;
    const/4 v4, 0x4

    const-string v5, "SyncTask-"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "["

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface/range {p2 .. p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "]("

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p4

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ") : Downsync - item : "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p8

    invoke-interface {v0, v4, v5, v6}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 244
    :try_start_4
    invoke-interface/range {p2 .. p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getCloudServiceControl()Lcom/samsung/android/scloud/sync/server/ICloudServiceControl;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-interface {v4, v0}, Lcom/samsung/android/scloud/sync/server/ICloudServiceControl;->downloadItem(Lcom/samsung/android/scloud/sync/data/SyncItem;)Z

    move-result v4

    if-eqz v4, :cond_1c

    .line 245
    move-object/from16 v0, p7

    iget-object v4, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v5, v4, Landroid/content/SyncStats;->numInserts:J

    const-wide/16 v8, 0x1

    add-long/2addr v5, v8

    iput-wide v5, v4, Landroid/content/SyncStats;->numInserts:J
    :try_end_4
    .catch Lcom/samsung/android/scloud/framework/PDMException; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_4

    .line 248
    :catch_3
    move-exception v14

    .line 249
    .restart local v14    # "e":Lcom/samsung/android/scloud/framework/PDMException;
    const-string v4, "SyncTask-"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Exception in downloading - ctid : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p4

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v14}, Lcom/samsung/android/scloud/framework/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 250
    const/16 v4, 0x132

    invoke-virtual {v14}, Lcom/samsung/android/scloud/framework/PDMException;->getExceptionCode()I

    move-result v5

    if-ne v4, v5, :cond_1d

    .line 251
    throw v14

    .line 247
    .end local v14    # "e":Lcom/samsung/android/scloud/framework/PDMException;
    :cond_1c
    :try_start_5
    move-object/from16 v0, p7

    iget-object v4, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v5, v4, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v8, 0x1

    add-long/2addr v5, v8

    iput-wide v5, v4, Landroid/content/SyncStats;->numAuthExceptions:J
    :try_end_5
    .catch Lcom/samsung/android/scloud/framework/PDMException; {:try_start_5 .. :try_end_5} :catch_3

    goto/16 :goto_4

    .line 252
    .restart local v14    # "e":Lcom/samsung/android/scloud/framework/PDMException;
    :cond_1d
    move-object/from16 v0, p7

    iget-object v4, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v5, v4, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v8, 0x1

    add-long/2addr v5, v8

    iput-wide v5, v4, Landroid/content/SyncStats;->numAuthExceptions:J

    goto/16 :goto_4

    .line 256
    .end local v14    # "e":Lcom/samsung/android/scloud/framework/PDMException;
    .end local v16    # "item":Lcom/samsung/android/scloud/sync/data/SyncItem;
    :cond_1e
    const/4 v4, 0x0

    const-string v5, "SyncTask-"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Downsync finished !! cnt : "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface/range {v29 .. v29}, Ljava/util/List;->size()I

    move-result v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p8

    invoke-interface {v0, v4, v5, v6}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 259
    const/4 v4, 0x4

    const-string v5, "SyncTask-"

    const-string v6, "delete to local start !!"

    move-object/from16 v0, p8

    invoke-interface {v0, v4, v5, v6}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 260
    invoke-interface/range {v27 .. v27}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_5
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1f

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/samsung/android/scloud/sync/data/SyncItem;

    .line 261
    .restart local v16    # "item":Lcom/samsung/android/scloud/sync/data/SyncItem;
    invoke-interface/range {p2 .. p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getOEMControl()Lcom/samsung/android/scloud/sync/oem/IOEMControl;

    move-result-object v4

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getLocalId()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-interface {v4, v0, v1, v5}, Lcom/samsung/android/scloud/sync/oem/IOEMControl;->deleteLocal(Landroid/content/Context;Lcom/samsung/android/scloud/sync/model/IModel;Ljava/lang/String;)Z

    goto :goto_5

    .line 263
    .end local v16    # "item":Lcom/samsung/android/scloud/sync/data/SyncItem;
    :cond_1f
    const/4 v4, 0x0

    const-string v5, "SyncTask-"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "delete to local finished !! cnt : "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface/range {v27 .. v27}, Ljava/util/List;->size()I

    move-result v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p8

    invoke-interface {v0, v4, v5, v6}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 267
    const/16 v12, 0x12d

    .line 268
    .local v12, "resultCode":I
    invoke-virtual/range {p7 .. p7}, Landroid/content/SyncResult;->hasError()Z

    move-result v4

    if-nez v4, :cond_20

    move-object/from16 v0, p7

    iget-object v4, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v4, Landroid/content/SyncStats;->numAuthExceptions:J

    move-object/from16 v0, p7

    iget-object v6, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v8, v6, Landroid/content/SyncStats;->numIoExceptions:J

    add-long/2addr v4, v8

    move-object/from16 v0, p7

    iget-object v6, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v8, v6, Landroid/content/SyncStats;->numParseExceptions:J

    add-long/2addr v4, v8

    const-wide/16 v8, 0x0

    cmp-long v4, v4, v8

    if-lez v4, :cond_21

    .line 269
    :cond_20
    const/16 v12, 0x135

    .line 271
    :cond_21
    const/4 v4, 0x0

    const-string v5, "SyncTask-"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "sync process finished !! syncResult.stats.numAuthExceptions : "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p7

    iget-object v8, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v8, v8, Landroid/content/SyncStats;->numAuthExceptions:J

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ", syncResult.stats.numIoExceptions : "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p7

    iget-object v8, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v8, v8, Landroid/content/SyncStats;->numIoExceptions:J

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ", syncResult.stats.numParseExceptions : "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p7

    iget-object v8, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v8, v8, Landroid/content/SyncStats;->numParseExceptions:J

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p8

    invoke-interface {v0, v4, v5, v6}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 273
    const/16 v4, 0x12d

    if-ne v12, v4, :cond_22

    .line 274
    move-wide/from16 v21, v19

    .line 275
    const/4 v4, 0x0

    const-string v5, "SyncTask-"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Sync success - lastSyncTime : "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v21

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p8

    invoke-interface {v0, v4, v5, v6}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 281
    :goto_6
    invoke-interface/range {p2 .. p2}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v10

    const/16 v11, 0x65

    const/4 v13, 0x0

    move-object/from16 v8, p8

    move-object/from16 v9, p0

    invoke-interface/range {v8 .. v13}, Lcom/samsung/android/scloud/framework/IStatusListener;->onFinished(Landroid/content/Context;Ljava/lang/String;IILjava/lang/Object;)V

    .line 282
    return-wide v21

    .line 279
    :cond_22
    const/4 v4, 0x0

    const-string v5, "SyncTask-"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Sync failed - lastSyncTime : "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v21

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p8

    invoke-interface {v0, v4, v5, v6}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_6
.end method
