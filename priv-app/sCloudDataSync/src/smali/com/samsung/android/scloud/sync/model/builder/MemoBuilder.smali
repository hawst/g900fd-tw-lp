.class public Lcom/samsung/android/scloud/sync/model/builder/MemoBuilder;
.super Lcom/samsung/android/scloud/sync/model/builder/IBuilder;
.source "MemoBuilder.java"


# static fields
.field public static final AUTHORITY:Ljava/lang/String; = "com.samsung.android.memo"

.field public static final CATEGORY_CONTENT_URI:Landroid/net/Uri;

.field private static final FILE_COLUMNS:[Ljava/lang/String;

.field public static final FILE_CONTENT_URI:Landroid/net/Uri;

.field public static final FILE_CONTENT_URI_OPEN_FILE:Landroid/net/Uri;

.field private static final KEY_FILE:Ljava/lang/String; = "FILE"

.field private static final KEY_MEMO:Ljava/lang/String; = "MEMO"

.field private static final MEMO_COLUMNS:[Ljava/lang/String;

.field public static final MEMO_CONTENT_URI:Landroid/net/Uri;

.field public static final MEMO_SYNC_STATE:Landroid/net/Uri;

.field private static final TAG:Ljava/lang/String; = "MemoBuilder"


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 38
    const-string v0, "content://com.samsung.android.memo/memo"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/scloud/sync/model/builder/MemoBuilder;->MEMO_CONTENT_URI:Landroid/net/Uri;

    .line 40
    const-string v0, "content://com.samsung.android.memo/file"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/scloud/sync/model/builder/MemoBuilder;->FILE_CONTENT_URI:Landroid/net/Uri;

    .line 42
    const-string v0, "content://com.samsung.android.memo/file"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/scloud/sync/model/builder/MemoBuilder;->FILE_CONTENT_URI_OPEN_FILE:Landroid/net/Uri;

    .line 43
    const-string v0, "content://com.samsung.android.memo/category"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/scloud/sync/model/builder/MemoBuilder;->CATEGORY_CONTENT_URI:Landroid/net/Uri;

    .line 44
    const-string v0, "content://com.samsung.android.memo/_sync_state"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/scloud/sync/model/builder/MemoBuilder;->MEMO_SYNC_STATE:Landroid/net/Uri;

    .line 307
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "UUID"

    aput-object v1, v0, v3

    const-string v1, "createdAt"

    aput-object v1, v0, v4

    const-string v1, "categoryUUID"

    aput-object v1, v0, v5

    const-string v1, "title"

    aput-object v1, v0, v6

    const-string v1, "content"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "vrfileuuid"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "strippedContent"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "_data"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "lastModifiedAt"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "accountName"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "accountType"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "sync2"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "sync1"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/scloud/sync/model/builder/MemoBuilder;->MEMO_COLUMNS:[Ljava/lang/String;

    .line 323
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "UUID"

    aput-object v1, v0, v3

    const-string v1, "memoUUID"

    aput-object v1, v0, v4

    const-string v1, "mime_type"

    aput-object v1, v0, v5

    const-string v1, "_display_name"

    aput-object v1, v0, v6

    const-string v1, "_size"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "orientation"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "_data"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "sync1"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/scloud/sync/model/builder/MemoBuilder;->FILE_COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/scloud/sync/model/IInternalModel;)V
    .locals 0
    .param p1, "mModel"    # Lcom/samsung/android/scloud/sync/model/IInternalModel;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/samsung/android/scloud/sync/model/builder/IBuilder;-><init>(Lcom/samsung/android/scloud/sync/model/IInternalModel;)V

    .line 52
    return-void
.end method


# virtual methods
.method public complete(Landroid/content/Context;Lcom/samsung/android/scloud/sync/data/SyncItem;I)Z
    .locals 17
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "item"    # Lcom/samsung/android/scloud/sync/data/SyncItem;
    .param p3, "rcode"    # I

    .prologue
    .line 269
    const/4 v15, 0x1

    .line 271
    .local v15, "result":Z
    const/4 v10, 0x0

    .line 272
    .local v10, "cursor":Landroid/database/Cursor;
    :try_start_0
    new-instance v11, Landroid/content/ContentValues;

    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    .line 274
    .local v11, "cv":Landroid/content/ContentValues;
    const/16 v1, 0x12d

    move/from16 v0, p3

    if-ne v0, v1, :cond_1

    .line 275
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getTimeStamp()J

    move-result-wide v13

    .line 276
    .local v13, "origtimestmp":J
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/scloud/sync/model/builder/MemoBuilder;->MEMO_CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/scloud/sync/model/builder/MemoBuilder;->myModel:Lcom/samsung/android/scloud/sync/model/IInternalModel;

    invoke-interface {v5}, Lcom/samsung/android/scloud/sync/model/IInternalModel;->getTimestampFieldName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "UUID=\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getLocalId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 279
    if-eqz v10, :cond_1

    .line 280
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 281
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/scloud/sync/model/builder/MemoBuilder;->myModel:Lcom/samsung/android/scloud/sync/model/IInternalModel;

    invoke-interface {v1}, Lcom/samsung/android/scloud/sync/model/IInternalModel;->getTimestampFieldName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    .line 282
    .local v16, "timestampColIdx":I
    move/from16 v0, v16

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 284
    .local v8, "currenttimestmp":J
    cmp-long v1, v13, v8

    if-nez v1, :cond_0

    .line 285
    const-string v1, "isDirty"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v11, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 286
    const-string v1, "isDeleted"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v11, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 289
    .end local v8    # "currenttimestmp":J
    .end local v16    # "timestampColIdx":I
    :cond_0
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 293
    .end local v13    # "origtimestmp":J
    :cond_1
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/scloud/sync/data/SyncItem;->isNew()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 294
    const-string v1, "sync2"

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getSyncKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v11, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    :cond_2
    invoke-virtual {v11}, Landroid/content/ContentValues;->size()I

    move-result v1

    if-lez v1, :cond_3

    .line 297
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/scloud/sync/model/builder/MemoBuilder;->MEMO_CONTENT_URI:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "UUID=\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getLocalId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v11, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v7

    .line 298
    .local v7, "cnt":I
    const-string v1, "MemoBuilder"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "completed : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 304
    .end local v7    # "cnt":I
    :cond_3
    return v15

    .line 300
    .end local v11    # "cv":Landroid/content/ContentValues;
    :catch_0
    move-exception v12

    .line 301
    .local v12, "e":Ljava/lang/Exception;
    const-string v1, "MemoBuilder"

    const-string v2, "complete err"

    invoke-static {v1, v2, v12}, Lcom/samsung/android/scloud/framework/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 302
    new-instance v1, Lcom/samsung/android/scloud/framework/PDMException;

    const/16 v2, 0x139

    invoke-direct {v1, v2, v12}, Lcom/samsung/android/scloud/framework/PDMException;-><init>(ILjava/lang/Throwable;)V

    throw v1
.end method

.method public deleteLocal(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "localId"    # Ljava/lang/String;

    .prologue
    .line 252
    const/4 v4, 0x1

    .line 254
    .local v4, "result":Z
    :try_start_0
    sget-object v5, Lcom/samsung/android/scloud/sync/model/builder/MemoBuilder;->FILE_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v5

    const-string v6, "caller_is_syncadapter"

    const-string v7, "true"

    invoke-virtual {v5, v6, v7}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 256
    .local v3, "fileUri":Landroid/net/Uri;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "memoUUID=\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v5, v3, v6, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 257
    .local v2, "fileCnt":I
    const-string v5, "MemoBuilder"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "File table deleted : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", localId : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/scloud/sync/model/builder/MemoBuilder;->MEMO_CONTENT_URI:Landroid/net/Uri;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "UUID=\""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 259
    .local v0, "cnt":I
    const-string v5, "MemoBuilder"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Memo table deleted : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", localId : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 264
    return v4

    .line 260
    .end local v0    # "cnt":I
    .end local v2    # "fileCnt":I
    .end local v3    # "fileUri":Landroid/net/Uri;
    :catch_0
    move-exception v1

    .line 261
    .local v1, "e":Ljava/lang/Exception;
    iget-object v5, p0, Lcom/samsung/android/scloud/sync/model/builder/MemoBuilder;->myModel:Lcom/samsung/android/scloud/sync/model/IInternalModel;

    invoke-interface {v5}, Lcom/samsung/android/scloud/sync/model/IInternalModel;->getName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "deleteLocal err"

    invoke-static {v5, v6, v1}, Lcom/samsung/android/scloud/framework/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 262
    new-instance v5, Lcom/samsung/android/scloud/framework/PDMException;

    const/16 v6, 0x139

    invoke-direct {v5, v6, v1}, Lcom/samsung/android/scloud/framework/PDMException;-><init>(ILjava/lang/Throwable;)V

    throw v5
.end method

.method public getAttachmentFileInfo(Landroid/content/Context;Ljava/lang/String;)Lcom/samsung/android/scloud/sync/data/Attachments;
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "localId"    # Ljava/lang/String;

    .prologue
    .line 56
    new-instance v8, Lcom/samsung/android/scloud/sync/data/Attachments;

    invoke-direct {v8}, Lcom/samsung/android/scloud/sync/data/Attachments;-><init>()V

    .line 57
    .local v8, "result":Lcom/samsung/android/scloud/sync/data/Attachments;
    const/4 v6, 0x0

    .line 59
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/scloud/sync/model/builder/MemoBuilder;->FILE_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "memoUUID=\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\" and "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "isDeleted"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=0"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 60
    if-eqz v6, :cond_1

    .line 61
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 62
    const-string v0, "UUID"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 63
    .local v9, "uuid":Ljava/lang/String;
    const-string v0, "MemoBuilder"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Att file in File table : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    const-string v0, "sync1"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {v8, v9, v0, v1}, Lcom/samsung/android/scloud/sync/data/Attachments;->addFile(Ljava/lang/String;J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 67
    .end local v9    # "uuid":Ljava/lang/String;
    :catch_0
    move-exception v7

    .line 68
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/model/builder/MemoBuilder;->myModel:Lcom/samsung/android/scloud/sync/model/IInternalModel;

    invoke-interface {v0}, Lcom/samsung/android/scloud/sync/model/IInternalModel;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "getAttachmentFileInfo err"

    invoke-static {v0, v1, v7}, Lcom/samsung/android/scloud/framework/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 69
    new-instance v0, Lcom/samsung/android/scloud/framework/PDMException;

    const/16 v1, 0x13a

    invoke-direct {v0, v1, v7}, Lcom/samsung/android/scloud/framework/PDMException;-><init>(ILjava/lang/Throwable;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 71
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v0

    :cond_1
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 73
    :cond_2
    return-object v8
.end method

.method public getLocalChange(Landroid/content/Context;Ljava/lang/String;Lcom/samsung/android/scloud/sync/data/Attachments;Ljava/lang/String;)Z
    .locals 22
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "localId"    # Ljava/lang/String;
    .param p3, "attachments"    # Lcom/samsung/android/scloud/sync/data/Attachments;
    .param p4, "filePath"    # Ljava/lang/String;

    .prologue
    .line 78
    const/16 v21, 0x1

    .line 79
    .local v21, "result":Z
    const/4 v8, 0x0

    .line 80
    .local v8, "cursor":Landroid/database/Cursor;
    const/4 v11, 0x0

    .line 82
    .local v11, "fileCursor":Landroid/database/Cursor;
    :try_start_0
    new-instance v19, Lorg/json/JSONObject;

    invoke-direct/range {v19 .. v19}, Lorg/json/JSONObject;-><init>()V

    .line 83
    .local v19, "json":Lorg/json/JSONObject;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/scloud/sync/model/builder/MemoBuilder;->MEMO_CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "UUID=\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 84
    if-eqz v8, :cond_4

    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 85
    sget-object v2, Lcom/samsung/android/scloud/sync/model/builder/MemoBuilder;->MEMO_COLUMNS:[Ljava/lang/String;

    invoke-static {v8, v2}, Lcom/samsung/android/scloud/framework/util/JSONParser;->toJSON(Landroid/database/Cursor;[Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v20

    .line 87
    .local v20, "memo":Lorg/json/JSONObject;
    const-string v2, "MemoBuilder"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "memo JSON : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    const-string v2, "MEMO"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 92
    new-instance v14, Lorg/json/JSONArray;

    invoke-direct {v14}, Lorg/json/JSONArray;-><init>()V

    .line 93
    .local v14, "fileJsonArr":Lorg/json/JSONArray;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/scloud/sync/model/builder/MemoBuilder;->FILE_CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "memoUUID=\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 94
    if-eqz v11, :cond_2

    .line 95
    :goto_0
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 96
    sget-object v2, Lcom/samsung/android/scloud/sync/model/builder/MemoBuilder;->FILE_COLUMNS:[Ljava/lang/String;

    invoke-static {v11, v2}, Lcom/samsung/android/scloud/framework/util/JSONParser;->toJSON(Landroid/database/Cursor;[Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v13

    .line 97
    .local v13, "fileDetailObject":Lorg/json/JSONObject;
    invoke-virtual {v14, v13}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 128
    .end local v13    # "fileDetailObject":Lorg/json/JSONObject;
    .end local v14    # "fileJsonArr":Lorg/json/JSONArray;
    .end local v19    # "json":Lorg/json/JSONObject;
    .end local v20    # "memo":Lorg/json/JSONObject;
    :catch_0
    move-exception v9

    .line 129
    .local v9, "e":Ljava/lang/Exception;
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/sync/model/builder/MemoBuilder;->myModel:Lcom/samsung/android/scloud/sync/model/IInternalModel;

    invoke-interface {v2}, Lcom/samsung/android/scloud/sync/model/IInternalModel;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "getLocalChange err"

    invoke-static {v2, v3, v9}, Lcom/samsung/android/scloud/framework/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 130
    new-instance v2, Lcom/samsung/android/scloud/framework/PDMException;

    const/16 v3, 0x13a

    invoke-direct {v2, v3, v9}, Lcom/samsung/android/scloud/framework/PDMException;-><init>(ILjava/lang/Throwable;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 132
    .end local v9    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    if-eqz v8, :cond_0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 133
    :cond_0
    if-eqz v11, :cond_1

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v2

    .line 100
    .restart local v14    # "fileJsonArr":Lorg/json/JSONArray;
    .restart local v19    # "json":Lorg/json/JSONObject;
    .restart local v20    # "memo":Lorg/json/JSONObject;
    :cond_2
    :try_start_2
    const-string v2, "MemoBuilder"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "attfile JSON : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    const-string v2, "FILE"

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v14}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 103
    invoke-virtual/range {p3 .. p3}, Lcom/samsung/android/scloud/sync/data/Attachments;->howManyFiles()I

    move-result v10

    .line 104
    .local v10, "fileCount":I
    const/16 v18, 0x0

    .local v18, "i":I
    :goto_1
    move/from16 v0, v18

    if-ge v0, v10, :cond_3

    .line 105
    move-object/from16 v0, p3

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/sync/data/Attachments;->getFileNameAt(I)Ljava/lang/String;

    move-result-object v15

    .line 106
    .local v15, "fileName":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/samsung/android/scloud/sync/model/builder/MemoBuilder;->FILE_CONTENT_URI_OPEN_FILE:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const-string v4, "r"

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v12

    .line 107
    .local v12, "fileDesc":Landroid/os/ParcelFileDescriptor;
    new-instance v16, Ljava/io/FileInputStream;

    invoke-virtual {v12}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-direct {v0, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V

    .line 109
    .local v16, "fis":Ljava/io/FileInputStream;
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p4

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/samsung/android/scloud/sync/model/builder/MemoBuilder$1;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/samsung/android/scloud/sync/model/builder/MemoBuilder$1;-><init>(Lcom/samsung/android/scloud/sync/model/builder/MemoBuilder;)V

    move-object/from16 v0, v16

    invoke-static {v0, v2, v3, v4, v5}, Lcom/samsung/android/scloud/framework/util/FileTool;->writeToFile(Ljava/io/InputStream;JLjava/lang/String;Lcom/samsung/android/scloud/framework/network/NetworkUtil$PDMProgressListener;)V

    .line 117
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileInputStream;->close()V

    .line 118
    invoke-virtual {v12}, Landroid/os/ParcelFileDescriptor;->close()V

    .line 119
    const-string v2, "MemoBuilder"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "copy att file to files dir : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p4

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    add-int/lit8 v18, v18, 0x1

    goto/16 :goto_1

    .line 122
    .end local v12    # "fileDesc":Landroid/os/ParcelFileDescriptor;
    .end local v15    # "fileName":Ljava/lang/String;
    .end local v16    # "fis":Ljava/io/FileInputStream;
    :cond_3
    new-instance v17, Ljava/io/FileWriter;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "content.sync"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    move-object/from16 v0, v17

    invoke-direct {v0, v2, v3}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;Z)V

    .line 123
    .local v17, "fw":Ljava/io/FileWriter;
    invoke-virtual/range {v19 .. v19}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 124
    invoke-virtual/range {v17 .. v17}, Ljava/io/FileWriter;->close()V

    .line 125
    const-string v2, "MemoBuilder"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "write content JSON : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p4

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "content.sync"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 132
    .end local v10    # "fileCount":I
    .end local v14    # "fileJsonArr":Lorg/json/JSONArray;
    .end local v17    # "fw":Ljava/io/FileWriter;
    .end local v18    # "i":I
    .end local v20    # "memo":Lorg/json/JSONObject;
    :cond_4
    if-eqz v8, :cond_5

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 133
    :cond_5
    if-eqz v11, :cond_6

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 135
    :cond_6
    return v21
.end method

.method public updateLocal(Landroid/content/Context;Lcom/samsung/android/scloud/sync/data/SyncItem;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;
    .locals 30
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "item"    # Lcom/samsung/android/scloud/sync/data/SyncItem;
    .param p5, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/samsung/android/scloud/sync/data/SyncItem;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 141
    .local p3, "toDownloadAttFileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p4, "toDeleteAttFileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v8, 0x0

    .line 142
    .local v8, "br":Ljava/io/BufferedReader;
    const/16 v21, 0x0

    .line 143
    .local v21, "fos":Ljava/io/FileOutputStream;
    const/4 v15, 0x0

    .line 144
    .local v15, "fileDesc":Landroid/os/ParcelFileDescriptor;
    const/4 v11, 0x0

    .line 145
    .local v11, "cs1":Landroid/database/Cursor;
    const/4 v12, 0x0

    .line 147
    .local v12, "cs2":Landroid/database/Cursor;
    :try_start_0
    new-instance v9, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/FileReader;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p5

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "content.sync"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v9, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 148
    .end local v8    # "br":Ljava/io/BufferedReader;
    .local v9, "br":Ljava/io/BufferedReader;
    :try_start_1
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    .line 149
    .local v26, "sb":Ljava/lang/StringBuilder;
    const/16 v27, 0x0

    .line 150
    .local v27, "str":Ljava/lang/String;
    :goto_0
    invoke-virtual {v9}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v27

    if-eqz v27, :cond_5

    .line 151
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 237
    .end local v26    # "sb":Ljava/lang/StringBuilder;
    .end local v27    # "str":Ljava/lang/String;
    :catch_0
    move-exception v14

    move-object v8, v9

    .line 238
    .end local v9    # "br":Ljava/io/BufferedReader;
    .restart local v8    # "br":Ljava/io/BufferedReader;
    .local v14, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/sync/model/builder/MemoBuilder;->myModel:Lcom/samsung/android/scloud/sync/model/IInternalModel;

    invoke-interface {v2}, Lcom/samsung/android/scloud/sync/model/IInternalModel;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v4, "updateLocal err"

    invoke-static {v2, v4, v14}, Lcom/samsung/android/scloud/framework/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 239
    new-instance v2, Lcom/samsung/android/scloud/framework/PDMException;

    const/16 v4, 0x139

    invoke-direct {v2, v4, v14}, Lcom/samsung/android/scloud/framework/PDMException;-><init>(ILjava/lang/Throwable;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 241
    .end local v14    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    :goto_2
    if-eqz v8, :cond_0

    :try_start_3
    invoke-virtual {v8}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    .line 242
    :cond_0
    :goto_3
    if-eqz v21, :cond_1

    :try_start_4
    invoke-virtual/range {v21 .. v21}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_5

    .line 243
    :cond_1
    :goto_4
    if-eqz v15, :cond_2

    :try_start_5
    invoke-virtual {v15}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_6

    .line 244
    :cond_2
    :goto_5
    if-eqz v11, :cond_3

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 245
    :cond_3
    if-eqz v12, :cond_4

    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v2

    .line 153
    .end local v8    # "br":Ljava/io/BufferedReader;
    .restart local v9    # "br":Ljava/io/BufferedReader;
    .restart local v26    # "sb":Ljava/lang/StringBuilder;
    .restart local v27    # "str":Ljava/lang/String;
    :cond_5
    :try_start_6
    new-instance v25, Lorg/json/JSONObject;

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v25

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 155
    .local v25, "memo":Lorg/json/JSONObject;
    const-string v2, "MemoBuilder"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "server JSON : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v25

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    if-eqz p3, :cond_9

    invoke-interface/range {p3 .. p3}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_9

    .line 159
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-result-object v24

    .local v24, "i$":Ljava/util/Iterator;
    move-object/from16 v22, v21

    .end local v21    # "fos":Ljava/io/FileOutputStream;
    .local v22, "fos":Ljava/io/FileOutputStream;
    :goto_6
    :try_start_7
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    .line 160
    .local v18, "fileName":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/samsung/android/scloud/sync/model/builder/MemoBuilder;->FILE_CONTENT_URI_OPEN_FILE:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    const-string v5, "w"

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v15

    .line 161
    new-instance v21, Ljava/io/FileOutputStream;

    invoke-virtual {v15}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v2

    move-object/from16 v0, v21

    invoke-direct {v0, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_8
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 163
    .end local v22    # "fos":Ljava/io/FileOutputStream;
    .restart local v21    # "fos":Ljava/io/FileOutputStream;
    :try_start_8
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p5

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-wide/16 v4, 0x0

    new-instance v6, Lcom/samsung/android/scloud/sync/model/builder/MemoBuilder$2;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Lcom/samsung/android/scloud/sync/model/builder/MemoBuilder$2;-><init>(Lcom/samsung/android/scloud/sync/model/builder/MemoBuilder;)V

    move-object/from16 v0, v21

    invoke-static {v2, v4, v5, v0, v6}, Lcom/samsung/android/scloud/framework/util/FileTool;->writeToFile(Ljava/lang/String;JLjava/io/FileOutputStream;Lcom/samsung/android/scloud/framework/network/NetworkUtil$PDMProgressListener;)V

    .line 171
    if-eqz v21, :cond_6

    .line 172
    invoke-virtual/range {v21 .. v21}, Ljava/io/FileOutputStream;->close()V

    .line 173
    const/16 v21, 0x0

    .line 175
    :cond_6
    if-eqz v15, :cond_7

    .line 176
    invoke-virtual {v15}, Landroid/os/ParcelFileDescriptor;->close()V

    .line 177
    const/4 v15, 0x0

    :cond_7
    move-object/from16 v22, v21

    .line 180
    .end local v21    # "fos":Ljava/io/FileOutputStream;
    .restart local v22    # "fos":Ljava/io/FileOutputStream;
    goto :goto_6

    .end local v18    # "fileName":Ljava/lang/String;
    :cond_8
    move-object/from16 v21, v22

    .line 183
    .end local v22    # "fos":Ljava/io/FileOutputStream;
    .end local v24    # "i$":Ljava/util/Iterator;
    .restart local v21    # "fos":Ljava/io/FileOutputStream;
    :cond_9
    const-string v2, "FILE"

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v16

    .line 184
    .local v16, "fileDetailList":Lorg/json/JSONArray;
    const/16 v23, 0x0

    .local v23, "i":I
    :goto_7
    invoke-virtual/range {v16 .. v16}, Lorg/json/JSONArray;->length()I

    move-result v2

    move/from16 v0, v23

    if-ge v0, v2, :cond_c

    .line 185
    move-object/from16 v0, v16

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v17

    .line 186
    .local v17, "fileDetailObj":Lorg/json/JSONObject;
    const-string v2, "_data"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 188
    .local v19, "filePathVal":Ljava/lang/String;
    sget-object v2, Lcom/samsung/android/scloud/sync/model/builder/MemoBuilder;->FILE_COLUMNS:[Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-static {v0, v2}, Lcom/samsung/android/scloud/framework/util/JSONParser;->fromJSON(Lorg/json/JSONObject;[Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v13

    .line 190
    .local v13, "cv":Landroid/content/ContentValues;
    const-string v2, "isDirty"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v13, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 192
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/scloud/sync/model/builder/MemoBuilder;->FILE_CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/samsung/android/scloud/sync/model/builder/MemoBuilder;->FILE_COLUMNS:[Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "UUID=\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "UUID"

    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 195
    if-eqz v11, :cond_b

    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 196
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v4, Lcom/samsung/android/scloud/sync/model/builder/MemoBuilder;->FILE_CONTENT_URI:Landroid/net/Uri;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "UUID=\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "UUID"

    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v2, v4, v13, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 202
    :goto_8
    if-eqz v11, :cond_a

    .line 203
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 204
    const/4 v11, 0x0

    .line 184
    :cond_a
    add-int/lit8 v23, v23, 0x1

    goto/16 :goto_7

    .line 198
    :cond_b
    const-string v2, "_srcUri"

    move-object/from16 v0, v19

    invoke-virtual {v13, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v4, Lcom/samsung/android/scloud/sync/model/builder/MemoBuilder;->FILE_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v4, v13}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto :goto_8

    .line 241
    .end local v13    # "cv":Landroid/content/ContentValues;
    .end local v16    # "fileDetailList":Lorg/json/JSONArray;
    .end local v17    # "fileDetailObj":Lorg/json/JSONObject;
    .end local v19    # "filePathVal":Ljava/lang/String;
    .end local v23    # "i":I
    .end local v25    # "memo":Lorg/json/JSONObject;
    .end local v26    # "sb":Ljava/lang/StringBuilder;
    .end local v27    # "str":Ljava/lang/String;
    :catchall_1
    move-exception v2

    move-object v8, v9

    .end local v9    # "br":Ljava/io/BufferedReader;
    .restart local v8    # "br":Ljava/io/BufferedReader;
    goto/16 :goto_2

    .line 208
    .end local v8    # "br":Ljava/io/BufferedReader;
    .restart local v9    # "br":Ljava/io/BufferedReader;
    .restart local v16    # "fileDetailList":Lorg/json/JSONArray;
    .restart local v23    # "i":I
    .restart local v25    # "memo":Lorg/json/JSONObject;
    .restart local v26    # "sb":Ljava/lang/StringBuilder;
    .restart local v27    # "str":Ljava/lang/String;
    :cond_c
    if-eqz p4, :cond_e

    invoke-interface/range {p4 .. p4}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_e

    .line 209
    const/4 v2, 0x0

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 210
    const-string v2, "UUID IN ("

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 211
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v24

    .restart local v24    # "i$":Ljava/util/Iterator;
    :goto_9
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_d

    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/lang/String;

    .line 212
    .local v20, "filename":Ljava/lang/String;
    const/16 v2, 0x27

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v4, 0x27

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v4, 0x2c

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_9

    .line 214
    .end local v20    # "filename":Ljava/lang/String;
    :cond_d
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 215
    const-string v2, ")"

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 216
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v4, Lcom/samsung/android/scloud/sync/model/builder/MemoBuilder;->FILE_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v2, v4, v5, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 218
    .end local v24    # "i$":Ljava/util/Iterator;
    :cond_e
    const-string v2, "MEMO"

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    sget-object v4, Lcom/samsung/android/scloud/sync/model/builder/MemoBuilder;->MEMO_COLUMNS:[Ljava/lang/String;

    invoke-static {v2, v4}, Lcom/samsung/android/scloud/framework/util/JSONParser;->fromJSON(Lorg/json/JSONObject;[Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v13

    .line 219
    .restart local v13    # "cv":Landroid/content/ContentValues;
    const-string v2, "MemoBuilder"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "local update item : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/scloud/sync/data/SyncItem;->isNew()Z

    move-result v2

    if-eqz v2, :cond_15

    .line 221
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v4, Lcom/samsung/android/scloud/sync/model/builder/MemoBuilder;->MEMO_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v4, v13}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v3

    .line 222
    .local v3, "inserted":Landroid/net/Uri;
    const-string v2, "MemoBuilder"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "inserted : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "UUID"

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 225
    if-eqz v12, :cond_14

    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_14

    .line 226
    const-string v2, "UUID"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v29

    .line 227
    .local v29, "uuid":Ljava/lang/String;
    move-object/from16 v0, p2

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/sync/data/SyncItem;->setLocalId(Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 241
    .end local v3    # "inserted":Landroid/net/Uri;
    .end local v29    # "uuid":Ljava/lang/String;
    :goto_a
    if-eqz v9, :cond_f

    :try_start_9
    invoke-virtual {v9}, Ljava/io/BufferedReader;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1

    .line 242
    :cond_f
    :goto_b
    if-eqz v21, :cond_10

    :try_start_a
    invoke-virtual/range {v21 .. v21}, Ljava/io/FileOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_2

    .line 243
    :cond_10
    :goto_c
    if-eqz v15, :cond_11

    :try_start_b
    invoke-virtual {v15}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_3

    .line 244
    :cond_11
    :goto_d
    if-eqz v11, :cond_12

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 245
    :cond_12
    if-eqz v12, :cond_13

    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 247
    :cond_13
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getLocalId()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 229
    .restart local v3    # "inserted":Landroid/net/Uri;
    :cond_14
    :try_start_c
    new-instance v2, Lcom/samsung/android/scloud/framework/PDMException;

    const/16 v4, 0x139

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Insert failed!! "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v4, v5}, Lcom/samsung/android/scloud/framework/PDMException;-><init>(ILjava/lang/String;)V

    throw v2

    .line 231
    .end local v3    # "inserted":Landroid/net/Uri;
    :cond_15
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "content://com.samsung.android.memo/memo/"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getLocalId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "caller_is_syncadapter"

    const-string v5, "true"

    invoke-virtual {v2, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v28

    .line 233
    .local v28, "updateUri":Landroid/net/Uri;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, v28

    invoke-virtual {v2, v0, v13, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v10

    .line 234
    .local v10, "cnt":I
    const-string v2, "MemoBuilder"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "updated : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_0
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    goto/16 :goto_a

    .line 241
    .end local v10    # "cnt":I
    .end local v28    # "updateUri":Landroid/net/Uri;
    :catch_1
    move-exception v14

    .local v14, "e":Ljava/io/IOException;
    invoke-virtual {v14}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_b

    .line 242
    .end local v14    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v14

    .restart local v14    # "e":Ljava/io/IOException;
    invoke-virtual {v14}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_c

    .line 243
    .end local v14    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v14

    .restart local v14    # "e":Ljava/io/IOException;
    invoke-virtual {v14}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_d

    .line 241
    .end local v9    # "br":Ljava/io/BufferedReader;
    .end local v13    # "cv":Landroid/content/ContentValues;
    .end local v14    # "e":Ljava/io/IOException;
    .end local v16    # "fileDetailList":Lorg/json/JSONArray;
    .end local v23    # "i":I
    .end local v25    # "memo":Lorg/json/JSONObject;
    .end local v26    # "sb":Ljava/lang/StringBuilder;
    .end local v27    # "str":Ljava/lang/String;
    .restart local v8    # "br":Ljava/io/BufferedReader;
    :catch_4
    move-exception v14

    .restart local v14    # "e":Ljava/io/IOException;
    invoke-virtual {v14}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    .line 242
    .end local v14    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v14

    .restart local v14    # "e":Ljava/io/IOException;
    invoke-virtual {v14}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_4

    .line 243
    .end local v14    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v14

    .restart local v14    # "e":Ljava/io/IOException;
    invoke-virtual {v14}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_5

    .line 241
    .end local v8    # "br":Ljava/io/BufferedReader;
    .end local v14    # "e":Ljava/io/IOException;
    .end local v21    # "fos":Ljava/io/FileOutputStream;
    .restart local v9    # "br":Ljava/io/BufferedReader;
    .restart local v22    # "fos":Ljava/io/FileOutputStream;
    .restart local v24    # "i$":Ljava/util/Iterator;
    .restart local v25    # "memo":Lorg/json/JSONObject;
    .restart local v26    # "sb":Ljava/lang/StringBuilder;
    .restart local v27    # "str":Ljava/lang/String;
    :catchall_2
    move-exception v2

    move-object/from16 v21, v22

    .end local v22    # "fos":Ljava/io/FileOutputStream;
    .restart local v21    # "fos":Ljava/io/FileOutputStream;
    move-object v8, v9

    .end local v9    # "br":Ljava/io/BufferedReader;
    .restart local v8    # "br":Ljava/io/BufferedReader;
    goto/16 :goto_2

    .line 237
    .end local v24    # "i$":Ljava/util/Iterator;
    .end local v25    # "memo":Lorg/json/JSONObject;
    .end local v26    # "sb":Ljava/lang/StringBuilder;
    .end local v27    # "str":Ljava/lang/String;
    :catch_7
    move-exception v14

    goto/16 :goto_1

    .end local v8    # "br":Ljava/io/BufferedReader;
    .end local v21    # "fos":Ljava/io/FileOutputStream;
    .restart local v9    # "br":Ljava/io/BufferedReader;
    .restart local v22    # "fos":Ljava/io/FileOutputStream;
    .restart local v24    # "i$":Ljava/util/Iterator;
    .restart local v25    # "memo":Lorg/json/JSONObject;
    .restart local v26    # "sb":Ljava/lang/StringBuilder;
    .restart local v27    # "str":Ljava/lang/String;
    :catch_8
    move-exception v14

    move-object/from16 v21, v22

    .end local v22    # "fos":Ljava/io/FileOutputStream;
    .restart local v21    # "fos":Ljava/io/FileOutputStream;
    move-object v8, v9

    .end local v9    # "br":Ljava/io/BufferedReader;
    .restart local v8    # "br":Ljava/io/BufferedReader;
    goto/16 :goto_1
.end method
