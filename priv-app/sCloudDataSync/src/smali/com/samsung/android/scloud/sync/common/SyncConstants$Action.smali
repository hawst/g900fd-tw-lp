.class public interface abstract Lcom/samsung/android/scloud/sync/common/SyncConstants$Action;
.super Ljava/lang/Object;
.source "SyncConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/sync/common/SyncConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Action"
.end annotation


# static fields
.field public static final OPERATION_CANCEL:Ljava/lang/String; = "OPERATION_CANCEL"

.field public static final OPERATION_PAUSE:Ljava/lang/String; = "OPERATION_PAUSE"

.field public static final OPERATION_RESUME:Ljava/lang/String; = "OPERATION_RESUME"

.field public static final REQUEST_SYNC:Ljava/lang/String; = "REQUEST_SYNC"
