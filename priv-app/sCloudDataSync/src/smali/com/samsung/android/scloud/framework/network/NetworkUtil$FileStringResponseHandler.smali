.class public abstract Lcom/samsung/android/scloud/framework/network/NetworkUtil$FileStringResponseHandler;
.super Ljava/lang/Object;
.source "NetworkUtil.java"

# interfaces
.implements Lcom/samsung/android/scloud/framework/network/NetworkUtil$FileResponseHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/framework/network/NetworkUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "FileStringResponseHandler"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 286
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract handleFileStringResponse(Ljava/lang/String;)V
.end method

.method public handleResponse(Lorg/apache/http/Header;Ljava/lang/String;JLjava/io/InputStream;)V
    .locals 7
    .param p1, "header"    # Lorg/apache/http/Header;
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "size"    # J
    .param p5, "stream"    # Ljava/io/InputStream;

    .prologue
    .line 290
    :try_start_0
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/InputStreamReader;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v5

    invoke-direct {v4, p5, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V

    invoke-direct {v0, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 291
    .local v0, "br":Ljava/io/BufferedReader;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 292
    .local v2, "sb":Ljava/lang/StringBuilder;
    const/4 v3, 0x0

    .line 293
    .local v3, "str":Ljava/lang/String;
    :goto_0
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Lcom/samsung/android/scloud/framework/PDMException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 297
    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v2    # "sb":Ljava/lang/StringBuilder;
    .end local v3    # "str":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 298
    .local v1, "e":Lcom/samsung/android/scloud/framework/PDMException;
    const-string v4, "NetworkUtil"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "handleFileStringResponse err :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/android/scloud/framework/PDMException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v1}, Lcom/samsung/android/scloud/framework/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 299
    throw v1

    .line 295
    .end local v1    # "e":Lcom/samsung/android/scloud/framework/PDMException;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    .restart local v2    # "sb":Ljava/lang/StringBuilder;
    .restart local v3    # "str":Ljava/lang/String;
    :cond_0
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/samsung/android/scloud/framework/network/NetworkUtil$FileStringResponseHandler;->handleFileStringResponse(Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/samsung/android/scloud/framework/PDMException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 304
    return-void

    .line 300
    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v2    # "sb":Ljava/lang/StringBuilder;
    .end local v3    # "str":Ljava/lang/String;
    :catch_1
    move-exception v1

    .line 301
    .local v1, "e":Ljava/lang/Exception;
    const-string v4, "NetworkUtil"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "handleFileStringResponse err :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v1}, Lcom/samsung/android/scloud/framework/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 302
    new-instance v4, Lcom/samsung/android/scloud/framework/PDMException;

    const/16 v5, 0x138

    invoke-direct {v4, v5, v1}, Lcom/samsung/android/scloud/framework/PDMException;-><init>(ILjava/lang/Throwable;)V

    throw v4
.end method
