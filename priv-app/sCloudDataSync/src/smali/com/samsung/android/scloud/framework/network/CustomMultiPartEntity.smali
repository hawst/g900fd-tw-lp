.class public Lcom/samsung/android/scloud/framework/network/CustomMultiPartEntity;
.super Lorg/apache/http/entity/mime/MultipartEntity;
.source "CustomMultiPartEntity.java"


# static fields
.field private static final FILE_MULTIPART:Ljava/lang/String; = "file"


# instance fields
.field private final listener:Lcom/samsung/android/scloud/framework/network/NetworkUtil$PDMProgressListener;

.field private size:J


# direct methods
.method public constructor <init>(Lcom/samsung/android/scloud/framework/network/NetworkUtil$PDMProgressListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/scloud/framework/network/NetworkUtil$PDMProgressListener;

    .prologue
    .line 40
    invoke-direct {p0}, Lorg/apache/http/entity/mime/MultipartEntity;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/samsung/android/scloud/framework/network/CustomMultiPartEntity;->listener:Lcom/samsung/android/scloud/framework/network/NetworkUtil$PDMProgressListener;

    .line 42
    return-void
.end method

.method public constructor <init>(Lorg/apache/http/entity/mime/HttpMultipartMode;Lcom/samsung/android/scloud/framework/network/NetworkUtil$PDMProgressListener;)V
    .locals 0
    .param p1, "mode"    # Lorg/apache/http/entity/mime/HttpMultipartMode;
    .param p2, "listener"    # Lcom/samsung/android/scloud/framework/network/NetworkUtil$PDMProgressListener;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lorg/apache/http/entity/mime/MultipartEntity;-><init>(Lorg/apache/http/entity/mime/HttpMultipartMode;)V

    .line 47
    iput-object p2, p0, Lcom/samsung/android/scloud/framework/network/CustomMultiPartEntity;->listener:Lcom/samsung/android/scloud/framework/network/NetworkUtil$PDMProgressListener;

    .line 48
    return-void
.end method

.method public constructor <init>(Lorg/apache/http/entity/mime/HttpMultipartMode;Ljava/lang/String;Ljava/nio/charset/Charset;Lcom/samsung/android/scloud/framework/network/NetworkUtil$PDMProgressListener;)V
    .locals 0
    .param p1, "mode"    # Lorg/apache/http/entity/mime/HttpMultipartMode;
    .param p2, "boundary"    # Ljava/lang/String;
    .param p3, "charset"    # Ljava/nio/charset/Charset;
    .param p4, "listener"    # Lcom/samsung/android/scloud/framework/network/NetworkUtil$PDMProgressListener;

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/http/entity/mime/MultipartEntity;-><init>(Lorg/apache/http/entity/mime/HttpMultipartMode;Ljava/lang/String;Ljava/nio/charset/Charset;)V

    .line 53
    iput-object p4, p0, Lcom/samsung/android/scloud/framework/network/CustomMultiPartEntity;->listener:Lcom/samsung/android/scloud/framework/network/NetworkUtil$PDMProgressListener;

    .line 54
    return-void
.end method


# virtual methods
.method public addPart(Ljava/lang/String;Lorg/apache/http/entity/mime/content/ContentBody;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "contentBody"    # Lorg/apache/http/entity/mime/content/ContentBody;

    .prologue
    .line 66
    invoke-super {p0, p1, p2}, Lorg/apache/http/entity/mime/MultipartEntity;->addPart(Ljava/lang/String;Lorg/apache/http/entity/mime/content/ContentBody;)V

    .line 68
    const-string v0, "file"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    instance-of v0, p2, Lorg/apache/http/entity/mime/content/FileBody;

    if-eqz v0, :cond_0

    .line 69
    invoke-interface {p2}, Lorg/apache/http/entity/mime/content/ContentBody;->getContentLength()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/scloud/framework/network/CustomMultiPartEntity;->size:J

    .line 70
    :cond_0
    return-void
.end method

.method public writeTo(Ljava/io/OutputStream;)V
    .locals 4
    .param p1, "outstream"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59
    new-instance v0, Lcom/samsung/android/scloud/framework/network/CountingOutputStream;

    iget-wide v1, p0, Lcom/samsung/android/scloud/framework/network/CustomMultiPartEntity;->size:J

    iget-object v3, p0, Lcom/samsung/android/scloud/framework/network/CustomMultiPartEntity;->listener:Lcom/samsung/android/scloud/framework/network/NetworkUtil$PDMProgressListener;

    invoke-direct {v0, v1, v2, p1, v3}, Lcom/samsung/android/scloud/framework/network/CountingOutputStream;-><init>(JLjava/io/OutputStream;Lcom/samsung/android/scloud/framework/network/NetworkUtil$PDMProgressListener;)V

    invoke-super {p0, v0}, Lorg/apache/http/entity/mime/MultipartEntity;->writeTo(Ljava/io/OutputStream;)V

    .line 60
    return-void
.end method
