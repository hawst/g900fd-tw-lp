.class Lcom/samsung/android/scloud/sync/oem/ExternalOEMControlProvider$1;
.super Lcom/samsung/android/scloud/sync/model/CommonORSModel;
.source "ExternalOEMControlProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/sync/oem/ExternalOEMControlProvider;->call(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final contentUri:Landroid/net/Uri;

.field final contentsId:Ljava/lang/String;

.field final synthetic this$0:Lcom/samsung/android/scloud/sync/oem/ExternalOEMControlProvider;

.field final synthetic val$extras:Landroid/os/Bundle;

.field final synthetic val$modelName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/sync/oem/ExternalOEMControlProvider;Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 92
    iput-object p1, p0, Lcom/samsung/android/scloud/sync/oem/ExternalOEMControlProvider$1;->this$0:Lcom/samsung/android/scloud/sync/oem/ExternalOEMControlProvider;

    iput-object p2, p0, Lcom/samsung/android/scloud/sync/oem/ExternalOEMControlProvider$1;->val$extras:Landroid/os/Bundle;

    iput-object p3, p0, Lcom/samsung/android/scloud/sync/oem/ExternalOEMControlProvider$1;->val$modelName:Ljava/lang/String;

    invoke-direct {p0}, Lcom/samsung/android/scloud/sync/model/CommonORSModel;-><init>()V

    .line 95
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/oem/ExternalOEMControlProvider$1;->val$extras:Landroid/os/Bundle;

    const-string v1, "content_uri"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/sync/oem/ExternalOEMControlProvider$1;->contentUri:Landroid/net/Uri;

    .line 96
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/oem/ExternalOEMControlProvider$1;->val$extras:Landroid/os/Bundle;

    const-string v1, "contents_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/sync/oem/ExternalOEMControlProvider$1;->contentsId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getCid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/oem/ExternalOEMControlProvider$1;->contentsId:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/oem/ExternalOEMControlProvider$1;->val$modelName:Ljava/lang/String;

    return-object v0
.end method

.method public getOEMControl()Lcom/samsung/android/scloud/sync/oem/IOEMControl;
    .locals 1

    .prologue
    .line 110
    invoke-static {}, Lcom/samsung/android/scloud/sync/oem/ExternalOEMControl;->getInstance()Lcom/samsung/android/scloud/sync/oem/IOEMControl;

    move-result-object v0

    return-object v0
.end method

.method public getOemContentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/oem/ExternalOEMControlProvider$1;->contentUri:Landroid/net/Uri;

    return-object v0
.end method
