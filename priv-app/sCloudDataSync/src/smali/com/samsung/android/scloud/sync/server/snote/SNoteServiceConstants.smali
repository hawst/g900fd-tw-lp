.class public interface abstract Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceConstants;
.super Ljava/lang/Object;
.source "SNoteServiceConstants.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceConstants$API_CODE;,
        Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceConstants$API;
    }
.end annotation


# static fields
.field public static final ACCESS_TOKEN_PARM:Ljava/lang/String; = "access_token"

.field public static final BOUNDARY:Ljava/lang/String; = "AaB03x"

.field public static final CID_PARM:Ljava/lang/String; = "cid"

.field public static final CLIENT_TIMESTAMP:Ljava/lang/String; = "clientTimestamp"

.field public static final CTID_PARM:Ljava/lang/String; = "ctid"

.field public static final DEVICE_ID_PARM:Ljava/lang/String; = "did"

.field public static final KEY:Ljava/lang/String; = "key"

.field public static final MODIFIED_AFTER_PARM:Ljava/lang/String; = "modified_after"

.field public static final NEXT:Ljava/lang/String; = "next"

.field public static final NEXT_KEY:Ljava/lang/String; = "nextKey"

.field public static final START_KEY:Ljava/lang/String; = "start_key"

.field public static final USER_ID_PARM:Ljava/lang/String; = "uid"
