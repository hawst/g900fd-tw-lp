.class public Lcom/samsung/android/scloud/framework/PDMException;
.super Ljava/lang/RuntimeException;
.source "PDMException.java"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private mExceptionCode:I

.field private tag:Ljava/lang/Object;


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "code"    # I

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/RuntimeException;-><init>()V

    .line 29
    iput p1, p0, Lcom/samsung/android/scloud/framework/PDMException;->mExceptionCode:I

    .line 30
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;)V
    .locals 0
    .param p1, "code"    # I
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 33
    invoke-direct {p0, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 34
    iput p1, p0, Lcom/samsung/android/scloud/framework/PDMException;->mExceptionCode:I

    .line 35
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/Object;)V
    .locals 0
    .param p1, "code"    # I
    .param p2, "msg"    # Ljava/lang/String;
    .param p3, "tag"    # Ljava/lang/Object;

    .prologue
    .line 38
    invoke-direct {p0, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 39
    iput p1, p0, Lcom/samsung/android/scloud/framework/PDMException;->mExceptionCode:I

    .line 40
    iput-object p3, p0, Lcom/samsung/android/scloud/framework/PDMException;->tag:Ljava/lang/Object;

    .line 41
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "code"    # I
    .param p2, "msg"    # Ljava/lang/String;
    .param p3, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 49
    invoke-direct {p0, p2, p3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 50
    iput p1, p0, Lcom/samsung/android/scloud/framework/PDMException;->mExceptionCode:I

    .line 51
    return-void
.end method

.method public constructor <init>(ILjava/lang/Throwable;)V
    .locals 0
    .param p1, "code"    # I
    .param p2, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 44
    invoke-direct {p0, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    .line 45
    iput p1, p0, Lcom/samsung/android/scloud/framework/PDMException;->mExceptionCode:I

    .line 46
    return-void
.end method


# virtual methods
.method public getExceptionCode()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/samsung/android/scloud/framework/PDMException;->mExceptionCode:I

    return v0
.end method

.method public getTag()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/samsung/android/scloud/framework/PDMException;->tag:Ljava/lang/Object;

    return-object v0
.end method

.method public setCode(I)V
    .locals 0
    .param p1, "code"    # I

    .prologue
    .line 67
    iput p1, p0, Lcom/samsung/android/scloud/framework/PDMException;->mExceptionCode:I

    .line 68
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 59
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", eCode : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/scloud/framework/PDMException;->mExceptionCode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
