.class public Lcom/samsung/android/scloud/sync/common/SyncConstants;
.super Ljava/lang/Object;
.source "SyncConstants.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/scloud/sync/common/SyncConstants$INTENT;,
        Lcom/samsung/android/scloud/sync/common/SyncConstants$Status;,
        Lcom/samsung/android/scloud/sync/common/SyncConstants$ServiceType;,
        Lcom/samsung/android/scloud/sync/common/SyncConstants$Action;,
        Lcom/samsung/android/scloud/sync/common/SyncConstants$KEY;,
        Lcom/samsung/android/scloud/sync/common/SyncConstants$Activity;
    }
.end annotation


# static fields
.field public static final ACCOUNT_TYPE:Ljava/lang/String; = "com.osp.app.signin"

.field public static final CALLER_IS_SYNCADAPTER:Ljava/lang/String; = "caller_is_syncadapter"

.field public static final CONTENT_SYNC_FILE:Ljava/lang/String; = "content.sync"

.field public static final MAX_PROGRESS:I = 0x64

.field public static final MAX_RETRY_COUNT:I = 0x3

.field public static final RETRY_INTERVAL:J = 0x7d0L

.field public static final SYNC:Ljava/lang/String; = "SYNC_"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    return-void
.end method
