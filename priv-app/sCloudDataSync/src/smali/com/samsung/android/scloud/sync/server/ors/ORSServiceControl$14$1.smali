.class Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$14$1;
.super Ljava/lang/Object;
.source "ORSServiceControl.java"

# interfaces
.implements Lcom/samsung/android/scloud/framework/network/NetworkUtil$PDMProgressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$14;->handleResponse(Lorg/apache/http/Header;Ljava/lang/String;JLjava/io/InputStream;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$14;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$14;)V
    .locals 0

    .prologue
    .line 485
    iput-object p1, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$14$1;->this$1:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$14;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public transferred(JJ)V
    .locals 5
    .param p1, "now"    # J
    .param p3, "total"    # J

    .prologue
    .line 489
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$14$1;->this$1:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$14;

    iget-object v0, v0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$14;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    # getter for: Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mListener:Lcom/samsung/android/scloud/framework/IStatusListener;
    invoke-static {v0}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->access$100(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Lcom/samsung/android/scloud/framework/IStatusListener;

    move-result-object v0

    const/4 v1, 0x4

    const-string v2, "ORSServiceControl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "downloadFile transferred : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 491
    return-void
.end method
