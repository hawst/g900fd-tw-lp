.class Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$1;
.super Lcom/samsung/android/scloud/framework/network/NetworkUtil$JSONResponseHandler;
.source "ORSServiceControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->getServerTimestamp()J
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

.field final synthetic val$serverTime:[J


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;[J)V
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$1;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    iput-object p2, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$1;->val$serverTime:[J

    invoke-direct {p0}, Lcom/samsung/android/scloud/framework/network/NetworkUtil$JSONResponseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleJSONResponse(Lorg/json/JSONObject;)V
    .locals 7
    .param p1, "data"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 81
    iget-object v2, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$1;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    # getter for: Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mListener:Lcom/samsung/android/scloud/framework/IStatusListener;
    invoke-static {v2}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->access$100(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Lcom/samsung/android/scloud/framework/IStatusListener;

    move-result-object v2

    const/4 v3, 0x4

    const-string v4, "ORSServiceControl"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getTimestamp Finished - "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$1;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    # getter for: Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mCtid:Ljava/lang/String;
    invoke-static {v6}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->access$000(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v3, v4, v5}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 83
    const-string v2, "serverTimestamp"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 84
    const-string v2, "serverTimestamp"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 85
    .local v0, "timestamp":J
    iget-object v2, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$1;->val$serverTime:[J

    const/4 v3, 0x0

    aput-wide v0, v2, v3

    .line 87
    invoke-static {}, Lcom/samsung/android/scloud/framework/util/TimeManager;->create()Lcom/samsung/android/scloud/framework/util/TimeManager;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl$1;->this$0:Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;

    # getter for: Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;->access$200(Lcom/samsung/android/scloud/sync/server/ors/ORSServiceControl;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3, v0, v1}, Lcom/samsung/android/scloud/framework/util/TimeManager;->updateSettingsUsingServer(Landroid/content/Context;J)V

    .line 90
    .end local v0    # "timestamp":J
    :cond_0
    return-void
.end method
