.class public interface abstract Lcom/samsung/android/scloud/sync/model/IInternalModel;
.super Ljava/lang/Object;
.source "IInternalModel.java"

# interfaces
.implements Lcom/samsung/android/scloud/sync/model/IModel;


# virtual methods
.method public abstract getAccountNameFieldName()Ljava/lang/String;
.end method

.method public abstract getAccountTypeFieldName()Ljava/lang/String;
.end method

.method public abstract getDeletedFieldName()Ljava/lang/String;
.end method

.method public abstract getDeletedValue()Ljava/lang/String;
.end method

.method public abstract getDirtyFieldName()Ljava/lang/String;
.end method

.method public abstract getDirtyValue()Ljava/lang/String;
.end method

.method public abstract getLocalIdFieldName()Ljava/lang/String;
.end method

.method public abstract getSyncKeyFieldName()Ljava/lang/String;
.end method

.method public abstract getTimestampFieldName()Ljava/lang/String;
.end method
