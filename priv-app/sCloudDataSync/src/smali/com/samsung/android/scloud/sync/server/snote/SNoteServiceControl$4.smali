.class Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$4;
.super Lcom/samsung/android/scloud/framework/network/NetworkUtil$JSONResponseHandler;
.source "SNoteServiceControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->uploadItem(Lcom/samsung/android/scloud/sync/data/SyncItem;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;

.field final synthetic val$item:Lcom/samsung/android/scloud/sync/data/SyncItem;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;Lcom/samsung/android/scloud/sync/data/SyncItem;)V
    .locals 0

    .prologue
    .line 148
    iput-object p1, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$4;->this$0:Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;

    iput-object p2, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$4;->val$item:Lcom/samsung/android/scloud/sync/data/SyncItem;

    invoke-direct {p0}, Lcom/samsung/android/scloud/framework/network/NetworkUtil$JSONResponseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleJSONResponse(Lorg/json/JSONObject;)V
    .locals 5
    .param p1, "json"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 152
    const-string v0, "rcode"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "rcode"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_1

    .line 153
    :cond_0
    new-instance v0, Lcom/samsung/android/scloud/framework/PDMException;

    const/16 v1, 0x13b

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Upload error : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/scloud/framework/PDMException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 155
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$4;->this$0:Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;

    # getter for: Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mListener:Lcom/samsung/android/scloud/framework/IStatusListener;
    invoke-static {v0}, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->access$100(Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;)Lcom/samsung/android/scloud/framework/IStatusListener;

    move-result-object v0

    const/4 v1, 0x4

    const-string v2, "SNoteServiceControl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "uploadFile Finished - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$4;->this$0:Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;

    # getter for: Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mCtid:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->access$000(Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$4;->val$item:Lcom/samsung/android/scloud/sync/data/SyncItem;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 156
    return-void
.end method
