.class public abstract Lcom/samsung/android/scloud/framework/PDMActivity;
.super Landroid/app/Activity;
.source "PDMActivity.java"

# interfaces
.implements Lcom/samsung/android/scloud/framework/IPDMContext;


# instance fields
.field protected ActivityHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 48
    new-instance v0, Lcom/samsung/android/scloud/framework/PDMActivity$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/scloud/framework/PDMActivity$1;-><init>(Lcom/samsung/android/scloud/framework/PDMActivity;)V

    iput-object v0, p0, Lcom/samsung/android/scloud/framework/PDMActivity;->ActivityHandler:Landroid/os/Handler;

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 34
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 35
    invoke-virtual {p0}, Lcom/samsung/android/scloud/framework/PDMActivity;->getTag()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    invoke-virtual {p0}, Lcom/samsung/android/scloud/framework/PDMActivity;->getTag()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/android/scloud/framework/PDMActivity;->getTargetEventServiceCodes()[I

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/framework/PDMActivity;->ActivityHandler:Landroid/os/Handler;

    invoke-static {v0, v1, v2}, Lcom/samsung/android/scloud/framework/PDMApp;->setHandlerActivity(Ljava/lang/String;[ILandroid/os/Handler;)V

    .line 38
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 42
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 43
    invoke-virtual {p0}, Lcom/samsung/android/scloud/framework/PDMActivity;->getTag()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onDestory()"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    invoke-virtual {p0}, Lcom/samsung/android/scloud/framework/PDMActivity;->getTag()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/scloud/framework/PDMApp;->removeHandlerActivity(Ljava/lang/String;)V

    .line 46
    return-void
.end method
