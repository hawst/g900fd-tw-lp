.class public Lcom/samsung/android/scloud/sync/server/snote/MFDownloadParser;
.super Ljava/lang/Object;
.source "MFDownloadParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/scloud/sync/server/snote/MFDownloadParser$1;,
        Lcom/samsung/android/scloud/sync/server/snote/MFDownloadParser$SNoteHeader;
    }
.end annotation


# static fields
.field private static final CONTENT_DISPOSITION_KEY:Ljava/lang/String; = "content-disposition"

.field private static final FILE:Ljava/lang/String; = "file"

.field private static final FILENAME_KEY:Ljava/lang/String; = "filename"

.field private static final NAME_KEY:Ljava/lang/String; = "name"

.field private static final SNOTE_DETAIL:Ljava/lang/String; = "snote_detail"

.field private static final TAG:Ljava/lang/String; = "MFDownloadParser"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    return-void
.end method

.method public static parseAndDownload(Lorg/apache/http/Header;Ljava/io/InputStream;Ljava/lang/String;)Ljava/util/List;
    .locals 16
    .param p0, "header"    # Lorg/apache/http/Header;
    .param p1, "stream"    # Ljava/io/InputStream;
    .param p2, "folderPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/http/Header;",
            "Ljava/io/InputStream;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 49
    const/4 v11, 0x0

    .line 50
    .local v11, "snotefileos":Ljava/io/FileOutputStream;
    const/4 v6, 0x0

    .line 51
    .local v6, "jsonfileos":Ljava/io/FileOutputStream;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 54
    .local v2, "downloadList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :try_start_0
    invoke-interface/range {p0 .. p0}, Lorg/apache/http/Header;->getElements()[Lorg/apache/http/HeaderElement;

    move-result-object v3

    .line 55
    .local v3, "elements":[Lorg/apache/http/HeaderElement;
    const-string v13, "multipart/form-data"

    const/4 v14, 0x0

    aget-object v14, v3, v14

    invoke-interface {v14}, Lorg/apache/http/HeaderElement;->getName()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Ljunit/framework/Assert;->assertEquals(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    const/4 v13, 0x0

    aget-object v13, v3, v13

    const-string v14, "boundary"

    invoke-interface {v13, v14}, Lorg/apache/http/HeaderElement;->getParameterByName(Ljava/lang/String;)Lorg/apache/http/NameValuePair;

    move-result-object v8

    .line 57
    .local v8, "p1":Lorg/apache/http/NameValuePair;
    invoke-static {v8}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    .line 59
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "--"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-interface {v8}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 60
    .local v1, "boundary":Ljava/lang/String;
    invoke-static {v1}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    .line 61
    const-string v13, "MFDownloadParser"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "boundary : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    new-instance v4, Ljava/io/BufferedReader;

    new-instance v13, Ljava/io/InputStreamReader;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-direct {v13, v0, v14}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V

    invoke-direct {v4, v13}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 65
    .local v4, "in":Ljava/io/BufferedReader;
    const-string v9, ""

    .line 67
    .local v9, "read":Ljava/lang/String;
    new-instance v10, Lcom/samsung/android/scloud/sync/server/snote/MFDownloadParser$SNoteHeader;

    const/4 v13, 0x0

    invoke-direct {v10, v13}, Lcom/samsung/android/scloud/sync/server/snote/MFDownloadParser$SNoteHeader;-><init>(Lcom/samsung/android/scloud/sync/server/snote/MFDownloadParser$1;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .local v10, "snoteHeader":Lcom/samsung/android/scloud/sync/server/snote/MFDownloadParser$SNoteHeader;
    move-object v7, v6

    .end local v6    # "jsonfileos":Ljava/io/FileOutputStream;
    .local v7, "jsonfileos":Ljava/io/FileOutputStream;
    move-object v12, v11

    .line 70
    .end local v11    # "snotefileos":Ljava/io/FileOutputStream;
    .local v12, "snotefileos":Ljava/io/FileOutputStream;
    :cond_0
    :goto_0
    :try_start_1
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_1

    .line 71
    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 72
    invoke-static {v4, v10}, Lcom/samsung/android/scloud/sync/server/snote/MFDownloadParser;->parseHeader(Ljava/io/BufferedReader;Lcom/samsung/android/scloud/sync/server/snote/MFDownloadParser$SNoteHeader;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v13

    if-nez v13, :cond_4

    .line 107
    :cond_1
    if-eqz v12, :cond_2

    invoke-virtual {v12}, Ljava/io/FileOutputStream;->close()V

    .line 108
    :cond_2
    if-eqz v7, :cond_3

    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V

    .line 111
    :cond_3
    const-string v13, "MFDownloadParser"

    const-string v14, "read end"

    invoke-static {v13, v14}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    return-object v2

    .line 75
    :cond_4
    :try_start_2
    iget v13, v10, Lcom/samsung/android/scloud/sync/server/snote/MFDownloadParser$SNoteHeader;->content_type:I

    const/16 v14, 0x8

    if-ne v13, v14, :cond_5

    .line 76
    new-instance v11, Ljava/io/FileOutputStream;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, v10, Lcom/samsung/android/scloud/sync/server/snote/MFDownloadParser$SNoteHeader;->filename:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v11, v13}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 77
    .end local v12    # "snotefileos":Ljava/io/FileOutputStream;
    .restart local v11    # "snotefileos":Ljava/io/FileOutputStream;
    :try_start_3
    iget-object v13, v10, Lcom/samsung/android/scloud/sync/server/snote/MFDownloadParser$SNoteHeader;->filename:Ljava/lang/String;

    invoke-interface {v2, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-object v12, v11

    .end local v11    # "snotefileos":Ljava/io/FileOutputStream;
    .restart local v12    # "snotefileos":Ljava/io/FileOutputStream;
    goto :goto_0

    .line 79
    :cond_5
    :try_start_4
    iget v13, v10, Lcom/samsung/android/scloud/sync/server/snote/MFDownloadParser$SNoteHeader;->content_type:I

    const/4 v14, 0x1

    if-ne v13, v14, :cond_0

    .line 80
    new-instance v6, Ljava/io/FileOutputStream;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "content.sync"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v6, v13}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .end local v7    # "jsonfileos":Ljava/io/FileOutputStream;
    .restart local v6    # "jsonfileos":Ljava/io/FileOutputStream;
    move-object v7, v6

    .end local v6    # "jsonfileos":Ljava/io/FileOutputStream;
    .restart local v7    # "jsonfileos":Ljava/io/FileOutputStream;
    goto :goto_0

    .line 85
    :cond_6
    iget v13, v10, Lcom/samsung/android/scloud/sync/server/snote/MFDownloadParser$SNoteHeader;->content_type:I

    const/16 v14, 0x8

    if-ne v13, v14, :cond_9

    .line 87
    if-eqz v12, :cond_0

    .line 88
    const-string v13, "MFDownloadParser"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "file read : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    const/4 v13, 0x0

    invoke-static {v9, v13}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_0

    .line 104
    :catch_0
    move-exception v5

    move-object v6, v7

    .end local v7    # "jsonfileos":Ljava/io/FileOutputStream;
    .restart local v6    # "jsonfileos":Ljava/io/FileOutputStream;
    move-object v11, v12

    .line 105
    .end local v1    # "boundary":Ljava/lang/String;
    .end local v3    # "elements":[Lorg/apache/http/HeaderElement;
    .end local v4    # "in":Ljava/io/BufferedReader;
    .end local v8    # "p1":Lorg/apache/http/NameValuePair;
    .end local v9    # "read":Ljava/lang/String;
    .end local v10    # "snoteHeader":Lcom/samsung/android/scloud/sync/server/snote/MFDownloadParser$SNoteHeader;
    .end local v12    # "snotefileos":Ljava/io/FileOutputStream;
    .local v5, "ioe":Ljava/io/IOException;
    .restart local v11    # "snotefileos":Ljava/io/FileOutputStream;
    :goto_1
    :try_start_5
    throw v5
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 107
    .end local v5    # "ioe":Ljava/io/IOException;
    :catchall_0
    move-exception v13

    :goto_2
    if-eqz v11, :cond_7

    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V

    .line 108
    :cond_7
    if-eqz v6, :cond_8

    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V

    :cond_8
    throw v13

    .line 93
    .end local v6    # "jsonfileos":Ljava/io/FileOutputStream;
    .end local v11    # "snotefileos":Ljava/io/FileOutputStream;
    .restart local v1    # "boundary":Ljava/lang/String;
    .restart local v3    # "elements":[Lorg/apache/http/HeaderElement;
    .restart local v4    # "in":Ljava/io/BufferedReader;
    .restart local v7    # "jsonfileos":Ljava/io/FileOutputStream;
    .restart local v8    # "p1":Lorg/apache/http/NameValuePair;
    .restart local v9    # "read":Ljava/lang/String;
    .restart local v10    # "snoteHeader":Lcom/samsung/android/scloud/sync/server/snote/MFDownloadParser$SNoteHeader;
    .restart local v12    # "snotefileos":Ljava/io/FileOutputStream;
    :cond_9
    :try_start_6
    iget v13, v10, Lcom/samsung/android/scloud/sync/server/snote/MFDownloadParser$SNoteHeader;->content_type:I

    const/4 v14, 0x1

    if-ne v13, v14, :cond_0

    .line 95
    if-eqz v7, :cond_0

    .line 96
    const-string v13, "MFDownloadParser"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "snote_detail : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v13

    invoke-virtual {v9, v13}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v13

    invoke-virtual {v7, v13}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto/16 :goto_0

    .line 107
    :catchall_1
    move-exception v13

    move-object v6, v7

    .end local v7    # "jsonfileos":Ljava/io/FileOutputStream;
    .restart local v6    # "jsonfileos":Ljava/io/FileOutputStream;
    move-object v11, v12

    .end local v12    # "snotefileos":Ljava/io/FileOutputStream;
    .restart local v11    # "snotefileos":Ljava/io/FileOutputStream;
    goto :goto_2

    .end local v6    # "jsonfileos":Ljava/io/FileOutputStream;
    .restart local v7    # "jsonfileos":Ljava/io/FileOutputStream;
    :catchall_2
    move-exception v13

    move-object v6, v7

    .end local v7    # "jsonfileos":Ljava/io/FileOutputStream;
    .restart local v6    # "jsonfileos":Ljava/io/FileOutputStream;
    goto :goto_2

    .line 104
    .end local v1    # "boundary":Ljava/lang/String;
    .end local v3    # "elements":[Lorg/apache/http/HeaderElement;
    .end local v4    # "in":Ljava/io/BufferedReader;
    .end local v8    # "p1":Lorg/apache/http/NameValuePair;
    .end local v9    # "read":Ljava/lang/String;
    .end local v10    # "snoteHeader":Lcom/samsung/android/scloud/sync/server/snote/MFDownloadParser$SNoteHeader;
    :catch_1
    move-exception v5

    goto :goto_1

    .end local v6    # "jsonfileos":Ljava/io/FileOutputStream;
    .restart local v1    # "boundary":Ljava/lang/String;
    .restart local v3    # "elements":[Lorg/apache/http/HeaderElement;
    .restart local v4    # "in":Ljava/io/BufferedReader;
    .restart local v7    # "jsonfileos":Ljava/io/FileOutputStream;
    .restart local v8    # "p1":Lorg/apache/http/NameValuePair;
    .restart local v9    # "read":Ljava/lang/String;
    .restart local v10    # "snoteHeader":Lcom/samsung/android/scloud/sync/server/snote/MFDownloadParser$SNoteHeader;
    :catch_2
    move-exception v5

    move-object v6, v7

    .end local v7    # "jsonfileos":Ljava/io/FileOutputStream;
    .restart local v6    # "jsonfileos":Ljava/io/FileOutputStream;
    goto :goto_1
.end method

.method private static parseHeader(Ljava/io/BufferedReader;Lcom/samsung/android/scloud/sync/server/snote/MFDownloadParser$SNoteHeader;)Z
    .locals 14
    .param p0, "in"    # Ljava/io/BufferedReader;
    .param p1, "snoteHeader"    # Lcom/samsung/android/scloud/sync/server/snote/MFDownloadParser$SNoteHeader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 116
    const-string v7, ""

    .line 117
    .local v7, "read":Ljava/lang/String;
    iput v10, p1, Lcom/samsung/android/scloud/sync/server/snote/MFDownloadParser$SNoteHeader;->content_type:I

    .line 118
    :cond_0
    invoke-virtual {p0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_1

    .line 119
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v11

    if-lt v11, v9, :cond_1

    invoke-virtual {v7, v10}, Ljava/lang/String;->charAt(I)C

    move-result v11

    const/16 v12, 0xa

    if-eq v11, v12, :cond_1

    invoke-virtual {v7, v10}, Ljava/lang/String;->charAt(I)C

    move-result v11

    const/16 v12, 0xd

    if-ne v11, v12, :cond_2

    .line 150
    :cond_1
    iget v11, p1, Lcom/samsung/android/scloud/sync/server/snote/MFDownloadParser$SNoteHeader;->content_type:I

    if-eqz v11, :cond_6

    :goto_0
    return v9

    .line 123
    :cond_2
    const-string v11, ":"

    invoke-virtual {v7, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 124
    .local v5, "lines":[Ljava/lang/String;
    array-length v11, v5

    if-le v11, v9, :cond_0

    .line 125
    aget-object v11, v5, v10

    const-string v12, "content-disposition"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 126
    aget-object v11, v5, v9

    const-string v12, ";"

    invoke-virtual {v11, v12}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 127
    .local v2, "elements":[Ljava/lang/String;
    array-length v11, v2

    if-le v11, v9, :cond_0

    .line 128
    move-object v0, v2

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v4, :cond_0

    aget-object v1, v0, v3

    .line 129
    .local v1, "element":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    const-string v12, "="

    invoke-virtual {v11, v12}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 130
    .local v8, "values":[Ljava/lang/String;
    array-length v11, v8

    if-le v11, v9, :cond_3

    .line 131
    aget-object v11, v8, v10

    const-string v12, "name"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 132
    aget-object v11, v8, v9

    const-string v12, "\""

    const-string v13, ""

    invoke-virtual {v11, v12, v13}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 133
    .local v6, "name":Ljava/lang/String;
    const-string v11, "file"

    invoke-virtual {v6, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 134
    const/16 v11, 0x8

    iput v11, p1, Lcom/samsung/android/scloud/sync/server/snote/MFDownloadParser$SNoteHeader;->content_type:I

    .line 128
    .end local v6    # "name":Ljava/lang/String;
    :cond_3
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 136
    .restart local v6    # "name":Ljava/lang/String;
    :cond_4
    const-string v11, "snote_detail"

    invoke-virtual {v6, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 137
    iput v9, p1, Lcom/samsung/android/scloud/sync/server/snote/MFDownloadParser$SNoteHeader;->content_type:I

    goto :goto_2

    .line 140
    .end local v6    # "name":Ljava/lang/String;
    :cond_5
    aget-object v11, v8, v10

    const-string v12, "filename"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 141
    aget-object v11, v8, v9

    const-string v12, "\""

    const-string v13, ""

    invoke-virtual {v11, v12, v13}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, p1, Lcom/samsung/android/scloud/sync/server/snote/MFDownloadParser$SNoteHeader;->filename:Ljava/lang/String;

    goto :goto_2

    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "element":Ljava/lang/String;
    .end local v2    # "elements":[Ljava/lang/String;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    .end local v5    # "lines":[Ljava/lang/String;
    .end local v8    # "values":[Ljava/lang/String;
    :cond_6
    move v9, v10

    .line 150
    goto :goto_0
.end method
