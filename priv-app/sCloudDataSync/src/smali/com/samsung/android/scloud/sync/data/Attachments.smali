.class public Lcom/samsung/android/scloud/sync/data/Attachments;
.super Ljava/lang/Object;
.source "Attachments.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "Attachments"


# instance fields
.field private filearray:[Ljava/lang/String;

.field private final filelists:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private hasFile:Z

.field private final timestamps:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/scloud/sync/data/Attachments;->filearray:[Ljava/lang/String;

    .line 14
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/sync/data/Attachments;->filelists:Ljava/util/List;

    .line 15
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/sync/data/Attachments;->timestamps:Ljava/util/List;

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/scloud/sync/data/Attachments;->hasFile:Z

    .line 66
    return-void
.end method

.method public constructor <init>([Ljava/lang/String;[J)V
    .locals 6
    .param p1, "filelists"    # [Ljava/lang/String;
    .param p2, "timestamps"    # [J

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/android/scloud/sync/data/Attachments;->filearray:[Ljava/lang/String;

    .line 14
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/scloud/sync/data/Attachments;->filelists:Ljava/util/List;

    .line 15
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/scloud/sync/data/Attachments;->timestamps:Ljava/util/List;

    .line 19
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/samsung/android/scloud/sync/data/Attachments;->hasFile:Z

    .line 24
    if-eqz p2, :cond_1

    array-length v2, p2

    if-lez v2, :cond_1

    if-eqz p1, :cond_1

    array-length v2, p1

    if-lez v2, :cond_1

    .line 25
    array-length v1, p1

    .line 26
    .local v1, "length":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 27
    iget-object v2, p0, Lcom/samsung/android/scloud/sync/data/Attachments;->filelists:Ljava/util/List;

    aget-object v3, p1, v0

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 28
    iget-object v2, p0, Lcom/samsung/android/scloud/sync/data/Attachments;->timestamps:Ljava/util/List;

    aget-wide v3, p2, v0

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 29
    const-string v2, "Attachments"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[Attachments] : LocalItem - file : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, p1, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", timestamp : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-wide v4, p2, v0

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/framework/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 32
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/scloud/sync/data/Attachments;->filearray:[Ljava/lang/String;

    .line 33
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/scloud/sync/data/Attachments;->hasFile:Z

    .line 35
    .end local v0    # "i":I
    .end local v1    # "length":I
    :cond_1
    return-void
.end method


# virtual methods
.method public addFile(Ljava/lang/String;J)V
    .locals 2
    .param p1, "filename"    # Ljava/lang/String;
    .param p2, "timestamp"    # J

    .prologue
    .line 97
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/data/Attachments;->filelists:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 98
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/data/Attachments;->timestamps:Ljava/util/List;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 99
    return-void
.end method

.method public final getFileArray()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/data/Attachments;->filearray:[Ljava/lang/String;

    return-object v0
.end method

.method public getFileNameAt(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 119
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/data/Attachments;->filelists:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getTimeStampAt(I)J
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 123
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/data/Attachments;->timestamps:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getTimeStampOf(Ljava/lang/String;)J
    .locals 3
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 127
    iget-object v1, p0, Lcom/samsung/android/scloud/sync/data/Attachments;->filelists:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 128
    .local v0, "index":I
    const/4 v1, -0x1

    if-le v0, v1, :cond_0

    .line 129
    iget-object v1, p0, Lcom/samsung/android/scloud/sync/data/Attachments;->timestamps:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    .line 130
    :goto_0
    return-wide v1

    :cond_0
    const-wide/16 v1, -0x1

    goto :goto_0
.end method

.method public hasFile()Z
    .locals 1

    .prologue
    .line 153
    iget-boolean v0, p0, Lcom/samsung/android/scloud/sync/data/Attachments;->hasFile:Z

    return v0
.end method

.method public hasFile(Ljava/lang/String;)Z
    .locals 1
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/data/Attachments;->filelists:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public howManyFiles()I
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/data/Attachments;->filelists:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/samsung/android/scloud/sync/data/Attachments;->filelists:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 109
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
