.class Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$2;
.super Lcom/samsung/android/scloud/framework/network/NetworkUtil$JSONResponseHandler;
.source "SNoteServiceControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->getKeys(Ljava/lang/String;JLjava/util/HashMap;)Ljava/lang/String;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;

.field final synthetic val$nextKey:[Ljava/lang/String;

.field final synthetic val$outServerChanges:Ljava/util/HashMap;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;[Ljava/lang/String;Ljava/util/HashMap;)V
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$2;->this$0:Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;

    iput-object p2, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$2;->val$nextKey:[Ljava/lang/String;

    iput-object p3, p0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$2;->val$outServerChanges:Ljava/util/HashMap;

    invoke-direct {p0}, Lcom/samsung/android/scloud/framework/network/NetworkUtil$JSONResponseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleJSONResponse(Lorg/json/JSONObject;)V
    .locals 18
    .param p1, "data"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 93
    const-string v12, "next"

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 94
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$2;->val$nextKey:[Ljava/lang/String;

    const/4 v13, 0x0

    const-string v14, "next_key"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    .line 98
    :goto_0
    const-string v12, "snote_list"

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v7

    .line 100
    .local v7, "list":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v12

    if-ge v2, v12, :cond_3

    .line 101
    invoke-virtual {v7, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 103
    .local v4, "json":Lorg/json/JSONObject;
    const/4 v9, 0x0

    .line 105
    .local v9, "syncName":Ljava/lang/String;
    :try_start_0
    const-string v12, "filepath"

    invoke-virtual {v4, v12}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 106
    const-string v12, "filepath"

    invoke-virtual {v4, v12}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 110
    :cond_0
    :goto_2
    const-string v12, "datakey"

    invoke-virtual {v4, v12}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 112
    .local v6, "key":Ljava/lang/String;
    const-string v12, "clientTimestamp"

    invoke-virtual {v4, v12}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    .line 113
    .local v10, "timestamp":J
    const-string v12, "deleted"

    invoke-virtual {v4, v12}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 116
    .local v1, "deleted":Z
    new-instance v3, Lcom/samsung/android/scloud/sync/data/SyncItem;

    invoke-direct {v3, v6, v10, v11, v1}, Lcom/samsung/android/scloud/sync/data/SyncItem;-><init>(Ljava/lang/String;JZ)V

    .line 117
    .local v3, "item":Lcom/samsung/android/scloud/sync/data/SyncItem;
    invoke-virtual {v3, v9}, Lcom/samsung/android/scloud/sync/data/SyncItem;->setTag(Ljava/lang/String;)V

    .line 119
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$2;->this$0:Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;

    # getter for: Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mListener:Lcom/samsung/android/scloud/framework/IStatusListener;
    invoke-static {v12}, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->access$100(Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;)Lcom/samsung/android/scloud/framework/IStatusListener;

    move-result-object v12

    const/4 v13, 0x4

    const-string v14, "SNoteServiceControl"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "["

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$2;->this$0:Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;

    move-object/from16 v16, v0

    # getter for: Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;
    invoke-static/range {v16 .. v16}, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->access$300(Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;)Lcom/samsung/android/scloud/sync/model/IModel;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "]("

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$2;->this$0:Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;

    move-object/from16 v16, v0

    # getter for: Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mCtid:Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->access$000(Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ") : ServerItem - key : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v3}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getSyncKey()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", timestamp : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v3}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getTimeStamp()J

    move-result-wide v16

    invoke-virtual/range {v15 .. v17}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", deleted : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v3}, Lcom/samsung/android/scloud/sync/data/SyncItem;->isDeleted()Z

    move-result v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", tag : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v3}, Lcom/samsung/android/scloud/sync/data/SyncItem;->getTag()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-interface {v12, v13, v14, v15}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 120
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$2;->val$outServerChanges:Ljava/util/HashMap;

    invoke-virtual {v12, v6, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    .line 100
    .end local v1    # "deleted":Z
    .end local v3    # "item":Lcom/samsung/android/scloud/sync/data/SyncItem;
    .end local v6    # "key":Ljava/lang/String;
    .end local v10    # "timestamp":J
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    .line 96
    .end local v2    # "i":I
    .end local v4    # "json":Lorg/json/JSONObject;
    .end local v7    # "list":Lorg/json/JSONArray;
    .end local v9    # "syncName":Ljava/lang/String;
    :cond_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$2;->val$nextKey:[Ljava/lang/String;

    const/4 v13, 0x0

    const/4 v14, 0x0

    aput-object v14, v12, v13

    goto/16 :goto_0

    .line 107
    .restart local v2    # "i":I
    .restart local v4    # "json":Lorg/json/JSONObject;
    .restart local v7    # "list":Lorg/json/JSONArray;
    .restart local v9    # "syncName":Ljava/lang/String;
    :cond_2
    :try_start_1
    const-string v12, "folderpath"

    invoke-virtual {v4, v12}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 108
    const-string v12, "folderpath"

    invoke-virtual {v4, v12}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v9

    goto/16 :goto_2

    .line 121
    :catch_0
    move-exception v5

    .line 122
    .local v5, "jsone":Lorg/json/JSONException;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$2;->this$0:Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;

    # getter for: Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mListener:Lcom/samsung/android/scloud/framework/IStatusListener;
    invoke-static {v12}, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->access$100(Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;)Lcom/samsung/android/scloud/framework/IStatusListener;

    move-result-object v12

    const/4 v13, 0x0

    const-string v14, "SNoteServiceControl"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "["

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$2;->this$0:Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;

    move-object/from16 v16, v0

    # getter for: Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;
    invoke-static/range {v16 .. v16}, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->access$300(Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;)Lcom/samsung/android/scloud/sync/model/IModel;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "]("

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$2;->this$0:Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;

    move-object/from16 v16, v0

    # getter for: Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mCtid:Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->access$000(Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ") : ServerItem , invalid value : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-interface {v12, v13, v14, v15}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 123
    .end local v5    # "jsone":Lorg/json/JSONException;
    :catch_1
    move-exception v8

    .line 124
    .local v8, "nfe":Ljava/lang/NumberFormatException;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$2;->this$0:Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;

    # getter for: Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mListener:Lcom/samsung/android/scloud/framework/IStatusListener;
    invoke-static {v12}, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->access$100(Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;)Lcom/samsung/android/scloud/framework/IStatusListener;

    move-result-object v12

    const/4 v13, 0x0

    const-string v14, "SNoteServiceControl"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "["

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$2;->this$0:Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;

    move-object/from16 v16, v0

    # getter for: Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mModel:Lcom/samsung/android/scloud/sync/model/IModel;
    invoke-static/range {v16 .. v16}, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->access$300(Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;)Lcom/samsung/android/scloud/sync/model/IModel;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Lcom/samsung/android/scloud/sync/model/IModel;->getName()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "]("

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl$2;->this$0:Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;

    move-object/from16 v16, v0

    # getter for: Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->mCtid:Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;->access$000(Lcom/samsung/android/scloud/sync/server/snote/SNoteServiceControl;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ") : ServerItem , invalid value : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-interface {v12, v13, v14, v15}, Lcom/samsung/android/scloud/framework/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 127
    .end local v4    # "json":Lorg/json/JSONObject;
    .end local v8    # "nfe":Ljava/lang/NumberFormatException;
    .end local v9    # "syncName":Ljava/lang/String;
    :cond_3
    return-void
.end method
