.class public Lcom/samsung/android/scloud/sync/model/ReadingList;
.super Lcom/samsung/android/scloud/sync/model/CommonORSModel;
.source "ReadingList.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/samsung/android/scloud/sync/model/CommonORSModel;-><init>()V

    return-void
.end method


# virtual methods
.method public getCid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    const-string v0, "P56GWW8N4r"

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    const-string v0, "READINGLIST_DATA"

    return-object v0
.end method

.method public getOEMControl()Lcom/samsung/android/scloud/sync/oem/IOEMControl;
    .locals 1

    .prologue
    .line 14
    invoke-static {}, Lcom/samsung/android/scloud/sync/oem/ExternalOEMControl;->getInstance()Lcom/samsung/android/scloud/sync/oem/IOEMControl;

    move-result-object v0

    return-object v0
.end method

.method public getOemContentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/samsung/android/scloud/sync/common/SyncSources$OEMAuthorities;->READINGLIST_OEM_CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method
