.class public final Lcom/sec/android/sCloudSync/Records/RecordKey;
.super Lcom/sec/android/sCloudSync/Records/RecordBaseKeyItem;
.source "RecordKey.java"


# static fields
.field private static final SIZE:Ljava/lang/String; = "size"


# instance fields
.field mSize:J


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/Long;JZ)V
    .locals 0
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "timeStamp"    # Ljava/lang/Long;
    .param p3, "size"    # J
    .param p5, "deleted"    # Z

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p5}, Lcom/sec/android/sCloudSync/Records/RecordBaseKeyItem;-><init>(Ljava/lang/String;Ljava/lang/Long;Z)V

    .line 35
    iput-wide p3, p0, Lcom/sec/android/sCloudSync/Records/RecordKey;->mSize:J

    .line 36
    return-void
.end method


# virtual methods
.method public fromJSON(Ljava/lang/Object;)V
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 39
    invoke-super {p0, p1}, Lcom/sec/android/sCloudSync/Records/RecordBaseKeyItem;->fromJSON(Ljava/lang/Object;)V

    move-object v0, p1

    .line 40
    check-cast v0, Lorg/json/JSONObject;

    .line 41
    .local v0, "json":Lorg/json/JSONObject;
    const-string v1, "size"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 42
    const-string v1, "size"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/sec/android/sCloudSync/Records/RecordKey;->mSize:J

    .line 43
    :cond_0
    return-void
.end method

.method public getSize()J
    .locals 2

    .prologue
    .line 30
    iget-wide v0, p0, Lcom/sec/android/sCloudSync/Records/RecordKey;->mSize:J

    return-wide v0
.end method

.method public toJSON()Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 46
    invoke-super {p0}, Lcom/sec/android/sCloudSync/Records/RecordBaseKeyItem;->toJSON()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    .line 47
    .local v0, "json":Lorg/json/JSONObject;
    const-string v1, "size"

    iget-wide v2, p0, Lcom/sec/android/sCloudSync/Records/RecordKey;->mSize:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 48
    return-object v0
.end method
