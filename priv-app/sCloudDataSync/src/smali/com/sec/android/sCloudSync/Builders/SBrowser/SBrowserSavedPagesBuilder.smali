.class public Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserSavedPagesBuilder;
.super Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;
.source "SBrowserSavedPagesBuilder.java"


# static fields
.field private static final SBROWSERSAVEDPAGES_COLUMNS:[Ljava/lang/String;

.field private static final SBROWSERSAVEDPAGES_URI:Landroid/net/Uri;

.field private static final TAG:Ljava/lang/String; = "SBrowserSavedPagesBuilder"


# instance fields
.field private final mSavedPagesDownloadList:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mSavedPagesUploadList:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 26
    sget-object v0, Lcom/sec/android/sCloudSync/Constants/SBrowserContract;->SBROWSER_SAVEDPAGES_URI:Landroid/net/Uri;

    const-string v1, "caller_is_syncadapter"

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserSavedPagesBuilder;->SBROWSERSAVEDPAGES_URI:Landroid/net/Uri;

    .line 30
    const/16 v0, 0x13

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "path"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "dir_path"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "description"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "favicon"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "url"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "account_name"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "account_type"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "device_id"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "device_name"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "source_id"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "version"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "date"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "created"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "modified"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "user_added"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "synced"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "is_read"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "is_image"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserSavedPagesBuilder;->SBROWSERSAVEDPAGES_COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentProviderClient;Landroid/accounts/Account;Landroid/content/Context;)V
    .locals 1
    .param p1, "provider"    # Landroid/content/ContentProviderClient;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;-><init>(Landroid/content/ContentProviderClient;Landroid/accounts/Account;Landroid/content/Context;)V

    .line 23
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserSavedPagesBuilder;->mSavedPagesUploadList:Landroid/util/SparseArray;

    .line 24
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserSavedPagesBuilder;->mSavedPagesDownloadList:Ljava/util/Map;

    .line 54
    return-void
.end method


# virtual methods
.method public doApplyBatch()Z
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x0

    return v0
.end method

.method public getSavedPagesDownloadList()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserSavedPagesBuilder;->mSavedPagesDownloadList:Ljava/util/Map;

    return-object v0
.end method

.method public getSavedPagesUploadList()Landroid/util/SparseArray;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserSavedPagesBuilder;->mSavedPagesUploadList:Landroid/util/SparseArray;

    return-object v0
.end method

.method public insert(Ljava/lang/String;Ljava/lang/String;J)Z
    .locals 10
    .param p1, "jsonString"    # Ljava/lang/String;
    .param p2, "syncKey"    # Ljava/lang/String;
    .param p3, "timeStamp"    # J

    .prologue
    const/4 v5, 0x1

    const/4 v9, 0x0

    const/4 v6, 0x0

    .line 137
    const/4 v4, 0x0

    .line 138
    .local v4, "sBrowserSavedPagesValues":Landroid/content/ContentValues;
    iget-object v7, p0, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserSavedPagesBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    sget-object v8, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserSavedPagesBuilder;->SBROWSERSAVEDPAGES_COLUMNS:[Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 141
    :try_start_0
    iget-object v7, p0, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserSavedPagesBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v7, p1}, Lcom/sec/android/sCloudSync/Records/JSONParser;->fromJSONString(Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v4

    .line 142
    const-string v7, "favicon"

    invoke-virtual {v4, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 143
    .local v2, "favIcon":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 146
    const/4 v7, 0x0

    invoke-static {v2, v7}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    .line 147
    .local v0, "buffer":[B
    const-string v7, "favicon"

    invoke-virtual {v4, v7, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 149
    .end local v0    # "buffer":[B
    :cond_0
    const-string v7, "sync1"

    invoke-virtual {v4, v7, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    const-string v7, "sync5"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 152
    const-string v7, "is_deleted"

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 158
    iget-object v7, p0, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserSavedPagesBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v7, v9}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 161
    const-string v7, "path"

    invoke-virtual {v4, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 162
    .local v3, "pathUrl":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserSavedPagesBuilder;->mSavedPagesDownloadList:Ljava/util/Map;

    invoke-interface {v7, p2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 165
    :try_start_1
    iget-object v7, p0, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserSavedPagesBuilder;->mProvider:Landroid/content/ContentProviderClient;

    sget-object v8, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserSavedPagesBuilder;->SBROWSERSAVEDPAGES_URI:Landroid/net/Uri;

    invoke-virtual {v7, v8, v4}, Landroid/content/ContentProviderClient;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 170
    .end local v2    # "favIcon":Ljava/lang/String;
    .end local v3    # "pathUrl":Ljava/lang/String;
    :goto_0
    return v5

    .line 154
    :catch_0
    move-exception v1

    .line 155
    .local v1, "e":Lorg/json/JSONException;
    :try_start_2
    const-string v5, "SBrowserSavedPagesBuilder"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unable to Parse;"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 158
    iget-object v5, p0, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserSavedPagesBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v5, v9}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    move v5, v6

    goto :goto_0

    .end local v1    # "e":Lorg/json/JSONException;
    :catchall_0
    move-exception v5

    iget-object v6, p0, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserSavedPagesBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v6, v9}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    throw v5

    .line 166
    .restart local v2    # "favIcon":Ljava/lang/String;
    .restart local v3    # "pathUrl":Ljava/lang/String;
    :catch_1
    move-exception v1

    .line 167
    .local v1, "e":Landroid/os/RemoteException;
    const-string v5, "SBrowserSavedPagesBuilder"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "RemoteException"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v5, v6

    .line 168
    goto :goto_0
.end method

.method public parse(Landroid/database/Cursor;JLjava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "rowId"    # J
    .param p4, "key"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 74
    const/4 v3, 0x0

    .line 75
    .local v3, "sBrowserSavedPagesString":Ljava/lang/String;
    if-nez p1, :cond_0

    .line 94
    :goto_0
    return-object v4

    .line 77
    :cond_0
    iget-object v5, p0, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserSavedPagesBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    sget-object v6, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserSavedPagesBuilder;->SBROWSERSAVEDPAGES_COLUMNS:[Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 80
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserSavedPagesBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v5, p1}, Lcom/sec/android/sCloudSync/Records/JSONParser;->toJSON(Landroid/database/Cursor;)Lorg/json/JSONObject;

    move-result-object v5

    invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 84
    iget-object v5, p0, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserSavedPagesBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v5, v4}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 87
    :goto_1
    const-string v5, "sync2"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 88
    .local v1, "isUploaded":Ljava/lang/String;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 89
    :cond_1
    const-string v4, "path"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 90
    .local v2, "pathUrl":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserSavedPagesBuilder;->mSavedPagesUploadList:Landroid/util/SparseArray;

    long-to-int v5, p2

    invoke-virtual {v4, v5, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .end local v2    # "pathUrl":Ljava/lang/String;
    :goto_2
    move-object v4, v3

    .line 94
    goto :goto_0

    .line 81
    .end local v1    # "isUploaded":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 82
    .local v0, "e":Lorg/json/JSONException;
    :try_start_1
    const-string v5, "SBrowserSavedPagesBuilder"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "parse():Exception in parsing"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 84
    iget-object v5, p0, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserSavedPagesBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v5, v4}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    goto :goto_1

    .end local v0    # "e":Lorg/json/JSONException;
    :catchall_0
    move-exception v5

    iget-object v6, p0, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserSavedPagesBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v6, v4}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    throw v5

    .line 92
    .restart local v1    # "isUploaded":Ljava/lang/String;
    :cond_2
    iget-object v5, p0, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserSavedPagesBuilder;->mSavedPagesUploadList:Landroid/util/SparseArray;

    long-to-int v6, p2

    invoke-virtual {v5, v6, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_2
.end method

.method public update(Ljava/lang/String;JJLjava/lang/String;)Z
    .locals 10
    .param p1, "jsonString"    # Ljava/lang/String;
    .param p2, "timeStamp"    # J
    .param p4, "rowId"    # J
    .param p6, "Key"    # Ljava/lang/String;

    .prologue
    .line 101
    const/4 v3, 0x0

    .line 102
    .local v3, "sBrowserSavedPagesValues":Landroid/content/ContentValues;
    iget-object v4, p0, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserSavedPagesBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    sget-object v5, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserSavedPagesBuilder;->SBROWSERSAVEDPAGES_COLUMNS:[Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 105
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserSavedPagesBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v4, p1}, Lcom/sec/android/sCloudSync/Records/JSONParser;->fromJSONString(Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v3

    .line 106
    const-string v4, "favicon"

    invoke-virtual {v3, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 107
    .local v2, "favIcon":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 110
    const/4 v4, 0x0

    invoke-static {v2, v4}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    .line 111
    .local v0, "buffer":[B
    const-string v4, "favicon"

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 113
    .end local v0    # "buffer":[B
    :cond_0
    const-string v4, "sync5"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 114
    const-string v4, "is_dirty"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 115
    const-string v4, "is_deleted"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 121
    iget-object v4, p0, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserSavedPagesBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 125
    :try_start_1
    iget-object v4, p0, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserSavedPagesBuilder;->mProvider:Landroid/content/ContentProviderClient;

    sget-object v5, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserSavedPagesBuilder;->SBROWSERSAVEDPAGES_URI:Landroid/net/Uri;

    const-string v6, "_id = ? "

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    invoke-static {p4, p5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentProviderClient;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 131
    const/4 v4, 0x1

    .end local v2    # "favIcon":Ljava/lang/String;
    :goto_0
    return v4

    .line 117
    :catch_0
    move-exception v1

    .line 118
    .local v1, "e":Lorg/json/JSONException;
    :try_start_2
    const-string v4, "SBrowserSavedPagesBuilder"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unable to Parse;"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 119
    const/4 v4, 0x0

    .line 121
    iget-object v5, p0, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserSavedPagesBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    goto :goto_0

    .end local v1    # "e":Lorg/json/JSONException;
    :catchall_0
    move-exception v4

    iget-object v5, p0, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserSavedPagesBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    throw v4

    .line 127
    .restart local v2    # "favIcon":Ljava/lang/String;
    :catch_1
    move-exception v1

    .line 128
    .local v1, "e":Landroid/os/RemoteException;
    const-string v4, "SBrowserSavedPagesBuilder"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "RemoteException"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    const/4 v4, 0x0

    goto :goto_0
.end method
