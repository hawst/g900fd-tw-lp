.class public interface abstract Lcom/sec/android/sCloudSync/Constants/SNote3Contract;
.super Ljava/lang/Object;
.source "SNote3Contract.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/sCloudSync/Constants/SNote3Contract$FileDetail;,
        Lcom/sec/android/sCloudSync/Constants/SNote3Contract$Files;
    }
.end annotation


# static fields
.field public static final APP_INFO_URI:Landroid/net/Uri;

.field public static final AUTHORITY:Ljava/lang/String; = "com.samsung.android.snoteprovider"

.field public static final CALLER_IS_SYNCADAPTER:Ljava/lang/String; = "caller_is_syncadapter"

.field public static final CREATESNB_CONTENT_URI:Landroid/net/Uri;

.field public static final DELETEALLSNB_CONTENT_URI:Landroid/net/Uri;

.field public static final DELETEDETAIL_CONTENT_URI:Landroid/net/Uri;

.field public static final DELETESNB_CONTENT_URI:Landroid/net/Uri;

.field public static final EXTRACTSNB_CONTENT_URI:Landroid/net/Uri;

.field public static final FILEDETAIL_CONTENT_URI:Landroid/net/Uri;

.field public static final FILE_CONTENT_URI:Landroid/net/Uri;

.field public static final SNOTE_SYNC_STATE_URI:Landroid/net/Uri;

.field public static final SYNC_SETTING_URI:Landroid/net/Uri;

.field public static final SYNC_STATE_DATA:Ljava/lang/String; = "data"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-string v0, "content://com.samsung.android.snoteprovider/fileMgr"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/sCloudSync/Constants/SNote3Contract;->FILE_CONTENT_URI:Landroid/net/Uri;

    .line 28
    const-string v0, "content://com.samsung.android.snoteprovider/appInfo"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/sCloudSync/Constants/SNote3Contract;->APP_INFO_URI:Landroid/net/Uri;

    .line 30
    const-string v0, "content://com.samsung.android.snoteprovider/fileMgrDetail"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/sCloudSync/Constants/SNote3Contract;->FILEDETAIL_CONTENT_URI:Landroid/net/Uri;

    .line 31
    const-string v0, "content://com.samsung.android.snoteprovider/openSPD"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/sCloudSync/Constants/SNote3Contract;->EXTRACTSNB_CONTENT_URI:Landroid/net/Uri;

    .line 32
    const-string v0, "content://com.samsung.android.snoteprovider/deleteTMP"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/sCloudSync/Constants/SNote3Contract;->DELETESNB_CONTENT_URI:Landroid/net/Uri;

    .line 33
    const-string v0, "content://com.samsung.android.snoteprovider/createSPD"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/sCloudSync/Constants/SNote3Contract;->CREATESNB_CONTENT_URI:Landroid/net/Uri;

    .line 34
    const-string v0, "content://com.samsung.android.snoteprovider/deleteDETAIL"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/sCloudSync/Constants/SNote3Contract;->DELETEDETAIL_CONTENT_URI:Landroid/net/Uri;

    .line 35
    const-string v0, "content://com.samsung.android.snoteprovider/deleteTMPALL"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/sCloudSync/Constants/SNote3Contract;->DELETEALLSNB_CONTENT_URI:Landroid/net/Uri;

    .line 36
    const-string v0, "content://com.samsung.android.snoteprovider/syncstate"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/sCloudSync/Constants/SNote3Contract;->SNOTE_SYNC_STATE_URI:Landroid/net/Uri;

    .line 40
    const-string v0, "content://com.samsung.android.snoteprovider/syncSetting"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/sCloudSync/Constants/SNote3Contract;->SYNC_SETTING_URI:Landroid/net/Uri;

    return-void
.end method
