.class public interface abstract Lcom/sec/android/sCloudSync/Constants/SMemoContract$PenMemo;
.super Ljava/lang/Object;
.source "SMemoContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/sCloudSync/Constants/SMemoContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "PenMemo"
.end annotation


# static fields
.field public static final KEY_ACCOUNT_NAME:Ljava/lang/String; = "account_name"

.field public static final KEY_ACCOUNT_TYPE:Ljava/lang/String; = "account_type"

.field public static final KEY_BLOB_DATA:Ljava/lang/String; = "PenMemo_blobData"

.field public static final KEY_COMPONENT_NAME:Ljava/lang/String; = "PenMemo_ComponentName"

.field public static final KEY_CONTENT:Ljava/lang/String; = "Content"

.field public static final KEY_CREATE_DATE:Ljava/lang/String; = "CreateDate"

.field public static final KEY_DATE:Ljava/lang/String; = "Date"

.field public static final KEY_DELETED:Ljava/lang/String; = "deleted"

.field public static final KEY_DIRTY:Ljava/lang/String; = "dirty"

.field public static final KEY_DOUBLE_DATA:Ljava/lang/String; = "PenMemo_doubleData"

.field public static final KEY_DRAWING:Ljava/lang/String; = "Drawing"

.field public static final KEY_DUMMY:Ljava/lang/String; = "Dummy"

.field public static final KEY_HASVOICE:Ljava/lang/String; = "HasVoice"

.field public static final KEY_ID:Ljava/lang/String; = "_id"

.field public static final KEY_ISFAVORITE:Ljava/lang/String; = "IsFavorite"

.field public static final KEY_ISLOCK:Ljava/lang/String; = "IsLock"

.field public static final KEY_ISTEMP:Ljava/lang/String; = "PenMemo_isTemp"

.field public static final KEY_ISTEMP_MINI:Ljava/lang/String; = "PenMemo_isMiniTemp"

.field public static final KEY_IS_FOLDER:Ljava/lang/String; = "IsFolder"

.field public static final KEY_LAST_MODE:Ljava/lang/String; = "LastMode"

.field public static final KEY_LINKED_MEMO:Ljava/lang/String; = "LinkedMemo"

.field public static final KEY_LINK_TYPE:Ljava/lang/String; = "PenMemo_Type"

.field public static final KEY_LONG_DATA1:Ljava/lang/String; = "PenMemo_longData1"

.field public static final KEY_LONG_DATA2:Ljava/lang/String; = "PenMemo_longData2"

.field public static final KEY_LONG_DATA3:Ljava/lang/String; = "PenMemo_longData3"

.field public static final KEY_LONG_DATA4:Ljava/lang/String; = "PenMemo_longData4"

.field public static final KEY_PARENT_ID:Ljava/lang/String; = "ParentID"

.field public static final KEY_PHONE_NUM:Ljava/lang/String; = "PhoneNum"

.field public static final KEY_PILE_ORDER:Ljava/lang/String; = "PileOrder"

.field public static final KEY_SORTABLE_TITLE:Ljava/lang/String; = "_SortableTitle"

.field public static final KEY_SWICHER_IMAGE:Ljava/lang/String; = "SwitcherImage"

.field public static final KEY_SWICHER_TITLE_IMAGE:Ljava/lang/String; = "SwitcherTitleImage"

.field public static final KEY_SYNC1:Ljava/lang/String; = "sync1"

.field public static final KEY_SYNC2:Ljava/lang/String; = "sync2"

.field public static final KEY_SYNC3:Ljava/lang/String; = "sync3"

.field public static final KEY_SYNC4:Ljava/lang/String; = "sync4"

.field public static final KEY_SYNC_ID:Ljava/lang/String; = "Sync"

.field public static final KEY_TAG:Ljava/lang/String; = "Tag"

.field public static final KEY_TEXT:Ljava/lang/String; = "Text"

.field public static final KEY_TEXT_DATA1:Ljava/lang/String; = "PenMemo_textData1"

.field public static final KEY_TEXT_DATA2:Ljava/lang/String; = "PenMemo_textData2"

.field public static final KEY_TEXT_DATA3:Ljava/lang/String; = "PenMemo_textData3"

.field public static final KEY_TEXT_DATA4:Ljava/lang/String; = "PenMemo_textData4"

.field public static final KEY_THEME:Ljava/lang/String; = "Tehme"

.field public static final KEY_THUMB:Ljava/lang/String; = "Thumb"

.field public static final KEY_TITLE:Ljava/lang/String; = "Title"

.field public static final KEY_USER_THEME_PATH:Ljava/lang/String; = "UserThemePath"

.field public static final KEY_VISIBLE_TITLE:Ljava/lang/String; = "_Title"
