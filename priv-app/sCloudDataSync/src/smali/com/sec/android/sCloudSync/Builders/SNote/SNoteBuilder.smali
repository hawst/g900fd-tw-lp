.class public Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;
.super Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;
.source "SNoteBuilder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder$PostOperationListener;
    }
.end annotation


# static fields
.field private static final CALLER_IS_SYNCADAPTER:Ljava/lang/String; = "caller_is_syncadapter"

.field private static final DETAIL_COLUMNS:[Ljava/lang/String;

.field private static final FILEDETAIL:Ljava/lang/String; = "FILEDETAIL"

.field private static final FILENAME:Ljava/lang/String; = "filename"

.field private static final FILES:Ljava/lang/String; = "FILES"

.field private static final FILE_COLUMNS_DOWN:[Ljava/lang/String;

.field private static final PRIVATEKEY:Ljava/lang/String; = "privatekey"

.field static final TAG:Ljava/lang/String; = "SNoteBuilder"


# instance fields
.field private mAdapterName:Ljava/lang/String;

.field private mContract:Lcom/sec/android/sCloudSync/Constants/SNoteContract;

.field private final mFileDownloadList:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mFileUploadList:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mJsonString:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mOperations:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;"
        }
    .end annotation
.end field

.field private mSNBFilePath:Ljava/lang/String;

.field private final mSNBFilePathDownloadList:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mServerFileDeleteList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 72
    const/16 v0, 0x16

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "path"

    aput-object v1, v0, v3

    const-string v1, "name"

    aput-object v1, v0, v4

    const-string v1, "syncpath"

    aput-object v1, v0, v5

    const-string v1, "syncname"

    aput-object v1, v0, v6

    const/4 v1, 0x4

    const-string v2, "GoogleId"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "EvernoteId"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "ModifiedTime"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "FileSize"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "IsLocked"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "HasFavorites"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "HasVoiceRecord"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "HasTag"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "TemplateType"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "CoverType"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "FolderPath"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "IsFolder"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "ChildFolderCount"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "InnerNoteCount"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "account_name"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "account_type"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "content"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "Tag_Content"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->FILE_COLUMNS_DOWN:[Ljava/lang/String;

    .line 118
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "checksum"

    aput-object v1, v0, v3

    const-string v1, "path"

    aput-object v1, v0, v4

    const-string v1, "snb_id"

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->DETAIL_COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentProviderClient;Landroid/accounts/Account;Landroid/content/Context;)V
    .locals 3
    .param p1, "provider"    # Landroid/content/ContentProviderClient;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 125
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;-><init>(Landroid/content/ContentProviderClient;Landroid/accounts/Account;Landroid/content/Context;)V

    .line 63
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mFileUploadList:Ljava/util/Map;

    .line 64
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mSNBFilePath:Ljava/lang/String;

    .line 65
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mSNBFilePathDownloadList:Ljava/util/Map;

    .line 66
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mServerFileDeleteList:Ljava/util/List;

    .line 67
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mFileDownloadList:Ljava/util/Map;

    .line 68
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mOperations:Ljava/util/ArrayList;

    .line 69
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mJsonString:Ljava/util/Map;

    .line 126
    invoke-static {p3}, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->getInstance(Landroid/content/Context;)Lcom/sec/android/sCloudSync/Constants/SNoteContract;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mContract:Lcom/sec/android/sCloudSync/Constants/SNoteContract;

    .line 128
    const-string v1, "SNOTE"

    iput-object v1, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mAdapterName:Ljava/lang/String;

    .line 130
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mContract:Lcom/sec/android/sCloudSync/Constants/SNoteContract;

    iget-object v1, v1, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->APP_INFO_URI:Landroid/net/Uri;

    invoke-static {p1, v1}, Lcom/sec/android/sCloudSync/Tools/AppTool;->getSupportedApplication(Landroid/content/ContentProviderClient;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mAdapterName:Ljava/lang/String;
    :try_end_0
    .catch Lcom/sec/android/sCloudSync/Util/SyncException; {:try_start_0 .. :try_end_0} :catch_0

    .line 135
    :goto_0
    return-void

    .line 131
    :catch_0
    move-exception v0

    .line 132
    .local v0, "e":Lcom/sec/android/sCloudSync/Util/SyncException;
    invoke-virtual {v0}, Lcom/sec/android/sCloudSync/Util/SyncException;->printStackTrace()V

    .line 133
    const-string v1, "SNoteBuilder"

    const-string v2, "There is no SNote Provider INFO."

    invoke-static {v1, v2}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;)Lcom/sec/android/sCloudSync/Constants/SNoteContract;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mContract:Lcom/sec/android/sCloudSync/Constants/SNoteContract;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mOperations:Ljava/util/ArrayList;

    return-object v0
.end method

.method private convertJsonOld2New(Landroid/content/ContentValues;)V
    .locals 3
    .param p1, "sNoteValues"    # Landroid/content/ContentValues;

    .prologue
    .line 427
    const-string v2, "syncname"

    invoke-virtual {p1, v2}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 428
    const-string v2, "name"

    invoke-virtual {p1, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 429
    .local v0, "syncName":Ljava/lang/String;
    const-string v2, "path"

    invoke-virtual {p1, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 430
    .local v1, "syncPath":Ljava/lang/String;
    const-string v2, "name"

    invoke-virtual {p1, v2}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 431
    const-string v2, "path"

    invoke-virtual {p1, v2}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 432
    const-string v2, "syncname"

    invoke-virtual {p1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 433
    const-string v2, "syncpath"

    invoke-virtual {p1, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    .end local v0    # "syncName":Ljava/lang/String;
    .end local v1    # "syncPath":Ljava/lang/String;
    :goto_0
    return-void

    .line 435
    :cond_0
    const-string v2, "name"

    invoke-virtual {p1, v2}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 436
    const-string v2, "path"

    invoke-virtual {p1, v2}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private createFileDetailValues(Lorg/json/JSONObject;Ljava/util/ArrayList;Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder$PostOperationListener;)Landroid/content/ContentValues;
    .locals 9
    .param p1, "jsonObject"    # Lorg/json/JSONObject;
    .param p3, "postOperationListener"    # Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder$PostOperationListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder$PostOperationListener;",
            ")",
            "Landroid/content/ContentValues;"
        }
    .end annotation

    .prologue
    .local p2, "downloadFileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v6, 0x0

    .line 462
    const/4 v3, 0x0

    .line 464
    .local v3, "fileDetailValues":Landroid/content/ContentValues;
    :try_start_0
    const-string v7, "FILEDETAIL"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 465
    .local v1, "fileDetailList":Lorg/json/JSONArray;
    iget-object v7, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    sget-object v8, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->DETAIL_COLUMNS:[Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 466
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v5, v7, :cond_1

    .line 467
    invoke-virtual {v1, v5}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 468
    .local v2, "fileDetailObj":Lorg/json/JSONObject;
    iget-object v7, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v7, v2}, Lcom/sec/android/sCloudSync/Records/JSONParser;->fromJSON(Lorg/json/JSONObject;)Landroid/content/ContentValues;

    move-result-object v3

    .line 469
    const-string v7, "path"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 470
    .local v4, "filePath":Ljava/lang/String;
    if-eqz v4, :cond_0

    const-string v7, ""

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 471
    invoke-virtual {p2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 473
    :cond_0
    const-string v7, "dirty"

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v3, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 474
    const-string v7, "deleted"

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v3, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 475
    invoke-interface {p3, v3}, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder$PostOperationListener;->doPost(Landroid/content/ContentValues;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 466
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 482
    .end local v2    # "fileDetailObj":Lorg/json/JSONObject;
    .end local v4    # "filePath":Ljava/lang/String;
    :cond_1
    iget-object v7, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v7, v6}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    move-object v6, v3

    .line 484
    .end local v1    # "fileDetailList":Lorg/json/JSONArray;
    .end local v5    # "i":I
    :goto_1
    return-object v6

    .line 477
    :catch_0
    move-exception v0

    .line 478
    .local v0, "e":Lorg/json/JSONException;
    :try_start_1
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 479
    const-string v7, "SNoteBuilder"

    const-string v8, "Unable to create File Detail Values"

    invoke-static {v7, v8}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 482
    iget-object v7, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v7, v6}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    goto :goto_1

    .end local v0    # "e":Lorg/json/JSONException;
    :catchall_0
    move-exception v7

    iget-object v8, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v8, v6}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    throw v7
.end method

.method private createFileTableValues(Lorg/json/JSONObject;Ljava/lang/String;J)Landroid/content/ContentValues;
    .locals 8
    .param p1, "jsonObject"    # Lorg/json/JSONObject;
    .param p2, "syncKey"    # Ljava/lang/String;
    .param p3, "timeStamp"    # J

    .prologue
    const/4 v7, 0x0

    const/4 v4, 0x0

    .line 395
    const/4 v1, 0x0

    .line 396
    .local v1, "sNoteJson":Lorg/json/JSONObject;
    const/4 v2, 0x0

    .line 399
    .local v2, "sNoteValues":Landroid/content/ContentValues;
    iget-object v5, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    sget-object v6, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->FILE_COLUMNS_DOWN:[Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 401
    :try_start_0
    const-string v5, "FILES"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 402
    iget-object v5, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v5, v1}, Lcom/sec/android/sCloudSync/Records/JSONParser;->fromJSON(Lorg/json/JSONObject;)Landroid/content/ContentValues;

    move-result-object v2

    .line 404
    invoke-direct {p0, v2}, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->convertJsonOld2New(Landroid/content/ContentValues;)V

    .line 406
    const-string v5, "syncpath"

    invoke-virtual {v2, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 407
    .local v3, "snbFilePath":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mSNBFilePathDownloadList:Ljava/util/Map;

    invoke-interface {v5, p2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 412
    iget-object v5, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v5, v4}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 415
    const-string v4, "sync1"

    invoke-virtual {v2, v4, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    const-string v4, "sync2"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 417
    const-string v4, "dirty"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 418
    const-string v4, "deleted"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    move-object v4, v2

    .line 421
    .end local v3    # "snbFilePath":Ljava/lang/String;
    :goto_0
    return-object v4

    .line 408
    :catch_0
    move-exception v0

    .line 409
    .local v0, "e":Lorg/json/JSONException;
    :try_start_1
    const-string v5, "SNoteBuilder"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unable to parse:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 412
    iget-object v5, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v5, v4}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    goto :goto_0

    .end local v0    # "e":Lorg/json/JSONException;
    :catchall_0
    move-exception v5

    iget-object v6, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v6, v4}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    throw v5
.end method

.method private insertFileDetailTable(Lorg/json/JSONObject;Ljava/util/ArrayList;Ljava/util/ArrayList;)Z
    .locals 1
    .param p1, "jsonObject"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 443
    .local p2, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .local p3, "downloadFileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder$1;

    invoke-direct {v0, p0, p2}, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder$1;-><init>(Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;Ljava/util/ArrayList;)V

    invoke-direct {p0, p1, p3, v0}, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->createFileDetailValues(Lorg/json/JSONObject;Ljava/util/ArrayList;Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder$PostOperationListener;)Landroid/content/ContentValues;

    .line 453
    const/4 v0, 0x1

    return v0
.end method

.method private insertFileTable(Lorg/json/JSONObject;Ljava/lang/String;JLjava/util/ArrayList;)Z
    .locals 6
    .param p1, "sNoteJson"    # Lorg/json/JSONObject;
    .param p2, "Key"    # Ljava/lang/String;
    .param p3, "timestamp"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            "Ljava/lang/String;",
            "J",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p5, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    const/4 v2, 0x1

    .line 334
    iget-object v3, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mContract:Lcom/sec/android/sCloudSync/Constants/SNoteContract;

    iget-object v3, v3, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->FILE_CONTENT_URI:Landroid/net/Uri;

    const-string v4, "caller_is_syncadapter"

    invoke-static {v3, v4}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "account_name"

    iget-object v5, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mAccount:Landroid/accounts/Account;

    iget-object v5, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "account_type"

    iget-object v5, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mAccount:Landroid/accounts/Account;

    iget-object v5, v5, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "privatekey"

    invoke-virtual {v3, v4, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 339
    .local v0, "sNoteUri":Landroid/net/Uri;
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->createFileTableValues(Lorg/json/JSONObject;Ljava/lang/String;J)Landroid/content/ContentValues;

    move-result-object v1

    .line 341
    .local v1, "sNoteValues":Landroid/content/ContentValues;
    if-nez v1, :cond_0

    .line 342
    const/4 v2, 0x0

    .line 367
    :goto_0
    return v2

    .line 343
    :cond_0
    iget-object v3, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mAdapterName:Ljava/lang/String;

    const-string v4, "SNOTE"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 344
    const-string v3, "content"

    invoke-virtual {v1, v3}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 345
    const-string v3, "Tag_Content"

    invoke-virtual {v1, v3}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 350
    :cond_1
    const-string v3, "deleted"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 353
    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {p5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private parseFileDetailsTable(JLorg/json/JSONObject;Ljava/util/HashMap;Ljava/util/ArrayList;)Z
    .locals 16
    .param p1, "rowId"    # J
    .param p3, "sNoteJson"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lorg/json/JSONObject;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 255
    .local p4, "fileUploadList":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .local p5, "fileDeleteList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v9, 0x0

    .line 256
    .local v9, "cursor":Landroid/database/Cursor;
    new-instance v13, Lorg/json/JSONArray;

    invoke-direct {v13}, Lorg/json/JSONArray;-><init>()V

    .line 259
    .local v13, "fileDetailJsonArr":Lorg/json/JSONArray;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mProvider:Landroid/content/ContentProviderClient;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mContract:Lcom/sec/android/sCloudSync/Constants/SNoteContract;

    iget-object v3, v3, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->FILEDETAIL_CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "snb_id = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, p1

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v9

    .line 266
    if-eqz v9, :cond_0

    .line 268
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    sget-object v3, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->DETAIL_COLUMNS:[Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 269
    const/4 v14, 0x0

    .line 271
    .local v14, "fileDetailObject":Lorg/json/JSONObject;
    :goto_0
    :try_start_1
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 272
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v2, v9}, Lcom/sec/android/sCloudSync/Records/JSONParser;->toJSON(Landroid/database/Cursor;)Lorg/json/JSONObject;

    move-result-object v14

    .line 273
    invoke-virtual {v14}, Lorg/json/JSONObject;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 274
    const-string v2, "SNoteBuilder"

    const-string v3, "FileDetailsTable: Unable to parse "

    invoke-static {v2, v3}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 293
    :catch_0
    move-exception v12

    .line 294
    .local v12, "e":Lorg/json/JSONException;
    :try_start_2
    const-string v2, "SNoteBuilder"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unable to parse:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v12}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 296
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 299
    .end local v12    # "e":Lorg/json/JSONException;
    .end local v14    # "fileDetailObject":Lorg/json/JSONObject;
    :cond_0
    :goto_1
    const/4 v2, 0x1

    :goto_2
    return v2

    .line 261
    :catch_1
    move-exception v12

    .line 262
    .local v12, "e":Landroid/os/RemoteException;
    const-string v2, "SNoteBuilder"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Parse(): Remote Exception:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v12}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    const/4 v2, 0x0

    goto :goto_2

    .line 276
    .end local v12    # "e":Landroid/os/RemoteException;
    .restart local v14    # "fileDetailObject":Lorg/json/JSONObject;
    :cond_1
    :try_start_3
    const-string v2, "dirty"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 277
    .local v11, "dirtyBit":I
    const-string v2, "deleted"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 278
    .local v10, "deletedBit":I
    const/4 v2, 0x1

    if-ne v11, v2, :cond_3

    .line 279
    const-string v2, "path"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 280
    .local v15, "filepath":Ljava/lang/String;
    const/4 v2, 0x1

    if-ne v10, v2, :cond_2

    .line 281
    move-object/from16 v0, p5

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 296
    .end local v10    # "deletedBit":I
    .end local v11    # "dirtyBit":I
    .end local v15    # "filepath":Ljava/lang/String;
    :catchall_0
    move-exception v2

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v2

    .line 285
    .restart local v10    # "deletedBit":I
    .restart local v11    # "dirtyBit":I
    .restart local v15    # "filepath":Ljava/lang/String;
    :cond_2
    :try_start_4
    const-string v2, "checksum"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 286
    .local v8, "checkSum":Ljava/lang/String;
    move-object/from16 v0, p4

    invoke-virtual {v0, v15, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 289
    .end local v8    # "checkSum":Ljava/lang/String;
    .end local v15    # "filepath":Ljava/lang/String;
    :cond_3
    invoke-virtual {v13, v14}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto/16 :goto_0

    .line 292
    .end local v10    # "deletedBit":I
    .end local v11    # "dirtyBit":I
    :cond_4
    const-string v2, "FILEDETAIL"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 296
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_1
.end method

.method private parseFileTable(Landroid/database/Cursor;Lorg/json/JSONObject;J)Z
    .locals 7
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "sNoteJson"    # Lorg/json/JSONObject;
    .param p3, "rowId"    # J

    .prologue
    const/4 v6, 0x0

    .line 228
    const/4 v1, 0x0

    .line 229
    .local v1, "fileTableJson":Lorg/json/JSONObject;
    const/4 v2, 0x0

    .line 230
    .local v2, "snbFilePath":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    sget-object v4, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->FILE_COLUMNS_DOWN:[Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 233
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v3, p1}, Lcom/sec/android/sCloudSync/Records/JSONParser;->toJSON(Landroid/database/Cursor;)Lorg/json/JSONObject;

    move-result-object v1

    .line 235
    const-string v3, "syncpath"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 236
    const-string v3, "syncpath"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 240
    :goto_0
    invoke-direct {p0, v2}, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->setSNBFilePath(Ljava/lang/String;)V

    .line 242
    const-string v3, "FILES"

    invoke-virtual {p2, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 247
    iget-object v3, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v3, v6}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 249
    const/4 v3, 0x1

    :goto_1
    return v3

    .line 238
    :cond_0
    :try_start_1
    const-string v3, "path"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    goto :goto_0

    .line 243
    :catch_0
    move-exception v0

    .line 244
    .local v0, "e":Lorg/json/JSONException;
    :try_start_2
    const-string v3, "SNoteBuilder"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unable to parse sNote Files Table:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 245
    const/4 v3, 0x0

    .line 247
    iget-object v4, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v4, v6}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    goto :goto_1

    .end local v0    # "e":Lorg/json/JSONException;
    :catchall_0
    move-exception v3

    iget-object v4, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v4, v6}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    throw v3
.end method

.method private setFileUploadList(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 154
    .local p1, "fileUploadList":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mFileUploadList:Ljava/util/Map;

    .line 155
    return-void
.end method

.method private setSNBFilePath(Ljava/lang/String;)V
    .locals 0
    .param p1, "snbFilePath"    # Ljava/lang/String;

    .prologue
    .line 174
    iput-object p1, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mSNBFilePath:Ljava/lang/String;

    .line 175
    return-void
.end method

.method private setServerFileDeleteList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 164
    .local p1, "serverFileDeleteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mServerFileDeleteList:Ljava/util/List;

    .line 165
    return-void
.end method

.method private updateFileDetailTable(Lorg/json/JSONObject;JLjava/util/ArrayList;)Z
    .locals 1
    .param p1, "jsonObject"    # Lorg/json/JSONObject;
    .param p2, "rowId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            "J",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 569
    .local p4, "downloadFileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder$2;

    invoke-direct {v0, p0, p2, p3}, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder$2;-><init>(Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;J)V

    invoke-direct {p0, p1, p4, v0}, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->createFileDetailValues(Lorg/json/JSONObject;Ljava/util/ArrayList;Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder$PostOperationListener;)Landroid/content/ContentValues;

    .line 579
    const/4 v0, 0x1

    return v0
.end method

.method private updateFileTable(Lorg/json/JSONObject;Ljava/lang/String;JJ)Z
    .locals 17
    .param p1, "jsonObject"    # Lorg/json/JSONObject;
    .param p2, "syncKey"    # Ljava/lang/String;
    .param p3, "timeStamp"    # J
    .param p5, "rowId"    # J

    .prologue
    .line 515
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mContract:Lcom/sec/android/sCloudSync/Constants/SNoteContract;

    iget-object v2, v2, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->FILE_CONTENT_URI:Landroid/net/Uri;

    move-wide/from16 v0, p5

    invoke-static {v2, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 516
    .local v3, "sNoteUri":Landroid/net/Uri;
    const-string v2, "caller_is_syncadapter"

    invoke-static {v3, v2}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "account_name"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mAccount:Landroid/accounts/Account;

    iget-object v5, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "account_type"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mAccount:Landroid/accounts/Account;

    iget-object v5, v5, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "privatekey"

    move-object/from16 v0, p2

    invoke-virtual {v2, v4, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 521
    invoke-direct/range {p0 .. p4}, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->createFileTableValues(Lorg/json/JSONObject;Ljava/lang/String;J)Landroid/content/ContentValues;

    move-result-object v16

    .line 523
    .local v16, "sNoteValues":Landroid/content/ContentValues;
    if-nez v16, :cond_0

    .line 524
    const/4 v2, 0x0

    .line 564
    :goto_0
    return v2

    .line 526
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mAdapterName:Ljava/lang/String;

    const-string v4, "SNOTE"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 527
    const-string v2, "content"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 528
    const-string v2, "Tag_Content"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 531
    :cond_1
    const-string v2, "IsFolder"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v4, 0x1

    if-ne v2, v4, :cond_3

    .line 533
    const/4 v8, 0x0

    .line 535
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mProvider:Landroid/content/ContentProviderClient;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "syncpath"

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    .line 542
    :goto_1
    if-eqz v8, :cond_3

    .line 543
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 544
    const-string v2, "syncpath"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 545
    .local v15, "oldFolderPath":Ljava/lang/String;
    const-string v2, "syncpath"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 546
    .local v13, "newFolderPath":Ljava/lang/String;
    invoke-virtual {v15, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 547
    new-instance v14, Ljava/io/File;

    invoke-direct {v14, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 548
    .local v14, "oldFolder":Ljava/io/File;
    new-instance v12, Ljava/io/File;

    invoke-direct {v12, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 549
    .local v12, "newFolder":Ljava/io/File;
    invoke-virtual {v14, v12}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v11

    .line 550
    .local v11, "isRenameSuccess":Z
    if-nez v11, :cond_2

    .line 551
    const-string v2, "SNoteBuilder"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Folder "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " could not be renamed"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 555
    .end local v11    # "isRenameSuccess":Z
    .end local v12    # "newFolder":Ljava/io/File;
    .end local v13    # "newFolderPath":Ljava/lang/String;
    .end local v14    # "oldFolder":Ljava/io/File;
    .end local v15    # "oldFolderPath":Ljava/lang/String;
    :cond_2
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 558
    .end local v8    # "cursor":Landroid/database/Cursor;
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mContract:Lcom/sec/android/sCloudSync/Constants/SNoteContract;

    iget-object v2, v2, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->FILEDETAIL_CONTENT_URI:Landroid/net/Uri;

    const-string v4, "caller_is_syncadapter"

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v10

    .line 560
    .local v10, "fileDetailUri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mOperations:Ljava/util/ArrayList;

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 561
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mOperations:Ljava/util/ArrayList;

    invoke-static {v10}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "snb_id="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, p5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 564
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 537
    .end local v10    # "fileDetailUri":Landroid/net/Uri;
    .restart local v8    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v9

    .line 538
    .local v9, "e":Landroid/os/RemoteException;
    invoke-virtual {v9}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_1
.end method


# virtual methods
.method public delete(Landroid/net/Uri;JLjava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "rowId"    # J
    .param p4, "selection"    # Ljava/lang/String;
    .param p5, "syncKey"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 590
    iget-object v2, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mContract:Lcom/sec/android/sCloudSync/Constants/SNoteContract;

    iget-object v2, v2, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->FILE_CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2, p2, p3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "caller_is_syncadapter"

    const-string v4, "true"

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "account_name"

    iget-object v4, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mAccount:Landroid/accounts/Account;

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "account_type"

    iget-object v4, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mAccount:Landroid/accounts/Account;

    iget-object v4, v4, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 595
    .local v1, "sNoteUri":Landroid/net/Uri;
    iget-object v2, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mContract:Lcom/sec/android/sCloudSync/Constants/SNoteContract;

    iget-object v2, v2, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->FILEDETAIL_CONTENT_URI:Landroid/net/Uri;

    const-string v3, "caller_is_syncadapter"

    invoke-static {v2, v3}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 598
    .local v0, "fileDetailUri":Landroid/net/Uri;
    iget-object v2, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mProvider:Landroid/content/ContentProviderClient;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "snb_id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3, v5}, Landroid/content/ContentProviderClient;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 599
    iget-object v2, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mProvider:Landroid/content/ContentProviderClient;

    invoke-virtual {v2, v1, v5, v5}, Landroid/content/ContentProviderClient;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 600
    return-void
.end method

.method public doApplyBatch()Z
    .locals 1

    .prologue
    .line 585
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mOperations:Ljava/util/ArrayList;

    invoke-virtual {p0, v0}, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->doApplyBatch(Ljava/util/ArrayList;)Z

    move-result v0

    return v0
.end method

.method public getFileDownloadList()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 139
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mFileDownloadList:Ljava/util/Map;

    return-object v0
.end method

.method public getFileUploadList()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 149
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mFileUploadList:Ljava/util/Map;

    return-object v0
.end method

.method public getOperations()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mOperations:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getSNBFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mSNBFilePath:Ljava/lang/String;

    return-object v0
.end method

.method public getSNBFilePathDownloadList()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 179
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mSNBFilePathDownloadList:Ljava/util/Map;

    return-object v0
.end method

.method public getServerFileDeleteList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 159
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mServerFileDeleteList:Ljava/util/List;

    return-object v0
.end method

.method public getjsonString()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 184
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mJsonString:Ljava/util/Map;

    return-object v0
.end method

.method public insert(Ljava/lang/String;Ljava/lang/String;J)Z
    .locals 10
    .param p1, "jsonString"    # Ljava/lang/String;
    .param p2, "Key"    # Ljava/lang/String;
    .param p3, "modifiedTime"    # J

    .prologue
    const/4 v9, 0x0

    .line 304
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 305
    .local v7, "downloadFileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 306
    .local v6, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    const/4 v2, 0x0

    .line 309
    .local v2, "sNoteJson":Lorg/json/JSONObject;
    :try_start_0
    new-instance v1, Lorg/json/JSONTokener;

    invoke-direct {v1, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lorg/json/JSONObject;

    move-object v2, v0
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, p0

    move-object v3, p2

    move-wide v4, p3

    .line 316
    invoke-direct/range {v1 .. v6}, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->insertFileTable(Lorg/json/JSONObject;Ljava/lang/String;JLjava/util/ArrayList;)Z

    move-result v1

    if-nez v1, :cond_0

    move v1, v9

    .line 329
    :goto_0
    return v1

    .line 310
    :catch_0
    move-exception v8

    .line 311
    .local v8, "e1":Lorg/json/JSONException;
    invoke-virtual {v8}, Lorg/json/JSONException;->printStackTrace()V

    move v1, v9

    .line 312
    goto :goto_0

    .line 320
    .end local v8    # "e1":Lorg/json/JSONException;
    :cond_0
    invoke-direct {p0, v2, v6, v7}, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->insertFileDetailTable(Lorg/json/JSONObject;Ljava/util/ArrayList;Ljava/util/ArrayList;)Z

    move-result v1

    if-nez v1, :cond_1

    move v1, v9

    .line 321
    goto :goto_0

    .line 327
    :cond_1
    iget-object v1, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mFileDownloadList:Ljava/util/Map;

    invoke-interface {v1, p2, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 328
    iget-object v1, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mJsonString:Ljava/util/Map;

    invoke-interface {v1, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 329
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public parse(Landroid/database/Cursor;JLjava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p1, "sNoteCursor"    # Landroid/database/Cursor;
    .param p2, "rowId"    # J
    .param p4, "key"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x0

    .line 190
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 191
    .local v4, "fileUploadList":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 192
    .local v5, "fileDeleteList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 195
    .local v3, "sNoteJson":Lorg/json/JSONObject;
    invoke-direct {p0, p1, v3, p2, p3}, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->parseFileTable(Landroid/database/Cursor;Lorg/json/JSONObject;J)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v10

    .line 223
    :goto_0
    return-object v0

    .line 199
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->getSNBFilePath()Ljava/lang/String;

    move-result-object v9

    .line 200
    .local v9, "snbFilePath":Ljava/lang/String;
    if-eqz v9, :cond_1

    .line 201
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mContract:Lcom/sec/android/sCloudSync/Constants/SNoteContract;

    iget-object v0, v0, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->EXTRACTSNB_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "filename"

    invoke-virtual {v0, v1, v9}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "privatekey"

    invoke-virtual {v0, v1, p4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v8

    .line 206
    .local v8, "extractSnbUri":Landroid/net/Uri;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mProvider:Landroid/content/ContentProviderClient;

    const/4 v1, 0x0

    invoke-virtual {v0, v8, v1}, Landroid/content/ContentProviderClient;->openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    .end local v8    # "extractSnbUri":Landroid/net/Uri;
    :cond_1
    move-object v0, p0

    move-wide v1, p2

    .line 217
    invoke-direct/range {v0 .. v5}, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->parseFileDetailsTable(JLorg/json/JSONObject;Ljava/util/HashMap;Ljava/util/ArrayList;)Z

    move-result v0

    if-nez v0, :cond_2

    move-object v0, v10

    .line 218
    goto :goto_0

    .line 207
    .restart local v8    # "extractSnbUri":Landroid/net/Uri;
    :catch_0
    move-exception v7

    .line 208
    .local v7, "e1":Ljava/io/FileNotFoundException;
    invoke-virtual {v7}, Ljava/io/FileNotFoundException;->printStackTrace()V

    move-object v0, v10

    .line 209
    goto :goto_0

    .line 210
    .end local v7    # "e1":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v6

    .line 211
    .local v6, "e":Landroid/os/RemoteException;
    invoke-virtual {v6}, Landroid/os/RemoteException;->printStackTrace()V

    move-object v0, v10

    .line 212
    goto :goto_0

    .line 220
    .end local v6    # "e":Landroid/os/RemoteException;
    .end local v8    # "extractSnbUri":Landroid/net/Uri;
    :cond_2
    invoke-direct {p0, v4}, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->setFileUploadList(Ljava/util/Map;)V

    .line 221
    invoke-direct {p0, v5}, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->setServerFileDeleteList(Ljava/util/List;)V

    .line 223
    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public update(Ljava/lang/String;JJLjava/lang/String;)Z
    .locals 10
    .param p1, "jsonString"    # Ljava/lang/String;
    .param p2, "timeStamp"    # J
    .param p4, "rowId"    # J
    .param p6, "syncKey"    # Ljava/lang/String;

    .prologue
    .line 489
    const/4 v2, 0x0

    .line 490
    .local v2, "sNoteJson":Lorg/json/JSONObject;
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 493
    .local v8, "downloadFileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :try_start_0
    new-instance v1, Lorg/json/JSONTokener;

    invoke-direct {v1, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lorg/json/JSONObject;

    move-object v2, v0
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, p0

    move-object/from16 v3, p6

    move-wide v4, p2

    move-wide v6, p4

    .line 500
    invoke-direct/range {v1 .. v7}, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->updateFileTable(Lorg/json/JSONObject;Ljava/lang/String;JJ)Z

    move-result v1

    if-nez v1, :cond_0

    .line 501
    const/4 v1, 0x0

    .line 510
    :goto_0
    return v1

    .line 494
    :catch_0
    move-exception v9

    .line 495
    .local v9, "e1":Lorg/json/JSONException;
    invoke-virtual {v9}, Lorg/json/JSONException;->printStackTrace()V

    .line 496
    const/4 v1, 0x0

    goto :goto_0

    .line 504
    .end local v9    # "e1":Lorg/json/JSONException;
    :cond_0
    invoke-direct {p0, v2, p4, p5, v8}, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->updateFileDetailTable(Lorg/json/JSONObject;JLjava/util/ArrayList;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 505
    const/4 v1, 0x0

    goto :goto_0

    .line 507
    :cond_1
    iget-object v1, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mFileDownloadList:Ljava/util/Map;

    move-object/from16 v0, p6

    invoke-interface {v1, v0, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 508
    iget-object v1, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mJsonString:Ljava/util/Map;

    move-object/from16 v0, p6

    invoke-interface {v1, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 510
    const/4 v1, 0x1

    goto :goto_0
.end method
