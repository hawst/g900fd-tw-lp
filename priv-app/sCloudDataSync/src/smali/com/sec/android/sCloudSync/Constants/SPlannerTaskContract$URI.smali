.class public interface abstract Lcom/sec/android/sCloudSync/Constants/SPlannerTaskContract$URI;
.super Ljava/lang/Object;
.source "SPlannerTaskContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/sCloudSync/Constants/SPlannerTaskContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "URI"
.end annotation


# static fields
.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final TASK:Landroid/net/Uri;

.field public static final TASKACCOUNT:Landroid/net/Uri;

.field public static final TASKGROUP:Landroid/net/Uri;

.field public static final TASKREMINDER:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 11
    const-string v0, "content://com.android.calendar"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/sCloudSync/Constants/SPlannerTaskContract$URI;->CONTENT_URI:Landroid/net/Uri;

    .line 12
    sget-object v0, Lcom/sec/android/sCloudSync/Constants/SPlannerTaskContract$URI;->CONTENT_URI:Landroid/net/Uri;

    const-string v1, "syncTasks"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/sCloudSync/Constants/SPlannerTaskContract$URI;->TASK:Landroid/net/Uri;

    .line 13
    sget-object v0, Lcom/sec/android/sCloudSync/Constants/SPlannerTaskContract$URI;->CONTENT_URI:Landroid/net/Uri;

    const-string v1, "TasksReminders"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/sCloudSync/Constants/SPlannerTaskContract$URI;->TASKREMINDER:Landroid/net/Uri;

    .line 14
    sget-object v0, Lcom/sec/android/sCloudSync/Constants/SPlannerTaskContract$URI;->CONTENT_URI:Landroid/net/Uri;

    const-string v1, "taskGroup"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/sCloudSync/Constants/SPlannerTaskContract$URI;->TASKGROUP:Landroid/net/Uri;

    .line 15
    sget-object v0, Lcom/sec/android/sCloudSync/Constants/SPlannerTaskContract$URI;->CONTENT_URI:Landroid/net/Uri;

    const-string v1, "TasksAccounts"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/sCloudSync/Constants/SPlannerTaskContract$URI;->TASKACCOUNT:Landroid/net/Uri;

    return-void
.end method
