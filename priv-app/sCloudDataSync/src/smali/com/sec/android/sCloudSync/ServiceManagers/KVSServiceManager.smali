.class public Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;
.super Lcom/sec/android/sCloudSync/ServiceManagers/AbstractServiceManager;
.source "KVSServiceManager.java"

# interfaces
.implements Lcom/sec/android/sCloudSync/ServiceManagers/IRecordServiceManager;


# static fields
.field private static final APP_ID:Ljava/lang/String; = "tj9u972o46"

.field private static final APP_ID_HEADER:Ljava/lang/String; = "x-sc-appid"

.field private static final DETAIL:Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "KVSServiceManager"

.field private static final USER_AGENT:Ljava/lang/String;


# instance fields
.field mKVSResponse:Lcom/sec/android/sCloudSync/Records/KVSResponse;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 49
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "; sw="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "android"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "DataSync"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "5.6.5"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->DETAIL:Ljava/lang/String;

    .line 51
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Landroid/os/Build;->DISPLAY:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->DETAIL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->USER_AGENT:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cid"    # Ljava/lang/String;
    .param p3, "cloud"    # Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    .prologue
    .line 57
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/sCloudSync/ServiceManagers/AbstractServiceManager;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;)V

    .line 52
    new-instance v0, Lcom/sec/android/sCloudSync/Records/KVSResponse;

    iget-object v1, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/sCloudSync/Records/KVSResponse;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mKVSResponse:Lcom/sec/android/sCloudSync/Records/KVSResponse;

    .line 58
    iget-object v0, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mNetClient:Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;

    invoke-virtual {v0, p0}, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->setServiceApiHelper(Lcom/sec/android/sCloudSync/ServiceManagers/IPDMServiceApiHelper;)V

    .line 59
    return-void
.end method


# virtual methods
.method public addHeaderForApi(Lorg/apache/http/client/methods/HttpUriRequest;)V
    .locals 2
    .param p1, "request"    # Lorg/apache/http/client/methods/HttpUriRequest;

    .prologue
    .line 261
    const-string v0, "x-sc-appid"

    const-string v1, "tj9u972o46"

    invoke-interface {p1, v0, v1}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    return-void
.end method

.method public deleteItems(Ljava/util/List;)Lcom/sec/android/sCloudSync/Records/KVSResponse;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/sCloudSync/Records/RecordBase;",
            ">;)",
            "Lcom/sec/android/sCloudSync/Records/KVSResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .local p1, "recordList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordBase;>;"
    const/16 v5, 0xd9

    .line 65
    if-nez p1, :cond_0

    .line 66
    const/4 v0, 0x0

    .line 99
    :goto_0
    return-object v0

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v0}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getAuthManager()Lcom/sec/android/sCloudSync/Auth/AuthManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/sCloudSync/Auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v6

    .line 69
    .local v6, "baseUrl":Ljava/lang/String;
    if-nez v6, :cond_1

    .line 71
    const-string v0, "KVSServiceManager"

    const-string v1, "There is NO Base URL."

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    new-instance v0, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v0

    .line 74
    :cond_1
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 75
    .local v12, "url":Ljava/lang/StringBuilder;
    const-string v0, "/kvs/?action=delete"

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    iget-object v0, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v0}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getAuthManager()Lcom/sec/android/sCloudSync/Auth/AuthManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mCid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/sCloudSync/Auth/AuthManager;->getPutApiParams(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    const-string v0, "ctid"

    iget-object v1, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v1}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getCtid()Ljava/lang/String;

    move-result-object v1

    invoke-static {v12, v0, v1}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addUriParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    const/4 v8, 0x0

    .line 81
    .local v8, "deleteobject":Lorg/json/JSONObject;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .line 82
    .local v10, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/sCloudSync/Records/RecordBase;>;"
    new-instance v7, Lorg/json/JSONArray;

    invoke-direct {v7}, Lorg/json/JSONArray;-><init>()V

    .line 84
    .local v7, "deletekeys":Lorg/json/JSONArray;
    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 85
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/sCloudSync/Records/RecordBase;

    .line 87
    .local v11, "recordItemObject":Lcom/sec/android/sCloudSync/Records/RecordBase;
    :try_start_0
    invoke-virtual {v11}, Lcom/sec/android/sCloudSync/Records/RecordBase;->toJSON()Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "deleteobject":Lorg/json/JSONObject;
    check-cast v8, Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 92
    .restart local v8    # "deleteobject":Lorg/json/JSONObject;
    invoke-virtual {v7, v8}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_1

    .line 88
    .end local v8    # "deleteobject":Lorg/json/JSONObject;
    :catch_0
    move-exception v9

    .line 89
    .local v9, "e":Lorg/json/JSONException;
    const-string v0, "KVSServiceManager"

    const-string v1, "JSON PARSER Exception"

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    new-instance v0, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v0

    .line 95
    .end local v9    # "e":Lorg/json/JSONException;
    .end local v11    # "recordItemObject":Lcom/sec/android/sCloudSync/Records/RecordBase;
    .restart local v8    # "deleteobject":Lorg/json/JSONObject;
    :cond_2
    iget-object v0, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v0, v5}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->setLastApi(I)V

    .line 97
    const/4 v1, 0x0

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mKVSResponse:Lcom/sec/android/sCloudSync/Records/KVSResponse;

    invoke-virtual {v7}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->handleRequest(ILjava/lang/String;Lcom/sec/android/sCloudSync/Records/AbstractJSON;Ljava/lang/String;I)Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;

    .line 99
    iget-object v0, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mKVSResponse:Lcom/sec/android/sCloudSync/Records/KVSResponse;

    goto/16 :goto_0
.end method

.method public getItems(Ljava/util/List;)Lcom/sec/android/sCloudSync/Records/KVSResponse;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/sec/android/sCloudSync/Records/KVSResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .local p1, "keyIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/16 v7, 0xd5

    .line 146
    iget-object v4, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v4}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getAuthManager()Lcom/sec/android/sCloudSync/Auth/AuthManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/sCloudSync/Auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v1

    .line 147
    .local v1, "baseUrl":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 149
    const-string v4, "KVSServiceManager"

    const-string v5, "There is NO Base URL."

    invoke-static {v4, v5}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    new-instance v4, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/4 v5, 0x3

    invoke-direct {v4, v5}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v4

    .line 152
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 153
    .local v3, "url":Ljava/lang/StringBuilder;
    const-string v4, "/kvs/item?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 154
    iget-object v4, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v4}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getAuthManager()Lcom/sec/android/sCloudSync/Auth/AuthManager;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mCid:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/sec/android/sCloudSync/Auth/AuthManager;->getGetApiParams(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    const-string v4, "ctid"

    iget-object v5, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v5}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getCtid()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addUriParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 159
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 160
    .local v0, "Key":Ljava/lang/String;
    const-string v4, "&key="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 161
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 164
    .end local v0    # "Key":Ljava/lang/String;
    :cond_1
    iget-object v4, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v4, v7}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->setLastApi(I)V

    .line 166
    const/4 v4, 0x1

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mKVSResponse:Lcom/sec/android/sCloudSync/Records/KVSResponse;

    invoke-virtual {p0, v4, v5, v6, v7}, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->handleRequest(ILjava/lang/String;Lcom/sec/android/sCloudSync/Records/AbstractJSON;I)Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;

    .line 167
    iget-object v4, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mKVSResponse:Lcom/sec/android/sCloudSync/Records/KVSResponse;

    return-object v4
.end method

.method public getKeys(Ljava/lang/String;I)Lcom/sec/android/sCloudSync/Records/KVSResponse;
    .locals 6
    .param p1, "startKey"    # Ljava/lang/String;
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    const/16 v5, 0xd3

    .line 172
    iget-object v2, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v2}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getAuthManager()Lcom/sec/android/sCloudSync/Auth/AuthManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/sCloudSync/Auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v0

    .line 173
    .local v0, "baseUrl":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 175
    const-string v2, "KVSServiceManager"

    const-string v3, "There is NO Base URL."

    invoke-static {v2, v3}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    new-instance v2, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/4 v3, 0x3

    invoke-direct {v2, v3}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v2

    .line 178
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 179
    .local v1, "url":Ljava/lang/StringBuilder;
    const-string v2, "/kvs/keys?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 180
    iget-object v2, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v2}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getAuthManager()Lcom/sec/android/sCloudSync/Auth/AuthManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mCid:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/android/sCloudSync/Auth/AuthManager;->getGetApiParams(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 181
    const-string v2, "&start="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 182
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 183
    const-string v2, "&count="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 184
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 186
    const-string v2, "ctid"

    iget-object v3, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v3}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getCtid()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addUriParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 189
    iget-object v2, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v2, v5}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->setLastApi(I)V

    .line 191
    const/4 v2, 0x1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mKVSResponse:Lcom/sec/android/sCloudSync/Records/KVSResponse;

    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->handleRequest(ILjava/lang/String;Lcom/sec/android/sCloudSync/Records/AbstractJSON;I)Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;

    .line 193
    iget-object v2, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mKVSResponse:Lcom/sec/android/sCloudSync/Records/KVSResponse;

    return-object v2
.end method

.method public getServerTimeStamp()J
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    const/16 v8, 0xd7

    .line 227
    const-wide/16 v2, 0x0

    .line 228
    .local v2, "serverTimeStamp":J
    iget-object v5, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v5}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getAuthManager()Lcom/sec/android/sCloudSync/Auth/AuthManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/sCloudSync/Auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v0

    .line 229
    .local v0, "baseUrl":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 231
    const-string v5, "KVSServiceManager"

    const-string v6, "There is NO Base URL."

    invoke-static {v5, v6}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    new-instance v5, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/4 v6, 0x3

    invoke-direct {v5, v6}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v5

    .line 234
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 235
    .local v4, "url":Ljava/lang/StringBuilder;
    const-string v5, "/kvs/timestamp?"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 236
    iget-object v5, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v5}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getAuthManager()Lcom/sec/android/sCloudSync/Auth/AuthManager;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mCid:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/sec/android/sCloudSync/Auth/AuthManager;->getGetApiParams(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 238
    const-string v5, "ctid"

    iget-object v6, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v6}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getCtid()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addUriParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 241
    iget-object v5, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v5, v8}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->setLastApi(I)V

    .line 243
    const/4 v5, 0x1

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mKVSResponse:Lcom/sec/android/sCloudSync/Records/KVSResponse;

    invoke-virtual {p0, v5, v6, v7, v8}, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->handleRequest(ILjava/lang/String;Lcom/sec/android/sCloudSync/Records/AbstractJSON;I)Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;

    .line 245
    iget-object v5, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mKVSResponse:Lcom/sec/android/sCloudSync/Records/KVSResponse;

    if-eqz v5, :cond_1

    .line 246
    iget-object v5, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mKVSResponse:Lcom/sec/android/sCloudSync/Records/KVSResponse;

    invoke-virtual {v5}, Lcom/sec/android/sCloudSync/Records/KVSResponse;->getResponseCode()I

    move-result v1

    .line 247
    .local v1, "responseCode":I
    if-nez v1, :cond_2

    .line 249
    iget-object v5, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mKVSResponse:Lcom/sec/android/sCloudSync/Records/KVSResponse;

    invoke-virtual {v5}, Lcom/sec/android/sCloudSync/Records/KVSResponse;->getServerTimeStamp()J

    move-result-wide v2

    .line 250
    const-string v5, "KVSServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " The server time stamp received is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    .end local v1    # "responseCode":I
    :cond_1
    :goto_0
    return-wide v2

    .line 253
    .restart local v1    # "responseCode":I
    :cond_2
    const-string v5, "KVSServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "There was an error in the request.Error Response code obtained"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getUpdates(Ljava/lang/String;Ljava/lang/String;I)Lcom/sec/android/sCloudSync/Records/KVSResponse;
    .locals 6
    .param p1, "startTimeStamp"    # Ljava/lang/String;
    .param p2, "synckey"    # Ljava/lang/String;
    .param p3, "count"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    const/16 v5, 0xd4

    .line 197
    iget-object v2, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v2}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getAuthManager()Lcom/sec/android/sCloudSync/Auth/AuthManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/sCloudSync/Auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v0

    .line 198
    .local v0, "baseUrl":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 200
    const-string v2, "KVSServiceManager"

    const-string v3, "There is NO Base URL."

    invoke-static {v2, v3}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    new-instance v2, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/4 v3, 0x3

    invoke-direct {v2, v3}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v2

    .line 203
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 204
    .local v1, "url":Ljava/lang/StringBuilder;
    const-string v2, "/kvs/updates?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 205
    iget-object v2, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v2}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getAuthManager()Lcom/sec/android/sCloudSync/Auth/AuthManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mCid:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/android/sCloudSync/Auth/AuthManager;->getGetApiParams(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 206
    const-string v2, "&start_timestamp="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 207
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 208
    if-eqz p2, :cond_1

    const-string v2, ""

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 209
    const-string v2, "&key="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 210
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 212
    :cond_1
    const-string v2, "&count="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 213
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 216
    const-string v2, "ctid"

    iget-object v3, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v3}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getCtid()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addUriParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 218
    iget-object v2, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v2, v5}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->setLastApi(I)V

    .line 220
    const/4 v2, 0x1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mKVSResponse:Lcom/sec/android/sCloudSync/Records/KVSResponse;

    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->handleRequest(ILjava/lang/String;Lcom/sec/android/sCloudSync/Records/AbstractJSON;I)Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;

    .line 222
    iget-object v2, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mKVSResponse:Lcom/sec/android/sCloudSync/Records/KVSResponse;

    return-object v2
.end method

.method public getUserAgent()Ljava/lang/String;
    .locals 1

    .prologue
    .line 266
    sget-object v0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->USER_AGENT:Ljava/lang/String;

    return-object v0
.end method

.method public serviceEnd(ZLjava/lang/String;)Lcom/sec/android/sCloudSync/Records/KVSResponse;
    .locals 7
    .param p1, "isSuc"    # Z
    .param p2, "msg"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .line 325
    iget-object v3, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v3}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getAuthManager()Lcom/sec/android/sCloudSync/Auth/AuthManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/sCloudSync/Auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v0

    .line 326
    .local v0, "baseUrl":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/sCloudSync/Tools/AppTool;->getClientDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 327
    .local v1, "deviceId":Ljava/lang/String;
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 329
    :cond_0
    const-string v3, "KVSServiceManager"

    const-string v4, "There is NO Base URL. or devideId"

    invoke-static {v3, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    new-instance v3, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/4 v4, 0x3

    invoke-direct {v3, v4}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v3

    .line 332
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 334
    .local v2, "url":Ljava/lang/StringBuilder;
    const-string v3, "/cloud/end?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 335
    iget-object v3, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v3}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getAuthManager()Lcom/sec/android/sCloudSync/Auth/AuthManager;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mCid:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sec/android/sCloudSync/Auth/AuthManager;->getGetApiParams(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 336
    const-string v3, "cdid"

    invoke-static {v2, v3, v1}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addUriParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337
    const-string v3, "did"

    iget-object v4, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v4}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getAuthManager()Lcom/sec/android/sCloudSync/Auth/AuthManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/sCloudSync/Auth/AuthManager;->getDeviceID()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addUriParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 338
    const-string v3, "ctid"

    iget-object v4, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v4}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getCtid()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addUriParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 339
    const-string v4, "result"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v5}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getLastApi()I

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    if-eqz p1, :cond_2

    const-string v3, "r"

    :goto_0
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v4, v3}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addUriParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 343
    const/4 v3, 0x1

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mKVSResponse:Lcom/sec/android/sCloudSync/Records/KVSResponse;

    const/16 v6, 0x32c

    invoke-virtual {p0, v3, v4, v5, v6}, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->handleRequest(ILjava/lang/String;Lcom/sec/android/sCloudSync/Records/AbstractJSON;I)Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;

    .line 344
    iget-object v3, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mKVSResponse:Lcom/sec/android/sCloudSync/Records/KVSResponse;

    return-object v3

    .line 339
    :cond_2
    const-string v3, "e"

    goto :goto_0
.end method

.method public servicePrepare(Ljava/lang/String;ZLjava/lang/String;)Lcom/sec/android/sCloudSync/Records/KVSResponse;
    .locals 7
    .param p1, "trigger"    # Ljava/lang/String;
    .param p2, "isSuc"    # Z
    .param p3, "msg"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    const/16 v6, 0x32a

    .line 271
    iget-object v3, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v3}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getAuthManager()Lcom/sec/android/sCloudSync/Auth/AuthManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/sCloudSync/Auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v0

    .line 272
    .local v0, "baseUrl":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/sCloudSync/Tools/AppTool;->getClientDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 273
    .local v1, "deviceId":Ljava/lang/String;
    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    .line 275
    :cond_0
    const-string v3, "KVSServiceManager"

    const-string v4, "There is NO Base URL."

    invoke-static {v3, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    new-instance v3, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/4 v4, 0x3

    invoke-direct {v3, v4}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v3

    .line 278
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 280
    .local v2, "url":Ljava/lang/StringBuilder;
    const-string v3, "/cloud/prepare?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 281
    iget-object v3, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v3}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getAuthManager()Lcom/sec/android/sCloudSync/Auth/AuthManager;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mCid:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sec/android/sCloudSync/Auth/AuthManager;->getGetApiParams(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 282
    const-string v3, "cdid"

    invoke-static {v2, v3, v1}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addUriParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 283
    const-string v3, "did"

    iget-object v4, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v4}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getAuthManager()Lcom/sec/android/sCloudSync/Auth/AuthManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/sCloudSync/Auth/AuthManager;->getDeviceID()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addUriParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 284
    const-string v3, "trigger"

    const-string v4, "sync_self"

    invoke-static {v2, v3, v4}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addUriParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 286
    const-string v3, "ctid"

    iget-object v4, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v4}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getCtid()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addUriParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 287
    const-string v4, "result"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v5}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getLastApi()I

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    if-eqz p2, :cond_2

    const-string v3, "r"

    :goto_0
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v4, v3}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addUriParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 291
    iget-object v3, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v3, v6}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->setLastApi(I)V

    .line 293
    const/4 v3, 0x1

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mKVSResponse:Lcom/sec/android/sCloudSync/Records/KVSResponse;

    invoke-virtual {p0, v3, v4, v5, v6}, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->handleRequest(ILjava/lang/String;Lcom/sec/android/sCloudSync/Records/AbstractJSON;I)Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;

    .line 294
    iget-object v3, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mKVSResponse:Lcom/sec/android/sCloudSync/Records/KVSResponse;

    return-object v3

    .line 287
    :cond_2
    const-string v3, "e"

    goto :goto_0
.end method

.method public serviceStart(Ljava/lang/String;)Lcom/sec/android/sCloudSync/Records/KVSResponse;
    .locals 7
    .param p1, "trigger"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    const/16 v6, 0x32b

    .line 299
    iget-object v3, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v3}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getAuthManager()Lcom/sec/android/sCloudSync/Auth/AuthManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/sCloudSync/Auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v0

    .line 300
    .local v0, "baseUrl":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/sCloudSync/Tools/AppTool;->getClientDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 301
    .local v1, "deviceId":Ljava/lang/String;
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 303
    :cond_0
    const-string v3, "KVSServiceManager"

    const-string v4, "There is NO Base URL. or deviceId"

    invoke-static {v3, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    new-instance v3, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/4 v4, 0x3

    invoke-direct {v3, v4}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v3

    .line 306
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 308
    .local v2, "url":Ljava/lang/StringBuilder;
    const-string v3, "/cloud/start?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 309
    iget-object v3, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v3}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getAuthManager()Lcom/sec/android/sCloudSync/Auth/AuthManager;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mCid:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sec/android/sCloudSync/Auth/AuthManager;->getGetApiParams(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 310
    const-string v3, "cdid"

    invoke-static {v2, v3, v1}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addUriParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 311
    const-string v3, "did"

    iget-object v4, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v4}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getAuthManager()Lcom/sec/android/sCloudSync/Auth/AuthManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/sCloudSync/Auth/AuthManager;->getDeviceID()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addUriParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 312
    const-string v3, "trigger"

    invoke-static {v2, v3, p1}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addUriParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 313
    const-string v3, "ctid"

    iget-object v4, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v4}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getCtid()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addUriParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 315
    iget-object v3, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v3, v6}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->setLastApi(I)V

    .line 317
    const/4 v3, 0x1

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mKVSResponse:Lcom/sec/android/sCloudSync/Records/KVSResponse;

    invoke-virtual {p0, v3, v4, v5, v6}, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->handleRequest(ILjava/lang/String;Lcom/sec/android/sCloudSync/Records/AbstractJSON;I)Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;

    .line 318
    iget-object v3, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mKVSResponse:Lcom/sec/android/sCloudSync/Records/KVSResponse;

    return-object v3
.end method

.method public setItems(Ljava/util/List;)Lcom/sec/android/sCloudSync/Records/KVSResponse;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/sCloudSync/Records/RecordSetItem;",
            ">;)",
            "Lcom/sec/android/sCloudSync/Records/KVSResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .local p1, "recordDataList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordSetItem;>;"
    const/16 v5, 0xd8

    .line 104
    if-nez p1, :cond_0

    .line 105
    const/4 v0, 0x0

    .line 141
    :goto_0
    return-object v0

    .line 108
    :cond_0
    iget-object v0, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v0}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getAuthManager()Lcom/sec/android/sCloudSync/Auth/AuthManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/sCloudSync/Auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v6

    .line 109
    .local v6, "baseUrl":Ljava/lang/String;
    if-nez v6, :cond_1

    .line 111
    const-string v0, "KVSServiceManager"

    const-string v1, "There is NO Base URL."

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    new-instance v0, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v0

    .line 114
    :cond_1
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 115
    .local v12, "url":Ljava/lang/StringBuilder;
    const-string v0, "/kvs/?action=set"

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 116
    iget-object v0, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v0}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getAuthManager()Lcom/sec/android/sCloudSync/Auth/AuthManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mCid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/sCloudSync/Auth/AuthManager;->getPutApiParams(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    const-string v0, "ctid"

    iget-object v1, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v1}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getCtid()Ljava/lang/String;

    move-result-object v1

    invoke-static {v12, v0, v1}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addUriParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    const/4 v8, 0x0

    .line 122
    .local v8, "insertKeyObject":Lorg/json/JSONObject;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .line 123
    .local v9, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/sCloudSync/Records/RecordSetItem;>;"
    new-instance v11, Lorg/json/JSONArray;

    invoke-direct {v11}, Lorg/json/JSONArray;-><init>()V

    .line 124
    .local v11, "setkeys":Lorg/json/JSONArray;
    const-string v0, "KVSServiceManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "RecordDataItem Size = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 127
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/sCloudSync/Records/RecordSetItem;

    .line 129
    .local v10, "recordItemObject":Lcom/sec/android/sCloudSync/Records/RecordSetItem;
    :try_start_0
    invoke-virtual {v10}, Lcom/sec/android/sCloudSync/Records/RecordSetItem;->toJSON()Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "insertKeyObject":Lorg/json/JSONObject;
    check-cast v8, Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 134
    .restart local v8    # "insertKeyObject":Lorg/json/JSONObject;
    invoke-virtual {v11, v8}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_1

    .line 130
    .end local v8    # "insertKeyObject":Lorg/json/JSONObject;
    :catch_0
    move-exception v7

    .line 131
    .local v7, "e":Lorg/json/JSONException;
    const-string v0, "KVSServiceManager"

    const-string v1, "JSON PARSER Exception"

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    new-instance v0, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v0

    .line 137
    .end local v7    # "e":Lorg/json/JSONException;
    .end local v10    # "recordItemObject":Lcom/sec/android/sCloudSync/Records/RecordSetItem;
    .restart local v8    # "insertKeyObject":Lorg/json/JSONObject;
    :cond_2
    iget-object v0, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v0, v5}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->setLastApi(I)V

    .line 139
    const/4 v1, 0x0

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mKVSResponse:Lcom/sec/android/sCloudSync/Records/KVSResponse;

    invoke-virtual {v11}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->handleRequest(ILjava/lang/String;Lcom/sec/android/sCloudSync/Records/AbstractJSON;Ljava/lang/String;I)Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;

    .line 141
    iget-object v0, p0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->mKVSResponse:Lcom/sec/android/sCloudSync/Records/KVSResponse;

    goto/16 :goto_0
.end method
