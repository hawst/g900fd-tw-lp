.class public interface abstract Lcom/sec/android/sCloudSync/ServiceManagers/ORSConstants;
.super Ljava/lang/Object;
.source "ORSConstants.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/sCloudSync/ServiceManagers/ORSConstants$ERROR;
    }
.end annotation


# static fields
.field public static final BOUNDARY:Ljava/lang/String; = "AaB03x"

.field public static final CREATE_FOLDER:Ljava/lang/String; = "?action=create"

.field public static final DELETE_FILE:Ljava/lang/String; = "?action=delete"

.field public static final DOWNLOAD_FILE:Ljava/lang/String; = "?action=download"

.field public static final FILE_NAME:Ljava/lang/String; = "&file_name="

.field public static final FILE_TIMESTAMP:Ljava/lang/String; = "&timestamp="

.field public static final FOLDER_NAME:Ljava/lang/String; = "&folder_name="

.field public static final GETMETADATA:Ljava/lang/String; = "?action=get"

.field public static final METADATA:Ljava/lang/String; = "metadata"

.field public static final METADATA_HEADER_NAME:Ljava/lang/String; = "Accept"

.field public static final METADATA_HEADER_VALUE:Ljava/lang/String; = "application/json"

.field public static final ORS:Ljava/lang/String; = "/ors"

.field public static final UPLOAD_FILE:Ljava/lang/String; = "?action=upload"
