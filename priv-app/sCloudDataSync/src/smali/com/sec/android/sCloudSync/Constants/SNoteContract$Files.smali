.class public interface abstract Lcom/sec/android/sCloudSync/Constants/SNoteContract$Files;
.super Ljava/lang/Object;
.source "SNoteContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/sCloudSync/Constants/SNoteContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Files"
.end annotation


# static fields
.field public static final ACCOUNT_NAME:Ljava/lang/String; = "account_name"

.field public static final ACCOUNT_TYPE:Ljava/lang/String; = "account_type"

.field public static final APP_INFO:Ljava/lang/String; = "appName"

.field public static final CHILD_FOLDER_COUNT:Ljava/lang/String; = "ChildFolderCount"

.field public static final CONTENT:Ljava/lang/String; = "content"

.field public static final COVER_TYPE:Ljava/lang/String; = "CoverType"

.field public static final DELETED:Ljava/lang/String; = "deleted"

.field public static final DIRTY:Ljava/lang/String; = "dirty"

.field public static final EVERNOTE_ID:Ljava/lang/String; = "EvernoteId"

.field public static final FILE_SIZE:Ljava/lang/String; = "FileSize"

.field public static final FOLDER_PATH:Ljava/lang/String; = "FolderPath"

.field public static final GOOGLE_ID:Ljava/lang/String; = "GoogleId"

.field public static final HAS_FAVORITES:Ljava/lang/String; = "HasFavorites"

.field public static final HAS_SCLOUD:Ljava/lang/String; = "HasScloud"

.field public static final HAS_TAG:Ljava/lang/String; = "HasTag"

.field public static final HAS_VOICERECORD:Ljava/lang/String; = "HasVoiceRecord"

.field public static final ID:Ljava/lang/String; = "_id"

.field public static final INNER_NOTE_COUNT:Ljava/lang/String; = "InnerNoteCount"

.field public static final IS_FOLDER:Ljava/lang/String; = "IsFolder"

.field public static final IS_LOCKED:Ljava/lang/String; = "IsLocked"

.field public static final MODIFIED_TIME:Ljava/lang/String; = "ModifiedTime"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final PATH:Ljava/lang/String; = "path"

.field public static final SYNC1:Ljava/lang/String; = "sync1"

.field public static final SYNC2:Ljava/lang/String; = "sync2"

.field public static final SYNC3:Ljava/lang/String; = "sync3"

.field public static final SYNC4:Ljava/lang/String; = "sync4"

.field public static final SYNC_NAME:Ljava/lang/String; = "syncname"

.field public static final SYNC_PATH:Ljava/lang/String; = "syncpath"

.field public static final TAG_CONTENT:Ljava/lang/String; = "Tag_Content"

.field public static final TEMPLATE_TYPE:Ljava/lang/String; = "TemplateType"
