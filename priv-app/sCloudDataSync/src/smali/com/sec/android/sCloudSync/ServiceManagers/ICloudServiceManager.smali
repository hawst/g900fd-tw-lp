.class public interface abstract Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;
.super Ljava/lang/Object;
.source "ICloudServiceManager.java"


# static fields
.field public static final CLOUD_START_R_CLEARED:I = -0x1

.field public static final CLOUD_START_R_FULLSYNC:I = 0x9

.field public static final CLOUD_START_R_SERVER_MAINTENCE:I = 0x5

.field public static final CLOUD_START_R_SUCCESS:I


# virtual methods
.method public abstract getAuthManager()Lcom/sec/android/sCloudSync/Auth/AuthManager;
.end method

.method public abstract getCtid()Ljava/lang/String;
.end method

.method public abstract getFileServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;
.end method

.method public abstract getLastApi()I
.end method

.method public abstract getRecordServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/IRecordServiceManager;
.end method

.method public abstract serviceEnd(ZLjava/lang/String;)Lcom/sec/android/sCloudSync/Records/KVSResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation
.end method

.method public abstract serviceStart(Ljava/lang/String;)Lcom/sec/android/sCloudSync/Records/KVSResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation
.end method

.method public abstract setAuthManager(Lcom/sec/android/sCloudSync/Auth/AuthManager;)V
.end method

.method public abstract setCtid(Ljava/lang/String;)V
.end method

.method public abstract setLastApi(I)V
.end method
