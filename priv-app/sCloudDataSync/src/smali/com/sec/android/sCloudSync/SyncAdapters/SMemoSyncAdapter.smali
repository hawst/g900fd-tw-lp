.class public Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;
.super Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;
.source "SMemoSyncAdapter.java"


# static fields
.field private static final SMEMO_FOLDER_PATH:Ljava/lang/String; = "/mnt/sdcard/Application/SMemo"

.field private static final SWITCHER_PATH:Ljava/lang/String; = "/mnt/sdcard/Application/SMemo/switcher/"

.field private static final TAG:Ljava/lang/String; = "SMemoSyncAdapter"

.field private static final TEMP_FILE:Ljava/lang/String; = "_temp"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 79
    new-instance v0, Lcom/sec/android/sCloudSync/ServiceManagers/SCloudServiceManager;

    const-string v1, "X08g96bD1C"

    const/4 v2, 0x1

    invoke-direct {v0, p1, v1, v2}, Lcom/sec/android/sCloudSync/ServiceManagers/SCloudServiceManager;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    invoke-direct {p0, p1, v0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;-><init>(Landroid/content/Context;Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;)V

    .line 80
    return-void
.end method

.method private compareFileCheckSum(Lcom/sec/android/sCloudSync/Records/KVSItem;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/Map;)V
    .locals 15
    .param p1, "item"    # Lcom/sec/android/sCloudSync/Records/KVSItem;
    .param p2, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/sCloudSync/Records/KVSItem;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .line 301
    .local p3, "fileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p4, "delFileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p5, "checkSumMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getID()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-lez v1, :cond_4

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/sCloudSync/Records/KVSItem;->isDeleted()Z

    move-result v1

    if-nez v1, :cond_4

    .line 303
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/sec/android/sCloudSync/Constants/SMemoContract;->FILETRACKER_URI:Landroid/net/Uri;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "FilePath"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "Checksum"

    aput-object v5, v3, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "sMemoId=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 309
    .local v7, "cursor":Landroid/database/Cursor;
    if-eqz v7, :cond_4

    .line 310
    :cond_0
    :goto_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 311
    const-string v1, "FilePath"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 312
    .local v9, "fPath":Ljava/lang/String;
    const-string v1, "Checksum"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 313
    .local v10, "localCheckSum":Ljava/lang/String;
    move-object/from16 v0, p3

    invoke-interface {v0, v9}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 314
    iget-boolean v1, p0, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->mbSyncCanceled:Z

    if-eqz v1, :cond_1

    .line 315
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 316
    new-instance v1, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/16 v2, 0x9

    invoke-direct {v1, v2}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v1

    .line 320
    :cond_1
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getCloudServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getFileServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;

    move-result-object v1

    invoke-virtual {v1, v9, v9}, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->getMetadata(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/sCloudSync/Records/ORSResponse;
    :try_end_0
    .catch Lcom/sec/android/sCloudSync/Util/SyncException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v12

    .line 326
    .local v12, "orsResponse":Lcom/sec/android/sCloudSync/Records/ORSResponse;
    invoke-virtual {v12}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getResponseCode()I

    move-result v13

    .line 327
    .local v13, "ret_code":I
    if-nez v13, :cond_0

    .line 328
    invoke-virtual {v12}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getORSResponse()Lcom/sec/android/sCloudSync/Records/RecordORSItem;

    move-result-object v11

    .line 329
    .local v11, "metadataRecords":Lcom/sec/android/sCloudSync/Records/RecordORSItem;
    invoke-virtual {v11}, Lcom/sec/android/sCloudSync/Records/RecordORSItem;->getValue()Ljava/lang/String;

    move-result-object v14

    .line 330
    .local v14, "serverCheckSum":Ljava/lang/String;
    move-object/from16 v0, p5

    invoke-interface {v0, v9, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 332
    invoke-virtual {v14, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 333
    move-object/from16 v0, p3

    invoke-interface {v0, v9}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 321
    .end local v11    # "metadataRecords":Lcom/sec/android/sCloudSync/Records/RecordORSItem;
    .end local v12    # "orsResponse":Lcom/sec/android/sCloudSync/Records/ORSResponse;
    .end local v13    # "ret_code":I
    .end local v14    # "serverCheckSum":Ljava/lang/String;
    :catch_0
    move-exception v8

    .line 322
    .local v8, "e":Lcom/sec/android/sCloudSync/Util/SyncException;
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 323
    throw v8

    .line 336
    .end local v8    # "e":Lcom/sec/android/sCloudSync/Util/SyncException;
    :cond_2
    move-object/from16 v0, p4

    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 339
    .end local v9    # "fPath":Ljava/lang/String;
    .end local v10    # "localCheckSum":Ljava/lang/String;
    :cond_3
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 344
    .end local v7    # "cursor":Landroid/database/Cursor;
    :cond_4
    return-void
.end method

.method private createAllFileList(Ljava/util/List;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 997
    .local p1, "filelist":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 998
    .local v2, "str":Ljava/lang/StringBuilder;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 999
    .local v0, "file":Ljava/lang/String;
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x2c

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1000
    .end local v0    # "file":Ljava/lang/String;
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_1

    .line 1001
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 1002
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private createSMemoFolders(Ljava/lang/String;)Z
    .locals 13
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .line 914
    const/16 v10, 0x9

    new-array v4, v10, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v11, "mnt"

    aput-object v11, v4, v10

    const/4 v10, 0x1

    const-string v11, "sdcard"

    aput-object v11, v4, v10

    const/4 v10, 0x2

    const-string v11, "Application"

    aput-object v11, v4, v10

    const/4 v10, 0x3

    const-string v11, "SMemo"

    aput-object v11, v4, v10

    const/4 v10, 0x4

    const-string v11, "cache"

    aput-object v11, v4, v10

    const/4 v10, 0x5

    const-string v11, "color"

    aput-object v11, v4, v10

    const/4 v10, 0x6

    const-string v11, "image"

    aput-object v11, v4, v10

    const/4 v10, 0x7

    const-string v11, "sound"

    aput-object v11, v4, v10

    const/16 v10, 0x8

    const-string v11, "switcher"

    aput-object v11, v4, v10

    .line 916
    .local v4, "folderNames":[Ljava/lang/String;
    const/16 v10, 0x9

    new-array v6, v10, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v11, "/"

    aput-object v11, v6, v10

    const/4 v10, 0x1

    const-string v11, "/mnt/"

    aput-object v11, v6, v10

    const/4 v10, 0x2

    const-string v11, "/mnt/sdcard/"

    aput-object v11, v6, v10

    const/4 v10, 0x3

    const-string v11, "/mnt/sdcard/Application/"

    aput-object v11, v6, v10

    const/4 v10, 0x4

    const-string v11, "/mnt/sdcard/Application/SMemo"

    aput-object v11, v6, v10

    const/4 v10, 0x5

    const-string v11, "/mnt/sdcard/Application/SMemo"

    aput-object v11, v6, v10

    const/4 v10, 0x6

    const-string v11, "/mnt/sdcard/Application/SMemo"

    aput-object v11, v6, v10

    const/4 v10, 0x7

    const-string v11, "/mnt/sdcard/Application/SMemo"

    aput-object v11, v6, v10

    const/16 v10, 0x8

    const-string v11, "/mnt/sdcard/Application/SMemo"

    aput-object v11, v6, v10

    .line 921
    .local v6, "folderPaths":[Ljava/lang/String;
    const/4 v0, 0x0

    .line 922
    .local v0, "createFolderResponse":Lcom/sec/android/sCloudSync/Records/ORSResponse;
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 924
    .local v9, "recordMetaDataList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordORSItem;>;"
    array-length v8, v4

    .line 925
    .local v8, "length":I
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-static {v10}, Lcom/samsung/android/scloud/framework/util/TimeManager;->getCurrentTime(Landroid/content/Context;)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 926
    .local v1, "currentTimeString":Ljava/lang/String;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    if-ge v7, v8, :cond_2

    .line 927
    aget-object v3, v4, v7

    .line 928
    .local v3, "folderName":Ljava/lang/String;
    aget-object v5, v6, v7

    .line 930
    .local v5, "folderPath":Ljava/lang/String;
    new-instance v10, Lcom/sec/android/sCloudSync/Records/RecordORSItem;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "FolderPath : /"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11, v3}, Lcom/sec/android/sCloudSync/Records/RecordORSItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 933
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getCloudServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    move-result-object v10

    invoke-interface {v10}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getFileServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;

    move-result-object v10

    invoke-virtual {v10, v9, v5, v3, v1}, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->createFolder(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/sCloudSync/Records/ORSResponse;
    :try_end_0
    .catch Lcom/sec/android/sCloudSync/Util/SyncException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 939
    invoke-virtual {v0}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getResponseCode()I

    move-result v10

    if-nez v10, :cond_0

    .line 940
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Folder "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " created successfully "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 926
    :goto_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 934
    :catch_0
    move-exception v2

    .line 935
    .local v2, "e":Lcom/sec/android/sCloudSync/Util/SyncException;
    invoke-virtual {v2}, Lcom/sec/android/sCloudSync/Util/SyncException;->printStackTrace()V

    .line 936
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Create Folder Exception received :- "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v2}, Lcom/sec/android/sCloudSync/Util/SyncException;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 937
    const/4 v10, 0x0

    .line 947
    .end local v2    # "e":Lcom/sec/android/sCloudSync/Util/SyncException;
    .end local v3    # "folderName":Ljava/lang/String;
    .end local v5    # "folderPath":Ljava/lang/String;
    :goto_2
    return v10

    .line 941
    .restart local v3    # "folderName":Ljava/lang/String;
    .restart local v5    # "folderPath":Ljava/lang/String;
    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getResponseCode()I

    move-result v10

    const/16 v11, 0x7918

    if-ne v10, v11, :cond_1

    .line 942
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Folder "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " already exists "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 944
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Folder "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " creation failed "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 947
    .end local v3    # "folderName":Ljava/lang/String;
    .end local v5    # "folderPath":Ljava/lang/String;
    :cond_2
    const/4 v10, 0x1

    goto :goto_2
.end method

.method private deleteFiles(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 985
    .local p1, "deleteFileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 986
    .local v3, "path":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 987
    .local v0, "filetoDelete":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 988
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v2

    .line 989
    .local v2, "isDeleteSuccess":Z
    if-nez v2, :cond_0

    .line 990
    const-string v4, "SMemoSyncAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "File "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " could not be deleted"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 994
    .end local v0    # "filetoDelete":Ljava/io/File;
    .end local v2    # "isDeleteSuccess":Z
    .end local v3    # "path":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private deleteFromServerORS(Ljava/util/List;)V
    .locals 25
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/sCloudSync/Records/RecordBase;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;,
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 618
    .local p1, "recordsToDeleteList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordBase;>;"
    if-eqz p1, :cond_0

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 707
    :cond_0
    :goto_0
    return-void

    .line 621
    :cond_1
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    .line 623
    .local v21, "keysTobeRemoved":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :goto_1
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/sCloudSync/Records/RecordBase;

    .line 624
    .local v9, "deleteItem":Lcom/sec/android/sCloudSync/Records/RecordBase;
    const/16 v22, 0x0

    .line 625
    .local v22, "orsResponse":Lcom/sec/android/sCloudSync/Records/ORSResponse;
    const/4 v15, 0x0

    .line 626
    .local v15, "hasDeleteFailed":Z
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 627
    .local v12, "deleteSuccess":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v9}, Lcom/sec/android/sCloudSync/Records/RecordBase;->getKEY()Ljava/lang/String;

    move-result-object v20

    .line 629
    .local v20, "key":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/sec/android/sCloudSync/Constants/SMemoContract;->FILETRACKER_URI:Landroid/net/Uri;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "FilePath"

    aput-object v6, v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "sMemoId = \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 632
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_7

    .line 634
    :goto_2
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 635
    const-string v2, "FilePath"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 637
    .local v11, "deletePath":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->mbSyncCanceled:Z

    if-eqz v2, :cond_2

    .line 638
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 639
    new-instance v2, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/16 v3, 0x9

    invoke-direct {v2, v3}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v2

    .line 642
    :cond_2
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getCloudServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getFileServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;

    move-result-object v2

    invoke-virtual {v2, v11}, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->deleteFile(Ljava/lang/String;)Lcom/sec/android/sCloudSync/Records/ORSResponse;
    :try_end_0
    .catch Lcom/sec/android/sCloudSync/Util/SyncException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v22

    .line 648
    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getResponseCode()I

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getResponseCode()I

    move-result v2

    const/16 v3, 0x7d02

    if-ne v2, v3, :cond_4

    .line 649
    :cond_3
    invoke-virtual {v12, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 643
    :catch_0
    move-exception v13

    .line 644
    .local v13, "e":Lcom/sec/android/sCloudSync/Util/SyncException;
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 645
    throw v13

    .line 651
    .end local v13    # "e":Lcom/sec/android/sCloudSync/Util/SyncException;
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->mLocalChangedRecords:Ljava/util/Map;

    move-object/from16 v0, v20

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 652
    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 653
    :cond_5
    const/4 v15, 0x1

    .line 657
    .end local v11    # "deletePath":Ljava/lang/String;
    :cond_6
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 662
    :cond_7
    if-nez v15, :cond_8

    .line 663
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/sec/android/sCloudSync/Constants/SMemoContract;->FILETRACKER_URI:Landroid/net/Uri;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "sMemoId=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_1

    .line 667
    :cond_8
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    .line 668
    .local v24, "where":Ljava/lang/StringBuilder;
    const-string v2, "FilePath"

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " IN ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 671
    invoke-virtual {v12}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v17

    .local v17, "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    .line 672
    .local v14, "file":Ljava/lang/String;
    const/16 v2, 0x27

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x27

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x2c

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 674
    .end local v14    # "file":Ljava/lang/String;
    :cond_9
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_a

    .line 675
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 676
    :cond_a
    const/16 v2, 0x29

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 677
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/sec/android/sCloudSync/Constants/SMemoContract;->FILETRACKER_URI:Landroid/net/Uri;

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_1

    .line 683
    .end local v8    # "cursor":Landroid/database/Cursor;
    .end local v9    # "deleteItem":Lcom/sec/android/sCloudSync/Records/RecordBase;
    .end local v12    # "deleteSuccess":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v15    # "hasDeleteFailed":Z
    .end local v17    # "i$":Ljava/util/Iterator;
    .end local v20    # "key":Ljava/lang/String;
    .end local v22    # "orsResponse":Lcom/sec/android/sCloudSync/Records/ORSResponse;
    .end local v24    # "where":Ljava/lang/StringBuilder;
    :cond_b
    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v19

    .line 685
    .local v19, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_4
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 686
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->mLocalChangedRecords:Ljava/util/Map;

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 689
    :cond_c
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .line 690
    .local v10, "deleteListIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/sCloudSync/Records/RecordBase;>;"
    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    .line 692
    .local v23, "recsTobeRemoved":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/sCloudSync/Records/RecordBase;>;"
    :cond_d
    :goto_5
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 693
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/sCloudSync/Records/RecordBase;

    .line 694
    .restart local v9    # "deleteItem":Lcom/sec/android/sCloudSync/Records/RecordBase;
    invoke-virtual {v9}, Lcom/sec/android/sCloudSync/Records/RecordBase;->getKEY()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 695
    move-object/from16 v0, v23

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 699
    .end local v9    # "deleteItem":Lcom/sec/android/sCloudSync/Records/RecordBase;
    :cond_e
    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v18

    .line 701
    .local v18, "ie":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/sCloudSync/Records/RecordBase;>;"
    :goto_6
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 702
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_6

    .line 705
    :cond_f
    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->clear()V

    .line 706
    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->clear()V

    goto/16 :goto_0
.end method

.method private deleteLocalFiles(Lcom/sec/android/sCloudSync/Records/KVSItem;Ljava/util/List;Ljava/util/List;Z)V
    .locals 12
    .param p1, "item"    # Lcom/sec/android/sCloudSync/Records/KVSItem;
    .param p4, "hasDownloadFailed"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/sCloudSync/Records/KVSItem;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 391
    .local p2, "fileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p3, "delFileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz p3, :cond_2

    invoke-interface {p3}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_2

    .line 392
    invoke-direct {p0, p3}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->deleteFiles(Ljava/util/List;)V

    .line 394
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 395
    .local v7, "where":Ljava/lang/StringBuilder;
    const-string v8, "FilePath"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " IN ("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 397
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 398
    .local v0, "file":Ljava/lang/String;
    const/16 v8, 0x27

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/16 v9, 0x27

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v8

    const/16 v9, 0x2c

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 400
    .end local v0    # "file":Ljava/lang/String;
    :cond_0
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v8

    if-lez v8, :cond_1

    .line 401
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 402
    :cond_1
    const/16 v8, 0x29

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 404
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    sget-object v9, Lcom/sec/android/sCloudSync/Constants/SMemoContract;->FILETRACKER_URI:Landroid/net/Uri;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v8, v9, v10, v11}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 407
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v7    # "where":Ljava/lang/StringBuilder;
    :cond_2
    invoke-virtual {p1}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getID()J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-lez v8, :cond_6

    invoke-virtual {p1}, Lcom/sec/android/sCloudSync/Records/KVSItem;->isDeleted()Z

    move-result v8

    if-nez v8, :cond_6

    .line 408
    if-nez p4, :cond_4

    .line 410
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .restart local v2    # "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 411
    .local v1, "filePath":Ljava/lang/String;
    new-instance v5, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "_temp"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 412
    .local v5, "tempFile":Ljava/io/File;
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 413
    .local v4, "origFile":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 414
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    move-result v3

    .line 415
    .local v3, "isDeleteSuccess":Z
    if-nez v3, :cond_3

    .line 416
    const-string v8, "SMemoSyncAdapter"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "File "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " could not be deleted"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    .end local v3    # "isDeleteSuccess":Z
    :cond_3
    invoke-virtual {v5, v4}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    goto :goto_1

    .line 422
    .end local v1    # "filePath":Ljava/lang/String;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "origFile":Ljava/io/File;
    .end local v5    # "tempFile":Ljava/io/File;
    :cond_4
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 423
    .local v6, "tempFileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .restart local v2    # "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 424
    .restart local v1    # "filePath":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "_temp"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 426
    .end local v1    # "filePath":Ljava/lang/String;
    :cond_5
    invoke-direct {p0, v6}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->deleteFiles(Ljava/util/List;)V

    .line 429
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v6    # "tempFileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_6
    return-void
.end method

.method private downloadFile(Lcom/sec/android/sCloudSync/Records/KVSItem;Ljava/lang/String;Ljava/util/Map;Ljava/util/List;Ljava/util/Map;Ljava/util/Set;)Z
    .locals 9
    .param p1, "item"    # Lcom/sec/android/sCloudSync/Records/KVSItem;
    .param p2, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/sCloudSync/Records/KVSItem;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .line 261
    .local p3, "fileDownloadList":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    .local p4, "fileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p5, "checkSumMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .local p6, "failedDownloads":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 262
    .local v1, "filePath":Ljava/lang/String;
    iget-boolean v5, p0, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->mbSyncCanceled:Z

    if-eqz v5, :cond_1

    .line 263
    new-instance v5, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/16 v6, 0x9

    invoke-direct {v5, v6}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v5

    .line 266
    :cond_1
    const/4 v3, 0x0

    .line 267
    .local v3, "localPath":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getID()J

    move-result-wide v5

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-lez v5, :cond_6

    .line 268
    invoke-virtual {p1}, Lcom/sec/android/sCloudSync/Records/KVSItem;->isDeleted()Z

    move-result v5

    if-nez v5, :cond_2

    .line 269
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_temp"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 275
    :cond_2
    :goto_0
    if-eqz v3, :cond_0

    .line 277
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getCloudServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    move-result-object v5

    invoke-interface {v5}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getFileServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v1, v3, v6}, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->downloadFile(Ljava/lang/String;Ljava/lang/String;Z)Lcom/sec/android/sCloudSync/Records/ORSResponse;

    move-result-object v4

    .line 278
    .local v4, "orsResponse":Lcom/sec/android/sCloudSync/Records/ORSResponse;
    invoke-interface {p5, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-nez v5, :cond_3

    invoke-virtual {v4}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getResponseCode()I

    move-result v5

    if-nez v5, :cond_3

    .line 279
    invoke-static {v3}, Lcom/sec/android/sCloudSync/Tools/FileTool;->getMessageDigest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {p5, v1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 281
    :cond_3
    invoke-virtual {v4}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getResponseCode()I

    move-result v5

    if-eqz v5, :cond_0

    .line 283
    invoke-virtual {v4}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getResponseCode()I

    move-result v5

    const/16 v6, 0x7d06

    if-eq v5, v6, :cond_4

    invoke-virtual {v4}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getResponseCode()I

    move-result v5

    const/16 v6, 0x7d02

    if-ne v5, v6, :cond_5

    .line 284
    :cond_4
    invoke-interface {p3, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    invoke-direct {p0, p2, v5}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->removeGarbagefromServer(Ljava/lang/String;Ljava/util/List;)V

    .line 286
    :cond_5
    invoke-interface {p6, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 287
    const/4 v5, 0x0

    .line 296
    .end local v1    # "filePath":Ljava/lang/String;
    .end local v3    # "localPath":Ljava/lang/String;
    .end local v4    # "orsResponse":Lcom/sec/android/sCloudSync/Records/ORSResponse;
    :goto_1
    return v5

    .line 272
    .restart local v1    # "filePath":Ljava/lang/String;
    .restart local v3    # "localPath":Ljava/lang/String;
    :cond_6
    move-object v3, v1

    goto :goto_0

    .line 289
    :catch_0
    move-exception v0

    .line 290
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 291
    invoke-interface {p6, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 292
    const/4 v5, 0x0

    goto :goto_1

    .line 296
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "filePath":Ljava/lang/String;
    .end local v3    # "localPath":Ljava/lang/String;
    :cond_7
    const/4 v5, 0x1

    goto :goto_1
.end method

.method private fixParentIds(Landroid/content/SyncStats;)V
    .locals 17
    .param p1, "stats"    # Landroid/content/SyncStats;

    .prologue
    .line 951
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getLocalupdatesUri()Landroid/net/Uri;

    move-result-object v2

    .line 952
    .local v2, "uri":Landroid/net/Uri;
    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getIdColumnName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v1

    const/4 v1, 0x1

    const-string v5, "sync4"

    aput-object v5, v3, v1

    .line 953
    .local v3, "projection":[Ljava/lang/String;
    const/4 v7, 0x0

    .line 954
    .local v7, "cursor":Landroid/database/Cursor;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getAccountName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " = \'"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v5

    iget-object v5, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "\' AND "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getAccountType()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " = \'"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v5

    iget-object v5, v5, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "\' AND ("

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "IsFolder"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " = 1 OR "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "IsFolder"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " = 2)"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 959
    .local v4, "where":Ljava/lang/String;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v1

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v7

    .line 964
    if-eqz v7, :cond_0

    .line 965
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getIdColumnName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    .line 966
    .local v12, "parentidIndex":I
    const-string v1, "sync4"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    .line 967
    .local v11, "parentKeyIndex":I
    new-instance v14, Landroid/content/ContentValues;

    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 969
    .local v14, "values":Landroid/content/ContentValues;
    :goto_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 970
    invoke-virtual {v14}, Landroid/content/ContentValues;->clear()V

    .line 971
    invoke-interface {v7, v12}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    .line 972
    .local v9, "parentId":J
    invoke-interface {v7, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 973
    .local v13, "parentkey":Ljava/lang/String;
    const-string v1, "ParentID"

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v14, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 975
    :try_start_1
    move-object/from16 v0, p1

    iget-wide v5, v0, Landroid/content/SyncStats;->numUpdates:J

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v1

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "sync3 = \'"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "\'"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v1, v2, v14, v15, v0}, Landroid/content/ContentProviderClient;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    int-to-long v15, v1

    add-long/2addr v5, v15

    move-object/from16 v0, p1

    iput-wide v5, v0, Landroid/content/SyncStats;->numUpdates:J
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 976
    :catch_0
    move-exception v8

    .line 977
    .local v8, "e":Landroid/os/RemoteException;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "fixParentIds(): "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v8}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 960
    .end local v8    # "e":Landroid/os/RemoteException;
    .end local v9    # "parentId":J
    .end local v11    # "parentKeyIndex":I
    .end local v12    # "parentidIndex":I
    .end local v13    # "parentkey":Ljava/lang/String;
    .end local v14    # "values":Landroid/content/ContentValues;
    :catch_1
    move-exception v8

    .line 961
    .restart local v8    # "e":Landroid/os/RemoteException;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "fixParentIds(): "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v8}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 982
    .end local v8    # "e":Landroid/os/RemoteException;
    :cond_0
    :goto_1
    return-void

    .line 980
    .restart local v11    # "parentKeyIndex":I
    .restart local v12    # "parentidIndex":I
    .restart local v14    # "values":Landroid/content/ContentValues;
    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_1
.end method

.method private handleDelete(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .line 711
    .local p1, "deleteFileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz p1, :cond_2

    .line 712
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 713
    .local v0, "deletefile":Ljava/lang/String;
    iget-boolean v3, p0, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->mbSyncCanceled:Z

    if-eqz v3, :cond_1

    .line 714
    new-instance v3, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/16 v4, 0x9

    invoke-direct {v3, v4}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v3

    .line 716
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getCloudServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    move-result-object v3

    invoke-interface {v3}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getFileServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->deleteFile(Ljava/lang/String;)Lcom/sec/android/sCloudSync/Records/ORSResponse;

    move-result-object v2

    .line 718
    .local v2, "orsResponse":Lcom/sec/android/sCloudSync/Records/ORSResponse;
    invoke-virtual {v2}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getResponseCode()I

    move-result v3

    if-nez v3, :cond_0

    .line 719
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/sec/android/sCloudSync/Constants/SMemoContract;->FILETRACKER_URI:Landroid/net/Uri;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "FilePath=\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0

    .line 726
    .end local v0    # "deletefile":Ljava/lang/String;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "orsResponse":Lcom/sec/android/sCloudSync/Records/ORSResponse;
    :cond_2
    return-void
.end method

.method private handleFailedDownload(Ljava/util/Set;Ljava/util/Set;Ljava/util/Map;)Z
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;)Z"
        }
    .end annotation

    .prologue
    .line 433
    .local p1, "failedDownloads":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .local p2, "handledFailDownloads":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .local p3, "fileDownloadList":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    const/4 v3, 0x0

    .line 434
    .local v3, "hasInsertFailed":Z
    if-eqz p1, :cond_6

    invoke-interface/range {p1 .. p1}, Ljava/util/Set;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_6

    .line 435
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->resetLastSyncTime()V

    .line 436
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 437
    .local v1, "deleteOperation":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    invoke-interface/range {p1 .. p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 438
    .local v2, "failed":Ljava/lang/String;
    iget-object v11, p0, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->mServerChangedRecords:Ljava/util/Map;

    invoke-interface {v11, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/sCloudSync/Records/KVSItem;

    .line 439
    .local v6, "item":Lcom/sec/android/sCloudSync/Records/KVSItem;
    invoke-virtual {v6}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getID()J

    move-result-wide v11

    const-wide/16 v13, 0x0

    cmp-long v11, v11, v13

    if-lez v11, :cond_5

    .line 440
    invoke-virtual {v6}, Lcom/sec/android/sCloudSync/Records/KVSItem;->isDeleted()Z

    move-result v11

    if-nez v11, :cond_0

    .line 442
    sget-object v11, Lcom/sec/android/sCloudSync/Constants/SMemoContract;->SMEMO_URI:Landroid/net/Uri;

    invoke-virtual {v6}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getID()J

    move-result-wide v12

    invoke-static {v11, v12, v13}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v10

    .line 443
    .local v10, "uri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getCallerSyncAdapter()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    invoke-virtual {v11}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v11

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getAccountName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v13

    iget-object v13, v13, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v11, v12, v13}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v11

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getAccountType()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v13

    iget-object v13, v13, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v11, v12, v13}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v11

    invoke-virtual {v11}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v10

    .line 447
    iget-object v11, p0, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    check-cast v11, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;

    invoke-virtual {v11}, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->getOperations()Ljava/util/ArrayList;

    move-result-object v9

    .line 448
    .local v9, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .line 449
    .local v7, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/content/ContentProviderOperation;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 450
    :cond_1
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_3

    .line 453
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/ContentProviderOperation;

    .line 454
    .local v8, "operation":Landroid/content/ContentProviderOperation;
    invoke-virtual {v8}, Landroid/content/ContentProviderOperation;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 455
    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 456
    :cond_2
    invoke-virtual {v8}, Landroid/content/ContentProviderOperation;->toString()Ljava/lang/String;

    move-result-object v11

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "MemoID="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v6}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getID()J

    move-result-wide v13

    invoke-virtual {v12, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 459
    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 462
    .end local v8    # "operation":Landroid/content/ContentProviderOperation;
    :cond_3
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 463
    .local v5, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/content/ContentProviderOperation;>;"
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_4

    .line 464
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    invoke-virtual {v9, v11}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_2

    .line 466
    :cond_4
    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 470
    .end local v5    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/content/ContentProviderOperation;>;"
    .end local v7    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/content/ContentProviderOperation;>;"
    .end local v9    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v10    # "uri":Landroid/net/Uri;
    :cond_5
    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 471
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 475
    .end local v1    # "deleteOperation":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v2    # "failed":Ljava/lang/String;
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v6    # "item":Lcom/sec/android/sCloudSync/Records/KVSItem;
    :cond_6
    return v3
.end method

.method private handleInsertFail(Ljava/util/Set;Ljava/util/Map;)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 480
    .local p1, "handledFailDownloads":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .local p2, "fileDownloadList":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getContentUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getCallerSyncAdapter()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getAccountName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v4

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getAccountType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v4

    iget-object v4, v4, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    .line 484
    .local v2, "memoUri":Landroid/net/Uri;
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-interface/range {p1 .. p1}, Ljava/util/Set;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    mul-int/lit16 v1, v1, 0x104

    invoke-direct {v14, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 485
    .local v14, "where":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getKeyColumnName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " IN ("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 486
    move-object/from16 v0, p1

    invoke-static {v14, v0}, Lcom/sec/android/sCloudSync/Tools/AppTool;->appendIds(Ljava/lang/StringBuilder;Ljava/util/Set;)V

    .line 487
    const/16 v1, 0x29

    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 488
    const/4 v7, 0x0

    .line 490
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v3, v4

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 496
    :goto_0
    new-instance v12, Ljava/util/HashSet;

    invoke-direct {v12}, Ljava/util/HashSet;-><init>()V

    .line 497
    .local v12, "ids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    if-eqz v7, :cond_1

    .line 498
    :goto_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 499
    const/4 v1, 0x0

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v12, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 492
    .end local v12    # "ids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    :catch_0
    move-exception v9

    .line 494
    .local v9, "e":Landroid/os/RemoteException;
    invoke-virtual {v9}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 501
    .end local v9    # "e":Landroid/os/RemoteException;
    .restart local v12    # "ids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 504
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v1

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentProviderClient;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 510
    :goto_2
    const/4 v1, 0x0

    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 511
    const-string v1, "MemoID"

    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " IN ("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 512
    invoke-interface {v12}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Long;

    .line 513
    .local v11, "id":Ljava/lang/Long;
    invoke-virtual {v14, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v3, 0x2c

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 505
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v11    # "id":Ljava/lang/Long;
    :catch_1
    move-exception v9

    .line 507
    .restart local v9    # "e":Landroid/os/RemoteException;
    invoke-virtual {v9}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_2

    .line 515
    .end local v9    # "e":Landroid/os/RemoteException;
    .restart local v10    # "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v12}, Ljava/util/Set;->size()I

    move-result v1

    if-lez v1, :cond_3

    .line 516
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 518
    :cond_3
    const/16 v1, 0x29

    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 520
    :try_start_2
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v1

    sget-object v3, Lcom/sec/android/sCloudSync/Constants/SMemoContract;->SMEMO_EXTDATA_URI:Landroid/net/Uri;

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v3, v4, v5}, Landroid/content/ContentProviderClient;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2

    .line 525
    :goto_4
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 526
    .local v8, "deleteFileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface/range {p1 .. p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_5
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    .line 527
    .local v13, "key":Ljava/lang/String;
    move-object/from16 v0, p2

    invoke-interface {v0, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "deleteFileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    check-cast v8, Ljava/util/List;

    .line 528
    .restart local v8    # "deleteFileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p2

    invoke-interface {v0, v13}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 529
    invoke-direct {p0, v8}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->deleteFiles(Ljava/util/List;)V

    goto :goto_5

    .line 522
    .end local v8    # "deleteFileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v13    # "key":Ljava/lang/String;
    :catch_2
    move-exception v9

    .line 523
    .restart local v9    # "e":Landroid/os/RemoteException;
    invoke-virtual {v9}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_4

    .line 531
    .end local v9    # "e":Landroid/os/RemoteException;
    .restart local v8    # "deleteFileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_4
    invoke-interface {v8}, Ljava/util/List;->clear()V

    .line 532
    return-void
.end method

.method private handleRecordNeedToBeRemoved(Ljava/util/List;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/sCloudSync/Records/RecordSetItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 848
    .local p1, "keysTobeRemoved":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p2, "recordstoSetList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordSetItem;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 849
    .local v2, "recordsTobeRemoved":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordSetItem;>;"
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 851
    .local v3, "setListIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/sCloudSync/Records/RecordSetItem;>;"
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 852
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/sCloudSync/Records/RecordSetItem;

    .line 853
    .local v1, "item":Lcom/sec/android/sCloudSync/Records/RecordSetItem;
    invoke-virtual {v1}, Lcom/sec/android/sCloudSync/Records/RecordSetItem;->getKEY()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 854
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 858
    .end local v1    # "item":Lcom/sec/android/sCloudSync/Records/RecordSetItem;
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 859
    .local v0, "ir":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/sCloudSync/Records/RecordSetItem;>;"
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 860
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {p2, v4}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 862
    :cond_2
    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 863
    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 864
    return-void
.end method

.method private handleUpload(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V
    .locals 21
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .line 733
    .local p2, "fileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p3, "keysTobeRemoved":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz p2, :cond_0

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 815
    :cond_0
    :goto_0
    return-void

    .line 736
    :cond_1
    new-instance v20, Landroid/content/ContentValues;

    invoke-direct/range {v20 .. v20}, Landroid/content/ContentValues;-><init>()V

    .line 737
    .local v20, "values":Landroid/content/ContentValues;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 739
    .local v3, "recordORSItemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordORSItem;>;"
    const/4 v4, 0x0

    .line 740
    .local v4, "fileName":Ljava/lang/String;
    const/4 v6, 0x0

    .line 741
    .local v6, "filePath":Ljava/lang/String;
    const/4 v15, 0x0

    .line 742
    .local v15, "messageDigest":Ljava/lang/String;
    const/16 v18, 0x0

    .line 743
    .local v18, "split":[Ljava/lang/String;
    const/4 v7, 0x0

    .line 745
    .local v7, "localFilepath":Ljava/lang/String;
    new-instance v19, Lcom/sec/android/sCloudSync/Records/RecordORSItem;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->createAllFileList(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2}, Lcom/sec/android/sCloudSync/Records/RecordORSItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 747
    .local v19, "switcherRecord":Lcom/sec/android/sCloudSync/Records/RecordORSItem;
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v13

    .line 748
    .local v13, "fileListSize":I
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_1
    if-ge v14, v13, :cond_0

    .line 749
    move-object/from16 v0, p2

    invoke-interface {v0, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "localFilepath":Ljava/lang/String;
    check-cast v7, Ljava/lang/String;

    .line 750
    .restart local v7    # "localFilepath":Ljava/lang/String;
    const-string v2, "/"

    invoke-virtual {v7, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v18

    .line 751
    move-object/from16 v0, v18

    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    aget-object v4, v18, v2

    .line 752
    const/4 v2, 0x0

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v8

    sub-int/2addr v5, v8

    invoke-virtual {v7, v2, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 755
    :try_start_0
    invoke-static {v7}, Lcom/sec/android/sCloudSync/Tools/FileTool;->getMessageDigest(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v15

    .line 762
    const-string v2, "/mnt/sdcard/Application/SMemo/switcher/"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 763
    move-object/from16 v0, v19

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 766
    :cond_2
    new-instance v2, Lcom/sec/android/sCloudSync/Records/RecordORSItem;

    invoke-direct {v2, v7, v15}, Lcom/sec/android/sCloudSync/Records/RecordORSItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 768
    const/16 v17, -0x1

    .line 771
    .local v17, "responseCode":I
    :try_start_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->mbSyncCanceled:Z

    if-eqz v2, :cond_3

    .line 772
    new-instance v2, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/16 v5, 0x9

    invoke-direct {v2, v5}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v2
    :try_end_1
    .catch Lcom/sec/android/sCloudSync/Util/SyncException; {:try_start_1 .. :try_end_1} :catch_0

    .line 781
    :catch_0
    move-exception v11

    .line 782
    .local v11, "e":Lcom/sec/android/sCloudSync/Util/SyncException;
    invoke-virtual {v11}, Lcom/sec/android/sCloudSync/Util/SyncException;->printStackTrace()V

    .line 783
    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 756
    .end local v11    # "e":Lcom/sec/android/sCloudSync/Util/SyncException;
    .end local v17    # "responseCode":I
    :catch_1
    move-exception v12

    .line 757
    .local v12, "e1":Ljava/lang/Exception;
    invoke-virtual {v12}, Ljava/lang/Exception;->printStackTrace()V

    .line 758
    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 774
    .end local v12    # "e1":Ljava/lang/Exception;
    .restart local v17    # "responseCode":I
    :cond_3
    :try_start_2
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getCloudServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getFileServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/android/scloud/framework/util/TimeManager;->getCurrentTime(Landroid/content/Context;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->uploadFile(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z[B)Lcom/sec/android/sCloudSync/Records/ORSResponse;

    move-result-object v16

    .line 779
    .local v16, "orsResponse":Lcom/sec/android/sCloudSync/Records/ORSResponse;
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getResponseCode()I
    :try_end_2
    .catch Lcom/sec/android/sCloudSync/Util/SyncException; {:try_start_2 .. :try_end_2} :catch_0

    move-result v17

    .line 787
    if-nez v17, :cond_5

    .line 789
    invoke-virtual/range {v20 .. v20}, Landroid/content/ContentValues;->clear()V

    .line 790
    const-string v2, "Checksum"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 792
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v5, Lcom/sec/android/sCloudSync/Constants/SMemoContract;->FILETRACKER_URI:Landroid/net/Uri;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "FilePath=\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v2, v5, v0, v8, v9}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v10

    .line 797
    .local v10, "count":I
    if-nez v10, :cond_4

    .line 798
    const-string v2, "sMemoId"

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 799
    const-string v2, "FilePath"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 800
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v5, Lcom/sec/android/sCloudSync/Constants/SMemoContract;->FILETRACKER_URI:Landroid/net/Uri;

    move-object/from16 v0, v20

    invoke-virtual {v2, v5, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 813
    .end local v10    # "count":I
    :cond_4
    :goto_2
    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 748
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_1

    .line 802
    :cond_5
    const/16 v2, 0x7d00

    move/from16 v0, v17

    if-ne v0, v2, :cond_6

    .line 803
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->createSMemoFolders(Ljava/lang/String;)Z

    .line 804
    add-int/lit8 v14, v14, -0x1

    goto :goto_2

    .line 805
    :cond_6
    const/16 v2, 0x4e23

    move/from16 v0, v17

    if-eq v0, v2, :cond_7

    const/16 v2, 0x4e24

    move/from16 v0, v17

    if-ne v0, v2, :cond_8

    .line 807
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->notifyServerStoragefull()V

    goto :goto_2

    .line 809
    :cond_8
    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method private removeGarbagefromServer(Ljava/lang/String;Ljava/util/List;)V
    .locals 7
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .line 1007
    .local p2, "fileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Removing Garbage Data for SMemo Key: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1009
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 1010
    .local v2, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1011
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getCloudServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    move-result-object v4

    invoke-interface {v4}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getFileServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;

    move-result-object v5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, v4}, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->deleteFile(Ljava/lang/String;)Lcom/sec/android/sCloudSync/Records/ORSResponse;

    goto :goto_0

    .line 1015
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1016
    .local v0, "deleteList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordBase;>;"
    new-instance v4, Lcom/sec/android/sCloudSync/Records/RecordBase;

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/android/scloud/framework/util/TimeManager;->getCurrentTime(Landroid/content/Context;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-direct {v4, p1, v5}, Lcom/sec/android/sCloudSync/Records/RecordBase;-><init>(Ljava/lang/String;Ljava/lang/Long;)V

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1018
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getCloudServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    move-result-object v4

    invoke-interface {v4}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getRecordServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/IRecordServiceManager;

    move-result-object v3

    .line 1019
    .local v3, "recordServiceManager":Lcom/sec/android/sCloudSync/ServiceManagers/IRecordServiceManager;
    invoke-interface {v3, v0}, Lcom/sec/android/sCloudSync/ServiceManagers/IRecordServiceManager;->deleteItems(Ljava/util/List;)Lcom/sec/android/sCloudSync/Records/KVSResponse;
    :try_end_0
    .catch Lcom/sec/android/sCloudSync/Util/SyncException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1025
    return-void

    .line 1021
    .end local v3    # "recordServiceManager":Lcom/sec/android/sCloudSync/ServiceManagers/IRecordServiceManager;
    :catch_0
    move-exception v1

    .line 1022
    .local v1, "e":Lcom/sec/android/sCloudSync/Util/SyncException;
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SYNC EXCEPTION: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/sec/android/sCloudSync/Util/SyncException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1023
    throw v1
.end method

.method private updateFileTracker(Lcom/sec/android/sCloudSync/Records/KVSItem;Ljava/lang/String;Ljava/util/Map;)V
    .locals 15
    .param p1, "item"    # Lcom/sec/android/sCloudSync/Records/KVSItem;
    .param p2, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/sCloudSync/Records/KVSItem;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 348
    .local p3, "checkSumMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 353
    .local v10, "values":Landroid/content/ContentValues;
    invoke-interface/range {p3 .. p3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 355
    .local v3, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 356
    .local v1, "checkSumFilePath":Ljava/lang/String;
    const-string v12, "Checksum"

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    invoke-virtual {v10, v12, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    const-string v11, "/cache/"

    invoke-virtual {v1, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getID()J

    move-result-wide v11

    const-wide/16 v13, 0x0

    cmp-long v11, v11, v13

    if-lez v11, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/sCloudSync/Records/KVSItem;->isDeleted()Z

    move-result v11

    if-nez v11, :cond_0

    .line 362
    const-string v11, "/"

    invoke-virtual {v1, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 363
    .local v8, "split":[Ljava/lang/String;
    array-length v11, v8

    add-int/lit8 v11, v11, -0x1

    aget-object v4, v8, v11

    .line 364
    .local v4, "fileName":Ljava/lang/String;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v12, 0x0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v13

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v14

    sub-int/2addr v13, v14

    invoke-virtual {v1, v12, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "stroke"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getID()J

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ".sfm"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 365
    .local v9, "strokeFilePath":Ljava/lang/String;
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 366
    .local v5, "filetoDelete":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 367
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    move-result v7

    .line 368
    .local v7, "isDeleteSuccess":Z
    if-nez v7, :cond_0

    .line 369
    const-string v11, "SMemoSyncAdapter"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "File "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " could not be deleted"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    .end local v4    # "fileName":Ljava/lang/String;
    .end local v5    # "filetoDelete":Ljava/io/File;
    .end local v7    # "isDeleteSuccess":Z
    .end local v8    # "split":[Ljava/lang/String;
    .end local v9    # "strokeFilePath":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    sget-object v12, Lcom/sec/android/sCloudSync/Constants/SMemoContract;->FILETRACKER_URI:Landroid/net/Uri;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "FilePath=\'"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\'"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v11, v12, v10, v13, v14}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 380
    .local v2, "count":I
    if-nez v2, :cond_1

    .line 381
    const-string v11, "FilePath"

    invoke-virtual {v10, v11, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 382
    const-string v11, "sMemoId"

    move-object/from16 v0, p2

    invoke-virtual {v10, v11, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    sget-object v12, Lcom/sec/android/sCloudSync/Constants/SMemoContract;->FILETRACKER_URI:Landroid/net/Uri;

    invoke-virtual {v11, v12, v10}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 385
    :cond_1
    invoke-virtual {v10}, Landroid/content/ContentValues;->clear()V

    goto/16 :goto_0

    .line 387
    .end local v1    # "checkSumFilePath":Ljava/lang/String;
    .end local v2    # "count":I
    .end local v3    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_2
    return-void
.end method

.method private uploadToServerORS(Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/sCloudSync/Records/RecordSetItem;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .line 819
    .local p1, "recordstoSetList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordSetItem;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 844
    :cond_0
    :goto_0
    return-void

    .line 822
    :cond_1
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 823
    .local v9, "keysTobeRemoved":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v10, p0, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    check-cast v10, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;

    invoke-virtual {v10}, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->getFileUploadList()Landroid/util/SparseArray;

    move-result-object v3

    .line 824
    .local v3, "fileUploadList":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;"
    iget-object v10, p0, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    check-cast v10, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;

    invoke-virtual {v10}, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->getServerFileDeleteList()Landroid/util/SparseArray;

    move-result-object v1

    .line 826
    .local v1, "fileDeleteList":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;"
    iget-object v10, p0, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->mLocalChangedRecords:Ljava/util/Map;

    invoke-interface {v10}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v8

    .line 828
    .local v8, "keySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 829
    .local v7, "key":Ljava/lang/String;
    iget-object v10, p0, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->mLocalChangedRecords:Ljava/util/Map;

    invoke-interface {v10, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Long;

    invoke-virtual {v10}, Ljava/lang/Long;->intValue()I

    move-result v5

    .line 830
    .local v5, "id":I
    invoke-virtual {v3, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 831
    .local v2, "fileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {v1, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 833
    .local v0, "deleteFileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0, v0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->handleDelete(Ljava/util/List;)V

    .line 834
    invoke-direct {p0, v7, v2, v9}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->handleUpload(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V

    goto :goto_1

    .line 839
    .end local v0    # "deleteFileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v2    # "fileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v5    # "id":I
    .end local v7    # "key":Ljava/lang/String;
    :cond_2
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 840
    .local v6, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    .line 841
    iget-object v10, p0, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->mLocalChangedRecords:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    invoke-interface {v10, v11}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 843
    :cond_3
    invoke-direct {p0, v9, p1}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->handleRecordNeedToBeRemoved(Ljava/util/List;Ljava/util/List;)V

    goto :goto_0
.end method


# virtual methods
.method public cancelSync()V
    .locals 1

    .prologue
    .line 909
    invoke-super {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->cancelSync()V

    .line 910
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getCloudServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getFileServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->close()V

    .line 911
    return-void
.end method

.method public doUpdateDelete(Lcom/sec/android/sCloudSync/Records/KVSItem;Lcom/sec/android/sCloudSync/Records/RecordItem;Landroid/content/SyncStats;)Z
    .locals 10
    .param p1, "item"    # Lcom/sec/android/sCloudSync/Records/KVSItem;
    .param p2, "record"    # Lcom/sec/android/sCloudSync/Records/RecordItem;
    .param p3, "stats"    # Landroid/content/SyncStats;

    .prologue
    const-wide/16 v8, 0x1

    .line 889
    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->isDeleted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 890
    iget-wide v0, p3, Landroid/content/SyncStats;->numDeletes:J

    add-long/2addr v0, v8

    iput-wide v0, p3, Landroid/content/SyncStats;->numDeletes:J

    .line 892
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getID()J

    move-result-wide v2

    const/4 v4, 0x0

    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getKEY()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;->delete(Landroid/net/Uri;JLjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 904
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 893
    :catch_0
    move-exception v7

    .line 895
    .local v7, "e":Landroid/os/RemoteException;
    invoke-virtual {v7}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 898
    .end local v7    # "e":Landroid/os/RemoteException;
    :cond_0
    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getValue()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getTimeStamp()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getID()J

    move-result-wide v4

    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getKEY()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;->update(Ljava/lang/String;JJLjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 899
    :cond_1
    const-string v0, "SMemoSyncAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to update record with key:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getKEY()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 901
    :cond_2
    iget-wide v0, p3, Landroid/content/SyncStats;->numUpdates:J

    add-long/2addr v0, v8

    iput-wide v0, p3, Landroid/content/SyncStats;->numUpdates:J

    goto :goto_0
.end method

.method protected getAccountName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 147
    const-string v0, "account_name"

    return-object v0
.end method

.method protected getAccountType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 152
    const-string v0, "account_type"

    return-object v0
.end method

.method protected getBuilder()Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;
    .locals 4

    .prologue
    .line 162
    new-instance v0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;-><init>(Landroid/content/ContentProviderClient;Landroid/accounts/Account;Landroid/content/Context;)V

    return-object v0
.end method

.method protected getCallerSyncAdapter()Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    const-string v0, "caller_is_syncadapter"

    return-object v0
.end method

.method protected getCid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    const-string v0, "X08g96bD1C"

    return-object v0
.end method

.method protected getContentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 104
    sget-object v0, Lcom/sec/android/sCloudSync/Constants/SMemoContract;->SMEMO_URI:Landroid/net/Uri;

    return-object v0
.end method

.method protected getCtidKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1028
    const-string v0, "SM"

    return-object v0
.end method

.method protected getDeletedColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    const-string v0, "deleted"

    return-object v0
.end method

.method protected getDirtyColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 137
    const-string v0, "dirty"

    return-object v0
.end method

.method public getFolderColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 177
    const-string v0, "IsFolder"

    return-object v0
.end method

.method protected getIdColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    const-string v0, "_id"

    return-object v0
.end method

.method protected getKeyColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    const-string v0, "sync1"

    return-object v0
.end method

.method protected getLocalUpdatesSelection()Ljava/lang/String;
    .locals 2

    .prologue
    .line 110
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getAccountName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v1

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getAccountType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v1

    iget-object v1, v1, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getDirtyColumnName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = 1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getProjection()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 167
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getIdColumnName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getKeyColumnName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getTimeStampColumnName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getDeletedColumnName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getDirtyColumnName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    return-object v0
.end method

.method public getSwitherImageColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 173
    const-string v0, "SwitcherImage"

    return-object v0
.end method

.method protected getSyncAdapterName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    const-string v0, "SMEMO"

    return-object v0
.end method

.method protected getSyncStateDataColumn()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getSyncStateURI()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    const-string v0, "SMemoSyncAdapter"

    return-object v0
.end method

.method protected getTimeStampColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    const-string v0, "sync2"

    return-object v0
.end method

.method protected removePreSyncedRecords(Ljava/util/Map;)Z
    .locals 23
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/sCloudSync/Records/KVSItem;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 182
    .local p1, "uServerRecords":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/sec/android/sCloudSync/Records/KVSItem;>;"
    if-eqz p1, :cond_0

    invoke-interface/range {p1 .. p1}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 183
    :cond_0
    const/4 v2, 0x0

    .line 256
    :goto_0
    return v2

    .line 185
    :cond_1
    const/16 v19, 0x0

    .line 186
    .local v19, "preCursor":Landroid/database/Cursor;
    invoke-interface/range {p1 .. p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v16

    .line 187
    .local v16, "keyset":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-interface/range {v16 .. v16}, Ljava/util/Set;->size()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    mul-int/lit16 v2, v2, 0x104

    move-object/from16 v0, v22

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 188
    .local v22, "where":Ljava/lang/StringBuilder;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getKeyColumnName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " IN ("

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 189
    move-object/from16 v0, v22

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Tools/AppTool;->appendIds(Ljava/lang/StringBuilder;Ljava/util/Set;)V

    .line 190
    const/16 v2, 0x29

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 192
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getContentUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getCallerSyncAdapter()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getAccountName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v5

    iget-object v5, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getAccountType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v5

    iget-object v5, v5, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 197
    .local v3, "uriFromSyncAdapter":Landroid/net/Uri;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getProjection()[Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v19

    .line 203
    if-nez v19, :cond_2

    .line 204
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string v4, "RemovePreSyncedRecords: (ERROR)Cursor is null"

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 198
    :catch_0
    move-exception v12

    .line 199
    .local v12, "e":Landroid/os/RemoteException;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "RemovePreSyncedRecords: (Exception)Exception in calling query "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v12}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 208
    .end local v12    # "e":Landroid/os/RemoteException;
    :cond_2
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-nez v2, :cond_3

    .line 209
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 210
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string v4, "RemovePreSyncedRecords: there is nothing to be removed."

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 214
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getIdColumnName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    .line 215
    .local v13, "id_index":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getKeyColumnName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    .line 216
    .local v15, "key_index":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getTimeStampColumnName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    .line 217
    .local v21, "time_index":I
    const-string v2, "dirty"

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    .line 218
    .local v11, "dirtyIndex":I
    const-string v2, "deleted"

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 221
    .local v9, "deletedIndex":I
    :cond_4
    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v17

    .line 222
    .local v17, "modifiedTimeUploadedtoServer":J
    move-object/from16 v0, v19

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 223
    .local v14, "key":Ljava/lang/String;
    move-object/from16 v0, v19

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 224
    .local v10, "dirtyField":I
    move-object/from16 v0, v19

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 226
    .local v8, "deletedField":I
    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/android/sCloudSync/Records/KVSItem;

    .line 227
    .local v20, "serverItem":Lcom/sec/android/sCloudSync/Records/KVSItem;
    if-eqz v20, :cond_8

    .line 228
    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getTimeStamp()J

    move-result-wide v4

    cmp-long v2, v17, v4

    if-ltz v2, :cond_7

    .line 229
    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/sCloudSync/Records/KVSItem;->isDeleted()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 233
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v4, Lcom/sec/android/sCloudSync/Constants/SMemoContract;->FILETRACKER_URI:Landroid/net/Uri;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "sMemoId=\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v2, v4, v5, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 239
    :cond_5
    if-nez v10, :cond_6

    const/4 v2, 0x1

    if-ne v8, v2, :cond_6

    .line 241
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getKeyColumnName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentProviderClient;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 254
    :goto_1
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_4

    .line 255
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 256
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 242
    :catch_1
    move-exception v12

    .line 243
    .restart local v12    # "e":Landroid/os/RemoteException;
    invoke-virtual {v12}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 246
    .end local v12    # "e":Landroid/os/RemoteException;
    :cond_6
    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 249
    :cond_7
    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/sCloudSync/Records/KVSItem;

    move-object/from16 v0, v19

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/sec/android/sCloudSync/Records/KVSItem;->setID(J)V

    goto :goto_1

    .line 252
    :cond_8
    const-string v2, "SMemoSyncAdapter"

    const-string v4, "Sync Broken : There is a duplicated record on Local."

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public updateLocalDb(Landroid/content/SyncStats;)V
    .locals 24
    .param p1, "stats"    # Landroid/content/SyncStats;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .line 536
    invoke-super/range {p0 .. p1}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->updateLocalDb(Landroid/content/SyncStats;)V

    .line 538
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v2

    const-string v8, "mounted"

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 540
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->resetLastSyncTime()V

    .line 541
    new-instance v2, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/16 v8, 0x8

    invoke-direct {v2, v8}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v2

    .line 544
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    if-nez v2, :cond_1

    .line 545
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string v8, "SMemo Builder is NULL"

    invoke-static {v2, v8}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 614
    :goto_0
    return-void

    .line 549
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    check-cast v2, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;

    invoke-virtual {v2}, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->getFileDownloadList()Ljava/util/HashMap;

    move-result-object v11

    .line 550
    .local v11, "fileDownloadList":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    new-instance v14, Ljava/util/HashSet;

    invoke-direct {v14}, Ljava/util/HashSet;-><init>()V

    .line 551
    .local v14, "failedDownloads":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v16, Ljava/util/HashSet;

    invoke-direct/range {v16 .. v16}, Ljava/util/HashSet;-><init>()V

    .line 552
    .local v16, "handledFailDownloads":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v11}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v20

    .line 554
    .local v20, "keySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 555
    .local v6, "delFileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 557
    .local v7, "checkSumMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface/range {v20 .. v20}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v19

    .local v19, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 558
    .local v4, "key":Ljava/lang/String;
    invoke-interface {v11, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    .line 559
    .local v5, "fileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/16 v17, 0x0

    .line 560
    .local v17, "hasDownloadFailed":Z
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->mServerChangedRecords:Ljava/util/Map;

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/sCloudSync/Records/KVSItem;

    .line 561
    .local v3, "item":Lcom/sec/android/sCloudSync/Records/KVSItem;
    invoke-interface {v6}, Ljava/util/List;->clear()V

    .line 562
    invoke-interface {v7}, Ljava/util/Map;->clear()V

    move-object/from16 v2, p0

    .line 568
    invoke-direct/range {v2 .. v7}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->compareFileCheckSum(Lcom/sec/android/sCloudSync/Records/KVSItem;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/Map;)V

    move-object/from16 v8, p0

    move-object v9, v3

    move-object v10, v4

    move-object v12, v5

    move-object v13, v7

    .line 571
    invoke-direct/range {v8 .. v14}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->downloadFile(Lcom/sec/android/sCloudSync/Records/KVSItem;Ljava/lang/String;Ljava/util/Map;Ljava/util/List;Ljava/util/Map;Ljava/util/Set;)Z

    move-result v2

    if-nez v2, :cond_3

    const/16 v17, 0x1

    .line 577
    :goto_2
    if-nez v17, :cond_2

    .line 578
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4, v7}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->updateFileTracker(Lcom/sec/android/sCloudSync/Records/KVSItem;Ljava/lang/String;Ljava/util/Map;)V

    .line 581
    :cond_2
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v3, v5, v6, v1}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->deleteLocalFiles(Lcom/sec/android/sCloudSync/Records/KVSItem;Ljava/util/List;Ljava/util/List;Z)V

    goto :goto_1

    .line 571
    :cond_3
    const/16 v17, 0x0

    goto :goto_2

    .line 585
    .end local v3    # "item":Lcom/sec/android/sCloudSync/Records/KVSItem;
    .end local v4    # "key":Ljava/lang/String;
    .end local v5    # "fileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v17    # "hasDownloadFailed":Z
    :cond_4
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v14, v1, v11}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->handleFailedDownload(Ljava/util/Set;Ljava/util/Set;Ljava/util/Map;)Z

    move-result v18

    .line 588
    .local v18, "hasInsertFailed":Z
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    invoke-virtual {v2}, Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;->doApplyBatch()Z

    .line 591
    if-eqz v18, :cond_5

    .line 592
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1, v11}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->handleInsertFail(Ljava/util/Set;Ljava/util/Map;)V

    .line 595
    :cond_5
    if-eqz v11, :cond_6

    invoke-interface {v11}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    .line 596
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getContentUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getCallerSyncAdapter()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getAccountName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v9

    iget-object v9, v9, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v8, v9}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getAccountType()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v9

    iget-object v9, v9, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v2, v8, v9}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v21

    .line 600
    .local v21, "memoUri":Landroid/net/Uri;
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-interface {v11}, Ljava/util/Map;->size()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    mul-int/lit16 v2, v2, 0x104

    move-object/from16 v0, v23

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 601
    .local v23, "whereSuccess":Ljava/lang/StringBuilder;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getKeyColumnName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v8, " IN ("

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 602
    invoke-interface {v11}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    move-object/from16 v0, v23

    invoke-static {v0, v2}, Lcom/sec/android/sCloudSync/Tools/AppTool;->appendIds(Ljava/lang/StringBuilder;Ljava/util/Set;)V

    .line 603
    const/16 v2, 0x29

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 605
    new-instance v22, Landroid/content/ContentValues;

    invoke-direct/range {v22 .. v22}, Landroid/content/ContentValues;-><init>()V

    .line 606
    .local v22, "values":Landroid/content/ContentValues;
    const-string v2, "deleted"

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    move-object/from16 v0, v22

    invoke-virtual {v0, v2, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 608
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v2

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v2, v0, v1, v8, v9}, Landroid/content/ContentProviderClient;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 613
    .end local v21    # "memoUri":Landroid/net/Uri;
    .end local v22    # "values":Landroid/content/ContentValues;
    .end local v23    # "whereSuccess":Ljava/lang/StringBuilder;
    :cond_6
    :goto_3
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->fixParentIds(Landroid/content/SyncStats;)V

    goto/16 :goto_0

    .line 609
    .restart local v21    # "memoUri":Landroid/net/Uri;
    .restart local v22    # "values":Landroid/content/ContentValues;
    .restart local v23    # "whereSuccess":Ljava/lang/StringBuilder;
    :catch_0
    move-exception v15

    .line 610
    .local v15, "e":Landroid/os/RemoteException;
    invoke-virtual {v15}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_3
.end method

.method public updatetoServer(Ljava/util/List;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/sCloudSync/Records/RecordSetItem;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/sCloudSync/Records/RecordBase;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;,
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 871
    .local p1, "recordstoSetList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordSetItem;>;"
    .local p2, "recordsToDeleteList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordBase;>;"
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mounted"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 872
    new-instance v0, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v0

    .line 876
    :cond_0
    invoke-direct {p0, p2}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->deleteFromServerORS(Ljava/util/List;)V

    .line 882
    invoke-direct {p0, p1}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;->uploadToServerORS(Ljava/util/List;)V

    .line 884
    invoke-super {p0, p1, p2}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->updatetoServer(Ljava/util/List;Ljava/util/List;)V

    .line 885
    return-void
.end method
