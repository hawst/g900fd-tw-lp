.class public Lcom/sec/android/sCloudSync/Receivers/TimeChangedReceiver;
.super Landroid/content/BroadcastReceiver;
.source "TimeChangedReceiver.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "TimeChangedReceiver"


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/sCloudSync/Receivers/TimeChangedReceiver;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/sCloudSync/Receivers/TimeChangedReceiver;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Receivers/TimeChangedReceiver;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 40
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 41
    .local v0, "action":Ljava/lang/String;
    iput-object p1, p0, Lcom/sec/android/sCloudSync/Receivers/TimeChangedReceiver;->mContext:Landroid/content/Context;

    .line 42
    if-nez v0, :cond_1

    .line 80
    :cond_0
    :goto_0
    return-void

    .line 45
    :cond_1
    const-string v2, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 55
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/sec/android/sCloudSync/Receivers/TimeChangedReceiver$1;

    invoke-direct {v2, p0}, Lcom/sec/android/sCloudSync/Receivers/TimeChangedReceiver$1;-><init>(Lcom/sec/android/sCloudSync/Receivers/TimeChangedReceiver;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 61
    .local v1, "workerThread":Ljava/lang/Thread;
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 62
    .end local v1    # "workerThread":Ljava/lang/Thread;
    :cond_2
    const-string v2, "android.intent.action.TIME_SET"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 67
    :cond_3
    const-string v2, "TimeChangedReceiver"

    const-string v3, "Inside the time changed intent"

    invoke-static {v2, v3}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/sec/android/sCloudSync/Receivers/TimeChangedReceiver$2;

    invoke-direct {v2, p0}, Lcom/sec/android/sCloudSync/Receivers/TimeChangedReceiver$2;-><init>(Lcom/sec/android/sCloudSync/Receivers/TimeChangedReceiver;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 74
    .restart local v1    # "workerThread":Ljava/lang/Thread;
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method
