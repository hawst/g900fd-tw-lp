.class public abstract Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;
.super Ljava/lang/Object;
.source "AbstractSyncAdapter.java"


# static fields
.field private static final DELETE_FROM_SERVER:I = 0x1

.field private static final DELIMTER:Ljava/lang/String; = "__"

.field private static final GET_KEYS_SIZE:I = 0xfa

.field private static final MAX_DOWNLOAD:J = 0x100000L

.field private static final MAX_UPLOAD:J = 0x100000L

.field private static final SIZE:I = 0x19

.field private static final SYNC_BASE_KEY:Ljava/lang/String; = "DATASYNC"

.field protected static final SYNC_META_DATA:Ljava/lang/String; = "SyncMetaData"

.field private static final TOO_OLD_TIME_STAMP:I = 0x4e22

.field private static final TOO_YOUNG_TIME_STAMP:I = 0x714e

.field private static final UPDATE_TO_SERVER:I

.field private static mClientDeviceId:Ljava/lang/String;


# instance fields
.field private mAccount:Landroid/accounts/Account;

.field private mAuthControl:Lcom/sec/android/sCloudSync/Auth/IAuthControl;

.field protected mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

.field private mContext:Landroid/content/Context;

.field private mLastSyncTime:Ljava/lang/String;

.field protected mLocalChangedRecords:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mProvider:Landroid/content/ContentProviderClient;

.field protected mServerChangedRecords:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/sCloudSync/Records/KVSItem;",
            ">;"
        }
    .end annotation
.end field

.field private mServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

.field private mTimeManager:Lcom/samsung/android/scloud/framework/util/TimeManager;

.field protected mbSyncCanceled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mClientDeviceId:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cloudServiceManager"    # Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    .prologue
    const/4 v1, 0x0

    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    iput-object v1, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    .line 99
    iput-object v1, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mLocalChangedRecords:Ljava/util/Map;

    .line 100
    iput-object v1, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mServerChangedRecords:Ljava/util/Map;

    .line 102
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mbSyncCanceled:Z

    .line 104
    iput-object v1, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mAuthControl:Lcom/sec/android/sCloudSync/Auth/IAuthControl;

    .line 105
    iput-object v1, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mTimeManager:Lcom/samsung/android/scloud/framework/util/TimeManager;

    .line 107
    iput-object v1, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    .line 127
    iput-object p1, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mContext:Landroid/content/Context;

    .line 129
    sget-object v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mClientDeviceId:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/sCloudSync/Tools/AppTool;->getClientDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mClientDeviceId:Ljava/lang/String;

    .line 133
    :cond_0
    new-instance v0, Lcom/sec/android/sCloudSync/Auth/AuthControlForNewDataRelay;

    invoke-direct {v0}, Lcom/sec/android/sCloudSync/Auth/AuthControlForNewDataRelay;-><init>()V

    iput-object v0, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mAuthControl:Lcom/sec/android/sCloudSync/Auth/IAuthControl;

    .line 135
    invoke-static {}, Lcom/samsung/android/scloud/framework/util/TimeManager;->create()Lcom/samsung/android/scloud/framework/util/TimeManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mTimeManager:Lcom/samsung/android/scloud/framework/util/TimeManager;

    .line 136
    iput-object p2, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    .line 137
    return-void
.end method

.method private addToDownloadKeyList(Lcom/sec/android/sCloudSync/Records/KVSResponse;Ljava/util/Map;)Z
    .locals 12
    .param p1, "response"    # Lcom/sec/android/sCloudSync/Records/KVSResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/sCloudSync/Records/KVSResponse;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/sCloudSync/Records/KVSItem;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 482
    .local p2, "serverChanges":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/sec/android/sCloudSync/Records/KVSItem;>;"
    invoke-virtual {p1}, Lcom/sec/android/sCloudSync/Records/KVSResponse;->getKeyDetailList()Ljava/util/List;

    move-result-object v11

    .line 484
    .local v11, "updates":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordKey;>;"
    if-eqz v11, :cond_0

    invoke-interface {v11}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 485
    :cond_0
    const/4 v1, 0x0

    .line 493
    :goto_0
    return v1

    .line 487
    :cond_1
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/sCloudSync/Records/RecordKey;

    .line 488
    .local v10, "record":Lcom/sec/android/sCloudSync/Records/RecordKey;
    invoke-virtual {v10}, Lcom/sec/android/sCloudSync/Records/RecordKey;->getKEY()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getSyncAdapterName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 489
    new-instance v0, Lcom/sec/android/sCloudSync/Records/KVSItem;

    const-wide/16 v1, -0x1

    invoke-virtual {v10}, Lcom/sec/android/sCloudSync/Records/RecordKey;->isDeleted()Z

    move-result v3

    invoke-virtual {v10}, Lcom/sec/android/sCloudSync/Records/RecordKey;->getTimeStamp()Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const/4 v6, 0x0

    invoke-virtual {v10}, Lcom/sec/android/sCloudSync/Records/RecordKey;->getSize()J

    move-result-wide v7

    invoke-direct/range {v0 .. v8}, Lcom/sec/android/sCloudSync/Records/KVSItem;-><init>(JZJLjava/lang/String;J)V

    .line 490
    .local v0, "item":Lcom/sec/android/sCloudSync/Records/KVSItem;
    invoke-virtual {v10}, Lcom/sec/android/sCloudSync/Records/RecordKey;->getKEY()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 493
    .end local v0    # "item":Lcom/sec/android/sCloudSync/Records/KVSItem;
    .end local v10    # "record":Lcom/sec/android/sCloudSync/Records/RecordKey;
    :cond_3
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private deleteFromServer(Ljava/util/List;Ljava/util/List;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/sCloudSync/Records/RecordBase;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .line 739
    .local p1, "deleteList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordBase;>;"
    .local p2, "operations":Ljava/util/List;, "Ljava/util/List<Landroid/content/ContentProviderOperation;>;"
    const/4 v10, 0x0

    .line 740
    .local v10, "start":I
    const/16 v7, 0x19

    .line 741
    .local v7, "maxPostSize":I
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v9

    .line 742
    .local v9, "size":I
    const/4 v6, 0x0

    .line 744
    .local v6, "end":I
    const/4 v1, 0x0

    .line 746
    .local v1, "kvsResponse":Lcom/sec/android/sCloudSync/Records/KVSResponse;
    :goto_0
    if-le v9, v10, :cond_2

    .line 747
    iget-boolean v0, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mbSyncCanceled:Z

    if-eqz v0, :cond_0

    .line 748
    new-instance v0, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/16 v2, 0x9

    invoke-direct {v0, v2}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v0

    .line 750
    :cond_0
    add-int/lit8 v6, v10, 0x19

    .line 752
    if-ge v9, v6, :cond_1

    .line 753
    move v6, v9

    .line 756
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "2101 Record Size is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 758
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getCloudServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    move-result-object v8

    .line 759
    .local v8, "recordServiceManager":Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;
    invoke-interface {v8}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getRecordServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/IRecordServiceManager;

    move-result-object v0

    invoke-interface {p1, v10, v6}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/sec/android/sCloudSync/ServiceManagers/IRecordServiceManager;->deleteItems(Ljava/util/List;)Lcom/sec/android/sCloudSync/Records/KVSResponse;

    move-result-object v1

    .line 761
    const/4 v3, 0x1

    sub-int v0, v6, v10

    int-to-long v4, v0

    move-object v0, p0

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->handleResponse(Lcom/sec/android/sCloudSync/Records/KVSResponse;Ljava/util/List;IJ)V

    .line 762
    move v10, v6

    .line 763
    goto :goto_0

    .line 764
    .end local v8    # "recordServiceManager":Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;
    :cond_2
    return-void
.end method

.method private doApplyBatch(Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 335
    .local p1, "operations":Ljava/util/List;, "Ljava/util/List<Landroid/content/ContentProviderOperation;>;"
    if-nez p1, :cond_1

    .line 362
    :cond_0
    return-void

    .line 338
    :cond_1
    iget-object v6, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    if-nez v6, :cond_2

    .line 339
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getBuilder()Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    .line 342
    :cond_2
    const/4 v4, 0x0

    .line 343
    .local v4, "start":I
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    .line 344
    .local v3, "size":I
    iget-object v6, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    invoke-virtual {v6}, Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;->getBatchSize()I

    move-result v2

    .line 345
    .local v2, "maxBatchSize":I
    const/4 v1, 0x0

    .line 347
    .local v1, "end":I
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 348
    .local v5, "subOperations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :goto_0
    if-le v3, v4, :cond_0

    .line 349
    add-int v1, v4, v2

    .line 350
    if-ge v3, v1, :cond_3

    .line 351
    move v1, v3

    .line 353
    :cond_3
    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 354
    invoke-interface {p1, v4, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 356
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mProvider:Landroid/content/ContentProviderClient;

    invoke-virtual {v6, v5}, Landroid/content/ContentProviderClient;->applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 360
    :goto_1
    move v4, v1

    goto :goto_0

    .line 357
    :catch_0
    move-exception v0

    .line 358
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "doApplyBatch: Exception is"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private getAllkeys(Ljava/util/Map;)Ljava/util/Map;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/sCloudSync/Records/KVSItem;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/sCloudSync/Records/KVSItem;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .line 438
    .local p1, "serverChanges":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/sec/android/sCloudSync/Records/KVSItem;>;"
    const/4 v5, 0x0

    .line 439
    .local v5, "response":Lcom/sec/android/sCloudSync/Records/KVSResponse;
    const-string v9, ""

    .line 441
    .local v9, "startKey":Ljava/lang/String;
    :cond_0
    iget-boolean v10, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mbSyncCanceled:Z

    if-eqz v10, :cond_1

    .line 442
    new-instance v10, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/16 v11, 0x9

    invoke-direct {v10, v11}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v10

    .line 444
    :cond_1
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getCloudServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    move-result-object v4

    .line 445
    .local v4, "recordServiceManager":Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;
    invoke-interface {v4}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getRecordServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/IRecordServiceManager;

    move-result-object v10

    const/16 v11, 0xfa

    invoke-interface {v10, v9, v11}, Lcom/sec/android/sCloudSync/ServiceManagers/IRecordServiceManager;->getKeys(Ljava/lang/String;I)Lcom/sec/android/sCloudSync/Records/KVSResponse;
    :try_end_0
    .catch Lcom/sec/android/sCloudSync/Util/SyncException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 451
    invoke-virtual {v5}, Lcom/sec/android/sCloudSync/Records/KVSResponse;->getResponseCode()I

    move-result v6

    .line 452
    .local v6, "ret_code":I
    if-nez v6, :cond_5

    .line 453
    invoke-direct {p0, v5, p1}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->addToDownloadKeyList(Lcom/sec/android/sCloudSync/Records/KVSResponse;Ljava/util/Map;)Z

    move-result v10

    if-nez v10, :cond_2

    .line 454
    const/4 v2, 0x0

    .line 478
    :goto_0
    return-object v2

    .line 446
    .end local v4    # "recordServiceManager":Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;
    .end local v6    # "ret_code":I
    :catch_0
    move-exception v0

    .line 447
    .local v0, "e":Lcom/sec/android/sCloudSync/Util/SyncException;
    invoke-virtual {v0}, Lcom/sec/android/sCloudSync/Util/SyncException;->printStackTrace()V

    .line 448
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "getAllkeys:Sync exception recieved :- "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v0}, Lcom/sec/android/sCloudSync/Util/SyncException;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 449
    throw v0

    .line 456
    .end local v0    # "e":Lcom/sec/android/sCloudSync/Util/SyncException;
    .restart local v4    # "recordServiceManager":Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;
    .restart local v6    # "ret_code":I
    :cond_2
    invoke-virtual {v5}, Lcom/sec/android/sCloudSync/Records/KVSResponse;->getNextKey()Ljava/lang/String;

    move-result-object v9

    .line 457
    invoke-virtual {v5}, Lcom/sec/android/sCloudSync/Records/KVSResponse;->getMaxTimeStamp()Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mLastSyncTime:Ljava/lang/String;

    .line 462
    :goto_1
    invoke-virtual {v5}, Lcom/sec/android/sCloudSync/Records/KVSResponse;->getNextKey()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_3

    invoke-virtual {v5}, Lcom/sec/android/sCloudSync/Records/KVSResponse;->getNextKey()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    if-nez v10, :cond_0

    .line 463
    :cond_3
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 464
    .local v7, "splitServerChanges":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/sec/android/sCloudSync/Records/KVSItem;>;"
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 465
    .local v2, "finalServerChanges":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/sec/android/sCloudSync/Records/KVSItem;>;"
    const/4 v8, 0x0

    .line 466
    .local v8, "splitSize":I
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_4
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 467
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/sec/android/sCloudSync/Records/KVSItem;>;"
    add-int/lit8 v8, v8, 0x1

    .line 468
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v10

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v11

    invoke-interface {v7, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 469
    invoke-direct {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getSplitServerChangeSize()I

    move-result v10

    if-ne v8, v10, :cond_4

    .line 470
    invoke-virtual {p0, v7}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->removePreSyncedRecords(Ljava/util/Map;)Z

    .line 471
    invoke-interface {v2, v7}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 472
    invoke-interface {v7}, Ljava/util/Map;->clear()V

    .line 473
    const/4 v8, 0x0

    goto :goto_2

    .line 459
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/sec/android/sCloudSync/Records/KVSItem;>;"
    .end local v2    # "finalServerChanges":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/sec/android/sCloudSync/Records/KVSItem;>;"
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v7    # "splitServerChanges":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/sec/android/sCloudSync/Records/KVSItem;>;"
    .end local v8    # "splitSize":I
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "getAllkeys: GetKeys returned"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v5}, Lcom/sec/android/sCloudSync/Records/KVSResponse;->getResponseCode()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 476
    .restart local v2    # "finalServerChanges":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/sec/android/sCloudSync/Records/KVSItem;>;"
    .restart local v3    # "i$":Ljava/util/Iterator;
    .restart local v7    # "splitServerChanges":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/sec/android/sCloudSync/Records/KVSItem;>;"
    .restart local v8    # "splitSize":I
    :cond_6
    invoke-virtual {p0, v7}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->removePreSyncedRecords(Ljava/util/Map;)Z

    .line 477
    invoke-interface {v2, v7}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    goto/16 :goto_0
.end method

.method private getItemsFromKeys(Ljava/util/List;Landroid/content/SyncStats;)V
    .locals 18
    .param p2, "stats"    # Landroid/content/SyncStats;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/content/SyncStats;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .line 266
    .local p1, "keys":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v12

    .line 268
    .local v12, "size":I
    const/4 v13, 0x0

    .line 269
    .local v13, "start":I
    const/16 v8, 0x19

    .line 270
    .local v8, "maxGetSize":I
    const/4 v4, 0x0

    .line 271
    .local v4, "end":I
    const/4 v7, 0x0

    .line 272
    .local v7, "itemResponse":Lcom/sec/android/sCloudSync/Records/KVSResponse;
    :cond_0
    :goto_0
    if-le v12, v13, :cond_4

    .line 273
    add-int/lit8 v4, v13, 0x19

    .line 275
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mbSyncCanceled:Z

    if-eqz v14, :cond_1

    .line 276
    new-instance v14, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/16 v15, 0x9

    invoke-direct {v14, v15}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v14

    .line 279
    :cond_1
    if-ge v12, v4, :cond_2

    .line 280
    move v4, v12

    .line 283
    :cond_2
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v14

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "2104 Record Size "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getCloudServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    move-result-object v10

    .line 286
    .local v10, "recordServiceManager":Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;
    invoke-interface {v10}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getRecordServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/IRecordServiceManager;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-interface {v0, v13, v4}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v15

    invoke-interface {v14, v15}, Lcom/sec/android/sCloudSync/ServiceManagers/IRecordServiceManager;->getItems(Ljava/util/List;)Lcom/sec/android/sCloudSync/Records/KVSResponse;
    :try_end_0
    .catch Lcom/sec/android/sCloudSync/Util/SyncException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 287
    move v13, v4

    .line 294
    invoke-virtual {v7}, Lcom/sec/android/sCloudSync/Records/KVSResponse;->getResponseCode()I

    move-result v11

    .line 296
    .local v11, "ret_code":I
    if-nez v11, :cond_0

    .line 297
    invoke-virtual {v7}, Lcom/sec/android/sCloudSync/Records/KVSResponse;->getItemDetailsList()Ljava/util/List;

    move-result-object v2

    .line 298
    .local v2, "dataRecords":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordItem;>;"
    if-eqz v2, :cond_3

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v14

    if-eqz v14, :cond_5

    .line 299
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v14

    const-string v15, "UpdateLocalDb: No records for KEY List "

    invoke-static {v14, v15}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    .end local v2    # "dataRecords":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordItem;>;"
    .end local v10    # "recordServiceManager":Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;
    .end local v11    # "ret_code":I
    :cond_4
    return-void

    .line 288
    :catch_0
    move-exception v3

    .line 289
    .local v3, "e":Lcom/sec/android/sCloudSync/Util/SyncException;
    invoke-virtual {v3}, Lcom/sec/android/sCloudSync/Util/SyncException;->printStackTrace()V

    .line 290
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v14

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "UpdateLocalDb: Sync exception recieved :- "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v3}, Lcom/sec/android/sCloudSync/Util/SyncException;->getMessage()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    throw v3

    .line 303
    .end local v3    # "e":Lcom/sec/android/sCloudSync/Util/SyncException;
    .restart local v2    # "dataRecords":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordItem;>;"
    .restart local v10    # "recordServiceManager":Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;
    .restart local v11    # "ret_code":I
    :cond_5
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_6
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_b

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/sCloudSync/Records/RecordItem;

    .line 304
    .local v9, "record":Lcom/sec/android/sCloudSync/Records/RecordItem;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mServerChangedRecords:Ljava/util/Map;

    invoke-virtual {v9}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getKEY()Ljava/lang/String;

    move-result-object v15

    invoke-interface {v14, v15}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/sCloudSync/Records/KVSItem;

    .line 305
    .local v6, "item":Lcom/sec/android/sCloudSync/Records/KVSItem;
    if-nez v6, :cond_7

    .line 306
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v14

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "GetItems retured a key which is not found in list of serverkeys. Key = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v9}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getKEY()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 310
    :cond_7
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mbSyncCanceled:Z

    if-eqz v14, :cond_8

    .line 311
    new-instance v14, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/16 v15, 0x9

    invoke-direct {v14, v15}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v14

    .line 313
    :cond_8
    invoke-virtual {v6}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getID()J

    move-result-wide v14

    const-wide/16 v16, 0x0

    cmp-long v14, v14, v16

    if-lez v14, :cond_a

    .line 316
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v6, v9, v1}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->doUpdateDelete(Lcom/sec/android/sCloudSync/Records/KVSItem;Lcom/sec/android/sCloudSync/Records/RecordItem;Landroid/content/SyncStats;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 324
    :cond_9
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mbSyncCanceled:Z

    if-eqz v14, :cond_6

    .line 325
    new-instance v14, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/16 v15, 0x9

    invoke-direct {v14, v15}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v14

    .line 318
    :cond_a
    invoke-virtual {v9}, Lcom/sec/android/sCloudSync/Records/RecordItem;->isDeleted()Z

    move-result v14

    if-nez v14, :cond_9

    .line 320
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v6, v9, v1}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->doInsert(Lcom/sec/android/sCloudSync/Records/KVSItem;Lcom/sec/android/sCloudSync/Records/RecordItem;Landroid/content/SyncStats;)Z

    move-result v14

    if-nez v14, :cond_9

    goto :goto_1

    .line 328
    .end local v6    # "item":Lcom/sec/android/sCloudSync/Records/KVSItem;
    .end local v9    # "record":Lcom/sec/android/sCloudSync/Records/RecordItem;
    :cond_b
    invoke-interface {v2}, Ljava/util/List;->clear()V

    goto/16 :goto_0
.end method

.method private getLastSyncTime(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 17
    .param p1, "syncAdapterName"    # Ljava/lang/String;
    .param p2, "bRaw"    # Z

    .prologue
    .line 690
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getSyncStateURI()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getSyncStateDataColumn()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 691
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getSyncStateURI()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getCallerSyncAdapter()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 692
    .local v2, "uri":Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getSyncAdapterName()Ljava/lang/String;

    move-result-object v7

    .line 693
    .local v7, "adapterName":Ljava/lang/String;
    const/4 v1, 0x1

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getSyncStateDataColumn()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v1

    .line 694
    .local v3, "projection":[Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getAccountType()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "= \'"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mAccount:Landroid/accounts/Account;

    iget-object v5, v5, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "\'"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 695
    .local v4, "where":Ljava/lang/String;
    const/4 v14, 0x0

    .line 697
    .local v14, "syncMetaCursor":Landroid/database/Cursor;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mProvider:Landroid/content/ContentProviderClient;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v14

    .line 707
    :goto_0
    if-nez v14, :cond_0

    .line 708
    const/4 v11, 0x0

    .line 733
    .end local v2    # "uri":Landroid/net/Uri;
    .end local v3    # "projection":[Ljava/lang/String;
    .end local v4    # "where":Ljava/lang/String;
    .end local v7    # "adapterName":Ljava/lang/String;
    .end local v14    # "syncMetaCursor":Landroid/database/Cursor;
    :goto_1
    return-object v11

    .line 698
    .restart local v2    # "uri":Landroid/net/Uri;
    .restart local v3    # "projection":[Ljava/lang/String;
    .restart local v4    # "where":Ljava/lang/String;
    .restart local v7    # "adapterName":Ljava/lang/String;
    .restart local v14    # "syncMetaCursor":Landroid/database/Cursor;
    :catch_0
    move-exception v9

    .line 699
    .local v9, "e":Landroid/os/RemoteException;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Exception while reading TimeStamp"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v9}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 700
    .end local v9    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v9

    .line 701
    .local v9, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Exception while reading TimeStamp"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v9}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 702
    .end local v9    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v9

    .line 703
    .local v9, "e":Ljava/lang/IllegalStateException;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Exception while reading TimeStamp"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v9}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 704
    .end local v9    # "e":Ljava/lang/IllegalStateException;
    :catch_3
    move-exception v9

    .line 705
    .local v9, "e":Ljava/lang/UnsupportedOperationException;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Exception while reading TimeStamp"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v9}, Ljava/lang/UnsupportedOperationException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 710
    .end local v9    # "e":Ljava/lang/UnsupportedOperationException;
    :cond_0
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 711
    const/4 v1, 0x0

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 712
    .local v11, "lastSyncTime":Ljava/lang/String;
    const/4 v1, 0x1

    move/from16 v0, p2

    if-ne v0, v1, :cond_1

    .line 713
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    .line 717
    :cond_1
    if-eqz v11, :cond_0

    invoke-virtual {v11, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 718
    const-string v1, "__"

    invoke-virtual {v11, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v15

    .line 719
    .local v15, "syncTimes":[Ljava/lang/String;
    move-object v8, v15

    .local v8, "arr$":[Ljava/lang/String;
    array-length v12, v8

    .local v12, "len$":I
    const/4 v10, 0x0

    .local v10, "i$":I
    :goto_2
    if-ge v10, v12, :cond_0

    aget-object v16, v8, v10

    .line 720
    .local v16, "time":Ljava/lang/String;
    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 721
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v1

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v16

    .line 722
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    move-object/from16 v11, v16

    .line 723
    goto/16 :goto_1

    .line 719
    :cond_2
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 728
    .end local v8    # "arr$":[Ljava/lang/String;
    .end local v10    # "i$":I
    .end local v11    # "lastSyncTime":Ljava/lang/String;
    .end local v12    # "len$":I
    .end local v15    # "syncTimes":[Ljava/lang/String;
    .end local v16    # "time":Ljava/lang/String;
    :cond_3
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 733
    const/4 v11, 0x0

    goto/16 :goto_1

    .line 730
    .end local v2    # "uri":Landroid/net/Uri;
    .end local v3    # "projection":[Ljava/lang/String;
    .end local v4    # "where":Ljava/lang/String;
    .end local v7    # "adapterName":Ljava/lang/String;
    .end local v14    # "syncMetaCursor":Landroid/database/Cursor;
    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mContext:Landroid/content/Context;

    const-string v5, "SyncMetaData"

    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v13

    .line 731
    .local v13, "syncMeta":Landroid/content/SharedPreferences;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getSyncAdapterName()Ljava/lang/String;

    move-result-object v1

    const/4 v5, 0x0

    invoke-interface {v13, v1, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    goto/16 :goto_1
.end method

.method private getLocalUpdates()V
    .locals 36
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .line 934
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getLocalupdatesUri()Landroid/net/Uri;

    move-result-object v4

    .line 935
    .local v4, "uri":Landroid/net/Uri;
    const-wide/16 v12, 0x0

    .line 937
    .local v12, "uploadsize":J
    const/16 v21, 0x0

    .line 938
    .local v21, "mDeletedIndex":I
    const/16 v23, 0x0

    .line 939
    .local v23, "mKeyIndex":I
    const/16 v22, 0x0

    .line 940
    .local v22, "mIdIndex":I
    const/16 v27, 0x0

    .line 942
    .local v27, "mTimestampIndex":I
    const/16 v20, -0x1

    .line 943
    .local v20, "mDeleted":I
    const/16 v24, 0x0

    .line 944
    .local v24, "mSyncKey":Ljava/lang/String;
    const-wide/16 v9, -0x1

    .line 945
    .local v9, "mRowId":J
    const-wide/16 v25, 0x0

    .line 947
    .local v25, "mTimeStamp":J
    const/16 v28, 0x0

    .line 948
    .local v28, "mTotalSize":I
    new-instance v29, Ljava/util/ArrayList;

    invoke-direct/range {v29 .. v29}, Ljava/util/ArrayList;-><init>()V

    .line 949
    .local v29, "recordsToDeleteList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordBase;>;"
    new-instance v30, Ljava/util/ArrayList;

    invoke-direct/range {v30 .. v30}, Ljava/util/ArrayList;-><init>()V

    .line 950
    .local v30, "recordstoSetList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordSetItem;>;"
    const-wide/16 v14, 0x0

    .line 951
    .local v14, "totalSize":J
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mLocalChangedRecords:Ljava/util/Map;

    .line 952
    new-instance v31, Ljava/util/ArrayList;

    invoke-direct/range {v31 .. v31}, Ljava/util/ArrayList;-><init>()V

    .line 953
    .local v31, "updateKeyOperation":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    new-instance v35, Landroid/content/ContentValues;

    invoke-direct/range {v35 .. v35}, Landroid/content/ContentValues;-><init>()V

    .line 954
    .local v35, "values":Landroid/content/ContentValues;
    const-wide/16 v16, 0x0

    .line 960
    .local v16, "currentCursorCount":J
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mProvider:Landroid/content/ContentProviderClient;

    const/4 v5, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getLocalUpdatesSelection()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getLocalUpdatesSortOrder()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 962
    .local v18, "cursor":Landroid/database/Cursor;
    if-nez v18, :cond_0

    .line 1076
    :goto_0
    return-void

    .line 965
    :cond_0
    :try_start_0
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->getCount()I

    move-result v28

    .line 966
    move/from16 v0, v28

    int-to-long v14, v0

    .line 967
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "2100 DB Queried Size "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 969
    const/4 v3, 0x1

    move/from16 v0, v28

    if-ge v0, v3, :cond_1

    .line 971
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1059
    :catch_0
    move-exception v19

    .line 1060
    .local v19, "e":Ljava/lang/IllegalStateException;
    :goto_1
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 1061
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v3

    const-string v5, "Illegal State Exception from Provider"

    invoke-static {v3, v5}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1062
    new-instance v3, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/16 v5, 0xf

    invoke-direct {v3, v5}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v3

    .line 974
    .end local v19    # "e":Ljava/lang/IllegalStateException;
    :cond_1
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getDeletedColumnName()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    .line 975
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getKeyColumnName()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v23

    .line 976
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getIdColumnName()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    .line 977
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTimeStampColumnName()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v27

    .line 979
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    if-nez v3, :cond_2

    .line 980
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getBuilder()Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_2
    move-wide/from16 v32, v12

    .line 982
    .end local v12    # "uploadsize":J
    .local v32, "uploadsize":J
    :goto_2
    :try_start_2
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 983
    move-object/from16 v0, v18

    move/from16 v1, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    .line 984
    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v24

    .line 985
    move-object/from16 v0, v18

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    .line 986
    move-object/from16 v0, v18

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v25

    .line 988
    const-wide/16 v5, 0x0

    cmp-long v3, v5, v25

    if-nez v3, :cond_3

    .line 989
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v3

    const-string v5, "TimeStamp is NULL....cannot be NULL"

    invoke-static {v3, v5}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 990
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/samsung/android/scloud/framework/util/TimeManager;->getCurrentTime(Landroid/content/Context;)J

    move-result-wide v25

    .line 993
    :cond_3
    if-nez v20, :cond_7

    .line 995
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mServerChangedRecords:Ljava/util/Map;

    if-eqz v3, :cond_4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mServerChangedRecords:Ljava/util/Map;

    move-object/from16 v0, v24

    invoke-interface {v3, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_b

    .line 997
    :cond_4
    if-eqz v24, :cond_5

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getBaseKeyHeader()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_2

    move-result v3

    if-nez v3, :cond_6

    .line 999
    :cond_5
    :try_start_3
    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v3, v1}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->generateKey(Ljava/lang/Long;Landroid/database/Cursor;)Ljava/lang/String;
    :try_end_3
    .catch Lcom/sec/android/sCloudSync/Util/SyncException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_2

    move-result-object v24

    .line 1004
    :try_start_4
    invoke-virtual/range {v35 .. v35}, Landroid/content/ContentValues;->clear()V

    .line 1005
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getKeyColumnName()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v35

    move-object/from16 v1, v24

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v5, p0

    move-object/from16 v6, v31

    move-object v7, v4

    move-object/from16 v8, v35

    .line 1006
    invoke-virtual/range {v5 .. v10}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->addToOperations(Ljava/util/List;Landroid/net/Uri;Landroid/content/ContentValues;J)V

    .line 1009
    :cond_6
    const/16 v34, 0x0

    .line 1010
    .local v34, "value":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    move-object/from16 v0, v18

    move-object/from16 v1, v24

    invoke-virtual {v3, v0, v9, v10, v1}, Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;->parse(Landroid/database/Cursor;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v34

    .line 1012
    if-eqz v34, :cond_b

    .line 1013
    invoke-virtual/range {v34 .. v34}, Ljava/lang/String;->length()I
    :try_end_4
    .catch Ljava/lang/IllegalStateException; {:try_start_4 .. :try_end_4} :catch_2

    move-result v3

    int-to-long v5, v3

    add-long v12, v32, v5

    .line 1014
    .end local v32    # "uploadsize":J
    .restart local v12    # "uploadsize":J
    :try_start_5
    new-instance v3, Lcom/sec/android/sCloudSync/Records/RecordSetItem;

    invoke-static/range {v25 .. v26}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v24

    move-object/from16 v1, v34

    invoke-direct {v3, v0, v5, v1}, Lcom/sec/android/sCloudSync/Records/RecordSetItem;-><init>(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;)V

    move-object/from16 v0, v30

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1015
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mLocalChangedRecords:Ljava/util/Map;

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v24

    invoke-interface {v3, v0, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1040
    .end local v34    # "value":Ljava/lang/String;
    :goto_3
    const-wide/16 v5, 0x1

    add-long v16, v16, v5

    move-object/from16 v11, p0

    .line 1041
    invoke-virtual/range {v11 .. v17}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->checkUploadLimit(JJJ)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1042
    move-object/from16 v0, p0

    move-object/from16 v1, v31

    invoke-direct {v0, v1}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->doApplyBatch(Ljava/util/List;)V
    :try_end_5
    .catch Ljava/lang/IllegalStateException; {:try_start_5 .. :try_end_5} :catch_0

    .line 1044
    :try_start_6
    move-object/from16 v0, p0

    move-object/from16 v1, v30

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->updatetoServer(Ljava/util/List;Ljava/util/List;)V
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_4
    .catch Lcom/sec/android/sCloudSync/Util/SyncException; {:try_start_6 .. :try_end_6} :catch_5
    .catch Ljava/lang/IllegalStateException; {:try_start_6 .. :try_end_6} :catch_0

    .line 1052
    :try_start_7
    invoke-interface/range {v30 .. v30}, Ljava/util/List;->clear()V

    .line 1053
    invoke-interface/range {v29 .. v29}, Ljava/util/List;->clear()V

    .line 1054
    const-wide/16 v12, 0x0

    .line 1055
    invoke-virtual/range {v31 .. v31}, Ljava/util/ArrayList;->clear()V
    :try_end_7
    .catch Ljava/lang/IllegalStateException; {:try_start_7 .. :try_end_7} :catch_0

    move-wide/from16 v32, v12

    .end local v12    # "uploadsize":J
    .restart local v32    # "uploadsize":J
    goto/16 :goto_2

    .line 1000
    :catch_1
    move-exception v19

    .line 1001
    .local v19, "e":Lcom/sec/android/sCloudSync/Util/SyncException;
    :try_start_8
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 1002
    throw v19

    .line 1059
    .end local v19    # "e":Lcom/sec/android/sCloudSync/Util/SyncException;
    :catch_2
    move-exception v19

    move-wide/from16 v12, v32

    .end local v32    # "uploadsize":J
    .restart local v12    # "uploadsize":J
    goto/16 :goto_1

    .line 1018
    .end local v12    # "uploadsize":J
    .restart local v32    # "uploadsize":J
    :cond_7
    const/4 v3, 0x1

    move/from16 v0, v20

    if-ne v0, v3, :cond_9

    if-eqz v24, :cond_9

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getBaseKeyHeader()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 1020
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mServerChangedRecords:Ljava/util/Map;

    if-eqz v3, :cond_8

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mServerChangedRecords:Ljava/util/Map;

    move-object/from16 v0, v24

    invoke-interface {v3, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_b

    .line 1021
    :cond_8
    new-instance v3, Lcom/sec/android/sCloudSync/Records/RecordBase;

    invoke-static/range {v25 .. v26}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v24

    invoke-direct {v3, v0, v5}, Lcom/sec/android/sCloudSync/Records/RecordBase;-><init>(Ljava/lang/String;Ljava/lang/Long;)V

    move-object/from16 v0, v29

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1022
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mLocalChangedRecords:Ljava/util/Map;

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v24

    invoke-interface {v3, v0, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_8
    .catch Ljava/lang/IllegalStateException; {:try_start_8 .. :try_end_8} :catch_2

    move-wide/from16 v12, v32

    .end local v32    # "uploadsize":J
    .restart local v12    # "uploadsize":J
    goto :goto_3

    .line 1024
    .end local v12    # "uploadsize":J
    .restart local v32    # "uploadsize":J
    :cond_9
    const/4 v3, 0x1

    move/from16 v0, v20

    if-ne v0, v3, :cond_b

    .line 1030
    :try_start_9
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getIdColumnName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    move-object v8, v4

    invoke-virtual/range {v7 .. v12}, Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;->delete(Landroid/net/Uri;JLjava/lang/String;Ljava/lang/String;)V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_9} :catch_3
    .catch Ljava/lang/IllegalStateException; {:try_start_9 .. :try_end_9} :catch_2

    .line 1036
    add-int/lit8 v28, v28, -0x1

    move-wide/from16 v12, v32

    .end local v32    # "uploadsize":J
    .restart local v12    # "uploadsize":J
    goto/16 :goto_3

    .line 1032
    .end local v12    # "uploadsize":J
    .restart local v32    # "uploadsize":J
    :catch_3
    move-exception v19

    .line 1033
    .local v19, "e":Landroid/os/RemoteException;
    :try_start_a
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 1034
    throw v19
    :try_end_a
    .catch Ljava/lang/IllegalStateException; {:try_start_a .. :try_end_a} :catch_2

    .line 1045
    .end local v19    # "e":Landroid/os/RemoteException;
    .end local v32    # "uploadsize":J
    .restart local v12    # "uploadsize":J
    :catch_4
    move-exception v19

    .line 1046
    .restart local v19    # "e":Landroid/os/RemoteException;
    :try_start_b
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 1047
    throw v19

    .line 1048
    .end local v19    # "e":Landroid/os/RemoteException;
    :catch_5
    move-exception v19

    .line 1049
    .local v19, "e":Lcom/sec/android/sCloudSync/Util/SyncException;
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 1050
    throw v19
    :try_end_b
    .catch Ljava/lang/IllegalStateException; {:try_start_b .. :try_end_b} :catch_0

    .line 1058
    .end local v12    # "uploadsize":J
    .end local v19    # "e":Lcom/sec/android/sCloudSync/Util/SyncException;
    .restart local v32    # "uploadsize":J
    :cond_a
    :try_start_c
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V
    :try_end_c
    .catch Ljava/lang/IllegalStateException; {:try_start_c .. :try_end_c} :catch_2

    .line 1064
    move-object/from16 v0, p0

    move-object/from16 v1, v31

    invoke-direct {v0, v1}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->doApplyBatch(Ljava/util/List;)V

    .line 1066
    :try_start_d
    move-object/from16 v0, p0

    move-object/from16 v1, v30

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->updatetoServer(Ljava/util/List;Ljava/util/List;)V
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_d .. :try_end_d} :catch_6
    .catch Lcom/sec/android/sCloudSync/Util/SyncException; {:try_start_d .. :try_end_d} :catch_7

    .line 1072
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mLocalChangedRecords:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->clear()V

    .line 1073
    invoke-interface/range {v30 .. v30}, Ljava/util/List;->clear()V

    .line 1074
    invoke-interface/range {v29 .. v29}, Ljava/util/List;->clear()V

    .line 1075
    invoke-virtual/range {v31 .. v31}, Ljava/util/ArrayList;->clear()V

    move-wide/from16 v12, v32

    .line 1076
    .end local v32    # "uploadsize":J
    .restart local v12    # "uploadsize":J
    goto/16 :goto_0

    .line 1067
    .end local v12    # "uploadsize":J
    .restart local v32    # "uploadsize":J
    :catch_6
    move-exception v19

    .line 1068
    .local v19, "e":Landroid/os/RemoteException;
    throw v19

    .line 1069
    .end local v19    # "e":Landroid/os/RemoteException;
    :catch_7
    move-exception v19

    .line 1070
    .local v19, "e":Lcom/sec/android/sCloudSync/Util/SyncException;
    throw v19

    .end local v19    # "e":Lcom/sec/android/sCloudSync/Util/SyncException;
    :cond_b
    move-wide/from16 v12, v32

    .end local v32    # "uploadsize":J
    .restart local v12    # "uploadsize":J
    goto/16 :goto_3
.end method

.method private getLocalUpdatesSortOrder()Ljava/lang/String;
    .locals 2

    .prologue
    .line 177
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getDeletedColumnName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " DESC"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getServerUpdate(Ljava/util/Map;)Ljava/util/Map;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/sCloudSync/Records/KVSItem;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/sCloudSync/Records/KVSItem;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .line 370
    .local p1, "ServerChanges":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/sec/android/sCloudSync/Records/KVSItem;>;"
    const/4 v5, 0x0

    .line 371
    .local v5, "response":Lcom/sec/android/sCloudSync/Records/KVSResponse;
    const/4 v3, 0x0

    .line 373
    .local v3, "nextKey":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "2105 Last Sync Time : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mLastSyncTime:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    iget-object v9, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mLastSyncTime:Ljava/lang/String;

    if-nez v9, :cond_0

    .line 376
    invoke-direct {p0, p1}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getAllkeys(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    .line 419
    :goto_0
    return-object v1

    .line 380
    :cond_0
    iget-boolean v9, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mbSyncCanceled:Z

    if-eqz v9, :cond_1

    .line 381
    new-instance v9, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/16 v10, 0x9

    invoke-direct {v9, v10}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v9

    .line 383
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getCloudServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    move-result-object v4

    .line 384
    .local v4, "recordServiceManager":Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;
    invoke-interface {v4}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getRecordServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/IRecordServiceManager;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mLastSyncTime:Ljava/lang/String;

    const/16 v11, 0xfa

    invoke-interface {v9, v10, v3, v11}, Lcom/sec/android/sCloudSync/ServiceManagers/IRecordServiceManager;->getUpdates(Ljava/lang/String;Ljava/lang/String;I)Lcom/sec/android/sCloudSync/Records/KVSResponse;

    move-result-object v5

    .line 385
    invoke-virtual {v5}, Lcom/sec/android/sCloudSync/Records/KVSResponse;->getNextKey()Ljava/lang/String;

    move-result-object v3

    .line 387
    invoke-virtual {v5}, Lcom/sec/android/sCloudSync/Records/KVSResponse;->getResponseCode()I

    move-result v6

    .line 388
    .local v6, "responseCode":I
    if-nez v6, :cond_5

    .line 393
    invoke-direct {p0, v5, p1}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->addToDownloadKeyList(Lcom/sec/android/sCloudSync/Records/KVSResponse;Ljava/util/Map;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 394
    const/4 v1, 0x0

    goto :goto_0

    .line 396
    :cond_2
    invoke-virtual {v5}, Lcom/sec/android/sCloudSync/Records/KVSResponse;->getMaxTimeStamp()Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mLastSyncTime:Ljava/lang/String;

    .line 403
    if-eqz v3, :cond_3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v9

    if-nez v9, :cond_0

    .line 404
    :cond_3
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 405
    .local v7, "splitServerChanges":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/sec/android/sCloudSync/Records/KVSItem;>;"
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 406
    .local v1, "finalServerChanges":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/sec/android/sCloudSync/Records/KVSItem;>;"
    const/4 v8, 0x0

    .line 407
    .local v8, "splitSize":I
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_4
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 408
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/sec/android/sCloudSync/Records/KVSItem;>;"
    add-int/lit8 v8, v8, 0x1

    .line 409
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v10

    invoke-interface {v7, v9, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 410
    invoke-direct {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getSplitServerChangeSize()I

    move-result v9

    if-ne v8, v9, :cond_4

    .line 411
    invoke-virtual {p0, v7}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->removePreSyncedRecords(Ljava/util/Map;)Z

    .line 412
    invoke-interface {v1, v7}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 413
    invoke-interface {v7}, Ljava/util/Map;->clear()V

    .line 414
    const/4 v8, 0x0

    goto :goto_1

    .line 398
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/sec/android/sCloudSync/Records/KVSItem;>;"
    .end local v1    # "finalServerChanges":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/sec/android/sCloudSync/Records/KVSItem;>;"
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v7    # "splitServerChanges":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/sec/android/sCloudSync/Records/KVSItem;>;"
    .end local v8    # "splitSize":I
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "2105GetUpdates returned"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 399
    invoke-direct {p0, v6, p1}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->handleError(ILjava/util/Map;)Ljava/util/Map;

    move-result-object v1

    goto/16 :goto_0

    .line 417
    .restart local v1    # "finalServerChanges":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/sec/android/sCloudSync/Records/KVSItem;>;"
    .restart local v2    # "i$":Ljava/util/Iterator;
    .restart local v7    # "splitServerChanges":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/sec/android/sCloudSync/Records/KVSItem;>;"
    .restart local v8    # "splitSize":I
    :cond_6
    invoke-virtual {p0, v7}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->removePreSyncedRecords(Ljava/util/Map;)Z

    .line 418
    invoke-interface {v1, v7}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    goto/16 :goto_0
.end method

.method private getSplitServerChangeSize()I
    .locals 1

    .prologue
    .line 167
    const/16 v0, 0x64

    return v0
.end method

.method private getSyncUnitDeviceKeyHeader()Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .line 855
    sget-object v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mClientDeviceId:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mClientDeviceId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mClientDeviceId:Ljava/lang/String;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 857
    :cond_0
    new-instance v0, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v0

    .line 859
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getSyncUnitKeyHeader()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mClientDeviceId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getSyncUnitKeyHeader()Ljava/lang/String;
    .locals 2

    .prologue
    .line 851
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getBaseKeyHeader()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getSyncAdapterName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private handleCloudStart(Lcom/sec/android/sCloudSync/Records/KVSResponse;)Z
    .locals 3
    .param p1, "response"    # Lcom/sec/android/sCloudSync/Records/KVSResponse;

    .prologue
    .line 1091
    const/4 v1, 0x0

    .line 1092
    .local v1, "result":Z
    invoke-virtual {p1}, Lcom/sec/android/sCloudSync/Records/KVSResponse;->getResponseCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 1110
    :goto_0
    return v1

    .line 1094
    :sswitch_0
    const/4 v1, 0x1

    .line 1095
    goto :goto_0

    .line 1097
    :sswitch_1
    const/4 v1, 0x1

    .line 1098
    goto :goto_0

    .line 1100
    :sswitch_2
    invoke-virtual {p1}, Lcom/sec/android/sCloudSync/Records/KVSResponse;->getResultMessage()Ljava/lang/String;

    move-result-object v0

    .line 1102
    .local v0, "msg":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->notifyServerMaintenance(Ljava/lang/String;)V

    goto :goto_0

    .line 1105
    .end local v0    # "msg":Ljava/lang/String;
    :sswitch_3
    invoke-direct {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->readyToFullSync()V

    goto :goto_0

    .line 1092
    nop

    :sswitch_data_0
    .sparse-switch
        -0x1 -> :sswitch_0
        0x0 -> :sswitch_1
        0x5 -> :sswitch_2
        0x9 -> :sswitch_3
    .end sparse-switch
.end method

.method private handleError(ILjava/util/Map;)Ljava/util/Map;
    .locals 3
    .param p1, "responseCode"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/sCloudSync/Records/KVSItem;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/sCloudSync/Records/KVSItem;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .line 424
    .local p2, "serverChanges":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/sec/android/sCloudSync/Records/KVSItem;>;"
    sparse-switch p1, :sswitch_data_0

    .line 429
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleError:GetUpdates returned unHandled response code"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 427
    :sswitch_0
    invoke-direct {p0, p2}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getAllkeys(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    goto :goto_0

    .line 424
    :sswitch_data_0
    .sparse-switch
        0x4e22 -> :sswitch_0
        0x714e -> :sswitch_0
    .end sparse-switch
.end method

.method private handleException(Landroid/content/SyncResult;ILandroid/os/Bundle;)V
    .locals 8
    .param p1, "syncResult"    # Landroid/content/SyncResult;
    .param p2, "code"    # I
    .param p3, "extras"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x1

    const-wide/16 v5, 0x1

    .line 497
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Sync Exception: code =  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 498
    packed-switch p2, :pswitch_data_0

    .line 546
    :pswitch_0
    iget-object v2, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v3, v2, Landroid/content/SyncStats;->numAuthExceptions:J

    add-long/2addr v3, v5

    iput-wide v3, v2, Landroid/content/SyncStats;->numAuthExceptions:J

    .line 549
    :goto_0
    return-void

    .line 500
    :pswitch_1
    iget-object v2, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v3, v2, Landroid/content/SyncStats;->numAuthExceptions:J

    add-long/2addr v3, v5

    iput-wide v3, v2, Landroid/content/SyncStats;->numAuthExceptions:J

    goto :goto_0

    .line 504
    :pswitch_2
    iget-object v2, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v3, v2, Landroid/content/SyncStats;->numAuthExceptions:J

    add-long/2addr v3, v5

    iput-wide v3, v2, Landroid/content/SyncStats;->numAuthExceptions:J

    goto :goto_0

    .line 509
    :pswitch_3
    iget-object v2, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v3, v2, Landroid/content/SyncStats;->numAuthExceptions:J

    add-long/2addr v3, v5

    iput-wide v3, v2, Landroid/content/SyncStats;->numAuthExceptions:J

    .line 512
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mAuthControl:Lcom/sec/android/sCloudSync/Auth/IAuthControl;

    iget-object v3, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v4}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getCtid()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v2, v3, v4, v5}, Lcom/sec/android/sCloudSync/Auth/IAuthControl;->getAuthInformation(Landroid/content/Context;Ljava/lang/String;Z)Lcom/sec/android/sCloudSync/Auth/AuthManager;

    move-result-object v0

    .line 513
    .local v0, "authManager":Lcom/sec/android/sCloudSync/Auth/AuthManager;
    iget-object v2, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v2, v0}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->setAuthManager(Lcom/sec/android/sCloudSync/Auth/AuthManager;)V
    :try_end_0
    .catch Lcom/sec/android/sCloudSync/Util/SyncException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 514
    .end local v0    # "authManager":Lcom/sec/android/sCloudSync/Auth/AuthManager;
    :catch_0
    move-exception v1

    .line 515
    .local v1, "e1":Lcom/sec/android/sCloudSync/Util/SyncException;
    invoke-virtual {v1}, Lcom/sec/android/sCloudSync/Util/SyncException;->printStackTrace()V

    .line 516
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string v3, "TOKEN REFRESH FAILED"

    invoke-static {v2, v3}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 517
    .end local v1    # "e1":Lcom/sec/android/sCloudSync/Util/SyncException;
    :catch_1
    move-exception v1

    .line 518
    .local v1, "e1":Ljava/lang/IllegalArgumentException;
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "TOKEN REFRESH FAILED"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 522
    .end local v1    # "e1":Ljava/lang/IllegalArgumentException;
    :pswitch_4
    iget-object v2, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v3, v2, Landroid/content/SyncStats;->numParseExceptions:J

    add-long/2addr v3, v5

    iput-wide v3, v2, Landroid/content/SyncStats;->numParseExceptions:J

    goto :goto_0

    .line 526
    :pswitch_5
    iget-object v2, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v3, v2, Landroid/content/SyncStats;->numParseExceptions:J

    add-long/2addr v3, v5

    iput-wide v3, v2, Landroid/content/SyncStats;->numParseExceptions:J

    goto :goto_0

    .line 530
    :pswitch_6
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Sync Cancelled Exception received"

    invoke-static {v2, v3}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 534
    :pswitch_7
    iget-object v2, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v3, v2, Landroid/content/SyncStats;->numAuthExceptions:J

    add-long/2addr v3, v5

    iput-wide v3, v2, Landroid/content/SyncStats;->numAuthExceptions:J

    goto :goto_0

    .line 538
    :pswitch_8
    iput-boolean v7, p1, Landroid/content/SyncResult;->databaseError:Z

    goto :goto_0

    .line 542
    :pswitch_9
    iput-boolean v7, p1, Landroid/content/SyncResult;->databaseError:Z

    goto/16 :goto_0

    .line 498
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_4
        :pswitch_2
        :pswitch_1
        :pswitch_8
        :pswitch_0
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
    .end packed-switch
.end method

.method private handleResponse(Lcom/sec/android/sCloudSync/Records/KVSResponse;Ljava/util/List;IJ)V
    .locals 9
    .param p1, "kvsResponse"    # Lcom/sec/android/sCloudSync/Records/KVSResponse;
    .param p3, "opType"    # I
    .param p4, "updated"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/sCloudSync/Records/KVSResponse;",
            "Ljava/util/List",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;IJ)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .line 875
    .local p2, "operations":Ljava/util/List;, "Ljava/util/List<Landroid/content/ContentProviderOperation;>;"
    if-eqz p1, :cond_7

    invoke-virtual {p1}, Lcom/sec/android/sCloudSync/Records/KVSResponse;->getResponseCode()I

    move-result v6

    if-nez v6, :cond_7

    .line 876
    invoke-virtual {p1}, Lcom/sec/android/sCloudSync/Records/KVSResponse;->getItemResponseList()Ljava/util/List;

    move-result-object v2

    .line 877
    .local v2, "response":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordItemResponse;>;"
    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 922
    .end local v2    # "response":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordItemResponse;>;"
    :cond_0
    :goto_0
    return-void

    .line 880
    .restart local v2    # "response":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordItemResponse;>;"
    :cond_1
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 881
    .local v5, "values":Landroid/content/ContentValues;
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/sCloudSync/Records/RecordItemResponse;

    .line 882
    .local v1, "item":Lcom/sec/android/sCloudSync/Records/RecordItemResponse;
    iget-boolean v6, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mbSyncCanceled:Z

    if-eqz v6, :cond_3

    .line 883
    new-instance v6, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/16 v7, 0x9

    invoke-direct {v6, v7}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v6

    .line 885
    :cond_3
    invoke-virtual {v1}, Lcom/sec/android/sCloudSync/Records/RecordItemResponse;->getRcode()I

    move-result v6

    if-nez v6, :cond_5

    .line 890
    iget-object v6, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mLocalChangedRecords:Ljava/util/Map;

    invoke-virtual {v1}, Lcom/sec/android/sCloudSync/Records/RecordItemResponse;->getKEY()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    .line 891
    .local v3, "rowId":Ljava/lang/Long;
    if-eqz v3, :cond_2

    .line 895
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getContentUri()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-static {v6, v7, v8}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    .line 896
    .local v4, "uri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getCallerSyncAdapter()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getAccountName()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mAccount:Landroid/accounts/Account;

    iget-object v8, v8, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getAccountType()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mAccount:Landroid/accounts/Account;

    iget-object v8, v8, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    .line 900
    if-nez p3, :cond_4

    .line 901
    invoke-virtual {v5}, Landroid/content/ContentValues;->clear()V

    .line 903
    invoke-virtual {p0, v5, v1}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->handleUpdateToServerRecord(Landroid/content/ContentValues;Lcom/sec/android/sCloudSync/Records/RecordItemResponse;)V

    .line 905
    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v6

    invoke-interface {p2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 907
    :cond_4
    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v6

    invoke-interface {p2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 909
    .end local v3    # "rowId":Ljava/lang/Long;
    .end local v4    # "uri":Landroid/net/Uri;
    :cond_5
    invoke-virtual {v1}, Lcom/sec/android/sCloudSync/Records/RecordItemResponse;->getRcode()I

    move-result v6

    const/16 v7, 0x4e23

    if-eq v6, v7, :cond_6

    invoke-virtual {v1}, Lcom/sec/android/sCloudSync/Records/RecordItemResponse;->getRcode()I

    move-result v6

    const/16 v7, 0x4e24

    if-ne v6, v7, :cond_2

    .line 911
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->notifyServerStoragefull()V

    goto/16 :goto_1

    .line 915
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "item":Lcom/sec/android/sCloudSync/Records/RecordItemResponse;
    .end local v2    # "response":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordItemResponse;>;"
    .end local v5    # "values":Landroid/content/ContentValues;
    :cond_7
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/sCloudSync/Records/KVSResponse;->getResponseCode()I

    move-result v6

    const/16 v7, 0x4e23

    if-eq v6, v7, :cond_8

    invoke-virtual {p1}, Lcom/sec/android/sCloudSync/Records/KVSResponse;->getResponseCode()I

    move-result v6

    const/16 v7, 0x4e24

    if-ne v6, v7, :cond_0

    .line 919
    :cond_8
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->notifyServerStoragefull()V

    goto/16 :goto_0
.end method

.method private handleSync(Landroid/content/SyncResult;Landroid/os/Bundle;)V
    .locals 7
    .param p1, "syncResult"    # Landroid/content/SyncResult;
    .param p2, "extras"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/concurrent/TimeoutException;,
            Lcom/sec/android/sCloudSync/Util/SyncException;,
            Landroid/os/RemoteException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1178
    iget-object v3, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mTimeManager:Lcom/samsung/android/scloud/framework/util/TimeManager;

    iget-object v4, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v5}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getRecordServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/IRecordServiceManager;

    move-result-object v5

    invoke-interface {v5}, Lcom/sec/android/sCloudSync/ServiceManagers/IRecordServiceManager;->getServerTimeStamp()J

    move-result-wide v5

    invoke-virtual {v3, v4, v5, v6}, Lcom/samsung/android/scloud/framework/util/TimeManager;->updateSettingsUsingServer(Landroid/content/Context;J)V

    .line 1180
    const/4 v2, 0x0

    .line 1181
    .local v2, "syncExceptionStorageFull":Lcom/sec/android/sCloudSync/Util/SyncException;
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getServerUpdates()Ljava/util/Map;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mServerChangedRecords:Ljava/util/Map;

    .line 1183
    :try_start_0
    invoke-direct {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getLocalUpdates()V
    :try_end_0
    .catch Lcom/sec/android/sCloudSync/Util/SyncException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1194
    :goto_0
    iget-object v3, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mServerChangedRecords:Ljava/util/Map;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mServerChangedRecords:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1195
    iget-object v3, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    invoke-virtual {p0, v3}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->updateLocalDb(Landroid/content/SyncStats;)V

    .line 1197
    :cond_0
    iget-object v3, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mLastSyncTime:Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 1198
    iget-object v3, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mLastSyncTime:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->setLastSyncTime(Ljava/lang/String;)V

    .line 1200
    :cond_1
    if-eqz v2, :cond_3

    .line 1202
    throw v2

    .line 1184
    :catch_0
    move-exception v0

    .line 1185
    .local v0, "e":Lcom/sec/android/sCloudSync/Util/SyncException;
    invoke-virtual {v0}, Lcom/sec/android/sCloudSync/Util/SyncException;->getmExceptionCode()I

    move-result v1

    .line 1187
    .local v1, "exception":I
    const/4 v3, 0x5

    if-ne v1, v3, :cond_2

    .line 1188
    move-object v2, v0

    goto :goto_0

    .line 1190
    :cond_2
    throw v0

    .line 1204
    .end local v0    # "e":Lcom/sec/android/sCloudSync/Util/SyncException;
    .end local v1    # "exception":I
    :cond_3
    return-void
.end method

.method private isChangeForSync()Z
    .locals 10

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 1114
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getContentUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getCallerSyncAdapter()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getAccountName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mAccount:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getAccountType()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mAccount:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 1117
    .local v1, "url":Landroid/net/Uri;
    new-array v4, v8, [Ljava/lang/String;

    const-string v0, "1"

    aput-object v0, v4, v9

    .line 1118
    .local v4, "selectionArg":[Ljava/lang/String;
    const/4 v7, 0x0

    .line 1120
    .local v7, "rows":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mProvider:Landroid/content/ContentProviderClient;

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getProjection()[Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getDirtyColumnName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "= ?"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1121
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Sync Object count : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-nez v7, :cond_1

    const-string v0, "null"

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1122
    if-eqz v7, :cond_2

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 1123
    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1130
    if-eqz v7, :cond_0

    .line 1131
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_0
    move v0, v8

    .line 1133
    :goto_1
    return v0

    .line 1121
    :cond_1
    :try_start_1
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 1130
    :cond_2
    if-eqz v7, :cond_3

    .line 1131
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_3
    :goto_2
    move v0, v9

    .line 1133
    goto :goto_1

    .line 1126
    :catch_0
    move-exception v6

    .line 1128
    .local v6, "e":Landroid/os/RemoteException;
    :try_start_2
    invoke-virtual {v6}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1130
    if-eqz v7, :cond_3

    .line 1131
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 1130
    .end local v6    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_4

    .line 1131
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method private notifyServerMaintenance(Ljava/lang/String;)V
    .locals 10
    .param p1, "timeStr"    # Ljava/lang/String;

    .prologue
    .line 1157
    :try_start_0
    const-string v7, ","

    invoke-virtual {p1, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 1159
    .local v0, "arr":[Ljava/lang/String;
    new-instance v6, Ljava/util/Date;

    const/4 v7, 0x0

    aget-object v7, v0, v7

    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-direct {v6, v7, v8}, Ljava/util/Date;-><init>(J)V

    .line 1160
    .local v6, "start":Ljava/util/Date;
    new-instance v3, Ljava/util/Date;

    const/4 v7, 0x1

    aget-object v7, v0, v7

    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-direct {v3, v7, v8}, Ljava/util/Date;-><init>(J)V

    .line 1163
    .local v3, "end":Ljava/util/Date;
    iget-object v7, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mContext:Landroid/content/Context;

    const-string v8, "notification"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/NotificationManager;

    .line 1164
    .local v5, "notificationManager":Landroid/app/NotificationManager;
    new-instance v1, Landroid/app/Notification$Builder;

    iget-object v7, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v1, v7}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 1165
    .local v1, "builder":Landroid/app/Notification$Builder;
    const/high16 v7, 0x7f020000

    invoke-virtual {v1, v7}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v7

    const-string v8, "Server Maintence"

    invoke-virtual {v7, v8}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v7

    const-string v8, "Server Maintence"

    invoke-virtual {v7, v8}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " ~ "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 1170
    invoke-virtual {v1}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v4

    .line 1171
    .local v4, "notification":Landroid/app/Notification;
    const/4 v7, 0x0

    invoke-virtual {v5, v7, v4}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1175
    .end local v0    # "arr":[Ljava/lang/String;
    .end local v1    # "builder":Landroid/app/Notification$Builder;
    .end local v3    # "end":Ljava/util/Date;
    .end local v4    # "notification":Landroid/app/Notification;
    .end local v5    # "notificationManager":Landroid/app/NotificationManager;
    .end local v6    # "start":Ljava/util/Date;
    :goto_0
    return-void

    .line 1172
    :catch_0
    move-exception v2

    .line 1173
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private readyToFullSync()V
    .locals 7

    .prologue
    .line 1137
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getContentUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getCallerSyncAdapter()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getAccountName()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mAccount:Landroid/accounts/Account;

    iget-object v6, v6, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getAccountType()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mAccount:Landroid/accounts/Account;

    iget-object v6, v6, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 1140
    .local v3, "url":Landroid/net/Uri;
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1142
    .local v0, "dirtyValues":Landroid/content/ContentValues;
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getDirtyColumnName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1143
    const/4 v2, 0x0

    .line 1145
    .local v2, "result":I
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mProvider:Landroid/content/ContentProviderClient;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v4, v3, v0, v5, v6}, Landroid/content/ContentProviderClient;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 1152
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Ready FullSync : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " records are updated to dirty = 1 "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1153
    return-void

    .line 1147
    :catch_0
    move-exception v1

    .line 1148
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 1149
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v4

    const-string v5, "COULDN\'T UPDATE DIRTY = 1"

    invoke-static {v4, v5}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setLastSyncTime(Ljava/lang/String;)V
    .locals 22
    .param p1, "lastSyncTime"    # Ljava/lang/String;

    .prologue
    .line 1207
    if-eqz p1, :cond_0

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->isEmpty()Z

    move-result v19

    if-nez v19, :cond_0

    .line 1208
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getSyncStateURI()Landroid/net/Uri;

    move-result-object v19

    if-eqz v19, :cond_6

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getSyncStateDataColumn()Ljava/lang/String;

    move-result-object v19

    if-eqz v19, :cond_6

    .line 1209
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getSyncStateURI()Landroid/net/Uri;

    move-result-object v19

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getCallerSyncAdapter()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v17

    .line 1210
    .local v17, "uri":Landroid/net/Uri;
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getAccountType()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "= \'"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mAccount:Landroid/accounts/Account;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\'"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 1212
    .local v18, "where":Ljava/lang/String;
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 1214
    .local v7, "cv":Landroid/content/ContentValues;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getAccountName()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mAccount:Landroid/accounts/Account;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1215
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getAccountType()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mAccount:Landroid/accounts/Account;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1216
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getSyncAdapterName()Ljava/lang/String;

    move-result-object v19

    const/16 v20, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getLastSyncTime(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v13

    .line 1217
    .local v13, "oldLastTime":Ljava/lang/String;
    if-nez v13, :cond_1

    .line 1218
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getSyncStateDataColumn()Ljava/lang/String;

    move-result-object v19

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getSyncAdapterName()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1249
    :goto_0
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mProvider:Landroid/content/ContentProviderClient;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    move-object/from16 v3, v20

    invoke-virtual {v0, v1, v7, v2, v3}, Landroid/content/ContentProviderClient;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v19

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-ge v0, v1, :cond_0

    .line 1250
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mProvider:Landroid/content/ContentProviderClient;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v7}, Landroid/content/ContentProviderClient;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v11

    .line 1251
    .local v11, "insertUri":Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v19

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "timestanp updated"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/sCloudSync/Util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_3

    .line 1272
    .end local v7    # "cv":Landroid/content/ContentValues;
    .end local v11    # "insertUri":Landroid/net/Uri;
    .end local v13    # "oldLastTime":Ljava/lang/String;
    .end local v17    # "uri":Landroid/net/Uri;
    .end local v18    # "where":Ljava/lang/String;
    :cond_0
    :goto_1
    return-void

    .line 1220
    .restart local v7    # "cv":Landroid/content/ContentValues;
    .restart local v13    # "oldLastTime":Ljava/lang/String;
    .restart local v17    # "uri":Landroid/net/Uri;
    .restart local v18    # "where":Ljava/lang/String;
    :cond_1
    const-string v8, ""

    .line 1221
    .local v8, "data":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    .line 1222
    .local v6, "buf":Ljava/lang/StringBuffer;
    const/4 v5, 0x0

    .line 1223
    .local v5, "bAdded":Z
    const-string v19, "__"

    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v15

    .line 1224
    .local v15, "splitStrings":[Ljava/lang/String;
    if-eqz v15, :cond_5

    .line 1227
    move-object v4, v15

    .local v4, "arr$":[Ljava/lang/String;
    array-length v12, v4

    .local v12, "len$":I
    const/4 v10, 0x0

    .local v10, "i$":I
    :goto_2
    if-ge v10, v12, :cond_3

    aget-object v14, v4, v10

    .line 1228
    .local v14, "split":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getSyncAdapterName()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_2

    .line 1229
    const/4 v5, 0x1

    .line 1230
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getSyncAdapterName()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "__"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1227
    :goto_3
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 1233
    :cond_2
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "__"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_3

    .line 1238
    .end local v14    # "split":Ljava/lang/String;
    :cond_3
    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1240
    if-nez v5, :cond_4

    .line 1241
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getSyncAdapterName()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "__"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1243
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getSyncStateDataColumn()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v7, v0, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1245
    .end local v4    # "arr$":[Ljava/lang/String;
    .end local v10    # "i$":I
    .end local v12    # "len$":I
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getSyncStateDataColumn()Ljava/lang/String;

    move-result-object v19

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getSyncAdapterName()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1253
    .end local v5    # "bAdded":Z
    .end local v6    # "buf":Ljava/lang/StringBuffer;
    .end local v8    # "data":Ljava/lang/String;
    .end local v15    # "splitStrings":[Ljava/lang/String;
    :catch_0
    move-exception v9

    .line 1254
    .local v9, "e":Landroid/os/RemoteException;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v19

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Exception while setting timestamp"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v9}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1256
    .end local v9    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v9

    .line 1257
    .local v9, "e":Ljava/lang/IllegalStateException;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v19

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Exception while setting timestamp"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v9}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1259
    .end local v9    # "e":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v9

    .line 1260
    .local v9, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v19

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Exception while setting timestamp"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v9}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1262
    .end local v9    # "e":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v9

    .line 1263
    .local v9, "e":Ljava/lang/UnsupportedOperationException;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v19

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Exception while setting timestamp"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v9}, Ljava/lang/UnsupportedOperationException;->getMessage()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1267
    .end local v7    # "cv":Landroid/content/ContentValues;
    .end local v9    # "e":Ljava/lang/UnsupportedOperationException;
    .end local v13    # "oldLastTime":Ljava/lang/String;
    .end local v17    # "uri":Landroid/net/Uri;
    .end local v18    # "where":Ljava/lang/String;
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    const-string v20, "SyncMetaData"

    const/16 v21, 0x0

    invoke-virtual/range {v19 .. v21}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v16

    .line 1268
    .local v16, "syncMeta":Landroid/content/SharedPreferences;
    invoke-interface/range {v16 .. v16}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v19

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getSyncAdapterName()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mLastSyncTime:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-interface/range {v19 .. v21}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1269
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v19

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Save Last Sync Time : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mLastSyncTime:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method private updateLocal(Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .line 797
    .local p1, "operations":Ljava/util/List;, "Ljava/util/List<Landroid/content/ContentProviderOperation;>;"
    const/4 v4, 0x0

    .line 798
    .local v4, "start":I
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    .line 799
    .local v3, "size":I
    const/4 v1, 0x0

    .line 801
    .local v1, "end":I
    iget-object v6, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    if-nez v6, :cond_0

    .line 802
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getBuilder()Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    .line 804
    :cond_0
    iget-object v6, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    invoke-virtual {v6}, Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;->getBatchSize()I

    move-result v2

    .line 805
    .local v2, "maxBatchSize":I
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 806
    .local v5, "subOperations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :goto_0
    if-le v3, v4, :cond_2

    .line 807
    add-int v1, v4, v2

    .line 809
    if-ge v3, v1, :cond_1

    .line 810
    move v1, v3

    .line 812
    :cond_1
    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 813
    invoke-interface {p1, v4, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 815
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "2103 Record Size is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 818
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mProvider:Landroid/content/ContentProviderClient;

    invoke-virtual {v6, v5}, Landroid/content/ContentProviderClient;->applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 823
    move v4, v1

    goto :goto_0

    .line 819
    :catch_0
    move-exception v0

    .line 820
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v6

    const-string v7, "SYNC EXCEPTION : failed - updateLocal with applyBatch."

    invoke-static {v6, v7}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 821
    new-instance v6, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/16 v7, 0xb

    invoke-direct {v6, v7}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v6

    .line 826
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    return-void
.end method

.method private uploadToServer(Ljava/util/List;Ljava/util/List;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/sCloudSync/Records/RecordSetItem;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .line 768
    .local p1, "setList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordSetItem;>;"
    .local p2, "operations":Ljava/util/List;, "Ljava/util/List<Landroid/content/ContentProviderOperation;>;"
    const/4 v10, 0x0

    .line 769
    .local v10, "start":I
    const/16 v7, 0x19

    .line 770
    .local v7, "maxPostSize":I
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v9

    .line 771
    .local v9, "size":I
    const/4 v6, 0x0

    .line 773
    .local v6, "end":I
    const/4 v1, 0x0

    .line 775
    .local v1, "kvsResponse":Lcom/sec/android/sCloudSync/Records/KVSResponse;
    :goto_0
    if-le v9, v10, :cond_2

    .line 776
    iget-boolean v0, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mbSyncCanceled:Z

    if-eqz v0, :cond_0

    .line 777
    new-instance v0, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/16 v2, 0x9

    invoke-direct {v0, v2}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v0

    .line 779
    :cond_0
    add-int/lit8 v6, v10, 0x19

    .line 781
    if-ge v9, v6, :cond_1

    .line 782
    move v6, v9

    .line 785
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "2102 Record Size is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 787
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getCloudServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    move-result-object v8

    .line 788
    .local v8, "recordServiceManager":Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;
    invoke-interface {v8}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getRecordServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/IRecordServiceManager;

    move-result-object v0

    invoke-interface {p1, v10, v6}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/sec/android/sCloudSync/ServiceManagers/IRecordServiceManager;->setItems(Ljava/util/List;)Lcom/sec/android/sCloudSync/Records/KVSResponse;

    move-result-object v1

    .line 790
    const/4 v3, 0x0

    sub-int v0, v6, v10

    int-to-long v4, v0

    move-object v0, p0

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->handleResponse(Lcom/sec/android/sCloudSync/Records/KVSResponse;Ljava/util/List;IJ)V

    .line 791
    move v10, v6

    .line 792
    goto :goto_0

    .line 793
    .end local v8    # "recordServiceManager":Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;
    :cond_2
    return-void
.end method


# virtual methods
.method protected addToOperations(Ljava/util/List;Landroid/net/Uri;Landroid/content/ContentValues;J)V
    .locals 3
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "values"    # Landroid/content/ContentValues;
    .param p4, "rowId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Landroid/net/Uri;",
            "Landroid/content/ContentValues;",
            "J)V"
        }
    .end annotation

    .prologue
    .line 1087
    .local p1, "updateKeyOperation":Ljava/util/List;, "Ljava/util/List<Landroid/content/ContentProviderOperation;>;"
    invoke-static {p2}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "_id ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1088
    return-void
.end method

.method public cancelSync()V
    .locals 3

    .prologue
    .line 552
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v1

    const-string v2, "USER CANCELLED!!!"

    invoke-static {v1, v2}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 553
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mbSyncCanceled:Z

    .line 554
    iget-object v1, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mAuthControl:Lcom/sec/android/sCloudSync/Auth/IAuthControl;

    invoke-interface {v1}, Lcom/sec/android/sCloudSync/Auth/IAuthControl;->cancel()V

    .line 555
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getCloudServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    move-result-object v0

    .line 556
    .local v0, "recordServiceManager":Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;
    invoke-interface {v0}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getRecordServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/IRecordServiceManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/sCloudSync/ServiceManagers/IRecordServiceManager;->close()V

    .line 557
    return-void
.end method

.method protected checkUploadLimit(JJJ)Z
    .locals 2
    .param p1, "uploadsize"    # J
    .param p3, "totalSize"    # J
    .param p5, "currentCount"    # J

    .prologue
    .line 1080
    const-wide/32 v0, 0x100000

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 1081
    const/4 v0, 0x1

    .line 1082
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected doInsert(Lcom/sec/android/sCloudSync/Records/KVSItem;Lcom/sec/android/sCloudSync/Records/RecordItem;Landroid/content/SyncStats;)Z
    .locals 6
    .param p1, "item"    # Lcom/sec/android/sCloudSync/Records/KVSItem;
    .param p2, "record"    # Lcom/sec/android/sCloudSync/Records/RecordItem;
    .param p3, "stats"    # Landroid/content/SyncStats;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .line 226
    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 227
    .local v0, "data":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 228
    :cond_0
    const/4 v1, 0x0

    .line 240
    :goto_0
    return v1

    .line 230
    :cond_1
    iget-object v2, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    if-nez v2, :cond_2

    .line 231
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getBuilder()Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    .line 233
    :cond_2
    iget-object v2, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getKEY()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getTimeStamp()Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, v0, v3, v4, v5}, Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;->insert(Ljava/lang/String;Ljava/lang/String;J)Z

    move-result v1

    .line 234
    .local v1, "result":Z
    if-nez v1, :cond_3

    .line 235
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Unable to insert the contact with key "

    invoke-static {v2, v3}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 238
    :cond_3
    iget-wide v2, p3, Landroid/content/SyncStats;->numInserts:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, p3, Landroid/content/SyncStats;->numInserts:J

    goto :goto_0
.end method

.method protected doUpdateDelete(Lcom/sec/android/sCloudSync/Records/KVSItem;Lcom/sec/android/sCloudSync/Records/RecordItem;Landroid/content/SyncStats;)Z
    .locals 10
    .param p1, "item"    # Lcom/sec/android/sCloudSync/Records/KVSItem;
    .param p2, "record"    # Lcom/sec/android/sCloudSync/Records/RecordItem;
    .param p3, "stats"    # Landroid/content/SyncStats;

    .prologue
    const-wide/16 v8, 0x1

    .line 183
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getContentUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {p1}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getID()J

    move-result-wide v5

    invoke-static {v4, v5, v6}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 184
    .local v3, "uri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getCallerSyncAdapter()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getAccountName()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mAccount:Landroid/accounts/Account;

    iget-object v6, v6, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getAccountType()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mAccount:Landroid/accounts/Account;

    iget-object v6, v6, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 189
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mProvider:Landroid/content/ContentProviderClient;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v4, v3, v5, v6}, Landroid/content/ContentProviderClient;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 196
    :goto_0
    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->isDeleted()Z

    move-result v4

    if-nez v4, :cond_4

    .line 199
    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 201
    .local v0, "data":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v4, ""

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 202
    :cond_0
    const/4 v2, 0x0

    .line 219
    .end local v0    # "data":Ljava/lang/String;
    :goto_1
    return v2

    .line 191
    :catch_0
    move-exception v1

    .line 192
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 193
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v4

    const-string v5, "COULDN\'T DELETE LOCAL RECORD"

    invoke-static {v4, v5}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 204
    .end local v1    # "e":Landroid/os/RemoteException;
    .restart local v0    # "data":Ljava/lang/String;
    :cond_1
    iget-object v4, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    if-nez v4, :cond_2

    .line 205
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getBuilder()Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    .line 207
    :cond_2
    iget-object v4, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getKEY()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getTimeStamp()Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v4, v0, v5, v6, v7}, Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;->insert(Ljava/lang/String;Ljava/lang/String;J)Z

    move-result v2

    .line 208
    .local v2, "result":Z
    if-nez v2, :cond_3

    .line 209
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v4

    const-string v5, "UpdateLocalDb: Error occured during Update (insertion)"

    invoke-static {v4, v5}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 212
    :cond_3
    iget-wide v4, p3, Landroid/content/SyncStats;->numUpdates:J

    add-long/2addr v4, v8

    iput-wide v4, p3, Landroid/content/SyncStats;->numUpdates:J

    goto :goto_1

    .line 215
    .end local v0    # "data":Ljava/lang/String;
    .end local v2    # "result":Z
    :cond_4
    const/4 v2, 0x1

    .line 216
    .restart local v2    # "result":Z
    iget-wide v4, p3, Landroid/content/SyncStats;->numDeletes:J

    add-long/2addr v4, v8

    iput-wide v4, p3, Landroid/content/SyncStats;->numDeletes:J

    goto :goto_1
.end method

.method protected generateKey(Ljava/lang/Long;Landroid/database/Cursor;)Ljava/lang/String;
    .locals 3
    .param p1, "rowId"    # Ljava/lang/Long;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .line 863
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    .line 864
    .local v0, "uuid":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getSyncUnitDeviceKeyHeader()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method protected final getAccount()Landroid/accounts/Account;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mAccount:Landroid/accounts/Account;

    return-object v0
.end method

.method protected abstract getAccountName()Ljava/lang/String;
.end method

.method protected abstract getAccountType()Ljava/lang/String;
.end method

.method protected getBaseKeyHeader()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1402
    const-string v0, "DATASYNC"

    return-object v0
.end method

.method protected abstract getBuilder()Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;
.end method

.method protected abstract getCallerSyncAdapter()Ljava/lang/String;
.end method

.method protected abstract getCid()Ljava/lang/String;
.end method

.method protected final getCloudServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;
    .locals 1

    .prologue
    .line 1410
    iget-object v0, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    return-object v0
.end method

.method protected abstract getContentUri()Landroid/net/Uri;
.end method

.method protected final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method protected abstract getCtidKey()Ljava/lang/String;
.end method

.method protected abstract getDeletedColumnName()Ljava/lang/String;
.end method

.method protected abstract getDirtyColumnName()Ljava/lang/String;
.end method

.method protected abstract getIdColumnName()Ljava/lang/String;
.end method

.method protected abstract getKeyColumnName()Ljava/lang/String;
.end method

.method protected getLocalUpdatesSelection()Ljava/lang/String;
    .locals 2

    .prologue
    .line 172
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getDirtyColumnName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = 1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getLocalupdatesUri()Landroid/net/Uri;
    .locals 4

    .prologue
    .line 925
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getContentUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getCallerSyncAdapter()Ljava/lang/String;

    move-result-object v2

    const-string v3, "true"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getAccountName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mAccount:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getAccountType()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mAccount:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 929
    .local v0, "uri":Landroid/net/Uri;
    return-object v0
.end method

.method protected getProjection()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 1406
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getIdColumnName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getKeyColumnName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTimeStampColumnName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getDeletedColumnName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    return-object v0
.end method

.method protected final getProvider()Landroid/content/ContentProviderClient;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mProvider:Landroid/content/ContentProviderClient;

    return-object v0
.end method

.method protected getServerUpdates()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/sCloudSync/Records/KVSItem;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .line 365
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 366
    .local v0, "ServerChanges":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/sec/android/sCloudSync/Records/KVSItem;>;"
    invoke-direct {p0, v0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getServerUpdate(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    return-object v1
.end method

.method protected abstract getSyncAdapterName()Ljava/lang/String;
.end method

.method protected abstract getSyncStateDataColumn()Ljava/lang/String;
.end method

.method protected abstract getSyncStateURI()Landroid/net/Uri;
.end method

.method protected abstract getTag()Ljava/lang/String;
.end method

.method protected abstract getTimeStampColumnName()Ljava/lang/String;
.end method

.method protected handleDeleteList(Ljava/util/List;Ljava/util/List;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/sCloudSync/Records/RecordBase;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/sCloudSync/Records/RecordSetItem;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;,
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 847
    .local p1, "deleteList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordBase;>;"
    .local p2, "setList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordSetItem;>;"
    const/4 v0, 0x0

    return v0
.end method

.method protected handleUpdateToServerRecord(Landroid/content/ContentValues;Lcom/sec/android/sCloudSync/Records/RecordItemResponse;)V
    .locals 2
    .param p1, "values"    # Landroid/content/ContentValues;
    .param p2, "item"    # Lcom/sec/android/sCloudSync/Records/RecordItemResponse;

    .prologue
    .line 869
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getDirtyColumnName()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 870
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTimeStampColumnName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItemResponse;->getTimeStamp()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 871
    return-void
.end method

.method protected notifyServerStoragefull()V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .line 1376
    iget-object v7, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mContext:Landroid/content/Context;

    const-string v8, "notification"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/NotificationManager;

    .line 1377
    .local v5, "notificationManager":Landroid/app/NotificationManager;
    const/4 v0, 0x0

    .line 1378
    .local v0, "bShowNotication":Z
    iget-object v7, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    .line 1380
    .local v6, "packageManager":Landroid/content/pm/PackageManager;
    :try_start_0
    const-string v7, "com.sec.android.scloud.quota"

    const/16 v8, 0x80

    invoke-virtual {v6, v7, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    .line 1381
    new-instance v3, Landroid/content/Intent;

    const-string v7, "com.sec.android.sCloudQuotaApp.QUOTA_FULL"

    invoke-direct {v3, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1382
    .local v3, "intent":Landroid/content/Intent;
    iget-object v7, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v7, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1388
    .end local v3    # "intent":Landroid/content/Intent;
    :goto_0
    if-eqz v0, :cond_0

    .line 1389
    new-instance v1, Landroid/app/Notification$Builder;

    iget-object v7, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v1, v7}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 1390
    .local v1, "builder":Landroid/app/Notification$Builder;
    const/high16 v7, 0x7f020000

    invoke-virtual {v1, v7}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mContext:Landroid/content/Context;

    const v9, 0x7f030002

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mContext:Landroid/content/Context;

    const v9, 0x7f030003

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mContext:Landroid/content/Context;

    const v9, 0x7f030001

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 1395
    invoke-virtual {v1}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v4

    .line 1396
    .local v4, "notification":Landroid/app/Notification;
    const/4 v7, 0x0

    invoke-virtual {v5, v7, v4}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 1398
    .end local v1    # "builder":Landroid/app/Notification$Builder;
    .end local v4    # "notification":Landroid/app/Notification;
    :cond_0
    new-instance v7, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/4 v8, 0x5

    invoke-direct {v7, v8}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v7

    .line 1383
    :catch_0
    move-exception v2

    .line 1384
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v2}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 1385
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public performSync(Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/SyncResult;)Landroid/content/SyncResult;
    .locals 19
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "extras"    # Landroid/os/Bundle;
    .param p3, "authority"    # Ljava/lang/String;
    .param p4, "syncResult"    # Landroid/content/SyncResult;

    .prologue
    .line 561
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v13

    const-string v14, "PERFORM SYNC : 5.6.5"

    invoke-static {v13, v14}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 563
    new-instance v13, Landroid/accounts/Account;

    const-string v14, "com.osp.app.signin"

    move-object/from16 v0, p1

    invoke-direct {v13, v0, v14}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mAccount:Landroid/accounts/Account;

    .line 564
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    move-object/from16 v0, p3

    invoke-virtual {v13, v0}, Landroid/content/ContentResolver;->acquireContentProviderClient(Ljava/lang/String;)Landroid/content/ContentProviderClient;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mProvider:Landroid/content/ContentProviderClient;

    .line 566
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mProvider:Landroid/content/ContentProviderClient;

    if-nez v13, :cond_0

    .line 568
    const/4 v13, 0x1

    move-object/from16 v0, p4

    iput-boolean v13, v0, Landroid/content/SyncResult;->databaseError:Z

    .line 686
    :goto_0
    return-object p4

    .line 571
    :cond_0
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mbSyncCanceled:Z

    .line 573
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getSyncAdapterName()Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v13, v14}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getLastSyncTime(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mLastSyncTime:Ljava/lang/String;

    .line 575
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v13

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "LastSyncTime : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mLastSyncTime:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 578
    const-string v12, "sync_self"

    .line 579
    .local v12, "trigger":Ljava/lang/String;
    const-string v13, "upload"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v8

    .line 581
    .local v8, "isOnChangeSync":Z
    const-string v13, "sync_push"

    const-string v14, "trigger"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 582
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v13

    const-string v14, "SYNC REQUEST FROM PUSH MESSAGE.."

    invoke-static {v13, v14}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 583
    const-string v12, "sync_push"

    .line 599
    :cond_1
    :goto_1
    const/4 v9, 0x1

    .line 600
    .local v9, "isSuc":Z
    const-string v11, "sync result"

    .line 605
    .local v11, "result":Ljava/lang/String;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getCtidKey()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const/16 v15, 0x8

    invoke-static {v15}, Lcom/sec/android/sCloudSync/Tools/AppTool;->generateCTID(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-interface {v13, v14}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->setCtid(Ljava/lang/String;)V

    .line 607
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mAuthControl:Lcom/sec/android/sCloudSync/Auth/IAuthControl;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v15}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getCtid()Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x0

    invoke-interface/range {v13 .. v16}, Lcom/sec/android/sCloudSync/Auth/IAuthControl;->getAuthInformation(Landroid/content/Context;Ljava/lang/String;Z)Lcom/sec/android/sCloudSync/Auth/AuthManager;

    move-result-object v3

    .line 608
    .local v3, "authManager":Lcom/sec/android/sCloudSync/Auth/AuthManager;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v13, v3}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->setAuthManager(Lcom/sec/android/sCloudSync/Auth/AuthManager;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/sec/android/sCloudSync/Util/SyncException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_c
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_f
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 616
    :try_start_1
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mbSyncCanceled:Z

    if-eqz v13, :cond_b

    .line 617
    new-instance v13, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/16 v14, 0x9

    invoke-direct {v13, v14}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v13
    :try_end_1
    .catch Lcom/sec/android/sCloudSync/Util/SyncException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_c
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_f
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 637
    .end local v3    # "authManager":Lcom/sec/android/sCloudSync/Auth/AuthManager;
    :catch_0
    move-exception v6

    .line 638
    .local v6, "e":Lcom/sec/android/sCloudSync/Util/SyncException;
    :try_start_2
    invoke-virtual {v6}, Lcom/sec/android/sCloudSync/Util/SyncException;->getmExceptionCode()I

    move-result v5

    .line 639
    .local v5, "code":I
    const/4 v9, 0x0

    .line 640
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ""

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 641
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->handleException(Landroid/content/SyncResult;ILandroid/os/Bundle;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 664
    :try_start_3
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mbSyncCanceled:Z

    if-nez v13, :cond_2

    .line 665
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v13}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getAuthManager()Lcom/sec/android/sCloudSync/Auth/AuthManager;

    move-result-object v13

    if-eqz v13, :cond_16

    .line 666
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v13, v9, v11}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->serviceEnd(ZLjava/lang/String;)Lcom/sec/android/sCloudSync/Records/KVSResponse;
    :try_end_3
    .catch Lcom/sec/android/sCloudSync/Util/SyncException; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_7

    .line 677
    .end local v6    # "e":Lcom/sec/android/sCloudSync/Util/SyncException;
    :cond_2
    :goto_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mServerChangedRecords:Ljava/util/Map;

    if-eqz v13, :cond_3

    .line 678
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mServerChangedRecords:Ljava/util/Map;

    invoke-interface {v13}, Ljava/util/Map;->clear()V

    .line 679
    :cond_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    if-eqz v13, :cond_4

    .line 680
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    .line 682
    :cond_4
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mbSyncCanceled:Z

    .line 684
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v13

    const-string v14, "PERFORM SYNC : END"

    invoke-static {v13, v14}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 584
    .end local v5    # "code":I
    .end local v9    # "isSuc":Z
    .end local v11    # "result":Ljava/lang/String;
    :cond_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mLastSyncTime:Ljava/lang/String;

    if-nez v13, :cond_6

    .line 585
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v13

    const-string v14, "FIRST SYNC REQUEST.."

    invoke-static {v13, v14}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 586
    :cond_6
    if-eqz v8, :cond_7

    .line 587
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v13

    const-string v14, "SYNC REQUEST FROM PROVIDER.."

    invoke-static {v13, v14}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 588
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->isChangeForSync()Z

    move-result v13

    if-nez v13, :cond_1

    .line 589
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v13

    const-string v14, "There are no changes for samsung account sync.."

    invoke-static {v13, v14}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 594
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v13

    const-string v14, "MANUAL SYNC REQUEST.."

    invoke-static {v13, v14}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 610
    .restart local v9    # "isSuc":Z
    .restart local v11    # "result":Ljava/lang/String;
    :catch_1
    move-exception v6

    .line 611
    .local v6, "e":Ljava/lang/IllegalArgumentException;
    :try_start_4
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v13

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Exception ="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v6}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 613
    new-instance v13, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/4 v14, 0x3

    invoke-direct {v13, v14}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v13
    :try_end_4
    .catch Lcom/sec/android/sCloudSync/Util/SyncException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_c
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_f
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 642
    .end local v6    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v6

    .line 643
    .local v6, "e":Ljava/util/concurrent/TimeoutException;
    const/4 v9, 0x0

    .line 644
    :try_start_5
    invoke-virtual {v6}, Ljava/util/concurrent/TimeoutException;->getMessage()Ljava/lang/String;

    move-result-object v11

    .line 645
    invoke-virtual {v6}, Ljava/util/concurrent/TimeoutException;->printStackTrace()V

    .line 646
    const/4 v13, 0x6

    move-object/from16 v0, p0

    move-object/from16 v1, p4

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v13, v2}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->handleException(Landroid/content/SyncResult;ILandroid/os/Bundle;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 664
    :try_start_6
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mbSyncCanceled:Z

    if-nez v13, :cond_8

    .line 665
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v13}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getAuthManager()Lcom/sec/android/sCloudSync/Auth/AuthManager;

    move-result-object v13

    if-eqz v13, :cond_17

    .line 666
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v13, v9, v11}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->serviceEnd(ZLjava/lang/String;)Lcom/sec/android/sCloudSync/Records/KVSResponse;
    :try_end_6
    .catch Lcom/sec/android/sCloudSync/Util/SyncException; {:try_start_6 .. :try_end_6} :catch_8
    .catch Ljava/lang/IllegalStateException; {:try_start_6 .. :try_end_6} :catch_9

    .line 677
    .end local v6    # "e":Ljava/util/concurrent/TimeoutException;
    :cond_8
    :goto_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mServerChangedRecords:Ljava/util/Map;

    if-eqz v13, :cond_9

    .line 678
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mServerChangedRecords:Ljava/util/Map;

    invoke-interface {v13}, Ljava/util/Map;->clear()V

    .line 679
    :cond_9
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    if-eqz v13, :cond_a

    .line 680
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    .line 682
    :cond_a
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mbSyncCanceled:Z

    .line 684
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v13

    const-string v14, "PERFORM SYNC : END"

    invoke-static {v13, v14}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 620
    .restart local v3    # "authManager":Lcom/sec/android/sCloudSync/Auth/AuthManager;
    :cond_b
    :try_start_7
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v13

    const-string v14, "PERFORM SYNC : START"

    invoke-static {v13, v14}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 621
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v13}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getAuthManager()Lcom/sec/android/sCloudSync/Auth/AuthManager;

    move-result-object v13

    if-eqz v13, :cond_11

    .line 622
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v13, v12}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->serviceStart(Ljava/lang/String;)Lcom/sec/android/sCloudSync/Records/KVSResponse;

    move-result-object v10

    .line 623
    .local v10, "response":Lcom/sec/android/sCloudSync/Records/KVSResponse;
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->handleCloudStart(Lcom/sec/android/sCloudSync/Records/KVSResponse;)Z

    move-result v4

    .line 624
    .local v4, "canStart":Z
    if-eqz v4, :cond_c

    .line 625
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->prepareSync()Z

    move-result v13

    if-eqz v13, :cond_10

    .line 626
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->handleSync(Landroid/content/SyncResult;Landroid/os/Bundle;)V

    .line 627
    const-string v11, "0"
    :try_end_7
    .catch Lcom/sec/android/sCloudSync/Util/SyncException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_c
    .catch Ljava/lang/OutOfMemoryError; {:try_start_7 .. :try_end_7} :catch_f
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 664
    .end local v4    # "canStart":Z
    .end local v10    # "response":Lcom/sec/android/sCloudSync/Records/KVSResponse;
    :cond_c
    :goto_4
    :try_start_8
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mbSyncCanceled:Z

    if-nez v13, :cond_d

    .line 665
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v13}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getAuthManager()Lcom/sec/android/sCloudSync/Auth/AuthManager;

    move-result-object v13

    if-eqz v13, :cond_15

    .line 666
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v13, v9, v11}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->serviceEnd(ZLjava/lang/String;)Lcom/sec/android/sCloudSync/Records/KVSResponse;
    :try_end_8
    .catch Lcom/sec/android/sCloudSync/Util/SyncException; {:try_start_8 .. :try_end_8} :catch_4
    .catch Ljava/lang/IllegalStateException; {:try_start_8 .. :try_end_8} :catch_5

    .line 677
    :cond_d
    :goto_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mServerChangedRecords:Ljava/util/Map;

    if-eqz v13, :cond_e

    .line 678
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mServerChangedRecords:Ljava/util/Map;

    invoke-interface {v13}, Ljava/util/Map;->clear()V

    .line 679
    :cond_e
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    if-eqz v13, :cond_f

    .line 680
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    .line 682
    :cond_f
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mbSyncCanceled:Z

    .line 684
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v13

    const-string v14, "PERFORM SYNC : END"

    invoke-static {v13, v14}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 629
    .restart local v4    # "canStart":Z
    .restart local v10    # "response":Lcom/sec/android/sCloudSync/Records/KVSResponse;
    :cond_10
    :try_start_9
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v13

    const-string v14, "syncPrepare failed.."

    invoke-static {v13, v14}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 630
    const-string v11, "e"

    goto :goto_4

    .line 634
    .end local v4    # "canStart":Z
    .end local v10    # "response":Lcom/sec/android/sCloudSync/Records/KVSResponse;
    :cond_11
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v13

    const-string v14, "No Auth Info.. Can not call cloud-start"

    invoke-static {v13, v14}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_9
    .catch Lcom/sec/android/sCloudSync/Util/SyncException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_9 .. :try_end_9} :catch_2
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_9} :catch_3
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_c
    .catch Ljava/lang/OutOfMemoryError; {:try_start_9 .. :try_end_9} :catch_f
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_4

    .line 647
    .end local v3    # "authManager":Lcom/sec/android/sCloudSync/Auth/AuthManager;
    :catch_3
    move-exception v6

    .line 648
    .local v6, "e":Landroid/os/RemoteException;
    const/4 v9, 0x0

    .line 649
    :try_start_a
    invoke-virtual {v6}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v11

    .line 650
    invoke-virtual {v6}, Landroid/os/RemoteException;->printStackTrace()V

    .line 651
    const/16 v13, 0xd

    move-object/from16 v0, p0

    move-object/from16 v1, p4

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v13, v2}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->handleException(Landroid/content/SyncResult;ILandroid/os/Bundle;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 664
    :try_start_b
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mbSyncCanceled:Z

    if-nez v13, :cond_12

    .line 665
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v13}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getAuthManager()Lcom/sec/android/sCloudSync/Auth/AuthManager;

    move-result-object v13

    if-eqz v13, :cond_18

    .line 666
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v13, v9, v11}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->serviceEnd(ZLjava/lang/String;)Lcom/sec/android/sCloudSync/Records/KVSResponse;
    :try_end_b
    .catch Lcom/sec/android/sCloudSync/Util/SyncException; {:try_start_b .. :try_end_b} :catch_a
    .catch Ljava/lang/IllegalStateException; {:try_start_b .. :try_end_b} :catch_b

    .line 677
    .end local v6    # "e":Landroid/os/RemoteException;
    :cond_12
    :goto_6
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mServerChangedRecords:Ljava/util/Map;

    if-eqz v13, :cond_13

    .line 678
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mServerChangedRecords:Ljava/util/Map;

    invoke-interface {v13}, Ljava/util/Map;->clear()V

    .line 679
    :cond_13
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    if-eqz v13, :cond_14

    .line 680
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    .line 682
    :cond_14
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mbSyncCanceled:Z

    .line 684
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v13

    const-string v14, "PERFORM SYNC : END"

    invoke-static {v13, v14}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 668
    .restart local v3    # "authManager":Lcom/sec/android/sCloudSync/Auth/AuthManager;
    :cond_15
    :try_start_c
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v13

    const-string v14, "No Auth Info.. Can not call cloud-end"

    invoke-static {v13, v14}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_c
    .catch Lcom/sec/android/sCloudSync/Util/SyncException; {:try_start_c .. :try_end_c} :catch_4
    .catch Ljava/lang/IllegalStateException; {:try_start_c .. :try_end_c} :catch_5

    goto/16 :goto_5

    .line 670
    :catch_4
    move-exception v7

    .line 671
    .local v7, "e1":Lcom/sec/android/sCloudSync/Util/SyncException;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v13

    const-string v14, "error in call cloud-end"

    invoke-static {v13, v14}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 672
    move-object/from16 v0, p4

    iget-object v13, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v14, v13, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v16, 0x1

    add-long v14, v14, v16

    iput-wide v14, v13, Landroid/content/SyncStats;->numAuthExceptions:J

    goto/16 :goto_5

    .line 673
    .end local v7    # "e1":Lcom/sec/android/sCloudSync/Util/SyncException;
    :catch_5
    move-exception v6

    .line 674
    .local v6, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v6}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto/16 :goto_5

    .line 668
    .end local v3    # "authManager":Lcom/sec/android/sCloudSync/Auth/AuthManager;
    .restart local v5    # "code":I
    .local v6, "e":Lcom/sec/android/sCloudSync/Util/SyncException;
    :cond_16
    :try_start_d
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v13

    const-string v14, "No Auth Info.. Can not call cloud-end"

    invoke-static {v13, v14}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_d
    .catch Lcom/sec/android/sCloudSync/Util/SyncException; {:try_start_d .. :try_end_d} :catch_6
    .catch Ljava/lang/IllegalStateException; {:try_start_d .. :try_end_d} :catch_7

    goto/16 :goto_2

    .line 670
    :catch_6
    move-exception v7

    .line 671
    .restart local v7    # "e1":Lcom/sec/android/sCloudSync/Util/SyncException;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v13

    const-string v14, "error in call cloud-end"

    invoke-static {v13, v14}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 672
    move-object/from16 v0, p4

    iget-object v13, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v14, v13, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v16, 0x1

    add-long v14, v14, v16

    iput-wide v14, v13, Landroid/content/SyncStats;->numAuthExceptions:J

    goto/16 :goto_2

    .line 673
    .end local v7    # "e1":Lcom/sec/android/sCloudSync/Util/SyncException;
    :catch_7
    move-exception v6

    .line 674
    .local v6, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v6}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto/16 :goto_2

    .line 668
    .end local v5    # "code":I
    .local v6, "e":Ljava/util/concurrent/TimeoutException;
    :cond_17
    :try_start_e
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v13

    const-string v14, "No Auth Info.. Can not call cloud-end"

    invoke-static {v13, v14}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_e
    .catch Lcom/sec/android/sCloudSync/Util/SyncException; {:try_start_e .. :try_end_e} :catch_8
    .catch Ljava/lang/IllegalStateException; {:try_start_e .. :try_end_e} :catch_9

    goto/16 :goto_3

    .line 670
    :catch_8
    move-exception v7

    .line 671
    .restart local v7    # "e1":Lcom/sec/android/sCloudSync/Util/SyncException;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v13

    const-string v14, "error in call cloud-end"

    invoke-static {v13, v14}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 672
    move-object/from16 v0, p4

    iget-object v13, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v14, v13, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v16, 0x1

    add-long v14, v14, v16

    iput-wide v14, v13, Landroid/content/SyncStats;->numAuthExceptions:J

    goto/16 :goto_3

    .line 673
    .end local v7    # "e1":Lcom/sec/android/sCloudSync/Util/SyncException;
    :catch_9
    move-exception v6

    .line 674
    .local v6, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v6}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto/16 :goto_3

    .line 668
    .local v6, "e":Landroid/os/RemoteException;
    :cond_18
    :try_start_f
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v13

    const-string v14, "No Auth Info.. Can not call cloud-end"

    invoke-static {v13, v14}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_f
    .catch Lcom/sec/android/sCloudSync/Util/SyncException; {:try_start_f .. :try_end_f} :catch_a
    .catch Ljava/lang/IllegalStateException; {:try_start_f .. :try_end_f} :catch_b

    goto/16 :goto_6

    .line 670
    :catch_a
    move-exception v7

    .line 671
    .restart local v7    # "e1":Lcom/sec/android/sCloudSync/Util/SyncException;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v13

    const-string v14, "error in call cloud-end"

    invoke-static {v13, v14}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 672
    move-object/from16 v0, p4

    iget-object v13, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v14, v13, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v16, 0x1

    add-long v14, v14, v16

    iput-wide v14, v13, Landroid/content/SyncStats;->numAuthExceptions:J

    goto/16 :goto_6

    .line 673
    .end local v7    # "e1":Lcom/sec/android/sCloudSync/Util/SyncException;
    :catch_b
    move-exception v6

    .line 674
    .local v6, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v6}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto/16 :goto_6

    .line 652
    .end local v6    # "e":Ljava/lang/IllegalStateException;
    :catch_c
    move-exception v6

    .line 653
    .local v6, "e":Ljava/io/IOException;
    const/4 v9, 0x0

    .line 654
    :try_start_10
    invoke-virtual {v6}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v11

    .line 655
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    .line 656
    const/4 v13, 0x4

    move-object/from16 v0, p0

    move-object/from16 v1, p4

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v13, v2}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->handleException(Landroid/content/SyncResult;ILandroid/os/Bundle;)V
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    .line 664
    :try_start_11
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mbSyncCanceled:Z

    if-nez v13, :cond_19

    .line 665
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v13}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getAuthManager()Lcom/sec/android/sCloudSync/Auth/AuthManager;

    move-result-object v13

    if-eqz v13, :cond_1c

    .line 666
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v13, v9, v11}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->serviceEnd(ZLjava/lang/String;)Lcom/sec/android/sCloudSync/Records/KVSResponse;
    :try_end_11
    .catch Lcom/sec/android/sCloudSync/Util/SyncException; {:try_start_11 .. :try_end_11} :catch_d
    .catch Ljava/lang/IllegalStateException; {:try_start_11 .. :try_end_11} :catch_e

    .line 677
    .end local v6    # "e":Ljava/io/IOException;
    :cond_19
    :goto_7
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mServerChangedRecords:Ljava/util/Map;

    if-eqz v13, :cond_1a

    .line 678
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mServerChangedRecords:Ljava/util/Map;

    invoke-interface {v13}, Ljava/util/Map;->clear()V

    .line 679
    :cond_1a
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    if-eqz v13, :cond_1b

    .line 680
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    .line 682
    :cond_1b
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mbSyncCanceled:Z

    .line 684
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v13

    const-string v14, "PERFORM SYNC : END"

    invoke-static {v13, v14}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 668
    .restart local v6    # "e":Ljava/io/IOException;
    :cond_1c
    :try_start_12
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v13

    const-string v14, "No Auth Info.. Can not call cloud-end"

    invoke-static {v13, v14}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_12
    .catch Lcom/sec/android/sCloudSync/Util/SyncException; {:try_start_12 .. :try_end_12} :catch_d
    .catch Ljava/lang/IllegalStateException; {:try_start_12 .. :try_end_12} :catch_e

    goto :goto_7

    .line 670
    :catch_d
    move-exception v7

    .line 671
    .restart local v7    # "e1":Lcom/sec/android/sCloudSync/Util/SyncException;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v13

    const-string v14, "error in call cloud-end"

    invoke-static {v13, v14}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 672
    move-object/from16 v0, p4

    iget-object v13, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v14, v13, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v16, 0x1

    add-long v14, v14, v16

    iput-wide v14, v13, Landroid/content/SyncStats;->numAuthExceptions:J

    goto :goto_7

    .line 673
    .end local v7    # "e1":Lcom/sec/android/sCloudSync/Util/SyncException;
    :catch_e
    move-exception v6

    .line 674
    .local v6, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v6}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_7

    .line 657
    .end local v6    # "e":Ljava/lang/IllegalStateException;
    :catch_f
    move-exception v6

    .line 658
    .local v6, "e":Ljava/lang/OutOfMemoryError;
    :try_start_13
    invoke-virtual {v6}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    .line 659
    invoke-virtual {v6}, Ljava/lang/OutOfMemoryError;->getMessage()Ljava/lang/String;

    move-result-object v11

    .line 660
    const/16 v13, 0xc

    move-object/from16 v0, p0

    move-object/from16 v1, p4

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v13, v2}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->handleException(Landroid/content/SyncResult;ILandroid/os/Bundle;)V
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    .line 664
    :try_start_14
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mbSyncCanceled:Z

    if-nez v13, :cond_1d

    .line 665
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v13}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getAuthManager()Lcom/sec/android/sCloudSync/Auth/AuthManager;

    move-result-object v13

    if-eqz v13, :cond_20

    .line 666
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v13, v9, v11}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->serviceEnd(ZLjava/lang/String;)Lcom/sec/android/sCloudSync/Records/KVSResponse;
    :try_end_14
    .catch Lcom/sec/android/sCloudSync/Util/SyncException; {:try_start_14 .. :try_end_14} :catch_10
    .catch Ljava/lang/IllegalStateException; {:try_start_14 .. :try_end_14} :catch_11

    .line 677
    .end local v6    # "e":Ljava/lang/OutOfMemoryError;
    :cond_1d
    :goto_8
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mServerChangedRecords:Ljava/util/Map;

    if-eqz v13, :cond_1e

    .line 678
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mServerChangedRecords:Ljava/util/Map;

    invoke-interface {v13}, Ljava/util/Map;->clear()V

    .line 679
    :cond_1e
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    if-eqz v13, :cond_1f

    .line 680
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    .line 682
    :cond_1f
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mbSyncCanceled:Z

    .line 684
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v13

    const-string v14, "PERFORM SYNC : END"

    invoke-static {v13, v14}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 668
    .restart local v6    # "e":Ljava/lang/OutOfMemoryError;
    :cond_20
    :try_start_15
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v13

    const-string v14, "No Auth Info.. Can not call cloud-end"

    invoke-static {v13, v14}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_15
    .catch Lcom/sec/android/sCloudSync/Util/SyncException; {:try_start_15 .. :try_end_15} :catch_10
    .catch Ljava/lang/IllegalStateException; {:try_start_15 .. :try_end_15} :catch_11

    goto :goto_8

    .line 670
    :catch_10
    move-exception v7

    .line 671
    .restart local v7    # "e1":Lcom/sec/android/sCloudSync/Util/SyncException;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v13

    const-string v14, "error in call cloud-end"

    invoke-static {v13, v14}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 672
    move-object/from16 v0, p4

    iget-object v13, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v14, v13, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v16, 0x1

    add-long v14, v14, v16

    iput-wide v14, v13, Landroid/content/SyncStats;->numAuthExceptions:J

    goto :goto_8

    .line 673
    .end local v7    # "e1":Lcom/sec/android/sCloudSync/Util/SyncException;
    :catch_11
    move-exception v6

    .line 674
    .local v6, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v6}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_8

    .line 663
    .end local v6    # "e":Ljava/lang/IllegalStateException;
    :catchall_0
    move-exception v13

    .line 664
    :try_start_16
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mbSyncCanceled:Z

    if-nez v14, :cond_21

    .line 665
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v14}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getAuthManager()Lcom/sec/android/sCloudSync/Auth/AuthManager;

    move-result-object v14

    if-eqz v14, :cond_24

    .line 666
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v14, v9, v11}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->serviceEnd(ZLjava/lang/String;)Lcom/sec/android/sCloudSync/Records/KVSResponse;
    :try_end_16
    .catch Lcom/sec/android/sCloudSync/Util/SyncException; {:try_start_16 .. :try_end_16} :catch_12
    .catch Ljava/lang/IllegalStateException; {:try_start_16 .. :try_end_16} :catch_13

    .line 677
    :cond_21
    :goto_9
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mServerChangedRecords:Ljava/util/Map;

    if-eqz v14, :cond_22

    .line 678
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mServerChangedRecords:Ljava/util/Map;

    invoke-interface {v14}, Ljava/util/Map;->clear()V

    .line 679
    :cond_22
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    if-eqz v14, :cond_23

    .line 680
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    .line 682
    :cond_23
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mbSyncCanceled:Z

    .line 684
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v14

    const-string v15, "PERFORM SYNC : END"

    invoke-static {v14, v15}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    throw v13

    .line 668
    :cond_24
    :try_start_17
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v14

    const-string v15, "No Auth Info.. Can not call cloud-end"

    invoke-static {v14, v15}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_17
    .catch Lcom/sec/android/sCloudSync/Util/SyncException; {:try_start_17 .. :try_end_17} :catch_12
    .catch Ljava/lang/IllegalStateException; {:try_start_17 .. :try_end_17} :catch_13

    goto :goto_9

    .line 670
    :catch_12
    move-exception v7

    .line 671
    .restart local v7    # "e1":Lcom/sec/android/sCloudSync/Util/SyncException;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v14

    const-string v15, "error in call cloud-end"

    invoke-static {v14, v15}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 672
    move-object/from16 v0, p4

    iget-object v14, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v15, v14, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v17, 0x1

    add-long v15, v15, v17

    iput-wide v15, v14, Landroid/content/SyncStats;->numAuthExceptions:J

    goto :goto_9

    .line 673
    .end local v7    # "e1":Lcom/sec/android/sCloudSync/Util/SyncException;
    :catch_13
    move-exception v6

    .line 674
    .restart local v6    # "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v6}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_9
.end method

.method protected prepareSync()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .line 161
    const/4 v0, 0x1

    return v0
.end method

.method protected removePreSyncedRecords(Ljava/util/Map;)Z
    .locals 31
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/sCloudSync/Records/KVSItem;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1276
    .local p1, "uServerRecords":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/sec/android/sCloudSync/Records/KVSItem;>;"
    if-eqz p1, :cond_0

    invoke-interface/range {p1 .. p1}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1277
    :cond_0
    const/4 v2, 0x0

    .line 1372
    :goto_0
    return v2

    .line 1279
    :cond_1
    const/16 v23, 0x0

    .line 1280
    .local v23, "preCursor":Landroid/database/Cursor;
    new-instance v12, Landroid/content/ContentValues;

    invoke-direct {v12}, Landroid/content/ContentValues;-><init>()V

    .line 1281
    .local v12, "dirtyValues":Landroid/content/ContentValues;
    const/4 v14, 0x0

    .line 1282
    .local v14, "getIdCursor":Landroid/database/Cursor;
    invoke-interface/range {p1 .. p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v19

    .line 1283
    .local v19, "keyset":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v30, Ljava/lang/StringBuilder;

    invoke-interface/range {v19 .. v19}, Ljava/util/Set;->size()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    mul-int/lit16 v2, v2, 0x104

    move-object/from16 v0, v30

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 1284
    .local v30, "where":Ljava/lang/StringBuilder;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getKeyColumnName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v30

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " IN ("

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1285
    move-object/from16 v0, v30

    move-object/from16 v1, v19

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Tools/AppTool;->appendIds(Ljava/lang/StringBuilder;Ljava/util/Set;)V

    .line 1286
    const/16 v2, 0x29

    move-object/from16 v0, v30

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1288
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getContentUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getCallerSyncAdapter()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getAccountName()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mAccount:Landroid/accounts/Account;

    iget-object v5, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getAccountType()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mAccount:Landroid/accounts/Account;

    iget-object v5, v5, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 1293
    .local v3, "uriFromSyncAdapter":Landroid/net/Uri;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mProvider:Landroid/content/ContentProviderClient;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getProjection()[Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v23

    .line 1299
    if-nez v23, :cond_2

    .line 1300
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string v4, "RemovePreSyncedRecords: (ERROR)Cursor is null"

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1301
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1294
    :catch_0
    move-exception v13

    .line 1295
    .local v13, "e":Landroid/os/RemoteException;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "RemovePreSyncedRecords: Exception in calling query "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v13}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1296
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1304
    .end local v13    # "e":Landroid/os/RemoteException;
    :cond_2
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    .line 1305
    .local v20, "localKeys":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-nez v2, :cond_4

    .line 1306
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    .line 1307
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string v4, "RemovePreSyncedRecords: there is nothing to be removed."

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1361
    :goto_1
    new-instance v27, Ljava/util/ArrayList;

    move-object/from16 v0, v27

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1362
    .local v27, "serverKeys":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v8, 0x0

    .line 1363
    .local v8, "cnt":I
    invoke-interface/range {v27 .. v27}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .local v15, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_2
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/String;

    .line 1364
    .local v26, "serverKey":Ljava/lang/String;
    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/sCloudSync/Records/KVSItem;

    invoke-virtual {v2}, Lcom/sec/android/sCloudSync/Records/KVSItem;->isDeleted()Z

    move-result v2

    if-eqz v2, :cond_3

    move-object/from16 v0, v20

    move-object/from16 v1, v26

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1365
    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1366
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 1310
    .end local v8    # "cnt":I
    .end local v15    # "i$":Ljava/util/Iterator;
    .end local v26    # "serverKey":Ljava/lang/String;
    .end local v27    # "serverKeys":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getIdColumnName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v23

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    .line 1311
    .local v16, "id_index":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getKeyColumnName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v23

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    .line 1312
    .local v18, "key_index":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTimeStampColumnName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v23

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v28

    .line 1313
    .local v28, "time_index":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getDeletedColumnName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v23

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    .line 1316
    .local v11, "deletedIndex":I
    :cond_5
    move-object/from16 v0, v23

    move/from16 v1, v28

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v21

    .line 1317
    .local v21, "modifiedTimeUploadedtoServer":J
    move-object/from16 v0, v23

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 1318
    .local v17, "key":Ljava/lang/String;
    move-object/from16 v0, v23

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 1320
    .local v10, "deletedField":I
    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1321
    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/sec/android/sCloudSync/Records/KVSItem;

    .line 1322
    .local v25, "serverItem":Lcom/sec/android/sCloudSync/Records/KVSItem;
    if-eqz v25, :cond_9

    .line 1323
    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getTimeStamp()J

    move-result-wide v4

    cmp-long v2, v21, v4

    if-ltz v2, :cond_8

    .line 1324
    const/4 v2, 0x1

    if-eq v10, v2, :cond_7

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/sCloudSync/Records/KVSItem;->isDeleted()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1325
    invoke-virtual {v12}, Landroid/content/ContentValues;->clear()V

    .line 1326
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getDirtyColumnName()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v12, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1329
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mProvider:Landroid/content/ContentProviderClient;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getIdColumnName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getKeyColumnName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "=\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 1330
    if-eqz v14, :cond_7

    .line 1331
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1332
    const/4 v2, 0x0

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v24

    .line 1333
    .local v24, "rowId":I
    move/from16 v0, v24

    int-to-long v4, v0

    invoke-static {v3, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v29

    .line 1334
    .local v29, "updateUri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mProvider:Landroid/content/ContentProviderClient;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, v29

    invoke-virtual {v2, v0, v12, v4, v5}, Landroid/content/ContentProviderClient;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v9

    .line 1335
    .local v9, "count":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "UPDATE DIRTY, Key : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1337
    .end local v9    # "count":I
    .end local v24    # "rowId":I
    .end local v29    # "updateUri":Landroid/net/Uri;
    :cond_6
    invoke-interface {v14}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1348
    :cond_7
    :goto_3
    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1356
    :goto_4
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_5

    .line 1358
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    .line 1340
    :catch_1
    move-exception v13

    .line 1341
    .restart local v13    # "e":Landroid/os/RemoteException;
    invoke-virtual {v13}, Landroid/os/RemoteException;->printStackTrace()V

    .line 1342
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string v4, "COULDN\'T UPDATE DIRTY = 1"

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1343
    if-eqz v14, :cond_7

    .line 1344
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    goto :goto_3

    .line 1350
    .end local v13    # "e":Landroid/os/RemoteException;
    :cond_8
    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/sCloudSync/Records/KVSItem;

    move-object/from16 v0, v23

    move/from16 v1, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/sec/android/sCloudSync/Records/KVSItem;->setID(J)V

    goto :goto_4

    .line 1354
    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string v4, "Sync Broken : There is a duplicated record on Local."

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 1369
    .end local v10    # "deletedField":I
    .end local v11    # "deletedIndex":I
    .end local v16    # "id_index":I
    .end local v17    # "key":Ljava/lang/String;
    .end local v18    # "key_index":I
    .end local v21    # "modifiedTimeUploadedtoServer":J
    .end local v25    # "serverItem":Lcom/sec/android/sCloudSync/Records/KVSItem;
    .end local v28    # "time_index":I
    .restart local v8    # "cnt":I
    .restart local v15    # "i$":Ljava/util/Iterator;
    .restart local v27    # "serverKeys":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_a
    if-lez v8, :cond_b

    .line 1370
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "RemovePreSyncedRecords: remove from serverChanges that already deleted in local : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1372
    :cond_b
    const/4 v2, 0x1

    goto/16 :goto_0
.end method

.method protected final resetLastSyncTime()V
    .locals 1

    .prologue
    .line 151
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mLastSyncTime:Ljava/lang/String;

    .line 152
    return-void
.end method

.method protected updateLocalDb(Landroid/content/SyncStats;)V
    .locals 7
    .param p1, "stats"    # Landroid/content/SyncStats;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .line 245
    const-wide/16 v0, 0x0

    .line 246
    .local v0, "downloadSize":J
    iget-object v5, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    if-nez v5, :cond_0

    .line 247
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getBuilder()Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    .line 249
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 250
    .local v4, "keys":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v5, p0, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->mServerChangedRecords:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 251
    .local v3, "keyItem":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/sec/android/sCloudSync/Records/KVSItem;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 252
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/sCloudSync/Records/KVSItem;

    invoke-virtual {v5}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getSize()J

    move-result-wide v5

    add-long/2addr v0, v5

    .line 253
    const-wide/32 v5, 0x100000

    cmp-long v5, v0, v5

    if-lez v5, :cond_1

    .line 254
    invoke-direct {p0, v4, p1}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getItemsFromKeys(Ljava/util/List;Landroid/content/SyncStats;)V

    .line 255
    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 256
    const-wide/16 v0, 0x0

    goto :goto_0

    .line 259
    .end local v3    # "keyItem":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/sec/android/sCloudSync/Records/KVSItem;>;"
    :cond_2
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_3

    .line 260
    invoke-direct {p0, v4, p1}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getItemsFromKeys(Ljava/util/List;Landroid/content/SyncStats;)V

    .line 262
    :cond_3
    return-void
.end method

.method protected updatetoServer(Ljava/util/List;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/sCloudSync/Records/RecordSetItem;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/sCloudSync/Records/RecordBase;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;,
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 830
    .local p1, "setList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordSetItem;>;"
    .local p2, "deleteList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordBase;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 833
    .local v0, "operations":Ljava/util/List;, "Ljava/util/List<Landroid/content/ContentProviderOperation;>;"
    invoke-direct {p0, p2, v0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->deleteFromServer(Ljava/util/List;Ljava/util/List;)V

    .line 836
    invoke-direct {p0, p1, v0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->uploadToServer(Ljava/util/List;Ljava/util/List;)V

    .line 838
    invoke-virtual {p0, p2, p1}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->handleDeleteList(Ljava/util/List;Ljava/util/List;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 840
    invoke-direct {p0, v0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->updateLocal(Ljava/util/List;)V

    .line 843
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 844
    return-void
.end method
