.class public Lcom/sec/android/sCloudSync/SyncAdapters/SPlannerEventSyncAdapter;
.super Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;
.source "SPlannerEventSyncAdapter.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "CalendarSyncAdapter"


# instance fields
.field private mDependentEvents:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/sCloudSync/Records/KVSItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 52
    new-instance v0, Lcom/sec/android/sCloudSync/ServiceManagers/SCloudServiceManager;

    const-string v1, "8kLTKS0V1y"

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v2}, Lcom/sec/android/sCloudSync/ServiceManagers/SCloudServiceManager;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    invoke-direct {p0, p1, v0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;-><init>(Landroid/content/Context;Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;)V

    .line 49
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/sCloudSync/SyncAdapters/SPlannerEventSyncAdapter;->mDependentEvents:Ljava/util/Map;

    .line 53
    return-void
.end method


# virtual methods
.method public doInsert(Lcom/sec/android/sCloudSync/Records/KVSItem;Lcom/sec/android/sCloudSync/Records/RecordItem;Landroid/content/SyncStats;)Z
    .locals 6
    .param p1, "item"    # Lcom/sec/android/sCloudSync/Records/KVSItem;
    .param p2, "record"    # Lcom/sec/android/sCloudSync/Records/RecordItem;
    .param p3, "stats"    # Landroid/content/SyncStats;

    .prologue
    .line 102
    :try_start_0
    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->isDeleted()Z

    move-result v2

    if-nez v2, :cond_1

    .line 103
    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 104
    .local v0, "CalData":Ljava/lang/String;
    if-eqz v0, :cond_3

    .line 105
    const-string v2, "original_id"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "original_sync_id"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 112
    :cond_0
    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getTimeStamp()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Lcom/sec/android/sCloudSync/Records/KVSItem;->setTimeStamp(J)V

    .line 113
    invoke-virtual {p1, v0}, Lcom/sec/android/sCloudSync/Records/KVSItem;->setValue(Ljava/lang/String;)V

    .line 114
    iget-object v2, p0, Lcom/sec/android/sCloudSync/SyncAdapters/SPlannerEventSyncAdapter;->mDependentEvents:Ljava/util/Map;

    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getKEY()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    .end local v0    # "CalData":Ljava/lang/String;
    :cond_1
    :goto_0
    const/4 v2, 0x1

    :goto_1
    return v2

    .line 117
    .restart local v0    # "CalData":Ljava/lang/String;
    :cond_2
    iget-object v2, p0, Lcom/sec/android/sCloudSync/SyncAdapters/SPlannerEventSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getKEY()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getTimeStamp()Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, v0, v3, v4, v5}, Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;->insert(Ljava/lang/String;Ljava/lang/String;J)Z

    .line 118
    iget-wide v2, p3, Landroid/content/SyncStats;->numInserts:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, p3, Landroid/content/SyncStats;->numInserts:J
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 129
    .end local v0    # "CalData":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 130
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    const-string v2, "CalendarSyncAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IllegalArgumentException while inserting calendar ignoring this event :  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 126
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    .restart local v0    # "CalData":Ljava/lang/String;
    :cond_3
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public doUpdateDelete(Lcom/sec/android/sCloudSync/Records/KVSItem;Lcom/sec/android/sCloudSync/Records/RecordItem;Landroid/content/SyncStats;)Z
    .locals 10
    .param p1, "item"    # Lcom/sec/android/sCloudSync/Records/KVSItem;
    .param p2, "record"    # Lcom/sec/android/sCloudSync/Records/RecordItem;
    .param p3, "stats"    # Landroid/content/SyncStats;

    .prologue
    const-wide/16 v8, 0x1

    .line 73
    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->isDeleted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    iget-wide v0, p3, Landroid/content/SyncStats;->numDeletes:J

    add-long/2addr v0, v8

    iput-wide v0, p3, Landroid/content/SyncStats;->numDeletes:J

    .line 76
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/sCloudSync/SyncAdapters/SPlannerEventSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getID()J

    move-result-wide v2

    const/4 v4, 0x0

    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getKEY()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;->delete(Landroid/net/Uri;JLjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 77
    :catch_0
    move-exception v7

    .line 79
    .local v7, "e":Landroid/os/RemoteException;
    invoke-virtual {v7}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 83
    .end local v7    # "e":Landroid/os/RemoteException;
    :cond_0
    :try_start_1
    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getValue()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/sCloudSync/SyncAdapters/SPlannerEventSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getTimeStamp()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getID()J

    move-result-wide v4

    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getKEY()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;->update(Ljava/lang/String;JJLjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 84
    :cond_1
    const-string v0, "CalendarSyncAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to update record with key:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getKEY()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 91
    :catch_1
    move-exception v7

    .line 92
    .local v7, "e":Ljava/lang/IllegalArgumentException;
    const-string v0, "CalendarSyncAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "IllegalArgumentException while updating calendar ignoring this event :  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 89
    .end local v7    # "e":Ljava/lang/IllegalArgumentException;
    :cond_2
    :try_start_2
    iget-wide v0, p3, Landroid/content/SyncStats;->numUpdates:J

    add-long/2addr v0, v8

    iput-wide v0, p3, Landroid/content/SyncStats;->numUpdates:J
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0
.end method

.method public getAccountName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 194
    const-string v0, "account_name"

    return-object v0
.end method

.method public getAccountType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 199
    const-string v0, "account_type"

    return-object v0
.end method

.method public getBuilder()Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;
    .locals 4

    .prologue
    .line 189
    new-instance v0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SPlannerEventSyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SPlannerEventSyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SPlannerEventSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;-><init>(Landroid/content/ContentProviderClient;Landroid/accounts/Account;Landroid/content/Context;)V

    return-object v0
.end method

.method public getCallerSyncAdapter()Ljava/lang/String;
    .locals 1

    .prologue
    .line 154
    const-string v0, "caller_is_syncadapter"

    return-object v0
.end method

.method public getCid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    const-string v0, "8kLTKS0V1y"

    return-object v0
.end method

.method public getContentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 159
    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method protected getCtidKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 203
    const-string v0, "PE"

    return-object v0
.end method

.method public getDeletedColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 164
    const-string v0, "deleted"

    return-object v0
.end method

.method public getDirtyColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 184
    const-string v0, "dirty"

    return-object v0
.end method

.method public getIdColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 179
    const-string v0, "_id"

    return-object v0
.end method

.method public getKeyColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 169
    const-string v0, "_sync_id"

    return-object v0
.end method

.method public getSyncAdapterName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    const-string v0, "CALENDAR"

    return-object v0
.end method

.method protected getSyncStateDataColumn()Ljava/lang/String;
    .locals 1

    .prologue
    .line 208
    const-string v0, "data"

    return-object v0
.end method

.method protected getSyncStateURI()Landroid/net/Uri;
    .locals 3

    .prologue
    .line 213
    sget-object v0, Landroid/provider/CalendarContract$SyncState;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SPlannerEventSyncAdapter;->getAccountName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SPlannerEventSyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v2

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SPlannerEventSyncAdapter;->getAccountType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SPlannerEventSyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v2

    iget-object v2, v2, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    const-string v0, "CalendarSyncAdapter"

    return-object v0
.end method

.method public getTimeStampColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 174
    const-string v0, "sync_data5"

    return-object v0
.end method

.method protected updateLocalDb(Landroid/content/SyncStats;)V
    .locals 8
    .param p1, "stats"    # Landroid/content/SyncStats;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .line 138
    invoke-super {p0, p1}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->updateLocalDb(Landroid/content/SyncStats;)V

    .line 140
    iget-object v3, p0, Lcom/sec/android/sCloudSync/SyncAdapters/SPlannerEventSyncAdapter;->mDependentEvents:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 141
    .local v2, "item":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/sec/android/sCloudSync/Records/KVSItem;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/sCloudSync/Records/KVSItem;

    invoke-virtual {v3}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 142
    .local v0, "calData":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 143
    iget-object v5, p0, Lcom/sec/android/sCloudSync/SyncAdapters/SPlannerEventSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/sCloudSync/Records/KVSItem;

    invoke-virtual {v4}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getTimeStamp()J

    move-result-wide v6

    invoke-virtual {v5, v0, v3, v6, v7}, Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;->insert(Ljava/lang/String;Ljava/lang/String;J)Z

    goto :goto_0

    .line 147
    .end local v0    # "calData":Ljava/lang/String;
    .end local v2    # "item":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/sec/android/sCloudSync/Records/KVSItem;>;"
    :cond_1
    iget-object v3, p0, Lcom/sec/android/sCloudSync/SyncAdapters/SPlannerEventSyncAdapter;->mDependentEvents:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->clear()V

    .line 148
    iget-object v3, p0, Lcom/sec/android/sCloudSync/SyncAdapters/SPlannerEventSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    invoke-virtual {v3}, Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;->doApplyBatch()Z

    .line 150
    return-void
.end method
