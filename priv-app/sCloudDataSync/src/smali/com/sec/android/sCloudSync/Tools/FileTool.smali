.class public final Lcom/sec/android/sCloudSync/Tools/FileTool;
.super Ljava/lang/Object;
.source "FileTool.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "FileTool"

.field private static mBytes:[B

.field private static mFileBuffer:[B

.field private static mMessageDigest:Ljava/security/MessageDigest;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0x2000

    .line 35
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/sCloudSync/Tools/FileTool;->mMessageDigest:Ljava/security/MessageDigest;

    .line 36
    new-array v0, v1, [B

    sput-object v0, Lcom/sec/android/sCloudSync/Tools/FileTool;->mBytes:[B

    .line 37
    new-array v0, v1, [B

    sput-object v0, Lcom/sec/android/sCloudSync/Tools/FileTool;->mFileBuffer:[B

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized getByteArr(Ljava/io/InputStream;)[B
    .locals 6
    .param p0, "inputStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 129
    const-class v4, Lcom/sec/android/sCloudSync/Tools/FileTool;

    monitor-enter v4

    :try_start_0
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 130
    .local v1, "byteOpStream":Ljava/io/ByteArrayOutputStream;
    const/16 v3, 0x400

    new-array v0, v3, [B

    .line 131
    .local v0, "buff":[B
    :goto_0
    const/4 v3, 0x0

    const/16 v5, 0x400

    invoke-virtual {p0, v0, v3, v5}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    .local v2, "len":I
    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 132
    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 129
    .end local v0    # "buff":[B
    .end local v1    # "byteOpStream":Ljava/io/ByteArrayOutputStream;
    .end local v2    # "len":I
    :catchall_0
    move-exception v3

    monitor-exit v4

    throw v3

    .line 133
    .restart local v0    # "buff":[B
    .restart local v1    # "byteOpStream":Ljava/io/ByteArrayOutputStream;
    .restart local v2    # "len":I
    :cond_0
    :try_start_1
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 134
    monitor-exit v4

    return-object v0
.end method

.method public static declared-synchronized getMessageDigest(Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p0, "filepath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/NoSuchAlgorithmException;
        }
    .end annotation

    .prologue
    .line 41
    const-class v8, Lcom/sec/android/sCloudSync/Tools/FileTool;

    monitor-enter v8

    const/4 v1, 0x0

    .line 43
    .local v1, "bHex":I
    :try_start_0
    sget-object v7, Lcom/sec/android/sCloudSync/Tools/FileTool;->mMessageDigest:Ljava/security/MessageDigest;

    if-nez v7, :cond_0

    .line 44
    const-string v7, "MD5"

    invoke-static {v7}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v7

    sput-object v7, Lcom/sec/android/sCloudSync/Tools/FileTool;->mMessageDigest:Ljava/security/MessageDigest;

    .line 48
    :goto_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 49
    .local v3, "fis":Ljava/io/FileInputStream;
    const/4 v5, 0x0

    .line 51
    .local v5, "len":I
    :goto_1
    :try_start_1
    sget-object v7, Lcom/sec/android/sCloudSync/Tools/FileTool;->mBytes:[B

    invoke-virtual {v3, v7}, Ljava/io/FileInputStream;->read([B)I

    move-result v5

    if-lez v5, :cond_1

    .line 52
    sget-object v7, Lcom/sec/android/sCloudSync/Tools/FileTool;->mMessageDigest:Ljava/security/MessageDigest;

    sget-object v9, Lcom/sec/android/sCloudSync/Tools/FileTool;->mBytes:[B

    const/4 v10, 0x0

    invoke-virtual {v7, v9, v10, v5}, Ljava/security/MessageDigest;->update([BII)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 54
    :catch_0
    move-exception v2

    .line 55
    .local v2, "e":Ljava/io/IOException;
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    .line 56
    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 41
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .end local v5    # "len":I
    :catchall_0
    move-exception v7

    monitor-exit v8

    throw v7

    .line 46
    :cond_0
    :try_start_3
    sget-object v7, Lcom/sec/android/sCloudSync/Tools/FileTool;->mMessageDigest:Ljava/security/MessageDigest;

    invoke-virtual {v7}, Ljava/security/MessageDigest;->reset()V

    goto :goto_0

    .line 58
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "len":I
    :cond_1
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    .line 59
    sget-object v7, Lcom/sec/android/sCloudSync/Tools/FileTool;->mMessageDigest:Ljava/security/MessageDigest;

    invoke-virtual {v7}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v6

    .line 60
    .local v6, "md5Data":[B
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 62
    .local v0, "UniqueID":Ljava/lang/StringBuilder;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    array-length v7, v6

    if-ge v4, v7, :cond_3

    .line 63
    aget-byte v7, v6, v4

    and-int/lit16 v1, v7, 0xff

    .line 64
    const/16 v7, 0xf

    if-gt v1, v7, :cond_2

    .line 66
    const/16 v7, 0x30

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 68
    :cond_2
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 70
    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v7

    monitor-exit v8

    return-object v7
.end method

.method public static isSameFile(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p0, "filepath"    # Ljava/lang/String;
    .param p1, "checkSum"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 76
    :try_start_0
    invoke-static {p0}, Lcom/sec/android/sCloudSync/Tools/FileTool;->getMessageDigest(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 85
    .local v0, "UniqueID":Ljava/lang/String;
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .end local v0    # "UniqueID":Ljava/lang/String;
    :goto_0
    return v2

    .line 77
    :catch_0
    move-exception v1

    .line 78
    .local v1, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v1}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    goto :goto_0

    .line 80
    .end local v1    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_1
    move-exception v1

    .line 81
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public static declared-synchronized writeToFile(Ljava/io/InputStream;Ljava/lang/String;)V
    .locals 13
    .param p0, "inputStream"    # Ljava/io/InputStream;
    .param p1, "filepath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 89
    const-class v10, Lcom/sec/android/sCloudSync/Tools/FileTool;

    monitor-enter v10

    const/4 v3, 0x0

    .line 92
    .local v3, "fileOpStream":Ljava/io/FileOutputStream;
    :try_start_0
    const-string v9, "/"

    invoke-virtual {p1, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 93
    .local v8, "split":[Ljava/lang/String;
    array-length v9, v8

    add-int/lit8 v9, v9, -0x1

    aget-object v2, v8, v9

    .line 94
    .local v2, "fileName":Ljava/lang/String;
    const/4 v9, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v11

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v12

    sub-int/2addr v11, v12

    invoke-virtual {p1, v9, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 96
    .local v5, "folderPath":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 97
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v9

    if-nez v9, :cond_1

    .line 98
    const-string v9, "FileTool"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Creating folder : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v11}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v7

    .line 101
    .local v7, "result":Z
    if-nez v7, :cond_1

    .line 103
    const-string v9, "FileTool"

    const-string v11, "ORSMetaResponse.fromBinaryFile(): Can not create directory. "

    invoke-static {v9, v11}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    new-instance v9, Ljava/io/IOException;

    invoke-direct {v9}, Ljava/io/IOException;-><init>()V

    throw v9
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 116
    .end local v1    # "file":Ljava/io/File;
    .end local v2    # "fileName":Ljava/lang/String;
    .end local v5    # "folderPath":Ljava/lang/String;
    .end local v7    # "result":Z
    .end local v8    # "split":[Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 117
    .local v0, "e":Ljava/io/IOException;
    :goto_0
    :try_start_1
    const-string v9, "FileTool"

    const-string v11, "ORSMetaResponse.fromBinaryFile(): IO Exception "

    invoke-static {v9, v11}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 120
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v9

    :goto_1
    if-eqz v3, :cond_0

    .line 121
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V

    :cond_0
    throw v9
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 89
    :catchall_1
    move-exception v9

    :goto_2
    monitor-exit v10

    throw v9

    .line 109
    .restart local v1    # "file":Ljava/io/File;
    .restart local v2    # "fileName":Ljava/lang/String;
    .restart local v5    # "folderPath":Ljava/lang/String;
    .restart local v8    # "split":[Ljava/lang/String;
    :cond_1
    :try_start_3
    new-instance v4, Ljava/io/FileOutputStream;

    const/4 v9, 0x0

    invoke-direct {v4, p1, v9}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;Z)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 110
    .end local v3    # "fileOpStream":Ljava/io/FileOutputStream;
    .local v4, "fileOpStream":Ljava/io/FileOutputStream;
    const/4 v6, 0x0

    .line 112
    .local v6, "len":I
    :goto_3
    :try_start_4
    sget-object v9, Lcom/sec/android/sCloudSync/Tools/FileTool;->mFileBuffer:[B

    invoke-virtual {p0, v9}, Ljava/io/InputStream;->read([B)I

    move-result v6

    if-lez v6, :cond_2

    .line 113
    sget-object v9, Lcom/sec/android/sCloudSync/Tools/FileTool;->mFileBuffer:[B

    const/4 v11, 0x0

    invoke-virtual {v4, v9, v11, v6}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    goto :goto_3

    .line 116
    :catch_1
    move-exception v0

    move-object v3, v4

    .end local v4    # "fileOpStream":Ljava/io/FileOutputStream;
    .restart local v3    # "fileOpStream":Ljava/io/FileOutputStream;
    goto :goto_0

    .line 120
    .end local v3    # "fileOpStream":Ljava/io/FileOutputStream;
    .restart local v4    # "fileOpStream":Ljava/io/FileOutputStream;
    :cond_2
    if-eqz v4, :cond_3

    .line 121
    :try_start_5
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 123
    :cond_3
    monitor-exit v10

    return-void

    .line 120
    :catchall_2
    move-exception v9

    move-object v3, v4

    .end local v4    # "fileOpStream":Ljava/io/FileOutputStream;
    .restart local v3    # "fileOpStream":Ljava/io/FileOutputStream;
    goto :goto_1

    .line 89
    .end local v3    # "fileOpStream":Ljava/io/FileOutputStream;
    .restart local v4    # "fileOpStream":Ljava/io/FileOutputStream;
    :catchall_3
    move-exception v9

    move-object v3, v4

    .end local v4    # "fileOpStream":Ljava/io/FileOutputStream;
    .restart local v3    # "fileOpStream":Ljava/io/FileOutputStream;
    goto :goto_2
.end method
