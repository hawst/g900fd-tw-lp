.class public final Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;
.super Ljava/lang/Object;
.source "NetworkClient.java"

# interfaces
.implements Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientResponseListener;


# static fields
.field private static final CONNECTION_TIMEOUT_DURATION:I = 0xea60

.field private static final DEFAULT_PORT:I = 0x50

.field private static final JSON_CONTENT:Ljava/lang/String;

.field private static final OCTET_STREAM:Ljava/lang/String;

.field private static final SECURE_PORT:I = 0x1bb

.field private static final SOCKET_BUFFER_SIZE:I = 0x80000

.field private static final TAG:Ljava/lang/String; = "NetworkClient"


# instance fields
.field private bNotified:Z

.field private mHttpClient:Lorg/apache/http/client/HttpClient;

.field private mNetworkResponse:Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;

.field private mServiceApiHelper:Lcom/sec/android/sCloudSync/ServiceManagers/IPDMServiceApiHelper;

.field private mThreadIOExceptionMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mThreadMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;",
            ">;"
        }
    .end annotation
.end field

.field private final syncLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 68
    const-string v0, "application/octet-stream"

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->OCTET_STREAM:Ljava/lang/String;

    .line 69
    const-string v0, "application/json;charset=utf-8"

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->JSON_CONTENT:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    new-instance v0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;

    invoke-direct {v0}, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;-><init>()V

    iput-object v0, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->mNetworkResponse:Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;

    .line 72
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->mHttpClient:Lorg/apache/http/client/HttpClient;

    .line 75
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->syncLock:Ljava/lang/Object;

    .line 117
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->mThreadMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 177
    return-void
.end method

.method private execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;
    .locals 12
    .param p1, "request"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v11, 0x3

    const/4 v8, 0x0

    .line 248
    iget-object v7, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->mServiceApiHelper:Lcom/sec/android/sCloudSync/ServiceManagers/IPDMServiceApiHelper;

    invoke-interface {v7, p1}, Lcom/sec/android/sCloudSync/ServiceManagers/IPDMServiceApiHelper;->addHeaderForApi(Lorg/apache/http/client/methods/HttpUriRequest;)V

    .line 251
    const-string v7, "NetworkClient"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Request : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface {p1}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v5

    .line 253
    .local v5, "requestID":Ljava/lang/String;
    new-instance v3, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;

    invoke-direct {p0}, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->getHttpClient()Lorg/apache/http/client/HttpClient;

    move-result-object v7

    invoke-direct {v3, p1, v7, p0, v5}, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;-><init>(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/HttpClient;Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientResponseListener;Ljava/lang/String;)V

    .line 256
    .local v3, "httpClientThread":Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;
    const/4 v4, 0x0

    .line 258
    .local v4, "httpResponse":Lorg/apache/http/HttpResponse;
    :try_start_0
    iget-object v7, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->mThreadMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v7, v5, v3}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 259
    invoke-virtual {v3}, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->start()V

    .line 260
    invoke-virtual {v3}, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->getThreadLock()Ljava/lang/Object;

    move-result-object v6

    .line 261
    .local v6, "threadLock":Ljava/lang/Object;
    monitor-enter v6
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 262
    const/4 v7, 0x0

    :try_start_1
    iput-boolean v7, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->bNotified:Z

    .line 263
    :goto_0
    iget-boolean v7, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->bNotified:Z

    if-nez v7, :cond_0

    .line 264
    invoke-virtual {v6}, Ljava/lang/Object;->wait()V

    goto :goto_0

    .line 266
    :catchall_0
    move-exception v7

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v7
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    .line 288
    .end local v6    # "threadLock":Ljava/lang/Object;
    :catch_0
    move-exception v2

    .line 289
    .local v2, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 290
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->close()V

    move-object v1, v8

    .line 305
    .end local v2    # "e":Ljava/lang/InterruptedException;
    :goto_1
    return-object v1

    .line 266
    .restart local v6    # "threadLock":Ljava/lang/Object;
    :cond_0
    :try_start_3
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 268
    :try_start_4
    iget-object v7, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->mThreadMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v7, v5}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    move-object v0, v7

    check-cast v0, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;

    move-object v3, v0

    .line 269
    if-nez v3, :cond_3

    .line 270
    iget-object v9, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->syncLock:Ljava/lang/Object;

    monitor-enter v9
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0

    .line 271
    :try_start_5
    iget-object v7, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->mThreadIOExceptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    if-eqz v7, :cond_2

    if-eqz v5, :cond_2

    iget-object v7, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->mThreadIOExceptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v7, v5}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-ne v11, v7, :cond_2

    .line 272
    new-instance v1, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;

    invoke-direct {v1}, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;-><init>()V

    .line 273
    .local v1, "IOExceptionResponse":Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;
    const/4 v7, 0x3

    invoke-virtual {v1, v7}, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;->setStatus(I)V

    .line 274
    iget-object v7, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->mThreadIOExceptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v7, v5}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 275
    iget-object v7, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->mThreadIOExceptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v7}, Ljava/util/concurrent/ConcurrentHashMap;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 276
    const/4 v7, 0x0

    iput-object v7, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->mThreadIOExceptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 277
    :cond_1
    monitor-exit v9

    goto :goto_1

    .line 282
    .end local v1    # "IOExceptionResponse":Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;
    :catchall_1
    move-exception v7

    monitor-exit v9
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    throw v7
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_0

    .line 280
    :cond_2
    :try_start_7
    monitor-exit v9
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    move-object v1, v8

    goto :goto_1

    .line 285
    :cond_3
    :try_start_8
    invoke-virtual {v3}, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->getHttpResponse()Lorg/apache/http/HttpResponse;

    move-result-object v4

    .line 286
    iget-object v7, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->mThreadMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v7, v5}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_8
    .catch Ljava/lang/InterruptedException; {:try_start_8 .. :try_end_8} :catch_0

    .line 294
    if-nez v4, :cond_4

    .line 296
    new-instance v7, Ljava/io/IOException;

    const-string v8, "Response is NULL."

    invoke-direct {v7, v8}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 299
    :cond_4
    iget-object v7, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->mNetworkResponse:Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;

    invoke-direct {p0, v4, v7}, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->handleResponse(Lorg/apache/http/HttpResponse;Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;)V

    .line 301
    invoke-virtual {v3}, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->isClosed()Z

    move-result v7

    if-eqz v7, :cond_5

    move-object v1, v8

    .line 302
    goto :goto_1

    .line 305
    :cond_5
    iget-object v1, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->mNetworkResponse:Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;

    goto :goto_1
.end method

.method private getHttpClient()Lorg/apache/http/client/HttpClient;
    .locals 8

    .prologue
    .line 147
    iget-object v4, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->mHttpClient:Lorg/apache/http/client/HttpClient;

    if-eqz v4, :cond_0

    .line 148
    iget-object v4, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->mHttpClient:Lorg/apache/http/client/HttpClient;

    .line 173
    :goto_0
    return-object v4

    .line 150
    :cond_0
    :try_start_0
    new-instance v2, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v2}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 151
    .local v2, "params":Lorg/apache/http/params/HttpParams;
    sget-object v4, Lorg/apache/http/HttpVersion;->HTTP_1_1:Lorg/apache/http/HttpVersion;

    invoke-static {v2, v4}, Lorg/apache/http/params/HttpProtocolParams;->setVersion(Lorg/apache/http/params/HttpParams;Lorg/apache/http/ProtocolVersion;)V

    .line 152
    const-string v4, "UTF-8"

    invoke-static {v2, v4}, Lorg/apache/http/params/HttpProtocolParams;->setContentCharset(Lorg/apache/http/params/HttpParams;Ljava/lang/String;)V

    .line 153
    iget-object v4, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->mServiceApiHelper:Lcom/sec/android/sCloudSync/ServiceManagers/IPDMServiceApiHelper;

    invoke-interface {v4}, Lcom/sec/android/sCloudSync/ServiceManagers/IPDMServiceApiHelper;->getUserAgent()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lorg/apache/http/params/HttpProtocolParams;->setUserAgent(Lorg/apache/http/params/HttpParams;Ljava/lang/String;)V

    .line 154
    const v4, 0xea60

    invoke-static {v2, v4}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 155
    const v4, 0xea60

    invoke-static {v2, v4}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 156
    const/high16 v4, 0x80000

    invoke-static {v2, v4}, Lorg/apache/http/params/HttpConnectionParams;->setSocketBufferSize(Lorg/apache/http/params/HttpParams;I)V

    .line 157
    const/4 v4, 0x0

    invoke-static {v2, v4}, Lorg/apache/http/params/HttpConnectionParams;->setStaleCheckingEnabled(Lorg/apache/http/params/HttpParams;Z)V

    .line 158
    const/4 v4, 0x0

    invoke-static {v2, v4}, Lorg/apache/http/client/params/HttpClientParams;->setRedirecting(Lorg/apache/http/params/HttpParams;Z)V

    .line 159
    const-string v4, "http.protocol.expect-continue"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-interface {v2, v4, v5}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 161
    new-instance v3, Lorg/apache/http/conn/scheme/SchemeRegistry;

    invoke-direct {v3}, Lorg/apache/http/conn/scheme/SchemeRegistry;-><init>()V

    .line 162
    .local v3, "registry":Lorg/apache/http/conn/scheme/SchemeRegistry;
    new-instance v4, Lorg/apache/http/conn/scheme/Scheme;

    const-string v5, "http"

    invoke-static {}, Lorg/apache/http/conn/scheme/PlainSocketFactory;->getSocketFactory()Lorg/apache/http/conn/scheme/PlainSocketFactory;

    move-result-object v6

    const/16 v7, 0x50

    invoke-direct {v4, v5, v6, v7}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v3, v4}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 163
    new-instance v4, Lorg/apache/http/conn/scheme/Scheme;

    const-string v5, "https"

    invoke-static {}, Lorg/apache/http/conn/ssl/SSLSocketFactory;->getSocketFactory()Lorg/apache/http/conn/ssl/SSLSocketFactory;

    move-result-object v6

    const/16 v7, 0x1bb

    invoke-direct {v4, v5, v6, v7}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v3, v4}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 165
    new-instance v0, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;

    invoke-direct {v0, v2, v3}, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;-><init>(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/scheme/SchemeRegistry;)V

    .line 167
    .local v0, "ccm":Lorg/apache/http/conn/ClientConnectionManager;
    new-instance v4, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v4, v0, v2}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V

    iput-object v4, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->mHttpClient:Lorg/apache/http/client/HttpClient;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 173
    .end local v0    # "ccm":Lorg/apache/http/conn/ClientConnectionManager;
    .end local v2    # "params":Lorg/apache/http/params/HttpParams;
    .end local v3    # "registry":Lorg/apache/http/conn/scheme/SchemeRegistry;
    :goto_1
    iget-object v4, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->mHttpClient:Lorg/apache/http/client/HttpClient;

    goto :goto_0

    .line 168
    :catch_0
    move-exception v1

    .line 169
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 170
    new-instance v4, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v4}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    iput-object v4, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->mHttpClient:Lorg/apache/http/client/HttpClient;

    goto :goto_1
.end method

.method private handleResponse(Lorg/apache/http/HttpResponse;Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;)V
    .locals 10
    .param p1, "httpResponse"    # Lorg/apache/http/HttpResponse;
    .param p2, "networkResponse"    # Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 208
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v6

    invoke-interface {v6}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v5

    .line 209
    .local v5, "status":I
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 210
    .local v1, "entity":Lorg/apache/http/HttpEntity;
    const/16 v6, 0x190

    if-eq v5, v6, :cond_0

    const/16 v6, 0xc8

    if-eq v5, v6, :cond_0

    .line 211
    const-string v6, "NetworkClient"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "There was a problem on the Sync Server. RESULT CODE: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    :cond_0
    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;->clear()V

    .line 215
    invoke-virtual {p2, v5}, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;->setStatus(I)V

    .line 217
    const-string v6, "Content-Type"

    invoke-interface {p1, v6}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v2

    .line 219
    .local v2, "header":Lorg/apache/http/Header;
    if-eqz v2, :cond_4

    if-eqz v1, :cond_4

    .line 220
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    .line 222
    .local v3, "headerString":Ljava/lang/String;
    sget-object v6, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->OCTET_STREAM:Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 223
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v6

    invoke-virtual {p2, v6}, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;->setFileInStream(Ljava/io/InputStream;)V

    .line 243
    :goto_0
    if-eqz v1, :cond_1

    const/16 v6, 0x194

    if-ne v5, v6, :cond_1

    .line 244
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 246
    :cond_1
    return-void

    .line 224
    :cond_2
    sget-object v6, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->JSON_CONTENT:Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 227
    :try_start_0
    const-string v6, "NetworkClient"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "SIZE :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    invoke-static {v1}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v4

    .line 229
    .local v4, "response":Ljava/lang/String;
    invoke-virtual {p2, v4}, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;->setBody(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 230
    .end local v4    # "response":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 231
    .local v0, "e":Ljava/lang/OutOfMemoryError;
    const-string v6, "NetworkClient"

    const-string v7, "Converting HTTPEntity to String returns out of Memory"

    invoke-static {v6, v7}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    new-instance v6, Ljava/io/IOException;

    const-string v7, "Converting HTTPEntity to String returns out of Memory"

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 235
    .end local v0    # "e":Ljava/lang/OutOfMemoryError;
    :cond_3
    const-string v6, "NetworkClient"

    const-string v7, "Incorrect Header"

    invoke-static {v6, v7}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    new-instance v6, Ljava/io/IOException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Incorrect Header"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 239
    .end local v3    # "headerString":Ljava/lang/String;
    :cond_4
    const-string v6, "NetworkClient"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Incorrect Response. Header : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", entity : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    new-instance v6, Ljava/io/IOException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Incorrect Response. Header : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", entity : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6
.end method

.method private isUnsafe(C)Z
    .locals 2
    .param p1, "ch"    # C

    .prologue
    const/4 v0, 0x0

    .line 419
    const/16 v1, 0x30

    if-lt p1, v1, :cond_0

    const/16 v1, 0x39

    if-le p1, v1, :cond_2

    :cond_0
    const/16 v1, 0x41

    if-lt p1, v1, :cond_1

    const/16 v1, 0x5a

    if-le p1, v1, :cond_2

    :cond_1
    const/16 v1, 0x61

    if-lt p1, v1, :cond_3

    const/16 v1, 0x7a

    if-gt p1, v1, :cond_3

    .line 422
    :cond_2
    :goto_0
    return v0

    :cond_3
    const-string v1, " $+,;@[]{}"

    invoke-virtual {v1, p1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-ltz v1, :cond_2

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private requestFile(Lorg/apache/http/client/methods/HttpUriRequest;)Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;
    .locals 12
    .param p1, "request"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v11, 0x3

    const/4 v8, 0x0

    .line 320
    iget-object v7, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->mServiceApiHelper:Lcom/sec/android/sCloudSync/ServiceManagers/IPDMServiceApiHelper;

    invoke-interface {v7, p1}, Lcom/sec/android/sCloudSync/ServiceManagers/IPDMServiceApiHelper;->addHeaderForApi(Lorg/apache/http/client/methods/HttpUriRequest;)V

    .line 322
    const-string v7, "NetworkClient"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "RequestforBinaryFile : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface {p1}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v5

    .line 325
    .local v5, "requestID":Ljava/lang/String;
    new-instance v3, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;

    invoke-direct {p0}, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->getHttpClient()Lorg/apache/http/client/HttpClient;

    move-result-object v7

    invoke-direct {v3, p1, v7, p0, v5}, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;-><init>(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/HttpClient;Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientResponseListener;Ljava/lang/String;)V

    .line 328
    .local v3, "httpClientThread":Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;
    const/4 v4, 0x0

    .line 330
    .local v4, "httpResponse":Lorg/apache/http/HttpResponse;
    :try_start_0
    iget-object v7, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->mThreadMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v7, v5, v3}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 331
    invoke-virtual {v3}, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->start()V

    .line 333
    invoke-virtual {v3}, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->getThreadLock()Ljava/lang/Object;

    move-result-object v6

    .line 334
    .local v6, "threadLock":Ljava/lang/Object;
    monitor-enter v6
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 335
    const/4 v7, 0x0

    :try_start_1
    iput-boolean v7, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->bNotified:Z

    .line 336
    :goto_0
    iget-boolean v7, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->bNotified:Z

    if-nez v7, :cond_0

    .line 337
    invoke-virtual {v6}, Ljava/lang/Object;->wait()V

    goto :goto_0

    .line 339
    :catchall_0
    move-exception v7

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v7
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    .line 361
    .end local v6    # "threadLock":Ljava/lang/Object;
    :catch_0
    move-exception v2

    .line 362
    .local v2, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 363
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->close()V

    move-object v1, v8

    .line 379
    .end local v2    # "e":Ljava/lang/InterruptedException;
    :goto_1
    return-object v1

    .line 339
    .restart local v6    # "threadLock":Ljava/lang/Object;
    :cond_0
    :try_start_3
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 341
    :try_start_4
    iget-object v7, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->mThreadMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v7, v5}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    move-object v0, v7

    check-cast v0, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;

    move-object v3, v0

    .line 342
    if-nez v3, :cond_3

    .line 343
    iget-object v9, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->syncLock:Ljava/lang/Object;

    monitor-enter v9
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0

    .line 344
    :try_start_5
    iget-object v7, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->mThreadIOExceptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    if-eqz v7, :cond_2

    if-eqz v5, :cond_2

    iget-object v7, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->mThreadIOExceptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v7, v5}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-ne v11, v7, :cond_2

    .line 345
    new-instance v1, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;

    invoke-direct {v1}, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;-><init>()V

    .line 346
    .local v1, "IOExceptionResponse":Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;
    const/4 v7, 0x3

    invoke-virtual {v1, v7}, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;->setStatus(I)V

    .line 347
    iget-object v7, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->mThreadIOExceptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v7, v5}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 348
    iget-object v7, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->mThreadIOExceptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v7}, Ljava/util/concurrent/ConcurrentHashMap;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 349
    const/4 v7, 0x0

    iput-object v7, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->mThreadIOExceptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 350
    :cond_1
    monitor-exit v9

    goto :goto_1

    .line 355
    .end local v1    # "IOExceptionResponse":Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;
    :catchall_1
    move-exception v7

    monitor-exit v9
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    throw v7
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_0

    .line 353
    :cond_2
    :try_start_7
    monitor-exit v9
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    move-object v1, v8

    goto :goto_1

    .line 358
    :cond_3
    :try_start_8
    invoke-virtual {v3}, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->getHttpResponse()Lorg/apache/http/HttpResponse;

    move-result-object v4

    .line 359
    iget-object v7, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->mThreadMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v7, v5}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_8
    .catch Ljava/lang/InterruptedException; {:try_start_8 .. :try_end_8} :catch_0

    .line 367
    if-nez v4, :cond_4

    .line 369
    new-instance v7, Ljava/io/IOException;

    const-string v8, "Response is NULL."

    invoke-direct {v7, v8}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 372
    :cond_4
    const-string v7, "NetworkClient"

    const-string v9, "RequestforBinaryFile : DONE"

    invoke-static {v7, v9}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    iget-object v7, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->mNetworkResponse:Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;

    invoke-direct {p0, v4, v7}, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->handleResponse(Lorg/apache/http/HttpResponse;Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;)V

    .line 376
    invoke-virtual {v3}, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->isClosed()Z

    move-result v7

    if-eqz v7, :cond_5

    move-object v1, v8

    .line 377
    goto :goto_1

    .line 379
    :cond_5
    iget-object v1, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->mNetworkResponse:Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;

    goto :goto_1
.end method

.method private toHex(I)C
    .locals 1
    .param p1, "ch"    # I

    .prologue
    .line 428
    const/16 v0, 0xa

    if-ge p1, v0, :cond_0

    add-int/lit8 v0, p1, 0x30

    :goto_0
    int-to-char v0, v0

    return v0

    :cond_0
    add-int/lit8 v0, p1, 0x41

    add-int/lit8 v0, v0, -0xa

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 6

    .prologue
    .line 123
    iget-object v5, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->mThreadMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v5}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 124
    .local v0, "HttpClientThreadRequests":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 126
    .local v3, "requestID":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->mThreadMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v5, v3}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;

    .line 127
    .local v1, "httpClientThread":Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;
    if-eqz v1, :cond_0

    .line 129
    invoke-virtual {v1}, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->close()V

    .line 130
    invoke-virtual {v1}, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->isAlive()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 131
    invoke-virtual {v1}, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->interrupt()V

    .line 133
    :cond_1
    iget-object v5, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->mThreadMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v5, v3}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    invoke-virtual {v1}, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->getThreadLock()Ljava/lang/Object;

    move-result-object v4

    .line 135
    .local v4, "threadLock":Ljava/lang/Object;
    monitor-enter v4

    .line 137
    const/4 v5, 0x1

    :try_start_0
    iput-boolean v5, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->bNotified:Z

    .line 138
    invoke-virtual {v4}, Ljava/lang/Object;->notify()V

    .line 139
    monitor-exit v4

    goto :goto_0

    :catchall_0
    move-exception v5

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    .line 143
    .end local v1    # "httpClientThread":Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;
    .end local v3    # "requestID":Ljava/lang/String;
    .end local v4    # "threadLock":Ljava/lang/Object;
    :cond_2
    return-void
.end method

.method public encodeURL(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 403
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 404
    .local v2, "encodedUrl":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    .local v0, "arr$":[C
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-char v1, v0, v3

    .line 406
    .local v1, "ch":C
    invoke-direct {p0, v1}, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->isUnsafe(C)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 407
    const/16 v5, 0x25

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 408
    div-int/lit8 v5, v1, 0x10

    invoke-direct {p0, v5}, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->toHex(I)C

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 409
    rem-int/lit8 v5, v1, 0x10

    invoke-direct {p0, v5}, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->toHex(I)C

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 404
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 411
    :cond_0
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 414
    .end local v1    # "ch":C
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method

.method public get(Ljava/lang/String;)Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;
    .locals 2
    .param p1, "Url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 186
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {p0, p1}, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->encodeURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 187
    .local v0, "get":Lorg/apache/http/client/methods/HttpGet;
    invoke-direct {p0, v0}, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;

    move-result-object v1

    return-object v1
.end method

.method public getFile(Ljava/lang/String;)Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;
    .locals 2
    .param p1, "Url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 313
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {p0, p1}, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->encodeURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 314
    .local v0, "get":Lorg/apache/http/client/methods/HttpGet;
    invoke-direct {p0, v0}, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->requestFile(Lorg/apache/http/client/methods/HttpUriRequest;)Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;

    move-result-object v1

    return-object v1
.end method

.method public getWithHeader(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;
    .locals 2
    .param p1, "Url"    # Ljava/lang/String;
    .param p2, "headerName"    # Ljava/lang/String;
    .param p3, "headerValue"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 385
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {p0, p1}, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->encodeURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 387
    .local v0, "get":Lorg/apache/http/client/methods/HttpGet;
    iget-object v1, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->mServiceApiHelper:Lcom/sec/android/sCloudSync/ServiceManagers/IPDMServiceApiHelper;

    invoke-interface {v1, v0}, Lcom/sec/android/sCloudSync/ServiceManagers/IPDMServiceApiHelper;->addHeaderForApi(Lorg/apache/http/client/methods/HttpUriRequest;)V

    .line 388
    invoke-virtual {v0, p2, p3}, Lorg/apache/http/client/methods/HttpGet;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    invoke-direct {p0, v0}, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;

    move-result-object v1

    return-object v1
.end method

.method public onResponse(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v6, 0x3

    .line 80
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;

    .line 82
    .local v1, "httpClientThread":Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;
    const-string v3, "NetworkClient"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "handleMessage - reason = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    iget v3, p1, Landroid/os/Message;->arg1:I

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    iget v3, p1, Landroid/os/Message;->arg1:I

    if-ne v3, v6, :cond_1

    .line 86
    :cond_0
    iget-object v3, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->mThreadMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->getRequestID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    :cond_1
    iget v3, p1, Landroid/os/Message;->arg1:I

    if-ne v3, v6, :cond_4

    .line 91
    iget-object v4, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->syncLock:Ljava/lang/Object;

    monitor-enter v4

    .line 92
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->mThreadIOExceptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    if-nez v3, :cond_2

    .line 93
    new-instance v3, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v3}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v3, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->mThreadIOExceptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 95
    :cond_2
    iget-object v3, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->mThreadIOExceptionMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->getRequestID()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x3

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 98
    iget-object v3, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->mHttpClient:Lorg/apache/http/client/HttpClient;

    if-eqz v3, :cond_4

    .line 100
    iget-object v3, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->mHttpClient:Lorg/apache/http/client/HttpClient;

    invoke-interface {v3}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v0

    .line 101
    .local v0, "ccm":Lorg/apache/http/conn/ClientConnectionManager;
    if-eqz v0, :cond_3

    .line 102
    invoke-interface {v0}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 104
    :cond_3
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->mHttpClient:Lorg/apache/http/client/HttpClient;

    .line 108
    .end local v0    # "ccm":Lorg/apache/http/conn/ClientConnectionManager;
    :cond_4
    invoke-virtual {v1}, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->getThreadLock()Ljava/lang/Object;

    move-result-object v2

    .line 109
    .local v2, "threadLock":Ljava/lang/Object;
    monitor-enter v2

    .line 111
    const/4 v3, 0x1

    :try_start_1
    iput-boolean v3, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->bNotified:Z

    .line 112
    invoke-virtual {v2}, Ljava/lang/Object;->notify()V

    .line 113
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 114
    return-void

    .line 96
    .end local v2    # "threadLock":Ljava/lang/Object;
    :catchall_0
    move-exception v3

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3

    .line 113
    .restart local v2    # "threadLock":Ljava/lang/Object;
    :catchall_1
    move-exception v3

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v3
.end method

.method public post(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;
    .locals 5
    .param p1, "Url"    # Ljava/lang/String;
    .param p2, "json"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 193
    new-instance v1, Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {p0, p1}, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->encodeURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 195
    .local v1, "post":Lorg/apache/http/client/methods/HttpPost;
    if-eqz p2, :cond_0

    .line 196
    new-instance v0, Lorg/apache/http/entity/StringEntity;

    const-string v2, "UTF-8"

    invoke-direct {v0, p2, v2}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    .local v0, "entity":Lorg/apache/http/entity/StringEntity;
    new-instance v2, Lorg/apache/http/message/BasicHeader;

    const-string v3, "Content-Type"

    const-string v4, "Application/JSON;charset=UTF-8"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lorg/apache/http/entity/StringEntity;->setContentType(Lorg/apache/http/Header;)V

    .line 198
    invoke-virtual {v1, v0}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 201
    .end local v0    # "entity":Lorg/apache/http/entity/StringEntity;
    :cond_0
    invoke-direct {p0, v1}, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;

    move-result-object v2

    return-object v2
.end method

.method public postMultiPart(Ljava/lang/String;Lorg/apache/http/entity/mime/MultipartEntity;)Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;
    .locals 2
    .param p1, "Url"    # Ljava/lang/String;
    .param p2, "requestEntity"    # Lorg/apache/http/entity/mime/MultipartEntity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 395
    new-instance v0, Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {p0, p1}, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->encodeURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 397
    .local v0, "post":Lorg/apache/http/client/methods/HttpPost;
    invoke-virtual {v0, p2}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 398
    invoke-direct {p0, v0}, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;

    move-result-object v1

    return-object v1
.end method

.method public setServiceApiHelper(Lcom/sec/android/sCloudSync/ServiceManagers/IPDMServiceApiHelper;)V
    .locals 0
    .param p1, "networkHelper"    # Lcom/sec/android/sCloudSync/ServiceManagers/IPDMServiceApiHelper;

    .prologue
    .line 180
    iput-object p1, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->mServiceApiHelper:Lcom/sec/android/sCloudSync/ServiceManagers/IPDMServiceApiHelper;

    .line 181
    return-void
.end method
