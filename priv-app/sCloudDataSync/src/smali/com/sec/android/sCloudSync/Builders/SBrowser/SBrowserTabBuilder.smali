.class public Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserTabBuilder;
.super Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;
.source "SBrowserTabBuilder.java"


# static fields
.field private static final SBROWSERTAB_COLUMNS:[Ljava/lang/String;

.field private static SBROWSERTAB_URI:Landroid/net/Uri; = null

.field private static final TAG:Ljava/lang/String; = "SBrowserTabBuilder"


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 23
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "TAB_ID"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "TAB_INDEX"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "TAB_URL"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "TAB_TITLE"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "TAB_FAV_ICON"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "TAB_ACTIVATE"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "IS_INCOGNITO"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "DATE_CREATED"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "DATE_MODIFIED"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "ACCOUNT_NAME"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "ACCOUNT_TYPE"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "DEVICE_NAME"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "DEVICE_ID"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "TAB_USAGE"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserTabBuilder;->SBROWSERTAB_COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentProviderClient;Landroid/accounts/Account;Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p1, "provider"    # Landroid/content/ContentProviderClient;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "authority"    # Ljava/lang/String;

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;-><init>(Landroid/content/ContentProviderClient;Landroid/accounts/Account;Landroid/content/Context;)V

    .line 42
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "content://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/tabs"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserTabBuilder;->SBROWSERTAB_URI:Landroid/net/Uri;

    .line 44
    return-void
.end method


# virtual methods
.method public doApplyBatch()Z
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    return v0
.end method

.method public insert(Ljava/lang/String;Ljava/lang/String;J)Z
    .locals 9
    .param p1, "jsonString"    # Ljava/lang/String;
    .param p2, "syncKey"    # Ljava/lang/String;
    .param p3, "timeStamp"    # J

    .prologue
    const/4 v8, 0x0

    const/4 v4, 0x0

    .line 104
    const/4 v3, 0x0

    .line 105
    .local v3, "sBrowserTabValues":Landroid/content/ContentValues;
    iget-object v5, p0, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserTabBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    sget-object v6, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserTabBuilder;->SBROWSERTAB_COLUMNS:[Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 108
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserTabBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v5, p1}, Lcom/sec/android/sCloudSync/Records/JSONParser;->fromJSONString(Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v3

    .line 109
    const-string v5, "TAB_FAV_ICON"

    invoke-virtual {v3, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 110
    .local v2, "favIcon":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 113
    const/4 v5, 0x0

    invoke-static {v2, v5}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    .line 114
    .local v0, "buffer":[B
    const-string v5, "TAB_FAV_ICON"

    invoke-virtual {v3, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 116
    .end local v0    # "buffer":[B
    :cond_0
    const-string v5, "SYNC1"

    invoke-virtual {v3, v5, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    const-string v5, "SYNC2"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 123
    iget-object v5, p0, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserTabBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v5, v8}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 127
    :try_start_1
    iget-object v5, p0, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserTabBuilder;->mProvider:Landroid/content/ContentProviderClient;

    sget-object v6, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserTabBuilder;->SBROWSERTAB_URI:Landroid/net/Uri;

    invoke-virtual {v5, v6, v3}, Landroid/content/ContentProviderClient;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 132
    const/4 v4, 0x1

    .end local v2    # "favIcon":Ljava/lang/String;
    :goto_0
    return v4

    .line 119
    :catch_0
    move-exception v1

    .line 120
    .local v1, "e":Lorg/json/JSONException;
    :try_start_2
    const-string v5, "SBrowserTabBuilder"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unable to Parse;"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 123
    iget-object v5, p0, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserTabBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v5, v8}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    goto :goto_0

    .end local v1    # "e":Lorg/json/JSONException;
    :catchall_0
    move-exception v4

    iget-object v5, p0, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserTabBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v5, v8}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    throw v4

    .line 128
    .restart local v2    # "favIcon":Ljava/lang/String;
    :catch_1
    move-exception v1

    .line 129
    .local v1, "e":Landroid/os/RemoteException;
    const-string v5, "SBrowserTabBuilder"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "RemoteException"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public parse(Landroid/database/Cursor;JLjava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "rowId"    # J
    .param p4, "key"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 53
    const/4 v1, 0x0

    .line 54
    .local v1, "sBrowserTabString":Ljava/lang/String;
    if-nez p1, :cond_0

    .line 65
    :goto_0
    return-object v2

    .line 56
    :cond_0
    iget-object v3, p0, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserTabBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    sget-object v4, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserTabBuilder;->SBROWSERTAB_COLUMNS:[Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 59
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserTabBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v3, p1}, Lcom/sec/android/sCloudSync/Records/JSONParser;->toJSON(Landroid/database/Cursor;)Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 63
    iget-object v3, p0, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserTabBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v3, v2}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    :goto_1
    move-object v2, v1

    .line 65
    goto :goto_0

    .line 60
    :catch_0
    move-exception v0

    .line 61
    .local v0, "e":Lorg/json/JSONException;
    :try_start_1
    const-string v3, "SBrowserTabBuilder"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "parse():Exception in parsing"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 63
    iget-object v3, p0, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserTabBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v3, v2}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    goto :goto_1

    .end local v0    # "e":Lorg/json/JSONException;
    :catchall_0
    move-exception v3

    iget-object v4, p0, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserTabBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v4, v2}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    throw v3
.end method

.method public update(Ljava/lang/String;JJLjava/lang/String;)Z
    .locals 10
    .param p1, "jsonString"    # Ljava/lang/String;
    .param p2, "timeStamp"    # J
    .param p4, "rowId"    # J
    .param p6, "Key"    # Ljava/lang/String;

    .prologue
    .line 72
    const/4 v3, 0x0

    .line 73
    .local v3, "sBrowserTabValues":Landroid/content/ContentValues;
    iget-object v4, p0, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserTabBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    sget-object v5, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserTabBuilder;->SBROWSERTAB_COLUMNS:[Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 76
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserTabBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v4, p1}, Lcom/sec/android/sCloudSync/Records/JSONParser;->fromJSONString(Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v3

    .line 77
    const-string v4, "TAB_FAV_ICON"

    invoke-virtual {v3, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 78
    .local v2, "favIcon":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 81
    const/4 v4, 0x0

    invoke-static {v2, v4}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    .line 82
    .local v0, "buffer":[B
    const-string v4, "TAB_FAV_ICON"

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 84
    .end local v0    # "buffer":[B
    :cond_0
    const-string v4, "SYNC2"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 85
    const-string v4, "DIRTY"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 86
    const-string v4, "IS_DELETED"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 93
    :try_start_1
    iget-object v4, p0, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserTabBuilder;->mProvider:Landroid/content/ContentProviderClient;

    sget-object v5, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserTabBuilder;->SBROWSERTAB_URI:Landroid/net/Uri;

    const-string v6, "_ID = ? "

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    invoke-static {p4, p5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentProviderClient;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 99
    const/4 v4, 0x1

    .end local v2    # "favIcon":Ljava/lang/String;
    :goto_0
    return v4

    .line 87
    :catch_0
    move-exception v1

    .line 88
    .local v1, "e":Lorg/json/JSONException;
    const-string v4, "SBrowserTabBuilder"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unable to Parse;"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    const/4 v4, 0x0

    goto :goto_0

    .line 95
    .end local v1    # "e":Lorg/json/JSONException;
    .restart local v2    # "favIcon":Ljava/lang/String;
    :catch_1
    move-exception v1

    .line 96
    .local v1, "e":Landroid/os/RemoteException;
    const-string v4, "SBrowserTabBuilder"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "RemoteException"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    const/4 v4, 0x0

    goto :goto_0
.end method
