.class Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder$2;
.super Ljava/lang/Object;
.source "SMemoBuilder.java"

# interfaces
.implements Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder$PostOperationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->updateExtData(Lorg/json/JSONObject;JLjava/util/ArrayList;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;

.field final synthetic val$rowId:J


# direct methods
.method constructor <init>(Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;J)V
    .locals 0

    .prologue
    .line 440
    iput-object p1, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder$2;->this$0:Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;

    iput-wide p2, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder$2;->val$rowId:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public doPost(Landroid/content/ContentValues;)V
    .locals 3
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 444
    const-string v0, "MemoID"

    iget-wide v1, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder$2;->val$rowId:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 445
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder$2;->this$0:Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;

    # getter for: Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mOperations:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->access$000(Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;)Ljava/util/ArrayList;

    move-result-object v0

    sget-object v1, Lcom/sec/android/sCloudSync/Constants/SMemoContract;->SMEMO_EXTDATA_URI:Landroid/net/Uri;

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 447
    return-void
.end method
