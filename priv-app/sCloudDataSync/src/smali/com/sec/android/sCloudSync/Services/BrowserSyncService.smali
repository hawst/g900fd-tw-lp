.class public Lcom/sec/android/sCloudSync/Services/BrowserSyncService;
.super Landroid/app/Service;
.source "BrowserSyncService.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "BROWSER-SYNC"


# instance fields
.field private final mBinder:Lcom/sec/android/sCloudSync/IDataSync/IDataSync$Stub;

.field private sync:Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Services/BrowserSyncService;->sync:Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;

    .line 53
    new-instance v0, Lcom/sec/android/sCloudSync/Services/BrowserSyncService$1;

    invoke-direct {v0, p0}, Lcom/sec/android/sCloudSync/Services/BrowserSyncService$1;-><init>(Lcom/sec/android/sCloudSync/Services/BrowserSyncService;)V

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Services/BrowserSyncService;->mBinder:Lcom/sec/android/sCloudSync/IDataSync/IDataSync$Stub;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/sCloudSync/Services/BrowserSyncService;)Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/sCloudSync/Services/BrowserSyncService;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Services/BrowserSyncService;->sync:Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Services/BrowserSyncService;->mBinder:Lcom/sec/android/sCloudSync/IDataSync/IDataSync$Stub;

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 37
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 38
    new-instance v0, Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/Services/BrowserSyncService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Services/BrowserSyncService;->sync:Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;

    .line 39
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 50
    const-string v0, "BROWSER-SYNC"

    const-string v1, "SYNC SERVICE END"

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    return-void
.end method
