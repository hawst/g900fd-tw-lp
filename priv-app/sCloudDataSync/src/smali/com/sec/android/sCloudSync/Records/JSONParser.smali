.class public Lcom/sec/android/sCloudSync/Records/JSONParser;
.super Ljava/lang/Object;
.source "JSONParser.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "JSONParser"


# instance fields
.field private mColumns:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Records/JSONParser;->mColumns:[Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public fromJSON(Lorg/json/JSONObject;)Landroid/content/ContentValues;
    .locals 8
    .param p1, "jsonObject"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 96
    const/4 v6, 0x0

    .line 97
    .local v6, "value":Ljava/lang/String;
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 99
    .local v2, "contentValues":Landroid/content/ContentValues;
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Records/JSONParser;->mColumns:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_1

    aget-object v1, v0, v4

    .line 101
    .local v1, "column":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 103
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 104
    invoke-virtual {v2, v1, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 99
    :cond_0
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 107
    :catch_0
    move-exception v3

    .line 108
    .local v3, "e":Lorg/json/JSONException;
    invoke-virtual {v3}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1

    .line 112
    .end local v1    # "column":Ljava/lang/String;
    .end local v3    # "e":Lorg/json/JSONException;
    :cond_1
    return-object v2
.end method

.method public fromJSONString(Ljava/lang/String;)Landroid/content/ContentValues;
    .locals 9
    .param p1, "jsonString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 74
    const/4 v7, 0x0

    .line 75
    .local v7, "value":Ljava/lang/String;
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 77
    .local v2, "contentValues":Landroid/content/ContentValues;
    new-instance v8, Lorg/json/JSONTokener;

    invoke-direct {v8, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/json/JSONObject;

    .line 79
    .local v5, "jsonObject":Lorg/json/JSONObject;
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Records/JSONParser;->mColumns:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v6, :cond_1

    aget-object v1, v0, v4

    .line 81
    .local v1, "column":Ljava/lang/String;
    :try_start_0
    invoke-virtual {v5, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 83
    invoke-virtual {v5, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 84
    invoke-virtual {v2, v1, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 79
    :cond_0
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 87
    :catch_0
    move-exception v3

    .line 88
    .local v3, "e":Lorg/json/JSONException;
    invoke-virtual {v3}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1

    .line 92
    .end local v1    # "column":Ljava/lang/String;
    .end local v3    # "e":Lorg/json/JSONException;
    :cond_1
    return-object v2
.end method

.method public setColumsList([Ljava/lang/String;)V
    .locals 0
    .param p1, "columnList"    # [Ljava/lang/String;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/sec/android/sCloudSync/Records/JSONParser;->mColumns:[Ljava/lang/String;

    .line 37
    return-void
.end method

.method public toJSON(Landroid/database/Cursor;)Lorg/json/JSONObject;
    .locals 11
    .param p1, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 40
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    .line 42
    .local v5, "jsonObj":Lorg/json/JSONObject;
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Records/JSONParser;->mColumns:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v6, :cond_1

    aget-object v1, v0, v3

    .line 43
    .local v1, "column":Ljava/lang/String;
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 44
    .local v2, "columnIndex":I
    const/4 v8, -0x1

    if-ne v2, v8, :cond_0

    .line 42
    :goto_1
    :pswitch_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 45
    :cond_0
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getType(I)I

    move-result v8

    packed-switch v8, :pswitch_data_0

    .line 65
    const-string v8, "JSONParser"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "NO SUCH COLUMN : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 47
    :pswitch_1
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v7

    .line 48
    .local v7, "temp":[B
    const/4 v8, 0x0

    invoke-static {v7, v8}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v4

    .line 49
    .local v4, "imageBase64":Ljava/lang/String;
    invoke-virtual {v5, v1, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_1

    .line 55
    .end local v4    # "imageBase64":Ljava/lang/String;
    .end local v7    # "temp":[B
    :pswitch_2
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v1, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_1

    .line 62
    :pswitch_3
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v1, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_1

    .line 69
    .end local v1    # "column":Ljava/lang/String;
    .end local v2    # "columnIndex":I
    :cond_1
    return-object v5

    .line 45
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method
