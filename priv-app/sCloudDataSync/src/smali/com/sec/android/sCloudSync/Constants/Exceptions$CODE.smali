.class public final Lcom/sec/android/sCloudSync/Constants/Exceptions$CODE;
.super Ljava/lang/Object;
.source "Exceptions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/sCloudSync/Constants/Exceptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CODE"
.end annotation


# static fields
.field public static final AUTHENTICATION:I = 0x3

.field public static final DATABASE_ERROR:I = 0xf

.field public static final HTTP:I = 0x1

.field public static final IO_ERROR:I = 0x4

.field public static final JSON:I = 0x2

.field public static final LOCAL_PROVIDER:I = 0xb

.field public static final NO_UNIQUEKEY:I = 0xa

.field public static final OTHER:I = 0x6

.field public static final OUTOFMEMORY:I = 0xc

.field public static final REMOTEEXCEPTION:I = 0xd

.field public static final SDCARD_NOT_MOUNTED:I = 0x8

.field public static final SERVER_AUTHENTICATION:I = 0x7

.field public static final STORAGE_FULL:I = 0x5

.field public static final SYNC_CANCELLED:I = 0x9


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
