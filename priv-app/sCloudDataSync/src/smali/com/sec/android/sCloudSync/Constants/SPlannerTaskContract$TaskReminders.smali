.class public interface abstract Lcom/sec/android/sCloudSync/Constants/SPlannerTaskContract$TaskReminders;
.super Ljava/lang/Object;
.source "SPlannerTaskContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/sCloudSync/Constants/SPlannerTaskContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "TaskReminders"
.end annotation


# static fields
.field public static final ACCOUNT_KEY:Ljava/lang/String; = "accountkey"

.field public static final DUE_DATE:Ljava/lang/String; = "due_date"

.field public static final REMINDER_TIME:Ljava/lang/String; = "reminder_time"

.field public static final REMINDER_TYPE:Ljava/lang/String; = "reminder_type"

.field public static final START_DATE:Ljava/lang/String; = "start_date"

.field public static final STATE:Ljava/lang/String; = "state"

.field public static final SUBJECT:Ljava/lang/String; = "subject"

.field public static final TASK_ID:Ljava/lang/String; = "task_id"
