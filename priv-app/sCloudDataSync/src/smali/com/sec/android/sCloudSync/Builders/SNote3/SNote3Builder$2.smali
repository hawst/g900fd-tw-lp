.class Lcom/sec/android/sCloudSync/Builders/SNote3/SNote3Builder$2;
.super Ljava/lang/Object;
.source "SNote3Builder.java"

# interfaces
.implements Lcom/sec/android/sCloudSync/Builders/SNote3/SNote3Builder$PostOperationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/sCloudSync/Builders/SNote3/SNote3Builder;->updateFileDetailTable(Lorg/json/JSONObject;JLjava/util/ArrayList;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/sCloudSync/Builders/SNote3/SNote3Builder;

.field final synthetic val$rowId:J


# direct methods
.method constructor <init>(Lcom/sec/android/sCloudSync/Builders/SNote3/SNote3Builder;J)V
    .locals 0

    .prologue
    .line 464
    iput-object p1, p0, Lcom/sec/android/sCloudSync/Builders/SNote3/SNote3Builder$2;->this$0:Lcom/sec/android/sCloudSync/Builders/SNote3/SNote3Builder;

    iput-wide p2, p0, Lcom/sec/android/sCloudSync/Builders/SNote3/SNote3Builder$2;->val$rowId:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public doPost(Landroid/content/ContentValues;)V
    .locals 4
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 467
    const-string v1, "spd_id"

    iget-wide v2, p0, Lcom/sec/android/sCloudSync/Builders/SNote3/SNote3Builder$2;->val$rowId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 468
    sget-object v1, Lcom/sec/android/sCloudSync/Constants/SNote3Contract;->FILEDETAIL_CONTENT_URI:Landroid/net/Uri;

    const-string v2, "caller_is_syncadapter"

    invoke-static {v1, v2}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 470
    .local v0, "fileDetailUri":Landroid/net/Uri;
    iget-object v1, p0, Lcom/sec/android/sCloudSync/Builders/SNote3/SNote3Builder$2;->this$0:Lcom/sec/android/sCloudSync/Builders/SNote3/SNote3Builder;

    # getter for: Lcom/sec/android/sCloudSync/Builders/SNote3/SNote3Builder;->mOperations:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/sCloudSync/Builders/SNote3/SNote3Builder;->access$000(Lcom/sec/android/sCloudSync/Builders/SNote3/SNote3Builder;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 472
    return-void
.end method
