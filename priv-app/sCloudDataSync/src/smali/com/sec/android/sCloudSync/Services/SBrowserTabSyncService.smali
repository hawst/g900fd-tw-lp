.class public Lcom/sec/android/sCloudSync/Services/SBrowserTabSyncService;
.super Landroid/app/Service;
.source "SBrowserTabSyncService.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SBROWSERTAB-SYNC"


# instance fields
.field private final mBinder:Lcom/sec/android/sCloudSync/IDataSync/IDataSync$Stub;

.field private sync:Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserTabSyncAdapter;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Services/SBrowserTabSyncService;->sync:Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserTabSyncAdapter;

    .line 52
    new-instance v0, Lcom/sec/android/sCloudSync/Services/SBrowserTabSyncService$1;

    invoke-direct {v0, p0}, Lcom/sec/android/sCloudSync/Services/SBrowserTabSyncService$1;-><init>(Lcom/sec/android/sCloudSync/Services/SBrowserTabSyncService;)V

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Services/SBrowserTabSyncService;->mBinder:Lcom/sec/android/sCloudSync/IDataSync/IDataSync$Stub;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/sCloudSync/Services/SBrowserTabSyncService;)Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserTabSyncAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/sCloudSync/Services/SBrowserTabSyncService;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Services/SBrowserTabSyncService;->sync:Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserTabSyncAdapter;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Services/SBrowserTabSyncService;->mBinder:Lcom/sec/android/sCloudSync/IDataSync/IDataSync$Stub;

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 36
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 37
    new-instance v0, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserTabSyncAdapter;

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/Services/SBrowserTabSyncService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserTabSyncAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Services/SBrowserTabSyncService;->sync:Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserTabSyncAdapter;

    .line 38
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 49
    const-string v0, "SBROWSERTAB-SYNC"

    const-string v1, "SYNC SERVICE END"

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    return-void
.end method
