.class public final Lcom/sec/android/sCloudSync/Records/KVSResponse;
.super Lcom/sec/android/sCloudSync/Records/AbstractJSON;
.source "KVSResponse.java"


# static fields
.field private static final AVAILABLE:Ljava/lang/String; = "available"

.field private static final DELETED:Ljava/lang/String; = "deleted"

.field private static final KEY:Ljava/lang/String; = "key"

.field private static final LEVEL:Ljava/lang/String; = "level"

.field private static final LIST:Ljava/lang/String; = "list"

.field private static final MAX_TIMESTAMP:Ljava/lang/String; = "maxTimestamp"

.field private static final NEXTKEY:Ljava/lang/String; = "nextKey"

.field private static final QUOTA:Ljava/lang/String; = "quota"

.field private static final RCODE:Ljava/lang/String; = "rcode"

.field private static final RMSG:Ljava/lang/String; = "rmsg"

.field private static final SERVER_TIMESTAMP:Ljava/lang/String; = "serverTimestamp"

.field private static final SIZE:Ljava/lang/String; = "size"

.field private static final STORAGEINFO:Ljava/lang/String; = "storageinfo"

.field private static final TAG:Ljava/lang/String; = "KVSResponse"

.field private static final TIMESTAMP:Ljava/lang/String; = "timestamp"

.field private static final USAGE:Ljava/lang/String; = "usage"

.field private static final VALUE:Ljava/lang/String; = "value"


# instance fields
.field private mContext:Landroid/content/Context;

.field private final mItemDetailsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/sCloudSync/Records/RecordItem;",
            ">;"
        }
    .end annotation
.end field

.field private final mItemResponseList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/sCloudSync/Records/RecordItemResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final mKeyDetailList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/sCloudSync/Records/RecordKey;",
            ">;"
        }
    .end annotation
.end field

.field private mMaxTimeStamp:Ljava/lang/String;

.field private mNextKey:Ljava/lang/String;

.field private mResponseCode:I

.field private mResultMessage:Ljava/lang/String;

.field private mServerTimeStamp:Ljava/lang/Long;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 34
    invoke-direct {p0}, Lcom/sec/android/sCloudSync/Records/AbstractJSON;-><init>()V

    .line 63
    iput-object v0, p0, Lcom/sec/android/sCloudSync/Records/KVSResponse;->mMaxTimeStamp:Ljava/lang/String;

    .line 64
    iput-object v0, p0, Lcom/sec/android/sCloudSync/Records/KVSResponse;->mNextKey:Ljava/lang/String;

    .line 66
    iput-object v0, p0, Lcom/sec/android/sCloudSync/Records/KVSResponse;->mResultMessage:Ljava/lang/String;

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Records/KVSResponse;->mItemResponseList:Ljava/util/List;

    .line 69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Records/KVSResponse;->mItemDetailsList:Ljava/util/List;

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Records/KVSResponse;->mKeyDetailList:Ljava/util/List;

    .line 35
    iput-object p1, p0, Lcom/sec/android/sCloudSync/Records/KVSResponse;->mContext:Landroid/content/Context;

    .line 36
    return-void
.end method

.method private notifyQuota(Lorg/json/JSONObject;)V
    .locals 4
    .param p1, "storageJson"    # Lorg/json/JSONObject;

    .prologue
    .line 188
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.sCloudQuotaApp.QUOTA_WARNING"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 189
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "KVSResponse"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Quota warning recieved. Available= :-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "available"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    const-string v1, "level"

    const-string v2, "level"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 191
    const-string v1, "quota"

    const-string v2, "quota"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 192
    const-string v1, "usage"

    const-string v2, "usage"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 193
    const-string v1, "available"

    const-string v2, "available"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 194
    iget-object v1, p0, Lcom/sec/android/sCloudSync/Records/KVSResponse;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 195
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 77
    iput-object v0, p0, Lcom/sec/android/sCloudSync/Records/KVSResponse;->mMaxTimeStamp:Ljava/lang/String;

    .line 78
    iput-object v0, p0, Lcom/sec/android/sCloudSync/Records/KVSResponse;->mNextKey:Ljava/lang/String;

    .line 79
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/sCloudSync/Records/KVSResponse;->mResponseCode:I

    .line 81
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Records/KVSResponse;->mServerTimeStamp:Ljava/lang/Long;

    .line 82
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Records/KVSResponse;->mItemResponseList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 83
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Records/KVSResponse;->mItemDetailsList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 84
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Records/KVSResponse;->mKeyDetailList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 85
    return-void
.end method

.method public fromJSON(Ljava/lang/Object;)V
    .locals 16
    .param p1, "json"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 129
    new-instance v9, Lorg/json/JSONObject;

    check-cast p1, Ljava/lang/String;

    .end local p1    # "json":Ljava/lang/Object;
    move-object/from16 v0, p1

    invoke-direct {v9, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 130
    .local v9, "jsonObj":Lorg/json/JSONObject;
    const-string v1, "rcode"

    invoke-virtual {v9, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 131
    const-string v1, "rcode"

    invoke-virtual {v9, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/sCloudSync/Records/KVSResponse;->mResponseCode:I

    .line 132
    :cond_0
    const-string v1, "rmsg"

    invoke-virtual {v9, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 133
    const-string v1, "rmsg"

    invoke-virtual {v9, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/sCloudSync/Records/KVSResponse;->mResultMessage:Ljava/lang/String;

    .line 136
    :cond_1
    const-string v1, "maxTimestamp"

    invoke-virtual {v9, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 137
    const-string v1, "maxTimestamp"

    invoke-virtual {v9, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/sCloudSync/Records/KVSResponse;->mMaxTimeStamp:Ljava/lang/String;

    .line 138
    :cond_2
    const-string v1, "nextKey"

    invoke-virtual {v9, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 139
    const-string v1, "nextKey"

    invoke-virtual {v9, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/sCloudSync/Records/KVSResponse;->mNextKey:Ljava/lang/String;

    .line 140
    :cond_3
    const-string v1, "serverTimestamp"

    invoke-virtual {v9, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 141
    const-string v1, "serverTimestamp"

    invoke-virtual {v9, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/sCloudSync/Records/KVSResponse;->mServerTimeStamp:Ljava/lang/Long;

    .line 142
    :cond_4
    const-string v1, "storageinfo"

    invoke-virtual {v9, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 143
    const-string v1, "storageinfo"

    invoke-virtual {v9, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v12

    .line 144
    .local v12, "storageJson":Lorg/json/JSONObject;
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/sec/android/sCloudSync/Records/KVSResponse;->notifyQuota(Lorg/json/JSONObject;)V

    .line 146
    .end local v12    # "storageJson":Lorg/json/JSONObject;
    :cond_5
    const-string v1, "list"

    invoke-virtual {v9, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 147
    const-string v1, "list"

    invoke-virtual {v9, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v7

    .line 149
    .local v7, "dataitemarray":Lorg/json/JSONArray;
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-ge v8, v1, :cond_9

    .line 150
    invoke-virtual {v7, v8}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v10

    .line 151
    .local v10, "obj":Lorg/json/JSONObject;
    const-string v1, "timestamp"

    invoke-virtual {v10, v1}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 152
    .local v3, "timestamp":Ljava/lang/Long;
    const-string v1, "key"

    invoke-virtual {v10, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 153
    .local v2, "key":Ljava/lang/String;
    const/4 v11, 0x0

    .line 154
    .local v11, "retcode":I
    const/4 v13, 0x0

    .line 155
    .local v13, "value":Ljava/lang/String;
    const/4 v6, 0x0

    .line 156
    .local v6, "deleted":Z
    const-wide/16 v4, 0x0

    .line 158
    .local v4, "size":J
    const-string v1, "rcode"

    invoke-virtual {v10, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 159
    const-string v1, "rcode"

    invoke-virtual {v10, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v11

    .line 160
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sCloudSync/Records/KVSResponse;->mItemResponseList:Ljava/util/List;

    new-instance v14, Lcom/sec/android/sCloudSync/Records/RecordItemResponse;

    invoke-direct {v14, v2, v3, v11}, Lcom/sec/android/sCloudSync/Records/RecordItemResponse;-><init>(Ljava/lang/String;Ljava/lang/Long;I)V

    invoke-interface {v1, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 163
    :cond_6
    const-string v1, "value"

    invoke-virtual {v10, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 164
    const-string v1, "deleted"

    invoke-virtual {v10, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v6

    .line 165
    const-string v1, "value"

    invoke-virtual {v10, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 166
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sCloudSync/Records/KVSResponse;->mItemDetailsList:Ljava/util/List;

    new-instance v14, Lcom/sec/android/sCloudSync/Records/RecordItem;

    invoke-direct {v14, v2, v3, v13, v6}, Lcom/sec/android/sCloudSync/Records/RecordItem;-><init>(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Z)V

    invoke-interface {v1, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 169
    :cond_7
    const-string v1, "size"

    invoke-virtual {v10, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 170
    const-string v1, "deleted"

    invoke-virtual {v10, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v6

    .line 171
    const-string v1, "size"

    invoke-virtual {v10, v1}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 172
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/sCloudSync/Records/KVSResponse;->mKeyDetailList:Ljava/util/List;

    new-instance v1, Lcom/sec/android/sCloudSync/Records/RecordKey;

    invoke-direct/range {v1 .. v6}, Lcom/sec/android/sCloudSync/Records/RecordKey;-><init>(Ljava/lang/String;Ljava/lang/Long;JZ)V

    invoke-interface {v14, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 149
    :cond_8
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 185
    .end local v2    # "key":Ljava/lang/String;
    .end local v3    # "timestamp":Ljava/lang/Long;
    .end local v4    # "size":J
    .end local v6    # "deleted":Z
    .end local v7    # "dataitemarray":Lorg/json/JSONArray;
    .end local v8    # "i":I
    .end local v10    # "obj":Lorg/json/JSONObject;
    .end local v11    # "retcode":I
    .end local v13    # "value":Ljava/lang/String;
    :cond_9
    return-void
.end method

.method public getItemDetailsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/sCloudSync/Records/RecordItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Records/KVSResponse;->mItemDetailsList:Ljava/util/List;

    return-object v0
.end method

.method public getItemResponseList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/sCloudSync/Records/RecordItemResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Records/KVSResponse;->mItemResponseList:Ljava/util/List;

    return-object v0
.end method

.method public getKeyDetailList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/sCloudSync/Records/RecordKey;",
            ">;"
        }
    .end annotation

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Records/KVSResponse;->mKeyDetailList:Ljava/util/List;

    return-object v0
.end method

.method public getMaxTimeStamp()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Records/KVSResponse;->mMaxTimeStamp:Ljava/lang/String;

    return-object v0
.end method

.method public getNextKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Records/KVSResponse;->mNextKey:Ljava/lang/String;

    return-object v0
.end method

.method public getResponseCode()I
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lcom/sec/android/sCloudSync/Records/KVSResponse;->mResponseCode:I

    return v0
.end method

.method public getResultMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Records/KVSResponse;->mResultMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getServerTimeStamp()J
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Records/KVSResponse;->mServerTimeStamp:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public toJSON()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 199
    const/4 v0, 0x0

    return-object v0
.end method
