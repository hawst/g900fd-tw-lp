.class public Lcom/sec/android/sCloudSync/Builders/Contact/ContactsGroupBuilder;
.super Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;
.source "ContactsGroupBuilder.java"


# static fields
.field private static final GROUP_COLUMNS:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "ContactGroupBuilder"


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 45
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "account_name"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "account_type"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "group_visible"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "should_sync"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "favorites"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "group_is_read_only"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsGroupBuilder;->GROUP_COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentProviderClient;Landroid/accounts/Account;Landroid/content/Context;)V
    .locals 0
    .param p1, "provider"    # Landroid/content/ContentProviderClient;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 56
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;-><init>(Landroid/content/ContentProviderClient;Landroid/accounts/Account;Landroid/content/Context;)V

    .line 58
    return-void
.end method


# virtual methods
.method public doApplyBatch()Z
    .locals 1

    .prologue
    .line 168
    const/4 v0, 0x0

    return v0
.end method

.method public insert(Ljava/lang/String;Ljava/lang/String;J)Z
    .locals 12
    .param p1, "jsonString"    # Ljava/lang/String;
    .param p2, "syncKey"    # Ljava/lang/String;
    .param p3, "timeStamp"    # J

    .prologue
    .line 111
    const/4 v10, 0x0

    .line 112
    .local v10, "values":Landroid/content/ContentValues;
    sget-object v0, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "caller_is_syncadapter"

    invoke-static {v0, v2}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 113
    .local v1, "uri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsGroupBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    sget-object v2, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsGroupBuilder;->GROUP_COLUMNS:[Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 116
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsGroupBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v0, p1}, Lcom/sec/android/sCloudSync/Records/JSONParser;->fromJSONString(Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v10

    .line 119
    const-string v11, "title = ? AND account_name = ? AND account_type =?"

    .line 122
    .local v11, "where":Ljava/lang/String;
    const/4 v0, 0x3

    new-array v4, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v0, "title"

    invoke-virtual {v10, v0}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v4, v2

    const/4 v2, 0x1

    const-string v0, "account_name"

    invoke-virtual {v10, v0}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v4, v2

    const/4 v2, 0x2

    const-string v0, "account_type"

    invoke-virtual {v10, v0}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v4, v2

    .line 127
    .local v4, "whereargs":[Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsGroupBuilder;->mProvider:Landroid/content/ContentProviderClient;

    const/4 v2, 0x0

    const-string v3, "title = ? AND account_name = ? AND account_type =?"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 130
    .local v9, "grpCursor":Landroid/database/Cursor;
    if-eqz v9, :cond_1

    .line 132
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v8

    .line 133
    .local v8, "grpCount":I
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 134
    const/4 v0, 0x1

    if-ne v8, v0, :cond_0

    .line 136
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 137
    .local v7, "groupValue":Landroid/content/ContentValues;
    const-string v0, "sync1"

    invoke-virtual {v7, v0, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    const-string v0, "sync3"

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v7, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 139
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsGroupBuilder;->mProvider:Landroid/content/ContentProviderClient;

    const-string v2, "title = ? AND account_name = ? AND account_type =?"

    invoke-virtual {v0, v1, v7, v2, v4}, Landroid/content/ContentProviderClient;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 140
    const/4 v0, 0x1

    .line 162
    .end local v4    # "whereargs":[Ljava/lang/String;
    .end local v7    # "groupValue":Landroid/content/ContentValues;
    .end local v8    # "grpCount":I
    .end local v9    # "grpCursor":Landroid/database/Cursor;
    .end local v11    # "where":Ljava/lang/String;
    :goto_0
    return v0

    .line 141
    .restart local v4    # "whereargs":[Ljava/lang/String;
    .restart local v8    # "grpCount":I
    .restart local v9    # "grpCursor":Landroid/database/Cursor;
    .restart local v11    # "where":Ljava/lang/String;
    :cond_0
    const/4 v0, 0x1

    if-le v8, v0, :cond_1

    .line 142
    const/4 v0, 0x0

    goto :goto_0

    .line 145
    .end local v8    # "grpCount":I
    :cond_1
    const-string v0, "sync1"

    invoke-virtual {v10, v0, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    const-string v0, "sync3"

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v10, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    .line 156
    .end local v4    # "whereargs":[Ljava/lang/String;
    .end local v9    # "grpCursor":Landroid/database/Cursor;
    .end local v11    # "where":Ljava/lang/String;
    :goto_1
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsGroupBuilder;->mProvider:Landroid/content/ContentProviderClient;

    invoke-virtual {v0, v1, v10}, Landroid/content/ContentProviderClient;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_2

    .line 162
    const/4 v0, 0x1

    goto :goto_0

    .line 148
    :catch_0
    move-exception v6

    .line 149
    .local v6, "e":Lorg/json/JSONException;
    const-string v0, "ContactGroupBuilder"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to Parse;"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v6}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    const/4 v0, 0x0

    goto :goto_0

    .line 151
    .end local v6    # "e":Lorg/json/JSONException;
    :catch_1
    move-exception v6

    .line 152
    .local v6, "e":Landroid/os/RemoteException;
    invoke-virtual {v6}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 157
    .end local v6    # "e":Landroid/os/RemoteException;
    :catch_2
    move-exception v6

    .line 158
    .restart local v6    # "e":Landroid/os/RemoteException;
    const-string v0, "ContactGroupBuilder"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "RemoteException"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v6}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public parse(Landroid/database/Cursor;JLjava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "rowId"    # J
    .param p4, "key"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 63
    const/4 v1, 0x0

    .line 64
    .local v1, "groupString":Ljava/lang/String;
    if-nez p1, :cond_0

    .line 77
    :goto_0
    return-object v2

    .line 67
    :cond_0
    iget-object v3, p0, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsGroupBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    sget-object v4, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsGroupBuilder;->GROUP_COLUMNS:[Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 70
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsGroupBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v3, p1}, Lcom/sec/android/sCloudSync/Records/JSONParser;->toJSON(Landroid/database/Cursor;)Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 74
    iget-object v3, p0, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsGroupBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v3, v2}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    :goto_1
    move-object v2, v1

    .line 77
    goto :goto_0

    .line 71
    :catch_0
    move-exception v0

    .line 72
    .local v0, "e":Lorg/json/JSONException;
    :try_start_1
    const-string v3, "ContactGroupBuilder"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "parse():Exception in parsing"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 74
    iget-object v3, p0, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsGroupBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v3, v2}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    goto :goto_1

    .end local v0    # "e":Lorg/json/JSONException;
    :catchall_0
    move-exception v3

    iget-object v4, p0, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsGroupBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v4, v2}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    throw v3
.end method

.method public update(Ljava/lang/String;JJLjava/lang/String;)Z
    .locals 8
    .param p1, "jsonString"    # Ljava/lang/String;
    .param p2, "modifiedTime"    # J
    .param p4, "rowId"    # J
    .param p6, "Key"    # Ljava/lang/String;

    .prologue
    .line 83
    const/4 v2, 0x0

    .line 84
    .local v2, "values":Landroid/content/ContentValues;
    sget-object v3, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    const-string v4, "caller_is_syncadapter"

    invoke-static {v3, v4}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 85
    .local v1, "uri":Landroid/net/Uri;
    iget-object v3, p0, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsGroupBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    sget-object v4, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsGroupBuilder;->GROUP_COLUMNS:[Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 88
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsGroupBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v3, p1}, Lcom/sec/android/sCloudSync/Records/JSONParser;->fromJSONString(Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v2

    .line 89
    const-string v3, "sync3"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 90
    const-string v3, "dirty"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 91
    const-string v3, "deleted"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 99
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsGroupBuilder;->mProvider:Landroid/content/ContentProviderClient;

    const-string v4, "_id = ? "

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static {p4, p5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v1, v2, v4, v5}, Landroid/content/ContentProviderClient;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 105
    const/4 v3, 0x1

    :goto_0
    return v3

    .line 93
    :catch_0
    move-exception v0

    .line 94
    .local v0, "e":Lorg/json/JSONException;
    const-string v3, "ContactGroupBuilder"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unable to Parse;"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    const/4 v3, 0x0

    goto :goto_0

    .line 100
    .end local v0    # "e":Lorg/json/JSONException;
    :catch_1
    move-exception v0

    .line 101
    .local v0, "e":Landroid/os/RemoteException;
    const-string v3, "ContactGroupBuilder"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "RemoteException"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    const/4 v3, 0x0

    goto :goto_0
.end method
