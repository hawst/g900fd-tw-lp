.class public Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;
.super Ljava/lang/Object;
.source "AuthControlForOldDataRelay.java"

# interfaces
.implements Lcom/sec/android/sCloudSync/Auth/IAuthControl;


# static fields
.field private static final ACCESS_TOKEN:Ljava/lang/String; = "com.sec.android.samsungcloudsync.AccessToken"

.field private static final AUTH_THREAD_NAME:Ljava/lang/String; = "AuthThread"

.field private static final TAG:Ljava/lang/String; = "AuthControl"


# instance fields
.field private mAuth:Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;

.field private mAuthManager:Lcom/sec/android/sCloudSync/Auth/AuthManager;

.field private final mPushClientConnection:Landroid/content/ServiceConnection;

.field private mService:Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;

.field private mbCancelled:Z

.field private final syncLock:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object v1, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->mService:Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;

    .line 39
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->syncLock:Ljava/lang/Object;

    .line 40
    iput-object v1, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->mAuthManager:Lcom/sec/android/sCloudSync/Auth/AuthManager;

    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->mbCancelled:Z

    .line 46
    new-instance v0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay$1;

    invoke-direct {v0, p0}, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay$1;-><init>(Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;)V

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->mPushClientConnection:Landroid/content/ServiceConnection;

    .line 72
    invoke-static {}, Lcom/sec/android/sCloudSync/Auth/AuthManager;->create()Lcom/sec/android/sCloudSync/Auth/AuthManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->mAuthManager:Lcom/sec/android/sCloudSync/Auth/AuthManager;

    .line 73
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;)Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->mService:Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;)Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;
    .param p1, "x1"    # Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->mService:Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->syncLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;)Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;
    .param p1, "x1"    # Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->mAuth:Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;)Landroid/content/ServiceConnection;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->mPushClientConnection:Landroid/content/ServiceConnection;

    return-object v0
.end method


# virtual methods
.method public cancel()V
    .locals 2

    .prologue
    .line 77
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->mbCancelled:Z

    .line 78
    iget-object v1, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->syncLock:Ljava/lang/Object;

    monitor-enter v1

    .line 79
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->syncLock:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 80
    monitor-exit v1

    .line 81
    return-void

    .line 80
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getAuthInformation(Landroid/content/Context;Ljava/lang/String;Z)Lcom/sec/android/sCloudSync/Auth/AuthManager;
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "ctid"    # Ljava/lang/String;
    .param p3, "bFresh"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/16 v6, 0x9

    .line 86
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->mbCancelled:Z

    .line 87
    iput-object v7, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->mService:Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;

    .line 88
    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.sec.android.samsungcloudsync.AccessToken"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 89
    .local v3, "intent":Landroid/content/Intent;
    iget-object v4, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->mPushClientConnection:Landroid/content/ServiceConnection;

    const/4 v5, 0x1

    invoke-virtual {p1, v3, v4, v5}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    .line 91
    .local v0, "bResult":Z
    if-eqz v0, :cond_6

    .line 92
    iget-object v4, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->mService:Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;

    if-nez v4, :cond_0

    .line 94
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->syncLock:Ljava/lang/Object;

    monitor-enter v5
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    :try_start_1
    iget-object v4, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->syncLock:Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Object;->wait()V

    .line 96
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 107
    :cond_0
    iget-object v4, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->mService:Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;

    if-nez v4, :cond_3

    .line 108
    iget-boolean v4, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->mbCancelled:Z

    if-eqz v4, :cond_2

    .line 109
    new-instance v4, Lcom/sec/android/sCloudSync/Util/SyncException;

    invoke-direct {v4, v6}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v4

    .line 96
    :catchall_0
    move-exception v4

    :try_start_2
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v4
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0

    .line 97
    :catch_0
    move-exception v1

    .line 98
    .local v1, "e1":Ljava/lang/InterruptedException;
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 99
    const-string v4, "AuthControl"

    const-string v5, "getAuthInformation: INTERRUPTED - wait()."

    invoke-static {v4, v5}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    iget-object v4, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->mService:Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;

    if-eqz v4, :cond_1

    .line 101
    iget-object v4, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->mPushClientConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p1, v4}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 102
    iput-object v7, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->mService:Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;

    .line 104
    :cond_1
    new-instance v4, Lcom/sec/android/sCloudSync/Util/SyncException;

    invoke-direct {v4, v6}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v4

    .line 111
    .end local v1    # "e1":Ljava/lang/InterruptedException;
    :cond_2
    new-instance v4, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/4 v5, 0x6

    invoke-direct {v4, v5}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v4

    .line 114
    :cond_3
    sget-object v4, Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v4, v7}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;

    iput-object v4, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->mAuth:Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;

    .line 115
    new-instance v2, Ljava/lang/Thread;

    new-instance v4, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay$2;

    invoke-direct {v4, p0, p3, p1}, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay$2;-><init>(Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;ZLandroid/content/Context;)V

    const-string v5, "AuthThread"

    invoke-direct {v2, v4, v5}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 140
    .local v2, "getAuthThread":Ljava/lang/Thread;
    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 142
    :try_start_4
    iget-object v5, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->syncLock:Ljava/lang/Object;

    monitor-enter v5
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_1

    .line 143
    :try_start_5
    iget-object v4, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->syncLock:Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Object;->wait()V

    .line 144
    monitor-exit v5
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 150
    iget-boolean v4, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->mbCancelled:Z

    if-eqz v4, :cond_4

    .line 151
    new-instance v4, Lcom/sec/android/sCloudSync/Util/SyncException;

    invoke-direct {v4, v6}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v4

    .line 144
    :catchall_1
    move-exception v4

    :try_start_6
    monitor-exit v5
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    throw v4
    :try_end_7
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_1

    .line 145
    :catch_1
    move-exception v1

    .line 146
    .restart local v1    # "e1":Ljava/lang/InterruptedException;
    const-string v4, "AuthControl"

    const-string v5, "getAuthInformation: INTERRUPTED - wait()."

    invoke-static {v4, v5}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    new-instance v4, Lcom/sec/android/sCloudSync/Util/SyncException;

    invoke-direct {v4, v6}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v4

    .line 152
    .end local v1    # "e1":Ljava/lang/InterruptedException;
    :cond_4
    iget-object v4, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->mService:Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;

    if-eqz v4, :cond_5

    .line 153
    iget-object v4, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->mPushClientConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p1, v4}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 154
    iput-object v7, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->mService:Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;

    .line 156
    :cond_5
    iget-object v4, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->mAuthManager:Lcom/sec/android/sCloudSync/Auth/AuthManager;

    iget-object v5, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->mAuth:Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;

    invoke-virtual {v4, v5}, Lcom/sec/android/sCloudSync/Auth/AuthManager;->updateAuthInformation(Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;)V

    .line 158
    .end local v2    # "getAuthThread":Ljava/lang/Thread;
    :cond_6
    iget-object v4, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->mAuthManager:Lcom/sec/android/sCloudSync/Auth/AuthManager;

    return-object v4
.end method
