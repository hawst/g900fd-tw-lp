.class public Lcom/sec/android/sCloudSync/Services/SNoteSyncService;
.super Landroid/app/Service;
.source "SNoteSyncService.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SNOTE-SYNC"


# instance fields
.field private final mBinder:Lcom/sec/android/sCloudSync/IDataSync/IDataSync$Stub;

.field private sync:Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 36
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Services/SNoteSyncService;->sync:Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;

    .line 55
    new-instance v0, Lcom/sec/android/sCloudSync/Services/SNoteSyncService$1;

    invoke-direct {v0, p0}, Lcom/sec/android/sCloudSync/Services/SNoteSyncService$1;-><init>(Lcom/sec/android/sCloudSync/Services/SNoteSyncService;)V

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Services/SNoteSyncService;->mBinder:Lcom/sec/android/sCloudSync/IDataSync/IDataSync$Stub;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/sCloudSync/Services/SNoteSyncService;)Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/sCloudSync/Services/SNoteSyncService;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Services/SNoteSyncService;->sync:Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Services/SNoteSyncService;->mBinder:Lcom/sec/android/sCloudSync/IDataSync/IDataSync$Stub;

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 39
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 40
    new-instance v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/Services/SNoteSyncService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Services/SNoteSyncService;->sync:Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;

    .line 41
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 52
    const-string v0, "SNOTE-SYNC"

    const-string v1, "SYNC SERVICE END"

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    return-void
.end method
