.class public final Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;
.super Ljava/lang/Object;
.source "NetworkResponse.java"


# instance fields
.field private mBody:Ljava/lang/String;

.field private mFile:Ljava/io/InputStream;

.field private mStatus:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 31
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;->mStatus:I

    .line 32
    iput-object v1, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;->mBody:Ljava/lang/String;

    .line 33
    iput-object v1, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;->mFile:Ljava/io/InputStream;

    .line 34
    return-void
.end method

.method public getBody()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;->mBody:Ljava/lang/String;

    return-object v0
.end method

.method public getFileInStream()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;->mFile:Ljava/io/InputStream;

    return-object v0
.end method

.method public getStatus()I
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;->mStatus:I

    return v0
.end method

.method public isFileStream()Z
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;->mFile:Ljava/io/InputStream;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setBody(Ljava/lang/String;)V
    .locals 0
    .param p1, "body"    # Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;->mBody:Ljava/lang/String;

    .line 48
    return-void
.end method

.method public setFileInStream(Ljava/io/InputStream;)V
    .locals 0
    .param p1, "file"    # Ljava/io/InputStream;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;->mFile:Ljava/io/InputStream;

    .line 53
    return-void
.end method

.method public setStatus(I)V
    .locals 0
    .param p1, "status"    # I

    .prologue
    .line 42
    iput p1, p0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;->mStatus:I

    .line 43
    return-void
.end method
