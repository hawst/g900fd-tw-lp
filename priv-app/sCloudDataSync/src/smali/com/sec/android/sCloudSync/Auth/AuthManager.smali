.class public final Lcom/sec/android/sCloudSync/Auth/AuthManager;
.super Ljava/lang/Object;
.source "AuthManager.java"


# static fields
.field private static final INFO_OK:I = 0x0

.field private static final TAG:Ljava/lang/String; = "AuthManager"

.field private static sAuthManager:Lcom/sec/android/sCloudSync/Auth/AuthManager;


# instance fields
.field private mAccessToken:Ljava/lang/String;

.field private mBaseUrlPrimary:Ljava/lang/String;

.field private mGetAPIparms:Ljava/lang/String;

.field private mPutAPIparms:Ljava/lang/String;

.field private mRegistrationID:Ljava/lang/String;

.field private mUserId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/sCloudSync/Auth/AuthManager;->sAuthManager:Lcom/sec/android/sCloudSync/Auth/AuthManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object v0, p0, Lcom/sec/android/sCloudSync/Auth/AuthManager;->mUserId:Ljava/lang/String;

    .line 35
    iput-object v0, p0, Lcom/sec/android/sCloudSync/Auth/AuthManager;->mAccessToken:Ljava/lang/String;

    .line 36
    iput-object v0, p0, Lcom/sec/android/sCloudSync/Auth/AuthManager;->mRegistrationID:Ljava/lang/String;

    .line 37
    iput-object v0, p0, Lcom/sec/android/sCloudSync/Auth/AuthManager;->mBaseUrlPrimary:Ljava/lang/String;

    .line 39
    iput-object v0, p0, Lcom/sec/android/sCloudSync/Auth/AuthManager;->mGetAPIparms:Ljava/lang/String;

    .line 40
    iput-object v0, p0, Lcom/sec/android/sCloudSync/Auth/AuthManager;->mPutAPIparms:Ljava/lang/String;

    .line 59
    return-void
.end method

.method public static declared-synchronized create()Lcom/sec/android/sCloudSync/Auth/AuthManager;
    .locals 2

    .prologue
    .line 63
    const-class v1, Lcom/sec/android/sCloudSync/Auth/AuthManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/sCloudSync/Auth/AuthManager;->sAuthManager:Lcom/sec/android/sCloudSync/Auth/AuthManager;

    if-nez v0, :cond_0

    .line 64
    new-instance v0, Lcom/sec/android/sCloudSync/Auth/AuthManager;

    invoke-direct {v0}, Lcom/sec/android/sCloudSync/Auth/AuthManager;-><init>()V

    sput-object v0, Lcom/sec/android/sCloudSync/Auth/AuthManager;->sAuthManager:Lcom/sec/android/sCloudSync/Auth/AuthManager;

    .line 66
    :cond_0
    sget-object v0, Lcom/sec/android/sCloudSync/Auth/AuthManager;->sAuthManager:Lcom/sec/android/sCloudSync/Auth/AuthManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 63
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private handleResult(I)V
    .locals 2
    .param p1, "status"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x3

    .line 124
    if-nez p1, :cond_1

    .line 125
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Auth/AuthManager;->mUserId:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/sCloudSync/Auth/AuthManager;->mRegistrationID:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/sCloudSync/Auth/AuthManager;->mAccessToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/sCloudSync/Auth/AuthManager;->mBaseUrlPrimary:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 127
    :cond_0
    new-instance v0, Lcom/sec/android/sCloudSync/Util/SyncException;

    invoke-direct {v0, v1}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v0

    .line 130
    :cond_1
    new-instance v0, Lcom/sec/android/sCloudSync/Util/SyncException;

    invoke-direct {v0, v1}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v0

    .line 132
    :cond_2
    return-void
.end method


# virtual methods
.method public accountRemoved(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 48
    iput-object v0, p0, Lcom/sec/android/sCloudSync/Auth/AuthManager;->mUserId:Ljava/lang/String;

    .line 49
    iput-object v0, p0, Lcom/sec/android/sCloudSync/Auth/AuthManager;->mAccessToken:Ljava/lang/String;

    .line 50
    iput-object v0, p0, Lcom/sec/android/sCloudSync/Auth/AuthManager;->mRegistrationID:Ljava/lang/String;

    .line 51
    iput-object v0, p0, Lcom/sec/android/sCloudSync/Auth/AuthManager;->mBaseUrlPrimary:Ljava/lang/String;

    .line 53
    iput-object v0, p0, Lcom/sec/android/sCloudSync/Auth/AuthManager;->mGetAPIparms:Ljava/lang/String;

    .line 54
    iput-object v0, p0, Lcom/sec/android/sCloudSync/Auth/AuthManager;->mPutAPIparms:Ljava/lang/String;

    .line 55
    return-void
.end method

.method public getBaseUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Auth/AuthManager;->mBaseUrlPrimary:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Auth/AuthManager;->mRegistrationID:Ljava/lang/String;

    return-object v0
.end method

.method public getGetApiParams(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "cid"    # Ljava/lang/String;

    .prologue
    .line 71
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/android/sCloudSync/Auth/AuthManager;->mGetAPIparms:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&cid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getGetApiParamsWithoutCid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Auth/AuthManager;->mGetAPIparms:Ljava/lang/String;

    return-object v0
.end method

.method public getPutAPIParamsWithoutCid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Auth/AuthManager;->mPutAPIparms:Ljava/lang/String;

    return-object v0
.end method

.method public getPutApiParams(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "cid"    # Ljava/lang/String;

    .prologue
    .line 76
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/android/sCloudSync/Auth/AuthManager;->mPutAPIparms:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&cid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized updateAuthInformation(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "status"    # I
    .param p2, "userId"    # Ljava/lang/String;
    .param p3, "regId"    # Ljava/lang/String;
    .param p4, "accessToken"    # Ljava/lang/String;
    .param p5, "baseUrl"    # Ljava/lang/String;
    .param p6, "baseUrl2"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .line 102
    monitor-enter p0

    :try_start_0
    const-string v0, "AuthManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getAuthInformation : STATUS = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    if-nez p1, :cond_0

    .line 104
    iput-object p5, p0, Lcom/sec/android/sCloudSync/Auth/AuthManager;->mBaseUrlPrimary:Ljava/lang/String;

    .line 106
    iput-object p4, p0, Lcom/sec/android/sCloudSync/Auth/AuthManager;->mAccessToken:Ljava/lang/String;

    .line 107
    iput-object p2, p0, Lcom/sec/android/sCloudSync/Auth/AuthManager;->mUserId:Ljava/lang/String;

    .line 108
    iput-object p3, p0, Lcom/sec/android/sCloudSync/Auth/AuthManager;->mRegistrationID:Ljava/lang/String;

    .line 110
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "&uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/sCloudSync/Auth/AuthManager;->mUserId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&access_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/sCloudSync/Auth/AuthManager;->mAccessToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Auth/AuthManager;->mGetAPIparms:Ljava/lang/String;

    .line 111
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/android/sCloudSync/Auth/AuthManager;->mGetAPIparms:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&did="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/sCloudSync/Auth/AuthManager;->mRegistrationID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Auth/AuthManager;->mPutAPIparms:Ljava/lang/String;

    .line 113
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/sCloudSync/Auth/AuthManager;->handleResult(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 114
    monitor-exit p0

    return-void

    .line 102
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized updateAuthInformation(Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;)V
    .locals 7
    .param p1, "auth"    # Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .line 118
    monitor-enter p0

    :try_start_0
    iget v1, p1, Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;->status:I

    iget-object v2, p1, Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;->userId:Ljava/lang/String;

    iget-object v3, p1, Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;->regId:Ljava/lang/String;

    iget-object v4, p1, Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;->accessToken:Ljava/lang/String;

    iget-object v5, p1, Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;->baseUrl:Ljava/lang/String;

    iget-object v6, p1, Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;->baseUrl2:Ljava/lang/String;

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/sCloudSync/Auth/AuthManager;->updateAuthInformation(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 119
    monitor-exit p0

    return-void

    .line 118
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
