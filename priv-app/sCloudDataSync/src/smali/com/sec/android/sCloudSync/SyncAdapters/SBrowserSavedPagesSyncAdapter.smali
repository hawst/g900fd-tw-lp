.class public Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;
.super Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;
.source "SBrowserSavedPagesSyncAdapter.java"


# static fields
.field private static final READ_MODE:Ljava/lang/String; = "r"

.field private static final SAVEDPAGESPATH:Ljava/lang/String; = "savedpagespath"

.field private static final SLASH:Ljava/lang/String; = "/"

.field private static final SYNCED:Ljava/lang/String; = "synced_to_server"

.field private static final TAG:Ljava/lang/String; = "SBrowserSavedPagesSyncAdapter"

.field private static final WRITE_MODE:Ljava/lang/String; = "w"

.field private static mData:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const/16 v0, 0x2000

    new-array v0, v0, [B

    sput-object v0, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->mData:[B

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 58
    new-instance v0, Lcom/sec/android/sCloudSync/ServiceManagers/SCloudServiceManager;

    const-string v1, "QUVql3tKM8"

    const/4 v2, 0x1

    invoke-direct {v0, p1, v1, v2}, Lcom/sec/android/sCloudSync/ServiceManagers/SCloudServiceManager;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    invoke-direct {p0, p1, v0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;-><init>(Landroid/content/Context;Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;)V

    .line 59
    return-void
.end method

.method private uploadSavedPagesToORSServer(Ljava/util/List;)V
    .locals 30
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/sCloudSync/Records/RecordSetItem;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;,
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 233
    .local p1, "recordstoSetList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordSetItem;>;"
    const/4 v13, 0x0

    .line 234
    .local v13, "fileUploadResponse":Lcom/sec/android/sCloudSync/Records/ORSResponse;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 235
    .local v3, "recordORSItemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordORSItem;>;"
    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    .line 237
    .local v23, "keysTobeRemoved":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz p1, :cond_a

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_a

    .line 239
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    check-cast v2, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserSavedPagesBuilder;

    invoke-virtual {v2}, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserSavedPagesBuilder;->getSavedPagesUploadList()Landroid/util/SparseArray;

    move-result-object v26

    .line 244
    .local v26, "savedPagesUploadListArr":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->mLocalChangedRecords:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v22

    .line 245
    .local v22, "keySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface/range {v22 .. v22}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .local v15, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/String;

    .line 246
    .local v21, "key":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->mLocalChangedRecords:Ljava/util/Map;

    move-object/from16 v0, v21

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    .line 247
    .local v16, "id":J
    move-wide/from16 v0, v16

    long-to-int v2, v0

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Ljava/lang/String;

    .line 249
    .local v29, "uploadFileUri":Ljava/lang/String;
    new-instance v2, Lcom/sec/android/sCloudSync/Records/RecordORSItem;

    const-string v5, "savedpagespath"

    move-object/from16 v0, v29

    invoke-direct {v2, v5, v0}, Lcom/sec/android/sCloudSync/Records/RecordORSItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 250
    if-eqz v29, :cond_4

    .line 251
    const-string v2, "/"

    move-object/from16 v0, v29

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v28

    .line 252
    .local v28, "split":[Ljava/lang/String;
    move-object/from16 v0, v28

    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    aget-object v4, v28, v2

    .line 255
    .local v4, "fileName":Ljava/lang/String;
    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->mbSyncCanceled:Z

    const/4 v5, 0x1

    if-ne v2, v5, :cond_1

    .line 256
    new-instance v2, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/16 v5, 0x9

    invoke-direct {v2, v5}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/sec/android/sCloudSync/Util/SyncException; {:try_start_0 .. :try_end_0} :catch_1

    .line 270
    :catch_0
    move-exception v11

    .line 271
    .local v11, "e":Ljava/io/IOException;
    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 290
    .end local v4    # "fileName":Ljava/lang/String;
    .end local v11    # "e":Ljava/io/IOException;
    .end local v16    # "id":J
    .end local v21    # "key":Ljava/lang/String;
    .end local v28    # "split":[Ljava/lang/String;
    .end local v29    # "uploadFileUri":Ljava/lang/String;
    :cond_0
    :goto_1
    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v19

    .line 291
    .local v19, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_2
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 292
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->mLocalChangedRecords:Ljava/util/Map;

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 258
    .end local v19    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .restart local v4    # "fileName":Ljava/lang/String;
    .restart local v16    # "id":J
    .restart local v21    # "key":Ljava/lang/String;
    .restart local v28    # "split":[Ljava/lang/String;
    .restart local v29    # "uploadFileUri":Ljava/lang/String;
    :cond_1
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v2

    invoke-static/range {v29 .. v29}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const-string v6, "r"

    invoke-virtual {v2, v5, v6}, Landroid/content/ContentProviderClient;->openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v12

    .line 259
    .local v12, "fileDesc":Landroid/os/ParcelFileDescriptor;
    new-instance v14, Ljava/io/FileInputStream;

    invoke-virtual {v12}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v2

    invoke-direct {v14, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V

    .line 260
    .local v14, "fin":Ljava/io/FileInputStream;
    new-instance v10, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v10}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 262
    .local v10, "buffer":Ljava/io/ByteArrayOutputStream;
    :goto_3
    sget-object v2, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->mData:[B

    const/4 v5, 0x0

    sget-object v6, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->mData:[B

    array-length v6, v6

    invoke-virtual {v14, v2, v5, v6}, Ljava/io/FileInputStream;->read([BII)I

    move-result v24

    .local v24, "nRead":I
    const/4 v2, -0x1

    move/from16 v0, v24

    if-eq v0, v2, :cond_2

    .line 263
    sget-object v2, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->mData:[B

    const/4 v5, 0x0

    move/from16 v0, v24

    invoke-virtual {v10, v2, v5, v0}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/sec/android/sCloudSync/Util/SyncException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_3

    .line 273
    .end local v10    # "buffer":Ljava/io/ByteArrayOutputStream;
    .end local v12    # "fileDesc":Landroid/os/ParcelFileDescriptor;
    .end local v14    # "fin":Ljava/io/FileInputStream;
    .end local v24    # "nRead":I
    :catch_1
    move-exception v11

    .line 274
    .local v11, "e":Lcom/sec/android/sCloudSync/Util/SyncException;
    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 265
    .end local v11    # "e":Lcom/sec/android/sCloudSync/Util/SyncException;
    .restart local v10    # "buffer":Ljava/io/ByteArrayOutputStream;
    .restart local v12    # "fileDesc":Landroid/os/ParcelFileDescriptor;
    .restart local v14    # "fin":Ljava/io/FileInputStream;
    .restart local v24    # "nRead":I
    :cond_2
    :try_start_2
    invoke-virtual {v10}, Ljava/io/ByteArrayOutputStream;->flush()V

    .line 266
    invoke-virtual {v14}, Ljava/io/FileInputStream;->close()V

    .line 267
    invoke-virtual {v12}, Landroid/os/ParcelFileDescriptor;->close()V

    .line 268
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getCloudServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getFileServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/android/scloud/framework/util/TimeManager;->getCurrentTime(Landroid/content/Context;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v21

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v10}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v9

    invoke-virtual/range {v2 .. v9}, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->uploadFile(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z[B)Lcom/sec/android/sCloudSync/Records/ORSResponse;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcom/sec/android/sCloudSync/Util/SyncException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v13

    .line 277
    invoke-virtual {v13}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getResponseCode()I

    move-result v2

    if-eqz v2, :cond_4

    .line 278
    invoke-virtual {v13}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getResponseCode()I

    move-result v2

    const/16 v5, 0x4e23

    if-eq v2, v5, :cond_3

    invoke-virtual {v13}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getResponseCode()I

    move-result v2

    const/16 v5, 0x4e24

    if-ne v2, v5, :cond_5

    .line 280
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->notifyServerStoragefull()V

    .line 287
    .end local v4    # "fileName":Ljava/lang/String;
    .end local v10    # "buffer":Ljava/io/ByteArrayOutputStream;
    .end local v12    # "fileDesc":Landroid/os/ParcelFileDescriptor;
    .end local v14    # "fin":Ljava/io/FileInputStream;
    .end local v24    # "nRead":I
    .end local v28    # "split":[Ljava/lang/String;
    :cond_4
    invoke-interface {v3}, Ljava/util/List;->clear()V

    goto/16 :goto_0

    .line 282
    .restart local v4    # "fileName":Ljava/lang/String;
    .restart local v10    # "buffer":Ljava/io/ByteArrayOutputStream;
    .restart local v12    # "fileDesc":Landroid/os/ParcelFileDescriptor;
    .restart local v14    # "fin":Ljava/io/FileInputStream;
    .restart local v24    # "nRead":I
    .restart local v28    # "split":[Ljava/lang/String;
    :cond_5
    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 297
    .end local v4    # "fileName":Ljava/lang/String;
    .end local v10    # "buffer":Ljava/io/ByteArrayOutputStream;
    .end local v12    # "fileDesc":Landroid/os/ParcelFileDescriptor;
    .end local v14    # "fin":Ljava/io/FileInputStream;
    .end local v16    # "id":J
    .end local v21    # "key":Ljava/lang/String;
    .end local v24    # "nRead":I
    .end local v28    # "split":[Ljava/lang/String;
    .end local v29    # "uploadFileUri":Ljava/lang/String;
    .restart local v19    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_6
    new-instance v25, Ljava/util/ArrayList;

    invoke-direct/range {v25 .. v25}, Ljava/util/ArrayList;-><init>()V

    .line 298
    .local v25, "recordsTobeRemoved":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordSetItem;>;"
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v27

    .line 300
    .local v27, "setListIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/sCloudSync/Records/RecordSetItem;>;"
    :cond_7
    :goto_4
    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 301
    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/android/sCloudSync/Records/RecordSetItem;

    .line 302
    .local v20, "item":Lcom/sec/android/sCloudSync/Records/RecordSetItem;
    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/sCloudSync/Records/RecordSetItem;->getKEY()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 303
    move-object/from16 v0, v25

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 307
    .end local v20    # "item":Lcom/sec/android/sCloudSync/Records/RecordSetItem;
    :cond_8
    invoke-interface/range {v25 .. v25}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    .line 308
    .local v18, "ir":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/sCloudSync/Records/RecordSetItem;>;"
    :goto_5
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 309
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_5

    .line 312
    :cond_9
    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->clear()V

    .line 313
    invoke-interface/range {v25 .. v25}, Ljava/util/List;->clear()V

    .line 315
    .end local v15    # "i$":Ljava/util/Iterator;
    .end local v18    # "ir":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/sCloudSync/Records/RecordSetItem;>;"
    .end local v19    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v22    # "keySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v25    # "recordsTobeRemoved":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordSetItem;>;"
    .end local v26    # "savedPagesUploadListArr":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    .end local v27    # "setListIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/sCloudSync/Records/RecordSetItem;>;"
    :cond_a
    return-void
.end method


# virtual methods
.method protected doUpdateDelete(Lcom/sec/android/sCloudSync/Records/KVSItem;Lcom/sec/android/sCloudSync/Records/RecordItem;Landroid/content/SyncStats;)Z
    .locals 10
    .param p1, "item"    # Lcom/sec/android/sCloudSync/Records/KVSItem;
    .param p2, "record"    # Lcom/sec/android/sCloudSync/Records/RecordItem;
    .param p3, "stats"    # Landroid/content/SyncStats;

    .prologue
    const/4 v9, 0x1

    .line 147
    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->isDeleted()Z

    move-result v0

    if-ne v0, v9, :cond_0

    .line 148
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getContentUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getID()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v8

    .line 149
    .local v8, "uri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getCallerSyncAdapter()Ljava/lang/String;

    move-result-object v0

    invoke-static {v8, v0}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getAccountName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v2

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getAccountType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v2

    iget-object v2, v2, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v8

    .line 154
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v8, v1, v2}, Landroid/content/ContentProviderClient;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 170
    .end local v8    # "uri":Landroid/net/Uri;
    :goto_0
    return v9

    .line 155
    .restart local v8    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v7

    .line 156
    .local v7, "e":Landroid/os/RemoteException;
    const-string v0, "SBrowserSavedPagesSyncAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to delete record with key:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getKEY()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    invoke-virtual {v7}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 161
    .end local v7    # "e":Landroid/os/RemoteException;
    .end local v8    # "uri":Landroid/net/Uri;
    :cond_0
    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getValue()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getTimeStamp()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getID()J

    move-result-wide v4

    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getKEY()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;->update(Ljava/lang/String;JJLjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 162
    :cond_1
    const-string v0, "SBrowserSavedPagesSyncAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to update record with key:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getKEY()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 167
    :cond_2
    iget-wide v0, p3, Landroid/content/SyncStats;->numUpdates:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p3, Landroid/content/SyncStats;->numUpdates:J

    goto :goto_0
.end method

.method protected getAccountName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    const-string v0, "account_name"

    return-object v0
.end method

.method protected getAccountType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    const-string v0, "account_type"

    return-object v0
.end method

.method protected getBuilder()Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;
    .locals 4

    .prologue
    .line 108
    new-instance v0, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserSavedPagesBuilder;

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserSavedPagesBuilder;-><init>(Landroid/content/ContentProviderClient;Landroid/accounts/Account;Landroid/content/Context;)V

    return-object v0
.end method

.method protected getCallerSyncAdapter()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    const-string v0, "caller_is_syncadapter"

    return-object v0
.end method

.method protected getCid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 133
    const-string v0, "QUVql3tKM8"

    return-object v0
.end method

.method protected getContentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 83
    sget-object v0, Lcom/sec/android/sCloudSync/Constants/SBrowserContract;->SBROWSER_SAVEDPAGES_URI:Landroid/net/Uri;

    return-object v0
.end method

.method protected getCtidKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 490
    const-string v0, "SS"

    return-object v0
.end method

.method protected getDeletedColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    const-string v0, "is_deleted"

    return-object v0
.end method

.method protected getDirtyColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    const-string v0, "is_dirty"

    return-object v0
.end method

.method protected getIdColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    const-string v0, "_id"

    return-object v0
.end method

.method protected getKeyColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    const-string v0, "sync1"

    return-object v0
.end method

.method protected getProjection()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 413
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getIdColumnName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getKeyColumnName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getDirtyColumnName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getTimeStampColumnName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getDeletedColumnName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    return-object v0
.end method

.method protected getSyncAdapterName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    const-string v0, "SBROWSER_SAVEDPAGES"

    return-object v0
.end method

.method protected getSyncStateDataColumn()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    const-string v0, "data"

    return-object v0
.end method

.method protected getSyncStateURI()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/sec/android/sCloudSync/Constants/SBrowserContract;->SBROWSER_SYNC_STATE_URI:Landroid/net/Uri;

    return-object v0
.end method

.method protected getTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 128
    const-string v0, "SBrowserSavedPagesSyncAdapter"

    return-object v0
.end method

.method protected getTimeStampColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    const-string v0, "sync5"

    return-object v0
.end method

.method protected handleDeleteList(Ljava/util/List;Ljava/util/List;)Z
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/sCloudSync/Records/RecordBase;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/sCloudSync/Records/RecordSetItem;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;,
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 188
    .local p1, "recordsToDeleteList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordBase;>;"
    .local p2, "setList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordSetItem;>;"
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 190
    .local v12, "keysTobeRemoved":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v1, 0x1

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "path"

    aput-object v2, v3, v1

    .line 191
    .local v3, "projection":[Ljava/lang/String;
    if-eqz p1, :cond_5

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    .line 192
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/sCloudSync/Records/RecordBase;

    .line 193
    .local v8, "deleteItem":Lcom/sec/android/sCloudSync/Records/RecordBase;
    const/4 v13, 0x0

    .line 194
    .local v13, "orsResponse":Lcom/sec/android/sCloudSync/Records/ORSResponse;
    invoke-virtual {v8}, Lcom/sec/android/sCloudSync/Records/RecordBase;->getKEY()Ljava/lang/String;

    move-result-object v11

    .line 196
    .local v11, "key":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getContentUri()Landroid/net/Uri;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "sync1 = \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 199
    .local v7, "cursor":Landroid/database/Cursor;
    if-eqz v7, :cond_0

    .line 202
    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 203
    const-string v1, "path"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 204
    .local v14, "pagePath":Ljava/lang/String;
    const-string v1, "/"

    invoke-virtual {v14, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v16

    .line 205
    .local v16, "splitPagePath":[Ljava/lang/String;
    move-object/from16 v0, v16

    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-object v15, v16, v1

    .line 207
    .local v15, "pagePathfileName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->mbSyncCanceled:Z

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 208
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 209
    new-instance v1, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/16 v2, 0x9

    invoke-direct {v1, v2}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v1

    .line 213
    :cond_2
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getCloudServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getFileServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "/"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v8}, Lcom/sec/android/sCloudSync/Records/RecordBase;->getKEY()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "/"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->deleteFile(Ljava/lang/String;)Lcom/sec/android/sCloudSync/Records/ORSResponse;
    :try_end_0
    .catch Lcom/sec/android/sCloudSync/Util/SyncException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v13

    .line 218
    invoke-virtual {v13}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getResponseCode()I

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v13}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getResponseCode()I

    move-result v1

    const/16 v2, 0x7d02

    if-eq v1, v2, :cond_1

    .line 219
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->mLocalChangedRecords:Ljava/util/Map;

    invoke-interface {v1, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 220
    invoke-virtual {v12, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 221
    :cond_3
    const/4 v1, 0x1

    .line 228
    .end local v7    # "cursor":Landroid/database/Cursor;
    .end local v8    # "deleteItem":Lcom/sec/android/sCloudSync/Records/RecordBase;
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v11    # "key":Ljava/lang/String;
    .end local v13    # "orsResponse":Lcom/sec/android/sCloudSync/Records/ORSResponse;
    .end local v14    # "pagePath":Ljava/lang/String;
    .end local v15    # "pagePathfileName":Ljava/lang/String;
    .end local v16    # "splitPagePath":[Ljava/lang/String;
    :goto_1
    return v1

    .line 214
    .restart local v7    # "cursor":Landroid/database/Cursor;
    .restart local v8    # "deleteItem":Lcom/sec/android/sCloudSync/Records/RecordBase;
    .restart local v10    # "i$":Ljava/util/Iterator;
    .restart local v11    # "key":Ljava/lang/String;
    .restart local v13    # "orsResponse":Lcom/sec/android/sCloudSync/Records/ORSResponse;
    .restart local v14    # "pagePath":Ljava/lang/String;
    .restart local v15    # "pagePathfileName":Ljava/lang/String;
    .restart local v16    # "splitPagePath":[Ljava/lang/String;
    :catch_0
    move-exception v9

    .line 215
    .local v9, "e":Lcom/sec/android/sCloudSync/Util/SyncException;
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 216
    throw v9

    .line 224
    .end local v9    # "e":Lcom/sec/android/sCloudSync/Util/SyncException;
    .end local v14    # "pagePath":Ljava/lang/String;
    .end local v15    # "pagePathfileName":Ljava/lang/String;
    .end local v16    # "splitPagePath":[Ljava/lang/String;
    :cond_4
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 228
    .end local v7    # "cursor":Landroid/database/Cursor;
    .end local v8    # "deleteItem":Lcom/sec/android/sCloudSync/Records/RecordBase;
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v11    # "key":Ljava/lang/String;
    .end local v13    # "orsResponse":Lcom/sec/android/sCloudSync/Records/ORSResponse;
    :cond_5
    const/4 v1, 0x0

    goto :goto_1
.end method

.method protected handleUpdateToServerRecord(Landroid/content/ContentValues;Lcom/sec/android/sCloudSync/Records/RecordItemResponse;)V
    .locals 2
    .param p1, "values"    # Landroid/content/ContentValues;
    .param p2, "item"    # Lcom/sec/android/sCloudSync/Records/RecordItemResponse;

    .prologue
    .line 139
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getDirtyColumnName()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 140
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getTimeStampColumnName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItemResponse;->getTimeStamp()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 141
    const-string v0, "sync2"

    const-string v1, "synced_to_server"

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    return-void
.end method

.method protected removePreSyncedRecords(Ljava/util/Map;)Z
    .locals 23
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/sCloudSync/Records/KVSItem;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 418
    .local p1, "uServerRecords":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/sec/android/sCloudSync/Records/KVSItem;>;"
    if-eqz p1, :cond_0

    invoke-interface/range {p1 .. p1}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 419
    :cond_0
    const/4 v2, 0x0

    .line 485
    :goto_0
    return v2

    .line 421
    :cond_1
    const/16 v19, 0x0

    .line 422
    .local v19, "preCursor":Landroid/database/Cursor;
    invoke-interface/range {p1 .. p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v16

    .line 423
    .local v16, "keyset":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-interface/range {v16 .. v16}, Ljava/util/Set;->size()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    mul-int/lit16 v2, v2, 0x104

    move-object/from16 v0, v22

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 424
    .local v22, "where":Ljava/lang/StringBuilder;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getKeyColumnName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " IN ("

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 425
    move-object/from16 v0, v22

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Tools/AppTool;->appendIds(Ljava/lang/StringBuilder;Ljava/util/Set;)V

    .line 426
    const-string v2, ")"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 428
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getContentUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getCallerSyncAdapter()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getAccountName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v5

    iget-object v5, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getAccountType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v5

    iget-object v5, v5, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 434
    .local v3, "uriFromSyncAdapter":Landroid/net/Uri;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getProjection()[Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v19

    .line 440
    if-nez v19, :cond_2

    .line 441
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string v4, "RemovePreSyncedRecords: (ERROR)Cursor is null"

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 442
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 435
    :catch_0
    move-exception v12

    .line 436
    .local v12, "e":Landroid/os/RemoteException;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "RemovePreSyncedRecords: Exception in calling query "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v12}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 445
    .end local v12    # "e":Landroid/os/RemoteException;
    :cond_2
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-nez v2, :cond_3

    .line 446
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 447
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string v4, "RemovePreSyncedRecords: there is nothing to be removed."

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 448
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 451
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getIdColumnName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    .line 452
    .local v13, "id_index":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getKeyColumnName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    .line 453
    .local v15, "key_index":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getTimeStampColumnName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    .line 454
    .local v21, "time_index":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getDirtyColumnName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    .line 455
    .local v11, "dirtyIndex":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getDeletedColumnName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 458
    .local v9, "deletedIndex":I
    :cond_4
    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v17

    .line 459
    .local v17, "modifiedTimeUploadedtoServer":J
    move-object/from16 v0, v19

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 460
    .local v14, "key":Ljava/lang/String;
    move-object/from16 v0, v19

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 461
    .local v10, "dirtyField":I
    move-object/from16 v0, v19

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 463
    .local v8, "deletedField":I
    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/android/sCloudSync/Records/KVSItem;

    .line 464
    .local v20, "serverItem":Lcom/sec/android/sCloudSync/Records/KVSItem;
    if-eqz v20, :cond_7

    .line 465
    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getTimeStamp()J

    move-result-wide v4

    cmp-long v2, v17, v4

    if-ltz v2, :cond_6

    .line 466
    if-nez v10, :cond_5

    const/4 v2, 0x1

    if-ne v8, v2, :cond_5

    .line 468
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getKeyColumnName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentProviderClient;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 482
    :goto_1
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_4

    .line 484
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 485
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 469
    :catch_1
    move-exception v12

    .line 470
    .restart local v12    # "e":Landroid/os/RemoteException;
    invoke-virtual {v12}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 473
    .end local v12    # "e":Landroid/os/RemoteException;
    :cond_5
    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 476
    :cond_6
    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/sCloudSync/Records/KVSItem;

    move-object/from16 v0, v19

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/sec/android/sCloudSync/Records/KVSItem;->setID(J)V

    goto :goto_1

    .line 480
    :cond_7
    const-string v2, "SBrowserSavedPagesSyncAdapter"

    const-string v4, "Sync Broken : There is a duplicated record on Local."

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected updateLocalDb(Landroid/content/SyncStats;)V
    .locals 22
    .param p1, "stats"    # Landroid/content/SyncStats;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .line 319
    invoke-super/range {p0 .. p1}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->updateLocalDb(Landroid/content/SyncStats;)V

    .line 320
    const/4 v7, 0x0

    .line 321
    .local v7, "fileDownResponse":Lcom/sec/android/sCloudSync/Records/ORSResponse;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    move-object/from16 v19, v0

    check-cast v19, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserSavedPagesBuilder;

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserSavedPagesBuilder;->getSavedPagesDownloadList()Ljava/util/Map;

    move-result-object v8

    .line 322
    .local v8, "fileDownloadMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v8}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v14

    .line 323
    .local v14, "keySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 327
    .local v5, "failedDownloads":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getContentUri()Landroid/net/Uri;

    move-result-object v19

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getCallerSyncAdapter()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v19

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getAccountName()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v21

    move-object/from16 v0, v21

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v19 .. v21}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v19

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getAccountType()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v21

    move-object/from16 v0, v21

    iget-object v0, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v19 .. v21}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v15

    .line 332
    .local v15, "savedpagesUri":Landroid/net/Uri;
    new-instance v17, Landroid/content/ContentValues;

    invoke-direct/range {v17 .. v17}, Landroid/content/ContentValues;-><init>()V

    .line 333
    .local v17, "values":Landroid/content/ContentValues;
    invoke-interface {v14}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .local v12, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_4

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    .line 335
    .local v13, "key":Ljava/lang/String;
    invoke-interface {v8, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 336
    .local v10, "fileToDownload":Ljava/lang/String;
    const-string v19, "/"

    move-object/from16 v0, v19

    invoke-virtual {v10, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v16

    .line 337
    .local v16, "split":[Ljava/lang/String;
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v19, v0

    add-int/lit8 v19, v19, -0x1

    aget-object v9, v16, v19

    .line 339
    .local v9, "fileName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->mbSyncCanceled:Z

    move/from16 v19, v0

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_0

    .line 340
    new-instance v19, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/16 v20, 0x9

    invoke-direct/range {v19 .. v20}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v19

    .line 343
    :cond_0
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getCloudServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getFileServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;

    move-result-object v19

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0x1

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v10, v2}, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->downloadFile(Ljava/lang/String;Ljava/lang/String;Z)Lcom/sec/android/sCloudSync/Records/ORSResponse;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 349
    if-nez v7, :cond_1

    .line 350
    invoke-interface {v5, v13}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 351
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v19

    const-string v20, "file download response is null "

    invoke-static/range {v19 .. v20}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 344
    :catch_0
    move-exception v4

    .line 345
    .local v4, "e":Ljava/io/IOException;
    invoke-interface {v5, v13}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 346
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v19

    const-string v20, "IOexception when trying to download file"

    invoke-static/range {v19 .. v20}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    new-instance v19, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/16 v20, 0x4

    invoke-direct/range {v19 .. v20}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v19

    .line 354
    .end local v4    # "e":Ljava/io/IOException;
    :cond_1
    invoke-virtual {v7}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getResponseCode()I

    move-result v19

    if-eqz v19, :cond_2

    .line 356
    invoke-interface {v5, v13}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 357
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v19

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "failed to download saved page file. Rcode is "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v7}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getResponseCode()I

    move-result v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v19

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "file name = "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 362
    :cond_2
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v19

    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v20

    const-string v21, "w"

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentProviderClient;->openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v6

    .line 363
    .local v6, "fileDesc":Landroid/os/ParcelFileDescriptor;
    if-nez v6, :cond_3

    .line 364
    invoke-interface {v5, v13}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    goto/16 :goto_0

    .line 372
    .end local v6    # "fileDesc":Landroid/os/ParcelFileDescriptor;
    :catch_1
    move-exception v4

    .line 373
    .local v4, "e":Landroid/os/RemoteException;
    invoke-virtual {v4}, Landroid/os/RemoteException;->printStackTrace()V

    .line 374
    invoke-interface {v5, v13}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 367
    .end local v4    # "e":Landroid/os/RemoteException;
    .restart local v6    # "fileDesc":Landroid/os/ParcelFileDescriptor;
    :cond_3
    :try_start_2
    new-instance v11, Ljava/io/FileOutputStream;

    invoke-virtual {v6}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-direct {v11, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V

    .line 368
    .local v11, "fos":Ljava/io/FileOutputStream;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getCloudServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getFileServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->getBuffer()[B

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v11, v0}, Ljava/io/FileOutputStream;->write([B)V

    .line 369
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V

    .line 370
    invoke-virtual {v6}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 388
    const-string v19, "is_deleted"

    const/16 v20, 0x0

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 389
    const-string v19, "sync2"

    const-string v20, "synced_to_server"

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    :try_start_3
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v19

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getKeyColumnName()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " = \'"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "\'"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    move-object/from16 v2, v20

    move-object/from16 v3, v21

    invoke-virtual {v0, v15, v1, v2, v3}, Landroid/content/ContentProviderClient;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_4

    .line 395
    :goto_1
    invoke-virtual/range {v17 .. v17}, Landroid/content/ContentValues;->clear()V

    goto/16 :goto_0

    .line 376
    .end local v6    # "fileDesc":Landroid/os/ParcelFileDescriptor;
    .end local v11    # "fos":Ljava/io/FileOutputStream;
    :catch_2
    move-exception v4

    .line 377
    .local v4, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v4}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 378
    invoke-interface {v5, v13}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 380
    .end local v4    # "e":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v4

    .line 381
    .local v4, "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    .line 382
    invoke-interface {v5, v13}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 392
    .end local v4    # "e":Ljava/io/IOException;
    .restart local v6    # "fileDesc":Landroid/os/ParcelFileDescriptor;
    .restart local v11    # "fos":Ljava/io/FileOutputStream;
    :catch_4
    move-exception v4

    .line 393
    .local v4, "e":Landroid/os/RemoteException;
    invoke-virtual {v4}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 398
    .end local v4    # "e":Landroid/os/RemoteException;
    .end local v6    # "fileDesc":Landroid/os/ParcelFileDescriptor;
    .end local v9    # "fileName":Ljava/lang/String;
    .end local v10    # "fileToDownload":Ljava/lang/String;
    .end local v11    # "fos":Ljava/io/FileOutputStream;
    .end local v13    # "key":Ljava/lang/String;
    .end local v16    # "split":[Ljava/lang/String;
    :cond_4
    if-eqz v5, :cond_5

    invoke-interface {v5}, Ljava/util/Set;->isEmpty()Z

    move-result v19

    if-nez v19, :cond_5

    .line 399
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v19

    add-int/lit8 v19, v19, 0x1

    move/from16 v0, v19

    mul-int/lit16 v0, v0, 0x104

    move/from16 v19, v0

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 400
    .local v18, "where":Ljava/lang/StringBuilder;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getKeyColumnName()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " IN ("

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 401
    move-object/from16 v0, v18

    invoke-static {v0, v5}, Lcom/sec/android/sCloudSync/Tools/AppTool;->appendIds(Ljava/lang/StringBuilder;Ljava/util/Set;)V

    .line 402
    const-string v19, ")"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 404
    :try_start_4
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v19

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v15, v1, v2}, Landroid/content/ContentProviderClient;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_5

    .line 409
    .end local v18    # "where":Ljava/lang/StringBuilder;
    :cond_5
    :goto_2
    return-void

    .line 405
    .restart local v18    # "where":Ljava/lang/StringBuilder;
    :catch_5
    move-exception v4

    .line 406
    .restart local v4    # "e":Landroid/os/RemoteException;
    invoke-virtual {v4}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_2
.end method

.method public updatetoServer(Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/sCloudSync/Records/RecordSetItem;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/sCloudSync/Records/RecordBase;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;,
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 181
    .local p1, "recordstoSetList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordSetItem;>;"
    .local p2, "recordsToDeleteList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordBase;>;"
    invoke-direct {p0, p1}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;->uploadSavedPagesToORSServer(Ljava/util/List;)V

    .line 182
    invoke-super {p0, p1, p2}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->updatetoServer(Ljava/util/List;Ljava/util/List;)V

    .line 183
    return-void
.end method
