.class public Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;
.super Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;
.source "SNoteSyncAdapter.java"


# static fields
.field private static final CHECSKSUM:Ljava/lang/String; = "checksum"

.field private static final DIRTY_VALUES:Landroid/content/ContentValues;

.field private static final FILENAME:Ljava/lang/String; = "filename"

.field private static final JSON:Ljava/lang/String; = "json"

.field private static final PRIVATEKEY:Ljava/lang/String; = "privatekey"

.field private static final SERVER_SNOTE_FOLDER:Ljava/lang/String; = "/storage/sdcard0/SNote/"

.field private static final SLASH:Ljava/lang/String; = "/"

.field private static final TAG:Ljava/lang/String; = "SNoteSyncAdapter"

.field private static final TIMESTAMP:Ljava/lang/String; = "timestamp"

.field private static mData:[B


# instance fields
.field private final TEMP_FOLDER:Ljava/lang/String;

.field private mContract:Lcom/sec/android/sCloudSync/Constants/SNoteContract;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 89
    const/high16 v0, 0x100000

    new-array v0, v0, [B

    sput-object v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->mData:[B

    .line 93
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    sput-object v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->DIRTY_VALUES:Landroid/content/ContentValues;

    .line 95
    sget-object v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->DIRTY_VALUES:Landroid/content/ContentValues;

    const-string v1, "dirty"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 96
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 99
    new-instance v0, Lcom/sec/android/sCloudSync/ServiceManagers/SCloudServiceManager;

    const-string v1, "a1QGNqwu27"

    const/4 v2, 0x1

    invoke-direct {v0, p1, v1, v2}, Lcom/sec/android/sCloudSync/ServiceManagers/SCloudServiceManager;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    invoke-direct {p0, p1, v0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;-><init>(Landroid/content/Context;Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;)V

    .line 79
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/files/temp/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->TEMP_FOLDER:Ljava/lang/String;

    .line 101
    invoke-static {p1}, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->getInstance(Landroid/content/Context;)Lcom/sec/android/sCloudSync/Constants/SNoteContract;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->mContract:Lcom/sec/android/sCloudSync/Constants/SNoteContract;

    .line 103
    return-void
.end method

.method private clearGarbage()Z
    .locals 4

    .prologue
    .line 206
    iget-object v2, p0, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->mContract:Lcom/sec/android/sCloudSync/Constants/SNoteContract;

    iget-object v2, v2, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->DELETEALLSNB_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 208
    .local v0, "deleteAllSnbUri":Landroid/net/Uri;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentProviderClient;->openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_2

    .line 216
    :goto_0
    const/4 v2, 0x1

    return v2

    .line 209
    :catch_0
    move-exception v1

    .line 210
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 211
    .end local v1    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v1

    .line 212
    .local v1, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 213
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    :catch_2
    move-exception v1

    .line 214
    .local v1, "e":Ljava/lang/UnsupportedOperationException;
    invoke-virtual {v1}, Ljava/lang/UnsupportedOperationException;->printStackTrace()V

    goto :goto_0
.end method

.method private createSNoteFolderStructure()Z
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .line 1040
    const/4 v0, 0x0

    .line 1041
    .local v0, "createFolderResponse":Lcom/sec/android/sCloudSync/Records/ORSResponse;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1043
    .local v2, "recordMetaDataList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordORSItem;>;"
    new-instance v3, Lcom/sec/android/sCloudSync/Records/RecordORSItem;

    const-string v4, "FolderPath : /"

    const-string v5, "SNote"

    invoke-direct {v3, v4, v5}, Lcom/sec/android/sCloudSync/Records/RecordORSItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1045
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getCloudServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    move-result-object v3

    invoke-interface {v3}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getFileServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;

    move-result-object v3

    const-string v4, "/"

    const-string v5, "SNote"

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/android/scloud/framework/util/TimeManager;->getCurrentTime(Landroid/content/Context;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v2, v4, v5, v6}, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->createFolder(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/sCloudSync/Records/ORSResponse;
    :try_end_0
    .catch Lcom/sec/android/sCloudSync/Util/SyncException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1052
    invoke-virtual {v0}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getResponseCode()I

    move-result v3

    if-nez v3, :cond_0

    .line 1053
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Folder created successfully "

    invoke-static {v3, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1059
    :goto_0
    const/4 v3, 0x1

    :goto_1
    return v3

    .line 1047
    :catch_0
    move-exception v1

    .line 1048
    .local v1, "e":Lcom/sec/android/sCloudSync/Util/SyncException;
    invoke-virtual {v1}, Lcom/sec/android/sCloudSync/Util/SyncException;->printStackTrace()V

    .line 1049
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Create Folder Exception received :- "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/sec/android/sCloudSync/Util/SyncException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1050
    const/4 v3, 0x0

    goto :goto_1

    .line 1054
    .end local v1    # "e":Lcom/sec/android/sCloudSync/Util/SyncException;
    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getResponseCode()I

    move-result v3

    const/16 v4, 0x7918

    if-ne v3, v4, :cond_1

    .line 1055
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Folder already exists "

    invoke-static {v3, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1057
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Folder creation failed "

    invoke-static {v3, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private deleteFiles(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1027
    .local p1, "deleteFileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1028
    .local v3, "path":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1029
    .local v0, "filetoDelete":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1030
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v2

    .line 1031
    .local v2, "isDeleteSuccess":Z
    if-nez v2, :cond_0

    .line 1032
    const-string v4, "SNoteSyncAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "File "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " could not be deleted"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1036
    .end local v0    # "filetoDelete":Ljava/io/File;
    .end local v2    # "isDeleteSuccess":Z
    .end local v3    # "path":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private filterSingleQuote(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "localPath"    # Ljava/lang/String;

    .prologue
    .line 597
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 599
    .local v1, "result":Ljava/lang/StringBuilder;
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x1

    if-ge v2, v3, :cond_1

    .line 600
    :cond_0
    const/4 v2, 0x0

    .line 609
    :goto_0
    return-object v2

    .line 602
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 603
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x27

    if-ne v2, v3, :cond_2

    .line 604
    const-string v2, "\'\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 602
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 606
    :cond_2
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 609
    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method private getAppSupportUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1063
    iget-object v0, p0, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->mContract:Lcom/sec/android/sCloudSync/Constants/SNoteContract;

    iget-object v0, v0, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->APP_INFO_URI:Landroid/net/Uri;

    return-object v0
.end method

.method private handleSetList(Ljava/util/List;)Z
    .locals 33
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/sCloudSync/Records/RecordSetItem;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;,
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 490
    .local p1, "setList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordSetItem;>;"
    const/16 v24, 0x0

    .line 491
    .local v24, "hasUploadFailed":Z
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->mContract:Lcom/sec/android/sCloudSync/Constants/SNoteContract;

    iget-object v2, v2, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->FILEDETAIL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getCallerSyncAdapter()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v17

    .line 495
    .local v17, "fileDetailUri":Landroid/net/Uri;
    if-eqz p1, :cond_f

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_f

    .line 497
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    check-cast v2, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;

    invoke-virtual {v2}, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->getFileUploadList()Ljava/util/Map;

    move-result-object v18

    .line 498
    .local v18, "fileUploadMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    check-cast v2, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;

    invoke-virtual {v2}, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->getSNBFilePath()Ljava/lang/String;

    move-result-object v30

    .line 499
    .local v30, "snbFilePath":Ljava/lang/String;
    const/16 v19, 0x0

    .line 502
    .local v19, "fileUploadResponse":Lcom/sec/android/sCloudSync/Records/ORSResponse;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 504
    .local v3, "recordORSItemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordORSItem;>;"
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v25

    :goto_0
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_f

    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lcom/sec/android/sCloudSync/Records/RecordSetItem;

    .line 505
    .local v29, "setItem":Lcom/sec/android/sCloudSync/Records/RecordSetItem;
    invoke-virtual/range {v29 .. v29}, Lcom/sec/android/sCloudSync/Records/RecordSetItem;->getKEY()Ljava/lang/String;

    move-result-object v27

    .line 507
    .local v27, "key":Ljava/lang/String;
    if-eqz v18, :cond_2

    invoke-interface/range {v18 .. v18}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 509
    invoke-interface/range {v18 .. v18}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v26

    .local v26, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/util/Map$Entry;

    .line 510
    .local v15, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v15}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 511
    .local v7, "localFilepath":Ljava/lang/String;
    invoke-interface {v15}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    .line 512
    .local v12, "checkSum":Ljava/lang/String;
    const-string v2, "/"

    invoke-virtual {v7, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v31

    .line 513
    .local v31, "split":[Ljava/lang/String;
    move-object/from16 v0, v31

    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    aget-object v4, v31, v2

    .line 515
    .local v4, "fileName":Ljava/lang/String;
    new-instance v2, Lcom/sec/android/sCloudSync/Records/RecordORSItem;

    const-string v5, "checksum"

    invoke-direct {v2, v5, v12}, Lcom/sec/android/sCloudSync/Records/RecordORSItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 517
    const/16 v16, 0x0

    .line 518
    .local v16, "fileDesc":Landroid/os/ParcelFileDescriptor;
    const/16 v22, 0x0

    .line 519
    .local v22, "fin":Ljava/io/FileInputStream;
    const/4 v10, 0x0

    .line 522
    .local v10, "buffer":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->mbSyncCanceled:Z

    if-eqz v2, :cond_3

    .line 523
    new-instance v2, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/16 v5, 0x9

    invoke-direct {v2, v5}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 538
    :catch_0
    move-exception v14

    .line 539
    .local v14, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_1
    invoke-virtual {v14}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 540
    const/16 v24, 0x1

    .line 544
    if-eqz v16, :cond_0

    .line 545
    :try_start_2
    invoke-virtual/range {v16 .. v16}, Landroid/os/ParcelFileDescriptor;->close()V

    .line 546
    :cond_0
    if-eqz v22, :cond_1

    .line 547
    invoke-virtual/range {v22 .. v22}, Ljava/io/FileInputStream;->close()V

    .line 548
    :cond_1
    if-eqz v10, :cond_2

    .line 549
    invoke-virtual {v10}, Ljava/io/ByteArrayOutputStream;->flush()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4

    .line 580
    .end local v4    # "fileName":Ljava/lang/String;
    .end local v7    # "localFilepath":Ljava/lang/String;
    .end local v10    # "buffer":Ljava/io/ByteArrayOutputStream;
    .end local v12    # "checkSum":Ljava/lang/String;
    .end local v14    # "e":Ljava/lang/Exception;
    .end local v15    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v16    # "fileDesc":Landroid/os/ParcelFileDescriptor;
    .end local v22    # "fin":Ljava/io/FileInputStream;
    .end local v26    # "i$":Ljava/util/Iterator;
    .end local v31    # "split":[Ljava/lang/String;
    :cond_2
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->mContract:Lcom/sec/android/sCloudSync/Constants/SNoteContract;

    iget-object v2, v2, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->DELETESNB_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v5, "filename"

    move-object/from16 v0, v30

    invoke-virtual {v2, v5, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v5, "privatekey"

    move-object/from16 v0, v27

    invoke-virtual {v2, v5, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v13

    .line 585
    .local v13, "deleteSnbUri":Landroid/net/Uri;
    :try_start_3
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v2

    const/4 v5, 0x0

    invoke-virtual {v2, v13, v5}, Landroid/content/ContentProviderClient;->openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_0

    .line 586
    :catch_1
    move-exception v14

    .line 587
    .local v14, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v14}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto/16 :goto_0

    .line 525
    .end local v13    # "deleteSnbUri":Landroid/net/Uri;
    .end local v14    # "e":Ljava/io/FileNotFoundException;
    .restart local v4    # "fileName":Ljava/lang/String;
    .restart local v7    # "localFilepath":Ljava/lang/String;
    .restart local v10    # "buffer":Ljava/io/ByteArrayOutputStream;
    .restart local v12    # "checkSum":Ljava/lang/String;
    .restart local v15    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v16    # "fileDesc":Landroid/os/ParcelFileDescriptor;
    .restart local v22    # "fin":Ljava/io/FileInputStream;
    .restart local v26    # "i$":Ljava/util/Iterator;
    .restart local v31    # "split":[Ljava/lang/String;
    :cond_3
    :try_start_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->mContract:Lcom/sec/android/sCloudSync/Constants/SNoteContract;

    iget-object v2, v2, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->FILEDETAIL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v5, "filename"

    invoke-virtual {v2, v5, v7}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v5, "privatekey"

    move-object/from16 v0, v27

    invoke-virtual {v2, v5, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v20

    .line 529
    .local v20, "filedetailUri":Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v2

    const/4 v5, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v2, v0, v5}, Landroid/content/ContentProviderClient;->openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v16

    .line 530
    new-instance v23, Ljava/io/FileInputStream;

    invoke-virtual/range {v16 .. v16}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v2

    move-object/from16 v0, v23

    invoke-direct {v0, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 531
    .end local v22    # "fin":Ljava/io/FileInputStream;
    .local v23, "fin":Ljava/io/FileInputStream;
    :try_start_5
    new-instance v11, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v11}, Ljava/io/ByteArrayOutputStream;-><init>()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_6
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 533
    .end local v10    # "buffer":Ljava/io/ByteArrayOutputStream;
    .local v11, "buffer":Ljava/io/ByteArrayOutputStream;
    :goto_4
    :try_start_6
    sget-object v2, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->mData:[B

    const/4 v5, 0x0

    sget-object v6, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->mData:[B

    array-length v6, v6

    move-object/from16 v0, v23

    invoke-virtual {v0, v2, v5, v6}, Ljava/io/FileInputStream;->read([BII)I

    move-result v28

    .local v28, "nRead":I
    const/4 v2, -0x1

    move/from16 v0, v28

    if-eq v0, v2, :cond_4

    .line 534
    sget-object v2, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->mData:[B

    const/4 v5, 0x0

    move/from16 v0, v28

    invoke-virtual {v11, v2, v5, v0}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_4

    .line 538
    .end local v28    # "nRead":I
    :catch_2
    move-exception v14

    move-object v10, v11

    .end local v11    # "buffer":Ljava/io/ByteArrayOutputStream;
    .restart local v10    # "buffer":Ljava/io/ByteArrayOutputStream;
    move-object/from16 v22, v23

    .end local v23    # "fin":Ljava/io/FileInputStream;
    .restart local v22    # "fin":Ljava/io/FileInputStream;
    goto/16 :goto_2

    .line 536
    .end local v10    # "buffer":Ljava/io/ByteArrayOutputStream;
    .end local v22    # "fin":Ljava/io/FileInputStream;
    .restart local v11    # "buffer":Ljava/io/ByteArrayOutputStream;
    .restart local v23    # "fin":Ljava/io/FileInputStream;
    .restart local v28    # "nRead":I
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getCloudServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getFileServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/android/scloud/framework/util/TimeManager;->getCurrentTime(Landroid/content/Context;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "/storage/sdcard0/SNote/"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v27

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "/"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v8, 0x0

    invoke-virtual {v11}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v9

    invoke-virtual/range {v2 .. v9}, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->uploadFile(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z[B)Lcom/sec/android/sCloudSync/Records/ORSResponse;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    move-result-object v19

    .line 544
    if-eqz v16, :cond_5

    .line 545
    :try_start_7
    invoke-virtual/range {v16 .. v16}, Landroid/os/ParcelFileDescriptor;->close()V

    .line 546
    :cond_5
    if-eqz v23, :cond_6

    .line 547
    invoke-virtual/range {v23 .. v23}, Ljava/io/FileInputStream;->close()V

    .line 548
    :cond_6
    if-eqz v11, :cond_7

    .line 549
    invoke-virtual {v11}, Ljava/io/ByteArrayOutputStream;->flush()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    .line 556
    :cond_7
    :goto_5
    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getResponseCode()I

    move-result v2

    if-nez v2, :cond_b

    .line 558
    new-instance v32, Landroid/content/ContentValues;

    invoke-direct/range {v32 .. v32}, Landroid/content/ContentValues;-><init>()V

    .line 559
    .local v32, "values":Landroid/content/ContentValues;
    const-string v2, "dirty"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v32

    invoke-virtual {v0, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 561
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->filterSingleQuote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 562
    .local v21, "filtered":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "path = \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v32

    invoke-virtual {v2, v0, v1, v5, v6}, Landroid/content/ContentProviderClient;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 575
    .end local v21    # "filtered":Ljava/lang/String;
    .end local v32    # "values":Landroid/content/ContentValues;
    :goto_6
    invoke-interface {v3}, Ljava/util/List;->clear()V

    goto/16 :goto_1

    .line 550
    :catch_3
    move-exception v14

    .line 551
    .local v14, "e":Ljava/io/IOException;
    invoke-virtual {v14}, Ljava/io/IOException;->printStackTrace()V

    .line 552
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string v5, "IOException when closing/flushing data"

    invoke-static {v2, v5}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 550
    .end local v11    # "buffer":Ljava/io/ByteArrayOutputStream;
    .end local v20    # "filedetailUri":Landroid/net/Uri;
    .end local v23    # "fin":Ljava/io/FileInputStream;
    .end local v28    # "nRead":I
    .restart local v10    # "buffer":Ljava/io/ByteArrayOutputStream;
    .local v14, "e":Ljava/lang/Exception;
    .restart local v22    # "fin":Ljava/io/FileInputStream;
    :catch_4
    move-exception v14

    .line 551
    .local v14, "e":Ljava/io/IOException;
    invoke-virtual {v14}, Ljava/io/IOException;->printStackTrace()V

    .line 552
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string v5, "IOException when closing/flushing data"

    invoke-static {v2, v5}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 543
    .end local v14    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    .line 544
    :goto_7
    if-eqz v16, :cond_8

    .line 545
    :try_start_8
    invoke-virtual/range {v16 .. v16}, Landroid/os/ParcelFileDescriptor;->close()V

    .line 546
    :cond_8
    if-eqz v22, :cond_9

    .line 547
    invoke-virtual/range {v22 .. v22}, Ljava/io/FileInputStream;->close()V

    .line 548
    :cond_9
    if-eqz v10, :cond_a

    .line 549
    invoke-virtual {v10}, Ljava/io/ByteArrayOutputStream;->flush()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 553
    :cond_a
    :goto_8
    throw v2

    .line 550
    :catch_5
    move-exception v14

    .line 551
    .restart local v14    # "e":Ljava/io/IOException;
    invoke-virtual {v14}, Ljava/io/IOException;->printStackTrace()V

    .line 552
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v5

    const-string v6, "IOException when closing/flushing data"

    invoke-static {v5, v6}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_8

    .line 564
    .end local v10    # "buffer":Ljava/io/ByteArrayOutputStream;
    .end local v14    # "e":Ljava/io/IOException;
    .end local v22    # "fin":Ljava/io/FileInputStream;
    .restart local v11    # "buffer":Ljava/io/ByteArrayOutputStream;
    .restart local v20    # "filedetailUri":Landroid/net/Uri;
    .restart local v23    # "fin":Ljava/io/FileInputStream;
    .restart local v28    # "nRead":I
    :cond_b
    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getResponseCode()I

    move-result v2

    const/16 v5, 0x7d00

    if-ne v2, v5, :cond_c

    .line 565
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->createSNoteFolderStructure()Z

    .line 566
    const/16 v24, 0x1

    goto :goto_6

    .line 567
    :cond_c
    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getResponseCode()I

    move-result v2

    const/16 v5, 0x4e23

    if-eq v2, v5, :cond_d

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getResponseCode()I

    move-result v2

    const/16 v5, 0x4e24

    if-ne v2, v5, :cond_e

    .line 569
    :cond_d
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->notifyServerStoragefull()V

    .line 570
    const/16 v24, 0x1

    goto :goto_6

    .line 572
    :cond_e
    const/16 v24, 0x1

    .line 573
    goto/16 :goto_3

    .line 592
    .end local v3    # "recordORSItemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordORSItem;>;"
    .end local v4    # "fileName":Ljava/lang/String;
    .end local v7    # "localFilepath":Ljava/lang/String;
    .end local v11    # "buffer":Ljava/io/ByteArrayOutputStream;
    .end local v12    # "checkSum":Ljava/lang/String;
    .end local v15    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v16    # "fileDesc":Landroid/os/ParcelFileDescriptor;
    .end local v18    # "fileUploadMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v19    # "fileUploadResponse":Lcom/sec/android/sCloudSync/Records/ORSResponse;
    .end local v20    # "filedetailUri":Landroid/net/Uri;
    .end local v23    # "fin":Ljava/io/FileInputStream;
    .end local v26    # "i$":Ljava/util/Iterator;
    .end local v27    # "key":Ljava/lang/String;
    .end local v28    # "nRead":I
    .end local v29    # "setItem":Lcom/sec/android/sCloudSync/Records/RecordSetItem;
    .end local v30    # "snbFilePath":Ljava/lang/String;
    .end local v31    # "split":[Ljava/lang/String;
    :cond_f
    return v24

    .line 543
    .restart local v3    # "recordORSItemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordORSItem;>;"
    .restart local v4    # "fileName":Ljava/lang/String;
    .restart local v7    # "localFilepath":Ljava/lang/String;
    .restart local v10    # "buffer":Ljava/io/ByteArrayOutputStream;
    .restart local v12    # "checkSum":Ljava/lang/String;
    .restart local v15    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v16    # "fileDesc":Landroid/os/ParcelFileDescriptor;
    .restart local v18    # "fileUploadMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v19    # "fileUploadResponse":Lcom/sec/android/sCloudSync/Records/ORSResponse;
    .restart local v20    # "filedetailUri":Landroid/net/Uri;
    .restart local v23    # "fin":Ljava/io/FileInputStream;
    .restart local v26    # "i$":Ljava/util/Iterator;
    .restart local v27    # "key":Ljava/lang/String;
    .restart local v29    # "setItem":Lcom/sec/android/sCloudSync/Records/RecordSetItem;
    .restart local v30    # "snbFilePath":Ljava/lang/String;
    .restart local v31    # "split":[Ljava/lang/String;
    :catchall_1
    move-exception v2

    move-object/from16 v22, v23

    .end local v23    # "fin":Ljava/io/FileInputStream;
    .restart local v22    # "fin":Ljava/io/FileInputStream;
    goto :goto_7

    .end local v10    # "buffer":Ljava/io/ByteArrayOutputStream;
    .end local v22    # "fin":Ljava/io/FileInputStream;
    .restart local v11    # "buffer":Ljava/io/ByteArrayOutputStream;
    .restart local v23    # "fin":Ljava/io/FileInputStream;
    :catchall_2
    move-exception v2

    move-object v10, v11

    .end local v11    # "buffer":Ljava/io/ByteArrayOutputStream;
    .restart local v10    # "buffer":Ljava/io/ByteArrayOutputStream;
    move-object/from16 v22, v23

    .end local v23    # "fin":Ljava/io/FileInputStream;
    .restart local v22    # "fin":Ljava/io/FileInputStream;
    goto :goto_7

    .line 538
    .end local v22    # "fin":Ljava/io/FileInputStream;
    .restart local v23    # "fin":Ljava/io/FileInputStream;
    :catch_6
    move-exception v14

    move-object/from16 v22, v23

    .end local v23    # "fin":Ljava/io/FileInputStream;
    .restart local v22    # "fin":Ljava/io/FileInputStream;
    goto/16 :goto_2
.end method


# virtual methods
.method public cancelSync()V
    .locals 1

    .prologue
    .line 615
    invoke-super {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->cancelSync()V

    .line 616
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getCloudServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getFileServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->close()V

    .line 617
    return-void
.end method

.method protected checkUploadLimit(JJJ)Z
    .locals 1
    .param p1, "uploadsize"    # J
    .param p3, "totalSize"    # J
    .param p5, "currentCount"    # J

    .prologue
    .line 221
    cmp-long v0, p5, p3

    if-gez v0, :cond_0

    .line 222
    const/4 v0, 0x1

    .line 223
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public doUpdateDelete(Lcom/sec/android/sCloudSync/Records/KVSItem;Lcom/sec/android/sCloudSync/Records/RecordItem;Landroid/content/SyncStats;)Z
    .locals 10
    .param p1, "item"    # Lcom/sec/android/sCloudSync/Records/KVSItem;
    .param p2, "record"    # Lcom/sec/android/sCloudSync/Records/RecordItem;
    .param p3, "stats"    # Landroid/content/SyncStats;

    .prologue
    const-wide/16 v8, 0x1

    .line 369
    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->isDeleted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 370
    iget-wide v0, p3, Landroid/content/SyncStats;->numDeletes:J

    add-long/2addr v0, v8

    iput-wide v0, p3, Landroid/content/SyncStats;->numDeletes:J

    .line 372
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getID()J

    move-result-wide v2

    const/4 v4, 0x0

    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getKEY()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;->delete(Landroid/net/Uri;JLjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 385
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 373
    :catch_0
    move-exception v7

    .line 375
    .local v7, "e":Landroid/os/RemoteException;
    invoke-virtual {v7}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 378
    .end local v7    # "e":Landroid/os/RemoteException;
    :cond_0
    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getValue()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getTimeStamp()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getID()J

    move-result-wide v4

    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getKEY()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;->update(Ljava/lang/String;JJLjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 379
    :cond_1
    const-string v0, "SNoteSyncAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to update record with key:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getKEY()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 382
    :cond_2
    iget-wide v0, p3, Landroid/content/SyncStats;->numUpdates:J

    add-long/2addr v0, v8

    iput-wide v0, p3, Landroid/content/SyncStats;->numUpdates:J

    goto :goto_0
.end method

.method protected generateKey(Ljava/lang/Long;Landroid/database/Cursor;)Ljava/lang/String;
    .locals 4
    .param p1, "rowId"    # Ljava/lang/Long;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .line 265
    invoke-super {p0, p1, p2}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->generateKey(Ljava/lang/Long;Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    .line 268
    .local v1, "result":Ljava/lang/String;
    const-string v2, "path"

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    if-gez v2, :cond_1

    .line 281
    .end local v1    # "result":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v1

    .line 271
    .restart local v1    # "result":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getSyncAdapterName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "SNOTE"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 272
    const-string v2, "path"

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 273
    .local v0, "path":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 274
    const-string v2, "/tmemo/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "/TMemo/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 275
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "TMEMO2"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method protected getAccountName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 173
    const-string v0, "account_name"

    return-object v0
.end method

.method protected getAccountType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 178
    const-string v0, "account_type"

    return-object v0
.end method

.method protected getBuilder()Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;
    .locals 4

    .prologue
    .line 188
    new-instance v0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;-><init>(Landroid/content/ContentProviderClient;Landroid/accounts/Account;Landroid/content/Context;)V

    return-object v0
.end method

.method protected getCallerSyncAdapter()Ljava/lang/String;
    .locals 1

    .prologue
    .line 168
    sget-object v0, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->CALLER_IS_SYNCADAPTER:Ljava/lang/String;

    return-object v0
.end method

.method protected getCid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 183
    const-string v0, "a1QGNqwu27"

    return-object v0
.end method

.method protected getContentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->mContract:Lcom/sec/android/sCloudSync/Constants/SNoteContract;

    iget-object v0, v0, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->FILE_CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method protected getCtidKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1067
    const-string v0, "SN"

    return-object v0
.end method

.method protected getDeletedColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 143
    const-string v0, "deleted"

    return-object v0
.end method

.method protected getDirtyColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 163
    const-string v0, "dirty"

    return-object v0
.end method

.method protected getIdColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 158
    const-string v0, "_id"

    return-object v0
.end method

.method protected getKeyColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 148
    const-string v0, "sync1"

    return-object v0
.end method

.method protected getLocalUpdatesSelection()Ljava/lang/String;
    .locals 2

    .prologue
    .line 136
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getAccountName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v1

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getAccountType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v1

    iget-object v1, v1, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getDirtyColumnName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = 1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getProjection()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 193
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getIdColumnName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getKeyColumnName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getTimeStampColumnName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getDeletedColumnName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getDirtyColumnName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    return-object v0
.end method

.method protected getServerUpdates()Ljava/util/Map;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/sCloudSync/Records/KVSItem;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .line 229
    invoke-super {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getServerUpdates()Ljava/util/Map;

    move-result-object v6

    .line 230
    .local v6, "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/sec/android/sCloudSync/Records/KVSItem;>;"
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 232
    .local v5, "itemsToBeRemoved":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/sec/android/sCloudSync/Records/KVSItem;>;"
    if-eqz v6, :cond_0

    invoke-interface {v6}, Ljava/util/Map;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 259
    :cond_0
    return-object v6

    .line 233
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v7

    invoke-direct {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getAppSupportUri()Landroid/net/Uri;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/sCloudSync/Tools/AppTool;->getSupportedApplication(Landroid/content/ContentProviderClient;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 234
    .local v0, "adapterName":Ljava/lang/String;
    if-eqz v0, :cond_3

    .line 235
    const-string v7, "TMEMO2"

    invoke-virtual {v0, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 243
    invoke-interface {v6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 245
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/sec/android/sCloudSync/Records/KVSItem;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    const-string v8, "_TMEMO2"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 246
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    invoke-interface {v5, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 252
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/sec/android/sCloudSync/Records/KVSItem;>;"
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v5}, Ljava/util/Map;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_0

    .line 254
    invoke-interface {v5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    .line 255
    .local v4, "items":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .restart local v2    # "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 256
    .local v3, "item":Ljava/lang/String;
    invoke-interface {v6, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method protected getSyncAdapterName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    const-string v0, "SNOTE"

    return-object v0
.end method

.method protected getSyncStateDataColumn()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1072
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getSyncStateURI()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1077
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    const-string v0, "SNoteSyncAdapter"

    return-object v0
.end method

.method protected getTimeStampColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 153
    const-string v0, "sync2"

    return-object v0
.end method

.method protected handleDeleteList(Ljava/util/List;Ljava/util/List;)Z
    .locals 25
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/sCloudSync/Records/RecordBase;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/sCloudSync/Records/RecordSetItem;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;,
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 404
    .local p1, "deleteList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordBase;>;"
    .local p2, "setList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordSetItem;>;"
    const/4 v13, 0x0

    .line 405
    .local v13, "fileDeleteResponse":Lcom/sec/android/sCloudSync/Records/ORSResponse;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->mContract:Lcom/sec/android/sCloudSync/Constants/SNoteContract;

    iget-object v1, v1, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->FILEDETAIL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getCallerSyncAdapter()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    .line 407
    .local v2, "fileDetailUri":Landroid/net/Uri;
    const/16 v16, 0x0

    .line 408
    .local v16, "hasDeleteFailed":Z
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    check-cast v1, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;

    invoke-virtual {v1}, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->getServerFileDeleteList()Ljava/util/List;

    move-result-object v12

    .line 413
    .local v12, "fileDeleteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz p1, :cond_6

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    .line 415
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    .line 416
    .local v24, "where":Ljava/lang/StringBuilder;
    const/4 v1, 0x1

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v4, "path"

    aput-object v4, v3, v1

    .line 417
    .local v3, "projection":[Ljava/lang/String;
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v17

    .local v17, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/sCloudSync/Records/RecordBase;

    .line 418
    .local v9, "deleteItem":Lcom/sec/android/sCloudSync/Records/RecordBase;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->mLocalChangedRecords:Ljava/util/Map;

    invoke-virtual {v9}, Lcom/sec/android/sCloudSync/Records/RecordBase;->getKEY()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Long;

    .line 419
    .local v19, "id":Ljava/lang/Long;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "snb_id = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 421
    .local v7, "cursor":Landroid/database/Cursor;
    if-eqz v7, :cond_4

    .line 422
    const/16 v21, 0x0

    .line 423
    .local v21, "lengthOffset":I
    const/4 v1, 0x0

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 424
    const-string v1, "path"

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " IN ("

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 427
    :goto_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 428
    const-string v1, "path"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 429
    .local v10, "deletePath":Ljava/lang/String;
    const-string v1, "/"

    invoke-virtual {v10, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v23

    .line 430
    .local v23, "split":[Ljava/lang/String;
    move-object/from16 v0, v23

    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-object v14, v23, v1

    .line 432
    .local v14, "fileName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->mbSyncCanceled:Z

    if-eqz v1, :cond_0

    .line 433
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 434
    new-instance v1, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/16 v4, 0x9

    invoke-direct {v1, v4}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v1

    .line 438
    :cond_0
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getCloudServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getFileServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "/storage/sdcard0/SNote/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v9}, Lcom/sec/android/sCloudSync/Records/RecordBase;->getKEY()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->deleteFile(Ljava/lang/String;)Lcom/sec/android/sCloudSync/Records/ORSResponse;
    :try_end_0
    .catch Lcom/sec/android/sCloudSync/Util/SyncException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v13

    .line 443
    invoke-virtual {v13}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getResponseCode()I

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v13}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getResponseCode()I

    move-result v1

    const/16 v4, 0x7d02

    if-eq v1, v4, :cond_1

    invoke-virtual {v13}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getResponseCode()I

    move-result v1

    const/16 v4, 0x7d06

    if-ne v1, v4, :cond_2

    .line 445
    :cond_1
    const/16 v1, 0x27

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v4, 0x27

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v4, 0x2c

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 446
    const/16 v21, 0x1

    goto/16 :goto_1

    .line 439
    :catch_0
    move-exception v11

    .line 440
    .local v11, "e":Lcom/sec/android/sCloudSync/Util/SyncException;
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 441
    throw v11

    .line 450
    .end local v11    # "e":Lcom/sec/android/sCloudSync/Util/SyncException;
    :cond_2
    const/16 v16, 0x1

    .line 454
    .end local v10    # "deletePath":Ljava/lang/String;
    .end local v14    # "fileName":Ljava/lang/String;
    .end local v23    # "split":[Ljava/lang/String;
    :cond_3
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 455
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    sub-int v1, v1, v21

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 456
    const/16 v1, 0x29

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 459
    .end local v21    # "lengthOffset":I
    :cond_4
    if-eqz v16, :cond_5

    .line 460
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v1

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/ContentProviderClient;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_0

    .line 462
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "snb_id = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/ContentProviderClient;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_0

    .line 465
    .end local v3    # "projection":[Ljava/lang/String;
    .end local v7    # "cursor":Landroid/database/Cursor;
    .end local v9    # "deleteItem":Lcom/sec/android/sCloudSync/Records/RecordBase;
    .end local v17    # "i$":Ljava/util/Iterator;
    .end local v19    # "id":Ljava/lang/Long;
    .end local v24    # "where":Ljava/lang/StringBuilder;
    :cond_6
    if-eqz p2, :cond_b

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_b

    if-eqz v12, :cond_b

    .line 466
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :cond_7
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/sec/android/sCloudSync/Records/RecordSetItem;

    .line 467
    .local v22, "setItem":Lcom/sec/android/sCloudSync/Records/RecordSetItem;
    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/sCloudSync/Records/RecordSetItem;->getKEY()Ljava/lang/String;

    move-result-object v20

    .line 468
    .local v20, "key":Ljava/lang/String;
    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    .local v18, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 469
    .local v8, "deleteFile":Ljava/lang/String;
    const-string v1, "/"

    invoke-virtual {v8, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v23

    .line 470
    .restart local v23    # "split":[Ljava/lang/String;
    move-object/from16 v0, v23

    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-object v14, v23, v1

    .line 471
    .restart local v14    # "fileName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->mbSyncCanceled:Z

    if-eqz v1, :cond_8

    .line 472
    new-instance v1, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/16 v4, 0x9

    invoke-direct {v1, v4}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v1

    .line 474
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getCloudServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getFileServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "/storage/sdcard0/SNote/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->deleteFile(Ljava/lang/String;)Lcom/sec/android/sCloudSync/Records/ORSResponse;

    move-result-object v13

    .line 475
    invoke-virtual {v13}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getResponseCode()I

    move-result v1

    if-eqz v1, :cond_9

    invoke-virtual {v13}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getResponseCode()I

    move-result v1

    const/16 v4, 0x7d02

    if-eq v1, v4, :cond_9

    invoke-virtual {v13}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getResponseCode()I

    move-result v1

    const/16 v4, 0x7d06

    if-ne v1, v4, :cond_a

    .line 477
    :cond_9
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->filterSingleQuote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 478
    .local v15, "filtered":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "path = \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/ContentProviderClient;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_2

    .line 480
    .end local v15    # "filtered":Ljava/lang/String;
    :cond_a
    const/16 v16, 0x1

    goto/16 :goto_2

    .line 485
    .end local v8    # "deleteFile":Ljava/lang/String;
    .end local v14    # "fileName":Ljava/lang/String;
    .end local v18    # "i$":Ljava/util/Iterator;
    .end local v20    # "key":Ljava/lang/String;
    .end local v22    # "setItem":Lcom/sec/android/sCloudSync/Records/RecordSetItem;
    .end local v23    # "split":[Ljava/lang/String;
    :cond_b
    return v16
.end method

.method protected prepareSync()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .line 201
    invoke-direct {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->clearGarbage()Z

    move-result v0

    return v0
.end method

.method protected removePreSyncedRecords(Ljava/util/Map;)Z
    .locals 26
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/sCloudSync/Records/KVSItem;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 288
    .local p1, "uServerRecords":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/sec/android/sCloudSync/Records/KVSItem;>;"
    if-eqz p1, :cond_0

    invoke-interface/range {p1 .. p1}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 289
    :cond_0
    const/4 v2, 0x0

    .line 364
    :goto_0
    return v2

    .line 291
    :cond_1
    const/16 v22, 0x0

    .line 292
    .local v22, "preCursor":Landroid/database/Cursor;
    invoke-interface/range {p1 .. p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v19

    .line 293
    .local v19, "keyset":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-interface/range {v19 .. v19}, Ljava/util/Set;->size()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    mul-int/lit16 v2, v2, 0x104

    move-object/from16 v0, v25

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 294
    .local v25, "where":Ljava/lang/StringBuilder;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getKeyColumnName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " IN ("

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 295
    move-object/from16 v0, v25

    move-object/from16 v1, v19

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Tools/AppTool;->appendIds(Ljava/lang/StringBuilder;Ljava/util/Set;)V

    .line 296
    const/16 v2, 0x29

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 298
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getContentUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getCallerSyncAdapter()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getAccountName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v5

    iget-object v5, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getAccountType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v5

    iget-object v5, v5, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 303
    .local v3, "uriFromSyncAdapter":Landroid/net/Uri;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getProjection()[Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v22

    .line 309
    if-nez v22, :cond_2

    .line 310
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string v4, "RemovePreSyncedRecords: (ERROR)Cursor is null"

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 311
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 304
    :catch_0
    move-exception v13

    .line 305
    .local v13, "e":Landroid/os/RemoteException;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "RemovePreSyncedRecords: Exception in calling query "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v13}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 314
    .end local v13    # "e":Landroid/os/RemoteException;
    :cond_2
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-nez v2, :cond_3

    .line 315
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    .line 316
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string v4, "RemovePreSyncedRecords: there is nothing to be removed."

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 363
    :goto_1
    invoke-super/range {p0 .. p1}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->removePreSyncedRecords(Ljava/util/Map;)Z

    .line 364
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 319
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getIdColumnName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    .line 320
    .local v16, "id_index":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getKeyColumnName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    .line 321
    .local v18, "key_index":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getTimeStampColumnName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v24

    .line 322
    .local v24, "time_index":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getDirtyColumnName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    .line 323
    .local v12, "dirtyIndex":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getDeletedColumnName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 326
    .local v10, "deletedIndex":I
    :cond_4
    move-object/from16 v0, v22

    move/from16 v1, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    .line 327
    .local v15, "id":I
    move-object/from16 v0, v22

    move/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    .line 328
    .local v20, "modifiedTimeUploadedtoServer":J
    move-object/from16 v0, v22

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 329
    .local v17, "key":Ljava/lang/String;
    move-object/from16 v0, v22

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 330
    .local v11, "dirtyField":I
    move-object/from16 v0, v22

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 332
    .local v9, "deletedField":I
    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Lcom/sec/android/sCloudSync/Records/KVSItem;

    .line 333
    .local v23, "serverItem":Lcom/sec/android/sCloudSync/Records/KVSItem;
    if-eqz v23, :cond_7

    .line 334
    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getTimeStamp()J

    move-result-wide v4

    cmp-long v2, v20, v4

    if-ltz v2, :cond_6

    .line 335
    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/sCloudSync/Records/KVSItem;->isDeleted()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 338
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->mContract:Lcom/sec/android/sCloudSync/Constants/SNoteContract;

    iget-object v2, v2, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->FILEDETAIL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getCallerSyncAdapter()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v14

    .line 340
    .local v14, "fileDetailUri":Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v4, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->DIRTY_VALUES:Landroid/content/ContentValues;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "snb_id = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v2, v14, v4, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v8

    .line 342
    .local v8, "count":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "UPDATE Detail DIRTY, id : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    .end local v8    # "count":I
    .end local v14    # "fileDetailUri":Landroid/net/Uri;
    :cond_5
    if-nez v11, :cond_6

    const/4 v2, 0x1

    if-ne v9, v2, :cond_6

    .line 346
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getKeyColumnName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentProviderClient;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 347
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->mContract:Lcom/sec/android/sCloudSync/Constants/SNoteContract;

    iget-object v2, v2, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->FILEDETAIL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getCallerSyncAdapter()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v14

    .line 349
    .restart local v14    # "fileDetailUri":Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "snb_id=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2, v14, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 359
    .end local v14    # "fileDetailUri":Landroid/net/Uri;
    :cond_6
    :goto_2
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_4

    .line 361
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    .line 350
    :catch_1
    move-exception v13

    .line 351
    .restart local v13    # "e":Landroid/os/RemoteException;
    invoke-virtual {v13}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_2

    .line 357
    .end local v13    # "e":Landroid/os/RemoteException;
    :cond_7
    const-string v2, "SNoteSyncAdapter"

    const-string v4, "Sync Broken : There is a duplicated record on Local."

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public updateLocalDb(Landroid/content/SyncStats;)V
    .locals 65
    .param p1, "stats"    # Landroid/content/SyncStats;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .line 623
    invoke-super/range {p0 .. p1}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->updateLocalDb(Landroid/content/SyncStats;)V

    .line 624
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v2

    const-string v4, "mounted"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 625
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->resetLastSyncTime()V

    .line 626
    new-instance v2, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/16 v4, 0x8

    invoke-direct {v2, v4}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v2

    .line 629
    :cond_0
    const/16 v26, 0x0

    .line 630
    .local v26, "fileDownResponse":Lcom/sec/android/sCloudSync/Records/ORSResponse;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    check-cast v2, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;

    invoke-virtual {v2}, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->getFileDownloadList()Ljava/util/Map;

    move-result-object v27

    .line 631
    .local v27, "fileDownloadList":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    check-cast v2, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;

    invoke-virtual {v2}, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->getSNBFilePathDownloadList()Ljava/util/Map;

    move-result-object v57

    .line 632
    .local v57, "snbFileMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    check-cast v2, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;

    invoke-virtual {v2}, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->getjsonString()Ljava/util/Map;

    move-result-object v47

    .line 633
    .local v47, "jsonStringMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface/range {v27 .. v27}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v49

    .line 634
    .local v49, "keySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v24, Ljava/util/HashSet;

    invoke-direct/range {v24 .. v24}, Ljava/util/HashSet;-><init>()V

    .line 635
    .local v24, "failedDownloads":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v22, Ljava/util/HashSet;

    invoke-direct/range {v22 .. v22}, Ljava/util/HashSet;-><init>()V

    .line 639
    .local v22, "failDownloads":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->mContract:Lcom/sec/android/sCloudSync/Constants/SNoteContract;

    iget-object v2, v2, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->FILEDETAIL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getCallerSyncAdapter()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 640
    .local v3, "fileDetailUri":Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getContentUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getCallerSyncAdapter()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getAccountName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v5

    iget-object v5, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getAccountType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v5

    iget-object v5, v5, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v52

    .line 645
    .local v52, "noteUri":Landroid/net/Uri;
    invoke-interface/range {v49 .. v49}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v37

    :cond_1
    :goto_0
    invoke-interface/range {v37 .. v37}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_26

    invoke-interface/range {v37 .. v37}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v48

    check-cast v48, Ljava/lang/String;

    .line 646
    .local v48, "key":Ljava/lang/String;
    const/16 v35, 0x0

    .line 647
    .local v35, "hasDownloadFailed":Z
    move-object/from16 v0, v27

    move-object/from16 v1, v48

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Ljava/util/List;

    .line 648
    .local v28, "fileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->mServerChangedRecords:Ljava/util/Map;

    move-object/from16 v0, v48

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v45

    check-cast v45, Lcom/sec/android/sCloudSync/Records/KVSItem;

    .line 649
    .local v45, "item":Lcom/sec/android/sCloudSync/Records/KVSItem;
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 652
    .local v13, "delFileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual/range {v45 .. v45}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getID()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-lez v2, :cond_2

    invoke-virtual/range {v45 .. v45}, Lcom/sec/android/sCloudSync/Records/KVSItem;->isDeleted()Z

    move-result v2

    if-nez v2, :cond_2

    .line 653
    move-object/from16 v0, v57

    move-object/from16 v1, v48

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v58

    check-cast v58, Ljava/lang/String;

    .line 654
    .local v58, "snbFilePath":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->mContract:Lcom/sec/android/sCloudSync/Constants/SNoteContract;

    iget-object v2, v2, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->EXTRACTSNB_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "filename"

    move-object/from16 v0, v58

    invoke-virtual {v2, v4, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "privatekey"

    move-object/from16 v0, v48

    invoke-virtual {v2, v4, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v20

    .line 658
    .local v20, "extractSnbUri":Landroid/net/Uri;
    if-eqz v58, :cond_2

    .line 660
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v2

    const/4 v4, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v2, v0, v4}, Landroid/content/ContentProviderClient;->openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    .line 670
    .end local v20    # "extractSnbUri":Landroid/net/Uri;
    .end local v58    # "snbFilePath":Ljava/lang/String;
    :cond_2
    :goto_1
    const/4 v12, 0x0

    .line 671
    .local v12, "cursor":Landroid/database/Cursor;
    invoke-virtual/range {v45 .. v45}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getID()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-lez v2, :cond_3

    invoke-virtual/range {v45 .. v45}, Lcom/sec/android/sCloudSync/Records/KVSItem;->isDeleted()Z

    move-result v2

    if-nez v2, :cond_3

    .line 673
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v2

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "path"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "checksum"

    aput-object v6, v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "snb_id = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v45 .. v45}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getID()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v12

    .line 683
    :cond_3
    :goto_2
    if-eqz v12, :cond_8

    .line 684
    :cond_4
    :goto_3
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 685
    const-string v2, "path"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    .line 687
    .local v21, "fPath":Ljava/lang/String;
    const-string v2, "checksum"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v50

    .line 689
    .local v50, "localCheckSum":Ljava/lang/String;
    move-object/from16 v0, v28

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 690
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->mbSyncCanceled:Z

    if-eqz v2, :cond_5

    .line 691
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 692
    new-instance v2, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/16 v4, 0x9

    invoke-direct {v2, v4}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v2

    .line 661
    .end local v12    # "cursor":Landroid/database/Cursor;
    .end local v21    # "fPath":Ljava/lang/String;
    .end local v50    # "localCheckSum":Ljava/lang/String;
    .restart local v20    # "extractSnbUri":Landroid/net/Uri;
    .restart local v58    # "snbFilePath":Ljava/lang/String;
    :catch_0
    move-exception v18

    .line 662
    .local v18, "e":Landroid/os/RemoteException;
    invoke-virtual/range {v18 .. v18}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 663
    .end local v18    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v18

    .line 664
    .local v18, "e":Ljava/io/FileNotFoundException;
    invoke-virtual/range {v18 .. v18}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_1

    .line 675
    .end local v18    # "e":Ljava/io/FileNotFoundException;
    .end local v20    # "extractSnbUri":Landroid/net/Uri;
    .end local v58    # "snbFilePath":Ljava/lang/String;
    .restart local v12    # "cursor":Landroid/database/Cursor;
    :catch_2
    move-exception v19

    .line 676
    .local v19, "e1":Landroid/os/RemoteException;
    invoke-virtual/range {v19 .. v19}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_2

    .line 694
    .end local v19    # "e1":Landroid/os/RemoteException;
    .restart local v21    # "fPath":Ljava/lang/String;
    .restart local v50    # "localCheckSum":Ljava/lang/String;
    :cond_5
    const-string v2, "/"

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v59

    .line 695
    .local v59, "split":[Ljava/lang/String;
    move-object/from16 v0, v59

    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    aget-object v29, v59, v2

    .line 697
    .local v29, "fileName":Ljava/lang/String;
    :try_start_2
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getCloudServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getFileServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "/storage/sdcard0/SNote/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v48

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v29

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->getMetadata(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/sCloudSync/Records/ORSResponse;
    :try_end_2
    .catch Lcom/sec/android/sCloudSync/Util/SyncException; {:try_start_2 .. :try_end_2} :catch_3

    move-result-object v34

    .line 702
    .local v34, "getMetadataResponse":Lcom/sec/android/sCloudSync/Records/ORSResponse;
    invoke-virtual/range {v34 .. v34}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getResponseCode()I

    move-result v55

    .line 703
    .local v55, "ret_code":I
    if-nez v55, :cond_4

    .line 704
    invoke-virtual/range {v34 .. v34}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getORSResponse()Lcom/sec/android/sCloudSync/Records/RecordORSItem;

    move-result-object v51

    .line 705
    .local v51, "metadataRecords":Lcom/sec/android/sCloudSync/Records/RecordORSItem;
    if-eqz v51, :cond_4

    .line 707
    invoke-virtual/range {v51 .. v51}, Lcom/sec/android/sCloudSync/Records/RecordORSItem;->getValue()Ljava/lang/String;

    move-result-object v56

    .line 709
    .local v56, "serverCheckSum":Ljava/lang/String;
    move-object/from16 v0, v56

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 710
    move-object/from16 v0, v28

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 698
    .end local v34    # "getMetadataResponse":Lcom/sec/android/sCloudSync/Records/ORSResponse;
    .end local v51    # "metadataRecords":Lcom/sec/android/sCloudSync/Records/RecordORSItem;
    .end local v55    # "ret_code":I
    .end local v56    # "serverCheckSum":Ljava/lang/String;
    :catch_3
    move-exception v18

    .line 699
    .local v18, "e":Lcom/sec/android/sCloudSync/Util/SyncException;
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 700
    throw v18

    .line 714
    .end local v18    # "e":Lcom/sec/android/sCloudSync/Util/SyncException;
    .end local v29    # "fileName":Ljava/lang/String;
    .end local v59    # "split":[Ljava/lang/String;
    :cond_6
    move-object/from16 v0, v21

    invoke-interface {v13, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 717
    .end local v21    # "fPath":Ljava/lang/String;
    .end local v50    # "localCheckSum":Ljava/lang/String;
    :cond_7
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 720
    :cond_8
    invoke-interface/range {v28 .. v28}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v38

    .local v38, "i$":Ljava/util/Iterator;
    :cond_9
    :goto_4
    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Ljava/lang/String;

    .line 721
    .local v30, "filePath":Ljava/lang/String;
    const-string v2, "/"

    move-object/from16 v0, v30

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v59

    .line 722
    .restart local v59    # "split":[Ljava/lang/String;
    move-object/from16 v0, v59

    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    aget-object v29, v59, v2

    .line 724
    .restart local v29    # "fileName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->mbSyncCanceled:Z

    if-eqz v2, :cond_a

    .line 725
    new-instance v2, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/16 v4, 0x9

    invoke-direct {v2, v4}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v2

    .line 728
    :cond_a
    :try_start_3
    invoke-virtual/range {v45 .. v45}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getID()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-lez v2, :cond_11

    .line 729
    invoke-virtual/range {v45 .. v45}, Lcom/sec/android/sCloudSync/Records/KVSItem;->isDeleted()Z

    move-result v2

    if-nez v2, :cond_b

    .line 730
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getCloudServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getFileServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "/storage/sdcard0/SNote/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v48

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v29

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->TEMP_FOLDER:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v29

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v2, v4, v5, v6}, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->downloadFile(Ljava/lang/String;Ljava/lang/String;Z)Lcom/sec/android/sCloudSync/Records/ORSResponse;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    move-result-object v26

    .line 742
    :cond_b
    :goto_5
    if-nez v26, :cond_12

    .line 743
    move-object/from16 v0, v24

    move-object/from16 v1, v48

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 744
    const/16 v35, 0x1

    .line 745
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string v4, "file download response is null "

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 813
    .end local v29    # "fileName":Ljava/lang/String;
    .end local v30    # "filePath":Ljava/lang/String;
    .end local v59    # "split":[Ljava/lang/String;
    :cond_c
    :goto_6
    invoke-virtual/range {v45 .. v45}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getID()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-lez v2, :cond_23

    invoke-virtual/range {v45 .. v45}, Lcom/sec/android/sCloudSync/Records/KVSItem;->isDeleted()Z

    move-result v2

    if-nez v2, :cond_23

    .line 814
    if-nez v35, :cond_21

    .line 816
    invoke-interface/range {v28 .. v28}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v38

    :cond_d
    :goto_7
    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_21

    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Ljava/lang/String;

    .line 817
    .restart local v30    # "filePath":Ljava/lang/String;
    const-string v2, "/"

    move-object/from16 v0, v30

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v59

    .line 818
    .restart local v59    # "split":[Ljava/lang/String;
    move-object/from16 v0, v59

    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    aget-object v29, v59, v2

    .line 819
    .restart local v29    # "fileName":Ljava/lang/String;
    new-instance v60, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->TEMP_FOLDER:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v29

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v60

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 820
    .local v60, "tempFile":Ljava/io/File;
    const/16 v41, 0x0

    .line 821
    .local v41, "inpStream":Ljava/io/InputStream;
    const/16 v25, 0x0

    .line 822
    .local v25, "fileDesc":Landroid/os/ParcelFileDescriptor;
    const/16 v32, 0x0

    .line 824
    .local v32, "fos":Ljava/io/FileOutputStream;
    :try_start_4
    new-instance v42, Ljava/io/FileInputStream;

    move-object/from16 v0, v42

    move-object/from16 v1, v60

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_f
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 825
    .end local v41    # "inpStream":Ljava/io/InputStream;
    .local v42, "inpStream":Ljava/io/InputStream;
    :try_start_5
    invoke-static/range {v42 .. v42}, Lcom/sec/android/sCloudSync/Tools/FileTool;->getByteArr(Ljava/io/InputStream;)[B

    move-result-object v10

    .line 831
    .local v10, "buffer":[B
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->mContract:Lcom/sec/android/sCloudSync/Constants/SNoteContract;

    iget-object v2, v2, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->DELETEDETAIL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "filename"

    move-object/from16 v0, v30

    invoke-virtual {v2, v4, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "privatekey"

    move-object/from16 v0, v48

    invoke-virtual {v2, v4, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v14

    .line 836
    .local v14, "deleteFileDetailUri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->mContract:Lcom/sec/android/sCloudSync/Constants/SNoteContract;

    iget-object v2, v2, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->FILEDETAIL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "filename"

    move-object/from16 v0, v30

    invoke-virtual {v2, v4, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "privatekey"

    move-object/from16 v0, v48

    invoke-virtual {v2, v4, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v31

    .line 841
    .local v31, "filedetailUri":Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, v14, v4}, Landroid/content/ContentProviderClient;->openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    .line 842
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v2

    const/4 v4, 0x0

    move-object/from16 v0, v31

    invoke-virtual {v2, v0, v4}, Landroid/content/ContentProviderClient;->openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v25

    .line 843
    new-instance v33, Ljava/io/FileOutputStream;

    invoke-virtual/range {v25 .. v25}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v2

    move-object/from16 v0, v33

    invoke-direct {v0, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1a
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 844
    .end local v32    # "fos":Ljava/io/FileOutputStream;
    .local v33, "fos":Ljava/io/FileOutputStream;
    :try_start_6
    move-object/from16 v0, v33

    invoke-virtual {v0, v10}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1b
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 849
    if-eqz v25, :cond_e

    .line 850
    :try_start_7
    invoke-virtual/range {v25 .. v25}, Landroid/os/ParcelFileDescriptor;->close()V

    .line 851
    :cond_e
    if-eqz v42, :cond_f

    .line 852
    invoke-virtual/range {v42 .. v42}, Ljava/io/InputStream;->close()V

    .line 853
    :cond_f
    if-eqz v33, :cond_10

    .line 854
    invoke-virtual/range {v33 .. v33}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_e

    :cond_10
    move-object/from16 v32, v33

    .end local v33    # "fos":Ljava/io/FileOutputStream;
    .restart local v32    # "fos":Ljava/io/FileOutputStream;
    move-object/from16 v41, v42

    .line 858
    .end local v42    # "inpStream":Ljava/io/InputStream;
    .restart local v41    # "inpStream":Ljava/io/InputStream;
    goto/16 :goto_7

    .line 733
    .end local v10    # "buffer":[B
    .end local v14    # "deleteFileDetailUri":Landroid/net/Uri;
    .end local v25    # "fileDesc":Landroid/os/ParcelFileDescriptor;
    .end local v31    # "filedetailUri":Landroid/net/Uri;
    .end local v32    # "fos":Ljava/io/FileOutputStream;
    .end local v41    # "inpStream":Ljava/io/InputStream;
    .end local v60    # "tempFile":Ljava/io/File;
    :cond_11
    :try_start_8
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getCloudServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getFileServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "/storage/sdcard0/SNote/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v48

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v29

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    move-object/from16 v0, v30

    invoke-virtual {v2, v4, v0, v5}, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->downloadFile(Ljava/lang/String;Ljava/lang/String;Z)Lcom/sec/android/sCloudSync/Records/ORSResponse;
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    move-result-object v26

    goto/16 :goto_5

    .line 735
    :catch_4
    move-exception v18

    .line 736
    .local v18, "e":Ljava/io/IOException;
    invoke-virtual/range {v18 .. v18}, Ljava/io/IOException;->printStackTrace()V

    .line 737
    move-object/from16 v0, v24

    move-object/from16 v1, v48

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 738
    const/16 v35, 0x1

    .line 739
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string v4, "IOexception when trying to download file"

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 748
    .end local v18    # "e":Ljava/io/IOException;
    :cond_12
    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getResponseCode()I

    move-result v2

    if-eqz v2, :cond_13

    .line 749
    move-object/from16 v0, v24

    move-object/from16 v1, v48

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 750
    const/16 v35, 0x1

    .line 751
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "failed to download snote file. Rcode is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getResponseCode()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 752
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "file name = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v29

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 755
    :cond_13
    invoke-virtual/range {v45 .. v45}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getID()J

    move-result-wide v4

    const-wide/16 v6, -0x1

    cmp-long v2, v4, v6

    if-nez v2, :cond_9

    .line 761
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->mContract:Lcom/sec/android/sCloudSync/Constants/SNoteContract;

    iget-object v2, v2, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->DELETEDETAIL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "filename"

    move-object/from16 v0, v30

    invoke-virtual {v2, v4, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "privatekey"

    move-object/from16 v0, v48

    invoke-virtual {v2, v4, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v14

    .line 766
    .restart local v14    # "deleteFileDetailUri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->mContract:Lcom/sec/android/sCloudSync/Constants/SNoteContract;

    iget-object v2, v2, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->FILEDETAIL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "filename"

    move-object/from16 v0, v30

    invoke-virtual {v2, v4, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "privatekey"

    move-object/from16 v0, v48

    invoke-virtual {v2, v4, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v31

    .line 770
    .restart local v31    # "filedetailUri":Landroid/net/Uri;
    const/16 v25, 0x0

    .line 771
    .restart local v25    # "fileDesc":Landroid/os/ParcelFileDescriptor;
    const/16 v32, 0x0

    .line 773
    .restart local v32    # "fos":Ljava/io/FileOutputStream;
    :try_start_9
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, v14, v4}, Landroid/content/ContentProviderClient;->openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    .line 774
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v2

    const/4 v4, 0x0

    move-object/from16 v0, v31

    invoke-virtual {v2, v0, v4}, Landroid/content/ContentProviderClient;->openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v25

    .line 775
    if-nez v25, :cond_15

    .line 776
    move-object/from16 v0, v24

    move-object/from16 v1, v48

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_9} :catch_7
    .catch Ljava/io/FileNotFoundException; {:try_start_9 .. :try_end_9} :catch_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_b
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 777
    const/16 v35, 0x1

    .line 800
    if-eqz v25, :cond_14

    .line 801
    :try_start_a
    invoke-virtual/range {v25 .. v25}, Landroid/os/ParcelFileDescriptor;->close()V

    .line 802
    :cond_14
    if-eqz v32, :cond_c

    .line 803
    invoke-virtual/range {v32 .. v32}, Ljava/io/FileOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5

    goto/16 :goto_6

    .line 804
    :catch_5
    move-exception v18

    .line 805
    .restart local v18    # "e":Ljava/io/IOException;
    invoke-virtual/range {v18 .. v18}, Ljava/io/IOException;->printStackTrace()V

    .line 806
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string v4, "IOException when closing ParcelFileDescriptor"

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 780
    .end local v18    # "e":Ljava/io/IOException;
    :cond_15
    :try_start_b
    new-instance v33, Ljava/io/FileOutputStream;

    invoke-virtual/range {v25 .. v25}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v2

    move-object/from16 v0, v33

    invoke-direct {v0, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_b} :catch_7
    .catch Ljava/io/FileNotFoundException; {:try_start_b .. :try_end_b} :catch_9
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 781
    .end local v32    # "fos":Ljava/io/FileOutputStream;
    .restart local v33    # "fos":Ljava/io/FileOutputStream;
    :try_start_c
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getCloudServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getFileServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->getBuffer()[B

    move-result-object v2

    move-object/from16 v0, v33

    invoke-virtual {v0, v2}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_c
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_c} :catch_1e
    .catch Ljava/io/FileNotFoundException; {:try_start_c .. :try_end_c} :catch_1d
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_1c
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    .line 800
    if-eqz v25, :cond_16

    .line 801
    :try_start_d
    invoke-virtual/range {v25 .. v25}, Landroid/os/ParcelFileDescriptor;->close()V

    .line 802
    :cond_16
    if-eqz v33, :cond_9

    .line 803
    invoke-virtual/range {v33 .. v33}, Ljava/io/FileOutputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_6

    goto/16 :goto_4

    .line 804
    :catch_6
    move-exception v18

    .line 805
    .restart local v18    # "e":Ljava/io/IOException;
    invoke-virtual/range {v18 .. v18}, Ljava/io/IOException;->printStackTrace()V

    .line 806
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string v4, "IOException when closing ParcelFileDescriptor"

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 783
    .end local v18    # "e":Ljava/io/IOException;
    .end local v33    # "fos":Ljava/io/FileOutputStream;
    .restart local v32    # "fos":Ljava/io/FileOutputStream;
    :catch_7
    move-exception v18

    .line 784
    .local v18, "e":Landroid/os/RemoteException;
    :goto_8
    :try_start_e
    invoke-virtual/range {v18 .. v18}, Landroid/os/RemoteException;->printStackTrace()V

    .line 785
    move-object/from16 v0, v24

    move-object/from16 v1, v48

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    .line 786
    const/16 v35, 0x1

    .line 800
    if-eqz v25, :cond_17

    .line 801
    :try_start_f
    invoke-virtual/range {v25 .. v25}, Landroid/os/ParcelFileDescriptor;->close()V

    .line 802
    :cond_17
    if-eqz v32, :cond_c

    .line 803
    invoke-virtual/range {v32 .. v32}, Ljava/io/FileOutputStream;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_8

    goto/16 :goto_6

    .line 804
    :catch_8
    move-exception v18

    .line 805
    .local v18, "e":Ljava/io/IOException;
    invoke-virtual/range {v18 .. v18}, Ljava/io/IOException;->printStackTrace()V

    .line 806
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string v4, "IOException when closing ParcelFileDescriptor"

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 788
    .end local v18    # "e":Ljava/io/IOException;
    :catch_9
    move-exception v18

    .line 789
    .local v18, "e":Ljava/io/FileNotFoundException;
    :goto_9
    :try_start_10
    invoke-virtual/range {v18 .. v18}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 790
    move-object/from16 v0, v24

    move-object/from16 v1, v48

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    .line 791
    const/16 v35, 0x1

    .line 800
    if-eqz v25, :cond_18

    .line 801
    :try_start_11
    invoke-virtual/range {v25 .. v25}, Landroid/os/ParcelFileDescriptor;->close()V

    .line 802
    :cond_18
    if-eqz v32, :cond_c

    .line 803
    invoke-virtual/range {v32 .. v32}, Ljava/io/FileOutputStream;->close()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_a

    goto/16 :goto_6

    .line 804
    :catch_a
    move-exception v18

    .line 805
    .local v18, "e":Ljava/io/IOException;
    invoke-virtual/range {v18 .. v18}, Ljava/io/IOException;->printStackTrace()V

    .line 806
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string v4, "IOException when closing ParcelFileDescriptor"

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 793
    .end local v18    # "e":Ljava/io/IOException;
    :catch_b
    move-exception v18

    .line 794
    .restart local v18    # "e":Ljava/io/IOException;
    :goto_a
    :try_start_12
    invoke-virtual/range {v18 .. v18}, Ljava/io/IOException;->printStackTrace()V

    .line 795
    move-object/from16 v0, v24

    move-object/from16 v1, v48

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    .line 796
    const/16 v35, 0x1

    .line 800
    if-eqz v25, :cond_19

    .line 801
    :try_start_13
    invoke-virtual/range {v25 .. v25}, Landroid/os/ParcelFileDescriptor;->close()V

    .line 802
    :cond_19
    if-eqz v32, :cond_c

    .line 803
    invoke-virtual/range {v32 .. v32}, Ljava/io/FileOutputStream;->close()V
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_c

    goto/16 :goto_6

    .line 804
    :catch_c
    move-exception v18

    .line 805
    invoke-virtual/range {v18 .. v18}, Ljava/io/IOException;->printStackTrace()V

    .line 806
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string v4, "IOException when closing ParcelFileDescriptor"

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 799
    .end local v18    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    .line 800
    :goto_b
    if-eqz v25, :cond_1a

    .line 801
    :try_start_14
    invoke-virtual/range {v25 .. v25}, Landroid/os/ParcelFileDescriptor;->close()V

    .line 802
    :cond_1a
    if-eqz v32, :cond_1b

    .line 803
    invoke-virtual/range {v32 .. v32}, Ljava/io/FileOutputStream;->close()V
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_d

    .line 807
    :cond_1b
    :goto_c
    throw v2

    .line 804
    :catch_d
    move-exception v18

    .line 805
    .restart local v18    # "e":Ljava/io/IOException;
    invoke-virtual/range {v18 .. v18}, Ljava/io/IOException;->printStackTrace()V

    .line 806
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v4

    const-string v5, "IOException when closing ParcelFileDescriptor"

    invoke-static {v4, v5}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_c

    .line 855
    .end local v18    # "e":Ljava/io/IOException;
    .end local v32    # "fos":Ljava/io/FileOutputStream;
    .restart local v10    # "buffer":[B
    .restart local v33    # "fos":Ljava/io/FileOutputStream;
    .restart local v42    # "inpStream":Ljava/io/InputStream;
    .restart local v60    # "tempFile":Ljava/io/File;
    :catch_e
    move-exception v18

    .line 856
    .restart local v18    # "e":Ljava/io/IOException;
    invoke-virtual/range {v18 .. v18}, Ljava/io/IOException;->printStackTrace()V

    .line 857
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string v4, "IOException when closing ParcelFileDescriptor"

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v32, v33

    .end local v33    # "fos":Ljava/io/FileOutputStream;
    .restart local v32    # "fos":Ljava/io/FileOutputStream;
    move-object/from16 v41, v42

    .line 859
    .end local v42    # "inpStream":Ljava/io/InputStream;
    .restart local v41    # "inpStream":Ljava/io/InputStream;
    goto/16 :goto_7

    .line 845
    .end local v10    # "buffer":[B
    .end local v14    # "deleteFileDetailUri":Landroid/net/Uri;
    .end local v18    # "e":Ljava/io/IOException;
    .end local v31    # "filedetailUri":Landroid/net/Uri;
    :catch_f
    move-exception v18

    .line 846
    .local v18, "e":Ljava/lang/Exception;
    :goto_d
    :try_start_15
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_1

    .line 849
    if-eqz v25, :cond_1c

    .line 850
    :try_start_16
    invoke-virtual/range {v25 .. v25}, Landroid/os/ParcelFileDescriptor;->close()V

    .line 851
    :cond_1c
    if-eqz v41, :cond_1d

    .line 852
    invoke-virtual/range {v41 .. v41}, Ljava/io/InputStream;->close()V

    .line 853
    :cond_1d
    if-eqz v32, :cond_d

    .line 854
    invoke-virtual/range {v32 .. v32}, Ljava/io/FileOutputStream;->close()V
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_16} :catch_10

    goto/16 :goto_7

    .line 855
    :catch_10
    move-exception v18

    .line 856
    .local v18, "e":Ljava/io/IOException;
    invoke-virtual/range {v18 .. v18}, Ljava/io/IOException;->printStackTrace()V

    .line 857
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string v4, "IOException when closing ParcelFileDescriptor"

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 848
    .end local v18    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v2

    .line 849
    :goto_e
    if-eqz v25, :cond_1e

    .line 850
    :try_start_17
    invoke-virtual/range {v25 .. v25}, Landroid/os/ParcelFileDescriptor;->close()V

    .line 851
    :cond_1e
    if-eqz v41, :cond_1f

    .line 852
    invoke-virtual/range {v41 .. v41}, Ljava/io/InputStream;->close()V

    .line 853
    :cond_1f
    if-eqz v32, :cond_20

    .line 854
    invoke-virtual/range {v32 .. v32}, Ljava/io/FileOutputStream;->close()V
    :try_end_17
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_17} :catch_11

    .line 858
    :cond_20
    :goto_f
    throw v2

    .line 855
    :catch_11
    move-exception v18

    .line 856
    .restart local v18    # "e":Ljava/io/IOException;
    invoke-virtual/range {v18 .. v18}, Ljava/io/IOException;->printStackTrace()V

    .line 857
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v4

    const-string v5, "IOException when closing ParcelFileDescriptor"

    invoke-static {v4, v5}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_f

    .line 862
    .end local v18    # "e":Ljava/io/IOException;
    .end local v25    # "fileDesc":Landroid/os/ParcelFileDescriptor;
    .end local v29    # "fileName":Ljava/lang/String;
    .end local v30    # "filePath":Ljava/lang/String;
    .end local v32    # "fos":Ljava/io/FileOutputStream;
    .end local v41    # "inpStream":Ljava/io/InputStream;
    .end local v59    # "split":[Ljava/lang/String;
    .end local v60    # "tempFile":Ljava/io/File;
    :cond_21
    new-instance v61, Ljava/util/ArrayList;

    invoke-direct/range {v61 .. v61}, Ljava/util/ArrayList;-><init>()V

    .line 863
    .local v61, "tempFileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface/range {v28 .. v28}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v38

    :goto_10
    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_22

    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Ljava/lang/String;

    .line 864
    .restart local v30    # "filePath":Ljava/lang/String;
    const-string v2, "/"

    move-object/from16 v0, v30

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v59

    .line 865
    .restart local v59    # "split":[Ljava/lang/String;
    move-object/from16 v0, v59

    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    aget-object v29, v59, v2

    .line 866
    .restart local v29    # "fileName":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->TEMP_FOLDER:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v29

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v61

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_10

    .line 869
    .end local v29    # "fileName":Ljava/lang/String;
    .end local v30    # "filePath":Ljava/lang/String;
    .end local v59    # "split":[Ljava/lang/String;
    :cond_22
    move-object/from16 v0, p0

    move-object/from16 v1, v61

    invoke-direct {v0, v1}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->deleteFiles(Ljava/util/List;)V

    .line 871
    new-instance v17, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->TEMP_FOLDER:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 872
    .local v17, "dir":Ljava/io/File;
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_23

    .line 873
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->delete()Z

    move-result v43

    .line 874
    .local v43, "isDeleteSuccess":Z
    if-nez v43, :cond_23

    .line 875
    const-string v2, "SNoteSyncAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Folder "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->TEMP_FOLDER:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " could not be deleted"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 881
    .end local v17    # "dir":Ljava/io/File;
    .end local v43    # "isDeleteSuccess":Z
    .end local v61    # "tempFileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_23
    if-eqz v13, :cond_24

    invoke-interface {v13}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_24

    .line 882
    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v38

    :goto_11
    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_24

    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/String;

    .line 883
    .restart local v21    # "fPath":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->mContract:Lcom/sec/android/sCloudSync/Constants/SNoteContract;

    iget-object v2, v2, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->DELETEDETAIL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "filename"

    move-object/from16 v0, v21

    invoke-virtual {v2, v4, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "privatekey"

    move-object/from16 v0, v48

    invoke-virtual {v2, v4, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v14

    .line 888
    .restart local v14    # "deleteFileDetailUri":Landroid/net/Uri;
    :try_start_18
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, v14, v4}, Landroid/content/ContentProviderClient;->openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    :try_end_18
    .catch Landroid/os/RemoteException; {:try_start_18 .. :try_end_18} :catch_12
    .catch Ljava/io/FileNotFoundException; {:try_start_18 .. :try_end_18} :catch_13

    goto :goto_11

    .line 889
    :catch_12
    move-exception v18

    .line 890
    .local v18, "e":Landroid/os/RemoteException;
    invoke-virtual/range {v18 .. v18}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_11

    .line 891
    .end local v18    # "e":Landroid/os/RemoteException;
    :catch_13
    move-exception v18

    .line 892
    .local v18, "e":Ljava/io/FileNotFoundException;
    invoke-virtual/range {v18 .. v18}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_11

    .line 897
    .end local v14    # "deleteFileDetailUri":Landroid/net/Uri;
    .end local v18    # "e":Ljava/io/FileNotFoundException;
    .end local v21    # "fPath":Ljava/lang/String;
    :cond_24
    move-object/from16 v0, v57

    move-object/from16 v1, v48

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v58

    check-cast v58, Ljava/lang/String;

    .line 898
    .restart local v58    # "snbFilePath":Ljava/lang/String;
    if-eqz v58, :cond_1

    .line 900
    if-nez v35, :cond_25

    .line 901
    :try_start_19
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->mContract:Lcom/sec/android/sCloudSync/Constants/SNoteContract;

    iget-object v2, v2, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->CREATESNB_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "json"

    invoke-interface/range {v47 .. v48}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v4, v5, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "privatekey"

    move-object/from16 v0, v48

    invoke-virtual {v2, v4, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "timestamp"

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->mServerChangedRecords:Ljava/util/Map;

    move-object/from16 v0, v48

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/sCloudSync/Records/KVSItem;

    invoke-virtual {v2}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getTimeStamp()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v5, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v11

    .line 906
    .local v11, "createSnbUri":Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, v11, v4}, Landroid/content/ContentProviderClient;->openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    .line 908
    invoke-virtual/range {v45 .. v45}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getID()J

    move-result-wide v4

    const-wide/16 v6, -0x1

    cmp-long v2, v4, v6

    if-nez v2, :cond_1

    .line 909
    new-instance v63, Landroid/content/ContentValues;

    invoke-direct/range {v63 .. v63}, Landroid/content/ContentValues;-><init>()V

    .line 910
    .local v63, "values":Landroid/content/ContentValues;
    const-string v2, "deleted"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v63

    invoke-virtual {v0, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_19
    .catch Landroid/os/RemoteException; {:try_start_19 .. :try_end_19} :catch_15
    .catch Ljava/io/FileNotFoundException; {:try_start_19 .. :try_end_19} :catch_16

    .line 912
    :try_start_1a
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getKeyColumnName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " = \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v48

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move-object/from16 v0, v52

    move-object/from16 v1, v63

    invoke-virtual {v2, v0, v1, v4, v5}, Landroid/content/ContentProviderClient;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1a
    .catch Landroid/os/RemoteException; {:try_start_1a .. :try_end_1a} :catch_14
    .catch Ljava/io/FileNotFoundException; {:try_start_1a .. :try_end_1a} :catch_16

    goto/16 :goto_0

    .line 913
    :catch_14
    move-exception v18

    .line 914
    .local v18, "e":Landroid/os/RemoteException;
    :try_start_1b
    invoke-virtual/range {v18 .. v18}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_1b
    .catch Landroid/os/RemoteException; {:try_start_1b .. :try_end_1b} :catch_15
    .catch Ljava/io/FileNotFoundException; {:try_start_1b .. :try_end_1b} :catch_16

    goto/16 :goto_0

    .line 925
    .end local v11    # "createSnbUri":Landroid/net/Uri;
    .end local v18    # "e":Landroid/os/RemoteException;
    .end local v63    # "values":Landroid/content/ContentValues;
    :catch_15
    move-exception v18

    .line 926
    .restart local v18    # "e":Landroid/os/RemoteException;
    invoke-virtual/range {v18 .. v18}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    .line 919
    .end local v18    # "e":Landroid/os/RemoteException;
    :cond_25
    :try_start_1c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->mContract:Lcom/sec/android/sCloudSync/Constants/SNoteContract;

    iget-object v2, v2, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->DELETESNB_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "filename"

    move-object/from16 v0, v58

    invoke-virtual {v2, v4, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "privatekey"

    move-object/from16 v0, v48

    invoke-virtual {v2, v4, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v16

    .line 923
    .local v16, "deleteSnbUri":Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v2

    const/4 v4, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v2, v0, v4}, Landroid/content/ContentProviderClient;->openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    :try_end_1c
    .catch Landroid/os/RemoteException; {:try_start_1c .. :try_end_1c} :catch_15
    .catch Ljava/io/FileNotFoundException; {:try_start_1c .. :try_end_1c} :catch_16

    goto/16 :goto_0

    .line 927
    .end local v16    # "deleteSnbUri":Landroid/net/Uri;
    :catch_16
    move-exception v18

    .line 928
    .local v18, "e":Ljava/io/FileNotFoundException;
    invoke-virtual/range {v18 .. v18}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto/16 :goto_0

    .line 933
    .end local v12    # "cursor":Landroid/database/Cursor;
    .end local v13    # "delFileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v18    # "e":Ljava/io/FileNotFoundException;
    .end local v28    # "fileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v35    # "hasDownloadFailed":Z
    .end local v38    # "i$":Ljava/util/Iterator;
    .end local v45    # "item":Lcom/sec/android/sCloudSync/Records/KVSItem;
    .end local v48    # "key":Ljava/lang/String;
    .end local v58    # "snbFilePath":Ljava/lang/String;
    :cond_26
    const/16 v36, 0x0

    .line 936
    .local v36, "hasInsertFailed":Z
    if-eqz v24, :cond_2d

    invoke-interface/range {v24 .. v24}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2d

    .line 937
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->resetLastSyncTime()V

    .line 938
    invoke-interface/range {v24 .. v24}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v37

    .local v37, "i$":Ljava/util/Iterator;
    :cond_27
    :goto_12
    invoke-interface/range {v37 .. v37}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2d

    invoke-interface/range {v37 .. v37}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/String;

    .line 939
    .local v23, "failed":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->mServerChangedRecords:Ljava/util/Map;

    move-object/from16 v0, v23

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v45

    check-cast v45, Lcom/sec/android/sCloudSync/Records/KVSItem;

    .line 940
    .restart local v45    # "item":Lcom/sec/android/sCloudSync/Records/KVSItem;
    invoke-virtual/range {v45 .. v45}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getID()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-lez v2, :cond_2c

    .line 941
    invoke-virtual/range {v45 .. v45}, Lcom/sec/android/sCloudSync/Records/KVSItem;->isDeleted()Z

    move-result v2

    if-nez v2, :cond_27

    .line 943
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getContentUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual/range {v45 .. v45}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getID()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v62

    .line 944
    .local v62, "uri":Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getCallerSyncAdapter()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v62

    invoke-static {v0, v2}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getAccountName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v5

    iget-object v5, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getAccountType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v5

    iget-object v5, v5, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v62

    .line 948
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    check-cast v2, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;

    invoke-virtual {v2}, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->getOperations()Ljava/util/ArrayList;

    move-result-object v54

    .line 949
    .local v54, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    invoke-virtual/range {v54 .. v54}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v46

    .line 950
    .local v46, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/content/ContentProviderOperation;>;"
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 951
    .local v15, "deleteOperation":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :cond_28
    :goto_13
    invoke-interface/range {v46 .. v46}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2a

    .line 953
    invoke-interface/range {v46 .. v46}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v53

    check-cast v53, Landroid/content/ContentProviderOperation;

    .line 954
    .local v53, "operation":Landroid/content/ContentProviderOperation;
    invoke-virtual/range {v53 .. v53}, Landroid/content/ContentProviderOperation;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {v62 .. v62}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_29

    .line 955
    move-object/from16 v0, v53

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 956
    :cond_29
    invoke-virtual/range {v53 .. v53}, Landroid/content/ContentProviderOperation;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "snb_id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v45 .. v45}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getID()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_28

    .line 958
    move-object/from16 v0, v53

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_13

    .line 960
    .end local v53    # "operation":Landroid/content/ContentProviderOperation;
    :cond_2a
    invoke-virtual {v15}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v44

    .line 961
    .local v44, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/content/ContentProviderOperation;>;"
    :goto_14
    invoke-interface/range {v44 .. v44}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2b

    .line 962
    invoke-interface/range {v44 .. v44}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object/from16 v0, v54

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_14

    .line 964
    :cond_2b
    move-object/from16 v0, v27

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_12

    .line 971
    .end local v15    # "deleteOperation":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v44    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/content/ContentProviderOperation;>;"
    .end local v46    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/content/ContentProviderOperation;>;"
    .end local v54    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v62    # "uri":Landroid/net/Uri;
    :cond_2c
    invoke-interface/range {v22 .. v23}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 972
    const/16 v36, 0x1

    goto/16 :goto_12

    .line 981
    .end local v23    # "failed":Ljava/lang/String;
    .end local v37    # "i$":Ljava/util/Iterator;
    .end local v45    # "item":Lcom/sec/android/sCloudSync/Records/KVSItem;
    :cond_2d
    if-eqz v36, :cond_32

    .line 982
    new-instance v64, Ljava/lang/StringBuilder;

    invoke-interface/range {v22 .. v22}, Ljava/util/Set;->size()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    mul-int/lit16 v2, v2, 0x104

    move-object/from16 v0, v64

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 983
    .local v64, "where":Ljava/lang/StringBuilder;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getKeyColumnName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v64

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " IN ("

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 984
    move-object/from16 v0, v64

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Tools/AppTool;->appendIds(Ljava/lang/StringBuilder;Ljava/util/Set;)V

    .line 985
    const/16 v2, 0x29

    move-object/from16 v0, v64

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 987
    const/4 v12, 0x0

    .line 989
    .restart local v12    # "cursor":Landroid/database/Cursor;
    :try_start_1d
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v4

    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v5, "_id"

    aput-object v5, v6, v2

    invoke-virtual/range {v64 .. v64}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v5, v52

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1d
    .catch Landroid/os/RemoteException; {:try_start_1d .. :try_end_1d} :catch_17

    move-result-object v12

    .line 994
    :goto_15
    new-instance v40, Ljava/util/HashSet;

    invoke-direct/range {v40 .. v40}, Ljava/util/HashSet;-><init>()V

    .line 995
    .local v40, "ids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    if-eqz v12, :cond_2f

    .line 996
    :goto_16
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2e

    .line 997
    const/4 v2, 0x0

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, v40

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_16

    .line 991
    .end local v40    # "ids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    :catch_17
    move-exception v18

    .line 992
    .local v18, "e":Landroid/os/RemoteException;
    invoke-virtual/range {v18 .. v18}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_15

    .line 999
    .end local v18    # "e":Landroid/os/RemoteException;
    .restart local v40    # "ids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    :cond_2e
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 1003
    :cond_2f
    :try_start_1e
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v2

    invoke-virtual/range {v64 .. v64}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move-object/from16 v0, v52

    invoke-virtual {v2, v0, v4, v5}, Landroid/content/ContentProviderClient;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1e
    .catch Landroid/os/RemoteException; {:try_start_1e .. :try_end_1e} :catch_18

    .line 1008
    :goto_17
    const/4 v2, 0x0

    move-object/from16 v0, v64

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 1009
    const-string v2, "snb_id"

    move-object/from16 v0, v64

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " IN ("

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1010
    invoke-interface/range {v40 .. v40}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v37

    .restart local v37    # "i$":Ljava/util/Iterator;
    :goto_18
    invoke-interface/range {v37 .. v37}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_30

    invoke-interface/range {v37 .. v37}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Ljava/lang/Long;

    .line 1011
    .local v39, "id":Ljava/lang/Long;
    move-object/from16 v0, v64

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v4, 0x2c

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_18

    .line 1004
    .end local v37    # "i$":Ljava/util/Iterator;
    .end local v39    # "id":Ljava/lang/Long;
    :catch_18
    move-exception v18

    .line 1005
    .restart local v18    # "e":Landroid/os/RemoteException;
    invoke-virtual/range {v18 .. v18}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_17

    .line 1014
    .end local v18    # "e":Landroid/os/RemoteException;
    .restart local v37    # "i$":Ljava/util/Iterator;
    :cond_30
    invoke-interface/range {v40 .. v40}, Ljava/util/Set;->size()I

    move-result v2

    if-lez v2, :cond_31

    .line 1015
    invoke-virtual/range {v64 .. v64}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move-object/from16 v0, v64

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 1017
    :cond_31
    const/16 v2, 0x29

    move-object/from16 v0, v64

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1019
    :try_start_1f
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v2

    invoke-virtual/range {v64 .. v64}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentProviderClient;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1f
    .catch Landroid/os/RemoteException; {:try_start_1f .. :try_end_1f} :catch_19

    .line 1024
    .end local v12    # "cursor":Landroid/database/Cursor;
    .end local v37    # "i$":Ljava/util/Iterator;
    .end local v40    # "ids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    .end local v64    # "where":Ljava/lang/StringBuilder;
    :cond_32
    :goto_19
    return-void

    .line 1020
    .restart local v12    # "cursor":Landroid/database/Cursor;
    .restart local v37    # "i$":Ljava/util/Iterator;
    .restart local v40    # "ids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    .restart local v64    # "where":Ljava/lang/StringBuilder;
    :catch_19
    move-exception v18

    .line 1021
    .restart local v18    # "e":Landroid/os/RemoteException;
    invoke-virtual/range {v18 .. v18}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_19

    .line 848
    .end local v18    # "e":Landroid/os/RemoteException;
    .end local v36    # "hasInsertFailed":Z
    .end local v37    # "i$":Ljava/util/Iterator;
    .end local v40    # "ids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    .end local v64    # "where":Ljava/lang/StringBuilder;
    .restart local v13    # "delFileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v25    # "fileDesc":Landroid/os/ParcelFileDescriptor;
    .restart local v28    # "fileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v29    # "fileName":Ljava/lang/String;
    .restart local v30    # "filePath":Ljava/lang/String;
    .restart local v32    # "fos":Ljava/io/FileOutputStream;
    .restart local v35    # "hasDownloadFailed":Z
    .restart local v38    # "i$":Ljava/util/Iterator;
    .restart local v42    # "inpStream":Ljava/io/InputStream;
    .restart local v45    # "item":Lcom/sec/android/sCloudSync/Records/KVSItem;
    .restart local v48    # "key":Ljava/lang/String;
    .restart local v59    # "split":[Ljava/lang/String;
    .restart local v60    # "tempFile":Ljava/io/File;
    :catchall_2
    move-exception v2

    move-object/from16 v41, v42

    .end local v42    # "inpStream":Ljava/io/InputStream;
    .restart local v41    # "inpStream":Ljava/io/InputStream;
    goto/16 :goto_e

    .end local v32    # "fos":Ljava/io/FileOutputStream;
    .end local v41    # "inpStream":Ljava/io/InputStream;
    .restart local v10    # "buffer":[B
    .restart local v14    # "deleteFileDetailUri":Landroid/net/Uri;
    .restart local v31    # "filedetailUri":Landroid/net/Uri;
    .restart local v33    # "fos":Ljava/io/FileOutputStream;
    .restart local v42    # "inpStream":Ljava/io/InputStream;
    :catchall_3
    move-exception v2

    move-object/from16 v32, v33

    .end local v33    # "fos":Ljava/io/FileOutputStream;
    .restart local v32    # "fos":Ljava/io/FileOutputStream;
    move-object/from16 v41, v42

    .end local v42    # "inpStream":Ljava/io/InputStream;
    .restart local v41    # "inpStream":Ljava/io/InputStream;
    goto/16 :goto_e

    .line 845
    .end local v10    # "buffer":[B
    .end local v14    # "deleteFileDetailUri":Landroid/net/Uri;
    .end local v31    # "filedetailUri":Landroid/net/Uri;
    .end local v41    # "inpStream":Ljava/io/InputStream;
    .restart local v42    # "inpStream":Ljava/io/InputStream;
    :catch_1a
    move-exception v18

    move-object/from16 v41, v42

    .end local v42    # "inpStream":Ljava/io/InputStream;
    .restart local v41    # "inpStream":Ljava/io/InputStream;
    goto/16 :goto_d

    .end local v32    # "fos":Ljava/io/FileOutputStream;
    .end local v41    # "inpStream":Ljava/io/InputStream;
    .restart local v10    # "buffer":[B
    .restart local v14    # "deleteFileDetailUri":Landroid/net/Uri;
    .restart local v31    # "filedetailUri":Landroid/net/Uri;
    .restart local v33    # "fos":Ljava/io/FileOutputStream;
    .restart local v42    # "inpStream":Ljava/io/InputStream;
    :catch_1b
    move-exception v18

    move-object/from16 v32, v33

    .end local v33    # "fos":Ljava/io/FileOutputStream;
    .restart local v32    # "fos":Ljava/io/FileOutputStream;
    move-object/from16 v41, v42

    .end local v42    # "inpStream":Ljava/io/InputStream;
    .restart local v41    # "inpStream":Ljava/io/InputStream;
    goto/16 :goto_d

    .line 799
    .end local v10    # "buffer":[B
    .end local v32    # "fos":Ljava/io/FileOutputStream;
    .end local v41    # "inpStream":Ljava/io/InputStream;
    .end local v60    # "tempFile":Ljava/io/File;
    .restart local v33    # "fos":Ljava/io/FileOutputStream;
    :catchall_4
    move-exception v2

    move-object/from16 v32, v33

    .end local v33    # "fos":Ljava/io/FileOutputStream;
    .restart local v32    # "fos":Ljava/io/FileOutputStream;
    goto/16 :goto_b

    .line 793
    .end local v32    # "fos":Ljava/io/FileOutputStream;
    .restart local v33    # "fos":Ljava/io/FileOutputStream;
    :catch_1c
    move-exception v18

    move-object/from16 v32, v33

    .end local v33    # "fos":Ljava/io/FileOutputStream;
    .restart local v32    # "fos":Ljava/io/FileOutputStream;
    goto/16 :goto_a

    .line 788
    .end local v32    # "fos":Ljava/io/FileOutputStream;
    .restart local v33    # "fos":Ljava/io/FileOutputStream;
    :catch_1d
    move-exception v18

    move-object/from16 v32, v33

    .end local v33    # "fos":Ljava/io/FileOutputStream;
    .restart local v32    # "fos":Ljava/io/FileOutputStream;
    goto/16 :goto_9

    .line 783
    .end local v32    # "fos":Ljava/io/FileOutputStream;
    .restart local v33    # "fos":Ljava/io/FileOutputStream;
    :catch_1e
    move-exception v18

    move-object/from16 v32, v33

    .end local v33    # "fos":Ljava/io/FileOutputStream;
    .restart local v32    # "fos":Ljava/io/FileOutputStream;
    goto/16 :goto_8
.end method

.method public updatetoServer(Ljava/util/List;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/sCloudSync/Records/RecordSetItem;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/sCloudSync/Records/RecordBase;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;,
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 391
    .local p1, "setList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordSetItem;>;"
    .local p2, "deleteList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordBase;>;"
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mounted"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 392
    new-instance v0, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v0

    .line 395
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->handleSetList(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 399
    :goto_0
    return-void

    .line 398
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->updatetoServer(Ljava/util/List;Ljava/util/List;)V

    goto :goto_0
.end method
