.class public Lcom/sec/android/sCloudSync/Services/AccountsChangeService;
.super Landroid/app/IntentService;
.source "AccountsChangeService.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "AccountsChangeService"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    const-string v0, "AccountsChangeService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 35
    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x0

    .line 39
    const-string v2, "AccountAction"

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 40
    .local v0, "action":I
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/Services/AccountsChangeService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/sCloudSync/Util/SyncAccountManager;->create(Landroid/content/Context;)Lcom/sec/android/sCloudSync/Util/SyncAccountManager;

    move-result-object v1

    .line 41
    .local v1, "syncAccountManager":Lcom/sec/android/sCloudSync/Util/SyncAccountManager;
    packed-switch v0, :pswitch_data_0

    .line 51
    const-string v2, "AccountsChangeService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Recieved unknown action"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    :goto_0
    return-void

    .line 44
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/Services/AccountsChangeService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/sCloudSync/Util/SyncAccountManager;->accountRemoved(Landroid/content/Context;Z)V

    .line 45
    const-string v2, "accountName"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/Services/AccountsChangeService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/sCloudSync/Util/SyncAccountManager;->accountAdded(Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_0

    .line 48
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/Services/AccountsChangeService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/sCloudSync/Util/SyncAccountManager;->accountRemoved(Landroid/content/Context;Z)V

    goto :goto_0

    .line 41
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
