.class Lcom/sec/android/sCloudSync/Services/SBrowserTabSyncService$1;
.super Lcom/sec/android/sCloudSync/IDataSync/IDataSync$Stub;
.source "SBrowserTabSyncService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/sCloudSync/Services/SBrowserTabSyncService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/sCloudSync/Services/SBrowserTabSyncService;


# direct methods
.method constructor <init>(Lcom/sec/android/sCloudSync/Services/SBrowserTabSyncService;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/sec/android/sCloudSync/Services/SBrowserTabSyncService$1;->this$0:Lcom/sec/android/sCloudSync/Services/SBrowserTabSyncService;

    invoke-direct {p0}, Lcom/sec/android/sCloudSync/IDataSync/IDataSync$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public cancelSync()V
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Services/SBrowserTabSyncService$1;->this$0:Lcom/sec/android/sCloudSync/Services/SBrowserTabSyncService;

    # getter for: Lcom/sec/android/sCloudSync/Services/SBrowserTabSyncService;->sync:Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserTabSyncAdapter;
    invoke-static {v0}, Lcom/sec/android/sCloudSync/Services/SBrowserTabSyncService;->access$000(Lcom/sec/android/sCloudSync/Services/SBrowserTabSyncService;)Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserTabSyncAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserTabSyncAdapter;->cancelSync()V

    .line 66
    return-void
.end method

.method public performSync(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;)Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;
    .locals 3
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "authority"    # Ljava/lang/String;
    .param p3, "dataSyncResult"    # Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;

    .prologue
    .line 56
    invoke-virtual {p3}, Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 57
    .local v0, "extras":Landroid/os/Bundle;
    invoke-virtual {p3}, Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;->getSyncResult()Landroid/content/SyncResult;

    move-result-object v1

    .line 58
    .local v1, "syncResult":Landroid/content/SyncResult;
    iget-object v2, p0, Lcom/sec/android/sCloudSync/Services/SBrowserTabSyncService$1;->this$0:Lcom/sec/android/sCloudSync/Services/SBrowserTabSyncService;

    # getter for: Lcom/sec/android/sCloudSync/Services/SBrowserTabSyncService;->sync:Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserTabSyncAdapter;
    invoke-static {v2}, Lcom/sec/android/sCloudSync/Services/SBrowserTabSyncService;->access$000(Lcom/sec/android/sCloudSync/Services/SBrowserTabSyncService;)Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserTabSyncAdapter;

    move-result-object v2

    invoke-virtual {v2, p1, v0, p2, v1}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserTabSyncAdapter;->performSync(Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/SyncResult;)Landroid/content/SyncResult;

    move-result-object v1

    .line 59
    invoke-virtual {p3, v1}, Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;->setSyncResult(Landroid/content/SyncResult;)V

    .line 60
    return-object p3
.end method
