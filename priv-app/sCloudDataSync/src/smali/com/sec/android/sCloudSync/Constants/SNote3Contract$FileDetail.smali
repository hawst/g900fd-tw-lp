.class public interface abstract Lcom/sec/android/sCloudSync/Constants/SNote3Contract$FileDetail;
.super Ljava/lang/Object;
.source "SNote3Contract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/sCloudSync/Constants/SNote3Contract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FileDetail"
.end annotation


# static fields
.field public static final CHECKSUM:Ljava/lang/String; = "checksum"

.field public static final DELETED:Ljava/lang/String; = "deleted"

.field public static final DIRTY:Ljava/lang/String; = "dirty"

.field public static final ID:Ljava/lang/String; = "_id"

.field public static final PATH:Ljava/lang/String; = "path"

.field public static final SPD_ID:Ljava/lang/String; = "spd_id"
