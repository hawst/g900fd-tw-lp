.class public interface abstract Lcom/sec/android/sCloudSync/Constants/SMemoContract;
.super Ljava/lang/Object;
.source "SMemoContract.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/sCloudSync/Constants/SMemoContract$FileTracker;,
        Lcom/sec/android/sCloudSync/Constants/SMemoContract$ExtData;,
        Lcom/sec/android/sCloudSync/Constants/SMemoContract$PenMemo;
    }
.end annotation


# static fields
.field public static final AUTHORITY:Ljava/lang/String; = "com.sec.android.widgetapp.q1_penmemo"

.field public static final CALLER_IS_SYNCADAPTER:Ljava/lang/String; = "caller_is_syncadapter"

.field public static final FILETRACKER_TABLE_NAME:Ljava/lang/String; = "FileTracker"

.field public static final FILETRACKER_URI:Landroid/net/Uri;

.field public static final SMEMO_EXTDATA_URI:Landroid/net/Uri;

.field public static final SMEMO_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-string v0, "content://com.sec.android.widgetapp.q1_penmemo/PenMemo"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/sCloudSync/Constants/SMemoContract;->SMEMO_URI:Landroid/net/Uri;

    .line 29
    const-string v0, "content://com.sec.android.widgetapp.q1_penmemo/ExtData"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/sCloudSync/Constants/SMemoContract;->SMEMO_EXTDATA_URI:Landroid/net/Uri;

    .line 32
    const-string v0, "content://com.sec.android.sCloudSync/FileTracker"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/sCloudSync/Constants/SMemoContract;->FILETRACKER_URI:Landroid/net/Uri;

    return-void
.end method
