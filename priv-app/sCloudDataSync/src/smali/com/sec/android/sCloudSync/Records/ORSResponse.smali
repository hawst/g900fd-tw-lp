.class public final Lcom/sec/android/sCloudSync/Records/ORSResponse;
.super Lcom/sec/android/sCloudSync/Records/AbstractJSON;
.source "ORSResponse.java"


# static fields
.field private static final KEY:Ljava/lang/String; = "key"

.field private static final METADATA:Ljava/lang/String; = "metadata"

.field private static final RCODE:Ljava/lang/String; = "rcode"

.field private static final VALUE:Ljava/lang/String; = "value"


# instance fields
.field private mORSResponse:Lcom/sec/android/sCloudSync/Records/RecordORSItem;

.field private mResponseCode:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/android/sCloudSync/Records/AbstractJSON;-><init>()V

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/sCloudSync/Records/ORSResponse;->mResponseCode:I

    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Records/ORSResponse;->mORSResponse:Lcom/sec/android/sCloudSync/Records/RecordORSItem;

    .line 42
    return-void
.end method

.method public fromJSON(Ljava/lang/Object;)V
    .locals 7
    .param p1, "json"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 54
    new-instance v2, Lorg/json/JSONObject;

    check-cast p1, Ljava/lang/String;

    .end local p1    # "json":Ljava/lang/Object;
    invoke-direct {v2, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 56
    .local v2, "jsonObj":Lorg/json/JSONObject;
    const-string v6, "rcode"

    invoke-virtual {v2, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 57
    const-string v6, "rcode"

    invoke-virtual {v2, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v6

    iput v6, p0, Lcom/sec/android/sCloudSync/Records/ORSResponse;->mResponseCode:I

    .line 59
    :cond_0
    const-string v6, "metadata"

    invoke-virtual {v2, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 60
    const-string v6, "metadata"

    invoke-virtual {v2, v6}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 61
    .local v0, "dataitemarray":Lorg/json/JSONArray;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v1, v6, :cond_1

    .line 62
    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 63
    .local v4, "obj":Lorg/json/JSONObject;
    const-string v6, "key"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 64
    .local v3, "key":Ljava/lang/String;
    const-string v6, "value"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 66
    .local v5, "value":Ljava/lang/String;
    new-instance v6, Lcom/sec/android/sCloudSync/Records/RecordORSItem;

    invoke-direct {v6, v3, v5}, Lcom/sec/android/sCloudSync/Records/RecordORSItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v6, p0, Lcom/sec/android/sCloudSync/Records/ORSResponse;->mORSResponse:Lcom/sec/android/sCloudSync/Records/RecordORSItem;

    .line 61
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 69
    .end local v0    # "dataitemarray":Lorg/json/JSONArray;
    .end local v1    # "i":I
    .end local v3    # "key":Ljava/lang/String;
    .end local v4    # "obj":Lorg/json/JSONObject;
    .end local v5    # "value":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method public getORSResponse()Lcom/sec/android/sCloudSync/Records/RecordORSItem;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Records/ORSResponse;->mORSResponse:Lcom/sec/android/sCloudSync/Records/RecordORSItem;

    return-object v0
.end method

.method public getResponseCode()I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/sec/android/sCloudSync/Records/ORSResponse;->mResponseCode:I

    return v0
.end method

.method public toJSON()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 74
    const/4 v0, 0x0

    return-object v0
.end method
