.class public Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;
.super Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;
.source "BrowserSyncAdapter.java"


# static fields
.field private static final SBROWSERPACKAGE:Ljava/lang/String; = "com.sec.android.app.sbrowser"

.field private static final TAG:Ljava/lang/String; = "BrowserSyncAdapter"


# instance fields
.field private mAuthority:Ljava/lang/String;

.field private mContentUri:Landroid/net/Uri;

.field private mDataColomn:Ljava/lang/String;

.field private mSyncStateUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x0

    .line 56
    new-instance v3, Lcom/sec/android/sCloudSync/ServiceManagers/SCloudServiceManager;

    const-string v4, "4OuNBe4y9z"

    const/4 v5, 0x0

    invoke-direct {v3, p1, v4, v5}, Lcom/sec/android/sCloudSync/ServiceManagers/SCloudServiceManager;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    invoke-direct {p0, p1, v3}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;-><init>(Landroid/content/Context;Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;)V

    .line 50
    iput-object v6, p0, Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;->mContentUri:Landroid/net/Uri;

    .line 51
    iput-object v6, p0, Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;->mAuthority:Ljava/lang/String;

    .line 53
    iput-object v6, p0, Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;->mSyncStateUri:Landroid/net/Uri;

    .line 54
    iput-object v6, p0, Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;->mDataColomn:Ljava/lang/String;

    .line 58
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 60
    .local v2, "packageManager":Landroid/content/pm/PackageManager;
    :try_start_0
    const-string v3, "com.sec.android.app.sbrowser"

    const/16 v4, 0x80

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 61
    .local v1, "packageInfo":Landroid/content/pm/PackageInfo;
    iget v3, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    const/16 v4, 0xf

    if-lt v3, v4, :cond_0

    .line 62
    const-string v3, "com.sec.android.app.sbrowser"

    iput-object v3, p0, Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;->mAuthority:Ljava/lang/String;

    .line 63
    const-string v3, "content://com.sec.android.app.sbrowser"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const-string v4, "bookmarks"

    invoke-static {v3, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;->mContentUri:Landroid/net/Uri;

    .line 64
    sget-object v3, Lcom/sec/android/sCloudSync/Constants/SBrowserContract;->SBROWSER_SYNC_STATE_URI:Landroid/net/Uri;

    iput-object v3, p0, Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;->mSyncStateUri:Landroid/net/Uri;

    .line 65
    const-string v3, "data"

    iput-object v3, p0, Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;->mDataColomn:Ljava/lang/String;

    .line 75
    .end local v1    # "packageInfo":Landroid/content/pm/PackageInfo;
    :goto_0
    return-void

    .line 67
    .restart local v1    # "packageInfo":Landroid/content/pm/PackageInfo;
    :cond_0
    const-string v3, "com.sec.android.app.sbrowser.browser"

    iput-object v3, p0, Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;->mAuthority:Ljava/lang/String;

    .line 68
    sget-object v3, Lcom/sec/android/sCloudSync/Constants/BrowserContract;->SBROWSER_CONTENT_URI:Landroid/net/Uri;

    iput-object v3, p0, Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;->mContentUri:Landroid/net/Uri;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 70
    .end local v1    # "packageInfo":Landroid/content/pm/PackageInfo;
    :catch_0
    move-exception v0

    .line 71
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 72
    sget-object v3, Lcom/sec/android/sCloudSync/Constants/BrowserContract$Bookmarks;->CONTENT_URI:Landroid/net/Uri;

    iput-object v3, p0, Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;->mContentUri:Landroid/net/Uri;

    .line 73
    const-string v3, "com.android.browser"

    iput-object v3, p0, Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;->mAuthority:Ljava/lang/String;

    goto :goto_0
.end method

.method private fixParentIds(Landroid/content/SyncStats;)V
    .locals 20
    .param p1, "stats"    # Landroid/content/SyncStats;

    .prologue
    .line 127
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;->getLocalupdatesUri()Landroid/net/Uri;

    move-result-object v4

    .line 128
    .local v4, "uri":Landroid/net/Uri;
    const/4 v3, 0x2

    new-array v5, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;->getIdColumnName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v3

    const/4 v3, 0x1

    const-string v7, "sync4"

    aput-object v7, v5, v3

    .line 129
    .local v5, "projection":[Ljava/lang/String;
    const/4 v9, 0x0

    .line 130
    .local v9, "cursor":Landroid/database/Cursor;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;->getAccountName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, " = \'"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v7

    iget-object v7, v7, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, "\' AND "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;->getAccountType()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, " = \'"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v7

    iget-object v7, v7, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, "\' AND "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, "folder"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, " = 1"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 134
    .local v6, "where":Ljava/lang/String;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v3

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v9

    .line 139
    if-eqz v9, :cond_0

    .line 140
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;->getIdColumnName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    .line 141
    .local v15, "parentidIndex":I
    const-string v3, "sync4"

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    .line 142
    .local v14, "parentKeyIndex":I
    new-instance v17, Landroid/content/ContentValues;

    invoke-direct/range {v17 .. v17}, Landroid/content/ContentValues;-><init>()V

    .line 143
    .local v17, "values":Landroid/content/ContentValues;
    :goto_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 144
    invoke-virtual/range {v17 .. v17}, Landroid/content/ContentValues;->clear()V

    .line 145
    invoke-interface {v9, v15}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 146
    .local v12, "parentId":J
    invoke-interface {v9, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 152
    .local v16, "parentkey":Ljava/lang/String;
    const-string v3, "parent"

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    move-object/from16 v0, v17

    invoke-virtual {v0, v3, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 154
    :try_start_1
    move-object/from16 v0, p1

    iget-wide v7, v0, Landroid/content/SyncStats;->numUpdates:J

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v3

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "sync3 = \'"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "\'"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    const/16 v19, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v3, v4, v0, v1, v2}, Landroid/content/ContentProviderClient;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    int-to-long v0, v3

    move-wide/from16 v18, v0

    add-long v7, v7, v18

    move-object/from16 v0, p1

    iput-wide v7, v0, Landroid/content/SyncStats;->numUpdates:J
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    .line 157
    :catch_0
    move-exception v10

    .line 158
    .local v10, "e":Landroid/os/RemoteException;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v3

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "fixParentIds(): "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v10}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 135
    .end local v10    # "e":Landroid/os/RemoteException;
    .end local v12    # "parentId":J
    .end local v14    # "parentKeyIndex":I
    .end local v15    # "parentidIndex":I
    .end local v16    # "parentkey":Ljava/lang/String;
    .end local v17    # "values":Landroid/content/ContentValues;
    :catch_1
    move-exception v10

    .line 136
    .restart local v10    # "e":Landroid/os/RemoteException;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v3

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "fixParentIds(): "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v10}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    .end local v10    # "e":Landroid/os/RemoteException;
    :cond_0
    :goto_1
    return-void

    .line 159
    .restart local v12    # "parentId":J
    .restart local v14    # "parentKeyIndex":I
    .restart local v15    # "parentidIndex":I
    .restart local v16    # "parentkey":Ljava/lang/String;
    .restart local v17    # "values":Landroid/content/ContentValues;
    :catch_2
    move-exception v11

    .line 160
    .local v11, "e2":Ljava/lang/NullPointerException;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v3

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "fixParentIds(): "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v11}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 163
    .end local v11    # "e2":Ljava/lang/NullPointerException;
    .end local v12    # "parentId":J
    .end local v16    # "parentkey":Ljava/lang/String;
    :cond_1
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_1
.end method


# virtual methods
.method protected doUpdateDelete(Lcom/sec/android/sCloudSync/Records/KVSItem;Lcom/sec/android/sCloudSync/Records/RecordItem;Landroid/content/SyncStats;)Z
    .locals 8
    .param p1, "item"    # Lcom/sec/android/sCloudSync/Records/KVSItem;
    .param p2, "record"    # Lcom/sec/android/sCloudSync/Records/RecordItem;
    .param p3, "stats"    # Landroid/content/SyncStats;

    .prologue
    .line 104
    const/4 v7, 0x0

    .line 105
    .local v7, "ret":Z
    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->isDeleted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->doUpdateDelete(Lcom/sec/android/sCloudSync/Records/KVSItem;Lcom/sec/android/sCloudSync/Records/RecordItem;Landroid/content/SyncStats;)Z

    move-result v7

    .line 115
    :goto_0
    return v7

    .line 108
    :cond_0
    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getValue()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getTimeStamp()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getID()J

    move-result-wide v4

    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getKEY()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;->update(Ljava/lang/String;JJLjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 109
    :cond_1
    const-string v0, "BrowserSyncAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to update record with key:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getKEY()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 112
    :cond_2
    iget-wide v0, p3, Landroid/content/SyncStats;->numUpdates:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p3, Landroid/content/SyncStats;->numUpdates:J

    goto :goto_0
.end method

.method public getAccountName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 217
    const-string v0, "account_name"

    return-object v0
.end method

.method public getAccountType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 222
    const-string v0, "account_type"

    return-object v0
.end method

.method public getBuilder()Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;
    .locals 5

    .prologue
    .line 212
    new-instance v0, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;->mAuthority:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;-><init>(Landroid/content/ContentProviderClient;Landroid/accounts/Account;Landroid/content/Context;Ljava/lang/String;)V

    return-object v0
.end method

.method public getCallerSyncAdapter()Ljava/lang/String;
    .locals 1

    .prologue
    .line 177
    const-string v0, "caller_is_syncadapter"

    return-object v0
.end method

.method public getCid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    const-string v0, "4OuNBe4y9z"

    return-object v0
.end method

.method public getContentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;->mContentUri:Landroid/net/Uri;

    return-object v0
.end method

.method protected getCtidKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 227
    const-string v0, "BR"

    return-object v0
.end method

.method public getDeletedColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 187
    const-string v0, "deleted"

    return-object v0
.end method

.method public getDirtyColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 207
    const-string v0, "dirty"

    return-object v0
.end method

.method public getIdColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 202
    const-string v0, "_id"

    return-object v0
.end method

.method public getKeyColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 192
    const-string v0, "sync1"

    return-object v0
.end method

.method protected getLocalUpdatesSelection()Ljava/lang/String;
    .locals 2

    .prologue
    .line 170
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;->getAccountName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v1

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;->getAccountType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v1

    iget-object v1, v1, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;->getDirtyColumnName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = 1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLocalupdatesUri()Landroid/net/Uri;
    .locals 4

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;->getContentUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;->getCallerSyncAdapter()Ljava/lang/String;

    move-result-object v2

    const-string v3, "true"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "account_name"

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v3

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "account_type"

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v3

    iget-object v3, v3, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "show_deleted"

    const-string v3, "true"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 98
    .local v0, "uri":Landroid/net/Uri;
    return-object v0
.end method

.method public getSyncAdapterName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    const-string v0, "BROWSER"

    return-object v0
.end method

.method protected getSyncStateDataColumn()Ljava/lang/String;
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;->mDataColomn:Ljava/lang/String;

    return-object v0
.end method

.method protected getSyncStateURI()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;->mSyncStateUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    const-string v0, "BrowserSyncAdapter"

    return-object v0
.end method

.method public getTimeStampColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 197
    const-string v0, "sync5"

    return-object v0
.end method

.method public updateLocalDb(Landroid/content/SyncStats;)V
    .locals 0
    .param p1, "stats"    # Landroid/content/SyncStats;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .line 122
    invoke-super {p0, p1}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->updateLocalDb(Landroid/content/SyncStats;)V

    .line 123
    invoke-direct {p0, p1}, Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;->fixParentIds(Landroid/content/SyncStats;)V

    .line 124
    return-void
.end method
