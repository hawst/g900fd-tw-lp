.class public interface abstract Lcom/sec/android/sCloudSync/Constants/SMemoContract$FileTracker;
.super Ljava/lang/Object;
.source "SMemoContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/sCloudSync/Constants/SMemoContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FileTracker"
.end annotation


# static fields
.field public static final FILEPATH:Ljava/lang/String; = "FilePath"

.field public static final FILE_CHECKSUM:Ljava/lang/String; = "Checksum"

.field public static final ID:Ljava/lang/String; = "_id"

.field public static final SMEMO_KEY:Ljava/lang/String; = "sMemoId"
