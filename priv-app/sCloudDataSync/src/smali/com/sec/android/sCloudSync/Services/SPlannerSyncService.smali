.class public Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;
.super Landroid/app/Service;
.source "SPlannerSyncService.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "CALENDAR-SYNC"


# instance fields
.field private eventSync:Lcom/sec/android/sCloudSync/SyncAdapters/SPlannerEventSyncAdapter;

.field private final mBinder:Lcom/sec/android/sCloudSync/IDataSync/IDataSync$Stub;

.field private mCancled:Z

.field private taskSync:Lcom/sec/android/sCloudSync/SyncAdapters/SPlannerTaskSyncAdapter;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 34
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 36
    iput-object v0, p0, Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;->eventSync:Lcom/sec/android/sCloudSync/SyncAdapters/SPlannerEventSyncAdapter;

    .line 37
    iput-object v0, p0, Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;->taskSync:Lcom/sec/android/sCloudSync/SyncAdapters/SPlannerTaskSyncAdapter;

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;->mCancled:Z

    .line 58
    new-instance v0, Lcom/sec/android/sCloudSync/Services/SPlannerSyncService$1;

    invoke-direct {v0, p0}, Lcom/sec/android/sCloudSync/Services/SPlannerSyncService$1;-><init>(Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;)V

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;->mBinder:Lcom/sec/android/sCloudSync/IDataSync/IDataSync$Stub;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;)Lcom/sec/android/sCloudSync/SyncAdapters/SPlannerEventSyncAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;->eventSync:Lcom/sec/android/sCloudSync/SyncAdapters/SPlannerEventSyncAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;->mCancled:Z

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;
    .param p1, "x1"    # Z

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;->mCancled:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;)Lcom/sec/android/sCloudSync/SyncAdapters/SPlannerTaskSyncAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;->taskSync:Lcom/sec/android/sCloudSync/SyncAdapters/SPlannerTaskSyncAdapter;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;->mBinder:Lcom/sec/android/sCloudSync/IDataSync/IDataSync$Stub;

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 41
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 42
    new-instance v0, Lcom/sec/android/sCloudSync/SyncAdapters/SPlannerEventSyncAdapter;

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/sCloudSync/SyncAdapters/SPlannerEventSyncAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;->eventSync:Lcom/sec/android/sCloudSync/SyncAdapters/SPlannerEventSyncAdapter;

    .line 43
    new-instance v0, Lcom/sec/android/sCloudSync/SyncAdapters/SPlannerTaskSyncAdapter;

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/sCloudSync/SyncAdapters/SPlannerTaskSyncAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;->taskSync:Lcom/sec/android/sCloudSync/SyncAdapters/SPlannerTaskSyncAdapter;

    .line 44
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 55
    const-string v0, "CALENDAR-SYNC"

    const-string v1, "SYNC SERVICE END"

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    return-void
.end method
