.class interface abstract Lcom/sec/android/sCloudSync/Constants/BrowserContract$SyncColumns;
.super Ljava/lang/Object;
.source "BrowserContract.java"

# interfaces
.implements Lcom/sec/android/sCloudSync/Constants/BrowserContract$BaseSyncColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/sCloudSync/Constants/BrowserContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x608
    name = "SyncColumns"
.end annotation


# static fields
.field public static final ACCOUNT_NAME:Ljava/lang/String; = "account_name"

.field public static final ACCOUNT_TYPE:Ljava/lang/String; = "account_type"

.field public static final DATE_MODIFIED:Ljava/lang/String; = "modified"

.field public static final DEVICE_ID:Ljava/lang/String; = "device_id"

.field public static final DEVICE_NAME:Ljava/lang/String; = "device_name"

.field public static final DIRTY:Ljava/lang/String; = "dirty"

.field public static final SOURCE_ID:Ljava/lang/String; = "sourceid"

.field public static final VERSION:Ljava/lang/String; = "version"
