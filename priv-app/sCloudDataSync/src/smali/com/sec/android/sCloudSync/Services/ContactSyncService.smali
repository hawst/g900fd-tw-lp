.class public Lcom/sec/android/sCloudSync/Services/ContactSyncService;
.super Landroid/app/Service;
.source "ContactSyncService.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "CONTACTS-SYNC"


# instance fields
.field contactsGrpSync:Lcom/sec/android/sCloudSync/SyncAdapters/ContactsGroupSyncAdapter;

.field contactsSync:Lcom/sec/android/sCloudSync/SyncAdapters/ContactsSyncAdapter;

.field private isCancel:Z

.field private final mBinder:Lcom/sec/android/sCloudSync/IDataSync/IDataSync$Stub;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 34
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 36
    iput-object v0, p0, Lcom/sec/android/sCloudSync/Services/ContactSyncService;->contactsSync:Lcom/sec/android/sCloudSync/SyncAdapters/ContactsSyncAdapter;

    .line 37
    iput-object v0, p0, Lcom/sec/android/sCloudSync/Services/ContactSyncService;->contactsGrpSync:Lcom/sec/android/sCloudSync/SyncAdapters/ContactsGroupSyncAdapter;

    .line 58
    new-instance v0, Lcom/sec/android/sCloudSync/Services/ContactSyncService$1;

    invoke-direct {v0, p0}, Lcom/sec/android/sCloudSync/Services/ContactSyncService$1;-><init>(Lcom/sec/android/sCloudSync/Services/ContactSyncService;)V

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Services/ContactSyncService;->mBinder:Lcom/sec/android/sCloudSync/IDataSync/IDataSync$Stub;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/sCloudSync/Services/ContactSyncService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/sCloudSync/Services/ContactSyncService;

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/sec/android/sCloudSync/Services/ContactSyncService;->isCancel:Z

    return v0
.end method

.method static synthetic access$002(Lcom/sec/android/sCloudSync/Services/ContactSyncService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/sCloudSync/Services/ContactSyncService;
    .param p1, "x1"    # Z

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/sec/android/sCloudSync/Services/ContactSyncService;->isCancel:Z

    return p1
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Services/ContactSyncService;->mBinder:Lcom/sec/android/sCloudSync/IDataSync/IDataSync$Stub;

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 41
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 42
    new-instance v0, Lcom/sec/android/sCloudSync/SyncAdapters/ContactsSyncAdapter;

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/Services/ContactSyncService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/sCloudSync/SyncAdapters/ContactsSyncAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Services/ContactSyncService;->contactsSync:Lcom/sec/android/sCloudSync/SyncAdapters/ContactsSyncAdapter;

    .line 43
    new-instance v0, Lcom/sec/android/sCloudSync/SyncAdapters/ContactsGroupSyncAdapter;

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/Services/ContactSyncService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/sCloudSync/SyncAdapters/ContactsGroupSyncAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Services/ContactSyncService;->contactsGrpSync:Lcom/sec/android/sCloudSync/SyncAdapters/ContactsGroupSyncAdapter;

    .line 44
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 55
    const-string v0, "CONTACTS-SYNC"

    const-string v1, "SYNC SERVICE END"

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    return-void
.end method
