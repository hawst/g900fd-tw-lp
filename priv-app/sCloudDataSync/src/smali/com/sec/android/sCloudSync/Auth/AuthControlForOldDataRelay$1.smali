.class Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay$1;
.super Ljava/lang/Object;
.source "AuthControlForOldDataRelay.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;


# direct methods
.method constructor <init>(Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;)V
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay$1;->this$0:Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "binder"    # Landroid/os/IBinder;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay$1;->this$0:Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;

    invoke-static {p2}, Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;

    move-result-object v1

    # setter for: Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->mService:Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;
    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->access$002(Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;)Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;

    .line 52
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay$1;->this$0:Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;

    # getter for: Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->mService:Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;
    invoke-static {v0}, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->access$000(Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;)Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;

    move-result-object v0

    if-nez v0, :cond_0

    .line 53
    const-string v0, "AuthControl"

    const-string v1, "onServiceConnected : There is BindingService Error."

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    :goto_0
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay$1;->this$0:Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;

    # getter for: Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->syncLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->access$100(Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 59
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay$1;->this$0:Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;

    # getter for: Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->syncLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->access$100(Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 60
    const-string v0, "AuthControl"

    const-string v2, "onServiceConnected : notify to performSync."

    invoke-static {v0, v2}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62
    return-void

    .line 55
    :cond_0
    const-string v0, "AuthControl"

    const-string v1, "onServiceConnected : Binded."

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 61
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay$1;->this$0:Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->mService:Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;
    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->access$002(Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;)Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;

    .line 67
    const-string v0, "AuthControl"

    const-string v1, "onServiceDisconnected : unBinded."

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    return-void
.end method
