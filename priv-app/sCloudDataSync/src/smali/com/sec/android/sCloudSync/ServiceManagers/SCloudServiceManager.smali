.class public Lcom/sec/android/sCloudSync/ServiceManagers/SCloudServiceManager;
.super Ljava/lang/Object;
.source "SCloudServiceManager.java"

# interfaces
.implements Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;


# instance fields
.field private fileServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;

.field private mAuthManager:Lcom/sec/android/sCloudSync/Auth/AuthManager;

.field private mCtid:Ljava/lang/String;

.field private mLastApiCode:I

.field private recordServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/IRecordServiceManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cid"    # Ljava/lang/String;
    .param p3, "isFileService"    # Z

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;

    invoke-direct {v0, p1, p2, p0}, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;)V

    iput-object v0, p0, Lcom/sec/android/sCloudSync/ServiceManagers/SCloudServiceManager;->recordServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/IRecordServiceManager;

    .line 23
    if-eqz p3, :cond_0

    .line 24
    new-instance v0, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;

    invoke-direct {v0, p1, p2, p0}, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;)V

    iput-object v0, p0, Lcom/sec/android/sCloudSync/ServiceManagers/SCloudServiceManager;->fileServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;

    .line 25
    :cond_0
    return-void
.end method


# virtual methods
.method public getAuthManager()Lcom/sec/android/sCloudSync/Auth/AuthManager;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/sCloudSync/ServiceManagers/SCloudServiceManager;->mAuthManager:Lcom/sec/android/sCloudSync/Auth/AuthManager;

    return-object v0
.end method

.method public getCtid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/sCloudSync/ServiceManagers/SCloudServiceManager;->mCtid:Ljava/lang/String;

    return-object v0
.end method

.method public getFileServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/sCloudSync/ServiceManagers/SCloudServiceManager;->fileServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;

    return-object v0
.end method

.method public getLastApi()I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lcom/sec/android/sCloudSync/ServiceManagers/SCloudServiceManager;->mLastApiCode:I

    return v0
.end method

.method public getRecordServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/IRecordServiceManager;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/sCloudSync/ServiceManagers/SCloudServiceManager;->recordServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/IRecordServiceManager;

    return-object v0
.end method

.method public serviceEnd(ZLjava/lang/String;)Lcom/sec/android/sCloudSync/Records/KVSResponse;
    .locals 1
    .param p1, "isSuc"    # Z
    .param p2, "msg"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/sCloudSync/ServiceManagers/SCloudServiceManager;->recordServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/IRecordServiceManager;

    check-cast v0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->serviceEnd(ZLjava/lang/String;)Lcom/sec/android/sCloudSync/Records/KVSResponse;

    move-result-object v0

    return-object v0
.end method

.method public serviceStart(Ljava/lang/String;)Lcom/sec/android/sCloudSync/Records/KVSResponse;
    .locals 1
    .param p1, "trigger"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/sCloudSync/ServiceManagers/SCloudServiceManager;->recordServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/IRecordServiceManager;

    check-cast v0, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;

    invoke-virtual {v0, p1}, Lcom/sec/android/sCloudSync/ServiceManagers/KVSServiceManager;->serviceStart(Ljava/lang/String;)Lcom/sec/android/sCloudSync/Records/KVSResponse;

    move-result-object v0

    return-object v0
.end method

.method public setAuthManager(Lcom/sec/android/sCloudSync/Auth/AuthManager;)V
    .locals 0
    .param p1, "authManger"    # Lcom/sec/android/sCloudSync/Auth/AuthManager;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/sec/android/sCloudSync/ServiceManagers/SCloudServiceManager;->mAuthManager:Lcom/sec/android/sCloudSync/Auth/AuthManager;

    .line 80
    return-void
.end method

.method public setCtid(Ljava/lang/String;)V
    .locals 0
    .param p1, "ctid"    # Ljava/lang/String;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/sCloudSync/ServiceManagers/SCloudServiceManager;->mCtid:Ljava/lang/String;

    .line 75
    return-void
.end method

.method public setLastApi(I)V
    .locals 0
    .param p1, "api"    # I

    .prologue
    .line 69
    iput p1, p0, Lcom/sec/android/sCloudSync/ServiceManagers/SCloudServiceManager;->mLastApiCode:I

    .line 70
    return-void
.end method
