.class Lcom/sec/android/sCloudSync/Services/SPlannerSyncService$1;
.super Lcom/sec/android/sCloudSync/IDataSync/IDataSync$Stub;
.source "SPlannerSyncService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;


# direct methods
.method constructor <init>(Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/sec/android/sCloudSync/Services/SPlannerSyncService$1;->this$0:Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;

    invoke-direct {p0}, Lcom/sec/android/sCloudSync/IDataSync/IDataSync$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public cancelSync()V
    .locals 2

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Services/SPlannerSyncService$1;->this$0:Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;

    # getter for: Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;->eventSync:Lcom/sec/android/sCloudSync/SyncAdapters/SPlannerEventSyncAdapter;
    invoke-static {v0}, Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;->access$000(Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;)Lcom/sec/android/sCloudSync/SyncAdapters/SPlannerEventSyncAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/sCloudSync/SyncAdapters/SPlannerEventSyncAdapter;->cancelSync()V

    .line 75
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Services/SPlannerSyncService$1;->this$0:Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;

    # getter for: Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;->taskSync:Lcom/sec/android/sCloudSync/SyncAdapters/SPlannerTaskSyncAdapter;
    invoke-static {v0}, Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;->access$200(Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;)Lcom/sec/android/sCloudSync/SyncAdapters/SPlannerTaskSyncAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/sCloudSync/SyncAdapters/SPlannerTaskSyncAdapter;->cancelSync()V

    .line 76
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Services/SPlannerSyncService$1;->this$0:Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;->mCancled:Z
    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;->access$102(Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;Z)Z

    .line 77
    return-void
.end method

.method public performSync(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;)Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;
    .locals 4
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "authority"    # Ljava/lang/String;
    .param p3, "dataSyncResult"    # Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;

    .prologue
    .line 62
    invoke-virtual {p3}, Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 63
    .local v0, "extras":Landroid/os/Bundle;
    invoke-virtual {p3}, Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;->getSyncResult()Landroid/content/SyncResult;

    move-result-object v1

    .line 64
    .local v1, "syncResult":Landroid/content/SyncResult;
    iget-object v2, p0, Lcom/sec/android/sCloudSync/Services/SPlannerSyncService$1;->this$0:Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;

    # getter for: Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;->eventSync:Lcom/sec/android/sCloudSync/SyncAdapters/SPlannerEventSyncAdapter;
    invoke-static {v2}, Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;->access$000(Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;)Lcom/sec/android/sCloudSync/SyncAdapters/SPlannerEventSyncAdapter;

    move-result-object v2

    invoke-virtual {v2, p1, v0, p2, v1}, Lcom/sec/android/sCloudSync/SyncAdapters/SPlannerEventSyncAdapter;->performSync(Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/SyncResult;)Landroid/content/SyncResult;

    move-result-object v1

    .line 65
    iget-object v2, p0, Lcom/sec/android/sCloudSync/Services/SPlannerSyncService$1;->this$0:Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;

    # getter for: Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;->mCancled:Z
    invoke-static {v2}, Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;->access$100(Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 66
    iget-object v2, p0, Lcom/sec/android/sCloudSync/Services/SPlannerSyncService$1;->this$0:Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;

    # getter for: Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;->taskSync:Lcom/sec/android/sCloudSync/SyncAdapters/SPlannerTaskSyncAdapter;
    invoke-static {v2}, Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;->access$200(Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;)Lcom/sec/android/sCloudSync/SyncAdapters/SPlannerTaskSyncAdapter;

    move-result-object v2

    invoke-virtual {v2, p1, v0, p2, v1}, Lcom/sec/android/sCloudSync/SyncAdapters/SPlannerTaskSyncAdapter;->performSync(Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/SyncResult;)Landroid/content/SyncResult;

    move-result-object v1

    .line 67
    :cond_0
    invoke-virtual {p3, v1}, Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;->setSyncResult(Landroid/content/SyncResult;)V

    .line 68
    iget-object v2, p0, Lcom/sec/android/sCloudSync/Services/SPlannerSyncService$1;->this$0:Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;

    const/4 v3, 0x0

    # setter for: Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;->mCancled:Z
    invoke-static {v2, v3}, Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;->access$102(Lcom/sec/android/sCloudSync/Services/SPlannerSyncService;Z)Z

    .line 69
    return-object p3
.end method
