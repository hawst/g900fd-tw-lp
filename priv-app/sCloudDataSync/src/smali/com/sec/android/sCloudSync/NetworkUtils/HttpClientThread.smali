.class public Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;
.super Ljava/lang/Thread;
.source "HttpClientThread.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread$RESPONSE;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "HttpClientThread"


# instance fields
.field private mHttpClient:Lorg/apache/http/client/HttpClient;

.field private mHttpClientResponseListener:Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientResponseListener;

.field private mHttpResponse:Lorg/apache/http/HttpResponse;

.field private mHttpUriRequest:Lorg/apache/http/client/methods/HttpUriRequest;

.field private mRequestID:Ljava/lang/String;

.field private mThreadLock:Ljava/lang/Object;

.field private mbClosed:Z


# direct methods
.method public constructor <init>(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/HttpClient;Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientResponseListener;Ljava/lang/String;)V
    .locals 1
    .param p1, "httpUriRequest"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .param p2, "httpClient"    # Lorg/apache/http/client/HttpClient;
    .param p3, "listener"    # Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientResponseListener;
    .param p4, "requestID"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 51
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 34
    iput-object v0, p0, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->mHttpClientResponseListener:Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientResponseListener;

    .line 35
    iput-object v0, p0, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->mHttpUriRequest:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 36
    iput-object v0, p0, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->mHttpClient:Lorg/apache/http/client/HttpClient;

    .line 37
    iput-object v0, p0, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->mRequestID:Ljava/lang/String;

    .line 38
    iput-object v0, p0, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->mHttpResponse:Lorg/apache/http/HttpResponse;

    .line 39
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->mbClosed:Z

    .line 40
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->mThreadLock:Ljava/lang/Object;

    .line 52
    iput-object p3, p0, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->mHttpClientResponseListener:Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientResponseListener;

    .line 53
    iput-object p1, p0, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->mHttpUriRequest:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 54
    iput-object p2, p0, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->mHttpClient:Lorg/apache/http/client/HttpClient;

    .line 55
    iput-object p4, p0, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->mRequestID:Ljava/lang/String;

    .line 57
    return-void
.end method

.method private removeThis(I)V
    .locals 2
    .param p1, "reason"    # I

    .prologue
    .line 110
    iget-object v1, p0, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->mHttpClientResponseListener:Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientResponseListener;

    if-eqz v1, :cond_0

    .line 111
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 112
    .local v0, "msg":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 113
    iput-object p0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 114
    iget-object v1, p0, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->mHttpClientResponseListener:Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientResponseListener;

    invoke-interface {v1, v0}, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientResponseListener;->onResponse(Landroid/os/Message;)V

    .line 116
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    return-void
.end method

.method private responseOK()V
    .locals 2

    .prologue
    .line 120
    iget-object v1, p0, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->mHttpClientResponseListener:Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientResponseListener;

    if-eqz v1, :cond_0

    .line 121
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 122
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 123
    iput-object p0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 124
    iget-object v1, p0, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->mHttpClientResponseListener:Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientResponseListener;

    invoke-interface {v1, v0}, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientResponseListener;->onResponse(Landroid/os/Message;)V

    .line 126
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 130
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->mbClosed:Z

    .line 131
    return-void
.end method

.method public getHttpResponse()Lorg/apache/http/HttpResponse;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->mHttpResponse:Lorg/apache/http/HttpResponse;

    return-object v0
.end method

.method public getRequestID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->mRequestID:Ljava/lang/String;

    return-object v0
.end method

.method public getThreadLock()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->mThreadLock:Ljava/lang/Object;

    return-object v0
.end method

.method public isClosed()Z
    .locals 1

    .prologue
    .line 135
    iget-boolean v0, p0, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->mbClosed:Z

    return v0
.end method

.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x3

    .line 71
    iget-object v1, p0, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->mHttpUriRequest:Lorg/apache/http/client/methods/HttpUriRequest;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->mHttpClient:Lorg/apache/http/client/HttpClient;

    if-eqz v1, :cond_0

    .line 74
    :try_start_0
    const-string v1, "HttpClientThread"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mHttpUriRequest : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->mHttpUriRequest:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    iget-object v1, p0, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->mHttpClient:Lorg/apache/http/client/HttpClient;

    iget-object v2, p0, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->mHttpUriRequest:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v1, v2}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->mHttpResponse:Lorg/apache/http/HttpResponse;

    .line 76
    const-string v1, "HttpClientThread"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Response : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->mHttpResponse:Lorg/apache/http/HttpResponse;

    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_3

    .line 78
    const-wide/16 v1, 0xa

    :try_start_1
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_3

    .line 85
    :try_start_2
    iget-boolean v1, p0, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->mbClosed:Z

    if-nez v1, :cond_1

    .line 86
    invoke-direct {p0}, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->responseOK()V

    .line 106
    :cond_0
    :goto_0
    return-void

    .line 79
    :catch_0
    move-exception v0

    .line 80
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 81
    iget-object v1, p0, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->mHttpUriRequest:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v1}, Lorg/apache/http/client/methods/HttpUriRequest;->abort()V

    .line 82
    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->removeThis(I)V
    :try_end_2
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_0

    .line 90
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v0

    .line 91
    .local v0, "e":Lorg/apache/http/client/ClientProtocolException;
    invoke-virtual {v0}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V

    .line 92
    iget-object v1, p0, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->mHttpUriRequest:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v1}, Lorg/apache/http/client/methods/HttpUriRequest;->abort()V

    .line 93
    const-string v1, "HttpClientThread"

    const-string v2, "This request is cancelled because of ClientProtocolException"

    invoke-static {v1, v2}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    invoke-direct {p0, v4}, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->removeThis(I)V

    goto :goto_0

    .line 88
    .end local v0    # "e":Lorg/apache/http/client/ClientProtocolException;
    :cond_1
    const/4 v1, 0x2

    :try_start_3
    invoke-direct {p0, v1}, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->removeThis(I)V
    :try_end_3
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_0

    .line 95
    :catch_2
    move-exception v0

    .line 96
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 97
    iget-object v1, p0, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->mHttpUriRequest:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v1}, Lorg/apache/http/client/methods/HttpUriRequest;->abort()V

    .line 98
    const-string v1, "HttpClientThread"

    const-string v2, "This request is cancelled because of IOException"

    invoke-static {v1, v2}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    invoke-direct {p0, v4}, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->removeThis(I)V

    goto :goto_0

    .line 100
    .end local v0    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v0

    .line 101
    .local v0, "e":Ljava/lang/OutOfMemoryError;
    iget-object v1, p0, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->mHttpUriRequest:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v1}, Lorg/apache/http/client/methods/HttpUriRequest;->abort()V

    .line 102
    const-string v1, "HttpClientThread"

    const-string v2, "This request is cancelled because of OutOfMemoryError"

    invoke-static {v1, v2}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    invoke-direct {p0, v4}, Lcom/sec/android/sCloudSync/NetworkUtils/HttpClientThread;->removeThis(I)V

    goto :goto_0
.end method
