.class public interface abstract Lcom/sec/android/sCloudSync/Constants/SMemoContract$ExtData;
.super Ljava/lang/Object;
.source "SMemoContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/sCloudSync/Constants/SMemoContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ExtData"
.end annotation


# static fields
.field public static final KEY_DATA:Ljava/lang/String; = "Data"

.field public static final KEY_DUMMY:Ljava/lang/String; = "Dummy"

.field public static final KEY_EXTRA:Ljava/lang/String; = "ExtraInfo"

.field public static final KEY_KEY_NUM:Ljava/lang/String; = "Keynum"

.field public static final KEY_MEMOID:Ljava/lang/String; = "MemoID"

.field public static final KEY_POSITION:Ljava/lang/String; = "Position"

.field public static final KEY_SCALE_XY:Ljava/lang/String; = "ScaleXY"

.field public static final KEY_SEQUENCE:Ljava/lang/String; = "Sequence"

.field public static final KEY_SIZE:Ljava/lang/String; = "Size"

.field public static final KEY_TEXTINFO:Ljava/lang/String; = "TextInfo"

.field public static final KEY_TYPE:Ljava/lang/String; = "Type"
