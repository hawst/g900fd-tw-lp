.class public interface abstract Lcom/sec/android/sCloudSync/Constants/SyncUnit;
.super Ljava/lang/Object;
.source "SyncUnit.java"


# static fields
.field public static final BROWSER:Ljava/lang/String; = "BROWSER"

.field public static final BROWSER_KEY:Ljava/lang/String; = "BR"

.field public static final CALENDAR:Ljava/lang/String; = "CALENDAR"

.field public static final CONTACT:Ljava/lang/String; = "CONTACT"

.field public static final CONTACT_KEY:Ljava/lang/String; = "CT"

.field public static final GROUPS:Ljava/lang/String; = "GROUPS"

.field public static final GROUPS_KEY:Ljava/lang/String; = "GR"

.field public static final SBROWSER_SAVEDPAGES:Ljava/lang/String; = "SBROWSER_SAVEDPAGES"

.field public static final SBROWSER_SCRAP_KEY:Ljava/lang/String; = "SS"

.field public static final SBROWSER_TAB:Ljava/lang/String; = "SBROWSER_TAB"

.field public static final SBROWSER_TAB_KEY:Ljava/lang/String; = "ST"

.field public static final SMEMO:Ljava/lang/String; = "SMEMO"

.field public static final SMEMO_KEY:Ljava/lang/String; = "SM"

.field public static final SNOTE:Ljava/lang/String; = "SNOTE"

.field public static final SNOTE3:Ljava/lang/String; = "S-NOTE3"

.field public static final SNOTE3_KEY:Ljava/lang/String; = "N3"

.field public static final SNOTE_KEY:Ljava/lang/String; = "SN"

.field public static final SPLANNER_EVENT_KEY:Ljava/lang/String; = "PE"

.field public static final SPLANNER_TASK_KEY:Ljava/lang/String; = "PT"

.field public static final TMEMO:Ljava/lang/String; = "TMEMO"

.field public static final TMEMO2:Ljava/lang/String; = "TMEMO2"

.field public static final TMEMO2_KEY:Ljava/lang/String; = "T2"

.field public static final TMEMO_KEY:Ljava/lang/String; = "TM"
