.class public Lcom/sec/android/sCloudSync/Services/SMemoSyncService;
.super Landroid/app/Service;
.source "SMemoSyncService.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SMEMO-SYNC"


# instance fields
.field private final mBinder:Lcom/sec/android/sCloudSync/IDataSync/IDataSync$Stub;

.field private sync:Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Services/SMemoSyncService;->sync:Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;

    .line 54
    new-instance v0, Lcom/sec/android/sCloudSync/Services/SMemoSyncService$1;

    invoke-direct {v0, p0}, Lcom/sec/android/sCloudSync/Services/SMemoSyncService$1;-><init>(Lcom/sec/android/sCloudSync/Services/SMemoSyncService;)V

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Services/SMemoSyncService;->mBinder:Lcom/sec/android/sCloudSync/IDataSync/IDataSync$Stub;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/sCloudSync/Services/SMemoSyncService;)Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/sCloudSync/Services/SMemoSyncService;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Services/SMemoSyncService;->sync:Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Services/SMemoSyncService;->mBinder:Lcom/sec/android/sCloudSync/IDataSync/IDataSync$Stub;

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 38
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 39
    new-instance v0, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/Services/SMemoSyncService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Services/SMemoSyncService;->sync:Lcom/sec/android/sCloudSync/SyncAdapters/SMemoSyncAdapter;

    .line 40
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 51
    const-string v0, "SMEMO-SYNC"

    const-string v1, "SYNC SERVICE END"

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    return-void
.end method
