.class public final Lcom/sec/android/sCloudSync/Records/RecordSetItem;
.super Lcom/sec/android/sCloudSync/Records/RecordBase;
.source "RecordSetItem.java"


# static fields
.field private static final VALUE:Ljava/lang/String; = "value"


# instance fields
.field mValue:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;)V
    .locals 0
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "timeStamp"    # Ljava/lang/Long;
    .param p3, "value"    # Ljava/lang/String;

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Lcom/sec/android/sCloudSync/Records/RecordBase;-><init>(Ljava/lang/String;Ljava/lang/Long;)V

    .line 39
    iput-object p3, p0, Lcom/sec/android/sCloudSync/Records/RecordSetItem;->mValue:Ljava/lang/String;

    .line 40
    return-void
.end method


# virtual methods
.method public fromJSON(Ljava/lang/Object;)V
    .locals 2
    .param p1, "obj"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 44
    invoke-super {p0, p1}, Lcom/sec/android/sCloudSync/Records/RecordBase;->fromJSON(Ljava/lang/Object;)V

    move-object v0, p1

    .line 45
    check-cast v0, Lorg/json/JSONObject;

    .line 46
    .local v0, "json":Lorg/json/JSONObject;
    const-string v1, "value"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 47
    const-string v1, "value"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/sCloudSync/Records/RecordSetItem;->mValue:Ljava/lang/String;

    .line 48
    :cond_0
    return-void
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Records/RecordSetItem;->mValue:Ljava/lang/String;

    return-object v0
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/sec/android/sCloudSync/Records/RecordSetItem;->mValue:Ljava/lang/String;

    .line 35
    return-void
.end method

.method public toJSON()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 52
    invoke-super {p0}, Lcom/sec/android/sCloudSync/Records/RecordBase;->toJSON()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    .line 53
    .local v0, "json":Lorg/json/JSONObject;
    const-string v1, "value"

    iget-object v2, p0, Lcom/sec/android/sCloudSync/Records/RecordSetItem;->mValue:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 54
    return-object v0
.end method
