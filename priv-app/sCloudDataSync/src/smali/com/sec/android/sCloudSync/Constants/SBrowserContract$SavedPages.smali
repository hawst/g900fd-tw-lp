.class public interface abstract Lcom/sec/android/sCloudSync/Constants/SBrowserContract$SavedPages;
.super Ljava/lang/Object;
.source "SBrowserContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/sCloudSync/Constants/SBrowserContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SavedPages"
.end annotation


# static fields
.field public static final ACCOUNT_NAME:Ljava/lang/String; = "account_name"

.field public static final ACCOUNT_TYPE:Ljava/lang/String; = "account_type"

.field public static final CREATED:Ljava/lang/String; = "created"

.field public static final DATE:Ljava/lang/String; = "date"

.field public static final DESCRIPTION:Ljava/lang/String; = "description"

.field public static final DEVICE_ID:Ljava/lang/String; = "device_id"

.field public static final DEVICE_NAME:Ljava/lang/String; = "device_name"

.field public static final DIR_PATH:Ljava/lang/String; = "dir_path"

.field public static final FAVICON:Ljava/lang/String; = "favicon"

.field public static final IS_DELETED:Ljava/lang/String; = "is_deleted"

.field public static final IS_DIRTY:Ljava/lang/String; = "is_dirty"

.field public static final IS_IMAGE:Ljava/lang/String; = "is_image"

.field public static final IS_READ:Ljava/lang/String; = "is_read"

.field public static final MODIFIED:Ljava/lang/String; = "modified"

.field public static final PATH:Ljava/lang/String; = "path"

.field public static final SOURCE_ID:Ljava/lang/String; = "source_id"

.field public static final SYNC1:Ljava/lang/String; = "sync1"

.field public static final SYNC2:Ljava/lang/String; = "sync2"

.field public static final SYNC3:Ljava/lang/String; = "sync3"

.field public static final SYNC4:Ljava/lang/String; = "sync4"

.field public static final SYNC5:Ljava/lang/String; = "sync5"

.field public static final SYNCED:Ljava/lang/String; = "synced"

.field public static final TITLE:Ljava/lang/String; = "title"

.field public static final URL:Ljava/lang/String; = "url"

.field public static final USER_ADDED:Ljava/lang/String; = "user_added"

.field public static final VERSION:Ljava/lang/String; = "version"

.field public static final _ID:Ljava/lang/String; = "_id"
