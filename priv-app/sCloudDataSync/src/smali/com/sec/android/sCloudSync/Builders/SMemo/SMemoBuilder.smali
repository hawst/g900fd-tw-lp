.class public Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;
.super Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;
.source "SMemoBuilder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder$PostOperationListener;
    }
.end annotation


# static fields
.field private static final CALLER_IS_SYNCADAPTER:Ljava/lang/String; = "caller_is_syncadapter"

.field private static final EXTDATA:Ljava/lang/String; = "EXTDATA"

.field private static final PENMEMO:Ljava/lang/String; = "PENMEMO"

.field private static final SMEMO_COLUMNS:[Ljava/lang/String;

.field private static final SMEMO_EXT_DATA_COLUMNS:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "SMemoBuilder"


# instance fields
.field private final mFileDownloadList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mFileUploadList:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mFolderMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mOperations:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;"
        }
    .end annotation
.end field

.field private final mServerFileDeleteList:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 64
    const/16 v0, 0x1e

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "Title"

    aput-object v1, v0, v3

    const-string v1, "Date"

    aput-object v1, v0, v4

    const-string v1, "Tehme"

    aput-object v1, v0, v5

    const-string v1, "IsLock"

    aput-object v1, v0, v6

    const-string v1, "IsFavorite"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "Drawing"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "Tag"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "HasVoice"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "Dummy"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "Sync"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "Text"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "LastMode"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "Content"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "PhoneNum"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "SwitcherImage"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "SwitcherTitleImage"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "LinkedMemo"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "IsFolder"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "ParentID"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "CreateDate"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "PileOrder"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "UserThemePath"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "PenMemo_Type"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "PenMemo_ComponentName"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "PenMemo_isTemp"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "PenMemo_isMiniTemp"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "sync3"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "sync4"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "account_name"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "account_type"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->SMEMO_COLUMNS:[Ljava/lang/String;

    .line 97
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "Dummy"

    aput-object v1, v0, v3

    const-string v1, "ExtraInfo"

    aput-object v1, v0, v4

    const-string v1, "Keynum"

    aput-object v1, v0, v5

    const-string v1, "Position"

    aput-object v1, v0, v6

    const-string v1, "ScaleXY"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "Sequence"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "Size"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "TextInfo"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "Type"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->SMEMO_EXT_DATA_COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentProviderClient;Landroid/accounts/Account;Landroid/content/Context;)V
    .locals 1
    .param p1, "provider"    # Landroid/content/ContentProviderClient;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 113
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;-><init>(Landroid/content/ContentProviderClient;Landroid/accounts/Account;Landroid/content/Context;)V

    .line 58
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mFileUploadList:Landroid/util/SparseArray;

    .line 59
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mServerFileDeleteList:Landroid/util/SparseArray;

    .line 60
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mFileDownloadList:Ljava/util/HashMap;

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mOperations:Ljava/util/ArrayList;

    .line 62
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mFolderMap:Ljava/util/HashMap;

    .line 115
    invoke-direct {p0}, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->createFolderList()V

    .line 116
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mOperations:Ljava/util/ArrayList;

    return-object v0
.end method

.method private createExtData(Lorg/json/JSONObject;Ljava/util/ArrayList;Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder$PostOperationListener;)V
    .locals 9
    .param p1, "jsonObject"    # Lorg/json/JSONObject;
    .param p3, "postOperationListener"    # Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder$PostOperationListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder$PostOperationListener;",
            ")V"
        }
    .end annotation

    .prologue
    .local p2, "downloadFileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v8, 0x0

    .line 401
    const/4 v3, 0x0

    .line 403
    .local v3, "extValues":Landroid/content/ContentValues;
    :try_start_0
    const-string v6, "EXTDATA"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 404
    .local v1, "extDataList":Lorg/json/JSONArray;
    iget-object v6, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    sget-object v7, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->SMEMO_EXT_DATA_COLUMNS:[Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 405
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v5, v6, :cond_1

    .line 406
    invoke-virtual {v1, v5}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 407
    .local v2, "extDataObj":Lorg/json/JSONObject;
    iget-object v6, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v6, v2}, Lcom/sec/android/sCloudSync/Records/JSONParser;->fromJSON(Lorg/json/JSONObject;)Landroid/content/ContentValues;

    move-result-object v3

    .line 408
    const-string v6, "Data"

    invoke-virtual {v2, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 409
    .local v4, "filePath":Ljava/lang/String;
    if-eqz v4, :cond_0

    const-string v6, ""

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 410
    invoke-virtual {p2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 411
    const-string v6, "Data"

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 413
    :cond_0
    invoke-interface {p3, v3}, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder$PostOperationListener;->doPost(Landroid/content/ContentValues;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 405
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 419
    .end local v2    # "extDataObj":Lorg/json/JSONObject;
    .end local v4    # "filePath":Ljava/lang/String;
    :cond_1
    iget-object v6, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v6, v8}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 421
    .end local v1    # "extDataList":Lorg/json/JSONArray;
    .end local v5    # "i":I
    :goto_1
    return-void

    .line 415
    :catch_0
    move-exception v0

    .line 416
    .local v0, "e":Lorg/json/JSONException;
    :try_start_1
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 417
    const-string v6, "SMemoBuilder"

    const-string v7, "Unable to Parse"

    invoke-static {v6, v7}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 419
    iget-object v6, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v6, v8}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    goto :goto_1

    .end local v0    # "e":Lorg/json/JSONException;
    :catchall_0
    move-exception v6

    iget-object v7, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v7, v8}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    throw v6
.end method

.method private createFolderList()V
    .locals 10

    .prologue
    .line 141
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mProvider:Landroid/content/ContentProviderClient;

    sget-object v1, Lcom/sec/android/sCloudSync/Constants/SMemoContract;->SMEMO_URI:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "sync4"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "_id"

    aput-object v4, v2, v3

    const-string v3, "(IsFolder= 1 OR IsFolder= 2 ) AND account_type= \'com.osp.app.signin\'"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 151
    .local v6, "cursor":Landroid/database/Cursor;
    if-nez v6, :cond_0

    .line 161
    .end local v6    # "cursor":Landroid/database/Cursor;
    :goto_0
    return-void

    .line 147
    :catch_0
    move-exception v7

    .line 148
    .local v7, "e":Landroid/os/RemoteException;
    const-string v0, "SMemoBuilder"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getFolderList() :RemoteException"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 153
    .end local v7    # "e":Landroid/os/RemoteException;
    .restart local v6    # "cursor":Landroid/database/Cursor;
    :cond_0
    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 154
    .local v8, "idIndex":I
    const-string v0, "sync4"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 156
    .local v9, "syncidIndex":I
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 157
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mFolderMap:Ljava/util/HashMap;

    invoke-interface {v6, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v6, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 160
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method private createMemoValues(Lorg/json/JSONObject;Ljava/lang/String;JLjava/util/ArrayList;)Landroid/content/ContentValues;
    .locals 10
    .param p1, "jsonObject"    # Lorg/json/JSONObject;
    .param p2, "syncKey"    # Ljava/lang/String;
    .param p3, "timeStamp"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            "Ljava/lang/String;",
            "J",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/content/ContentValues;"
        }
    .end annotation

    .prologue
    .local p5, "downloadFileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v9, 0x0

    const/4 v6, 0x0

    .line 305
    const/4 v4, 0x0

    .line 306
    .local v4, "penJson":Lorg/json/JSONObject;
    const/4 v5, 0x0

    .line 308
    .local v5, "smemoValues":Landroid/content/ContentValues;
    iget-object v7, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    sget-object v8, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->SMEMO_COLUMNS:[Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 310
    :try_start_0
    const-string v7, "PENMEMO"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    .line 311
    iget-object v7, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v7, v4}, Lcom/sec/android/sCloudSync/Records/JSONParser;->fromJSON(Lorg/json/JSONObject;)Landroid/content/ContentValues;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 316
    iget-object v7, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v7, v6}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 319
    const-string v7, "SwitcherImage"

    invoke-virtual {v5, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 320
    .local v2, "filePath":Ljava/lang/String;
    if-eqz v2, :cond_0

    const-string v7, ""

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 321
    invoke-virtual {p5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 325
    :cond_0
    :try_start_1
    const-string v7, "Thumb"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    .line 332
    :goto_0
    if-eqz v2, :cond_1

    const-string v7, ""

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 333
    invoke-virtual {p5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 334
    const-string v7, "Thumb"

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 337
    :cond_1
    const-string v7, "sync1"

    invoke-virtual {v5, v7, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 338
    const-string v7, "sync2"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 339
    const-string v7, "dirty"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 340
    const-string v7, "deleted"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 342
    const-string v7, "Drawing"

    invoke-virtual {v5, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 343
    .local v0, "drawing":Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 345
    const/4 v7, 0x0

    :try_start_2
    invoke-static {v0, v7}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v3

    .line 346
    .local v3, "image":[B
    const-string v7, "Drawing"

    invoke-virtual {v5, v7, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2

    .end local v3    # "image":[B
    :cond_2
    move-object v6, v5

    .line 354
    .end local v0    # "drawing":Ljava/lang/String;
    .end local v2    # "filePath":Ljava/lang/String;
    :goto_1
    return-object v6

    .line 312
    :catch_0
    move-exception v1

    .line 313
    .local v1, "e":Lorg/json/JSONException;
    :try_start_3
    const-string v7, "SMemoBuilder"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Unable to parse:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 316
    iget-object v7, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v7, v6}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    goto :goto_1

    .end local v1    # "e":Lorg/json/JSONException;
    :catchall_0
    move-exception v7

    iget-object v8, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v8, v6}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    throw v7

    .line 326
    .restart local v2    # "filePath":Ljava/lang/String;
    :catch_1
    move-exception v1

    .line 328
    .restart local v1    # "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 329
    const/4 v2, 0x0

    goto :goto_0

    .line 347
    .end local v1    # "e":Lorg/json/JSONException;
    .restart local v0    # "drawing":Ljava/lang/String;
    :catch_2
    move-exception v1

    .line 348
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 349
    const-string v7, "SMemoBuilder"

    const-string v8, "Base64 Decoding Failed when decoding SMemo drawing"

    invoke-static {v7, v8}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private deleteExtData(JLjava/util/ArrayList;)Z
    .locals 12
    .param p1, "rowId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p3, "deleteList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v10, 0x1

    const/4 v11, 0x0

    .line 588
    const/4 v9, 0x0

    .line 590
    .local v9, "extCursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mProvider:Landroid/content/ContentProviderClient;

    sget-object v1, Lcom/sec/android/sCloudSync/Constants/SMemoContract;->SMEMO_EXTDATA_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "Data"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MemoID = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v9

    .line 596
    if-eqz v9, :cond_1

    .line 597
    :goto_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 598
    const-string v0, "Data"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 599
    .local v7, "dataIndex":I
    invoke-interface {v9, v7}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v6

    .line 601
    .local v6, "buff":[B
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v6}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 592
    .end local v6    # "buff":[B
    .end local v7    # "dataIndex":I
    :catch_0
    move-exception v8

    .line 593
    .local v8, "e":Landroid/os/RemoteException;
    const-string v0, "SMemoBuilder"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Delete(): Remote Exception:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v8}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v11

    .line 605
    .end local v8    # "e":Landroid/os/RemoteException;
    :goto_1
    return v0

    .line 603
    :cond_0
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_1
    move v0, v10

    .line 605
    goto :goto_1
.end method

.method private deleteLocalFiles(Ljava/util/ArrayList;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 641
    .local p1, "deleteFileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 642
    .local v3, "path":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 643
    .local v0, "filetoDelete":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 644
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v2

    .line 645
    .local v2, "isDeleteSuccess":Z
    if-nez v2, :cond_0

    .line 646
    const-string v4, "SMemoBuilder"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "File "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " could not be deleted"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 650
    .end local v0    # "filetoDelete":Ljava/io/File;
    .end local v2    # "isDeleteSuccess":Z
    .end local v3    # "path":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private deletePenMemo(Landroid/net/Uri;Ljava/util/ArrayList;)Z
    .locals 21
    .param p1, "memoUri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 515
    .local p2, "deleteList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v9, 0x0

    .line 516
    .local v9, "cursor":Landroid/database/Cursor;
    const/4 v15, 0x0

    .line 518
    .local v15, "penMemoCursor":Landroid/database/Cursor;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mProvider:Landroid/content/ContentProviderClient;

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "SwitcherImage"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "Thumb"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "_id"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-string v6, "IsFolder"

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v3, p1

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v9

    .line 526
    if-eqz v9, :cond_4

    .line 527
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 528
    const-string v2, "SwitcherImage"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    .line 529
    .local v18, "switcherImageIndex":I
    const-string v2, "Thumb"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    .line 530
    .local v19, "thumbIndex":I
    const-string v2, "_id"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    .line 532
    .local v13, "idIndex":I
    const-string v2, "IsFolder"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    .line 533
    .local v14, "isFolderIndex":I
    invoke-interface {v9, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v4, 0x1

    if-ne v2, v4, :cond_0

    .line 536
    sget-object v2, Lcom/sec/android/sCloudSync/Constants/SMemoContract;->SMEMO_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "caller_is_syncadapter"

    const-string v5, "true"

    invoke-virtual {v2, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 542
    .local v3, "memoUriWithoutAccount":Landroid/net/Uri;
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mProvider:Landroid/content/ContentProviderClient;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "_id"

    aput-object v6, v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ParentID= "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v9, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " AND ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "account_type"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "!= \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "com.osp.app.signin"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\' OR "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "account_type"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " IS NULL )"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    .line 548
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    .line 549
    .local v20, "where":Ljava/lang/StringBuilder;
    if-eqz v15, :cond_0

    invoke-interface {v15}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_0

    .line 550
    const-string v2, "_id"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " IN ("

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551
    :goto_0
    invoke-interface {v15}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 552
    const-string v2, "_id"

    invoke-interface {v15, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v15, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v4, 0x2c

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 559
    .end local v20    # "where":Ljava/lang/StringBuilder;
    :catch_0
    move-exception v10

    .line 560
    .local v10, "e":Landroid/os/RemoteException;
    invoke-virtual {v10}, Landroid/os/RemoteException;->printStackTrace()V

    .line 563
    .end local v3    # "memoUriWithoutAccount":Landroid/net/Uri;
    .end local v10    # "e":Landroid/os/RemoteException;
    :cond_0
    :goto_1
    move/from16 v0, v18

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 564
    move/from16 v0, v18

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 566
    :cond_1
    move/from16 v0, v19

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v8

    .line 568
    .local v8, "buff":[B
    if-eqz v8, :cond_2

    .line 569
    new-instance v12, Ljava/lang/String;

    invoke-direct {v12, v8}, Ljava/lang/String;-><init>([B)V

    .line 570
    .local v12, "filepath":Ljava/lang/String;
    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 573
    const-string v2, "/"

    invoke-virtual {v12, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v16

    .line 574
    .local v16, "split":[Ljava/lang/String;
    move-object/from16 v0, v16

    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    aget-object v11, v16, v2

    .line 575
    .local v11, "fileName":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v4, 0x0

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual {v12, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "stroke"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v9, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ".sfm"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 576
    .local v17, "strokeFilePath":Ljava/lang/String;
    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 579
    .end local v8    # "buff":[B
    .end local v11    # "fileName":Ljava/lang/String;
    .end local v12    # "filepath":Ljava/lang/String;
    .end local v13    # "idIndex":I
    .end local v14    # "isFolderIndex":I
    .end local v16    # "split":[Ljava/lang/String;
    .end local v17    # "strokeFilePath":Ljava/lang/String;
    .end local v18    # "switcherImageIndex":I
    .end local v19    # "thumbIndex":I
    :cond_2
    if-eqz v15, :cond_3

    .line 580
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    .line 581
    :cond_3
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 583
    :cond_4
    const/4 v2, 0x1

    :goto_2
    return v2

    .line 521
    :catch_1
    move-exception v10

    .line 522
    .restart local v10    # "e":Landroid/os/RemoteException;
    invoke-virtual {v10}, Landroid/os/RemoteException;->printStackTrace()V

    .line 523
    const/4 v2, 0x0

    goto :goto_2

    .line 554
    .end local v10    # "e":Landroid/os/RemoteException;
    .restart local v3    # "memoUriWithoutAccount":Landroid/net/Uri;
    .restart local v13    # "idIndex":I
    .restart local v14    # "isFolderIndex":I
    .restart local v18    # "switcherImageIndex":I
    .restart local v19    # "thumbIndex":I
    .restart local v20    # "where":Ljava/lang/StringBuilder;
    :cond_5
    :try_start_2
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 555
    const/16 v2, 0x29

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 557
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mProvider:Landroid/content/ContentProviderClient;

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentProviderClient;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_1
.end method

.method private fileTracking(Landroid/database/Cursor;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 13
    .param p1, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .local p2, "uploadFileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v4, 0x0

    .line 254
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 255
    .local v6, "deleteFileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v0, "sync1"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 257
    .local v12, "syncKey":Ljava/lang/String;
    if-eqz v12, :cond_3

    invoke-virtual {v12}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 258
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/sCloudSync/Constants/SMemoContract;->FILETRACKER_URI:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "FilePath"

    aput-object v5, v2, v3

    const/4 v3, 0x1

    const-string v5, "Checksum"

    aput-object v5, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "sMemoId=\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 263
    .local v11, "fileTrackingCursor":Landroid/database/Cursor;
    if-eqz v11, :cond_3

    .line 264
    const-string v0, "FilePath"

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 265
    .local v10, "filePathIndex":I
    const-string v0, "Checksum"

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 266
    .local v8, "fileCheckSumIndex":I
    :cond_0
    :goto_0
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 267
    invoke-interface {v11, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 268
    .local v9, "filePath":Ljava/lang/String;
    invoke-interface {v11, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 269
    .local v7, "fileCheckSum":Ljava/lang/String;
    invoke-virtual {p2, v9}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 270
    invoke-static {v9, v7}, Lcom/sec/android/sCloudSync/Tools/FileTool;->isSameFile(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 271
    invoke-virtual {p2, v9}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 273
    :cond_1
    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 276
    .end local v7    # "fileCheckSum":Ljava/lang/String;
    .end local v9    # "filePath":Ljava/lang/String;
    :cond_2
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 279
    .end local v8    # "fileCheckSumIndex":I
    .end local v10    # "filePathIndex":I
    .end local v11    # "fileTrackingCursor":Landroid/database/Cursor;
    :cond_3
    return-object v6
.end method

.method private insertExtData(Lorg/json/JSONObject;Ljava/util/ArrayList;Ljava/util/ArrayList;)Z
    .locals 1
    .param p1, "jsonObject"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 425
    .local p2, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .local p3, "downloadFileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder$1;

    invoke-direct {v0, p0, p2}, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder$1;-><init>(Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;Ljava/util/ArrayList;)V

    invoke-direct {p0, p1, p3, v0}, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->createExtData(Lorg/json/JSONObject;Ljava/util/ArrayList;Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder$PostOperationListener;)V

    .line 435
    const/4 v0, 0x1

    return v0
.end method

.method private insertPenMemo(Lorg/json/JSONObject;Ljava/lang/String;JLjava/util/ArrayList;Ljava/util/ArrayList;)Z
    .locals 9
    .param p1, "jsonObject"    # Lorg/json/JSONObject;
    .param p2, "syncKey"    # Ljava/lang/String;
    .param p3, "timeStamp"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            "Ljava/lang/String;",
            "J",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p5, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .local p6, "downloadFileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v8, 0x1

    .line 358
    sget-object v6, Lcom/sec/android/sCloudSync/Constants/SMemoContract;->SMEMO_URI:Landroid/net/Uri;

    .local v6, "memoUri":Landroid/net/Uri;
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    move-object v5, p6

    .line 359
    invoke-direct/range {v0 .. v5}, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->createMemoValues(Lorg/json/JSONObject;Ljava/lang/String;JLjava/util/ArrayList;)Landroid/content/ContentValues;

    move-result-object v7

    .line 361
    .local v7, "smemoValues":Landroid/content/ContentValues;
    if-nez v7, :cond_0

    .line 362
    const/4 v0, 0x0

    .line 373
    :goto_0
    return v0

    .line 364
    :cond_0
    const-string v0, "IsFolder"

    invoke-virtual {v7, v0}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_1

    .line 365
    const-string v0, "deleted"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 368
    :cond_1
    const-string v0, "caller_is_syncadapter"

    invoke-static {v6, v0}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_name"

    iget-object v2, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mAccount:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_type"

    iget-object v2, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mAccount:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v6

    .line 372
    invoke-static {v6}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v8

    .line 373
    goto :goto_0
.end method

.method private parseExtData(Landroid/database/Cursor;JLorg/json/JSONObject;Ljava/util/ArrayList;)Z
    .locals 15
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "rowId"    # J
    .param p4, "parsedJson"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "J",
            "Lorg/json/JSONObject;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 206
    .local p5, "uploadFileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v11, 0x0

    .line 207
    .local v11, "extCursor":Landroid/database/Cursor;
    new-instance v12, Lorg/json/JSONArray;

    invoke-direct {v12}, Lorg/json/JSONArray;-><init>()V

    .line 209
    .local v12, "extDataArray":Lorg/json/JSONArray;
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mProvider:Landroid/content/ContentProviderClient;

    sget-object v3, Lcom/sec/android/sCloudSync/Constants/SMemoContract;->SMEMO_EXTDATA_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "MemoID = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, p2

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v11

    .line 216
    if-eqz v11, :cond_0

    .line 217
    iget-object v2, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    sget-object v3, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->SMEMO_EXT_DATA_COLUMNS:[Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 218
    const/4 v13, 0x0

    .line 222
    .local v13, "extDataObject":Lorg/json/JSONObject;
    :goto_0
    :try_start_1
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 223
    iget-object v2, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v2, v11}, Lcom/sec/android/sCloudSync/Records/JSONParser;->toJSON(Landroid/database/Cursor;)Lorg/json/JSONObject;

    move-result-object v13

    .line 224
    invoke-virtual {v13}, Lorg/json/JSONObject;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 225
    const-string v2, "SMemoBuilder"

    const-string v3, "ExtData: Unable to parse "

    invoke-static {v2, v3}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 243
    :catch_0
    move-exception v9

    .line 244
    .local v9, "e":Lorg/json/JSONException;
    :try_start_2
    const-string v2, "SMemoBuilder"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unable to parse:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v9}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 246
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 249
    .end local v9    # "e":Lorg/json/JSONException;
    .end local v13    # "extDataObject":Lorg/json/JSONObject;
    :cond_0
    :goto_1
    const/4 v2, 0x1

    :goto_2
    return v2

    .line 212
    :catch_1
    move-exception v9

    .line 213
    .local v9, "e":Landroid/os/RemoteException;
    const-string v2, "SMemoBuilder"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Parse(): Remote Exception:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v9}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    const/4 v2, 0x0

    goto :goto_2

    .line 228
    .end local v9    # "e":Landroid/os/RemoteException;
    .restart local v13    # "extDataObject":Lorg/json/JSONObject;
    :cond_1
    :try_start_3
    const-string v2, "Data"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v8

    .line 229
    .local v8, "buff":[B
    new-instance v14, Ljava/lang/String;

    invoke-direct {v14, v8}, Ljava/lang/String;-><init>([B)V

    .line 230
    .local v14, "filepath":Ljava/lang/String;
    if-eqz v14, :cond_2

    const-string v2, ""

    invoke-virtual {v14, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 231
    const-string v2, "Data"

    invoke-virtual {v13, v2, v14}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 232
    move-object/from16 v0, p5

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 238
    .end local v8    # "buff":[B
    .end local v14    # "filepath":Ljava/lang/String;
    :cond_2
    :goto_3
    :try_start_4
    invoke-virtual {v12, v13}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 246
    :catchall_0
    move-exception v2

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    throw v2

    .line 234
    :catch_2
    move-exception v10

    .line 236
    .local v10, "e1":Lorg/json/JSONException;
    :try_start_5
    const-string v2, "SMemoBuilder"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "unable to Parse"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v10}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 241
    .end local v10    # "e1":Lorg/json/JSONException;
    :cond_3
    const-string v2, "EXTDATA"

    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v12}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 246
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    goto :goto_1
.end method

.method private parsePenMemo(Landroid/database/Cursor;JLorg/json/JSONObject;Ljava/util/ArrayList;)Z
    .locals 12
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "rowId"    # J
    .param p4, "parsedJson"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "J",
            "Lorg/json/JSONObject;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 165
    .local p5, "uploadFileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v6, 0x0

    .line 166
    .local v6, "penMemoJson":Lorg/json/JSONObject;
    iget-object v9, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    sget-object v10, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->SMEMO_COLUMNS:[Ljava/lang/String;

    invoke-virtual {v9, v10}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 169
    :try_start_0
    iget-object v9, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v9, p1}, Lcom/sec/android/sCloudSync/Records/JSONParser;->toJSON(Landroid/database/Cursor;)Lorg/json/JSONObject;

    move-result-object v6

    .line 171
    const-string v9, "ParentID"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 172
    .local v4, "parentId":J
    const-string v9, "IsFolder"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 173
    .local v3, "isFolder":I
    const-wide/16 v9, 0x0

    cmp-long v9, v4, v9

    if-eqz v9, :cond_0

    .line 174
    const-string v9, "sync3"

    iget-object v10, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mFolderMap:Ljava/util/HashMap;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v6, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 177
    :cond_0
    if-nez v3, :cond_2

    .line 178
    const-string v9, "SwitcherImage"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 179
    .local v7, "switcherFile":Ljava/lang/String;
    if-eqz v7, :cond_1

    const-string v9, ""

    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 180
    move-object/from16 v0, p5

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 183
    :cond_1
    const-string v9, "Thumb"

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    .line 184
    .local v1, "buff":[B
    if-eqz v1, :cond_2

    .line 185
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v1}, Ljava/lang/String;-><init>([B)V

    .line 186
    .local v8, "thumbFile":Ljava/lang/String;
    if-eqz v8, :cond_2

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 187
    const-string v9, "Thumb"

    invoke-virtual {v6, v9, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 188
    move-object/from16 v0, p5

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 193
    .end local v1    # "buff":[B
    .end local v7    # "switcherFile":Ljava/lang/String;
    .end local v8    # "thumbFile":Ljava/lang/String;
    :cond_2
    const-string v9, "PENMEMO"

    move-object/from16 v0, p4

    invoke-virtual {v0, v9, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 198
    iget-object v9, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 201
    const/4 v9, 0x1

    .end local v3    # "isFolder":I
    .end local v4    # "parentId":J
    :goto_0
    return v9

    .line 194
    :catch_0
    move-exception v2

    .line 195
    .local v2, "e":Lorg/json/JSONException;
    :try_start_1
    const-string v9, "SMemoBuilder"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "parsePenMemo:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v2}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 196
    const/4 v9, 0x0

    .line 198
    iget-object v10, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    goto :goto_0

    .end local v2    # "e":Lorg/json/JSONException;
    :catchall_0
    move-exception v9

    iget-object v10, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    throw v9
.end method

.method private updateExtData(Lorg/json/JSONObject;JLjava/util/ArrayList;)Z
    .locals 1
    .param p1, "jsonObject"    # Lorg/json/JSONObject;
    .param p2, "rowId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            "J",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 440
    .local p4, "downloadFileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder$2;

    invoke-direct {v0, p0, p2, p3}, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder$2;-><init>(Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;J)V

    invoke-direct {p0, p1, p4, v0}, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->createExtData(Lorg/json/JSONObject;Ljava/util/ArrayList;Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder$PostOperationListener;)V

    .line 450
    const/4 v0, 0x1

    return v0
.end method

.method private updatePenMemo(Lorg/json/JSONObject;Ljava/lang/String;JJLjava/util/ArrayList;)Z
    .locals 8
    .param p1, "jsonObject"    # Lorg/json/JSONObject;
    .param p2, "syncKey"    # Ljava/lang/String;
    .param p3, "timeStamp"    # J
    .param p5, "rowId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            "Ljava/lang/String;",
            "JJ",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 378
    .local p7, "downloadFileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    sget-object v0, Lcom/sec/android/sCloudSync/Constants/SMemoContract;->SMEMO_URI:Landroid/net/Uri;

    invoke-static {v0, p5, p6}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v6

    .local v6, "memoUri":Landroid/net/Uri;
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    move-object v5, p7

    .line 379
    invoke-direct/range {v0 .. v5}, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->createMemoValues(Lorg/json/JSONObject;Ljava/lang/String;JLjava/util/ArrayList;)Landroid/content/ContentValues;

    move-result-object v7

    .line 381
    .local v7, "smemoValues":Landroid/content/ContentValues;
    if-nez v7, :cond_0

    .line 382
    const/4 v0, 0x0

    .line 392
    :goto_0
    return v0

    .line 384
    :cond_0
    const-string v0, "caller_is_syncadapter"

    invoke-static {v6, v0}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_name"

    iget-object v2, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mAccount:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_type"

    iget-object v2, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mAccount:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v6

    .line 388
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mOperations:Ljava/util/ArrayList;

    invoke-static {v6}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 389
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mOperations:Ljava/util/ArrayList;

    sget-object v1, Lcom/sec/android/sCloudSync/Constants/SMemoContract;->SMEMO_EXTDATA_URI:Landroid/net/Uri;

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MemoID="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5, p6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 392
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public delete(Landroid/net/Uri;JLjava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "rowId"    # J
    .param p4, "selection"    # Ljava/lang/String;
    .param p5, "syncKey"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 610
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 612
    .local v0, "deleteList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    sget-object v2, Lcom/sec/android/sCloudSync/Constants/SMemoContract;->SMEMO_URI:Landroid/net/Uri;

    invoke-static {v2, p2, p3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "caller_is_syncadapter"

    const-string v4, "true"

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "account_name"

    iget-object v4, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mAccount:Landroid/accounts/Account;

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "account_type"

    iget-object v4, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mAccount:Landroid/accounts/Account;

    iget-object v4, v4, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 621
    .local v1, "memoUri":Landroid/net/Uri;
    invoke-direct {p0, v1, v0}, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->deletePenMemo(Landroid/net/Uri;Ljava/util/ArrayList;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 638
    :cond_0
    :goto_0
    return-void

    .line 625
    :cond_1
    invoke-direct {p0, p2, p3, v0}, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->deleteExtData(JLjava/util/ArrayList;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 628
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 629
    invoke-direct {p0, v0}, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->deleteLocalFiles(Ljava/util/ArrayList;)V

    .line 631
    :cond_2
    iget-object v2, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mProvider:Landroid/content/ContentProviderClient;

    invoke-virtual {v2, v1, v6, v6}, Landroid/content/ContentProviderClient;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 632
    iget-object v2, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mProvider:Landroid/content/ContentProviderClient;

    sget-object v3, Lcom/sec/android/sCloudSync/Constants/SMemoContract;->SMEMO_EXTDATA_URI:Landroid/net/Uri;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MemoID = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v6}, Landroid/content/ContentProviderClient;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 634
    if-eqz p5, :cond_0

    .line 636
    iget-object v2, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/sec/android/sCloudSync/Constants/SMemoContract;->FILETRACKER_URI:Landroid/net/Uri;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "sMemoId=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method public doApplyBatch()Z
    .locals 1

    .prologue
    .line 510
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mOperations:Ljava/util/ArrayList;

    invoke-virtual {p0, v0}, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->doApplyBatch(Ljava/util/ArrayList;)Z

    move-result v0

    return v0
.end method

.method public getFileDownloadList()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mFileDownloadList:Ljava/util/HashMap;

    return-object v0
.end method

.method public getFileUploadList()Landroid/util/SparseArray;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 130
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mFileUploadList:Landroid/util/SparseArray;

    return-object v0
.end method

.method public getOperations()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mOperations:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getServerFileDeleteList()Landroid/util/SparseArray;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 135
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mServerFileDeleteList:Landroid/util/SparseArray;

    return-object v0
.end method

.method public insert(Ljava/lang/String;Ljava/lang/String;J)Z
    .locals 10
    .param p1, "jsonString"    # Ljava/lang/String;
    .param p2, "syncKey"    # Ljava/lang/String;
    .param p3, "timeStamp"    # J

    .prologue
    const/4 v9, 0x0

    .line 455
    const/4 v2, 0x0

    .line 457
    .local v2, "jsonObject":Lorg/json/JSONObject;
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 458
    .local v7, "dowloadFileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 461
    .local v6, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :try_start_0
    new-instance v1, Lorg/json/JSONTokener;

    invoke-direct {v1, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lorg/json/JSONObject;

    move-object v2, v0
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, p0

    move-object v3, p2

    move-wide v4, p3

    .line 468
    invoke-direct/range {v1 .. v7}, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->insertPenMemo(Lorg/json/JSONObject;Ljava/lang/String;JLjava/util/ArrayList;Ljava/util/ArrayList;)Z

    move-result v1

    if-nez v1, :cond_0

    move v1, v9

    .line 480
    :goto_0
    return v1

    .line 462
    :catch_0
    move-exception v8

    .line 463
    .local v8, "e1":Lorg/json/JSONException;
    invoke-virtual {v8}, Lorg/json/JSONException;->printStackTrace()V

    move v1, v9

    .line 464
    goto :goto_0

    .line 472
    .end local v8    # "e1":Lorg/json/JSONException;
    :cond_0
    invoke-direct {p0, v2, v6, v7}, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->insertExtData(Lorg/json/JSONObject;Ljava/util/ArrayList;Ljava/util/ArrayList;)Z

    move-result v1

    if-nez v1, :cond_1

    move v1, v9

    .line 473
    goto :goto_0

    .line 476
    :cond_1
    invoke-virtual {p0, v6}, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->doApplyBatch(Ljava/util/ArrayList;)Z

    move-result v1

    if-nez v1, :cond_2

    move v1, v9

    .line 477
    goto :goto_0

    .line 479
    :cond_2
    iget-object v1, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mFileDownloadList:Ljava/util/HashMap;

    invoke-virtual {v1, p2, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 480
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public parse(Landroid/database/Cursor;JLjava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "rowId"    # J
    .param p4, "key"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 284
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 285
    .local v5, "uploadFileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    .local v4, "parsedJson":Lorg/json/JSONObject;
    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    .line 288
    invoke-direct/range {v0 .. v5}, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->parsePenMemo(Landroid/database/Cursor;JLorg/json/JSONObject;Ljava/util/ArrayList;)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v7

    .line 300
    :goto_0
    return-object v0

    :cond_0
    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    .line 292
    invoke-direct/range {v0 .. v5}, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->parseExtData(Landroid/database/Cursor;JLorg/json/JSONObject;Ljava/util/ArrayList;)Z

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v7

    .line 293
    goto :goto_0

    .line 296
    :cond_1
    invoke-direct {p0, p1, v5}, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->fileTracking(Landroid/database/Cursor;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v6

    .line 298
    .local v6, "deleteFileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mServerFileDeleteList:Landroid/util/SparseArray;

    long-to-int v1, p2

    invoke-virtual {v0, v1, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 299
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mFileUploadList:Landroid/util/SparseArray;

    long-to-int v1, p2

    invoke-virtual {v0, v1, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 300
    invoke-virtual {v4}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public update(Ljava/lang/String;JJLjava/lang/String;)Z
    .locals 10
    .param p1, "jsonString"    # Ljava/lang/String;
    .param p2, "timeStamp"    # J
    .param p4, "rowId"    # J
    .param p6, "syncKey"    # Ljava/lang/String;

    .prologue
    .line 485
    const/4 v2, 0x0

    .line 486
    .local v2, "jsonObject":Lorg/json/JSONObject;
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 489
    .local v8, "downloadFileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :try_start_0
    new-instance v1, Lorg/json/JSONTokener;

    invoke-direct {v1, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lorg/json/JSONObject;

    move-object v2, v0
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, p0

    move-object/from16 v3, p6

    move-wide v4, p2

    move-wide v6, p4

    .line 496
    invoke-direct/range {v1 .. v8}, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->updatePenMemo(Lorg/json/JSONObject;Ljava/lang/String;JJLjava/util/ArrayList;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 497
    const/4 v1, 0x0

    .line 504
    :goto_0
    return v1

    .line 490
    :catch_0
    move-exception v9

    .line 491
    .local v9, "e1":Lorg/json/JSONException;
    invoke-virtual {v9}, Lorg/json/JSONException;->printStackTrace()V

    .line 492
    const/4 v1, 0x0

    goto :goto_0

    .line 500
    .end local v9    # "e1":Lorg/json/JSONException;
    :cond_0
    invoke-direct {p0, v2, p4, p5, v8}, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->updateExtData(Lorg/json/JSONObject;JLjava/util/ArrayList;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 501
    const/4 v1, 0x0

    goto :goto_0

    .line 503
    :cond_1
    iget-object v1, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->mFileDownloadList:Ljava/util/HashMap;

    move-object/from16 v0, p6

    invoke-virtual {v1, v0, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 504
    const/4 v1, 0x1

    goto :goto_0
.end method
