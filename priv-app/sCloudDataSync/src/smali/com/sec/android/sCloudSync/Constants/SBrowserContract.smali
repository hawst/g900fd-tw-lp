.class public Lcom/sec/android/sCloudSync/Constants/SBrowserContract;
.super Ljava/lang/Object;
.source "SBrowserContract.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/sCloudSync/Constants/SBrowserContract$InternetSync;,
        Lcom/sec/android/sCloudSync/Constants/SBrowserContract$SavedPages;,
        Lcom/sec/android/sCloudSync/Constants/SBrowserContract$TABS;
    }
.end annotation


# static fields
.field public static final AUTHORITY:Ljava/lang/String; = "com.sec.android.app.sbrowser"

.field public static final AUTHORITY_URI:Landroid/net/Uri;

.field public static final CALLER_IS_SYNCADAPTER:Ljava/lang/String; = "caller_is_syncadapter"

.field public static final SBROWSERPACKAGE:Ljava/lang/String; = "com.sec.android.app.sbrowser"

.field public static final SBROWSER_SAVEDPAGES_URI:Landroid/net/Uri;

.field public static final SBROWSER_SYNC_STATE_URI:Landroid/net/Uri;

.field public static final SBROWSER_TAB_AUTHORITY_OLD:Ljava/lang/String; = "com.sec.android.app.sbrowser.tabs"

.field public static final SBROWSER_TAB_URI:Landroid/net/Uri;

.field public static final SBROWSER_TAB_URI_OLD:Landroid/net/Uri;

.field public static final SBrowser_INTERNETSYNC_URI:Landroid/net/Uri;

.field public static final SYNC_STATE_DATA:Ljava/lang/String; = "data"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 9
    const-string v0, "content://com.sec.android.app.sbrowser"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/sCloudSync/Constants/SBrowserContract;->AUTHORITY_URI:Landroid/net/Uri;

    .line 11
    sget-object v0, Lcom/sec/android/sCloudSync/Constants/SBrowserContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v1, "tabs"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/sCloudSync/Constants/SBrowserContract;->SBROWSER_TAB_URI:Landroid/net/Uri;

    .line 14
    const-string v0, "content://com.sec.android.app.sbrowser.tabs/tabs"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/sCloudSync/Constants/SBrowserContract;->SBROWSER_TAB_URI_OLD:Landroid/net/Uri;

    .line 16
    sget-object v0, Lcom/sec/android/sCloudSync/Constants/SBrowserContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v1, "savepage"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/sCloudSync/Constants/SBrowserContract;->SBROWSER_SAVEDPAGES_URI:Landroid/net/Uri;

    .line 19
    sget-object v0, Lcom/sec/android/sCloudSync/Constants/SBrowserContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v1, "internet_sync"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/sCloudSync/Constants/SBrowserContract;->SBrowser_INTERNETSYNC_URI:Landroid/net/Uri;

    .line 22
    const-string v0, "content://com.sec.android.app.sbrowser/syncstate"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/sCloudSync/Constants/SBrowserContract;->SBROWSER_SYNC_STATE_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    return-void
.end method
