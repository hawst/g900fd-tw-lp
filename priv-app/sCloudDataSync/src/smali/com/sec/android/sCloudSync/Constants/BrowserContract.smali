.class public Lcom/sec/android/sCloudSync/Constants/BrowserContract;
.super Ljava/lang/Object;
.source "BrowserContract.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/sCloudSync/Constants/BrowserContract$Searches;,
        Lcom/sec/android/sCloudSync/Constants/BrowserContract$History;,
        Lcom/sec/android/sCloudSync/Constants/BrowserContract$Accounts;,
        Lcom/sec/android/sCloudSync/Constants/BrowserContract$Bookmarks;,
        Lcom/sec/android/sCloudSync/Constants/BrowserContract$HistoryColumns;,
        Lcom/sec/android/sCloudSync/Constants/BrowserContract$ImageColumns;,
        Lcom/sec/android/sCloudSync/Constants/BrowserContract$CommonColumns;,
        Lcom/sec/android/sCloudSync/Constants/BrowserContract$SyncColumns;,
        Lcom/sec/android/sCloudSync/Constants/BrowserContract$ChromeSyncColumns;,
        Lcom/sec/android/sCloudSync/Constants/BrowserContract$BaseSyncColumns;
    }
.end annotation


# static fields
.field public static final AUTHORITY:Ljava/lang/String; = "com.android.browser"

.field public static final AUTHORITY_URI:Landroid/net/Uri;

.field public static final CALLER_IS_SYNCADAPTER:Ljava/lang/String; = "caller_is_syncadapter"

.field public static final PARAM_LIMIT:Ljava/lang/String; = "limit"

.field public static final SBROWSER_AUTHORITY:Ljava/lang/String; = "com.sec.android.app.sbrowser.browser"

.field public static final SBROWSER_AUTHORITY_URI:Landroid/net/Uri;

.field public static final SBROWSER_CONTENT_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 31
    const-string v0, "content://com.android.browser"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/sCloudSync/Constants/BrowserContract;->AUTHORITY_URI:Landroid/net/Uri;

    .line 34
    const-string v0, "content://com.sec.android.app.sbrowser.browser"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/sCloudSync/Constants/BrowserContract;->SBROWSER_AUTHORITY_URI:Landroid/net/Uri;

    .line 35
    sget-object v0, Lcom/sec/android/sCloudSync/Constants/BrowserContract;->SBROWSER_AUTHORITY_URI:Landroid/net/Uri;

    const-string v1, "bookmarks"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/sCloudSync/Constants/BrowserContract;->SBROWSER_CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 384
    return-void
.end method
