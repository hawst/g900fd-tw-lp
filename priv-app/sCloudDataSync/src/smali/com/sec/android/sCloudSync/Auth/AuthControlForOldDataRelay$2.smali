.class Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay$2;
.super Ljava/lang/Object;
.source "AuthControlForOldDataRelay.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->getAuthInformation(Landroid/content/Context;Ljava/lang/String;Z)Lcom/sec/android/sCloudSync/Auth/AuthManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;

.field final synthetic val$bFresh:Z

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;ZLandroid/content/Context;)V
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay$2;->this$0:Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;

    iput-boolean p2, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay$2;->val$bFresh:Z

    iput-object p3, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay$2;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 118
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay$2;->this$0:Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;

    # getter for: Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->mService:Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;
    invoke-static {v1}, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->access$000(Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;)Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 119
    iget-object v1, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay$2;->this$0:Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;

    iget-object v2, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay$2;->this$0:Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;

    # getter for: Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->mService:Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;
    invoke-static {v2}, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->access$000(Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;)Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay$2;->val$bFresh:Z

    invoke-interface {v2, v3}, Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;->getAuthInformation(Z)Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;

    move-result-object v2

    # setter for: Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->mAuth:Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;
    invoke-static {v1, v2}, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->access$202(Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;)Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 126
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay$2;->this$0:Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;

    # getter for: Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->mService:Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;
    invoke-static {v1}, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->access$000(Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;)Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 128
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay$2;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay$2;->this$0:Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;

    # getter for: Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->mPushClientConnection:Landroid/content/ServiceConnection;
    invoke-static {v2}, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->access$300(Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;)Landroid/content/ServiceConnection;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    .line 132
    :goto_1
    iget-object v1, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay$2;->this$0:Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->mService:Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;
    invoke-static {v1, v2}, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->access$002(Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;)Lcom/sec/android/sCloudRelayData/RemoteService/IsCloudRemoteService;

    .line 135
    :cond_1
    iget-object v1, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay$2;->this$0:Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;

    # getter for: Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->syncLock:Ljava/lang/Object;
    invoke-static {v1}, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->access$100(Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 136
    :try_start_2
    iget-object v1, p0, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay$2;->this$0:Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;

    # getter for: Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->syncLock:Ljava/lang/Object;
    invoke-static {v1}, Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;->access$100(Lcom/sec/android/sCloudSync/Auth/AuthControlForOldDataRelay;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notify()V

    .line 137
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 138
    return-void

    .line 121
    :catch_0
    move-exception v0

    .line 122
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 123
    const-string v1, "AuthControl"

    const-string v2, "getAuthInformation.run(): FAILED - RemoteService."

    invoke-static {v1, v2}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 129
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 130
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v1, "AuthControl"

    const-string v2, "Illegal Argument Exception when unbinding service"

    invoke-static {v1, v2}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 137
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catchall_0
    move-exception v1

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v1
.end method
