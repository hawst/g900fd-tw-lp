.class public interface abstract Lcom/sec/android/sCloudSync/Constants/SPlannerTaskContract$Task;
.super Ljava/lang/Object;
.source "SPlannerTaskContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/sCloudSync/Constants/SPlannerTaskContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Task"
.end annotation


# static fields
.field public static final ACCOUNTKEY:Ljava/lang/String; = "accountKey"

.field public static final ACCOUNTNAME:Ljava/lang/String; = "accountName"

.field public static final BODY:Ljava/lang/String; = "body"

.field public static final BODYTYPE:Ljava/lang/String; = "bodyType"

.field public static final BODY_SIZE:Ljava/lang/String; = "body_size"

.field public static final BODY_TRUNCATED:Ljava/lang/String; = "body_truncated"

.field public static final CATEGORY1:Ljava/lang/String; = "category1"

.field public static final CATEGORY2:Ljava/lang/String; = "category2"

.field public static final CATEGORY3:Ljava/lang/String; = "category3"

.field public static final CLIENTID:Ljava/lang/String; = "clientId"

.field public static final COMPLETE:Ljava/lang/String; = "complete"

.field public static final DATE_COMPLETED:Ljava/lang/String; = "date_completed"

.field public static final DELETED:Ljava/lang/String; = "deleted"

.field public static final DISPLAY_NAME:Ljava/lang/String; = "displayName"

.field public static final DUE_DATE:Ljava/lang/String; = "due_date"

.field public static final GROUPID:Ljava/lang/String; = "groupId"

.field public static final IMPORTANCE:Ljava/lang/String; = "importance"

.field public static final MAILBOXKEY:Ljava/lang/String; = "mailboxKey"

.field public static final PARENTID:Ljava/lang/String; = "parentId"

.field public static final PREVIOUSID:Ljava/lang/String; = "previousId"

.field public static final READ:Ljava/lang/String; = "read"

.field public static final RECURRENCE_DAY_OF_MONTH:Ljava/lang/String; = "recurrence_day_of_month"

.field public static final RECURRENCE_DAY_OF_WEEK:Ljava/lang/String; = "recurrence_day_of_week"

.field public static final RECURRENCE_DEAD_OCCUR:Ljava/lang/String; = "recurrence_dead_occur"

.field public static final RECURRENCE_INTERVAL:Ljava/lang/String; = "recurrence_interval"

.field public static final RECURRENCE_MONTH_OF_YEAR:Ljava/lang/String; = "recurrence_month_of_year"

.field public static final RECURRENCE_OCCURANCE:Ljava/lang/String; = "recurrence_occurrences"

.field public static final RECURRENCE_REGENERATE:Ljava/lang/String; = "recurrence_regenerate"

.field public static final RECURRENCE_START:Ljava/lang/String; = "recurrence_start"

.field public static final RECURRENCE_TYPE:Ljava/lang/String; = "recurrence_type"

.field public static final RECURRENCE_UNTIL:Ljava/lang/String; = "recurrence_until"

.field public static final RECURRENCE_WEEK_OF_MONTH:Ljava/lang/String; = "recurrence_week_of_month"

.field public static final REMINDER_SET:Ljava/lang/String; = "reminder_set"

.field public static final REMINDER_TIME:Ljava/lang/String; = "reminder_time"

.field public static final REMINDER_TYPE:Ljava/lang/String; = "reminder_type"

.field public static final SENSITIVITY:Ljava/lang/String; = "sensitivity"

.field public static final SOURCEID:Ljava/lang/String; = "sourceid"

.field public static final START_DATE:Ljava/lang/String; = "start_date"

.field public static final SUBJECT:Ljava/lang/String; = "subject"

.field public static final SYNCDIRTY:Ljava/lang/String; = "_sync_dirty"

.field public static final SYNCTIME:Ljava/lang/String; = "syncTime"

.field public static final SYNC_SERVER_ID:Ljava/lang/String; = "syncServerId"

.field public static final UTC_DUE_DATE:Ljava/lang/String; = "utc_due_date"

.field public static final UTC_START_DATE:Ljava/lang/String; = "utc_start_date"

.field public static final _ID:Ljava/lang/String; = "_id"
