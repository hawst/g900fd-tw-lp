.class public Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;
.super Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;
.source "SPlannerEventBuilder.java"


# static fields
.field private static final EVENT:Ljava/lang/String; = "EVENT"

.field private static final EVENT_COLUMNS:[Ljava/lang/String;

.field private static final EXTENDED_PROPERTIES_COLUMNS:[Ljava/lang/String;

.field private static final EXTRAPROPERTIES:Ljava/lang/String; = "EXTRAPROPERTIES"

.field private static final MAP:Ljava/lang/String; = "MAP"

.field private static final MAPURI:Landroid/net/Uri;

.field private static final MAP_COLUMNS:[Ljava/lang/String;

.field private static final REMINDERS:Ljava/lang/String; = "REMINDERS"

.field private static final REMINDER_COLUMNS:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "sPlannerEventBuilder"


# instance fields
.field private mCalendarId:I

.field private mEventUri:Landroid/net/Uri;

.field private mExtendedPropertiesUri:Landroid/net/Uri;

.field private mMapUri:Landroid/net/Uri;

.field private mOperations:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;"
        }
    .end annotation
.end field

.field private mReminderUri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 53
    const-string v0, "content://com.android.calendar/maps"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->MAPURI:Landroid/net/Uri;

    .line 63
    const/16 v0, 0x24

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "title"

    aput-object v1, v0, v3

    const-string v1, "eventLocation"

    aput-object v1, v0, v4

    const-string v1, "description"

    aput-object v1, v0, v5

    const/4 v1, 0x3

    const-string v2, "eventStatus"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "eventColor"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "eventColor_index"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "selfAttendeeStatus"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "dtstart"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "dtend"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "eventTimezone"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "eventEndTimezone"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "allDay"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "accessLevel"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "availability"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "hasAlarm"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "hasExtendedProperties"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "rrule"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "rdate"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "exrule"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "exdate"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "original_sync_id"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "original_id"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "originalInstanceTime"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "originalAllDay"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "lastDate"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "hasAttendeeData"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "calendar_id"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "guestsCanInviteOthers"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "guestsCanModify"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "guestsCanSeeGuests"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "organizer"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "_sync_id"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "latitude"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "longitude"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "availabilityStatus"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->EVENT_COLUMNS:[Ljava/lang/String;

    .line 86
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "method"

    aput-object v1, v0, v3

    const-string v1, "minutes"

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->REMINDER_COLUMNS:[Ljava/lang/String;

    .line 90
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "name"

    aput-object v1, v0, v3

    const-string v1, "value"

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->EXTENDED_PROPERTIES_COLUMNS:[Ljava/lang/String;

    .line 94
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "map"

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->MAP_COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentProviderClient;Landroid/accounts/Account;Landroid/content/Context;)V
    .locals 2
    .param p1, "provider"    # Landroid/content/ContentProviderClient;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 97
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;-><init>(Landroid/content/ContentProviderClient;Landroid/accounts/Account;Landroid/content/Context;)V

    .line 55
    sget-object v0, Landroid/provider/CalendarContract$ExtendedProperties;->CONTENT_URI:Landroid/net/Uri;

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mExtendedPropertiesUri:Landroid/net/Uri;

    .line 56
    sget-object v0, Landroid/provider/CalendarContract$Reminders;->CONTENT_URI:Landroid/net/Uri;

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mReminderUri:Landroid/net/Uri;

    .line 57
    sget-object v0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->MAPURI:Landroid/net/Uri;

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mMapUri:Landroid/net/Uri;

    .line 58
    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mEventUri:Landroid/net/Uri;

    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mOperations:Ljava/util/ArrayList;

    .line 61
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mCalendarId:I

    .line 98
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mOperations:Ljava/util/ArrayList;

    .line 100
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mExtendedPropertiesUri:Landroid/net/Uri;

    const-string v1, "caller_is_syncadapter"

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mExtendedPropertiesUri:Landroid/net/Uri;

    .line 101
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mExtendedPropertiesUri:Landroid/net/Uri;

    iget-object v1, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mAccount:Landroid/accounts/Account;

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->addAccountParameter(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mExtendedPropertiesUri:Landroid/net/Uri;

    .line 103
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mReminderUri:Landroid/net/Uri;

    const-string v1, "caller_is_syncadapter"

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mReminderUri:Landroid/net/Uri;

    .line 104
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mReminderUri:Landroid/net/Uri;

    iget-object v1, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mAccount:Landroid/accounts/Account;

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->addAccountParameter(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mReminderUri:Landroid/net/Uri;

    .line 106
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mMapUri:Landroid/net/Uri;

    const-string v1, "caller_is_syncadapter"

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mMapUri:Landroid/net/Uri;

    .line 107
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mMapUri:Landroid/net/Uri;

    iget-object v1, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mAccount:Landroid/accounts/Account;

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->addAccountParameter(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mMapUri:Landroid/net/Uri;

    .line 109
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mEventUri:Landroid/net/Uri;

    const-string v1, "caller_is_syncadapter"

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mEventUri:Landroid/net/Uri;

    .line 110
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mEventUri:Landroid/net/Uri;

    iget-object v1, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mAccount:Landroid/accounts/Account;

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->addAccountParameter(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mEventUri:Landroid/net/Uri;

    .line 112
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mAccount:Landroid/accounts/Account;

    iget-object v1, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mContext:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Util/SyncAccountManager;->addSamsungCalendar(Landroid/accounts/Account;Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mCalendarId:I

    .line 113
    return-void
.end method

.method private static addAccountParameter(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;
    .locals 3
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 266
    const-string v1, "account_name"

    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {p0, v1, v2}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addQeuryParameter(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 267
    .local v0, "AccountUri":Landroid/net/Uri;
    const-string v1, "account_type"

    iget-object v2, p1, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addQeuryParameter(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 268
    return-object v0
.end method

.method private deleteExtras(J)V
    .locals 12
    .param p1, "eventId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 336
    const-string v11, "event_id = ?"

    .line 337
    .local v11, "where":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 340
    .local v4, "whereArgs":[Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mProvider:Landroid/content/ContentProviderClient;

    iget-object v1, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mMapUri:Landroid/net/Uri;

    const-string v3, "event_id = ?"

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 341
    .local v7, "mapCursor":Landroid/database/Cursor;
    if-eqz v7, :cond_1

    .line 342
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 343
    const-string v0, "_id"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    .line 344
    .local v9, "mapId":J
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mMapUri:Landroid/net/Uri;

    invoke-static {v0, v9, v10}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v8

    .line 346
    .local v8, "mapDeleteUri":Landroid/net/Uri;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mProvider:Landroid/content/ContentProviderClient;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v8, v1, v2}, Landroid/content/ContentProviderClient;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 351
    .end local v8    # "mapDeleteUri":Landroid/net/Uri;
    .end local v9    # "mapId":J
    :cond_0
    :goto_0
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 354
    :cond_1
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mProvider:Landroid/content/ContentProviderClient;

    iget-object v1, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mReminderUri:Landroid/net/Uri;

    const-string v2, "event_id = ?"

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/ContentProviderClient;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 355
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mProvider:Landroid/content/ContentProviderClient;

    iget-object v1, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mExtendedPropertiesUri:Landroid/net/Uri;

    const-string v2, "event_id = ?"

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/ContentProviderClient;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 356
    return-void

    .line 347
    .restart local v8    # "mapDeleteUri":Landroid/net/Uri;
    .restart local v9    # "mapId":J
    :catch_0
    move-exception v6

    .line 348
    .local v6, "e":Landroid/os/RemoteException;
    const-string v0, "sPlannerEventBuilder"

    const-string v1, "Remote Exception in deleteExtras"

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private insertEvent(Lorg/json/JSONObject;Ljava/lang/String;J)Landroid/net/Uri;
    .locals 15
    .param p1, "jsonObject"    # Lorg/json/JSONObject;
    .param p2, "syncKey"    # Ljava/lang/String;
    .param p3, "timeStamp"    # J

    .prologue
    .line 384
    const/4 v12, 0x0

    .line 385
    .local v12, "eventValue":Landroid/content/ContentValues;
    iget-object v1, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    sget-object v2, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->EVENT_COLUMNS:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 388
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    const-string v2, "EVENT"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/sCloudSync/Records/JSONParser;->fromJSON(Lorg/json/JSONObject;)Landroid/content/ContentValues;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v12

    .line 395
    iget-object v1, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 397
    invoke-virtual {v12}, Landroid/content/ContentValues;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 398
    const-string v1, "sPlannerEventBuilder"

    const-string v2, "insert: unable to parse;"

    invoke-static {v1, v2}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 399
    const/4 v13, 0x0

    .line 440
    :goto_0
    return-object v13

    .line 389
    :catch_0
    move-exception v9

    .line 390
    .local v9, "e1":Lorg/json/JSONException;
    invoke-virtual {v9}, Lorg/json/JSONException;->printStackTrace()V

    .line 391
    const-string v1, "sPlannerEventBuilder"

    const-string v2, "insert: unable to parse;"

    invoke-static {v1, v2}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    const/4 v13, 0x0

    goto :goto_0

    .line 402
    .end local v9    # "e1":Lorg/json/JSONException;
    :cond_0
    const-string v1, "original_sync_id"

    invoke-virtual {v12, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 403
    const/4 v7, 0x0

    .line 404
    .local v7, "cursor":Landroid/database/Cursor;
    const-wide/16 v10, -0x1

    .line 406
    .local v10, "eventId":J
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mProvider:Landroid/content/ContentProviderClient;

    iget-object v2, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mEventUri:Landroid/net/Uri;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v3, v4

    const-string v4, "_sync_id = ? "

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v14, "original_sync_id"

    invoke-virtual {v12, v14}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v5, v6

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v7

    .line 413
    if-eqz v7, :cond_2

    .line 414
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 415
    const/4 v1, 0x0

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 417
    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 419
    :cond_2
    const-string v1, "original_id"

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v12, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 422
    .end local v7    # "cursor":Landroid/database/Cursor;
    .end local v10    # "eventId":J
    :cond_3
    const-string v1, "_sync_id"

    move-object/from16 v0, p2

    invoke-virtual {v12, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 423
    const-string v1, "sync_data5"

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v12, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 425
    const-string v1, "hasAttendeeData"

    invoke-virtual {v12, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 426
    const-string v1, "hasAttendeeData"

    invoke-virtual {v12, v1}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 428
    :cond_4
    const-string v1, "selfAttendeeStatus"

    invoke-virtual {v12, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 429
    const-string v1, "selfAttendeeStatus"

    invoke-virtual {v12, v1}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 431
    :cond_5
    const-string v1, "calendar_id"

    iget v2, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mCalendarId:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v12, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 433
    const/4 v13, 0x0

    .line 435
    .local v13, "resultUri":Landroid/net/Uri;
    :try_start_2
    iget-object v1, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mProvider:Landroid/content/ContentProviderClient;

    iget-object v2, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mEventUri:Landroid/net/Uri;

    invoke-virtual {v1, v2, v12}, Landroid/content/ContentProviderClient;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v13

    goto/16 :goto_0

    .line 407
    .end local v13    # "resultUri":Landroid/net/Uri;
    .restart local v7    # "cursor":Landroid/database/Cursor;
    .restart local v10    # "eventId":J
    :catch_1
    move-exception v8

    .line 408
    .local v8, "e":Landroid/os/RemoteException;
    invoke-virtual {v8}, Landroid/os/RemoteException;->printStackTrace()V

    .line 409
    const-string v1, "sPlannerEventBuilder"

    const-string v2, "insert: unable to query"

    invoke-static {v1, v2}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 436
    .end local v7    # "cursor":Landroid/database/Cursor;
    .end local v8    # "e":Landroid/os/RemoteException;
    .end local v10    # "eventId":J
    .restart local v13    # "resultUri":Landroid/net/Uri;
    :catch_2
    move-exception v8

    .line 437
    .restart local v8    # "e":Landroid/os/RemoteException;
    const-string v1, "sPlannerEventBuilder"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "insertEvent:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v8}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    const/4 v13, 0x0

    goto/16 :goto_0
.end method

.method private insertExtendedProperties(JLorg/json/JSONObject;)V
    .locals 8
    .param p1, "eventId"    # J
    .param p3, "jsonObject"    # Lorg/json/JSONObject;

    .prologue
    .line 445
    const-string v6, "EXTRAPROPERTIES"

    invoke-virtual {p3, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 446
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 449
    .local v1, "extPropertyValue":Landroid/content/ContentValues;
    const/4 v3, 0x0

    .line 450
    .local v3, "extraList":Lorg/json/JSONArray;
    :try_start_0
    const-string v6, "EXTRAPROPERTIES"

    invoke-virtual {p3, v6}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 451
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v2

    .line 452
    .local v2, "extraCount":I
    iget-object v6, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    sget-object v7, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->EVENT_COLUMNS:[Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 453
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-ge v5, v2, :cond_3

    .line 454
    invoke-virtual {v3, v5}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 455
    .local v4, "extraObj":Lorg/json/JSONObject;
    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    .line 456
    iget-object v6, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v6, v4}, Lcom/sec/android/sCloudSync/Records/JSONParser;->fromJSON(Lorg/json/JSONObject;)Landroid/content/ContentValues;

    move-result-object v1

    .line 457
    if-nez v1, :cond_1

    .line 458
    const-string v6, "sPlannerEventBuilder"

    const-string v7, "insertExtProperties: unable to parse;"

    invoke-static {v6, v7}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 453
    :cond_0
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 461
    :cond_1
    const-string v6, "event_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    .line 463
    :try_start_1
    iget-object v6, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mProvider:Landroid/content/ContentProviderClient;

    iget-object v7, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mExtendedPropertiesUri:Landroid/net/Uri;

    invoke-virtual {v6, v7, v1}, Landroid/content/ContentProviderClient;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v6

    if-nez v6, :cond_0

    .line 464
    const-string v6, "sPlannerEventBuilder"

    const-string v7, "insertExtProperties: unable to insert Extra properties"

    invoke-static {v6, v7}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 466
    :catch_0
    move-exception v0

    .line 467
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_2
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 471
    .end local v0    # "e":Landroid/os/RemoteException;
    .end local v2    # "extraCount":I
    .end local v4    # "extraObj":Lorg/json/JSONObject;
    .end local v5    # "i":I
    :catch_1
    move-exception v0

    .line 472
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 473
    const-string v6, "sPlannerEventBuilder"

    const-string v7, "insertExtras: Unable to insert "

    invoke-static {v6, v7}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 476
    .end local v0    # "e":Lorg/json/JSONException;
    .end local v1    # "extPropertyValue":Landroid/content/ContentValues;
    .end local v3    # "extraList":Lorg/json/JSONArray;
    :cond_2
    :goto_2
    return-void

    .line 470
    .restart local v1    # "extPropertyValue":Landroid/content/ContentValues;
    .restart local v2    # "extraCount":I
    .restart local v3    # "extraList":Lorg/json/JSONArray;
    .restart local v5    # "i":I
    :cond_3
    :try_start_3
    iget-object v6, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_2
.end method

.method private insertExtras(JLorg/json/JSONObject;)Z
    .locals 1
    .param p1, "eventId"    # J
    .param p3, "jsonObject"    # Lorg/json/JSONObject;

    .prologue
    .line 567
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->insertExtendedProperties(JLorg/json/JSONObject;)V

    .line 570
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->insertReminders(JLorg/json/JSONObject;)Z

    .line 573
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->insertMap(JLorg/json/JSONObject;)Z

    .line 575
    const/4 v0, 0x1

    return v0
.end method

.method private insertMap(JLorg/json/JSONObject;)Z
    .locals 10
    .param p1, "eventId"    # J
    .param p3, "jsonObject"    # Lorg/json/JSONObject;

    .prologue
    const/4 v6, 0x0

    .line 522
    const-string v7, "MAP"

    invoke-virtual {p3, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 523
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 524
    .local v5, "mapValues":Landroid/content/ContentValues;
    const/4 v2, 0x0

    .line 527
    .local v2, "mapJson":Lorg/json/JSONObject;
    :try_start_0
    const-string v7, "MAP"

    invoke-virtual {p3, v7}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 533
    iget-object v7, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    sget-object v8, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->MAP_COLUMNS:[Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 535
    :try_start_1
    iget-object v7, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v7, v2}, Lcom/sec/android/sCloudSync/Records/JSONParser;->fromJSON(Lorg/json/JSONObject;)Landroid/content/ContentValues;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v5

    .line 542
    iget-object v7, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 543
    sget-object v7, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->MAP_COLUMNS:[Ljava/lang/String;

    aget-object v7, v7, v6

    invoke-virtual {v5, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 545
    .local v3, "mapString":Ljava/lang/String;
    const/4 v1, 0x0

    .line 546
    .local v1, "image":[B
    if-eqz v3, :cond_0

    .line 547
    invoke-static {v3, v6}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    .line 548
    const-string v7, "map"

    invoke-virtual {v5, v7, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 549
    const-string v7, "event_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 552
    :try_start_2
    iget-object v7, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mProvider:Landroid/content/ContentProviderClient;

    iget-object v8, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mMapUri:Landroid/net/Uri;

    invoke-virtual {v7, v8, v5}, Landroid/content/ContentProviderClient;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v4

    .line 553
    .local v4, "mapUri":Landroid/net/Uri;
    if-nez v4, :cond_0

    .line 554
    const-string v7, "sPlannerEventBuilder"

    const-string v8, "insertMaps: unable to insert map"

    invoke-static {v7, v8}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2

    .line 562
    .end local v1    # "image":[B
    .end local v2    # "mapJson":Lorg/json/JSONObject;
    .end local v3    # "mapString":Ljava/lang/String;
    .end local v4    # "mapUri":Landroid/net/Uri;
    .end local v5    # "mapValues":Landroid/content/ContentValues;
    :cond_0
    const/4 v6, 0x1

    :goto_0
    return v6

    .line 528
    .restart local v2    # "mapJson":Lorg/json/JSONObject;
    .restart local v5    # "mapValues":Landroid/content/ContentValues;
    :catch_0
    move-exception v0

    .line 529
    .local v0, "e":Lorg/json/JSONException;
    const-string v7, "sPlannerEventBuilder"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "insertMap:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 536
    .end local v0    # "e":Lorg/json/JSONException;
    :catch_1
    move-exception v0

    .line 537
    .restart local v0    # "e":Lorg/json/JSONException;
    const-string v7, "sPlannerEventBuilder"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "insertMap:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 538
    invoke-virtual {v5}, Landroid/content/ContentValues;->clear()V

    goto :goto_0

    .line 556
    .end local v0    # "e":Lorg/json/JSONException;
    .restart local v1    # "image":[B
    .restart local v3    # "mapString":Ljava/lang/String;
    :catch_2
    move-exception v0

    .line 557
    .local v0, "e":Landroid/os/RemoteException;
    const-string v7, "sPlannerEventBuilder"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "insertMap:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private insertReminders(JLorg/json/JSONObject;)Z
    .locals 10
    .param p1, "eventId"    # J
    .param p3, "jsonObject"    # Lorg/json/JSONObject;

    .prologue
    const/4 v6, 0x0

    .line 479
    const-string v7, "REMINDERS"

    invoke-virtual {p3, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 480
    const/4 v5, 0x0

    .line 482
    .local v5, "reminderValue":Landroid/content/ContentValues;
    const/4 v3, 0x0

    .line 484
    .local v3, "reminderList":Lorg/json/JSONArray;
    :try_start_0
    const-string v7, "REMINDERS"

    invoke-virtual {p3, v7}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 490
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v2

    .line 491
    .local v2, "reminderCount":I
    iget-object v7, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    sget-object v8, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->REMINDER_COLUMNS:[Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 493
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_2

    .line 495
    :try_start_1
    invoke-virtual {v3, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 496
    .local v4, "reminderObj":Lorg/json/JSONObject;
    iget-object v7, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v7, v4}, Lcom/sec/android/sCloudSync/Records/JSONParser;->fromJSON(Lorg/json/JSONObject;)Landroid/content/ContentValues;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v5

    .line 501
    invoke-virtual {v5}, Landroid/content/ContentValues;->size()I

    move-result v7

    if-nez v7, :cond_1

    .line 502
    const-string v7, "sPlannerEventBuilder"

    const-string v8, "insertReminders: Empty value."

    invoke-static {v7, v8}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 493
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 485
    .end local v1    # "i":I
    .end local v2    # "reminderCount":I
    .end local v4    # "reminderObj":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 486
    .local v0, "e":Lorg/json/JSONException;
    const-string v7, "sPlannerEventBuilder"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "insertReminders:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 518
    .end local v0    # "e":Lorg/json/JSONException;
    .end local v3    # "reminderList":Lorg/json/JSONArray;
    .end local v5    # "reminderValue":Landroid/content/ContentValues;
    :goto_2
    return v6

    .line 497
    .restart local v1    # "i":I
    .restart local v2    # "reminderCount":I
    .restart local v3    # "reminderList":Lorg/json/JSONArray;
    .restart local v5    # "reminderValue":Landroid/content/ContentValues;
    :catch_1
    move-exception v0

    .line 498
    .restart local v0    # "e":Lorg/json/JSONException;
    const-string v7, "sPlannerEventBuilder"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "insertReminders:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 506
    .end local v0    # "e":Lorg/json/JSONException;
    .restart local v4    # "reminderObj":Lorg/json/JSONObject;
    :cond_1
    const-string v7, "event_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 508
    :try_start_2
    iget-object v7, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mProvider:Landroid/content/ContentProviderClient;

    iget-object v8, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mReminderUri:Landroid/net/Uri;

    invoke-virtual {v7, v8, v5}, Landroid/content/ContentProviderClient;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v7

    if-nez v7, :cond_0

    .line 509
    const-string v7, "sPlannerEventBuilder"

    const-string v8, "reminderValue: Unable to insert Reminder"

    invoke-static {v7, v8}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    .line 511
    :catch_2
    move-exception v0

    .line 512
    .local v0, "e":Landroid/os/RemoteException;
    const-string v7, "sPlannerEventBuilder"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "insertReminders:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 516
    .end local v0    # "e":Landroid/os/RemoteException;
    .end local v4    # "reminderObj":Lorg/json/JSONObject;
    :cond_2
    iget-object v6, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 518
    .end local v1    # "i":I
    .end local v2    # "reminderCount":I
    .end local v3    # "reminderList":Lorg/json/JSONArray;
    .end local v5    # "reminderValue":Landroid/content/ContentValues;
    :cond_3
    const/4 v6, 0x1

    goto :goto_2
.end method

.method private parseEvent(Landroid/database/Cursor;Lorg/json/JSONObject;Ljava/lang/Long;)V
    .locals 6
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "parsedEventJson"    # Lorg/json/JSONObject;
    .param p3, "rowId"    # Ljava/lang/Long;

    .prologue
    const/4 v5, 0x0

    .line 117
    const/4 v1, 0x0

    .line 118
    .local v1, "eventJson":Lorg/json/JSONObject;
    iget-object v2, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    sget-object v3, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->EVENT_COLUMNS:[Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 120
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v2, p1}, Lcom/sec/android/sCloudSync/Records/JSONParser;->toJSON(Landroid/database/Cursor;)Lorg/json/JSONObject;

    move-result-object v1

    .line 121
    const-string v2, "EVENT"

    invoke-virtual {p2, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 125
    iget-object v2, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v2, v5}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 127
    :goto_0
    return-void

    .line 122
    :catch_0
    move-exception v0

    .line 123
    .local v0, "e":Lorg/json/JSONException;
    :try_start_1
    const-string v2, "sPlannerEventBuilder"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "parse():Exception in parsing"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 125
    iget-object v2, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v2, v5}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    goto :goto_0

    .end local v0    # "e":Lorg/json/JSONException;
    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v3, v5}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    throw v2
.end method

.method private parseExtendedProperties(Landroid/database/Cursor;Lorg/json/JSONObject;Ljava/lang/Long;)V
    .locals 11
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "parsedEventJson"    # Lorg/json/JSONObject;
    .param p3, "rowId"    # Ljava/lang/Long;

    .prologue
    const/4 v1, 0x1

    .line 171
    const-string v0, "hasExtendedProperties"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 173
    new-instance v8, Lorg/json/JSONArray;

    invoke-direct {v8}, Lorg/json/JSONArray;-><init>()V

    .line 175
    .local v8, "extraList":Lorg/json/JSONArray;
    const-string v10, "event_id= ?"

    .line 176
    .local v10, "selection":Ljava/lang/String;
    new-array v4, v1, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 177
    .local v4, "selectionArgs":[Ljava/lang/String;
    const/4 v7, 0x0

    .line 180
    .local v7, "extPropertiesCursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mProvider:Landroid/content/ContentProviderClient;

    sget-object v1, Landroid/provider/CalendarContract$ExtendedProperties;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->EXTENDED_PROPERTIES_COLUMNS:[Ljava/lang/String;

    const-string v3, "event_id= ?"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v7

    .line 187
    :goto_0
    if-eqz v7, :cond_0

    .line 188
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    sget-object v1, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->EXTENDED_PROPERTIES_COLUMNS:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 190
    :goto_1
    :try_start_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 191
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v0, v7}, Lcom/sec/android/sCloudSync/Records/JSONParser;->toJSON(Landroid/database/Cursor;)Lorg/json/JSONObject;

    move-result-object v9

    .line 192
    .local v9, "extraObj":Lorg/json/JSONObject;
    invoke-virtual {v9}, Lorg/json/JSONObject;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 193
    const-string v0, "sPlannerEventBuilder"

    const-string v1, "getExtendedProperties: Unable to parse: "

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 200
    .end local v9    # "extraObj":Lorg/json/JSONObject;
    :catch_0
    move-exception v6

    .line 201
    .local v6, "e":Lorg/json/JSONException;
    :try_start_2
    const-string v0, "sPlannerEventBuilder"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getExtendedProperties: Unable to parse: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v6}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 203
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 207
    .end local v4    # "selectionArgs":[Ljava/lang/String;
    .end local v6    # "e":Lorg/json/JSONException;
    .end local v7    # "extPropertiesCursor":Landroid/database/Cursor;
    .end local v8    # "extraList":Lorg/json/JSONArray;
    .end local v10    # "selection":Ljava/lang/String;
    :cond_0
    :goto_2
    return-void

    .line 184
    .restart local v4    # "selectionArgs":[Ljava/lang/String;
    .restart local v7    # "extPropertiesCursor":Landroid/database/Cursor;
    .restart local v8    # "extraList":Lorg/json/JSONArray;
    .restart local v10    # "selection":Ljava/lang/String;
    :catch_1
    move-exception v6

    .line 185
    .local v6, "e":Landroid/os/RemoteException;
    const-string v0, "sPlannerEventBuilder"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getExtendedProperties: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v6}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 195
    .end local v6    # "e":Landroid/os/RemoteException;
    .restart local v9    # "extraObj":Lorg/json/JSONObject;
    :cond_1
    :try_start_3
    invoke-virtual {v8, v9}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 203
    .end local v9    # "extraObj":Lorg/json/JSONObject;
    :catchall_0
    move-exception v0

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v0

    .line 198
    :cond_2
    if-eqz v8, :cond_3

    .line 199
    :try_start_4
    const-string v0, "EXTRAPROPERTIES"

    invoke-virtual {p2, v0, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 203
    :cond_3
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_2
.end method

.method private parseMap(Landroid/database/Cursor;Lorg/json/JSONObject;Ljava/lang/Long;)V
    .locals 10
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "parsedEventJson"    # Lorg/json/JSONObject;
    .param p3, "rowId"    # Ljava/lang/Long;

    .prologue
    .line 211
    const-string v9, "event_id = ?"

    .line 212
    .local v9, "mapSelection":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 213
    .local v4, "mapSelectionArgs":[Ljava/lang/String;
    const/4 v7, 0x0

    .line 216
    .local v7, "mapCursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mProvider:Landroid/content/ContentProviderClient;

    sget-object v1, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->MAPURI:Landroid/net/Uri;

    sget-object v2, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->MAP_COLUMNS:[Ljava/lang/String;

    const-string v3, "event_id = ?"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 221
    :goto_0
    if-eqz v7, :cond_1

    .line 222
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    sget-object v1, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->MAP_COLUMNS:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 224
    :try_start_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 225
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v0, v7}, Lcom/sec/android/sCloudSync/Records/JSONParser;->toJSON(Landroid/database/Cursor;)Lorg/json/JSONObject;

    move-result-object v8

    .line 226
    .local v8, "mapJson":Lorg/json/JSONObject;
    invoke-virtual {v8}, Lorg/json/JSONObject;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 227
    const-string v0, "sPlannerEventBuilder"

    const-string v1, "MAP: Unable to parse: "

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 237
    .end local v8    # "mapJson":Lorg/json/JSONObject;
    :cond_0
    :goto_1
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 240
    :cond_1
    :goto_2
    return-void

    .line 217
    :catch_0
    move-exception v6

    .line 218
    .local v6, "e":Landroid/os/RemoteException;
    const-string v0, "sPlannerEventBuilder"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getExtendedProperties: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v6}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 229
    .end local v6    # "e":Landroid/os/RemoteException;
    .restart local v8    # "mapJson":Lorg/json/JSONObject;
    :cond_2
    if-eqz v8, :cond_0

    .line 230
    :try_start_2
    const-string v0, "MAP"

    invoke-virtual {p2, v0, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 233
    .end local v8    # "mapJson":Lorg/json/JSONObject;
    :catch_1
    move-exception v6

    .line 234
    .local v6, "e":Lorg/json/JSONException;
    :try_start_3
    const-string v0, "sPlannerEventBuilder"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MAP: Unable to parse: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v6}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 237
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .end local v6    # "e":Lorg/json/JSONException;
    :catchall_0
    move-exception v0

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private parseReminder(Landroid/database/Cursor;Lorg/json/JSONObject;Ljava/lang/Long;)V
    .locals 11
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "parsedEventJson"    # Lorg/json/JSONObject;
    .param p3, "rowId"    # Ljava/lang/Long;

    .prologue
    const/4 v1, 0x1

    .line 131
    const-string v0, "hasAlarm"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 132
    new-instance v9, Lorg/json/JSONArray;

    invoke-direct {v9}, Lorg/json/JSONArray;-><init>()V

    .line 134
    .local v9, "reminderList":Lorg/json/JSONArray;
    const-string v10, "event_id= ?"

    .line 135
    .local v10, "selection":Ljava/lang/String;
    new-array v4, v1, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 136
    .local v4, "selectionArgs":[Ljava/lang/String;
    const/4 v7, 0x0

    .line 139
    .local v7, "reminderCursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mProvider:Landroid/content/ContentProviderClient;

    sget-object v1, Landroid/provider/CalendarContract$Reminders;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->REMINDER_COLUMNS:[Ljava/lang/String;

    const-string v3, "event_id= ?"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v7

    .line 145
    :goto_0
    if-eqz v7, :cond_0

    .line 146
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    sget-object v1, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->REMINDER_COLUMNS:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 147
    const/4 v8, 0x0

    .line 149
    .local v8, "reminderJson":Lorg/json/JSONObject;
    :goto_1
    :try_start_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 150
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v0, v7}, Lcom/sec/android/sCloudSync/Records/JSONParser;->toJSON(Landroid/database/Cursor;)Lorg/json/JSONObject;

    move-result-object v8

    .line 151
    invoke-virtual {v8}, Lorg/json/JSONObject;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 152
    const-string v0, "sPlannerEventBuilder"

    const-string v1, "GetReminders: Unable to parse "

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 159
    :catch_0
    move-exception v6

    .line 160
    .local v6, "e":Lorg/json/JSONException;
    :try_start_2
    const-string v0, "sPlannerEventBuilder"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getReminders: Unable to parse: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v6}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 163
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 167
    .end local v4    # "selectionArgs":[Ljava/lang/String;
    .end local v6    # "e":Lorg/json/JSONException;
    .end local v7    # "reminderCursor":Landroid/database/Cursor;
    .end local v8    # "reminderJson":Lorg/json/JSONObject;
    .end local v9    # "reminderList":Lorg/json/JSONArray;
    .end local v10    # "selection":Ljava/lang/String;
    :cond_0
    :goto_2
    return-void

    .line 142
    .restart local v4    # "selectionArgs":[Ljava/lang/String;
    .restart local v7    # "reminderCursor":Landroid/database/Cursor;
    .restart local v9    # "reminderList":Lorg/json/JSONArray;
    .restart local v10    # "selection":Ljava/lang/String;
    :catch_1
    move-exception v6

    .line 143
    .local v6, "e":Landroid/os/RemoteException;
    const-string v0, "sPlannerEventBuilder"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getReminders: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v6}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 154
    .end local v6    # "e":Landroid/os/RemoteException;
    .restart local v8    # "reminderJson":Lorg/json/JSONObject;
    :cond_1
    :try_start_3
    invoke-virtual {v9, v8}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 163
    :catchall_0
    move-exception v0

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v0

    .line 157
    :cond_2
    if-eqz v9, :cond_3

    .line 158
    :try_start_4
    const-string v0, "REMINDERS"

    invoke-virtual {p2, v0, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 163
    :cond_3
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_2
.end method


# virtual methods
.method public delete(Landroid/net/Uri;JLjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "rowId"    # J
    .param p4, "selection"    # Ljava/lang/String;
    .param p5, "syncKey"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 580
    sget-object v1, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v1, p2, p3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "caller_is_syncadapter"

    const-string v3, "true"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "account_name"

    iget-object v3, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mAccount:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "account_type"

    iget-object v3, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mAccount:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 585
    .local v0, "CalUri":Landroid/net/Uri;
    iget-object v1, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mOperations:Ljava/util/ArrayList;

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 586
    return-void
.end method

.method public doApplyBatch()Z
    .locals 1

    .prologue
    .line 591
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mOperations:Ljava/util/ArrayList;

    invoke-virtual {p0, v0}, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->doApplyBatch(Ljava/util/ArrayList;)Z

    move-result v0

    return v0
.end method

.method public insert(Ljava/lang/String;Ljava/lang/String;J)Z
    .locals 8
    .param p1, "jsonString"    # Ljava/lang/String;
    .param p2, "syncKey"    # Ljava/lang/String;
    .param p3, "timeStamp"    # J

    .prologue
    const/4 v7, 0x0

    .line 361
    const/4 v4, 0x0

    .line 364
    .local v4, "jsonObject":Lorg/json/JSONObject;
    :try_start_0
    new-instance v6, Lorg/json/JSONTokener;

    invoke-direct {v6, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Lorg/json/JSONObject;

    move-object v4, v0
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 370
    invoke-direct {p0, v4, p2, p3, p4}, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->insertEvent(Lorg/json/JSONObject;Ljava/lang/String;J)Landroid/net/Uri;

    move-result-object v5

    .line 371
    .local v5, "resultUri":Landroid/net/Uri;
    if-nez v5, :cond_0

    move v6, v7

    .line 379
    .end local v5    # "resultUri":Landroid/net/Uri;
    :goto_0
    return v6

    .line 365
    :catch_0
    move-exception v1

    .line 366
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    move v6, v7

    .line 367
    goto :goto_0

    .line 375
    .end local v1    # "e":Lorg/json/JSONException;
    .restart local v5    # "resultUri":Landroid/net/Uri;
    :cond_0
    invoke-static {v5}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v2

    .line 376
    .local v2, "eventId":J
    const-wide/16 v6, 0x0

    cmp-long v6, v2, v6

    if-lez v6, :cond_1

    .line 377
    invoke-direct {p0, v2, v3, v4}, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->insertExtras(JLorg/json/JSONObject;)Z

    move-result v6

    goto :goto_0

    .line 379
    :cond_1
    const/4 v6, 0x1

    goto :goto_0
.end method

.method public parse(Landroid/database/Cursor;JLjava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "rowId"    # J
    .param p4, "key"    # Ljava/lang/String;

    .prologue
    .line 245
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 247
    .local v0, "parsedEventJson":Lorg/json/JSONObject;
    if-nez p1, :cond_0

    .line 248
    const/4 v1, 0x0

    .line 261
    :goto_0
    return-object v1

    .line 250
    :cond_0
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->parseEvent(Landroid/database/Cursor;Lorg/json/JSONObject;Ljava/lang/Long;)V

    .line 253
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->parseReminder(Landroid/database/Cursor;Lorg/json/JSONObject;Ljava/lang/Long;)V

    .line 256
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->parseExtendedProperties(Landroid/database/Cursor;Lorg/json/JSONObject;Ljava/lang/Long;)V

    .line 259
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->parseMap(Landroid/database/Cursor;Lorg/json/JSONObject;Ljava/lang/Long;)V

    .line 261
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public update(Ljava/lang/String;JJLjava/lang/String;)Z
    .locals 13
    .param p1, "jsonString"    # Ljava/lang/String;
    .param p2, "timeStamp"    # J
    .param p4, "rowId"    # J
    .param p6, "synckey"    # Ljava/lang/String;

    .prologue
    .line 273
    const/4 v6, 0x0

    .line 275
    .local v6, "jsonObject":Lorg/json/JSONObject;
    :try_start_0
    new-instance v8, Lorg/json/JSONTokener;

    invoke-direct {v8, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object v8

    move-object v0, v8

    check-cast v0, Lorg/json/JSONObject;

    move-object v6, v0
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 281
    const/4 v5, 0x0

    .line 282
    .local v5, "eventValue":Landroid/content/ContentValues;
    iget-object v8, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    sget-object v9, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->EVENT_COLUMNS:[Ljava/lang/String;

    invoke-virtual {v8, v9}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 284
    :try_start_1
    iget-object v8, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    const-string v9, "EVENT"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/sec/android/sCloudSync/Records/JSONParser;->fromJSON(Lorg/json/JSONObject;)Landroid/content/ContentValues;

    move-result-object v5

    .line 285
    iget-object v8, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 286
    if-nez v5, :cond_0

    .line 287
    const-string v8, "sPlannerEventBuilder"

    const-string v9, "UpdateEvent: unable to parse;"

    invoke-static {v8, v9}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 288
    const/4 v8, 0x0

    .line 332
    .end local v5    # "eventValue":Landroid/content/ContentValues;
    :goto_0
    return v8

    .line 276
    :catch_0
    move-exception v3

    .line 277
    .local v3, "e1":Lorg/json/JSONException;
    const-string v8, "sPlannerEventBuilder"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "updateEvent(): "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    const/4 v8, 0x0

    goto :goto_0

    .line 290
    .end local v3    # "e1":Lorg/json/JSONException;
    .restart local v5    # "eventValue":Landroid/content/ContentValues;
    :cond_0
    :try_start_2
    const-string v8, "original_id"

    invoke-virtual {v5, v8}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 291
    const-string v8, "original_id"

    invoke-virtual {v5, v8}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 294
    :cond_1
    const-string v8, "selfAttendeeStatus"

    invoke-virtual {v5, v8}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 296
    const-string v8, "calendar_id"

    invoke-virtual {v5, v8}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 298
    const-string v8, "hasAttendeeData"

    invoke-virtual {v5, v8}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 299
    const-string v8, "hasAttendeeData"

    invoke-virtual {v5, v8}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 302
    :cond_2
    const-string v8, "sync_data5"

    invoke-static/range {p2 .. p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 303
    const-string v8, "dirty"

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 304
    const-string v8, "deleted"

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    .line 311
    sget-object v8, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    const-string v9, "caller_is_syncadapter"

    invoke-static {v8, v9}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 312
    .local v4, "eventUri":Landroid/net/Uri;
    iget-object v8, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mAccount:Landroid/accounts/Account;

    invoke-static {v4, v8}, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->addAccountParameter(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v4

    .line 317
    :try_start_3
    iget-object v8, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->mProvider:Landroid/content/ContentProviderClient;

    const-string v9, "_id = ? "

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-static/range {p4 .. p5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v4, v5, v9, v10}, Landroid/content/ContentProviderClient;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_2

    move-result v7

    .line 318
    .local v7, "updated":I
    if-nez v7, :cond_3

    .line 319
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 306
    .end local v4    # "eventUri":Landroid/net/Uri;
    .end local v7    # "updated":I
    :catch_1
    move-exception v2

    .line 307
    .local v2, "e":Lorg/json/JSONException;
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    .line 308
    const-string v8, "sPlannerEventBuilder"

    const-string v9, "updateEvent(): unable to parse;"

    invoke-static {v8, v9}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 321
    .end local v2    # "e":Lorg/json/JSONException;
    .restart local v4    # "eventUri":Landroid/net/Uri;
    :catch_2
    move-exception v2

    .line 322
    .local v2, "e":Landroid/os/RemoteException;
    const-string v8, "sPlannerEventBuilder"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "updateEvent():"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v2}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 326
    .end local v2    # "e":Landroid/os/RemoteException;
    .restart local v7    # "updated":I
    :cond_3
    :try_start_4
    move-wide/from16 v0, p4

    invoke-direct {p0, v0, v1}, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->deleteExtras(J)V
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_3

    .line 331
    :goto_1
    move-wide/from16 v0, p4

    invoke-direct {p0, v0, v1, v6}, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerEventBuilder;->insertExtras(JLorg/json/JSONObject;)Z

    .line 332
    const/4 v8, 0x1

    goto/16 :goto_0

    .line 327
    :catch_3
    move-exception v2

    .line 328
    .restart local v2    # "e":Landroid/os/RemoteException;
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    .line 329
    const-string v8, "sPlannerEventBuilder"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "updateEvent(): Unable to delete extra :"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v2}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
