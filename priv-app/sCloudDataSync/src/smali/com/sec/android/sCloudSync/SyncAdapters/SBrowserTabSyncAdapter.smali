.class public Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserTabSyncAdapter;
.super Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;
.source "SBrowserTabSyncAdapter.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SBrowserTabSyncAdapter"


# instance fields
.field private mAuthority:Ljava/lang/String;

.field private mDataColumn:Ljava/lang/String;

.field private mSyncStateUri:Landroid/net/Uri;

.field private mUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x0

    .line 33
    new-instance v3, Lcom/sec/android/sCloudSync/ServiceManagers/SCloudServiceManager;

    const-string v4, "P56GWW8N4r"

    const/4 v5, 0x0

    invoke-direct {v3, p1, v4, v5}, Lcom/sec/android/sCloudSync/ServiceManagers/SCloudServiceManager;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    invoke-direct {p0, p1, v3}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;-><init>(Landroid/content/Context;Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;)V

    .line 27
    iput-object v6, p0, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserTabSyncAdapter;->mSyncStateUri:Landroid/net/Uri;

    .line 28
    iput-object v6, p0, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserTabSyncAdapter;->mDataColumn:Ljava/lang/String;

    .line 34
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserTabSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 36
    .local v2, "packageManager":Landroid/content/pm/PackageManager;
    :try_start_0
    const-string v3, "com.sec.android.app.sbrowser"

    const/16 v4, 0x80

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 37
    .local v1, "packageInfo":Landroid/content/pm/PackageInfo;
    iget v3, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    const/16 v4, 0xf

    if-lt v3, v4, :cond_0

    .line 38
    sget-object v3, Lcom/sec/android/sCloudSync/Constants/SBrowserContract;->SBROWSER_TAB_URI:Landroid/net/Uri;

    iput-object v3, p0, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserTabSyncAdapter;->mUri:Landroid/net/Uri;

    .line 39
    sget-object v3, Lcom/sec/android/sCloudSync/Constants/SBrowserContract;->SBROWSER_SYNC_STATE_URI:Landroid/net/Uri;

    iput-object v3, p0, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserTabSyncAdapter;->mSyncStateUri:Landroid/net/Uri;

    .line 40
    const-string v3, "data"

    iput-object v3, p0, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserTabSyncAdapter;->mDataColumn:Ljava/lang/String;

    .line 41
    const-string v3, "com.sec.android.app.sbrowser"

    iput-object v3, p0, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserTabSyncAdapter;->mAuthority:Ljava/lang/String;

    .line 49
    .end local v1    # "packageInfo":Landroid/content/pm/PackageInfo;
    :goto_0
    return-void

    .line 43
    .restart local v1    # "packageInfo":Landroid/content/pm/PackageInfo;
    :cond_0
    sget-object v3, Lcom/sec/android/sCloudSync/Constants/SBrowserContract;->SBROWSER_TAB_URI_OLD:Landroid/net/Uri;

    iput-object v3, p0, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserTabSyncAdapter;->mUri:Landroid/net/Uri;

    .line 44
    const-string v3, "com.sec.android.app.sbrowser.tabs"

    iput-object v3, p0, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserTabSyncAdapter;->mAuthority:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 46
    .end local v1    # "packageInfo":Landroid/content/pm/PackageInfo;
    :catch_0
    move-exception v0

    .line 47
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserTabSyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Sbrowser package not found"

    invoke-static {v3, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method protected doUpdateDelete(Lcom/sec/android/sCloudSync/Records/KVSItem;Lcom/sec/android/sCloudSync/Records/RecordItem;Landroid/content/SyncStats;)Z
    .locals 9
    .param p1, "item"    # Lcom/sec/android/sCloudSync/Records/KVSItem;
    .param p2, "record"    # Lcom/sec/android/sCloudSync/Records/RecordItem;
    .param p3, "stats"    # Landroid/content/SyncStats;

    .prologue
    .line 128
    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->isDeleted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 129
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserTabSyncAdapter;->getContentUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getID()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v8

    .line 130
    .local v8, "uri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserTabSyncAdapter;->getCallerSyncAdapter()Ljava/lang/String;

    move-result-object v0

    invoke-static {v8, v0}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserTabSyncAdapter;->getAccountName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserTabSyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v2

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserTabSyncAdapter;->getAccountType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserTabSyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v2

    iget-object v2, v2, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v8

    .line 135
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserTabSyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v8, v1, v2}, Landroid/content/ContentProviderClient;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 151
    .end local v8    # "uri":Landroid/net/Uri;
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 136
    .restart local v8    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v7

    .line 137
    .local v7, "e":Landroid/os/RemoteException;
    const-string v0, "SBrowserTabSyncAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to delete record with key:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getKEY()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    invoke-virtual {v7}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 142
    .end local v7    # "e":Landroid/os/RemoteException;
    .end local v8    # "uri":Landroid/net/Uri;
    :cond_0
    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getValue()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserTabSyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getTimeStamp()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getID()J

    move-result-wide v4

    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getKEY()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;->update(Ljava/lang/String;JJLjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 143
    :cond_1
    const-string v0, "SBrowserTabSyncAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to update record with key:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getKEY()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 148
    :cond_2
    iget-wide v0, p3, Landroid/content/SyncStats;->numUpdates:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p3, Landroid/content/SyncStats;->numUpdates:J

    goto :goto_0
.end method

.method protected getAccountName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    const-string v0, "ACCOUNT_NAME"

    return-object v0
.end method

.method protected getAccountType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    const-string v0, "ACCOUNT_TYPE"

    return-object v0
.end method

.method protected getBuilder()Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;
    .locals 5

    .prologue
    .line 98
    new-instance v0, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserTabBuilder;

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserTabSyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserTabSyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserTabSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserTabSyncAdapter;->mAuthority:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/sCloudSync/Builders/SBrowser/SBrowserTabBuilder;-><init>(Landroid/content/ContentProviderClient;Landroid/accounts/Account;Landroid/content/Context;Ljava/lang/String;)V

    return-object v0
.end method

.method protected getCallerSyncAdapter()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    const-string v0, "caller_is_syncadapter"

    return-object v0
.end method

.method protected getCid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    const-string v0, "P56GWW8N4r"

    return-object v0
.end method

.method protected getContentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserTabSyncAdapter;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method protected getCtidKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 155
    const-string v0, "ST"

    return-object v0
.end method

.method protected getDeletedColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    const-string v0, "IS_DELETED"

    return-object v0
.end method

.method protected getDirtyColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    const-string v0, "DIRTY"

    return-object v0
.end method

.method protected getIdColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    const-string v0, "_ID"

    return-object v0
.end method

.method protected getKeyColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    const-string v0, "SYNC1"

    return-object v0
.end method

.method protected getSyncAdapterName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    const-string v0, "SBROWSER_TAB"

    return-object v0
.end method

.method protected getSyncStateDataColumn()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserTabSyncAdapter;->mDataColumn:Ljava/lang/String;

    return-object v0
.end method

.method protected getSyncStateURI()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserTabSyncAdapter;->mSyncStateUri:Landroid/net/Uri;

    return-object v0
.end method

.method protected getTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    const-string v0, "SBrowserTabSyncAdapter"

    return-object v0
.end method

.method protected getTimeStampColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    const-string v0, "SYNC5"

    return-object v0
.end method
