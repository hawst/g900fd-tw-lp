.class public Lcom/sec/android/sCloudSync/Builders/Contact/ContactsBuilder;
.super Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;
.source "ContactsBuilder.java"


# static fields
.field private static final DATALIST:Ljava/lang/String; = "DATALIST"

.field private static final DATA_COLUMNS:[Ljava/lang/String;

.field private static final GROUP_COLUMNS:[Ljava/lang/String;

.field private static final RAWCONTACT:Ljava/lang/String; = "RAWCONTACT"

.field private static final RAW_CONTACTS_COLUMNS:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "ContactsBuilder"


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 55
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "account_name"

    aput-object v1, v0, v3

    const-string v1, "account_type"

    aput-object v1, v0, v4

    const-string v1, "contact_id"

    aput-object v1, v0, v5

    const-string v1, "aggregation_mode"

    aput-object v1, v0, v6

    const-string v1, "starred"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "display_name_alt"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "display_name_source"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "phonetic_name"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "phonetic_name_style"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsBuilder;->RAW_CONTACTS_COLUMNS:[Ljava/lang/String;

    .line 68
    const/16 v0, 0x10

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "mimetype"

    aput-object v1, v0, v3

    const-string v1, "data1"

    aput-object v1, v0, v4

    const-string v1, "data2"

    aput-object v1, v0, v5

    const-string v1, "data3"

    aput-object v1, v0, v6

    const-string v1, "data4"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "data5"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "data6"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "data7"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "data8"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "data9"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "data10"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "data11"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "data12"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "data13"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "data14"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "data15"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsBuilder;->DATA_COLUMNS:[Ljava/lang/String;

    .line 88
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "account_name"

    aput-object v1, v0, v3

    const-string v1, "account_type"

    aput-object v1, v0, v4

    const-string v1, "data_set"

    aput-object v1, v0, v5

    const-string v1, "title"

    aput-object v1, v0, v6

    const-string v1, "title_res"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "notes"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "system_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsBuilder;->GROUP_COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentProviderClient;Landroid/accounts/Account;Landroid/content/Context;)V
    .locals 0
    .param p1, "provider"    # Landroid/content/ContentProviderClient;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 100
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;-><init>(Landroid/content/ContentProviderClient;Landroid/accounts/Account;Landroid/content/Context;)V

    .line 101
    return-void
.end method

.method private checkGroupInformation(Landroid/database/Cursor;)Lorg/json/JSONObject;
    .locals 14
    .param p1, "dataCursor"    # Landroid/database/Cursor;

    .prologue
    .line 157
    const-string v0, "mimetype"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 160
    .local v6, "Mimetype":Ljava/lang/String;
    const-string v0, "group_membership"

    invoke-virtual {v6, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 161
    const-string v0, "data1"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    .line 165
    .local v11, "groupId":J
    const-wide/16 v0, 0x1

    cmp-long v0, v11, v0

    if-gez v0, :cond_0

    .line 166
    const/4 v13, 0x0

    .line 197
    .end local v11    # "groupId":J
    :goto_0
    return-object v13

    .line 167
    .restart local v11    # "groupId":J
    :cond_0
    const/4 v10, 0x0

    .line 169
    .local v10, "groupCursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsBuilder;->mProvider:Landroid/content/ContentProviderClient;

    sget-object v1, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v1, v11, v12}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsBuilder;->GROUP_COLUMNS:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v10

    .line 176
    :goto_1
    if-nez v10, :cond_1

    .line 177
    const/4 v13, 0x0

    goto :goto_0

    .line 172
    :catch_0
    move-exception v9

    .line 173
    .local v9, "e1":Landroid/os/RemoteException;
    const-string v0, "ContactsBuilder"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unable to query"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v9}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 179
    .end local v9    # "e1":Landroid/os/RemoteException;
    :cond_1
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_2

    .line 180
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 181
    const/4 v13, 0x0

    goto :goto_0

    .line 184
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    sget-object v1, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsBuilder;->GROUP_COLUMNS:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 185
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v0, v10}, Lcom/sec/android/sCloudSync/Records/JSONParser;->toJSON(Landroid/database/Cursor;)Lorg/json/JSONObject;

    move-result-object v7

    .line 187
    .local v7, "dataObj":Lorg/json/JSONObject;
    new-instance v13, Lorg/json/JSONObject;

    invoke-direct {v13}, Lorg/json/JSONObject;-><init>()V

    .line 188
    .local v13, "jsonObj":Lorg/json/JSONObject;
    const-string v0, "mimetype"

    invoke-virtual {v13, v0, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 189
    const-string v0, "data1"

    invoke-virtual {v13, v0, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 194
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 191
    .end local v7    # "dataObj":Lorg/json/JSONObject;
    .end local v13    # "jsonObj":Lorg/json/JSONObject;
    :catch_1
    move-exception v8

    .line 192
    .local v8, "e":Lorg/json/JSONException;
    :try_start_2
    const-string v0, "ContactsBuilder"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unable to parse"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v8}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 194
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 197
    .end local v8    # "e":Lorg/json/JSONException;
    .end local v10    # "groupCursor":Landroid/database/Cursor;
    .end local v11    # "groupId":J
    :cond_3
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 194
    .restart local v10    # "groupCursor":Landroid/database/Cursor;
    .restart local v11    # "groupId":J
    :catchall_0
    move-exception v0

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private createGroup(Landroid/content/ContentValues;)J
    .locals 4
    .param p1, "groupValue"    # Landroid/content/ContentValues;

    .prologue
    .line 359
    const/4 v1, 0x0

    .line 361
    .local v1, "uri":Landroid/net/Uri;
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsBuilder;->mProvider:Landroid/content/ContentProviderClient;

    sget-object v3, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3, p1}, Landroid/content/ContentProviderClient;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 366
    :goto_0
    if-eqz v1, :cond_0

    .line 367
    invoke-static {v1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v2

    .line 368
    :goto_1
    return-wide v2

    .line 362
    :catch_0
    move-exception v0

    .line 363
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 368
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const-wide/16 v2, -0x1

    goto :goto_1
.end method

.method private getGroupId(Ljava/lang/String;)J
    .locals 16
    .param p1, "GroupInfo"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .line 298
    const/4 v8, 0x0

    .line 299
    .local v8, "groupValue":Landroid/content/ContentValues;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    sget-object v2, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsBuilder;->GROUP_COLUMNS:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 301
    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 302
    :cond_0
    new-instance v1, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v1
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_3

    .line 345
    :catch_0
    move-exception v7

    .line 346
    .local v7, "e":Lorg/json/JSONException;
    new-instance v1, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v1

    .line 304
    .end local v7    # "e":Lorg/json/JSONException;
    :cond_1
    :try_start_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/sec/android/sCloudSync/Records/JSONParser;->fromJSONString(Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v8

    .line 305
    if-nez v8, :cond_2

    .line 306
    const-wide/16 v10, -0x1

    .line 343
    :goto_0
    return-wide v10

    .line 308
    :cond_2
    const-string v15, "title = ? AND account_name = ? AND account_type =?"

    .line 311
    .local v15, "where":Ljava/lang/String;
    const-string v1, "title"

    invoke-virtual {v8, v1}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    .line 312
    .local v14, "grp_title":Ljava/lang/String;
    const-string v1, "account_name"

    invoke-virtual {v8, v1}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    .line 313
    .local v12, "grp_accntName":Ljava/lang/String;
    const-string v1, "account_type"

    invoke-virtual {v8, v1}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    .line 315
    .local v13, "grp_accntType":Ljava/lang/String;
    if-eqz v14, :cond_3

    invoke-virtual {v14}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    if-eqz v12, :cond_3

    invoke-virtual {v12}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    if-eqz v13, :cond_3

    invoke-virtual {v13}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 316
    :cond_3
    const-wide/16 v10, -0x1

    goto :goto_0

    .line 318
    :cond_4
    const/4 v1, 0x3

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object v14, v5, v1

    const/4 v1, 0x1

    aput-object v12, v5, v1

    const/4 v1, 0x2

    aput-object v13, v5, v1
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_3

    .line 324
    .local v5, "whereargs":[Ljava/lang/String;
    const/4 v9, 0x0

    .line 327
    .local v9, "grpCursor":Landroid/database/Cursor;
    :try_start_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsBuilder;->mProvider:Landroid/content/ContentProviderClient;

    sget-object v2, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v6, "_id"

    aput-object v6, v3, v4

    const-string v4, "title = ? AND account_name = ? AND account_type =?"

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/ClassCastException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_3

    move-result-object v9

    .line 333
    :goto_1
    if-eqz v9, :cond_5

    :try_start_3
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v1

    const/4 v2, 0x1

    if-ge v1, v2, :cond_7

    .line 334
    :cond_5
    if-eqz v9, :cond_6

    .line 335
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 337
    :cond_6
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsBuilder;->createGroup(Landroid/content/ContentValues;)J

    move-result-wide v10

    goto :goto_0

    .line 329
    :catch_1
    move-exception v7

    .line 330
    .local v7, "e":Landroid/os/RemoteException;
    invoke-virtual {v7}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/ClassCastException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_1

    .line 347
    .end local v5    # "whereargs":[Ljava/lang/String;
    .end local v7    # "e":Landroid/os/RemoteException;
    .end local v9    # "grpCursor":Landroid/database/Cursor;
    .end local v12    # "grp_accntName":Ljava/lang/String;
    .end local v13    # "grp_accntType":Ljava/lang/String;
    .end local v14    # "grp_title":Ljava/lang/String;
    .end local v15    # "where":Ljava/lang/String;
    :catch_2
    move-exception v7

    .line 348
    .local v7, "e":Ljava/lang/ClassCastException;
    invoke-virtual {v7}, Ljava/lang/ClassCastException;->printStackTrace()V

    .line 349
    const-string v1, "ContactsBuilder"

    const-string v2, "ClassCastException while getting group ID for contact"

    invoke-static {v1, v2}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 350
    new-instance v1, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v1

    .line 340
    .end local v7    # "e":Ljava/lang/ClassCastException;
    .restart local v5    # "whereargs":[Ljava/lang/String;
    .restart local v9    # "grpCursor":Landroid/database/Cursor;
    .restart local v12    # "grp_accntName":Ljava/lang/String;
    .restart local v13    # "grp_accntType":Ljava/lang/String;
    .restart local v14    # "grp_title":Ljava/lang/String;
    .restart local v15    # "where":Ljava/lang/String;
    :cond_7
    :try_start_4
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    .line 341
    const-string v1, "_id"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 342
    .local v10, "grpId":J
    invoke-interface {v9}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/ClassCastException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_4 .. :try_end_4} :catch_3

    goto/16 :goto_0

    .line 351
    .end local v5    # "whereargs":[Ljava/lang/String;
    .end local v9    # "grpCursor":Landroid/database/Cursor;
    .end local v10    # "grpId":J
    .end local v12    # "grp_accntName":Ljava/lang/String;
    .end local v13    # "grp_accntType":Ljava/lang/String;
    .end local v14    # "grp_title":Ljava/lang/String;
    .end local v15    # "where":Ljava/lang/String;
    :catch_3
    move-exception v7

    .line 352
    .local v7, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v7}, Ljava/lang/IllegalStateException;->printStackTrace()V

    .line 353
    const-string v1, "ContactsBuilder"

    const-string v2, "Illegal State Exception from Provider"

    invoke-static {v1, v2}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    new-instance v1, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/16 v2, 0xf

    invoke-direct {v1, v2}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v1
.end method

.method private insertData(Lorg/json/JSONObject;Ljava/lang/String;JLjava/util/ArrayList;)Z
    .locals 15
    .param p1, "jsonObject"    # Lorg/json/JSONObject;
    .param p2, "syncKey"    # Ljava/lang/String;
    .param p3, "timeStamp"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            "Ljava/lang/String;",
            "J",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 230
    .local p5, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    sget-object v12, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const-string v13, "caller_is_syncadapter"

    invoke-static {v12, v13}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 232
    .local v4, "dataUri":Landroid/net/Uri;
    :try_start_0
    const-string v12, "DATALIST"

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 233
    .local v2, "dataList":Lorg/json/JSONArray;
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v12

    if-ge v8, v12, :cond_3

    .line 235
    iget-object v12, p0, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    sget-object v13, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsBuilder;->DATA_COLUMNS:[Ljava/lang/String;

    invoke-virtual {v12, v13}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 236
    invoke-virtual {v2, v8}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 237
    .local v3, "dataObj":Lorg/json/JSONObject;
    iget-object v12, p0, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v12, v3}, Lcom/sec/android/sCloudSync/Records/JSONParser;->fromJSON(Lorg/json/JSONObject;)Landroid/content/ContentValues;

    move-result-object v11

    .line 238
    .local v11, "values":Landroid/content/ContentValues;
    const-string v12, "mimetype"

    invoke-virtual {v11, v12}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 240
    .local v1, "Mimetype":Ljava/lang/String;
    if-eqz v1, :cond_1

    const-string v12, "group_membership"

    invoke-virtual {v1, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v12

    if-eqz v12, :cond_1

    .line 243
    :try_start_1
    const-string v12, "data1"

    invoke-virtual {v11, v12}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    invoke-direct {p0, v12}, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsBuilder;->getGroupId(Ljava/lang/String;)J
    :try_end_1
    .catch Lcom/sec/android/sCloudSync/Util/SyncException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v6

    .line 248
    .local v6, "groupId":J
    :try_start_2
    const-string v12, "data1"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 259
    .end local v6    # "groupId":J
    :cond_0
    :goto_1
    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "raw_contact_id"

    const/4 v14, 0x0

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    invoke-virtual {v12, v11}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v12

    move-object/from16 v0, p5

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 233
    :goto_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 244
    :catch_0
    move-exception v5

    .line 245
    .local v5, "e":Lcom/sec/android/sCloudSync/Util/SyncException;
    invoke-virtual {v5}, Lcom/sec/android/sCloudSync/Util/SyncException;->printStackTrace()V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 263
    .end local v1    # "Mimetype":Ljava/lang/String;
    .end local v2    # "dataList":Lorg/json/JSONArray;
    .end local v3    # "dataObj":Lorg/json/JSONObject;
    .end local v5    # "e":Lcom/sec/android/sCloudSync/Util/SyncException;
    .end local v8    # "i":I
    .end local v11    # "values":Landroid/content/ContentValues;
    :catch_1
    move-exception v5

    .line 264
    .local v5, "e":Lorg/json/JSONException;
    :try_start_3
    const-string v12, "ContactsBuilder"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "unable to insert:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v5}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 265
    const/4 v12, 0x0

    .line 267
    iget-object v13, p0, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 269
    .end local v5    # "e":Lorg/json/JSONException;
    :goto_3
    return v12

    .line 249
    .restart local v1    # "Mimetype":Ljava/lang/String;
    .restart local v2    # "dataList":Lorg/json/JSONArray;
    .restart local v3    # "dataObj":Lorg/json/JSONObject;
    .restart local v8    # "i":I
    .restart local v11    # "values":Landroid/content/ContentValues;
    :cond_1
    if-eqz v1, :cond_0

    :try_start_4
    const-string v12, "photo"

    invoke-virtual {v1, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 250
    const-string v12, "data15"

    invoke-virtual {v11, v12}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 251
    .local v10, "temp":Ljava/lang/String;
    const/4 v9, 0x0

    .line 252
    .local v9, "image":[B
    if-eqz v10, :cond_2

    .line 253
    const/4 v12, 0x0

    invoke-static {v10, v12}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v9

    .line 255
    :cond_2
    const-string v12, "data15"

    invoke-virtual {v11, v12, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 256
    const-string v12, "data14"

    invoke-virtual {v11, v12}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 267
    .end local v1    # "Mimetype":Ljava/lang/String;
    .end local v2    # "dataList":Lorg/json/JSONArray;
    .end local v3    # "dataObj":Lorg/json/JSONObject;
    .end local v8    # "i":I
    .end local v9    # "image":[B
    .end local v10    # "temp":Ljava/lang/String;
    .end local v11    # "values":Landroid/content/ContentValues;
    :catchall_0
    move-exception v12

    iget-object v13, p0, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    throw v12

    .restart local v2    # "dataList":Lorg/json/JSONArray;
    .restart local v8    # "i":I
    :cond_3
    iget-object v12, p0, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 269
    const/4 v12, 0x1

    goto :goto_3
.end method

.method private insertRawContact(Lorg/json/JSONObject;Ljava/lang/String;JLjava/util/ArrayList;)Z
    .locals 7
    .param p1, "jsonObject"    # Lorg/json/JSONObject;
    .param p2, "syncKey"    # Ljava/lang/String;
    .param p3, "timeStamp"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            "Ljava/lang/String;",
            "J",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p5, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    const/4 v6, 0x0

    .line 208
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 209
    .local v1, "rawContactValue":Landroid/content/ContentValues;
    iget-object v3, p0, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    sget-object v4, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsBuilder;->RAW_CONTACTS_COLUMNS:[Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 211
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    const-string v4, "RAWCONTACT"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/sCloudSync/Records/JSONParser;->fromJSON(Lorg/json/JSONObject;)Landroid/content/ContentValues;

    move-result-object v1

    .line 213
    const-string v3, "sync1"

    invoke-virtual {v1, v3, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    const-string v3, "sync3"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 220
    iget-object v3, p0, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v3, v6}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 223
    sget-object v3, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    const-string v4, "caller_is_syncadapter"

    invoke-static {v3, v4}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 224
    .local v2, "rawUri":Landroid/net/Uri;
    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {p5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 225
    const/4 v3, 0x1

    .end local v2    # "rawUri":Landroid/net/Uri;
    :goto_0
    return v3

    .line 215
    :catch_0
    move-exception v0

    .line 216
    .local v0, "e":Lorg/json/JSONException;
    :try_start_1
    const-string v3, "ContactsBuilder"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "unable to insert:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 218
    const/4 v3, 0x0

    .line 220
    iget-object v4, p0, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v4, v6}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    goto :goto_0

    .end local v0    # "e":Lorg/json/JSONException;
    :catchall_0
    move-exception v3

    iget-object v4, p0, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v4, v6}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    throw v3
.end method


# virtual methods
.method public doApplyBatch()Z
    .locals 1

    .prologue
    .line 374
    const/4 v0, 0x0

    return v0
.end method

.method public insert(Ljava/lang/String;Ljava/lang/String;J)Z
    .locals 9
    .param p1, "jsonString"    # Ljava/lang/String;
    .param p2, "syncKey"    # Ljava/lang/String;
    .param p3, "timeStamp"    # J

    .prologue
    const/4 v8, 0x0

    .line 275
    const/4 v2, 0x0

    .line 277
    .local v2, "jsonObject":Lorg/json/JSONObject;
    :try_start_0
    new-instance v1, Lorg/json/JSONTokener;

    invoke-direct {v1, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lorg/json/JSONObject;

    move-object v2, v0
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 283
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .local v6, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    move-object v1, p0

    move-object v3, p2

    move-wide v4, p3

    .line 286
    invoke-direct/range {v1 .. v6}, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsBuilder;->insertRawContact(Lorg/json/JSONObject;Ljava/lang/String;JLjava/util/ArrayList;)Z

    move-result v1

    if-nez v1, :cond_0

    move v1, v8

    .line 294
    .end local v6    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :goto_0
    return v1

    .line 278
    :catch_0
    move-exception v7

    .line 279
    .local v7, "e1":Lorg/json/JSONException;
    invoke-virtual {v7}, Lorg/json/JSONException;->printStackTrace()V

    move v1, v8

    .line 280
    goto :goto_0

    .end local v7    # "e1":Lorg/json/JSONException;
    .restart local v6    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :cond_0
    move-object v1, p0

    move-object v3, p2

    move-wide v4, p3

    .line 290
    invoke-direct/range {v1 .. v6}, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsBuilder;->insertData(Lorg/json/JSONObject;Ljava/lang/String;JLjava/util/ArrayList;)Z

    move-result v1

    if-nez v1, :cond_1

    move v1, v8

    .line 291
    goto :goto_0

    .line 294
    :cond_1
    invoke-virtual {p0, v6}, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsBuilder;->doApplyBatch(Ljava/util/ArrayList;)Z

    move-result v1

    goto :goto_0
.end method

.method public parse(Landroid/database/Cursor;JLjava/lang/String;)Ljava/lang/String;
    .locals 13
    .param p1, "rawCursor"    # Landroid/database/Cursor;
    .param p2, "rowId"    # J
    .param p4, "key"    # Ljava/lang/String;

    .prologue
    .line 105
    new-instance v12, Lorg/json/JSONObject;

    invoke-direct {v12}, Lorg/json/JSONObject;-><init>()V

    .line 106
    .local v12, "parsedContactJson":Lorg/json/JSONObject;
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    sget-object v2, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsBuilder;->RAW_CONTACTS_COLUMNS:[Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 108
    const/4 v11, 0x0

    .line 109
    .local v11, "jsonObj":Lorg/json/JSONObject;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v0, p1}, Lcom/sec/android/sCloudSync/Records/JSONParser;->toJSON(Landroid/database/Cursor;)Lorg/json/JSONObject;

    move-result-object v11

    .line 110
    const-string v0, "RAWCONTACT"

    invoke-virtual {v12, v0, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 114
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 118
    :goto_0
    new-instance v7, Lorg/json/JSONArray;

    invoke-direct {v7}, Lorg/json/JSONArray;-><init>()V

    .line 119
    .local v7, "dataList":Lorg/json/JSONArray;
    sget-object v0, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static/range {p2 .. p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "data"

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 123
    .local v1, "dataUri":Landroid/net/Uri;
    const/4 v6, 0x0

    .line 125
    .local v6, "dataCursor":Landroid/database/Cursor;
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsBuilder;->mProvider:Landroid/content/ContentProviderClient;

    sget-object v2, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsBuilder;->DATA_COLUMNS:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v6

    .line 129
    :goto_1
    if-eqz v6, :cond_1

    .line 131
    const/4 v8, 0x0

    .line 132
    .local v8, "dataObj":Lorg/json/JSONObject;
    :goto_2
    :try_start_2
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 133
    invoke-direct {p0, v6}, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsBuilder;->checkGroupInformation(Landroid/database/Cursor;)Lorg/json/JSONObject;

    move-result-object v8

    .line 134
    if-nez v8, :cond_0

    .line 135
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    sget-object v2, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsBuilder;->DATA_COLUMNS:[Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 136
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v0, v6}, Lcom/sec/android/sCloudSync/Records/JSONParser;->toJSON(Landroid/database/Cursor;)Lorg/json/JSONObject;

    move-result-object v8

    .line 138
    :cond_0
    invoke-virtual {v7, v8}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_2

    .line 140
    :catch_0
    move-exception v9

    .line 141
    .local v9, "e":Lorg/json/JSONException;
    :try_start_3
    const-string v0, "ContactsBuilder"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unable to parse"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v9}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 143
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 144
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 148
    .end local v8    # "dataObj":Lorg/json/JSONObject;
    .end local v9    # "e":Lorg/json/JSONException;
    :cond_1
    :goto_3
    if-eqz v7, :cond_2

    .line 149
    :try_start_4
    const-string v0, "DATALIST"

    invoke-virtual {v12, v0, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_3

    .line 153
    :cond_2
    :goto_4
    invoke-virtual {v12}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 111
    .end local v1    # "dataUri":Landroid/net/Uri;
    .end local v6    # "dataCursor":Landroid/database/Cursor;
    .end local v7    # "dataList":Lorg/json/JSONArray;
    :catch_1
    move-exception v9

    .line 112
    .restart local v9    # "e":Lorg/json/JSONException;
    :try_start_5
    const-string v0, "ContactsBuilder"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "parse():Exception in parsing"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v9}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 114
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    goto/16 :goto_0

    .end local v9    # "e":Lorg/json/JSONException;
    :catchall_0
    move-exception v0

    iget-object v2, p0, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    throw v0

    .line 126
    .restart local v1    # "dataUri":Landroid/net/Uri;
    .restart local v6    # "dataCursor":Landroid/database/Cursor;
    .restart local v7    # "dataList":Lorg/json/JSONArray;
    :catch_2
    move-exception v10

    .line 127
    .local v10, "e1":Landroid/os/RemoteException;
    invoke-virtual {v10}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_1

    .line 143
    .end local v10    # "e1":Landroid/os/RemoteException;
    .restart local v8    # "dataObj":Lorg/json/JSONObject;
    :cond_3
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 144
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    goto :goto_3

    .line 143
    :catchall_1
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 144
    iget-object v2, p0, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    throw v0

    .line 150
    .end local v8    # "dataObj":Lorg/json/JSONObject;
    :catch_3
    move-exception v9

    .line 151
    .restart local v9    # "e":Lorg/json/JSONException;
    const-string v0, "ContactsBuilder"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unable to parse"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v9}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4
.end method

.method public update(Ljava/lang/String;JJLjava/lang/String;)Z
    .locals 1
    .param p1, "jsonString"    # Ljava/lang/String;
    .param p2, "timeStamp"    # J
    .param p4, "rowId"    # J
    .param p6, "syncKey"    # Ljava/lang/String;

    .prologue
    .line 202
    const/4 v0, 0x0

    return v0
.end method
