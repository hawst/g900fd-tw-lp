.class public interface abstract Lcom/sec/android/sCloudSync/Constants/SBrowserContract$TABS;
.super Ljava/lang/Object;
.source "SBrowserContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/sCloudSync/Constants/SBrowserContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "TABS"
.end annotation


# static fields
.field public static final ACCOUNT_NAME:Ljava/lang/String; = "ACCOUNT_NAME"

.field public static final ACCOUNT_TYPE:Ljava/lang/String; = "ACCOUNT_TYPE"

.field public static final DATE_CREATED:Ljava/lang/String; = "DATE_CREATED"

.field public static final DATE_MODIFIED:Ljava/lang/String; = "DATE_MODIFIED"

.field public static final DEVICE_ID:Ljava/lang/String; = "DEVICE_ID"

.field public static final DEVICE_NAME:Ljava/lang/String; = "DEVICE_NAME"

.field public static final DIRTY:Ljava/lang/String; = "DIRTY"

.field public static final ID:Ljava/lang/String; = "_ID"

.field public static final IS_DELETED:Ljava/lang/String; = "IS_DELETED"

.field public static final IS_INCOGNITO:Ljava/lang/String; = "IS_INCOGNITO"

.field public static final SYNC1:Ljava/lang/String; = "SYNC1"

.field public static final SYNC2:Ljava/lang/String; = "SYNC2"

.field public static final SYNC3:Ljava/lang/String; = "SYNC3"

.field public static final SYNC4:Ljava/lang/String; = "SYNC4"

.field public static final SYNC5:Ljava/lang/String; = "SYNC5"

.field public static final TAB_ACTIVATE:Ljava/lang/String; = "TAB_ACTIVATE"

.field public static final TAB_FAV_ICON:Ljava/lang/String; = "TAB_FAV_ICON"

.field public static final TAB_ID:Ljava/lang/String; = "TAB_ID"

.field public static final TAB_INDEX:Ljava/lang/String; = "TAB_INDEX"

.field public static final TAB_TITLE:Ljava/lang/String; = "TAB_TITLE"

.field public static final TAB_URL:Ljava/lang/String; = "TAB_URL"

.field public static final TAB_USAGE:Ljava/lang/String; = "TAB_USAGE"
