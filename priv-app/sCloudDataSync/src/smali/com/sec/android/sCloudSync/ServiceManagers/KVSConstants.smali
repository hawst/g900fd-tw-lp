.class public interface abstract Lcom/sec/android/sCloudSync/ServiceManagers/KVSConstants;
.super Ljava/lang/Object;
.source "KVSConstants.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/sCloudSync/ServiceManagers/KVSConstants$API;
    }
.end annotation


# static fields
.field public static final CDID:Ljava/lang/String; = "cdid"

.field public static final CLOUD_END:Ljava/lang/String; = "/cloud/end?"

.field public static final CLOUD_PREPARE:Ljava/lang/String; = "/cloud/prepare?"

.field public static final CLOUD_START:Ljava/lang/String; = "/cloud/start?"

.field public static final COUNT:Ljava/lang/String; = "&count="

.field public static final CTID:Ljava/lang/String; = "ctid"

.field public static final DELETE:Ljava/lang/String; = "/kvs/?action=delete"

.field public static final DID:Ljava/lang/String; = "did"

.field public static final GETKEY:Ljava/lang/String; = "/kvs/key?"

.field public static final GETKEYS:Ljava/lang/String; = "/kvs/keys?"

.field public static final ITEM:Ljava/lang/String; = "/kvs/item?"

.field public static final ITEMS:Ljava/lang/String; = "/kvs/items?"

.field public static final RESULT:Ljava/lang/String; = "result"

.field public static final RESULT_ERROR:Ljava/lang/String; = "e"

.field public static final RESULT_RCODE:Ljava/lang/String; = "r"

.field public static final RESULT_RCODE_SUC:Ljava/lang/String; = "0"

.field public static final SET:Ljava/lang/String; = "/kvs/?action=set"

.field public static final START:Ljava/lang/String; = "&start="

.field public static final START_TIMESTAMP:Ljava/lang/String; = "&start_timestamp="

.field public static final SYNC_PUSH:Ljava/lang/String; = "sync_push"

.field public static final SYNC_SELF:Ljava/lang/String; = "sync_self"

.field public static final TIMESTAMP:Ljava/lang/String; = "/kvs/timestamp?"

.field public static final TRIGGER:Ljava/lang/String; = "trigger"

.field public static final UPDATE:Ljava/lang/String; = "/kvs/updates?"
