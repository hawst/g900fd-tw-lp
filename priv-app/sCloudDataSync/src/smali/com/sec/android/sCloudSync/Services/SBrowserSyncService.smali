.class public Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;
.super Landroid/app/Service;
.source "SBrowserSyncService.java"


# static fields
.field private static final ADAPTER_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private static final ENABLED_SYNC:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final SYNC_BOOKMARKS:Ljava/lang/String; = "sync_bookmarks"

.field private static final SYNC_INTERNET_DATA:Ljava/lang/String; = "sync_internet_data"

.field private static final SYNC_SAVEDPAGES:Ljava/lang/String; = "sync_saved_pages"

.field private static final SYNC_TABS:Ljava/lang/String; = "sync_open_pages"

.field private static final TAG:Ljava/lang/String; = "SBROWSER-SYNC"


# instance fields
.field private isCancel:Z

.field private final mBinder:Lcom/sec/android/sCloudSync/IDataSync/IDataSync$Stub;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;->ADAPTER_MAP:Ljava/util/Map;

    .line 54
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;->ENABLED_SYNC:Ljava/util/HashSet;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 78
    new-instance v0, Lcom/sec/android/sCloudSync/Services/SBrowserSyncService$1;

    invoke-direct {v0, p0}, Lcom/sec/android/sCloudSync/Services/SBrowserSyncService$1;-><init>(Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;)V

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;->mBinder:Lcom/sec/android/sCloudSync/IDataSync/IDataSync$Stub;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;->isCancel:Z

    return v0
.end method

.method static synthetic access$002(Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;
    .param p1, "x1"    # Z

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;->isCancel:Z

    return p1
.end method

.method static synthetic access$100()Ljava/util/HashSet;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;->ENABLED_SYNC:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic access$200()Ljava/util/Map;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;->ADAPTER_MAP:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;->mBinder:Lcom/sec/android/sCloudSync/IDataSync/IDataSync$Stub;

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    .prologue
    .line 58
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 60
    sget-object v0, Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;->ADAPTER_MAP:Ljava/util/Map;

    const-string v1, "sync_saved_pages"

    new-instance v2, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserSavedPagesSyncAdapter;-><init>(Landroid/content/Context;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    sget-object v0, Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;->ADAPTER_MAP:Ljava/util/Map;

    const-string v1, "sync_open_pages"

    new-instance v2, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserTabSyncAdapter;

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/android/sCloudSync/SyncAdapters/SBrowserTabSyncAdapter;-><init>(Landroid/content/Context;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    sget-object v0, Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;->ADAPTER_MAP:Ljava/util/Map;

    const-string v1, "sync_bookmarks"

    new-instance v2, Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/android/sCloudSync/SyncAdapters/BrowserSyncAdapter;-><init>(Landroid/content/Context;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 74
    const-string v0, "SBROWSER-SYNC"

    const-string v1, "SYNC SERVICE END"

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 76
    return-void
.end method
