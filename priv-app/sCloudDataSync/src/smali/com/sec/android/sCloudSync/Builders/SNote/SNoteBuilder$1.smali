.class Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder$1;
.super Ljava/lang/Object;
.source "SNoteBuilder.java"

# interfaces
.implements Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder$PostOperationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->insertFileDetailTable(Lorg/json/JSONObject;Ljava/util/ArrayList;Ljava/util/ArrayList;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;

.field final synthetic val$operations:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 443
    iput-object p1, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder$1;->this$0:Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;

    iput-object p2, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder$1;->val$operations:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public doPost(Landroid/content/ContentValues;)V
    .locals 5
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 446
    iget-object v1, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder$1;->this$0:Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;

    # getter for: Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->mContract:Lcom/sec/android/sCloudSync/Constants/SNoteContract;
    invoke-static {v1}, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;->access$000(Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder;)Lcom/sec/android/sCloudSync/Constants/SNoteContract;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->FILEDETAIL_CONTENT_URI:Landroid/net/Uri;

    const-string v2, "caller_is_syncadapter"

    invoke-static {v1, v2}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 448
    .local v0, "fileDetailUri":Landroid/net/Uri;
    iget-object v1, p0, Lcom/sec/android/sCloudSync/Builders/SNote/SNoteBuilder$1;->val$operations:Ljava/util/ArrayList;

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "snb_id"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 451
    return-void
.end method
