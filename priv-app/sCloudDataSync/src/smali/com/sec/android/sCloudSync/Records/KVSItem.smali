.class public Lcom/sec/android/sCloudSync/Records/KVSItem;
.super Ljava/lang/Object;
.source "KVSItem.java"


# instance fields
.field private mDeleted:Z

.field private mId:J

.field private mSize:J

.field private mTimeStamp:J

.field private mValue:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const-wide/16 v1, -0x1

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-wide v1, p0, Lcom/sec/android/sCloudSync/Records/KVSItem;->mId:J

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/sCloudSync/Records/KVSItem;->mDeleted:Z

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Records/KVSItem;->mValue:Ljava/lang/String;

    .line 34
    iput-wide v1, p0, Lcom/sec/android/sCloudSync/Records/KVSItem;->mSize:J

    .line 35
    return-void
.end method

.method public constructor <init>(JZJLjava/lang/String;J)V
    .locals 0
    .param p1, "id"    # J
    .param p3, "bDeleted"    # Z
    .param p4, "timeStamp"    # J
    .param p6, "value"    # Ljava/lang/String;
    .param p7, "size"    # J

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-wide p1, p0, Lcom/sec/android/sCloudSync/Records/KVSItem;->mId:J

    .line 40
    iput-boolean p3, p0, Lcom/sec/android/sCloudSync/Records/KVSItem;->mDeleted:Z

    .line 41
    iput-wide p4, p0, Lcom/sec/android/sCloudSync/Records/KVSItem;->mTimeStamp:J

    .line 42
    iput-object p6, p0, Lcom/sec/android/sCloudSync/Records/KVSItem;->mValue:Ljava/lang/String;

    .line 43
    iput-wide p7, p0, Lcom/sec/android/sCloudSync/Records/KVSItem;->mSize:J

    .line 44
    return-void
.end method


# virtual methods
.method public getID()J
    .locals 2

    .prologue
    .line 48
    iget-wide v0, p0, Lcom/sec/android/sCloudSync/Records/KVSItem;->mId:J

    return-wide v0
.end method

.method public getSize()J
    .locals 2

    .prologue
    .line 83
    iget-wide v0, p0, Lcom/sec/android/sCloudSync/Records/KVSItem;->mSize:J

    return-wide v0
.end method

.method public getTimeStamp()J
    .locals 2

    .prologue
    .line 63
    iget-wide v0, p0, Lcom/sec/android/sCloudSync/Records/KVSItem;->mTimeStamp:J

    return-wide v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Records/KVSItem;->mValue:Ljava/lang/String;

    return-object v0
.end method

.method public isDeleted()Z
    .locals 1

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/sec/android/sCloudSync/Records/KVSItem;->mDeleted:Z

    return v0
.end method

.method public setID(J)V
    .locals 0
    .param p1, "id"    # J

    .prologue
    .line 53
    iput-wide p1, p0, Lcom/sec/android/sCloudSync/Records/KVSItem;->mId:J

    .line 54
    return-void
.end method

.method public setTimeStamp(J)V
    .locals 0
    .param p1, "timeStamp"    # J

    .prologue
    .line 68
    iput-wide p1, p0, Lcom/sec/android/sCloudSync/Records/KVSItem;->mTimeStamp:J

    .line 69
    return-void
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/sec/android/sCloudSync/Records/KVSItem;->mValue:Ljava/lang/String;

    .line 79
    return-void
.end method
