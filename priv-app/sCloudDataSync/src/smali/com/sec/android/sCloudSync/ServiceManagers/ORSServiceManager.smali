.class public Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;
.super Lcom/sec/android/sCloudSync/ServiceManagers/AbstractServiceManager;
.source "ORSServiceManager.java"

# interfaces
.implements Lcom/sec/android/sCloudSync/ServiceManagers/IPDMServiceApiHelper;


# static fields
.field private static final APP_ID:Ljava/lang/String; = "tj9u972o46"

.field private static final APP_ID_HEADER:Ljava/lang/String; = "x-sc-appid"

.field private static final DETAIL:Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "ORSServiceManager"

.field private static final USER_AGENT:Ljava/lang/String;


# instance fields
.field private mFileBuffer:[B

.field mORSMetaResponse:Lcom/sec/android/sCloudSync/Records/ORSResponse;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 57
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "; sw="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "android"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "DataSync"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "5.6.5"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->DETAIL:Ljava/lang/String;

    .line 59
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Landroid/os/Build;->DISPLAY:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->DETAIL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->USER_AGENT:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cid"    # Ljava/lang/String;
    .param p3, "cloud"    # Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    .prologue
    .line 66
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/sCloudSync/ServiceManagers/AbstractServiceManager;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;)V

    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->mFileBuffer:[B

    .line 63
    new-instance v0, Lcom/sec/android/sCloudSync/Records/ORSResponse;

    invoke-direct {v0}, Lcom/sec/android/sCloudSync/Records/ORSResponse;-><init>()V

    iput-object v0, p0, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->mORSMetaResponse:Lcom/sec/android/sCloudSync/Records/ORSResponse;

    .line 67
    iget-object v0, p0, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->mNetClient:Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;

    invoke-virtual {v0, p0}, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->setServiceApiHelper(Lcom/sec/android/sCloudSync/ServiceManagers/IPDMServiceApiHelper;)V

    .line 68
    return-void
.end method

.method private setBuffer([B)V
    .locals 0
    .param p1, "buffer"    # [B

    .prologue
    .line 190
    iput-object p1, p0, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->mFileBuffer:[B

    .line 191
    return-void
.end method


# virtual methods
.method public addHeaderForApi(Lorg/apache/http/client/methods/HttpUriRequest;)V
    .locals 2
    .param p1, "request"    # Lorg/apache/http/client/methods/HttpUriRequest;

    .prologue
    .line 291
    const-string v0, "x-sc-appid"

    const-string v1, "tj9u972o46"

    invoke-interface {p1, v0, v1}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    return-void
.end method

.method public createFolder(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/sCloudSync/Records/ORSResponse;
    .locals 15
    .param p2, "folderPath"    # Ljava/lang/String;
    .param p3, "folderName"    # Ljava/lang/String;
    .param p4, "timeStamp"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/sCloudSync/Records/RecordORSItem;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/sec/android/sCloudSync/Records/ORSResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .line 194
    .local p1, "recordORSList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordORSItem;>;"
    iget-object v1, p0, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v1}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getAuthManager()Lcom/sec/android/sCloudSync/Auth/AuthManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/sCloudSync/Auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v7

    .line 195
    .local v7, "baseUrl":Ljava/lang/String;
    if-nez v7, :cond_0

    .line 197
    const-string v1, "ORSServiceManager"

    const-string v2, "There is NO Base URL."

    invoke-static {v1, v2}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    new-instance v1, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/4 v2, 0x3

    invoke-direct {v1, v2}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v1

    .line 200
    :cond_0
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 201
    .local v14, "url":Ljava/lang/StringBuilder;
    const-string v1, "/ors"

    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 202
    move-object/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 203
    const-string v1, "?action=create"

    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 204
    iget-object v1, p0, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v1}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getAuthManager()Lcom/sec/android/sCloudSync/Auth/AuthManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->mCid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/sCloudSync/Auth/AuthManager;->getPutApiParams(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 205
    const-string v1, "&folder_name="

    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 206
    move-object/from16 v0, p3

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 207
    const-string v1, "&timestamp="

    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 208
    move-object/from16 v0, p4

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 210
    const-string v1, "ctid"

    iget-object v2, p0, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v2}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getCtid()Ljava/lang/String;

    move-result-object v2

    invoke-static {v14, v1, v2}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addUriParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 213
    const/4 v9, 0x0

    .line 214
    .local v9, "insertKeyObject":Lorg/json/JSONObject;
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .line 215
    .local v10, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/sCloudSync/Records/RecordORSItem;>;"
    new-instance v13, Lorg/json/JSONArray;

    invoke-direct {v13}, Lorg/json/JSONArray;-><init>()V

    .line 216
    .local v13, "setkeys":Lorg/json/JSONArray;
    const-string v1, "ORSServiceManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "RecordDataItem Size = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    new-instance v11, Lorg/json/JSONObject;

    invoke-direct {v11}, Lorg/json/JSONObject;-><init>()V

    .line 220
    .local v11, "json":Lorg/json/JSONObject;
    :goto_0
    :try_start_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 221
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/sCloudSync/Records/RecordORSItem;

    .line 222
    .local v12, "recordItemObject":Lcom/sec/android/sCloudSync/Records/RecordORSItem;
    invoke-virtual {v12}, Lcom/sec/android/sCloudSync/Records/RecordORSItem;->toJSON()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lorg/json/JSONObject;

    move-object v9, v0

    .line 223
    invoke-virtual {v13, v9}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 226
    .end local v12    # "recordItemObject":Lcom/sec/android/sCloudSync/Records/RecordORSItem;
    :catch_0
    move-exception v8

    .line 227
    .local v8, "e":Lorg/json/JSONException;
    const-string v1, "ORSServiceManager"

    const-string v2, "JSON PARSER Exception"

    invoke-static {v1, v2}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    new-instance v1, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v1

    .line 225
    .end local v8    # "e":Lorg/json/JSONException;
    :cond_1
    :try_start_1
    const-string v1, "metadata"

    invoke-virtual {v11, v1, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 231
    iget-object v1, p0, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    const/16 v2, 0x136

    invoke-interface {v1, v2}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->setLastApi(I)V

    .line 232
    const/4 v2, 0x0

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->mORSMetaResponse:Lcom/sec/android/sCloudSync/Records/ORSResponse;

    invoke-virtual {v11}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x136

    move-object v1, p0

    invoke-virtual/range {v1 .. v6}, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->handleRequest(ILjava/lang/String;Lcom/sec/android/sCloudSync/Records/AbstractJSON;Ljava/lang/String;I)Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;

    .line 234
    iget-object v1, p0, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->mORSMetaResponse:Lcom/sec/android/sCloudSync/Records/ORSResponse;

    return-object v1
.end method

.method public deleteFile(Ljava/lang/String;)Lcom/sec/android/sCloudSync/Records/ORSResponse;
    .locals 6
    .param p1, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    const/16 v5, 0x137

    .line 239
    iget-object v2, p0, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v2}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getAuthManager()Lcom/sec/android/sCloudSync/Auth/AuthManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/sCloudSync/Auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v0

    .line 240
    .local v0, "baseUrl":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 242
    const-string v2, "ORSServiceManager"

    const-string v3, "There is NO Base URL."

    invoke-static {v2, v3}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    new-instance v2, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/4 v3, 0x3

    invoke-direct {v2, v3}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v2

    .line 245
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 246
    .local v1, "url":Ljava/lang/StringBuilder;
    const-string v2, "/ors"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 247
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 248
    const-string v2, "?action=delete"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 249
    iget-object v2, p0, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v2}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getAuthManager()Lcom/sec/android/sCloudSync/Auth/AuthManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->mCid:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/android/sCloudSync/Auth/AuthManager;->getPutApiParams(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 251
    const-string v2, "ctid"

    iget-object v3, p0, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v3}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getCtid()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addUriParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 254
    iget-object v2, p0, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v2, v5}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->setLastApi(I)V

    .line 255
    const/4 v2, 0x0

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->mORSMetaResponse:Lcom/sec/android/sCloudSync/Records/ORSResponse;

    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->handleRequest(ILjava/lang/String;Lcom/sec/android/sCloudSync/Records/AbstractJSON;I)Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;

    .line 257
    iget-object v2, p0, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->mORSMetaResponse:Lcom/sec/android/sCloudSync/Records/ORSResponse;

    return-object v2
.end method

.method public downloadFile(Ljava/lang/String;Ljava/lang/String;Z)Lcom/sec/android/sCloudSync/Records/ORSResponse;
    .locals 10
    .param p1, "serverFilePath"    # Ljava/lang/String;
    .param p2, "localFilePath"    # Ljava/lang/String;
    .param p3, "writeToByteArray"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x4

    .line 138
    iget-object v5, p0, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v5}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getAuthManager()Lcom/sec/android/sCloudSync/Auth/AuthManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/sCloudSync/Auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v0

    .line 139
    .local v0, "baseUrl":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 141
    const-string v5, "ORSServiceManager"

    const-string v6, "There is NO Base URL."

    invoke-static {v5, v6}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    new-instance v5, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/4 v6, 0x3

    invoke-direct {v5, v6}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v5

    .line 144
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 145
    .local v4, "url":Ljava/lang/StringBuilder;
    const-string v5, "/ors"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 146
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    const-string v5, "?action=download"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 148
    iget-object v5, p0, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v5}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getAuthManager()Lcom/sec/android/sCloudSync/Auth/AuthManager;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->mCid:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/sec/android/sCloudSync/Auth/AuthManager;->getPutApiParams(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    const-string v5, "ctid"

    iget-object v6, p0, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v6}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getCtid()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addUriParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 154
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    const/16 v6, 0x13c

    invoke-interface {v5, v6}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->setLastApi(I)V

    .line 155
    const/4 v5, 0x4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->mORSMetaResponse:Lcom/sec/android/sCloudSync/Records/ORSResponse;

    const/16 v8, 0x13c

    invoke-virtual {p0, v5, v6, v7, v8}, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->handleRequest(ILjava/lang/String;Lcom/sec/android/sCloudSync/Records/AbstractJSON;I)Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;

    move-result-object v3

    .line 156
    .local v3, "networkResponse":Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;
    invoke-virtual {v3}, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;->isFileStream()Z
    :try_end_0
    .catch Lcom/sec/android/sCloudSync/Util/SyncException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v5

    if-eqz v5, :cond_1

    .line 158
    :try_start_1
    iget-object v5, p0, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->mORSMetaResponse:Lcom/sec/android/sCloudSync/Records/ORSResponse;

    invoke-virtual {v5}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->clear()V

    .line 159
    if-eqz p3, :cond_2

    .line 160
    invoke-virtual {v3}, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;->getFileInStream()Ljava/io/InputStream;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/sCloudSync/Tools/FileTool;->getByteArr(Ljava/io/InputStream;)[B

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->setBuffer([B)V

    .line 163
    :goto_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 164
    .local v2, "json":Lorg/json/JSONObject;
    const-string v5, "rcode"

    const/4 v6, 0x0

    invoke-virtual {v2, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 165
    iget-object v5, p0, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->mORSMetaResponse:Lcom/sec/android/sCloudSync/Records/ORSResponse;

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->fromJSON(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/sec/android/sCloudSync/Util/SyncException; {:try_start_1 .. :try_end_1} :catch_1

    .line 180
    .end local v2    # "json":Lorg/json/JSONObject;
    :cond_1
    iget-object v5, p0, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->mORSMetaResponse:Lcom/sec/android/sCloudSync/Records/ORSResponse;

    return-object v5

    .line 162
    :cond_2
    :try_start_2
    invoke-virtual {v3}, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;->getFileInStream()Ljava/io/InputStream;

    move-result-object v5

    invoke-static {v5, p2}, Lcom/sec/android/sCloudSync/Tools/FileTool;->writeToFile(Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lcom/sec/android/sCloudSync/Util/SyncException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 166
    :catch_0
    move-exception v1

    .line 167
    .local v1, "e":Ljava/io/IOException;
    :try_start_3
    const-string v5, "ORSServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IOException occured while Http Request, caused by : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    throw v1
    :try_end_3
    .catch Lcom/sec/android/sCloudSync/Util/SyncException; {:try_start_3 .. :try_end_3} :catch_1

    .line 174
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "networkResponse":Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;
    :catch_1
    move-exception v1

    .line 175
    .local v1, "e":Lcom/sec/android/sCloudSync/Util/SyncException;
    invoke-virtual {v1}, Lcom/sec/android/sCloudSync/Util/SyncException;->getmExceptionCode()I

    move-result v5

    if-ne v5, v9, :cond_3

    .line 176
    new-instance v5, Ljava/io/IOException;

    invoke-direct {v5}, Ljava/io/IOException;-><init>()V

    throw v5

    .line 169
    .end local v1    # "e":Lcom/sec/android/sCloudSync/Util/SyncException;
    .restart local v3    # "networkResponse":Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;
    :catch_2
    move-exception v1

    .line 170
    .local v1, "e":Lorg/json/JSONException;
    :try_start_4
    const-string v5, "ORSServiceManager"

    const-string v6, "JSON PARSER Exception"

    invoke-static {v5, v6}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    new-instance v5, Ljava/io/IOException;

    invoke-direct {v5, v1}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v5
    :try_end_4
    .catch Lcom/sec/android/sCloudSync/Util/SyncException; {:try_start_4 .. :try_end_4} :catch_1

    .line 178
    .end local v3    # "networkResponse":Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;
    .local v1, "e":Lcom/sec/android/sCloudSync/Util/SyncException;
    :cond_3
    new-instance v5, Lcom/sec/android/sCloudSync/Util/SyncException;

    invoke-direct {v5, v9}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v5
.end method

.method public getBuffer()[B
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->mFileBuffer:[B

    return-object v0
.end method

.method public getMetadata(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/sCloudSync/Records/ORSResponse;
    .locals 6
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "syncKey"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    const/16 v5, 0x13e

    .line 262
    iget-object v2, p0, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v2}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getAuthManager()Lcom/sec/android/sCloudSync/Auth/AuthManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/sCloudSync/Auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v0

    .line 263
    .local v0, "baseUrl":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 265
    const-string v2, "ORSServiceManager"

    const-string v3, "There is NO Base URL."

    invoke-static {v2, v3}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    new-instance v2, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/4 v3, 0x3

    invoke-direct {v2, v3}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v2

    .line 269
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 270
    .local v1, "url":Ljava/lang/StringBuilder;
    const-string v2, "/ors"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 271
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 272
    const-string v2, "?action=get"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 273
    iget-object v2, p0, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v2}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getAuthManager()Lcom/sec/android/sCloudSync/Auth/AuthManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->mCid:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/android/sCloudSync/Auth/AuthManager;->getPutApiParams(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 275
    const-string v2, "ctid"

    iget-object v3, p0, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v3}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getCtid()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addUriParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 278
    if-eqz p2, :cond_1

    .line 279
    const-string v2, "&key="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 280
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 283
    :cond_1
    iget-object v2, p0, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v2, v5}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->setLastApi(I)V

    .line 284
    const/4 v2, 0x5

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->mORSMetaResponse:Lcom/sec/android/sCloudSync/Records/ORSResponse;

    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->handleRequest(ILjava/lang/String;Lcom/sec/android/sCloudSync/Records/AbstractJSON;I)Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;

    .line 286
    iget-object v2, p0, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->mORSMetaResponse:Lcom/sec/android/sCloudSync/Records/ORSResponse;

    return-object v2
.end method

.method public getUserAgent()Ljava/lang/String;
    .locals 1

    .prologue
    .line 296
    sget-object v0, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->USER_AGENT:Ljava/lang/String;

    return-object v0
.end method

.method public uploadFile(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z[B)Lcom/sec/android/sCloudSync/Records/ORSResponse;
    .locals 17
    .param p2, "fileName"    # Ljava/lang/String;
    .param p3, "timeStamp"    # Ljava/lang/String;
    .param p4, "serverFilePath"    # Ljava/lang/String;
    .param p5, "localFilePath"    # Ljava/lang/String;
    .param p6, "isFileBody"    # Z
    .param p7, "byteArray"    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/sCloudSync/Records/RecordORSItem;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z[B)",
            "Lcom/sec/android/sCloudSync/Records/ORSResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .line 75
    .local p1, "recordORSList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordORSItem;>;"
    if-nez p1, :cond_0

    .line 76
    const/4 v2, 0x0

    .line 133
    :goto_0
    return-object v2

    .line 79
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v2}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getAuthManager()Lcom/sec/android/sCloudSync/Auth/AuthManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/sCloudSync/Auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v8

    .line 80
    .local v8, "baseUrl":Ljava/lang/String;
    if-nez v8, :cond_1

    .line 82
    const-string v2, "ORSServiceManager"

    const-string v3, "There is NO Base URL."

    invoke-static {v2, v3}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    new-instance v2, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/4 v3, 0x3

    invoke-direct {v2, v3}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v2

    .line 85
    :cond_1
    new-instance v16, Ljava/lang/StringBuilder;

    move-object/from16 v0, v16

    invoke-direct {v0, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 87
    .local v16, "url":Ljava/lang/StringBuilder;
    const-string v2, "/ors"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    move-object/from16 v0, v16

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    const-string v2, "?action=upload"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v2}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getAuthManager()Lcom/sec/android/sCloudSync/Auth/AuthManager;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->mCid:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/android/sCloudSync/Auth/AuthManager;->getPutApiParams(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    const-string v2, "&file_name="

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 92
    move-object/from16 v0, v16

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 93
    const-string v2, "&timestamp="

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    move-object/from16 v0, v16

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    const-string v2, "ctid"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    invoke-interface {v3}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getCtid()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-static {v0, v2, v3}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addUriParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 99
    const/4 v11, 0x0

    .line 100
    .local v11, "insertKeyObject":Lorg/json/JSONObject;
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .line 101
    .local v13, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/sCloudSync/Records/RecordORSItem;>;"
    new-instance v15, Lorg/json/JSONArray;

    invoke-direct {v15}, Lorg/json/JSONArray;-><init>()V

    .line 102
    .local v15, "setkeys":Lorg/json/JSONArray;
    const-string v2, "ORSServiceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "RecordFileItem Size = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    new-instance v14, Lorg/json/JSONObject;

    invoke-direct {v14}, Lorg/json/JSONObject;-><init>()V

    .line 105
    .local v14, "json":Lorg/json/JSONObject;
    new-instance v6, Lorg/apache/http/entity/mime/MultipartEntity;

    const/4 v2, 0x0

    const-string v3, "AaB03x"

    const/4 v4, 0x0

    invoke-direct {v6, v2, v3, v4}, Lorg/apache/http/entity/mime/MultipartEntity;-><init>(Lorg/apache/http/entity/mime/HttpMultipartMode;Ljava/lang/String;Ljava/nio/charset/Charset;)V

    .line 108
    .local v6, "requestEntity":Lorg/apache/http/entity/mime/MultipartEntity;
    :goto_1
    :try_start_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 109
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/sCloudSync/Records/RecordORSItem;

    .line 110
    .local v12, "item":Lcom/sec/android/sCloudSync/Records/RecordORSItem;
    invoke-virtual {v12}, Lcom/sec/android/sCloudSync/Records/RecordORSItem;->toJSON()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lorg/json/JSONObject;

    move-object v11, v0

    .line 111
    invoke-virtual {v15, v11}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 122
    .end local v12    # "item":Lcom/sec/android/sCloudSync/Records/RecordORSItem;
    :catch_0
    move-exception v10

    .line 123
    .local v10, "e1":Lorg/json/JSONException;
    const-string v2, "ORSServiceManager"

    const-string v3, "JSON PARSER Exception."

    invoke-static {v2, v3}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    new-instance v2, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/4 v3, 0x2

    invoke-direct {v2, v3}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v2

    .line 114
    .end local v10    # "e1":Lorg/json/JSONException;
    :cond_2
    :try_start_1
    const-string v2, "metadata"

    invoke-virtual {v14, v2, v15}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 115
    const-string v2, "metadata"

    new-instance v3, Lorg/apache/http/entity/mime/content/StringBody;

    invoke-virtual {v14}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "application/json"

    const-string v7, "UTF-8"

    invoke-static {v7}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v7

    invoke-direct {v3, v4, v5, v7}, Lorg/apache/http/entity/mime/content/StringBody;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/nio/charset/Charset;)V

    invoke-virtual {v6, v2, v3}, Lorg/apache/http/entity/mime/MultipartEntity;->addPart(Ljava/lang/String;Lorg/apache/http/entity/mime/content/ContentBody;)V

    .line 117
    if-eqz p6, :cond_3

    .line 118
    const-string v2, "file"

    new-instance v3, Lorg/apache/http/entity/mime/content/FileBody;

    new-instance v4, Ljava/io/File;

    move-object/from16 v0, p5

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v5, "application/octet-stream"

    const-string v7, "binary"

    move-object/from16 v0, p2

    invoke-direct {v3, v4, v0, v5, v7}, Lorg/apache/http/entity/mime/content/FileBody;-><init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v2, v3}, Lorg/apache/http/entity/mime/MultipartEntity;->addPart(Ljava/lang/String;Lorg/apache/http/entity/mime/content/ContentBody;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    .line 130
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    const/16 v3, 0x13b

    invoke-interface {v2, v3}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->setLastApi(I)V

    .line 131
    const/4 v3, 0x2

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->mORSMetaResponse:Lcom/sec/android/sCloudSync/Records/ORSResponse;

    const/16 v7, 0x13b

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->handleRequest(ILjava/lang/String;Lcom/sec/android/sCloudSync/Records/AbstractJSON;Lorg/apache/http/entity/mime/MultipartEntity;I)Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;

    .line 133
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->mORSMetaResponse:Lcom/sec/android/sCloudSync/Records/ORSResponse;

    goto/16 :goto_0

    .line 121
    :cond_3
    :try_start_2
    const-string v2, "file"

    new-instance v3, Lorg/apache/http/entity/mime/content/ByteArrayBody;

    const-string v4, "application/octet-stream"

    move-object/from16 v0, p7

    move-object/from16 v1, p2

    invoke-direct {v3, v0, v4, v1}, Lorg/apache/http/entity/mime/content/ByteArrayBody;-><init>([BLjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v2, v3}, Lorg/apache/http/entity/mime/MultipartEntity;->addPart(Ljava/lang/String;Lorg/apache/http/entity/mime/content/ContentBody;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    .line 125
    :catch_1
    move-exception v9

    .line 126
    .local v9, "e":Ljava/io/UnsupportedEncodingException;
    const-string v2, "ORSServiceManager"

    const-string v3, "UnsupportedEncodingException"

    invoke-static {v2, v3}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    new-instance v2, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v2
.end method
