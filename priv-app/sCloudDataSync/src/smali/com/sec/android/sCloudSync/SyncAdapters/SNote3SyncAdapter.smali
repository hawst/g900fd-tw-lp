.class public Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;
.super Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;
.source "SNote3SyncAdapter.java"


# static fields
.field private static final CHECSKSUM:Ljava/lang/String; = "checksum"

.field private static final DIRTY_VALUES:Landroid/content/ContentValues;

.field private static final FILENAME:Ljava/lang/String; = "filename"

.field private static final JSON:Ljava/lang/String; = "json"

.field private static final PRIVATEKEY:Ljava/lang/String; = "privatekey"

.field private static final SERVER_SNOTE_FOLDER:Ljava/lang/String; = "/SNOTE3/"

.field private static final SLASH:Ljava/lang/String; = "/"

.field private static final TAG:Ljava/lang/String; = "SNote3SyncAdapter"

.field private static final TIMESTAMP:Ljava/lang/String; = "timestamp"

.field private static mData:[B


# instance fields
.field private final TEMP_FOLDER:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 86
    const/high16 v0, 0x100000

    new-array v0, v0, [B

    sput-object v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->mData:[B

    .line 88
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    sput-object v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->DIRTY_VALUES:Landroid/content/ContentValues;

    .line 90
    sget-object v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->DIRTY_VALUES:Landroid/content/ContentValues;

    const-string v1, "dirty"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 91
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 94
    new-instance v0, Lcom/sec/android/sCloudSync/ServiceManagers/SCloudServiceManager;

    const-string v1, "PM3HWwUYhP"

    const/4 v2, 0x1

    invoke-direct {v0, p1, v1, v2}, Lcom/sec/android/sCloudSync/ServiceManagers/SCloudServiceManager;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    invoke-direct {p0, p1, v0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;-><init>(Landroid/content/Context;Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;)V

    .line 77
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/files/temp/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->TEMP_FOLDER:Ljava/lang/String;

    .line 95
    return-void
.end method

.method private clearGarbage()Z
    .locals 4

    .prologue
    .line 217
    sget-object v2, Lcom/sec/android/sCloudSync/Constants/SNote3Contract;->DELETEALLSNB_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 219
    .local v0, "deleteAllSnbUri":Landroid/net/Uri;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentProviderClient;->openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_2

    .line 227
    :goto_0
    const/4 v2, 0x1

    return v2

    .line 220
    :catch_0
    move-exception v1

    .line 221
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 222
    .end local v1    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v1

    .line 223
    .local v1, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 224
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    :catch_2
    move-exception v1

    .line 225
    .local v1, "e":Ljava/lang/UnsupportedOperationException;
    invoke-virtual {v1}, Ljava/lang/UnsupportedOperationException;->printStackTrace()V

    goto :goto_0
.end method

.method private createSNoteFolderStructure()Z
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .line 1040
    const/4 v0, 0x0

    .line 1041
    .local v0, "createFolderResponse":Lcom/sec/android/sCloudSync/Records/ORSResponse;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1043
    .local v2, "recordMetaDataList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordORSItem;>;"
    new-instance v3, Lcom/sec/android/sCloudSync/Records/RecordORSItem;

    const-string v4, "FolderPath : /"

    const-string v5, "SNote"

    invoke-direct {v3, v4, v5}, Lcom/sec/android/sCloudSync/Records/RecordORSItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1045
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getCloudServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    move-result-object v3

    invoke-interface {v3}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getFileServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;

    move-result-object v3

    const-string v4, "/"

    const-string v5, "SNote"

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/android/scloud/framework/util/TimeManager;->getCurrentTime(Landroid/content/Context;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v2, v4, v5, v6}, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->createFolder(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/sCloudSync/Records/ORSResponse;
    :try_end_0
    .catch Lcom/sec/android/sCloudSync/Util/SyncException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1052
    invoke-virtual {v0}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getResponseCode()I

    move-result v3

    if-nez v3, :cond_0

    .line 1053
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Folder created successfully "

    invoke-static {v3, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1059
    :goto_0
    const/4 v3, 0x1

    :goto_1
    return v3

    .line 1047
    :catch_0
    move-exception v1

    .line 1048
    .local v1, "e":Lcom/sec/android/sCloudSync/Util/SyncException;
    invoke-virtual {v1}, Lcom/sec/android/sCloudSync/Util/SyncException;->printStackTrace()V

    .line 1049
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Create Folder Exception received :- "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/sec/android/sCloudSync/Util/SyncException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1050
    const/4 v3, 0x0

    goto :goto_1

    .line 1054
    .end local v1    # "e":Lcom/sec/android/sCloudSync/Util/SyncException;
    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getResponseCode()I

    move-result v3

    const/16 v4, 0x7918

    if-ne v3, v4, :cond_1

    .line 1055
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Folder already exists "

    invoke-static {v3, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1057
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Folder creation failed "

    invoke-static {v3, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private deleteFiles(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1027
    .local p1, "deleteFileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1028
    .local v3, "path":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1029
    .local v0, "filetoDelete":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1030
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v2

    .line 1031
    .local v2, "isDeleteSuccess":Z
    if-nez v2, :cond_0

    .line 1032
    const-string v4, "SNote3SyncAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "File "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " could not be deleted"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1036
    .end local v0    # "filetoDelete":Ljava/io/File;
    .end local v2    # "isDeleteSuccess":Z
    .end local v3    # "path":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private filterSingleQuote(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "localPath"    # Ljava/lang/String;

    .prologue
    .line 583
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 585
    .local v1, "result":Ljava/lang/StringBuilder;
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x1

    if-ge v2, v3, :cond_1

    .line 586
    :cond_0
    const/4 v2, 0x0

    .line 595
    :goto_0
    return-object v2

    .line 588
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 589
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x27

    if-ne v2, v3, :cond_2

    .line 590
    const-string v2, "\'\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 588
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 592
    :cond_2
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 595
    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method private getAppSupportUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1063
    sget-object v0, Lcom/sec/android/sCloudSync/Constants/SNote3Contract;->APP_INFO_URI:Landroid/net/Uri;

    return-object v0
.end method

.method private handleSetList(Ljava/util/List;)Z
    .locals 33
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/sCloudSync/Records/RecordSetItem;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;,
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 478
    .local p1, "setList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordSetItem;>;"
    const/16 v24, 0x0

    .line 479
    .local v24, "hasUploadFailed":Z
    sget-object v2, Lcom/sec/android/sCloudSync/Constants/SNote3Contract;->FILEDETAIL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getCallerSyncAdapter()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v17

    .line 482
    .local v17, "fileDetailUri":Landroid/net/Uri;
    if-eqz p1, :cond_f

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_f

    .line 484
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    check-cast v2, Lcom/sec/android/sCloudSync/Builders/SNote3/SNote3Builder;

    invoke-virtual {v2}, Lcom/sec/android/sCloudSync/Builders/SNote3/SNote3Builder;->getFileUploadList()Ljava/util/Map;

    move-result-object v18

    .line 485
    .local v18, "fileUploadMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    check-cast v2, Lcom/sec/android/sCloudSync/Builders/SNote3/SNote3Builder;

    invoke-virtual {v2}, Lcom/sec/android/sCloudSync/Builders/SNote3/SNote3Builder;->getSNBFilePath()Ljava/lang/String;

    move-result-object v30

    .line 486
    .local v30, "snbFilePath":Ljava/lang/String;
    const/16 v19, 0x0

    .line 489
    .local v19, "fileUploadResponse":Lcom/sec/android/sCloudSync/Records/ORSResponse;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 491
    .local v3, "recordORSItemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordORSItem;>;"
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v25

    :goto_0
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_f

    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lcom/sec/android/sCloudSync/Records/RecordSetItem;

    .line 492
    .local v29, "setItem":Lcom/sec/android/sCloudSync/Records/RecordSetItem;
    invoke-virtual/range {v29 .. v29}, Lcom/sec/android/sCloudSync/Records/RecordSetItem;->getKEY()Ljava/lang/String;

    move-result-object v27

    .line 493
    .local v27, "key":Ljava/lang/String;
    if-eqz v18, :cond_2

    invoke-interface/range {v18 .. v18}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 495
    invoke-interface/range {v18 .. v18}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v26

    .local v26, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/util/Map$Entry;

    .line 496
    .local v15, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v15}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 497
    .local v7, "localFilepath":Ljava/lang/String;
    invoke-interface {v15}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    .line 498
    .local v12, "checkSum":Ljava/lang/String;
    const-string v2, "/"

    invoke-virtual {v7, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v31

    .line 499
    .local v31, "split":[Ljava/lang/String;
    move-object/from16 v0, v31

    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    aget-object v4, v31, v2

    .line 501
    .local v4, "fileName":Ljava/lang/String;
    new-instance v2, Lcom/sec/android/sCloudSync/Records/RecordORSItem;

    const-string v5, "checksum"

    invoke-direct {v2, v5, v12}, Lcom/sec/android/sCloudSync/Records/RecordORSItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 503
    const/16 v16, 0x0

    .line 504
    .local v16, "fileDesc":Landroid/os/ParcelFileDescriptor;
    const/16 v22, 0x0

    .line 505
    .local v22, "fin":Ljava/io/FileInputStream;
    const/4 v10, 0x0

    .line 508
    .local v10, "buffer":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->mbSyncCanceled:Z

    if-eqz v2, :cond_3

    .line 509
    new-instance v2, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/16 v5, 0x9

    invoke-direct {v2, v5}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 524
    :catch_0
    move-exception v14

    .line 525
    .local v14, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_1
    invoke-virtual {v14}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 526
    const/16 v24, 0x1

    .line 530
    if-eqz v16, :cond_0

    .line 531
    :try_start_2
    invoke-virtual/range {v16 .. v16}, Landroid/os/ParcelFileDescriptor;->close()V

    .line 532
    :cond_0
    if-eqz v22, :cond_1

    .line 533
    invoke-virtual/range {v22 .. v22}, Ljava/io/FileInputStream;->close()V

    .line 534
    :cond_1
    if-eqz v10, :cond_2

    .line 535
    invoke-virtual {v10}, Ljava/io/ByteArrayOutputStream;->flush()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4

    .line 566
    .end local v4    # "fileName":Ljava/lang/String;
    .end local v7    # "localFilepath":Ljava/lang/String;
    .end local v10    # "buffer":Ljava/io/ByteArrayOutputStream;
    .end local v12    # "checkSum":Ljava/lang/String;
    .end local v14    # "e":Ljava/lang/Exception;
    .end local v15    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v16    # "fileDesc":Landroid/os/ParcelFileDescriptor;
    .end local v22    # "fin":Ljava/io/FileInputStream;
    .end local v26    # "i$":Ljava/util/Iterator;
    .end local v31    # "split":[Ljava/lang/String;
    :cond_2
    :goto_3
    sget-object v2, Lcom/sec/android/sCloudSync/Constants/SNote3Contract;->DELETESNB_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v5, "filename"

    move-object/from16 v0, v30

    invoke-virtual {v2, v5, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v5, "privatekey"

    move-object/from16 v0, v27

    invoke-virtual {v2, v5, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v13

    .line 571
    .local v13, "deleteSnbUri":Landroid/net/Uri;
    :try_start_3
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v2

    const/4 v5, 0x0

    invoke-virtual {v2, v13, v5}, Landroid/content/ContentProviderClient;->openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_0

    .line 572
    :catch_1
    move-exception v14

    .line 573
    .local v14, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v14}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto/16 :goto_0

    .line 511
    .end local v13    # "deleteSnbUri":Landroid/net/Uri;
    .end local v14    # "e":Ljava/io/FileNotFoundException;
    .restart local v4    # "fileName":Ljava/lang/String;
    .restart local v7    # "localFilepath":Ljava/lang/String;
    .restart local v10    # "buffer":Ljava/io/ByteArrayOutputStream;
    .restart local v12    # "checkSum":Ljava/lang/String;
    .restart local v15    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v16    # "fileDesc":Landroid/os/ParcelFileDescriptor;
    .restart local v22    # "fin":Ljava/io/FileInputStream;
    .restart local v26    # "i$":Ljava/util/Iterator;
    .restart local v31    # "split":[Ljava/lang/String;
    :cond_3
    :try_start_4
    sget-object v2, Lcom/sec/android/sCloudSync/Constants/SNote3Contract;->FILEDETAIL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v5, "filename"

    invoke-virtual {v2, v5, v7}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v5, "privatekey"

    move-object/from16 v0, v27

    invoke-virtual {v2, v5, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v20

    .line 515
    .local v20, "filedetailUri":Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v2

    const/4 v5, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v2, v0, v5}, Landroid/content/ContentProviderClient;->openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v16

    .line 516
    new-instance v23, Ljava/io/FileInputStream;

    invoke-virtual/range {v16 .. v16}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v2

    move-object/from16 v0, v23

    invoke-direct {v0, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 517
    .end local v22    # "fin":Ljava/io/FileInputStream;
    .local v23, "fin":Ljava/io/FileInputStream;
    :try_start_5
    new-instance v11, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v11}, Ljava/io/ByteArrayOutputStream;-><init>()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_6
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 519
    .end local v10    # "buffer":Ljava/io/ByteArrayOutputStream;
    .local v11, "buffer":Ljava/io/ByteArrayOutputStream;
    :goto_4
    :try_start_6
    sget-object v2, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->mData:[B

    const/4 v5, 0x0

    sget-object v6, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->mData:[B

    array-length v6, v6

    move-object/from16 v0, v23

    invoke-virtual {v0, v2, v5, v6}, Ljava/io/FileInputStream;->read([BII)I

    move-result v28

    .local v28, "nRead":I
    const/4 v2, -0x1

    move/from16 v0, v28

    if-eq v0, v2, :cond_4

    .line 520
    sget-object v2, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->mData:[B

    const/4 v5, 0x0

    move/from16 v0, v28

    invoke-virtual {v11, v2, v5, v0}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_4

    .line 524
    .end local v28    # "nRead":I
    :catch_2
    move-exception v14

    move-object v10, v11

    .end local v11    # "buffer":Ljava/io/ByteArrayOutputStream;
    .restart local v10    # "buffer":Ljava/io/ByteArrayOutputStream;
    move-object/from16 v22, v23

    .end local v23    # "fin":Ljava/io/FileInputStream;
    .restart local v22    # "fin":Ljava/io/FileInputStream;
    goto/16 :goto_2

    .line 522
    .end local v10    # "buffer":Ljava/io/ByteArrayOutputStream;
    .end local v22    # "fin":Ljava/io/FileInputStream;
    .restart local v11    # "buffer":Ljava/io/ByteArrayOutputStream;
    .restart local v23    # "fin":Ljava/io/FileInputStream;
    .restart local v28    # "nRead":I
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getCloudServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getFileServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/android/scloud/framework/util/TimeManager;->getCurrentTime(Landroid/content/Context;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "/SNOTE3/"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v27

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "/"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v8, 0x0

    invoke-virtual {v11}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v9

    invoke-virtual/range {v2 .. v9}, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->uploadFile(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z[B)Lcom/sec/android/sCloudSync/Records/ORSResponse;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    move-result-object v19

    .line 530
    if-eqz v16, :cond_5

    .line 531
    :try_start_7
    invoke-virtual/range {v16 .. v16}, Landroid/os/ParcelFileDescriptor;->close()V

    .line 532
    :cond_5
    if-eqz v23, :cond_6

    .line 533
    invoke-virtual/range {v23 .. v23}, Ljava/io/FileInputStream;->close()V

    .line 534
    :cond_6
    if-eqz v11, :cond_7

    .line 535
    invoke-virtual {v11}, Ljava/io/ByteArrayOutputStream;->flush()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    .line 542
    :cond_7
    :goto_5
    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getResponseCode()I

    move-result v2

    if-nez v2, :cond_b

    .line 544
    new-instance v32, Landroid/content/ContentValues;

    invoke-direct/range {v32 .. v32}, Landroid/content/ContentValues;-><init>()V

    .line 545
    .local v32, "values":Landroid/content/ContentValues;
    const-string v2, "dirty"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v32

    invoke-virtual {v0, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 547
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->filterSingleQuote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 548
    .local v21, "filtered":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "path = \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v32

    invoke-virtual {v2, v0, v1, v5, v6}, Landroid/content/ContentProviderClient;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 561
    .end local v21    # "filtered":Ljava/lang/String;
    .end local v32    # "values":Landroid/content/ContentValues;
    :goto_6
    invoke-interface {v3}, Ljava/util/List;->clear()V

    goto/16 :goto_1

    .line 536
    :catch_3
    move-exception v14

    .line 537
    .local v14, "e":Ljava/io/IOException;
    invoke-virtual {v14}, Ljava/io/IOException;->printStackTrace()V

    .line 538
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string v5, "IOException when closing/flushing data"

    invoke-static {v2, v5}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 536
    .end local v11    # "buffer":Ljava/io/ByteArrayOutputStream;
    .end local v20    # "filedetailUri":Landroid/net/Uri;
    .end local v23    # "fin":Ljava/io/FileInputStream;
    .end local v28    # "nRead":I
    .restart local v10    # "buffer":Ljava/io/ByteArrayOutputStream;
    .local v14, "e":Ljava/lang/Exception;
    .restart local v22    # "fin":Ljava/io/FileInputStream;
    :catch_4
    move-exception v14

    .line 537
    .local v14, "e":Ljava/io/IOException;
    invoke-virtual {v14}, Ljava/io/IOException;->printStackTrace()V

    .line 538
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string v5, "IOException when closing/flushing data"

    invoke-static {v2, v5}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 529
    .end local v14    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    .line 530
    :goto_7
    if-eqz v16, :cond_8

    .line 531
    :try_start_8
    invoke-virtual/range {v16 .. v16}, Landroid/os/ParcelFileDescriptor;->close()V

    .line 532
    :cond_8
    if-eqz v22, :cond_9

    .line 533
    invoke-virtual/range {v22 .. v22}, Ljava/io/FileInputStream;->close()V

    .line 534
    :cond_9
    if-eqz v10, :cond_a

    .line 535
    invoke-virtual {v10}, Ljava/io/ByteArrayOutputStream;->flush()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 539
    :cond_a
    :goto_8
    throw v2

    .line 536
    :catch_5
    move-exception v14

    .line 537
    .restart local v14    # "e":Ljava/io/IOException;
    invoke-virtual {v14}, Ljava/io/IOException;->printStackTrace()V

    .line 538
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v5

    const-string v6, "IOException when closing/flushing data"

    invoke-static {v5, v6}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_8

    .line 550
    .end local v10    # "buffer":Ljava/io/ByteArrayOutputStream;
    .end local v14    # "e":Ljava/io/IOException;
    .end local v22    # "fin":Ljava/io/FileInputStream;
    .restart local v11    # "buffer":Ljava/io/ByteArrayOutputStream;
    .restart local v20    # "filedetailUri":Landroid/net/Uri;
    .restart local v23    # "fin":Ljava/io/FileInputStream;
    .restart local v28    # "nRead":I
    :cond_b
    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getResponseCode()I

    move-result v2

    const/16 v5, 0x7d00

    if-ne v2, v5, :cond_c

    .line 551
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->createSNoteFolderStructure()Z

    .line 552
    const/16 v24, 0x1

    goto :goto_6

    .line 553
    :cond_c
    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getResponseCode()I

    move-result v2

    const/16 v5, 0x4e23

    if-eq v2, v5, :cond_d

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getResponseCode()I

    move-result v2

    const/16 v5, 0x4e24

    if-ne v2, v5, :cond_e

    .line 555
    :cond_d
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->notifyServerStoragefull()V

    .line 556
    const/16 v24, 0x1

    goto :goto_6

    .line 558
    :cond_e
    const/16 v24, 0x1

    .line 559
    goto/16 :goto_3

    .line 578
    .end local v3    # "recordORSItemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordORSItem;>;"
    .end local v4    # "fileName":Ljava/lang/String;
    .end local v7    # "localFilepath":Ljava/lang/String;
    .end local v11    # "buffer":Ljava/io/ByteArrayOutputStream;
    .end local v12    # "checkSum":Ljava/lang/String;
    .end local v15    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v16    # "fileDesc":Landroid/os/ParcelFileDescriptor;
    .end local v18    # "fileUploadMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v19    # "fileUploadResponse":Lcom/sec/android/sCloudSync/Records/ORSResponse;
    .end local v20    # "filedetailUri":Landroid/net/Uri;
    .end local v23    # "fin":Ljava/io/FileInputStream;
    .end local v26    # "i$":Ljava/util/Iterator;
    .end local v27    # "key":Ljava/lang/String;
    .end local v28    # "nRead":I
    .end local v29    # "setItem":Lcom/sec/android/sCloudSync/Records/RecordSetItem;
    .end local v30    # "snbFilePath":Ljava/lang/String;
    .end local v31    # "split":[Ljava/lang/String;
    :cond_f
    return v24

    .line 529
    .restart local v3    # "recordORSItemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordORSItem;>;"
    .restart local v4    # "fileName":Ljava/lang/String;
    .restart local v7    # "localFilepath":Ljava/lang/String;
    .restart local v10    # "buffer":Ljava/io/ByteArrayOutputStream;
    .restart local v12    # "checkSum":Ljava/lang/String;
    .restart local v15    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v16    # "fileDesc":Landroid/os/ParcelFileDescriptor;
    .restart local v18    # "fileUploadMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v19    # "fileUploadResponse":Lcom/sec/android/sCloudSync/Records/ORSResponse;
    .restart local v20    # "filedetailUri":Landroid/net/Uri;
    .restart local v23    # "fin":Ljava/io/FileInputStream;
    .restart local v26    # "i$":Ljava/util/Iterator;
    .restart local v27    # "key":Ljava/lang/String;
    .restart local v29    # "setItem":Lcom/sec/android/sCloudSync/Records/RecordSetItem;
    .restart local v30    # "snbFilePath":Ljava/lang/String;
    .restart local v31    # "split":[Ljava/lang/String;
    :catchall_1
    move-exception v2

    move-object/from16 v22, v23

    .end local v23    # "fin":Ljava/io/FileInputStream;
    .restart local v22    # "fin":Ljava/io/FileInputStream;
    goto :goto_7

    .end local v10    # "buffer":Ljava/io/ByteArrayOutputStream;
    .end local v22    # "fin":Ljava/io/FileInputStream;
    .restart local v11    # "buffer":Ljava/io/ByteArrayOutputStream;
    .restart local v23    # "fin":Ljava/io/FileInputStream;
    :catchall_2
    move-exception v2

    move-object v10, v11

    .end local v11    # "buffer":Ljava/io/ByteArrayOutputStream;
    .restart local v10    # "buffer":Ljava/io/ByteArrayOutputStream;
    move-object/from16 v22, v23

    .end local v23    # "fin":Ljava/io/FileInputStream;
    .restart local v22    # "fin":Ljava/io/FileInputStream;
    goto :goto_7

    .line 524
    .end local v22    # "fin":Ljava/io/FileInputStream;
    .restart local v23    # "fin":Ljava/io/FileInputStream;
    :catch_6
    move-exception v14

    move-object/from16 v22, v23

    .end local v23    # "fin":Ljava/io/FileInputStream;
    .restart local v22    # "fin":Ljava/io/FileInputStream;
    goto/16 :goto_2
.end method


# virtual methods
.method public cancelSync()V
    .locals 1

    .prologue
    .line 601
    invoke-super {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->cancelSync()V

    .line 602
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getCloudServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getFileServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->close()V

    .line 603
    return-void
.end method

.method protected checkUploadLimit(JJJ)Z
    .locals 1
    .param p1, "uploadsize"    # J
    .param p3, "totalSize"    # J
    .param p5, "currentCount"    # J

    .prologue
    .line 232
    cmp-long v0, p5, p3

    if-gez v0, :cond_0

    .line 233
    const/4 v0, 0x1

    .line 234
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public doUpdateDelete(Lcom/sec/android/sCloudSync/Records/KVSItem;Lcom/sec/android/sCloudSync/Records/RecordItem;Landroid/content/SyncStats;)Z
    .locals 10
    .param p1, "item"    # Lcom/sec/android/sCloudSync/Records/KVSItem;
    .param p2, "record"    # Lcom/sec/android/sCloudSync/Records/RecordItem;
    .param p3, "stats"    # Landroid/content/SyncStats;

    .prologue
    const-wide/16 v8, 0x1

    .line 357
    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->isDeleted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 358
    iget-wide v0, p3, Landroid/content/SyncStats;->numDeletes:J

    add-long/2addr v0, v8

    iput-wide v0, p3, Landroid/content/SyncStats;->numDeletes:J

    .line 360
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getID()J

    move-result-wide v2

    const/4 v4, 0x0

    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getKEY()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;->delete(Landroid/net/Uri;JLjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 373
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 361
    :catch_0
    move-exception v7

    .line 363
    .local v7, "e":Landroid/os/RemoteException;
    invoke-virtual {v7}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 366
    .end local v7    # "e":Landroid/os/RemoteException;
    :cond_0
    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getValue()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getTimeStamp()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getID()J

    move-result-wide v4

    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getKEY()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;->update(Ljava/lang/String;JJLjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 367
    :cond_1
    const-string v0, "SNote3SyncAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to update record with key:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/RecordItem;->getKEY()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 370
    :cond_2
    iget-wide v0, p3, Landroid/content/SyncStats;->numUpdates:J

    add-long/2addr v0, v8

    iput-wide v0, p3, Landroid/content/SyncStats;->numUpdates:J

    goto :goto_0
.end method

.method protected getAccountName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 161
    const-string v0, "account_name"

    return-object v0
.end method

.method protected getAccountType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 166
    const-string v0, "account_type"

    return-object v0
.end method

.method protected getBuilder()Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;
    .locals 4

    .prologue
    .line 176
    new-instance v0, Lcom/sec/android/sCloudSync/Builders/SNote3/SNote3Builder;

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/sCloudSync/Builders/SNote3/SNote3Builder;-><init>(Landroid/content/ContentProviderClient;Landroid/accounts/Account;Landroid/content/Context;)V

    return-object v0
.end method

.method protected getCallerSyncAdapter()Ljava/lang/String;
    .locals 1

    .prologue
    .line 156
    const-string v0, "caller_is_syncadapter"

    return-object v0
.end method

.method protected getCid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 171
    const-string v0, "PM3HWwUYhP"

    return-object v0
.end method

.method protected getContentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 118
    sget-object v0, Lcom/sec/android/sCloudSync/Constants/SNote3Contract;->FILE_CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method protected getCtidKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1067
    const-string v0, "N3"

    return-object v0
.end method

.method protected getDeletedColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 131
    const-string v0, "deleted"

    return-object v0
.end method

.method protected getDirtyColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 151
    const-string v0, "dirty"

    return-object v0
.end method

.method protected getIdColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 146
    const-string v0, "_id"

    return-object v0
.end method

.method protected getKeyColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 136
    const-string v0, "sync1"

    return-object v0
.end method

.method protected getLocalUpdatesSelection()Ljava/lang/String;
    .locals 2

    .prologue
    .line 124
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getAccountName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v1

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getAccountType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v1

    iget-object v1, v1, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getDirtyColumnName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = 1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getProjection()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 181
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getIdColumnName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getKeyColumnName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getTimeStampColumnName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getDeletedColumnName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getDirtyColumnName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    return-object v0
.end method

.method protected getServerUpdates()Ljava/util/Map;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/sCloudSync/Records/KVSItem;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .line 240
    invoke-super {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->getServerUpdates()Ljava/util/Map;

    move-result-object v6

    .line 241
    .local v6, "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/sec/android/sCloudSync/Records/KVSItem;>;"
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 243
    .local v5, "itemsToBeRemoved":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/sec/android/sCloudSync/Records/KVSItem;>;"
    if-eqz v6, :cond_0

    invoke-interface {v6}, Ljava/util/Map;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 270
    :cond_0
    return-object v6

    .line 244
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v7

    invoke-direct {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getAppSupportUri()Landroid/net/Uri;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/sCloudSync/Tools/AppTool;->getSupportedApplication(Landroid/content/ContentProviderClient;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 245
    .local v0, "adapterName":Ljava/lang/String;
    if-eqz v0, :cond_3

    .line 246
    const-string v7, "TMEMO2"

    invoke-virtual {v0, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 254
    invoke-interface {v6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 256
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/sec/android/sCloudSync/Records/KVSItem;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    const-string v8, "_TMEMO2"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 257
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    invoke-interface {v5, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 263
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/sec/android/sCloudSync/Records/KVSItem;>;"
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v5}, Ljava/util/Map;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_0

    .line 265
    invoke-interface {v5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    .line 266
    .local v4, "items":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .restart local v2    # "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 267
    .local v3, "item":Ljava/lang/String;
    invoke-interface {v6, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method protected getSyncAdapterName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    const-string v0, "S-NOTE3"

    return-object v0
.end method

.method protected getSyncStateDataColumn()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    const-string v0, "data"

    return-object v0
.end method

.method protected getSyncStateURI()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 104
    sget-object v0, Lcom/sec/android/sCloudSync/Constants/SNote3Contract;->SNOTE_SYNC_STATE_URI:Landroid/net/Uri;

    return-object v0
.end method

.method protected getTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    const-string v0, "SNote3SyncAdapter"

    return-object v0
.end method

.method protected getTimeStampColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 141
    const-string v0, "sync2"

    return-object v0
.end method

.method protected handleDeleteList(Ljava/util/List;Ljava/util/List;)Z
    .locals 25
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/sCloudSync/Records/RecordBase;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/sCloudSync/Records/RecordSetItem;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;,
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 392
    .local p1, "deleteList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordBase;>;"
    .local p2, "setList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordSetItem;>;"
    const/4 v13, 0x0

    .line 393
    .local v13, "fileDeleteResponse":Lcom/sec/android/sCloudSync/Records/ORSResponse;
    sget-object v1, Lcom/sec/android/sCloudSync/Constants/SNote3Contract;->FILEDETAIL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getCallerSyncAdapter()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    .line 395
    .local v2, "fileDetailUri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    check-cast v1, Lcom/sec/android/sCloudSync/Builders/SNote3/SNote3Builder;

    invoke-virtual {v1}, Lcom/sec/android/sCloudSync/Builders/SNote3/SNote3Builder;->getServerFileDeleteList()Ljava/util/List;

    move-result-object v12

    .line 396
    .local v12, "fileDeleteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/16 v16, 0x0

    .line 400
    .local v16, "hasDeleteFailed":Z
    if-eqz p1, :cond_6

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    .line 402
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    .line 403
    .local v24, "where":Ljava/lang/StringBuilder;
    const/4 v1, 0x1

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v4, "path"

    aput-object v4, v3, v1

    .line 404
    .local v3, "projection":[Ljava/lang/String;
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v17

    .local v17, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/sCloudSync/Records/RecordBase;

    .line 405
    .local v9, "deleteItem":Lcom/sec/android/sCloudSync/Records/RecordBase;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->mLocalChangedRecords:Ljava/util/Map;

    invoke-virtual {v9}, Lcom/sec/android/sCloudSync/Records/RecordBase;->getKEY()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Long;

    .line 406
    .local v19, "id":Ljava/lang/Long;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "spd_id = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 408
    .local v7, "cursor":Landroid/database/Cursor;
    if-eqz v7, :cond_4

    .line 410
    const/16 v21, 0x0

    .line 411
    .local v21, "lengthOffset":I
    const/4 v1, 0x0

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 412
    const-string v1, "path"

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " IN ("

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 415
    :goto_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 416
    const-string v1, "path"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 417
    .local v10, "deletePath":Ljava/lang/String;
    const-string v1, "/"

    invoke-virtual {v10, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v23

    .line 418
    .local v23, "split":[Ljava/lang/String;
    move-object/from16 v0, v23

    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-object v14, v23, v1

    .line 420
    .local v14, "fileName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->mbSyncCanceled:Z

    if-eqz v1, :cond_0

    .line 421
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 422
    new-instance v1, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/16 v4, 0x9

    invoke-direct {v1, v4}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v1

    .line 426
    :cond_0
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getCloudServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getFileServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "/SNOTE3/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v9}, Lcom/sec/android/sCloudSync/Records/RecordBase;->getKEY()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->deleteFile(Ljava/lang/String;)Lcom/sec/android/sCloudSync/Records/ORSResponse;
    :try_end_0
    .catch Lcom/sec/android/sCloudSync/Util/SyncException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v13

    .line 431
    invoke-virtual {v13}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getResponseCode()I

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v13}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getResponseCode()I

    move-result v1

    const/16 v4, 0x7d02

    if-eq v1, v4, :cond_1

    invoke-virtual {v13}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getResponseCode()I

    move-result v1

    const/16 v4, 0x7d06

    if-ne v1, v4, :cond_2

    .line 433
    :cond_1
    const/16 v1, 0x27

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v4, 0x27

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v4, 0x2c

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 434
    const/16 v21, 0x1

    goto/16 :goto_1

    .line 427
    :catch_0
    move-exception v11

    .line 428
    .local v11, "e":Lcom/sec/android/sCloudSync/Util/SyncException;
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 429
    throw v11

    .line 438
    .end local v11    # "e":Lcom/sec/android/sCloudSync/Util/SyncException;
    :cond_2
    const/16 v16, 0x1

    .line 442
    .end local v10    # "deletePath":Ljava/lang/String;
    .end local v14    # "fileName":Ljava/lang/String;
    .end local v23    # "split":[Ljava/lang/String;
    :cond_3
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 443
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    sub-int v1, v1, v21

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 444
    const/16 v1, 0x29

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 447
    .end local v21    # "lengthOffset":I
    :cond_4
    if-eqz v16, :cond_5

    .line 448
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v1

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/ContentProviderClient;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_0

    .line 450
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "spd_id = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/ContentProviderClient;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_0

    .line 453
    .end local v3    # "projection":[Ljava/lang/String;
    .end local v7    # "cursor":Landroid/database/Cursor;
    .end local v9    # "deleteItem":Lcom/sec/android/sCloudSync/Records/RecordBase;
    .end local v17    # "i$":Ljava/util/Iterator;
    .end local v19    # "id":Ljava/lang/Long;
    .end local v24    # "where":Ljava/lang/StringBuilder;
    :cond_6
    if-eqz p2, :cond_b

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_b

    if-eqz v12, :cond_b

    .line 454
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :cond_7
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/sec/android/sCloudSync/Records/RecordSetItem;

    .line 455
    .local v22, "setItem":Lcom/sec/android/sCloudSync/Records/RecordSetItem;
    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/sCloudSync/Records/RecordSetItem;->getKEY()Ljava/lang/String;

    move-result-object v20

    .line 456
    .local v20, "key":Ljava/lang/String;
    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    .local v18, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 457
    .local v8, "deleteFile":Ljava/lang/String;
    const-string v1, "/"

    invoke-virtual {v8, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v23

    .line 458
    .restart local v23    # "split":[Ljava/lang/String;
    move-object/from16 v0, v23

    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-object v14, v23, v1

    .line 459
    .restart local v14    # "fileName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->mbSyncCanceled:Z

    if-eqz v1, :cond_8

    .line 460
    new-instance v1, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/16 v4, 0x9

    invoke-direct {v1, v4}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v1

    .line 462
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getCloudServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getFileServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "/SNOTE3/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->deleteFile(Ljava/lang/String;)Lcom/sec/android/sCloudSync/Records/ORSResponse;

    move-result-object v13

    .line 463
    invoke-virtual {v13}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getResponseCode()I

    move-result v1

    if-eqz v1, :cond_9

    invoke-virtual {v13}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getResponseCode()I

    move-result v1

    const/16 v4, 0x7d02

    if-eq v1, v4, :cond_9

    invoke-virtual {v13}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getResponseCode()I

    move-result v1

    const/16 v4, 0x7d06

    if-ne v1, v4, :cond_a

    .line 465
    :cond_9
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->filterSingleQuote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 466
    .local v15, "filtered":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "path = \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/ContentProviderClient;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_2

    .line 468
    .end local v15    # "filtered":Ljava/lang/String;
    :cond_a
    const/16 v16, 0x1

    goto/16 :goto_2

    .line 473
    .end local v8    # "deleteFile":Ljava/lang/String;
    .end local v14    # "fileName":Ljava/lang/String;
    .end local v18    # "i$":Ljava/util/Iterator;
    .end local v20    # "key":Ljava/lang/String;
    .end local v22    # "setItem":Lcom/sec/android/sCloudSync/Records/RecordSetItem;
    .end local v23    # "split":[Ljava/lang/String;
    :cond_b
    return v16
.end method

.method protected prepareSync()Z
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .line 189
    const/4 v8, 0x0

    .line 190
    .local v8, "result":Z
    sget-object v0, Lcom/sec/android/sCloudSync/Constants/SNote3Contract;->SYNC_SETTING_URI:Landroid/net/Uri;

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getCallerSyncAdapter()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 192
    .local v1, "syncSettingUri":Landroid/net/Uri;
    const/4 v6, 0x0

    .line 194
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 195
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 196
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 197
    .local v9, "setting":Ljava/lang/String;
    const-string v0, "SNote3SyncAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SNote3 SyncSetting : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    invoke-static {v9}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v8

    .line 199
    if-nez v8, :cond_2

    .line 200
    new-instance v0, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/16 v2, 0xf

    invoke-direct {v0, v2}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 202
    .end local v9    # "setting":Ljava/lang/String;
    :catch_0
    move-exception v7

    .line 204
    .local v7, "e":Landroid/os/RemoteException;
    :try_start_1
    invoke-virtual {v7}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 205
    const/4 v8, 0x0

    .line 207
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 210
    .end local v7    # "e":Landroid/os/RemoteException;
    :cond_0
    :goto_0
    if-eqz v8, :cond_1

    .line 211
    invoke-direct {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->clearGarbage()Z

    .line 212
    :cond_1
    return v8

    .line 207
    :cond_2
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method protected removePreSyncedRecords(Ljava/util/Map;)Z
    .locals 26
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/sCloudSync/Records/KVSItem;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 276
    .local p1, "uServerRecords":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/sec/android/sCloudSync/Records/KVSItem;>;"
    if-eqz p1, :cond_0

    invoke-interface/range {p1 .. p1}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 277
    :cond_0
    const/4 v2, 0x0

    .line 352
    :goto_0
    return v2

    .line 279
    :cond_1
    const/16 v22, 0x0

    .line 280
    .local v22, "preCursor":Landroid/database/Cursor;
    invoke-interface/range {p1 .. p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v19

    .line 281
    .local v19, "keyset":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-interface/range {v19 .. v19}, Ljava/util/Set;->size()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    mul-int/lit16 v2, v2, 0x104

    move-object/from16 v0, v25

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 282
    .local v25, "where":Ljava/lang/StringBuilder;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getKeyColumnName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " IN ("

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 283
    move-object/from16 v0, v25

    move-object/from16 v1, v19

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Tools/AppTool;->appendIds(Ljava/lang/StringBuilder;Ljava/util/Set;)V

    .line 284
    const/16 v2, 0x29

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 286
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getContentUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getCallerSyncAdapter()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getAccountName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v5

    iget-object v5, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getAccountType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v5

    iget-object v5, v5, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 291
    .local v3, "uriFromSyncAdapter":Landroid/net/Uri;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getProjection()[Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v22

    .line 297
    if-nez v22, :cond_2

    .line 298
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string v4, "RemovePreSyncedRecords: (ERROR)Cursor is null"

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 292
    :catch_0
    move-exception v13

    .line 293
    .local v13, "e":Landroid/os/RemoteException;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "RemovePreSyncedRecords: Exception in calling query "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v13}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 302
    .end local v13    # "e":Landroid/os/RemoteException;
    :cond_2
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-nez v2, :cond_3

    .line 303
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    .line 304
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string v4, "RemovePreSyncedRecords: there is nothing to be removed."

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    :goto_1
    invoke-super/range {p0 .. p1}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->removePreSyncedRecords(Ljava/util/Map;)Z

    .line 352
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 307
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getIdColumnName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    .line 308
    .local v16, "id_index":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getKeyColumnName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    .line 309
    .local v18, "key_index":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getTimeStampColumnName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v24

    .line 310
    .local v24, "time_index":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getDirtyColumnName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    .line 311
    .local v12, "dirtyIndex":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getDeletedColumnName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 314
    .local v10, "deletedIndex":I
    :cond_4
    move-object/from16 v0, v22

    move/from16 v1, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    .line 315
    .local v15, "id":I
    move-object/from16 v0, v22

    move/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    .line 316
    .local v20, "modifiedTimeUploadedtoServer":J
    move-object/from16 v0, v22

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 317
    .local v17, "key":Ljava/lang/String;
    move-object/from16 v0, v22

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 318
    .local v11, "dirtyField":I
    move-object/from16 v0, v22

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 320
    .local v9, "deletedField":I
    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Lcom/sec/android/sCloudSync/Records/KVSItem;

    .line 321
    .local v23, "serverItem":Lcom/sec/android/sCloudSync/Records/KVSItem;
    if-eqz v23, :cond_7

    .line 322
    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getTimeStamp()J

    move-result-wide v4

    cmp-long v2, v20, v4

    if-ltz v2, :cond_6

    .line 323
    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/sCloudSync/Records/KVSItem;->isDeleted()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 326
    sget-object v2, Lcom/sec/android/sCloudSync/Constants/SNote3Contract;->FILEDETAIL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getCallerSyncAdapter()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v14

    .line 328
    .local v14, "fileDetailUri":Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v4, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->DIRTY_VALUES:Landroid/content/ContentValues;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "spd_id = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v2, v14, v4, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v8

    .line 330
    .local v8, "count":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "UPDATE Detail DIRTY, id : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    .end local v8    # "count":I
    .end local v14    # "fileDetailUri":Landroid/net/Uri;
    :cond_5
    if-nez v11, :cond_6

    const/4 v2, 0x1

    if-ne v9, v2, :cond_6

    .line 334
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getKeyColumnName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentProviderClient;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 335
    sget-object v2, Lcom/sec/android/sCloudSync/Constants/SNote3Contract;->FILEDETAIL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getCallerSyncAdapter()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v14

    .line 337
    .restart local v14    # "fileDetailUri":Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "spd_id=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2, v14, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 347
    .end local v14    # "fileDetailUri":Landroid/net/Uri;
    :cond_6
    :goto_2
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_4

    .line 349
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    .line 338
    :catch_1
    move-exception v13

    .line 339
    .restart local v13    # "e":Landroid/os/RemoteException;
    invoke-virtual {v13}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_2

    .line 345
    .end local v13    # "e":Landroid/os/RemoteException;
    :cond_7
    const-string v2, "SNote3SyncAdapter"

    const-string v4, "Sync Broken : There is a duplicated record on Local."

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public updateLocalDb(Landroid/content/SyncStats;)V
    .locals 66
    .param p1, "stats"    # Landroid/content/SyncStats;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .line 609
    invoke-super/range {p0 .. p1}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->updateLocalDb(Landroid/content/SyncStats;)V

    .line 610
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v2

    const-string v4, "mounted"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 611
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->resetLastSyncTime()V

    .line 612
    new-instance v2, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/16 v4, 0x8

    invoke-direct {v2, v4}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v2

    .line 615
    :cond_0
    const/16 v26, 0x0

    .line 616
    .local v26, "fileDownResponse":Lcom/sec/android/sCloudSync/Records/ORSResponse;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    check-cast v2, Lcom/sec/android/sCloudSync/Builders/SNote3/SNote3Builder;

    invoke-virtual {v2}, Lcom/sec/android/sCloudSync/Builders/SNote3/SNote3Builder;->getFileDownloadList()Ljava/util/Map;

    move-result-object v27

    .line 617
    .local v27, "fileDownloadList":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    check-cast v2, Lcom/sec/android/sCloudSync/Builders/SNote3/SNote3Builder;

    invoke-virtual {v2}, Lcom/sec/android/sCloudSync/Builders/SNote3/SNote3Builder;->getSNBFilePathDownloadList()Ljava/util/Map;

    move-result-object v58

    .line 618
    .local v58, "snbFileMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    check-cast v2, Lcom/sec/android/sCloudSync/Builders/SNote3/SNote3Builder;

    invoke-virtual {v2}, Lcom/sec/android/sCloudSync/Builders/SNote3/SNote3Builder;->getjsonString()Ljava/util/Map;

    move-result-object v48

    .line 619
    .local v48, "jsonStringMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface/range {v27 .. v27}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v50

    .line 620
    .local v50, "keySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v24, Ljava/util/HashSet;

    invoke-direct/range {v24 .. v24}, Ljava/util/HashSet;-><init>()V

    .line 621
    .local v24, "failedDownloads":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v22, Ljava/util/HashSet;

    invoke-direct/range {v22 .. v22}, Ljava/util/HashSet;-><init>()V

    .line 625
    .local v22, "failDownloads":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    sget-object v2, Lcom/sec/android/sCloudSync/Constants/SNote3Contract;->FILEDETAIL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getCallerSyncAdapter()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 626
    .local v3, "fileDetailUri":Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getContentUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getCallerSyncAdapter()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getAccountName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v5

    iget-object v5, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getAccountType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v5

    iget-object v5, v5, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v53

    .line 631
    .local v53, "noteUri":Landroid/net/Uri;
    invoke-interface/range {v50 .. v50}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v37

    :cond_1
    :goto_0
    invoke-interface/range {v37 .. v37}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_27

    invoke-interface/range {v37 .. v37}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v49

    check-cast v49, Ljava/lang/String;

    .line 632
    .local v49, "key":Ljava/lang/String;
    const/16 v35, 0x0

    .line 633
    .local v35, "hasDownloadFailed":Z
    move-object/from16 v0, v27

    move-object/from16 v1, v49

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Ljava/util/List;

    .line 634
    .local v28, "fileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->mServerChangedRecords:Ljava/util/Map;

    move-object/from16 v0, v49

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v46

    check-cast v46, Lcom/sec/android/sCloudSync/Records/KVSItem;

    .line 635
    .local v46, "item":Lcom/sec/android/sCloudSync/Records/KVSItem;
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 637
    .local v13, "delFileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/16 v44, 0x0

    .line 639
    .local v44, "isFullDownload":Z
    invoke-virtual/range {v46 .. v46}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getID()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-lez v2, :cond_2

    invoke-virtual/range {v46 .. v46}, Lcom/sec/android/sCloudSync/Records/KVSItem;->isDeleted()Z

    move-result v2

    if-nez v2, :cond_2

    .line 640
    move-object/from16 v0, v58

    move-object/from16 v1, v49

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v59

    check-cast v59, Ljava/lang/String;

    .line 641
    .local v59, "snbFilePath":Ljava/lang/String;
    sget-object v2, Lcom/sec/android/sCloudSync/Constants/SNote3Contract;->EXTRACTSNB_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "filename"

    move-object/from16 v0, v59

    invoke-virtual {v2, v4, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "privatekey"

    move-object/from16 v0, v49

    invoke-virtual {v2, v4, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v20

    .line 645
    .local v20, "extractSnbUri":Landroid/net/Uri;
    if-eqz v59, :cond_2

    .line 647
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v2

    const/4 v4, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v2, v0, v4}, Landroid/content/ContentProviderClient;->openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    .line 657
    .end local v20    # "extractSnbUri":Landroid/net/Uri;
    .end local v59    # "snbFilePath":Ljava/lang/String;
    :cond_2
    :goto_1
    if-nez v44, :cond_a

    .line 660
    const/4 v12, 0x0

    .line 661
    .local v12, "cursor":Landroid/database/Cursor;
    invoke-virtual/range {v46 .. v46}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getID()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-lez v2, :cond_3

    invoke-virtual/range {v46 .. v46}, Lcom/sec/android/sCloudSync/Records/KVSItem;->isDeleted()Z

    move-result v2

    if-nez v2, :cond_3

    .line 663
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v2

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "path"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "checksum"

    aput-object v6, v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "spd_id = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v46 .. v46}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getID()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v12

    .line 673
    :cond_3
    :goto_2
    if-eqz v12, :cond_8

    .line 674
    :cond_4
    :goto_3
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 675
    const-string v2, "path"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    .line 676
    .local v21, "fPath":Ljava/lang/String;
    const-string v2, "checksum"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v51

    .line 677
    .local v51, "localCheckSum":Ljava/lang/String;
    move-object/from16 v0, v28

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 678
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->mbSyncCanceled:Z

    if-eqz v2, :cond_5

    .line 679
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 680
    new-instance v2, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/16 v4, 0x9

    invoke-direct {v2, v4}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v2

    .line 648
    .end local v12    # "cursor":Landroid/database/Cursor;
    .end local v21    # "fPath":Ljava/lang/String;
    .end local v51    # "localCheckSum":Ljava/lang/String;
    .restart local v20    # "extractSnbUri":Landroid/net/Uri;
    .restart local v59    # "snbFilePath":Ljava/lang/String;
    :catch_0
    move-exception v18

    .line 649
    .local v18, "e":Landroid/os/RemoteException;
    invoke-virtual/range {v18 .. v18}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 650
    .end local v18    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v18

    .line 651
    .local v18, "e":Ljava/io/FileNotFoundException;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "FileNotFoundException occurs from provider : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v18 .. v18}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 652
    const/16 v44, 0x1

    goto/16 :goto_1

    .line 665
    .end local v18    # "e":Ljava/io/FileNotFoundException;
    .end local v20    # "extractSnbUri":Landroid/net/Uri;
    .end local v59    # "snbFilePath":Ljava/lang/String;
    .restart local v12    # "cursor":Landroid/database/Cursor;
    :catch_2
    move-exception v19

    .line 666
    .local v19, "e1":Landroid/os/RemoteException;
    invoke-virtual/range {v19 .. v19}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_2

    .line 682
    .end local v19    # "e1":Landroid/os/RemoteException;
    .restart local v21    # "fPath":Ljava/lang/String;
    .restart local v51    # "localCheckSum":Ljava/lang/String;
    :cond_5
    const-string v2, "/"

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v60

    .line 683
    .local v60, "split":[Ljava/lang/String;
    move-object/from16 v0, v60

    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    aget-object v29, v60, v2

    .line 685
    .local v29, "fileName":Ljava/lang/String;
    :try_start_2
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getCloudServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getFileServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "/SNOTE3/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v49

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v29

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->getMetadata(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/sCloudSync/Records/ORSResponse;
    :try_end_2
    .catch Lcom/sec/android/sCloudSync/Util/SyncException; {:try_start_2 .. :try_end_2} :catch_3

    move-result-object v34

    .line 691
    .local v34, "getMetadataResponse":Lcom/sec/android/sCloudSync/Records/ORSResponse;
    invoke-virtual/range {v34 .. v34}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getResponseCode()I

    move-result v56

    .line 692
    .local v56, "ret_code":I
    if-nez v56, :cond_4

    .line 693
    invoke-virtual/range {v34 .. v34}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getORSResponse()Lcom/sec/android/sCloudSync/Records/RecordORSItem;

    move-result-object v52

    .line 694
    .local v52, "metadataRecords":Lcom/sec/android/sCloudSync/Records/RecordORSItem;
    if-eqz v52, :cond_4

    .line 696
    invoke-virtual/range {v52 .. v52}, Lcom/sec/android/sCloudSync/Records/RecordORSItem;->getValue()Ljava/lang/String;

    move-result-object v57

    .line 698
    .local v57, "serverCheckSum":Ljava/lang/String;
    move-object/from16 v0, v57

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 699
    move-object/from16 v0, v28

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 686
    .end local v34    # "getMetadataResponse":Lcom/sec/android/sCloudSync/Records/ORSResponse;
    .end local v52    # "metadataRecords":Lcom/sec/android/sCloudSync/Records/RecordORSItem;
    .end local v56    # "ret_code":I
    .end local v57    # "serverCheckSum":Ljava/lang/String;
    :catch_3
    move-exception v18

    .line 687
    .local v18, "e":Lcom/sec/android/sCloudSync/Util/SyncException;
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 688
    throw v18

    .line 703
    .end local v18    # "e":Lcom/sec/android/sCloudSync/Util/SyncException;
    .end local v29    # "fileName":Ljava/lang/String;
    .end local v60    # "split":[Ljava/lang/String;
    :cond_6
    move-object/from16 v0, v21

    invoke-interface {v13, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 706
    .end local v21    # "fPath":Ljava/lang/String;
    .end local v51    # "localCheckSum":Ljava/lang/String;
    :cond_7
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 712
    .end local v12    # "cursor":Landroid/database/Cursor;
    :cond_8
    :goto_4
    invoke-interface/range {v28 .. v28}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v38

    .local v38, "i$":Ljava/util/Iterator;
    :cond_9
    :goto_5
    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_d

    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Ljava/lang/String;

    .line 713
    .local v30, "filePath":Ljava/lang/String;
    const-string v2, "/"

    move-object/from16 v0, v30

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v60

    .line 714
    .restart local v60    # "split":[Ljava/lang/String;
    move-object/from16 v0, v60

    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    aget-object v29, v60, v2

    .line 716
    .restart local v29    # "fileName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->mbSyncCanceled:Z

    if-eqz v2, :cond_b

    .line 717
    new-instance v2, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/16 v4, 0x9

    invoke-direct {v2, v4}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v2

    .line 709
    .end local v29    # "fileName":Ljava/lang/String;
    .end local v30    # "filePath":Ljava/lang/String;
    .end local v38    # "i$":Ljava/util/Iterator;
    .end local v60    # "split":[Ljava/lang/String;
    :cond_a
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "FullDownload because of FileNotFound from Provider - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v49

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface/range {v28 .. v28}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " files"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 720
    .restart local v29    # "fileName":Ljava/lang/String;
    .restart local v30    # "filePath":Ljava/lang/String;
    .restart local v38    # "i$":Ljava/util/Iterator;
    .restart local v60    # "split":[Ljava/lang/String;
    :cond_b
    :try_start_3
    invoke-virtual/range {v46 .. v46}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getID()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-lez v2, :cond_12

    .line 721
    invoke-virtual/range {v46 .. v46}, Lcom/sec/android/sCloudSync/Records/KVSItem;->isDeleted()Z

    move-result v2

    if-nez v2, :cond_c

    .line 722
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getCloudServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getFileServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "/SNOTE3/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v49

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v29

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->TEMP_FOLDER:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v29

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v2, v4, v5, v6}, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->downloadFile(Ljava/lang/String;Ljava/lang/String;Z)Lcom/sec/android/sCloudSync/Records/ORSResponse;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    move-result-object v26

    .line 734
    :cond_c
    :goto_6
    if-nez v26, :cond_13

    .line 735
    move-object/from16 v0, v24

    move-object/from16 v1, v49

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 736
    const/16 v35, 0x1

    .line 737
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string v4, "file download response is null "

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 811
    .end local v29    # "fileName":Ljava/lang/String;
    .end local v30    # "filePath":Ljava/lang/String;
    .end local v60    # "split":[Ljava/lang/String;
    :cond_d
    :goto_7
    invoke-virtual/range {v46 .. v46}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getID()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-lez v2, :cond_24

    invoke-virtual/range {v46 .. v46}, Lcom/sec/android/sCloudSync/Records/KVSItem;->isDeleted()Z

    move-result v2

    if-nez v2, :cond_24

    .line 812
    if-nez v35, :cond_22

    .line 814
    invoke-interface/range {v28 .. v28}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v38

    :cond_e
    :goto_8
    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_22

    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Ljava/lang/String;

    .line 815
    .restart local v30    # "filePath":Ljava/lang/String;
    const-string v2, "/"

    move-object/from16 v0, v30

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v60

    .line 816
    .restart local v60    # "split":[Ljava/lang/String;
    move-object/from16 v0, v60

    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    aget-object v29, v60, v2

    .line 817
    .restart local v29    # "fileName":Ljava/lang/String;
    new-instance v61, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->TEMP_FOLDER:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v29

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v61

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 818
    .local v61, "tempFile":Ljava/io/File;
    const/16 v41, 0x0

    .line 819
    .local v41, "inpStream":Ljava/io/InputStream;
    const/16 v25, 0x0

    .line 820
    .local v25, "fileDesc":Landroid/os/ParcelFileDescriptor;
    const/16 v32, 0x0

    .line 822
    .local v32, "fos":Ljava/io/FileOutputStream;
    :try_start_4
    new-instance v42, Ljava/io/FileInputStream;

    move-object/from16 v0, v42

    move-object/from16 v1, v61

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_f
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 823
    .end local v41    # "inpStream":Ljava/io/InputStream;
    .local v42, "inpStream":Ljava/io/InputStream;
    :try_start_5
    invoke-static/range {v42 .. v42}, Lcom/sec/android/sCloudSync/Tools/FileTool;->getByteArr(Ljava/io/InputStream;)[B

    move-result-object v10

    .line 829
    .local v10, "buffer":[B
    sget-object v2, Lcom/sec/android/sCloudSync/Constants/SNote3Contract;->DELETEDETAIL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "filename"

    move-object/from16 v0, v30

    invoke-virtual {v2, v4, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "privatekey"

    move-object/from16 v0, v49

    invoke-virtual {v2, v4, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v14

    .line 834
    .local v14, "deleteFileDetailUri":Landroid/net/Uri;
    sget-object v2, Lcom/sec/android/sCloudSync/Constants/SNote3Contract;->FILEDETAIL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "filename"

    move-object/from16 v0, v30

    invoke-virtual {v2, v4, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "privatekey"

    move-object/from16 v0, v49

    invoke-virtual {v2, v4, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v31

    .line 839
    .local v31, "filedetailUri":Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, v14, v4}, Landroid/content/ContentProviderClient;->openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    .line 840
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v2

    const/4 v4, 0x0

    move-object/from16 v0, v31

    invoke-virtual {v2, v0, v4}, Landroid/content/ContentProviderClient;->openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v25

    .line 841
    new-instance v33, Ljava/io/FileOutputStream;

    invoke-virtual/range {v25 .. v25}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v2

    move-object/from16 v0, v33

    invoke-direct {v0, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1a
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 842
    .end local v32    # "fos":Ljava/io/FileOutputStream;
    .local v33, "fos":Ljava/io/FileOutputStream;
    :try_start_6
    move-object/from16 v0, v33

    invoke-virtual {v0, v10}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1b
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 847
    if-eqz v25, :cond_f

    .line 848
    :try_start_7
    invoke-virtual/range {v25 .. v25}, Landroid/os/ParcelFileDescriptor;->close()V

    .line 849
    :cond_f
    if-eqz v42, :cond_10

    .line 850
    invoke-virtual/range {v42 .. v42}, Ljava/io/InputStream;->close()V

    .line 851
    :cond_10
    if-eqz v33, :cond_11

    .line 852
    invoke-virtual/range {v33 .. v33}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_e

    :cond_11
    move-object/from16 v32, v33

    .end local v33    # "fos":Ljava/io/FileOutputStream;
    .restart local v32    # "fos":Ljava/io/FileOutputStream;
    move-object/from16 v41, v42

    .line 856
    .end local v42    # "inpStream":Ljava/io/InputStream;
    .restart local v41    # "inpStream":Ljava/io/InputStream;
    goto/16 :goto_8

    .line 725
    .end local v10    # "buffer":[B
    .end local v14    # "deleteFileDetailUri":Landroid/net/Uri;
    .end local v25    # "fileDesc":Landroid/os/ParcelFileDescriptor;
    .end local v31    # "filedetailUri":Landroid/net/Uri;
    .end local v32    # "fos":Ljava/io/FileOutputStream;
    .end local v41    # "inpStream":Ljava/io/InputStream;
    .end local v61    # "tempFile":Ljava/io/File;
    :cond_12
    :try_start_8
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getCloudServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getFileServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "/SNOTE3/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v49

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v29

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    move-object/from16 v0, v30

    invoke-virtual {v2, v4, v0, v5}, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->downloadFile(Ljava/lang/String;Ljava/lang/String;Z)Lcom/sec/android/sCloudSync/Records/ORSResponse;
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    move-result-object v26

    goto/16 :goto_6

    .line 727
    :catch_4
    move-exception v18

    .line 728
    .local v18, "e":Ljava/io/IOException;
    invoke-virtual/range {v18 .. v18}, Ljava/io/IOException;->printStackTrace()V

    .line 729
    move-object/from16 v0, v24

    move-object/from16 v1, v49

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 730
    const/16 v35, 0x1

    .line 731
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string v4, "IOexception when trying to download file"

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 740
    .end local v18    # "e":Ljava/io/IOException;
    :cond_13
    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getResponseCode()I

    move-result v2

    if-eqz v2, :cond_14

    .line 747
    move-object/from16 v0, v24

    move-object/from16 v1, v49

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 748
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "failed to download snote file. Rcode is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/sCloudSync/Records/ORSResponse;->getResponseCode()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 749
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "file name = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v29

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 750
    const/16 v35, 0x1

    .line 751
    goto/16 :goto_7

    .line 753
    :cond_14
    invoke-virtual/range {v46 .. v46}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getID()J

    move-result-wide v4

    const-wide/16 v6, -0x1

    cmp-long v2, v4, v6

    if-nez v2, :cond_9

    .line 759
    sget-object v2, Lcom/sec/android/sCloudSync/Constants/SNote3Contract;->DELETEDETAIL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "filename"

    move-object/from16 v0, v30

    invoke-virtual {v2, v4, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "privatekey"

    move-object/from16 v0, v49

    invoke-virtual {v2, v4, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v14

    .line 764
    .restart local v14    # "deleteFileDetailUri":Landroid/net/Uri;
    sget-object v2, Lcom/sec/android/sCloudSync/Constants/SNote3Contract;->FILEDETAIL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "filename"

    move-object/from16 v0, v30

    invoke-virtual {v2, v4, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "privatekey"

    move-object/from16 v0, v49

    invoke-virtual {v2, v4, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v31

    .line 768
    .restart local v31    # "filedetailUri":Landroid/net/Uri;
    const/16 v25, 0x0

    .line 769
    .restart local v25    # "fileDesc":Landroid/os/ParcelFileDescriptor;
    const/16 v32, 0x0

    .line 771
    .restart local v32    # "fos":Ljava/io/FileOutputStream;
    :try_start_9
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, v14, v4}, Landroid/content/ContentProviderClient;->openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    .line 772
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v2

    const/4 v4, 0x0

    move-object/from16 v0, v31

    invoke-virtual {v2, v0, v4}, Landroid/content/ContentProviderClient;->openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v25

    .line 773
    if-nez v25, :cond_16

    .line 774
    move-object/from16 v0, v24

    move-object/from16 v1, v49

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_9} :catch_7
    .catch Ljava/io/FileNotFoundException; {:try_start_9 .. :try_end_9} :catch_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_b
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 775
    const/16 v35, 0x1

    .line 798
    if-eqz v25, :cond_15

    .line 799
    :try_start_a
    invoke-virtual/range {v25 .. v25}, Landroid/os/ParcelFileDescriptor;->close()V

    .line 800
    :cond_15
    if-eqz v32, :cond_d

    .line 801
    invoke-virtual/range {v32 .. v32}, Ljava/io/FileOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5

    goto/16 :goto_7

    .line 802
    :catch_5
    move-exception v18

    .line 803
    .restart local v18    # "e":Ljava/io/IOException;
    invoke-virtual/range {v18 .. v18}, Ljava/io/IOException;->printStackTrace()V

    .line 804
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string v4, "IOException when closing ParcelFileDescriptor"

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 778
    .end local v18    # "e":Ljava/io/IOException;
    :cond_16
    :try_start_b
    new-instance v33, Ljava/io/FileOutputStream;

    invoke-virtual/range {v25 .. v25}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v2

    move-object/from16 v0, v33

    invoke-direct {v0, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_b} :catch_7
    .catch Ljava/io/FileNotFoundException; {:try_start_b .. :try_end_b} :catch_9
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 779
    .end local v32    # "fos":Ljava/io/FileOutputStream;
    .restart local v33    # "fos":Ljava/io/FileOutputStream;
    :try_start_c
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getCloudServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;->getFileServiceManager()Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/sCloudSync/ServiceManagers/ORSServiceManager;->getBuffer()[B

    move-result-object v2

    move-object/from16 v0, v33

    invoke-virtual {v0, v2}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_c
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_c} :catch_1e
    .catch Ljava/io/FileNotFoundException; {:try_start_c .. :try_end_c} :catch_1d
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_1c
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    .line 798
    if-eqz v25, :cond_17

    .line 799
    :try_start_d
    invoke-virtual/range {v25 .. v25}, Landroid/os/ParcelFileDescriptor;->close()V

    .line 800
    :cond_17
    if-eqz v33, :cond_9

    .line 801
    invoke-virtual/range {v33 .. v33}, Ljava/io/FileOutputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_6

    goto/16 :goto_5

    .line 802
    :catch_6
    move-exception v18

    .line 803
    .restart local v18    # "e":Ljava/io/IOException;
    invoke-virtual/range {v18 .. v18}, Ljava/io/IOException;->printStackTrace()V

    .line 804
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string v4, "IOException when closing ParcelFileDescriptor"

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 781
    .end local v18    # "e":Ljava/io/IOException;
    .end local v33    # "fos":Ljava/io/FileOutputStream;
    .restart local v32    # "fos":Ljava/io/FileOutputStream;
    :catch_7
    move-exception v18

    .line 782
    .local v18, "e":Landroid/os/RemoteException;
    :goto_9
    :try_start_e
    invoke-virtual/range {v18 .. v18}, Landroid/os/RemoteException;->printStackTrace()V

    .line 783
    move-object/from16 v0, v24

    move-object/from16 v1, v49

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    .line 784
    const/16 v35, 0x1

    .line 798
    if-eqz v25, :cond_18

    .line 799
    :try_start_f
    invoke-virtual/range {v25 .. v25}, Landroid/os/ParcelFileDescriptor;->close()V

    .line 800
    :cond_18
    if-eqz v32, :cond_d

    .line 801
    invoke-virtual/range {v32 .. v32}, Ljava/io/FileOutputStream;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_8

    goto/16 :goto_7

    .line 802
    :catch_8
    move-exception v18

    .line 803
    .local v18, "e":Ljava/io/IOException;
    invoke-virtual/range {v18 .. v18}, Ljava/io/IOException;->printStackTrace()V

    .line 804
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string v4, "IOException when closing ParcelFileDescriptor"

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 786
    .end local v18    # "e":Ljava/io/IOException;
    :catch_9
    move-exception v18

    .line 787
    .local v18, "e":Ljava/io/FileNotFoundException;
    :goto_a
    :try_start_10
    invoke-virtual/range {v18 .. v18}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 788
    move-object/from16 v0, v24

    move-object/from16 v1, v49

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    .line 789
    const/16 v35, 0x1

    .line 798
    if-eqz v25, :cond_19

    .line 799
    :try_start_11
    invoke-virtual/range {v25 .. v25}, Landroid/os/ParcelFileDescriptor;->close()V

    .line 800
    :cond_19
    if-eqz v32, :cond_d

    .line 801
    invoke-virtual/range {v32 .. v32}, Ljava/io/FileOutputStream;->close()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_a

    goto/16 :goto_7

    .line 802
    :catch_a
    move-exception v18

    .line 803
    .local v18, "e":Ljava/io/IOException;
    invoke-virtual/range {v18 .. v18}, Ljava/io/IOException;->printStackTrace()V

    .line 804
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string v4, "IOException when closing ParcelFileDescriptor"

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 791
    .end local v18    # "e":Ljava/io/IOException;
    :catch_b
    move-exception v18

    .line 792
    .restart local v18    # "e":Ljava/io/IOException;
    :goto_b
    :try_start_12
    invoke-virtual/range {v18 .. v18}, Ljava/io/IOException;->printStackTrace()V

    .line 793
    move-object/from16 v0, v24

    move-object/from16 v1, v49

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    .line 794
    const/16 v35, 0x1

    .line 798
    if-eqz v25, :cond_1a

    .line 799
    :try_start_13
    invoke-virtual/range {v25 .. v25}, Landroid/os/ParcelFileDescriptor;->close()V

    .line 800
    :cond_1a
    if-eqz v32, :cond_d

    .line 801
    invoke-virtual/range {v32 .. v32}, Ljava/io/FileOutputStream;->close()V
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_c

    goto/16 :goto_7

    .line 802
    :catch_c
    move-exception v18

    .line 803
    invoke-virtual/range {v18 .. v18}, Ljava/io/IOException;->printStackTrace()V

    .line 804
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string v4, "IOException when closing ParcelFileDescriptor"

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 797
    .end local v18    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    .line 798
    :goto_c
    if-eqz v25, :cond_1b

    .line 799
    :try_start_14
    invoke-virtual/range {v25 .. v25}, Landroid/os/ParcelFileDescriptor;->close()V

    .line 800
    :cond_1b
    if-eqz v32, :cond_1c

    .line 801
    invoke-virtual/range {v32 .. v32}, Ljava/io/FileOutputStream;->close()V
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_d

    .line 805
    :cond_1c
    :goto_d
    throw v2

    .line 802
    :catch_d
    move-exception v18

    .line 803
    .restart local v18    # "e":Ljava/io/IOException;
    invoke-virtual/range {v18 .. v18}, Ljava/io/IOException;->printStackTrace()V

    .line 804
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v4

    const-string v5, "IOException when closing ParcelFileDescriptor"

    invoke-static {v4, v5}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_d

    .line 853
    .end local v18    # "e":Ljava/io/IOException;
    .end local v32    # "fos":Ljava/io/FileOutputStream;
    .restart local v10    # "buffer":[B
    .restart local v33    # "fos":Ljava/io/FileOutputStream;
    .restart local v42    # "inpStream":Ljava/io/InputStream;
    .restart local v61    # "tempFile":Ljava/io/File;
    :catch_e
    move-exception v18

    .line 854
    .restart local v18    # "e":Ljava/io/IOException;
    invoke-virtual/range {v18 .. v18}, Ljava/io/IOException;->printStackTrace()V

    .line 855
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string v4, "IOException when closing ParcelFileDescriptor"

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v32, v33

    .end local v33    # "fos":Ljava/io/FileOutputStream;
    .restart local v32    # "fos":Ljava/io/FileOutputStream;
    move-object/from16 v41, v42

    .line 857
    .end local v42    # "inpStream":Ljava/io/InputStream;
    .restart local v41    # "inpStream":Ljava/io/InputStream;
    goto/16 :goto_8

    .line 843
    .end local v10    # "buffer":[B
    .end local v14    # "deleteFileDetailUri":Landroid/net/Uri;
    .end local v18    # "e":Ljava/io/IOException;
    .end local v31    # "filedetailUri":Landroid/net/Uri;
    :catch_f
    move-exception v18

    .line 844
    .local v18, "e":Ljava/lang/Exception;
    :goto_e
    :try_start_15
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_1

    .line 847
    if-eqz v25, :cond_1d

    .line 848
    :try_start_16
    invoke-virtual/range {v25 .. v25}, Landroid/os/ParcelFileDescriptor;->close()V

    .line 849
    :cond_1d
    if-eqz v41, :cond_1e

    .line 850
    invoke-virtual/range {v41 .. v41}, Ljava/io/InputStream;->close()V

    .line 851
    :cond_1e
    if-eqz v32, :cond_e

    .line 852
    invoke-virtual/range {v32 .. v32}, Ljava/io/FileOutputStream;->close()V
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_16} :catch_10

    goto/16 :goto_8

    .line 853
    :catch_10
    move-exception v18

    .line 854
    .local v18, "e":Ljava/io/IOException;
    invoke-virtual/range {v18 .. v18}, Ljava/io/IOException;->printStackTrace()V

    .line 855
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string v4, "IOException when closing ParcelFileDescriptor"

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_8

    .line 846
    .end local v18    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v2

    .line 847
    :goto_f
    if-eqz v25, :cond_1f

    .line 848
    :try_start_17
    invoke-virtual/range {v25 .. v25}, Landroid/os/ParcelFileDescriptor;->close()V

    .line 849
    :cond_1f
    if-eqz v41, :cond_20

    .line 850
    invoke-virtual/range {v41 .. v41}, Ljava/io/InputStream;->close()V

    .line 851
    :cond_20
    if-eqz v32, :cond_21

    .line 852
    invoke-virtual/range {v32 .. v32}, Ljava/io/FileOutputStream;->close()V
    :try_end_17
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_17} :catch_11

    .line 856
    :cond_21
    :goto_10
    throw v2

    .line 853
    :catch_11
    move-exception v18

    .line 854
    .restart local v18    # "e":Ljava/io/IOException;
    invoke-virtual/range {v18 .. v18}, Ljava/io/IOException;->printStackTrace()V

    .line 855
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getTag()Ljava/lang/String;

    move-result-object v4

    const-string v5, "IOException when closing ParcelFileDescriptor"

    invoke-static {v4, v5}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_10

    .line 860
    .end local v18    # "e":Ljava/io/IOException;
    .end local v25    # "fileDesc":Landroid/os/ParcelFileDescriptor;
    .end local v29    # "fileName":Ljava/lang/String;
    .end local v30    # "filePath":Ljava/lang/String;
    .end local v32    # "fos":Ljava/io/FileOutputStream;
    .end local v41    # "inpStream":Ljava/io/InputStream;
    .end local v60    # "split":[Ljava/lang/String;
    .end local v61    # "tempFile":Ljava/io/File;
    :cond_22
    new-instance v62, Ljava/util/ArrayList;

    invoke-direct/range {v62 .. v62}, Ljava/util/ArrayList;-><init>()V

    .line 861
    .local v62, "tempFileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface/range {v28 .. v28}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v38

    :goto_11
    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_23

    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Ljava/lang/String;

    .line 862
    .restart local v30    # "filePath":Ljava/lang/String;
    const-string v2, "/"

    move-object/from16 v0, v30

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v60

    .line 863
    .restart local v60    # "split":[Ljava/lang/String;
    move-object/from16 v0, v60

    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    aget-object v29, v60, v2

    .line 864
    .restart local v29    # "fileName":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->TEMP_FOLDER:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v29

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v62

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_11

    .line 867
    .end local v29    # "fileName":Ljava/lang/String;
    .end local v30    # "filePath":Ljava/lang/String;
    .end local v60    # "split":[Ljava/lang/String;
    :cond_23
    move-object/from16 v0, p0

    move-object/from16 v1, v62

    invoke-direct {v0, v1}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->deleteFiles(Ljava/util/List;)V

    .line 869
    new-instance v17, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->TEMP_FOLDER:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 870
    .local v17, "dir":Ljava/io/File;
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_24

    .line 872
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->delete()Z

    move-result v43

    .line 873
    .local v43, "isDeleteSuccess":Z
    if-nez v43, :cond_24

    .line 874
    const-string v2, "SNote3SyncAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Folder "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->TEMP_FOLDER:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " could not be deleted"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 880
    .end local v17    # "dir":Ljava/io/File;
    .end local v43    # "isDeleteSuccess":Z
    .end local v62    # "tempFileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_24
    if-eqz v13, :cond_25

    invoke-interface {v13}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_25

    .line 881
    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v38

    :goto_12
    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_25

    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/String;

    .line 882
    .restart local v21    # "fPath":Ljava/lang/String;
    sget-object v2, Lcom/sec/android/sCloudSync/Constants/SNote3Contract;->DELETEDETAIL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "filename"

    move-object/from16 v0, v21

    invoke-virtual {v2, v4, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "privatekey"

    move-object/from16 v0, v49

    invoke-virtual {v2, v4, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v14

    .line 887
    .restart local v14    # "deleteFileDetailUri":Landroid/net/Uri;
    :try_start_18
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, v14, v4}, Landroid/content/ContentProviderClient;->openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    :try_end_18
    .catch Landroid/os/RemoteException; {:try_start_18 .. :try_end_18} :catch_12
    .catch Ljava/io/FileNotFoundException; {:try_start_18 .. :try_end_18} :catch_13

    goto :goto_12

    .line 888
    :catch_12
    move-exception v18

    .line 889
    .local v18, "e":Landroid/os/RemoteException;
    invoke-virtual/range {v18 .. v18}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_12

    .line 890
    .end local v18    # "e":Landroid/os/RemoteException;
    :catch_13
    move-exception v18

    .line 891
    .local v18, "e":Ljava/io/FileNotFoundException;
    invoke-virtual/range {v18 .. v18}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_12

    .line 896
    .end local v14    # "deleteFileDetailUri":Landroid/net/Uri;
    .end local v18    # "e":Ljava/io/FileNotFoundException;
    .end local v21    # "fPath":Ljava/lang/String;
    :cond_25
    move-object/from16 v0, v58

    move-object/from16 v1, v49

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v59

    check-cast v59, Ljava/lang/String;

    .line 897
    .restart local v59    # "snbFilePath":Ljava/lang/String;
    if-eqz v59, :cond_1

    .line 899
    if-nez v35, :cond_26

    .line 900
    :try_start_19
    sget-object v2, Lcom/sec/android/sCloudSync/Constants/SNote3Contract;->CREATESNB_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "json"

    invoke-interface/range {v48 .. v49}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v4, v5, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "privatekey"

    move-object/from16 v0, v49

    invoke-virtual {v2, v4, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "timestamp"

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->mServerChangedRecords:Ljava/util/Map;

    move-object/from16 v0, v49

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/sCloudSync/Records/KVSItem;

    invoke-virtual {v2}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getTimeStamp()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v5, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v11

    .line 906
    .local v11, "createSnbUri":Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, v11, v4}, Landroid/content/ContentProviderClient;->openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    .line 908
    invoke-virtual/range {v46 .. v46}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getID()J

    move-result-wide v4

    const-wide/16 v6, -0x1

    cmp-long v2, v4, v6

    if-nez v2, :cond_1

    .line 909
    new-instance v64, Landroid/content/ContentValues;

    invoke-direct/range {v64 .. v64}, Landroid/content/ContentValues;-><init>()V

    .line 910
    .local v64, "values":Landroid/content/ContentValues;
    const-string v2, "deleted"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v64

    invoke-virtual {v0, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_19
    .catch Landroid/os/RemoteException; {:try_start_19 .. :try_end_19} :catch_15
    .catch Ljava/io/FileNotFoundException; {:try_start_19 .. :try_end_19} :catch_16

    .line 912
    :try_start_1a
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getKeyColumnName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " = \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v49

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move-object/from16 v0, v53

    move-object/from16 v1, v64

    invoke-virtual {v2, v0, v1, v4, v5}, Landroid/content/ContentProviderClient;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1a
    .catch Landroid/os/RemoteException; {:try_start_1a .. :try_end_1a} :catch_14
    .catch Ljava/io/FileNotFoundException; {:try_start_1a .. :try_end_1a} :catch_16

    goto/16 :goto_0

    .line 913
    :catch_14
    move-exception v18

    .line 914
    .local v18, "e":Landroid/os/RemoteException;
    :try_start_1b
    invoke-virtual/range {v18 .. v18}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_1b
    .catch Landroid/os/RemoteException; {:try_start_1b .. :try_end_1b} :catch_15
    .catch Ljava/io/FileNotFoundException; {:try_start_1b .. :try_end_1b} :catch_16

    goto/16 :goto_0

    .line 925
    .end local v11    # "createSnbUri":Landroid/net/Uri;
    .end local v18    # "e":Landroid/os/RemoteException;
    .end local v64    # "values":Landroid/content/ContentValues;
    :catch_15
    move-exception v18

    .line 926
    .restart local v18    # "e":Landroid/os/RemoteException;
    invoke-virtual/range {v18 .. v18}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    .line 919
    .end local v18    # "e":Landroid/os/RemoteException;
    :cond_26
    :try_start_1c
    sget-object v2, Lcom/sec/android/sCloudSync/Constants/SNote3Contract;->DELETESNB_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "filename"

    move-object/from16 v0, v59

    invoke-virtual {v2, v4, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "privatekey"

    move-object/from16 v0, v49

    invoke-virtual {v2, v4, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v16

    .line 923
    .local v16, "deleteSnbUri":Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v2

    const/4 v4, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v2, v0, v4}, Landroid/content/ContentProviderClient;->openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    :try_end_1c
    .catch Landroid/os/RemoteException; {:try_start_1c .. :try_end_1c} :catch_15
    .catch Ljava/io/FileNotFoundException; {:try_start_1c .. :try_end_1c} :catch_16

    goto/16 :goto_0

    .line 927
    .end local v16    # "deleteSnbUri":Landroid/net/Uri;
    :catch_16
    move-exception v18

    .line 928
    .local v18, "e":Ljava/io/FileNotFoundException;
    invoke-virtual/range {v18 .. v18}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto/16 :goto_0

    .line 933
    .end local v13    # "delFileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v18    # "e":Ljava/io/FileNotFoundException;
    .end local v28    # "fileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v35    # "hasDownloadFailed":Z
    .end local v38    # "i$":Ljava/util/Iterator;
    .end local v44    # "isFullDownload":Z
    .end local v46    # "item":Lcom/sec/android/sCloudSync/Records/KVSItem;
    .end local v49    # "key":Ljava/lang/String;
    .end local v59    # "snbFilePath":Ljava/lang/String;
    :cond_27
    const/16 v36, 0x0

    .line 936
    .local v36, "hasInsertFailed":Z
    if-eqz v24, :cond_2e

    invoke-interface/range {v24 .. v24}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2e

    .line 937
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->resetLastSyncTime()V

    .line 938
    invoke-interface/range {v24 .. v24}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v37

    .local v37, "i$":Ljava/util/Iterator;
    :cond_28
    :goto_13
    invoke-interface/range {v37 .. v37}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2e

    invoke-interface/range {v37 .. v37}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/String;

    .line 939
    .local v23, "failed":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->mServerChangedRecords:Ljava/util/Map;

    move-object/from16 v0, v23

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v46

    check-cast v46, Lcom/sec/android/sCloudSync/Records/KVSItem;

    .line 940
    .restart local v46    # "item":Lcom/sec/android/sCloudSync/Records/KVSItem;
    invoke-virtual/range {v46 .. v46}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getID()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-lez v2, :cond_2d

    .line 941
    invoke-virtual/range {v46 .. v46}, Lcom/sec/android/sCloudSync/Records/KVSItem;->isDeleted()Z

    move-result v2

    if-nez v2, :cond_28

    .line 943
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getContentUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual/range {v46 .. v46}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getID()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v63

    .line 944
    .local v63, "uri":Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getCallerSyncAdapter()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v63

    invoke-static {v0, v2}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getAccountName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v5

    iget-object v5, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getAccountType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v5

    iget-object v5, v5, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v63

    .line 948
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->mBuilder:Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;

    check-cast v2, Lcom/sec/android/sCloudSync/Builders/SNote3/SNote3Builder;

    invoke-virtual {v2}, Lcom/sec/android/sCloudSync/Builders/SNote3/SNote3Builder;->getOperations()Ljava/util/ArrayList;

    move-result-object v55

    .line 949
    .local v55, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    invoke-virtual/range {v55 .. v55}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v47

    .line 950
    .local v47, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/content/ContentProviderOperation;>;"
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 951
    .local v15, "deleteOperation":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :cond_29
    :goto_14
    invoke-interface/range {v47 .. v47}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2b

    .line 953
    invoke-interface/range {v47 .. v47}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v54

    check-cast v54, Landroid/content/ContentProviderOperation;

    .line 954
    .local v54, "operation":Landroid/content/ContentProviderOperation;
    invoke-virtual/range {v54 .. v54}, Landroid/content/ContentProviderOperation;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {v63 .. v63}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2a

    .line 955
    move-object/from16 v0, v54

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 956
    :cond_2a
    invoke-virtual/range {v54 .. v54}, Landroid/content/ContentProviderOperation;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "spd_id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v46 .. v46}, Lcom/sec/android/sCloudSync/Records/KVSItem;->getID()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_29

    .line 958
    move-object/from16 v0, v54

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_14

    .line 960
    .end local v54    # "operation":Landroid/content/ContentProviderOperation;
    :cond_2b
    invoke-virtual {v15}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v45

    .line 961
    .local v45, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/content/ContentProviderOperation;>;"
    :goto_15
    invoke-interface/range {v45 .. v45}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2c

    .line 962
    invoke-interface/range {v45 .. v45}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object/from16 v0, v55

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_15

    .line 964
    :cond_2c
    move-object/from16 v0, v27

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_13

    .line 971
    .end local v15    # "deleteOperation":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v45    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/content/ContentProviderOperation;>;"
    .end local v47    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/content/ContentProviderOperation;>;"
    .end local v55    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v63    # "uri":Landroid/net/Uri;
    :cond_2d
    invoke-interface/range {v22 .. v23}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 972
    const/16 v36, 0x1

    goto/16 :goto_13

    .line 981
    .end local v23    # "failed":Ljava/lang/String;
    .end local v37    # "i$":Ljava/util/Iterator;
    .end local v46    # "item":Lcom/sec/android/sCloudSync/Records/KVSItem;
    :cond_2e
    if-eqz v36, :cond_33

    .line 982
    new-instance v65, Ljava/lang/StringBuilder;

    invoke-interface/range {v22 .. v22}, Ljava/util/Set;->size()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    mul-int/lit16 v2, v2, 0x104

    move-object/from16 v0, v65

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 983
    .local v65, "where":Ljava/lang/StringBuilder;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getKeyColumnName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v65

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " IN ("

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 984
    move-object/from16 v0, v65

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Tools/AppTool;->appendIds(Ljava/lang/StringBuilder;Ljava/util/Set;)V

    .line 985
    const/16 v2, 0x29

    move-object/from16 v0, v65

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 987
    const/4 v12, 0x0

    .line 989
    .restart local v12    # "cursor":Landroid/database/Cursor;
    :try_start_1d
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v4

    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v5, "_id"

    aput-object v5, v6, v2

    invoke-virtual/range {v65 .. v65}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v5, v53

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1d
    .catch Landroid/os/RemoteException; {:try_start_1d .. :try_end_1d} :catch_17

    move-result-object v12

    .line 994
    :goto_16
    new-instance v40, Ljava/util/HashSet;

    invoke-direct/range {v40 .. v40}, Ljava/util/HashSet;-><init>()V

    .line 995
    .local v40, "ids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    if-eqz v12, :cond_30

    .line 996
    :goto_17
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2f

    .line 997
    const/4 v2, 0x0

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, v40

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_17

    .line 991
    .end local v40    # "ids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    :catch_17
    move-exception v18

    .line 992
    .local v18, "e":Landroid/os/RemoteException;
    invoke-virtual/range {v18 .. v18}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_16

    .line 999
    .end local v18    # "e":Landroid/os/RemoteException;
    .restart local v40    # "ids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    :cond_2f
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 1003
    :cond_30
    :try_start_1e
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v2

    invoke-virtual/range {v65 .. v65}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move-object/from16 v0, v53

    invoke-virtual {v2, v0, v4, v5}, Landroid/content/ContentProviderClient;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1e
    .catch Landroid/os/RemoteException; {:try_start_1e .. :try_end_1e} :catch_18

    .line 1008
    :goto_18
    const/4 v2, 0x0

    move-object/from16 v0, v65

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 1009
    const-string v2, "spd_id"

    move-object/from16 v0, v65

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " IN ("

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1010
    invoke-interface/range {v40 .. v40}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v37

    .restart local v37    # "i$":Ljava/util/Iterator;
    :goto_19
    invoke-interface/range {v37 .. v37}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_31

    invoke-interface/range {v37 .. v37}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Ljava/lang/Long;

    .line 1011
    .local v39, "id":Ljava/lang/Long;
    move-object/from16 v0, v65

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v4, 0x2c

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_19

    .line 1004
    .end local v37    # "i$":Ljava/util/Iterator;
    .end local v39    # "id":Ljava/lang/Long;
    :catch_18
    move-exception v18

    .line 1005
    .restart local v18    # "e":Landroid/os/RemoteException;
    invoke-virtual/range {v18 .. v18}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_18

    .line 1014
    .end local v18    # "e":Landroid/os/RemoteException;
    .restart local v37    # "i$":Ljava/util/Iterator;
    :cond_31
    invoke-interface/range {v40 .. v40}, Ljava/util/Set;->size()I

    move-result v2

    if-lez v2, :cond_32

    .line 1015
    invoke-virtual/range {v65 .. v65}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move-object/from16 v0, v65

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 1017
    :cond_32
    const/16 v2, 0x29

    move-object/from16 v0, v65

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1019
    :try_start_1f
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v2

    invoke-virtual/range {v65 .. v65}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentProviderClient;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1f
    .catch Landroid/os/RemoteException; {:try_start_1f .. :try_end_1f} :catch_19

    .line 1024
    .end local v12    # "cursor":Landroid/database/Cursor;
    .end local v37    # "i$":Ljava/util/Iterator;
    .end local v40    # "ids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    .end local v65    # "where":Ljava/lang/StringBuilder;
    :cond_33
    :goto_1a
    return-void

    .line 1020
    .restart local v12    # "cursor":Landroid/database/Cursor;
    .restart local v37    # "i$":Ljava/util/Iterator;
    .restart local v40    # "ids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    .restart local v65    # "where":Ljava/lang/StringBuilder;
    :catch_19
    move-exception v18

    .line 1021
    .restart local v18    # "e":Landroid/os/RemoteException;
    invoke-virtual/range {v18 .. v18}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1a

    .line 846
    .end local v12    # "cursor":Landroid/database/Cursor;
    .end local v18    # "e":Landroid/os/RemoteException;
    .end local v36    # "hasInsertFailed":Z
    .end local v37    # "i$":Ljava/util/Iterator;
    .end local v40    # "ids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    .end local v65    # "where":Ljava/lang/StringBuilder;
    .restart local v13    # "delFileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v25    # "fileDesc":Landroid/os/ParcelFileDescriptor;
    .restart local v28    # "fileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v29    # "fileName":Ljava/lang/String;
    .restart local v30    # "filePath":Ljava/lang/String;
    .restart local v32    # "fos":Ljava/io/FileOutputStream;
    .restart local v35    # "hasDownloadFailed":Z
    .restart local v38    # "i$":Ljava/util/Iterator;
    .restart local v42    # "inpStream":Ljava/io/InputStream;
    .restart local v44    # "isFullDownload":Z
    .restart local v46    # "item":Lcom/sec/android/sCloudSync/Records/KVSItem;
    .restart local v49    # "key":Ljava/lang/String;
    .restart local v60    # "split":[Ljava/lang/String;
    .restart local v61    # "tempFile":Ljava/io/File;
    :catchall_2
    move-exception v2

    move-object/from16 v41, v42

    .end local v42    # "inpStream":Ljava/io/InputStream;
    .restart local v41    # "inpStream":Ljava/io/InputStream;
    goto/16 :goto_f

    .end local v32    # "fos":Ljava/io/FileOutputStream;
    .end local v41    # "inpStream":Ljava/io/InputStream;
    .restart local v10    # "buffer":[B
    .restart local v14    # "deleteFileDetailUri":Landroid/net/Uri;
    .restart local v31    # "filedetailUri":Landroid/net/Uri;
    .restart local v33    # "fos":Ljava/io/FileOutputStream;
    .restart local v42    # "inpStream":Ljava/io/InputStream;
    :catchall_3
    move-exception v2

    move-object/from16 v32, v33

    .end local v33    # "fos":Ljava/io/FileOutputStream;
    .restart local v32    # "fos":Ljava/io/FileOutputStream;
    move-object/from16 v41, v42

    .end local v42    # "inpStream":Ljava/io/InputStream;
    .restart local v41    # "inpStream":Ljava/io/InputStream;
    goto/16 :goto_f

    .line 843
    .end local v10    # "buffer":[B
    .end local v14    # "deleteFileDetailUri":Landroid/net/Uri;
    .end local v31    # "filedetailUri":Landroid/net/Uri;
    .end local v41    # "inpStream":Ljava/io/InputStream;
    .restart local v42    # "inpStream":Ljava/io/InputStream;
    :catch_1a
    move-exception v18

    move-object/from16 v41, v42

    .end local v42    # "inpStream":Ljava/io/InputStream;
    .restart local v41    # "inpStream":Ljava/io/InputStream;
    goto/16 :goto_e

    .end local v32    # "fos":Ljava/io/FileOutputStream;
    .end local v41    # "inpStream":Ljava/io/InputStream;
    .restart local v10    # "buffer":[B
    .restart local v14    # "deleteFileDetailUri":Landroid/net/Uri;
    .restart local v31    # "filedetailUri":Landroid/net/Uri;
    .restart local v33    # "fos":Ljava/io/FileOutputStream;
    .restart local v42    # "inpStream":Ljava/io/InputStream;
    :catch_1b
    move-exception v18

    move-object/from16 v32, v33

    .end local v33    # "fos":Ljava/io/FileOutputStream;
    .restart local v32    # "fos":Ljava/io/FileOutputStream;
    move-object/from16 v41, v42

    .end local v42    # "inpStream":Ljava/io/InputStream;
    .restart local v41    # "inpStream":Ljava/io/InputStream;
    goto/16 :goto_e

    .line 797
    .end local v10    # "buffer":[B
    .end local v32    # "fos":Ljava/io/FileOutputStream;
    .end local v41    # "inpStream":Ljava/io/InputStream;
    .end local v61    # "tempFile":Ljava/io/File;
    .restart local v33    # "fos":Ljava/io/FileOutputStream;
    :catchall_4
    move-exception v2

    move-object/from16 v32, v33

    .end local v33    # "fos":Ljava/io/FileOutputStream;
    .restart local v32    # "fos":Ljava/io/FileOutputStream;
    goto/16 :goto_c

    .line 791
    .end local v32    # "fos":Ljava/io/FileOutputStream;
    .restart local v33    # "fos":Ljava/io/FileOutputStream;
    :catch_1c
    move-exception v18

    move-object/from16 v32, v33

    .end local v33    # "fos":Ljava/io/FileOutputStream;
    .restart local v32    # "fos":Ljava/io/FileOutputStream;
    goto/16 :goto_b

    .line 786
    .end local v32    # "fos":Ljava/io/FileOutputStream;
    .restart local v33    # "fos":Ljava/io/FileOutputStream;
    :catch_1d
    move-exception v18

    move-object/from16 v32, v33

    .end local v33    # "fos":Ljava/io/FileOutputStream;
    .restart local v32    # "fos":Ljava/io/FileOutputStream;
    goto/16 :goto_a

    .line 781
    .end local v32    # "fos":Ljava/io/FileOutputStream;
    .restart local v33    # "fos":Ljava/io/FileOutputStream;
    :catch_1e
    move-exception v18

    move-object/from16 v32, v33

    .end local v33    # "fos":Ljava/io/FileOutputStream;
    .restart local v32    # "fos":Ljava/io/FileOutputStream;
    goto/16 :goto_9
.end method

.method public updatetoServer(Ljava/util/List;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/sCloudSync/Records/RecordSetItem;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/sCloudSync/Records/RecordBase;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;,
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 379
    .local p1, "setList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordSetItem;>;"
    .local p2, "deleteList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/sCloudSync/Records/RecordBase;>;"
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mounted"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 380
    new-instance v0, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v0

    .line 383
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/sCloudSync/SyncAdapters/SNote3SyncAdapter;->handleSetList(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 387
    :goto_0
    return-void

    .line 386
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->updatetoServer(Ljava/util/List;Ljava/util/List;)V

    goto :goto_0
.end method
