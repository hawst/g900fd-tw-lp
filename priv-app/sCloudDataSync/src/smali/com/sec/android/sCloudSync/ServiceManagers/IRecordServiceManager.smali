.class public interface abstract Lcom/sec/android/sCloudSync/ServiceManagers/IRecordServiceManager;
.super Ljava/lang/Object;
.source "IRecordServiceManager.java"

# interfaces
.implements Lcom/sec/android/sCloudSync/ServiceManagers/IPDMServiceApiHelper;


# virtual methods
.method public abstract close()V
.end method

.method public abstract deleteItems(Ljava/util/List;)Lcom/sec/android/sCloudSync/Records/KVSResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/sCloudSync/Records/RecordBase;",
            ">;)",
            "Lcom/sec/android/sCloudSync/Records/KVSResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation
.end method

.method public abstract getItems(Ljava/util/List;)Lcom/sec/android/sCloudSync/Records/KVSResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/sec/android/sCloudSync/Records/KVSResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation
.end method

.method public abstract getKeys(Ljava/lang/String;I)Lcom/sec/android/sCloudSync/Records/KVSResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation
.end method

.method public abstract getServerTimeStamp()J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation
.end method

.method public abstract getUpdates(Ljava/lang/String;Ljava/lang/String;I)Lcom/sec/android/sCloudSync/Records/KVSResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation
.end method

.method public abstract setItems(Ljava/util/List;)Lcom/sec/android/sCloudSync/Records/KVSResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/sCloudSync/Records/RecordSetItem;",
            ">;)",
            "Lcom/sec/android/sCloudSync/Records/KVSResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation
.end method
