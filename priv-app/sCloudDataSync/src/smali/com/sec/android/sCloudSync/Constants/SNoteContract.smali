.class public Lcom/sec/android/sCloudSync/Constants/SNoteContract;
.super Ljava/lang/Object;
.source "SNoteContract.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/sCloudSync/Constants/SNoteContract$FileDetail;,
        Lcom/sec/android/sCloudSync/Constants/SNoteContract$Files;
    }
.end annotation


# static fields
.field public static AUTHORITY:Ljava/lang/String; = null

.field public static final AUTHORITY2_FOR_NOTE3:Ljava/lang/String; = "com.samsung.android.provider.snote2"

.field public static final AUTHORITY_SNOTE:Ljava/lang/String; = "com.infraware.provider.SNoteProvider"

.field public static CALLER_IS_SYNCADAPTER:Ljava/lang/String; = null

.field private static INSTANCE:Lcom/sec/android/sCloudSync/Constants/SNoteContract; = null

.field private static final PACKAGE_SNOTE:Ljava/lang/String; = "com.sec.android.provider.snote"


# instance fields
.field public APP_INFO_URI:Landroid/net/Uri;

.field public CREATESNB_CONTENT_URI:Landroid/net/Uri;

.field public DELETEALLSNB_CONTENT_URI:Landroid/net/Uri;

.field public DELETEDETAIL_CONTENT_URI:Landroid/net/Uri;

.field public DELETESNB_CONTENT_URI:Landroid/net/Uri;

.field public EXTRACTSNB_CONTENT_URI:Landroid/net/Uri;

.field public FILEDETAIL_CONTENT_URI:Landroid/net/Uri;

.field public FILE_CONTENT_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->AUTHORITY:Ljava/lang/String;

    .line 40
    const-string v0, "caller_is_syncadapter"

    sput-object v0, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->CALLER_IS_SYNCADAPTER:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object v2, p0, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->FILE_CONTENT_URI:Landroid/net/Uri;

    .line 43
    iput-object v2, p0, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->APP_INFO_URI:Landroid/net/Uri;

    .line 44
    iput-object v2, p0, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->FILEDETAIL_CONTENT_URI:Landroid/net/Uri;

    .line 45
    iput-object v2, p0, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->EXTRACTSNB_CONTENT_URI:Landroid/net/Uri;

    .line 46
    iput-object v2, p0, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->DELETESNB_CONTENT_URI:Landroid/net/Uri;

    .line 47
    iput-object v2, p0, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->CREATESNB_CONTENT_URI:Landroid/net/Uri;

    .line 48
    iput-object v2, p0, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->DELETEDETAIL_CONTENT_URI:Landroid/net/Uri;

    .line 49
    iput-object v2, p0, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->DELETEALLSNB_CONTENT_URI:Landroid/net/Uri;

    .line 54
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 56
    .local v1, "packageManager":Landroid/content/pm/PackageManager;
    :try_start_0
    const-string v2, "com.sec.android.provider.snote"

    const/16 v3, 0x80

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    .line 57
    const-string v2, "com.infraware.provider.SNoteProvider"

    sput-object v2, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->AUTHORITY:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 62
    :goto_0
    const-string v2, "SNoteContract"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Set SNote Authority : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->AUTHORITY:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/sCloudSync/Util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "content://"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->AUTHORITY:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/fileMgr"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->FILE_CONTENT_URI:Landroid/net/Uri;

    .line 65
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "content://"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->AUTHORITY:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/appInfo"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->APP_INFO_URI:Landroid/net/Uri;

    .line 66
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "content://"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->AUTHORITY:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/fileMgrDetail"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->FILEDETAIL_CONTENT_URI:Landroid/net/Uri;

    .line 67
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "content://"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->AUTHORITY:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/openSNB"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->EXTRACTSNB_CONTENT_URI:Landroid/net/Uri;

    .line 68
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "content://"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->AUTHORITY:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/deleteTMP"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->DELETESNB_CONTENT_URI:Landroid/net/Uri;

    .line 69
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "content://"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->AUTHORITY:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/createSNB"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->CREATESNB_CONTENT_URI:Landroid/net/Uri;

    .line 70
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "content://"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->AUTHORITY:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/deleteDETAIL"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->DELETEDETAIL_CONTENT_URI:Landroid/net/Uri;

    .line 71
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "content://"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->AUTHORITY:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/deleteTMPALL"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->DELETEALLSNB_CONTENT_URI:Landroid/net/Uri;

    .line 72
    return-void

    .line 59
    :catch_0
    move-exception v0

    .line 60
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v2, "com.samsung.android.provider.snote2"

    sput-object v2, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->AUTHORITY:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/sec/android/sCloudSync/Constants/SNoteContract;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 75
    const-class v1, Lcom/sec/android/sCloudSync/Constants/SNoteContract;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->INSTANCE:Lcom/sec/android/sCloudSync/Constants/SNoteContract;

    if-nez v0, :cond_0

    .line 76
    new-instance v0, Lcom/sec/android/sCloudSync/Constants/SNoteContract;

    invoke-direct {v0, p0}, Lcom/sec/android/sCloudSync/Constants/SNoteContract;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->INSTANCE:Lcom/sec/android/sCloudSync/Constants/SNoteContract;

    .line 77
    :cond_0
    sget-object v0, Lcom/sec/android/sCloudSync/Constants/SNoteContract;->INSTANCE:Lcom/sec/android/sCloudSync/Constants/SNoteContract;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 75
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
