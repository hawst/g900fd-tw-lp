.class public abstract Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;
.super Ljava/lang/Object;
.source "AbstractBuilder.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "AbstractBuilder"


# instance fields
.field protected mAccount:Landroid/accounts/Account;

.field protected mContext:Landroid/content/Context;

.field protected mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

.field protected mProvider:Landroid/content/ContentProviderClient;


# direct methods
.method public constructor <init>(Landroid/content/ContentProviderClient;Landroid/accounts/Account;Landroid/content/Context;)V
    .locals 1
    .param p1, "provider"    # Landroid/content/ContentProviderClient;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object v0, p0, Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;->mProvider:Landroid/content/ContentProviderClient;

    .line 39
    iput-object v0, p0, Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;->mContext:Landroid/content/Context;

    .line 40
    iput-object v0, p0, Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    .line 43
    iput-object p1, p0, Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;->mProvider:Landroid/content/ContentProviderClient;

    .line 44
    iput-object p2, p0, Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;->mAccount:Landroid/accounts/Account;

    .line 45
    iput-object p3, p0, Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;->mContext:Landroid/content/Context;

    .line 46
    new-instance v0, Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-direct {v0}, Lcom/sec/android/sCloudSync/Records/JSONParser;-><init>()V

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    .line 47
    return-void
.end method


# virtual methods
.method public delete(Landroid/net/Uri;JLjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "rowId"    # J
    .param p4, "selection"    # Ljava/lang/String;
    .param p5, "syncKey"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;->mProvider:Landroid/content/ContentProviderClient;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p4, v1}, Landroid/content/ContentProviderClient;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 86
    return-void
.end method

.method public abstract doApplyBatch()Z
.end method

.method protected doApplyBatch(Ljava/util/ArrayList;)Z
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 59
    .local p1, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    const/4 v4, 0x0

    .line 60
    .local v4, "start":I
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 61
    .local v3, "size":I
    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;->getBatchSize()I

    move-result v2

    .line 62
    .local v2, "maxBatchSize":I
    const/4 v1, 0x0

    .line 64
    .local v1, "end":I
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 65
    .local v5, "subOperations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :goto_0
    if-le v3, v4, :cond_1

    .line 66
    add-int v1, v4, v2

    .line 67
    if-ge v3, v1, :cond_0

    .line 68
    move v1, v3

    .line 70
    :cond_0
    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 71
    invoke-virtual {p1, v4, v1}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 73
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;->mProvider:Landroid/content/ContentProviderClient;

    invoke-virtual {v6, v5}, Landroid/content/ContentProviderClient;->applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 74
    move v4, v1

    goto :goto_0

    .line 75
    :catch_0
    move-exception v0

    .line 76
    .local v0, "e":Ljava/lang/Exception;
    const-string v6, "AbstractBuilder"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Exception received : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    const/4 v6, 0x0

    .line 80
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    return v6

    :cond_1
    const/4 v6, 0x1

    goto :goto_1
.end method

.method public getBatchSize()I
    .locals 1

    .prologue
    .line 54
    const/16 v0, 0xfa

    return v0
.end method

.method public abstract insert(Ljava/lang/String;Ljava/lang/String;J)Z
.end method

.method public abstract parse(Landroid/database/Cursor;JLjava/lang/String;)Ljava/lang/String;
.end method

.method public abstract update(Ljava/lang/String;JJLjava/lang/String;)Z
.end method
