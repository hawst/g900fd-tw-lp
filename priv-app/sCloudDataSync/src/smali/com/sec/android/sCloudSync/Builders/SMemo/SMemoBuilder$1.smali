.class Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder$1;
.super Ljava/lang/Object;
.source "SMemoBuilder.java"

# interfaces
.implements Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder$PostOperationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;->insertExtData(Lorg/json/JSONObject;Ljava/util/ArrayList;Ljava/util/ArrayList;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;

.field final synthetic val$operations:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 425
    iput-object p1, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder$1;->this$0:Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder;

    iput-object p2, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder$1;->val$operations:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public doPost(Landroid/content/ContentValues;)V
    .locals 4
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 429
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SMemo/SMemoBuilder$1;->val$operations:Ljava/util/ArrayList;

    sget-object v1, Lcom/sec/android/sCloudSync/Constants/SMemoContract;->SMEMO_EXTDATA_URI:Landroid/net/Uri;

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v2, "MemoID"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 432
    return-void
.end method
