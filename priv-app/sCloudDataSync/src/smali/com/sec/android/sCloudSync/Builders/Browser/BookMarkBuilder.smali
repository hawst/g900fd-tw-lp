.class public Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;
.super Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;
.source "BookMarkBuilder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder$FOLDER;
    }
.end annotation


# static fields
.field private static final ACCOUNT_SELECTION:Ljava/lang/String; = "account_type= \'com.osp.app.signin\'"

.field private static final SBOOKMARK_COLUMNS:[Ljava/lang/String;

.field private static final STOCKBOOKMARK_COLUMNS:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "BookMarkBuilder"

.field private static bookmarksColumns:[Ljava/lang/String;

.field public static mAuthorityUri:Landroid/net/Uri;

.field public static mBookmarkUri:Landroid/net/Uri;

.field public static mContentUri:Landroid/net/Uri;


# instance fields
.field private mBookmarkId:J

.field private mFolderMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 49
    sput-object v0, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->mContentUri:Landroid/net/Uri;

    .line 50
    sput-object v0, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->mAuthorityUri:Landroid/net/Uri;

    .line 54
    sput-object v0, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->mBookmarkUri:Landroid/net/Uri;

    .line 68
    const/16 v0, 0x11

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "title"

    aput-object v1, v0, v3

    const-string v1, "url"

    aput-object v1, v0, v4

    const-string v1, "favicon"

    aput-object v1, v0, v5

    const-string v1, "thumbnail"

    aput-object v1, v0, v6

    const/4 v1, 0x4

    const-string v2, "touch_icon"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "folder"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "parent"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "position"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "insert_after"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "account_name"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "account_type"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "sourceid"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "version"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "created"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "modified"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "sync3"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "sync4"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->STOCKBOOKMARK_COLUMNS:[Ljava/lang/String;

    .line 89
    const/16 v0, 0x11

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "title"

    aput-object v1, v0, v3

    const-string v1, "url"

    aput-object v1, v0, v4

    const-string v1, "favicon"

    aput-object v1, v0, v5

    const-string v1, "parent"

    aput-object v1, v0, v6

    const/4 v1, 0x4

    const-string v2, "folder"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "position"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "insert_after"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "account_name"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "account_type"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "sourceid"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "device_name"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "device_id"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "created"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "modified"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "sync3"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "sync4"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "tags"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->SBOOKMARK_COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentProviderClient;Landroid/accounts/Account;Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p1, "provider"    # Landroid/content/ContentProviderClient;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "authority"    # Ljava/lang/String;

    .prologue
    const-wide/16 v1, 0x0

    .line 111
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;-><init>(Landroid/content/ContentProviderClient;Landroid/accounts/Account;Landroid/content/Context;)V

    .line 47
    iput-wide v1, p0, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->mBookmarkId:J

    .line 48
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->mFolderMap:Ljava/util/HashMap;

    .line 112
    const-string v0, "com.android.browser"

    invoke-virtual {p4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 114
    sget-object v0, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->STOCKBOOKMARK_COLUMNS:[Ljava/lang/String;

    sput-object v0, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->bookmarksColumns:[Ljava/lang/String;

    .line 115
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->mAccount:Landroid/accounts/Account;

    iget-object v1, p0, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->mContext:Landroid/content/Context;

    sget-object v2, Lcom/sec/android/sCloudSync/Constants/BrowserContract$Bookmarks;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, v1, v2}, Lcom/sec/android/sCloudSync/Util/SyncAccountManager;->addSamsungBookmark(Landroid/accounts/Account;Landroid/content/Context;Landroid/net/Uri;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->mBookmarkId:J

    .line 123
    :cond_0
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "content://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->mAuthorityUri:Landroid/net/Uri;

    .line 124
    sget-object v0, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->mAuthorityUri:Landroid/net/Uri;

    const-string v1, "bookmarks"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->mContentUri:Landroid/net/Uri;

    .line 125
    sget-object v0, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->mContentUri:Landroid/net/Uri;

    const-string v1, "caller_is_syncadapter"

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->mBookmarkUri:Landroid/net/Uri;

    .line 126
    invoke-direct {p0}, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->getFolderList()V

    .line 127
    return-void

    .line 117
    :cond_1
    const-string v0, "com.sec.android.app.sbrowser"

    invoke-virtual {p4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "com.sec.android.app.sbrowser.browser"

    invoke-virtual {v0, p4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    :cond_2
    sget-object v0, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->SBOOKMARK_COLUMNS:[Ljava/lang/String;

    sput-object v0, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->bookmarksColumns:[Ljava/lang/String;

    .line 121
    iput-wide v1, p0, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->mBookmarkId:J

    goto :goto_0
.end method

.method private getFolderList()V
    .locals 8

    .prologue
    .line 130
    const/4 v6, 0x0

    .line 132
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->mProvider:Landroid/content/ContentProviderClient;

    sget-object v1, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->mContentUri:Landroid/net/Uri;

    # getter for: Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder$FOLDER;->PROJECTION:[Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder$FOLDER;->access$000()[Ljava/lang/String;

    move-result-object v2

    const-string v3, "folder= 1 AND account_type= \'com.osp.app.signin\'"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 137
    if-nez v6, :cond_0

    .line 145
    :goto_0
    return-void

    .line 133
    :catch_0
    move-exception v7

    .line 134
    .local v7, "e":Landroid/os/RemoteException;
    const-string v0, "BookMarkBuilder"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getFolderList() :RemoteException"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 140
    .end local v7    # "e":Landroid/os/RemoteException;
    :cond_0
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->mFolderMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 141
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 142
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->mFolderMap:Ljava/util/HashMap;

    const/4 v1, 0x1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 144
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method


# virtual methods
.method public doApplyBatch()Z
    .locals 1

    .prologue
    .line 293
    const/4 v0, 0x0

    return v0
.end method

.method public insert(Ljava/lang/String;Ljava/lang/String;J)Z
    .locals 10
    .param p1, "jsonString"    # Ljava/lang/String;
    .param p2, "syncKey"    # Ljava/lang/String;
    .param p3, "timeStamp"    # J

    .prologue
    .line 237
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 238
    .local v0, "bookmarkValue":Landroid/content/ContentValues;
    iget-object v7, p0, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    sget-object v8, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->bookmarksColumns:[Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 240
    :try_start_0
    iget-object v7, p0, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v7, p1}, Lcom/sec/android/sCloudSync/Records/JSONParser;->fromJSONString(Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v0

    .line 241
    const-string v7, "thumbnail"

    invoke-virtual {v0, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 242
    .local v5, "thumbnaildata":Ljava/lang/String;
    if-eqz v5, :cond_0

    .line 244
    const/4 v7, 0x0

    invoke-static {v5, v7}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    .line 245
    .local v1, "buffer":[B
    const-string v7, "thumbnail"

    invoke-virtual {v0, v7, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 247
    .end local v1    # "buffer":[B
    :cond_0
    const-string v7, "favicon"

    invoke-virtual {v0, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 248
    .local v3, "favIcon":Ljava/lang/String;
    if-eqz v3, :cond_1

    .line 250
    const/4 v7, 0x0

    invoke-static {v3, v7}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    .line 251
    .restart local v1    # "buffer":[B
    const-string v7, "favicon"

    invoke-virtual {v0, v7, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 253
    .end local v1    # "buffer":[B
    :cond_1
    const-string v7, "touch_icon"

    invoke-virtual {v0, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 254
    .local v6, "touchIcon":Ljava/lang/String;
    if-eqz v6, :cond_2

    .line 256
    const/4 v7, 0x0

    invoke-static {v6, v7}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    .line 257
    .restart local v1    # "buffer":[B
    const-string v7, "touch_icon"

    invoke-virtual {v0, v7, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 259
    .end local v1    # "buffer":[B
    :cond_2
    const-string v7, "sync1"

    invoke-virtual {v0, v7, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    const-string v7, "sync5"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 261
    const-string v7, "sync3"

    invoke-virtual {v0, v7}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 262
    const-string v7, "sync3"

    invoke-virtual {v0, v7}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 263
    .local v4, "sync3":Ljava/lang/String;
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 264
    :cond_3
    const-string v7, "parent"

    iget-wide v8, p0, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->mBookmarkId:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 278
    .end local v4    # "sync3":Ljava/lang/String;
    :cond_4
    :goto_0
    iget-object v7, p0, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 282
    :try_start_1
    iget-object v7, p0, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->mProvider:Landroid/content/ContentProviderClient;

    sget-object v8, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->mBookmarkUri:Landroid/net/Uri;

    invoke-virtual {v7, v8, v0}, Landroid/content/ContentProviderClient;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 287
    const/4 v7, 0x1

    .end local v3    # "favIcon":Ljava/lang/String;
    .end local v5    # "thumbnaildata":Ljava/lang/String;
    .end local v6    # "touchIcon":Ljava/lang/String;
    :goto_1
    return v7

    .line 265
    .restart local v3    # "favIcon":Ljava/lang/String;
    .restart local v4    # "sync3":Ljava/lang/String;
    .restart local v5    # "thumbnaildata":Ljava/lang/String;
    .restart local v6    # "touchIcon":Ljava/lang/String;
    :cond_5
    :try_start_2
    const-string v7, "bookmark_bar"

    const-string v8, "sync3"

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 267
    invoke-virtual {v0}, Landroid/content/ContentValues;->clear()V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 268
    const/4 v7, 0x1

    .line 278
    iget-object v8, p0, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    goto :goto_1

    .line 271
    .end local v4    # "sync3":Ljava/lang/String;
    :cond_6
    :try_start_3
    const-string v7, "parent"

    iget-wide v8, p0, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->mBookmarkId:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 273
    .end local v3    # "favIcon":Ljava/lang/String;
    .end local v5    # "thumbnaildata":Ljava/lang/String;
    .end local v6    # "touchIcon":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 274
    .local v2, "e":Lorg/json/JSONException;
    :try_start_4
    const-string v7, "BookMarkBuilder"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Unable to Parse;"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    invoke-virtual {v0}, Landroid/content/ContentValues;->clear()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 276
    const/4 v7, 0x0

    .line 278
    iget-object v8, p0, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    goto :goto_1

    .end local v2    # "e":Lorg/json/JSONException;
    :catchall_0
    move-exception v7

    iget-object v8, p0, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    throw v7

    .line 283
    .restart local v3    # "favIcon":Ljava/lang/String;
    .restart local v5    # "thumbnaildata":Ljava/lang/String;
    .restart local v6    # "touchIcon":Ljava/lang/String;
    :catch_1
    move-exception v2

    .line 284
    .local v2, "e":Landroid/os/RemoteException;
    const-string v7, "BookMarkBuilder"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "RemoteException"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    const/4 v7, 0x0

    goto :goto_1
.end method

.method public parse(Landroid/database/Cursor;JLjava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p1, "bookMarkCursor"    # Landroid/database/Cursor;
    .param p2, "rowId"    # J
    .param p4, "key"    # Ljava/lang/String;

    .prologue
    .line 149
    const/4 v1, 0x0

    .line 150
    .local v1, "bookmarkString":Ljava/lang/String;
    if-nez p1, :cond_0

    .line 151
    const/4 v7, 0x0

    .line 182
    :goto_0
    return-object v7

    .line 153
    :cond_0
    iget-object v7, p0, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    sget-object v8, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->bookmarksColumns:[Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 155
    :try_start_0
    iget-object v7, p0, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v7, p1}, Lcom/sec/android/sCloudSync/Records/JSONParser;->toJSON(Landroid/database/Cursor;)Lorg/json/JSONObject;

    move-result-object v7

    invoke-virtual {v7}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    .line 156
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 158
    .local v0, "BookmarkJson":Lorg/json/JSONObject;
    const-string v7, "parent"

    invoke-virtual {v0, v7}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 159
    .local v4, "parentId":J
    iget-wide v7, p0, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->mBookmarkId:J

    cmp-long v7, v4, v7

    if-eqz v7, :cond_1

    .line 160
    const-string v7, "sync3"

    invoke-virtual {v0, v7}, Lorg/json/JSONObject;->remove(Ljava/lang/String;)Ljava/lang/Object;

    .line 161
    const-string v7, "sync3"

    iget-object v8, p0, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->mFolderMap:Ljava/util/HashMap;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 167
    :goto_1
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 168
    .local v6, "values":Landroid/content/ContentValues;
    const-string v8, "sync3"

    iget-object v7, p0, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->mFolderMap:Ljava/util/HashMap;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v6, v8, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 170
    :try_start_1
    iget-object v7, p0, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->mProvider:Landroid/content/ContentProviderClient;

    sget-object v8, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->mBookmarkUri:Landroid/net/Uri;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "_id = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v7, v8, v6, v9, v10}, Landroid/content/ContentProviderClient;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 171
    .local v2, "count":I
    const-string v7, "BookMarkBuilder"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "updated bookmark with new  sync3 "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/sCloudSync/Util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 176
    .end local v2    # "count":I
    :goto_2
    :try_start_2
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v1

    .line 180
    iget-object v7, p0, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .end local v0    # "BookmarkJson":Lorg/json/JSONObject;
    .end local v4    # "parentId":J
    .end local v6    # "values":Landroid/content/ContentValues;
    :goto_3
    move-object v7, v1

    .line 182
    goto/16 :goto_0

    .line 163
    .restart local v0    # "BookmarkJson":Lorg/json/JSONObject;
    .restart local v4    # "parentId":J
    :cond_1
    :try_start_3
    const-string v7, "sync3"

    const-string v8, ""

    invoke-virtual {v0, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 177
    .end local v0    # "BookmarkJson":Lorg/json/JSONObject;
    .end local v4    # "parentId":J
    :catch_0
    move-exception v3

    .line 178
    .local v3, "e":Lorg/json/JSONException;
    :try_start_4
    const-string v7, "BookMarkBuilder"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "parse():Exception in parsing"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 180
    iget-object v7, p0, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    goto :goto_3

    .line 172
    .end local v3    # "e":Lorg/json/JSONException;
    .restart local v0    # "BookmarkJson":Lorg/json/JSONObject;
    .restart local v4    # "parentId":J
    .restart local v6    # "values":Landroid/content/ContentValues;
    :catch_1
    move-exception v3

    .line 173
    .local v3, "e":Landroid/os/RemoteException;
    :try_start_5
    const-string v7, "BookMarkBuilder"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Error while trying to update sync3 "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    .line 180
    .end local v0    # "BookmarkJson":Lorg/json/JSONObject;
    .end local v3    # "e":Landroid/os/RemoteException;
    .end local v4    # "parentId":J
    .end local v6    # "values":Landroid/content/ContentValues;
    :catchall_0
    move-exception v7

    iget-object v8, p0, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    throw v7
.end method

.method public update(Ljava/lang/String;JJLjava/lang/String;)Z
    .locals 13
    .param p1, "jsonString"    # Ljava/lang/String;
    .param p2, "timeStamp"    # J
    .param p4, "rowId"    # J
    .param p6, "syncKey"    # Ljava/lang/String;

    .prologue
    .line 187
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 188
    .local v2, "bookmarkValue":Landroid/content/ContentValues;
    iget-object v9, p0, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    sget-object v10, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->bookmarksColumns:[Ljava/lang/String;

    invoke-virtual {v9, v10}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 190
    :try_start_0
    iget-object v9, p0, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v9, p1}, Lcom/sec/android/sCloudSync/Records/JSONParser;->fromJSONString(Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v2

    .line 191
    const-string v9, "thumbnail"

    invoke-virtual {v2, v9}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 192
    .local v7, "thumbnaildata":Ljava/lang/String;
    if-eqz v7, :cond_0

    .line 194
    const/4 v9, 0x0

    invoke-static {v7, v9}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v3

    .line 195
    .local v3, "buffer":[B
    const-string v9, "thumbnail"

    invoke-virtual {v2, v9, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 197
    .end local v3    # "buffer":[B
    :cond_0
    const-string v9, "favicon"

    invoke-virtual {v2, v9}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 198
    .local v5, "favIcon":Ljava/lang/String;
    if-eqz v5, :cond_1

    .line 200
    const/4 v9, 0x0

    invoke-static {v5, v9}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v3

    .line 201
    .restart local v3    # "buffer":[B
    const-string v9, "favicon"

    invoke-virtual {v2, v9, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 203
    .end local v3    # "buffer":[B
    :cond_1
    const-string v9, "touch_icon"

    invoke-virtual {v2, v9}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 204
    .local v8, "touchIcon":Ljava/lang/String;
    if-eqz v8, :cond_2

    .line 206
    const/4 v9, 0x0

    invoke-static {v8, v9}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v3

    .line 207
    .restart local v3    # "buffer":[B
    const-string v9, "touch_icon"

    invoke-virtual {v2, v9, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 209
    .end local v3    # "buffer":[B
    :cond_2
    const-string v9, "sync5"

    invoke-static/range {p2 .. p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v2, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 210
    const-string v9, "dirty"

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v2, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 211
    const-string v9, "deleted"

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v2, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 212
    const-string v9, "parent"

    invoke-virtual {v2, v9}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 213
    const-string v9, "sync3"

    invoke-virtual {v2, v9}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 214
    const-string v9, "sync3"

    invoke-virtual {v2, v9}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 215
    .local v6, "sync3":Ljava/lang/String;
    if-eqz v6, :cond_3

    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 216
    :cond_3
    const-string v9, "parent"

    iget-wide v10, p0, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->mBookmarkId:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v2, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 223
    .end local v6    # "sync3":Ljava/lang/String;
    :cond_4
    iget-object v9, p0, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 227
    :try_start_1
    iget-object v9, p0, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->mProvider:Landroid/content/ContentProviderClient;

    sget-object v10, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->mBookmarkUri:Landroid/net/Uri;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "_id="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-wide/from16 v0, p4

    invoke-virtual {v11, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v9, v10, v2, v11, v12}, Landroid/content/ContentProviderClient;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 232
    const/4 v9, 0x1

    .end local v5    # "favIcon":Ljava/lang/String;
    .end local v7    # "thumbnaildata":Ljava/lang/String;
    .end local v8    # "touchIcon":Ljava/lang/String;
    :goto_0
    return v9

    .line 218
    :catch_0
    move-exception v4

    .line 219
    .local v4, "e":Lorg/json/JSONException;
    :try_start_2
    const-string v9, "BookMarkBuilder"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Unable to Parse;"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v4}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    invoke-virtual {v2}, Landroid/content/ContentValues;->clear()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 221
    const/4 v9, 0x0

    .line 223
    iget-object v10, p0, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    goto :goto_0

    .end local v4    # "e":Lorg/json/JSONException;
    :catchall_0
    move-exception v9

    iget-object v10, p0, Lcom/sec/android/sCloudSync/Builders/Browser/BookMarkBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    throw v9

    .line 228
    .restart local v5    # "favIcon":Ljava/lang/String;
    .restart local v7    # "thumbnaildata":Ljava/lang/String;
    .restart local v8    # "touchIcon":Ljava/lang/String;
    :catch_1
    move-exception v4

    .line 229
    .local v4, "e":Landroid/os/RemoteException;
    const-string v9, "BookMarkBuilder"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "RemoteException"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v4}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    const/4 v9, 0x0

    goto :goto_0
.end method
