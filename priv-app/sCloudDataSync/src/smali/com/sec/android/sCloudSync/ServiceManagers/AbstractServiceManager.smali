.class public abstract Lcom/sec/android/sCloudSync/ServiceManagers/AbstractServiceManager;
.super Ljava/lang/Object;
.source "AbstractServiceManager.java"


# static fields
.field private static final RCODE:Ljava/lang/String; = "rcode"

.field private static final TAG:Ljava/lang/String; = "AbstractServiceManager"


# instance fields
.field protected mCid:Ljava/lang/String;

.field protected mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

.field protected mContext:Landroid/content/Context;

.field protected mNetClient:Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;

.field protected mSycnCancled:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cid"    # Ljava/lang/String;
    .param p3, "cloud"    # Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    .prologue
    const/4 v0, 0x0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object v0, p0, Lcom/sec/android/sCloudSync/ServiceManagers/AbstractServiceManager;->mCid:Ljava/lang/String;

    .line 46
    iput-object v0, p0, Lcom/sec/android/sCloudSync/ServiceManagers/AbstractServiceManager;->mNetClient:Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;

    .line 54
    iput-object p2, p0, Lcom/sec/android/sCloudSync/ServiceManagers/AbstractServiceManager;->mCid:Ljava/lang/String;

    .line 55
    new-instance v0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;

    invoke-direct {v0}, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;-><init>()V

    iput-object v0, p0, Lcom/sec/android/sCloudSync/ServiceManagers/AbstractServiceManager;->mNetClient:Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;

    .line 56
    iput-object p1, p0, Lcom/sec/android/sCloudSync/ServiceManagers/AbstractServiceManager;->mContext:Landroid/content/Context;

    .line 57
    iput-object p3, p0, Lcom/sec/android/sCloudSync/ServiceManagers/AbstractServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    .line 58
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/sCloudSync/ServiceManagers/AbstractServiceManager;->mSycnCancled:Z

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cid"    # Ljava/lang/String;
    .param p3, "clientDeviceId"    # Ljava/lang/String;
    .param p4, "cloud"    # Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    .prologue
    const/4 v0, 0x0

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object v0, p0, Lcom/sec/android/sCloudSync/ServiceManagers/AbstractServiceManager;->mCid:Ljava/lang/String;

    .line 46
    iput-object v0, p0, Lcom/sec/android/sCloudSync/ServiceManagers/AbstractServiceManager;->mNetClient:Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;

    .line 62
    iput-object p2, p0, Lcom/sec/android/sCloudSync/ServiceManagers/AbstractServiceManager;->mCid:Ljava/lang/String;

    .line 63
    new-instance v0, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;

    invoke-direct {v0}, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;-><init>()V

    iput-object v0, p0, Lcom/sec/android/sCloudSync/ServiceManagers/AbstractServiceManager;->mNetClient:Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;

    .line 65
    iput-object p1, p0, Lcom/sec/android/sCloudSync/ServiceManagers/AbstractServiceManager;->mContext:Landroid/content/Context;

    .line 66
    iput-object p4, p0, Lcom/sec/android/sCloudSync/ServiceManagers/AbstractServiceManager;->mCloudServiceManager:Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;

    .line 67
    return-void
.end method

.method private createMetaResponse(Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;Lcom/sec/android/sCloudSync/Records/AbstractJSON;)V
    .locals 3
    .param p1, "networkResponse"    # Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;
    .param p2, "metaResponse"    # Lcom/sec/android/sCloudSync/Records/AbstractJSON;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .line 77
    invoke-virtual {p2}, Lcom/sec/android/sCloudSync/Records/AbstractJSON;->clear()V

    .line 79
    :try_start_0
    invoke-virtual {p1}, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;->getBody()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 80
    invoke-virtual {p1}, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/sec/android/sCloudSync/Records/AbstractJSON;->fromJSON(Ljava/lang/Object;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    .line 88
    :cond_0
    return-void

    .line 81
    :catch_0
    move-exception v0

    .line 82
    .local v0, "e":Lorg/json/JSONException;
    const-string v1, "AbstractServiceManager"

    const-string v2, "JSON PARSER Exception"

    invoke-static {v1, v2}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    new-instance v1, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v1

    .line 85
    .end local v0    # "e":Lorg/json/JSONException;
    :catch_1
    move-exception v0

    .line 86
    .local v0, "e":Ljava/lang/OutOfMemoryError;
    new-instance v1, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/4 v2, 0x4

    invoke-direct {v1, v2}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v1
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/sCloudSync/ServiceManagers/AbstractServiceManager;->mNetClient:Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;

    invoke-virtual {v0}, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->close()V

    .line 72
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/sCloudSync/ServiceManagers/AbstractServiceManager;->mSycnCancled:Z

    .line 73
    return-void
.end method

.method protected handleRequest(ILjava/lang/String;Lcom/sec/android/sCloudSync/Records/AbstractJSON;I)Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;
    .locals 7
    .param p1, "requestType"    # I
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "responseMeta"    # Lcom/sec/android/sCloudSync/Records/AbstractJSON;
    .param p4, "apiCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 102
    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, v4

    move v6, p4

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/sCloudSync/ServiceManagers/AbstractServiceManager;->handleRequest(ILjava/lang/String;Lcom/sec/android/sCloudSync/Records/AbstractJSON;Ljava/lang/String;Lorg/apache/http/entity/mime/MultipartEntity;I)Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;

    move-result-object v0

    return-object v0
.end method

.method protected handleRequest(ILjava/lang/String;Lcom/sec/android/sCloudSync/Records/AbstractJSON;Ljava/lang/String;I)Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;
    .locals 7
    .param p1, "requestType"    # I
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "responseMeta"    # Lcom/sec/android/sCloudSync/Records/AbstractJSON;
    .param p4, "Json"    # Ljava/lang/String;
    .param p5, "apiCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .line 97
    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/sCloudSync/ServiceManagers/AbstractServiceManager;->handleRequest(ILjava/lang/String;Lcom/sec/android/sCloudSync/Records/AbstractJSON;Ljava/lang/String;Lorg/apache/http/entity/mime/MultipartEntity;I)Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;

    move-result-object v0

    return-object v0
.end method

.method protected handleRequest(ILjava/lang/String;Lcom/sec/android/sCloudSync/Records/AbstractJSON;Ljava/lang/String;Lorg/apache/http/entity/mime/MultipartEntity;I)Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;
    .locals 11
    .param p1, "requestType"    # I
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "responseMeta"    # Lcom/sec/android/sCloudSync/Records/AbstractJSON;
    .param p4, "Json"    # Ljava/lang/String;
    .param p5, "multipart"    # Lorg/apache/http/entity/mime/MultipartEntity;
    .param p6, "apiCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .line 108
    const/4 v3, 0x0

    .line 110
    .local v3, "networkResponse":Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;
    const-string v7, "AbstractServiceManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "URL : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    const-string v7, "AbstractServiceManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "API Code is "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move/from16 v0, p6

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    iget-object v8, p0, Lcom/sec/android/sCloudSync/ServiceManagers/AbstractServiceManager;->mNetClient:Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;

    monitor-enter v8

    .line 116
    const/4 v7, 0x0

    :try_start_0
    iput-boolean v7, p0, Lcom/sec/android/sCloudSync/ServiceManagers/AbstractServiceManager;->mSycnCancled:Z

    .line 117
    packed-switch p1, :pswitch_data_0

    .line 142
    :pswitch_0
    const-string v7, "AbstractServiceManager"

    const-string v9, "Wrong Request."

    invoke-static {v7, v9}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    new-instance v7, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/4 v9, 0x6

    invoke-direct {v7, v9}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v7
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 146
    :catch_0
    move-exception v1

    .line 147
    .local v1, "e":Ljava/io/IOException;
    :try_start_1
    const-string v7, "AbstractServiceManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "IOException occured while Http Request, caused by : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    new-instance v7, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/4 v9, 0x4

    invoke-direct {v7, v9, v1}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(ILjava/lang/Throwable;)V

    throw v7

    .line 200
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v7

    .line 120
    :pswitch_1
    if-eqz p4, :cond_0

    .line 121
    :try_start_2
    const-string v7, "AbstractServiceManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Getting Headers : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    :cond_0
    iget-object v7, p0, Lcom/sec/android/sCloudSync/ServiceManagers/AbstractServiceManager;->mNetClient:Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;

    invoke-virtual {v7, p2, p4}, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->post(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v3

    .line 150
    :goto_0
    if-nez v3, :cond_2

    .line 152
    :try_start_3
    const-string v7, "AbstractServiceManager"

    const-string v9, "There is no Network Response."

    invoke-static {v7, v9}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    iget-boolean v7, p0, Lcom/sec/android/sCloudSync/ServiceManagers/AbstractServiceManager;->mSycnCancled:Z

    if-eqz v7, :cond_1

    .line 154
    new-instance v7, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/16 v9, 0x9

    invoke-direct {v7, v9}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 126
    :pswitch_2
    :try_start_4
    iget-object v7, p0, Lcom/sec/android/sCloudSync/ServiceManagers/AbstractServiceManager;->mNetClient:Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;

    invoke-virtual {v7, p2}, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->get(Ljava/lang/String;)Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;

    move-result-object v3

    .line 127
    goto :goto_0

    .line 130
    :pswitch_3
    iget-object v7, p0, Lcom/sec/android/sCloudSync/ServiceManagers/AbstractServiceManager;->mNetClient:Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;

    move-object/from16 v0, p5

    invoke-virtual {v7, p2, v0}, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->postMultiPart(Ljava/lang/String;Lorg/apache/http/entity/mime/MultipartEntity;)Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;

    move-result-object v3

    .line 131
    goto :goto_0

    .line 134
    :pswitch_4
    iget-object v7, p0, Lcom/sec/android/sCloudSync/ServiceManagers/AbstractServiceManager;->mNetClient:Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;

    invoke-virtual {v7, p2}, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->getFile(Ljava/lang/String;)Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;

    move-result-object v3

    .line 135
    goto :goto_0

    .line 138
    :pswitch_5
    iget-object v7, p0, Lcom/sec/android/sCloudSync/ServiceManagers/AbstractServiceManager;->mNetClient:Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;

    const-string v9, "Accept"

    const-string v10, "application/json"

    invoke-virtual {v7, p2, v9, v10}, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkClient;->getWithHeader(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v3

    .line 139
    goto :goto_0

    .line 156
    :cond_1
    :try_start_5
    new-instance v7, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/4 v9, 0x1

    invoke-direct {v7, v9}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v7

    .line 159
    :cond_2
    invoke-virtual {v3}, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;->getBody()Ljava/lang/String;

    move-result-object v5

    .line 160
    .local v5, "responseBody":Ljava/lang/String;
    const-string v7, "AbstractServiceManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Response : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    invoke-virtual {v3}, Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;->getStatus()I

    move-result v6

    .line 163
    .local v6, "status":I
    sparse-switch v6, :sswitch_data_0

    .line 185
    :cond_3
    const-string v7, "AbstractServiceManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "HTTPEXCEPTION : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 186
    if-eqz v5, :cond_4

    .line 188
    :try_start_6
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v5}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 189
    .local v2, "json":Lorg/json/JSONObject;
    const-string v7, "rcode"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 190
    const-string v7, "rcode"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 191
    .local v4, "rcode":I
    const-string v7, "AbstractServiceManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "HTTPEXCEPTION, rcode is "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Lorg/json/JSONException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 198
    .end local v2    # "json":Lorg/json/JSONObject;
    .end local v4    # "rcode":I
    :cond_4
    :goto_1
    :try_start_7
    new-instance v7, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/4 v9, 0x1

    invoke-direct {v7, v9}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v7
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 169
    :sswitch_0
    if-eqz v5, :cond_3

    .line 171
    :try_start_8
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v5}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 172
    .restart local v2    # "json":Lorg/json/JSONObject;
    const-string v7, "rcode"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 173
    const-string v7, "rcode"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 174
    .restart local v4    # "rcode":I
    const/16 v7, 0x4a40

    if-ne v7, v4, :cond_3

    .line 175
    new-instance v7, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/4 v9, 0x7

    invoke-direct {v7, v9}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v7
    :try_end_8
    .catch Lorg/json/JSONException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 178
    .end local v2    # "json":Lorg/json/JSONObject;
    .end local v4    # "rcode":I
    :catch_1
    move-exception v1

    .line 179
    .local v1, "e":Lorg/json/JSONException;
    :try_start_9
    const-string v7, "AbstractServiceManager"

    const-string v9, "JSON PARSER Exception"

    invoke-static {v7, v9}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    new-instance v7, Lcom/sec/android/sCloudSync/Util/SyncException;

    const/4 v9, 0x2

    invoke-direct {v7, v9}, Lcom/sec/android/sCloudSync/Util/SyncException;-><init>(I)V

    throw v7

    .line 193
    .end local v1    # "e":Lorg/json/JSONException;
    :catch_2
    move-exception v1

    .line 194
    .restart local v1    # "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 195
    const-string v7, "AbstractServiceManager"

    const-string v9, "JSON PARSER Exception"

    invoke-static {v7, v9}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 200
    .end local v1    # "e":Lorg/json/JSONException;
    :sswitch_1
    monitor-exit v8
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 202
    invoke-direct {p0, v3, p3}, Lcom/sec/android/sCloudSync/ServiceManagers/AbstractServiceManager;->createMetaResponse(Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;Lcom/sec/android/sCloudSync/Records/AbstractJSON;)V

    .line 203
    return-object v3

    .line 117
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 163
    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_1
        0x190 -> :sswitch_0
    .end sparse-switch
.end method

.method protected handleRequest(ILjava/lang/String;Lcom/sec/android/sCloudSync/Records/AbstractJSON;Lorg/apache/http/entity/mime/MultipartEntity;I)Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;
    .locals 7
    .param p1, "requestType"    # I
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "responseMeta"    # Lcom/sec/android/sCloudSync/Records/AbstractJSON;
    .param p4, "multipart"    # Lorg/apache/http/entity/mime/MultipartEntity;
    .param p5, "apiCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sCloudSync/Util/SyncException;
        }
    .end annotation

    .prologue
    .line 92
    const/4 v4, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    move v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/sCloudSync/ServiceManagers/AbstractServiceManager;->handleRequest(ILjava/lang/String;Lcom/sec/android/sCloudSync/Records/AbstractJSON;Ljava/lang/String;Lorg/apache/http/entity/mime/MultipartEntity;I)Lcom/sec/android/sCloudSync/NetworkUtils/NetworkResponse;

    move-result-object v0

    return-object v0
.end method
