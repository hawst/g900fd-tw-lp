.class public Lcom/sec/android/sCloudSync/Util/SyncException;
.super Ljava/lang/Exception;
.source "SyncException.java"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private mExceptionCode:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "code"    # I

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    .line 28
    iput p1, p0, Lcom/sec/android/sCloudSync/Util/SyncException;->mExceptionCode:I

    .line 30
    return-void
.end method

.method public constructor <init>(ILjava/lang/Throwable;)V
    .locals 0
    .param p1, "code"    # I
    .param p2, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 33
    invoke-direct {p0, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/Throwable;)V

    .line 34
    iput p1, p0, Lcom/sec/android/sCloudSync/Util/SyncException;->mExceptionCode:I

    .line 35
    return-void
.end method


# virtual methods
.method public getmExceptionCode()I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/sec/android/sCloudSync/Util/SyncException;->mExceptionCode:I

    return v0
.end method
