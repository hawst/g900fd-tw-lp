.class public Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;
.super Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;
.source "SPlannerTaskBuilder.java"


# static fields
.field private static final REMINDERS:Ljava/lang/String; = "REMINDERS"

.field private static final REMINDER_COLUMNS:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "sPlannerTaskBuilder"

.field private static final TASK:Ljava/lang/String; = "TASK"

.field private static final TASK_COLUMNS:[Ljava/lang/String;


# instance fields
.field private mAccountKey:I

.field private mTasksGroupsUri:Landroid/net/Uri;

.field private mTasksReminderUri:Landroid/net/Uri;

.field private mTasksUri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 52
    const/16 v0, 0x27

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "accountName"

    aput-object v1, v0, v3

    const-string v1, "body"

    aput-object v1, v0, v4

    const-string v1, "body_size"

    aput-object v1, v0, v5

    const-string v1, "body_truncated"

    aput-object v1, v0, v6

    const-string v1, "bodyType"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "category1"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "category2"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "category3"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "clientId"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "complete"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "date_completed"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "displayName"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "due_date"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "groupId"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "importance"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "mailboxKey"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "parentId"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "previousId"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "read"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "recurrence_day_of_month"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "recurrence_day_of_week"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "recurrence_dead_occur"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "recurrence_interval"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "recurrence_month_of_year"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "recurrence_occurrences"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "recurrence_regenerate"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "recurrence_start"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "recurrence_type"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "recurrence_until"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "recurrence_week_of_month"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "reminder_set"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "reminder_time"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "reminder_type"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "sensitivity"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "start_date"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "sourceid"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "subject"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "utc_due_date"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, "utc_start_date"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->TASK_COLUMNS:[Ljava/lang/String;

    .line 94
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "state"

    aput-object v1, v0, v3

    const-string v1, "subject"

    aput-object v1, v0, v4

    const-string v1, "start_date"

    aput-object v1, v0, v5

    const-string v1, "due_date"

    aput-object v1, v0, v6

    const-string v1, "reminder_time"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "reminder_type"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->REMINDER_COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentProviderClient;Landroid/accounts/Account;Landroid/content/Context;)V
    .locals 2
    .param p1, "provider"    # Landroid/content/ContentProviderClient;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 110
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;-><init>(Landroid/content/ContentProviderClient;Landroid/accounts/Account;Landroid/content/Context;)V

    .line 111
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->mAccount:Landroid/accounts/Account;

    iget-object v1, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->mContext:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Util/SyncAccountManager;->addSamsungTask(Landroid/accounts/Account;Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->mAccountKey:I

    .line 113
    sget-object v0, Lcom/sec/android/sCloudSync/Constants/SPlannerTaskContract$URI;->TASK:Landroid/net/Uri;

    const-string v1, "caller_is_syncadapter"

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->mTasksUri:Landroid/net/Uri;

    .line 114
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->mTasksUri:Landroid/net/Uri;

    iget-object v1, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->mAccount:Landroid/accounts/Account;

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->addAccountParameter(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->mTasksUri:Landroid/net/Uri;

    .line 116
    sget-object v0, Lcom/sec/android/sCloudSync/Constants/SPlannerTaskContract$URI;->TASKREMINDER:Landroid/net/Uri;

    const-string v1, "caller_is_syncadapter"

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->mTasksReminderUri:Landroid/net/Uri;

    .line 117
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->mTasksReminderUri:Landroid/net/Uri;

    iget-object v1, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->mAccount:Landroid/accounts/Account;

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->addAccountParameter(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->mTasksReminderUri:Landroid/net/Uri;

    .line 119
    sget-object v0, Lcom/sec/android/sCloudSync/Constants/SPlannerTaskContract$URI;->TASKGROUP:Landroid/net/Uri;

    const-string v1, "caller_is_syncadapter"

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->mTasksGroupsUri:Landroid/net/Uri;

    .line 120
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->mTasksGroupsUri:Landroid/net/Uri;

    iget-object v1, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->mAccount:Landroid/accounts/Account;

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->addAccountParameter(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->mTasksGroupsUri:Landroid/net/Uri;

    .line 121
    return-void
.end method

.method private static addAccountParameter(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;
    .locals 3
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 104
    const-string v1, "account_name"

    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {p0, v1, v2}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addQeuryParameter(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 105
    .local v0, "AccountUri":Landroid/net/Uri;
    const-string v1, "account_type"

    iget-object v2, p1, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addQeuryParameter(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 106
    return-object v0
.end method

.method private getRemindersValues(JLorg/json/JSONObject;)Landroid/content/ContentValues;
    .locals 7
    .param p1, "taskId"    # J
    .param p3, "jsonObject"    # Lorg/json/JSONObject;

    .prologue
    const/4 v3, 0x0

    .line 302
    const/4 v2, 0x0

    .line 303
    .local v2, "reminderValue":Landroid/content/ContentValues;
    const-string v4, "REMINDERS"

    invoke-virtual {p3, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 304
    iget-object v4, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    sget-object v5, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->REMINDER_COLUMNS:[Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 306
    :try_start_0
    const-string v4, "REMINDERS"

    invoke-virtual {p3, v4}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 307
    .local v1, "reminderObj":Lorg/json/JSONObject;
    iget-object v4, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v4, v1}, Lcom/sec/android/sCloudSync/Records/JSONParser;->fromJSON(Lorg/json/JSONObject;)Landroid/content/ContentValues;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 312
    iget-object v4, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v4, v3}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 313
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/content/ContentValues;->size()I

    move-result v4

    if-nez v4, :cond_1

    .line 314
    :cond_0
    const-string v4, "sPlannerTaskBuilder"

    const-string v5, "insertReminders: Empty value."

    invoke-static {v4, v5}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    .end local v1    # "reminderObj":Lorg/json/JSONObject;
    :goto_0
    return-object v3

    .line 308
    :catch_0
    move-exception v0

    .line 309
    .local v0, "e":Lorg/json/JSONException;
    const-string v4, "sPlannerTaskBuilder"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "insertReminders:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .end local v0    # "e":Lorg/json/JSONException;
    :cond_1
    move-object v3, v2

    .line 318
    goto :goto_0
.end method

.method private getTaskValue(Lorg/json/JSONObject;)Landroid/content/ContentValues;
    .locals 4
    .param p1, "jsonObject"    # Lorg/json/JSONObject;

    .prologue
    .line 287
    const/4 v1, 0x0

    .line 288
    .local v1, "taskValues":Landroid/content/ContentValues;
    iget-object v2, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    sget-object v3, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->TASK_COLUMNS:[Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 290
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    const-string v3, "TASK"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/sCloudSync/Records/JSONParser;->fromJSON(Lorg/json/JSONObject;)Landroid/content/ContentValues;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 295
    :goto_0
    iget-object v2, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 296
    return-object v1

    .line 291
    :catch_0
    move-exception v0

    .line 292
    .local v0, "e1":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 293
    const-string v2, "sPlannerTaskBuilder"

    const-string v3, "insert: unable to parse;"

    invoke-static {v2, v3}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private insertReminder(JLorg/json/JSONObject;)V
    .locals 4
    .param p1, "taskId"    # J
    .param p3, "jsonObject"    # Lorg/json/JSONObject;

    .prologue
    .line 274
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->getRemindersValues(JLorg/json/JSONObject;)Landroid/content/ContentValues;

    move-result-object v1

    .line 275
    .local v1, "reminderValues":Landroid/content/ContentValues;
    if-eqz v1, :cond_0

    .line 276
    const-string v2, "task_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 278
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->mProvider:Landroid/content/ContentProviderClient;

    iget-object v3, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->mTasksReminderUri:Landroid/net/Uri;

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentProviderClient;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 284
    :cond_0
    :goto_0
    return-void

    .line 279
    :catch_0
    move-exception v0

    .line 280
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "sPlannerTaskBuilder"

    const-string v3, "Remote exception while inserting Task Reminder"

    invoke-static {v2, v3}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method private parseReminder(Landroid/database/Cursor;Lorg/json/JSONObject;Ljava/lang/Long;)V
    .locals 11
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "parsedTaskJson"    # Lorg/json/JSONObject;
    .param p3, "rowId"    # Ljava/lang/Long;

    .prologue
    const/4 v1, 0x1

    const/4 v10, 0x0

    .line 139
    const-string v0, "reminder_set"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 140
    const-string v9, "task_id= ?"

    .line 141
    .local v9, "selection":Ljava/lang/String;
    new-array v4, v1, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 142
    .local v4, "selectionArgs":[Ljava/lang/String;
    const/4 v7, 0x0

    .line 145
    .local v7, "reminderCursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->mProvider:Landroid/content/ContentProviderClient;

    iget-object v1, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->mTasksReminderUri:Landroid/net/Uri;

    sget-object v2, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->REMINDER_COLUMNS:[Ljava/lang/String;

    const-string v3, "task_id= ?"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 150
    :goto_0
    if-eqz v7, :cond_1

    .line 151
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    sget-object v1, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->REMINDER_COLUMNS:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 152
    const/4 v8, 0x0

    .line 154
    .local v8, "reminderJson":Lorg/json/JSONObject;
    :try_start_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v0, v7}, Lcom/sec/android/sCloudSync/Records/JSONParser;->toJSON(Landroid/database/Cursor;)Lorg/json/JSONObject;

    move-result-object v8

    .line 156
    invoke-virtual {v8}, Lorg/json/JSONObject;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 157
    const-string v0, "REMINDERS"

    invoke-virtual {p2, v0, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 162
    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 163
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v0, v10}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 167
    .end local v4    # "selectionArgs":[Ljava/lang/String;
    .end local v7    # "reminderCursor":Landroid/database/Cursor;
    .end local v8    # "reminderJson":Lorg/json/JSONObject;
    .end local v9    # "selection":Ljava/lang/String;
    :cond_1
    :goto_1
    return-void

    .line 147
    .restart local v4    # "selectionArgs":[Ljava/lang/String;
    .restart local v7    # "reminderCursor":Landroid/database/Cursor;
    .restart local v9    # "selection":Ljava/lang/String;
    :catch_0
    move-exception v6

    .line 148
    .local v6, "e":Landroid/os/RemoteException;
    const-string v0, "sPlannerTaskBuilder"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getReminders: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v6}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 159
    .end local v6    # "e":Landroid/os/RemoteException;
    .restart local v8    # "reminderJson":Lorg/json/JSONObject;
    :catch_1
    move-exception v6

    .line 160
    .local v6, "e":Lorg/json/JSONException;
    :try_start_2
    const-string v0, "sPlannerTaskBuilder"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getReminders: Unable to parse: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v6}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 162
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 163
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v0, v10}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    goto :goto_1

    .line 162
    .end local v6    # "e":Lorg/json/JSONException;
    :catchall_0
    move-exception v0

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 163
    iget-object v1, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v1, v10}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    throw v0
.end method

.method private parseTask(Landroid/database/Cursor;Lorg/json/JSONObject;Ljava/lang/Long;)V
    .locals 6
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "parsedTaskJson"    # Lorg/json/JSONObject;
    .param p3, "rowId"    # Ljava/lang/Long;

    .prologue
    const/4 v5, 0x0

    .line 125
    const/4 v1, 0x0

    .line 126
    .local v1, "taskJson":Lorg/json/JSONObject;
    iget-object v2, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    sget-object v3, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->TASK_COLUMNS:[Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 128
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v2, p1}, Lcom/sec/android/sCloudSync/Records/JSONParser;->toJSON(Landroid/database/Cursor;)Lorg/json/JSONObject;

    move-result-object v1

    .line 129
    const-string v2, "TASK"

    invoke-virtual {p2, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 133
    iget-object v2, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v2, v5}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 135
    :goto_0
    return-void

    .line 130
    :catch_0
    move-exception v0

    .line 131
    .local v0, "e":Lorg/json/JSONException;
    :try_start_1
    const-string v2, "sPlannerTaskBuilder"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "parse():Exception in parsing"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 133
    iget-object v2, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v2, v5}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    goto :goto_0

    .end local v0    # "e":Lorg/json/JSONException;
    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    invoke-virtual {v3, v5}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    throw v2
.end method

.method private updateReminders(JLorg/json/JSONObject;)V
    .locals 4
    .param p1, "rowId"    # J
    .param p3, "jsonObject"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 219
    iget-object v2, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->mTasksReminderUri:Landroid/net/Uri;

    invoke-static {v2, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 220
    .local v1, "uri":Landroid/net/Uri;
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->getRemindersValues(JLorg/json/JSONObject;)Landroid/content/ContentValues;

    move-result-object v0

    .line 222
    .local v0, "reminderValues":Landroid/content/ContentValues;
    if-nez v0, :cond_1

    .line 230
    :cond_0
    :goto_0
    return-void

    .line 226
    :cond_1
    iget-object v2, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->mProvider:Landroid/content/ContentProviderClient;

    invoke-virtual {v2, v1, v0, v3, v3}, Landroid/content/ContentProviderClient;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x1

    if-ge v2, v3, :cond_0

    .line 227
    const-string v2, "task_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 228
    iget-object v2, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->mProvider:Landroid/content/ContentProviderClient;

    iget-object v3, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->mTasksReminderUri:Landroid/net/Uri;

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentProviderClient;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto :goto_0
.end method


# virtual methods
.method public delete(Landroid/net/Uri;JLjava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "rowId"    # J
    .param p4, "selection"    # Ljava/lang/String;
    .param p5, "syncKey"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 329
    iget-object v1, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->mTasksUri:Landroid/net/Uri;

    invoke-static {v1, p2, p3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 330
    .local v0, "taskUri":Landroid/net/Uri;
    iget-object v1, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->mProvider:Landroid/content/ContentProviderClient;

    invoke-virtual {v1, v0, v2, v2}, Landroid/content/ContentProviderClient;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 331
    return-void
.end method

.method public doApplyBatch()Z
    .locals 1

    .prologue
    .line 324
    const/4 v0, 0x0

    return v0
.end method

.method public insert(Ljava/lang/String;Ljava/lang/String;J)Z
    .locals 12
    .param p1, "jsonString"    # Ljava/lang/String;
    .param p2, "syncKey"    # Ljava/lang/String;
    .param p3, "timeStamp"    # J

    .prologue
    const/4 v9, 0x0

    .line 235
    const/4 v3, 0x0

    .line 236
    .local v3, "jsonObject":Lorg/json/JSONObject;
    iget-object v8, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->mJsonParser:Lcom/sec/android/sCloudSync/Records/JSONParser;

    sget-object v10, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->TASK_COLUMNS:[Ljava/lang/String;

    invoke-virtual {v8, v10}, Lcom/sec/android/sCloudSync/Records/JSONParser;->setColumsList([Ljava/lang/String;)V

    .line 239
    :try_start_0
    new-instance v8, Lorg/json/JSONTokener;

    invoke-direct {v8, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object v8

    move-object v0, v8

    check-cast v0, Lorg/json/JSONObject;

    move-object v3, v0
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 245
    invoke-direct {p0, v3}, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->getTaskValue(Lorg/json/JSONObject;)Landroid/content/ContentValues;

    move-result-object v7

    .line 246
    .local v7, "taskValues":Landroid/content/ContentValues;
    if-eqz v7, :cond_0

    invoke-virtual {v7}, Landroid/content/ContentValues;->size()I

    move-result v8

    if-nez v8, :cond_1

    .line 247
    :cond_0
    const-string v8, "sPlannerTaskBuilder"

    const-string v10, "insert: unable to parse;"

    invoke-static {v8, v10}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    move v8, v9

    .line 270
    .end local v7    # "taskValues":Landroid/content/ContentValues;
    :goto_0
    return v8

    .line 240
    :catch_0
    move-exception v2

    .line 241
    .local v2, "e1":Lorg/json/JSONException;
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    .line 242
    const-string v8, "sPlannerTaskBuilder"

    const-string v10, "insert: unable to parse;"

    invoke-static {v8, v10}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v8, v9

    .line 243
    goto :goto_0

    .line 251
    .end local v2    # "e1":Lorg/json/JSONException;
    .restart local v7    # "taskValues":Landroid/content/ContentValues;
    :cond_1
    const-string v8, "syncServerId"

    invoke-virtual {v7, v8, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    const-string v8, "syncTime"

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v7, v8, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 253
    const-string v8, "accountKey"

    iget v10, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->mAccountKey:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v7, v8, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 255
    const/4 v4, 0x0

    .line 257
    .local v4, "resultUri":Landroid/net/Uri;
    :try_start_1
    iget-object v8, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->mProvider:Landroid/content/ContentProviderClient;

    iget-object v10, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->mTasksUri:Landroid/net/Uri;

    invoke-virtual {v8, v10, v7}, Landroid/content/ContentProviderClient;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v4

    .line 262
    if-nez v4, :cond_2

    move v8, v9

    .line 263
    goto :goto_0

    .line 258
    :catch_1
    move-exception v1

    .line 259
    .local v1, "e":Landroid/os/RemoteException;
    const-string v8, "sPlannerTaskBuilder"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "insertEvent:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v1}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v8, v9

    .line 260
    goto :goto_0

    .line 266
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_2
    invoke-static {v4}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v5

    .line 267
    .local v5, "taskId":J
    const-wide/16 v8, 0x0

    cmp-long v8, v5, v8

    if-lez v8, :cond_3

    .line 268
    invoke-direct {p0, v5, v6, v3}, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->insertReminder(JLorg/json/JSONObject;)V

    .line 270
    :cond_3
    const/4 v8, 0x1

    goto :goto_0
.end method

.method public parse(Landroid/database/Cursor;JLjava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "rowId"    # J
    .param p4, "key"    # Ljava/lang/String;

    .prologue
    .line 173
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 174
    .local v0, "parsedTaskJson":Lorg/json/JSONObject;
    if-nez p1, :cond_0

    .line 175
    const/4 v1, 0x0

    .line 180
    :goto_0
    return-object v1

    .line 177
    :cond_0
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->parseTask(Landroid/database/Cursor;Lorg/json/JSONObject;Ljava/lang/Long;)V

    .line 179
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->parseReminder(Landroid/database/Cursor;Lorg/json/JSONObject;Ljava/lang/Long;)V

    .line 180
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public update(Ljava/lang/String;JJLjava/lang/String;)Z
    .locals 10
    .param p1, "jsonString"    # Ljava/lang/String;
    .param p2, "timeStamp"    # J
    .param p4, "rowId"    # J
    .param p6, "synckey"    # Ljava/lang/String;

    .prologue
    .line 185
    const/4 v3, 0x0

    .line 187
    .local v3, "jsonObject":Lorg/json/JSONObject;
    :try_start_0
    new-instance v7, Lorg/json/JSONTokener;

    invoke-direct {v7, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object v7

    move-object v0, v7

    check-cast v0, Lorg/json/JSONObject;

    move-object v3, v0
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 193
    invoke-direct {p0, v3}, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->getTaskValue(Lorg/json/JSONObject;)Landroid/content/ContentValues;

    move-result-object v4

    .line 194
    .local v4, "taskValues":Landroid/content/ContentValues;
    if-nez v4, :cond_0

    .line 195
    const-string v7, "sPlannerTaskBuilder"

    const-string v8, "UpdateEvent: unable to parse;"

    invoke-static {v7, v8}, Lcom/sec/android/sCloudSync/Util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    const/4 v7, 0x0

    .line 214
    .end local v4    # "taskValues":Landroid/content/ContentValues;
    :goto_0
    return v7

    .line 188
    :catch_0
    move-exception v2

    .line 189
    .local v2, "e1":Lorg/json/JSONException;
    const-string v7, "sPlannerTaskBuilder"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "updatetask(): "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    const/4 v7, 0x0

    goto :goto_0

    .line 198
    .end local v2    # "e1":Lorg/json/JSONException;
    .restart local v4    # "taskValues":Landroid/content/ContentValues;
    :cond_0
    const-string v7, "syncTime"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 199
    const-string v7, "_sync_dirty"

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 200
    const-string v7, "deleted"

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 204
    :try_start_1
    iget-object v7, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->mTasksUri:Landroid/net/Uri;

    invoke-static {v7, p4, p5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v6

    .line 205
    .local v6, "uri":Landroid/net/Uri;
    iget-object v7, p0, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->mProvider:Landroid/content/ContentProviderClient;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v7, v6, v4, v8, v9}, Landroid/content/ContentProviderClient;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    .line 206
    .local v5, "updated":I
    if-nez v5, :cond_1

    .line 207
    const/4 v7, 0x0

    goto :goto_0

    .line 209
    :cond_1
    invoke-direct {p0, p4, p5, v3}, Lcom/sec/android/sCloudSync/Builders/SPlanner/SPlannerTaskBuilder;->updateReminders(JLorg/json/JSONObject;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 214
    const/4 v7, 0x1

    goto :goto_0

    .line 210
    .end local v5    # "updated":I
    .end local v6    # "uri":Landroid/net/Uri;
    :catch_1
    move-exception v1

    .line 211
    .local v1, "e":Landroid/os/RemoteException;
    const-string v7, "sPlannerTaskBuilder"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "updateTask():"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/sCloudSync/Util/LOG;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    const/4 v7, 0x0

    goto :goto_0
.end method
