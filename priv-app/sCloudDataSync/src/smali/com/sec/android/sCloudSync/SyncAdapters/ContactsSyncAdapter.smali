.class public Lcom/sec/android/sCloudSync/SyncAdapters/ContactsSyncAdapter;
.super Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;
.source "ContactsSyncAdapter.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ContactsSyncAdapter"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 41
    new-instance v0, Lcom/sec/android/sCloudSync/ServiceManagers/SCloudServiceManager;

    const-string v1, "KEqLhXhtEP"

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v2}, Lcom/sec/android/sCloudSync/ServiceManagers/SCloudServiceManager;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    invoke-direct {p0, p1, v0}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;-><init>(Landroid/content/Context;Lcom/sec/android/sCloudSync/ServiceManagers/ICloudServiceManager;)V

    .line 42
    return-void
.end method


# virtual methods
.method public getAccountName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    const-string v0, "account_name"

    return-object v0
.end method

.method public getAccountType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    const-string v0, "account_type"

    return-object v0
.end method

.method public getBuilder()Lcom/sec/android/sCloudSync/Builders/AbstractBuilder;
    .locals 4

    .prologue
    .line 98
    new-instance v0, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsBuilder;

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/ContactsSyncAdapter;->getProvider()Landroid/content/ContentProviderClient;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/ContactsSyncAdapter;->getAccount()Landroid/accounts/Account;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/sCloudSync/SyncAdapters/ContactsSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/sCloudSync/Builders/Contact/ContactsBuilder;-><init>(Landroid/content/ContentProviderClient;Landroid/accounts/Account;Landroid/content/Context;)V

    return-object v0
.end method

.method public getCallerSyncAdapter()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    const-string v0, "caller_is_syncadapter"

    return-object v0
.end method

.method public getCid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    const-string v0, "KEqLhXhtEP"

    return-object v0
.end method

.method public getContentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 63
    sget-object v0, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method protected getCtidKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    const-string v0, "CT"

    return-object v0
.end method

.method public getDeletedColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    const-string v0, "deleted"

    return-object v0
.end method

.method public getDirtyColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    const-string v0, "dirty"

    return-object v0
.end method

.method public getIdColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    const-string v0, "_id"

    return-object v0
.end method

.method public getKeyColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    const-string v0, "sync1"

    return-object v0
.end method

.method public getSyncAdapterName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    const-string v0, "CONTACT"

    return-object v0
.end method

.method protected getSyncStateDataColumn()Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    const-string v0, "data"

    return-object v0
.end method

.method protected getSyncStateURI()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 127
    sget-object v0, Landroid/provider/ContactsContract$SyncState;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method public getTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    const-string v0, "ContactsSyncAdapter"

    return-object v0
.end method

.method public getTimeStampColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    const-string v0, "sync3"

    return-object v0
.end method
