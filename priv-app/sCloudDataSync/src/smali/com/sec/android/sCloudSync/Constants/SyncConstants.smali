.class public interface abstract Lcom/sec/android/sCloudSync/Constants/SyncConstants;
.super Ljava/lang/Object;
.source "SyncConstants.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/sCloudSync/Constants/SyncConstants$METHOD_CODE;,
        Lcom/sec/android/sCloudSync/Constants/SyncConstants$REQUEST_TYPE;,
        Lcom/sec/android/sCloudSync/Constants/SyncConstants$SYNC_META;,
        Lcom/sec/android/sCloudSync/Constants/SyncConstants$INTENT;
    }
.end annotation


# static fields
.field public static final ACCOUNT_TYPE:Ljava/lang/String; = "com.osp.app.signin"

.field public static final BOOKMARK_BAR:Ljava/lang/String; = "bookmark_bar"

.field public static final BOOK_MARK_TITLE:Ljava/lang/String; = "Samsung Bookmark"

.field public static final CALENDAR_COLOR:I

.field public static final CALENDAR_DISPLAY:Ljava/lang/String; = "Samsung Calendar"

.field public static final CALENDAR_NAME:Ljava/lang/String; = "Samsung Calendar"

.field public static final CALLER_IS_SYNCADAPTER:Ljava/lang/String; = "caller_is_syncadapter"

.field public static final SMEMO_AUTH:Ljava/lang/String; = "com.sec.android.widgetapp.q1_penmemo"

.field public static final TASK_DISPLAY_NAME:Ljava/lang/String; = "Samsung Tasks"

.field public static final TMEMO_AUTH:Ljava/lang/String; = "com.samsung.sec.android"


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 33
    const/4 v0, 0x0

    const/16 v1, 0x46

    const/16 v2, 0xad

    invoke-static {v0, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    sput v0, Lcom/sec/android/sCloudSync/Constants/SyncConstants;->CALENDAR_COLOR:I

    return-void
.end method
