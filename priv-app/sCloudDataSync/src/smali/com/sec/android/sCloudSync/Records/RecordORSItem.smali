.class public final Lcom/sec/android/sCloudSync/Records/RecordORSItem;
.super Lcom/sec/android/sCloudSync/Records/AbstractJSON;
.source "RecordORSItem.java"


# static fields
.field private static final KEY:Ljava/lang/String; = "key"

.field private static final VALUE:Ljava/lang/String; = "value"


# instance fields
.field mKey:Ljava/lang/String;

.field mValue:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/sec/android/sCloudSync/Records/AbstractJSON;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/sec/android/sCloudSync/Records/RecordORSItem;->mKey:Ljava/lang/String;

    .line 41
    iput-object p2, p0, Lcom/sec/android/sCloudSync/Records/RecordORSItem;->mValue:Ljava/lang/String;

    .line 42
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 0

    .prologue
    .line 67
    return-void
.end method

.method public fromJSON(Ljava/lang/Object;)V
    .locals 2
    .param p1, "obj"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 46
    move-object v0, p1

    check-cast v0, Lorg/json/JSONObject;

    .line 47
    .local v0, "json":Lorg/json/JSONObject;
    const-string v1, "key"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 48
    const-string v1, "key"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/sCloudSync/Records/RecordORSItem;->mKey:Ljava/lang/String;

    .line 49
    :cond_0
    const-string v1, "value"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 50
    const-string v1, "value"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/sCloudSync/Records/RecordORSItem;->mValue:Ljava/lang/String;

    .line 51
    :cond_1
    return-void
.end method

.method public getKEY()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Records/RecordORSItem;->mKey:Ljava/lang/String;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Records/RecordORSItem;->mValue:Ljava/lang/String;

    return-object v0
.end method

.method public toJSON()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 55
    iget-object v1, p0, Lcom/sec/android/sCloudSync/Records/RecordORSItem;->mKey:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/sCloudSync/Records/RecordORSItem;->mValue:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 56
    :cond_0
    new-instance v1, Lorg/json/JSONException;

    const-string v2, "RecordORSItem :Input parsing error"

    invoke-direct {v1, v2}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 58
    :cond_1
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 59
    .local v0, "json":Lorg/json/JSONObject;
    const-string v1, "key"

    iget-object v2, p0, Lcom/sec/android/sCloudSync/Records/RecordORSItem;->mKey:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 60
    const-string v1, "value"

    iget-object v2, p0, Lcom/sec/android/sCloudSync/Records/RecordORSItem;->mValue:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 61
    return-object v0
.end method
