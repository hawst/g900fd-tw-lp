.class Lcom/sec/android/sCloudSync/Services/SBrowserSyncService$1;
.super Lcom/sec/android/sCloudSync/IDataSync/IDataSync$Stub;
.source "SBrowserSyncService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;


# direct methods
.method constructor <init>(Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;)V
    .locals 0

    .prologue
    .line 78
    iput-object p1, p0, Lcom/sec/android/sCloudSync/Services/SBrowserSyncService$1;->this$0:Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;

    invoke-direct {p0}, Lcom/sec/android/sCloudSync/IDataSync/IDataSync$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public cancelSync()V
    .locals 5

    .prologue
    .line 139
    iget-object v2, p0, Lcom/sec/android/sCloudSync/Services/SBrowserSyncService$1;->this$0:Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;

    const/4 v3, 0x1

    # setter for: Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;->isCancel:Z
    invoke-static {v2, v3}, Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;->access$002(Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;Z)Z

    .line 141
    # getter for: Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;->ADAPTER_MAP:Ljava/util/Map;
    invoke-static {}, Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;->access$200()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 143
    .local v1, "sync":Ljava/lang/String;
    const-string v2, "sync_saved_pages"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 144
    invoke-static {}, Lcom/samsung/android/scloud/sync/model/ModelManager;->getInstance()Lcom/samsung/android/scloud/sync/model/ModelManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/sCloudSync/Services/SBrowserSyncService$1;->this$0:Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;

    const-string v4, "READINGLIST_DATA"

    invoke-virtual {v2, v3, v4}, Lcom/samsung/android/scloud/sync/model/ModelManager;->getSyncManager(Landroid/content/Context;Ljava/lang/String;)Lcom/samsung/android/scloud/sync/core/SyncManager;

    move-result-object v2

    invoke-static {}, Lcom/samsung/android/scloud/sync/model/ModelManager;->getInstance()Lcom/samsung/android/scloud/sync/model/ModelManager;

    move-result-object v3

    const-string v4, "READINGLIST_DATA"

    invoke-virtual {v3, v4}, Lcom/samsung/android/scloud/sync/model/ModelManager;->getModel(Ljava/lang/String;)Lcom/samsung/android/scloud/sync/model/IModel;

    move-result-object v3

    invoke-interface {v3}, Lcom/samsung/android/scloud/sync/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/scloud/sync/core/SyncManager;->requestCancel(Ljava/lang/String;)V

    goto :goto_0

    .line 146
    :cond_0
    # getter for: Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;->ADAPTER_MAP:Ljava/util/Map;
    invoke-static {}, Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;->access$200()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;

    invoke-virtual {v2}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->cancelSync()V

    goto :goto_0

    .line 148
    .end local v1    # "sync":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method public performSync(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;)Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;
    .locals 16
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "authority"    # Ljava/lang/String;
    .param p3, "dataSyncResult"    # Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;

    .prologue
    .line 82
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/Services/SBrowserSyncService$1;->this$0:Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;

    const/4 v5, 0x0

    # setter for: Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;->isCancel:Z
    invoke-static {v2, v5}, Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;->access$002(Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;Z)Z

    .line 83
    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;->getBundle()Landroid/os/Bundle;

    move-result-object v9

    .line 84
    .local v9, "extras":Landroid/os/Bundle;
    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;->getSyncResult()Landroid/content/SyncResult;

    move-result-object v13

    .line 86
    .local v13, "syncResult":Landroid/content/SyncResult;
    sget-object v2, Lcom/sec/android/sCloudSync/Constants/SBrowserContract;->SBrowser_INTERNETSYNC_URI:Landroid/net/Uri;

    const-string v5, "caller_is_syncadapter"

    invoke-static {v2, v5}, Lcom/sec/android/sCloudSync/Tools/UriTool;->addCallerIsSyncAdapterParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 88
    .local v3, "uriFromSyncAdapter":Landroid/net/Uri;
    const/4 v2, 0x2

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v5, "SYNC_KEY"

    aput-object v5, v4, v2

    const/4 v2, 0x1

    const-string v5, "SYNC_VALUE"

    aput-object v5, v4, v2

    .line 90
    .local v4, "projection":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/Services/SBrowserSyncService$1;->this$0:Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;

    invoke-virtual {v2}, Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 92
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_4

    .line 94
    # getter for: Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;->ENABLED_SYNC:Ljava/util/HashSet;
    invoke-static {}, Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;->access$100()Ljava/util/HashSet;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashSet;->clear()V

    .line 95
    :cond_0
    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 97
    const-string v2, "SYNC_KEY"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 98
    .local v12, "syncKey":Ljava/lang/String;
    const-string v2, "SYNC_VALUE"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 99
    .local v14, "syncValue":I
    const-string v2, "SBROWSER-SYNC"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SyncKey is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " SyncValue is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/sec/android/sCloudSync/Util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    const/4 v2, 0x1

    if-ne v14, v2, :cond_0

    .line 102
    # getter for: Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;->ENABLED_SYNC:Ljava/util/HashSet;
    invoke-static {}, Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;->access$100()Ljava/util/HashSet;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 105
    .end local v12    # "syncKey":Ljava/lang/String;
    .end local v14    # "syncValue":I
    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 107
    # getter for: Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;->ENABLED_SYNC:Ljava/util/HashSet;
    invoke-static {}, Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;->access$100()Ljava/util/HashSet;

    move-result-object v2

    const-string v5, "sync_internet_data"

    invoke-virtual {v2, v5}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 109
    # getter for: Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;->ENABLED_SYNC:Ljava/util/HashSet;
    invoke-static {}, Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;->access$100()Ljava/util/HashSet;

    move-result-object v2

    const-string v5, "sync_internet_data"

    invoke-virtual {v2, v5}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 110
    # getter for: Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;->ENABLED_SYNC:Ljava/util/HashSet;
    invoke-static {}, Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;->access$100()Ljava/util/HashSet;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .line 112
    .local v11, "sync":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/sCloudSync/Services/SBrowserSyncService$1;->this$0:Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;

    # getter for: Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;->isCancel:Z
    invoke-static {v2}, Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;->access$000(Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 114
    const-string v2, "sync_saved_pages"

    invoke-virtual {v11, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 121
    # getter for: Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;->ADAPTER_MAP:Ljava/util/Map;
    invoke-static {}, Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;->access$200()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;

    .line 122
    .local v15, "syncadapter":Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;
    if-eqz v15, :cond_2

    .line 123
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v15, v0, v9, v1, v13}, Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;->performSync(Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/SyncResult;)Landroid/content/SyncResult;

    goto :goto_1

    .line 128
    .end local v11    # "sync":Ljava/lang/String;
    .end local v15    # "syncadapter":Lcom/sec/android/sCloudSync/SyncAdapters/AbstractSyncAdapter;
    :cond_3
    # getter for: Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;->ENABLED_SYNC:Ljava/util/HashSet;
    invoke-static {}, Lcom/sec/android/sCloudSync/Services/SBrowserSyncService;->access$100()Ljava/util/HashSet;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashSet;->clear()V

    .line 133
    .end local v10    # "i$":Ljava/util/Iterator;
    :cond_4
    move-object/from16 v0, p3

    invoke-virtual {v0, v13}, Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;->setSyncResult(Landroid/content/SyncResult;)V

    .line 134
    return-object p3
.end method
