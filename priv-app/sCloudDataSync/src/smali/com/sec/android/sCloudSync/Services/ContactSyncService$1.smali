.class Lcom/sec/android/sCloudSync/Services/ContactSyncService$1;
.super Lcom/sec/android/sCloudSync/IDataSync/IDataSync$Stub;
.source "ContactSyncService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/sCloudSync/Services/ContactSyncService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/sCloudSync/Services/ContactSyncService;


# direct methods
.method constructor <init>(Lcom/sec/android/sCloudSync/Services/ContactSyncService;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/sec/android/sCloudSync/Services/ContactSyncService$1;->this$0:Lcom/sec/android/sCloudSync/Services/ContactSyncService;

    invoke-direct {p0}, Lcom/sec/android/sCloudSync/IDataSync/IDataSync$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public cancelSync()V
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Services/ContactSyncService$1;->this$0:Lcom/sec/android/sCloudSync/Services/ContactSyncService;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/sCloudSync/Services/ContactSyncService;->isCancel:Z
    invoke-static {v0, v1}, Lcom/sec/android/sCloudSync/Services/ContactSyncService;->access$002(Lcom/sec/android/sCloudSync/Services/ContactSyncService;Z)Z

    .line 78
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Services/ContactSyncService$1;->this$0:Lcom/sec/android/sCloudSync/Services/ContactSyncService;

    iget-object v0, v0, Lcom/sec/android/sCloudSync/Services/ContactSyncService;->contactsSync:Lcom/sec/android/sCloudSync/SyncAdapters/ContactsSyncAdapter;

    invoke-virtual {v0}, Lcom/sec/android/sCloudSync/SyncAdapters/ContactsSyncAdapter;->cancelSync()V

    .line 79
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Services/ContactSyncService$1;->this$0:Lcom/sec/android/sCloudSync/Services/ContactSyncService;

    iget-object v0, v0, Lcom/sec/android/sCloudSync/Services/ContactSyncService;->contactsGrpSync:Lcom/sec/android/sCloudSync/SyncAdapters/ContactsGroupSyncAdapter;

    invoke-virtual {v0}, Lcom/sec/android/sCloudSync/SyncAdapters/ContactsGroupSyncAdapter;->cancelSync()V

    .line 80
    return-void
.end method

.method public performSync(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;)Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;
    .locals 4
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "authority"    # Ljava/lang/String;
    .param p3, "dataSyncResult"    # Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;

    .prologue
    .line 62
    iget-object v2, p0, Lcom/sec/android/sCloudSync/Services/ContactSyncService$1;->this$0:Lcom/sec/android/sCloudSync/Services/ContactSyncService;

    const/4 v3, 0x0

    # setter for: Lcom/sec/android/sCloudSync/Services/ContactSyncService;->isCancel:Z
    invoke-static {v2, v3}, Lcom/sec/android/sCloudSync/Services/ContactSyncService;->access$002(Lcom/sec/android/sCloudSync/Services/ContactSyncService;Z)Z

    .line 63
    invoke-virtual {p3}, Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 64
    .local v0, "extras":Landroid/os/Bundle;
    invoke-virtual {p3}, Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;->getSyncResult()Landroid/content/SyncResult;

    move-result-object v1

    .line 66
    .local v1, "syncResult":Landroid/content/SyncResult;
    iget-object v2, p0, Lcom/sec/android/sCloudSync/Services/ContactSyncService$1;->this$0:Lcom/sec/android/sCloudSync/Services/ContactSyncService;

    iget-object v2, v2, Lcom/sec/android/sCloudSync/Services/ContactSyncService;->contactsSync:Lcom/sec/android/sCloudSync/SyncAdapters/ContactsSyncAdapter;

    invoke-virtual {v2, p1, v0, p2, v1}, Lcom/sec/android/sCloudSync/SyncAdapters/ContactsSyncAdapter;->performSync(Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/SyncResult;)Landroid/content/SyncResult;

    move-result-object v1

    .line 67
    iget-object v2, p0, Lcom/sec/android/sCloudSync/Services/ContactSyncService$1;->this$0:Lcom/sec/android/sCloudSync/Services/ContactSyncService;

    # getter for: Lcom/sec/android/sCloudSync/Services/ContactSyncService;->isCancel:Z
    invoke-static {v2}, Lcom/sec/android/sCloudSync/Services/ContactSyncService;->access$000(Lcom/sec/android/sCloudSync/Services/ContactSyncService;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 68
    iget-object v2, p0, Lcom/sec/android/sCloudSync/Services/ContactSyncService$1;->this$0:Lcom/sec/android/sCloudSync/Services/ContactSyncService;

    iget-object v2, v2, Lcom/sec/android/sCloudSync/Services/ContactSyncService;->contactsGrpSync:Lcom/sec/android/sCloudSync/SyncAdapters/ContactsGroupSyncAdapter;

    invoke-virtual {v2, p1, v0, p2, v1}, Lcom/sec/android/sCloudSync/SyncAdapters/ContactsGroupSyncAdapter;->performSync(Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/SyncResult;)Landroid/content/SyncResult;

    move-result-object v1

    .line 70
    :cond_0
    invoke-virtual {p3, v1}, Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;->setSyncResult(Landroid/content/SyncResult;)V

    .line 71
    return-object p3
.end method
