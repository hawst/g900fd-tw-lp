.class Lcom/sec/android/sCloudSync/Services/SNoteSyncService$1;
.super Lcom/sec/android/sCloudSync/IDataSync/IDataSync$Stub;
.source "SNoteSyncService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/sCloudSync/Services/SNoteSyncService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/sCloudSync/Services/SNoteSyncService;


# direct methods
.method constructor <init>(Lcom/sec/android/sCloudSync/Services/SNoteSyncService;)V
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/sec/android/sCloudSync/Services/SNoteSyncService$1;->this$0:Lcom/sec/android/sCloudSync/Services/SNoteSyncService;

    invoke-direct {p0}, Lcom/sec/android/sCloudSync/IDataSync/IDataSync$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public cancelSync()V
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/sCloudSync/Services/SNoteSyncService$1;->this$0:Lcom/sec/android/sCloudSync/Services/SNoteSyncService;

    # getter for: Lcom/sec/android/sCloudSync/Services/SNoteSyncService;->sync:Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;
    invoke-static {v0}, Lcom/sec/android/sCloudSync/Services/SNoteSyncService;->access$000(Lcom/sec/android/sCloudSync/Services/SNoteSyncService;)Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->cancelSync()V

    .line 70
    return-void
.end method

.method public performSync(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;)Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;
    .locals 3
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "authority"    # Ljava/lang/String;
    .param p3, "dataSyncResult"    # Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;

    .prologue
    .line 59
    invoke-virtual {p3}, Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 60
    .local v0, "extras":Landroid/os/Bundle;
    invoke-virtual {p3}, Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;->getSyncResult()Landroid/content/SyncResult;

    move-result-object v1

    .line 61
    .local v1, "syncResult":Landroid/content/SyncResult;
    iget-object v2, p0, Lcom/sec/android/sCloudSync/Services/SNoteSyncService$1;->this$0:Lcom/sec/android/sCloudSync/Services/SNoteSyncService;

    # getter for: Lcom/sec/android/sCloudSync/Services/SNoteSyncService;->sync:Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;
    invoke-static {v2}, Lcom/sec/android/sCloudSync/Services/SNoteSyncService;->access$000(Lcom/sec/android/sCloudSync/Services/SNoteSyncService;)Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;

    move-result-object v2

    invoke-virtual {v2, p1, v0, p2, v1}, Lcom/sec/android/sCloudSync/SyncAdapters/SNoteSyncAdapter;->performSync(Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/SyncResult;)Landroid/content/SyncResult;

    move-result-object v1

    .line 62
    invoke-virtual {p3, v1}, Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;->setSyncResult(Landroid/content/SyncResult;)V

    .line 64
    return-object p3
.end method
