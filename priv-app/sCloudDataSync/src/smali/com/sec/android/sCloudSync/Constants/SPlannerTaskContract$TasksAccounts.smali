.class public interface abstract Lcom/sec/android/sCloudSync/Constants/SPlannerTaskContract$TasksAccounts;
.super Ljava/lang/Object;
.source "SPlannerTaskContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/sCloudSync/Constants/SPlannerTaskContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "TasksAccounts"
.end annotation


# static fields
.field public static final ACCOUNT_KEY:Ljava/lang/String; = "_sync_account_key"

.field public static final ACCOUNT_NAME:Ljava/lang/String; = "_sync_account"

.field public static final ACCOUNT_TYPE:Ljava/lang/String; = "_sync_account_type"

.field public static final DISPLAYNAME:Ljava/lang/String; = "displayName"

.field public static final SELECTED:Ljava/lang/String; = "selected"

.field public static final URL:Ljava/lang/String; = "url"
