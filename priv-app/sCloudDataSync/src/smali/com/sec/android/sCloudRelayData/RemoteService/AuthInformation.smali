.class public final Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;
.super Ljava/lang/Object;
.source "AuthInformation.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public accessToken:Ljava/lang/String;

.field public baseUrl:Ljava/lang/String;

.field public baseUrl2:Ljava/lang/String;

.field public mKey:Ljava/lang/String;

.field public regId:Ljava/lang/String;

.field public sKey:I

.field public status:I

.field public userId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    new-instance v0, Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation$1;

    invoke-direct {v0}, Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation$1;-><init>()V

    sput-object v0, Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 13
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput v1, p0, Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;->status:I

    .line 15
    iput v1, p0, Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;->sKey:I

    .line 16
    iput-object v0, p0, Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;->mKey:Ljava/lang/String;

    .line 17
    iput-object v0, p0, Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;->userId:Ljava/lang/String;

    .line 18
    iput-object v0, p0, Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;->regId:Ljava/lang/String;

    .line 19
    iput-object v0, p0, Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;->accessToken:Ljava/lang/String;

    .line 20
    iput-object v0, p0, Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;->baseUrl:Ljava/lang/String;

    .line 21
    iput-object v0, p0, Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;->baseUrl2:Ljava/lang/String;

    .line 60
    if-eqz p1, :cond_0

    .line 62
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;->status:I

    .line 63
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;->sKey:I

    .line 64
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;->mKey:Ljava/lang/String;

    .line 65
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;->userId:Ljava/lang/String;

    .line 66
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;->regId:Ljava/lang/String;

    .line 67
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;->accessToken:Ljava/lang/String;

    .line 68
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;->baseUrl:Ljava/lang/String;

    .line 69
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;->baseUrl2:Ljava/lang/String;

    .line 71
    :cond_0
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    .line 31
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;->writeToParcel(Landroid/os/Parcel;I)V

    .line 32
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "arg1"    # I

    .prologue
    .line 36
    if-eqz p1, :cond_0

    .line 38
    iget v0, p0, Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;->status:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 39
    iget v0, p0, Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;->sKey:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 40
    iget-object v0, p0, Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;->mKey:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 41
    iget-object v0, p0, Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;->userId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 42
    iget-object v0, p0, Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;->regId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 43
    iget-object v0, p0, Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;->accessToken:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 44
    iget-object v0, p0, Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;->baseUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 45
    iget-object v0, p0, Lcom/sec/android/sCloudRelayData/RemoteService/AuthInformation;->baseUrl2:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 47
    :cond_0
    return-void
.end method
