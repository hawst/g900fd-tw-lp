.class public Lcom/policydm/db/XDBPollingInfo;
.super Ljava/lang/Object;
.source "XDBPollingInfo.java"

# interfaces
.implements Lcom/policydm/interfaces/XPollingInterface;
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public m_nextpollingtime:J

.field public m_nextreporttime:J

.field public m_szFileName:Ljava/lang/String;

.field public m_szOrgVersionUrl:Ljava/lang/String;

.field public m_szPeriodUnit:Ljava/lang/String;

.field public m_szVersionUrl:Ljava/lang/String;

.field public nPeriod:I

.field public nRange:I

.field public nReportPeriod:I

.field public nReportRange:I

.field public nReportTime:I

.field public nTime:I


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const-wide/16 v3, 0x0

    const/16 v2, 0xf

    const/16 v1, 0x9

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const-string v0, "http://svc-org.spd.samsungdm.com/"

    iput-object v0, p0, Lcom/policydm/db/XDBPollingInfo;->m_szOrgVersionUrl:Ljava/lang/String;

    .line 27
    const-string v0, "http://svc-cf.spd.samsungdm.com/"

    iput-object v0, p0, Lcom/policydm/db/XDBPollingInfo;->m_szVersionUrl:Ljava/lang/String;

    .line 28
    const-string v0, "version.xml"

    iput-object v0, p0, Lcom/policydm/db/XDBPollingInfo;->m_szFileName:Ljava/lang/String;

    .line 29
    const-string v0, "day"

    iput-object v0, p0, Lcom/policydm/db/XDBPollingInfo;->m_szPeriodUnit:Ljava/lang/String;

    .line 30
    const/4 v0, 0x1

    iput v0, p0, Lcom/policydm/db/XDBPollingInfo;->nPeriod:I

    .line 31
    iput v2, p0, Lcom/policydm/db/XDBPollingInfo;->nTime:I

    .line 32
    iput v1, p0, Lcom/policydm/db/XDBPollingInfo;->nRange:I

    .line 33
    const/16 v0, 0x1e

    iput v0, p0, Lcom/policydm/db/XDBPollingInfo;->nReportPeriod:I

    .line 34
    iput v2, p0, Lcom/policydm/db/XDBPollingInfo;->nReportTime:I

    .line 35
    iput v1, p0, Lcom/policydm/db/XDBPollingInfo;->nReportRange:I

    .line 36
    iput-wide v3, p0, Lcom/policydm/db/XDBPollingInfo;->m_nextpollingtime:J

    .line 37
    iput-wide v3, p0, Lcom/policydm/db/XDBPollingInfo;->m_nextreporttime:J

    .line 38
    return-void
.end method
