.class public Lcom/policydm/db/XDBFumoAdp;
.super Ljava/lang/Object;
.source "XDBFumoAdp.java"

# interfaces
.implements Lcom/policydm/interfaces/XDBInterface;
.implements Lcom/policydm/interfaces/XDLInterface;
.implements Lcom/policydm/interfaces/XDMInterface;


# static fields
.field public static gFumoStatus:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const/4 v0, -0x1

    sput v0, Lcom/policydm/db/XDBFumoAdp;->gFumoStatus:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xdbGetDLProtocol()Ljava/lang/String;
    .locals 4

    .prologue
    .line 63
    const-string v2, ""

    .line 66
    .local v2, "szProtocol":Ljava/lang/String;
    const/16 v3, 0xcd

    :try_start_0
    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 72
    :goto_0
    return-object v2

    .line 68
    :catch_0
    move-exception v1

    .line 70
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetFUMOCorrelator()Ljava/lang/String;
    .locals 5

    .prologue
    .line 495
    const-string v2, ""

    .line 498
    .local v2, "szCorrelator":Ljava/lang/String;
    const/16 v3, 0xdc

    :try_start_0
    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 504
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "pCorrelator = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 506
    return-object v2

    .line 500
    :catch_0
    move-exception v1

    .line 502
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetFUMODescription()Ljava/lang/String;
    .locals 4

    .prologue
    .line 342
    const-string v2, ""

    .line 345
    .local v2, "szDescription":Ljava/lang/String;
    const/16 v3, 0xd7

    :try_start_0
    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 352
    :goto_0
    return-object v2

    .line 347
    :catch_0
    move-exception v1

    .line 349
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetFUMODownloadAddr(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "szURL"    # Ljava/lang/String;

    .prologue
    .line 300
    const-string v2, ""

    .line 303
    .local v2, "szReportURI":Ljava/lang/String;
    const/16 v3, 0xce

    :try_start_0
    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 309
    :goto_0
    return-object v2

    .line 305
    :catch_0
    move-exception v1

    .line 307
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetFUMODownloadMode()Ljava/lang/Boolean;
    .locals 4

    .prologue
    .line 454
    const/4 v1, 0x0

    .line 458
    .local v1, "nDownloadMode":Z
    const/16 v3, 0xdb

    :try_start_0
    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v2

    .line 459
    .local v2, "oStatus":Ljava/lang/Object;
    if-eqz v2, :cond_0

    .line 460
    check-cast v2, Ljava/lang/Boolean;

    .end local v2    # "oStatus":Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 466
    :cond_0
    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    return-object v3

    .line 462
    :catch_0
    move-exception v0

    .line 464
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetFUMODownloadResultCode()Ljava/lang/String;
    .locals 4

    .prologue
    .line 549
    const-string v2, ""

    .line 552
    .local v2, "szDownloadResultCode":Ljava/lang/String;
    const/16 v3, 0xde

    :try_start_0
    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 559
    :goto_0
    return-object v2

    .line 554
    :catch_0
    move-exception v1

    .line 556
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetFUMOInfo()Lcom/policydm/db/XDBFumoInfo;
    .locals 4

    .prologue
    .line 20
    new-instance v2, Lcom/policydm/db/XDBFumoInfo;

    invoke-direct {v2}, Lcom/policydm/db/XDBFumoInfo;-><init>()V

    .line 24
    .local v2, "ptObjFUMO":Lcom/policydm/db/XDBFumoInfo;
    const/16 v3, 0xc8

    :try_start_0
    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/policydm/db/XDBFumoInfo;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 30
    :goto_0
    return-object v2

    .line 26
    :catch_0
    move-exception v1

    .line 28
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetFUMOInitiatedType()I
    .locals 4

    .prologue
    .line 577
    const/4 v1, 0x0

    .line 580
    .local v1, "nInitType":I
    const/16 v2, 0xdf

    :try_start_0
    invoke-static {v2}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 586
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Initiated Type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 587
    return v1

    .line 582
    :catch_0
    move-exception v0

    .line 584
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetFUMOObjectSize()I
    .locals 4

    .prologue
    .line 383
    const/4 v1, 0x0

    .line 387
    .local v1, "nSize":I
    const/16 v3, 0xd9

    :try_start_0
    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v2

    .line 388
    .local v2, "oStatus":Ljava/lang/Object;
    if-eqz v2, :cond_0

    .line 389
    check-cast v2, Ljava/lang/Integer;

    .end local v2    # "oStatus":Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 395
    :cond_0
    :goto_0
    return v1

    .line 391
    :catch_0
    move-exception v0

    .line 393
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetFUMOPkgName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 314
    const-string v2, ""

    .line 317
    .local v2, "szPkgName":Ljava/lang/String;
    const/16 v3, 0xd5

    :try_start_0
    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 323
    :goto_0
    return-object v2

    .line 319
    :catch_0
    move-exception v1

    .line 321
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetFUMOPkgVersion()Ljava/lang/String;
    .locals 4

    .prologue
    .line 328
    const-string v2, ""

    .line 331
    .local v2, "szPkgVer":Ljava/lang/String;
    const/16 v3, 0xd6

    :try_start_0
    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 337
    :goto_0
    return-object v2

    .line 333
    :catch_0
    move-exception v1

    .line 335
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetFUMOProcessId()Ljava/lang/String;
    .locals 4

    .prologue
    .line 369
    const-string v2, ""

    .line 372
    .local v2, "szResultCode":Ljava/lang/String;
    const/16 v3, 0xd8

    :try_start_0
    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 378
    :goto_0
    return-object v2

    .line 374
    :catch_0
    move-exception v1

    .line 376
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetFUMOResultCode()Ljava/lang/String;
    .locals 4

    .prologue
    .line 523
    const-string v2, ""

    .line 526
    .local v2, "szResultCode":Ljava/lang/String;
    const/16 v3, 0xdd

    :try_start_0
    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 532
    :goto_0
    return-object v2

    .line 528
    :catch_0
    move-exception v1

    .line 530
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetFUMOStatus()I
    .locals 5

    .prologue
    .line 400
    const/4 v1, 0x0

    .line 402
    .local v1, "nStatus":I
    sget v3, Lcom/policydm/db/XDBFumoAdp;->gFumoStatus:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 404
    sget v1, Lcom/policydm/db/XDBFumoAdp;->gFumoStatus:I

    .line 427
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "xdbGetFUMOStatus : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 428
    return v1

    .line 408
    :cond_0
    const/4 v2, 0x0

    .line 411
    .local v2, "oStatus":Ljava/lang/Object;
    const/16 v3, 0xda

    :try_start_0
    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 417
    .end local v2    # "oStatus":Ljava/lang/Object;
    :goto_1
    if-eqz v2, :cond_1

    .line 419
    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0

    .line 413
    .restart local v2    # "oStatus":Ljava/lang/Object;
    :catch_0
    move-exception v0

    .line 415
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 423
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "oStatus":Ljava/lang/Object;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static xdbGetFUMOStatusAddr(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "szURL"    # Ljava/lang/String;

    .prologue
    .line 286
    const-string v2, ""

    .line 289
    .local v2, "szReportURI":Ljava/lang/String;
    const/16 v3, 0xd2

    :try_start_0
    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 295
    :goto_0
    return-object v2

    .line 291
    :catch_0
    move-exception v1

    .line 293
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetProtocol()Ljava/lang/String;
    .locals 4

    .prologue
    .line 49
    const-string v2, ""

    .line 52
    .local v2, "szProtocol":Ljava/lang/String;
    const/16 v3, 0x34

    :try_start_0
    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 58
    :goto_0
    return-object v2

    .line 54
    :catch_0
    move-exception v1

    .line 56
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetReportURL()Ljava/lang/String;
    .locals 4

    .prologue
    .line 241
    const-string v2, ""

    .line 244
    .local v2, "szProtocol":Ljava/lang/String;
    const/16 v3, 0xe0

    :try_start_0
    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 250
    :goto_0
    return-object v2

    .line 246
    :catch_0
    move-exception v1

    .line 248
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetFUMOCorrelator(Ljava/lang/String;)V
    .locals 2
    .param p0, "szCorrelator"    # Ljava/lang/String;

    .prologue
    .line 485
    const/16 v1, 0xdc

    :try_start_0
    invoke-static {v1, p0}, Lcom/policydm/db/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 491
    :goto_0
    return-void

    .line 487
    :catch_0
    move-exception v0

    .line 489
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetFUMODownloadEmptyServerUrl()V
    .locals 3

    .prologue
    .line 270
    const-string v1, ""

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 273
    const/16 v1, 0xc9

    :try_start_0
    const-string v2, ""

    invoke-static {v1, v2}, Lcom/policydm/db/XDB;->xdbWrite(ILjava/lang/Object;)V

    .line 274
    const/16 v1, 0xca

    const-string v2, ""

    invoke-static {v1, v2}, Lcom/policydm/db/XDB;->xdbWrite(ILjava/lang/Object;)V

    .line 275
    const/16 v1, 0xcb

    const-string v2, ""

    invoke-static {v1, v2}, Lcom/policydm/db/XDB;->xdbWrite(ILjava/lang/Object;)V

    .line 276
    const/16 v1, 0xcc

    const/16 v2, 0x50

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/policydm/db/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 282
    .local v0, "e":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 278
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_0
    move-exception v0

    .line 280
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetFUMODownloadMode(Ljava/lang/Boolean;)V
    .locals 3
    .param p0, "nDownloadMode"    # Ljava/lang/Boolean;

    .prologue
    .line 473
    const/16 v1, 0xdb

    :try_start_0
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/policydm/db/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 479
    :goto_0
    return-void

    .line 475
    :catch_0
    move-exception v0

    .line 477
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetFUMODownloadResultCode(Ljava/lang/String;)V
    .locals 2
    .param p0, "szDownloadResultCode"    # Ljava/lang/String;

    .prologue
    .line 539
    const/16 v1, 0xde

    :try_start_0
    invoke-static {v1, p0}, Lcom/policydm/db/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 545
    :goto_0
    return-void

    .line 541
    :catch_0
    move-exception v0

    .line 543
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetFUMOInfo(Ljava/lang/Object;)V
    .locals 3
    .param p0, "ptObj"    # Ljava/lang/Object;

    .prologue
    .line 35
    move-object v1, p0

    check-cast v1, Lcom/policydm/db/XDBFumoInfo;

    .line 39
    .local v1, "ptObjFUMO":Lcom/policydm/db/XDBFumoInfo;
    const/16 v2, 0xc8

    :try_start_0
    invoke-static {v2, v1}, Lcom/policydm/db/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 45
    :goto_0
    return-void

    .line 41
    :catch_0
    move-exception v0

    .line 43
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetFUMOInitiatedType(I)V
    .locals 3
    .param p0, "nInitType"    # I

    .prologue
    .line 564
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Initiated Type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 567
    const/16 v1, 0xdf

    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/policydm/db/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 573
    :goto_0
    return-void

    .line 569
    :catch_0
    move-exception v0

    .line 571
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetFUMOObjectServerUrl(Ljava/lang/String;)Z
    .locals 13
    .param p0, "szURL"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x0

    .line 158
    const/16 v11, 0x100

    new-array v2, v11, [C

    .line 160
    .local v2, "aServerAddr":[C
    const/4 v4, 0x0

    .line 161
    .local v4, "aTempServerAddr":[C
    const/16 v11, 0x10

    new-array v1, v11, [C

    .line 162
    .local v1, "aProtocol":[C
    const/4 v11, 0x6

    new-array v3, v11, [C

    .line 164
    .local v3, "aTempPort":[C
    const/4 v9, 0x0

    .line 167
    .local v9, "nPort":I
    move-object v0, p0

    .line 170
    .local v0, "DecodingUrl":Ljava/lang/String;
    :try_start_0
    const-string v11, "utf-8"

    invoke-static {p0, v11}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 181
    :goto_0
    :try_start_1
    invoke-static {v0}, Lcom/policydm/tp/XTPHttpUtil;->xtpURLParser(Ljava/lang/String;)Lcom/policydm/db/XDBUrlInfo;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v8

    .line 182
    .local v8, "getParser":Lcom/policydm/db/XDBUrlInfo;
    if-nez v8, :cond_1

    .line 235
    .end local v8    # "getParser":Lcom/policydm/db/XDBUrlInfo;
    :cond_0
    :goto_1
    return v10

    .line 172
    :catch_0
    move-exception v6

    .line 175
    .local v6, "e1":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v6}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0

    .line 187
    .end local v6    # "e1":Ljava/io/UnsupportedEncodingException;
    .restart local v8    # "getParser":Lcom/policydm/db/XDBUrlInfo;
    :cond_1
    :try_start_2
    iget v9, v8, Lcom/policydm/db/XDBUrlInfo;->nPort:I

    .line 188
    iget-object p0, v8, Lcom/policydm/db/XDBUrlInfo;->pURL:Ljava/lang/String;

    .line 189
    iget-object v11, v8, Lcom/policydm/db/XDBUrlInfo;->pAddress:Ljava/lang/String;

    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    .line 190
    iget-object v11, v8, Lcom/policydm/db/XDBUrlInfo;->pProtocol:Ljava/lang/String;

    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    .line 198
    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    .line 199
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    invoke-static {v11, v3}, Lcom/policydm/db/XDB;->xdbDoDMBootStrapURI([C[C)[C

    move-result-object v4

    .line 201
    if-eqz v4, :cond_0

    .line 206
    invoke-static {v4}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/policydm/tp/XTPHttpUtil;->xtpURLParser(Ljava/lang/String;)Lcom/policydm/db/XDBUrlInfo;

    move-result-object v8

    .line 208
    iget-object v11, v8, Lcom/policydm/db/XDBUrlInfo;->pAddress:Ljava/lang/String;

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 210
    const-string v11, "Parsing Error"

    invoke-static {v11}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 232
    .end local v8    # "getParser":Lcom/policydm/db/XDBUrlInfo;
    :catch_1
    move-exception v7

    .line 234
    .local v7, "ex":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 214
    .end local v7    # "ex":Ljava/lang/Exception;
    .restart local v8    # "getParser":Lcom/policydm/db/XDBUrlInfo;
    :cond_2
    :try_start_3
    iget v9, v8, Lcom/policydm/db/XDBUrlInfo;->nPort:I

    .line 215
    iget-object p0, v8, Lcom/policydm/db/XDBUrlInfo;->pURL:Ljava/lang/String;

    .line 216
    iget-object v11, v8, Lcom/policydm/db/XDBUrlInfo;->pProtocol:Ljava/lang/String;

    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    .line 217
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "aProtocol = ["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, v8, Lcom/policydm/db/XDBUrlInfo;->pProtocol:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "] "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "pURL = ["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "] aServerAddr = ["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, v8, Lcom/policydm/db/XDBUrlInfo;->pAddress:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "] nPort = ["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, v8, Lcom/policydm/db/XDBUrlInfo;->nPort:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "]"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 221
    const/16 v11, 0xcd

    :try_start_4
    invoke-static {v1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/policydm/db/XDB;->xdbWrite(ILjava/lang/Object;)V

    .line 222
    const/16 v11, 0xce

    invoke-static {v11, p0}, Lcom/policydm/db/XDB;->xdbWrite(ILjava/lang/Object;)V

    .line 223
    const/16 v11, 0xcf

    invoke-static {v2}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/policydm/db/XDB;->xdbWrite(ILjava/lang/Object;)V

    .line 224
    const/16 v11, 0xd0

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/policydm/db/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    .line 230
    :goto_2
    const/4 v10, 0x1

    goto/16 :goto_1

    .line 226
    :catch_2
    move-exception v5

    .line 228
    .local v5, "e":Ljava/lang/Exception;
    :try_start_5
    invoke-virtual {v5}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_2
.end method

.method public static xdbSetFUMOProcessId(Ljava/lang/String;)V
    .locals 2
    .param p0, "szResultCode"    # Ljava/lang/String;

    .prologue
    .line 359
    const/16 v1, 0xd8

    :try_start_0
    invoke-static {v1, p0}, Lcom/policydm/db/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 365
    :goto_0
    return-void

    .line 361
    :catch_0
    move-exception v0

    .line 363
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetFUMOResultCode(Ljava/lang/String;)V
    .locals 2
    .param p0, "szResultCode"    # Ljava/lang/String;

    .prologue
    .line 513
    const/16 v1, 0xdd

    :try_start_0
    invoke-static {v1, p0}, Lcom/policydm/db/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 519
    :goto_0
    return-void

    .line 515
    :catch_0
    move-exception v0

    .line 517
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetFUMOServerUrl(Ljava/lang/String;)Z
    .locals 11
    .param p0, "szURL"    # Ljava/lang/String;

    .prologue
    .line 77
    const/16 v9, 0x100

    new-array v1, v9, [C

    .line 79
    .local v1, "aServerAddr":[C
    const/4 v3, 0x0

    .line 80
    .local v3, "aTempServerAddr":[C
    const/16 v9, 0x10

    new-array v0, v9, [C

    .line 81
    .local v0, "aProtocol":[C
    const/4 v9, 0x6

    new-array v2, v9, [C

    .line 83
    .local v2, "aTempPort":[C
    const/4 v7, 0x0

    .line 84
    .local v7, "nPort":I
    const/4 v8, 0x1

    .line 87
    .local v8, "ret":Z
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "xdbSetFUMOServerUrl pURL: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 91
    :try_start_0
    invoke-static {p0}, Lcom/policydm/tp/XTPHttpUtil;->xtpURLParser(Ljava/lang/String;)Lcom/policydm/db/XDBUrlInfo;

    move-result-object v6

    .line 92
    .local v6, "getParser":Lcom/policydm/db/XDBUrlInfo;
    if-nez v6, :cond_0

    .line 95
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMODownloadEmptyServerUrl()V

    .line 152
    .end local v6    # "getParser":Lcom/policydm/db/XDBUrlInfo;
    :goto_0
    return v8

    .line 99
    .restart local v6    # "getParser":Lcom/policydm/db/XDBUrlInfo;
    :cond_0
    iget v7, v6, Lcom/policydm/db/XDBUrlInfo;->nPort:I

    .line 100
    iget-object p0, v6, Lcom/policydm/db/XDBUrlInfo;->pURL:Ljava/lang/String;

    .line 101
    iget-object v9, v6, Lcom/policydm/db/XDBUrlInfo;->pAddress:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    .line 102
    iget-object v9, v6, Lcom/policydm/db/XDBUrlInfo;->pProtocol:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    .line 110
    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    .line 111
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v9

    invoke-static {v9, v2}, Lcom/policydm/db/XDB;->xdbDoDMBootStrapURI([C[C)[C

    move-result-object v3

    .line 113
    if-nez v3, :cond_1

    .line 115
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMODownloadEmptyServerUrl()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 148
    .end local v6    # "getParser":Lcom/policydm/db/XDBUrlInfo;
    :catch_0
    move-exception v5

    .line 150
    .local v5, "ex":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 151
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMODownloadEmptyServerUrl()V

    goto :goto_0

    .line 119
    .end local v5    # "ex":Ljava/lang/Exception;
    .restart local v6    # "getParser":Lcom/policydm/db/XDBUrlInfo;
    :cond_1
    :try_start_1
    invoke-static {v3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/policydm/tp/XTPHttpUtil;->xtpURLParser(Ljava/lang/String;)Lcom/policydm/db/XDBUrlInfo;

    move-result-object v6

    .line 121
    iget-object v9, v6, Lcom/policydm/db/XDBUrlInfo;->pAddress:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 124
    const-string v9, "Parsing Error"

    invoke-static {v9}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 125
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMODownloadEmptyServerUrl()V

    goto :goto_0

    .line 129
    :cond_2
    iget v7, v6, Lcom/policydm/db/XDBUrlInfo;->nPort:I

    .line 130
    iget-object p0, v6, Lcom/policydm/db/XDBUrlInfo;->pURL:Ljava/lang/String;

    .line 131
    iget-object v9, v6, Lcom/policydm/db/XDBUrlInfo;->pProtocol:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    .line 132
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "aProtocol = ["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v6, Lcom/policydm/db/XDBUrlInfo;->pProtocol:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "] "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "pURL = ["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "] aServerAddr = ["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v6, Lcom/policydm/db/XDBUrlInfo;->pAddress:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "] nPort = ["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, v6, Lcom/policydm/db/XDBUrlInfo;->nPort:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "]"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 136
    const/16 v9, 0xc9

    :try_start_2
    invoke-static {v0}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/policydm/db/XDB;->xdbWrite(ILjava/lang/Object;)V

    .line 137
    const/16 v9, 0xca

    invoke-static {v9, p0}, Lcom/policydm/db/XDB;->xdbWrite(ILjava/lang/Object;)V

    .line 138
    const/16 v9, 0xcb

    invoke-static {v1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/policydm/db/XDB;->xdbWrite(ILjava/lang/Object;)V

    .line 139
    const/16 v9, 0xcc

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/policydm/db/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 141
    :catch_1
    move-exception v4

    .line 143
    .local v4, "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_0
.end method

.method public static xdbSetFUMOStatus(I)V
    .locals 4
    .param p0, "nstatus"    # I

    .prologue
    .line 435
    sget v1, Lcom/policydm/db/XDBFumoAdp;->gFumoStatus:I

    .line 436
    .local v1, "nBuckupStatus":I
    sput p0, Lcom/policydm/db/XDBFumoAdp;->gFumoStatus:I

    .line 438
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "xdbSetFUMOStatus : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 441
    const/16 v2, 0xda

    :try_start_0
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/policydm/db/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 450
    :goto_0
    return-void

    .line 443
    :catch_0
    move-exception v0

    .line 445
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 447
    sput v1, Lcom/policydm/db/XDBFumoAdp;->gFumoStatus:I

    .line 448
    const-string v2, "xdbSetFUMOStatus db write was failed"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetReportUrl(Ljava/lang/String;)Z
    .locals 2
    .param p0, "szURL"    # Ljava/lang/String;

    .prologue
    .line 257
    const/16 v1, 0xe0

    :try_start_0
    invoke-static {v1, p0}, Lcom/policydm/db/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 265
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 259
    :catch_0
    move-exception v0

    .line 261
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 262
    const/4 v1, 0x0

    goto :goto_0
.end method
