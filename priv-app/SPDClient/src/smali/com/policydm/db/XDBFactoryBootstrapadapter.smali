.class public Lcom/policydm/db/XDBFactoryBootstrapadapter;
.super Ljava/lang/Object;
.source "XDBFactoryBootstrapadapter.java"


# static fields
.field private static g_szPasswdDict:[[B

.field private static szDict:[B

.field private static final xdb_hexTable:[C


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/16 v3, 0xf

    .line 13
    const/4 v0, 0x3

    new-array v0, v0, [[B

    const/4 v1, 0x0

    new-array v2, v3, [B

    fill-array-data v2, :array_0

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-array v2, v3, [B

    fill-array-data v2, :array_1

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-array v2, v3, [B

    fill-array-data v2, :array_2

    aput-object v2, v0, v1

    sput-object v0, Lcom/policydm/db/XDBFactoryBootstrapadapter;->g_szPasswdDict:[[B

    .line 20
    const/16 v0, 0x28

    new-array v0, v0, [B

    fill-array-data v0, :array_3

    sput-object v0, Lcom/policydm/db/XDBFactoryBootstrapadapter;->szDict:[B

    .line 57
    const/16 v0, 0x10

    new-array v0, v0, [C

    fill-array-data v0, :array_4

    sput-object v0, Lcom/policydm/db/XDBFactoryBootstrapadapter;->xdb_hexTable:[C

    return-void

    .line 13
    :array_0
    .array-data 1
        0xet
        0x6t
        0x10t
        0xct
        0xat
        0xet
        0x5t
        0xct
        0x12t
        0xat
        0xbt
        0x6t
        0xdt
        0xet
        0x5t
    .end array-data

    :array_1
    .array-data 1
        0xat
        0x6t
        0xet
        0xet
        0xat
        0xbt
        0x6t
        0xet
        0xbt
        0x4t
        0x4t
        0x7t
        0x11t
        0xct
        0xct
    .end array-data

    :array_2
    .array-data 1
        0xat
        0x6t
        0x10t
        0xet
        0xat
        0xbt
        0x5t
        0xet
        0x12t
        0x4t
        0xbt
        0x7t
        0xdt
        0xct
        0x5t
    .end array-data

    .line 20
    :array_3
    .array-data 1
        0x1t
        0xft
        0x5t
        0xbt
        0x13t
        0x1ct
        0x17t
        0x2ft
        0x23t
        0x2ct
        0x2t
        0xet
        0x6t
        0xat
        0x12t
        0xdt
        0x16t
        0x1at
        0x20t
        0x2ft
        0x3t
        0xdt
        0x7t
        0x9t
        0x11t
        0x1et
        0x15t
        0x19t
        0x21t
        0x2dt
        0x4t
        0xct
        0x8t
        0x3ft
        0x10t
        0x1ft
        0x14t
        0x18t
        0x22t
        0x2et
    .end array-data

    .line 57
    :array_4
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x61s
        0x62s
        0x63s
        0x64s
        0x65s
        0x66s
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xdbFBAdpEncodeHex([B)[C
    .locals 8
    .param p0, "data"    # [B

    .prologue
    .line 69
    array-length v4, p0

    .line 70
    .local v4, "len":I
    mul-int/lit8 v6, v4, 0x2

    new-array v5, v6, [C

    .line 71
    .local v5, "output":[C
    const/4 v2, 0x0

    .line 73
    .local v2, "j":I
    const/4 v1, 0x0

    .local v1, "i":I
    move v3, v2

    .end local v2    # "j":I
    .local v3, "j":I
    :goto_0
    if-ge v1, v4, :cond_0

    .line 75
    aget-byte v0, p0, v1

    .line 77
    .local v0, "d":B
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "j":I
    .restart local v2    # "j":I
    sget-object v6, Lcom/policydm/db/XDBFactoryBootstrapadapter;->xdb_hexTable:[C

    and-int/lit8 v7, v0, 0xf

    aget-char v6, v6, v7

    aput-char v6, v5, v3

    .line 78
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "j":I
    .restart local v3    # "j":I
    sget-object v6, Lcom/policydm/db/XDBFactoryBootstrapadapter;->xdb_hexTable:[C

    ushr-int/lit8 v7, v0, 0x4

    and-int/lit8 v7, v7, 0xf

    aget-char v6, v6, v7

    aput-char v6, v5, v2

    .line 73
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 81
    .end local v0    # "d":B
    :cond_0
    return-object v5
.end method

.method public static xdbFBAdpGeneratePasswd(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .locals 21
    .param p0, "szDeviceId"    # Ljava/lang/String;
    .param p1, "szServerId"    # Ljava/lang/String;
    .param p2, "nKeyType"    # I

    .prologue
    .line 87
    const-string v15, ""

    .line 88
    .local v15, "szKey":Ljava/lang/String;
    const/4 v11, 0x0

    .line 89
    .local v11, "szBuf":[B
    const-string v17, ""

    .line 91
    .local v17, "szTmpBuf":Ljava/lang/String;
    const/4 v6, 0x0

    .line 92
    .local v6, "devidSize":I
    const/4 v4, 0x0

    .line 93
    .local v4, "cnt":I
    const-string v16, ""

    .line 94
    .local v16, "szTempPasswd":Ljava/lang/String;
    const-string v13, ""

    .line 96
    .local v13, "szDest":Ljava/lang/String;
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-static {v0, v1}, Lcom/policydm/db/XDBFactoryBootstrapadapter;->xdbFBAdpGeneratePasswdKey(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v15

    .line 98
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 99
    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v18

    move/from16 v0, v18

    new-array v11, v0, [B

    .line 100
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v11

    .line 104
    :try_start_0
    const-string v18, "MD5"

    invoke-static/range {v18 .. v18}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v10

    .line 105
    .local v10, "md5Digest":Ljava/security/MessageDigest;
    invoke-virtual {v10}, Ljava/security/MessageDigest;->reset()V

    .line 107
    invoke-virtual {v10, v11}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v7

    .line 108
    .local v7, "digest":[B
    invoke-static {v7}, Lcom/policydm/db/XDBFactoryBootstrapadapter;->xdbFBAdpEncodeHex([B)[C

    move-result-object v9

    .line 110
    .local v9, "encodedStr1":[C
    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v6

    .line 113
    const/16 v18, 0x0

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v19

    add-int/lit8 v20, v6, -0x2

    aget-byte v19, v19, v20

    aput-byte v19, v11, v18

    .line 114
    const/16 v18, 0x1

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v19

    add-int/lit8 v20, v6, -0x1

    aget-byte v19, v19, v20

    aput-byte v19, v11, v18

    .line 116
    new-instance v5, Lcom/policydm/db/XDBCrypt;

    invoke-direct {v5}, Lcom/policydm/db/XDBCrypt;-><init>()V

    .line 117
    .local v5, "cryptData":Lcom/policydm/db/XDBCrypt;
    move-object/from16 v0, p0

    invoke-virtual {v5, v0, v11}, Lcom/policydm/db/XDBCrypt;->xdbCryptGenerate(Ljava/lang/String;[B)Ljava/lang/String;

    move-result-object v12

    .line 118
    .local v12, "szCrypteDeviceID":Ljava/lang/String;
    invoke-virtual {v12}, Ljava/lang/String;->toCharArray()[C

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/policydm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v12

    .line 121
    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v3, v0, [C

    .line 122
    .local v3, "cBuf":[C
    const/16 v18, 0x0

    const/16 v19, 0x2

    aget-char v19, v9, v19

    aput-char v19, v3, v18

    .line 123
    const/16 v18, 0x1

    const/16 v19, 0x4

    aget-char v19, v9, v19

    aput-char v19, v3, v18

    .line 124
    const/16 v18, 0x2

    const/16 v19, 0x8

    aget-char v19, v9, v19

    aput-char v19, v3, v18

    .line 126
    invoke-static {v3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v16

    .line 127
    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 129
    const/4 v2, 0x0

    .line 130
    .local v2, "SBuffer":Ljava/lang/StringBuffer;
    new-instance v18, Ljava/lang/StringBuffer;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuffer;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    .line 133
    if-nez p2, :cond_0

    .line 135
    const/4 v4, 0x0

    :goto_0
    const/16 v18, 0x3

    move/from16 v0, v18

    if-ge v4, v0, :cond_2

    .line 137
    invoke-static {v2}, Lcom/policydm/db/XDBFactoryBootstrapadapter;->xdbFBAdpShuffle(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    move-result-object v2

    .line 135
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 141
    :cond_0
    const/16 v18, 0x1

    move/from16 v0, p2

    move/from16 v1, v18

    if-ne v0, v1, :cond_1

    .line 143
    const/4 v4, 0x0

    :goto_1
    const/16 v18, 0x2

    move/from16 v0, v18

    if-ge v4, v0, :cond_2

    .line 145
    invoke-static {v2}, Lcom/policydm/db/XDBFactoryBootstrapadapter;->xdbFBAdpShuffle(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    move-result-object v2

    .line 143
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 151
    :cond_1
    const/4 v4, 0x0

    :goto_2
    const/16 v18, 0x4

    move/from16 v0, v18

    if-ge v4, v0, :cond_2

    .line 153
    invoke-static {v2}, Lcom/policydm/db/XDBFactoryBootstrapadapter;->xdbFBAdpShuffle(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    move-result-object v2

    .line 151
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 157
    :cond_2
    new-instance v14, Ljava/lang/String;

    invoke-direct {v14, v2}, Ljava/lang/String;-><init>(Ljava/lang/StringBuffer;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v13    # "szDest":Ljava/lang/String;
    .local v14, "szDest":Ljava/lang/String;
    move-object v13, v14

    .line 165
    .end local v2    # "SBuffer":Ljava/lang/StringBuffer;
    .end local v3    # "cBuf":[C
    .end local v5    # "cryptData":Lcom/policydm/db/XDBCrypt;
    .end local v7    # "digest":[B
    .end local v9    # "encodedStr1":[C
    .end local v10    # "md5Digest":Ljava/security/MessageDigest;
    .end local v12    # "szCrypteDeviceID":Ljava/lang/String;
    .end local v14    # "szDest":Ljava/lang/String;
    .restart local v13    # "szDest":Ljava/lang/String;
    :goto_3
    return-object v13

    .line 160
    :catch_0
    move-exception v8

    .line 162
    .local v8, "e":Ljava/lang/Exception;
    invoke-virtual {v8}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_3
.end method

.method private static xdbFBAdpGeneratePasswdKey(Ljava/lang/String;I)Ljava/lang/String;
    .locals 8
    .param p0, "szDeviceId"    # Ljava/lang/String;
    .param p1, "type"    # I

    .prologue
    .line 170
    const/4 v0, 0x0

    .line 171
    .local v0, "i":I
    const/4 v1, 0x0

    .line 172
    .local v1, "nLen":I
    const/4 v3, 0x0

    .local v3, "serial1":I
    const/4 v4, 0x0

    .line 173
    .local v4, "serial2":I
    const-string v5, ""

    .line 175
    .local v5, "szKeyStr":Ljava/lang/String;
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    .line 176
    new-array v2, v1, [B

    .line 177
    .local v2, "pDeviceId":[B
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v6

    invoke-virtual {p0, v6}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v2

    .line 179
    const/4 v0, 0x0

    :goto_0
    add-int/lit8 v6, v1, -0x3

    if-ge v0, v6, :cond_0

    .line 181
    add-int/lit8 v6, v0, 0x3

    aget-byte v6, v2, v6

    sget-object v7, Lcom/policydm/db/XDBFactoryBootstrapadapter;->g_szPasswdDict:[[B

    aget-object v7, v7, p1

    aget-byte v7, v7, v0

    mul-int/2addr v6, v7

    add-int/2addr v3, v6

    .line 182
    add-int/lit8 v6, v0, 0x3

    aget-byte v6, v2, v6

    add-int/lit8 v7, v0, 0x2

    aget-byte v7, v2, v7

    mul-int/2addr v6, v7

    sget-object v7, Lcom/policydm/db/XDBFactoryBootstrapadapter;->g_szPasswdDict:[[B

    aget-object v7, v7, p1

    aget-byte v7, v7, v0

    mul-int/2addr v6, v7

    add-int/2addr v4, v6

    .line 179
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 185
    :cond_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 186
    return-object v5
.end method

.method public static xdbFBAdpSPDGenerateDevicePwd(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 21
    .param p0, "szDeviceId"    # Ljava/lang/String;
    .param p1, "szServerId"    # Ljava/lang/String;

    .prologue
    .line 216
    const-string v17, ""

    .line 217
    .local v17, "szTmpBuf":Ljava/lang/String;
    const-string v15, ""

    .line 218
    .local v15, "szKey":Ljava/lang/String;
    const-string v13, ""

    .line 219
    .local v13, "szDevicePw":Ljava/lang/String;
    const-string v12, ""

    .line 220
    .local v12, "szCryptResult":Ljava/lang/String;
    const-string v16, ""

    .line 222
    .local v16, "szTempPasswd":Ljava/lang/String;
    const/4 v11, 0x0

    .line 224
    .local v11, "szBuf":[B
    const/4 v6, 0x0

    .line 225
    .local v6, "devidSize":I
    const/4 v4, 0x0

    .line 227
    .local v4, "cnt":I
    invoke-static/range {p0 .. p0}, Lcom/policydm/db/XDBFactoryBootstrapadapter;->xdbFBAdpSPDGeneratePwdKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 228
    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 229
    const/16 v18, 0x0

    .line 276
    :goto_0
    return-object v18

    .line 231
    :cond_0
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 232
    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v18

    move/from16 v0, v18

    new-array v11, v0, [B

    .line 233
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v11

    .line 237
    :try_start_0
    const-string v18, "MD5"

    invoke-static/range {v18 .. v18}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v10

    .line 238
    .local v10, "md5Digest":Ljava/security/MessageDigest;
    invoke-virtual {v10}, Ljava/security/MessageDigest;->reset()V

    .line 240
    invoke-virtual {v10, v11}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v7

    .line 241
    .local v7, "digest":[B
    invoke-static {v7}, Lcom/policydm/db/XDBFactoryBootstrapadapter;->xdbFBAdpEncodeHex([B)[C

    move-result-object v8

    .line 243
    .local v8, "digestHex":[C
    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v6

    .line 246
    const/16 v18, 0x0

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v19

    add-int/lit8 v20, v6, -0x2

    aget-byte v19, v19, v20

    aput-byte v19, v11, v18

    .line 247
    const/16 v18, 0x1

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v19

    add-int/lit8 v20, v6, -0x1

    aget-byte v19, v19, v20

    aput-byte v19, v11, v18

    .line 249
    new-instance v5, Lcom/policydm/db/XDBCrypt;

    invoke-direct {v5}, Lcom/policydm/db/XDBCrypt;-><init>()V

    .line 250
    .local v5, "cryptData":Lcom/policydm/db/XDBCrypt;
    move-object/from16 v0, p0

    invoke-virtual {v5, v0, v11}, Lcom/policydm/db/XDBCrypt;->xdbCryptGenerate(Ljava/lang/String;[B)Ljava/lang/String;

    move-result-object v12

    .line 251
    invoke-virtual {v12}, Ljava/lang/String;->toCharArray()[C

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/policydm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v12

    .line 253
    const/16 v18, 0x4

    move/from16 v0, v18

    new-array v3, v0, [C

    .line 254
    .local v3, "cBuf":[C
    const/16 v18, 0x0

    const/16 v19, 0x1

    aget-char v19, v8, v19

    aput-char v19, v3, v18

    .line 255
    const/16 v18, 0x1

    const/16 v19, 0x4

    aget-char v19, v8, v19

    aput-char v19, v3, v18

    .line 256
    const/16 v18, 0x2

    const/16 v19, 0x5

    aget-char v19, v8, v19

    aput-char v19, v3, v18

    .line 257
    const/16 v18, 0x3

    const/16 v19, 0x7

    aget-char v19, v8, v19

    aput-char v19, v3, v18

    .line 259
    invoke-static {v3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v16

    .line 260
    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 262
    const/4 v2, 0x0

    .line 263
    .local v2, "SBuffer":Ljava/lang/StringBuffer;
    new-instance v18, Ljava/lang/StringBuffer;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuffer;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    .line 265
    const/4 v4, 0x0

    :goto_1
    const/16 v18, 0x3

    move/from16 v0, v18

    if-ge v4, v0, :cond_1

    .line 267
    invoke-static {v2}, Lcom/policydm/db/XDBFactoryBootstrapadapter;->xdbFBAdpShuffle(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    move-result-object v2

    .line 265
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 269
    :cond_1
    new-instance v14, Ljava/lang/String;

    invoke-direct {v14, v2}, Ljava/lang/String;-><init>(Ljava/lang/StringBuffer;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v13    # "szDevicePw":Ljava/lang/String;
    .local v14, "szDevicePw":Ljava/lang/String;
    move-object v13, v14

    .end local v2    # "SBuffer":Ljava/lang/StringBuffer;
    .end local v3    # "cBuf":[C
    .end local v5    # "cryptData":Lcom/policydm/db/XDBCrypt;
    .end local v7    # "digest":[B
    .end local v8    # "digestHex":[C
    .end local v10    # "md5Digest":Ljava/security/MessageDigest;
    .end local v14    # "szDevicePw":Ljava/lang/String;
    .restart local v13    # "szDevicePw":Ljava/lang/String;
    :goto_2
    move-object/from16 v18, v13

    .line 276
    goto/16 :goto_0

    .line 271
    :catch_0
    move-exception v9

    .line 273
    .local v9, "e":Ljava/lang/Exception;
    invoke-virtual {v9}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_2
.end method

.method private static xdbFBAdpSPDGeneratePwdKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p0, "szId"    # Ljava/lang/String;

    .prologue
    .line 191
    const-string v7, ""

    .line 192
    .local v7, "szKey":Ljava/lang/String;
    const-wide/16 v3, 0x0

    .local v3, "serialNum1":J
    const-wide/16 v5, 0x0

    .line 193
    .local v5, "serialNum2":J
    const/4 v8, 0x0

    .line 195
    .local v8, "szTmp":Ljava/lang/String;
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    .line 196
    .local v1, "nLen":I
    new-array v2, v1, [B

    .line 197
    .local v2, "pDeviceId":[B
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v9

    invoke-virtual {p0, v9}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v2

    .line 199
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    add-int/lit8 v9, v1, -0x1

    if-ge v0, v9, :cond_0

    .line 201
    aget-byte v9, v2, v0

    sget-object v10, Lcom/policydm/db/XDBFactoryBootstrapadapter;->szDict:[B

    aget-byte v10, v10, v0

    mul-int/2addr v9, v10

    int-to-long v9, v9

    add-long/2addr v3, v9

    .line 202
    aget-byte v9, v2, v0

    sub-int v10, v1, v0

    add-int/lit8 v10, v10, -0x1

    aget-byte v10, v2, v10

    mul-int/2addr v9, v10

    sget-object v10, Lcom/policydm/db/XDBFactoryBootstrapadapter;->szDict:[B

    aget-byte v10, v10, v0

    mul-int/2addr v9, v10

    int-to-long v9, v9

    add-long/2addr v5, v9

    .line 199
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 204
    :cond_0
    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    .line 205
    invoke-virtual {v7, v8}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 207
    const/4 v8, 0x0

    .line 208
    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    .line 209
    invoke-virtual {v7, v8}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 211
    return-object v7
.end method

.method public static xdbFBAdpSPDGenerateServerPwd(Ljava/lang/String;)Ljava/lang/String;
    .locals 21
    .param p0, "szServerId"    # Ljava/lang/String;

    .prologue
    .line 281
    const-string v17, ""

    .line 282
    .local v17, "szTmpBuf":Ljava/lang/String;
    const-string v13, ""

    .line 283
    .local v13, "szKey":Ljava/lang/String;
    const-string v14, ""

    .line 284
    .local v14, "szServerPw":Ljava/lang/String;
    const-string v12, ""

    .line 285
    .local v12, "szCryptResult":Ljava/lang/String;
    const-string v16, ""

    .line 287
    .local v16, "szTempPasswd":Ljava/lang/String;
    const/4 v11, 0x0

    .line 289
    .local v11, "szBuf":[B
    const/4 v10, 0x0

    .line 290
    .local v10, "serveridSize":I
    const/4 v4, 0x0

    .line 292
    .local v4, "cnt":I
    invoke-static/range {p0 .. p0}, Lcom/policydm/db/XDBFactoryBootstrapadapter;->xdbFBAdpSPDGeneratePwdKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 293
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 294
    const/16 v18, 0x0

    .line 341
    :goto_0
    return-object v18

    .line 296
    :cond_0
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 297
    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v18

    move/from16 v0, v18

    new-array v11, v0, [B

    .line 298
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v11

    .line 302
    :try_start_0
    const-string v18, "MD5"

    invoke-static/range {v18 .. v18}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v9

    .line 303
    .local v9, "md5Digest":Ljava/security/MessageDigest;
    invoke-virtual {v9}, Ljava/security/MessageDigest;->reset()V

    .line 305
    invoke-virtual {v9, v11}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v6

    .line 306
    .local v6, "digest":[B
    invoke-static {v6}, Lcom/policydm/db/XDBFactoryBootstrapadapter;->xdbFBAdpEncodeHex([B)[C

    move-result-object v7

    .line 308
    .local v7, "digestHex":[C
    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v10

    .line 311
    const/16 v18, 0x0

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v19

    add-int/lit8 v20, v10, -0x2

    aget-byte v19, v19, v20

    aput-byte v19, v11, v18

    .line 312
    const/16 v18, 0x1

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v19

    add-int/lit8 v20, v10, -0x1

    aget-byte v19, v19, v20

    aput-byte v19, v11, v18

    .line 314
    new-instance v5, Lcom/policydm/db/XDBCrypt;

    invoke-direct {v5}, Lcom/policydm/db/XDBCrypt;-><init>()V

    .line 315
    .local v5, "cryptData":Lcom/policydm/db/XDBCrypt;
    move-object/from16 v0, p0

    invoke-virtual {v5, v0, v11}, Lcom/policydm/db/XDBCrypt;->xdbCryptGenerate(Ljava/lang/String;[B)Ljava/lang/String;

    move-result-object v12

    .line 316
    invoke-virtual {v12}, Ljava/lang/String;->toCharArray()[C

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/policydm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v12

    .line 318
    const/16 v18, 0x4

    move/from16 v0, v18

    new-array v3, v0, [C

    .line 319
    .local v3, "cBuf":[C
    const/16 v18, 0x0

    const/16 v19, 0x1

    aget-char v19, v7, v19

    aput-char v19, v3, v18

    .line 320
    const/16 v18, 0x1

    const/16 v19, 0x4

    aget-char v19, v7, v19

    aput-char v19, v3, v18

    .line 321
    const/16 v18, 0x2

    const/16 v19, 0x5

    aget-char v19, v7, v19

    aput-char v19, v3, v18

    .line 322
    const/16 v18, 0x3

    const/16 v19, 0x7

    aget-char v19, v7, v19

    aput-char v19, v3, v18

    .line 324
    invoke-static {v3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v16

    .line 325
    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 327
    const/4 v2, 0x0

    .line 328
    .local v2, "SBuffer":Ljava/lang/StringBuffer;
    new-instance v18, Ljava/lang/StringBuffer;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuffer;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    .line 330
    const/4 v4, 0x0

    :goto_1
    const/16 v18, 0x2

    move/from16 v0, v18

    if-ge v4, v0, :cond_1

    .line 332
    invoke-static {v2}, Lcom/policydm/db/XDBFactoryBootstrapadapter;->xdbFBAdpShuffle(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    move-result-object v2

    .line 330
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 334
    :cond_1
    new-instance v15, Ljava/lang/String;

    invoke-direct {v15, v2}, Ljava/lang/String;-><init>(Ljava/lang/StringBuffer;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v14    # "szServerPw":Ljava/lang/String;
    .local v15, "szServerPw":Ljava/lang/String;
    move-object v14, v15

    .end local v2    # "SBuffer":Ljava/lang/StringBuffer;
    .end local v3    # "cBuf":[C
    .end local v5    # "cryptData":Lcom/policydm/db/XDBCrypt;
    .end local v6    # "digest":[B
    .end local v7    # "digestHex":[C
    .end local v9    # "md5Digest":Ljava/security/MessageDigest;
    .end local v15    # "szServerPw":Ljava/lang/String;
    .restart local v14    # "szServerPw":Ljava/lang/String;
    :goto_2
    move-object/from16 v18, v14

    .line 341
    goto/16 :goto_0

    .line 336
    :catch_0
    move-exception v8

    .line 338
    .local v8, "e":Ljava/lang/Exception;
    invoke-virtual {v8}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public static xdbFBAdpShuffle(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;
    .locals 7
    .param p0, "buffer"    # Ljava/lang/StringBuffer;

    .prologue
    .line 30
    invoke-virtual {p0}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    .line 31
    .local v4, "nLen":I
    rem-int/lit8 v2, v4, 0x2

    .line 32
    .local v2, "mod":I
    if-nez v2, :cond_0

    div-int/lit8 v5, v4, 0x2

    .line 34
    .local v5, "secondHalfPos":I
    :goto_0
    move v1, v5

    .local v1, "i":I
    :goto_1
    if-ge v1, v4, :cond_2

    .line 36
    invoke-virtual {p0, v1}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v0

    .line 37
    .local v0, "ch":C
    invoke-virtual {p0, v1}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    .line 38
    if-nez v2, :cond_1

    sub-int v6, v4, v1

    add-int/lit8 v3, v6, -0x1

    .line 39
    .local v3, "nInsertPos":I
    :goto_2
    invoke-virtual {p0, v3, v0}, Ljava/lang/StringBuffer;->insert(IC)Ljava/lang/StringBuffer;

    .line 34
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 32
    .end local v0    # "ch":C
    .end local v1    # "i":I
    .end local v3    # "nInsertPos":I
    .end local v5    # "secondHalfPos":I
    :cond_0
    div-int/lit8 v6, v4, 0x2

    add-int/lit8 v5, v6, 0x1

    goto :goto_0

    .line 38
    .restart local v0    # "ch":C
    .restart local v1    # "i":I
    .restart local v5    # "secondHalfPos":I
    :cond_1
    sub-int v3, v4, v1

    goto :goto_2

    .line 49
    .end local v0    # "ch":C
    :cond_2
    return-object p0
.end method
