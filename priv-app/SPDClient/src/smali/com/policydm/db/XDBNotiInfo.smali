.class public Lcom/policydm/db/XDBNotiInfo;
.super Ljava/lang/Object;
.source "XDBNotiInfo.java"

# interfaces
.implements Lcom/policydm/interfaces/XDMInterface;
.implements Lcom/policydm/interfaces/XNOTIInterface;
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public appId:I

.field public jobId:J

.field public m_szServerId:Ljava/lang/String;

.field public m_szSessionId:Ljava/lang/String;

.field public opMode:I

.field public rowId:I

.field public uiMode:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput v1, p0, Lcom/policydm/db/XDBNotiInfo;->rowId:I

    .line 24
    iput v1, p0, Lcom/policydm/db/XDBNotiInfo;->appId:I

    .line 25
    const/4 v0, 0x1

    iput v0, p0, Lcom/policydm/db/XDBNotiInfo;->uiMode:I

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/db/XDBNotiInfo;->m_szSessionId:Ljava/lang/String;

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/db/XDBNotiInfo;->m_szServerId:Ljava/lang/String;

    .line 28
    iput v1, p0, Lcom/policydm/db/XDBNotiInfo;->opMode:I

    .line 29
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/policydm/db/XDBNotiInfo;->jobId:J

    .line 30
    return-void
.end method
