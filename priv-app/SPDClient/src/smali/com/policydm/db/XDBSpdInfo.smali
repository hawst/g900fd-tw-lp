.class public Lcom/policydm/db/XDBSpdInfo;
.super Ljava/lang/Object;
.source "XDBSpdInfo.java"

# interfaces
.implements Lcom/policydm/interfaces/XSPDInterface;
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public m_bInitialized:Z

.field public m_nDeviceCreate:I

.field public m_nDeviceRegi:I

.field public m_nDeviceUpdate:I

.field public m_nEulaTime:J

.field public m_nSPDState:I

.field public m_szPolicyMode:Ljava/lang/String;

.field public m_szPushRegID:Ljava/lang/String;

.field public m_szPushType:Ljava/lang/String;

.field public m_szSPDServerNonce:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput v2, p0, Lcom/policydm/db/XDBSpdInfo;->m_nDeviceRegi:I

    .line 26
    iput v2, p0, Lcom/policydm/db/XDBSpdInfo;->m_nDeviceCreate:I

    .line 27
    iput v2, p0, Lcom/policydm/db/XDBSpdInfo;->m_nDeviceUpdate:I

    .line 28
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/db/XDBSpdInfo;->m_szPushType:Ljava/lang/String;

    .line 29
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/db/XDBSpdInfo;->m_szPushRegID:Ljava/lang/String;

    .line 30
    iput v2, p0, Lcom/policydm/db/XDBSpdInfo;->m_nSPDState:I

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/db/XDBSpdInfo;->m_szSPDServerNonce:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/db/XDBSpdInfo;->m_szPolicyMode:Ljava/lang/String;

    .line 33
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/policydm/db/XDBSpdInfo;->m_nEulaTime:J

    .line 34
    iput-boolean v2, p0, Lcom/policydm/db/XDBSpdInfo;->m_bInitialized:Z

    .line 35
    return-void
.end method
