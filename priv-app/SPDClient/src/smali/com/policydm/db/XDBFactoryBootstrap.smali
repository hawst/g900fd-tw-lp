.class public Lcom/policydm/db/XDBFactoryBootstrap;
.super Ljava/lang/Object;
.source "XDBFactoryBootstrap.java"

# interfaces
.implements Lcom/policydm/db/XDBSql;
.implements Lcom/policydm/interfaces/XDMDefInterface;
.implements Lcom/policydm/interfaces/XDMInterface;
.implements Ljava/io/Serializable;


# static fields
.field public static final DEFAULT_NONCE:Ljava/lang/String; = "MTIzNDU="

.field public static final DM_SERVER_PATH:Ljava/lang/String; = "/oma.dm"

.field public static final DM_SERVER_URL:Ljava/lang/String; = "https://dm.spd.samsungdm.com"

.field public static final FACTORYBOOTSTRAP_CLIENTID:I = 0x1

.field public static final FACTORYBOOTSTRAP_CLIENTPWD:I = 0x2

.field public static final FACTORYBOOTSTRAP_SERVERPWD:I = 0x0

.field public static final SPD_AUTH_ID:Ljava/lang/String; = "SPD-AUTH"

.field public static SPD_ProfileName:[Ljava/lang/String; = null

.field public static final SPD_SERVER_ID:Ljava/lang/String; = "1m553ne3y9"

.field public static final SPD_SERVER_URL:Ljava/lang/String; = "https://svc.spd.samsungdm.com"

.field public static SPD_ServerID:[Ljava/lang/String; = null

.field public static SPD_ServerUrl:[Ljava/lang/String; = null

.field private static final serialVersionUID:J = 0x1L


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 31
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "1m553ne3y9"

    aput-object v1, v0, v2

    const-string v1, "1m553ne3y9"

    aput-object v1, v0, v3

    const-string v1, "mform"

    aput-object v1, v0, v4

    sput-object v0, Lcom/policydm/db/XDBFactoryBootstrap;->SPD_ServerID:[Ljava/lang/String;

    .line 32
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "spd-server"

    aput-object v1, v0, v2

    const-string v1, "dm-server"

    aput-object v1, v0, v3

    const-string v1, "Mformation"

    aput-object v1, v0, v4

    sput-object v0, Lcom/policydm/db/XDBFactoryBootstrap;->SPD_ProfileName:[Ljava/lang/String;

    .line 34
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "https://svc.spd.samsungdm.com/service/policy/device"

    aput-object v1, v0, v2

    const-string v1, "https://dm.spd.samsungdm.com/oma.dm"

    aput-object v1, v0, v3

    const-string v1, "https://bobafett.mformation.com:560/oma/iop"

    aput-object v1, v0, v4

    sput-object v0, Lcom/policydm/db/XDBFactoryBootstrap;->SPD_ServerUrl:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xdbFBGenerateFactoryNonce()Ljava/lang/String;
    .locals 10

    .prologue
    .line 133
    const-string v6, ""

    .line 135
    .local v6, "szNonce":Ljava/lang/String;
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    .line 136
    .local v1, "date":Ljava/util/Date;
    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    .line 137
    .local v4, "seed":J
    new-instance v3, Ljava/util/Random;

    invoke-direct {v3, v4, v5}, Ljava/util/Random;-><init>(J)V

    .line 138
    .local v3, "rnd":Ljava/util/Random;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Ljava/util/Random;->nextInt()I

    move-result v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "SPDNextNonce"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 139
    .local v7, "szTemp":Ljava/lang/String;
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    .line 141
    .local v0, "buf":[B
    invoke-static {v0}, Lcom/policydm/eng/core/XDMBase64;->xdmBase64Encode([B)[B

    move-result-object v2

    .line 142
    .local v2, "encoder":[B
    new-instance v6, Ljava/lang/String;

    .end local v6    # "szNonce":Ljava/lang/String;
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v8

    invoke-direct {v6, v2, v8}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 144
    .restart local v6    # "szNonce":Ljava/lang/String;
    return-object v6
.end method

.method public static xdbFBGetFactoryBootstrapData(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 11
    .param p0, "pNVMDMInfo"    # Ljava/lang/Object;
    .param p1, "nIdex"    # I

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x2

    const/4 v7, 0x0

    const/4 v8, 0x1

    .line 40
    const/4 v1, 0x0

    .line 41
    .local v1, "pProfileInfo":Lcom/policydm/db/XDBProfileInfo;
    const/4 v3, 0x0

    .line 44
    .local v3, "szNonce":Ljava/lang/String;
    const/4 v4, 0x0

    .line 45
    .local v4, "xdbURLParser":Lcom/policydm/db/XDBUrlInfo;
    const/4 v5, 0x0

    .line 46
    .local v5, "xdbURLParser2":Lcom/policydm/db/XDBUrlInfo;
    invoke-static {}, Lcom/policydm/adapter/XDMDevinfAdapter;->xdmDevAdpGetDeviceID()Ljava/lang/String;

    move-result-object v2

    .local v2, "szDevid":Ljava/lang/String;
    move-object v1, p0

    .line 48
    check-cast v1, Lcom/policydm/db/XDBProfileInfo;

    .line 49
    sget-object v6, Lcom/policydm/db/XDBFactoryBootstrap;->SPD_ProfileName:[Ljava/lang/String;

    aget-object v6, v6, p1

    iput-object v6, v1, Lcom/policydm/db/XDBProfileInfo;->ProfileName:Ljava/lang/String;

    .line 50
    sget-object v6, Lcom/policydm/db/XDBFactoryBootstrap;->SPD_ServerID:[Ljava/lang/String;

    aget-object v6, v6, p1

    iput-object v6, v1, Lcom/policydm/db/XDBProfileInfo;->ServerID:Ljava/lang/String;

    .line 51
    sget-object v6, Lcom/policydm/db/XDBFactoryBootstrap;->SPD_ServerUrl:[Ljava/lang/String;

    aget-object v6, v6, p1

    iput-object v6, v1, Lcom/policydm/db/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    .line 52
    iput v7, v1, Lcom/policydm/db/XDBProfileInfo;->AuthType:I

    .line 56
    const-string v6, "w7"

    iput-object v6, v1, Lcom/policydm/db/XDBProfileInfo;->AppID:Ljava/lang/String;

    .line 57
    const-string v6, "CLCRED"

    iput-object v6, v1, Lcom/policydm/db/XDBProfileInfo;->AuthLevel:Ljava/lang/String;

    .line 58
    const-string v6, "SVRCRED"

    iput-object v6, v1, Lcom/policydm/db/XDBProfileInfo;->ServerAuthLevel:Ljava/lang/String;

    .line 61
    iget-object v6, v1, Lcom/policydm/db/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    invoke-static {v6}, Lcom/policydm/tp/XTPHttpUtil;->xtpURLParser(Ljava/lang/String;)Lcom/policydm/db/XDBUrlInfo;

    move-result-object v4

    .line 62
    iget-object v6, v4, Lcom/policydm/db/XDBUrlInfo;->pURL:Ljava/lang/String;

    iput-object v6, v1, Lcom/policydm/db/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    .line 63
    iget-object v6, v4, Lcom/policydm/db/XDBUrlInfo;->pAddress:Ljava/lang/String;

    iput-object v6, v1, Lcom/policydm/db/XDBProfileInfo;->ServerIP:Ljava/lang/String;

    .line 64
    iget-object v6, v4, Lcom/policydm/db/XDBUrlInfo;->pPath:Ljava/lang/String;

    iput-object v6, v1, Lcom/policydm/db/XDBProfileInfo;->ServerPath:Ljava/lang/String;

    .line 65
    iget v6, v4, Lcom/policydm/db/XDBUrlInfo;->nPort:I

    iput v6, v1, Lcom/policydm/db/XDBProfileInfo;->ServerPort:I

    .line 66
    iget-object v6, v4, Lcom/policydm/db/XDBUrlInfo;->pProtocol:Ljava/lang/String;

    iput-object v6, v1, Lcom/policydm/db/XDBProfileInfo;->Protocol:Ljava/lang/String;

    .line 67
    iget-object v6, v1, Lcom/policydm/db/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    iput-object v6, v1, Lcom/policydm/db/XDBProfileInfo;->ServerUrl_Org:Ljava/lang/String;

    .line 69
    iget-object v6, v1, Lcom/policydm/db/XDBProfileInfo;->ServerUrl_Org:Ljava/lang/String;

    invoke-static {v6}, Lcom/policydm/tp/XTPHttpUtil;->xtpURLParser(Ljava/lang/String;)Lcom/policydm/db/XDBUrlInfo;

    move-result-object v5

    .line 70
    iget-object v6, v5, Lcom/policydm/db/XDBUrlInfo;->pURL:Ljava/lang/String;

    iput-object v6, v1, Lcom/policydm/db/XDBProfileInfo;->ServerUrl_Org:Ljava/lang/String;

    .line 71
    iget-object v6, v5, Lcom/policydm/db/XDBUrlInfo;->pAddress:Ljava/lang/String;

    iput-object v6, v1, Lcom/policydm/db/XDBProfileInfo;->ServerIP_Org:Ljava/lang/String;

    .line 72
    iget-object v6, v5, Lcom/policydm/db/XDBUrlInfo;->pPath:Ljava/lang/String;

    iput-object v6, v1, Lcom/policydm/db/XDBProfileInfo;->ServerPath_Org:Ljava/lang/String;

    .line 73
    iget v6, v5, Lcom/policydm/db/XDBUrlInfo;->nPort:I

    iput v6, v1, Lcom/policydm/db/XDBProfileInfo;->ServerPort_Org:I

    .line 74
    iget-object v6, v5, Lcom/policydm/db/XDBUrlInfo;->pProtocol:Ljava/lang/String;

    iput-object v6, v1, Lcom/policydm/db/XDBProfileInfo;->Protocol_Org:Ljava/lang/String;

    .line 75
    iput-boolean v7, v1, Lcom/policydm/db/XDBProfileInfo;->bChangedProtocol:Z

    .line 77
    const/4 v6, 0x3

    if-ge p1, v6, :cond_0

    .line 79
    iput v8, v1, Lcom/policydm/db/XDBProfileInfo;->AuthType:I

    .line 80
    iput v8, v1, Lcom/policydm/db/XDBProfileInfo;->nServerAuthType:I

    .line 82
    if-ne p1, v9, :cond_1

    .line 84
    iget-object v6, v1, Lcom/policydm/db/XDBProfileInfo;->ServerID:Ljava/lang/String;

    invoke-static {v2, v6, v7}, Lcom/policydm/db/XDBFactoryBootstrapadapter;->xdbFBAdpGeneratePasswd(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v1, Lcom/policydm/db/XDBProfileInfo;->ServerPwd:Ljava/lang/String;

    .line 85
    iget-object v6, v1, Lcom/policydm/db/XDBProfileInfo;->ServerID:Ljava/lang/String;

    invoke-static {v2, v6, v8}, Lcom/policydm/db/XDBFactoryBootstrapadapter;->xdbFBAdpGeneratePasswd(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v1, Lcom/policydm/db/XDBProfileInfo;->UserName:Ljava/lang/String;

    .line 86
    iget-object v6, v1, Lcom/policydm/db/XDBProfileInfo;->ServerID:Ljava/lang/String;

    invoke-static {v2, v6, v9}, Lcom/policydm/db/XDBFactoryBootstrapadapter;->xdbFBAdpGeneratePasswd(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v1, Lcom/policydm/db/XDBProfileInfo;->Password:Ljava/lang/String;

    .line 95
    :goto_0
    invoke-static {}, Lcom/policydm/db/XDBFactoryBootstrap;->xdbFBGenerateFactoryNonce()Ljava/lang/String;

    move-result-object v3

    .line 99
    new-instance v0, Lcom/policydm/db/XDBAccXNodeInfo;

    invoke-direct {v0}, Lcom/policydm/db/XDBAccXNodeInfo;-><init>()V

    .line 100
    .local v0, "dm_AccXNodeInfo":Lcom/policydm/db/XDBAccXNodeInfo;
    const-string v6, "./DMAcc/SPD"

    iput-object v6, v0, Lcom/policydm/db/XDBAccXNodeInfo;->m_szAccount:Ljava/lang/String;

    .line 101
    const-string v6, "./DMAcc/SPD/AppAddr/AppAddrX"

    iput-object v6, v0, Lcom/policydm/db/XDBAccXNodeInfo;->m_szAppAddr:Ljava/lang/String;

    .line 102
    const-string v6, "./DMAcc/SPD/AppAddr/AppAddrX/Port/PortX"

    iput-object v6, v0, Lcom/policydm/db/XDBAccXNodeInfo;->m_szAppAddrPort:Ljava/lang/String;

    .line 103
    const-string v6, "./DMAcc/SPD/AppAuth/ClientSide"

    iput-object v6, v0, Lcom/policydm/db/XDBAccXNodeInfo;->m_szClientAppAuth:Ljava/lang/String;

    .line 104
    const-string v6, "./DMAcc/SPD/AppAuth/ServerSide"

    iput-object v6, v0, Lcom/policydm/db/XDBAccXNodeInfo;->m_szServerAppAuth:Ljava/lang/String;

    .line 105
    const-string v6, "./DMAcc/SPD/ToConRef/Connectivity Reference Name"

    iput-object v6, v0, Lcom/policydm/db/XDBAccXNodeInfo;->m_szToConRef:Ljava/lang/String;

    .line 106
    const/16 v6, 0x83

    invoke-static {v6, v0}, Lcom/policydm/db/XDBSqlAdapter;->xdmDbSqlCreate(ILjava/lang/Object;)V

    .line 109
    if-nez p1, :cond_2

    .line 111
    iput-object v10, v1, Lcom/policydm/db/XDBProfileInfo;->ClientNonce:Ljava/lang/String;

    .line 112
    iput-object v10, v1, Lcom/policydm/db/XDBProfileInfo;->ServerNonce:Ljava/lang/String;

    .line 119
    :goto_1
    iput v8, v1, Lcom/policydm/db/XDBProfileInfo;->ServerNonceFormat:I

    .line 120
    iput v8, v1, Lcom/policydm/db/XDBProfileInfo;->ClientNonceFormat:I

    .line 122
    .end local v0    # "dm_AccXNodeInfo":Lcom/policydm/db/XDBAccXNodeInfo;
    :cond_0
    return-object v1

    .line 90
    :cond_1
    invoke-static {}, Lcom/policydm/adapter/XDMDevinfAdapter;->xdmDevAdpGetFullDeviceID()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v1, Lcom/policydm/db/XDBProfileInfo;->UserName:Ljava/lang/String;

    .line 91
    iget-object v6, v1, Lcom/policydm/db/XDBProfileInfo;->UserName:Ljava/lang/String;

    iget-object v7, v1, Lcom/policydm/db/XDBProfileInfo;->ServerID:Ljava/lang/String;

    invoke-static {v6, v7}, Lcom/policydm/db/XDBFactoryBootstrapadapter;->xdbFBAdpSPDGenerateDevicePwd(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v1, Lcom/policydm/db/XDBProfileInfo;->Password:Ljava/lang/String;

    .line 92
    iget-object v6, v1, Lcom/policydm/db/XDBProfileInfo;->ServerID:Ljava/lang/String;

    invoke-static {v6}, Lcom/policydm/db/XDBFactoryBootstrapadapter;->xdbFBAdpSPDGenerateServerPwd(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v1, Lcom/policydm/db/XDBProfileInfo;->ServerPwd:Ljava/lang/String;

    goto :goto_0

    .line 116
    .restart local v0    # "dm_AccXNodeInfo":Lcom/policydm/db/XDBAccXNodeInfo;
    :cond_2
    iput-object v3, v1, Lcom/policydm/db/XDBProfileInfo;->ClientNonce:Ljava/lang/String;

    .line 117
    iput-object v3, v1, Lcom/policydm/db/XDBProfileInfo;->ServerNonce:Ljava/lang/String;

    goto :goto_1
.end method

.method public static xdbFBGetFactoryBootstrapServerID(I)Ljava/lang/String;
    .locals 1
    .param p0, "nIdex"    # I

    .prologue
    .line 127
    sget-object v0, Lcom/policydm/db/XDBFactoryBootstrap;->SPD_ServerID:[Ljava/lang/String;

    aget-object v0, v0, p0

    return-object v0
.end method
