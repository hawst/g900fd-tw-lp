.class public Lcom/policydm/db/XDBInfoConRef;
.super Ljava/lang/Object;
.source "XDBInfoConRef.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public Active:Z

.field public NAP:Lcom/policydm/db/XDBConRefNAP;

.field public PX:Lcom/policydm/db/XDBConRefPX;

.field public bProxyUse:Z

.field public m_szHomeUrl:Ljava/lang/String;

.field public nService:I

.field public tAdvSetting:Lcom/policydm/db/XDBConRefAdvanceSetting;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/db/XDBInfoConRef;->m_szHomeUrl:Ljava/lang/String;

    .line 20
    new-instance v0, Lcom/policydm/db/XDBConRefNAP;

    invoke-direct {v0}, Lcom/policydm/db/XDBConRefNAP;-><init>()V

    iput-object v0, p0, Lcom/policydm/db/XDBInfoConRef;->NAP:Lcom/policydm/db/XDBConRefNAP;

    .line 21
    new-instance v0, Lcom/policydm/db/XDBConRefPX;

    invoke-direct {v0}, Lcom/policydm/db/XDBConRefPX;-><init>()V

    iput-object v0, p0, Lcom/policydm/db/XDBInfoConRef;->PX:Lcom/policydm/db/XDBConRefPX;

    .line 22
    new-instance v0, Lcom/policydm/db/XDBConRefAdvanceSetting;

    invoke-direct {v0}, Lcom/policydm/db/XDBConRefAdvanceSetting;-><init>()V

    iput-object v0, p0, Lcom/policydm/db/XDBInfoConRef;->tAdvSetting:Lcom/policydm/db/XDBConRefAdvanceSetting;

    .line 23
    return-void
.end method
