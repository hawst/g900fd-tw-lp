.class public Lcom/policydm/db/XDBFile;
.super Ljava/lang/Object;
.source "XDBFile.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method

.method private static xdbByteArray2Hex([B)Ljava/lang/String;
    .locals 9
    .param p0, "hash"    # [B

    .prologue
    .line 623
    new-instance v2, Ljava/util/Formatter;

    invoke-direct {v2}, Ljava/util/Formatter;-><init>()V

    .line 624
    .local v2, "formatter":Ljava/util/Formatter;
    move-object v0, p0

    .local v0, "arr$":[B
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_0

    aget-byte v1, v0, v3

    .line 626
    .local v1, "b":B
    const-string v5, "%02x"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v2, v5, v6}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    .line 624
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 628
    .end local v1    # "b":B
    :cond_0
    invoke-virtual {v2}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method

.method public static xdbCalculateHash(Ljava/security/MessageDigest;Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p0, "algorithm"    # Ljava/security/MessageDigest;
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 588
    const/4 v5, 0x0

    .line 589
    .local v5, "fis":Ljava/io/FileInputStream;
    const/4 v0, 0x0

    .line 590
    .local v0, "bis":Ljava/io/BufferedInputStream;
    const/4 v2, 0x0

    .line 595
    .local v2, "dis":Ljava/security/DigestInputStream;
    :try_start_0
    new-instance v6, Ljava/io/FileInputStream;

    new-instance v8, Ljava/io/File;

    invoke-direct {v8, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v6, v8}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 596
    .end local v5    # "fis":Ljava/io/FileInputStream;
    .local v6, "fis":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v1, Ljava/io/BufferedInputStream;

    invoke-direct {v1, v6}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    .line 597
    .end local v0    # "bis":Ljava/io/BufferedInputStream;
    .local v1, "bis":Ljava/io/BufferedInputStream;
    :try_start_2
    new-instance v3, Ljava/security/DigestInputStream;

    invoke-direct {v3, v1, p0}, Ljava/security/DigestInputStream;-><init>(Ljava/io/InputStream;Ljava/security/MessageDigest;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_7
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4

    .line 599
    .end local v2    # "dis":Ljava/security/DigestInputStream;
    .local v3, "dis":Ljava/security/DigestInputStream;
    :cond_0
    :try_start_3
    invoke-virtual {v3}, Ljava/security/DigestInputStream;->read()I
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_8
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5

    move-result v8

    const/4 v9, -0x1

    if-ne v8, v9, :cond_0

    move-object v2, v3

    .end local v3    # "dis":Ljava/security/DigestInputStream;
    .restart local v2    # "dis":Ljava/security/DigestInputStream;
    move-object v0, v1

    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .restart local v0    # "bis":Ljava/io/BufferedInputStream;
    move-object v5, v6

    .line 608
    .end local v6    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    :goto_0
    invoke-virtual {p0}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v7

    .line 611
    .local v7, "hash":[B
    :try_start_4
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 618
    :goto_1
    invoke-static {v7}, Lcom/policydm/db/XDBFile;->xdbByteArray2Hex([B)Ljava/lang/String;

    move-result-object v8

    return-object v8

    .line 601
    .end local v7    # "hash":[B
    :catch_0
    move-exception v4

    .line 602
    .local v4, "e":Ljava/io/FileNotFoundException;
    :goto_2
    invoke-virtual {v4}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 603
    .end local v4    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v4

    .line 604
    .local v4, "e":Ljava/io/IOException;
    :goto_3
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 613
    .end local v4    # "e":Ljava/io/IOException;
    .restart local v7    # "hash":[B
    :catch_2
    move-exception v4

    .line 615
    .restart local v4    # "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 603
    .end local v4    # "e":Ljava/io/IOException;
    .end local v5    # "fis":Ljava/io/FileInputStream;
    .end local v7    # "hash":[B
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    :catch_3
    move-exception v4

    move-object v5, v6

    .end local v6    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    goto :goto_3

    .end local v0    # "bis":Ljava/io/BufferedInputStream;
    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "bis":Ljava/io/BufferedInputStream;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    :catch_4
    move-exception v4

    move-object v0, v1

    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .restart local v0    # "bis":Ljava/io/BufferedInputStream;
    move-object v5, v6

    .end local v6    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    goto :goto_3

    .end local v0    # "bis":Ljava/io/BufferedInputStream;
    .end local v2    # "dis":Ljava/security/DigestInputStream;
    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "bis":Ljava/io/BufferedInputStream;
    .restart local v3    # "dis":Ljava/security/DigestInputStream;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    :catch_5
    move-exception v4

    move-object v2, v3

    .end local v3    # "dis":Ljava/security/DigestInputStream;
    .restart local v2    # "dis":Ljava/security/DigestInputStream;
    move-object v0, v1

    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .restart local v0    # "bis":Ljava/io/BufferedInputStream;
    move-object v5, v6

    .end local v6    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    goto :goto_3

    .line 601
    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    :catch_6
    move-exception v4

    move-object v5, v6

    .end local v6    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    goto :goto_2

    .end local v0    # "bis":Ljava/io/BufferedInputStream;
    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "bis":Ljava/io/BufferedInputStream;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    :catch_7
    move-exception v4

    move-object v0, v1

    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .restart local v0    # "bis":Ljava/io/BufferedInputStream;
    move-object v5, v6

    .end local v6    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    goto :goto_2

    .end local v0    # "bis":Ljava/io/BufferedInputStream;
    .end local v2    # "dis":Ljava/security/DigestInputStream;
    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "bis":Ljava/io/BufferedInputStream;
    .restart local v3    # "dis":Ljava/security/DigestInputStream;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    :catch_8
    move-exception v4

    move-object v2, v3

    .end local v3    # "dis":Ljava/security/DigestInputStream;
    .restart local v2    # "dis":Ljava/security/DigestInputStream;
    move-object v0, v1

    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .restart local v0    # "bis":Ljava/io/BufferedInputStream;
    move-object v5, v6

    .end local v6    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    goto :goto_2
.end method

.method public static xdbFileCopy(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 17
    .param p0, "sourceDirPath"    # Ljava/lang/String;
    .param p1, "targetDirPath"    # Ljava/lang/String;
    .param p2, "fileName"    # Ljava/lang/String;

    .prologue
    .line 377
    const/4 v8, 0x0

    .line 378
    .local v8, "result":Z
    const/4 v9, 0x0

    .line 379
    .local v9, "sourceFile":Ljava/io/File;
    const/4 v12, 0x0

    .line 380
    .local v12, "targetFile":Ljava/io/File;
    const/4 v3, 0x0

    .line 381
    .local v3, "fileBytes":[B
    const/4 v6, 0x0

    .line 382
    .local v6, "raf":Ljava/io/RandomAccessFile;
    const/4 v4, 0x0

    .line 386
    .local v4, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v10, Ljava/io/File;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v10, v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 388
    .end local v9    # "sourceFile":Ljava/io/File;
    .local v10, "sourceFile":Ljava/io/File;
    if-eqz v10, :cond_2

    :try_start_1
    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v14

    if-eqz v14, :cond_2

    .line 390
    new-instance v7, Ljava/io/RandomAccessFile;

    const-string v14, "r"

    invoke-direct {v7, v10, v14}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 391
    .end local v6    # "raf":Ljava/io/RandomAccessFile;
    .local v7, "raf":Ljava/io/RandomAccessFile;
    :try_start_2
    invoke-virtual {v10}, Ljava/io/File;->length()J

    move-result-wide v14

    long-to-int v14, v14

    new-array v3, v14, [B

    .line 392
    if-eqz v7, :cond_0

    .line 394
    invoke-virtual {v7, v3}, Ljava/io/RandomAccessFile;->readFully([B)V

    .line 397
    :cond_0
    new-instance v11, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v11, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 398
    .local v11, "targetDirFile":Ljava/io/File;
    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v14

    if-nez v14, :cond_1

    .line 400
    invoke-virtual {v11}, Ljava/io/File;->mkdirs()Z

    .line 402
    :cond_1
    new-instance v13, Ljava/io/File;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-direct {v13, v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_8
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 405
    .end local v12    # "targetFile":Ljava/io/File;
    .local v13, "targetFile":Ljava/io/File;
    if-eqz v13, :cond_9

    if-eqz v3, :cond_9

    .line 407
    :try_start_3
    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, v13}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_9
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 408
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .local v5, "fos":Ljava/io/FileOutputStream;
    :try_start_4
    invoke-virtual {v5, v3}, Ljava/io/FileOutputStream;->write([B)V

    .line 409
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->flush()V

    .line 410
    const/4 v14, 0x1

    const/4 v15, 0x0

    invoke-virtual {v13, v14, v15}, Ljava/io/File;->setReadable(ZZ)Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_a
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 412
    const/4 v8, 0x1

    move-object v4, v5

    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    move-object v6, v7

    .end local v7    # "raf":Ljava/io/RandomAccessFile;
    .restart local v6    # "raf":Ljava/io/RandomAccessFile;
    move-object v12, v13

    .line 425
    .end local v11    # "targetDirFile":Ljava/io/File;
    .end local v13    # "targetFile":Ljava/io/File;
    .restart local v12    # "targetFile":Ljava/io/File;
    :cond_2
    :goto_0
    if-eqz v6, :cond_3

    .line 427
    :try_start_5
    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    .line 437
    :cond_3
    :goto_1
    if-eqz v4, :cond_4

    .line 439
    :try_start_6
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    :cond_4
    move-object v9, v10

    .line 448
    .end local v10    # "sourceFile":Ljava/io/File;
    .restart local v9    # "sourceFile":Ljava/io/File;
    :cond_5
    :goto_2
    return v8

    .line 430
    .end local v9    # "sourceFile":Ljava/io/File;
    .restart local v10    # "sourceFile":Ljava/io/File;
    :catch_0
    move-exception v2

    .line 432
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 433
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Error closing : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 442
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 444
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 445
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Error closing : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move-object v9, v10

    .line 447
    .end local v10    # "sourceFile":Ljava/io/File;
    .restart local v9    # "sourceFile":Ljava/io/File;
    goto :goto_2

    .line 416
    .end local v2    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v2

    .line 418
    .local v2, "e":Ljava/lang/Exception;
    :goto_3
    :try_start_7
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 419
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Exception in copy : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 425
    if-eqz v6, :cond_6

    .line 427
    :try_start_8
    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    .line 437
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_6
    :goto_4
    if-eqz v4, :cond_5

    .line 439
    :try_start_9
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3

    goto :goto_2

    .line 442
    :catch_3
    move-exception v2

    .line 444
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 445
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Error closing : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 430
    .local v2, "e":Ljava/lang/Exception;
    :catch_4
    move-exception v2

    .line 432
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 433
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Error closing : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_4

    .line 423
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v14

    .line 425
    :goto_5
    if-eqz v6, :cond_7

    .line 427
    :try_start_a
    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5

    .line 437
    :cond_7
    :goto_6
    if-eqz v4, :cond_8

    .line 439
    :try_start_b
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_6

    .line 446
    :cond_8
    :goto_7
    throw v14

    .line 430
    :catch_5
    move-exception v2

    .line 432
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 433
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Error closing : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_6

    .line 442
    .end local v2    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v2

    .line 444
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 445
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Error closing : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_7

    .line 423
    .end local v2    # "e":Ljava/io/IOException;
    .end local v9    # "sourceFile":Ljava/io/File;
    .restart local v10    # "sourceFile":Ljava/io/File;
    :catchall_1
    move-exception v14

    move-object v9, v10

    .end local v10    # "sourceFile":Ljava/io/File;
    .restart local v9    # "sourceFile":Ljava/io/File;
    goto :goto_5

    .end local v6    # "raf":Ljava/io/RandomAccessFile;
    .end local v9    # "sourceFile":Ljava/io/File;
    .restart local v7    # "raf":Ljava/io/RandomAccessFile;
    .restart local v10    # "sourceFile":Ljava/io/File;
    :catchall_2
    move-exception v14

    move-object v6, v7

    .end local v7    # "raf":Ljava/io/RandomAccessFile;
    .restart local v6    # "raf":Ljava/io/RandomAccessFile;
    move-object v9, v10

    .end local v10    # "sourceFile":Ljava/io/File;
    .restart local v9    # "sourceFile":Ljava/io/File;
    goto :goto_5

    .end local v6    # "raf":Ljava/io/RandomAccessFile;
    .end local v9    # "sourceFile":Ljava/io/File;
    .end local v12    # "targetFile":Ljava/io/File;
    .restart local v7    # "raf":Ljava/io/RandomAccessFile;
    .restart local v10    # "sourceFile":Ljava/io/File;
    .restart local v11    # "targetDirFile":Ljava/io/File;
    .restart local v13    # "targetFile":Ljava/io/File;
    :catchall_3
    move-exception v14

    move-object v6, v7

    .end local v7    # "raf":Ljava/io/RandomAccessFile;
    .restart local v6    # "raf":Ljava/io/RandomAccessFile;
    move-object v12, v13

    .end local v13    # "targetFile":Ljava/io/File;
    .restart local v12    # "targetFile":Ljava/io/File;
    move-object v9, v10

    .end local v10    # "sourceFile":Ljava/io/File;
    .restart local v9    # "sourceFile":Ljava/io/File;
    goto :goto_5

    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .end local v6    # "raf":Ljava/io/RandomAccessFile;
    .end local v9    # "sourceFile":Ljava/io/File;
    .end local v12    # "targetFile":Ljava/io/File;
    .restart local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v7    # "raf":Ljava/io/RandomAccessFile;
    .restart local v10    # "sourceFile":Ljava/io/File;
    .restart local v13    # "targetFile":Ljava/io/File;
    :catchall_4
    move-exception v14

    move-object v4, v5

    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    move-object v6, v7

    .end local v7    # "raf":Ljava/io/RandomAccessFile;
    .restart local v6    # "raf":Ljava/io/RandomAccessFile;
    move-object v12, v13

    .end local v13    # "targetFile":Ljava/io/File;
    .restart local v12    # "targetFile":Ljava/io/File;
    move-object v9, v10

    .end local v10    # "sourceFile":Ljava/io/File;
    .restart local v9    # "sourceFile":Ljava/io/File;
    goto :goto_5

    .line 416
    .end local v9    # "sourceFile":Ljava/io/File;
    .end local v11    # "targetDirFile":Ljava/io/File;
    .restart local v10    # "sourceFile":Ljava/io/File;
    :catch_7
    move-exception v2

    move-object v9, v10

    .end local v10    # "sourceFile":Ljava/io/File;
    .restart local v9    # "sourceFile":Ljava/io/File;
    goto/16 :goto_3

    .end local v6    # "raf":Ljava/io/RandomAccessFile;
    .end local v9    # "sourceFile":Ljava/io/File;
    .restart local v7    # "raf":Ljava/io/RandomAccessFile;
    .restart local v10    # "sourceFile":Ljava/io/File;
    :catch_8
    move-exception v2

    move-object v6, v7

    .end local v7    # "raf":Ljava/io/RandomAccessFile;
    .restart local v6    # "raf":Ljava/io/RandomAccessFile;
    move-object v9, v10

    .end local v10    # "sourceFile":Ljava/io/File;
    .restart local v9    # "sourceFile":Ljava/io/File;
    goto/16 :goto_3

    .end local v6    # "raf":Ljava/io/RandomAccessFile;
    .end local v9    # "sourceFile":Ljava/io/File;
    .end local v12    # "targetFile":Ljava/io/File;
    .restart local v7    # "raf":Ljava/io/RandomAccessFile;
    .restart local v10    # "sourceFile":Ljava/io/File;
    .restart local v11    # "targetDirFile":Ljava/io/File;
    .restart local v13    # "targetFile":Ljava/io/File;
    :catch_9
    move-exception v2

    move-object v6, v7

    .end local v7    # "raf":Ljava/io/RandomAccessFile;
    .restart local v6    # "raf":Ljava/io/RandomAccessFile;
    move-object v12, v13

    .end local v13    # "targetFile":Ljava/io/File;
    .restart local v12    # "targetFile":Ljava/io/File;
    move-object v9, v10

    .end local v10    # "sourceFile":Ljava/io/File;
    .restart local v9    # "sourceFile":Ljava/io/File;
    goto/16 :goto_3

    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .end local v6    # "raf":Ljava/io/RandomAccessFile;
    .end local v9    # "sourceFile":Ljava/io/File;
    .end local v12    # "targetFile":Ljava/io/File;
    .restart local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v7    # "raf":Ljava/io/RandomAccessFile;
    .restart local v10    # "sourceFile":Ljava/io/File;
    .restart local v13    # "targetFile":Ljava/io/File;
    :catch_a
    move-exception v2

    move-object v4, v5

    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    move-object v6, v7

    .end local v7    # "raf":Ljava/io/RandomAccessFile;
    .restart local v6    # "raf":Ljava/io/RandomAccessFile;
    move-object v12, v13

    .end local v13    # "targetFile":Ljava/io/File;
    .restart local v12    # "targetFile":Ljava/io/File;
    move-object v9, v10

    .end local v10    # "sourceFile":Ljava/io/File;
    .restart local v9    # "sourceFile":Ljava/io/File;
    goto/16 :goto_3

    .end local v6    # "raf":Ljava/io/RandomAccessFile;
    .end local v9    # "sourceFile":Ljava/io/File;
    .end local v12    # "targetFile":Ljava/io/File;
    .restart local v7    # "raf":Ljava/io/RandomAccessFile;
    .restart local v10    # "sourceFile":Ljava/io/File;
    .restart local v13    # "targetFile":Ljava/io/File;
    :cond_9
    move-object v6, v7

    .end local v7    # "raf":Ljava/io/RandomAccessFile;
    .restart local v6    # "raf":Ljava/io/RandomAccessFile;
    move-object v12, v13

    .end local v13    # "targetFile":Ljava/io/File;
    .restart local v12    # "targetFile":Ljava/io/File;
    goto/16 :goto_0
.end method

.method public static xdbFileCreateWrite(Ljava/lang/String;[B)Z
    .locals 5
    .param p0, "szPath"    # Ljava/lang/String;
    .param p1, "data"    # [B

    .prologue
    .line 299
    const/4 v0, 0x0

    .line 302
    .local v0, "ObjOut":Ljava/io/DataOutputStream;
    :try_start_0
    new-instance v1, Ljava/io/DataOutputStream;

    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, p0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 303
    .end local v0    # "ObjOut":Ljava/io/DataOutputStream;
    .local v1, "ObjOut":Ljava/io/DataOutputStream;
    :try_start_1
    invoke-virtual {v1, p1}, Ljava/io/DataOutputStream;->write([B)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 314
    if-eqz v1, :cond_0

    .line 316
    :try_start_2
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 325
    :cond_0
    :goto_0
    const/4 v3, 0x1

    move-object v0, v1

    .end local v1    # "ObjOut":Ljava/io/DataOutputStream;
    .restart local v0    # "ObjOut":Ljava/io/DataOutputStream;
    :cond_1
    :goto_1
    return v3

    .line 319
    .end local v0    # "ObjOut":Ljava/io/DataOutputStream;
    .restart local v1    # "ObjOut":Ljava/io/DataOutputStream;
    :catch_0
    move-exception v2

    .line 321
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 305
    .end local v1    # "ObjOut":Ljava/io/DataOutputStream;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v0    # "ObjOut":Ljava/io/DataOutputStream;
    :catch_1
    move-exception v2

    .line 307
    .local v2, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_3
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 308
    const/4 v3, 0x0

    .line 314
    if-eqz v0, :cond_1

    .line 316
    :try_start_4
    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    .line 319
    :catch_2
    move-exception v2

    .line 321
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 312
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    .line 314
    :goto_3
    if-eqz v0, :cond_2

    .line 316
    :try_start_5
    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 322
    :cond_2
    :goto_4
    throw v3

    .line 319
    :catch_3
    move-exception v2

    .line 321
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_4

    .line 312
    .end local v0    # "ObjOut":Ljava/io/DataOutputStream;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v1    # "ObjOut":Ljava/io/DataOutputStream;
    :catchall_1
    move-exception v3

    move-object v0, v1

    .end local v1    # "ObjOut":Ljava/io/DataOutputStream;
    .restart local v0    # "ObjOut":Ljava/io/DataOutputStream;
    goto :goto_3

    .line 305
    .end local v0    # "ObjOut":Ljava/io/DataOutputStream;
    .restart local v1    # "ObjOut":Ljava/io/DataOutputStream;
    :catch_4
    move-exception v2

    move-object v0, v1

    .end local v1    # "ObjOut":Ljava/io/DataOutputStream;
    .restart local v0    # "ObjOut":Ljava/io/DataOutputStream;
    goto :goto_2
.end method

.method public static xdbFileDirRemove(Ljava/lang/String;)Z
    .locals 11
    .param p0, "szPath"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 332
    :try_start_0
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 333
    .local v7, "targetDir":Ljava/io/File;
    if-eqz v7, :cond_2

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-virtual {v7}, Ljava/io/File;->isDirectory()Z

    move-result v10

    if-eqz v10, :cond_2

    .line 335
    invoke-virtual {v7}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v0

    .line 336
    .local v0, "allFiles":[Ljava/lang/String;
    move-object v1, v0

    .local v1, "arr$":[Ljava/lang/String;
    array-length v6, v1

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_0
    if-ge v5, v6, :cond_1

    aget-object v4, v1, v5

    .line 338
    .local v4, "filename":Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v10

    if-nez v10, :cond_0

    .line 351
    .end local v0    # "allFiles":[Ljava/lang/String;
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v3    # "file":Ljava/io/File;
    .end local v4    # "filename":Ljava/lang/String;
    .end local v5    # "i$":I
    .end local v6    # "len$":I
    .end local v7    # "targetDir":Ljava/io/File;
    :goto_1
    return v8

    .line 336
    .restart local v0    # "allFiles":[Ljava/lang/String;
    .restart local v1    # "arr$":[Ljava/lang/String;
    .restart local v3    # "file":Ljava/io/File;
    .restart local v4    # "filename":Ljava/lang/String;
    .restart local v5    # "i$":I
    .restart local v6    # "len$":I
    .restart local v7    # "targetDir":Ljava/io/File;
    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .end local v3    # "file":Ljava/io/File;
    .end local v4    # "filename":Ljava/lang/String;
    :cond_1
    move v8, v9

    .line 342
    goto :goto_1

    .line 345
    .end local v0    # "allFiles":[Ljava/lang/String;
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v5    # "i$":I
    .end local v6    # "len$":I
    .end local v7    # "targetDir":Ljava/io/File;
    :catch_0
    move-exception v2

    .line 347
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 348
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v7    # "targetDir":Ljava/io/File;
    :cond_2
    move v8, v9

    .line 351
    goto :goto_1
.end method

.method public static xdbFileExists(Ljava/lang/String;)Z
    .locals 3
    .param p0, "szFileName"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 33
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 35
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 37
    invoke-virtual {v0}, Ljava/io/File;->canRead()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 39
    const/4 v1, 0x1

    .line 48
    :cond_0
    return v1
.end method

.method public static xdbFileExists(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p0, "targetDirPath"    # Ljava/lang/String;
    .param p1, "targetName"    # Ljava/lang/String;

    .prologue
    .line 54
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    .local v0, "targetFile":Ljava/io/File;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 59
    const/4 v1, 0x1

    .line 65
    :goto_0
    return v1

    .line 63
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "doesn\'t exist target file: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 65
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static xdbFileGetHash(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "Path"    # Ljava/lang/String;
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "hashMethod"    # Ljava/lang/String;

    .prologue
    .line 574
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 575
    .local v1, "fullfileName":Ljava/lang/String;
    const/4 v2, 0x0

    .line 578
    .local v2, "method":Ljava/security/MessageDigest;
    :try_start_0
    invoke-static {p2}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 583
    :goto_0
    invoke-static {v2, v1}, Lcom/policydm/db/XDBFile;->xdbCalculateHash(Ljava/security/MessageDigest;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 579
    :catch_0
    move-exception v0

    .line 580
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    goto :goto_0
.end method

.method public static xdbFileGetSize(Ljava/lang/String;)J
    .locals 8
    .param p0, "szPath"    # Ljava/lang/String;

    .prologue
    .line 126
    const/4 v0, 0x0

    .line 127
    .local v0, "ObjIn":Ljava/io/DataInputStream;
    const-wide/16 v4, 0x0

    .line 130
    .local v4, "len":J
    :try_start_0
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 131
    .local v3, "file":Ljava/io/File;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 133
    new-instance v1, Ljava/io/DataInputStream;

    new-instance v6, Ljava/io/FileInputStream;

    invoke-direct {v6, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v6}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 134
    .end local v0    # "ObjIn":Ljava/io/DataInputStream;
    .local v1, "ObjIn":Ljava/io/DataInputStream;
    :try_start_1
    invoke-virtual {v1}, Ljava/io/DataInputStream;->available()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v6

    int-to-long v4, v6

    move-object v0, v1

    .line 145
    .end local v1    # "ObjIn":Ljava/io/DataInputStream;
    .restart local v0    # "ObjIn":Ljava/io/DataInputStream;
    :cond_0
    if-eqz v0, :cond_1

    .line 147
    :try_start_2
    invoke-virtual {v0}, Ljava/io/DataInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 155
    .end local v3    # "file":Ljava/io/File;
    :cond_1
    :goto_0
    return-wide v4

    .line 150
    .restart local v3    # "file":Ljava/io/File;
    :catch_0
    move-exception v2

    .line 152
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 137
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "file":Ljava/io/File;
    :catch_1
    move-exception v2

    .line 139
    .local v2, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_3
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 145
    if-eqz v0, :cond_1

    .line 147
    :try_start_4
    invoke-virtual {v0}, Ljava/io/DataInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 150
    :catch_2
    move-exception v2

    .line 152
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 143
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    .line 145
    :goto_2
    if-eqz v0, :cond_2

    .line 147
    :try_start_5
    invoke-virtual {v0}, Ljava/io/DataInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 153
    :cond_2
    :goto_3
    throw v6

    .line 150
    :catch_3
    move-exception v2

    .line 152
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_3

    .line 143
    .end local v0    # "ObjIn":Ljava/io/DataInputStream;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v1    # "ObjIn":Ljava/io/DataInputStream;
    .restart local v3    # "file":Ljava/io/File;
    :catchall_1
    move-exception v6

    move-object v0, v1

    .end local v1    # "ObjIn":Ljava/io/DataInputStream;
    .restart local v0    # "ObjIn":Ljava/io/DataInputStream;
    goto :goto_2

    .line 137
    .end local v0    # "ObjIn":Ljava/io/DataInputStream;
    .restart local v1    # "ObjIn":Ljava/io/DataInputStream;
    :catch_4
    move-exception v2

    move-object v0, v1

    .end local v1    # "ObjIn":Ljava/io/DataInputStream;
    .restart local v0    # "ObjIn":Ljava/io/DataInputStream;
    goto :goto_1
.end method

.method public static xdbFileRead(Ljava/lang/String;[BII)Z
    .locals 6
    .param p0, "szPath"    # Ljava/lang/String;
    .param p1, "buffer"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    const/4 v4, 0x0

    .line 198
    const/4 v3, 0x0

    .line 199
    .local v3, "ret":I
    const/4 v0, 0x0

    .line 200
    .local v0, "ObjIn":Ljava/io/DataInputStream;
    if-gtz p3, :cond_1

    .line 229
    :cond_0
    :goto_0
    return v4

    .line 204
    :cond_1
    :try_start_0
    new-instance v1, Ljava/io/DataInputStream;

    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v5}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 205
    .end local v0    # "ObjIn":Ljava/io/DataInputStream;
    .local v1, "ObjIn":Ljava/io/DataInputStream;
    :try_start_1
    invoke-virtual {v1, p1, p2, p3}, Ljava/io/DataInputStream;->read([BII)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v3

    .line 215
    if-eqz v1, :cond_2

    .line 217
    :try_start_2
    invoke-virtual {v1}, Ljava/io/DataInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_2
    move-object v0, v1

    .line 226
    .end local v1    # "ObjIn":Ljava/io/DataInputStream;
    .restart local v0    # "ObjIn":Ljava/io/DataInputStream;
    :cond_3
    :goto_1
    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    .line 227
    const/4 v4, 0x1

    goto :goto_0

    .line 220
    .end local v0    # "ObjIn":Ljava/io/DataInputStream;
    .restart local v1    # "ObjIn":Ljava/io/DataInputStream;
    :catch_0
    move-exception v2

    .line 222
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move-object v0, v1

    .line 224
    .end local v1    # "ObjIn":Ljava/io/DataInputStream;
    .restart local v0    # "ObjIn":Ljava/io/DataInputStream;
    goto :goto_1

    .line 207
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 209
    .local v2, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_3
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 215
    if-eqz v0, :cond_3

    .line 217
    :try_start_4
    invoke-virtual {v0}, Ljava/io/DataInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    .line 220
    :catch_2
    move-exception v2

    .line 222
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 213
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 215
    :goto_3
    if-eqz v0, :cond_4

    .line 217
    :try_start_5
    invoke-virtual {v0}, Ljava/io/DataInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 223
    :cond_4
    :goto_4
    throw v4

    .line 220
    :catch_3
    move-exception v2

    .line 222
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_4

    .line 213
    .end local v0    # "ObjIn":Ljava/io/DataInputStream;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v1    # "ObjIn":Ljava/io/DataInputStream;
    :catchall_1
    move-exception v4

    move-object v0, v1

    .end local v1    # "ObjIn":Ljava/io/DataInputStream;
    .restart local v0    # "ObjIn":Ljava/io/DataInputStream;
    goto :goto_3

    .line 207
    .end local v0    # "ObjIn":Ljava/io/DataInputStream;
    .restart local v1    # "ObjIn":Ljava/io/DataInputStream;
    :catch_4
    move-exception v2

    move-object v0, v1

    .end local v1    # "ObjIn":Ljava/io/DataInputStream;
    .restart local v0    # "ObjIn":Ljava/io/DataInputStream;
    goto :goto_2
.end method

.method public static xdbFileRead(Ljava/lang/String;)[B
    .locals 8
    .param p0, "szFile"    # Ljava/lang/String;

    .prologue
    .line 160
    const/4 v0, 0x0

    .line 161
    .local v0, "bytearray":[B
    const/4 v2, 0x0

    .line 164
    .local v2, "fileinputstream":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 165
    .end local v2    # "fileinputstream":Ljava/io/FileInputStream;
    .local v3, "fileinputstream":Ljava/io/FileInputStream;
    :try_start_1
    invoke-virtual {v3}, Ljava/io/FileInputStream;->available()I

    move-result v4

    .line 166
    .local v4, "numberBytes":I
    new-array v0, v4, [B

    .line 167
    invoke-virtual {v3, v0}, Ljava/io/FileInputStream;->read([B)I

    move-result v5

    .line 168
    .local v5, "readBytes":I
    if-gez v5, :cond_1

    .line 170
    const-string v6, ""

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 171
    const/4 v6, 0x0

    .line 182
    if-eqz v3, :cond_0

    .line 184
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    :goto_0
    move-object v2, v3

    .line 193
    .end local v3    # "fileinputstream":Ljava/io/FileInputStream;
    .end local v4    # "numberBytes":I
    .end local v5    # "readBytes":I
    .restart local v2    # "fileinputstream":Ljava/io/FileInputStream;
    :goto_1
    return-object v6

    .line 187
    .end local v2    # "fileinputstream":Ljava/io/FileInputStream;
    .restart local v3    # "fileinputstream":Ljava/io/FileInputStream;
    .restart local v4    # "numberBytes":I
    .restart local v5    # "readBytes":I
    :catch_0
    move-exception v1

    .line 189
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 182
    .end local v1    # "e":Ljava/io/IOException;
    :cond_1
    if-eqz v3, :cond_2

    .line 184
    :try_start_3
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    :cond_2
    move-object v2, v3

    .end local v3    # "fileinputstream":Ljava/io/FileInputStream;
    .end local v4    # "numberBytes":I
    .end local v5    # "readBytes":I
    .restart local v2    # "fileinputstream":Ljava/io/FileInputStream;
    :cond_3
    :goto_2
    move-object v6, v0

    .line 193
    goto :goto_1

    .line 187
    .end local v2    # "fileinputstream":Ljava/io/FileInputStream;
    .restart local v3    # "fileinputstream":Ljava/io/FileInputStream;
    .restart local v4    # "numberBytes":I
    .restart local v5    # "readBytes":I
    :catch_1
    move-exception v1

    .line 189
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move-object v2, v3

    .line 191
    .end local v3    # "fileinputstream":Ljava/io/FileInputStream;
    .restart local v2    # "fileinputstream":Ljava/io/FileInputStream;
    goto :goto_2

    .line 174
    .end local v1    # "e":Ljava/io/IOException;
    .end local v4    # "numberBytes":I
    .end local v5    # "readBytes":I
    :catch_2
    move-exception v1

    .line 176
    .restart local v1    # "e":Ljava/io/IOException;
    :goto_3
    :try_start_4
    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 182
    if-eqz v2, :cond_3

    .line 184
    :try_start_5
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_2

    .line 187
    :catch_3
    move-exception v1

    .line 189
    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_2

    .line 180
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    .line 182
    :goto_4
    if-eqz v2, :cond_4

    .line 184
    :try_start_6
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 190
    :cond_4
    :goto_5
    throw v6

    .line 187
    :catch_4
    move-exception v1

    .line 189
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_5

    .line 180
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "fileinputstream":Ljava/io/FileInputStream;
    .restart local v3    # "fileinputstream":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v6

    move-object v2, v3

    .end local v3    # "fileinputstream":Ljava/io/FileInputStream;
    .restart local v2    # "fileinputstream":Ljava/io/FileInputStream;
    goto :goto_4

    .line 174
    .end local v2    # "fileinputstream":Ljava/io/FileInputStream;
    .restart local v3    # "fileinputstream":Ljava/io/FileInputStream;
    :catch_5
    move-exception v1

    move-object v2, v3

    .end local v3    # "fileinputstream":Ljava/io/FileInputStream;
    .restart local v2    # "fileinputstream":Ljava/io/FileInputStream;
    goto :goto_3
.end method

.method public static xdbFileRemove(Ljava/lang/String;)Z
    .locals 4
    .param p0, "szPath"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 358
    :try_start_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 359
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 361
    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-nez v3, :cond_0

    .line 372
    .end local v1    # "file":Ljava/io/File;
    :goto_0
    return v2

    .line 365
    :catch_0
    move-exception v0

    .line 367
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 368
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 372
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "file":Ljava/io/File;
    :cond_0
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static xdbFileUnzip(Ljava/io/File;Ljava/io/File;Ljava/lang/String;)Z
    .locals 15
    .param p0, "zipFile"    # Ljava/io/File;
    .param p1, "targetDir"    # Ljava/io/File;
    .param p2, "password"    # Ljava/lang/String;

    .prologue
    .line 453
    const/4 v6, 0x0

    .line 454
    .local v6, "result":Z
    const/4 v3, 0x0

    .line 455
    .local v3, "fis":Ljava/io/FileInputStream;
    const/4 v10, 0x0

    .line 456
    .local v10, "zis":Ljava/util/zip/ZipInputStream;
    const/4 v9, 0x0

    .line 460
    .local v9, "zentry":Ljava/util/zip/ZipEntry;
    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 462
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .local v4, "fis":Ljava/io/FileInputStream;
    :try_start_1
    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_3

    .line 464
    new-instance v8, Lcom/policydm/db/XDBZipDecryptInputStream;

    move-object/from16 v0, p2

    invoke-direct {v8, v4, v0}, Lcom/policydm/db/XDBZipDecryptInputStream;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 465
    .local v8, "zdis":Lcom/policydm/db/XDBZipDecryptInputStream;
    new-instance v11, Ljava/util/zip/ZipInputStream;

    invoke-direct {v11, v8}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V

    .end local v10    # "zis":Ljava/util/zip/ZipInputStream;
    .local v11, "zis":Ljava/util/zip/ZipInputStream;
    move-object v10, v11

    .line 472
    .end local v8    # "zdis":Lcom/policydm/db/XDBZipDecryptInputStream;
    .end local v11    # "zis":Ljava/util/zip/ZipInputStream;
    .restart local v10    # "zis":Ljava/util/zip/ZipInputStream;
    :goto_0
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "start unzip file:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " to target directory:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 474
    :cond_0
    :goto_1
    invoke-virtual {v10}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v9

    if-eqz v9, :cond_6

    .line 476
    invoke-virtual {v9}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v2

    .line 478
    .local v2, "fileNameToUnzip":Ljava/lang/String;
    new-instance v7, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v7, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 480
    .local v7, "targetFile":Ljava/io/File;
    invoke-virtual {v9}, Ljava/util/zip/ZipEntry;->isDirectory()Z

    move-result v12

    if-eqz v12, :cond_4

    .line 482
    new-instance v5, Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v5, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 483
    .local v5, "path":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v12

    if-nez v12, :cond_0

    .line 485
    invoke-virtual {v5}, Ljava/io/File;->mkdirs()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    .line 499
    .end local v2    # "fileNameToUnzip":Ljava/lang/String;
    .end local v5    # "path":Ljava/io/File;
    .end local v7    # "targetFile":Ljava/io/File;
    :catch_0
    move-exception v1

    move-object v3, v4

    .line 501
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .local v1, "e":Ljava/lang/Exception;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    :goto_2
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 502
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Exception in unzip : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 508
    if-eqz v10, :cond_1

    .line 510
    :try_start_3
    invoke-virtual {v10}, Ljava/util/zip/ZipInputStream;->close()V

    .line 512
    :cond_1
    if-eqz v3, :cond_2

    .line 514
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 523
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_2
    :goto_3
    return v6

    .line 469
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    :cond_3
    :try_start_4
    new-instance v11, Ljava/util/zip/ZipInputStream;

    invoke-direct {v11, v4}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V

    .end local v10    # "zis":Ljava/util/zip/ZipInputStream;
    .restart local v11    # "zis":Ljava/util/zip/ZipInputStream;
    move-object v10, v11

    .end local v11    # "zis":Ljava/util/zip/ZipInputStream;
    .restart local v10    # "zis":Ljava/util/zip/ZipInputStream;
    goto/16 :goto_0

    .line 490
    .restart local v2    # "fileNameToUnzip":Ljava/lang/String;
    .restart local v7    # "targetFile":Ljava/io/File;
    :cond_4
    new-instance v5, Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v5, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 491
    .restart local v5    # "path":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v12

    if-nez v12, :cond_5

    .line 493
    invoke-virtual {v5}, Ljava/io/File;->mkdirs()Z

    .line 495
    :cond_5
    invoke-static {v10, v7}, Lcom/policydm/db/XDBFile;->xdbFileUnzipEntry(Ljava/util/zip/ZipInputStream;Ljava/io/File;)Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result v6

    goto :goto_1

    .line 508
    .end local v2    # "fileNameToUnzip":Ljava/lang/String;
    .end local v5    # "path":Ljava/io/File;
    .end local v7    # "targetFile":Ljava/io/File;
    :cond_6
    if-eqz v10, :cond_7

    .line 510
    :try_start_5
    invoke-virtual {v10}, Ljava/util/zip/ZipInputStream;->close()V

    .line 512
    :cond_7
    if-eqz v4, :cond_8

    .line 514
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    :cond_8
    move-object v3, v4

    .line 521
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    goto :goto_3

    .line 517
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    :catch_1
    move-exception v1

    .line 519
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 520
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Error closing : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move-object v3, v4

    .line 522
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    goto :goto_3

    .line 517
    .local v1, "e":Ljava/lang/Exception;
    :catch_2
    move-exception v1

    .line 519
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 520
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Error closing : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_3

    .line 506
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v12

    .line 508
    :goto_4
    if-eqz v10, :cond_9

    .line 510
    :try_start_6
    invoke-virtual {v10}, Ljava/util/zip/ZipInputStream;->close()V

    .line 512
    :cond_9
    if-eqz v3, :cond_a

    .line 514
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 521
    :cond_a
    :goto_5
    throw v12

    .line 517
    :catch_3
    move-exception v1

    .line 519
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 520
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Error closing : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_5

    .line 506
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v12

    move-object v3, v4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    goto :goto_4

    .line 499
    :catch_4
    move-exception v1

    goto/16 :goto_2
.end method

.method private static xdbFileUnzipEntry(Ljava/util/zip/ZipInputStream;Ljava/io/File;)Z
    .locals 10
    .param p0, "zis"    # Ljava/util/zip/ZipInputStream;
    .param p1, "targetFile"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 528
    const/4 v1, 0x1

    .line 529
    .local v1, "bRet":Z
    const v0, 0xfa000

    .line 531
    .local v0, "BUFFER_SIZE":I
    const/4 v4, 0x0

    .line 535
    .local v4, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 537
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .local v5, "fos":Ljava/io/FileOutputStream;
    :try_start_1
    new-array v2, v0, [B

    .line 538
    .local v2, "buffer":[B
    const/4 v6, 0x0

    .line 539
    .local v6, "len":I
    :goto_0
    invoke-virtual {p0, v2}, Ljava/util/zip/ZipInputStream;->read([B)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v6

    const/4 v8, -0x1

    if-eq v6, v8, :cond_3

    .line 541
    if-nez v6, :cond_1

    .line 558
    if-eqz v5, :cond_0

    .line 560
    :try_start_2
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    :goto_1
    move-object v4, v5

    .line 569
    .end local v2    # "buffer":[B
    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .end local v6    # "len":I
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    :goto_2
    return v7

    .line 563
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "buffer":[B
    .restart local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "len":I
    :catch_0
    move-exception v3

    .line 565
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    .line 566
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Error closing : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 545
    .end local v3    # "e":Ljava/io/IOException;
    :cond_1
    const/4 v8, 0x0

    :try_start_3
    invoke-virtual {v5, v2, v8, v6}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    .line 548
    .end local v2    # "buffer":[B
    .end local v6    # "len":I
    :catch_1
    move-exception v3

    move-object v4, v5

    .line 550
    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .local v3, "e":Ljava/lang/Exception;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    :goto_3
    const/4 v1, 0x0

    .line 551
    :try_start_4
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    .line 552
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Exception in unzipEntry : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 558
    if-eqz v4, :cond_2

    .line 560
    :try_start_5
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .end local v3    # "e":Ljava/lang/Exception;
    :cond_2
    :goto_4
    move v7, v1

    .line 569
    goto :goto_2

    .line 558
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "buffer":[B
    .restart local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "len":I
    :cond_3
    if-eqz v5, :cond_4

    .line 560
    :try_start_6
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    :cond_4
    move-object v4, v5

    .line 567
    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    goto :goto_4

    .line 563
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v5    # "fos":Ljava/io/FileOutputStream;
    :catch_2
    move-exception v3

    .line 565
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    .line 566
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Error closing : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move-object v4, v5

    .line 568
    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    goto :goto_4

    .line 563
    .end local v2    # "buffer":[B
    .end local v6    # "len":I
    .local v3, "e":Ljava/lang/Exception;
    :catch_3
    move-exception v3

    .line 565
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    .line 566
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Error closing : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_4

    .line 556
    .end local v3    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    .line 558
    :goto_5
    if-eqz v4, :cond_5

    .line 560
    :try_start_7
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    .line 567
    :cond_5
    :goto_6
    throw v7

    .line 563
    :catch_4
    move-exception v3

    .line 565
    .restart local v3    # "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    .line 566
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Error closing : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_6

    .line 556
    .end local v3    # "e":Ljava/io/IOException;
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v5    # "fos":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v7

    move-object v4, v5

    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    goto :goto_5

    .line 548
    :catch_5
    move-exception v3

    goto/16 :goto_3
.end method

.method public static xdbFileWrite(Ljava/lang/String;ILjava/lang/Object;)Z
    .locals 6
    .param p0, "szPath"    # Ljava/lang/String;
    .param p1, "nSize"    # I
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v4, 0x0

    .line 234
    check-cast p2, [B

    .end local p2    # "data":Ljava/lang/Object;
    move-object v3, p2

    check-cast v3, [B

    .line 235
    .local v3, "tmp":[B
    const/4 v0, 0x0

    .line 239
    .local v0, "ObjOut":Ljava/io/DataOutputStream;
    :try_start_0
    new-instance v1, Ljava/io/DataOutputStream;

    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, p0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v5}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 240
    .end local v0    # "ObjOut":Ljava/io/DataOutputStream;
    .local v1, "ObjOut":Ljava/io/DataOutputStream;
    const/4 v5, 0x0

    :try_start_1
    invoke-virtual {v1, v3, v5, p1}, Ljava/io/DataOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 251
    if-eqz v1, :cond_0

    .line 253
    :try_start_2
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 261
    :cond_0
    :goto_0
    const/4 v3, 0x0

    .line 262
    const/4 v4, 0x1

    move-object v0, v1

    .end local v1    # "ObjOut":Ljava/io/DataOutputStream;
    .restart local v0    # "ObjOut":Ljava/io/DataOutputStream;
    :cond_1
    :goto_1
    return v4

    .line 256
    .end local v0    # "ObjOut":Ljava/io/DataOutputStream;
    .restart local v1    # "ObjOut":Ljava/io/DataOutputStream;
    :catch_0
    move-exception v2

    .line 258
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 242
    .end local v1    # "ObjOut":Ljava/io/DataOutputStream;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v0    # "ObjOut":Ljava/io/DataOutputStream;
    :catch_1
    move-exception v2

    .line 244
    .local v2, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_3
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 251
    if-eqz v0, :cond_1

    .line 253
    :try_start_4
    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    .line 256
    :catch_2
    move-exception v2

    .line 258
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 249
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 251
    :goto_3
    if-eqz v0, :cond_2

    .line 253
    :try_start_5
    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 259
    :cond_2
    :goto_4
    throw v4

    .line 256
    :catch_3
    move-exception v2

    .line 258
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_4

    .line 249
    .end local v0    # "ObjOut":Ljava/io/DataOutputStream;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v1    # "ObjOut":Ljava/io/DataOutputStream;
    :catchall_1
    move-exception v4

    move-object v0, v1

    .end local v1    # "ObjOut":Ljava/io/DataOutputStream;
    .restart local v0    # "ObjOut":Ljava/io/DataOutputStream;
    goto :goto_3

    .line 242
    .end local v0    # "ObjOut":Ljava/io/DataOutputStream;
    .restart local v1    # "ObjOut":Ljava/io/DataOutputStream;
    :catch_4
    move-exception v2

    move-object v0, v1

    .end local v1    # "ObjOut":Ljava/io/DataOutputStream;
    .restart local v0    # "ObjOut":Ljava/io/DataOutputStream;
    goto :goto_2
.end method

.method public static xdbFileWrite(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 5
    .param p0, "szPath"    # Ljava/lang/String;
    .param p1, "data"    # Ljava/lang/Object;

    .prologue
    .line 267
    const/4 v0, 0x0

    .line 270
    .local v0, "ObjOut":Ljava/io/ObjectOutputStream;
    :try_start_0
    new-instance v1, Ljava/io/ObjectOutputStream;

    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, p0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v3}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 271
    .end local v0    # "ObjOut":Ljava/io/ObjectOutputStream;
    .local v1, "ObjOut":Ljava/io/ObjectOutputStream;
    :try_start_1
    invoke-virtual {v1}, Ljava/io/ObjectOutputStream;->reset()V

    .line 272
    invoke-virtual {v1, p1}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 283
    if-eqz v1, :cond_0

    .line 285
    :try_start_2
    invoke-virtual {v1}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 294
    :cond_0
    :goto_0
    const/4 v3, 0x1

    move-object v0, v1

    .end local v1    # "ObjOut":Ljava/io/ObjectOutputStream;
    .restart local v0    # "ObjOut":Ljava/io/ObjectOutputStream;
    :cond_1
    :goto_1
    return v3

    .line 288
    .end local v0    # "ObjOut":Ljava/io/ObjectOutputStream;
    .restart local v1    # "ObjOut":Ljava/io/ObjectOutputStream;
    :catch_0
    move-exception v2

    .line 290
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 274
    .end local v1    # "ObjOut":Ljava/io/ObjectOutputStream;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v0    # "ObjOut":Ljava/io/ObjectOutputStream;
    :catch_1
    move-exception v2

    .line 276
    .local v2, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_3
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 277
    const/4 v3, 0x0

    .line 283
    if-eqz v0, :cond_1

    .line 285
    :try_start_4
    invoke-virtual {v0}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    .line 288
    :catch_2
    move-exception v2

    .line 290
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 281
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    .line 283
    :goto_3
    if-eqz v0, :cond_2

    .line 285
    :try_start_5
    invoke-virtual {v0}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 291
    :cond_2
    :goto_4
    throw v3

    .line 288
    :catch_3
    move-exception v2

    .line 290
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_4

    .line 281
    .end local v0    # "ObjOut":Ljava/io/ObjectOutputStream;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v1    # "ObjOut":Ljava/io/ObjectOutputStream;
    :catchall_1
    move-exception v3

    move-object v0, v1

    .end local v1    # "ObjOut":Ljava/io/ObjectOutputStream;
    .restart local v0    # "ObjOut":Ljava/io/ObjectOutputStream;
    goto :goto_3

    .line 274
    .end local v0    # "ObjOut":Ljava/io/ObjectOutputStream;
    .restart local v1    # "ObjOut":Ljava/io/ObjectOutputStream;
    :catch_4
    move-exception v2

    move-object v0, v1

    .end local v1    # "ObjOut":Ljava/io/ObjectOutputStream;
    .restart local v0    # "ObjOut":Ljava/io/ObjectOutputStream;
    goto :goto_2
.end method

.method public static xdbFolderCreate(Ljava/lang/String;)Z
    .locals 5
    .param p0, "szPath"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 108
    :try_start_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 109
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v3

    if-nez v3, :cond_1

    .line 111
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v3

    if-nez v3, :cond_0

    .line 121
    .end local v1    # "file":Ljava/io/File;
    :goto_0
    return v2

    .line 113
    .restart local v1    # "file":Ljava/io/File;
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "make ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] folder"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 121
    :cond_1
    const/4 v2, 0x1

    goto :goto_0

    .line 116
    .end local v1    # "file":Ljava/io/File;
    :catch_0
    move-exception v0

    .line 118
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbFolderExist(Ljava/lang/String;)Z
    .locals 4
    .param p0, "szPath"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 72
    :try_start_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 73
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v3

    if-nez v3, :cond_0

    .line 75
    const-string v3, "Folder is not Exist!!"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    .end local v1    # "file":Ljava/io/File;
    :goto_0
    return v2

    .line 79
    :catch_0
    move-exception v0

    .line 81
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 84
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "file":Ljava/io/File;
    :cond_0
    const/4 v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public xdbFileCreate(Ljava/lang/String;)Z
    .locals 4
    .param p1, "szPath"    # Ljava/lang/String;

    .prologue
    .line 89
    const/4 v0, 0x0

    .line 93
    .local v0, "bRet":Z
    :try_start_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 94
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 101
    .end local v2    # "file":Ljava/io/File;
    :goto_0
    return v0

    .line 96
    :catch_0
    move-exception v1

    .line 98
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method
