.class public Lcom/policydm/db/XDB;
.super Lcom/policydm/db/XDBFactoryBootstrap;
.source "XDB.java"

# interfaces
.implements Lcom/policydm/interfaces/XDBInterface;
.implements Lcom/policydm/interfaces/XDLInterface;
.implements Lcom/policydm/interfaces/XDMExternalInterface;
.implements Lcom/policydm/interfaces/XTPInterface;
.implements Lcom/policydm/interfaces/XUICInterface;
.implements Lcom/policydm/interfaces/XUIInterface;
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/policydm/db/XDB$XDMFileParameter;,
        Lcom/policydm/db/XDB$XDMNVMParameter;
    }
.end annotation


# static fields
.field public static ConRef:Lcom/policydm/db/XDBInfoConRef; = null

.field public static final DMINFOMAGIC:I = 0x933

.field public static final DMPROFILEMAGIC:I = 0xec7

.field public static final NVMDMAccXNode:I = 0x83

.field public static final NVMDMAgentInfo:I = 0x61

.field public static final NVMDMInfo1:I = 0x52

.field public static final NVMDMInfo2:I = 0x53

.field public static final NVMDMInfo3:I = 0x54

.field public static final NVMDMInfo4:I = 0x55

.field public static final NVMDMInfo5:I = 0x56

.field public static final NVMDMProfile:I = 0x51

.field public static final NVMFUMOInfo:I = 0x57

.field public static final NVMIMSIInfo:I = 0x58

.field public static final NVMNetworkInfo1:I = 0x80

.field public static final NVMNetworkInfo2:I = 0x81

.field public static final NVMNetworkInfo3:I = 0x82

.field public static final NVMNotiInfo:I = 0x62

.field public static final NVMPollingInfo:I = 0x59

.field public static final NVMResyncMode:I = 0x60

.field public static final NVMSPDInfo:I = 0x63

.field public static NetProfileClass:Lcom/policydm/db/XDBNetworkProfileList; = null

.field public static ProfileInfoClass:[Lcom/policydm/db/XDBProfileInfo; = null

.field private static XDMFileParam:[Lcom/policydm/db/XDBFileParam; = null

.field private static XDMNVMParamCount:I = 0x0

.field public static XDMNvmClass:Lcom/policydm/db/XDBNvm; = null

.field public static final XDM_NET_PROFILE_LIST:I = 0x1

.field public static final XDM_PROFILE_LIST:I = 0x0

.field public static final XDM_PROFILE_LIST_VIEW:I = 0x2

.field private static final serialVersionUID:J = 0x1L

.field private static xdmSyncMLFileParam:[Lcom/policydm/db/XDBFileNVMParam;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 134
    sget-object v0, Lcom/policydm/db/XDB$XDMNVMParameter;->NVM_MAX:Lcom/policydm/db/XDB$XDMNVMParameter;

    invoke-virtual {v0}, Lcom/policydm/db/XDB$XDMNVMParameter;->Value()I

    move-result v0

    sput v0, Lcom/policydm/db/XDB;->XDMNVMParamCount:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/policydm/db/XDBFactoryBootstrap;-><init>()V

    .line 66
    return-void
.end method

.method public static xdbCheckActiveProfileIndexByServerID(Ljava/lang/String;)Z
    .locals 5
    .param p0, "szInputServerId"    # Ljava/lang/String;

    .prologue
    .line 1792
    const/4 v3, 0x0

    .line 1793
    .local v3, "szServerId":Ljava/lang/String;
    const/4 v0, 0x0

    .line 1795
    .local v0, "bActiveProfile":Z
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1797
    const-string v4, "ServerID is NULL"

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1798
    const/4 v4, 0x0

    .line 1819
    :goto_0
    return v4

    .line 1801
    :cond_0
    invoke-static {}, Lcom/policydm/db/XDBProfileAdp;->xdbGetServerID()Ljava/lang/String;

    move-result-object v3

    .line 1802
    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1803
    const/4 v4, 0x1

    goto :goto_0

    .line 1805
    :cond_1
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    const/4 v4, 0x3

    if-ge v2, v4, :cond_2

    .line 1807
    add-int/lit8 v4, v2, 0x52

    invoke-static {v4}, Lcom/policydm/db/XDBSqlAdapter;->xdmDbSqlRead(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/policydm/db/XDBProfileInfo;

    .line 1809
    .local v1, "dmInfo":Lcom/policydm/db/XDBProfileInfo;
    if-eqz v1, :cond_3

    .line 1811
    iget-object v4, v1, Lcom/policydm/db/XDBProfileInfo;->ServerID:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1813
    const/4 v0, 0x1

    .end local v1    # "dmInfo":Lcom/policydm/db/XDBProfileInfo;
    :cond_2
    move v4, v0

    .line 1819
    goto :goto_0

    .line 1805
    .restart local v1    # "dmInfo":Lcom/policydm/db/XDBProfileInfo;
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public static xdbCheckProfileListExist()Z
    .locals 4

    .prologue
    .line 1769
    const/4 v2, 0x0

    .line 1770
    .local v2, "pDMProfile":Lcom/policydm/db/XDBProfileListInfo;
    const/4 v0, 0x0

    .line 1771
    .local v0, "bReturn":Z
    const/4 v1, 0x0

    .line 1773
    .local v1, "i":I
    new-instance v2, Lcom/policydm/db/XDBProfileListInfo;

    .end local v2    # "pDMProfile":Lcom/policydm/db/XDBProfileListInfo;
    invoke-direct {v2}, Lcom/policydm/db/XDBProfileListInfo;-><init>()V

    .line 1774
    .restart local v2    # "pDMProfile":Lcom/policydm/db/XDBProfileListInfo;
    invoke-static {v2}, Lcom/policydm/db/XDBProfileListAdp;->xdbGetProflieList(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "pDMProfile":Lcom/policydm/db/XDBProfileListInfo;
    check-cast v2, Lcom/policydm/db/XDBProfileListInfo;

    .line 1776
    .restart local v2    # "pDMProfile":Lcom/policydm/db/XDBProfileListInfo;
    if-eqz v2, :cond_0

    .line 1778
    const/4 v1, 0x0

    :goto_0
    const/4 v3, 0x3

    if-ge v1, v3, :cond_0

    .line 1780
    iget-object v3, v2, Lcom/policydm/db/XDBProfileListInfo;->ProfileName:[Ljava/lang/String;

    aget-object v3, v3, v1

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1782
    const/4 v0, 0x1

    .line 1787
    :cond_0
    return v0

    .line 1778
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static xdbDMffs_Init()V
    .locals 7

    .prologue
    const-wide/16 v5, 0x1

    const/4 v4, 0x0

    .line 1528
    const/4 v1, 0x0

    .line 1530
    .local v1, "nCount":I
    const-string v2, "xdbDMffs_Init"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1535
    const/16 v0, 0x83

    .line 1536
    .local v0, "AreaCodeTemp":I
    const-wide/16 v2, 0x3

    invoke-static {v2, v3}, Lcom/policydm/db/XDBSqlQuery;->xdmDbExistsAccXListNodeRow(J)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1537
    invoke-static {v0, v4}, Lcom/policydm/db/XDB;->xdbInitFfsFile(II)V

    .line 1540
    :cond_0
    const/4 v1, 0x0

    :goto_0
    const/4 v2, 0x3

    if-ge v1, v2, :cond_2

    .line 1542
    add-int/lit8 v0, v1, 0x52

    .line 1543
    add-int/lit8 v2, v1, 0x1

    int-to-long v2, v2

    invoke-static {v2, v3}, Lcom/policydm/db/XDBSqlQuery;->xdmDbExistsProfileRow(J)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1544
    add-int/lit16 v2, v1, 0x933

    invoke-static {v0, v2}, Lcom/policydm/db/XDB;->xdbInitFfsFile(II)V

    .line 1540
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1547
    :cond_2
    const/16 v0, 0x51

    .line 1548
    invoke-static {v5, v6}, Lcom/policydm/db/XDBSqlQuery;->xdmDbExistsProfileListRow(J)Z

    move-result v2

    if-nez v2, :cond_9

    .line 1549
    const/16 v2, 0xec7

    invoke-static {v0, v2}, Lcom/policydm/db/XDB;->xdbInitFfsFile(II)V

    .line 1553
    :goto_1
    const/16 v0, 0x57

    .line 1554
    invoke-static {v5, v6}, Lcom/policydm/db/XDBSqlQuery;->xdmDbExistsFUMORow(J)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1555
    invoke-static {v0, v4}, Lcom/policydm/db/XDB;->xdbInitFfsFile(II)V

    .line 1557
    :cond_3
    const/16 v0, 0x58

    .line 1558
    invoke-static {v5, v6}, Lcom/policydm/db/XDBSqlQuery;->xdmDbExistsSimInfoRow(J)Z

    move-result v2

    if-nez v2, :cond_4

    .line 1559
    invoke-static {v0, v4}, Lcom/policydm/db/XDB;->xdbInitFfsFile(II)V

    .line 1561
    :cond_4
    const/16 v0, 0x59

    .line 1562
    invoke-static {v5, v6}, Lcom/policydm/db/XDBSqlQuery;->xdmDbExistsPollingRow(J)Z

    move-result v2

    if-nez v2, :cond_5

    .line 1563
    invoke-static {v0, v4}, Lcom/policydm/db/XDB;->xdbInitFfsFile(II)V

    .line 1567
    :cond_5
    const/16 v0, 0x60

    .line 1568
    invoke-static {v5, v6}, Lcom/policydm/db/XDBSqlQuery;->xdmDbExistsResyncModeRow(J)Z

    move-result v2

    if-nez v2, :cond_6

    .line 1569
    invoke-static {v0, v4}, Lcom/policydm/db/XDB;->xdbInitFfsFile(II)V

    .line 1572
    :cond_6
    const/16 v0, 0x61

    .line 1573
    invoke-static {v5, v6}, Lcom/policydm/db/XDBSqlQuery;->xdmDbExistsAgentInfoRow(J)Z

    move-result v2

    if-nez v2, :cond_7

    .line 1574
    invoke-static {v0, v4}, Lcom/policydm/db/XDB;->xdbInitFfsFile(II)V

    .line 1576
    :cond_7
    const/16 v0, 0x63

    .line 1577
    invoke-static {v5, v6}, Lcom/policydm/db/XDBSqlQuery;->xdmDbExistsSpdRow(J)Z

    move-result v2

    if-nez v2, :cond_8

    .line 1578
    invoke-static {v0, v4}, Lcom/policydm/db/XDB;->xdbInitFfsFile(II)V

    .line 1579
    :cond_8
    return-void

    .line 1551
    :cond_9
    invoke-static {v4}, Lcom/policydm/db/XDB;->xdbReadListInfo(I)Ljava/lang/Object;

    goto :goto_1
.end method

.method public static xdbDoDMBootStrapURI([C[C)[C
    .locals 12
    .param p0, "BootURI"    # [C
    .param p1, "BootPort"    # [C

    .prologue
    const/4 v9, 0x0

    .line 1949
    const/4 v1, 0x0

    .line 1950
    .local v1, "UriLen":I
    const/16 v10, 0x40

    new-array v8, v10, [C

    .line 1951
    .local v8, "temp":[C
    const/4 v0, 0x0

    .line 1952
    .local v0, "ResultURI":[C
    const/4 v2, 0x0

    .line 1953
    .local v2, "i":I
    const/4 v7, 0x0

    .line 1954
    .local v7, "t":I
    const/4 v3, 0x0

    .line 1955
    .local v3, "nCount":I
    const/4 v4, 0x0

    .line 1957
    .local v4, "nPortCount":I
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 2012
    :cond_0
    :goto_0
    return-object v9

    .line 1960
    :cond_1
    array-length v1, p0

    .line 1962
    const/4 v2, 0x0

    :goto_1
    if-ge v2, v1, :cond_0

    .line 1964
    aget-char v10, p0, v2

    const/16 v11, 0x2f

    if-ne v10, v11, :cond_3

    .line 1966
    add-int/lit8 v3, v3, 0x1

    .line 1973
    :cond_2
    :goto_2
    const/4 v10, 0x2

    if-ne v4, v10, :cond_4

    .line 1975
    move-object v0, p0

    move-object v9, v0

    .line 1976
    goto :goto_0

    .line 1968
    :cond_3
    aget-char v10, p0, v2

    const/16 v11, 0x3a

    if-ne v10, v11, :cond_2

    .line 1970
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 1979
    :cond_4
    const/4 v10, 0x3

    if-ne v3, v10, :cond_6

    .line 1981
    array-length v9, p1

    if-nez v9, :cond_5

    const/4 v9, 0x0

    aget-char v9, p1, v9

    if-nez v9, :cond_5

    .line 1983
    const-string v9, "80"

    invoke-virtual {v9}, Ljava/lang/String;->toCharArray()[C

    move-result-object p1

    .line 1984
    invoke-static {v8}, Lcom/policydm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v5

    .line 1985
    .local v5, "szArg":Ljava/lang/String;
    const-string v9, ":"

    invoke-virtual {v5, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1986
    invoke-static {p1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1987
    invoke-static {p0}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v6

    .line 1988
    .local v6, "szPath":Ljava/lang/String;
    invoke-virtual {v6, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    .line 1989
    invoke-virtual {v5, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1990
    invoke-virtual {v5}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    move-object v9, v0

    .line 1992
    goto :goto_0

    .line 1996
    .end local v5    # "szArg":Ljava/lang/String;
    .end local v6    # "szPath":Ljava/lang/String;
    :cond_5
    invoke-static {v8}, Lcom/policydm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v5

    .line 1997
    .restart local v5    # "szArg":Ljava/lang/String;
    const-string v9, ":"

    invoke-virtual {v5, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1998
    invoke-static {p1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1999
    invoke-static {p0}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v6

    .line 2000
    .restart local v6    # "szPath":Ljava/lang/String;
    invoke-virtual {v6, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    .line 2001
    invoke-virtual {v5, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2002
    invoke-virtual {v5}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    .line 2004
    const/4 v7, 0x0

    move-object v9, v0

    .line 2005
    goto :goto_0

    .line 2009
    .end local v5    # "szArg":Ljava/lang/String;
    .end local v6    # "szPath":Ljava/lang/String;
    :cond_6
    aget-char v10, p0, v2

    aput-char v10, v8, v7

    .line 2010
    add-int/lit8 v7, v7, 0x1

    .line 1962
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public static xdbFullResetAll()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1493
    invoke-static {}, Lcom/policydm/db/XDB;->xdbFullResetFFS()V

    .line 1494
    invoke-static {}, Lcom/policydm/db/XDBSqlQuery;->xdmDbFullReset()V

    .line 1495
    invoke-static {}, Lcom/policydm/db/XDB;->xdbInit()Z

    .line 1496
    invoke-static {}, Lcom/policydm/db/XDB;->xdbDMffs_Init()V

    .line 1497
    const/4 v0, -0x1

    sput v0, Lcom/policydm/db/XDBFumoAdp;->gFumoStatus:I

    .line 1498
    invoke-static {}, Lcom/policydm/adapter/XDMInitAdapter;->xdmInitAdpEXTInit()Z

    .line 1499
    invoke-static {v1}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetSyncMode(I)Z

    .line 1500
    invoke-static {v1}, Lcom/policydm/agent/XDMAgent;->xdmAgentTpSetRetryCount(I)V

    .line 1502
    invoke-static {v1}, Lcom/policydm/ui/XUINotificationManager;->xuiSetIndicator(I)V

    .line 1503
    return-void
.end method

.method public static xdbFullResetFFS()V
    .locals 5

    .prologue
    .line 1507
    const/4 v1, 0x0

    .line 1511
    .local v1, "i":I
    sget-object v3, Lcom/policydm/db/XDB$XDMFileParameter;->FileMax:Lcom/policydm/db/XDB$XDMFileParameter;

    invoke-virtual {v3}, Lcom/policydm/db/XDB$XDMFileParameter;->Index()I

    move-result v0

    .line 1512
    .local v0, "IndexLen":I
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    .line 1514
    sget-object v3, Lcom/policydm/db/XDB;->XDMFileParam:[Lcom/policydm/db/XDBFileParam;

    aget-object v2, v3, v1

    .line 1515
    .local v2, "pFileParam":Lcom/policydm/db/XDBFileParam;
    const/4 v3, 0x0

    iget v4, v2, Lcom/policydm/db/XDBFileParam;->FileID:I

    invoke-static {v3, v4}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpFileExists(Ljava/lang/String;I)I

    move-result v3

    if-nez v3, :cond_0

    .line 1517
    iget v3, v2, Lcom/policydm/db/XDBFileParam;->FileID:I

    invoke-static {v3}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpDeleteFile(I)I

    move-result v3

    if-nez v3, :cond_0

    .line 1519
    const/4 v3, 0x0

    iput v3, v2, Lcom/policydm/db/XDBFileParam;->nSize:I

    .line 1512
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1523
    .end local v2    # "pFileParam":Lcom/policydm/db/XDBFileParam;
    :cond_1
    return-void
.end method

.method public static xdbGetAppID(I)Ljava/lang/String;
    .locals 4
    .param p0, "appId"    # I

    .prologue
    .line 1744
    const-string v2, ""

    .line 1747
    .local v2, "szAppID":Ljava/lang/String;
    packed-switch p0, :pswitch_data_0

    .line 1756
    const/16 v3, 0x3a

    :try_start_0
    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0

    .line 1764
    :goto_0
    :pswitch_0
    return-object v2

    .line 1750
    :pswitch_1
    const/16 v3, 0x3a

    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1751
    goto :goto_0

    .line 1760
    :catch_0
    move-exception v1

    .line 1762
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 1747
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static xdbGetAutoUpdateFlag()Z
    .locals 6

    .prologue
    .line 2121
    const/4 v2, 0x0

    .line 2122
    .local v2, "status":I
    const/4 v0, 0x0

    .line 2125
    .local v0, "bAutoUpdate":Z
    :try_start_0
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "security_update_db"

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 2132
    :goto_0
    if-lez v2, :cond_0

    .line 2133
    const/4 v0, 0x1

    .line 2135
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bAutoUpdate : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 2137
    return v0

    .line 2127
    :catch_0
    move-exception v1

    .line 2129
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetConnectType(I)I
    .locals 3
    .param p0, "nAppId"    # I

    .prologue
    .line 1841
    const/4 v1, 0x0

    .line 1843
    .local v1, "type":I
    packed-switch p0, :pswitch_data_0

    .line 1873
    const/4 v1, 0x2

    .line 1876
    :goto_0
    return v1

    .line 1847
    :pswitch_0
    const-string v0, ""

    .line 1848
    .local v0, "szProtocol":Ljava/lang/String;
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetProtocol()Ljava/lang/String;

    move-result-object v0

    .line 1849
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1851
    invoke-static {v0}, Lcom/policydm/tp/XTPHttpUtil;->xtpHttpExchangeProtocolType(Ljava/lang/String;)I

    move-result v1

    goto :goto_0

    .line 1855
    :cond_0
    const/4 v1, 0x2

    .line 1857
    goto :goto_0

    .line 1861
    .end local v0    # "szProtocol":Ljava/lang/String;
    :pswitch_1
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetDLProtocol()Ljava/lang/String;

    move-result-object v0

    .line 1862
    .restart local v0    # "szProtocol":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1864
    invoke-static {v0}, Lcom/policydm/tp/XTPHttpUtil;->xtpHttpExchangeProtocolType(Ljava/lang/String;)I

    move-result v1

    goto :goto_0

    .line 1868
    :cond_1
    const/4 v1, 0x2

    .line 1870
    goto :goto_0

    .line 1843
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static xdbGetDMXNodeInfo(I)Ljava/lang/Object;
    .locals 3
    .param p0, "nIndex"    # I

    .prologue
    .line 1638
    const/4 v1, 0x0

    .line 1641
    .local v1, "ptAccXNodeInfo":Ljava/lang/Object;
    :try_start_0
    invoke-static {p0}, Lcom/policydm/db/XDB;->xdb_E2P_XDM_ACCXNODE_INFO(I)I

    move-result v2

    invoke-static {v2}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1647
    .end local v1    # "ptAccXNodeInfo":Ljava/lang/Object;
    :goto_0
    return-object v1

    .line 1643
    .restart local v1    # "ptAccXNodeInfo":Ljava/lang/Object;
    :catch_0
    move-exception v0

    .line 1645
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetEULAAgreement()Z
    .locals 6

    .prologue
    .line 2217
    const/4 v2, 0x0

    .line 2218
    .local v2, "status":I
    const/4 v0, 0x0

    .line 2220
    .local v0, "bAgree":Z
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmKorModel()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmEulaModel()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2222
    :cond_0
    const/4 v0, 0x0

    .line 2238
    :cond_1
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Samsung eula Agree : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 2240
    return v0

    .line 2228
    :cond_2
    :try_start_0
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "samsung_eula_agree"

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 2235
    :goto_1
    if-lez v2, :cond_1

    .line 2236
    const/4 v0, 0x1

    goto :goto_0

    .line 2230
    :catch_0
    move-exception v1

    .line 2232
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static xdbGetFileIdFirmwareData()I
    .locals 1

    .prologue
    .line 1611
    sget-object v0, Lcom/policydm/db/XDB$XDMFileParameter;->FileFirmwareData:Lcom/policydm/db/XDB$XDMFileParameter;

    invoke-virtual {v0}, Lcom/policydm/db/XDB$XDMFileParameter;->FileId()I

    move-result v0

    return v0
.end method

.method public static xdbGetFileIdLargeObjectData()I
    .locals 1

    .prologue
    .line 1616
    sget-object v0, Lcom/policydm/db/XDB$XDMFileParameter;->FileLargeObjectData:Lcom/policydm/db/XDB$XDMFileParameter;

    invoke-virtual {v0}, Lcom/policydm/db/XDB$XDMFileParameter;->FileId()I

    move-result v0

    return v0
.end method

.method public static xdbGetFileIdObjectData()I
    .locals 1

    .prologue
    .line 1601
    sget-object v0, Lcom/policydm/db/XDB$XDMFileParameter;->FileObjectData:Lcom/policydm/db/XDB$XDMFileParameter;

    invoke-virtual {v0}, Lcom/policydm/db/XDB$XDMFileParameter;->FileId()I

    move-result v0

    return v0
.end method

.method public static xdbGetFileIdObjectTreeInfo()I
    .locals 1

    .prologue
    .line 1606
    sget-object v0, Lcom/policydm/db/XDB$XDMFileParameter;->FileObjectTreeInfo:Lcom/policydm/db/XDB$XDMFileParameter;

    invoke-virtual {v0}, Lcom/policydm/db/XDB$XDMFileParameter;->FileId()I

    move-result v0

    return v0
.end method

.method public static xdbGetFileIdTNDS()I
    .locals 1

    .prologue
    .line 1621
    sget-object v0, Lcom/policydm/db/XDB$XDMFileParameter;->FileTndsXmlData:Lcom/policydm/db/XDB$XDMFileParameter;

    invoke-virtual {v0}, Lcom/policydm/db/XDB$XDMFileParameter;->FileId()I

    move-result v0

    return v0
.end method

.method public static xdbGetNotiDigest(Ljava/lang/String;I[BI)Ljava/lang/String;
    .locals 11
    .param p0, "szServerID"    # Ljava/lang/String;
    .param p1, "nAuthType"    # I
    .param p2, "pPacketBody"    # [B
    .param p3, "nBodyLen"    # I

    .prologue
    const/4 v0, 0x0

    .line 1899
    const-string v10, ""

    .line 1900
    .local v10, "szServerNonce":Ljava/lang/String;
    const-string v1, ""

    .line 1901
    .local v1, "szServerId":Ljava/lang/String;
    const-string v2, ""

    .line 1902
    .local v2, "szServerPwd":Ljava/lang/String;
    const-string v9, ""

    .line 1904
    .local v9, "szDigest":Ljava/lang/String;
    const/4 v4, 0x0

    .line 1906
    .local v4, "nNonceLen":I
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1908
    const-string v5, "pServerID is NULL"

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1944
    :cond_0
    :goto_0
    return-object v0

    .line 1912
    :cond_1
    invoke-static {}, Lcom/policydm/db/XDBProfileListAdp;->xdbGetProflieIndex()I

    move-result v8

    .line 1915
    .local v8, "nActive":I
    :try_start_0
    invoke-static {}, Lcom/policydm/db/XDBProfileAdp;->xdbGetServerID()Ljava/lang/String;

    move-result-object v1

    .line 1916
    invoke-static {}, Lcom/policydm/db/XDBProfileAdp;->xdbGetServerPassword()Ljava/lang/String;

    move-result-object v2

    .line 1917
    invoke-static {}, Lcom/policydm/db/XDBProfileAdp;->xdbGetServerNonce()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v10

    .line 1924
    :goto_1
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1928
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "nActive = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 1931
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "szServerNonce = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 1933
    const/4 v3, 0x0

    .line 1934
    .local v3, "pNonce":[B
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v5

    invoke-virtual {v10, v5}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v5

    invoke-static {v5}, Lcom/policydm/eng/core/XDMBase64;->xdmBase64Decode([B)[B

    move-result-object v3

    .line 1937
    if-eqz v3, :cond_0

    .line 1942
    array-length v4, v3

    move v0, p1

    move-object v5, p2

    move v6, p3

    .line 1943
    invoke-static/range {v0 .. v6}, Lcom/policydm/eng/core/XDMAuth;->xdmAuthMakeDigest(ILjava/lang/String;Ljava/lang/String;[BI[BI)Ljava/lang/String;

    move-result-object v9

    move-object v0, v9

    .line 1944
    goto :goto_0

    .line 1919
    .end local v3    # "pNonce":[B
    :catch_0
    move-exception v7

    .line 1921
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static xdbGetServerAddress(I)Ljava/lang/String;
    .locals 4
    .param p0, "appId"    # I

    .prologue
    .line 1691
    const-string v2, ""

    .line 1694
    .local v2, "szServerIP":Ljava/lang/String;
    packed-switch p0, :pswitch_data_0

    .line 1705
    const/16 v3, 0x36

    :try_start_0
    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0

    .line 1713
    :goto_0
    return-object v2

    .line 1697
    :pswitch_0
    const/16 v3, 0x36

    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0

    .line 1698
    goto :goto_0

    .line 1701
    :pswitch_1
    const/16 v3, 0xcf

    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1702
    goto :goto_0

    .line 1709
    :catch_0
    move-exception v1

    .line 1711
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 1694
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static xdbGetServerPort(I)I
    .locals 3
    .param p0, "appId"    # I

    .prologue
    .line 1718
    const/4 v1, 0x0

    .line 1721
    .local v1, "port":I
    packed-switch p0, :pswitch_data_0

    .line 1739
    :goto_0
    return v1

    .line 1724
    :pswitch_0
    const/16 v2, 0x38

    :try_start_0
    invoke-static {v2}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 1725
    goto :goto_0

    .line 1728
    :pswitch_1
    const/16 v2, 0xd0

    invoke-static {v2}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1729
    goto :goto_0

    .line 1735
    :catch_0
    move-exception v0

    .line 1737
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 1721
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static xdbGetServerUrl(I)Ljava/lang/String;
    .locals 4
    .param p0, "appId"    # I

    .prologue
    .line 1664
    const-string v2, ""

    .line 1667
    .local v2, "szServerUrl":Ljava/lang/String;
    packed-switch p0, :pswitch_data_0

    .line 1678
    const/16 v3, 0x35

    :try_start_0
    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0

    .line 1686
    :goto_0
    return-object v2

    .line 1670
    :pswitch_0
    const/16 v3, 0x35

    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0

    .line 1671
    goto :goto_0

    .line 1674
    :pswitch_1
    const/16 v3, 0xce

    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1675
    goto :goto_0

    .line 1682
    :catch_0
    move-exception v1

    .line 1684
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 1667
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static xdbGetSyncMLFileParamAreaCode(I)Ljava/lang/Object;
    .locals 3
    .param p0, "areacode"    # I

    .prologue
    .line 1442
    const/16 v1, 0x51

    if-ne p0, v1, :cond_0

    .line 1444
    sget-object v1, Lcom/policydm/db/XDB;->xdmSyncMLFileParam:[Lcom/policydm/db/XDBFileNVMParam;

    sget-object v2, Lcom/policydm/db/XDB$XDMNVMParameter;->NVM_DM_PROFILE:Lcom/policydm/db/XDB$XDMNVMParameter;

    invoke-virtual {v2}, Lcom/policydm/db/XDB$XDMNVMParameter;->Value()I

    move-result v2

    aget-object v0, v1, v2

    .line 1488
    :goto_0
    return-object v0

    .line 1446
    :cond_0
    const/16 v1, 0x52

    if-lt p0, v1, :cond_1

    const/16 v1, 0x56

    if-gt p0, v1, :cond_1

    .line 1448
    sget-object v1, Lcom/policydm/db/XDB;->xdmSyncMLFileParam:[Lcom/policydm/db/XDBFileNVMParam;

    sget-object v2, Lcom/policydm/db/XDB$XDMNVMParameter;->NVM_DM_INFO:Lcom/policydm/db/XDB$XDMNVMParameter;

    invoke-virtual {v2}, Lcom/policydm/db/XDB$XDMNVMParameter;->Value()I

    move-result v2

    aget-object v0, v1, v2

    .local v0, "SyncMLFileParamtemp":Lcom/policydm/db/XDBFileNVMParam;
    goto :goto_0

    .line 1450
    .end local v0    # "SyncMLFileParamtemp":Lcom/policydm/db/XDBFileNVMParam;
    :cond_1
    const/16 v1, 0x57

    if-ne p0, v1, :cond_2

    .line 1452
    sget-object v1, Lcom/policydm/db/XDB;->xdmSyncMLFileParam:[Lcom/policydm/db/XDBFileNVMParam;

    sget-object v2, Lcom/policydm/db/XDB$XDMNVMParameter;->NVM_FUMO_INFO:Lcom/policydm/db/XDB$XDMNVMParameter;

    invoke-virtual {v2}, Lcom/policydm/db/XDB$XDMNVMParameter;->Value()I

    move-result v2

    aget-object v0, v1, v2

    .restart local v0    # "SyncMLFileParamtemp":Lcom/policydm/db/XDBFileNVMParam;
    goto :goto_0

    .line 1454
    .end local v0    # "SyncMLFileParamtemp":Lcom/policydm/db/XDBFileNVMParam;
    :cond_2
    const/16 v1, 0x58

    if-ne p0, v1, :cond_3

    .line 1456
    sget-object v1, Lcom/policydm/db/XDB;->xdmSyncMLFileParam:[Lcom/policydm/db/XDBFileNVMParam;

    sget-object v2, Lcom/policydm/db/XDB$XDMNVMParameter;->NVM_IMSI_INFO:Lcom/policydm/db/XDB$XDMNVMParameter;

    invoke-virtual {v2}, Lcom/policydm/db/XDB$XDMNVMParameter;->Value()I

    move-result v2

    aget-object v0, v1, v2

    .restart local v0    # "SyncMLFileParamtemp":Lcom/policydm/db/XDBFileNVMParam;
    goto :goto_0

    .line 1458
    .end local v0    # "SyncMLFileParamtemp":Lcom/policydm/db/XDBFileNVMParam;
    :cond_3
    const/16 v1, 0x59

    if-ne p0, v1, :cond_4

    .line 1460
    sget-object v1, Lcom/policydm/db/XDB;->xdmSyncMLFileParam:[Lcom/policydm/db/XDBFileNVMParam;

    sget-object v2, Lcom/policydm/db/XDB$XDMNVMParameter;->NVM_POLLING_INFO:Lcom/policydm/db/XDB$XDMNVMParameter;

    invoke-virtual {v2}, Lcom/policydm/db/XDB$XDMNVMParameter;->Value()I

    move-result v2

    aget-object v0, v1, v2

    .restart local v0    # "SyncMLFileParamtemp":Lcom/policydm/db/XDBFileNVMParam;
    goto :goto_0

    .line 1462
    .end local v0    # "SyncMLFileParamtemp":Lcom/policydm/db/XDBFileNVMParam;
    :cond_4
    const/16 v1, 0x83

    if-ne p0, v1, :cond_5

    .line 1464
    sget-object v1, Lcom/policydm/db/XDB;->xdmSyncMLFileParam:[Lcom/policydm/db/XDBFileNVMParam;

    sget-object v2, Lcom/policydm/db/XDB$XDMNVMParameter;->NVM_DM_ACC_X_NODE:Lcom/policydm/db/XDB$XDMNVMParameter;

    invoke-virtual {v2}, Lcom/policydm/db/XDB$XDMNVMParameter;->Value()I

    move-result v2

    aget-object v0, v1, v2

    .restart local v0    # "SyncMLFileParamtemp":Lcom/policydm/db/XDBFileNVMParam;
    goto :goto_0

    .line 1466
    .end local v0    # "SyncMLFileParamtemp":Lcom/policydm/db/XDBFileNVMParam;
    :cond_5
    const/16 v1, 0x60

    if-ne p0, v1, :cond_6

    .line 1468
    sget-object v1, Lcom/policydm/db/XDB;->xdmSyncMLFileParam:[Lcom/policydm/db/XDBFileNVMParam;

    sget-object v2, Lcom/policydm/db/XDB$XDMNVMParameter;->NVM_NOTI_RESYNC_MODE:Lcom/policydm/db/XDB$XDMNVMParameter;

    invoke-virtual {v2}, Lcom/policydm/db/XDB$XDMNVMParameter;->Value()I

    move-result v2

    aget-object v0, v1, v2

    .restart local v0    # "SyncMLFileParamtemp":Lcom/policydm/db/XDBFileNVMParam;
    goto :goto_0

    .line 1470
    .end local v0    # "SyncMLFileParamtemp":Lcom/policydm/db/XDBFileNVMParam;
    :cond_6
    const/16 v1, 0x61

    if-ne p0, v1, :cond_7

    .line 1472
    sget-object v1, Lcom/policydm/db/XDB;->xdmSyncMLFileParam:[Lcom/policydm/db/XDBFileNVMParam;

    sget-object v2, Lcom/policydm/db/XDB$XDMNVMParameter;->NVM_DM_AGENT_INFO:Lcom/policydm/db/XDB$XDMNVMParameter;

    invoke-virtual {v2}, Lcom/policydm/db/XDB$XDMNVMParameter;->Value()I

    move-result v2

    aget-object v0, v1, v2

    .restart local v0    # "SyncMLFileParamtemp":Lcom/policydm/db/XDBFileNVMParam;
    goto :goto_0

    .line 1474
    .end local v0    # "SyncMLFileParamtemp":Lcom/policydm/db/XDBFileNVMParam;
    :cond_7
    const/16 v1, 0x62

    if-ne p0, v1, :cond_8

    .line 1476
    sget-object v1, Lcom/policydm/db/XDB;->xdmSyncMLFileParam:[Lcom/policydm/db/XDBFileNVMParam;

    sget-object v2, Lcom/policydm/db/XDB$XDMNVMParameter;->NVM_NOTI_INFO:Lcom/policydm/db/XDB$XDMNVMParameter;

    invoke-virtual {v2}, Lcom/policydm/db/XDB$XDMNVMParameter;->Value()I

    move-result v2

    aget-object v0, v1, v2

    .restart local v0    # "SyncMLFileParamtemp":Lcom/policydm/db/XDBFileNVMParam;
    goto :goto_0

    .line 1478
    .end local v0    # "SyncMLFileParamtemp":Lcom/policydm/db/XDBFileNVMParam;
    :cond_8
    const/16 v1, 0x63

    if-ne p0, v1, :cond_9

    .line 1480
    sget-object v1, Lcom/policydm/db/XDB;->xdmSyncMLFileParam:[Lcom/policydm/db/XDBFileNVMParam;

    sget-object v2, Lcom/policydm/db/XDB$XDMNVMParameter;->NVM_SPD_INFO:Lcom/policydm/db/XDB$XDMNVMParameter;

    invoke-virtual {v2}, Lcom/policydm/db/XDB$XDMNVMParameter;->Value()I

    move-result v2

    aget-object v0, v1, v2

    .restart local v0    # "SyncMLFileParamtemp":Lcom/policydm/db/XDBFileNVMParam;
    goto/16 :goto_0

    .line 1484
    .end local v0    # "SyncMLFileParamtemp":Lcom/policydm/db/XDBFileNVMParam;
    :cond_9
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FFS not find area code by num :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1485
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public static xdbGetWaitWifiFlag()Z
    .locals 6

    .prologue
    .line 2161
    const/4 v2, 0x0

    .line 2162
    .local v2, "status":I
    const/4 v0, 0x0

    .line 2165
    .local v0, "bWaitWifi":Z
    :try_start_0
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "use_Wait_Wifi_db"

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 2172
    :goto_0
    if-lez v2, :cond_0

    .line 2173
    const/4 v0, 0x1

    .line 2175
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bWaitWifi : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 2177
    return v0

    .line 2167
    :catch_0
    move-exception v1

    .line 2169
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetWifiOnlyFlag()Z
    .locals 6

    .prologue
    .line 2081
    const/4 v2, 0x0

    .line 2082
    .local v2, "status":I
    const/4 v0, 0x0

    .line 2085
    .local v0, "bWifiOnly":Z
    :try_start_0
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "use_wifi_only_db"

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 2092
    :goto_0
    if-lez v2, :cond_0

    .line 2093
    const/4 v0, 0x1

    .line 2095
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bWifiOnly : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 2097
    return v0

    .line 2087
    :catch_0
    move-exception v1

    .line 2089
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbInit()Z
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 177
    :try_start_0
    invoke-static {}, Lcom/policydm/db/XDB$XDMFileParameter;->values()[Lcom/policydm/db/XDB$XDMFileParameter;

    move-result-object v1

    .line 179
    .local v1, "Files":[Lcom/policydm/db/XDB$XDMFileParameter;
    sget-object v7, Lcom/policydm/db/XDB$XDMFileParameter;->FileMax:Lcom/policydm/db/XDB$XDMFileParameter;

    invoke-virtual {v7}, Lcom/policydm/db/XDB$XDMFileParameter;->Index()I

    move-result v7

    new-array v7, v7, [Lcom/policydm/db/XDBFileParam;

    sput-object v7, Lcom/policydm/db/XDB;->XDMFileParam:[Lcom/policydm/db/XDBFileParam;

    .line 181
    move-object v2, v1

    .local v2, "arr$":[Lcom/policydm/db/XDB$XDMFileParameter;
    array-length v5, v2

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v0, v2, v4

    .line 183
    .local v0, "File":Lcom/policydm/db/XDB$XDMFileParameter;
    invoke-virtual {v0}, Lcom/policydm/db/XDB$XDMFileParameter;->Index()I

    move-result v7

    sget-object v8, Lcom/policydm/db/XDB$XDMFileParameter;->FileMax:Lcom/policydm/db/XDB$XDMFileParameter;

    invoke-virtual {v8}, Lcom/policydm/db/XDB$XDMFileParameter;->Index()I

    move-result v8

    if-ne v7, v8, :cond_1

    .line 191
    .end local v0    # "File":Lcom/policydm/db/XDB$XDMFileParameter;
    :cond_0
    sget v7, Lcom/policydm/db/XDB;->XDMNVMParamCount:I

    new-array v7, v7, [Lcom/policydm/db/XDBFileNVMParam;

    sput-object v7, Lcom/policydm/db/XDB;->xdmSyncMLFileParam:[Lcom/policydm/db/XDBFileNVMParam;

    .line 192
    sget-object v7, Lcom/policydm/db/XDB;->xdmSyncMLFileParam:[Lcom/policydm/db/XDBFileNVMParam;

    sget v8, Lcom/policydm/db/XDB;->XDMNVMParamCount:I

    invoke-static {v7, v8}, Lcom/policydm/db/XDB;->xdbSyncMLFileParmInit([Lcom/policydm/db/XDBFileNVMParam;I)[Lcom/policydm/db/XDBFileNVMParam;

    move-result-object v7

    sput-object v7, Lcom/policydm/db/XDB;->xdmSyncMLFileParam:[Lcom/policydm/db/XDBFileNVMParam;

    .line 194
    const/4 v7, 0x3

    new-array v7, v7, [Lcom/policydm/db/XDBProfileInfo;

    sput-object v7, Lcom/policydm/db/XDB;->ProfileInfoClass:[Lcom/policydm/db/XDBProfileInfo;

    .line 195
    new-instance v7, Lcom/policydm/db/XDBNetworkProfileList;

    invoke-direct {v7}, Lcom/policydm/db/XDBNetworkProfileList;-><init>()V

    sput-object v7, Lcom/policydm/db/XDB;->NetProfileClass:Lcom/policydm/db/XDBNetworkProfileList;

    .line 196
    new-instance v7, Lcom/policydm/db/XDBNvm;

    invoke-direct {v7}, Lcom/policydm/db/XDBNvm;-><init>()V

    sput-object v7, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    .line 197
    new-instance v7, Lcom/policydm/db/XDBInfoConRef;

    invoke-direct {v7}, Lcom/policydm/db/XDBInfoConRef;-><init>()V

    sput-object v7, Lcom/policydm/db/XDB;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    .line 205
    const/4 v6, 0x1

    .end local v2    # "arr$":[Lcom/policydm/db/XDB$XDMFileParameter;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    :goto_1
    return v6

    .line 186
    .restart local v0    # "File":Lcom/policydm/db/XDB$XDMFileParameter;
    .restart local v2    # "arr$":[Lcom/policydm/db/XDB$XDMFileParameter;
    .restart local v4    # "i$":I
    .restart local v5    # "len$":I
    :cond_1
    sget-object v7, Lcom/policydm/db/XDB;->XDMFileParam:[Lcom/policydm/db/XDBFileParam;

    invoke-virtual {v0}, Lcom/policydm/db/XDB$XDMFileParameter;->Index()I

    move-result v8

    new-instance v9, Lcom/policydm/db/XDBFileParam;

    invoke-direct {v9}, Lcom/policydm/db/XDBFileParam;-><init>()V

    aput-object v9, v7, v8

    .line 187
    sget-object v7, Lcom/policydm/db/XDB;->XDMFileParam:[Lcom/policydm/db/XDBFileParam;

    invoke-virtual {v0}, Lcom/policydm/db/XDB$XDMFileParameter;->Index()I

    move-result v8

    aget-object v7, v7, v8

    invoke-virtual {v0}, Lcom/policydm/db/XDB$XDMFileParameter;->FileId()I

    move-result v8

    iput v8, v7, Lcom/policydm/db/XDBFileParam;->FileID:I

    .line 188
    sget-object v7, Lcom/policydm/db/XDB;->XDMFileParam:[Lcom/policydm/db/XDBFileParam;

    invoke-virtual {v0}, Lcom/policydm/db/XDB$XDMFileParameter;->Index()I

    move-result v8

    aget-object v7, v7, v8

    const/4 v8, 0x0

    iput v8, v7, Lcom/policydm/db/XDBFileParam;->nSize:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 181
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 199
    .end local v0    # "File":Lcom/policydm/db/XDB$XDMFileParameter;
    .end local v2    # "arr$":[Lcom/policydm/db/XDB$XDMFileParameter;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    :catch_0
    move-exception v3

    .line 201
    .local v3, "ex":Ljava/lang/Exception;
    const-string v7, "xdbInit Exception"

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static xdbInitFfsFile(II)V
    .locals 3
    .param p0, "FileID"    # I
    .param p1, "nMagicNumber"    # I

    .prologue
    .line 1418
    const/4 v0, 0x0

    .line 1420
    .local v0, "pfileparam":Lcom/policydm/db/XDBFileNVMParam;
    invoke-static {p0}, Lcom/policydm/db/XDB;->xdbGetSyncMLFileParamAreaCode(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "pfileparam":Lcom/policydm/db/XDBFileNVMParam;
    check-cast v0, Lcom/policydm/db/XDBFileNVMParam;

    .line 1421
    .restart local v0    # "pfileparam":Lcom/policydm/db/XDBFileNVMParam;
    if-nez v0, :cond_1

    .line 1436
    :cond_0
    :goto_0
    return-void

    .line 1424
    :cond_1
    invoke-static {v0}, Lcom/policydm/db/XDB;->xdbSetSyncMLNVMUser(Lcom/policydm/db/XDBFileNVMParam;)Lcom/policydm/db/XDBFileNVMParam;

    move-result-object v0

    .line 1425
    if-eqz v0, :cond_0

    .line 1428
    iget v1, v0, Lcom/policydm/db/XDBFileNVMParam;->AreaCode:I

    iget-object v2, v0, Lcom/policydm/db/XDBFileNVMParam;->pNVMUser:Ljava/lang/Object;

    invoke-static {v1, v2, p1}, Lcom/policydm/db/XDB;->xdbLoadCallback(ILjava/lang/Object;I)Ljava/lang/Object;

    move-result-object v1

    iput-object v1, v0, Lcom/policydm/db/XDBFileNVMParam;->pNVMUser:Ljava/lang/Object;

    .line 1429
    iget-object v1, v0, Lcom/policydm/db/XDBFileNVMParam;->pNVMUser:Ljava/lang/Object;

    if-eqz v1, :cond_0

    .line 1432
    const/16 v1, 0x51

    if-lt p0, v1, :cond_0

    const/16 v1, 0x63

    if-gt p0, v1, :cond_0

    .line 1434
    iget-object v1, v0, Lcom/policydm/db/XDBFileNVMParam;->pNVMUser:Ljava/lang/Object;

    invoke-static {p0, v1}, Lcom/policydm/db/XDBSqlAdapter;->xdmDbSqlCreate(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method private static xdbInitNVMAccXNode(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p0, "SyncDMAccXNode"    # Ljava/lang/Object;

    .prologue
    .line 1310
    move-object v0, p0

    check-cast v0, Lcom/policydm/db/XDBAccXListNode;

    .line 1311
    .local v0, "stSyncDMAccXNode":Lcom/policydm/db/XDBAccXListNode;
    return-object v0
.end method

.method private static xdbInitNVMAgentTypeInfo(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p0, "NVMDmAgentInfo"    # Ljava/lang/Object;

    .prologue
    .line 1328
    move-object v0, p0

    check-cast v0, Lcom/policydm/db/XDBAgentInfo;

    .line 1330
    .local v0, "DmAgentInfo":Lcom/policydm/db/XDBAgentInfo;
    if-nez v0, :cond_0

    .line 1332
    new-instance v0, Lcom/policydm/db/XDBAgentInfo;

    .end local v0    # "DmAgentInfo":Lcom/policydm/db/XDBAgentInfo;
    invoke-direct {v0}, Lcom/policydm/db/XDBAgentInfo;-><init>()V

    .line 1339
    .restart local v0    # "DmAgentInfo":Lcom/policydm/db/XDBAgentInfo;
    :goto_0
    return-object v0

    .line 1336
    :cond_0
    const/4 v1, 0x0

    iput v1, v0, Lcom/policydm/db/XDBAgentInfo;->m_nAgentType:I

    goto :goto_0
.end method

.method private static xdbInitNVMDMInfo(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 2
    .param p0, "SyncdmInfo"    # Ljava/lang/Object;
    .param p1, "nMagicNumber"    # I

    .prologue
    .line 1266
    move-object v0, p0

    check-cast v0, Lcom/policydm/db/XDBProfileInfo;

    .line 1267
    .local v0, "NVMDMInfo":Lcom/policydm/db/XDBProfileInfo;
    const/4 v1, 0x0

    .line 1269
    .local v1, "nProfileIndex":I
    add-int/lit16 v1, p1, -0x933

    .line 1270
    invoke-static {v0, v1}, Lcom/policydm/db/XDB;->xdbFBGetFactoryBootstrapData(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "NVMDMInfo":Lcom/policydm/db/XDBProfileInfo;
    check-cast v0, Lcom/policydm/db/XDBProfileInfo;

    .line 1271
    .restart local v0    # "NVMDMInfo":Lcom/policydm/db/XDBProfileInfo;
    invoke-static {v0}, Lcom/policydm/adapter/XDMNetworkAdapter;->xdbAdpInitNetProfile(Ljava/lang/Object;)V

    .line 1272
    iput p1, v0, Lcom/policydm/db/XDBProfileInfo;->MagicNumber:I

    .line 1273
    return-object v0
.end method

.method private static xdbInitNVMFUMOInfo(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p0, "SyncDMFUMOInfo"    # Ljava/lang/Object;

    .prologue
    .line 1278
    move-object v0, p0

    check-cast v0, Lcom/policydm/db/XDBFumoInfo;

    .line 1280
    .local v0, "pFUMOInfo":Lcom/policydm/db/XDBFumoInfo;
    if-nez v0, :cond_0

    .line 1282
    new-instance v0, Lcom/policydm/db/XDBFumoInfo;

    .end local v0    # "pFUMOInfo":Lcom/policydm/db/XDBFumoInfo;
    invoke-direct {v0}, Lcom/policydm/db/XDBFumoInfo;-><init>()V

    .line 1283
    .restart local v0    # "pFUMOInfo":Lcom/policydm/db/XDBFumoInfo;
    const-string v1, "pFUMOInfo = null"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1286
    :cond_0
    const-string v1, "http://"

    iput-object v1, v0, Lcom/policydm/db/XDBFumoInfo;->m_szServerUrl:Ljava/lang/String;

    .line 1287
    const-string v1, "0.0.0.0"

    iput-object v1, v0, Lcom/policydm/db/XDBFumoInfo;->m_szServerIP:Ljava/lang/String;

    .line 1288
    const-string v1, ""

    iput-object v1, v0, Lcom/policydm/db/XDBFumoInfo;->m_szResultCode:Ljava/lang/String;

    .line 1289
    const/16 v1, 0x50

    iput v1, v0, Lcom/policydm/db/XDBFumoInfo;->m_nServerPort:I

    .line 1290
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/policydm/db/XDBFumoInfo;->m_nDownloadMode:Z

    .line 1291
    const-string v1, "http"

    iput-object v1, v0, Lcom/policydm/db/XDBFumoInfo;->m_szProtocol:Ljava/lang/String;

    .line 1293
    return-object v0
.end method

.method public static xdbInitNVMNoti(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p0, "NVMNotiInfo"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x0

    .line 1344
    move-object v0, p0

    check-cast v0, Lcom/policydm/db/XDBNotiInfo;

    .line 1346
    .local v0, "notiInfo":Lcom/policydm/db/XDBNotiInfo;
    if-nez v0, :cond_0

    .line 1348
    new-instance v0, Lcom/policydm/db/XDBNotiInfo;

    .end local v0    # "notiInfo":Lcom/policydm/db/XDBNotiInfo;
    invoke-direct {v0}, Lcom/policydm/db/XDBNotiInfo;-><init>()V

    .line 1359
    .restart local v0    # "notiInfo":Lcom/policydm/db/XDBNotiInfo;
    :goto_0
    return-object v0

    .line 1352
    :cond_0
    iput v2, v0, Lcom/policydm/db/XDBNotiInfo;->appId:I

    .line 1353
    const/4 v1, 0x1

    iput v1, v0, Lcom/policydm/db/XDBNotiInfo;->uiMode:I

    .line 1354
    const-string v1, ""

    iput-object v1, v0, Lcom/policydm/db/XDBNotiInfo;->m_szSessionId:Ljava/lang/String;

    .line 1355
    const-string v1, ""

    iput-object v1, v0, Lcom/policydm/db/XDBNotiInfo;->m_szServerId:Ljava/lang/String;

    .line 1356
    iput v2, v0, Lcom/policydm/db/XDBNotiInfo;->opMode:I

    .line 1357
    const-wide/16 v1, 0x0

    iput-wide v1, v0, Lcom/policydm/db/XDBNotiInfo;->jobId:J

    goto :goto_0
.end method

.method private static xdbInitNVMPollingInfo(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p0, "SyncDMPollingInfo"    # Ljava/lang/Object;

    .prologue
    .line 1304
    move-object v0, p0

    check-cast v0, Lcom/policydm/db/XDBPollingInfo;

    .line 1305
    .local v0, "pPollingInfo":Lcom/policydm/db/XDBPollingInfo;
    return-object v0
.end method

.method private static xdbInitNVMResyncMode(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p0, "SyncDMResyncMode"    # Ljava/lang/Object;

    .prologue
    .line 1316
    move-object v0, p0

    check-cast v0, Lcom/policydm/db/XDBResyncModeInfo;

    .line 1322
    .local v0, "bResyncMode":Lcom/policydm/db/XDBResyncModeInfo;
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/policydm/db/XDBResyncModeInfo;->nNoceResyncMode:Z

    .line 1323
    return-object v0
.end method

.method private static xdbInitNVMSIMInfo(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p0, "SyncDMSimInfo"    # Ljava/lang/Object;

    .prologue
    .line 1298
    move-object v0, p0

    check-cast v0, Lcom/policydm/db/XDBSimInfo;

    .line 1299
    .local v0, "pSimInfo":Lcom/policydm/db/XDBSimInfo;
    return-object v0
.end method

.method private static xdbInitNVMSPD(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p0, "NVMSpdInfo"    # Ljava/lang/Object;

    .prologue
    .line 1364
    move-object v0, p0

    check-cast v0, Lcom/policydm/db/XDBSpdInfo;

    .line 1365
    .local v0, "spdInfo":Lcom/policydm/db/XDBSpdInfo;
    return-object v0
.end method

.method static xdbInitNVMSyncDMProfile(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 5
    .param p0, "pProfileList"    # Ljava/lang/Object;
    .param p1, "nMagicNumber"    # I

    .prologue
    .line 1234
    move-object v1, p0

    check-cast v1, Lcom/policydm/db/XDBProfileListInfo;

    .line 1235
    .local v1, "ptProflieList":Lcom/policydm/db/XDBProfileListInfo;
    const/4 v0, 0x0

    .line 1236
    .local v0, "nCount":I
    const-string v2, ""

    .line 1237
    .local v2, "szTemp":Ljava/lang/String;
    sget-object v3, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    if-eqz v3, :cond_0

    sget-object v3, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v3, v3, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    if-nez v3, :cond_1

    .line 1239
    :cond_0
    const-string v3, "XDMNvmClass or XDMNvmClass.tProfileList is null. return"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1240
    const/4 p0, 0x0

    .line 1261
    .end local p0    # "pProfileList":Ljava/lang/Object;
    :goto_0
    return-object p0

    .line 1243
    .restart local p0    # "pProfileList":Ljava/lang/Object;
    :cond_1
    const/4 v0, 0x0

    :goto_1
    const/4 v3, 0x3

    if-ge v0, v3, :cond_3

    .line 1245
    sget-object v3, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v3, v3, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    iput v0, v3, Lcom/policydm/db/XDBProfileListInfo;->Profileindex:I

    .line 1246
    const/16 v3, 0x45

    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "szTemp":Ljava/lang/String;
    check-cast v2, Ljava/lang/String;

    .line 1248
    .restart local v2    # "szTemp":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1250
    invoke-static {}, Lcom/policydm/db/XDBProfileAdp;->xdbGetServerID()Ljava/lang/String;

    move-result-object v2

    .line 1251
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1252
    const-string v2, ""

    .line 1254
    :cond_2
    iget-object v3, v1, Lcom/policydm/db/XDBProfileListInfo;->ProfileName:[Ljava/lang/String;

    aput-object v2, v3, v0

    .line 1243
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1256
    :cond_3
    sget-object v3, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v3, v3, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    const/4 v4, 0x0

    iput v4, v3, Lcom/policydm/db/XDBProfileListInfo;->Profileindex:I

    .line 1258
    iput p1, v1, Lcom/policydm/db/XDBProfileListInfo;->MagicNumber:I

    .line 1259
    const-string v3, "DM Profile"

    iput-object v3, v1, Lcom/policydm/db/XDBProfileListInfo;->m_szNetworkConnName:Ljava/lang/String;

    goto :goto_0
.end method

.method public static xdbIsAlreadyAgree()Z
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, -0x1

    .line 2182
    const/4 v2, 0x0

    .line 2183
    .local v2, "status":I
    const/4 v0, 0x0

    .line 2186
    .local v0, "bAgree":Z
    :try_start_0
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "SPD_REGISTERD"

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 2193
    :goto_0
    if-lez v2, :cond_1

    .line 2195
    const/4 v0, 0x1

    .line 2210
    :cond_0
    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bAgree : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 2212
    return v0

    .line 2188
    :catch_0
    move-exception v1

    .line 2190
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 2199
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_1
    invoke-static {}, Lcom/policydm/db/XDB;->xdbGetEULAAgreement()Z

    move-result v0

    .line 2200
    if-eqz v0, :cond_0

    .line 2202
    invoke-static {v7}, Lcom/policydm/db/XDB;->xdbSetSPDRegisterStatus(I)V

    .line 2203
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "security_update_db"

    invoke-static {v3, v4, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-ne v3, v6, :cond_0

    .line 2205
    invoke-static {v7}, Lcom/policydm/db/XDB;->xdbSetAutoUpdateFlag(Z)V

    goto :goto_1
.end method

.method private static xdbLoadCallback(ILjava/lang/Object;I)Ljava/lang/Object;
    .locals 1
    .param p0, "areaCode"    # I
    .param p1, "pNVMUser"    # Ljava/lang/Object;
    .param p2, "magicNumber"    # I

    .prologue
    .line 1370
    const/16 v0, 0x51

    if-ne p0, v0, :cond_1

    .line 1372
    invoke-static {p1, p2}, Lcom/policydm/db/XDB;->xdbInitNVMSyncDMProfile(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object p1

    .line 1413
    :cond_0
    :goto_0
    return-object p1

    .line 1374
    :cond_1
    const/16 v0, 0x52

    if-lt p0, v0, :cond_2

    const/16 v0, 0x56

    if-gt p0, v0, :cond_2

    .line 1376
    invoke-static {p1, p2}, Lcom/policydm/db/XDB;->xdbInitNVMDMInfo(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object p1

    goto :goto_0

    .line 1378
    :cond_2
    const/16 v0, 0x57

    if-ne p0, v0, :cond_3

    .line 1380
    invoke-static {p1}, Lcom/policydm/db/XDB;->xdbInitNVMFUMOInfo(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_0

    .line 1382
    :cond_3
    const/16 v0, 0x58

    if-ne p0, v0, :cond_4

    .line 1384
    invoke-static {p1}, Lcom/policydm/db/XDB;->xdbInitNVMSIMInfo(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_0

    .line 1386
    :cond_4
    const/16 v0, 0x59

    if-ne p0, v0, :cond_5

    .line 1388
    invoke-static {p1}, Lcom/policydm/db/XDB;->xdbInitNVMPollingInfo(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_0

    .line 1390
    :cond_5
    const/16 v0, 0x83

    if-ne p0, v0, :cond_6

    .line 1393
    invoke-static {p1}, Lcom/policydm/db/XDB;->xdbInitNVMAccXNode(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_0

    .line 1395
    :cond_6
    const/16 v0, 0x60

    if-ne p0, v0, :cond_7

    .line 1398
    invoke-static {p1}, Lcom/policydm/db/XDB;->xdbInitNVMResyncMode(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_0

    .line 1400
    :cond_7
    const/16 v0, 0x61

    if-ne p0, v0, :cond_8

    .line 1402
    invoke-static {p1}, Lcom/policydm/db/XDB;->xdbInitNVMAgentTypeInfo(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_0

    .line 1404
    :cond_8
    const/16 v0, 0x62

    if-ne p0, v0, :cond_9

    .line 1406
    invoke-static {p1}, Lcom/policydm/db/XDB;->xdbInitNVMNoti(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_0

    .line 1408
    :cond_9
    const/16 v0, 0x63

    if-ne p0, v0, :cond_0

    .line 1410
    invoke-static {p1}, Lcom/policydm/db/XDB;->xdbInitNVMSPD(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_0
.end method

.method public static xdbNotiDbDelete(ILjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "FileID"    # I
    .param p1, "szColName"    # Ljava/lang/String;
    .param p2, "szColData"    # Ljava/lang/String;

    .prologue
    .line 692
    packed-switch p0, :pswitch_data_0

    .line 699
    const-string v0, "Not Support file id----"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 702
    :goto_0
    return-void

    .line 695
    :pswitch_0
    const/16 v0, 0x62

    invoke-static {v0, p1, p2}, Lcom/policydm/db/XDBSqlAdapter;->xdmDbSqlDelete(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 692
    nop

    :pswitch_data_0
    .packed-switch 0x12c
        :pswitch_0
    .end packed-switch
.end method

.method public static xdbNotiDbExists(I)Z
    .locals 2
    .param p0, "FileID"    # I

    .prologue
    .line 662
    const/4 v0, 0x0

    .line 664
    .local v0, "bExists":Z
    packed-switch p0, :pswitch_data_0

    .line 670
    const-string v1, "Not Support file id----"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 673
    :goto_0
    return v0

    .line 667
    :pswitch_0
    invoke-static {}, Lcom/policydm/db/XDBSqlQuery;->xdmDbExistNotiRow()Z

    move-result v0

    .line 668
    goto :goto_0

    .line 664
    nop

    :pswitch_data_0
    .packed-switch 0x12c
        :pswitch_0
    .end packed-switch
.end method

.method public static xdbNotiDbInsert(ILjava/lang/Object;)V
    .locals 1
    .param p0, "FileID"    # I
    .param p1, "Input"    # Ljava/lang/Object;

    .prologue
    .line 678
    packed-switch p0, :pswitch_data_0

    .line 685
    const-string v0, "Not Support file id----"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 688
    :goto_0
    return-void

    .line 681
    :pswitch_0
    const/16 v0, 0x62

    invoke-static {v0, p1}, Lcom/policydm/db/XDBSqlAdapter;->xdmDbSqlCreate(ILjava/lang/Object;)V

    goto :goto_0

    .line 678
    nop

    :pswitch_data_0
    .packed-switch 0x12c
        :pswitch_0
    .end packed-switch
.end method

.method public static xdbRead(I)Ljava/lang/Object;
    .locals 6
    .param p0, "nType"    # I

    .prologue
    const/16 v5, 0x51

    const/4 v3, 0x0

    .line 706
    if-ltz p0, :cond_1

    const/16 v2, 0x11

    if-ge p0, v2, :cond_1

    .line 708
    sget-object v4, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    invoke-static {v5}, Lcom/policydm/db/XDBSqlAdapter;->xdmDbSqlRead(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/policydm/db/XDBProfileListInfo;

    iput-object v2, v4, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    .line 709
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v2, v2, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    if-nez v2, :cond_0

    move-object v2, v3

    .line 897
    :goto_0
    return-object v2

    .line 712
    :cond_0
    invoke-static {p0}, Lcom/policydm/db/XDB;->xdbReadProfileList(I)Ljava/lang/Object;

    move-result-object v2

    goto :goto_0

    .line 714
    :cond_1
    const/16 v2, 0x47

    if-ne p0, v2, :cond_3

    .line 716
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v4, v2, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    const/16 v2, 0x80

    invoke-static {v2}, Lcom/policydm/db/XDBSqlAdapter;->xdmDbSqlRead(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/policydm/db/XDBInfoConRef;

    iput-object v2, v4, Lcom/policydm/db/XDBProfileInfo;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    .line 717
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v2, v2, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    iget-object v2, v2, Lcom/policydm/db/XDBProfileInfo;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    if-nez v2, :cond_2

    move-object v2, v3

    .line 718
    goto :goto_0

    .line 720
    :cond_2
    invoke-static {p0}, Lcom/policydm/db/XDB;->xdbReadProfile(I)Ljava/lang/Object;

    move-result-object v2

    goto :goto_0

    .line 722
    :cond_3
    const/16 v2, 0x32

    if-lt p0, v2, :cond_6

    const/16 v2, 0x48

    if-ge p0, v2, :cond_6

    .line 724
    sget-object v4, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    invoke-static {v5}, Lcom/policydm/db/XDBSqlAdapter;->xdmDbSqlRead(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/policydm/db/XDBProfileListInfo;

    iput-object v2, v4, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    .line 725
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v2, v2, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    if-nez v2, :cond_4

    move-object v2, v3

    .line 726
    goto :goto_0

    .line 727
    :cond_4
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v2, v2, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    iget v1, v2, Lcom/policydm/db/XDBProfileListInfo;->Profileindex:I

    .line 728
    .local v1, "rowid":I
    const/4 v0, 0x0

    .line 729
    .local v0, "FileID":I
    packed-switch v1, :pswitch_data_0

    .line 743
    :goto_1
    sget-object v4, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    invoke-static {v0}, Lcom/policydm/db/XDBSqlAdapter;->xdmDbSqlRead(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/policydm/db/XDBProfileInfo;

    iput-object v2, v4, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    .line 745
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v2, v2, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    if-nez v2, :cond_5

    move-object v2, v3

    .line 746
    goto :goto_0

    .line 732
    :pswitch_0
    const/16 v0, 0x52

    .line 733
    goto :goto_1

    .line 735
    :pswitch_1
    const/16 v0, 0x53

    .line 736
    goto :goto_1

    .line 738
    :pswitch_2
    const/16 v0, 0x54

    .line 739
    goto :goto_1

    .line 748
    :cond_5
    invoke-static {p0}, Lcom/policydm/db/XDB;->xdbReadProfile(I)Ljava/lang/Object;

    move-result-object v2

    goto :goto_0

    .line 750
    .end local v0    # "FileID":I
    .end local v1    # "rowid":I
    :cond_6
    const/16 v2, 0x64

    if-lt p0, v2, :cond_7

    const/16 v2, 0x69

    if-ge p0, v2, :cond_7

    .line 752
    sget-object v4, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    const/16 v2, 0x83

    invoke-static {v2}, Lcom/policydm/db/XDBSqlAdapter;->xdmDbSqlRead(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/policydm/db/XDBAccXListNode;

    iput-object v2, v4, Lcom/policydm/db/XDBNvm;->NVMAccXNode:Lcom/policydm/db/XDBAccXListNode;

    .line 753
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v2, v2, Lcom/policydm/db/XDBNvm;->NVMAccXNode:Lcom/policydm/db/XDBAccXListNode;

    if-nez v2, :cond_10

    move-object v2, v3

    .line 754
    goto/16 :goto_0

    .line 757
    :cond_7
    const/16 v2, 0x96

    if-lt p0, v2, :cond_8

    const/16 v2, 0x97

    if-ge p0, v2, :cond_8

    .line 759
    sget-object v4, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    const/16 v2, 0x60

    invoke-static {v2}, Lcom/policydm/db/XDBSqlAdapter;->xdmDbSqlRead(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/policydm/db/XDBResyncModeInfo;

    iput-object v2, v4, Lcom/policydm/db/XDBNvm;->NVMResyncMode:Lcom/policydm/db/XDBResyncModeInfo;

    .line 760
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v2, v2, Lcom/policydm/db/XDBNvm;->NVMResyncMode:Lcom/policydm/db/XDBResyncModeInfo;

    if-nez v2, :cond_10

    move-object v2, v3

    .line 761
    goto/16 :goto_0

    .line 763
    :cond_8
    const/16 v2, 0xc8

    if-lt p0, v2, :cond_a

    const/16 v2, 0xe1

    if-ge p0, v2, :cond_a

    .line 765
    sget-object v4, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    const/16 v2, 0x57

    invoke-static {v2}, Lcom/policydm/db/XDBSqlAdapter;->xdmDbSqlRead(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/policydm/db/XDBFumoInfo;

    iput-object v2, v4, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    .line 766
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v2, v2, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    if-nez v2, :cond_9

    move-object v2, v3

    .line 767
    goto/16 :goto_0

    .line 769
    :cond_9
    invoke-static {p0}, Lcom/policydm/db/XDB;->xdbReadFumo(I)Ljava/lang/Object;

    move-result-object v2

    goto/16 :goto_0

    .line 771
    :cond_a
    const/16 v2, 0x78

    if-lt p0, v2, :cond_b

    const/16 v2, 0x79

    if-ge p0, v2, :cond_b

    .line 773
    sget-object v4, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    const/16 v2, 0x58

    invoke-static {v2}, Lcom/policydm/db/XDBSqlAdapter;->xdmDbSqlRead(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/policydm/db/XDBSimInfo;

    iput-object v2, v4, Lcom/policydm/db/XDBNvm;->NVMSimInfo:Lcom/policydm/db/XDBSimInfo;

    .line 774
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v2, v2, Lcom/policydm/db/XDBNvm;->NVMSimInfo:Lcom/policydm/db/XDBSimInfo;

    if-nez v2, :cond_10

    move-object v2, v3

    .line 775
    goto/16 :goto_0

    .line 777
    :cond_b
    const/16 v2, 0x82

    if-lt p0, v2, :cond_c

    const/16 v2, 0x8f

    if-ge p0, v2, :cond_c

    .line 779
    sget-object v4, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    const/16 v2, 0x59

    invoke-static {v2}, Lcom/policydm/db/XDBSqlAdapter;->xdmDbSqlRead(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/policydm/db/XDBPollingInfo;

    iput-object v2, v4, Lcom/policydm/db/XDBNvm;->NVMPollingInfo:Lcom/policydm/db/XDBPollingInfo;

    .line 780
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v2, v2, Lcom/policydm/db/XDBNvm;->NVMPollingInfo:Lcom/policydm/db/XDBPollingInfo;

    if-nez v2, :cond_10

    move-object v2, v3

    .line 781
    goto/16 :goto_0

    .line 783
    :cond_c
    const/16 v2, 0x6e

    if-lt p0, v2, :cond_d

    const/16 v2, 0x70

    if-ge p0, v2, :cond_d

    .line 785
    sget-object v4, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    const/16 v2, 0x61

    invoke-static {v2}, Lcom/policydm/db/XDBSqlAdapter;->xdmDbSqlRead(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/policydm/db/XDBAgentInfo;

    iput-object v2, v4, Lcom/policydm/db/XDBNvm;->NVMDmAgentInfo:Lcom/policydm/db/XDBAgentInfo;

    .line 786
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v2, v2, Lcom/policydm/db/XDBNvm;->NVMDmAgentInfo:Lcom/policydm/db/XDBAgentInfo;

    if-nez v2, :cond_10

    move-object v2, v3

    .line 787
    goto/16 :goto_0

    .line 789
    :cond_d
    const/16 v2, 0x12c

    if-lt p0, v2, :cond_e

    const/16 v2, 0x133

    if-ge p0, v2, :cond_e

    .line 791
    sget-object v4, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    const/16 v2, 0x62

    invoke-static {v2}, Lcom/policydm/db/XDBSqlAdapter;->xdmDbSqlRead(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/policydm/db/XDBNotiInfo;

    iput-object v2, v4, Lcom/policydm/db/XDBNvm;->NVMNotiInfo:Lcom/policydm/db/XDBNotiInfo;

    .line 792
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v2, v2, Lcom/policydm/db/XDBNvm;->NVMNotiInfo:Lcom/policydm/db/XDBNotiInfo;

    if-nez v2, :cond_10

    move-object v2, v3

    .line 793
    goto/16 :goto_0

    .line 795
    :cond_e
    const/16 v2, 0x190

    if-lt p0, v2, :cond_f

    const/16 v2, 0x19a

    if-ge p0, v2, :cond_f

    .line 797
    sget-object v4, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    const/16 v2, 0x63

    invoke-static {v2}, Lcom/policydm/db/XDBSqlAdapter;->xdmDbSqlRead(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/policydm/db/XDBSpdInfo;

    iput-object v2, v4, Lcom/policydm/db/XDBNvm;->NVMSPDInfo:Lcom/policydm/db/XDBSpdInfo;

    .line 798
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v2, v2, Lcom/policydm/db/XDBNvm;->NVMSPDInfo:Lcom/policydm/db/XDBSpdInfo;

    if-nez v2, :cond_10

    move-object v2, v3

    .line 799
    goto/16 :goto_0

    .line 803
    :cond_f
    const-string v2, "----wrong file id----"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move-object v2, v3

    .line 804
    goto/16 :goto_0

    .line 807
    :cond_10
    sparse-switch p0, :sswitch_data_0

    :sswitch_0
    move-object v2, v3

    .line 897
    goto/16 :goto_0

    .line 811
    :sswitch_1
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v2, v2, Lcom/policydm/db/XDBNvm;->NVMAccXNode:Lcom/policydm/db/XDBAccXListNode;

    iget-object v2, v2, Lcom/policydm/db/XDBAccXListNode;->stAccXNodeList:[Lcom/policydm/db/XDBAccXNodeInfo;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    goto/16 :goto_0

    .line 814
    :sswitch_2
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v2, v2, Lcom/policydm/db/XDBNvm;->NVMAccXNode:Lcom/policydm/db/XDBAccXListNode;

    iget-object v2, v2, Lcom/policydm/db/XDBAccXListNode;->stAccXNodeList:[Lcom/policydm/db/XDBAccXNodeInfo;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    goto/16 :goto_0

    .line 817
    :sswitch_3
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v2, v2, Lcom/policydm/db/XDBNvm;->NVMAccXNode:Lcom/policydm/db/XDBAccXListNode;

    iget-object v2, v2, Lcom/policydm/db/XDBAccXListNode;->stAccXNodeList:[Lcom/policydm/db/XDBAccXNodeInfo;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    goto/16 :goto_0

    .line 824
    :sswitch_4
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v2, v2, Lcom/policydm/db/XDBNvm;->NVMResyncMode:Lcom/policydm/db/XDBResyncModeInfo;

    iget-boolean v2, v2, Lcom/policydm/db/XDBResyncModeInfo;->nNoceResyncMode:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto/16 :goto_0

    .line 827
    :sswitch_5
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v2, v2, Lcom/policydm/db/XDBNvm;->NVMSimInfo:Lcom/policydm/db/XDBSimInfo;

    goto/16 :goto_0

    .line 830
    :sswitch_6
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v2, v2, Lcom/policydm/db/XDBNvm;->NVMPollingInfo:Lcom/policydm/db/XDBPollingInfo;

    goto/16 :goto_0

    .line 832
    :sswitch_7
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v2, v2, Lcom/policydm/db/XDBNvm;->NVMPollingInfo:Lcom/policydm/db/XDBPollingInfo;

    iget-object v2, v2, Lcom/policydm/db/XDBPollingInfo;->m_szOrgVersionUrl:Ljava/lang/String;

    goto/16 :goto_0

    .line 834
    :sswitch_8
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v2, v2, Lcom/policydm/db/XDBNvm;->NVMPollingInfo:Lcom/policydm/db/XDBPollingInfo;

    iget-object v2, v2, Lcom/policydm/db/XDBPollingInfo;->m_szVersionUrl:Ljava/lang/String;

    goto/16 :goto_0

    .line 836
    :sswitch_9
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v2, v2, Lcom/policydm/db/XDBNvm;->NVMPollingInfo:Lcom/policydm/db/XDBPollingInfo;

    iget-object v2, v2, Lcom/policydm/db/XDBPollingInfo;->m_szFileName:Ljava/lang/String;

    goto/16 :goto_0

    .line 838
    :sswitch_a
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v2, v2, Lcom/policydm/db/XDBNvm;->NVMPollingInfo:Lcom/policydm/db/XDBPollingInfo;

    iget-object v2, v2, Lcom/policydm/db/XDBPollingInfo;->m_szPeriodUnit:Ljava/lang/String;

    goto/16 :goto_0

    .line 840
    :sswitch_b
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v2, v2, Lcom/policydm/db/XDBNvm;->NVMPollingInfo:Lcom/policydm/db/XDBPollingInfo;

    iget v2, v2, Lcom/policydm/db/XDBPollingInfo;->nPeriod:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_0

    .line 842
    :sswitch_c
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v2, v2, Lcom/policydm/db/XDBNvm;->NVMPollingInfo:Lcom/policydm/db/XDBPollingInfo;

    iget v2, v2, Lcom/policydm/db/XDBPollingInfo;->nTime:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_0

    .line 844
    :sswitch_d
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v2, v2, Lcom/policydm/db/XDBNvm;->NVMPollingInfo:Lcom/policydm/db/XDBPollingInfo;

    iget v2, v2, Lcom/policydm/db/XDBPollingInfo;->nRange:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_0

    .line 846
    :sswitch_e
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v2, v2, Lcom/policydm/db/XDBNvm;->NVMPollingInfo:Lcom/policydm/db/XDBPollingInfo;

    iget v2, v2, Lcom/policydm/db/XDBPollingInfo;->nReportPeriod:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_0

    .line 848
    :sswitch_f
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v2, v2, Lcom/policydm/db/XDBNvm;->NVMPollingInfo:Lcom/policydm/db/XDBPollingInfo;

    iget v2, v2, Lcom/policydm/db/XDBPollingInfo;->nReportTime:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_0

    .line 850
    :sswitch_10
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v2, v2, Lcom/policydm/db/XDBNvm;->NVMPollingInfo:Lcom/policydm/db/XDBPollingInfo;

    iget v2, v2, Lcom/policydm/db/XDBPollingInfo;->nReportRange:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_0

    .line 852
    :sswitch_11
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v2, v2, Lcom/policydm/db/XDBNvm;->NVMPollingInfo:Lcom/policydm/db/XDBPollingInfo;

    iget-wide v2, v2, Lcom/policydm/db/XDBPollingInfo;->m_nextpollingtime:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    goto/16 :goto_0

    .line 854
    :sswitch_12
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v2, v2, Lcom/policydm/db/XDBNvm;->NVMPollingInfo:Lcom/policydm/db/XDBPollingInfo;

    iget-wide v2, v2, Lcom/policydm/db/XDBPollingInfo;->m_nextreporttime:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    goto/16 :goto_0

    .line 857
    :sswitch_13
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v2, v2, Lcom/policydm/db/XDBNvm;->NVMDmAgentInfo:Lcom/policydm/db/XDBAgentInfo;

    goto/16 :goto_0

    .line 859
    :sswitch_14
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v2, v2, Lcom/policydm/db/XDBNvm;->NVMDmAgentInfo:Lcom/policydm/db/XDBAgentInfo;

    iget v2, v2, Lcom/policydm/db/XDBAgentInfo;->m_nAgentType:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_0

    .line 862
    :sswitch_15
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v2, v2, Lcom/policydm/db/XDBNvm;->NVMNotiInfo:Lcom/policydm/db/XDBNotiInfo;

    goto/16 :goto_0

    .line 864
    :sswitch_16
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v2, v2, Lcom/policydm/db/XDBNvm;->NVMNotiInfo:Lcom/policydm/db/XDBNotiInfo;

    iget v2, v2, Lcom/policydm/db/XDBNotiInfo;->appId:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_0

    .line 866
    :sswitch_17
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v2, v2, Lcom/policydm/db/XDBNvm;->NVMNotiInfo:Lcom/policydm/db/XDBNotiInfo;

    iget v2, v2, Lcom/policydm/db/XDBNotiInfo;->uiMode:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_0

    .line 868
    :sswitch_18
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v2, v2, Lcom/policydm/db/XDBNvm;->NVMNotiInfo:Lcom/policydm/db/XDBNotiInfo;

    iget-object v2, v2, Lcom/policydm/db/XDBNotiInfo;->m_szSessionId:Ljava/lang/String;

    goto/16 :goto_0

    .line 870
    :sswitch_19
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v2, v2, Lcom/policydm/db/XDBNvm;->NVMNotiInfo:Lcom/policydm/db/XDBNotiInfo;

    iget-object v2, v2, Lcom/policydm/db/XDBNotiInfo;->m_szServerId:Ljava/lang/String;

    goto/16 :goto_0

    .line 872
    :sswitch_1a
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v2, v2, Lcom/policydm/db/XDBNvm;->NVMNotiInfo:Lcom/policydm/db/XDBNotiInfo;

    iget v2, v2, Lcom/policydm/db/XDBNotiInfo;->opMode:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_0

    .line 874
    :sswitch_1b
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v2, v2, Lcom/policydm/db/XDBNvm;->NVMNotiInfo:Lcom/policydm/db/XDBNotiInfo;

    iget-wide v2, v2, Lcom/policydm/db/XDBNotiInfo;->jobId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    goto/16 :goto_0

    .line 877
    :sswitch_1c
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v2, v2, Lcom/policydm/db/XDBNvm;->NVMSPDInfo:Lcom/policydm/db/XDBSpdInfo;

    iget v2, v2, Lcom/policydm/db/XDBSpdInfo;->m_nDeviceRegi:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_0

    .line 879
    :sswitch_1d
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v2, v2, Lcom/policydm/db/XDBNvm;->NVMSPDInfo:Lcom/policydm/db/XDBSpdInfo;

    iget v2, v2, Lcom/policydm/db/XDBSpdInfo;->m_nDeviceCreate:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_0

    .line 881
    :sswitch_1e
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v2, v2, Lcom/policydm/db/XDBNvm;->NVMSPDInfo:Lcom/policydm/db/XDBSpdInfo;

    iget v2, v2, Lcom/policydm/db/XDBSpdInfo;->m_nDeviceUpdate:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_0

    .line 883
    :sswitch_1f
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v2, v2, Lcom/policydm/db/XDBNvm;->NVMSPDInfo:Lcom/policydm/db/XDBSpdInfo;

    iget-object v2, v2, Lcom/policydm/db/XDBSpdInfo;->m_szPushType:Ljava/lang/String;

    goto/16 :goto_0

    .line 885
    :sswitch_20
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v2, v2, Lcom/policydm/db/XDBNvm;->NVMSPDInfo:Lcom/policydm/db/XDBSpdInfo;

    iget-object v2, v2, Lcom/policydm/db/XDBSpdInfo;->m_szPushRegID:Ljava/lang/String;

    goto/16 :goto_0

    .line 887
    :sswitch_21
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v2, v2, Lcom/policydm/db/XDBNvm;->NVMSPDInfo:Lcom/policydm/db/XDBSpdInfo;

    iget v2, v2, Lcom/policydm/db/XDBSpdInfo;->m_nSPDState:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_0

    .line 889
    :sswitch_22
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v2, v2, Lcom/policydm/db/XDBNvm;->NVMSPDInfo:Lcom/policydm/db/XDBSpdInfo;

    iget-object v2, v2, Lcom/policydm/db/XDBSpdInfo;->m_szSPDServerNonce:Ljava/lang/String;

    goto/16 :goto_0

    .line 891
    :sswitch_23
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v2, v2, Lcom/policydm/db/XDBNvm;->NVMSPDInfo:Lcom/policydm/db/XDBSpdInfo;

    iget-object v2, v2, Lcom/policydm/db/XDBSpdInfo;->m_szPolicyMode:Ljava/lang/String;

    goto/16 :goto_0

    .line 893
    :sswitch_24
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v2, v2, Lcom/policydm/db/XDBNvm;->NVMSPDInfo:Lcom/policydm/db/XDBSpdInfo;

    iget-wide v2, v2, Lcom/policydm/db/XDBSpdInfo;->m_nEulaTime:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    goto/16 :goto_0

    .line 729
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 807
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_1
        0x65 -> :sswitch_2
        0x66 -> :sswitch_3
        0x67 -> :sswitch_0
        0x68 -> :sswitch_0
        0x6e -> :sswitch_13
        0x6f -> :sswitch_14
        0x78 -> :sswitch_5
        0x82 -> :sswitch_6
        0x83 -> :sswitch_7
        0x84 -> :sswitch_8
        0x85 -> :sswitch_9
        0x86 -> :sswitch_a
        0x87 -> :sswitch_b
        0x88 -> :sswitch_c
        0x89 -> :sswitch_d
        0x8a -> :sswitch_e
        0x8b -> :sswitch_f
        0x8c -> :sswitch_10
        0x8d -> :sswitch_11
        0x8e -> :sswitch_12
        0x96 -> :sswitch_4
        0x12c -> :sswitch_15
        0x12d -> :sswitch_16
        0x12e -> :sswitch_17
        0x12f -> :sswitch_18
        0x130 -> :sswitch_19
        0x131 -> :sswitch_1a
        0x132 -> :sswitch_1b
        0x191 -> :sswitch_1c
        0x192 -> :sswitch_1d
        0x193 -> :sswitch_1e
        0x194 -> :sswitch_1f
        0x195 -> :sswitch_20
        0x196 -> :sswitch_21
        0x197 -> :sswitch_22
        0x198 -> :sswitch_23
        0x199 -> :sswitch_24
    .end sparse-switch
.end method

.method public static xdbReadFumo(I)Ljava/lang/Object;
    .locals 1
    .param p0, "nType"    # I

    .prologue
    .line 378
    packed-switch p0, :pswitch_data_0

    .line 431
    const-string v0, "Wrong Type"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 434
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 381
    :pswitch_0
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    goto :goto_0

    .line 383
    :pswitch_1
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    iget-object v0, v0, Lcom/policydm/db/XDBFumoInfo;->m_szProtocol:Ljava/lang/String;

    goto :goto_0

    .line 385
    :pswitch_2
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    iget-object v0, v0, Lcom/policydm/db/XDBFumoInfo;->m_szServerUrl:Ljava/lang/String;

    goto :goto_0

    .line 387
    :pswitch_3
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    iget-object v0, v0, Lcom/policydm/db/XDBFumoInfo;->m_szServerIP:Ljava/lang/String;

    goto :goto_0

    .line 389
    :pswitch_4
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    iget v0, v0, Lcom/policydm/db/XDBFumoInfo;->m_nServerPort:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 391
    :pswitch_5
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    iget-object v0, v0, Lcom/policydm/db/XDBFumoInfo;->m_szObjectDownloadProtocol:Ljava/lang/String;

    goto :goto_0

    .line 393
    :pswitch_6
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    iget-object v0, v0, Lcom/policydm/db/XDBFumoInfo;->m_szObjectDownloadUrl:Ljava/lang/String;

    goto :goto_0

    .line 395
    :pswitch_7
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    iget-object v0, v0, Lcom/policydm/db/XDBFumoInfo;->m_szObjectDownloadIP:Ljava/lang/String;

    goto :goto_0

    .line 397
    :pswitch_8
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    iget v0, v0, Lcom/policydm/db/XDBFumoInfo;->m_nObjectDownloadPort:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 399
    :pswitch_9
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    iget-object v0, v0, Lcom/policydm/db/XDBFumoInfo;->m_szStatusNotifyProtocol:Ljava/lang/String;

    goto :goto_0

    .line 401
    :pswitch_a
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    iget-object v0, v0, Lcom/policydm/db/XDBFumoInfo;->m_szStatusNotifyUrl:Ljava/lang/String;

    goto :goto_0

    .line 403
    :pswitch_b
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    iget-object v0, v0, Lcom/policydm/db/XDBFumoInfo;->m_szStatusNotifyIP:Ljava/lang/String;

    goto :goto_0

    .line 405
    :pswitch_c
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    iget v0, v0, Lcom/policydm/db/XDBFumoInfo;->m_nStatusNotifyPort:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 407
    :pswitch_d
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    iget-object v0, v0, Lcom/policydm/db/XDBFumoInfo;->m_szPkgName:Ljava/lang/String;

    goto :goto_0

    .line 409
    :pswitch_e
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    iget-object v0, v0, Lcom/policydm/db/XDBFumoInfo;->m_szPkgVer:Ljava/lang/String;

    goto :goto_0

    .line 411
    :pswitch_f
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    iget-object v0, v0, Lcom/policydm/db/XDBFumoInfo;->m_szPkgDesc:Ljava/lang/String;

    goto :goto_0

    .line 413
    :pswitch_10
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    iget-object v0, v0, Lcom/policydm/db/XDBFumoInfo;->m_szProcessId:Ljava/lang/String;

    goto/16 :goto_0

    .line 415
    :pswitch_11
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    iget v0, v0, Lcom/policydm/db/XDBFumoInfo;->m_nPkgSize:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_0

    .line 417
    :pswitch_12
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    iget v0, v0, Lcom/policydm/db/XDBFumoInfo;->m_nStatus:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_0

    .line 419
    :pswitch_13
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    iget-boolean v0, v0, Lcom/policydm/db/XDBFumoInfo;->m_nDownloadMode:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_0

    .line 421
    :pswitch_14
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    iget-object v0, v0, Lcom/policydm/db/XDBFumoInfo;->m_szCorrelator:Ljava/lang/String;

    goto/16 :goto_0

    .line 423
    :pswitch_15
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    iget-object v0, v0, Lcom/policydm/db/XDBFumoInfo;->m_szResultCode:Ljava/lang/String;

    goto/16 :goto_0

    .line 425
    :pswitch_16
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    iget-object v0, v0, Lcom/policydm/db/XDBFumoInfo;->m_szDownloadResultCode:Ljava/lang/String;

    goto/16 :goto_0

    .line 427
    :pswitch_17
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    iget v0, v0, Lcom/policydm/db/XDBFumoInfo;->m_nInitiatedType:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_0

    .line 429
    :pswitch_18
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    iget-object v0, v0, Lcom/policydm/db/XDBFumoInfo;->m_szReportUrl:Ljava/lang/String;

    goto/16 :goto_0

    .line 378
    :pswitch_data_0
    .packed-switch 0xc8
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
    .end packed-switch
.end method

.method public static xdbReadListInfo(I)Ljava/lang/Object;
    .locals 4
    .param p0, "nType"    # I

    .prologue
    .line 245
    const/4 v1, 0x0

    .line 247
    .local v1, "outPut":Ljava/lang/Object;
    packed-switch p0, :pswitch_data_0

    .line 268
    .end local v1    # "outPut":Ljava/lang/Object;
    :goto_0
    return-object v1

    .line 250
    .restart local v1    # "outPut":Ljava/lang/Object;
    :pswitch_0
    sget-object v3, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    const/16 v2, 0x51

    invoke-static {v2}, Lcom/policydm/db/XDBSqlAdapter;->xdmDbSqlRead(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/policydm/db/XDBProfileListInfo;

    iput-object v2, v3, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    .line 251
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v1, v2, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    goto :goto_0

    .line 254
    :pswitch_1
    sget-object v2, Lcom/policydm/db/XDB;->NetProfileClass:Lcom/policydm/db/XDBNetworkProfileList;

    const-string v3, "NetWork Info"

    iput-object v3, v2, Lcom/policydm/db/XDBNetworkProfileList;->m_szConRefName:Ljava/lang/String;

    .line 255
    sget-object v3, Lcom/policydm/db/XDB;->NetProfileClass:Lcom/policydm/db/XDBNetworkProfileList;

    const/16 v2, 0x80

    invoke-static {v2}, Lcom/policydm/db/XDBSqlAdapter;->xdmDbSqlRead(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/policydm/db/XDBInfoConRef;

    iput-object v2, v3, Lcom/policydm/db/XDBNetworkProfileList;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    .line 256
    sget-object v1, Lcom/policydm/db/XDB;->NetProfileClass:Lcom/policydm/db/XDBNetworkProfileList;

    goto :goto_0

    .line 259
    :pswitch_2
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    const/4 v2, 0x3

    if-ge v0, v2, :cond_0

    .line 261
    sget-object v3, Lcom/policydm/db/XDB;->ProfileInfoClass:[Lcom/policydm/db/XDBProfileInfo;

    add-int/lit8 v2, v0, 0x52

    invoke-static {v2}, Lcom/policydm/db/XDBSqlAdapter;->xdmDbSqlRead(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/policydm/db/XDBProfileInfo;

    aput-object v2, v3, v0

    .line 259
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 263
    :cond_0
    sget-object v1, Lcom/policydm/db/XDB;->ProfileInfoClass:[Lcom/policydm/db/XDBProfileInfo;

    goto :goto_0

    .line 247
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static xdbReadProfile(I)Ljava/lang/Object;
    .locals 1
    .param p0, "nType"    # I

    .prologue
    .line 319
    packed-switch p0, :pswitch_data_0

    .line 370
    const-string v0, "Wrong Type"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 373
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 322
    :pswitch_0
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    goto :goto_0

    .line 324
    :pswitch_1
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    iget v0, v0, Lcom/policydm/db/XDBProfileInfo;->MagicNumber:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 326
    :pswitch_2
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    iget-object v0, v0, Lcom/policydm/db/XDBProfileInfo;->Protocol:Ljava/lang/String;

    goto :goto_0

    .line 328
    :pswitch_3
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    iget-object v0, v0, Lcom/policydm/db/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    goto :goto_0

    .line 330
    :pswitch_4
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    iget-object v0, v0, Lcom/policydm/db/XDBProfileInfo;->ServerIP:Ljava/lang/String;

    goto :goto_0

    .line 332
    :pswitch_5
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    iget-object v0, v0, Lcom/policydm/db/XDBProfileInfo;->ServerPath:Ljava/lang/String;

    goto :goto_0

    .line 334
    :pswitch_6
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    iget v0, v0, Lcom/policydm/db/XDBProfileInfo;->ServerPort:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 336
    :pswitch_7
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    iget-boolean v0, v0, Lcom/policydm/db/XDBProfileInfo;->bChangedProtocol:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 339
    :pswitch_8
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    iget-object v0, v0, Lcom/policydm/db/XDBProfileInfo;->AppID:Ljava/lang/String;

    goto :goto_0

    .line 342
    :pswitch_9
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    iget-object v0, v0, Lcom/policydm/db/XDBProfileInfo;->AuthLevel:Ljava/lang/String;

    goto :goto_0

    .line 345
    :pswitch_a
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    iget-object v0, v0, Lcom/policydm/db/XDBProfileInfo;->ServerAuthLevel:Ljava/lang/String;

    goto :goto_0

    .line 347
    :pswitch_b
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    iget v0, v0, Lcom/policydm/db/XDBProfileInfo;->AuthType:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 349
    :pswitch_c
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    iget v0, v0, Lcom/policydm/db/XDBProfileInfo;->nServerAuthType:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 351
    :pswitch_d
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    iget-object v0, v0, Lcom/policydm/db/XDBProfileInfo;->UserName:Ljava/lang/String;

    goto :goto_0

    .line 353
    :pswitch_e
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    iget-object v0, v0, Lcom/policydm/db/XDBProfileInfo;->Password:Ljava/lang/String;

    goto :goto_0

    .line 355
    :pswitch_f
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    iget-object v0, v0, Lcom/policydm/db/XDBProfileInfo;->ServerID:Ljava/lang/String;

    goto/16 :goto_0

    .line 357
    :pswitch_10
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    iget-object v0, v0, Lcom/policydm/db/XDBProfileInfo;->ServerPwd:Ljava/lang/String;

    goto/16 :goto_0

    .line 359
    :pswitch_11
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    iget-object v0, v0, Lcom/policydm/db/XDBProfileInfo;->ClientNonce:Ljava/lang/String;

    goto/16 :goto_0

    .line 361
    :pswitch_12
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    iget-object v0, v0, Lcom/policydm/db/XDBProfileInfo;->ServerNonce:Ljava/lang/String;

    goto/16 :goto_0

    .line 363
    :pswitch_13
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    iget-object v0, v0, Lcom/policydm/db/XDBProfileInfo;->ProfileName:Ljava/lang/String;

    goto/16 :goto_0

    .line 366
    :pswitch_14
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    iget-object v0, v0, Lcom/policydm/db/XDBProfileInfo;->PrefConRef:Ljava/lang/String;

    goto/16 :goto_0

    .line 368
    :pswitch_15
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    iget-object v0, v0, Lcom/policydm/db/XDBProfileInfo;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    goto/16 :goto_0

    .line 319
    nop

    :pswitch_data_0
    .packed-switch 0x32
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
    .end packed-switch
.end method

.method public static xdbReadProfileList(I)Ljava/lang/Object;
    .locals 2
    .param p0, "nType"    # I

    .prologue
    .line 273
    packed-switch p0, :pswitch_data_0

    .line 311
    const-string v0, "Wrong Type"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 314
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 276
    :pswitch_0
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    goto :goto_0

    .line 278
    :pswitch_1
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    iget-object v0, v0, Lcom/policydm/db/XDBProfileListInfo;->m_szNetworkConnName:Ljava/lang/String;

    goto :goto_0

    .line 280
    :pswitch_2
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    iget v0, v0, Lcom/policydm/db/XDBProfileListInfo;->nProxyIndex:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 282
    :pswitch_3
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    iget v0, v0, Lcom/policydm/db/XDBProfileListInfo;->Profileindex:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 284
    :pswitch_4
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    iget-object v0, v0, Lcom/policydm/db/XDBProfileListInfo;->ProfileName:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    goto :goto_0

    .line 286
    :pswitch_5
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    iget-object v0, v0, Lcom/policydm/db/XDBProfileListInfo;->ProfileName:[Ljava/lang/String;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    goto :goto_0

    .line 288
    :pswitch_6
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    iget-object v0, v0, Lcom/policydm/db/XDBProfileListInfo;->ProfileName:[Ljava/lang/String;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    goto :goto_0

    .line 290
    :pswitch_7
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    iget-object v0, v0, Lcom/policydm/db/XDBProfileListInfo;->m_szSessionID:Ljava/lang/String;

    goto :goto_0

    .line 292
    :pswitch_8
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    iget v0, v0, Lcom/policydm/db/XDBProfileListInfo;->nNotiEvent:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 294
    :pswitch_9
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    iget v0, v0, Lcom/policydm/db/XDBProfileListInfo;->nNotiOpMode:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 296
    :pswitch_a
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    iget-wide v0, v0, Lcom/policydm/db/XDBProfileListInfo;->nNotiJobId:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 299
    :pswitch_b
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    iget v0, v0, Lcom/policydm/db/XDBProfileListInfo;->nNotiReSyncMode:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 301
    :pswitch_c
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    iget-boolean v0, v0, Lcom/policydm/db/XDBProfileListInfo;->bSkipDevDiscovery:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 303
    :pswitch_d
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    iget v0, v0, Lcom/policydm/db/XDBProfileListInfo;->MagicNumber:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_0

    .line 305
    :pswitch_e
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    iget-object v0, v0, Lcom/policydm/db/XDBProfileListInfo;->NotiResumeState:Lcom/policydm/db/XDBSessionSaveInfo;

    goto/16 :goto_0

    .line 307
    :pswitch_f
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    iget-object v0, v0, Lcom/policydm/db/XDBProfileListInfo;->tUicResultKeep:Lcom/policydm/db/XDBUICResultKeepInfo;

    goto/16 :goto_0

    .line 309
    :pswitch_10
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    iget-object v0, v0, Lcom/policydm/db/XDBProfileListInfo;->tUicResultKeep:Lcom/policydm/db/XDBUICResultKeepInfo;

    iget v0, v0, Lcom/policydm/db/XDBUICResultKeepInfo;->eStatus:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_0

    .line 273
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method

.method public static xdbSPDGetServerURL(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "szPath"    # Ljava/lang/String;

    .prologue
    .line 2017
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2019
    const-string v3, "szPath is null. return"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 2020
    const/4 v3, 0x0

    .line 2045
    :goto_0
    return-object v3

    .line 2023
    :cond_0
    const/16 v3, 0x52

    invoke-static {v3}, Lcom/policydm/db/XDBSqlAdapter;->xdmDbSqlRead(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/policydm/db/XDBProfileInfo;

    .line 2024
    .local v0, "pProfileInfo":Lcom/policydm/db/XDBProfileInfo;
    iget-object v3, v0, Lcom/policydm/db/XDBProfileInfo;->ServerIP:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, v0, Lcom/policydm/db/XDBProfileInfo;->ServerIP:Ljava/lang/String;

    const-string v4, "dm.spd.samsungdm.com"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2025
    :cond_1
    const-string v3, "svc.spd.samsungdm.com"

    iput-object v3, v0, Lcom/policydm/db/XDBProfileInfo;->ServerIP:Ljava/lang/String;

    .line 2027
    :cond_2
    iget-object v1, v0, Lcom/policydm/db/XDBProfileInfo;->Protocol:Ljava/lang/String;

    .line 2028
    .local v1, "szSPDurl":Ljava/lang/String;
    const-string v3, "://"

    invoke-virtual {v1, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2029
    iget-object v3, v0, Lcom/policydm/db/XDBProfileInfo;->ServerIP:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2030
    invoke-virtual {v1, p0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2032
    iget-object v3, v0, Lcom/policydm/db/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_3

    .line 2036
    invoke-static {v1}, Lcom/policydm/tp/XTPHttpUtil;->xtpURLParser(Ljava/lang/String;)Lcom/policydm/db/XDBUrlInfo;

    move-result-object v2

    .line 2038
    .local v2, "xdbURLParser":Lcom/policydm/db/XDBUrlInfo;
    iget-object v3, v2, Lcom/policydm/db/XDBUrlInfo;->pURL:Ljava/lang/String;

    iput-object v3, v0, Lcom/policydm/db/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    .line 2039
    const-string v3, "Changed ServerURL"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 2045
    .end local v2    # "xdbURLParser":Lcom/policydm/db/XDBUrlInfo;
    :goto_1
    iget-object v3, v0, Lcom/policydm/db/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    goto :goto_0

    .line 2043
    :cond_3
    const-string v3, "Not Changed ServerURL"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static xdbSetAutoUpdateFlag(Z)V
    .locals 4
    .param p0, "bAutoUpdate"    # Z

    .prologue
    .line 2102
    const/4 v1, 0x0

    .line 2104
    .local v1, "nStatus":I
    if-eqz p0, :cond_0

    .line 2105
    const/4 v1, 0x1

    .line 2109
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "security_update_db"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2116
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bAutoUpdate : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 2117
    return-void

    .line 2111
    :catch_0
    move-exception v0

    .line 2113
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetBackUpServerUrl()V
    .locals 2

    .prologue
    .line 1824
    const-string v1, ""

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1826
    invoke-static {}, Lcom/policydm/db/XDBProfileAdp;->xdbGetProfileInfo()Lcom/policydm/db/XDBProfileInfo;

    move-result-object v0

    .line 1827
    .local v0, "pNvInfo":Lcom/policydm/db/XDBProfileInfo;
    if-nez v0, :cond_0

    .line 1829
    const-string v1, "pNvInfo is NULL"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1837
    :goto_0
    return-void

    .line 1832
    :cond_0
    iget-object v1, v0, Lcom/policydm/db/XDBProfileInfo;->ServerUrl_Org:Ljava/lang/String;

    invoke-static {v1}, Lcom/policydm/db/XDBProfileAdp;->xdbSetServerUrl(Ljava/lang/String;)V

    .line 1833
    iget-object v1, v0, Lcom/policydm/db/XDBProfileInfo;->ServerIP_Org:Ljava/lang/String;

    invoke-static {v1}, Lcom/policydm/db/XDBProfileAdp;->xdbSetServerAddress(Ljava/lang/String;)V

    .line 1834
    iget v1, v0, Lcom/policydm/db/XDBProfileInfo;->ServerPort_Org:I

    invoke-static {v1}, Lcom/policydm/db/XDBProfileAdp;->xdbSetServerPort(I)V

    .line 1835
    iget-object v1, v0, Lcom/policydm/db/XDBProfileInfo;->Protocol_Org:Ljava/lang/String;

    invoke-static {v1}, Lcom/policydm/db/XDBProfileAdp;->xdbSetServerProtocol(Ljava/lang/String;)V

    .line 1836
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/policydm/db/XDBProfileAdp;->xdbSetChangedProtocol(Z)V

    goto :goto_0
.end method

.method public static xdbSetDMXNodeInfo(ILjava/lang/Object;)V
    .locals 2
    .param p0, "nIndex"    # I
    .param p1, "ptAccXNodeInfo"    # Ljava/lang/Object;

    .prologue
    .line 1654
    :try_start_0
    invoke-static {p0}, Lcom/policydm/db/XDB;->xdb_E2P_XDM_ACCXNODE_INFO(I)I

    move-result v1

    invoke-static {v1, p1}, Lcom/policydm/db/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1660
    :goto_0
    return-void

    .line 1656
    :catch_0
    move-exception v0

    .line 1658
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetProfileName(ILjava/lang/String;)V
    .locals 2
    .param p0, "nIdx"    # I
    .param p1, "szProflieName"    # Ljava/lang/String;

    .prologue
    .line 1628
    :try_start_0
    invoke-static {p0}, Lcom/policydm/db/XDB;->xdb_E2P_XDM_PROFILENAME(I)I

    move-result v1

    invoke-static {v1, p1}, Lcom/policydm/db/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1634
    :goto_0
    return-void

    .line 1630
    :catch_0
    move-exception v0

    .line 1632
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetSPDRegisterStatus(I)V
    .locals 3
    .param p0, "Status"    # I

    .prologue
    .line 2052
    :try_start_0
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "SPD_REGISTERD"

    invoke-static {v1, v2, p0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2058
    :goto_0
    return-void

    .line 2054
    :catch_0
    move-exception v0

    .line 2056
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetSyncMLNVMUser(Lcom/policydm/db/XDBFileNVMParam;)Lcom/policydm/db/XDBFileNVMParam;
    .locals 2
    .param p0, "pfileparam"    # Lcom/policydm/db/XDBFileNVMParam;

    .prologue
    .line 1182
    iget v0, p0, Lcom/policydm/db/XDBFileNVMParam;->AreaCode:I

    const/16 v1, 0x51

    if-ne v0, v1, :cond_0

    .line 1184
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    iput-object v0, p0, Lcom/policydm/db/XDBFileNVMParam;->pNVMUser:Ljava/lang/Object;

    .line 1229
    :goto_0
    return-object p0

    .line 1186
    :cond_0
    iget v0, p0, Lcom/policydm/db/XDBFileNVMParam;->AreaCode:I

    const/16 v1, 0x52

    if-lt v0, v1, :cond_1

    iget v0, p0, Lcom/policydm/db/XDBFileNVMParam;->AreaCode:I

    const/16 v1, 0x56

    if-gt v0, v1, :cond_1

    .line 1188
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    iput-object v0, p0, Lcom/policydm/db/XDBFileNVMParam;->pNVMUser:Ljava/lang/Object;

    goto :goto_0

    .line 1190
    :cond_1
    iget v0, p0, Lcom/policydm/db/XDBFileNVMParam;->AreaCode:I

    const/16 v1, 0x57

    if-ne v0, v1, :cond_2

    .line 1192
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    iput-object v0, p0, Lcom/policydm/db/XDBFileNVMParam;->pNVMUser:Ljava/lang/Object;

    goto :goto_0

    .line 1194
    :cond_2
    iget v0, p0, Lcom/policydm/db/XDBFileNVMParam;->AreaCode:I

    const/16 v1, 0x58

    if-ne v0, v1, :cond_3

    .line 1196
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMSimInfo:Lcom/policydm/db/XDBSimInfo;

    iput-object v0, p0, Lcom/policydm/db/XDBFileNVMParam;->pNVMUser:Ljava/lang/Object;

    goto :goto_0

    .line 1198
    :cond_3
    iget v0, p0, Lcom/policydm/db/XDBFileNVMParam;->AreaCode:I

    const/16 v1, 0x59

    if-ne v0, v1, :cond_4

    .line 1200
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMPollingInfo:Lcom/policydm/db/XDBPollingInfo;

    iput-object v0, p0, Lcom/policydm/db/XDBFileNVMParam;->pNVMUser:Ljava/lang/Object;

    goto :goto_0

    .line 1202
    :cond_4
    iget v0, p0, Lcom/policydm/db/XDBFileNVMParam;->AreaCode:I

    const/16 v1, 0x83

    if-ne v0, v1, :cond_5

    .line 1205
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMAccXNode:Lcom/policydm/db/XDBAccXListNode;

    iput-object v0, p0, Lcom/policydm/db/XDBFileNVMParam;->pNVMUser:Ljava/lang/Object;

    goto :goto_0

    .line 1207
    :cond_5
    iget v0, p0, Lcom/policydm/db/XDBFileNVMParam;->AreaCode:I

    const/16 v1, 0x60

    if-ne v0, v1, :cond_6

    .line 1210
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMResyncMode:Lcom/policydm/db/XDBResyncModeInfo;

    iput-object v0, p0, Lcom/policydm/db/XDBFileNVMParam;->pNVMUser:Ljava/lang/Object;

    goto :goto_0

    .line 1212
    :cond_6
    iget v0, p0, Lcom/policydm/db/XDBFileNVMParam;->AreaCode:I

    const/16 v1, 0x61

    if-ne v0, v1, :cond_7

    .line 1214
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMDmAgentInfo:Lcom/policydm/db/XDBAgentInfo;

    iput-object v0, p0, Lcom/policydm/db/XDBFileNVMParam;->pNVMUser:Ljava/lang/Object;

    goto :goto_0

    .line 1216
    :cond_7
    iget v0, p0, Lcom/policydm/db/XDBFileNVMParam;->AreaCode:I

    const/16 v1, 0x62

    if-ne v0, v1, :cond_8

    .line 1218
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMNotiInfo:Lcom/policydm/db/XDBNotiInfo;

    iput-object v0, p0, Lcom/policydm/db/XDBFileNVMParam;->pNVMUser:Ljava/lang/Object;

    goto :goto_0

    .line 1220
    :cond_8
    iget v0, p0, Lcom/policydm/db/XDBFileNVMParam;->AreaCode:I

    const/16 v1, 0x63

    if-ne v0, v1, :cond_9

    .line 1222
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMSPDInfo:Lcom/policydm/db/XDBSpdInfo;

    iput-object v0, p0, Lcom/policydm/db/XDBFileNVMParam;->pNVMUser:Ljava/lang/Object;

    goto :goto_0

    .line 1226
    :cond_9
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Not Support Area Code: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/policydm/db/XDBFileNVMParam;->AreaCode:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public static xdbSetWaitWifiFlag(Z)V
    .locals 4
    .param p0, "bWaitWifi"    # Z

    .prologue
    .line 2142
    const/4 v1, 0x0

    .line 2144
    .local v1, "nStatus":I
    if-eqz p0, :cond_0

    .line 2145
    const/4 v1, 0x1

    .line 2149
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "use_Wait_Wifi_db"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2156
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bWaitWifi : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 2157
    return-void

    .line 2151
    :catch_0
    move-exception v0

    .line 2153
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetWifiOnlyFlag(Z)V
    .locals 4
    .param p0, "bWifiOnly"    # Z

    .prologue
    .line 2062
    const/4 v1, 0x0

    .line 2064
    .local v1, "nStatus":I
    if-eqz p0, :cond_0

    .line 2065
    const/4 v1, 0x1

    .line 2069
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "use_wifi_only_db"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2076
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bWifiOnly : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 2077
    return-void

    .line 2071
    :catch_0
    move-exception v0

    .line 2073
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static xdbSyncMLFileParmInit([Lcom/policydm/db/XDBFileNVMParam;I)[Lcom/policydm/db/XDBFileNVMParam;
    .locals 5
    .param p0, "xdmSyncMLFileParam2"    # [Lcom/policydm/db/XDBFileNVMParam;
    .param p1, "MaxIndex"    # I

    .prologue
    const/4 v4, 0x0

    .line 141
    const/4 v0, 0x0

    .line 142
    .local v0, "Areacodeindex":[I
    new-array v0, p1, [I

    .line 145
    sget-object v2, Lcom/policydm/db/XDB$XDMNVMParameter;->NVM_DM_PROFILE:Lcom/policydm/db/XDB$XDMNVMParameter;

    invoke-virtual {v2}, Lcom/policydm/db/XDB$XDMNVMParameter;->Value()I

    move-result v2

    const/16 v3, 0x51

    aput v3, v0, v2

    .line 146
    sget-object v2, Lcom/policydm/db/XDB$XDMNVMParameter;->NVM_DM_INFO:Lcom/policydm/db/XDB$XDMNVMParameter;

    invoke-virtual {v2}, Lcom/policydm/db/XDB$XDMNVMParameter;->Value()I

    move-result v2

    const/16 v3, 0x52

    aput v3, v0, v2

    .line 147
    sget-object v2, Lcom/policydm/db/XDB$XDMNVMParameter;->NVM_FUMO_INFO:Lcom/policydm/db/XDB$XDMNVMParameter;

    invoke-virtual {v2}, Lcom/policydm/db/XDB$XDMNVMParameter;->Value()I

    move-result v2

    const/16 v3, 0x57

    aput v3, v0, v2

    .line 148
    sget-object v2, Lcom/policydm/db/XDB$XDMNVMParameter;->NVM_IMSI_INFO:Lcom/policydm/db/XDB$XDMNVMParameter;

    invoke-virtual {v2}, Lcom/policydm/db/XDB$XDMNVMParameter;->Value()I

    move-result v2

    const/16 v3, 0x58

    aput v3, v0, v2

    .line 149
    sget-object v2, Lcom/policydm/db/XDB$XDMNVMParameter;->NVM_POLLING_INFO:Lcom/policydm/db/XDB$XDMNVMParameter;

    invoke-virtual {v2}, Lcom/policydm/db/XDB$XDMNVMParameter;->Value()I

    move-result v2

    const/16 v3, 0x59

    aput v3, v0, v2

    .line 153
    sget-object v2, Lcom/policydm/db/XDB$XDMNVMParameter;->NVM_DM_ACC_X_NODE:Lcom/policydm/db/XDB$XDMNVMParameter;

    invoke-virtual {v2}, Lcom/policydm/db/XDB$XDMNVMParameter;->Value()I

    move-result v2

    const/16 v3, 0x83

    aput v3, v0, v2

    .line 154
    sget-object v2, Lcom/policydm/db/XDB$XDMNVMParameter;->NVM_NOTI_RESYNC_MODE:Lcom/policydm/db/XDB$XDMNVMParameter;

    invoke-virtual {v2}, Lcom/policydm/db/XDB$XDMNVMParameter;->Value()I

    move-result v2

    const/16 v3, 0x60

    aput v3, v0, v2

    .line 157
    sget-object v2, Lcom/policydm/db/XDB$XDMNVMParameter;->NVM_DM_AGENT_INFO:Lcom/policydm/db/XDB$XDMNVMParameter;

    invoke-virtual {v2}, Lcom/policydm/db/XDB$XDMNVMParameter;->Value()I

    move-result v2

    const/16 v3, 0x61

    aput v3, v0, v2

    .line 158
    sget-object v2, Lcom/policydm/db/XDB$XDMNVMParameter;->NVM_NOTI_INFO:Lcom/policydm/db/XDB$XDMNVMParameter;

    invoke-virtual {v2}, Lcom/policydm/db/XDB$XDMNVMParameter;->Value()I

    move-result v2

    const/16 v3, 0x62

    aput v3, v0, v2

    .line 159
    sget-object v2, Lcom/policydm/db/XDB$XDMNVMParameter;->NVM_SPD_INFO:Lcom/policydm/db/XDB$XDMNVMParameter;

    invoke-virtual {v2}, Lcom/policydm/db/XDB$XDMNVMParameter;->Value()I

    move-result v2

    const/16 v3, 0x63

    aput v3, v0, v2

    .line 161
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, p1, :cond_0

    .line 163
    new-instance v2, Lcom/policydm/db/XDBFileNVMParam;

    invoke-direct {v2}, Lcom/policydm/db/XDBFileNVMParam;-><init>()V

    aput-object v2, p0, v1

    .line 164
    aget-object v2, p0, v1

    aget v3, v0, v1

    iput v3, v2, Lcom/policydm/db/XDBFileNVMParam;->AreaCode:I

    .line 165
    aget-object v2, p0, v1

    iput v4, v2, Lcom/policydm/db/XDBFileNVMParam;->pExtFileID:I

    .line 166
    aget-object v2, p0, v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v2, Lcom/policydm/db/XDBFileNVMParam;->pNVMUser:Ljava/lang/Object;

    .line 161
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 169
    :cond_0
    return-object p0
.end method

.method public static xdbWrite(IILjava/lang/Object;)V
    .locals 3
    .param p0, "nType"    # I
    .param p1, "rowId"    # I
    .param p2, "oInput"    # Ljava/lang/Object;

    .prologue
    .line 1145
    if-nez p2, :cond_0

    .line 1147
    const-string v1, "oInput is null"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1178
    .end local p2    # "oInput":Ljava/lang/Object;
    :goto_0
    return-void

    .line 1151
    .restart local p2    # "oInput":Ljava/lang/Object;
    :cond_0
    packed-switch p0, :pswitch_data_0

    .line 1175
    const-string v1, "Not Support file id----"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 1154
    :pswitch_0
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    move-object v1, p2

    check-cast v1, Lcom/policydm/db/XDBProfileInfo;

    iput-object v1, v2, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    .line 1155
    const/4 v0, 0x0

    .line 1157
    .local v0, "SqlID":I
    packed-switch p1, :pswitch_data_1

    .line 1171
    :goto_1
    check-cast p2, Lcom/policydm/db/XDBProfileInfo;

    .end local p2    # "oInput":Ljava/lang/Object;
    invoke-static {v0, p2}, Lcom/policydm/db/XDBSqlAdapter;->xdmDbSqlUpdate(ILjava/lang/Object;)V

    goto :goto_0

    .line 1160
    .restart local p2    # "oInput":Ljava/lang/Object;
    :pswitch_1
    const/16 v0, 0x52

    .line 1161
    goto :goto_1

    .line 1163
    :pswitch_2
    const/16 v0, 0x53

    .line 1164
    goto :goto_1

    .line 1166
    :pswitch_3
    const/16 v0, 0x54

    .line 1167
    goto :goto_1

    .line 1151
    nop

    :pswitch_data_0
    .packed-switch 0x32
        :pswitch_0
    .end packed-switch

    .line 1157
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static xdbWrite(ILjava/lang/Object;)V
    .locals 11
    .param p0, "nType"    # I
    .param p1, "oInput"    # Ljava/lang/Object;

    .prologue
    const/16 v10, 0x19a

    const/16 v9, 0x190

    const/16 v8, 0x63

    const/16 v7, 0x51

    const/4 v6, 0x0

    .line 902
    if-nez p1, :cond_1

    .line 904
    const-string v3, "oInput is null"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1141
    :cond_0
    :goto_0
    return-void

    .line 907
    :cond_1
    if-lt p0, v9, :cond_2

    if-ge p0, v10, :cond_2

    .line 909
    sget-object v3, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v3, v3, Lcom/policydm/db/XDBNvm;->NVMSPDInfo:Lcom/policydm/db/XDBSpdInfo;

    iget-boolean v3, v3, Lcom/policydm/db/XDBSpdInfo;->m_bInitialized:Z

    if-nez v3, :cond_2

    .line 910
    invoke-static {v8}, Lcom/policydm/db/XDBSqlAdapter;->xdmDbSqlRead(I)Ljava/lang/Object;

    .line 912
    :cond_2
    sparse-switch p0, :sswitch_data_0

    .line 1038
    :goto_1
    :sswitch_0
    new-instance v1, Lcom/policydm/db/XDBFileNVMParam;

    invoke-direct {v1}, Lcom/policydm/db/XDBFileNVMParam;-><init>()V

    .line 1040
    .local v1, "pfileparam":Lcom/policydm/db/XDBFileNVMParam;
    if-ltz p0, :cond_3

    const/16 v3, 0x11

    if-ge p0, v3, :cond_3

    .line 1042
    invoke-static {p0, p1}, Lcom/policydm/db/XDB;->xdbWriteProfileList(ILjava/lang/Object;)V

    .line 1043
    sget-object v3, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v3, v3, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    invoke-static {v7, v3}, Lcom/policydm/db/XDBSqlAdapter;->xdmDbSqlUpdate(ILjava/lang/Object;)V

    goto :goto_0

    .line 916
    .end local v1    # "pfileparam":Lcom/policydm/db/XDBFileNVMParam;
    :sswitch_1
    sget-object v3, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v3, v3, Lcom/policydm/db/XDBNvm;->NVMAccXNode:Lcom/policydm/db/XDBAccXListNode;

    iget-object v4, v3, Lcom/policydm/db/XDBAccXListNode;->stAccXNodeList:[Lcom/policydm/db/XDBAccXNodeInfo;

    move-object v3, p1

    check-cast v3, Lcom/policydm/db/XDBAccXNodeInfo;

    aput-object v3, v4, v6

    goto :goto_1

    .line 920
    :sswitch_2
    sget-object v3, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v3, v3, Lcom/policydm/db/XDBNvm;->NVMAccXNode:Lcom/policydm/db/XDBAccXListNode;

    iget-object v4, v3, Lcom/policydm/db/XDBAccXListNode;->stAccXNodeList:[Lcom/policydm/db/XDBAccXNodeInfo;

    const/4 v5, 0x1

    move-object v3, p1

    check-cast v3, Lcom/policydm/db/XDBAccXNodeInfo;

    aput-object v3, v4, v5

    goto :goto_1

    .line 924
    :sswitch_3
    sget-object v3, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v3, v3, Lcom/policydm/db/XDBNvm;->NVMAccXNode:Lcom/policydm/db/XDBAccXListNode;

    iget-object v4, v3, Lcom/policydm/db/XDBAccXListNode;->stAccXNodeList:[Lcom/policydm/db/XDBAccXNodeInfo;

    const/4 v5, 0x2

    move-object v3, p1

    check-cast v3, Lcom/policydm/db/XDBAccXNodeInfo;

    aput-object v3, v4, v5

    goto :goto_1

    .line 931
    :sswitch_4
    sget-object v3, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v3, v3, Lcom/policydm/db/XDBNvm;->NVMResyncMode:Lcom/policydm/db/XDBResyncModeInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    iput-boolean v4, v3, Lcom/policydm/db/XDBResyncModeInfo;->nNoceResyncMode:Z

    goto :goto_1

    .line 935
    :sswitch_5
    sget-object v4, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    move-object v3, p1

    check-cast v3, Lcom/policydm/db/XDBSimInfo;

    iput-object v3, v4, Lcom/policydm/db/XDBNvm;->NVMSimInfo:Lcom/policydm/db/XDBSimInfo;

    goto :goto_1

    .line 939
    :sswitch_6
    sget-object v4, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    move-object v3, p1

    check-cast v3, Lcom/policydm/db/XDBPollingInfo;

    iput-object v3, v4, Lcom/policydm/db/XDBNvm;->NVMPollingInfo:Lcom/policydm/db/XDBPollingInfo;

    goto :goto_1

    .line 942
    :sswitch_7
    sget-object v3, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v3, v3, Lcom/policydm/db/XDBNvm;->NVMPollingInfo:Lcom/policydm/db/XDBPollingInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/policydm/db/XDBPollingInfo;->m_szOrgVersionUrl:Ljava/lang/String;

    goto :goto_1

    .line 945
    :sswitch_8
    sget-object v3, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v3, v3, Lcom/policydm/db/XDBNvm;->NVMPollingInfo:Lcom/policydm/db/XDBPollingInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/policydm/db/XDBPollingInfo;->m_szVersionUrl:Ljava/lang/String;

    goto :goto_1

    .line 948
    :sswitch_9
    sget-object v3, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v3, v3, Lcom/policydm/db/XDBNvm;->NVMPollingInfo:Lcom/policydm/db/XDBPollingInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/policydm/db/XDBPollingInfo;->m_szFileName:Ljava/lang/String;

    goto :goto_1

    .line 951
    :sswitch_a
    sget-object v3, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v3, v3, Lcom/policydm/db/XDBNvm;->NVMPollingInfo:Lcom/policydm/db/XDBPollingInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/policydm/db/XDBPollingInfo;->m_szPeriodUnit:Ljava/lang/String;

    goto/16 :goto_1

    .line 954
    :sswitch_b
    sget-object v3, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v3, v3, Lcom/policydm/db/XDBNvm;->NVMPollingInfo:Lcom/policydm/db/XDBPollingInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iput v4, v3, Lcom/policydm/db/XDBPollingInfo;->nPeriod:I

    goto/16 :goto_1

    .line 957
    :sswitch_c
    sget-object v3, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v3, v3, Lcom/policydm/db/XDBNvm;->NVMPollingInfo:Lcom/policydm/db/XDBPollingInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iput v4, v3, Lcom/policydm/db/XDBPollingInfo;->nTime:I

    goto/16 :goto_1

    .line 960
    :sswitch_d
    sget-object v3, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v3, v3, Lcom/policydm/db/XDBNvm;->NVMPollingInfo:Lcom/policydm/db/XDBPollingInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iput v4, v3, Lcom/policydm/db/XDBPollingInfo;->nRange:I

    goto/16 :goto_1

    .line 963
    :sswitch_e
    sget-object v3, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v3, v3, Lcom/policydm/db/XDBNvm;->NVMPollingInfo:Lcom/policydm/db/XDBPollingInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iput v4, v3, Lcom/policydm/db/XDBPollingInfo;->nReportPeriod:I

    goto/16 :goto_1

    .line 966
    :sswitch_f
    sget-object v3, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v3, v3, Lcom/policydm/db/XDBNvm;->NVMPollingInfo:Lcom/policydm/db/XDBPollingInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iput v4, v3, Lcom/policydm/db/XDBPollingInfo;->nReportTime:I

    goto/16 :goto_1

    .line 969
    :sswitch_10
    sget-object v3, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v3, v3, Lcom/policydm/db/XDBNvm;->NVMPollingInfo:Lcom/policydm/db/XDBPollingInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iput v4, v3, Lcom/policydm/db/XDBPollingInfo;->nReportRange:I

    goto/16 :goto_1

    .line 972
    :sswitch_11
    sget-object v3, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v3, v3, Lcom/policydm/db/XDBNvm;->NVMPollingInfo:Lcom/policydm/db/XDBPollingInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iput-wide v4, v3, Lcom/policydm/db/XDBPollingInfo;->m_nextpollingtime:J

    goto/16 :goto_1

    .line 975
    :sswitch_12
    sget-object v3, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v3, v3, Lcom/policydm/db/XDBNvm;->NVMPollingInfo:Lcom/policydm/db/XDBPollingInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iput-wide v4, v3, Lcom/policydm/db/XDBPollingInfo;->m_nextreporttime:J

    goto/16 :goto_1

    .line 979
    :sswitch_13
    sget-object v4, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    move-object v3, p1

    check-cast v3, Lcom/policydm/db/XDBAgentInfo;

    iput-object v3, v4, Lcom/policydm/db/XDBNvm;->NVMDmAgentInfo:Lcom/policydm/db/XDBAgentInfo;

    goto/16 :goto_1

    .line 982
    :sswitch_14
    sget-object v3, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v4, v3, Lcom/policydm/db/XDBNvm;->NVMDmAgentInfo:Lcom/policydm/db/XDBAgentInfo;

    move-object v3, p1

    check-cast v3, Ljava/lang/Integer;

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iput v3, v4, Lcom/policydm/db/XDBAgentInfo;->m_nAgentType:I

    goto/16 :goto_1

    .line 986
    :sswitch_15
    sget-object v4, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    move-object v3, p1

    check-cast v3, Lcom/policydm/db/XDBNotiInfo;

    check-cast v3, Lcom/policydm/db/XDBNotiInfo;

    iput-object v3, v4, Lcom/policydm/db/XDBNvm;->NVMNotiInfo:Lcom/policydm/db/XDBNotiInfo;

    goto/16 :goto_1

    .line 989
    :sswitch_16
    sget-object v3, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v3, v3, Lcom/policydm/db/XDBNvm;->NVMNotiInfo:Lcom/policydm/db/XDBNotiInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iput v4, v3, Lcom/policydm/db/XDBNotiInfo;->appId:I

    goto/16 :goto_1

    .line 992
    :sswitch_17
    sget-object v3, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v3, v3, Lcom/policydm/db/XDBNvm;->NVMNotiInfo:Lcom/policydm/db/XDBNotiInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iput v4, v3, Lcom/policydm/db/XDBNotiInfo;->uiMode:I

    goto/16 :goto_1

    .line 995
    :sswitch_18
    sget-object v3, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v3, v3, Lcom/policydm/db/XDBNvm;->NVMNotiInfo:Lcom/policydm/db/XDBNotiInfo;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/policydm/db/XDBNotiInfo;->m_szSessionId:Ljava/lang/String;

    goto/16 :goto_1

    .line 998
    :sswitch_19
    sget-object v3, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v3, v3, Lcom/policydm/db/XDBNvm;->NVMNotiInfo:Lcom/policydm/db/XDBNotiInfo;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/policydm/db/XDBNotiInfo;->m_szServerId:Ljava/lang/String;

    goto/16 :goto_1

    .line 1001
    :sswitch_1a
    sget-object v3, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v3, v3, Lcom/policydm/db/XDBNvm;->NVMNotiInfo:Lcom/policydm/db/XDBNotiInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iput v4, v3, Lcom/policydm/db/XDBNotiInfo;->opMode:I

    goto/16 :goto_1

    .line 1004
    :sswitch_1b
    sget-object v3, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v3, v3, Lcom/policydm/db/XDBNvm;->NVMNotiInfo:Lcom/policydm/db/XDBNotiInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iput-wide v4, v3, Lcom/policydm/db/XDBNotiInfo;->jobId:J

    goto/16 :goto_1

    .line 1008
    :sswitch_1c
    sget-object v3, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v3, v3, Lcom/policydm/db/XDBNvm;->NVMSPDInfo:Lcom/policydm/db/XDBSpdInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iput v4, v3, Lcom/policydm/db/XDBSpdInfo;->m_nDeviceRegi:I

    goto/16 :goto_1

    .line 1011
    :sswitch_1d
    sget-object v3, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v3, v3, Lcom/policydm/db/XDBNvm;->NVMSPDInfo:Lcom/policydm/db/XDBSpdInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iput v4, v3, Lcom/policydm/db/XDBSpdInfo;->m_nDeviceCreate:I

    goto/16 :goto_1

    .line 1014
    :sswitch_1e
    sget-object v3, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v3, v3, Lcom/policydm/db/XDBNvm;->NVMSPDInfo:Lcom/policydm/db/XDBSpdInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iput v4, v3, Lcom/policydm/db/XDBSpdInfo;->m_nDeviceUpdate:I

    goto/16 :goto_1

    .line 1017
    :sswitch_1f
    sget-object v3, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v3, v3, Lcom/policydm/db/XDBNvm;->NVMSPDInfo:Lcom/policydm/db/XDBSpdInfo;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/policydm/db/XDBSpdInfo;->m_szPushType:Ljava/lang/String;

    goto/16 :goto_1

    .line 1020
    :sswitch_20
    sget-object v3, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v3, v3, Lcom/policydm/db/XDBNvm;->NVMSPDInfo:Lcom/policydm/db/XDBSpdInfo;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/policydm/db/XDBSpdInfo;->m_szPushRegID:Ljava/lang/String;

    goto/16 :goto_1

    .line 1023
    :sswitch_21
    sget-object v3, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v3, v3, Lcom/policydm/db/XDBNvm;->NVMSPDInfo:Lcom/policydm/db/XDBSpdInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iput v4, v3, Lcom/policydm/db/XDBSpdInfo;->m_nSPDState:I

    goto/16 :goto_1

    .line 1026
    :sswitch_22
    sget-object v3, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v3, v3, Lcom/policydm/db/XDBNvm;->NVMSPDInfo:Lcom/policydm/db/XDBSpdInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/policydm/db/XDBSpdInfo;->m_szSPDServerNonce:Ljava/lang/String;

    goto/16 :goto_1

    .line 1029
    :sswitch_23
    sget-object v3, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v3, v3, Lcom/policydm/db/XDBNvm;->NVMSPDInfo:Lcom/policydm/db/XDBSpdInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/policydm/db/XDBSpdInfo;->m_szPolicyMode:Ljava/lang/String;

    goto/16 :goto_1

    .line 1032
    :sswitch_24
    sget-object v3, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v3, v3, Lcom/policydm/db/XDBNvm;->NVMSPDInfo:Lcom/policydm/db/XDBSpdInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iput-wide v4, v3, Lcom/policydm/db/XDBSpdInfo;->m_nEulaTime:J

    goto/16 :goto_1

    .line 1045
    .restart local v1    # "pfileparam":Lcom/policydm/db/XDBFileNVMParam;
    :cond_3
    const/16 v3, 0x47

    if-ne p0, v3, :cond_5

    .line 1047
    invoke-static {p0, p1}, Lcom/policydm/db/XDB;->xdbWriteProfile(ILjava/lang/Object;)V

    .line 1048
    const-wide/16 v3, 0x1

    invoke-static {v3, v4}, Lcom/policydm/db/XDBSqlQuery;->xdmDbExistsNetworkRow(J)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1050
    const/16 v3, 0x80

    sget-object v4, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v4, v4, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    iget-object v4, v4, Lcom/policydm/db/XDBProfileInfo;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    invoke-static {v3, v4}, Lcom/policydm/db/XDBSqlAdapter;->xdmDbSqlUpdate(ILjava/lang/Object;)V

    .line 1052
    invoke-static {}, Lcom/policydm/ui/XUINetProfileActivity;->getUpdateState()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1054
    sget-object v3, Lcom/policydm/ui/XUIMainActivity;->g_UiNetInfo:Lcom/policydm/adapter/XDMTelephonyAdapter;

    sget-object v4, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v4, v4, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    iget-object v4, v4, Lcom/policydm/db/XDBProfileInfo;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    iget-object v4, v4, Lcom/policydm/db/XDBInfoConRef;->NAP:Lcom/policydm/db/XDBConRefNAP;

    iget-object v4, v4, Lcom/policydm/db/XDBConRefNAP;->Addr:Ljava/lang/String;

    iput-object v4, v3, Lcom/policydm/adapter/XDMTelephonyAdapter;->szAccountName:Ljava/lang/String;

    .line 1055
    sget-object v3, Lcom/policydm/ui/XUIMainActivity;->g_UiNetInfo:Lcom/policydm/adapter/XDMTelephonyAdapter;

    iget-object v3, v3, Lcom/policydm/adapter/XDMTelephonyAdapter;->napAddr:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;

    sget-object v4, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v4, v4, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    iget-object v4, v4, Lcom/policydm/db/XDBProfileInfo;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    iget-object v4, v4, Lcom/policydm/db/XDBInfoConRef;->NAP:Lcom/policydm/db/XDBConRefNAP;

    iget-object v4, v4, Lcom/policydm/db/XDBConRefNAP;->NetworkProfileName:Ljava/lang/String;

    iput-object v4, v3, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;->m_szApn:Ljava/lang/String;

    .line 1056
    sget-object v3, Lcom/policydm/ui/XUIMainActivity;->g_UiNetInfo:Lcom/policydm/adapter/XDMTelephonyAdapter;

    iget-object v3, v3, Lcom/policydm/adapter/XDMTelephonyAdapter;->napAddr:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;

    sget-object v4, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v4, v4, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    iget-object v4, v4, Lcom/policydm/db/XDBProfileInfo;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    iget-object v4, v4, Lcom/policydm/db/XDBInfoConRef;->PX:Lcom/policydm/db/XDBConRefPX;

    iget-object v4, v4, Lcom/policydm/db/XDBConRefPX;->Addr:Ljava/lang/String;

    iput-object v4, v3, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;->m_szPrimaryProxyAddr:Ljava/lang/String;

    .line 1057
    sget-object v3, Lcom/policydm/ui/XUIMainActivity;->g_UiNetInfo:Lcom/policydm/adapter/XDMTelephonyAdapter;

    iget-object v3, v3, Lcom/policydm/adapter/XDMTelephonyAdapter;->napAddr:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;

    sget-object v4, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v4, v4, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    iget-object v4, v4, Lcom/policydm/db/XDBProfileInfo;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    iget-object v4, v4, Lcom/policydm/db/XDBInfoConRef;->PX:Lcom/policydm/db/XDBConRefPX;

    iget v4, v4, Lcom/policydm/db/XDBConRefPX;->nPortNbr:I

    iput v4, v3, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;->nPrimary_proxy_port:I

    .line 1058
    sget-object v3, Lcom/policydm/ui/XUIMainActivity;->g_UiNetInfo:Lcom/policydm/adapter/XDMTelephonyAdapter;

    iget-object v3, v3, Lcom/policydm/adapter/XDMTelephonyAdapter;->authInfo:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMAuthInfo;

    sget-object v4, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v4, v4, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    iget-object v4, v4, Lcom/policydm/db/XDBProfileInfo;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    iget-object v4, v4, Lcom/policydm/db/XDBInfoConRef;->NAP:Lcom/policydm/db/XDBConRefNAP;

    iget-object v4, v4, Lcom/policydm/db/XDBConRefNAP;->Auth:Lcom/policydm/db/XDBConRefAuth;

    iget-object v4, v4, Lcom/policydm/db/XDBConRefAuth;->PAP_ID:Ljava/lang/String;

    iput-object v4, v3, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMAuthInfo;->szId:Ljava/lang/String;

    .line 1059
    sget-object v3, Lcom/policydm/ui/XUIMainActivity;->g_UiNetInfo:Lcom/policydm/adapter/XDMTelephonyAdapter;

    iget-object v3, v3, Lcom/policydm/adapter/XDMTelephonyAdapter;->authInfo:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMAuthInfo;

    sget-object v4, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v4, v4, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    iget-object v4, v4, Lcom/policydm/db/XDBProfileInfo;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    iget-object v4, v4, Lcom/policydm/db/XDBInfoConRef;->NAP:Lcom/policydm/db/XDBConRefNAP;

    iget-object v4, v4, Lcom/policydm/db/XDBConRefNAP;->Auth:Lcom/policydm/db/XDBConRefAuth;

    iget-object v4, v4, Lcom/policydm/db/XDBConRefAuth;->PAP_Secret:Ljava/lang/String;

    iput-object v4, v3, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMAuthInfo;->szPasswd:Ljava/lang/String;

    .line 1060
    sget-object v3, Lcom/policydm/ui/XUIMainActivity;->g_UiNetInfo:Lcom/policydm/adapter/XDMTelephonyAdapter;

    invoke-static {v3}, Lcom/policydm/tp/XTPAdapter;->xtpAdpNetSaveProfile(Lcom/policydm/adapter/XDMTelephonyAdapter;)I

    .line 1061
    invoke-static {v6}, Lcom/policydm/ui/XUINetProfileActivity;->setUpdateState(I)V

    goto/16 :goto_0

    .line 1066
    :cond_4
    sget-object v3, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v3, v3, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    iget-object v3, v3, Lcom/policydm/db/XDBProfileInfo;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    invoke-static {v3}, Lcom/policydm/db/XDBSqlQuery;->xdmDbInsertNetworkRow(Lcom/policydm/db/XDBInfoConRef;)V

    goto/16 :goto_0

    .line 1069
    :cond_5
    const/16 v3, 0x32

    if-lt p0, v3, :cond_8

    const/16 v3, 0x48

    if-ge p0, v3, :cond_8

    .line 1071
    const/4 v0, 0x0

    .line 1072
    .local v0, "SqlID":I
    const/4 v2, 0x0

    .line 1074
    .local v2, "row":I
    invoke-static {}, Lcom/policydm/ui/XUIProfileActivity;->xuiGetRowState()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1076
    invoke-static {}, Lcom/policydm/ui/XUIProfileActivity;->xuiGetRow()I

    move-result v2

    .line 1090
    :goto_2
    packed-switch v2, :pswitch_data_0

    .line 1104
    :goto_3
    invoke-static {p0, p1}, Lcom/policydm/db/XDB;->xdbWriteProfile(ILjava/lang/Object;)V

    .line 1105
    sget-object v3, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v3, v3, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    invoke-static {v0, v3}, Lcom/policydm/db/XDBSqlAdapter;->xdmDbSqlUpdate(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 1080
    :cond_6
    sget-object v4, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    invoke-static {v7}, Lcom/policydm/db/XDBSqlAdapter;->xdmDbSqlRead(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/policydm/db/XDBProfileListInfo;

    iput-object v3, v4, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    .line 1081
    sget-object v3, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v3, v3, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    if-eqz v3, :cond_7

    .line 1083
    sget-object v3, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v3, v3, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    iget v2, v3, Lcom/policydm/db/XDBProfileListInfo;->Profileindex:I

    goto :goto_2

    .line 1087
    :cond_7
    const-string v3, "XDMNvmClass.tProfileList is null"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_2

    .line 1093
    :pswitch_0
    const/16 v0, 0x52

    .line 1094
    goto :goto_3

    .line 1096
    :pswitch_1
    const/16 v0, 0x53

    .line 1097
    goto :goto_3

    .line 1099
    :pswitch_2
    const/16 v0, 0x54

    .line 1100
    goto :goto_3

    .line 1107
    .end local v0    # "SqlID":I
    .end local v2    # "row":I
    :cond_8
    const/16 v3, 0x64

    if-lt p0, v3, :cond_9

    const/16 v3, 0x69

    if-ge p0, v3, :cond_9

    .line 1109
    const/16 v3, 0x83

    iput v3, v1, Lcom/policydm/db/XDBFileNVMParam;->AreaCode:I

    .line 1110
    iget v3, v1, Lcom/policydm/db/XDBFileNVMParam;->AreaCode:I

    sget-object v4, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v4, v4, Lcom/policydm/db/XDBNvm;->NVMAccXNode:Lcom/policydm/db/XDBAccXListNode;

    invoke-static {v3, v4}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpWriteFile(ILjava/lang/Object;)Z

    goto/16 :goto_0

    .line 1112
    :cond_9
    const/16 v3, 0x96

    if-lt p0, v3, :cond_a

    const/16 v3, 0x97

    if-ge p0, v3, :cond_a

    .line 1114
    const/16 v3, 0x60

    sget-object v4, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v4, v4, Lcom/policydm/db/XDBNvm;->NVMResyncMode:Lcom/policydm/db/XDBResyncModeInfo;

    invoke-static {v3, v4}, Lcom/policydm/db/XDBSqlAdapter;->xdmDbSqlUpdate(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 1116
    :cond_a
    const/16 v3, 0xc8

    if-lt p0, v3, :cond_b

    const/16 v3, 0xe1

    if-ge p0, v3, :cond_b

    .line 1118
    invoke-static {p0, p1}, Lcom/policydm/db/XDB;->xdbWriteFumo(ILjava/lang/Object;)V

    .line 1119
    const/16 v3, 0x57

    sget-object v4, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v4, v4, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    invoke-static {v3, v4}, Lcom/policydm/db/XDBSqlAdapter;->xdmDbSqlUpdate(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 1121
    :cond_b
    const/16 v3, 0x78

    if-lt p0, v3, :cond_c

    const/16 v3, 0x79

    if-ge p0, v3, :cond_c

    .line 1123
    const/16 v3, 0x58

    sget-object v4, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v4, v4, Lcom/policydm/db/XDBNvm;->NVMSimInfo:Lcom/policydm/db/XDBSimInfo;

    invoke-static {v3, v4}, Lcom/policydm/db/XDBSqlAdapter;->xdmDbSqlUpdate(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 1125
    :cond_c
    const/16 v3, 0x82

    if-lt p0, v3, :cond_d

    const/16 v3, 0x8f

    if-ge p0, v3, :cond_d

    .line 1127
    const/16 v3, 0x59

    sget-object v4, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v4, v4, Lcom/policydm/db/XDBNvm;->NVMPollingInfo:Lcom/policydm/db/XDBPollingInfo;

    invoke-static {v3, v4}, Lcom/policydm/db/XDBSqlAdapter;->xdmDbSqlUpdate(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 1129
    :cond_d
    const/16 v3, 0x6e

    if-lt p0, v3, :cond_e

    const/16 v3, 0x70

    if-ge p0, v3, :cond_e

    .line 1131
    const/16 v3, 0x61

    sget-object v4, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v4, v4, Lcom/policydm/db/XDBNvm;->NVMDmAgentInfo:Lcom/policydm/db/XDBAgentInfo;

    invoke-static {v3, v4}, Lcom/policydm/db/XDBSqlAdapter;->xdmDbSqlUpdate(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 1133
    :cond_e
    const/16 v3, 0x12c

    if-lt p0, v3, :cond_f

    const/16 v3, 0x133

    if-ge p0, v3, :cond_f

    .line 1135
    const/16 v3, 0x62

    sget-object v4, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v4, v4, Lcom/policydm/db/XDBNvm;->NVMNotiInfo:Lcom/policydm/db/XDBNotiInfo;

    invoke-static {v3, v4}, Lcom/policydm/db/XDBSqlAdapter;->xdmDbSqlUpdate(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 1137
    :cond_f
    if-lt p0, v9, :cond_0

    if-ge p0, v10, :cond_0

    .line 1139
    sget-object v3, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v3, v3, Lcom/policydm/db/XDBNvm;->NVMSPDInfo:Lcom/policydm/db/XDBSpdInfo;

    invoke-static {v8, v3}, Lcom/policydm/db/XDBSqlAdapter;->xdmDbSqlUpdate(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 912
    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_1
        0x65 -> :sswitch_2
        0x66 -> :sswitch_3
        0x67 -> :sswitch_0
        0x68 -> :sswitch_0
        0x6e -> :sswitch_13
        0x6f -> :sswitch_14
        0x78 -> :sswitch_5
        0x82 -> :sswitch_6
        0x83 -> :sswitch_7
        0x84 -> :sswitch_8
        0x85 -> :sswitch_9
        0x86 -> :sswitch_a
        0x87 -> :sswitch_b
        0x88 -> :sswitch_c
        0x89 -> :sswitch_d
        0x8a -> :sswitch_e
        0x8b -> :sswitch_f
        0x8c -> :sswitch_10
        0x8d -> :sswitch_11
        0x8e -> :sswitch_12
        0x96 -> :sswitch_4
        0x12c -> :sswitch_15
        0x12d -> :sswitch_16
        0x12e -> :sswitch_17
        0x12f -> :sswitch_18
        0x130 -> :sswitch_19
        0x131 -> :sswitch_1a
        0x132 -> :sswitch_1b
        0x191 -> :sswitch_1c
        0x192 -> :sswitch_1d
        0x193 -> :sswitch_1e
        0x194 -> :sswitch_1f
        0x195 -> :sswitch_20
        0x196 -> :sswitch_21
        0x197 -> :sswitch_22
        0x198 -> :sswitch_23
        0x199 -> :sswitch_24
    .end sparse-switch

    .line 1090
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static xdbWriteFumo(ILjava/lang/Object;)V
    .locals 2
    .param p0, "nType"    # I
    .param p1, "oInput"    # Ljava/lang/Object;

    .prologue
    .line 577
    packed-switch p0, :pswitch_data_0

    .line 655
    const-string v0, "Wrong Type"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 658
    .end local p1    # "oInput":Ljava/lang/Object;
    :goto_0
    return-void

    .line 580
    .restart local p1    # "oInput":Ljava/lang/Object;
    :pswitch_0
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    check-cast p1, Lcom/policydm/db/XDBFumoInfo;

    .end local p1    # "oInput":Ljava/lang/Object;
    iput-object p1, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    goto :goto_0

    .line 583
    .restart local p1    # "oInput":Ljava/lang/Object;
    :pswitch_1
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/policydm/db/XDBFumoInfo;->m_szProtocol:Ljava/lang/String;

    goto :goto_0

    .line 586
    :pswitch_2
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/policydm/db/XDBFumoInfo;->m_szServerUrl:Ljava/lang/String;

    goto :goto_0

    .line 589
    :pswitch_3
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/policydm/db/XDBFumoInfo;->m_szServerIP:Ljava/lang/String;

    goto :goto_0

    .line 592
    :pswitch_4
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/policydm/db/XDBFumoInfo;->m_nServerPort:I

    goto :goto_0

    .line 595
    :pswitch_5
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/policydm/db/XDBFumoInfo;->m_szObjectDownloadProtocol:Ljava/lang/String;

    goto :goto_0

    .line 598
    :pswitch_6
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/policydm/db/XDBFumoInfo;->m_szObjectDownloadUrl:Ljava/lang/String;

    goto :goto_0

    .line 601
    :pswitch_7
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/policydm/db/XDBFumoInfo;->m_szObjectDownloadIP:Ljava/lang/String;

    goto :goto_0

    .line 604
    :pswitch_8
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/policydm/db/XDBFumoInfo;->m_nObjectDownloadPort:I

    goto :goto_0

    .line 607
    :pswitch_9
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/policydm/db/XDBFumoInfo;->m_szStatusNotifyProtocol:Ljava/lang/String;

    goto :goto_0

    .line 610
    :pswitch_a
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/policydm/db/XDBFumoInfo;->m_szStatusNotifyUrl:Ljava/lang/String;

    goto/16 :goto_0

    .line 613
    :pswitch_b
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/policydm/db/XDBFumoInfo;->m_szStatusNotifyIP:Ljava/lang/String;

    goto/16 :goto_0

    .line 616
    :pswitch_c
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/policydm/db/XDBFumoInfo;->m_nStatusNotifyPort:I

    goto/16 :goto_0

    .line 619
    :pswitch_d
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/policydm/db/XDBFumoInfo;->m_szPkgName:Ljava/lang/String;

    goto/16 :goto_0

    .line 622
    :pswitch_e
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/policydm/db/XDBFumoInfo;->m_szPkgVer:Ljava/lang/String;

    goto/16 :goto_0

    .line 625
    :pswitch_f
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/policydm/db/XDBFumoInfo;->m_szPkgDesc:Ljava/lang/String;

    goto/16 :goto_0

    .line 628
    :pswitch_10
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/policydm/db/XDBFumoInfo;->m_szProcessId:Ljava/lang/String;

    goto/16 :goto_0

    .line 631
    :pswitch_11
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/policydm/db/XDBFumoInfo;->m_nPkgSize:I

    goto/16 :goto_0

    .line 634
    :pswitch_12
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/policydm/db/XDBFumoInfo;->m_nStatus:I

    goto/16 :goto_0

    .line 637
    :pswitch_13
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, v0, Lcom/policydm/db/XDBFumoInfo;->m_nDownloadMode:Z

    goto/16 :goto_0

    .line 640
    :pswitch_14
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/policydm/db/XDBFumoInfo;->m_szCorrelator:Ljava/lang/String;

    goto/16 :goto_0

    .line 643
    :pswitch_15
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/policydm/db/XDBFumoInfo;->m_szResultCode:Ljava/lang/String;

    goto/16 :goto_0

    .line 646
    :pswitch_16
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/policydm/db/XDBFumoInfo;->m_szDownloadResultCode:Ljava/lang/String;

    goto/16 :goto_0

    .line 649
    :pswitch_17
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/policydm/db/XDBFumoInfo;->m_nInitiatedType:I

    goto/16 :goto_0

    .line 652
    :pswitch_18
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/policydm/db/XDBFumoInfo;->m_szReportUrl:Ljava/lang/String;

    goto/16 :goto_0

    .line 577
    nop

    :pswitch_data_0
    .packed-switch 0xc8
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
    .end packed-switch
.end method

.method public static xdbWriteProfile(ILjava/lang/Object;)V
    .locals 2
    .param p0, "nType"    # I
    .param p1, "oInput"    # Ljava/lang/Object;

    .prologue
    .line 501
    packed-switch p0, :pswitch_data_0

    .line 570
    const-string v0, "Wrong Type"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 573
    .end local p1    # "oInput":Ljava/lang/Object;
    :goto_0
    return-void

    .line 504
    .restart local p1    # "oInput":Ljava/lang/Object;
    :pswitch_0
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    check-cast p1, Lcom/policydm/db/XDBProfileInfo;

    .end local p1    # "oInput":Ljava/lang/Object;
    iput-object p1, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    goto :goto_0

    .line 507
    .restart local p1    # "oInput":Ljava/lang/Object;
    :pswitch_1
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/policydm/db/XDBProfileInfo;->MagicNumber:I

    goto :goto_0

    .line 510
    :pswitch_2
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/policydm/db/XDBProfileInfo;->Protocol:Ljava/lang/String;

    goto :goto_0

    .line 513
    :pswitch_3
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/policydm/db/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    goto :goto_0

    .line 516
    :pswitch_4
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/policydm/db/XDBProfileInfo;->ServerIP:Ljava/lang/String;

    goto :goto_0

    .line 519
    :pswitch_5
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/policydm/db/XDBProfileInfo;->ServerPath:Ljava/lang/String;

    goto :goto_0

    .line 522
    :pswitch_6
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/policydm/db/XDBProfileInfo;->ServerPort:I

    goto :goto_0

    .line 525
    :pswitch_7
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, v0, Lcom/policydm/db/XDBProfileInfo;->bChangedProtocol:Z

    goto :goto_0

    .line 528
    :pswitch_8
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/policydm/db/XDBProfileInfo;->AppID:Ljava/lang/String;

    goto :goto_0

    .line 531
    :pswitch_9
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/policydm/db/XDBProfileInfo;->AuthLevel:Ljava/lang/String;

    goto/16 :goto_0

    .line 534
    :pswitch_a
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/policydm/db/XDBProfileInfo;->ServerAuthLevel:Ljava/lang/String;

    goto/16 :goto_0

    .line 537
    :pswitch_b
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/policydm/db/XDBProfileInfo;->AuthType:I

    goto/16 :goto_0

    .line 540
    :pswitch_c
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/policydm/db/XDBProfileInfo;->nServerAuthType:I

    goto/16 :goto_0

    .line 543
    :pswitch_d
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/policydm/db/XDBProfileInfo;->UserName:Ljava/lang/String;

    goto/16 :goto_0

    .line 546
    :pswitch_e
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/policydm/db/XDBProfileInfo;->Password:Ljava/lang/String;

    goto/16 :goto_0

    .line 549
    :pswitch_f
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/policydm/db/XDBProfileInfo;->ServerID:Ljava/lang/String;

    goto/16 :goto_0

    .line 552
    :pswitch_10
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/policydm/db/XDBProfileInfo;->ServerPwd:Ljava/lang/String;

    goto/16 :goto_0

    .line 555
    :pswitch_11
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/policydm/db/XDBProfileInfo;->ClientNonce:Ljava/lang/String;

    goto/16 :goto_0

    .line 558
    :pswitch_12
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/policydm/db/XDBProfileInfo;->ServerNonce:Ljava/lang/String;

    goto/16 :goto_0

    .line 561
    :pswitch_13
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/policydm/db/XDBProfileInfo;->ProfileName:Ljava/lang/String;

    goto/16 :goto_0

    .line 564
    :pswitch_14
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/policydm/db/XDBProfileInfo;->PrefConRef:Ljava/lang/String;

    goto/16 :goto_0

    .line 567
    :pswitch_15
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    check-cast p1, Lcom/policydm/db/XDBInfoConRef;

    .end local p1    # "oInput":Ljava/lang/Object;
    iput-object p1, v0, Lcom/policydm/db/XDBProfileInfo;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    goto/16 :goto_0

    .line 501
    :pswitch_data_0
    .packed-switch 0x32
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
    .end packed-switch
.end method

.method public static xdbWriteProfileList(ILjava/lang/Object;)V
    .locals 3
    .param p0, "nType"    # I
    .param p1, "oInput"    # Ljava/lang/Object;

    .prologue
    .line 439
    packed-switch p0, :pswitch_data_0

    .line 494
    const-string v0, "Wrong Type"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 497
    .end local p1    # "oInput":Ljava/lang/Object;
    :goto_0
    return-void

    .line 442
    .restart local p1    # "oInput":Ljava/lang/Object;
    :pswitch_0
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    check-cast p1, Lcom/policydm/db/XDBProfileListInfo;

    .end local p1    # "oInput":Ljava/lang/Object;
    iput-object p1, v0, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    goto :goto_0

    .line 445
    .restart local p1    # "oInput":Ljava/lang/Object;
    :pswitch_1
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/policydm/db/XDBProfileListInfo;->m_szNetworkConnName:Ljava/lang/String;

    goto :goto_0

    .line 448
    :pswitch_2
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/policydm/db/XDBProfileListInfo;->nProxyIndex:I

    goto :goto_0

    .line 451
    :pswitch_3
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/policydm/db/XDBProfileListInfo;->Profileindex:I

    goto :goto_0

    .line 454
    :pswitch_4
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    iget-object v0, v0, Lcom/policydm/db/XDBProfileListInfo;->ProfileName:[Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    goto :goto_0

    .line 457
    :pswitch_5
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    iget-object v0, v0, Lcom/policydm/db/XDBProfileListInfo;->ProfileName:[Ljava/lang/String;

    const/4 v1, 0x1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    goto :goto_0

    .line 460
    :pswitch_6
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    iget-object v0, v0, Lcom/policydm/db/XDBProfileListInfo;->ProfileName:[Ljava/lang/String;

    const/4 v1, 0x2

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    goto :goto_0

    .line 463
    :pswitch_7
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/policydm/db/XDBProfileListInfo;->m_szSessionID:Ljava/lang/String;

    goto :goto_0

    .line 466
    :pswitch_8
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/policydm/db/XDBProfileListInfo;->nNotiEvent:I

    goto :goto_0

    .line 469
    :pswitch_9
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/policydm/db/XDBProfileListInfo;->nNotiOpMode:I

    goto/16 :goto_0

    .line 472
    :pswitch_a
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/policydm/db/XDBProfileListInfo;->nNotiJobId:J

    goto/16 :goto_0

    .line 476
    :pswitch_b
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/policydm/db/XDBProfileListInfo;->nNotiReSyncMode:I

    goto/16 :goto_0

    .line 479
    :pswitch_c
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, v0, Lcom/policydm/db/XDBProfileListInfo;->bSkipDevDiscovery:Z

    goto/16 :goto_0

    .line 482
    :pswitch_d
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/policydm/db/XDBProfileListInfo;->MagicNumber:I

    goto/16 :goto_0

    .line 485
    :pswitch_e
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    check-cast p1, Lcom/policydm/db/XDBSessionSaveInfo;

    .end local p1    # "oInput":Ljava/lang/Object;
    iput-object p1, v0, Lcom/policydm/db/XDBProfileListInfo;->NotiResumeState:Lcom/policydm/db/XDBSessionSaveInfo;

    goto/16 :goto_0

    .line 488
    .restart local p1    # "oInput":Ljava/lang/Object;
    :pswitch_f
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    check-cast p1, Lcom/policydm/db/XDBUICResultKeepInfo;

    .end local p1    # "oInput":Ljava/lang/Object;
    iput-object p1, v0, Lcom/policydm/db/XDBProfileListInfo;->tUicResultKeep:Lcom/policydm/db/XDBUICResultKeepInfo;

    goto/16 :goto_0

    .line 491
    .restart local p1    # "oInput":Ljava/lang/Object;
    :pswitch_10
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    iget-object v0, v0, Lcom/policydm/db/XDBProfileListInfo;->tUicResultKeep:Lcom/policydm/db/XDBUICResultKeepInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/policydm/db/XDBUICResultKeepInfo;->eStatus:I

    goto/16 :goto_0

    .line 439
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method

.method public static xdb_E2P_XDM_ACCXNODE_INFO(I)I
    .locals 1
    .param p0, "i"    # I

    .prologue
    .line 228
    const/4 v0, 0x0

    .line 229
    .local v0, "ret":I
    packed-switch p0, :pswitch_data_0

    .line 240
    .end local v0    # "ret":I
    :goto_0
    return v0

    .line 232
    .restart local v0    # "ret":I
    :pswitch_0
    const/16 v0, 0x64

    goto :goto_0

    .line 234
    :pswitch_1
    const/16 v0, 0x65

    goto :goto_0

    .line 236
    :pswitch_2
    const/16 v0, 0x66

    goto :goto_0

    .line 229
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static xdb_E2P_XDM_PROFILENAME(I)I
    .locals 1
    .param p0, "i"    # I

    .prologue
    .line 210
    const/4 v0, 0x0

    .line 212
    .local v0, "ret":I
    packed-switch p0, :pswitch_data_0

    .line 223
    .end local v0    # "ret":I
    :goto_0
    return v0

    .line 215
    .restart local v0    # "ret":I
    :pswitch_0
    const/4 v0, 0x4

    goto :goto_0

    .line 217
    :pswitch_1
    const/4 v0, 0x5

    goto :goto_0

    .line 219
    :pswitch_2
    const/4 v0, 0x6

    goto :goto_0

    .line 212
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public xdbGetXDMFileParamFileID(I)Lcom/policydm/db/XDBFileParam;
    .locals 3
    .param p1, "FileID"    # I

    .prologue
    .line 1584
    sget-object v2, Lcom/policydm/db/XDB$XDMFileParameter;->FileMax:Lcom/policydm/db/XDB$XDMFileParameter;

    invoke-virtual {v2}, Lcom/policydm/db/XDB$XDMFileParameter;->Index()I

    move-result v0

    .line 1585
    .local v0, "IndexLen":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 1587
    sget-object v2, Lcom/policydm/db/XDB;->XDMFileParam:[Lcom/policydm/db/XDBFileParam;

    aget-object v2, v2, v1

    iget v2, v2, Lcom/policydm/db/XDBFileParam;->FileID:I

    if-ne v2, p1, :cond_0

    .line 1589
    sget-object v2, Lcom/policydm/db/XDB;->XDMFileParam:[Lcom/policydm/db/XDBFileParam;

    aget-object v2, v2, v1

    .line 1593
    :goto_1
    return-object v2

    .line 1585
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1593
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method
