.class public Lcom/policydm/db/XDBAESCrypt;
.super Ljava/lang/Object;
.source "XDBAESCrypt.java"


# static fields
.field private static final CRYPTO_KEY_ALGORITHM:Ljava/lang/String; = "AES"

.field private static final CRYPTO_KEY_BLOCKMODE:Ljava/lang/String; = "CBC"

.field private static final CRYPTO_KEY_PADDING:Ljava/lang/String; = "PKCS5Padding"

.field private static final CRYPTO_KEY_SIZE:I = 0x80

.field private static final CRYPTO_RANDOM_ALGORITHM:Ljava/lang/String; = "SHA1PRNG"

.field private static final CRYPTO_RANDOM_ALGORITHM2:Ljava/lang/String; = "Crypto"

.field private static CRYPTO_STRING:Ljava/lang/String;

.field public static ivBytes:[B

.field public static preferences:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-string v0, ""

    sput-object v0, Lcom/policydm/db/XDBAESCrypt;->CRYPTO_STRING:Ljava/lang/String;

    .line 32
    const/16 v0, 0x10

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/policydm/db/XDBAESCrypt;->ivBytes:[B

    return-void

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static generateKey()Ljavax/crypto/SecretKey;
    .locals 5

    .prologue
    .line 243
    const-string v4, ""

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 244
    const/4 v1, 0x0

    .line 247
    .local v1, "key":Ljavax/crypto/SecretKey;
    :try_start_0
    new-instance v3, Ljava/security/SecureRandom;

    invoke-direct {v3}, Ljava/security/SecureRandom;-><init>()V

    .line 248
    .local v3, "secureRandom":Ljava/security/SecureRandom;
    const-string v4, "AES"

    invoke-static {v4}, Ljavax/crypto/KeyGenerator;->getInstance(Ljava/lang/String;)Ljavax/crypto/KeyGenerator;

    move-result-object v2

    .line 249
    .local v2, "keyGenerator":Ljavax/crypto/KeyGenerator;
    const/16 v4, 0x100

    invoke-virtual {v2, v4, v3}, Ljavax/crypto/KeyGenerator;->init(ILjava/security/SecureRandom;)V

    .line 250
    invoke-virtual {v2}, Ljavax/crypto/KeyGenerator;->generateKey()Ljavax/crypto/SecretKey;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 256
    .end local v2    # "keyGenerator":Ljavax/crypto/KeyGenerator;
    .end local v3    # "secureRandom":Ljava/security/SecureRandom;
    :goto_0
    return-object v1

    .line 252
    :catch_0
    move-exception v0

    .line 254
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    goto :goto_0
.end method

.method public static xdbDecryptor([B)Ljava/lang/String;
    .locals 6
    .param p0, "decryptData"    # [B

    .prologue
    .line 118
    const-string v2, ""

    .line 119
    .local v2, "szDeCryptionResult":Ljava/lang/String;
    const/4 v1, 0x0

    .line 122
    .local v1, "result":[B
    const/4 v4, 0x2

    const/4 v5, 0x0

    :try_start_0
    invoke-static {p0, v4, v5}, Lcom/policydm/db/XDBAESCrypt;->xdbGetCryptionResult([BILjava/lang/String;)[B

    move-result-object v1

    .line 123
    new-instance v3, Ljava/lang/String;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v4

    invoke-direct {v3, v1, v4}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v2    # "szDeCryptionResult":Ljava/lang/String;
    .local v3, "szDeCryptionResult":Ljava/lang/String;
    move-object v2, v3

    .line 129
    .end local v3    # "szDeCryptionResult":Ljava/lang/String;
    .restart local v2    # "szDeCryptionResult":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 125
    :catch_0
    move-exception v0

    .line 127
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbDecryptor([BLjava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "decryptData"    # [B
    .param p1, "szSeedPassword"    # Ljava/lang/String;

    .prologue
    .line 83
    const-string v2, ""

    .line 84
    .local v2, "szDeCryptionResult":Ljava/lang/String;
    const/4 v1, 0x0

    .line 87
    .local v1, "result":[B
    const/4 v4, 0x2

    :try_start_0
    invoke-static {p0, v4, p1}, Lcom/policydm/db/XDBAESCrypt;->xdbGetCryptionResult([BILjava/lang/String;)[B

    move-result-object v1

    .line 88
    new-instance v3, Ljava/lang/String;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v4

    invoke-direct {v3, v1, v4}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v2    # "szDeCryptionResult":Ljava/lang/String;
    .local v3, "szDeCryptionResult":Ljava/lang/String;
    move-object v2, v3

    .line 94
    .end local v3    # "szDeCryptionResult":Ljava/lang/String;
    .restart local v2    # "szDeCryptionResult":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 90
    :catch_0
    move-exception v0

    .line 92
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbDecryptorStrBase64(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "szBase64"    # Ljava/lang/String;
    .param p1, "szSeedPassword"    # Ljava/lang/String;

    .prologue
    .line 99
    const-string v3, ""

    .line 100
    .local v3, "szDeCryptionResult":Ljava/lang/String;
    const/4 v0, 0x0

    .line 101
    .local v0, "decryptData":[B
    const/4 v2, 0x0

    .line 105
    .local v2, "result":[B
    const/4 v5, 0x0

    :try_start_0
    invoke-static {p0, v5}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    .line 106
    const/4 v5, 0x2

    invoke-static {v0, v5, p1}, Lcom/policydm/db/XDBAESCrypt;->xdbGetCryptionResult([BILjava/lang/String;)[B

    move-result-object v2

    .line 107
    new-instance v4, Ljava/lang/String;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v5

    invoke-direct {v4, v2, v5}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v3    # "szDeCryptionResult":Ljava/lang/String;
    .local v4, "szDeCryptionResult":Ljava/lang/String;
    move-object v3, v4

    .line 113
    .end local v4    # "szDeCryptionResult":Ljava/lang/String;
    .restart local v3    # "szDeCryptionResult":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 109
    :catch_0
    move-exception v1

    .line 111
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbEncryptor(Ljava/lang/String;)[B
    .locals 5
    .param p0, "szEncryptText"    # Ljava/lang/String;

    .prologue
    .line 68
    const/4 v1, 0x0

    .line 71
    .local v1, "encryptionResult":[B
    :try_start_0
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/policydm/db/XDBAESCrypt;->xdbGetCryptionResult([BILjava/lang/String;)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 78
    :goto_0
    return-object v1

    .line 73
    :catch_0
    move-exception v0

    .line 75
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbEncryptor(Ljava/lang/String;Ljava/lang/String;)[B
    .locals 4
    .param p0, "szEncryptText"    # Ljava/lang/String;
    .param p1, "szSeedPassword"    # Ljava/lang/String;

    .prologue
    .line 37
    const/4 v1, 0x0

    .line 40
    .local v1, "encryptionResult":[B
    :try_start_0
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v2, v3, p1}, Lcom/policydm/db/XDBAESCrypt;->xdbGetCryptionResult([BILjava/lang/String;)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 46
    :goto_0
    return-object v1

    .line 42
    :catch_0
    move-exception v0

    .line 44
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbEncryptorStrBase64(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "szEncryptText"    # Ljava/lang/String;
    .param p1, "szSeedPassword"    # Ljava/lang/String;

    .prologue
    .line 51
    const/4 v1, 0x0

    .line 52
    .local v1, "encryptionResult":[B
    const-string v2, ""

    .line 55
    .local v2, "result":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v3

    invoke-virtual {p0, v3}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v3, v4, p1}, Lcom/policydm/db/XDBAESCrypt;->xdbGetCryptionResult([BILjava/lang/String;)[B

    move-result-object v1

    .line 56
    if-eqz v1, :cond_0

    .line 57
    const/4 v3, 0x0

    invoke-static {v1, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 63
    :cond_0
    :goto_0
    return-object v2

    .line 59
    :catch_0
    move-exception v0

    .line 61
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static xdbGetCryptionResult([BILjava/lang/String;)[B
    .locals 11
    .param p0, "cryptionData"    # [B
    .param p1, "nCryptionMode"    # I
    .param p2, "szSeedPassword"    # Ljava/lang/String;

    .prologue
    const/16 v9, 0x10

    .line 134
    const/4 v3, 0x0

    .line 135
    .local v3, "mCipher":Ljavax/crypto/Cipher;
    const/4 v5, 0x0

    .line 136
    .local v5, "mLogCipher":Ljavax/crypto/Cipher;
    const/4 v0, 0x0

    .line 137
    .local v0, "cryptResult":[B
    const/4 v1, 0x0

    .line 138
    .local v1, "cryptionKey":[B
    const/4 v7, 0x0

    .line 139
    .local v7, "mSecureRandom":Ljava/security/SecureRandom;
    const/4 v4, 0x0

    .line 140
    .local v4, "mKeyGenerator":Ljavax/crypto/KeyGenerator;
    const/4 v6, 0x0

    .line 144
    .local v6, "mSecretKey":Ljavax/crypto/SecretKey;
    :try_start_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 146
    if-eqz v5, :cond_0

    if-eqz p1, :cond_1

    .line 148
    :cond_0
    const-string v8, "AES"

    invoke-static {v8}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v5

    .line 149
    new-instance v8, Ljavax/crypto/spec/SecretKeySpec;

    const/16 v9, 0x172c

    const/16 v10, 0x10

    invoke-static {v9, v10}, Lcom/policydm/db/XDBAESCrypt;->xdbMealyMachine(II)Ljava/lang/String;

    move-result-object v9

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v9

    const-string v10, "AES"

    invoke-direct {v8, v9, v10}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    invoke-virtual {v5, p1, v8}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 151
    :cond_1
    invoke-virtual {v5, p0}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v0

    .line 175
    :goto_0
    return-object v0

    .line 155
    :cond_2
    sget v8, Landroid/os/Build$VERSION;->SDK_INT:I

    if-le v8, v9, :cond_3

    .line 156
    const-string v8, "SHA1PRNG"

    const-string v9, "Crypto"

    invoke-static {v8, v9}, Ljava/security/SecureRandom;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljava/security/SecureRandom;

    move-result-object v7

    .line 159
    :goto_1
    const-string v8, "AES"

    invoke-static {v8}, Ljavax/crypto/KeyGenerator;->getInstance(Ljava/lang/String;)Ljavax/crypto/KeyGenerator;

    move-result-object v4

    .line 160
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v8

    invoke-virtual {p2, v8}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/security/SecureRandom;->setSeed([B)V

    .line 161
    const/16 v8, 0x80

    invoke-virtual {v4, v8, v7}, Ljavax/crypto/KeyGenerator;->init(ILjava/security/SecureRandom;)V

    .line 162
    invoke-virtual {v4}, Ljavax/crypto/KeyGenerator;->generateKey()Ljavax/crypto/SecretKey;

    move-result-object v6

    .line 163
    invoke-interface {v6}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v1

    .line 165
    invoke-static {}, Lcom/policydm/db/XDBAESCrypt;->xdmgetTransFormation()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v3

    .line 166
    new-instance v8, Ljavax/crypto/spec/SecretKeySpec;

    const-string v9, "AES"

    invoke-direct {v8, v1, v9}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    new-instance v9, Ljavax/crypto/spec/IvParameterSpec;

    sget-object v10, Lcom/policydm/db/XDBAESCrypt;->ivBytes:[B

    invoke-direct {v9, v10}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    invoke-virtual {v3, p1, v8, v9}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 167
    invoke-virtual {v3, p0}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v0

    goto :goto_0

    .line 158
    :cond_3
    const-string v8, "SHA1PRNG"

    invoke-static {v8}, Ljava/security/SecureRandom;->getInstance(Ljava/lang/String;)Ljava/security/SecureRandom;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    goto :goto_1

    .line 170
    :catch_0
    move-exception v2

    .line 172
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetCryptionkey()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 215
    sget-object v3, Lcom/policydm/db/XDBAESCrypt;->CRYPTO_STRING:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 217
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "config"

    invoke-virtual {v3, v4, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    sput-object v3, Lcom/policydm/db/XDBAESCrypt;->preferences:Landroid/content/SharedPreferences;

    .line 218
    sget-object v3, Lcom/policydm/db/XDBAESCrypt;->preferences:Landroid/content/SharedPreferences;

    const-string v4, "dbString"

    const-string v5, ""

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/policydm/db/XDBAESCrypt;->CRYPTO_STRING:Ljava/lang/String;

    .line 219
    const-string v3, "try read dbString"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 220
    sget-object v3, Lcom/policydm/db/XDBAESCrypt;->CRYPTO_STRING:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 222
    const-string v3, "not generated. create dbString"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 223
    invoke-static {}, Lcom/policydm/db/XDBAESCrypt;->generateKey()Ljavax/crypto/SecretKey;

    move-result-object v3

    invoke-interface {v3}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v3

    invoke-static {v3, v6}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/policydm/db/XDBAESCrypt;->CRYPTO_STRING:Ljava/lang/String;

    .line 224
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 225
    .local v0, "buf":Ljava/lang/StringBuffer;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    sget-object v3, Lcom/policydm/db/XDBAESCrypt;->CRYPTO_STRING:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v2, v3, :cond_4

    .line 227
    sget-object v3, Lcom/policydm/db/XDBAESCrypt;->CRYPTO_STRING:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x41

    if-lt v3, v4, :cond_0

    sget-object v3, Lcom/policydm/db/XDBAESCrypt;->CRYPTO_STRING:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x5a

    if-le v3, v4, :cond_2

    :cond_0
    sget-object v3, Lcom/policydm/db/XDBAESCrypt;->CRYPTO_STRING:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x61

    if-lt v3, v4, :cond_1

    sget-object v3, Lcom/policydm/db/XDBAESCrypt;->CRYPTO_STRING:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x7a

    if-le v3, v4, :cond_2

    :cond_1
    sget-object v3, Lcom/policydm/db/XDBAESCrypt;->CRYPTO_STRING:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x30

    if-lt v3, v4, :cond_3

    sget-object v3, Lcom/policydm/db/XDBAESCrypt;->CRYPTO_STRING:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x39

    if-gt v3, v4, :cond_3

    .line 229
    :cond_2
    sget-object v3, Lcom/policydm/db/XDBAESCrypt;->CRYPTO_STRING:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 225
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 232
    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/policydm/db/XDBAESCrypt;->CRYPTO_STRING:Ljava/lang/String;

    .line 233
    sget-object v3, Lcom/policydm/db/XDBAESCrypt;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 234
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v3, "dbString"

    sget-object v4, Lcom/policydm/db/XDBAESCrypt;->CRYPTO_STRING:Ljava/lang/String;

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 235
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 238
    :cond_5
    sget-object v3, Lcom/policydm/db/XDBAESCrypt;->CRYPTO_STRING:Ljava/lang/String;

    return-object v3
.end method

.method private static xdbMealyMachine(II)Ljava/lang/String;
    .locals 14
    .param p0, "v"    # I
    .param p1, "sz"    # I

    .prologue
    const/4 v13, 0x4

    const/4 v12, 0x3

    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v9, 0x2

    .line 180
    new-array v6, p1, [B

    .line 181
    .local v6, "str":[B
    const/16 v7, 0x10

    new-array v3, v7, [[I

    new-array v7, v9, [I

    fill-array-data v7, :array_0

    aput-object v7, v3, v10

    new-array v7, v9, [I

    fill-array-data v7, :array_1

    aput-object v7, v3, v11

    new-array v7, v9, [I

    fill-array-data v7, :array_2

    aput-object v7, v3, v9

    new-array v7, v9, [I

    fill-array-data v7, :array_3

    aput-object v7, v3, v12

    new-array v7, v9, [I

    fill-array-data v7, :array_4

    aput-object v7, v3, v13

    const/4 v7, 0x5

    new-array v8, v9, [I

    fill-array-data v8, :array_5

    aput-object v8, v3, v7

    const/4 v7, 0x6

    new-array v8, v9, [I

    fill-array-data v8, :array_6

    aput-object v8, v3, v7

    const/4 v7, 0x7

    new-array v8, v9, [I

    fill-array-data v8, :array_7

    aput-object v8, v3, v7

    const/16 v7, 0x8

    new-array v8, v9, [I

    fill-array-data v8, :array_8

    aput-object v8, v3, v7

    const/16 v7, 0x9

    new-array v8, v9, [I

    fill-array-data v8, :array_9

    aput-object v8, v3, v7

    const/16 v7, 0xa

    new-array v8, v9, [I

    fill-array-data v8, :array_a

    aput-object v8, v3, v7

    const/16 v7, 0xb

    new-array v8, v9, [I

    fill-array-data v8, :array_b

    aput-object v8, v3, v7

    const/16 v7, 0xc

    new-array v8, v9, [I

    fill-array-data v8, :array_c

    aput-object v8, v3, v7

    const/16 v7, 0xd

    new-array v8, v9, [I

    fill-array-data v8, :array_d

    aput-object v8, v3, v7

    const/16 v7, 0xe

    new-array v8, v9, [I

    fill-array-data v8, :array_e

    aput-object v8, v3, v7

    const/16 v7, 0xf

    new-array v8, v9, [I

    fill-array-data v8, :array_f

    aput-object v8, v3, v7

    .line 182
    .local v3, "next":[[I
    const/16 v7, 0x10

    new-array v4, v7, [[C

    new-array v7, v9, [C

    fill-array-data v7, :array_10

    aput-object v7, v4, v10

    new-array v7, v9, [C

    fill-array-data v7, :array_11

    aput-object v7, v4, v11

    new-array v7, v9, [C

    fill-array-data v7, :array_12

    aput-object v7, v4, v9

    new-array v7, v9, [C

    fill-array-data v7, :array_13

    aput-object v7, v4, v12

    new-array v7, v9, [C

    fill-array-data v7, :array_14

    aput-object v7, v4, v13

    const/4 v7, 0x5

    new-array v8, v9, [C

    fill-array-data v8, :array_15

    aput-object v8, v4, v7

    const/4 v7, 0x6

    new-array v8, v9, [C

    fill-array-data v8, :array_16

    aput-object v8, v4, v7

    const/4 v7, 0x7

    new-array v8, v9, [C

    fill-array-data v8, :array_17

    aput-object v8, v4, v7

    const/16 v7, 0x8

    new-array v8, v9, [C

    fill-array-data v8, :array_18

    aput-object v8, v4, v7

    const/16 v7, 0x9

    new-array v8, v9, [C

    fill-array-data v8, :array_19

    aput-object v8, v4, v7

    const/16 v7, 0xa

    new-array v8, v9, [C

    fill-array-data v8, :array_1a

    aput-object v8, v4, v7

    const/16 v7, 0xb

    new-array v8, v9, [C

    fill-array-data v8, :array_1b

    aput-object v8, v4, v7

    const/16 v7, 0xc

    new-array v8, v9, [C

    fill-array-data v8, :array_1c

    aput-object v8, v4, v7

    const/16 v7, 0xd

    new-array v8, v9, [C

    fill-array-data v8, :array_1d

    aput-object v8, v4, v7

    const/16 v7, 0xe

    new-array v8, v9, [C

    fill-array-data v8, :array_1e

    aput-object v8, v4, v7

    const/16 v7, 0xf

    new-array v8, v9, [C

    fill-array-data v8, :array_1f

    aput-object v8, v4, v7

    .line 200
    .local v4, "out_char":[[C
    const/4 v5, 0x0

    .local v5, "state":I
    const/4 v1, 0x0

    .local v1, "len":I
    move v2, v1

    .line 202
    .end local v1    # "len":I
    .local v2, "len":I
    :goto_0
    if-ge v2, p1, :cond_0

    .line 204
    and-int/lit8 v0, p0, 0x1

    .line 205
    .local v0, "input":I
    shr-int/lit8 p0, p0, 0x1

    .line 206
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "len":I
    .restart local v1    # "len":I
    aget-object v7, v4, v5

    aget-char v7, v7, v0

    int-to-byte v7, v7

    aput-byte v7, v6, v2

    .line 207
    aget-object v7, v3, v5

    aget v5, v7, v0

    move v2, v1

    .line 208
    .end local v1    # "len":I
    .restart local v2    # "len":I
    goto :goto_0

    .line 210
    .end local v0    # "input":I
    :cond_0
    new-instance v7, Ljava/lang/String;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v8

    invoke-direct {v7, v6, v8}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    return-object v7

    .line 181
    nop

    :array_0
    .array-data 4
        0xb
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x4
    .end array-data

    :array_2
    .array-data 4
        0x8
        0xf
    .end array-data

    :array_3
    .array-data 4
        0xb
        0x2
    .end array-data

    :array_4
    .array-data 4
        0x0
        0x3
    .end array-data

    :array_5
    .array-data 4
        0x9
        0x0
    .end array-data

    :array_6
    .array-data 4
        0xf
        0x0
    .end array-data

    :array_7
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_8
    .array-data 4
        0x5
        0x0
    .end array-data

    :array_9
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_a
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_b
    .array-data 4
        0x1
        0x6
    .end array-data

    :array_c
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_d
    .array-data 4
        0x3
        0xd
    .end array-data

    :array_e
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_f
    .array-data 4
        0x2
        0xd
    .end array-data

    .line 182
    :array_10
    .array-data 2
        0x73s
        0x33s
    .end array-data

    :array_11
    .array-data 2
        0x76s
        0x6es
    .end array-data

    :array_12
    .array-data 2
        0x31s
        0x39s
    .end array-data

    :array_13
    .array-data 2
        0x6ds
        0x30s
    .end array-data

    :array_14
    .array-data 2
        0x65s
        0x63s
    .end array-data

    :array_15
    .array-data 2
        0x33s
        0x42s
    .end array-data

    :array_16
    .array-data 2
        0x37s
        0x4es
    .end array-data

    :array_17
    .array-data 2
        0x6bs
        0x32s
    .end array-data

    :array_18
    .array-data 2
        0x32s
        0x43s
    .end array-data

    :array_19
    .array-data 2
        0x61s
        0x43s
    .end array-data

    :array_1a
    .array-data 2
        0x4as
        0x32s
    .end array-data

    :array_1b
    .array-data 2
        0x79s
        0x6cs
    .end array-data

    :array_1c
    .array-data 2
        0x38s
        0x64s
    .end array-data

    :array_1d
    .array-data 2
        0x31s
        0x30s
    .end array-data

    :array_1e
    .array-data 2
        0x41s
        0x5es
    .end array-data

    :array_1f
    .array-data 2
        0x37s
        0x30s
    .end array-data
.end method

.method private static xdmgetTransFormation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 261
    const-string v0, "AES/CBC/PKCS5Padding"

    return-object v0
.end method
