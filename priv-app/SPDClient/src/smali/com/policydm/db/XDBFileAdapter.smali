.class public Lcom/policydm/db/XDBFileAdapter;
.super Ljava/lang/Object;
.source "XDBFileAdapter.java"

# interfaces
.implements Lcom/policydm/interfaces/XDBInterface;
.implements Lcom/policydm/interfaces/XDMDefInterface;
.implements Lcom/policydm/interfaces/XDMInterface;


# static fields
.field private static final FFS_OWNER_SYNCML:I = 0xf0

.field private static final XDM_FFS_FILE_EXTENTION:Ljava/lang/String; = ".cfg"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    return-void
.end method

.method public static xdbAdpAppendDeltaFile(I[B)I
    .locals 4
    .param p0, "FileID"    # I
    .param p1, "pBuffer"    # [B

    .prologue
    .line 253
    const/4 v2, 0x0

    .line 254
    .local v2, "eRet":I
    invoke-static {}, Lcom/policydm/dlagent/XFOTADlAgent;->xfotaDlAgentGetWriteStatus()Z

    move-result v0

    .line 258
    .local v0, "bWriteStatus":Z
    :try_start_0
    invoke-static {p0}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpFileOpen(I)I

    move-result v2

    .line 259
    if-eqz v2, :cond_1

    if-nez v0, :cond_1

    .line 261
    invoke-static {p0, p1}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpFileCreate(I[B)I

    move-result v2

    .line 262
    if-nez v2, :cond_0

    .line 264
    const/4 v3, 0x1

    invoke-static {v3}, Lcom/policydm/dlagent/XFOTADlAgent;->xfotaDlAgentSetWriteStatus(Z)Z

    .line 292
    :goto_0
    return v2

    .line 268
    :cond_0
    const-string v3, "Create FAILED"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 288
    :catch_0
    move-exception v1

    .line 290
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 271
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_1
    if-eqz v2, :cond_2

    if-eqz v0, :cond_2

    .line 273
    :try_start_1
    const-string v3, "Append FAILED"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 277
    :cond_2
    invoke-static {p0, p1}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpAppendFileWrite(I[B)I

    move-result v2

    .line 278
    if-nez v2, :cond_3

    .line 280
    const/4 v3, 0x1

    invoke-static {v3}, Lcom/policydm/dlagent/XFOTADlAgent;->xfotaDlAgentSetWriteStatus(Z)Z

    goto :goto_0

    .line 284
    :cond_3
    const-string v3, "Append FAILED"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public static xdbAdpAppendFile(I[B)I
    .locals 3
    .param p0, "FileID"    # I
    .param p1, "pBuffer"    # [B

    .prologue
    .line 222
    const/4 v1, 0x0

    .line 226
    .local v1, "eRet":I
    :try_start_0
    invoke-static {p0}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpFileOpen(I)I

    move-result v1

    .line 227
    if-eqz v1, :cond_1

    .line 229
    invoke-static {p0, p1}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpFileCreate(I[B)I

    move-result v1

    .line 230
    if-eqz v1, :cond_0

    .line 232
    const-string v2, "Create FAILED"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 248
    :cond_0
    :goto_0
    return v1

    .line 237
    :cond_1
    invoke-static {p0, p1}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpAppendFileWrite(I[B)I

    move-result v1

    .line 238
    if-eqz v1, :cond_0

    .line 240
    const-string v2, "Append FAILED"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 244
    :catch_0
    move-exception v0

    .line 246
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbAdpAppendFileWrite(I[B)I
    .locals 9
    .param p0, "FileID"    # I
    .param p1, "buffer"    # [B

    .prologue
    .line 337
    const-string v6, ""

    .line 338
    .local v6, "szFileName":Ljava/lang/String;
    const/4 v0, 0x0

    .line 339
    .local v0, "Data":Ljava/io/DataOutputStream;
    const/4 v2, 0x0

    .line 340
    .local v2, "File":Ljava/io/FileOutputStream;
    const/4 v5, 0x0

    .line 342
    .local v5, "nRtnStatus":I
    if-lez p0, :cond_3

    .line 343
    invoke-static {p0}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpGetNameFromCallerID(I)Ljava/lang/String;

    move-result-object v6

    .line 349
    :try_start_0
    new-instance v3, Ljava/io/FileOutputStream;

    const/4 v7, 0x1

    invoke-direct {v3, v6, v7}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;Z)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_8
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 350
    .end local v2    # "File":Ljava/io/FileOutputStream;
    .local v3, "File":Ljava/io/FileOutputStream;
    :try_start_1
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_9
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 352
    .end local v0    # "Data":Ljava/io/DataOutputStream;
    .local v1, "Data":Ljava/io/DataOutputStream;
    :try_start_2
    monitor-enter v1
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_7
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 354
    :try_start_3
    invoke-virtual {v1, p1}, Ljava/io/DataOutputStream;->write([B)V

    .line 355
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 356
    :try_start_4
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->flush()V

    .line 357
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/FileDescriptor;->sync()V
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_7
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 373
    if-eqz v1, :cond_0

    .line 375
    :try_start_5
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V

    .line 377
    :cond_0
    if-eqz v3, :cond_1

    .line 379
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    :cond_1
    move-object v2, v3

    .end local v3    # "File":Ljava/io/FileOutputStream;
    .restart local v2    # "File":Ljava/io/FileOutputStream;
    move-object v0, v1

    .end local v1    # "Data":Ljava/io/DataOutputStream;
    .restart local v0    # "Data":Ljava/io/DataOutputStream;
    :cond_2
    :goto_0
    move v7, v5

    .line 389
    :goto_1
    return v7

    .line 345
    :cond_3
    const/4 v7, 0x2

    goto :goto_1

    .line 355
    .end local v0    # "Data":Ljava/io/DataOutputStream;
    .end local v2    # "File":Ljava/io/FileOutputStream;
    .restart local v1    # "Data":Ljava/io/DataOutputStream;
    .restart local v3    # "File":Ljava/io/FileOutputStream;
    :catchall_0
    move-exception v7

    :try_start_6
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :try_start_7
    throw v7
    :try_end_7
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 359
    :catch_0
    move-exception v4

    move-object v2, v3

    .end local v3    # "File":Ljava/io/FileOutputStream;
    .restart local v2    # "File":Ljava/io/FileOutputStream;
    move-object v0, v1

    .line 361
    .end local v1    # "Data":Ljava/io/DataOutputStream;
    .restart local v0    # "Data":Ljava/io/DataOutputStream;
    .local v4, "e":Ljava/io/FileNotFoundException;
    :goto_2
    :try_start_8
    invoke-virtual {v4}, Ljava/io/FileNotFoundException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 362
    const/4 v5, 0x3

    .line 373
    if-eqz v0, :cond_4

    .line 375
    :try_start_9
    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V

    .line 377
    :cond_4
    if-eqz v2, :cond_2

    .line 379
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1

    goto :goto_0

    .line 382
    :catch_1
    move-exception v4

    .line 384
    .local v4, "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 385
    const/4 v5, 0x5

    .line 387
    goto :goto_0

    .line 382
    .end local v0    # "Data":Ljava/io/DataOutputStream;
    .end local v2    # "File":Ljava/io/FileOutputStream;
    .end local v4    # "e":Ljava/io/IOException;
    .restart local v1    # "Data":Ljava/io/DataOutputStream;
    .restart local v3    # "File":Ljava/io/FileOutputStream;
    :catch_2
    move-exception v4

    .line 384
    .restart local v4    # "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 385
    const/4 v5, 0x5

    move-object v2, v3

    .end local v3    # "File":Ljava/io/FileOutputStream;
    .restart local v2    # "File":Ljava/io/FileOutputStream;
    move-object v0, v1

    .line 387
    .end local v1    # "Data":Ljava/io/DataOutputStream;
    .restart local v0    # "Data":Ljava/io/DataOutputStream;
    goto :goto_0

    .line 364
    .end local v4    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v4

    .line 366
    .restart local v4    # "e":Ljava/io/IOException;
    :goto_3
    :try_start_a
    invoke-virtual {v4}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 367
    const/4 v5, 0x4

    .line 373
    if-eqz v0, :cond_5

    .line 375
    :try_start_b
    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V

    .line 377
    :cond_5
    if-eqz v2, :cond_2

    .line 379
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_4

    goto :goto_0

    .line 382
    :catch_4
    move-exception v4

    .line 384
    invoke-virtual {v4}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 385
    const/4 v5, 0x5

    .line 387
    goto :goto_0

    .line 371
    .end local v4    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v7

    .line 373
    :goto_4
    if-eqz v0, :cond_6

    .line 375
    :try_start_c
    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V

    .line 377
    :cond_6
    if-eqz v2, :cond_7

    .line 379
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_5

    .line 386
    :cond_7
    :goto_5
    throw v7

    .line 382
    :catch_5
    move-exception v4

    .line 384
    .restart local v4    # "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 385
    const/4 v5, 0x5

    goto :goto_5

    .line 371
    .end local v2    # "File":Ljava/io/FileOutputStream;
    .end local v4    # "e":Ljava/io/IOException;
    .restart local v3    # "File":Ljava/io/FileOutputStream;
    :catchall_2
    move-exception v7

    move-object v2, v3

    .end local v3    # "File":Ljava/io/FileOutputStream;
    .restart local v2    # "File":Ljava/io/FileOutputStream;
    goto :goto_4

    .end local v0    # "Data":Ljava/io/DataOutputStream;
    .end local v2    # "File":Ljava/io/FileOutputStream;
    .restart local v1    # "Data":Ljava/io/DataOutputStream;
    .restart local v3    # "File":Ljava/io/FileOutputStream;
    :catchall_3
    move-exception v7

    move-object v2, v3

    .end local v3    # "File":Ljava/io/FileOutputStream;
    .restart local v2    # "File":Ljava/io/FileOutputStream;
    move-object v0, v1

    .end local v1    # "Data":Ljava/io/DataOutputStream;
    .restart local v0    # "Data":Ljava/io/DataOutputStream;
    goto :goto_4

    .line 364
    .end local v2    # "File":Ljava/io/FileOutputStream;
    .restart local v3    # "File":Ljava/io/FileOutputStream;
    :catch_6
    move-exception v4

    move-object v2, v3

    .end local v3    # "File":Ljava/io/FileOutputStream;
    .restart local v2    # "File":Ljava/io/FileOutputStream;
    goto :goto_3

    .end local v0    # "Data":Ljava/io/DataOutputStream;
    .end local v2    # "File":Ljava/io/FileOutputStream;
    .restart local v1    # "Data":Ljava/io/DataOutputStream;
    .restart local v3    # "File":Ljava/io/FileOutputStream;
    :catch_7
    move-exception v4

    move-object v2, v3

    .end local v3    # "File":Ljava/io/FileOutputStream;
    .restart local v2    # "File":Ljava/io/FileOutputStream;
    move-object v0, v1

    .end local v1    # "Data":Ljava/io/DataOutputStream;
    .restart local v0    # "Data":Ljava/io/DataOutputStream;
    goto :goto_3

    .line 359
    :catch_8
    move-exception v4

    goto :goto_2

    .end local v2    # "File":Ljava/io/FileOutputStream;
    .restart local v3    # "File":Ljava/io/FileOutputStream;
    :catch_9
    move-exception v4

    move-object v2, v3

    .end local v3    # "File":Ljava/io/FileOutputStream;
    .restart local v2    # "File":Ljava/io/FileOutputStream;
    goto :goto_2
.end method

.method public static xdbAdpDeleteFile(I)I
    .locals 2
    .param p0, "FileID"    # I

    .prologue
    .line 415
    const/4 v0, 0x0

    .line 416
    .local v0, "rc":I
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    invoke-static {v1, p0}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpFileDelete(Ljava/lang/String;I)I

    move-result v0

    .line 418
    return v0
.end method

.method public static xdbAdpFileCreate(I[B)I
    .locals 3
    .param p0, "FileID"    # I
    .param p1, "pBuffer"    # [B

    .prologue
    .line 79
    const/4 v1, 0x0

    .line 80
    .local v1, "szFileName":Ljava/lang/String;
    const/4 v0, 0x0

    .line 81
    .local v0, "nRet":I
    invoke-static {p0}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpGetNameFromCallerID(I)Ljava/lang/String;

    move-result-object v1

    .line 83
    invoke-static {v1, p1}, Lcom/policydm/db/XDBFile;->xdbFileCreateWrite(Ljava/lang/String;[B)Z

    move-result v2

    if-nez v2, :cond_0

    .line 84
    const/4 v0, 0x5

    .line 86
    :cond_0
    return v0
.end method

.method public static xdbAdpFileDelete(Ljava/lang/String;I)I
    .locals 5
    .param p0, "szName"    # Ljava/lang/String;
    .param p1, "FileID"    # I

    .prologue
    .line 131
    const/4 v2, 0x0

    .line 132
    .local v2, "eRet":I
    const/4 v3, 0x0

    .line 134
    .local v3, "szFileName":Ljava/lang/String;
    if-lez p1, :cond_0

    .line 136
    invoke-static {p1}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpGetNameFromCallerID(I)Ljava/lang/String;

    move-result-object v3

    .line 152
    :goto_0
    :try_start_0
    invoke-static {v3}, Lcom/policydm/db/XDBFile;->xdbFileRemove(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 153
    .local v0, "bRet":Z
    if-eqz v0, :cond_2

    .line 154
    const/4 v2, 0x0

    .end local v0    # "bRet":Z
    :goto_1
    move v4, v2

    .line 162
    :goto_2
    return v4

    .line 140
    :cond_0
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 142
    const-string v4, "pszFileName is NULL"

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 143
    const/4 v4, 0x2

    goto :goto_2

    .line 147
    :cond_1
    move-object v3, p0

    goto :goto_0

    .line 156
    .restart local v0    # "bRet":Z
    :cond_2
    const/4 v2, -0x1

    goto :goto_1

    .line 158
    .end local v0    # "bRet":Z
    :catch_0
    move-exception v1

    .line 160
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static xdbAdpFileExists(Ljava/lang/String;I)I
    .locals 4
    .param p0, "szName"    # Ljava/lang/String;
    .param p1, "FileID"    # I

    .prologue
    .line 91
    const/4 v2, 0x0

    .line 92
    .local v2, "szFileName":Ljava/lang/String;
    const/4 v1, 0x0

    .line 94
    .local v1, "eRet":I
    if-lez p1, :cond_0

    .line 96
    invoke-static {p1}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpGetNameFromCallerID(I)Ljava/lang/String;

    move-result-object v2

    .line 110
    :goto_0
    invoke-static {v2}, Lcom/policydm/db/XDBFile;->xdbFileExists(Ljava/lang/String;)Z

    move-result v0

    .line 111
    .local v0, "bRet":Z
    if-eqz v0, :cond_2

    .line 112
    const/4 v1, 0x0

    :goto_1
    move v3, v1

    .line 115
    .end local v0    # "bRet":Z
    :goto_2
    return v3

    .line 100
    :cond_0
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 102
    const/4 v3, 0x2

    goto :goto_2

    .line 106
    :cond_1
    move-object v2, p0

    goto :goto_0

    .line 114
    .restart local v0    # "bRet":Z
    :cond_2
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public static xdbAdpFileExists(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p0, "targetDirPath"    # Ljava/lang/String;
    .param p1, "targetName"    # Ljava/lang/String;

    .prologue
    .line 120
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 122
    :cond_0
    const/4 v0, 0x0

    .line 126
    :goto_0
    return v0

    .line 125
    :cond_1
    invoke-static {p0, p1}, Lcom/policydm/db/XDBFile;->xdbFileExists(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 126
    .local v0, "bRet":Z
    goto :goto_0
.end method

.method public static xdbAdpFileFreeSizeCheck(I)I
    .locals 11
    .param p0, "nDataSize"    # I

    .prologue
    const/4 v5, 0x0

    const/4 v10, 0x1

    .line 61
    const/4 v4, 0x4

    .line 62
    .local v4, "nret":I
    const-wide/16 v0, 0x0

    .line 63
    .local v0, "RemainSize":J
    const-wide/16 v2, 0x0

    .line 65
    .local v2, "TotalSize":J
    invoke-static {v10}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetAvailableMemorySize(I)J

    move-result-wide v0

    .line 66
    invoke-static {v10}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTotalMemorySize(I)J

    move-result-wide v2

    .line 67
    int-to-long v6, p0

    cmp-long v6, v6, v0

    if-gtz v6, :cond_0

    .line 69
    const-string v6, "Interior memory >>> XDB_FS_OK..."

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 70
    const-string v6, "Remain size : %d, Total size : %d and Delta Size : %d bytes"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v7, v5

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v7, v10

    const/4 v8, 0x2

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 71
    invoke-static {v10}, Lcom/policydm/dlagent/XFOTADlAgent;->xfotaDlAgentSetFileSaveIndex(I)V

    move v4, v5

    .line 74
    .end local v4    # "nret":I
    :cond_0
    return v4
.end method

.method private static xdbAdpFileOpen(I)I
    .locals 6
    .param p0, "FileID"    # I

    .prologue
    .line 297
    const-string v3, ""

    .line 298
    .local v3, "szFileName":Ljava/lang/String;
    const/4 v0, 0x0

    .line 300
    .local v0, "Input":Ljava/io/DataInputStream;
    if-lez p0, :cond_3

    .line 302
    invoke-static {p0}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpGetNameFromCallerID(I)Ljava/lang/String;

    move-result-object v3

    .line 305
    :try_start_0
    new-instance v1, Ljava/io/DataInputStream;

    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v3}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v4}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 316
    .end local v0    # "Input":Ljava/io/DataInputStream;
    .local v1, "Input":Ljava/io/DataInputStream;
    if-eqz v1, :cond_0

    .line 318
    :try_start_1
    invoke-virtual {v1}, Ljava/io/DataInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 332
    :cond_0
    :goto_0
    const/4 v4, 0x0

    move-object v0, v1

    .end local v1    # "Input":Ljava/io/DataInputStream;
    .restart local v0    # "Input":Ljava/io/DataInputStream;
    :cond_1
    :goto_1
    return v4

    .line 321
    .end local v0    # "Input":Ljava/io/DataInputStream;
    .restart local v1    # "Input":Ljava/io/DataInputStream;
    :catch_0
    move-exception v2

    .line 323
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 307
    .end local v1    # "Input":Ljava/io/DataInputStream;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v0    # "Input":Ljava/io/DataInputStream;
    :catch_1
    move-exception v2

    .line 309
    .local v2, "e":Ljava/io/FileNotFoundException;
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 310
    const/4 v4, 0x3

    .line 316
    if-eqz v0, :cond_1

    .line 318
    :try_start_3
    invoke-virtual {v0}, Ljava/io/DataInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    .line 321
    :catch_2
    move-exception v2

    .line 323
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 314
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 316
    if-eqz v0, :cond_2

    .line 318
    :try_start_4
    invoke-virtual {v0}, Ljava/io/DataInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 324
    :cond_2
    :goto_2
    throw v4

    .line 321
    :catch_3
    move-exception v2

    .line 323
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_2

    .line 329
    .end local v2    # "e":Ljava/io/IOException;
    :cond_3
    const/4 v4, 0x5

    goto :goto_1
.end method

.method public static xdbAdpGetFileSize(I)I
    .locals 5
    .param p0, "FileID"    # I

    .prologue
    .line 394
    const/4 v2, 0x0

    .line 395
    .local v2, "szFileName":Ljava/lang/String;
    const/4 v1, 0x0

    .line 397
    .local v1, "nSize":I
    if-lez p0, :cond_0

    .line 398
    invoke-static {p0}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpGetNameFromCallerID(I)Ljava/lang/String;

    move-result-object v2

    .line 404
    :try_start_0
    invoke-static {v2}, Lcom/policydm/db/XDBFile;->xdbFileGetSize(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v3

    long-to-int v1, v3

    :goto_0
    move v3, v1

    .line 410
    :goto_1
    return v3

    .line 400
    :cond_0
    const/4 v3, -0x1

    goto :goto_1

    .line 406
    :catch_0
    move-exception v0

    .line 408
    .local v0, "ex":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbAdpGetNameFromCallerID(I)Ljava/lang/String;
    .locals 10
    .param p0, "FileID"    # I

    .prologue
    const/4 v5, 0x3

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x2

    .line 32
    const/4 v3, 0x0

    .line 33
    .local v3, "szFileName":Ljava/lang/String;
    const-wide/16 v0, 0x0

    .line 35
    .local v0, "handle":J
    sget-object v4, Lcom/policydm/db/XDB$XDMFileParameter;->FileFirmwareData:Lcom/policydm/db/XDB$XDMFileParameter;

    invoke-virtual {v4}, Lcom/policydm/db/XDB$XDMFileParameter;->FileId()I

    move-result v4

    if-ne p0, v4, :cond_2

    .line 37
    invoke-static {}, Lcom/policydm/dlagent/XFOTADlAgent;->xfotaDlAgentGetFileSaveIndex()I

    move-result v2

    .line 38
    .local v2, "nDeltaFileIndex":I
    if-ne v2, v7, :cond_0

    .line 40
    const-string v4, "%s/%s"

    new-array v5, v7, [Ljava/lang/Object;

    sget-object v6, Lcom/policydm/adapter/XDMTargetAdapter;->EXTERNAL_DIR_PATH:Ljava/lang/String;

    aput-object v6, v5, v8

    const-string v6, "SE_policy_file.zip"

    aput-object v6, v5, v9

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 56
    .end local v2    # "nDeltaFileIndex":I
    :goto_0
    return-object v3

    .line 42
    .restart local v2    # "nDeltaFileIndex":I
    :cond_0
    if-ne v2, v5, :cond_1

    .line 44
    const-string v4, "%s/%s"

    new-array v5, v7, [Ljava/lang/Object;

    sget-object v6, Lcom/policydm/adapter/XDMTargetAdapter;->EXTERNAL_SD_DIR_PATH:Ljava/lang/String;

    aput-object v6, v5, v8

    const-string v6, "SE_policy_file.zip"

    aput-object v6, v5, v9

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 48
    :cond_1
    const-string v4, "%s%s"

    new-array v5, v7, [Ljava/lang/Object;

    const-string v6, "data/data/com.policydm/"

    aput-object v6, v5, v8

    const-string v6, "SE_policy_file.zip"

    aput-object v6, v5, v9

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 53
    .end local v2    # "nDeltaFileIndex":I
    :cond_2
    const v4, 0x249f00

    add-int/2addr v4, p0

    int-to-long v0, v4

    .line 54
    const-string v4, "%s%d%s"

    new-array v5, v5, [Ljava/lang/Object;

    const-string v6, "data/data/com.policydm/"

    aput-object v6, v5, v8

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v9

    const-string v6, ".cfg"

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public static xdbAdpReadFile(III)Ljava/lang/Object;
    .locals 4
    .param p0, "FileID"    # I
    .param p1, "nOffset"    # I
    .param p2, "nSize"    # I

    .prologue
    .line 184
    const/4 v2, 0x0

    .line 185
    .local v2, "szFileName":Ljava/lang/String;
    new-array v0, p2, [B

    .line 188
    .local v0, "Input":[B
    :try_start_0
    invoke-static {p0}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpGetNameFromCallerID(I)Ljava/lang/String;

    move-result-object v2

    .line 189
    invoke-static {v2, v0, p1, p2}, Lcom/policydm/db/XDBFile;->xdbFileRead(Ljava/lang/String;[BII)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 195
    :goto_0
    return-object v0

    .line 191
    :catch_0
    move-exception v1

    .line 193
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbAdpReadFile(III[B)Z
    .locals 4
    .param p0, "FileID"    # I
    .param p1, "nOffset"    # I
    .param p2, "nSize"    # I
    .param p3, "pData"    # [B

    .prologue
    .line 167
    const/4 v2, 0x0

    .line 168
    .local v2, "szFileName":Ljava/lang/String;
    const/4 v0, 0x0

    .line 172
    .local v0, "bRet":Z
    :try_start_0
    invoke-static {p0}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpGetNameFromCallerID(I)Ljava/lang/String;

    move-result-object v2

    .line 173
    invoke-static {v2, p3, p1, p2}, Lcom/policydm/db/XDBFile;->xdbFileRead(Ljava/lang/String;[BII)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 179
    :goto_0
    return v0

    .line 175
    :catch_0
    move-exception v1

    .line 177
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbAdpWriteFile(IILjava/lang/Object;)Z
    .locals 2
    .param p0, "FileID"    # I
    .param p1, "nSize"    # I
    .param p2, "pData"    # Ljava/lang/Object;

    .prologue
    .line 211
    const/4 v1, 0x0

    .line 212
    .local v1, "szFileName":Ljava/lang/String;
    const/4 v0, 0x1

    .line 214
    .local v0, "bRet":Z
    invoke-static {p0}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpGetNameFromCallerID(I)Ljava/lang/String;

    move-result-object v1

    .line 216
    invoke-static {v1, p1, p2}, Lcom/policydm/db/XDBFile;->xdbFileWrite(Ljava/lang/String;ILjava/lang/Object;)Z

    move-result v0

    .line 217
    return v0
.end method

.method public static xdbAdpWriteFile(ILjava/lang/Object;)Z
    .locals 2
    .param p0, "FileID"    # I
    .param p1, "pData"    # Ljava/lang/Object;

    .prologue
    .line 200
    const/4 v1, 0x0

    .line 201
    .local v1, "szFileName":Ljava/lang/String;
    const/4 v0, 0x1

    .line 203
    .local v0, "bRet":Z
    invoke-static {p0}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpGetNameFromCallerID(I)Ljava/lang/String;

    move-result-object v1

    .line 205
    invoke-static {v1, p1}, Lcom/policydm/db/XDBFile;->xdbFileWrite(Ljava/lang/String;Ljava/lang/Object;)Z

    move-result v0

    .line 206
    return v0
.end method
