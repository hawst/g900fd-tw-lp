.class final enum Lcom/policydm/db/XDB$XDMNVMParameter;
.super Ljava/lang/Enum;
.source "XDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/policydm/db/XDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "XDMNVMParameter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/policydm/db/XDB$XDMNVMParameter;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/policydm/db/XDB$XDMNVMParameter;

.field public static final enum NVM_DM_ACC_X_NODE:Lcom/policydm/db/XDB$XDMNVMParameter;

.field public static final enum NVM_DM_AGENT_INFO:Lcom/policydm/db/XDB$XDMNVMParameter;

.field public static final enum NVM_DM_INFO:Lcom/policydm/db/XDB$XDMNVMParameter;

.field public static final enum NVM_DM_PROFILE:Lcom/policydm/db/XDB$XDMNVMParameter;

.field public static final enum NVM_FUMO_INFO:Lcom/policydm/db/XDB$XDMNVMParameter;

.field public static final enum NVM_IMSI_INFO:Lcom/policydm/db/XDB$XDMNVMParameter;

.field public static final enum NVM_MAX:Lcom/policydm/db/XDB$XDMNVMParameter;

.field public static final enum NVM_NOTI_INFO:Lcom/policydm/db/XDB$XDMNVMParameter;

.field public static final enum NVM_NOTI_RESYNC_MODE:Lcom/policydm/db/XDB$XDMNVMParameter;

.field public static final enum NVM_POLLING_INFO:Lcom/policydm/db/XDB$XDMNVMParameter;

.field public static final enum NVM_SPD_INFO:Lcom/policydm/db/XDB$XDMNVMParameter;


# instance fields
.field private final nIndex:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 41
    new-instance v0, Lcom/policydm/db/XDB$XDMNVMParameter;

    const-string v1, "NVM_DM_PROFILE"

    invoke-direct {v0, v1, v4, v4}, Lcom/policydm/db/XDB$XDMNVMParameter;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/policydm/db/XDB$XDMNVMParameter;->NVM_DM_PROFILE:Lcom/policydm/db/XDB$XDMNVMParameter;

    .line 42
    new-instance v0, Lcom/policydm/db/XDB$XDMNVMParameter;

    const-string v1, "NVM_DM_INFO"

    invoke-direct {v0, v1, v5, v5}, Lcom/policydm/db/XDB$XDMNVMParameter;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/policydm/db/XDB$XDMNVMParameter;->NVM_DM_INFO:Lcom/policydm/db/XDB$XDMNVMParameter;

    .line 43
    new-instance v0, Lcom/policydm/db/XDB$XDMNVMParameter;

    const-string v1, "NVM_FUMO_INFO"

    invoke-direct {v0, v1, v6, v6}, Lcom/policydm/db/XDB$XDMNVMParameter;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/policydm/db/XDB$XDMNVMParameter;->NVM_FUMO_INFO:Lcom/policydm/db/XDB$XDMNVMParameter;

    .line 44
    new-instance v0, Lcom/policydm/db/XDB$XDMNVMParameter;

    const-string v1, "NVM_IMSI_INFO"

    invoke-direct {v0, v1, v7, v7}, Lcom/policydm/db/XDB$XDMNVMParameter;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/policydm/db/XDB$XDMNVMParameter;->NVM_IMSI_INFO:Lcom/policydm/db/XDB$XDMNVMParameter;

    .line 45
    new-instance v0, Lcom/policydm/db/XDB$XDMNVMParameter;

    const-string v1, "NVM_POLLING_INFO"

    invoke-direct {v0, v1, v8, v8}, Lcom/policydm/db/XDB$XDMNVMParameter;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/policydm/db/XDB$XDMNVMParameter;->NVM_POLLING_INFO:Lcom/policydm/db/XDB$XDMNVMParameter;

    .line 46
    new-instance v0, Lcom/policydm/db/XDB$XDMNVMParameter;

    const-string v1, "NVM_DM_ACC_X_NODE"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/policydm/db/XDB$XDMNVMParameter;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/policydm/db/XDB$XDMNVMParameter;->NVM_DM_ACC_X_NODE:Lcom/policydm/db/XDB$XDMNVMParameter;

    .line 47
    new-instance v0, Lcom/policydm/db/XDB$XDMNVMParameter;

    const-string v1, "NVM_NOTI_RESYNC_MODE"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/policydm/db/XDB$XDMNVMParameter;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/policydm/db/XDB$XDMNVMParameter;->NVM_NOTI_RESYNC_MODE:Lcom/policydm/db/XDB$XDMNVMParameter;

    .line 48
    new-instance v0, Lcom/policydm/db/XDB$XDMNVMParameter;

    const-string v1, "NVM_DM_AGENT_INFO"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/policydm/db/XDB$XDMNVMParameter;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/policydm/db/XDB$XDMNVMParameter;->NVM_DM_AGENT_INFO:Lcom/policydm/db/XDB$XDMNVMParameter;

    .line 49
    new-instance v0, Lcom/policydm/db/XDB$XDMNVMParameter;

    const-string v1, "NVM_NOTI_INFO"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/policydm/db/XDB$XDMNVMParameter;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/policydm/db/XDB$XDMNVMParameter;->NVM_NOTI_INFO:Lcom/policydm/db/XDB$XDMNVMParameter;

    .line 50
    new-instance v0, Lcom/policydm/db/XDB$XDMNVMParameter;

    const-string v1, "NVM_SPD_INFO"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/policydm/db/XDB$XDMNVMParameter;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/policydm/db/XDB$XDMNVMParameter;->NVM_SPD_INFO:Lcom/policydm/db/XDB$XDMNVMParameter;

    .line 51
    new-instance v0, Lcom/policydm/db/XDB$XDMNVMParameter;

    const-string v1, "NVM_MAX"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/policydm/db/XDB$XDMNVMParameter;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/policydm/db/XDB$XDMNVMParameter;->NVM_MAX:Lcom/policydm/db/XDB$XDMNVMParameter;

    .line 39
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/policydm/db/XDB$XDMNVMParameter;

    sget-object v1, Lcom/policydm/db/XDB$XDMNVMParameter;->NVM_DM_PROFILE:Lcom/policydm/db/XDB$XDMNVMParameter;

    aput-object v1, v0, v4

    sget-object v1, Lcom/policydm/db/XDB$XDMNVMParameter;->NVM_DM_INFO:Lcom/policydm/db/XDB$XDMNVMParameter;

    aput-object v1, v0, v5

    sget-object v1, Lcom/policydm/db/XDB$XDMNVMParameter;->NVM_FUMO_INFO:Lcom/policydm/db/XDB$XDMNVMParameter;

    aput-object v1, v0, v6

    sget-object v1, Lcom/policydm/db/XDB$XDMNVMParameter;->NVM_IMSI_INFO:Lcom/policydm/db/XDB$XDMNVMParameter;

    aput-object v1, v0, v7

    sget-object v1, Lcom/policydm/db/XDB$XDMNVMParameter;->NVM_POLLING_INFO:Lcom/policydm/db/XDB$XDMNVMParameter;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/policydm/db/XDB$XDMNVMParameter;->NVM_DM_ACC_X_NODE:Lcom/policydm/db/XDB$XDMNVMParameter;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/policydm/db/XDB$XDMNVMParameter;->NVM_NOTI_RESYNC_MODE:Lcom/policydm/db/XDB$XDMNVMParameter;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/policydm/db/XDB$XDMNVMParameter;->NVM_DM_AGENT_INFO:Lcom/policydm/db/XDB$XDMNVMParameter;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/policydm/db/XDB$XDMNVMParameter;->NVM_NOTI_INFO:Lcom/policydm/db/XDB$XDMNVMParameter;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/policydm/db/XDB$XDMNVMParameter;->NVM_SPD_INFO:Lcom/policydm/db/XDB$XDMNVMParameter;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/policydm/db/XDB$XDMNVMParameter;->NVM_MAX:Lcom/policydm/db/XDB$XDMNVMParameter;

    aput-object v2, v0, v1

    sput-object v0, Lcom/policydm/db/XDB$XDMNVMParameter;->$VALUES:[Lcom/policydm/db/XDB$XDMNVMParameter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "nIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 57
    iput p3, p0, Lcom/policydm/db/XDB$XDMNVMParameter;->nIndex:I

    .line 58
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/policydm/db/XDB$XDMNVMParameter;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 39
    const-class v0, Lcom/policydm/db/XDB$XDMNVMParameter;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/policydm/db/XDB$XDMNVMParameter;

    return-object v0
.end method

.method public static values()[Lcom/policydm/db/XDB$XDMNVMParameter;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/policydm/db/XDB$XDMNVMParameter;->$VALUES:[Lcom/policydm/db/XDB$XDMNVMParameter;

    invoke-virtual {v0}, [Lcom/policydm/db/XDB$XDMNVMParameter;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/policydm/db/XDB$XDMNVMParameter;

    return-object v0
.end method


# virtual methods
.method Value()I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lcom/policydm/db/XDB$XDMNVMParameter;->nIndex:I

    return v0
.end method
