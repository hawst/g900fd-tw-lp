.class public interface abstract Lcom/policydm/db/XDBSql;
.super Ljava/lang/Object;
.source "XDBSql.java"


# static fields
.field public static final DATABASE_ACCXLISTNODE_CREATE:Ljava/lang/String; = "create table if not exists accxlistnode (rowid integer primary key autoincrement, account text, appaddr text, appaddrport text, clientappauth text, serverappauth text, toconref text);"

.field public static final DATABASE_AGENTTYPE_CREATE:Ljava/lang/String; = "create table if not exists dmagent (rowid integer primary key autoincrement, agenttype integer);"

.field public static final DATABASE_FUMO_CREATE:Ljava/lang/String; = "create table if not exists fumo (rowid integer primary key autoincrement, protocol integer, serverurl text, serverip text, serverport integer, objectdownloadprotocol text, objectdownloadurl text, objectdownloadip text, objectdownloadport integer, statusnotifyprotocol text, statusnotifyurl text, statusnotifyip text, statusnotifyport integer, pkgname text, pkgversion text, pkgdesc text, processid text, pkgsize integer, status integer, downloadmode integer, correlator text, resultcode text, downloadResultcode text, initiatedtype integer, reporturl text);"

.field public static final DATABASE_NAME:Ljava/lang/String; = "policydmdb.db"

.field public static final DATABASE_NETWORK_CREATE:Ljava/lang/String; = "create table if not exists network (rowid integer primary key autoincrement, homeurl text, service integer, active integer, proxyuse integer, napnetworkprofilename text, napbearer integer, napaddrtype integer, napaddr text, nappapid text, nappapsecret text, pxportnbr integer, pxaddrtype integer, pxaddr text, pxpapid text, pxpapsecret text, staticipuse integer, staticip text, staticdnsuse integer, staticdns1 integer, staticdns2 integer, trafficclass integer);"

.field public static final DATABASE_NOTIFICATION_CREATE:Ljava/lang/String; = "create table if not exists notification (rowid integer primary key autoincrement, appId integer, uiMode integer, sessinoId text, serverId text, opmode integer, jobId integer);"

.field public static final DATABASE_POLLING_CREATE:Ljava/lang/String; = "create table if not exists polling (rowid integer primary key autoincrement, orgversionfileurl text, versionfileurl text, filename text, periodunit text, period integer, time integer, range integer, reportperiod integer, reporttime integer, reportrange integer, nextpollingtime integer, nextreporttime integer);"

.field public static final DATABASE_PROFILELIST_CREATE:Ljava/lang/String; = "create table if not exists profilelist (rowid integer primary key autoincrement, networkconnname text, proxyindex integer, profileindex integer, profilename1 text, profilename2 text, profilename3 text, sessionid text, notievent integer, notiopmode integer, notijobid integer, notiresyncmode integer, skipdevdiscovery integer, magicnumber integer, sessionsavestate integer, notiuievent integer, notiretrycount integer, status integer, appid integer, uictype integer, result integer, number integer, text text, len integer, size integer);"

.field public static final DATABASE_PROFILE_CREATE:Ljava/lang/String; = "create table if not exists profile (rowid integer primary key autoincrement, protocol text, serverurl text, serverip text, serverpath text, serverport integer, protocol_org text, serverurl_org text, serverip_org text, serverpath_org text, serverport_org integer, changedprotocol integer, appid text, authlevel text, serverauthlevel text, authtype integer, serverauthtype integer, username byte, password byte, serverid byte, serverpwd byte, clientnonce text, servernonce text, servernonceformat integer, clientnonceformat integer, profilename text, networkconnname text, prefconref text, magicnumber integer);"

.field public static final DATABASE_RESYNCMODE_CREATE:Ljava/lang/String; = "create table if not exists resyncmode (rowid integer primary key autoincrement, nonceresyncmode integer);"

.field public static final DATABASE_SIMINFO_CREATE:Ljava/lang/String; = "create table if not exists siminfo (rowid integer primary key autoincrement, imsi text, imsi2 text);"

.field public static final DATABASE_SPD_CREATE:Ljava/lang/String; = "create table if not exists spd (rowid integer primary key autoincrement, deviceregister integer, devicecreate integer, deviceupdate integer, pushtype text, pushregid text, status integer, servernonce text, policymode text, eulatime integer);"

.field public static final DATABASE_VERSION:I = 0xa

.field public static final XDB_ACCXLISTNODE_TABLE:Ljava/lang/String; = "accxlistnode"

.field public static final XDB_AGENTTYPE_TABLE:Ljava/lang/String; = "dmagent"

.field public static final XDB_FUMO_TABLE:Ljava/lang/String; = "fumo"

.field public static final XDB_NETWORK_TABLE:Ljava/lang/String; = "network"

.field public static final XDB_NOTI_TABLE:Ljava/lang/String; = "notification"

.field public static final XDB_POLLING_TABLE:Ljava/lang/String; = "polling"

.field public static final XDB_PROFILELIST_TABLE:Ljava/lang/String; = "profilelist"

.field public static final XDB_PROFILE_TABLE:Ljava/lang/String; = "profile"

.field public static final XDB_RESYNCMODE_TABLE:Ljava/lang/String; = "resyncmode"

.field public static final XDB_SIMINFO_TABLE:Ljava/lang/String; = "siminfo"

.field public static final XDB_SPD_TABLE:Ljava/lang/String; = "spd"

.field public static final XDB_SQL_ERR_COLUMN_NOT_FOUND:I = 0x3

.field public static final XDB_SQL_ERR_EXIST:I = 0x1

.field public static final XDB_SQL_ERR_TABLE_NOT_FOUND:I = 0x2

.field public static final XDB_SQL_FAIL:I = 0x4

.field public static final XDB_SQL_OK:I = 0x0

.field public static final XDM_SQL_DB_ACCXLISTNODE_ACCOUNT:Ljava/lang/String; = "account"

.field public static final XDM_SQL_DB_ACCXLISTNODE_APPADDR:Ljava/lang/String; = "appaddr"

.field public static final XDM_SQL_DB_ACCXLISTNODE_APPADDRPORT:Ljava/lang/String; = "appaddrport"

.field public static final XDM_SQL_DB_ACCXLISTNODE_CLIENTAPPAUTH:Ljava/lang/String; = "clientappauth"

.field public static final XDM_SQL_DB_ACCXLISTNODE_ROWID:J = 0x1L

.field public static final XDM_SQL_DB_ACCXLISTNODE_SERVERAPPAUTH:Ljava/lang/String; = "serverappauth"

.field public static final XDM_SQL_DB_ACCXLISTNODE_TOCONREF:Ljava/lang/String; = "toconref"

.field public static final XDM_SQL_DB_AGENTTYPE_ROWID:J = 0x1L

.field public static final XDM_SQL_DB_AGENT_INFO_AGENT_TYPE:Ljava/lang/String; = "agenttype"

.field public static final XDM_SQL_DB_FUMO_CORRELATOR:Ljava/lang/String; = "correlator"

.field public static final XDM_SQL_DB_FUMO_DOWNLOADMODE:Ljava/lang/String; = "downloadmode"

.field public static final XDM_SQL_DB_FUMO_DOWNLOAD_RESULTCODE:Ljava/lang/String; = "downloadResultcode"

.field public static final XDM_SQL_DB_FUMO_INITIATED_TYPE:Ljava/lang/String; = "initiatedtype"

.field public static final XDM_SQL_DB_FUMO_OBJECTDOWNLOADIP:Ljava/lang/String; = "objectdownloadip"

.field public static final XDM_SQL_DB_FUMO_OBJECTDOWNLOADPORT:Ljava/lang/String; = "objectdownloadport"

.field public static final XDM_SQL_DB_FUMO_OBJECTDOWNLOADPROTOCOL:Ljava/lang/String; = "objectdownloadprotocol"

.field public static final XDM_SQL_DB_FUMO_OBJECTDOWNLOADURL:Ljava/lang/String; = "objectdownloadurl"

.field public static final XDM_SQL_DB_FUMO_PKGDESC:Ljava/lang/String; = "pkgdesc"

.field public static final XDM_SQL_DB_FUMO_PKGNAME:Ljava/lang/String; = "pkgname"

.field public static final XDM_SQL_DB_FUMO_PKGSIZE:Ljava/lang/String; = "pkgsize"

.field public static final XDM_SQL_DB_FUMO_PKGVERSION:Ljava/lang/String; = "pkgversion"

.field public static final XDM_SQL_DB_FUMO_PROCESSID:Ljava/lang/String; = "processid"

.field public static final XDM_SQL_DB_FUMO_PROTOCOL:Ljava/lang/String; = "protocol"

.field public static final XDM_SQL_DB_FUMO_REPORTURL:Ljava/lang/String; = "reporturl"

.field public static final XDM_SQL_DB_FUMO_RESULTCODE:Ljava/lang/String; = "resultcode"

.field public static final XDM_SQL_DB_FUMO_ROWID:J = 0x1L

.field public static final XDM_SQL_DB_FUMO_SERVERIP:Ljava/lang/String; = "serverip"

.field public static final XDM_SQL_DB_FUMO_SERVERPORT:Ljava/lang/String; = "serverport"

.field public static final XDM_SQL_DB_FUMO_SERVERURL:Ljava/lang/String; = "serverurl"

.field public static final XDM_SQL_DB_FUMO_STATUS:Ljava/lang/String; = "status"

.field public static final XDM_SQL_DB_FUMO_STATUSNOTIFYIP:Ljava/lang/String; = "statusnotifyip"

.field public static final XDM_SQL_DB_FUMO_STATUSNOTIFYPORT:Ljava/lang/String; = "statusnotifyport"

.field public static final XDM_SQL_DB_FUMO_STATUSNOTIFYPROTOCOL:Ljava/lang/String; = "statusnotifyprotocol"

.field public static final XDM_SQL_DB_FUMO_STATUSNOTIFYURL:Ljava/lang/String; = "statusnotifyurl"

.field public static final XDM_SQL_DB_NETWORK1_ROWID:J = 0x1L

.field public static final XDM_SQL_DB_NETWORK_ACTIVE:Ljava/lang/String; = "active"

.field public static final XDM_SQL_DB_NETWORK_ADVSETTING_STATICDNS1:Ljava/lang/String; = "staticdns1"

.field public static final XDM_SQL_DB_NETWORK_ADVSETTING_STATICDNS2:Ljava/lang/String; = "staticdns2"

.field public static final XDM_SQL_DB_NETWORK_ADVSETTING_STATICDNSUSE:Ljava/lang/String; = "staticdnsuse"

.field public static final XDM_SQL_DB_NETWORK_ADVSETTING_STATICIP:Ljava/lang/String; = "staticip"

.field public static final XDM_SQL_DB_NETWORK_ADVSETTING_STATICIPUSE:Ljava/lang/String; = "staticipuse"

.field public static final XDM_SQL_DB_NETWORK_ADVSETTING_TRAFFICCLASS:Ljava/lang/String; = "trafficclass"

.field public static final XDM_SQL_DB_NETWORK_HOMEURL:Ljava/lang/String; = "homeurl"

.field public static final XDM_SQL_DB_NETWORK_NAP_ADDR:Ljava/lang/String; = "napaddr"

.field public static final XDM_SQL_DB_NETWORK_NAP_ADDRTYPE:Ljava/lang/String; = "napaddrtype"

.field public static final XDM_SQL_DB_NETWORK_NAP_AUTH_PAPID:Ljava/lang/String; = "nappapid"

.field public static final XDM_SQL_DB_NETWORK_NAP_AUTH_PAPSECRET:Ljava/lang/String; = "nappapsecret"

.field public static final XDM_SQL_DB_NETWORK_NAP_BEARER:Ljava/lang/String; = "napbearer"

.field public static final XDM_SQL_DB_NETWORK_NAP_NETWORKPROFILENAME:Ljava/lang/String; = "napnetworkprofilename"

.field public static final XDM_SQL_DB_NETWORK_PROXYUSE:Ljava/lang/String; = "proxyuse"

.field public static final XDM_SQL_DB_NETWORK_PX_ADDR:Ljava/lang/String; = "pxaddr"

.field public static final XDM_SQL_DB_NETWORK_PX_ADDRTYPE:Ljava/lang/String; = "pxaddrtype"

.field public static final XDM_SQL_DB_NETWORK_PX_AUTH_PAPID:Ljava/lang/String; = "pxpapid"

.field public static final XDM_SQL_DB_NETWORK_PX_AUTH_PAPSECRET:Ljava/lang/String; = "pxpapsecret"

.field public static final XDM_SQL_DB_NETWORK_PX_PORTNBR:Ljava/lang/String; = "pxportnbr"

.field public static final XDM_SQL_DB_NETWORK_SERVICE:Ljava/lang/String; = "service"

.field public static final XDM_SQL_DB_NOTI_APPID:Ljava/lang/String; = "appId"

.field public static final XDM_SQL_DB_NOTI_JOBID:Ljava/lang/String; = "jobId"

.field public static final XDM_SQL_DB_NOTI_OPMODE:Ljava/lang/String; = "opmode"

.field public static final XDM_SQL_DB_NOTI_ROWID:J = 0x1L

.field public static final XDM_SQL_DB_NOTI_SERVERID:Ljava/lang/String; = "serverId"

.field public static final XDM_SQL_DB_NOTI_SESSIONID:Ljava/lang/String; = "sessinoId"

.field public static final XDM_SQL_DB_NOTI_UIMODE:Ljava/lang/String; = "uiMode"

.field public static final XDM_SQL_DB_POLLINGINFO_ROWID:J = 0x1L

.field public static final XDM_SQL_DB_POLLING_FILENAME:Ljava/lang/String; = "filename"

.field public static final XDM_SQL_DB_POLLING_NEXTPOLLINGTIME:Ljava/lang/String; = "nextpollingtime"

.field public static final XDM_SQL_DB_POLLING_NEXTREPORTTIME:Ljava/lang/String; = "nextreporttime"

.field public static final XDM_SQL_DB_POLLING_ORGVERSIONURL:Ljava/lang/String; = "orgversionfileurl"

.field public static final XDM_SQL_DB_POLLING_PERIOD:Ljava/lang/String; = "period"

.field public static final XDM_SQL_DB_POLLING_PERIODUNIT:Ljava/lang/String; = "periodunit"

.field public static final XDM_SQL_DB_POLLING_RANGE:Ljava/lang/String; = "range"

.field public static final XDM_SQL_DB_POLLING_REPORT_PERIOD:Ljava/lang/String; = "reportperiod"

.field public static final XDM_SQL_DB_POLLING_REPORT_RANGE:Ljava/lang/String; = "reportrange"

.field public static final XDM_SQL_DB_POLLING_REPORT_TIME:Ljava/lang/String; = "reporttime"

.field public static final XDM_SQL_DB_POLLING_TIME:Ljava/lang/String; = "time"

.field public static final XDM_SQL_DB_POLLING_VERSIONURL:Ljava/lang/String; = "versionfileurl"

.field public static final XDM_SQL_DB_PROFILE1_ROWID:J = 0x1L

.field public static final XDM_SQL_DB_PROFILE2_ROWID:J = 0x2L

.field public static final XDM_SQL_DB_PROFILE3_ROWID:J = 0x3L

.field public static final XDM_SQL_DB_PROFILELIST_MAGICNUMBER:Ljava/lang/String; = "magicnumber"

.field public static final XDM_SQL_DB_PROFILELIST_NETWORKCONNNAME:Ljava/lang/String; = "networkconnname"

.field public static final XDM_SQL_DB_PROFILELIST_NOTIEVENT:Ljava/lang/String; = "notievent"

.field public static final XDM_SQL_DB_PROFILELIST_NOTIJOBID:Ljava/lang/String; = "notijobid"

.field public static final XDM_SQL_DB_PROFILELIST_NOTIOPMODE:Ljava/lang/String; = "notiopmode"

.field public static final XDM_SQL_DB_PROFILELIST_NOTIRESUMESTATE_NOTIRETRYCOUNT:Ljava/lang/String; = "notiretrycount"

.field public static final XDM_SQL_DB_PROFILELIST_NOTIRESUMESTATE_NOTIUIEVENT:Ljava/lang/String; = "notiuievent"

.field public static final XDM_SQL_DB_PROFILELIST_NOTIRESUMESTATE_SESSIONSAVESTATE:Ljava/lang/String; = "sessionsavestate"

.field public static final XDM_SQL_DB_PROFILELIST_NOTIRESYNCMODE:Ljava/lang/String; = "notiresyncmode"

.field public static final XDM_SQL_DB_PROFILELIST_PROFILEINDEX:Ljava/lang/String; = "profileindex"

.field public static final XDM_SQL_DB_PROFILELIST_PROFILENAME1:Ljava/lang/String; = "profilename1"

.field public static final XDM_SQL_DB_PROFILELIST_PROFILENAME2:Ljava/lang/String; = "profilename2"

.field public static final XDM_SQL_DB_PROFILELIST_PROFILENAME3:Ljava/lang/String; = "profilename3"

.field public static final XDM_SQL_DB_PROFILELIST_PROXYINDEX:Ljava/lang/String; = "proxyindex"

.field public static final XDM_SQL_DB_PROFILELIST_ROWID:J = 0x1L

.field public static final XDM_SQL_DB_PROFILELIST_SESSIONID:Ljava/lang/String; = "sessionid"

.field public static final XDM_SQL_DB_PROFILELIST_SKIPDEVDISCOVERY:Ljava/lang/String; = "skipdevdiscovery"

.field public static final XDM_SQL_DB_PROFILELIST_UICRESULTKEEP_APPID:Ljava/lang/String; = "appid"

.field public static final XDM_SQL_DB_PROFILELIST_UICRESULTKEEP_LEN:Ljava/lang/String; = "len"

.field public static final XDM_SQL_DB_PROFILELIST_UICRESULTKEEP_NUMBER:Ljava/lang/String; = "number"

.field public static final XDM_SQL_DB_PROFILELIST_UICRESULTKEEP_RESULT:Ljava/lang/String; = "result"

.field public static final XDM_SQL_DB_PROFILELIST_UICRESULTKEEP_SIZE:Ljava/lang/String; = "size"

.field public static final XDM_SQL_DB_PROFILELIST_UICRESULTKEEP_STATUS:Ljava/lang/String; = "status"

.field public static final XDM_SQL_DB_PROFILELIST_UICRESULTKEEP_TEXT:Ljava/lang/String; = "text"

.field public static final XDM_SQL_DB_PROFILELIST_UICRESULTKEEP_UICTYPE:Ljava/lang/String; = "uictype"

.field public static final XDM_SQL_DB_PROFILE_APPID:Ljava/lang/String; = "appid"

.field public static final XDM_SQL_DB_PROFILE_AUTHLEVEL:Ljava/lang/String; = "authlevel"

.field public static final XDM_SQL_DB_PROFILE_AUTHTYPE:Ljava/lang/String; = "authtype"

.field public static final XDM_SQL_DB_PROFILE_CHANGEDPROTOCOL:Ljava/lang/String; = "changedprotocol"

.field public static final XDM_SQL_DB_PROFILE_CLIENTNONCE:Ljava/lang/String; = "clientnonce"

.field public static final XDM_SQL_DB_PROFILE_CLIENTNONCEFORMAT:Ljava/lang/String; = "clientnonceformat"

.field public static final XDM_SQL_DB_PROFILE_MAGICNUMBER:Ljava/lang/String; = "magicnumber"

.field public static final XDM_SQL_DB_PROFILE_NETWORKCONNNAME:Ljava/lang/String; = "networkconnname"

.field public static final XDM_SQL_DB_PROFILE_PASSWORD:Ljava/lang/String; = "password"

.field public static final XDM_SQL_DB_PROFILE_PREFCONREF:Ljava/lang/String; = "prefconref"

.field public static final XDM_SQL_DB_PROFILE_PROFILENAME:Ljava/lang/String; = "profilename"

.field public static final XDM_SQL_DB_PROFILE_PROTOCOL:Ljava/lang/String; = "protocol"

.field public static final XDM_SQL_DB_PROFILE_PROTOCOL_ORG:Ljava/lang/String; = "protocol_org"

.field public static final XDM_SQL_DB_PROFILE_SERVERAUTHLEVEL:Ljava/lang/String; = "serverauthlevel"

.field public static final XDM_SQL_DB_PROFILE_SERVERAUTHTYPE:Ljava/lang/String; = "serverauthtype"

.field public static final XDM_SQL_DB_PROFILE_SERVERID:Ljava/lang/String; = "serverid"

.field public static final XDM_SQL_DB_PROFILE_SERVERIP:Ljava/lang/String; = "serverip"

.field public static final XDM_SQL_DB_PROFILE_SERVERIP_ORG:Ljava/lang/String; = "serverip_org"

.field public static final XDM_SQL_DB_PROFILE_SERVERNONCE:Ljava/lang/String; = "servernonce"

.field public static final XDM_SQL_DB_PROFILE_SERVERNONCEFORMAT:Ljava/lang/String; = "servernonceformat"

.field public static final XDM_SQL_DB_PROFILE_SERVERPATH:Ljava/lang/String; = "serverpath"

.field public static final XDM_SQL_DB_PROFILE_SERVERPATH_ORG:Ljava/lang/String; = "serverpath_org"

.field public static final XDM_SQL_DB_PROFILE_SERVERPORT:Ljava/lang/String; = "serverport"

.field public static final XDM_SQL_DB_PROFILE_SERVERPORT_ORG:Ljava/lang/String; = "serverport_org"

.field public static final XDM_SQL_DB_PROFILE_SERVERPWD:Ljava/lang/String; = "serverpwd"

.field public static final XDM_SQL_DB_PROFILE_SERVERURL:Ljava/lang/String; = "serverurl"

.field public static final XDM_SQL_DB_PROFILE_SERVERURL_ORG:Ljava/lang/String; = "serverurl_org"

.field public static final XDM_SQL_DB_PROFILE_USERNAME:Ljava/lang/String; = "username"

.field public static final XDM_SQL_DB_RESYNCMODE_NONCERESYNCMODE:Ljava/lang/String; = "nonceresyncmode"

.field public static final XDM_SQL_DB_RESYNCMODE_ROWID:J = 0x1L

.field public static final XDM_SQL_DB_ROWID:Ljava/lang/String; = "rowid"

.field public static final XDM_SQL_DB_SIMINFO_IMSI:Ljava/lang/String; = "imsi"

.field public static final XDM_SQL_DB_SIMINFO_IMSI2:Ljava/lang/String; = "imsi2"

.field public static final XDM_SQL_DB_SIMINFO_ROWID:J = 0x1L

.field public static final XDM_SQL_DB_SPD_DEVICECREATE:Ljava/lang/String; = "devicecreate"

.field public static final XDM_SQL_DB_SPD_DEVICEREGISTER:Ljava/lang/String; = "deviceregister"

.field public static final XDM_SQL_DB_SPD_DEVICEUPDATE:Ljava/lang/String; = "deviceupdate"

.field public static final XDM_SQL_DB_SPD_EULATIME:Ljava/lang/String; = "eulatime"

.field public static final XDM_SQL_DB_SPD_POLICYMODE:Ljava/lang/String; = "policymode"

.field public static final XDM_SQL_DB_SPD_PUSH_REGID:Ljava/lang/String; = "pushregid"

.field public static final XDM_SQL_DB_SPD_PUSH_TYPE:Ljava/lang/String; = "pushtype"

.field public static final XDM_SQL_DB_SPD_ROWID:J = 0x1L

.field public static final XDM_SQL_DB_SPD_SERVERNONCE:Ljava/lang/String; = "servernonce"

.field public static final XDM_SQL_DB_SPD_STATUS:Ljava/lang/String; = "status"

.field public static final xdmSqlDbIdAccXNode:I = 0x83

.field public static final xdmSqlDbIdDmAgentInfo:I = 0x61

.field public static final xdmSqlDbIdFUMOInfo:I = 0x57

.field public static final xdmSqlDbIdIMSIInfo:I = 0x58

.field public static final xdmSqlDbIdNetworkInfo:I = 0x80

.field public static final xdmSqlDbIdNotiInfo:I = 0x62

.field public static final xdmSqlDbIdPollingInfo:I = 0x59

.field public static final xdmSqlDbIdProfileInfo1:I = 0x52

.field public static final xdmSqlDbIdProfileInfo2:I = 0x53

.field public static final xdmSqlDbIdProfileInfo3:I = 0x54

.field public static final xdmSqlDbIdProfileInfo4:I = 0x55

.field public static final xdmSqlDbIdProfileInfo5:I = 0x56

.field public static final xdmSqlDbIdProfileList:I = 0x51

.field public static final xdmSqlDbIdResyncMode:I = 0x60

.field public static final xdmSqlDbIdSPDInfo:I = 0x63
