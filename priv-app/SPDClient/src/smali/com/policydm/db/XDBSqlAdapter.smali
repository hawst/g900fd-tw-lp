.class public Lcom/policydm/db/XDBSqlAdapter;
.super Lcom/policydm/db/XDBFactoryBootstrap;
.source "XDBSqlAdapter.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/policydm/db/XDBFactoryBootstrap;-><init>()V

    return-void
.end method

.method public static xdmDbSqlCreate(ILjava/lang/Object;)V
    .locals 0
    .param p0, "SqlID"    # I
    .param p1, "Input"    # Ljava/lang/Object;

    .prologue
    .line 14
    sparse-switch p0, :sswitch_data_0

    .line 64
    .end local p1    # "Input":Ljava/lang/Object;
    :goto_0
    return-void

    .line 17
    .restart local p1    # "Input":Ljava/lang/Object;
    :sswitch_0
    check-cast p1, Lcom/policydm/db/XDBProfileListInfo;

    .end local p1    # "Input":Ljava/lang/Object;
    invoke-static {p1}, Lcom/policydm/db/XDBSqlQuery;->xdmDbInsertProfileListRow(Lcom/policydm/db/XDBProfileListInfo;)V

    goto :goto_0

    .line 25
    .restart local p1    # "Input":Ljava/lang/Object;
    :sswitch_1
    check-cast p1, Lcom/policydm/db/XDBProfileInfo;

    .end local p1    # "Input":Ljava/lang/Object;
    invoke-static {p1}, Lcom/policydm/db/XDBSqlQuery;->xdmDbInsertProfileRow(Lcom/policydm/db/XDBProfileInfo;)V

    goto :goto_0

    .line 29
    .restart local p1    # "Input":Ljava/lang/Object;
    :sswitch_2
    check-cast p1, Lcom/policydm/db/XDBFumoInfo;

    .end local p1    # "Input":Ljava/lang/Object;
    invoke-static {p1}, Lcom/policydm/db/XDBSqlQuery;->xdmDbInsertFUMORow(Lcom/policydm/db/XDBFumoInfo;)V

    goto :goto_0

    .line 33
    .restart local p1    # "Input":Ljava/lang/Object;
    :sswitch_3
    check-cast p1, Lcom/policydm/db/XDBSimInfo;

    .end local p1    # "Input":Ljava/lang/Object;
    invoke-static {p1}, Lcom/policydm/db/XDBSqlQuery;->xdmDbInsertSimInfoRow(Lcom/policydm/db/XDBSimInfo;)V

    goto :goto_0

    .line 37
    .restart local p1    # "Input":Ljava/lang/Object;
    :sswitch_4
    check-cast p1, Lcom/policydm/db/XDBPollingInfo;

    .end local p1    # "Input":Ljava/lang/Object;
    invoke-static {p1}, Lcom/policydm/db/XDBSqlQuery;->xdmDbInsertPollingRow(Lcom/policydm/db/XDBPollingInfo;)V

    goto :goto_0

    .line 42
    .restart local p1    # "Input":Ljava/lang/Object;
    :sswitch_5
    check-cast p1, Lcom/policydm/db/XDBAccXNodeInfo;

    .end local p1    # "Input":Ljava/lang/Object;
    invoke-static {p1}, Lcom/policydm/db/XDBSqlQuery;->xdmDbInsertAccXListNodeRow(Lcom/policydm/db/XDBAccXNodeInfo;)V

    goto :goto_0

    .line 46
    .restart local p1    # "Input":Ljava/lang/Object;
    :sswitch_6
    check-cast p1, Lcom/policydm/db/XDBResyncModeInfo;

    .end local p1    # "Input":Ljava/lang/Object;
    invoke-static {p1}, Lcom/policydm/db/XDBSqlQuery;->xdmDbInsertResyncModeRow(Lcom/policydm/db/XDBResyncModeInfo;)V

    goto :goto_0

    .line 50
    .restart local p1    # "Input":Ljava/lang/Object;
    :sswitch_7
    check-cast p1, Lcom/policydm/db/XDBAgentInfo;

    .end local p1    # "Input":Ljava/lang/Object;
    invoke-static {p1}, Lcom/policydm/db/XDBSqlQuery;->xdmDbInsertAgentInfoRow(Lcom/policydm/db/XDBAgentInfo;)V

    goto :goto_0

    .line 54
    .restart local p1    # "Input":Ljava/lang/Object;
    :sswitch_8
    check-cast p1, Lcom/policydm/db/XDBNotiInfo;

    .end local p1    # "Input":Ljava/lang/Object;
    invoke-static {p1}, Lcom/policydm/db/XDBSqlQuery;->xdmDbInsertNotiRow(Lcom/policydm/db/XDBNotiInfo;)V

    goto :goto_0

    .line 58
    .restart local p1    # "Input":Ljava/lang/Object;
    :sswitch_9
    check-cast p1, Lcom/policydm/db/XDBSpdInfo;

    .end local p1    # "Input":Ljava/lang/Object;
    invoke-static {p1}, Lcom/policydm/db/XDBSqlQuery;->xdmDbInsertSpdRow(Lcom/policydm/db/XDBSpdInfo;)V

    goto :goto_0

    .line 14
    :sswitch_data_0
    .sparse-switch
        0x51 -> :sswitch_0
        0x52 -> :sswitch_1
        0x53 -> :sswitch_1
        0x54 -> :sswitch_1
        0x55 -> :sswitch_1
        0x56 -> :sswitch_1
        0x57 -> :sswitch_2
        0x58 -> :sswitch_3
        0x59 -> :sswitch_4
        0x60 -> :sswitch_6
        0x61 -> :sswitch_7
        0x62 -> :sswitch_8
        0x63 -> :sswitch_9
        0x83 -> :sswitch_5
    .end sparse-switch
.end method

.method public static xdmDbSqlDelete(I)V
    .locals 2
    .param p0, "SqlID"    # I

    .prologue
    const-wide/16 v0, 0x1

    .line 157
    sparse-switch p0, :sswitch_data_0

    .line 216
    :goto_0
    :sswitch_0
    return-void

    .line 160
    :sswitch_1
    invoke-static {v0, v1}, Lcom/policydm/db/XDBSqlQuery;->xdmDbDeleteProfileListRow(J)V

    goto :goto_0

    .line 164
    :sswitch_2
    invoke-static {v0, v1}, Lcom/policydm/db/XDBSqlQuery;->xdmDbDeleteProfileRow(J)V

    .line 165
    invoke-static {v0, v1}, Lcom/policydm/db/XDBSqlQuery;->xdmDbDeleteNetworkRow(J)V

    goto :goto_0

    .line 169
    :sswitch_3
    const-wide/16 v0, 0x2

    invoke-static {v0, v1}, Lcom/policydm/db/XDBSqlQuery;->xdmDbDeleteProfileRow(J)V

    goto :goto_0

    .line 173
    :sswitch_4
    const-wide/16 v0, 0x3

    invoke-static {v0, v1}, Lcom/policydm/db/XDBSqlQuery;->xdmDbDeleteProfileRow(J)V

    goto :goto_0

    .line 181
    :sswitch_5
    invoke-static {v0, v1}, Lcom/policydm/db/XDBSqlQuery;->xdmDbDeleteFUMORow(J)V

    goto :goto_0

    .line 185
    :sswitch_6
    invoke-static {v0, v1}, Lcom/policydm/db/XDBSqlQuery;->xdmDbDeleteSimInfoRow(J)V

    goto :goto_0

    .line 189
    :sswitch_7
    invoke-static {v0, v1}, Lcom/policydm/db/XDBSqlQuery;->xdmDbDeletePollingRow(J)V

    goto :goto_0

    .line 194
    :sswitch_8
    invoke-static {v0, v1}, Lcom/policydm/db/XDBSqlQuery;->xdmDbDeleteAccXListNodeRow(J)V

    goto :goto_0

    .line 198
    :sswitch_9
    invoke-static {v0, v1}, Lcom/policydm/db/XDBSqlQuery;->xdmDbDeleteResyncModeRow(J)V

    goto :goto_0

    .line 202
    :sswitch_a
    invoke-static {v0, v1}, Lcom/policydm/db/XDBSqlQuery;->xdmDbDeleteAgentInfoRow(J)V

    goto :goto_0

    .line 206
    :sswitch_b
    invoke-static {v0, v1}, Lcom/policydm/db/XDBSqlQuery;->xdmDbDeleteNotiRow(J)V

    goto :goto_0

    .line 210
    :sswitch_c
    invoke-static {v0, v1}, Lcom/policydm/db/XDBSqlQuery;->xdmDbDeleteSpdRow(J)V

    goto :goto_0

    .line 157
    nop

    :sswitch_data_0
    .sparse-switch
        0x51 -> :sswitch_1
        0x52 -> :sswitch_2
        0x53 -> :sswitch_3
        0x54 -> :sswitch_4
        0x55 -> :sswitch_0
        0x56 -> :sswitch_0
        0x57 -> :sswitch_5
        0x58 -> :sswitch_6
        0x59 -> :sswitch_7
        0x60 -> :sswitch_9
        0x61 -> :sswitch_a
        0x62 -> :sswitch_b
        0x63 -> :sswitch_c
        0x83 -> :sswitch_8
    .end sparse-switch
.end method

.method public static xdmDbSqlDelete(II)V
    .locals 2
    .param p0, "SqlID"    # I
    .param p1, "rowId"    # I

    .prologue
    .line 220
    packed-switch p0, :pswitch_data_0

    .line 229
    :goto_0
    return-void

    .line 223
    :pswitch_0
    int-to-long v0, p1

    invoke-static {v0, v1}, Lcom/policydm/db/XDBSqlQuery;->xdmDbDeleteNotiRow(J)V

    goto :goto_0

    .line 220
    nop

    :pswitch_data_0
    .packed-switch 0x62
        :pswitch_0
    .end packed-switch
.end method

.method public static xdmDbSqlDelete(ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "SqlID"    # I
    .param p1, "szColName"    # Ljava/lang/String;
    .param p2, "szColData"    # Ljava/lang/String;

    .prologue
    .line 233
    packed-switch p0, :pswitch_data_0

    .line 242
    :goto_0
    return-void

    .line 236
    :pswitch_0
    invoke-static {p1, p2}, Lcom/policydm/db/XDBSqlQuery;->xdmDbDeleteNotiRow(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 233
    :pswitch_data_0
    .packed-switch 0x62
        :pswitch_0
    .end packed-switch
.end method

.method public static xdmDbSqlRead(I)Ljava/lang/Object;
    .locals 8
    .param p0, "FileID"    # I

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    const-wide/16 v3, 0x1

    .line 246
    sparse-switch p0, :sswitch_data_0

    .line 312
    const/4 v1, 0x0

    :goto_0
    return-object v1

    .line 249
    :sswitch_0
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    sget-object v1, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v1, v1, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    invoke-static {v3, v4, v1}, Lcom/policydm/db/XDBSqlQuery;->xdmDbFetchProfileListRow(JLcom/policydm/db/XDBProfileListInfo;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/policydm/db/XDBProfileListInfo;

    iput-object v1, v2, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    .line 250
    sget-object v1, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v1, v1, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    goto :goto_0

    .line 253
    :sswitch_1
    sget-object v1, Lcom/policydm/db/XDB;->ProfileInfoClass:[Lcom/policydm/db/XDBProfileInfo;

    new-instance v2, Lcom/policydm/db/XDBProfileInfo;

    invoke-direct {v2}, Lcom/policydm/db/XDBProfileInfo;-><init>()V

    aput-object v2, v1, v6

    .line 254
    sget-object v2, Lcom/policydm/db/XDB;->ProfileInfoClass:[Lcom/policydm/db/XDBProfileInfo;

    sget-object v1, Lcom/policydm/db/XDB;->ProfileInfoClass:[Lcom/policydm/db/XDBProfileInfo;

    aget-object v1, v1, v6

    invoke-static {v3, v4, v1}, Lcom/policydm/db/XDBSqlQuery;->xdmDbFetchProfileRow(JLcom/policydm/db/XDBProfileInfo;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/policydm/db/XDBProfileInfo;

    aput-object v1, v2, v6

    .line 255
    sget-object v1, Lcom/policydm/db/XDB;->ProfileInfoClass:[Lcom/policydm/db/XDBProfileInfo;

    aget-object v1, v1, v6

    goto :goto_0

    .line 258
    :sswitch_2
    sget-object v1, Lcom/policydm/db/XDB;->ProfileInfoClass:[Lcom/policydm/db/XDBProfileInfo;

    new-instance v2, Lcom/policydm/db/XDBProfileInfo;

    invoke-direct {v2}, Lcom/policydm/db/XDBProfileInfo;-><init>()V

    aput-object v2, v1, v5

    .line 259
    sget-object v2, Lcom/policydm/db/XDB;->ProfileInfoClass:[Lcom/policydm/db/XDBProfileInfo;

    const-wide/16 v3, 0x2

    sget-object v1, Lcom/policydm/db/XDB;->ProfileInfoClass:[Lcom/policydm/db/XDBProfileInfo;

    aget-object v1, v1, v5

    invoke-static {v3, v4, v1}, Lcom/policydm/db/XDBSqlQuery;->xdmDbFetchProfileRow(JLcom/policydm/db/XDBProfileInfo;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/policydm/db/XDBProfileInfo;

    aput-object v1, v2, v5

    .line 260
    sget-object v1, Lcom/policydm/db/XDB;->ProfileInfoClass:[Lcom/policydm/db/XDBProfileInfo;

    aget-object v1, v1, v5

    goto :goto_0

    .line 263
    :sswitch_3
    sget-object v1, Lcom/policydm/db/XDB;->ProfileInfoClass:[Lcom/policydm/db/XDBProfileInfo;

    new-instance v2, Lcom/policydm/db/XDBProfileInfo;

    invoke-direct {v2}, Lcom/policydm/db/XDBProfileInfo;-><init>()V

    aput-object v2, v1, v7

    .line 264
    sget-object v2, Lcom/policydm/db/XDB;->ProfileInfoClass:[Lcom/policydm/db/XDBProfileInfo;

    const-wide/16 v3, 0x3

    sget-object v1, Lcom/policydm/db/XDB;->ProfileInfoClass:[Lcom/policydm/db/XDBProfileInfo;

    aget-object v1, v1, v7

    invoke-static {v3, v4, v1}, Lcom/policydm/db/XDBSqlQuery;->xdmDbFetchProfileRow(JLcom/policydm/db/XDBProfileInfo;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/policydm/db/XDBProfileInfo;

    aput-object v1, v2, v7

    .line 265
    sget-object v1, Lcom/policydm/db/XDB;->ProfileInfoClass:[Lcom/policydm/db/XDBProfileInfo;

    aget-object v1, v1, v7

    goto :goto_0

    .line 268
    :sswitch_4
    new-instance v1, Lcom/policydm/db/XDBInfoConRef;

    invoke-direct {v1}, Lcom/policydm/db/XDBInfoConRef;-><init>()V

    sput-object v1, Lcom/policydm/db/XDB;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    .line 269
    sget-object v1, Lcom/policydm/db/XDB;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    invoke-static {v3, v4, v1}, Lcom/policydm/db/XDBSqlQuery;->xdmDbFetchNetworkRow(JLcom/policydm/db/XDBInfoConRef;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/policydm/db/XDBInfoConRef;

    sput-object v1, Lcom/policydm/db/XDB;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    .line 270
    sget-object v1, Lcom/policydm/db/XDB;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    goto :goto_0

    .line 273
    :sswitch_5
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    sget-object v1, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v1, v1, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    invoke-static {v3, v4, v1}, Lcom/policydm/db/XDBSqlQuery;->xdmDbFetchFUMORow(JLcom/policydm/db/XDBFumoInfo;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/policydm/db/XDBFumoInfo;

    iput-object v1, v2, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    .line 274
    sget-object v1, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v1, v1, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    goto/16 :goto_0

    .line 277
    :sswitch_6
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    sget-object v1, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v1, v1, Lcom/policydm/db/XDBNvm;->NVMSimInfo:Lcom/policydm/db/XDBSimInfo;

    invoke-static {v3, v4, v1}, Lcom/policydm/db/XDBSqlQuery;->xdmDbFetchSimInfoRow(JLcom/policydm/db/XDBSimInfo;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/policydm/db/XDBSimInfo;

    iput-object v1, v2, Lcom/policydm/db/XDBNvm;->NVMSimInfo:Lcom/policydm/db/XDBSimInfo;

    .line 278
    sget-object v1, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v1, v1, Lcom/policydm/db/XDBNvm;->NVMSimInfo:Lcom/policydm/db/XDBSimInfo;

    goto/16 :goto_0

    .line 281
    :sswitch_7
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    sget-object v1, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v1, v1, Lcom/policydm/db/XDBNvm;->NVMPollingInfo:Lcom/policydm/db/XDBPollingInfo;

    invoke-static {v3, v4, v1}, Lcom/policydm/db/XDBSqlQuery;->xdmDbFetchPollingRow(JLcom/policydm/db/XDBPollingInfo;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/policydm/db/XDBPollingInfo;

    iput-object v1, v2, Lcom/policydm/db/XDBNvm;->NVMPollingInfo:Lcom/policydm/db/XDBPollingInfo;

    .line 282
    sget-object v1, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v1, v1, Lcom/policydm/db/XDBNvm;->NVMPollingInfo:Lcom/policydm/db/XDBPollingInfo;

    goto/16 :goto_0

    .line 285
    :sswitch_8
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    .line 287
    sget-object v1, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v1, v1, Lcom/policydm/db/XDBNvm;->NVMAccXNode:Lcom/policydm/db/XDBAccXListNode;

    iget-object v1, v1, Lcom/policydm/db/XDBAccXListNode;->stAccXNodeList:[Lcom/policydm/db/XDBAccXNodeInfo;

    new-instance v2, Lcom/policydm/db/XDBAccXNodeInfo;

    invoke-direct {v2}, Lcom/policydm/db/XDBAccXNodeInfo;-><init>()V

    aput-object v2, v1, v0

    .line 288
    sget-object v1, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v1, v1, Lcom/policydm/db/XDBNvm;->NVMAccXNode:Lcom/policydm/db/XDBAccXListNode;

    iget-object v2, v1, Lcom/policydm/db/XDBAccXListNode;->stAccXNodeList:[Lcom/policydm/db/XDBAccXNodeInfo;

    int-to-long v3, v0

    sget-object v1, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v1, v1, Lcom/policydm/db/XDBNvm;->NVMAccXNode:Lcom/policydm/db/XDBAccXListNode;

    iget-object v1, v1, Lcom/policydm/db/XDBAccXListNode;->stAccXNodeList:[Lcom/policydm/db/XDBAccXNodeInfo;

    aget-object v1, v1, v0

    invoke-static {v3, v4, v1}, Lcom/policydm/db/XDBSqlQuery;->xdmDbFetchAccXListNodeRow(JLcom/policydm/db/XDBAccXNodeInfo;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/policydm/db/XDBAccXNodeInfo;

    aput-object v1, v2, v0

    .line 285
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 290
    :cond_0
    sget-object v1, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v1, v1, Lcom/policydm/db/XDBNvm;->NVMAccXNode:Lcom/policydm/db/XDBAccXListNode;

    goto/16 :goto_0

    .line 293
    .end local v0    # "i":I
    :sswitch_9
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    sget-object v1, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v1, v1, Lcom/policydm/db/XDBNvm;->NVMResyncMode:Lcom/policydm/db/XDBResyncModeInfo;

    invoke-static {v3, v4, v1}, Lcom/policydm/db/XDBSqlQuery;->xdmDbFetchResyncModeRow(JLcom/policydm/db/XDBResyncModeInfo;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/policydm/db/XDBResyncModeInfo;

    iput-object v1, v2, Lcom/policydm/db/XDBNvm;->NVMResyncMode:Lcom/policydm/db/XDBResyncModeInfo;

    .line 294
    sget-object v1, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v1, v1, Lcom/policydm/db/XDBNvm;->NVMResyncMode:Lcom/policydm/db/XDBResyncModeInfo;

    goto/16 :goto_0

    .line 297
    :sswitch_a
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    sget-object v1, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v1, v1, Lcom/policydm/db/XDBNvm;->NVMDmAgentInfo:Lcom/policydm/db/XDBAgentInfo;

    invoke-static {v3, v4, v1}, Lcom/policydm/db/XDBSqlQuery;->xdmDbFetchAgentInfoRow(JLcom/policydm/db/XDBAgentInfo;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/policydm/db/XDBAgentInfo;

    iput-object v1, v2, Lcom/policydm/db/XDBNvm;->NVMDmAgentInfo:Lcom/policydm/db/XDBAgentInfo;

    .line 298
    sget-object v1, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v1, v1, Lcom/policydm/db/XDBNvm;->NVMDmAgentInfo:Lcom/policydm/db/XDBAgentInfo;

    goto/16 :goto_0

    .line 301
    :sswitch_b
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    sget-object v1, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v1, v1, Lcom/policydm/db/XDBNvm;->NVMNotiInfo:Lcom/policydm/db/XDBNotiInfo;

    invoke-static {v1}, Lcom/policydm/db/XDBSqlQuery;->xdmDbFetchNotiRow(Lcom/policydm/db/XDBNotiInfo;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/policydm/db/XDBNotiInfo;

    iput-object v1, v2, Lcom/policydm/db/XDBNvm;->NVMNotiInfo:Lcom/policydm/db/XDBNotiInfo;

    .line 302
    sget-object v1, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v1, v1, Lcom/policydm/db/XDBNvm;->NVMNotiInfo:Lcom/policydm/db/XDBNotiInfo;

    goto/16 :goto_0

    .line 305
    :sswitch_c
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    sget-object v1, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v1, v1, Lcom/policydm/db/XDBNvm;->NVMSPDInfo:Lcom/policydm/db/XDBSpdInfo;

    invoke-static {v3, v4, v1}, Lcom/policydm/db/XDBSqlQuery;->xdmDbFetchSpdRow(JLcom/policydm/db/XDBSpdInfo;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/policydm/db/XDBSpdInfo;

    iput-object v1, v2, Lcom/policydm/db/XDBNvm;->NVMSPDInfo:Lcom/policydm/db/XDBSpdInfo;

    .line 306
    sget-object v1, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v1, v1, Lcom/policydm/db/XDBNvm;->NVMSPDInfo:Lcom/policydm/db/XDBSpdInfo;

    iput-boolean v5, v1, Lcom/policydm/db/XDBSpdInfo;->m_bInitialized:Z

    .line 307
    sget-object v1, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v1, v1, Lcom/policydm/db/XDBNvm;->NVMSPDInfo:Lcom/policydm/db/XDBSpdInfo;

    goto/16 :goto_0

    .line 246
    :sswitch_data_0
    .sparse-switch
        0x51 -> :sswitch_0
        0x52 -> :sswitch_1
        0x53 -> :sswitch_2
        0x54 -> :sswitch_3
        0x57 -> :sswitch_5
        0x58 -> :sswitch_6
        0x59 -> :sswitch_7
        0x60 -> :sswitch_9
        0x61 -> :sswitch_a
        0x62 -> :sswitch_b
        0x63 -> :sswitch_c
        0x80 -> :sswitch_4
        0x83 -> :sswitch_8
    .end sparse-switch
.end method

.method public static xdmDbSqlRead(II)Ljava/lang/Object;
    .locals 4
    .param p0, "FileID"    # I
    .param p1, "rowId"    # I

    .prologue
    .line 317
    packed-switch p0, :pswitch_data_0

    .line 326
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 320
    :pswitch_0
    sget-object v1, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    int-to-long v2, p1

    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMNotiInfo:Lcom/policydm/db/XDBNotiInfo;

    invoke-static {v2, v3, v0}, Lcom/policydm/db/XDBSqlQuery;->xdmDbFetchNotiRow(JLcom/policydm/db/XDBNotiInfo;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/policydm/db/XDBNotiInfo;

    iput-object v0, v1, Lcom/policydm/db/XDBNvm;->NVMNotiInfo:Lcom/policydm/db/XDBNotiInfo;

    .line 321
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMNotiInfo:Lcom/policydm/db/XDBNotiInfo;

    goto :goto_0

    .line 317
    nop

    :pswitch_data_0
    .packed-switch 0x62
        :pswitch_0
    .end packed-switch
.end method

.method public static xdmDbSqlUpdate(IILjava/lang/Object;)V
    .locals 2
    .param p0, "SqlID"    # I
    .param p1, "rowId"    # I
    .param p2, "Input"    # Ljava/lang/Object;

    .prologue
    .line 143
    packed-switch p0, :pswitch_data_0

    .line 153
    .end local p2    # "Input":Ljava/lang/Object;
    :goto_0
    return-void

    .line 146
    .restart local p2    # "Input":Ljava/lang/Object;
    :pswitch_0
    sget-object v1, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    move-object v0, p2

    check-cast v0, Lcom/policydm/db/XDBNotiInfo;

    iput-object v0, v1, Lcom/policydm/db/XDBNvm;->NVMNotiInfo:Lcom/policydm/db/XDBNotiInfo;

    .line 147
    int-to-long v0, p1

    check-cast p2, Lcom/policydm/db/XDBNotiInfo;

    .end local p2    # "Input":Ljava/lang/Object;
    invoke-static {v0, v1, p2}, Lcom/policydm/db/XDBSqlQuery;->xdmDbUpdateNotiRow(JLcom/policydm/db/XDBNotiInfo;)V

    goto :goto_0

    .line 143
    :pswitch_data_0
    .packed-switch 0x62
        :pswitch_0
    .end packed-switch
.end method

.method public static xdmDbSqlUpdate(ILjava/lang/Object;)V
    .locals 5
    .param p0, "SqlID"    # I
    .param p1, "Input"    # Ljava/lang/Object;

    .prologue
    const-wide/16 v3, 0x1

    .line 68
    sparse-switch p0, :sswitch_data_0

    .line 139
    .end local p1    # "Input":Ljava/lang/Object;
    :cond_0
    :goto_0
    return-void

    .line 71
    .restart local p1    # "Input":Ljava/lang/Object;
    :sswitch_0
    sget-object v1, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    check-cast p1, Lcom/policydm/db/XDBProfileListInfo;

    .end local p1    # "Input":Ljava/lang/Object;
    iput-object p1, v1, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    .line 72
    sget-object v1, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v1, v1, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    invoke-static {v3, v4, v1}, Lcom/policydm/db/XDBSqlQuery;->xdmDbUpdateProfileListRow(JLcom/policydm/db/XDBProfileListInfo;)V

    goto :goto_0

    .line 76
    .restart local p1    # "Input":Ljava/lang/Object;
    :sswitch_1
    sget-object v1, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    check-cast p1, Lcom/policydm/db/XDBProfileInfo;

    .end local p1    # "Input":Ljava/lang/Object;
    iput-object p1, v1, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    .line 77
    sget-object v1, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v1, v1, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    invoke-static {v3, v4, v1}, Lcom/policydm/db/XDBSqlQuery;->xdmDbUpdateProfileRow(JLcom/policydm/db/XDBProfileInfo;)V

    goto :goto_0

    .line 81
    .restart local p1    # "Input":Ljava/lang/Object;
    :sswitch_2
    sget-object v1, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    check-cast p1, Lcom/policydm/db/XDBProfileInfo;

    .end local p1    # "Input":Ljava/lang/Object;
    iput-object p1, v1, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    .line 82
    const-wide/16 v1, 0x2

    sget-object v3, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v3, v3, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    invoke-static {v1, v2, v3}, Lcom/policydm/db/XDBSqlQuery;->xdmDbUpdateProfileRow(JLcom/policydm/db/XDBProfileInfo;)V

    goto :goto_0

    .line 86
    .restart local p1    # "Input":Ljava/lang/Object;
    :sswitch_3
    sget-object v1, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    check-cast p1, Lcom/policydm/db/XDBProfileInfo;

    .end local p1    # "Input":Ljava/lang/Object;
    iput-object p1, v1, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    .line 87
    const-wide/16 v1, 0x3

    sget-object v3, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v3, v3, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    invoke-static {v1, v2, v3}, Lcom/policydm/db/XDBSqlQuery;->xdmDbUpdateProfileRow(JLcom/policydm/db/XDBProfileInfo;)V

    goto :goto_0

    .line 91
    .restart local p1    # "Input":Ljava/lang/Object;
    :sswitch_4
    sget-object v1, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v1, v1, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    check-cast p1, Lcom/policydm/db/XDBInfoConRef;

    .end local p1    # "Input":Ljava/lang/Object;
    iput-object p1, v1, Lcom/policydm/db/XDBProfileInfo;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    .line 92
    sget-object v1, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v1, v1, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    iget-object v1, v1, Lcom/policydm/db/XDBProfileInfo;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    invoke-static {v3, v4, v1}, Lcom/policydm/db/XDBSqlQuery;->xdmDbUpdateNetworkRow(JLcom/policydm/db/XDBInfoConRef;)V

    goto :goto_0

    .line 96
    .restart local p1    # "Input":Ljava/lang/Object;
    :sswitch_5
    sget-object v1, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    check-cast p1, Lcom/policydm/db/XDBFumoInfo;

    .end local p1    # "Input":Ljava/lang/Object;
    iput-object p1, v1, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    .line 97
    sget-object v1, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v1, v1, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    invoke-static {v3, v4, v1}, Lcom/policydm/db/XDBSqlQuery;->xdmDbUpdateFUMORow(JLcom/policydm/db/XDBFumoInfo;)V

    goto :goto_0

    .line 101
    .restart local p1    # "Input":Ljava/lang/Object;
    :sswitch_6
    sget-object v1, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    check-cast p1, Lcom/policydm/db/XDBSimInfo;

    .end local p1    # "Input":Ljava/lang/Object;
    iput-object p1, v1, Lcom/policydm/db/XDBNvm;->NVMSimInfo:Lcom/policydm/db/XDBSimInfo;

    .line 102
    sget-object v1, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v1, v1, Lcom/policydm/db/XDBNvm;->NVMSimInfo:Lcom/policydm/db/XDBSimInfo;

    invoke-static {v3, v4, v1}, Lcom/policydm/db/XDBSqlQuery;->xdmDbUpdateSimInfoRow(JLcom/policydm/db/XDBSimInfo;)V

    goto :goto_0

    .line 106
    .restart local p1    # "Input":Ljava/lang/Object;
    :sswitch_7
    sget-object v1, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    check-cast p1, Lcom/policydm/db/XDBPollingInfo;

    .end local p1    # "Input":Ljava/lang/Object;
    iput-object p1, v1, Lcom/policydm/db/XDBNvm;->NVMPollingInfo:Lcom/policydm/db/XDBPollingInfo;

    .line 107
    sget-object v1, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v1, v1, Lcom/policydm/db/XDBNvm;->NVMPollingInfo:Lcom/policydm/db/XDBPollingInfo;

    invoke-static {v3, v4, v1}, Lcom/policydm/db/XDBSqlQuery;->xdmDbUpdatePollingRow(JLcom/policydm/db/XDBPollingInfo;)V

    goto :goto_0

    .line 111
    .restart local p1    # "Input":Ljava/lang/Object;
    :sswitch_8
    sget-object v1, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    check-cast p1, Lcom/policydm/db/XDBAccXListNode;

    .end local p1    # "Input":Ljava/lang/Object;
    iput-object p1, v1, Lcom/policydm/db/XDBNvm;->NVMAccXNode:Lcom/policydm/db/XDBAccXListNode;

    .line 112
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    .line 113
    int-to-long v1, v0

    sget-object v3, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v3, v3, Lcom/policydm/db/XDBNvm;->NVMAccXNode:Lcom/policydm/db/XDBAccXListNode;

    iget-object v3, v3, Lcom/policydm/db/XDBAccXListNode;->stAccXNodeList:[Lcom/policydm/db/XDBAccXNodeInfo;

    aget-object v3, v3, v0

    invoke-static {v1, v2, v3}, Lcom/policydm/db/XDBSqlQuery;->xdmDbUpdateAccXListNodeRow(JLcom/policydm/db/XDBAccXNodeInfo;)V

    .line 112
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 117
    .end local v0    # "i":I
    .restart local p1    # "Input":Ljava/lang/Object;
    :sswitch_9
    sget-object v1, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    check-cast p1, Lcom/policydm/db/XDBResyncModeInfo;

    .end local p1    # "Input":Ljava/lang/Object;
    iput-object p1, v1, Lcom/policydm/db/XDBNvm;->NVMResyncMode:Lcom/policydm/db/XDBResyncModeInfo;

    .line 118
    sget-object v1, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v1, v1, Lcom/policydm/db/XDBNvm;->NVMResyncMode:Lcom/policydm/db/XDBResyncModeInfo;

    invoke-static {v3, v4, v1}, Lcom/policydm/db/XDBSqlQuery;->xdmDbUpdateResyncModeRow(JLcom/policydm/db/XDBResyncModeInfo;)V

    goto/16 :goto_0

    .line 122
    .restart local p1    # "Input":Ljava/lang/Object;
    :sswitch_a
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    move-object v1, p1

    check-cast v1, Lcom/policydm/db/XDBAgentInfo;

    iput-object v1, v2, Lcom/policydm/db/XDBNvm;->NVMDmAgentInfo:Lcom/policydm/db/XDBAgentInfo;

    .line 123
    check-cast p1, Lcom/policydm/db/XDBAgentInfo;

    .end local p1    # "Input":Ljava/lang/Object;
    invoke-static {v3, v4, p1}, Lcom/policydm/db/XDBSqlQuery;->xdmDbUpdateAgentInfoRow(JLcom/policydm/db/XDBAgentInfo;)V

    goto/16 :goto_0

    .line 127
    .restart local p1    # "Input":Ljava/lang/Object;
    :sswitch_b
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    move-object v1, p1

    check-cast v1, Lcom/policydm/db/XDBNotiInfo;

    iput-object v1, v2, Lcom/policydm/db/XDBNvm;->NVMNotiInfo:Lcom/policydm/db/XDBNotiInfo;

    .line 128
    check-cast p1, Lcom/policydm/db/XDBNotiInfo;

    .end local p1    # "Input":Ljava/lang/Object;
    invoke-static {v3, v4, p1}, Lcom/policydm/db/XDBSqlQuery;->xdmDbUpdateNotiRow(JLcom/policydm/db/XDBNotiInfo;)V

    goto/16 :goto_0

    .line 132
    .restart local p1    # "Input":Ljava/lang/Object;
    :sswitch_c
    sget-object v2, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    move-object v1, p1

    check-cast v1, Lcom/policydm/db/XDBSpdInfo;

    iput-object v1, v2, Lcom/policydm/db/XDBNvm;->NVMSPDInfo:Lcom/policydm/db/XDBSpdInfo;

    .line 133
    check-cast p1, Lcom/policydm/db/XDBSpdInfo;

    .end local p1    # "Input":Ljava/lang/Object;
    invoke-static {v3, v4, p1}, Lcom/policydm/db/XDBSqlQuery;->xdmDbUpdateSpdRow(JLcom/policydm/db/XDBSpdInfo;)V

    goto/16 :goto_0

    .line 68
    :sswitch_data_0
    .sparse-switch
        0x51 -> :sswitch_0
        0x52 -> :sswitch_1
        0x53 -> :sswitch_2
        0x54 -> :sswitch_3
        0x57 -> :sswitch_5
        0x58 -> :sswitch_6
        0x59 -> :sswitch_7
        0x60 -> :sswitch_9
        0x61 -> :sswitch_a
        0x62 -> :sswitch_b
        0x63 -> :sswitch_c
        0x80 -> :sswitch_4
        0x83 -> :sswitch_8
    .end sparse-switch
.end method
