.class final enum Lcom/policydm/db/XDB$XDMFileParameter;
.super Ljava/lang/Enum;
.source "XDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/policydm/db/XDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "XDMFileParameter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/policydm/db/XDB$XDMFileParameter;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/policydm/db/XDB$XDMFileParameter;

.field public static final enum FileFirmwareData:Lcom/policydm/db/XDB$XDMFileParameter;

.field public static final enum FileLargeObjectData:Lcom/policydm/db/XDB$XDMFileParameter;

.field public static final enum FileMax:Lcom/policydm/db/XDB$XDMFileParameter;

.field public static final enum FileObjectData:Lcom/policydm/db/XDB$XDMFileParameter;

.field public static final enum FileObjectTreeInfo:Lcom/policydm/db/XDB$XDMFileParameter;

.field public static final enum FileTndsXmlData:Lcom/policydm/db/XDB$XDMFileParameter;


# instance fields
.field private final nFileId:I

.field private final nIndex:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 68
    new-instance v0, Lcom/policydm/db/XDB$XDMFileParameter;

    const-string v1, "FileObjectTreeInfo"

    const/16 v2, 0x100

    invoke-direct {v0, v1, v5, v5, v2}, Lcom/policydm/db/XDB$XDMFileParameter;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/policydm/db/XDB$XDMFileParameter;->FileObjectTreeInfo:Lcom/policydm/db/XDB$XDMFileParameter;

    .line 69
    new-instance v0, Lcom/policydm/db/XDB$XDMFileParameter;

    const-string v1, "FileObjectData"

    const/16 v2, 0x101

    invoke-direct {v0, v1, v6, v6, v2}, Lcom/policydm/db/XDB$XDMFileParameter;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/policydm/db/XDB$XDMFileParameter;->FileObjectData:Lcom/policydm/db/XDB$XDMFileParameter;

    .line 70
    new-instance v0, Lcom/policydm/db/XDB$XDMFileParameter;

    const-string v1, "FileFirmwareData"

    const/16 v2, 0x102

    invoke-direct {v0, v1, v7, v7, v2}, Lcom/policydm/db/XDB$XDMFileParameter;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/policydm/db/XDB$XDMFileParameter;->FileFirmwareData:Lcom/policydm/db/XDB$XDMFileParameter;

    .line 71
    new-instance v0, Lcom/policydm/db/XDB$XDMFileParameter;

    const-string v1, "FileLargeObjectData"

    const/16 v2, 0x103

    invoke-direct {v0, v1, v8, v8, v2}, Lcom/policydm/db/XDB$XDMFileParameter;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/policydm/db/XDB$XDMFileParameter;->FileLargeObjectData:Lcom/policydm/db/XDB$XDMFileParameter;

    .line 72
    new-instance v0, Lcom/policydm/db/XDB$XDMFileParameter;

    const-string v1, "FileTndsXmlData"

    const/16 v2, 0x104

    invoke-direct {v0, v1, v9, v9, v2}, Lcom/policydm/db/XDB$XDMFileParameter;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/policydm/db/XDB$XDMFileParameter;->FileTndsXmlData:Lcom/policydm/db/XDB$XDMFileParameter;

    .line 73
    new-instance v0, Lcom/policydm/db/XDB$XDMFileParameter;

    const-string v1, "FileMax"

    const/4 v2, 0x5

    const/4 v3, 0x5

    const/16 v4, 0x105

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/policydm/db/XDB$XDMFileParameter;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/policydm/db/XDB$XDMFileParameter;->FileMax:Lcom/policydm/db/XDB$XDMFileParameter;

    .line 66
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/policydm/db/XDB$XDMFileParameter;

    sget-object v1, Lcom/policydm/db/XDB$XDMFileParameter;->FileObjectTreeInfo:Lcom/policydm/db/XDB$XDMFileParameter;

    aput-object v1, v0, v5

    sget-object v1, Lcom/policydm/db/XDB$XDMFileParameter;->FileObjectData:Lcom/policydm/db/XDB$XDMFileParameter;

    aput-object v1, v0, v6

    sget-object v1, Lcom/policydm/db/XDB$XDMFileParameter;->FileFirmwareData:Lcom/policydm/db/XDB$XDMFileParameter;

    aput-object v1, v0, v7

    sget-object v1, Lcom/policydm/db/XDB$XDMFileParameter;->FileLargeObjectData:Lcom/policydm/db/XDB$XDMFileParameter;

    aput-object v1, v0, v8

    sget-object v1, Lcom/policydm/db/XDB$XDMFileParameter;->FileTndsXmlData:Lcom/policydm/db/XDB$XDMFileParameter;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/policydm/db/XDB$XDMFileParameter;->FileMax:Lcom/policydm/db/XDB$XDMFileParameter;

    aput-object v2, v0, v1

    sput-object v0, Lcom/policydm/db/XDB$XDMFileParameter;->$VALUES:[Lcom/policydm/db/XDB$XDMFileParameter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "nIndex"    # I
    .param p4, "nFileId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 79
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 80
    iput p3, p0, Lcom/policydm/db/XDB$XDMFileParameter;->nIndex:I

    .line 81
    iput p4, p0, Lcom/policydm/db/XDB$XDMFileParameter;->nFileId:I

    .line 82
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/policydm/db/XDB$XDMFileParameter;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 66
    const-class v0, Lcom/policydm/db/XDB$XDMFileParameter;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/policydm/db/XDB$XDMFileParameter;

    return-object v0
.end method

.method public static values()[Lcom/policydm/db/XDB$XDMFileParameter;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/policydm/db/XDB$XDMFileParameter;->$VALUES:[Lcom/policydm/db/XDB$XDMFileParameter;

    invoke-virtual {v0}, [Lcom/policydm/db/XDB$XDMFileParameter;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/policydm/db/XDB$XDMFileParameter;

    return-object v0
.end method


# virtual methods
.method FileId()I
    .locals 1

    .prologue
    .line 91
    iget v0, p0, Lcom/policydm/db/XDB$XDMFileParameter;->nFileId:I

    return v0
.end method

.method Index()I
    .locals 1

    .prologue
    .line 86
    iget v0, p0, Lcom/policydm/db/XDB$XDMFileParameter;->nIndex:I

    return v0
.end method
