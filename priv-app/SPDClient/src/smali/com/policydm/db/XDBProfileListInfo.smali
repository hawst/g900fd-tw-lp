.class public Lcom/policydm/db/XDBProfileListInfo;
.super Ljava/lang/Object;
.source "XDBProfileListInfo.java"

# interfaces
.implements Lcom/policydm/interfaces/XDMInterface;
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public MagicNumber:I

.field public NotiResumeState:Lcom/policydm/db/XDBSessionSaveInfo;

.field public ProfileName:[Ljava/lang/String;

.field public Profileindex:I

.field public bSkipDevDiscovery:Z

.field public m_szNetworkConnName:Ljava/lang/String;

.field public m_szSessionID:Ljava/lang/String;

.field public nNotiEvent:I

.field public nNotiJobId:J

.field public nNotiOpMode:I

.field public nNotiReSyncMode:I

.field public nProxyIndex:I

.field public tUicResultKeep:Lcom/policydm/db/XDBUICResultKeepInfo;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/db/XDBProfileListInfo;->m_szNetworkConnName:Ljava/lang/String;

    .line 28
    iput v1, p0, Lcom/policydm/db/XDBProfileListInfo;->nProxyIndex:I

    .line 29
    iput v1, p0, Lcom/policydm/db/XDBProfileListInfo;->Profileindex:I

    .line 30
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/policydm/db/XDBProfileListInfo;->ProfileName:[Ljava/lang/String;

    .line 31
    iput-boolean v1, p0, Lcom/policydm/db/XDBProfileListInfo;->bSkipDevDiscovery:Z

    .line 32
    new-instance v0, Lcom/policydm/db/XDBSessionSaveInfo;

    invoke-direct {v0}, Lcom/policydm/db/XDBSessionSaveInfo;-><init>()V

    iput-object v0, p0, Lcom/policydm/db/XDBProfileListInfo;->NotiResumeState:Lcom/policydm/db/XDBSessionSaveInfo;

    .line 33
    new-instance v0, Lcom/policydm/db/XDBUICResultKeepInfo;

    invoke-direct {v0}, Lcom/policydm/db/XDBUICResultKeepInfo;-><init>()V

    iput-object v0, p0, Lcom/policydm/db/XDBProfileListInfo;->tUicResultKeep:Lcom/policydm/db/XDBUICResultKeepInfo;

    .line 34
    return-void
.end method
