.class public Lcom/policydm/db/XDBFumoInfo;
.super Ljava/lang/Object;
.source "XDBFumoInfo.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public m_nDownloadMode:Z

.field public m_nInitiatedType:I

.field public m_nObjectDownloadPort:I

.field public m_nPkgSize:I

.field public m_nServerPort:I

.field public m_nStatus:I

.field public m_nStatusNotifyPort:I

.field public m_szCorrelator:Ljava/lang/String;

.field public m_szDownloadResultCode:Ljava/lang/String;

.field public m_szObjectDownloadIP:Ljava/lang/String;

.field public m_szObjectDownloadProtocol:Ljava/lang/String;

.field public m_szObjectDownloadUrl:Ljava/lang/String;

.field public m_szPkgDesc:Ljava/lang/String;

.field public m_szPkgName:Ljava/lang/String;

.field public m_szPkgVer:Ljava/lang/String;

.field public m_szProcessId:Ljava/lang/String;

.field public m_szProtocol:Ljava/lang/String;

.field public m_szReportUrl:Ljava/lang/String;

.field public m_szResultCode:Ljava/lang/String;

.field public m_szServerIP:Ljava/lang/String;

.field public m_szServerUrl:Ljava/lang/String;

.field public m_szStatusNotifyIP:Ljava/lang/String;

.field public m_szStatusNotifyProtocol:Ljava/lang/String;

.field public m_szStatusNotifyUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/db/XDBFumoInfo;->m_szProtocol:Ljava/lang/String;

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/db/XDBFumoInfo;->m_szServerUrl:Ljava/lang/String;

    .line 40
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/db/XDBFumoInfo;->m_szServerIP:Ljava/lang/String;

    .line 41
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/db/XDBFumoInfo;->m_szObjectDownloadProtocol:Ljava/lang/String;

    .line 42
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/db/XDBFumoInfo;->m_szObjectDownloadUrl:Ljava/lang/String;

    .line 43
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/db/XDBFumoInfo;->m_szObjectDownloadIP:Ljava/lang/String;

    .line 44
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/db/XDBFumoInfo;->m_szStatusNotifyProtocol:Ljava/lang/String;

    .line 45
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/db/XDBFumoInfo;->m_szStatusNotifyUrl:Ljava/lang/String;

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/db/XDBFumoInfo;->m_szStatusNotifyIP:Ljava/lang/String;

    .line 48
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/db/XDBFumoInfo;->m_szPkgName:Ljava/lang/String;

    .line 49
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/db/XDBFumoInfo;->m_szPkgVer:Ljava/lang/String;

    .line 50
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/db/XDBFumoInfo;->m_szPkgDesc:Ljava/lang/String;

    .line 51
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/db/XDBFumoInfo;->m_szProcessId:Ljava/lang/String;

    .line 53
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/policydm/db/XDBFumoInfo;->m_nDownloadMode:Z

    .line 54
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/db/XDBFumoInfo;->m_szCorrelator:Ljava/lang/String;

    .line 55
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/db/XDBFumoInfo;->m_szResultCode:Ljava/lang/String;

    .line 56
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/db/XDBFumoInfo;->m_szDownloadResultCode:Ljava/lang/String;

    .line 58
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/db/XDBFumoInfo;->m_szReportUrl:Ljava/lang/String;

    .line 59
    return-void
.end method
