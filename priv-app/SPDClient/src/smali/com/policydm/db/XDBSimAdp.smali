.class public Lcom/policydm/db/XDBSimAdp;
.super Ljava/lang/Object;
.source "XDBSimAdp.java"

# interfaces
.implements Lcom/policydm/interfaces/XDBInterface;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xdbGetSimIMSI()Lcom/policydm/db/XDBSimInfo;
    .locals 4

    .prologue
    .line 10
    const/4 v2, 0x0

    .line 13
    .local v2, "pIMSI":Lcom/policydm/db/XDBSimInfo;
    const/16 v3, 0x78

    :try_start_0
    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/policydm/db/XDBSimInfo;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 19
    :goto_0
    return-object v2

    .line 15
    :catch_0
    move-exception v1

    .line 17
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetSimIMSI(Lcom/policydm/db/XDBSimInfo;)V
    .locals 2
    .param p0, "pIMSI"    # Lcom/policydm/db/XDBSimInfo;

    .prologue
    .line 24
    if-nez p0, :cond_0

    .line 26
    const-string v1, "pIMSI is NULL"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 38
    :goto_0
    return-void

    .line 32
    :cond_0
    const/16 v1, 0x78

    :try_start_0
    invoke-static {v1, p0}, Lcom/policydm/db/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 34
    :catch_0
    move-exception v0

    .line 36
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method
