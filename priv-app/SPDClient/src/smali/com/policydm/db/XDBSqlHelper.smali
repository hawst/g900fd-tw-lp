.class public Lcom/policydm/db/XDBSqlHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "XDBSqlHelper.java"

# interfaces
.implements Lcom/policydm/db/XDBSql;
.implements Lcom/policydm/interfaces/XSPDInterface;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 18
    const-string v0, "policydmdb.db"

    const/4 v1, 0x0

    const/16 v2, 0xa

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 19
    return-void
.end method

.method private xdmDBCreateReportURL(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 6
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 134
    const-string v0, "fumo"

    const-string v1, "reporturl"

    invoke-direct {p0, p1, v0, v1}, Lcom/policydm/db/XDBSqlHelper;->xdmDbExistCheckColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    const-string v0, "Exec"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 137
    const-string v2, "fumo"

    const-string v3, "reporturl"

    const-string v4, "String"

    const-string v5, " "

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/policydm/db/XDBSqlHelper;->xdmDbAddColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    :goto_0
    return-void

    .line 141
    :cond_0
    const-string v0, "Nothing"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private xdmDBOperateFirstUpdate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 97
    const-string v0, "spd"

    const-string v1, "policymode"

    invoke-direct {p0, p1, v0, v1}, Lcom/policydm/db/XDBSqlHelper;->xdmDbExistCheckColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    const-string v0, "Exec"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 101
    invoke-static {p1}, Lcom/policydm/db/XDBSqlHelper;->xdmDbDropAllTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 102
    invoke-direct {p0, p1}, Lcom/policydm/db/XDBSqlHelper;->xdmDbCreateAllTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 103
    invoke-static {}, Lcom/policydm/db/XDB;->xdbFullResetFFS()V

    .line 109
    :goto_0
    return-void

    .line 107
    :cond_0
    const-string v0, "Nothing"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private xdmDBOperateSecondUpdate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 113
    invoke-direct {p0, p1}, Lcom/policydm/db/XDBSqlHelper;->xdmDbCheckDeviceID(Landroid/database/sqlite/SQLiteDatabase;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 115
    const-string v0, "Nothing"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 117
    :cond_0
    return-void
.end method

.method private xdmDBOperateThirdUpdate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 6
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 121
    const-string v0, "spd"

    const-string v1, "eulatime"

    invoke-direct {p0, p1, v0, v1}, Lcom/policydm/db/XDBSqlHelper;->xdmDbExistCheckColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 123
    const-string v0, "Exec"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 124
    const-string v2, "spd"

    const-string v3, "eulatime"

    const-string v4, "integer"

    const-string v5, ""

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/policydm/db/XDBSqlHelper;->xdmDbAddColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    :goto_0
    return-void

    .line 128
    :cond_0
    const-string v0, "Nothing"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private xdmDbAddColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "szTabelName"    # Ljava/lang/String;
    .param p3, "szColumn"    # Ljava/lang/String;
    .param p4, "szType"    # Ljava/lang/String;
    .param p5, "szDefaultValue"    # Ljava/lang/String;

    .prologue
    .line 278
    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ALTER TABLE "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ADD "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 279
    .local v2, "szSql":Ljava/lang/String;
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 281
    .local v1, "initialValues":Landroid/content/ContentValues;
    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 282
    const-string v3, ""

    invoke-virtual {v1, p3, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    :goto_0
    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 287
    const-string v3, "rowid=1"

    const/4 v4, 0x0

    invoke-virtual {p1, p2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 288
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Database Add Column : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " / "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " / "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 294
    .end local v1    # "initialValues":Landroid/content/ContentValues;
    .end local v2    # "szSql":Ljava/lang/String;
    :goto_1
    return-void

    .line 284
    .restart local v1    # "initialValues":Landroid/content/ContentValues;
    .restart local v2    # "szSql":Ljava/lang/String;
    :cond_0
    invoke-virtual {v1, p3, p5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 290
    .end local v1    # "initialValues":Landroid/content/ContentValues;
    .end local v2    # "szSql":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 292
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private xdmDbCheckDeviceID(Landroid/database/sqlite/SQLiteDatabase;)Z
    .locals 12
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v11, 0x1

    const/4 v8, 0x0

    .line 174
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 175
    .local v1, "bRet":Ljava/lang/Boolean;
    invoke-static {}, Lcom/policydm/adapter/XDMDevinfAdapter;->xdmDevAdpGetFullDeviceID()Ljava/lang/String;

    move-result-object v4

    .line 176
    .local v4, "szDeviceid":Ljava/lang/String;
    const-string v8, "profile"

    const-string v9, "username"

    invoke-direct {p0, p1, v8, v9, v11}, Lcom/policydm/db/XDBSqlHelper;->xdmDbGetEncryptorData(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v7

    .line 178
    .local v7, "szUsername":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "szDeviceid : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 179
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "szUsername : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 181
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 183
    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 185
    const-string v5, ""

    .line 186
    .local v5, "szPassword":Ljava/lang/String;
    const-string v8, "DeviceID is Not equals"

    invoke-static {v8}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 189
    const/4 v3, 0x1

    .local v3, "i":I
    :goto_0
    const/4 v8, 0x3

    if-gt v3, v8, :cond_1

    .line 191
    const-string v8, "profile"

    const-string v9, "serverid"

    invoke-direct {p0, p1, v8, v9, v3}, Lcom/policydm/db/XDBSqlHelper;->xdmDbGetEncryptorData(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v6

    .line 192
    .local v6, "szServerid":Ljava/lang/String;
    invoke-static {v4, v6}, Lcom/policydm/db/XDBFactoryBootstrapadapter;->xdbFBAdpSPDGenerateDevicePwd(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 193
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 195
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 198
    .local v0, "args":Landroid/content/ContentValues;
    :try_start_0
    const-string v8, "username"

    invoke-static {}, Lcom/policydm/db/XDBAESCrypt;->xdbGetCryptionkey()Ljava/lang/String;

    move-result-object v9

    invoke-static {v4, v9}, Lcom/policydm/db/XDBAESCrypt;->xdbEncryptor(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 199
    const-string v8, "password"

    invoke-static {}, Lcom/policydm/db/XDBAESCrypt;->xdbGetCryptionkey()Ljava/lang/String;

    move-result-object v9

    invoke-static {v5, v9}, Lcom/policydm/db/XDBAESCrypt;->xdbEncryptor(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 201
    const-string v8, "profile"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "rowid="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {p1, v8, v0, v9, v10}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 213
    .end local v0    # "args":Landroid/content/ContentValues;
    :goto_1
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "szServerid : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 214
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "szPassword : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 189
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 203
    .restart local v0    # "args":Landroid/content/ContentValues;
    :catch_0
    move-exception v2

    .line 205
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_1

    .line 210
    .end local v0    # "args":Landroid/content/ContentValues;
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_0
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Password is null, Index : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 218
    .end local v6    # "szServerid":Ljava/lang/String;
    :cond_1
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 221
    .restart local v0    # "args":Landroid/content/ContentValues;
    :try_start_1
    const-string v8, "deviceregister"

    const/4 v9, 0x2

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 222
    const-string v8, "devicecreate"

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 223
    const-string v8, "deviceupdate"

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 225
    const-string v8, "spd"

    const-string v9, "rowid=1"

    const/4 v10, 0x0

    invoke-virtual {p1, v8, v0, v9, v10}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 231
    :goto_2
    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 238
    .end local v0    # "args":Landroid/content/ContentValues;
    .end local v3    # "i":I
    .end local v5    # "szPassword":Ljava/lang/String;
    :cond_2
    :goto_3
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    return v8

    .line 227
    .restart local v0    # "args":Landroid/content/ContentValues;
    .restart local v3    # "i":I
    .restart local v5    # "szPassword":Ljava/lang/String;
    :catch_1
    move-exception v2

    .line 229
    .restart local v2    # "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_2

    .line 235
    .end local v0    # "args":Landroid/content/ContentValues;
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v3    # "i":I
    .end local v5    # "szPassword":Ljava/lang/String;
    :cond_3
    const-string v8, "szDeviceid and szUsername equals"

    invoke-static {v8}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_3
.end method

.method private xdmDbCreateAllTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 48
    const-string v1, ""

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 52
    :try_start_0
    const-string v1, "create table if not exists profile (rowid integer primary key autoincrement, protocol text, serverurl text, serverip text, serverpath text, serverport integer, protocol_org text, serverurl_org text, serverip_org text, serverpath_org text, serverport_org integer, changedprotocol integer, appid text, authlevel text, serverauthlevel text, authtype integer, serverauthtype integer, username byte, password byte, serverid byte, serverpwd byte, clientnonce text, servernonce text, servernonceformat integer, clientnonceformat integer, profilename text, networkconnname text, prefconref text, magicnumber integer);"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 53
    const-string v1, "create table if not exists network (rowid integer primary key autoincrement, homeurl text, service integer, active integer, proxyuse integer, napnetworkprofilename text, napbearer integer, napaddrtype integer, napaddr text, nappapid text, nappapsecret text, pxportnbr integer, pxaddrtype integer, pxaddr text, pxpapid text, pxpapsecret text, staticipuse integer, staticip text, staticdnsuse integer, staticdns1 integer, staticdns2 integer, trafficclass integer);"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 54
    const-string v1, "create table if not exists profilelist (rowid integer primary key autoincrement, networkconnname text, proxyindex integer, profileindex integer, profilename1 text, profilename2 text, profilename3 text, sessionid text, notievent integer, notiopmode integer, notijobid integer, notiresyncmode integer, skipdevdiscovery integer, magicnumber integer, sessionsavestate integer, notiuievent integer, notiretrycount integer, status integer, appid integer, uictype integer, result integer, number integer, text text, len integer, size integer);"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 55
    const-string v1, "create table if not exists fumo (rowid integer primary key autoincrement, protocol integer, serverurl text, serverip text, serverport integer, objectdownloadprotocol text, objectdownloadurl text, objectdownloadip text, objectdownloadport integer, statusnotifyprotocol text, statusnotifyurl text, statusnotifyip text, statusnotifyport integer, pkgname text, pkgversion text, pkgdesc text, processid text, pkgsize integer, status integer, downloadmode integer, correlator text, resultcode text, downloadResultcode text, initiatedtype integer, reporturl text);"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 56
    const-string v1, "create table if not exists siminfo (rowid integer primary key autoincrement, imsi text, imsi2 text);"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 57
    const-string v1, "create table if not exists polling (rowid integer primary key autoincrement, orgversionfileurl text, versionfileurl text, filename text, periodunit text, period integer, time integer, range integer, reportperiod integer, reporttime integer, reportrange integer, nextpollingtime integer, nextreporttime integer);"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 59
    const-string v1, "create table if not exists accxlistnode (rowid integer primary key autoincrement, account text, appaddr text, appaddrport text, clientappauth text, serverappauth text, toconref text);"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 60
    const-string v1, "create table if not exists resyncmode (rowid integer primary key autoincrement, nonceresyncmode integer);"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 62
    const-string v1, "create table if not exists dmagent (rowid integer primary key autoincrement, agenttype integer);"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 63
    const-string v1, "create table if not exists notification (rowid integer primary key autoincrement, appId integer, uiMode integer, sessinoId text, serverId text, opmode integer, jobId integer);"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 64
    const-string v1, "create table if not exists spd (rowid integer primary key autoincrement, deviceregister integer, devicecreate integer, deviceupdate integer, pushtype text, pushregid text, status integer, servernonce text, policymode text, eulatime integer);"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 70
    :goto_0
    return-void

    .line 66
    :catch_0
    move-exception v0

    .line 68
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdmDbDropAllTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 74
    const-string v1, ""

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 77
    :try_start_0
    const-string v1, "DROP TABLE IF EXISTS profile"

    invoke-virtual {p0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 78
    const-string v1, "DROP TABLE IF EXISTS network"

    invoke-virtual {p0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 79
    const-string v1, "DROP TABLE IF EXISTS profilelist"

    invoke-virtual {p0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 80
    const-string v1, "DROP TABLE IF EXISTS fumo"

    invoke-virtual {p0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 81
    const-string v1, "DROP TABLE IF EXISTS siminfo"

    invoke-virtual {p0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 82
    const-string v1, "DROP TABLE IF EXISTS polling"

    invoke-virtual {p0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 83
    const-string v1, "DROP TABLE IF EXISTS accxlistnode"

    invoke-virtual {p0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 84
    const-string v1, "DROP TABLE IF EXISTS resyncmode"

    invoke-virtual {p0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 85
    const-string v1, "DROP TABLE IF EXISTS dmagent"

    invoke-virtual {p0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 86
    const-string v1, "DROP TABLE IF EXISTS notification"

    invoke-virtual {p0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 87
    const-string v1, "DROP TABLE IF EXISTS spd"

    invoke-virtual {p0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 93
    :goto_0
    return-void

    .line 89
    :catch_0
    move-exception v0

    .line 91
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private xdmDbExistCheckColumn(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "szTableName"    # Ljava/lang/String;
    .param p3, "szColumn"    # Ljava/lang/String;

    .prologue
    .line 147
    const/4 v0, 0x1

    .line 148
    .local v0, "bUpdate":Z
    const/4 v1, 0x0

    .line 152
    .local v1, "cursor":Landroid/database/Cursor;
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "select "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " from "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 153
    .local v3, "sql":Ljava/lang/String;
    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 155
    invoke-interface {v1}, Landroid/database/Cursor;->getColumnCount()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-lez v4, :cond_0

    .line 157
    const/4 v0, 0x0

    .line 166
    :cond_0
    if-eqz v1, :cond_1

    .line 167
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 169
    .end local v3    # "sql":Ljava/lang/String;
    :cond_1
    :goto_0
    return v0

    .line 160
    :catch_0
    move-exception v2

    .line 162
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 166
    if-eqz v1, :cond_1

    .line 167
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 166
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    if-eqz v1, :cond_2

    .line 167
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v4
.end method

.method private xdmDbGetEncryptorData(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .locals 14
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "szTableName"    # Ljava/lang/String;
    .param p3, "szColumn"    # Ljava/lang/String;
    .param p4, "rowid"    # I

    .prologue
    .line 243
    const-string v13, ""

    .line 245
    .local v13, "szValue":Ljava/lang/String;
    const/4 v11, 0x0

    .line 246
    .local v11, "cursor":Landroid/database/Cursor;
    const/4 v1, 0x1

    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p3, v4, v1

    .line 251
    .local v4, "FROM":[Ljava/lang/String;
    const/4 v2, 0x1

    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "rowid="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move/from16 v0, p4

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v1, p1

    move-object/from16 v3, p2

    invoke-virtual/range {v1 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 253
    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 255
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 257
    const/4 v1, 0x0

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    invoke-static {}, Lcom/policydm/db/XDBAESCrypt;->xdbGetCryptionkey()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/policydm/db/XDBAESCrypt;->xdbDecryptor([BLjava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v13

    .line 267
    :cond_0
    if-eqz v11, :cond_1

    .line 268
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 271
    :cond_1
    :goto_0
    return-object v13

    .line 261
    :catch_0
    move-exception v12

    .line 263
    .local v12, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v12}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 267
    if-eqz v11, :cond_1

    .line 268
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 267
    .end local v12    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    if-eqz v11, :cond_2

    .line 268
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v1
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 24
    const-string v0, ""

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 27
    invoke-direct {p0, p1}, Lcom/policydm/db/XDBSqlHelper;->xdmDbCreateAllTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 28
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    const/16 v3, 0xa

    const/16 v2, 0x9

    .line 33
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "oldVersion : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", newVersion : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 35
    if-ge p2, v2, :cond_0

    if-lt p3, v2, :cond_0

    .line 37
    invoke-static {p1}, Lcom/policydm/db/XDBSqlHelper;->xdmDbDropAllTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 38
    invoke-direct {p0, p1}, Lcom/policydm/db/XDBSqlHelper;->xdmDbCreateAllTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 39
    invoke-static {}, Lcom/policydm/db/XDB;->xdbFullResetFFS()V

    .line 42
    :cond_0
    if-ge p2, v3, :cond_1

    if-lt p3, v3, :cond_1

    .line 43
    invoke-direct {p0, p1}, Lcom/policydm/db/XDBSqlHelper;->xdmDBCreateReportURL(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 44
    :cond_1
    return-void
.end method
