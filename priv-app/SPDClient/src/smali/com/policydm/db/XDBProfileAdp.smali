.class public Lcom/policydm/db/XDBProfileAdp;
.super Ljava/lang/Object;
.source "XDBProfileAdp.java"

# interfaces
.implements Lcom/policydm/interfaces/XDBInterface;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xdbGetAuthLevel()Ljava/lang/String;
    .locals 4

    .prologue
    .line 126
    const-string v2, ""

    .line 129
    .local v2, "szAuthLevel":Ljava/lang/String;
    const/16 v3, 0x3b

    :try_start_0
    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 135
    :goto_0
    return-object v2

    .line 131
    :catch_0
    move-exception v1

    .line 133
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetAuthType()I
    .locals 3

    .prologue
    .line 154
    const/4 v0, 0x0

    .line 157
    .local v0, "authType":I
    const/16 v2, 0x3d

    :try_start_0
    invoke-static {v2}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 163
    :goto_0
    return v0

    .line 159
    :catch_0
    move-exception v1

    .line 161
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetChangedProtocol()Z
    .locals 3

    .prologue
    .line 220
    const/4 v0, 0x0

    .line 223
    .local v0, "bChanged":Z
    const/16 v2, 0x39

    :try_start_0
    invoke-static {v2}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 229
    :goto_0
    return v0

    .line 225
    :catch_0
    move-exception v1

    .line 227
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetClientNonce()Ljava/lang/String;
    .locals 4

    .prologue
    .line 290
    const-string v2, ""

    .line 293
    .local v2, "szNonce":Ljava/lang/String;
    const/16 v3, 0x43

    :try_start_0
    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 299
    :goto_0
    return-object v2

    .line 295
    :catch_0
    move-exception v1

    .line 297
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetClientPassword()Ljava/lang/String;
    .locals 4

    .prologue
    .line 248
    const-string v2, ""

    .line 251
    .local v2, "szClientPassword":Ljava/lang/String;
    const/16 v3, 0x40

    :try_start_0
    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 257
    :goto_0
    return-object v2

    .line 253
    :catch_0
    move-exception v1

    .line 255
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetConRef()Lcom/policydm/db/XDBInfoConRef;
    .locals 4

    .prologue
    .line 372
    new-instance v1, Lcom/policydm/db/XDBInfoConRef;

    invoke-direct {v1}, Lcom/policydm/db/XDBInfoConRef;-><init>()V

    .line 375
    .local v1, "Conref":Lcom/policydm/db/XDBInfoConRef;
    const/16 v3, 0x47

    :try_start_0
    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/policydm/db/XDBInfoConRef;

    move-object v1, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 381
    :goto_0
    return-object v1

    .line 377
    :catch_0
    move-exception v2

    .line 379
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetPrefConRef()Ljava/lang/String;
    .locals 4

    .prologue
    .line 358
    const-string v2, ""

    .line 361
    .local v2, "szPrefConRef":Ljava/lang/String;
    const/16 v3, 0x46

    :try_start_0
    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 367
    :goto_0
    return-object v2

    .line 363
    :catch_0
    move-exception v1

    .line 365
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetProfileInfo()Lcom/policydm/db/XDBProfileInfo;
    .locals 4

    .prologue
    .line 10
    new-instance v2, Lcom/policydm/db/XDBProfileInfo;

    invoke-direct {v2}, Lcom/policydm/db/XDBProfileInfo;-><init>()V

    .line 13
    .local v2, "ptProfileInfo":Lcom/policydm/db/XDBProfileInfo;
    const/16 v3, 0x32

    :try_start_0
    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/policydm/db/XDBProfileInfo;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 19
    :goto_0
    return-object v2

    .line 15
    :catch_0
    move-exception v1

    .line 17
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetProfileName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 344
    const-string v2, "server1"

    .line 347
    .local v2, "szProfileName":Ljava/lang/String;
    const/16 v3, 0x45

    :try_start_0
    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 353
    :goto_0
    return-object v2

    .line 349
    :catch_0
    move-exception v1

    .line 351
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetServerAuthLevel()Ljava/lang/String;
    .locals 4

    .prologue
    .line 140
    const-string v2, ""

    .line 143
    .local v2, "szAuthLevel":Ljava/lang/String;
    const/16 v3, 0x3c

    :try_start_0
    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 149
    :goto_0
    return-object v2

    .line 145
    :catch_0
    move-exception v1

    .line 147
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetServerAuthType()I
    .locals 3

    .prologue
    .line 180
    const/4 v0, 0x0

    .line 183
    .local v0, "authType":I
    const/16 v2, 0x3e

    :try_start_0
    invoke-static {v2}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 189
    :goto_0
    return v0

    .line 185
    :catch_0
    move-exception v1

    .line 187
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetServerID()Ljava/lang/String;
    .locals 4

    .prologue
    .line 262
    const-string v2, ""

    .line 265
    .local v2, "szSeverid":Ljava/lang/String;
    const/16 v3, 0x41

    :try_start_0
    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 271
    :goto_0
    return-object v2

    .line 267
    :catch_0
    move-exception v1

    .line 269
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetServerNonce()Ljava/lang/String;
    .locals 4

    .prologue
    .line 317
    const-string v2, ""

    .line 320
    .local v2, "szNonce":Ljava/lang/String;
    const/16 v3, 0x44

    :try_start_0
    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 326
    :goto_0
    return-object v2

    .line 322
    :catch_0
    move-exception v1

    .line 324
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetServerPassword()Ljava/lang/String;
    .locals 4

    .prologue
    .line 276
    const-string v2, ""

    .line 279
    .local v2, "szServerPassword":Ljava/lang/String;
    const/16 v3, 0x42

    :try_start_0
    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 285
    :goto_0
    return-object v2

    .line 281
    :catch_0
    move-exception v1

    .line 283
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetServerPath()Ljava/lang/String;
    .locals 4

    .prologue
    .line 100
    const-string v2, ""

    .line 103
    .local v2, "szServerPath":Ljava/lang/String;
    const/16 v3, 0x37

    :try_start_0
    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 109
    :goto_0
    return-object v2

    .line 105
    :catch_0
    move-exception v1

    .line 107
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetUsername()Ljava/lang/String;
    .locals 4

    .prologue
    .line 234
    const-string v2, ""

    .line 237
    .local v2, "szUserName":Ljava/lang/String;
    const/16 v3, 0x3f

    :try_start_0
    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 243
    :goto_0
    return-object v2

    .line 239
    :catch_0
    move-exception v1

    .line 241
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetAuthType(I)V
    .locals 3
    .param p0, "authType"    # I

    .prologue
    .line 170
    const/16 v1, 0x3d

    :try_start_0
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/policydm/db/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 176
    :goto_0
    return-void

    .line 172
    :catch_0
    move-exception v0

    .line 174
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetChangedProtocol(Z)V
    .locals 3
    .param p0, "bChanged"    # Z

    .prologue
    .line 206
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "xdbSetChangedProtocol : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 210
    const/16 v1, 0x39

    :try_start_0
    invoke-static {p0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/policydm/db/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 216
    :goto_0
    return-void

    .line 212
    :catch_0
    move-exception v0

    .line 214
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetClientNonce(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "szNonce"    # Ljava/lang/String;

    .prologue
    .line 306
    const/16 v1, 0x43

    :try_start_0
    invoke-static {v1, p0}, Lcom/policydm/db/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 312
    :goto_0
    return-object p0

    .line 308
    :catch_0
    move-exception v0

    .line 310
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetConRef(Lcom/policydm/db/XDBInfoConRef;)V
    .locals 2
    .param p0, "ptConRef"    # Lcom/policydm/db/XDBInfoConRef;

    .prologue
    .line 388
    const/16 v1, 0x47

    :try_start_0
    invoke-static {v1, p0}, Lcom/policydm/db/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 394
    :goto_0
    return-void

    .line 390
    :catch_0
    move-exception v0

    .line 392
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetProfileInfo(Lcom/policydm/db/XDBProfileInfo;)Z
    .locals 3
    .param p0, "ptProfileInfo"    # Lcom/policydm/db/XDBProfileInfo;

    .prologue
    .line 24
    const/4 v1, 0x0

    .line 27
    .local v1, "nRet":Z
    const/16 v2, 0x32

    :try_start_0
    invoke-static {v2, p0}, Lcom/policydm/db/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 33
    :goto_0
    return v1

    .line 29
    :catch_0
    move-exception v0

    .line 31
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetProfileInfo(Lcom/policydm/db/XDBProfileInfo;I)Z
    .locals 3
    .param p0, "ptProfileInfo"    # Lcom/policydm/db/XDBProfileInfo;
    .param p1, "RowId"    # I

    .prologue
    .line 38
    const/4 v1, 0x0

    .line 41
    .local v1, "nRet":Z
    const/16 v2, 0x32

    :try_start_0
    invoke-static {v2, p1, p0}, Lcom/policydm/db/XDB;->xdbWrite(IILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 47
    :goto_0
    return v1

    .line 43
    :catch_0
    move-exception v0

    .line 45
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetServerAddress(Ljava/lang/String;)V
    .locals 2
    .param p0, "szAddress"    # Ljava/lang/String;

    .prologue
    .line 78
    const/16 v1, 0x36

    :try_start_0
    invoke-static {v1, p0}, Lcom/policydm/db/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    :goto_0
    return-void

    .line 80
    :catch_0
    move-exception v0

    .line 82
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetServerAuthType(I)V
    .locals 3
    .param p0, "authType"    # I

    .prologue
    .line 196
    const/16 v1, 0x3e

    :try_start_0
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/policydm/db/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 202
    :goto_0
    return-void

    .line 198
    :catch_0
    move-exception v0

    .line 200
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetServerNonce(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "szNonce"    # Ljava/lang/String;

    .prologue
    .line 333
    const/16 v1, 0x44

    :try_start_0
    invoke-static {v1, p0}, Lcom/policydm/db/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 339
    :goto_0
    return-object p0

    .line 335
    :catch_0
    move-exception v0

    .line 337
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetServerPath(Ljava/lang/String;)V
    .locals 2
    .param p0, "szServerPath"    # Ljava/lang/String;

    .prologue
    .line 90
    const/16 v1, 0x37

    :try_start_0
    invoke-static {v1, p0}, Lcom/policydm/db/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 96
    :goto_0
    return-void

    .line 92
    :catch_0
    move-exception v0

    .line 94
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetServerPort(I)V
    .locals 3
    .param p0, "nPort"    # I

    .prologue
    .line 116
    const/16 v1, 0x38

    :try_start_0
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/policydm/db/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 122
    :goto_0
    return-void

    .line 118
    :catch_0
    move-exception v0

    .line 120
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetServerProtocol(Ljava/lang/String;)V
    .locals 2
    .param p0, "szProtocol"    # Ljava/lang/String;

    .prologue
    .line 54
    const/16 v1, 0x34

    :try_start_0
    invoke-static {v1, p0}, Lcom/policydm/db/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 60
    :goto_0
    return-void

    .line 56
    :catch_0
    move-exception v0

    .line 58
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetServerUrl(Ljava/lang/String;)V
    .locals 2
    .param p0, "szServerUrl"    # Ljava/lang/String;

    .prologue
    .line 66
    const/16 v1, 0x35

    :try_start_0
    invoke-static {v1, p0}, Lcom/policydm/db/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 72
    :goto_0
    return-void

    .line 68
    :catch_0
    move-exception v0

    .line 70
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method
