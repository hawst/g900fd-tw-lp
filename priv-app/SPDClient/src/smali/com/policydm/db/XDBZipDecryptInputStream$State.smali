.class final enum Lcom/policydm/db/XDBZipDecryptInputStream$State;
.super Ljava/lang/Enum;
.source "XDBZipDecryptInputStream.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/policydm/db/XDBZipDecryptInputStream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/policydm/db/XDBZipDecryptInputStream$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/policydm/db/XDBZipDecryptInputStream$State;

.field public static final enum COMPRESSED_SIZE:Lcom/policydm/db/XDBZipDecryptInputStream$State;

.field public static final enum DATA:Lcom/policydm/db/XDBZipDecryptInputStream$State;

.field public static final enum EF_LENGTH:Lcom/policydm/db/XDBZipDecryptInputStream$State;

.field public static final enum FLAGS:Lcom/policydm/db/XDBZipDecryptInputStream$State;

.field public static final enum FN_LENGTH:Lcom/policydm/db/XDBZipDecryptInputStream$State;

.field public static final enum HEADER:Lcom/policydm/db/XDBZipDecryptInputStream$State;

.field public static final enum SIGNATURE:Lcom/policydm/db/XDBZipDecryptInputStream$State;

.field public static final enum TAIL:Lcom/policydm/db/XDBZipDecryptInputStream$State;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 44
    new-instance v0, Lcom/policydm/db/XDBZipDecryptInputStream$State;

    const-string v1, "SIGNATURE"

    invoke-direct {v0, v1, v3}, Lcom/policydm/db/XDBZipDecryptInputStream$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/policydm/db/XDBZipDecryptInputStream$State;->SIGNATURE:Lcom/policydm/db/XDBZipDecryptInputStream$State;

    .line 45
    new-instance v0, Lcom/policydm/db/XDBZipDecryptInputStream$State;

    const-string v1, "FLAGS"

    invoke-direct {v0, v1, v4}, Lcom/policydm/db/XDBZipDecryptInputStream$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/policydm/db/XDBZipDecryptInputStream$State;->FLAGS:Lcom/policydm/db/XDBZipDecryptInputStream$State;

    .line 46
    new-instance v0, Lcom/policydm/db/XDBZipDecryptInputStream$State;

    const-string v1, "COMPRESSED_SIZE"

    invoke-direct {v0, v1, v5}, Lcom/policydm/db/XDBZipDecryptInputStream$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/policydm/db/XDBZipDecryptInputStream$State;->COMPRESSED_SIZE:Lcom/policydm/db/XDBZipDecryptInputStream$State;

    .line 47
    new-instance v0, Lcom/policydm/db/XDBZipDecryptInputStream$State;

    const-string v1, "FN_LENGTH"

    invoke-direct {v0, v1, v6}, Lcom/policydm/db/XDBZipDecryptInputStream$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/policydm/db/XDBZipDecryptInputStream$State;->FN_LENGTH:Lcom/policydm/db/XDBZipDecryptInputStream$State;

    .line 48
    new-instance v0, Lcom/policydm/db/XDBZipDecryptInputStream$State;

    const-string v1, "EF_LENGTH"

    invoke-direct {v0, v1, v7}, Lcom/policydm/db/XDBZipDecryptInputStream$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/policydm/db/XDBZipDecryptInputStream$State;->EF_LENGTH:Lcom/policydm/db/XDBZipDecryptInputStream$State;

    .line 49
    new-instance v0, Lcom/policydm/db/XDBZipDecryptInputStream$State;

    const-string v1, "HEADER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/policydm/db/XDBZipDecryptInputStream$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/policydm/db/XDBZipDecryptInputStream$State;->HEADER:Lcom/policydm/db/XDBZipDecryptInputStream$State;

    .line 50
    new-instance v0, Lcom/policydm/db/XDBZipDecryptInputStream$State;

    const-string v1, "DATA"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/policydm/db/XDBZipDecryptInputStream$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/policydm/db/XDBZipDecryptInputStream$State;->DATA:Lcom/policydm/db/XDBZipDecryptInputStream$State;

    .line 51
    new-instance v0, Lcom/policydm/db/XDBZipDecryptInputStream$State;

    const-string v1, "TAIL"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/policydm/db/XDBZipDecryptInputStream$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/policydm/db/XDBZipDecryptInputStream$State;->TAIL:Lcom/policydm/db/XDBZipDecryptInputStream$State;

    .line 42
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/policydm/db/XDBZipDecryptInputStream$State;

    sget-object v1, Lcom/policydm/db/XDBZipDecryptInputStream$State;->SIGNATURE:Lcom/policydm/db/XDBZipDecryptInputStream$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/policydm/db/XDBZipDecryptInputStream$State;->FLAGS:Lcom/policydm/db/XDBZipDecryptInputStream$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/policydm/db/XDBZipDecryptInputStream$State;->COMPRESSED_SIZE:Lcom/policydm/db/XDBZipDecryptInputStream$State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/policydm/db/XDBZipDecryptInputStream$State;->FN_LENGTH:Lcom/policydm/db/XDBZipDecryptInputStream$State;

    aput-object v1, v0, v6

    sget-object v1, Lcom/policydm/db/XDBZipDecryptInputStream$State;->EF_LENGTH:Lcom/policydm/db/XDBZipDecryptInputStream$State;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/policydm/db/XDBZipDecryptInputStream$State;->HEADER:Lcom/policydm/db/XDBZipDecryptInputStream$State;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/policydm/db/XDBZipDecryptInputStream$State;->DATA:Lcom/policydm/db/XDBZipDecryptInputStream$State;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/policydm/db/XDBZipDecryptInputStream$State;->TAIL:Lcom/policydm/db/XDBZipDecryptInputStream$State;

    aput-object v2, v0, v1

    sput-object v0, Lcom/policydm/db/XDBZipDecryptInputStream$State;->$VALUES:[Lcom/policydm/db/XDBZipDecryptInputStream$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/policydm/db/XDBZipDecryptInputStream$State;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 42
    const-class v0, Lcom/policydm/db/XDBZipDecryptInputStream$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/policydm/db/XDBZipDecryptInputStream$State;

    return-object v0
.end method

.method public static values()[Lcom/policydm/db/XDBZipDecryptInputStream$State;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/policydm/db/XDBZipDecryptInputStream$State;->$VALUES:[Lcom/policydm/db/XDBZipDecryptInputStream$State;

    invoke-virtual {v0}, [Lcom/policydm/db/XDBZipDecryptInputStream$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/policydm/db/XDBZipDecryptInputStream$State;

    return-object v0
.end method
