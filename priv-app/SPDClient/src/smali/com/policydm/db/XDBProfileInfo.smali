.class public Lcom/policydm/db/XDBProfileInfo;
.super Ljava/lang/Object;
.source "XDBProfileInfo.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public AppID:Ljava/lang/String;

.field public AuthLevel:Ljava/lang/String;

.field public AuthType:I

.field public ClientNonce:Ljava/lang/String;

.field public ClientNonceFormat:I

.field public ConRef:Lcom/policydm/db/XDBInfoConRef;

.field public MagicNumber:I

.field public NetworkConnName:Ljava/lang/String;

.field public Password:Ljava/lang/String;

.field public PrefConRef:Ljava/lang/String;

.field public ProfileName:Ljava/lang/String;

.field public Protocol:Ljava/lang/String;

.field public Protocol_Org:Ljava/lang/String;

.field public ServerAuthLevel:Ljava/lang/String;

.field public ServerID:Ljava/lang/String;

.field public ServerIP:Ljava/lang/String;

.field public ServerIP_Org:Ljava/lang/String;

.field public ServerNonce:Ljava/lang/String;

.field public ServerNonceFormat:I

.field public ServerPath:Ljava/lang/String;

.field public ServerPath_Org:Ljava/lang/String;

.field public ServerPort:I

.field public ServerPort_Org:I

.field public ServerPwd:Ljava/lang/String;

.field public ServerUrl:Ljava/lang/String;

.field public ServerUrl_Org:Ljava/lang/String;

.field public UserName:Ljava/lang/String;

.field public bChangedProtocol:Z

.field public nServerAuthType:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/db/XDBProfileInfo;->Protocol:Ljava/lang/String;

    .line 47
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/db/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    .line 48
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/db/XDBProfileInfo;->ServerIP:Ljava/lang/String;

    .line 49
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/db/XDBProfileInfo;->ServerPath:Ljava/lang/String;

    .line 50
    const/16 v0, 0x50

    iput v0, p0, Lcom/policydm/db/XDBProfileInfo;->ServerPort:I

    .line 51
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/db/XDBProfileInfo;->Protocol_Org:Ljava/lang/String;

    .line 52
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/db/XDBProfileInfo;->ServerUrl_Org:Ljava/lang/String;

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/db/XDBProfileInfo;->ServerIP_Org:Ljava/lang/String;

    .line 54
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/db/XDBProfileInfo;->ServerPath_Org:Ljava/lang/String;

    .line 56
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/db/XDBProfileInfo;->AppID:Ljava/lang/String;

    .line 57
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/db/XDBProfileInfo;->AuthLevel:Ljava/lang/String;

    .line 58
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/db/XDBProfileInfo;->ServerAuthLevel:Ljava/lang/String;

    .line 59
    iput v1, p0, Lcom/policydm/db/XDBProfileInfo;->AuthType:I

    .line 60
    iput v1, p0, Lcom/policydm/db/XDBProfileInfo;->nServerAuthType:I

    .line 62
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/db/XDBProfileInfo;->UserName:Ljava/lang/String;

    .line 63
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/db/XDBProfileInfo;->Password:Ljava/lang/String;

    .line 64
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/db/XDBProfileInfo;->ServerID:Ljava/lang/String;

    .line 65
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/db/XDBProfileInfo;->ServerPwd:Ljava/lang/String;

    .line 67
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/db/XDBProfileInfo;->ClientNonce:Ljava/lang/String;

    .line 68
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/db/XDBProfileInfo;->ServerNonce:Ljava/lang/String;

    .line 69
    iput v1, p0, Lcom/policydm/db/XDBProfileInfo;->ServerNonceFormat:I

    .line 70
    iput v1, p0, Lcom/policydm/db/XDBProfileInfo;->ClientNonceFormat:I

    .line 72
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/db/XDBProfileInfo;->ProfileName:Ljava/lang/String;

    .line 73
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/db/XDBProfileInfo;->NetworkConnName:Ljava/lang/String;

    .line 74
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/db/XDBProfileInfo;->PrefConRef:Ljava/lang/String;

    .line 76
    new-instance v0, Lcom/policydm/db/XDBInfoConRef;

    invoke-direct {v0}, Lcom/policydm/db/XDBInfoConRef;-><init>()V

    iput-object v0, p0, Lcom/policydm/db/XDBProfileInfo;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    .line 77
    return-void
.end method
