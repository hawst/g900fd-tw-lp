.class public Lcom/policydm/db/XDBZipDecryptInputStream;
.super Ljava/io/InputStream;
.source "XDBZipDecryptInputStream.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/policydm/db/XDBZipDecryptInputStream$1;,
        Lcom/policydm/db/XDBZipDecryptInputStream$State;
    }
.end annotation


# static fields
.field private static final CRC_TABLE:[I

.field private static final DECRYPT_HEADER_SIZE:I = 0xc

.field private static final LFH_SIGNATURE:[I


# instance fields
.field private compressedSize:I

.field private final delegate:Ljava/io/InputStream;

.field private final keys:[I

.field private final password:Ljava/lang/String;

.field private skipBytes:I

.field private state:Lcom/policydm/db/XDBZipDecryptInputStream$State;

.field private value:I

.field private valueInc:I

.field private valuePos:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/16 v5, 0x100

    .line 8
    new-array v3, v5, [I

    sput-object v3, Lcom/policydm/db/XDBZipDecryptInputStream;->CRC_TABLE:[I

    .line 12
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v5, :cond_2

    .line 14
    move v2, v0

    .line 15
    .local v2, "r":I
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    const/16 v3, 0x8

    if-ge v1, v3, :cond_1

    .line 17
    and-int/lit8 v3, v2, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 19
    ushr-int/lit8 v3, v2, 0x1

    const v4, -0x12477ce0

    xor-int v2, v3, v4

    .line 15
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 23
    :cond_0
    ushr-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 26
    :cond_1
    sget-object v3, Lcom/policydm/db/XDBZipDecryptInputStream;->CRC_TABLE:[I

    aput v2, v3, v0

    .line 12
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 30
    .end local v1    # "j":I
    .end local v2    # "r":I
    :cond_2
    const/4 v3, 0x4

    new-array v3, v3, [I

    fill-array-data v3, :array_0

    sput-object v3, Lcom/policydm/db/XDBZipDecryptInputStream;->LFH_SIGNATURE:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x50
        0x4b
        0x3
        0x4
    .end array-data
.end method

.method public constructor <init>(Ljava/io/InputStream;Ljava/lang/String;)V
    .locals 1
    .param p1, "stream"    # Ljava/io/InputStream;
    .param p2, "password"    # Ljava/lang/String;

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 34
    const/4 v0, 0x3

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->keys:[I

    .line 35
    sget-object v0, Lcom/policydm/db/XDBZipDecryptInputStream$State;->SIGNATURE:Lcom/policydm/db/XDBZipDecryptInputStream$State;

    iput-object v0, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->state:Lcom/policydm/db/XDBZipDecryptInputStream$State;

    .line 56
    iput-object p1, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->delegate:Ljava/io/InputStream;

    .line 57
    iput-object p2, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->password:Ljava/lang/String;

    .line 58
    return-void
.end method

.method private crc32(IB)I
    .locals 3
    .param p1, "oldCrc"    # I
    .param p2, "charAt"    # B

    .prologue
    .line 209
    ushr-int/lit8 v0, p1, 0x8

    sget-object v1, Lcom/policydm/db/XDBZipDecryptInputStream;->CRC_TABLE:[I

    xor-int v2, p1, p2

    and-int/lit16 v2, v2, 0xff

    aget v1, v1, v2

    xor-int/2addr v0, v1

    return v0
.end method

.method private decryptByte()B
    .locals 3

    .prologue
    .line 203
    iget-object v1, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->keys:[I

    const/4 v2, 0x2

    aget v1, v1, v2

    or-int/lit8 v0, v1, 0x2

    .line 204
    .local v0, "temp":I
    xor-int/lit8 v1, v0, 0x1

    mul-int/2addr v1, v0

    ushr-int/lit8 v1, v1, 0x8

    int-to-byte v1, v1

    return v1
.end method

.method private initKeys(Ljava/lang/String;)V
    .locals 4
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    .line 184
    iget-object v1, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->keys:[I

    const/4 v2, 0x0

    const v3, 0x12345678

    aput v3, v1, v2

    .line 185
    iget-object v1, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->keys:[I

    const/4 v2, 0x1

    const v3, 0x23456789

    aput v3, v1, v2

    .line 186
    iget-object v1, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->keys:[I

    const/4 v2, 0x2

    const v3, 0x34567890

    aput v3, v1, v2

    .line 187
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 189
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    invoke-direct {p0, v1}, Lcom/policydm/db/XDBZipDecryptInputStream;->updateKeys(B)V

    .line 187
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 191
    :cond_0
    return-void
.end method

.method private updateKeys(B)V
    .locals 6
    .param p1, "charAt"    # B

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 195
    iget-object v0, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->keys:[I

    iget-object v1, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->keys:[I

    aget v1, v1, v4

    invoke-direct {p0, v1, p1}, Lcom/policydm/db/XDBZipDecryptInputStream;->crc32(IB)I

    move-result v1

    aput v1, v0, v4

    .line 196
    iget-object v0, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->keys:[I

    aget v1, v0, v3

    iget-object v2, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->keys:[I

    aget v2, v2, v4

    and-int/lit16 v2, v2, 0xff

    add-int/2addr v1, v2

    aput v1, v0, v3

    .line 197
    iget-object v0, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->keys:[I

    iget-object v1, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->keys:[I

    aget v1, v1, v3

    const v2, 0x8088405

    mul-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x1

    aput v1, v0, v3

    .line 198
    iget-object v0, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->keys:[I

    iget-object v1, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->keys:[I

    aget v1, v1, v5

    iget-object v2, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->keys:[I

    aget v2, v2, v3

    shr-int/lit8 v2, v2, 0x18

    int-to-byte v2, v2

    invoke-direct {p0, v1, v2}, Lcom/policydm/db/XDBZipDecryptInputStream;->crc32(IB)I

    move-result v1

    aput v1, v0, v5

    .line 199
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 178
    iget-object v0, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->delegate:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 179
    invoke-super {p0}, Ljava/io/InputStream;->close()V

    .line 180
    return-void
.end method

.method public read()I
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v6, 0xc

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 63
    iget-object v2, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->delegate:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->read()I

    move-result v1

    .line 64
    .local v1, "result":I
    iget v2, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->skipBytes:I

    if-nez v2, :cond_9

    .line 66
    sget-object v2, Lcom/policydm/db/XDBZipDecryptInputStream$1;->$SwitchMap$com$policydm$db$XDBZipDecryptInputStream$State:[I

    iget-object v3, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->state:Lcom/policydm/db/XDBZipDecryptInputStream$State;

    invoke-virtual {v3}, Lcom/policydm/db/XDBZipDecryptInputStream$State;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 172
    :cond_0
    :goto_0
    return v1

    .line 69
    :pswitch_0
    sget-object v2, Lcom/policydm/db/XDBZipDecryptInputStream;->LFH_SIGNATURE:[I

    iget v3, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->valuePos:I

    aget v2, v2, v3

    if-eq v1, v2, :cond_1

    .line 71
    sget-object v2, Lcom/policydm/db/XDBZipDecryptInputStream$State;->TAIL:Lcom/policydm/db/XDBZipDecryptInputStream$State;

    iput-object v2, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->state:Lcom/policydm/db/XDBZipDecryptInputStream$State;

    goto :goto_0

    .line 75
    :cond_1
    iget v2, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->valuePos:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->valuePos:I

    .line 76
    iget v2, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->valuePos:I

    sget-object v3, Lcom/policydm/db/XDBZipDecryptInputStream;->LFH_SIGNATURE:[I

    array-length v3, v3

    if-lt v2, v3, :cond_0

    .line 78
    const/4 v2, 0x2

    iput v2, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->skipBytes:I

    .line 79
    sget-object v2, Lcom/policydm/db/XDBZipDecryptInputStream$State;->FLAGS:Lcom/policydm/db/XDBZipDecryptInputStream$State;

    iput-object v2, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->state:Lcom/policydm/db/XDBZipDecryptInputStream$State;

    goto :goto_0

    .line 84
    :pswitch_1
    and-int/lit8 v2, v1, 0x1

    if-nez v2, :cond_2

    .line 86
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "ZIP not password protected."

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 88
    :cond_2
    and-int/lit8 v2, v1, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_3

    .line 90
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Strong encryption used."

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 92
    :cond_3
    and-int/lit8 v2, v1, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_4

    .line 94
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Unsupported ZIP format."

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 96
    :cond_4
    add-int/lit8 v1, v1, -0x1

    .line 97
    iput v4, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->compressedSize:I

    .line 98
    iput v4, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->valuePos:I

    .line 99
    iput v6, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->valueInc:I

    .line 100
    sget-object v2, Lcom/policydm/db/XDBZipDecryptInputStream$State;->COMPRESSED_SIZE:Lcom/policydm/db/XDBZipDecryptInputStream$State;

    iput-object v2, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->state:Lcom/policydm/db/XDBZipDecryptInputStream$State;

    .line 101
    const/16 v2, 0xb

    iput v2, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->skipBytes:I

    goto :goto_0

    .line 104
    :pswitch_2
    iget v2, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->compressedSize:I

    iget v3, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->valuePos:I

    mul-int/lit8 v3, v3, 0x8

    shl-int v3, v1, v3

    add-int/2addr v2, v3

    iput v2, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->compressedSize:I

    .line 105
    iget v2, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->valueInc:I

    sub-int/2addr v1, v2

    .line 106
    if-gez v1, :cond_5

    .line 108
    iput v5, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->valueInc:I

    .line 109
    add-int/lit16 v1, v1, 0x100

    .line 115
    :goto_1
    iget v2, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->valuePos:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->valuePos:I

    .line 116
    iget v2, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->valuePos:I

    const/4 v3, 0x3

    if-le v2, v3, :cond_0

    .line 118
    iput v4, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->valuePos:I

    .line 119
    iput v4, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->value:I

    .line 120
    sget-object v2, Lcom/policydm/db/XDBZipDecryptInputStream$State;->FN_LENGTH:Lcom/policydm/db/XDBZipDecryptInputStream$State;

    iput-object v2, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->state:Lcom/policydm/db/XDBZipDecryptInputStream$State;

    .line 121
    const/4 v2, 0x4

    iput v2, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->skipBytes:I

    goto/16 :goto_0

    .line 113
    :cond_5
    iput v4, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->valueInc:I

    goto :goto_1

    .line 126
    :pswitch_3
    iget v2, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->value:I

    iget v3, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->valuePos:I

    mul-int/lit8 v3, v3, 0x8

    shl-int v3, v1, v3

    add-int/2addr v2, v3

    iput v2, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->value:I

    .line 127
    iget v2, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->valuePos:I

    if-ne v2, v5, :cond_7

    .line 129
    iput v4, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->valuePos:I

    .line 130
    iget-object v2, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->state:Lcom/policydm/db/XDBZipDecryptInputStream$State;

    sget-object v3, Lcom/policydm/db/XDBZipDecryptInputStream$State;->FN_LENGTH:Lcom/policydm/db/XDBZipDecryptInputStream$State;

    if-ne v2, v3, :cond_6

    .line 132
    sget-object v2, Lcom/policydm/db/XDBZipDecryptInputStream$State;->EF_LENGTH:Lcom/policydm/db/XDBZipDecryptInputStream$State;

    iput-object v2, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->state:Lcom/policydm/db/XDBZipDecryptInputStream$State;

    goto/16 :goto_0

    .line 136
    :cond_6
    sget-object v2, Lcom/policydm/db/XDBZipDecryptInputStream$State;->HEADER:Lcom/policydm/db/XDBZipDecryptInputStream$State;

    iput-object v2, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->state:Lcom/policydm/db/XDBZipDecryptInputStream$State;

    .line 137
    iget v2, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->value:I

    iput v2, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->skipBytes:I

    goto/16 :goto_0

    .line 142
    :cond_7
    iput v5, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->valuePos:I

    goto/16 :goto_0

    .line 146
    :pswitch_4
    iget-object v2, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->password:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/policydm/db/XDBZipDecryptInputStream;->initKeys(Ljava/lang/String;)V

    .line 147
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    if-ge v0, v6, :cond_8

    .line 149
    invoke-direct {p0}, Lcom/policydm/db/XDBZipDecryptInputStream;->decryptByte()B

    move-result v2

    xor-int/2addr v2, v1

    int-to-byte v2, v2

    invoke-direct {p0, v2}, Lcom/policydm/db/XDBZipDecryptInputStream;->updateKeys(B)V

    .line 150
    iget-object v2, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->delegate:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->read()I

    move-result v1

    .line 147
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 152
    :cond_8
    iget v2, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->compressedSize:I

    add-int/lit8 v2, v2, -0xc

    iput v2, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->compressedSize:I

    .line 153
    sget-object v2, Lcom/policydm/db/XDBZipDecryptInputStream$State;->DATA:Lcom/policydm/db/XDBZipDecryptInputStream$State;

    iput-object v2, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->state:Lcom/policydm/db/XDBZipDecryptInputStream$State;

    .line 156
    .end local v0    # "i":I
    :pswitch_5
    invoke-direct {p0}, Lcom/policydm/db/XDBZipDecryptInputStream;->decryptByte()B

    move-result v2

    xor-int/2addr v2, v1

    and-int/lit16 v1, v2, 0xff

    .line 157
    int-to-byte v2, v1

    invoke-direct {p0, v2}, Lcom/policydm/db/XDBZipDecryptInputStream;->updateKeys(B)V

    .line 158
    iget v2, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->compressedSize:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->compressedSize:I

    .line 159
    iget v2, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->compressedSize:I

    if-nez v2, :cond_0

    .line 161
    iput v4, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->valuePos:I

    .line 162
    sget-object v2, Lcom/policydm/db/XDBZipDecryptInputStream$State;->SIGNATURE:Lcom/policydm/db/XDBZipDecryptInputStream$State;

    iput-object v2, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->state:Lcom/policydm/db/XDBZipDecryptInputStream$State;

    goto/16 :goto_0

    .line 170
    :cond_9
    iget v2, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->skipBytes:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/policydm/db/XDBZipDecryptInputStream;->skipBytes:I

    goto/16 :goto_0

    .line 66
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
