.class public Lcom/policydm/db/XDBProfileListAdp;
.super Ljava/lang/Object;
.source "XDBProfileListAdp.java"

# interfaces
.implements Lcom/policydm/interfaces/XDBInterface;
.implements Lcom/policydm/interfaces/XDMInterface;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xdbClearUicResultKeepFlag()V
    .locals 4

    .prologue
    .line 286
    const/4 v1, 0x0

    .line 289
    .local v1, "eUIcKeepFlag":I
    const/16 v2, 0x10

    :try_start_0
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/policydm/db/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 295
    :goto_0
    return-void

    .line 291
    :catch_0
    move-exception v0

    .line 293
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetNotiEvent()I
    .locals 4

    .prologue
    .line 120
    const/4 v1, 0x0

    .line 124
    .local v1, "nEvent":I
    const/16 v3, 0x8

    :try_start_0
    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v2

    .line 125
    .local v2, "oStatus":Ljava/lang/Object;
    if-eqz v2, :cond_0

    .line 126
    check-cast v2, Ljava/lang/Integer;

    .end local v2    # "oStatus":Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 132
    :cond_0
    :goto_0
    return v1

    .line 128
    :catch_0
    move-exception v0

    .line 130
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetNotiJobId()J
    .locals 5

    .prologue
    .line 180
    const-wide/16 v1, 0x0

    .line 184
    .local v1, "nJobId":J
    const/16 v4, 0xa

    :try_start_0
    invoke-static {v4}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    .line 185
    .local v3, "oStatus":Ljava/lang/Object;
    if-eqz v3, :cond_0

    .line 186
    check-cast v3, Ljava/lang/Long;

    .end local v3    # "oStatus":Ljava/lang/Object;
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v1

    .line 192
    :cond_0
    :goto_0
    return-wide v1

    .line 188
    :catch_0
    move-exception v0

    .line 190
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetNotiOpMode()I
    .locals 4

    .prologue
    .line 150
    const/4 v1, 0x0

    .line 154
    .local v1, "nMode":I
    const/16 v3, 0x9

    :try_start_0
    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v2

    .line 155
    .local v2, "oStatus":Ljava/lang/Object;
    if-eqz v2, :cond_0

    .line 156
    check-cast v2, Ljava/lang/Integer;

    .end local v2    # "oStatus":Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 162
    :cond_0
    :goto_0
    return v1

    .line 158
    :catch_0
    move-exception v0

    .line 160
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetNotiReSyncMode()I
    .locals 4

    .prologue
    .line 223
    const/4 v1, 0x0

    .line 227
    .local v1, "nMode":I
    const/16 v3, 0xb

    :try_start_0
    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v2

    .line 228
    .local v2, "oStatus":Ljava/lang/Object;
    if-eqz v2, :cond_0

    .line 229
    check-cast v2, Ljava/lang/Integer;

    .end local v2    # "oStatus":Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 235
    :cond_0
    :goto_0
    return v1

    .line 231
    :catch_0
    move-exception v0

    .line 233
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetNotiSavedInfo()Lcom/policydm/db/XDBSessionSaveInfo;
    .locals 5

    .prologue
    .line 240
    new-instance v2, Lcom/policydm/db/XDBSessionSaveInfo;

    invoke-direct {v2}, Lcom/policydm/db/XDBSessionSaveInfo;-><init>()V

    .line 243
    .local v2, "pSessionSaveInfo":Lcom/policydm/db/XDBSessionSaveInfo;
    const/16 v3, 0xe

    :try_start_0
    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/policydm/db/XDBSessionSaveInfo;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 250
    :goto_0
    if-nez v2, :cond_0

    .line 252
    const-string v3, "Read Error"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 253
    const/4 v2, 0x0

    .line 258
    :goto_1
    return-object v2

    .line 245
    :catch_0
    move-exception v1

    .line 247
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 257
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "nSessionSaveState ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v2, Lcom/policydm/db/XDBSessionSaveInfo;->nSessionSaveState:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "], nNotiUiEvent ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v2, Lcom/policydm/db/XDBSessionSaveInfo;->nNotiUiEvent:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static xdbGetProflieIndex()I
    .locals 4

    .prologue
    .line 37
    const/4 v1, 0x0

    .line 41
    .local v1, "nIdx":I
    const/4 v3, 0x3

    :try_start_0
    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v2

    .line 42
    .local v2, "oStatus":Ljava/lang/Object;
    if-eqz v2, :cond_0

    .line 43
    check-cast v2, Ljava/lang/Integer;

    .end local v2    # "oStatus":Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 49
    :cond_0
    :goto_0
    return v1

    .line 45
    :catch_0
    move-exception v0

    .line 47
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetProflieList(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p0, "ptProflieList"    # Ljava/lang/Object;

    .prologue
    .line 14
    const/4 v1, 0x0

    :try_start_0
    invoke-static {v1}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p0

    .line 20
    :goto_0
    return-object p0

    .line 16
    :catch_0
    move-exception v0

    .line 18
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbGetSessionID(I)Ljava/lang/String;
    .locals 4
    .param p0, "appId"    # I

    .prologue
    .line 69
    const-string v2, ""

    .line 72
    .local v2, "szSessionId":Ljava/lang/String;
    packed-switch p0, :pswitch_data_0

    .line 80
    :try_start_0
    const-string v3, "Not Support Application"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 81
    const/4 v3, 0x7

    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0

    .line 90
    :goto_0
    return-object v2

    .line 75
    :pswitch_0
    const/4 v3, 0x7

    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 76
    goto :goto_0

    .line 85
    :catch_0
    move-exception v1

    .line 87
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 72
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public static xdbSetNotiEvent(I)V
    .locals 3
    .param p0, "nEvent"    # I

    .prologue
    .line 139
    const/16 v1, 0x8

    :try_start_0
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/policydm/db/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 145
    :goto_0
    return-void

    .line 141
    :catch_0
    move-exception v0

    .line 143
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetNotiJobId(J)V
    .locals 3
    .param p0, "nJobId"    # J

    .prologue
    .line 199
    const/16 v1, 0xa

    :try_start_0
    invoke-static {p0, p1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/policydm/db/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 205
    :goto_0
    return-void

    .line 201
    :catch_0
    move-exception v0

    .line 203
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetNotiOpMode(I)V
    .locals 3
    .param p0, "nMode"    # I

    .prologue
    .line 169
    const/16 v1, 0x9

    :try_start_0
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/policydm/db/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 175
    :goto_0
    return-void

    .line 171
    :catch_0
    move-exception v0

    .line 173
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetNotiReSyncMode(I)Ljava/lang/Boolean;
    .locals 3
    .param p0, "nMode"    # I

    .prologue
    .line 212
    const/16 v1, 0xb

    :try_start_0
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/policydm/db/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 218
    :goto_0
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    return-object v1

    .line 214
    :catch_0
    move-exception v0

    .line 216
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetProfileIndex(I)V
    .locals 3
    .param p0, "idx"    # I

    .prologue
    .line 56
    const/4 v1, 0x3

    :try_start_0
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/policydm/db/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 64
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Set Profile Index : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 65
    return-void

    .line 58
    :catch_0
    move-exception v0

    .line 60
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unable to write index : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 61
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetProflieList(Ljava/lang/Object;)V
    .locals 2
    .param p0, "ptProflieList"    # Ljava/lang/Object;

    .prologue
    .line 27
    const/4 v1, 0x0

    :try_start_0
    invoke-static {v1, p0}, Lcom/policydm/db/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 33
    :goto_0
    return-void

    .line 29
    :catch_0
    move-exception v0

    .line 31
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbSetSessionID(ILjava/lang/String;)V
    .locals 2
    .param p0, "appId"    # I
    .param p1, "szSessionID"    # Ljava/lang/String;

    .prologue
    .line 95
    packed-switch p0, :pswitch_data_0

    .line 113
    const-string v1, "Not Support Application"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 116
    :goto_0
    return-void

    .line 100
    :pswitch_0
    const/4 v1, 0x7

    :try_start_0
    invoke-static {v1, p1}, Lcom/policydm/db/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 102
    :catch_0
    move-exception v0

    .line 104
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 109
    .end local v0    # "e":Ljava/lang/Exception;
    :pswitch_1
    const-string v1, "Not Support Application"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 95
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static xdbSetSessionSaveStatus(III)Z
    .locals 5
    .param p0, "nState"    # I
    .param p1, "nNotiUiEvent"    # I
    .param p2, "nNotiRetryCount"    # I

    .prologue
    .line 263
    new-instance v2, Lcom/policydm/db/XDBSessionSaveInfo;

    invoke-direct {v2}, Lcom/policydm/db/XDBSessionSaveInfo;-><init>()V

    .line 264
    .local v2, "pSessionSave":Lcom/policydm/db/XDBSessionSaveInfo;
    const/4 v0, 0x1

    .line 265
    .local v0, "bResult":Z
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "nSessionSaveState ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "], nNotiUiEvent ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "], nNotiRetrycount ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 267
    iput p0, v2, Lcom/policydm/db/XDBSessionSaveInfo;->nSessionSaveState:I

    .line 268
    iput p1, v2, Lcom/policydm/db/XDBSessionSaveInfo;->nNotiUiEvent:I

    .line 269
    iput p2, v2, Lcom/policydm/db/XDBSessionSaveInfo;->nNotiRetryCount:I

    .line 273
    const/16 v3, 0xe

    :try_start_0
    invoke-static {v3, v2}, Lcom/policydm/db/XDB;->xdbWrite(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 280
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "return value :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 281
    return v0

    .line 275
    :catch_0
    move-exception v1

    .line 277
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 278
    const/4 v0, 0x0

    goto :goto_0
.end method
