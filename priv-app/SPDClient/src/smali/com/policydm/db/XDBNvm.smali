.class public Lcom/policydm/db/XDBNvm;
.super Ljava/lang/Object;
.source "XDBNvm.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public NVMAccXNode:Lcom/policydm/db/XDBAccXListNode;

.field public NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

.field public NVMDmAgentInfo:Lcom/policydm/db/XDBAgentInfo;

.field public NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

.field public NVMNotiInfo:Lcom/policydm/db/XDBNotiInfo;

.field public NVMPollingInfo:Lcom/policydm/db/XDBPollingInfo;

.field public NVMResyncMode:Lcom/policydm/db/XDBResyncModeInfo;

.field public NVMSPDInfo:Lcom/policydm/db/XDBSpdInfo;

.field public NVMSimInfo:Lcom/policydm/db/XDBSimInfo;

.field public tProfileList:Lcom/policydm/db/XDBProfileListInfo;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Lcom/policydm/db/XDBProfileListInfo;

    invoke-direct {v0}, Lcom/policydm/db/XDBProfileListInfo;-><init>()V

    iput-object v0, p0, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    .line 25
    new-instance v0, Lcom/policydm/db/XDBProfileInfo;

    invoke-direct {v0}, Lcom/policydm/db/XDBProfileInfo;-><init>()V

    iput-object v0, p0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    .line 26
    new-instance v0, Lcom/policydm/db/XDBFumoInfo;

    invoke-direct {v0}, Lcom/policydm/db/XDBFumoInfo;-><init>()V

    iput-object v0, p0, Lcom/policydm/db/XDBNvm;->NVMFUMOInfo:Lcom/policydm/db/XDBFumoInfo;

    .line 27
    new-instance v0, Lcom/policydm/db/XDBSimInfo;

    invoke-direct {v0}, Lcom/policydm/db/XDBSimInfo;-><init>()V

    iput-object v0, p0, Lcom/policydm/db/XDBNvm;->NVMSimInfo:Lcom/policydm/db/XDBSimInfo;

    .line 28
    new-instance v0, Lcom/policydm/db/XDBPollingInfo;

    invoke-direct {v0}, Lcom/policydm/db/XDBPollingInfo;-><init>()V

    iput-object v0, p0, Lcom/policydm/db/XDBNvm;->NVMPollingInfo:Lcom/policydm/db/XDBPollingInfo;

    .line 32
    new-instance v0, Lcom/policydm/db/XDBAccXListNode;

    invoke-direct {v0}, Lcom/policydm/db/XDBAccXListNode;-><init>()V

    iput-object v0, p0, Lcom/policydm/db/XDBNvm;->NVMAccXNode:Lcom/policydm/db/XDBAccXListNode;

    .line 33
    new-instance v0, Lcom/policydm/db/XDBResyncModeInfo;

    invoke-direct {v0}, Lcom/policydm/db/XDBResyncModeInfo;-><init>()V

    iput-object v0, p0, Lcom/policydm/db/XDBNvm;->NVMResyncMode:Lcom/policydm/db/XDBResyncModeInfo;

    .line 36
    new-instance v0, Lcom/policydm/db/XDBNotiInfo;

    invoke-direct {v0}, Lcom/policydm/db/XDBNotiInfo;-><init>()V

    iput-object v0, p0, Lcom/policydm/db/XDBNvm;->NVMNotiInfo:Lcom/policydm/db/XDBNotiInfo;

    .line 38
    new-instance v0, Lcom/policydm/db/XDBAgentInfo;

    invoke-direct {v0}, Lcom/policydm/db/XDBAgentInfo;-><init>()V

    iput-object v0, p0, Lcom/policydm/db/XDBNvm;->NVMDmAgentInfo:Lcom/policydm/db/XDBAgentInfo;

    .line 39
    new-instance v0, Lcom/policydm/db/XDBSpdInfo;

    invoke-direct {v0}, Lcom/policydm/db/XDBSpdInfo;-><init>()V

    iput-object v0, p0, Lcom/policydm/db/XDBNvm;->NVMSPDInfo:Lcom/policydm/db/XDBSpdInfo;

    .line 40
    return-void
.end method
