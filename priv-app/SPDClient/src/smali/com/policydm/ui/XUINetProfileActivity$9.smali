.class Lcom/policydm/ui/XUINetProfileActivity$9;
.super Ljava/lang/Object;
.source "XUINetProfileActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/policydm/ui/XUINetProfileActivity;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/policydm/ui/XUINetProfileActivity;


# direct methods
.method constructor <init>(Lcom/policydm/ui/XUINetProfileActivity;)V
    .locals 0

    .prologue
    .line 242
    iput-object p1, p0, Lcom/policydm/ui/XUINetProfileActivity$9;->this$0:Lcom/policydm/ui/XUINetProfileActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 246
    const-string v0, ""

    .line 247
    .local v0, "proxyAddr":Ljava/lang/String;
    # getter for: Lcom/policydm/ui/XUINetProfileActivity;->m_LocalNetworkProfileList_t:Lcom/policydm/db/XDBNetworkProfileList;
    invoke-static {}, Lcom/policydm/ui/XUINetProfileActivity;->access$000()Lcom/policydm/db/XDBNetworkProfileList;

    move-result-object v1

    iget-object v1, v1, Lcom/policydm/db/XDBNetworkProfileList;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    iget-object v1, v1, Lcom/policydm/db/XDBInfoConRef;->PX:Lcom/policydm/db/XDBConRefPX;

    iget-object v0, v1, Lcom/policydm/db/XDBConRefPX;->Addr:Ljava/lang/String;

    .line 248
    if-eqz v0, :cond_0

    const-string v1, "0.0.0.0"

    invoke-virtual {v1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    .line 250
    # getter for: Lcom/policydm/ui/XUINetProfileActivity;->m_LocalNetworkProfileList_t:Lcom/policydm/db/XDBNetworkProfileList;
    invoke-static {}, Lcom/policydm/ui/XUINetProfileActivity;->access$000()Lcom/policydm/db/XDBNetworkProfileList;

    move-result-object v1

    iget-object v1, v1, Lcom/policydm/db/XDBNetworkProfileList;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/policydm/db/XDBInfoConRef;->bProxyUse:Z

    .line 257
    :goto_0
    # getter for: Lcom/policydm/ui/XUINetProfileActivity;->m_LocalNetworkProfileList_t:Lcom/policydm/db/XDBNetworkProfileList;
    invoke-static {}, Lcom/policydm/ui/XUINetProfileActivity;->access$000()Lcom/policydm/db/XDBNetworkProfileList;

    move-result-object v1

    # getter for: Lcom/policydm/ui/XUINetProfileActivity;->m_nNProfile:I
    invoke-static {}, Lcom/policydm/ui/XUINetProfileActivity;->access$200()I

    move-result v2

    iput v2, v1, Lcom/policydm/db/XDBNetworkProfileList;->ActivateID:I

    .line 258
    sget-object v1, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v1, v1, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    # getter for: Lcom/policydm/ui/XUINetProfileActivity;->m_LocalNetworkProfileList_t:Lcom/policydm/db/XDBNetworkProfileList;
    invoke-static {}, Lcom/policydm/ui/XUINetProfileActivity;->access$000()Lcom/policydm/db/XDBNetworkProfileList;

    move-result-object v2

    iget-object v2, v2, Lcom/policydm/db/XDBNetworkProfileList;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    iput-object v2, v1, Lcom/policydm/db/XDBProfileInfo;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    .line 259
    sget-object v1, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v1, v1, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    iget-object v1, v1, Lcom/policydm/db/XDBProfileInfo;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    invoke-static {v1}, Lcom/policydm/db/XDBProfileAdp;->xdbSetConRef(Lcom/policydm/db/XDBInfoConRef;)V

    .line 260
    iget-object v1, p0, Lcom/policydm/ui/XUINetProfileActivity$9;->this$0:Lcom/policydm/ui/XUINetProfileActivity;

    # invokes: Lcom/policydm/ui/XUINetProfileActivity;->xuiCallUiDmProfile()V
    invoke-static {v1}, Lcom/policydm/ui/XUINetProfileActivity;->access$100(Lcom/policydm/ui/XUINetProfileActivity;)V

    .line 261
    return-void

    .line 254
    :cond_0
    # getter for: Lcom/policydm/ui/XUINetProfileActivity;->m_LocalNetworkProfileList_t:Lcom/policydm/db/XDBNetworkProfileList;
    invoke-static {}, Lcom/policydm/ui/XUINetProfileActivity;->access$000()Lcom/policydm/db/XDBNetworkProfileList;

    move-result-object v1

    iget-object v1, v1, Lcom/policydm/db/XDBNetworkProfileList;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/policydm/db/XDBInfoConRef;->bProxyUse:Z

    goto :goto_0
.end method
