.class public Lcom/policydm/ui/XUINotificationManager;
.super Ljava/lang/Object;
.source "XUINotificationManager.java"

# interfaces
.implements Lcom/policydm/interfaces/XUIInterface;


# static fields
.field private static final NOT_USE_TICKER:I

.field private static m_Context:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const/4 v0, 0x0

    sput-object v0, Lcom/policydm/ui/XUINotificationManager;->m_Context:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xuiNotiInitialize(Landroid/content/Context;)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 24
    sput-object p0, Lcom/policydm/ui/XUINotificationManager;->m_Context:Landroid/content/Context;

    .line 25
    return-void
.end method

.method public static xuiSetIndicator(I)V
    .locals 10
    .param p0, "nIndicator"    # I

    .prologue
    const/16 v9, 0x8

    const/4 v8, 0x6

    const/high16 v1, 0x7f020000

    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 29
    sget-object v6, Lcom/policydm/ui/XUINotificationManager;->m_Context:Landroid/content/Context;

    if-nez v6, :cond_0

    .line 31
    const-string v1, "m_Context is null, return"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 146
    :goto_0
    return-void

    .line 35
    :cond_0
    const-string v6, "notification"

    invoke-static {v6}, Lcom/policydm/XDMApplication;->xdmGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 36
    .local v0, "notificationManager":Landroid/app/NotificationManager;
    if-nez v0, :cond_1

    .line 38
    const-string v1, "NotificationManager is null, return"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 42
    :cond_1
    const v2, 0x7f080033

    .line 43
    .local v2, "nTitleStrId":I
    const/4 v6, 0x2

    if-eq p0, v6, :cond_2

    const/4 v6, 0x3

    if-eq p0, v6, :cond_2

    const/4 v6, 0x4

    if-ne p0, v6, :cond_3

    .line 44
    :cond_2
    const v2, 0x7f080050

    .line 46
    :cond_3
    const/4 v3, 0x0

    .line 47
    .local v3, "nTextStrId":I
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Indicator id : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 49
    packed-switch p0, :pswitch_data_0

    .line 87
    :goto_1
    packed-switch p0, :pswitch_data_1

    .line 145
    :goto_2
    :pswitch_0
    invoke-static {p0}, Lcom/policydm/XDMApplication;->xdmSetNotibarState(I)V

    goto :goto_0

    .line 52
    :pswitch_1
    invoke-virtual {v0, v5}, Landroid/app/NotificationManager;->cancel(I)V

    .line 53
    invoke-virtual {v0, v8}, Landroid/app/NotificationManager;->cancel(I)V

    .line 54
    invoke-virtual {v0, v9}, Landroid/app/NotificationManager;->cancel(I)V

    goto :goto_1

    .line 62
    :pswitch_2
    invoke-virtual {v0, v8}, Landroid/app/NotificationManager;->cancel(I)V

    .line 63
    invoke-virtual {v0, v9}, Landroid/app/NotificationManager;->cancel(I)V

    goto :goto_1

    .line 67
    :pswitch_3
    invoke-virtual {v0, v5}, Landroid/app/NotificationManager;->cancel(I)V

    .line 68
    invoke-virtual {v0, v9}, Landroid/app/NotificationManager;->cancel(I)V

    goto :goto_1

    .line 71
    :pswitch_4
    invoke-virtual {v0, v8}, Landroid/app/NotificationManager;->cancel(I)V

    goto :goto_1

    .line 75
    :pswitch_5
    invoke-virtual {v0, v5}, Landroid/app/NotificationManager;->cancel(I)V

    .line 76
    invoke-virtual {v0, v8}, Landroid/app/NotificationManager;->cancel(I)V

    goto :goto_1

    .line 80
    :pswitch_6
    invoke-virtual {v0, v9}, Landroid/app/NotificationManager;->cancel(I)V

    goto :goto_1

    .line 90
    :pswitch_7
    const v3, 0x7f080030

    move v6, p0

    .line 91
    invoke-static/range {v0 .. v6}, Lcom/policydm/ui/XUINotificationManager;->xuiSetNotification(Landroid/app/NotificationManager;IIIIII)V

    goto :goto_2

    .line 95
    :pswitch_8
    const v3, 0x7f080027

    .line 96
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmCheckNAProduct()Z

    move-result v6

    if-nez v6, :cond_4

    .line 97
    const v3, 0x7f08002c

    :cond_4
    move v6, p0

    .line 98
    invoke-static/range {v0 .. v6}, Lcom/policydm/ui/XUINotificationManager;->xuiSetNotification(Landroid/app/NotificationManager;IIIIII)V

    goto :goto_2

    .line 102
    :pswitch_9
    const v3, 0x7f080049

    .line 103
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmCheckNAProduct()Z

    move-result v6

    if-nez v6, :cond_5

    .line 104
    const v3, 0x7f08004c

    :cond_5
    move v6, p0

    .line 105
    invoke-static/range {v0 .. v6}, Lcom/policydm/ui/XUINotificationManager;->xuiSetNotification(Landroid/app/NotificationManager;IIIIII)V

    goto :goto_2

    .line 109
    :pswitch_a
    const v3, 0x7f08004d

    move v6, p0

    .line 110
    invoke-static/range {v0 .. v6}, Lcom/policydm/ui/XUINotificationManager;->xuiSetNotification(Landroid/app/NotificationManager;IIIIII)V

    goto :goto_2

    .line 114
    :pswitch_b
    const v3, 0x7f080051

    .line 115
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmCheckNAProduct()Z

    move-result v6

    if-nez v6, :cond_6

    .line 116
    const v3, 0x7f080052

    :cond_6
    move v6, p0

    .line 117
    invoke-static/range {v0 .. v6}, Lcom/policydm/ui/XUINotificationManager;->xuiSetNotification(Landroid/app/NotificationManager;IIIIII)V

    goto :goto_2

    .line 121
    :pswitch_c
    const v3, 0x7f080022

    .line 122
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmCheckNAProduct()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 124
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmTablet()Z

    move-result v5

    if-eqz v5, :cond_9

    .line 125
    const v3, 0x7f080024

    .line 129
    :cond_7
    :goto_3
    const-string v5, "ATT"

    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetCustomCode()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 131
    const v3, 0x7f080023

    :cond_8
    move v5, v8

    move v6, p0

    .line 133
    invoke-static/range {v0 .. v6}, Lcom/policydm/ui/XUINotificationManager;->xuiSetNotification(Landroid/app/NotificationManager;IIIIII)V

    goto/16 :goto_2

    .line 127
    :cond_9
    const v3, 0x7f080021

    goto :goto_3

    .line 137
    :pswitch_d
    const v3, 0x7f080005

    move v5, v9

    move v6, p0

    .line 138
    invoke-static/range {v0 .. v6}, Lcom/policydm/ui/XUINotificationManager;->xuiSetNotification(Landroid/app/NotificationManager;IIIIII)V

    goto/16 :goto_2

    .line 49
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 87
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_0
        :pswitch_d
    .end packed-switch
.end method

.method public static xuiSetNotification(Landroid/app/NotificationManager;IIIIII)V
    .locals 13
    .param p0, "notificationManager"    # Landroid/app/NotificationManager;
    .param p1, "moodId"    # I
    .param p2, "titleId"    # I
    .param p3, "textId"    # I
    .param p4, "tickerId"    # I
    .param p5, "id"    # I
    .param p6, "nIndicatorState"    # I

    .prologue
    .line 150
    const/4 v3, 0x0

    .line 151
    .local v3, "m_Builder":Landroid/app/Notification$Builder;
    const/4 v4, 0x0

    .line 152
    .local v4, "notification":Landroid/app/Notification;
    const/4 v6, 0x0

    .line 153
    .local v6, "text":Ljava/lang/CharSequence;
    const/4 v7, 0x0

    .line 154
    .local v7, "titletext":Ljava/lang/CharSequence;
    const/4 v5, 0x0

    .line 155
    .local v5, "szTickerText":Ljava/lang/String;
    const/4 v2, 0x0

    .line 157
    .local v2, "intent":Landroid/content/Intent;
    sget-object v8, Lcom/policydm/ui/XUINotificationManager;->m_Context:Landroid/content/Context;

    if-nez v8, :cond_0

    .line 159
    const-string v8, "m_Context is null, return"

    invoke-static {v8}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 223
    :goto_0
    return-void

    .line 163
    :cond_0
    if-nez p0, :cond_1

    .line 165
    const-string v8, "NotificationManager is null, return"

    invoke-static {v8}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 169
    :cond_1
    if-lez p3, :cond_2

    .line 170
    sget-object v8, Lcom/policydm/ui/XUINotificationManager;->m_Context:Landroid/content/Context;

    move/from16 v0, p3

    invoke-virtual {v8, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    .line 171
    :cond_2
    if-lez p2, :cond_3

    .line 172
    sget-object v8, Lcom/policydm/ui/XUINotificationManager;->m_Context:Landroid/content/Context;

    invoke-virtual {v8, p2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v7

    .line 173
    :cond_3
    if-lez p4, :cond_4

    .line 174
    sget-object v8, Lcom/policydm/ui/XUINotificationManager;->m_Context:Landroid/content/Context;

    move/from16 v0, p4

    invoke-virtual {v8, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    .end local v5    # "szTickerText":Ljava/lang/String;
    check-cast v5, Ljava/lang/String;

    .line 176
    .restart local v5    # "szTickerText":Ljava/lang/String;
    :cond_4
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "NotificationID : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move/from16 v0, p5

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " / Notification IndicatorState : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move/from16 v0, p6

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 178
    new-instance v3, Landroid/app/Notification$Builder;

    .end local v3    # "m_Builder":Landroid/app/Notification$Builder;
    sget-object v8, Lcom/policydm/ui/XUINotificationManager;->m_Context:Landroid/content/Context;

    invoke-direct {v3, v8}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 179
    .restart local v3    # "m_Builder":Landroid/app/Notification$Builder;
    invoke-virtual {v3, v7}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 180
    invoke-virtual {v3, v6}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 181
    invoke-virtual {v3, p1}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    .line 182
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v3, v8, v9}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    .line 183
    new-instance v8, Landroid/app/Notification$BigTextStyle;

    invoke-direct {v8}, Landroid/app/Notification$BigTextStyle;-><init>()V

    invoke-virtual {v8, v6}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    move-result-object v8

    invoke-virtual {v3, v8}, Landroid/app/Notification$Builder;->setStyle(Landroid/app/Notification$Style;)Landroid/app/Notification$Builder;

    .line 184
    sget-object v8, Lcom/policydm/ui/XUINotificationManager;->m_Context:Landroid/content/Context;

    const/4 v9, 0x0

    new-instance v10, Landroid/content/Intent;

    const-string v11, "com.policydm.intent.action.DELETE_NOTI"

    invoke-direct {v10, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v11, 0x0

    invoke-static {v8, v9, v10, v11}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v8

    invoke-virtual {v3, v8}, Landroid/app/Notification$Builder;->setDeleteIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 185
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_5

    .line 186
    invoke-virtual {v3, v5}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 187
    :cond_5
    invoke-virtual {v3}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v4

    .line 190
    packed-switch p6, :pswitch_data_0

    .line 216
    :pswitch_0
    const/16 v8, 0x10

    iput v8, v4, Landroid/app/Notification;->flags:I

    .line 217
    sget-object v8, Lcom/policydm/ui/XUINotificationManager;->m_Context:Landroid/content/Context;

    const/4 v9, 0x0

    new-instance v10, Landroid/content/Intent;

    sget-object v11, Lcom/policydm/ui/XUINotificationManager;->m_Context:Landroid/content/Context;

    const-class v12, Lcom/policydm/XDMApplication;

    invoke-direct {v10, v11, v12}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v11, 0x0

    invoke-static {v8, v9, v10, v11}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 218
    .local v1, "contentIntent":Landroid/app/PendingIntent;
    iput-object v1, v4, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    .line 222
    :goto_1
    move/from16 v0, p5

    invoke-virtual {p0, v0, v4}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto/16 :goto_0

    .line 197
    .end local v1    # "contentIntent":Landroid/app/PendingIntent;
    :pswitch_1
    const/16 v8, 0x10

    iput v8, v4, Landroid/app/Notification;->flags:I

    .line 198
    new-instance v2, Landroid/content/Intent;

    .end local v2    # "intent":Landroid/content/Intent;
    sget-object v8, Lcom/policydm/ui/XUINotificationManager;->m_Context:Landroid/content/Context;

    const-class v9, Lcom/policydm/ui/XUINotificationConnectActivity;

    invoke-direct {v2, v8, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 199
    .restart local v2    # "intent":Landroid/content/Intent;
    const/high16 v8, 0x14000000

    invoke-virtual {v2, v8}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 200
    invoke-static/range {p6 .. p6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 201
    sget-object v8, Lcom/policydm/ui/XUINotificationManager;->m_Context:Landroid/content/Context;

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static {v8, v9, v2, v10}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 202
    .restart local v1    # "contentIntent":Landroid/app/PendingIntent;
    iput-object v1, v4, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    goto :goto_1

    .line 207
    .end local v1    # "contentIntent":Landroid/app/PendingIntent;
    :pswitch_2
    const/16 v8, 0x10

    iput v8, v4, Landroid/app/Notification;->flags:I

    .line 208
    new-instance v2, Landroid/content/Intent;

    .end local v2    # "intent":Landroid/content/Intent;
    sget-object v8, Lcom/policydm/ui/XUINotificationManager;->m_Context:Landroid/content/Context;

    const-class v9, Lcom/policydm/ui/XUINotificationConnectActivity;

    invoke-direct {v2, v8, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 209
    .restart local v2    # "intent":Landroid/content/Intent;
    const/high16 v8, 0x14000000

    invoke-virtual {v2, v8}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 210
    invoke-static/range {p6 .. p6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 211
    sget-object v8, Lcom/policydm/ui/XUINotificationManager;->m_Context:Landroid/content/Context;

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static {v8, v9, v2, v10}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 212
    .restart local v1    # "contentIntent":Landroid/app/PendingIntent;
    iput-object v1, v4, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    goto :goto_1

    .line 190
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
