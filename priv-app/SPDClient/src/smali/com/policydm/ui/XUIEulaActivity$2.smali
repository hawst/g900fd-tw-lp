.class Lcom/policydm/ui/XUIEulaActivity$2;
.super Ljava/lang/Object;
.source "XUIEulaActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/policydm/ui/XUIEulaActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/policydm/ui/XUIEulaActivity;


# direct methods
.method constructor <init>(Lcom/policydm/ui/XUIEulaActivity;)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/policydm/ui/XUIEulaActivity$2;->this$0:Lcom/policydm/ui/XUIEulaActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 90
    const-string v0, "Eula Confirm!!"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 91
    iget-object v0, p0, Lcom/policydm/ui/XUIEulaActivity$2;->this$0:Lcom/policydm/ui/XUIEulaActivity;

    # setter for: Lcom/policydm/ui/XUIEulaActivity;->mEULAAccept:I
    invoke-static {v0, v1}, Lcom/policydm/ui/XUIEulaActivity;->access$002(Lcom/policydm/ui/XUIEulaActivity;I)I

    .line 92
    invoke-static {v1}, Lcom/policydm/db/XDB;->xdbSetSPDRegisterStatus(I)V

    .line 93
    const/4 v0, 0x7

    invoke-static {v0}, Lcom/policydm/ui/XUINotificationManager;->xuiSetIndicator(I)V

    .line 94
    invoke-static {v2}, Lcom/policydm/XDMApplication;->xdmStopAlarm(I)V

    .line 95
    invoke-static {v1}, Lcom/policydm/db/XDB;->xdbSetAutoUpdateFlag(Z)V

    .line 96
    iget-object v0, p0, Lcom/policydm/ui/XUIEulaActivity$2;->this$0:Lcom/policydm/ui/XUIEulaActivity;

    # getter for: Lcom/policydm/ui/XUIEulaActivity;->mEULAType:I
    invoke-static {v0}, Lcom/policydm/ui/XUIEulaActivity;->access$100(Lcom/policydm/ui/XUIEulaActivity;)I

    move-result v0

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/policydm/ui/XUIEulaActivity$2;->this$0:Lcom/policydm/ui/XUIEulaActivity;

    # getter for: Lcom/policydm/ui/XUIEulaActivity;->mEULAType:I
    invoke-static {v0}, Lcom/policydm/ui/XUIEulaActivity;->access$100(Lcom/policydm/ui/XUIEulaActivity;)I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 98
    :cond_0
    invoke-static {}, Lcom/policydm/XDMBroadcastReceiver;->callRegisterStart()V

    .line 99
    const/16 v0, 0xcb

    invoke-static {v0}, Lcom/policydm/XDMApplication;->xdmSendMessageDmHandler(I)V

    .line 112
    :goto_0
    iget-object v0, p0, Lcom/policydm/ui/XUIEulaActivity$2;->this$0:Lcom/policydm/ui/XUIEulaActivity;

    invoke-virtual {v0}, Lcom/policydm/ui/XUIEulaActivity;->finish()V

    .line 113
    return-void

    .line 101
    :cond_1
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmProtoIsWIFIConnected()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lcom/policydm/XDMApplication;->xdmProtoIsMobileDataConnected()Z

    move-result v0

    if-nez v0, :cond_2

    .line 103
    invoke-static {v2}, Lcom/policydm/ui/XUINotificationManager;->xuiSetIndicator(I)V

    goto :goto_0

    .line 107
    :cond_2
    const/16 v0, 0x67

    invoke-static {v0}, Lcom/policydm/XDMApplication;->xdmSendMessageDmHandler(I)V

    .line 109
    const-string v0, "Go SPD Device Registraion!!"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 110
    const/16 v0, 0x3c

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/policydm/eng/core/XDMMsg;->xdmSendRCMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method
