.class Lcom/policydm/ui/XUINetProfileActivity$3;
.super Ljava/lang/Object;
.source "XUINetProfileActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/policydm/ui/XUINetProfileActivity;->xuiCurrentTabSet(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/policydm/ui/XUINetProfileActivity;


# direct methods
.method constructor <init>(Lcom/policydm/ui/XUINetProfileActivity;)V
    .locals 0

    .prologue
    .line 102
    iput-object p1, p0, Lcom/policydm/ui/XUINetProfileActivity$3;->this$0:Lcom/policydm/ui/XUINetProfileActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 105
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 106
    # getter for: Lcom/policydm/ui/XUINetProfileActivity;->m_LocalNetworkProfileList_t:Lcom/policydm/db/XDBNetworkProfileList;
    invoke-static {}, Lcom/policydm/ui/XUINetProfileActivity;->access$000()Lcom/policydm/db/XDBNetworkProfileList;

    move-result-object v0

    iget-object v0, v0, Lcom/policydm/db/XDBNetworkProfileList;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    iget-object v0, v0, Lcom/policydm/db/XDBInfoConRef;->PX:Lcom/policydm/db/XDBConRefPX;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/policydm/db/XDBConRefPX;->nPortNbr:I

    .line 109
    :goto_0
    return-void

    .line 108
    :cond_0
    # getter for: Lcom/policydm/ui/XUINetProfileActivity;->m_LocalNetworkProfileList_t:Lcom/policydm/db/XDBNetworkProfileList;
    invoke-static {}, Lcom/policydm/ui/XUINetProfileActivity;->access$000()Lcom/policydm/db/XDBNetworkProfileList;

    move-result-object v0

    iget-object v0, v0, Lcom/policydm/db/XDBNetworkProfileList;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    iget-object v0, v0, Lcom/policydm/db/XDBInfoConRef;->PX:Lcom/policydm/db/XDBConRefPX;

    const/4 v1, 0x0

    iput v1, v0, Lcom/policydm/db/XDBConRefPX;->nPortNbr:I

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 113
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 117
    return-void
.end method
