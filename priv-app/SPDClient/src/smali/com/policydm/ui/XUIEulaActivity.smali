.class public Lcom/policydm/ui/XUIEulaActivity;
.super Landroid/app/Activity;
.source "XUIEulaActivity.java"

# interfaces
.implements Lcom/policydm/interfaces/XEventInterface;
.implements Lcom/policydm/interfaces/XSPDInterface;
.implements Lcom/policydm/interfaces/XUIInterface;


# instance fields
.field private mEULAAccept:I

.field private mEULAType:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 31
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 33
    iput v0, p0, Lcom/policydm/ui/XUIEulaActivity;->mEULAType:I

    .line 34
    iput v0, p0, Lcom/policydm/ui/XUIEulaActivity;->mEULAAccept:I

    return-void
.end method

.method static synthetic access$002(Lcom/policydm/ui/XUIEulaActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/policydm/ui/XUIEulaActivity;
    .param p1, "x1"    # I

    .prologue
    .line 31
    iput p1, p0, Lcom/policydm/ui/XUIEulaActivity;->mEULAAccept:I

    return p1
.end method

.method static synthetic access$100(Lcom/policydm/ui/XUIEulaActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/policydm/ui/XUIEulaActivity;

    .prologue
    .line 31
    iget v0, p0, Lcom/policydm/ui/XUIEulaActivity;->mEULAType:I

    return v0
.end method

.method private getContents(I)Ljava/lang/String;
    .locals 8
    .param p1, "id"    # I

    .prologue
    .line 136
    const/4 v2, 0x0

    .line 139
    .local v2, "in":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v6, Ljava/io/InputStreamReader;

    invoke-virtual {p0}, Lcom/policydm/ui/XUIEulaActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, p1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v3, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    .line 147
    .end local v2    # "in":Ljava/io/BufferedReader;
    .local v3, "in":Ljava/io/BufferedReader;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 151
    .local v5, "sb":Ljava/lang/StringBuilder;
    if-eqz v3, :cond_1

    .line 153
    :goto_0
    :try_start_1
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    .local v4, "line":Ljava/lang/String;
    if-eqz v4, :cond_1

    .line 155
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 159
    .end local v4    # "line":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 161
    .local v0, "e":Ljava/io/IOException;
    :try_start_2
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 167
    if-eqz v3, :cond_0

    .line 169
    :try_start_3
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 177
    .end local v0    # "e":Ljava/io/IOException;
    :cond_0
    :goto_1
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object v2, v3

    .end local v3    # "in":Ljava/io/BufferedReader;
    .end local v5    # "sb":Ljava/lang/StringBuilder;
    .restart local v2    # "in":Ljava/io/BufferedReader;
    :goto_2
    return-object v6

    .line 141
    :catch_1
    move-exception v0

    .line 143
    .local v0, "e":Landroid/content/res/Resources$NotFoundException;
    invoke-virtual {v0}, Landroid/content/res/Resources$NotFoundException;->printStackTrace()V

    .line 144
    const/4 v6, 0x0

    goto :goto_2

    .line 167
    .end local v0    # "e":Landroid/content/res/Resources$NotFoundException;
    .end local v2    # "in":Ljava/io/BufferedReader;
    .restart local v3    # "in":Ljava/io/BufferedReader;
    .restart local v5    # "sb":Ljava/lang/StringBuilder;
    :cond_1
    if-eqz v3, :cond_0

    .line 169
    :try_start_4
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    .line 172
    :catch_2
    move-exception v1

    .line 174
    .local v1, "e2":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 172
    .end local v1    # "e2":Ljava/io/IOException;
    .local v0, "e":Ljava/io/IOException;
    :catch_3
    move-exception v1

    .line 174
    .restart local v1    # "e2":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 165
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "e2":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    .line 167
    if-eqz v3, :cond_2

    .line 169
    :try_start_5
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    .line 175
    :cond_2
    :goto_3
    throw v6

    .line 172
    :catch_4
    move-exception v1

    .line 174
    .restart local v1    # "e2":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3
.end method

.method private updateContent()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 182
    const v3, 0x7f0a0001

    invoke-virtual {p0, v3}, Lcom/policydm/ui/XUIEulaActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    .line 183
    .local v0, "eulaView":Landroid/webkit/WebView;
    const/high16 v6, 0x7f050000

    .line 185
    .local v6, "id":I
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmKorModel()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 187
    const v6, 0x7f050001

    .line 190
    :cond_0
    invoke-direct {p0, v6}, Lcom/policydm/ui/XUIEulaActivity;->getContents(I)Ljava/lang/String;

    move-result-object v2

    .line 193
    .local v2, "contents":Ljava/lang/String;
    const-string v3, "<BODY>"

    const-string v4, "<BODY style=\"word-wrap:break-word;white-space:pre-wrap;\">"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 194
    if-eqz v2, :cond_1

    .line 196
    const-string v3, "text/html"

    const-string v4, "UTF-8"

    move-object v5, v1

    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    :cond_1
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    .line 48
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 50
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmTablet()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmWhite()Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    const-string v3, "ro.build.scafe.cream"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "white"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 52
    :cond_1
    const v3, 0x7f090003

    invoke-virtual {p0, v3}, Lcom/policydm/ui/XUIEulaActivity;->setTheme(I)V

    .line 54
    :cond_2
    invoke-static {v5}, Lcom/policydm/XDMBroadcastReceiver;->xdmSetCheckedIntent(Z)V

    .line 56
    const/high16 v3, 0x7f030000

    invoke-virtual {p0, v3}, Lcom/policydm/ui/XUIEulaActivity;->setContentView(I)V

    .line 58
    const/4 v0, 0x0

    .line 59
    .local v0, "bCancel":Landroid/widget/Button;
    const/4 v1, 0x0

    .line 61
    .local v1, "bConfirm":Landroid/widget/Button;
    invoke-virtual {p0}, Lcom/policydm/ui/XUIEulaActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 62
    .local v2, "extras":Landroid/os/Bundle;
    if-eqz v2, :cond_3

    .line 64
    const-string v3, "EULA_TYPE"

    invoke-virtual {v2, v3, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/policydm/ui/XUIEulaActivity;->mEULAType:I

    .line 65
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "EULA type : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/policydm/ui/XUIEulaActivity;->mEULAType:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 68
    :cond_3
    invoke-direct {p0}, Lcom/policydm/ui/XUIEulaActivity;->updateContent()V

    .line 70
    const v3, 0x7f0a0002

    invoke-virtual {p0, v3}, Lcom/policydm/ui/XUIEulaActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "bCancel":Landroid/widget/Button;
    check-cast v0, Landroid/widget/Button;

    .line 71
    .restart local v0    # "bCancel":Landroid/widget/Button;
    new-instance v3, Lcom/policydm/ui/XUIEulaActivity$1;

    invoke-direct {v3, p0}, Lcom/policydm/ui/XUIEulaActivity$1;-><init>(Lcom/policydm/ui/XUIEulaActivity;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    const v3, 0x7f0a0003

    invoke-virtual {p0, v3}, Lcom/policydm/ui/XUIEulaActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .end local v1    # "bConfirm":Landroid/widget/Button;
    check-cast v1, Landroid/widget/Button;

    .line 86
    .restart local v1    # "bConfirm":Landroid/widget/Button;
    new-instance v3, Lcom/policydm/ui/XUIEulaActivity$2;

    invoke-direct {v3, p0}, Lcom/policydm/ui/XUIEulaActivity$2;-><init>(Lcom/policydm/ui/XUIEulaActivity;)V

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 115
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 119
    iget v0, p0, Lcom/policydm/ui/XUIEulaActivity;->mEULAAccept:I

    if-nez v0, :cond_0

    .line 121
    invoke-static {v1}, Lcom/policydm/db/XDB;->xdbSetAutoUpdateFlag(Z)V

    .line 122
    invoke-static {v1}, Lcom/policydm/db/XDBSpdAdp;->xdbSetSpdDeviceRegister(I)V

    .line 123
    const/4 v0, 0x7

    invoke-static {v0}, Lcom/policydm/ui/XUINotificationManager;->xuiSetIndicator(I)V

    .line 124
    const/16 v0, 0xcc

    invoke-static {v0}, Lcom/policydm/XDMApplication;->xdmSendMessageDmHandler(I)V

    .line 126
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 127
    return-void
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 131
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method protected onStart()V
    .locals 0

    .prologue
    .line 43
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 44
    return-void
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 38
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 39
    return-void
.end method
