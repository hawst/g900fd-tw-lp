.class public Lcom/policydm/ui/XUINotificationConnectActivity;
.super Landroid/app/Activity;
.source "XUINotificationConnectActivity.java"

# interfaces
.implements Lcom/policydm/interfaces/XDMInterface;
.implements Lcom/policydm/interfaces/XEventInterface;
.implements Lcom/policydm/interfaces/XNOTIInterface;
.implements Lcom/policydm/interfaces/XUIInterface;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    .line 27
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 29
    invoke-virtual {p0}, Lcom/policydm/ui/XUINotificationConnectActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    .line 30
    .local v2, "szId":Ljava/lang/String;
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 32
    .local v0, "id":I
    packed-switch v0, :pswitch_data_0

    .line 83
    :goto_0
    :pswitch_0
    const/4 v3, 0x0

    invoke-static {v3}, Lcom/policydm/ui/XUINotificationManager;->xuiSetIndicator(I)V

    .line 84
    invoke-virtual {p0}, Lcom/policydm/ui/XUINotificationConnectActivity;->finish()V

    .line 85
    return-void

    .line 35
    :pswitch_1
    const-string v3, "VZW"

    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetCustomCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 36
    const/16 v3, 0xca

    invoke-static {v3}, Lcom/policydm/XDMApplication;->xdmSendMessageDmHandler(I)V

    goto :goto_0

    .line 38
    :cond_0
    const/16 v3, 0xce

    invoke-static {v3}, Lcom/policydm/XDMApplication;->xdmSendMessageDmHandler(I)V

    goto :goto_0

    .line 42
    :pswitch_2
    const/16 v3, 0x78

    invoke-static {v3}, Lcom/policydm/XDMApplication;->xdmSendMessageDmHandler(I)V

    goto :goto_0

    .line 46
    :pswitch_3
    const/16 v3, 0x79

    invoke-static {v3}, Lcom/policydm/XDMApplication;->xdmSendMessageDmHandler(I)V

    goto :goto_0

    .line 50
    :pswitch_4
    const/16 v3, 0x7a

    invoke-static {v3}, Lcom/policydm/XDMApplication;->xdmSendMessageDmHandler(I)V

    goto :goto_0

    .line 54
    :pswitch_5
    invoke-static {}, Lcom/policydm/db/XDB;->xdbGetWifiOnlyFlag()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {}, Lcom/policydm/XDMApplication;->xdmProtoIsWIFIConnected()Z

    move-result v3

    if-nez v3, :cond_1

    .line 55
    const/16 v3, 0x7c

    invoke-static {v3}, Lcom/policydm/XDMApplication;->xdmSendMessageDmHandler(I)V

    goto :goto_0

    .line 58
    :cond_1
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMOInitiatedType()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_2

    .line 60
    invoke-static {}, Lcom/policydm/db/XDBNotiAdp;->xdbNotiGetInfo()Lcom/policydm/db/XDBNotiInfo;

    move-result-object v1

    .line 61
    .local v1, "notiInfo":Lcom/policydm/db/XDBNotiInfo;
    const/4 v3, 0x5

    iput v3, v1, Lcom/policydm/db/XDBNotiInfo;->uiMode:I

    .line 62
    const/16 v3, 0x1f

    invoke-static {v3, v1, v5}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 76
    .end local v1    # "notiInfo":Lcom/policydm/db/XDBNotiInfo;
    :goto_1
    const/16 v3, 0x9

    invoke-static {v3}, Lcom/policydm/ui/XUINotificationManager;->xuiSetIndicator(I)V

    goto :goto_0

    .line 64
    :cond_2
    invoke-static {}, Lcom/policydm/ui/XUIAdapter;->xuiAdpStartSession()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 66
    const/16 v3, 0x65

    invoke-static {v5, v3}, Lcom/policydm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    .line 67
    const-wide/16 v3, 0x0

    invoke-static {v3, v4}, Lcom/policydm/db/XDBProfileListAdp;->xdbSetNotiJobId(J)V

    .line 68
    const/16 v3, 0x67

    invoke-static {v3}, Lcom/policydm/XDMApplication;->xdmSendMessageDmHandler(I)V

    .line 69
    invoke-static {}, Lcom/policydm/noti/XNOTIAdapter;->xnotiPushAdpDeleteAllNotiQueue()V

    goto :goto_1

    .line 73
    :cond_3
    const-string v3, "Unable to connect network"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 32
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method
