.class public Lcom/policydm/ui/XUISPDActivity;
.super Landroid/preference/PreferenceActivity;
.source "XUISPDActivity.java"

# interfaces
.implements Lcom/policydm/interfaces/XEventInterface;
.implements Lcom/policydm/interfaces/XSPDInterface;


# static fields
.field private static final DEVICE_REGISTERD:Ljava/lang/String; = "DevcieRegisterd"

.field private static final ENTERPRISER_MODE:Ljava/lang/String; = "EnterpriseMode"

.field private static final LATEST_VERSION:Ljava/lang/String; = "LatestVersion"

.field private static final SEANDROID_MODE:Ljava/lang/String; = "SEAndroidMode"

.field private static final SPOTA_VERSION:Ljava/lang/String; = "SPotaVersion"

.field private static final SYSTEM_VERSION:Ljava/lang/String; = "SystemVersion"

.field private static m_Builder:Landroid/app/AlertDialog$Builder;

.field private static m_ConnectAlertDialog:Landroid/app/AlertDialog;


# instance fields
.field private device_regi:Landroid/preference/Preference;

.field private enterprise_mode:Landroid/preference/Preference;

.field private latest_Version:Landroid/preference/Preference;

.field private m_Context:Landroid/content/Context;

.field private m_szResponseText:Ljava/lang/String;

.field private seandroid_mode:Landroid/preference/Preference;

.field private spota_version:Landroid/preference/Preference;

.field private system_version:Landroid/preference/Preference;

.field private szPushMsg:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 48
    sput-object v0, Lcom/policydm/ui/XUISPDActivity;->m_Builder:Landroid/app/AlertDialog$Builder;

    .line 49
    sput-object v0, Lcom/policydm/ui/XUISPDActivity;->m_ConnectAlertDialog:Landroid/app/AlertDialog;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 50
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/ui/XUISPDActivity;->m_szResponseText:Ljava/lang/String;

    .line 52
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/ui/XUISPDActivity;->szPushMsg:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/policydm/ui/XUISPDActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/policydm/ui/XUISPDActivity;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/policydm/ui/XUISPDActivity;->m_szResponseText:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$002(Lcom/policydm/ui/XUISPDActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/policydm/ui/XUISPDActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/policydm/ui/XUISPDActivity;->m_szResponseText:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$100(Lcom/policydm/ui/XUISPDActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/policydm/ui/XUISPDActivity;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/policydm/ui/XUISPDActivity;->szPushMsg:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/policydm/ui/XUISPDActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/policydm/ui/XUISPDActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/policydm/ui/XUISPDActivity;->szPushMsg:Ljava/lang/String;

    return-object p1
.end method

.method private callPolicyModeChange()V
    .locals 7

    .prologue
    .line 436
    const/4 v5, 0x2

    new-array v4, v5, [Ljava/lang/CharSequence;

    const/4 v5, 0x0

    const-string v6, "enforcing"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "reset"

    aput-object v6, v4, v5

    .line 439
    .local v4, "type":[Ljava/lang/CharSequence;
    const/4 v2, 0x0

    .line 441
    .local v2, "nFlag":I
    invoke-static {}, Lcom/policydm/adapter/XSPDAdapter;->xspdGetPolicyMode()Ljava/lang/String;

    move-result-object v3

    .line 442
    .local v3, "policymode":Ljava/lang/String;
    const-string v5, "enforcing"

    invoke-virtual {v5, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_1

    .line 443
    const/4 v2, 0x0

    .line 449
    :cond_0
    :goto_0
    const/4 v0, 0x0

    .line 450
    .local v0, "alert":Landroid/app/AlertDialog;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 451
    .local v1, "alt_bld":Landroid/app/AlertDialog$Builder;
    const-string v5, "Policy Mode Change"

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 452
    new-instance v5, Lcom/policydm/ui/XUISPDActivity$13;

    invoke-direct {v5, p0}, Lcom/policydm/ui/XUISPDActivity$13;-><init>(Lcom/policydm/ui/XUISPDActivity;)V

    invoke-virtual {v1, v4, v2, v5}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 471
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 472
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 473
    return-void

    .line 444
    .end local v0    # "alert":Landroid/app/AlertDialog;
    .end local v1    # "alt_bld":Landroid/app/AlertDialog$Builder;
    :cond_1
    const-string v5, "permissive"

    invoke-virtual {v5, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_2

    .line 445
    const/4 v2, 0x1

    goto :goto_0

    .line 446
    :cond_2
    const-string v5, "enterprise"

    invoke-virtual {v5, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_0

    .line 447
    const/4 v2, 0x2

    goto :goto_0
.end method

.method private callPollingTest()V
    .locals 6

    .prologue
    .line 404
    const/4 v4, 0x2

    new-array v3, v4, [Ljava/lang/CharSequence;

    const/4 v4, 0x0

    const-string v5, "DeviceInit Session"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "Get Version.xml"

    aput-object v5, v3, v4

    .line 406
    .local v3, "type":[Ljava/lang/CharSequence;
    const/4 v2, 0x0

    .line 408
    .local v2, "nFlag":I
    const/4 v0, 0x0

    .line 409
    .local v0, "alert":Landroid/app/AlertDialog;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 410
    .local v1, "alt_bld":Landroid/app/AlertDialog$Builder;
    const-string v4, "Polling"

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 411
    new-instance v4, Lcom/policydm/ui/XUISPDActivity$12;

    invoke-direct {v4, p0}, Lcom/policydm/ui/XUISPDActivity$12;-><init>(Lcom/policydm/ui/XUISPDActivity;)V

    invoke-virtual {v1, v3, v2, v4}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 430
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 431
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 432
    return-void
.end method

.method private callPushTest()V
    .locals 6

    .prologue
    .line 367
    const/4 v4, 0x2

    new-array v3, v4, [Ljava/lang/CharSequence;

    const/4 v4, 0x0

    const-string v5, "PUSH Download"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "PUSH Config"

    aput-object v5, v3, v4

    .line 370
    .local v3, "type":[Ljava/lang/CharSequence;
    const/4 v2, 0x0

    .line 372
    .local v2, "nFlag":I
    const/4 v0, 0x0

    .line 373
    .local v0, "alert":Landroid/app/AlertDialog;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 374
    .local v1, "alt_bld":Landroid/app/AlertDialog$Builder;
    const-string v4, "PUSH Download/Config"

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 375
    new-instance v4, Lcom/policydm/ui/XUISPDActivity$11;

    invoke-direct {v4, p0}, Lcom/policydm/ui/XUISPDActivity$11;-><init>(Lcom/policydm/ui/XUISPDActivity;)V

    invoke-virtual {v1, v3, v2, v4}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 398
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 399
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 400
    return-void
.end method

.method private xuiApiTest()V
    .locals 6

    .prologue
    .line 324
    const/4 v4, 0x5

    new-array v3, v4, [Ljava/lang/CharSequence;

    const/4 v4, 0x0

    const-string v5, "Device Create"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "Device Update"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "Device Result"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-string v5, "Device Read"

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string v5, "Get Version.xml"

    aput-object v5, v3, v4

    .line 327
    .local v3, "type":[Ljava/lang/CharSequence;
    const/4 v2, 0x0

    .line 329
    .local v2, "nFlag":I
    const/4 v0, 0x0

    .line 330
    .local v0, "alert":Landroid/app/AlertDialog;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 331
    .local v1, "alt_bld":Landroid/app/AlertDialog$Builder;
    const-string v4, "SPD API Test"

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 332
    new-instance v4, Lcom/policydm/ui/XUISPDActivity$10;

    invoke-direct {v4, p0}, Lcom/policydm/ui/XUISPDActivity$10;-><init>(Lcom/policydm/ui/XUISPDActivity;)V

    invoke-virtual {v1, v3, v2, v4}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 361
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 362
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 363
    return-void
.end method

.method private xuiInputCommandDialog()V
    .locals 7

    .prologue
    .line 188
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 189
    .local v0, "factory":Landroid/view/LayoutInflater;
    const v4, 0x7f030003

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 190
    .local v3, "textEntryView":Landroid/view/View;
    const v4, 0x7f0a0026

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 191
    .local v1, "msg":Landroid/widget/TextView;
    const v4, 0x7f0a0027

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    .line 193
    .local v2, "text":Landroid/widget/EditText;
    const-string v4, "Input command key string"

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 194
    new-instance v4, Lcom/policydm/ui/XUISPDActivity$7;

    invoke-direct {v4, p0}, Lcom/policydm/ui/XUISPDActivity$7;-><init>(Lcom/policydm/ui/XUISPDActivity;)V

    invoke-virtual {v2, v4}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 210
    new-instance v4, Landroid/app/AlertDialog$Builder;

    iget-object v5, p0, Lcom/policydm/ui/XUISPDActivity;->m_Context:Landroid/content/Context;

    invoke-direct {v4, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sput-object v4, Lcom/policydm/ui/XUISPDActivity;->m_Builder:Landroid/app/AlertDialog$Builder;

    .line 211
    sget-object v4, Lcom/policydm/ui/XUISPDActivity;->m_Builder:Landroid/app/AlertDialog$Builder;

    const v5, 0x7f080033

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 212
    sget-object v4, Lcom/policydm/ui/XUISPDActivity;->m_Builder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v4, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f080031

    new-instance v6, Lcom/policydm/ui/XUISPDActivity$8;

    invoke-direct {v6, p0}, Lcom/policydm/ui/XUISPDActivity$8;-><init>(Lcom/policydm/ui/XUISPDActivity;)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 219
    sget-object v4, Lcom/policydm/ui/XUISPDActivity;->m_Builder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    sput-object v4, Lcom/policydm/ui/XUISPDActivity;->m_ConnectAlertDialog:Landroid/app/AlertDialog;

    .line 220
    sget-object v4, Lcom/policydm/ui/XUISPDActivity;->m_ConnectAlertDialog:Landroid/app/AlertDialog;

    new-instance v5, Lcom/policydm/ui/XUISPDActivity$9;

    invoke-direct {v5, p0}, Lcom/policydm/ui/XUISPDActivity$9;-><init>(Lcom/policydm/ui/XUISPDActivity;)V

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 227
    sget-object v4, Lcom/policydm/ui/XUISPDActivity;->m_ConnectAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v4}, Landroid/app/AlertDialog;->show()V

    .line 228
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 77
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 79
    iput-object p0, p0, Lcom/policydm/ui/XUISPDActivity;->m_Context:Landroid/content/Context;

    .line 80
    const/high16 v0, 0x7f040000

    invoke-virtual {p0, v0}, Lcom/policydm/ui/XUISPDActivity;->addPreferencesFromResource(I)V

    .line 82
    const-string v0, "DevcieRegisterd"

    invoke-virtual {p0, v0}, Lcom/policydm/ui/XUISPDActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/policydm/ui/XUISPDActivity;->device_regi:Landroid/preference/Preference;

    .line 83
    iget-object v1, p0, Lcom/policydm/ui/XUISPDActivity;->device_regi:Landroid/preference/Preference;

    invoke-static {}, Lcom/policydm/db/XDBSpdAdp;->xdbGetSpdDeviceRegister()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    const-string v0, "true"

    :goto_0
    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 84
    iget-object v0, p0, Lcom/policydm/ui/XUISPDActivity;->device_regi:Landroid/preference/Preference;

    new-instance v1, Lcom/policydm/ui/XUISPDActivity$1;

    invoke-direct {v1, p0}, Lcom/policydm/ui/XUISPDActivity$1;-><init>(Lcom/policydm/ui/XUISPDActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 92
    const-string v0, "SEAndroidMode"

    invoke-virtual {p0, v0}, Lcom/policydm/ui/XUISPDActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/policydm/ui/XUISPDActivity;->seandroid_mode:Landroid/preference/Preference;

    .line 93
    iget-object v0, p0, Lcom/policydm/ui/XUISPDActivity;->seandroid_mode:Landroid/preference/Preference;

    invoke-static {}, Lcom/policydm/adapter/XSPDAdapter;->xspdGetPolicyMode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 94
    iget-object v0, p0, Lcom/policydm/ui/XUISPDActivity;->seandroid_mode:Landroid/preference/Preference;

    new-instance v1, Lcom/policydm/ui/XUISPDActivity$2;

    invoke-direct {v1, p0}, Lcom/policydm/ui/XUISPDActivity$2;-><init>(Lcom/policydm/ui/XUISPDActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 102
    const-string v0, "EnterpriseMode"

    invoke-virtual {p0, v0}, Lcom/policydm/ui/XUISPDActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/policydm/ui/XUISPDActivity;->enterprise_mode:Landroid/preference/Preference;

    .line 103
    iget-object v1, p0, Lcom/policydm/ui/XUISPDActivity;->enterprise_mode:Landroid/preference/Preference;

    invoke-static {}, Lcom/policydm/adapter/XSPDAdapter;->xspdIsEnterprisePolicy()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "activate"

    :goto_1
    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 104
    iget-object v0, p0, Lcom/policydm/ui/XUISPDActivity;->enterprise_mode:Landroid/preference/Preference;

    new-instance v1, Lcom/policydm/ui/XUISPDActivity$3;

    invoke-direct {v1, p0}, Lcom/policydm/ui/XUISPDActivity$3;-><init>(Lcom/policydm/ui/XUISPDActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 112
    const-string v0, "SystemVersion"

    invoke-virtual {p0, v0}, Lcom/policydm/ui/XUISPDActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/policydm/ui/XUISPDActivity;->system_version:Landroid/preference/Preference;

    .line 113
    iget-object v0, p0, Lcom/policydm/ui/XUISPDActivity;->system_version:Landroid/preference/Preference;

    invoke-static {}, Lcom/policydm/adapter/XSPDAdapter;->xspdGetSystemPolicyVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 114
    iget-object v0, p0, Lcom/policydm/ui/XUISPDActivity;->system_version:Landroid/preference/Preference;

    new-instance v1, Lcom/policydm/ui/XUISPDActivity$4;

    invoke-direct {v1, p0}, Lcom/policydm/ui/XUISPDActivity$4;-><init>(Lcom/policydm/ui/XUISPDActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 122
    const-string v0, "SPotaVersion"

    invoke-virtual {p0, v0}, Lcom/policydm/ui/XUISPDActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/policydm/ui/XUISPDActivity;->spota_version:Landroid/preference/Preference;

    .line 123
    iget-object v0, p0, Lcom/policydm/ui/XUISPDActivity;->spota_version:Landroid/preference/Preference;

    invoke-static {}, Lcom/policydm/adapter/XSPDAdapter;->xspdGetSPotaPolicyVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 124
    iget-object v0, p0, Lcom/policydm/ui/XUISPDActivity;->spota_version:Landroid/preference/Preference;

    new-instance v1, Lcom/policydm/ui/XUISPDActivity$5;

    invoke-direct {v1, p0}, Lcom/policydm/ui/XUISPDActivity$5;-><init>(Lcom/policydm/ui/XUISPDActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 132
    const-string v0, "LatestVersion"

    invoke-virtual {p0, v0}, Lcom/policydm/ui/XUISPDActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/policydm/ui/XUISPDActivity;->latest_Version:Landroid/preference/Preference;

    .line 133
    iget-object v0, p0, Lcom/policydm/ui/XUISPDActivity;->latest_Version:Landroid/preference/Preference;

    invoke-static {}, Lcom/policydm/adapter/XSPDAdapter;->xspdGetLatestPolicyVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 134
    iget-object v0, p0, Lcom/policydm/ui/XUISPDActivity;->latest_Version:Landroid/preference/Preference;

    new-instance v1, Lcom/policydm/ui/XUISPDActivity$6;

    invoke-direct {v1, p0}, Lcom/policydm/ui/XUISPDActivity$6;-><init>(Lcom/policydm/ui/XUISPDActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 141
    return-void

    .line 83
    :cond_0
    const-string v0, "false"

    goto/16 :goto_0

    .line 103
    :cond_1
    const-string v0, "deactivate"

    goto :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v3, 0x1080042

    const/4 v2, 0x0

    .line 163
    const-string v0, "Input MasterKey"

    invoke-interface {p1, v2, v2, v2, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 164
    const/4 v0, 0x1

    const-string v1, "refresh"

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 166
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 172
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 183
    :goto_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 175
    :pswitch_0
    invoke-direct {p0}, Lcom/policydm/ui/XUISPDActivity;->xuiInputCommandDialog()V

    goto :goto_0

    .line 178
    :pswitch_1
    invoke-virtual {p0}, Lcom/policydm/ui/XUISPDActivity;->refresh()V

    goto :goto_0

    .line 172
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 146
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    .line 148
    invoke-virtual {p0}, Lcom/policydm/ui/XUISPDActivity;->refresh()V

    .line 149
    return-void
.end method

.method protected onStart()V
    .locals 0

    .prologue
    .line 71
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onStart()V

    .line 72
    return-void
.end method

.method public refresh()V
    .locals 3

    .prologue
    .line 153
    iget-object v1, p0, Lcom/policydm/ui/XUISPDActivity;->device_regi:Landroid/preference/Preference;

    invoke-static {}, Lcom/policydm/db/XDBSpdAdp;->xdbGetSpdDeviceRegister()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    const-string v0, "true"

    :goto_0
    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 154
    iget-object v0, p0, Lcom/policydm/ui/XUISPDActivity;->seandroid_mode:Landroid/preference/Preference;

    invoke-static {}, Lcom/policydm/adapter/XSPDAdapter;->xspdGetPolicyMode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 155
    iget-object v1, p0, Lcom/policydm/ui/XUISPDActivity;->enterprise_mode:Landroid/preference/Preference;

    invoke-static {}, Lcom/policydm/adapter/XSPDAdapter;->xspdIsEnterprisePolicy()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "activate"

    :goto_1
    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 156
    iget-object v0, p0, Lcom/policydm/ui/XUISPDActivity;->spota_version:Landroid/preference/Preference;

    invoke-static {}, Lcom/policydm/adapter/XSPDAdapter;->xspdGetSPotaPolicyVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 157
    iget-object v0, p0, Lcom/policydm/ui/XUISPDActivity;->latest_Version:Landroid/preference/Preference;

    invoke-static {}, Lcom/policydm/adapter/XSPDAdapter;->xspdGetLatestPolicyVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 158
    return-void

    .line 153
    :cond_0
    const-string v0, "false"

    goto :goto_0

    .line 155
    :cond_1
    const-string v0, "deactivate"

    goto :goto_1
.end method

.method protected xuiSelectTestMenu(Ljava/lang/String;)V
    .locals 12
    .param p1, "szInputKey"    # Ljava/lang/String;

    .prologue
    .line 232
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "key event occured on Dialog : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 233
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 320
    :cond_0
    :goto_0
    return-void

    .line 236
    :cond_1
    const-string v10, "reset"

    invoke-virtual {v10, p1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v10

    if-nez v10, :cond_2

    .line 238
    const-string v10, "Reset and Kill Client"

    const/4 v11, 0x0

    invoke-static {p0, v10, v11}, Lcom/policydm/XDMApplication;->xdmShowToast(Landroid/content/Context;Ljava/lang/String;I)V

    .line 239
    invoke-static {}, Lcom/policydm/db/XDB;->xdbFullResetAll()V

    .line 240
    const/4 v10, 0x0

    invoke-static {v10}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOStatus(I)V

    .line 241
    const/4 v10, 0x0

    invoke-static {v10}, Lcom/policydm/ui/XUIAdapter;->xuiAdpSetUiMode(I)V

    .line 242
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v10

    invoke-static {v10}, Landroid/os/Process;->killProcess(I)V

    goto :goto_0

    .line 244
    :cond_2
    const-string v10, "privatelog"

    invoke-virtual {v10, p1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v10

    if-nez v10, :cond_3

    .line 246
    invoke-static {}, Lcom/policydm/agent/XDMDebug;->xdmSetPrivateLogOnOff()V

    goto :goto_0

    .line 248
    :cond_3
    const-string v10, "setting"

    invoke-virtual {v10, p1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v10

    if-nez v10, :cond_4

    .line 250
    new-instance v1, Landroid/content/Intent;

    const-class v10, Lcom/policydm/ui/XUIMainActivity;

    invoke-direct {v1, p0, v10}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 251
    .local v1, "i":Landroid/content/Intent;
    const/high16 v10, 0x34000000

    invoke-virtual {v1, v10}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 252
    invoke-virtual {p0, v1}, Lcom/policydm/ui/XUISPDActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 254
    .end local v1    # "i":Landroid/content/Intent;
    :cond_4
    const-string v10, "api"

    invoke-virtual {v10, p1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v10

    if-nez v10, :cond_5

    .line 256
    invoke-direct {p0}, Lcom/policydm/ui/XUISPDActivity;->xuiApiTest()V

    goto :goto_0

    .line 258
    :cond_5
    const-string v10, "pull"

    invoke-virtual {v10, p1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v10

    if-nez v10, :cond_6

    .line 260
    new-instance v1, Landroid/content/Intent;

    const-string v10, "com.policydm.intent.action.PULL_RECEIVE"

    invoke-direct {v1, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 261
    .restart local v1    # "i":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/policydm/ui/XUISPDActivity;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 263
    .end local v1    # "i":Landroid/content/Intent;
    :cond_6
    const-string v10, "push"

    invoke-virtual {v10, p1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v10

    if-nez v10, :cond_7

    .line 265
    invoke-direct {p0}, Lcom/policydm/ui/XUISPDActivity;->callPushTest()V

    goto :goto_0

    .line 267
    :cond_7
    const-string v10, "polling"

    invoke-virtual {v10, p1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v10

    if-nez v10, :cond_8

    .line 269
    invoke-direct {p0}, Lcom/policydm/ui/XUISPDActivity;->callPollingTest()V

    goto :goto_0

    .line 271
    :cond_8
    const-string v10, "pollingtime"

    invoke-virtual {v10, p1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v10

    if-nez v10, :cond_b

    .line 273
    invoke-static {}, Lcom/policydm/db/XDBPollingAdp;->xdbGetPollingNextTime()J

    move-result-wide v2

    .line 274
    .local v2, "nexttime":J
    const-wide/16 v10, 0x0

    cmp-long v10, v2, v10

    if-eqz v10, :cond_9

    .line 276
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v10, "yyyy/MM/dd/HH:mm:ss"

    sget-object v11, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v10, v11}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 277
    .local v0, "df":Ljava/text/DateFormat;
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 278
    .local v6, "szNexttime":Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "next polling time : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 285
    .end local v0    # "df":Ljava/text/DateFormat;
    .end local v6    # "szNexttime":Ljava/lang/String;
    :goto_1
    invoke-static {}, Lcom/policydm/db/XDBPollingAdp;->xdbGetPollingStatusReportTime()J

    move-result-wide v4

    .line 286
    .local v4, "reporttime":J
    const-wide/16 v10, 0x0

    cmp-long v10, v4, v10

    if-eqz v10, :cond_a

    .line 288
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v10, "yyyy/MM/dd/HH:mm:ss"

    sget-object v11, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v10, v11}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 289
    .restart local v0    # "df":Ljava/text/DateFormat;
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 290
    .local v7, "szReporttime":Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "status report polling time : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 282
    .end local v0    # "df":Ljava/text/DateFormat;
    .end local v4    # "reporttime":J
    .end local v7    # "szReporttime":Ljava/lang/String;
    :cond_9
    const-string v10, "next polling time : 0 "

    invoke-static {v10}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_1

    .line 294
    .restart local v4    # "reporttime":J
    :cond_a
    const-string v10, "status report polling time : 0 "

    invoke-static {v10}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 297
    .end local v2    # "nexttime":J
    .end local v4    # "reporttime":J
    :cond_b
    const-string v10, "eulatime"

    invoke-virtual {v10, p1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v10

    if-nez v10, :cond_d

    .line 299
    invoke-static {}, Lcom/policydm/db/XDBSpdAdp;->xdbGetSpdEulaTime()J

    move-result-wide v8

    .line 300
    .local v8, "time":J
    const-wide/16 v10, 0x0

    cmp-long v10, v8, v10

    if-eqz v10, :cond_c

    .line 302
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v10, "yyyy/MM/dd/HH:mm:ss"

    sget-object v11, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v10, v11}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 303
    .restart local v0    # "df":Ljava/text/DateFormat;
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 304
    .restart local v6    # "szNexttime":Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "eula time : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 308
    .end local v0    # "df":Ljava/text/DateFormat;
    .end local v6    # "szNexttime":Ljava/lang/String;
    :cond_c
    const-string v10, "eula time : 0 "

    invoke-static {v10}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 311
    .end local v8    # "time":J
    :cond_d
    const-string v10, "mode"

    invoke-virtual {v10, p1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v10

    if-nez v10, :cond_e

    .line 313
    invoke-direct {p0}, Lcom/policydm/ui/XUISPDActivity;->callPolicyModeChange()V

    goto/16 :goto_0

    .line 315
    :cond_e
    const-string v10, "file"

    invoke-virtual {v10, p1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v10

    if-nez v10, :cond_0

    .line 317
    invoke-static {}, Lcom/policydm/spd/XSPDAgent;->xspdApplySPotaPolicy()I

    .line 318
    invoke-static {}, Lcom/policydm/spd/XSPDAgent;->xspdRemoveSPotaPolicy()V

    goto/16 :goto_0
.end method
