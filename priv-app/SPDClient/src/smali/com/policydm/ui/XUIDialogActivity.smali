.class public Lcom/policydm/ui/XUIDialogActivity;
.super Landroid/app/Activity;
.source "XUIDialogActivity.java"

# interfaces
.implements Lcom/policydm/interfaces/XDMInterface;
.implements Lcom/policydm/interfaces/XEventInterface;
.implements Lcom/policydm/interfaces/XUICInterface;
.implements Lcom/policydm/interfaces/XUIInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/policydm/ui/XUIDialogActivity$XUICloseTimerTask;
    }
.end annotation


# static fields
.field private static final SECURE_ANDROID_URL:Ljava/lang/String; = "http://"

.field private static final SECURE_ANDROID_URL_TEXT:Ljava/lang/String; = "www.samsung.com/seforandroid"

.field private static final SECURE_ANDROID_URL_VZW:Ljava/lang/String; = "www.samsung.com/us/secureandroid"

.field public static g_UicOption:Ljava/lang/Object;

.field private static m_DialogActivity:Landroid/app/Activity;

.field private static m_DialogId:I

.field private static m_UicResult:Ljava/lang/Object;


# instance fields
.field private final POLICY_FILE_SIZE:I

.field private mLayourFillWrap:Landroid/view/ViewGroup$LayoutParams;

.field private m_UicPrgDialog:Landroid/app/ProgressDialog;

.field private m_UicTimer:Ljava/util/Timer;

.field private m_bDRMultiSelected:[Z

.field private m_nDRSingleSelected:I

.field private m_szDefaultResponseText:Ljava/lang/String;

.field private m_szResponseText:Ljava/lang/String;

.field private m_szUicConformText:Ljava/lang/String;

.field private m_szUicMenuList:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x0

    sput v0, Lcom/policydm/ui/XUIDialogActivity;->m_DialogId:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 62
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 74
    iput-object v0, p0, Lcom/policydm/ui/XUIDialogActivity;->m_UicTimer:Ljava/util/Timer;

    .line 79
    iput-object v0, p0, Lcom/policydm/ui/XUIDialogActivity;->mLayourFillWrap:Landroid/view/ViewGroup$LayoutParams;

    .line 84
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/policydm/ui/XUIDialogActivity;->POLICY_FILE_SIZE:I

    .line 87
    return-void
.end method

.method static synthetic access$000(Lcom/policydm/ui/XUIDialogActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/policydm/ui/XUIDialogActivity;

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/policydm/ui/XUIDialogActivity;->xuiDlgCPopupKillTimer()V

    return-void
.end method

.method static synthetic access$100(Lcom/policydm/ui/XUIDialogActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/policydm/ui/XUIDialogActivity;

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/policydm/ui/XUIDialogActivity;->xuiDlgUicDisplayResult()V

    return-void
.end method

.method static synthetic access$202(Lcom/policydm/ui/XUIDialogActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/policydm/ui/XUIDialogActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/policydm/ui/XUIDialogActivity;->m_szResponseText:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/policydm/ui/XUIDialogActivity;->m_UicResult:Ljava/lang/Object;

    return-object v0
.end method

.method private xuiDlgCPopupKillTimer()V
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, Lcom/policydm/ui/XUIDialogActivity;->m_UicTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 309
    iget-object v0, p0, Lcom/policydm/ui/XUIDialogActivity;->m_UicTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 311
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/policydm/ui/XUIDialogActivity;->m_UicTimer:Ljava/util/Timer;

    .line 312
    return-void
.end method

.method private xuiDlgPopup(I)V
    .locals 6
    .param p1, "type"    # I

    .prologue
    .line 270
    sget-object v2, Lcom/policydm/ui/XUIDialogActivity;->g_UicOption:Ljava/lang/Object;

    check-cast v2, Lcom/policydm/eng/core/XDMUicOption;

    .line 271
    .local v2, "pUicOption":Lcom/policydm/eng/core/XDMUicOption;
    const/4 v0, 0x0

    .line 273
    .local v0, "displayTime":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "xuiUICPopup type"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 276
    iget v3, v2, Lcom/policydm/eng/core/XDMUicOption;->minDT:I

    if-lez v3, :cond_0

    .line 278
    iget v3, v2, Lcom/policydm/eng/core/XDMUicOption;->minDT:I

    mul-int/lit16 v0, v3, 0x3e8

    .line 281
    :cond_0
    iget v3, v2, Lcom/policydm/eng/core/XDMUicOption;->maxDT:I

    if-lez v3, :cond_1

    .line 283
    iget v3, v2, Lcom/policydm/eng/core/XDMUicOption;->maxDT:I

    mul-int/lit16 v0, v3, 0x3e8

    .line 286
    :cond_1
    iget-object v3, p0, Lcom/policydm/ui/XUIDialogActivity;->m_UicTimer:Ljava/util/Timer;

    if-nez v3, :cond_3

    .line 288
    if-lez v0, :cond_2

    .line 290
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "--> display time is :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 291
    new-instance v1, Lcom/policydm/ui/XUIDialogActivity$XUICloseTimerTask;

    add-int/lit8 v3, p1, 0x6e

    invoke-direct {v1, p0, v3}, Lcom/policydm/ui/XUIDialogActivity$XUICloseTimerTask;-><init>(Lcom/policydm/ui/XUIDialogActivity;I)V

    .line 292
    .local v1, "mytime":Lcom/policydm/ui/XUIDialogActivity$XUICloseTimerTask;
    new-instance v3, Ljava/util/Timer;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Ljava/util/Timer;-><init>(Z)V

    iput-object v3, p0, Lcom/policydm/ui/XUIDialogActivity;->m_UicTimer:Ljava/util/Timer;

    .line 293
    iget-object v3, p0, Lcom/policydm/ui/XUIDialogActivity;->m_UicTimer:Ljava/util/Timer;

    int-to-long v4, v0

    invoke-virtual {v3, v1, v4, v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 300
    .end local v1    # "mytime":Lcom/policydm/ui/XUIDialogActivity$XUICloseTimerTask;
    :cond_2
    :goto_0
    iget-object v3, v2, Lcom/policydm/eng/core/XDMUicOption;->defaultResponse:Lcom/policydm/eng/core/XDMText;

    iget-object v3, v3, Lcom/policydm/eng/core/XDMText;->text:Ljava/lang/String;

    iput-object v3, p0, Lcom/policydm/ui/XUIDialogActivity;->m_szDefaultResponseText:Ljava/lang/String;

    .line 303
    add-int/lit8 v3, p1, 0x6e

    invoke-virtual {p0, v3}, Lcom/policydm/ui/XUIDialogActivity;->showDialog(I)V

    .line 304
    return-void

    .line 298
    :cond_3
    const-string v3, "mUicTimer is running or already started!!"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xuiDlgRemove()V
    .locals 2

    .prologue
    .line 1257
    :try_start_0
    sget-object v1, Lcom/policydm/ui/XUIDialogActivity;->m_DialogActivity:Landroid/app/Activity;

    if-eqz v1, :cond_0

    .line 1259
    const/4 v1, 0x0

    sput v1, Lcom/policydm/ui/XUIDialogActivity;->m_DialogId:I

    .line 1260
    const-string v1, "DialogActvity Remove and reset mDialogId"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1261
    sget-object v1, Lcom/policydm/ui/XUIDialogActivity;->m_DialogActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1268
    .local v0, "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    return-void

    .line 1264
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_0
    move-exception v0

    .line 1266
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private xuiDlgUicDisplayResult()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 316
    sget-object v1, Lcom/policydm/ui/XUIDialogActivity;->m_UicResult:Ljava/lang/Object;

    check-cast v1, Lcom/policydm/eng/core/XDMUicResult;

    .line 317
    .local v1, "uicResult":Lcom/policydm/eng/core/XDMUicResult;
    const/4 v0, 0x0

    .line 318
    .local v0, "pUicResult":Lcom/policydm/eng/core/XDMUicResult;
    if-nez v1, :cond_0

    .line 329
    :goto_0
    return-void

    .line 321
    :cond_0
    const/4 v2, 0x1

    iput v2, v1, Lcom/policydm/eng/core/XDMUicResult;->result:I

    .line 323
    invoke-static {}, Lcom/policydm/eng/core/XDMUic;->xdmUicCreateResult()Lcom/policydm/eng/core/XDMUicResult;

    move-result-object v0

    .line 324
    invoke-static {v0, v1}, Lcom/policydm/eng/core/XDMUic;->xdmUicCopyResult(Lcom/policydm/eng/core/XDMUicResult;Lcom/policydm/eng/core/XDMUicResult;)Lcom/policydm/eng/core/XDMUicResult;

    move-result-object v0

    .line 326
    const/16 v2, 0x15

    invoke-static {v2, v0, v3}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 327
    invoke-static {v1}, Lcom/policydm/eng/core/XDMUic;->xdmUicFreeResult(Lcom/policydm/eng/core/XDMUicResult;)Ljava/lang/Object;

    .line 328
    sput-object v3, Lcom/policydm/ui/XUIDialogActivity;->m_UicResult:Ljava/lang/Object;

    goto :goto_0
.end method

.method private xuiDlgUicProgressResult(Ljava/lang/Object;)V
    .locals 1
    .param p1, "pThis"    # Ljava/lang/Object;

    .prologue
    .line 444
    move-object v0, p1

    check-cast v0, Lcom/policydm/eng/core/XDMUicOption;

    .line 446
    .local v0, "pUicOption":Lcom/policydm/eng/core/XDMUicOption;
    invoke-static {v0}, Lcom/policydm/eng/core/XDMUic;->xdmUicFreeUicOption(Lcom/policydm/eng/core/XDMUicOption;)Lcom/policydm/eng/core/XDMUicOption;

    .line 447
    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 0
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 1202
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 1204
    invoke-static {}, Lcom/policydm/ui/XUIDialogActivity;->xuiDlgRemove()V

    .line 1205
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 1161
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 1163
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iput-object v1, p0, Lcom/policydm/ui/XUIDialogActivity;->mLayourFillWrap:Landroid/view/ViewGroup$LayoutParams;

    .line 1165
    sput-object p0, Lcom/policydm/ui/XUIDialogActivity;->m_DialogActivity:Landroid/app/Activity;

    .line 1166
    invoke-virtual {p0}, Lcom/policydm/ui/XUIDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1167
    .local v0, "sid":Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sput v1, Lcom/policydm/ui/XUIDialogActivity;->m_DialogId:I

    .line 1168
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 29
    .param p1, "id"    # I

    .prologue
    .line 452
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmTablet()Z

    move-result v24

    if-eqz v24, :cond_0

    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmWhite()Z

    move-result v24

    if-nez v24, :cond_1

    :cond_0
    const-string v24, "ro.build.scafe.cream"

    invoke-static/range {v24 .. v24}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    const-string v25, "white"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v24

    if-eqz v24, :cond_2

    .line 454
    :cond_1
    const v24, 0x7f090003

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/policydm/ui/XUIDialogActivity;->setTheme(I)V

    .line 457
    :cond_2
    sparse-switch p1, :sswitch_data_0

    .line 1137
    invoke-super/range {p0 .. p1}, Landroid/app/Activity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v14

    :goto_0
    return-object v14

    .line 460
    :sswitch_0
    const/16 v24, 0x1

    invoke-static/range {v24 .. v24}, Lcom/policydm/ui/XUIAdapter;->xuiAdpSetUiMode(I)V

    .line 461
    new-instance v14, Landroid/app/ProgressDialog;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 462
    .local v14, "prgDialog":Landroid/app/ProgressDialog;
    const v24, 0x7f080033

    move/from16 v0, v24

    invoke-virtual {v14, v0}, Landroid/app/ProgressDialog;->setTitle(I)V

    .line 463
    const v24, 0x7f08000a

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/policydm/ui/XUIDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v14, v0}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 464
    const/16 v24, 0x0

    move/from16 v0, v24

    invoke-virtual {v14, v0}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 465
    new-instance v24, Lcom/policydm/ui/XUIDialogActivity$1;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/policydm/ui/XUIDialogActivity$1;-><init>(Lcom/policydm/ui/XUIDialogActivity;)V

    move-object/from16 v0, v24

    invoke-virtual {v14, v0}, Landroid/app/ProgressDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    goto :goto_0

    .line 479
    .end local v14    # "prgDialog":Landroid/app/ProgressDialog;
    :sswitch_1
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v24

    const v25, 0x7f080042

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/policydm/ui/XUIDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v25

    const/16 v26, 0x1

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    const/16 v28, 0x1f4

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    aput-object v28, v26, v27

    invoke-static/range {v24 .. v26}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    .line 480
    .local v16, "str":Ljava/lang/String;
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmCheckCHNProduct()Z

    move-result v24

    if-eqz v24, :cond_3

    .line 481
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v24

    const v25, 0x7f080043

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/policydm/ui/XUIDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v25

    const/16 v26, 0x1

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    const/16 v28, 0x1f4

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    aput-object v28, v26, v27

    invoke-static/range {v24 .. v26}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    .line 482
    :cond_3
    new-instance v24, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v25, 0x7f08000b

    invoke-virtual/range {v24 .. v25}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    const v25, 0x7f080031

    new-instance v26, Lcom/policydm/ui/XUIDialogActivity$5;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/policydm/ui/XUIDialogActivity$5;-><init>(Lcom/policydm/ui/XUIDialogActivity;)V

    invoke-virtual/range {v24 .. v26}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    const v25, 0x7f080006

    new-instance v26, Lcom/policydm/ui/XUIDialogActivity$4;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/policydm/ui/XUIDialogActivity$4;-><init>(Lcom/policydm/ui/XUIDialogActivity;)V

    invoke-virtual/range {v24 .. v26}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    new-instance v25, Lcom/policydm/ui/XUIDialogActivity$3;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/policydm/ui/XUIDialogActivity$3;-><init>(Lcom/policydm/ui/XUIDialogActivity;)V

    invoke-virtual/range {v24 .. v25}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    new-instance v25, Lcom/policydm/ui/XUIDialogActivity$2;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/policydm/ui/XUIDialogActivity$2;-><init>(Lcom/policydm/ui/XUIDialogActivity;)V

    invoke-virtual/range {v24 .. v25}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v14

    goto/16 :goto_0

    .line 563
    .end local v16    # "str":Ljava/lang/String;
    :sswitch_2
    const v19, 0x7f080018

    .line 564
    .local v19, "textId":I
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmCheckNAProduct()Z

    move-result v24

    if-eqz v24, :cond_4

    .line 566
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmTablet()Z

    move-result v24

    if-eqz v24, :cond_7

    .line 567
    const v19, 0x7f08001a

    .line 571
    :cond_4
    :goto_1
    const-string v24, "ATT"

    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetCustomCode()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_5

    .line 573
    const v19, 0x7f080019

    .line 575
    :cond_5
    const v7, 0x7f080026

    .line 576
    .local v7, "buttonId":I
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmKorModel()Z

    move-result v24

    if-eqz v24, :cond_6

    .line 577
    const v7, 0x7f08002e

    .line 578
    :cond_6
    new-instance v24, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v25, 0x7f080033

    invoke-virtual/range {v24 .. v25}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    move-object/from16 v0, v24

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    new-instance v25, Lcom/policydm/ui/XUIDialogActivity$9;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/policydm/ui/XUIDialogActivity$9;-><init>(Lcom/policydm/ui/XUIDialogActivity;)V

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v0, v7, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    const v25, 0x7f080006

    new-instance v26, Lcom/policydm/ui/XUIDialogActivity$8;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/policydm/ui/XUIDialogActivity$8;-><init>(Lcom/policydm/ui/XUIDialogActivity;)V

    invoke-virtual/range {v24 .. v26}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    new-instance v25, Lcom/policydm/ui/XUIDialogActivity$7;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/policydm/ui/XUIDialogActivity$7;-><init>(Lcom/policydm/ui/XUIDialogActivity;)V

    invoke-virtual/range {v24 .. v25}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    new-instance v25, Lcom/policydm/ui/XUIDialogActivity$6;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/policydm/ui/XUIDialogActivity$6;-><init>(Lcom/policydm/ui/XUIDialogActivity;)V

    invoke-virtual/range {v24 .. v25}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v14

    goto/16 :goto_0

    .line 569
    .end local v7    # "buttonId":I
    :cond_7
    const v19, 0x7f080017

    goto :goto_1

    .line 619
    .end local v19    # "textId":I
    :sswitch_3
    const v19, 0x7f080011

    .line 620
    .restart local v19    # "textId":I
    const-string v24, "VZW"

    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetCustomCode()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-nez v24, :cond_9

    .line 624
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->isHmodel()Z

    move-result v24

    if-eqz v24, :cond_8

    .line 625
    const v19, 0x7f080015

    .line 626
    :cond_8
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->isJmodel()Z

    move-result v24

    if-eqz v24, :cond_9

    .line 627
    const v19, 0x7f080016

    .line 629
    :cond_9
    new-instance v24, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v25, 0x7f080033

    invoke-virtual/range {v24 .. v25}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    move-object/from16 v0, v24

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    const v25, 0x7f080031

    new-instance v26, Lcom/policydm/ui/XUIDialogActivity$12;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/policydm/ui/XUIDialogActivity$12;-><init>(Lcom/policydm/ui/XUIDialogActivity;)V

    invoke-virtual/range {v24 .. v26}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    new-instance v25, Lcom/policydm/ui/XUIDialogActivity$11;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/policydm/ui/XUIDialogActivity$11;-><init>(Lcom/policydm/ui/XUIDialogActivity;)V

    invoke-virtual/range {v24 .. v25}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    new-instance v25, Lcom/policydm/ui/XUIDialogActivity$10;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/policydm/ui/XUIDialogActivity$10;-><init>(Lcom/policydm/ui/XUIDialogActivity;)V

    invoke-virtual/range {v24 .. v25}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v14

    goto/16 :goto_0

    .line 657
    .end local v19    # "textId":I
    :sswitch_4
    const v19, 0x7f08001b

    .line 658
    .restart local v19    # "textId":I
    const-string v24, "VZW"

    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetCustomCode()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-nez v24, :cond_b

    .line 662
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->isHmodel()Z

    move-result v24

    if-eqz v24, :cond_a

    .line 663
    const v19, 0x7f08001f

    .line 664
    :cond_a
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->isJmodel()Z

    move-result v24

    if-eqz v24, :cond_b

    .line 665
    const v19, 0x7f080020

    .line 667
    :cond_b
    new-instance v24, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v25, 0x7f080033

    invoke-virtual/range {v24 .. v25}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    move-object/from16 v0, v24

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    const v25, 0x7f080031

    new-instance v26, Lcom/policydm/ui/XUIDialogActivity$15;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/policydm/ui/XUIDialogActivity$15;-><init>(Lcom/policydm/ui/XUIDialogActivity;)V

    invoke-virtual/range {v24 .. v26}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    new-instance v25, Lcom/policydm/ui/XUIDialogActivity$14;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/policydm/ui/XUIDialogActivity$14;-><init>(Lcom/policydm/ui/XUIDialogActivity;)V

    invoke-virtual/range {v24 .. v25}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    new-instance v25, Lcom/policydm/ui/XUIDialogActivity$13;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/policydm/ui/XUIDialogActivity$13;-><init>(Lcom/policydm/ui/XUIDialogActivity;)V

    invoke-virtual/range {v24 .. v25}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v14

    goto/16 :goto_0

    .line 695
    .end local v19    # "textId":I
    :sswitch_5
    const v24, 0x7f08002a

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/policydm/ui/XUIDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v24

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    const-string v27, "www.samsung.com/seforandroid"

    aput-object v27, v25, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 696
    .local v6, "body":Ljava/lang/String;
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmCheckNAProduct()Z

    move-result v24

    if-nez v24, :cond_c

    .line 697
    const v24, 0x7f080028

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/policydm/ui/XUIDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v24

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    const-string v27, "www.samsung.com/seforandroid"

    aput-object v27, v25, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 698
    :cond_c
    const-string v24, "VZW"

    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetCustomCode()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_d

    .line 699
    const v24, 0x7f080028

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/policydm/ui/XUIDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v24

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    const-string v27, "www.samsung.com/us/secureandroid"

    aput-object v27, v25, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 700
    :cond_d
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmCheckCHNProduct()Z

    move-result v24

    if-eqz v24, :cond_e

    .line 701
    const v24, 0x7f080029

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/policydm/ui/XUIDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v24

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    const-string v27, "www.samsung.com/seforandroid"

    aput-object v27, v25, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 702
    :cond_e
    new-instance v15, Landroid/text/SpannableString;

    invoke-direct {v15, v6}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 703
    .local v15, "s":Landroid/text/SpannableString;
    const-string v24, "www.samsung.com/seforandroid"

    invoke-static/range {v24 .. v24}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v13

    .line 704
    .local v13, "p":Ljava/util/regex/Pattern;
    const-string v24, "VZW"

    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetCustomCode()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_f

    .line 705
    const-string v24, "www.samsung.com/us/secureandroid"

    invoke-static/range {v24 .. v24}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v13

    .line 706
    :cond_f
    const-string v24, "http://"

    move-object/from16 v0, v24

    invoke-static {v15, v13, v0}, Landroid/text/util/Linkify;->addLinks(Landroid/text/Spannable;Ljava/util/regex/Pattern;Ljava/lang/String;)Z

    .line 707
    new-instance v24, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v25, 0x7f080050

    invoke-virtual/range {v24 .. v25}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v15}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    const v25, 0x7f080031

    new-instance v26, Lcom/policydm/ui/XUIDialogActivity$18;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/policydm/ui/XUIDialogActivity$18;-><init>(Lcom/policydm/ui/XUIDialogActivity;)V

    invoke-virtual/range {v24 .. v26}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    new-instance v25, Lcom/policydm/ui/XUIDialogActivity$17;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/policydm/ui/XUIDialogActivity$17;-><init>(Lcom/policydm/ui/XUIDialogActivity;)V

    invoke-virtual/range {v24 .. v25}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    new-instance v25, Lcom/policydm/ui/XUIDialogActivity$16;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/policydm/ui/XUIDialogActivity$16;-><init>(Lcom/policydm/ui/XUIDialogActivity;)V

    invoke-virtual/range {v24 .. v25}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v14

    goto/16 :goto_0

    .line 735
    .end local v6    # "body":Ljava/lang/String;
    .end local v13    # "p":Ljava/util/regex/Pattern;
    .end local v15    # "s":Landroid/text/SpannableString;
    :sswitch_6
    const v24, 0x7f08004f

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/policydm/ui/XUIDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v24

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    const-string v27, "www.samsung.com/seforandroid"

    aput-object v27, v25, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 736
    .restart local v6    # "body":Ljava/lang/String;
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmCheckNAProduct()Z

    move-result v24

    if-nez v24, :cond_10

    .line 737
    const v24, 0x7f08004e

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/policydm/ui/XUIDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v24

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    const-string v27, "www.samsung.com/seforandroid"

    aput-object v27, v25, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 738
    :cond_10
    const-string v24, "VZW"

    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetCustomCode()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_11

    .line 739
    const v24, 0x7f08004e

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/policydm/ui/XUIDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v24

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    const-string v27, "www.samsung.com/us/secureandroid"

    aput-object v27, v25, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 740
    :cond_11
    new-instance v15, Landroid/text/SpannableString;

    invoke-direct {v15, v6}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 741
    .restart local v15    # "s":Landroid/text/SpannableString;
    const-string v24, "www.samsung.com/seforandroid"

    invoke-static/range {v24 .. v24}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v13

    .line 742
    .restart local v13    # "p":Ljava/util/regex/Pattern;
    const-string v24, "VZW"

    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetCustomCode()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_12

    .line 743
    const-string v24, "www.samsung.com/us/secureandroid"

    invoke-static/range {v24 .. v24}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v13

    .line 744
    :cond_12
    const-string v24, "http://"

    move-object/from16 v0, v24

    invoke-static {v15, v13, v0}, Landroid/text/util/Linkify;->addLinks(Landroid/text/Spannable;Ljava/util/regex/Pattern;Ljava/lang/String;)Z

    .line 745
    new-instance v24, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v25, 0x7f080050

    invoke-virtual/range {v24 .. v25}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v15}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    const v25, 0x7f080031

    new-instance v26, Lcom/policydm/ui/XUIDialogActivity$21;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/policydm/ui/XUIDialogActivity$21;-><init>(Lcom/policydm/ui/XUIDialogActivity;)V

    invoke-virtual/range {v24 .. v26}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    new-instance v25, Lcom/policydm/ui/XUIDialogActivity$20;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/policydm/ui/XUIDialogActivity$20;-><init>(Lcom/policydm/ui/XUIDialogActivity;)V

    invoke-virtual/range {v24 .. v25}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    new-instance v25, Lcom/policydm/ui/XUIDialogActivity$19;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/policydm/ui/XUIDialogActivity$19;-><init>(Lcom/policydm/ui/XUIDialogActivity;)V

    invoke-virtual/range {v24 .. v25}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v14

    goto/16 :goto_0

    .line 773
    .end local v6    # "body":Ljava/lang/String;
    .end local v13    # "p":Ljava/util/regex/Pattern;
    .end local v15    # "s":Landroid/text/SpannableString;
    :sswitch_7
    const v24, 0x7f08004b

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/policydm/ui/XUIDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v24

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    const-string v27, "www.samsung.com/seforandroid"

    aput-object v27, v25, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 774
    .restart local v6    # "body":Ljava/lang/String;
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmCheckNAProduct()Z

    move-result v24

    if-nez v24, :cond_13

    .line 775
    const v24, 0x7f08004a

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/policydm/ui/XUIDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v24

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    const-string v27, "www.samsung.com/seforandroid"

    aput-object v27, v25, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 776
    :cond_13
    const-string v24, "VZW"

    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetCustomCode()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_14

    .line 777
    const v24, 0x7f08004a

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/policydm/ui/XUIDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v24

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    const-string v27, "www.samsung.com/us/secureandroid"

    aput-object v27, v25, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 778
    :cond_14
    new-instance v15, Landroid/text/SpannableString;

    invoke-direct {v15, v6}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 779
    .restart local v15    # "s":Landroid/text/SpannableString;
    const-string v24, "www.samsung.com/seforandroid"

    invoke-static/range {v24 .. v24}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v13

    .line 780
    .restart local v13    # "p":Ljava/util/regex/Pattern;
    const-string v24, "VZW"

    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetCustomCode()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_15

    .line 781
    const-string v24, "www.samsung.com/us/secureandroid"

    invoke-static/range {v24 .. v24}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v13

    .line 782
    :cond_15
    const-string v24, "http://"

    move-object/from16 v0, v24

    invoke-static {v15, v13, v0}, Landroid/text/util/Linkify;->addLinks(Landroid/text/Spannable;Ljava/util/regex/Pattern;Ljava/lang/String;)Z

    .line 783
    new-instance v24, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v25, 0x7f080050

    invoke-virtual/range {v24 .. v25}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v15}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    const v25, 0x7f080031

    new-instance v26, Lcom/policydm/ui/XUIDialogActivity$24;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/policydm/ui/XUIDialogActivity$24;-><init>(Lcom/policydm/ui/XUIDialogActivity;)V

    invoke-virtual/range {v24 .. v26}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    new-instance v25, Lcom/policydm/ui/XUIDialogActivity$23;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/policydm/ui/XUIDialogActivity$23;-><init>(Lcom/policydm/ui/XUIDialogActivity;)V

    invoke-virtual/range {v24 .. v25}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    new-instance v25, Lcom/policydm/ui/XUIDialogActivity$22;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/policydm/ui/XUIDialogActivity$22;-><init>(Lcom/policydm/ui/XUIDialogActivity;)V

    invoke-virtual/range {v24 .. v25}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v14

    goto/16 :goto_0

    .line 811
    .end local v6    # "body":Ljava/lang/String;
    .end local v13    # "p":Ljava/util/regex/Pattern;
    .end local v15    # "s":Landroid/text/SpannableString;
    :sswitch_8
    const v19, 0x7f080018

    .line 812
    .restart local v19    # "textId":I
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmCheckNAProduct()Z

    move-result v24

    if-eqz v24, :cond_16

    .line 814
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmTablet()Z

    move-result v24

    if-eqz v24, :cond_18

    .line 815
    const v19, 0x7f08001a

    .line 819
    :cond_16
    :goto_2
    const-string v24, "ATT"

    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetCustomCode()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_17

    .line 821
    const v19, 0x7f080019

    .line 823
    :cond_17
    new-instance v24, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v25, 0x7f080004

    invoke-virtual/range {v24 .. v25}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    move-object/from16 v0, v24

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    const v25, 0x7f080031

    new-instance v26, Lcom/policydm/ui/XUIDialogActivity$28;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/policydm/ui/XUIDialogActivity$28;-><init>(Lcom/policydm/ui/XUIDialogActivity;)V

    invoke-virtual/range {v24 .. v26}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    const v25, 0x7f080006

    new-instance v26, Lcom/policydm/ui/XUIDialogActivity$27;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/policydm/ui/XUIDialogActivity$27;-><init>(Lcom/policydm/ui/XUIDialogActivity;)V

    invoke-virtual/range {v24 .. v26}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    new-instance v25, Lcom/policydm/ui/XUIDialogActivity$26;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/policydm/ui/XUIDialogActivity$26;-><init>(Lcom/policydm/ui/XUIDialogActivity;)V

    invoke-virtual/range {v24 .. v25}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    new-instance v25, Lcom/policydm/ui/XUIDialogActivity$25;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/policydm/ui/XUIDialogActivity$25;-><init>(Lcom/policydm/ui/XUIDialogActivity;)V

    invoke-virtual/range {v24 .. v25}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v14

    goto/16 :goto_0

    .line 817
    :cond_18
    const v19, 0x7f080017

    goto :goto_2

    .line 861
    .end local v19    # "textId":I
    :sswitch_9
    new-instance v24, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v25, 0x7f080047

    invoke-virtual/range {v24 .. v25}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v25

    sget-object v24, Lcom/policydm/ui/XUIDialogActivity;->g_UicOption:Ljava/lang/Object;

    check-cast v24, Lcom/policydm/eng/core/XDMUicOption;

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/policydm/eng/core/XDMUicOption;->text:Lcom/policydm/eng/core/XDMText;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/policydm/eng/core/XDMText;->text:Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    const v25, 0x7f080031

    new-instance v26, Lcom/policydm/ui/XUIDialogActivity$31;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/policydm/ui/XUIDialogActivity$31;-><init>(Lcom/policydm/ui/XUIDialogActivity;)V

    invoke-virtual/range {v24 .. v26}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    new-instance v25, Lcom/policydm/ui/XUIDialogActivity$30;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/policydm/ui/XUIDialogActivity$30;-><init>(Lcom/policydm/ui/XUIDialogActivity;)V

    invoke-virtual/range {v24 .. v25}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    new-instance v25, Lcom/policydm/ui/XUIDialogActivity$29;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/policydm/ui/XUIDialogActivity$29;-><init>(Lcom/policydm/ui/XUIDialogActivity;)V

    invoke-virtual/range {v24 .. v25}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v14

    goto/16 :goto_0

    .line 890
    :sswitch_a
    new-instance v24, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v25, 0x7f080047

    invoke-virtual/range {v24 .. v25}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v25

    sget-object v24, Lcom/policydm/ui/XUIDialogActivity;->g_UicOption:Ljava/lang/Object;

    check-cast v24, Lcom/policydm/eng/core/XDMUicOption;

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/policydm/eng/core/XDMUicOption;->text:Lcom/policydm/eng/core/XDMText;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/policydm/eng/core/XDMText;->text:Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    const v25, 0x7f080031

    new-instance v26, Lcom/policydm/ui/XUIDialogActivity$35;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/policydm/ui/XUIDialogActivity$35;-><init>(Lcom/policydm/ui/XUIDialogActivity;)V

    invoke-virtual/range {v24 .. v26}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    const v25, 0x7f08002f

    new-instance v26, Lcom/policydm/ui/XUIDialogActivity$34;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/policydm/ui/XUIDialogActivity$34;-><init>(Lcom/policydm/ui/XUIDialogActivity;)V

    invoke-virtual/range {v24 .. v26}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    new-instance v25, Lcom/policydm/ui/XUIDialogActivity$33;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/policydm/ui/XUIDialogActivity$33;-><init>(Lcom/policydm/ui/XUIDialogActivity;)V

    invoke-virtual/range {v24 .. v25}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    new-instance v25, Lcom/policydm/ui/XUIDialogActivity$32;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/policydm/ui/XUIDialogActivity$32;-><init>(Lcom/policydm/ui/XUIDialogActivity;)V

    invoke-virtual/range {v24 .. v25}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v14

    goto/16 :goto_0

    .line 931
    :sswitch_b
    invoke-static/range {p0 .. p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v9

    .line 932
    .local v9, "factory":Landroid/view/LayoutInflater;
    const v24, 0x7f030003

    const/16 v25, 0x0

    move/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v9, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v18

    .line 933
    .local v18, "textEntryView":Landroid/view/View;
    const v24, 0x7f0a0026

    move-object/from16 v0, v18

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    .line 934
    .local v11, "msg":Landroid/widget/TextView;
    const v24, 0x7f0a0027

    move-object/from16 v0, v18

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/EditText;

    .line 936
    .local v17, "text":Landroid/widget/EditText;
    sget-object v24, Lcom/policydm/ui/XUIDialogActivity;->g_UicOption:Ljava/lang/Object;

    check-cast v24, Lcom/policydm/eng/core/XDMUicOption;

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/policydm/eng/core/XDMUicOption;->text:Lcom/policydm/eng/core/XDMText;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/policydm/eng/core/XDMText;->text:Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v11, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 937
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/ui/XUIDialogActivity;->m_szDefaultResponseText:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-static/range {v24 .. v24}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v24

    if-nez v24, :cond_19

    .line 939
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/ui/XUIDialogActivity;->m_szDefaultResponseText:Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/policydm/ui/XUIDialogActivity;->m_szResponseText:Ljava/lang/String;

    .line 940
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/ui/XUIDialogActivity;->m_szDefaultResponseText:Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 941
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/ui/XUIDialogActivity;->m_szDefaultResponseText:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->length()I

    move-result v24

    move-object/from16 v0, v17

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 944
    :cond_19
    new-instance v24, Lcom/policydm/ui/XUIDialogActivity$36;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/policydm/ui/XUIDialogActivity$36;-><init>(Lcom/policydm/ui/XUIDialogActivity;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 959
    new-instance v24, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v25, 0x7f080047

    invoke-virtual/range {v24 .. v25}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    const v25, 0x7f080031

    new-instance v26, Lcom/policydm/ui/XUIDialogActivity$39;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/policydm/ui/XUIDialogActivity$39;-><init>(Lcom/policydm/ui/XUIDialogActivity;)V

    invoke-virtual/range {v24 .. v26}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    new-instance v25, Lcom/policydm/ui/XUIDialogActivity$38;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/policydm/ui/XUIDialogActivity$38;-><init>(Lcom/policydm/ui/XUIDialogActivity;)V

    invoke-virtual/range {v24 .. v25}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    new-instance v25, Lcom/policydm/ui/XUIDialogActivity$37;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/policydm/ui/XUIDialogActivity$37;-><init>(Lcom/policydm/ui/XUIDialogActivity;)V

    invoke-virtual/range {v24 .. v25}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v14

    goto/16 :goto_0

    .line 990
    .end local v9    # "factory":Landroid/view/LayoutInflater;
    .end local v11    # "msg":Landroid/widget/TextView;
    .end local v17    # "text":Landroid/widget/EditText;
    .end local v18    # "textEntryView":Landroid/view/View;
    :sswitch_c
    invoke-static/range {p0 .. p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v22

    .line 991
    .local v22, "uicSingle":Landroid/view/LayoutInflater;
    const v24, 0x7f030004

    const/16 v25, 0x0

    move-object/from16 v0, v22

    move/from16 v1, v24

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 992
    .local v4, "SingleEntryView":Landroid/view/View;
    const v24, 0x7f0a0028

    move/from16 v0, v24

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v23

    check-cast v23, Landroid/widget/TextView;

    .line 993
    .local v23, "uicSingleMsg":Landroid/widget/TextView;
    const v24, 0x7f0a0029

    move/from16 v0, v24

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RadioGroup;

    .line 995
    .local v5, "SinglerGroup":Landroid/widget/RadioGroup;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/policydm/ui/XUIDialogActivity;->m_nDRSingleSelected:I

    move/from16 v24, v0

    const/16 v25, -0x1

    move/from16 v0, v24

    move/from16 v1, v25

    if-eq v0, v1, :cond_1a

    .line 996
    sget-object v24, Lcom/policydm/ui/XUIDialogActivity;->m_UicResult:Ljava/lang/Object;

    check-cast v24, Lcom/policydm/eng/core/XDMUicResult;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/policydm/ui/XUIDialogActivity;->m_nDRSingleSelected:I

    move/from16 v25, v0

    add-int/lit8 v25, v25, 0x1

    move/from16 v0, v25

    move-object/from16 v1, v24

    iput v0, v1, Lcom/policydm/eng/core/XDMUicResult;->SingleSelected:I

    .line 998
    :cond_1a
    sget-object v24, Lcom/policydm/ui/XUIDialogActivity;->g_UicOption:Ljava/lang/Object;

    check-cast v24, Lcom/policydm/eng/core/XDMUicOption;

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/policydm/eng/core/XDMUicOption;->text:Lcom/policydm/eng/core/XDMText;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/policydm/eng/core/XDMText;->text:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1000
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/ui/XUIDialogActivity;->m_szUicMenuList:[Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    array-length v0, v0

    move/from16 v24, v0

    move/from16 v0, v24

    if-ge v10, v0, :cond_1c

    .line 1002
    new-instance v8, Landroid/widget/RadioButton;

    move-object/from16 v0, p0

    invoke-direct {v8, v0}, Landroid/widget/RadioButton;-><init>(Landroid/content/Context;)V

    .line 1003
    .local v8, "chkBox":Landroid/widget/RadioButton;
    invoke-virtual {v8, v10}, Landroid/widget/RadioButton;->setId(I)V

    .line 1004
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/ui/XUIDialogActivity;->m_szUicMenuList:[Ljava/lang/String;

    move-object/from16 v24, v0

    aget-object v24, v24, v10

    move-object/from16 v0, v24

    invoke-virtual {v8, v0}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 1005
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/ui/XUIDialogActivity;->mLayourFillWrap:Landroid/view/ViewGroup$LayoutParams;

    move-object/from16 v24, v0

    if-eqz v24, :cond_1b

    .line 1006
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/ui/XUIDialogActivity;->mLayourFillWrap:Landroid/view/ViewGroup$LayoutParams;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v5, v8, v0}, Landroid/widget/RadioGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1000
    :cond_1b
    add-int/lit8 v10, v10, 0x1

    goto :goto_3

    .line 1008
    .end local v8    # "chkBox":Landroid/widget/RadioButton;
    :cond_1c
    move-object/from16 v0, p0

    iget v0, v0, Lcom/policydm/ui/XUIDialogActivity;->m_nDRSingleSelected:I

    move/from16 v24, v0

    move/from16 v0, v24

    invoke-virtual {v5, v0}, Landroid/widget/RadioGroup;->check(I)V

    .line 1009
    new-instance v24, Lcom/policydm/ui/XUIDialogActivity$40;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/policydm/ui/XUIDialogActivity$40;-><init>(Lcom/policydm/ui/XUIDialogActivity;)V

    move-object/from16 v0, v24

    invoke-virtual {v5, v0}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 1017
    new-instance v24, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v25, 0x7f080047

    invoke-virtual/range {v24 .. v25}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    const v25, 0x7f080031

    new-instance v26, Lcom/policydm/ui/XUIDialogActivity$43;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/policydm/ui/XUIDialogActivity$43;-><init>(Lcom/policydm/ui/XUIDialogActivity;)V

    invoke-virtual/range {v24 .. v26}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    new-instance v25, Lcom/policydm/ui/XUIDialogActivity$42;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/policydm/ui/XUIDialogActivity$42;-><init>(Lcom/policydm/ui/XUIDialogActivity;)V

    invoke-virtual/range {v24 .. v25}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    new-instance v25, Lcom/policydm/ui/XUIDialogActivity$41;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/policydm/ui/XUIDialogActivity$41;-><init>(Lcom/policydm/ui/XUIDialogActivity;)V

    invoke-virtual/range {v24 .. v25}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v14

    goto/16 :goto_0

    .line 1048
    .end local v4    # "SingleEntryView":Landroid/view/View;
    .end local v5    # "SinglerGroup":Landroid/widget/RadioGroup;
    .end local v10    # "i":I
    .end local v22    # "uicSingle":Landroid/view/LayoutInflater;
    .end local v23    # "uicSingleMsg":Landroid/widget/TextView;
    :sswitch_d
    invoke-static/range {p0 .. p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v20

    .line 1049
    .local v20, "uicMulti":Landroid/view/LayoutInflater;
    const v24, 0x7f030004

    const/16 v25, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v24

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 1050
    .local v3, "MultiEntryView":Landroid/view/View;
    const v24, 0x7f0a0028

    move/from16 v0, v24

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/TextView;

    .line 1051
    .local v21, "uicMultiMsg":Landroid/widget/TextView;
    const v24, 0x7f0a0029

    move/from16 v0, v24

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/RadioGroup;

    .line 1053
    .local v12, "multiGroup":Landroid/widget/RadioGroup;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/ui/XUIDialogActivity;->m_bDRMultiSelected:[Z

    move-object/from16 v24, v0

    if-eqz v24, :cond_1e

    .line 1055
    const/4 v10, 0x0

    .restart local v10    # "i":I
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/ui/XUIDialogActivity;->m_bDRMultiSelected:[Z

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    array-length v0, v0

    move/from16 v24, v0

    move/from16 v0, v24

    if-ge v10, v0, :cond_1e

    .line 1057
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/ui/XUIDialogActivity;->m_bDRMultiSelected:[Z

    move-object/from16 v24, v0

    aget-boolean v24, v24, v10

    if-eqz v24, :cond_1d

    .line 1058
    sget-object v24, Lcom/policydm/ui/XUIDialogActivity;->m_UicResult:Ljava/lang/Object;

    check-cast v24, Lcom/policydm/eng/core/XDMUicResult;

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/policydm/eng/core/XDMUicResult;->MultiSelected:[I

    move-object/from16 v24, v0

    const/16 v25, 0x1

    aput v25, v24, v10

    .line 1055
    :cond_1d
    add-int/lit8 v10, v10, 0x1

    goto :goto_4

    .line 1062
    .end local v10    # "i":I
    :cond_1e
    sget-object v24, Lcom/policydm/ui/XUIDialogActivity;->g_UicOption:Ljava/lang/Object;

    check-cast v24, Lcom/policydm/eng/core/XDMUicOption;

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/policydm/eng/core/XDMUicOption;->text:Lcom/policydm/eng/core/XDMText;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/policydm/eng/core/XDMText;->text:Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1064
    const/4 v10, 0x0

    .restart local v10    # "i":I
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/ui/XUIDialogActivity;->m_szUicMenuList:[Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    array-length v0, v0

    move/from16 v24, v0

    move/from16 v0, v24

    if-ge v10, v0, :cond_1f

    .line 1066
    new-instance v8, Landroid/widget/CheckBox;

    move-object/from16 v0, p0

    invoke-direct {v8, v0}, Landroid/widget/CheckBox;-><init>(Landroid/content/Context;)V

    .line 1067
    .local v8, "chkBox":Landroid/widget/CheckBox;
    invoke-virtual {v8, v10}, Landroid/widget/CheckBox;->setId(I)V

    .line 1068
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/ui/XUIDialogActivity;->m_szUicMenuList:[Ljava/lang/String;

    move-object/from16 v24, v0

    aget-object v24, v24, v10

    move-object/from16 v0, v24

    invoke-virtual {v8, v0}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 1069
    new-instance v24, Lcom/policydm/ui/XUIDialogActivity$44;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/policydm/ui/XUIDialogActivity$44;-><init>(Lcom/policydm/ui/XUIDialogActivity;)V

    move-object/from16 v0, v24

    invoke-virtual {v8, v0}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1080
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/ui/XUIDialogActivity;->mLayourFillWrap:Landroid/view/ViewGroup$LayoutParams;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v12, v8, v0}, Landroid/widget/RadioGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1064
    add-int/lit8 v10, v10, 0x1

    goto :goto_5

    .line 1083
    .end local v8    # "chkBox":Landroid/widget/CheckBox;
    :cond_1f
    new-instance v24, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v25, 0x7f080047

    invoke-virtual/range {v24 .. v25}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    const v25, 0x7f080031

    new-instance v26, Lcom/policydm/ui/XUIDialogActivity$47;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/policydm/ui/XUIDialogActivity$47;-><init>(Lcom/policydm/ui/XUIDialogActivity;)V

    invoke-virtual/range {v24 .. v26}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    new-instance v25, Lcom/policydm/ui/XUIDialogActivity$46;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/policydm/ui/XUIDialogActivity$46;-><init>(Lcom/policydm/ui/XUIDialogActivity;)V

    invoke-virtual/range {v24 .. v25}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    new-instance v25, Lcom/policydm/ui/XUIDialogActivity$45;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/policydm/ui/XUIDialogActivity$45;-><init>(Lcom/policydm/ui/XUIDialogActivity;)V

    invoke-virtual/range {v24 .. v25}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v14

    goto/16 :goto_0

    .line 1114
    .end local v3    # "MultiEntryView":Landroid/view/View;
    .end local v10    # "i":I
    .end local v12    # "multiGroup":Landroid/widget/RadioGroup;
    .end local v20    # "uicMulti":Landroid/view/LayoutInflater;
    .end local v21    # "uicMultiMsg":Landroid/widget/TextView;
    :sswitch_e
    new-instance v24, Landroid/app/ProgressDialog;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/policydm/ui/XUIDialogActivity;->m_UicPrgDialog:Landroid/app/ProgressDialog;

    .line 1115
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/ui/XUIDialogActivity;->m_UicPrgDialog:Landroid/app/ProgressDialog;

    move-object/from16 v24, v0

    const v25, 0x7f080047

    invoke-virtual/range {v24 .. v25}, Landroid/app/ProgressDialog;->setTitle(I)V

    .line 1116
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/ui/XUIDialogActivity;->m_UicPrgDialog:Landroid/app/ProgressDialog;

    move-object/from16 v24, v0

    const-string v25, "Please wait while Downloading..."

    invoke-virtual/range {v24 .. v25}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 1117
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/ui/XUIDialogActivity;->m_UicPrgDialog:Landroid/app/ProgressDialog;

    move-object/from16 v24, v0

    const/16 v25, 0x64

    invoke-virtual/range {v24 .. v25}, Landroid/app/ProgressDialog;->setMax(I)V

    .line 1118
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/ui/XUIDialogActivity;->m_UicPrgDialog:Landroid/app/ProgressDialog;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 1119
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/ui/XUIDialogActivity;->m_UicPrgDialog:Landroid/app/ProgressDialog;

    move-object/from16 v24, v0

    const/16 v25, 0x1

    invoke-virtual/range {v24 .. v25}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 1120
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/ui/XUIDialogActivity;->m_UicPrgDialog:Landroid/app/ProgressDialog;

    move-object/from16 v24, v0

    const/16 v25, 0x1

    invoke-virtual/range {v24 .. v25}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 1121
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/ui/XUIDialogActivity;->m_UicPrgDialog:Landroid/app/ProgressDialog;

    move-object/from16 v24, v0

    new-instance v25, Lcom/policydm/ui/XUIDialogActivity$48;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/policydm/ui/XUIDialogActivity$48;-><init>(Lcom/policydm/ui/XUIDialogActivity;)V

    invoke-virtual/range {v24 .. v25}, Landroid/app/ProgressDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 1132
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/policydm/ui/XUIDialogActivity;->m_UicPrgDialog:Landroid/app/ProgressDialog;

    goto/16 :goto_0

    .line 457
    nop

    :sswitch_data_0
    .sparse-switch
        0x67 -> :sswitch_0
        0x6f -> :sswitch_9
        0x70 -> :sswitch_a
        0x71 -> :sswitch_b
        0x72 -> :sswitch_c
        0x73 -> :sswitch_d
        0x74 -> :sswitch_e
        0x78 -> :sswitch_5
        0x79 -> :sswitch_6
        0x7a -> :sswitch_7
        0x7c -> :sswitch_1
        0xcb -> :sswitch_3
        0xcc -> :sswitch_4
        0xcd -> :sswitch_8
        0xce -> :sswitch_2
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 1174
    const-string v0, ""

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1175
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 1176
    return-void
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "id"    # I
    .param p2, "dialog"    # Landroid/app/Dialog;
    .param p3, "args"    # Landroid/os/Bundle;

    .prologue
    .line 1143
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "id "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1145
    packed-switch p1, :pswitch_data_0

    .line 1155
    :goto_0
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V

    .line 1156
    return-void

    .line 1150
    :pswitch_0
    const v0, 0x102000b

    invoke-virtual {p2, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    goto :goto_0

    .line 1145
    :pswitch_data_0
    .packed-switch 0x78
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 1182
    const-string v1, ""

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1186
    :try_start_0
    sget v1, Lcom/policydm/ui/XUIDialogActivity;->m_DialogId:I

    if-lez v1, :cond_0

    .line 1188
    sget v1, Lcom/policydm/ui/XUIDialogActivity;->m_DialogId:I

    invoke-virtual {p0, v1}, Lcom/policydm/ui/XUIDialogActivity;->xuiDlgShow(I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1196
    :cond_0
    :goto_0
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 1197
    return-void

    .line 1191
    :catch_0
    move-exception v0

    .line 1193
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public xuiDlgShow(I)Z
    .locals 5
    .param p1, "id"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/16 v2, 0x67

    .line 1209
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "id "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1210
    invoke-static {v3}, Lcom/policydm/XDMBroadcastReceiver;->xdmSetCheckedIntent(Z)V

    .line 1211
    sparse-switch p1, :sswitch_data_0

    .line 1250
    :goto_0
    return v4

    .line 1214
    :sswitch_0
    invoke-virtual {p0, v2}, Lcom/policydm/ui/XUIDialogActivity;->showDialog(I)V

    goto :goto_0

    .line 1217
    :sswitch_1
    invoke-virtual {p0, v2}, Lcom/policydm/ui/XUIDialogActivity;->removeDialog(I)V

    .line 1218
    const/16 v0, 0x7c

    invoke-virtual {p0, v0}, Lcom/policydm/ui/XUIDialogActivity;->showDialog(I)V

    goto :goto_0

    .line 1221
    :sswitch_2
    const/16 v0, 0xce

    invoke-virtual {p0, v0}, Lcom/policydm/ui/XUIDialogActivity;->showDialog(I)V

    goto :goto_0

    .line 1224
    :sswitch_3
    const/16 v0, 0xcb

    invoke-virtual {p0, v0}, Lcom/policydm/ui/XUIDialogActivity;->showDialog(I)V

    goto :goto_0

    .line 1227
    :sswitch_4
    const/16 v0, 0xcc

    invoke-virtual {p0, v0}, Lcom/policydm/ui/XUIDialogActivity;->showDialog(I)V

    goto :goto_0

    .line 1230
    :sswitch_5
    const/16 v0, 0x78

    invoke-virtual {p0, v0}, Lcom/policydm/ui/XUIDialogActivity;->showDialog(I)V

    goto :goto_0

    .line 1233
    :sswitch_6
    const/16 v0, 0x79

    invoke-virtual {p0, v0}, Lcom/policydm/ui/XUIDialogActivity;->showDialog(I)V

    goto :goto_0

    .line 1236
    :sswitch_7
    const/16 v0, 0x7a

    invoke-virtual {p0, v0}, Lcom/policydm/ui/XUIDialogActivity;->showDialog(I)V

    goto :goto_0

    .line 1239
    :sswitch_8
    const/16 v0, 0xcd

    invoke-virtual {p0, v0}, Lcom/policydm/ui/XUIDialogActivity;->showDialog(I)V

    goto :goto_0

    .line 1243
    :sswitch_9
    invoke-virtual {p0, v2}, Lcom/policydm/ui/XUIDialogActivity;->removeDialog(I)V

    .line 1244
    sget-object v0, Lcom/policydm/ui/XUIDialogActivity;->g_UicOption:Ljava/lang/Object;

    invoke-virtual {p0, v0, v4}, Lcom/policydm/ui/XUIDialogActivity;->xuiDlgUICRequest(Ljava/lang/Object;Z)V

    .line 1245
    invoke-static {v3}, Lcom/policydm/db/XDBProfileListAdp;->xdbSetNotiEvent(I)V

    goto :goto_0

    .line 1211
    :sswitch_data_0
    .sparse-switch
        0x67 -> :sswitch_0
        0x6e -> :sswitch_9
        0x78 -> :sswitch_5
        0x79 -> :sswitch_6
        0x7a -> :sswitch_7
        0x7c -> :sswitch_1
        0xcb -> :sswitch_3
        0xcc -> :sswitch_4
        0xcd -> :sswitch_8
        0xce -> :sswitch_2
    .end sparse-switch
.end method

.method protected xuiDlgUICRequest(Ljava/lang/Object;Z)V
    .locals 10
    .param p1, "uicOption"    # Ljava/lang/Object;
    .param p2, "bDisplay"    # Z

    .prologue
    const/4 v7, 0x5

    const/4 v5, 0x2

    const/4 v6, 0x1

    .line 151
    const/4 v2, 0x0

    .line 152
    .local v2, "pUicOption":Lcom/policydm/eng/core/XDMUicOption;
    const/4 v3, 0x0

    .line 154
    .local v3, "pUicResult":Lcom/policydm/eng/core/XDMUicResult;
    if-eqz p1, :cond_2

    move-object v2, p1

    .line 156
    check-cast v2, Lcom/policydm/eng/core/XDMUicOption;

    .line 164
    const-string v4, ""

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 166
    iget v4, v2, Lcom/policydm/eng/core/XDMUicOption;->UICType:I

    if-lt v4, v6, :cond_0

    iget v4, v2, Lcom/policydm/eng/core/XDMUicOption;->UICType:I

    if-gt v4, v7, :cond_0

    .line 168
    invoke-static {}, Lcom/policydm/eng/core/XDMUic;->xdmUicCreateResult()Lcom/policydm/eng/core/XDMUicResult;

    move-result-object v3

    .line 170
    iget v4, v2, Lcom/policydm/eng/core/XDMUicOption;->appId:I

    iput v4, v3, Lcom/policydm/eng/core/XDMUicResult;->appId:I

    .line 171
    iget v4, v2, Lcom/policydm/eng/core/XDMUicOption;->UICType:I

    iput v4, v3, Lcom/policydm/eng/core/XDMUicResult;->UICType:I

    .line 172
    sput-object v3, Lcom/policydm/ui/XUIDialogActivity;->m_UicResult:Ljava/lang/Object;

    .line 175
    :cond_0
    if-eqz p2, :cond_1

    .line 177
    iget v4, v2, Lcom/policydm/eng/core/XDMUicOption;->UICType:I

    packed-switch v4, :pswitch_data_0

    .line 266
    :cond_1
    :goto_0
    return-void

    .line 160
    :cond_2
    const-string v4, "uicOption is null"

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 181
    :pswitch_0
    invoke-direct {p0, v6}, Lcom/policydm/ui/XUIDialogActivity;->xuiDlgPopup(I)V

    goto :goto_0

    .line 185
    :pswitch_1
    invoke-direct {p0, v5}, Lcom/policydm/ui/XUIDialogActivity;->xuiDlgPopup(I)V

    goto :goto_0

    .line 189
    :pswitch_2
    sget-object v4, Lcom/policydm/ui/XUIDialogActivity;->g_UicOption:Ljava/lang/Object;

    check-cast v4, Lcom/policydm/eng/core/XDMUicOption;

    iget v4, v4, Lcom/policydm/eng/core/XDMUicOption;->maxLen:I

    if-lez v4, :cond_3

    .line 190
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Max Len :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v4, Lcom/policydm/ui/XUIDialogActivity;->g_UicOption:Ljava/lang/Object;

    check-cast v4, Lcom/policydm/eng/core/XDMUicOption;

    iget v4, v4, Lcom/policydm/eng/core/XDMUicOption;->maxLen:I

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v4, Lcom/policydm/ui/XUIDialogActivity;->g_UicOption:Ljava/lang/Object;

    check-cast v4, Lcom/policydm/eng/core/XDMUicOption;

    iget-object v4, v4, Lcom/policydm/eng/core/XDMUicOption;->defaultResponse:Lcom/policydm/eng/core/XDMText;

    iget-object v4, v4, Lcom/policydm/eng/core/XDMText;->text:Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/policydm/ui/XUIDialogActivity;->m_szUicConformText:Ljava/lang/String;

    .line 191
    :cond_3
    const/4 v4, 0x3

    invoke-direct {p0, v4}, Lcom/policydm/ui/XUIDialogActivity;->xuiDlgPopup(I)V

    goto :goto_0

    .line 195
    :pswitch_3
    iget v4, v2, Lcom/policydm/eng/core/XDMUicOption;->uicMenuNumbers:I

    new-array v4, v4, [Ljava/lang/String;

    iput-object v4, p0, Lcom/policydm/ui/XUIDialogActivity;->m_szUicMenuList:[Ljava/lang/String;

    .line 196
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget v4, v2, Lcom/policydm/eng/core/XDMUicOption;->uicMenuNumbers:I

    if-ge v1, v4, :cond_5

    .line 198
    iget-object v4, p0, Lcom/policydm/ui/XUIDialogActivity;->m_szUicMenuList:[Ljava/lang/String;

    iget-object v5, v2, Lcom/policydm/eng/core/XDMUicOption;->uicMenuList:[Ljava/lang/String;

    aget-object v5, v5, v1

    aput-object v5, v4, v1

    .line 199
    iget-object v4, p0, Lcom/policydm/ui/XUIDialogActivity;->m_szUicMenuList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 200
    iget-object v4, p0, Lcom/policydm/ui/XUIDialogActivity;->m_szUicMenuList:[Ljava/lang/String;

    const-string v5, ""

    aput-object v5, v4, v1

    .line 196
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 203
    :cond_5
    iget-object v4, v2, Lcom/policydm/eng/core/XDMUicOption;->defaultResponse:Lcom/policydm/eng/core/XDMText;

    iget v4, v4, Lcom/policydm/eng/core/XDMText;->len:I

    if-eqz v4, :cond_6

    const-string v4, "0"

    iget-object v5, v2, Lcom/policydm/eng/core/XDMUicOption;->defaultResponse:Lcom/policydm/eng/core/XDMText;

    iget-object v5, v5, Lcom/policydm/eng/core/XDMText;->text:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-eqz v4, :cond_6

    .line 204
    iget-object v4, v2, Lcom/policydm/eng/core/XDMUicOption;->defaultResponse:Lcom/policydm/eng/core/XDMText;

    iget-object v4, v4, Lcom/policydm/eng/core/XDMText;->text:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/policydm/ui/XUIDialogActivity;->m_nDRSingleSelected:I

    .line 208
    :goto_2
    const/4 v4, 0x4

    invoke-direct {p0, v4}, Lcom/policydm/ui/XUIDialogActivity;->xuiDlgPopup(I)V

    goto/16 :goto_0

    .line 206
    :cond_6
    const/4 v4, -0x1

    iput v4, p0, Lcom/policydm/ui/XUIDialogActivity;->m_nDRSingleSelected:I

    goto :goto_2

    .line 212
    .end local v1    # "i":I
    :pswitch_4
    iget v4, v2, Lcom/policydm/eng/core/XDMUicOption;->uicMenuNumbers:I

    new-array v4, v4, [Ljava/lang/String;

    iput-object v4, p0, Lcom/policydm/ui/XUIDialogActivity;->m_szUicMenuList:[Ljava/lang/String;

    .line 213
    iget v4, v2, Lcom/policydm/eng/core/XDMUicOption;->uicMenuNumbers:I

    new-array v4, v4, [Z

    iput-object v4, p0, Lcom/policydm/ui/XUIDialogActivity;->m_bDRMultiSelected:[Z

    .line 215
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    iget v4, v2, Lcom/policydm/eng/core/XDMUicOption;->uicMenuNumbers:I

    if-ge v1, v4, :cond_8

    .line 217
    iget-object v4, p0, Lcom/policydm/ui/XUIDialogActivity;->m_szUicMenuList:[Ljava/lang/String;

    iget-object v5, v2, Lcom/policydm/eng/core/XDMUicOption;->uicMenuList:[Ljava/lang/String;

    aget-object v5, v5, v1

    aput-object v5, v4, v1

    .line 218
    iget-object v4, p0, Lcom/policydm/ui/XUIDialogActivity;->m_szUicMenuList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 219
    iget-object v4, p0, Lcom/policydm/ui/XUIDialogActivity;->m_szUicMenuList:[Ljava/lang/String;

    const-string v5, ""

    aput-object v5, v4, v1

    .line 215
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 223
    :cond_8
    if-nez v3, :cond_9

    .line 225
    const-string v4, "pUicResult is null"

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 229
    :cond_9
    const/4 v1, 0x0

    :goto_4
    iget-object v4, v3, Lcom/policydm/eng/core/XDMUicResult;->MultiSelected:[I

    array-length v4, v4

    if-ge v1, v4, :cond_c

    .line 231
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 232
    .local v0, "DefaultSet":Ljava/lang/String;
    iget-object v4, v2, Lcom/policydm/eng/core/XDMUicOption;->defaultResponse:Lcom/policydm/eng/core/XDMText;

    iget v4, v4, Lcom/policydm/eng/core/XDMText;->len:I

    if-eqz v4, :cond_b

    const-string v4, "0"

    iget-object v5, v2, Lcom/policydm/eng/core/XDMUicOption;->defaultResponse:Lcom/policydm/eng/core/XDMText;

    iget-object v5, v5, Lcom/policydm/eng/core/XDMText;->text:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-eqz v4, :cond_b

    .line 234
    iget-object v4, v2, Lcom/policydm/eng/core/XDMUicOption;->defaultResponse:Lcom/policydm/eng/core/XDMText;

    iget-object v4, v4, Lcom/policydm/eng/core/XDMText;->text:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 236
    iget-object v4, p0, Lcom/policydm/ui/XUIDialogActivity;->m_bDRMultiSelected:[Z

    if-eqz v4, :cond_a

    .line 238
    iget-object v4, p0, Lcom/policydm/ui/XUIDialogActivity;->m_bDRMultiSelected:[Z

    add-int/lit8 v5, v1, -0x1

    aput-boolean v6, v4, v5

    .line 229
    :cond_a
    :goto_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 244
    :cond_b
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/policydm/ui/XUIDialogActivity;->m_bDRMultiSelected:[Z

    goto :goto_5

    .line 248
    .end local v0    # "DefaultSet":Ljava/lang/String;
    :cond_c
    invoke-direct {p0, v7}, Lcom/policydm/ui/XUIDialogActivity;->xuiDlgPopup(I)V

    goto/16 :goto_0

    .line 252
    .end local v1    # "i":I
    :pswitch_5
    iget v4, v2, Lcom/policydm/eng/core/XDMUicOption;->progrType:I

    if-nez v4, :cond_d

    .line 253
    const/4 v4, 0x6

    invoke-direct {p0, v4}, Lcom/policydm/ui/XUIDialogActivity;->xuiDlgPopup(I)V

    goto/16 :goto_0

    .line 254
    :cond_d
    iget v4, v2, Lcom/policydm/eng/core/XDMUicOption;->progrType:I

    if-ne v4, v5, :cond_e

    .line 255
    sget-object v4, Lcom/policydm/ui/XUIDialogActivity;->g_UicOption:Ljava/lang/Object;

    invoke-direct {p0, v4}, Lcom/policydm/ui/XUIDialogActivity;->xuiDlgUicProgressResult(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 258
    :cond_e
    iget-object v5, p0, Lcom/policydm/ui/XUIDialogActivity;->m_UicPrgDialog:Landroid/app/ProgressDialog;

    sget-object v4, Lcom/policydm/ui/XUIDialogActivity;->g_UicOption:Ljava/lang/Object;

    check-cast v4, Lcom/policydm/eng/core/XDMUicOption;

    iget-wide v6, v4, Lcom/policydm/eng/core/XDMUicOption;->progrCurSize:J

    const-wide/16 v8, 0x64

    mul-long/2addr v6, v8

    sget-object v4, Lcom/policydm/ui/XUIDialogActivity;->g_UicOption:Ljava/lang/Object;

    check-cast v4, Lcom/policydm/eng/core/XDMUicOption;

    iget-wide v8, v4, Lcom/policydm/eng/core/XDMUicOption;->progrMaxSize:J

    div-long/2addr v6, v8

    long-to-int v4, v6

    invoke-virtual {v5, v4}, Landroid/app/ProgressDialog;->setProgress(I)V

    goto/16 :goto_0

    .line 177
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public xuiDlgUicChoiceResult(Ljava/lang/Object;I)V
    .locals 7
    .param p1, "pThis"    # Ljava/lang/Object;
    .param p2, "StateCode"    # I

    .prologue
    const/16 v3, 0x10

    const/4 v6, 0x5

    const/4 v5, 0x4

    const/4 v4, 0x0

    .line 396
    move-object v0, p1

    check-cast v0, Lcom/policydm/eng/core/XDMUicOption;

    .line 397
    .local v0, "pUicOption":Lcom/policydm/eng/core/XDMUicOption;
    sget-object v2, Lcom/policydm/ui/XUIDialogActivity;->m_UicResult:Ljava/lang/Object;

    check-cast v2, Lcom/policydm/eng/core/XDMUicResult;

    .line 398
    .local v2, "uicResult":Lcom/policydm/eng/core/XDMUicResult;
    const/4 v1, 0x0

    .line 400
    .local v1, "pUicResult":Lcom/policydm/eng/core/XDMUicResult;
    sparse-switch p2, :sswitch_data_0

    .line 426
    iput v3, v2, Lcom/policydm/eng/core/XDMUicResult;->result:I

    .line 430
    :cond_0
    :goto_0
    iget v3, v0, Lcom/policydm/eng/core/XDMUicOption;->uicMenuNumbers:I

    iput v3, v2, Lcom/policydm/eng/core/XDMUicResult;->MenuNumbers:I

    .line 431
    invoke-static {}, Lcom/policydm/eng/core/XDMUic;->xdmUicCreateResult()Lcom/policydm/eng/core/XDMUicResult;

    move-result-object v1

    .line 432
    invoke-static {v1, v2}, Lcom/policydm/eng/core/XDMUic;->xdmUicCopyResult(Lcom/policydm/eng/core/XDMUicResult;Lcom/policydm/eng/core/XDMUicResult;)Lcom/policydm/eng/core/XDMUicResult;

    move-result-object v1

    .line 434
    const/16 v3, 0x15

    invoke-static {v3, v1, v4}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 435
    invoke-static {v2}, Lcom/policydm/eng/core/XDMUic;->xdmUicFreeResult(Lcom/policydm/eng/core/XDMUicResult;)Ljava/lang/Object;

    .line 437
    invoke-static {v0}, Lcom/policydm/eng/core/XDMUic;->xdmUicFreeUicOption(Lcom/policydm/eng/core/XDMUicOption;)Lcom/policydm/eng/core/XDMUicOption;

    .line 438
    sput-object v4, Lcom/policydm/ui/XUIDialogActivity;->g_UicOption:Ljava/lang/Object;

    .line 439
    sput-object v4, Lcom/policydm/ui/XUIDialogActivity;->m_UicResult:Ljava/lang/Object;

    .line 440
    return-void

    .line 404
    :sswitch_0
    iget v3, v0, Lcom/policydm/eng/core/XDMUicOption;->UICType:I

    if-ne v3, v5, :cond_1

    .line 406
    iput v5, v2, Lcom/policydm/eng/core/XDMUicResult;->result:I

    goto :goto_0

    .line 408
    :cond_1
    iget v3, v0, Lcom/policydm/eng/core/XDMUicOption;->UICType:I

    if-ne v3, v6, :cond_0

    .line 410
    iput v6, v2, Lcom/policydm/eng/core/XDMUicResult;->result:I

    goto :goto_0

    .line 416
    :sswitch_1
    const/4 v3, 0x2

    iput v3, v2, Lcom/policydm/eng/core/XDMUicResult;->result:I

    goto :goto_0

    .line 421
    :sswitch_2
    iput v3, v2, Lcom/policydm/eng/core/XDMUicResult;->result:I

    goto :goto_0

    .line 400
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x2 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method protected xuiDlgUicConfirmResult(Ljava/lang/Object;I)V
    .locals 5
    .param p1, "pThis"    # Ljava/lang/Object;
    .param p2, "StateCode"    # I

    .prologue
    const/4 v4, 0x0

    .line 333
    move-object v0, p1

    check-cast v0, Lcom/policydm/eng/core/XDMUicOption;

    .line 334
    .local v0, "pUicOption":Lcom/policydm/eng/core/XDMUicOption;
    sget-object v2, Lcom/policydm/ui/XUIDialogActivity;->m_UicResult:Ljava/lang/Object;

    check-cast v2, Lcom/policydm/eng/core/XDMUicResult;

    .line 335
    .local v2, "uicResult":Lcom/policydm/eng/core/XDMUicResult;
    const/4 v1, 0x0

    .line 337
    .local v1, "pUicResult":Lcom/policydm/eng/core/XDMUicResult;
    if-nez v2, :cond_0

    .line 351
    :goto_0
    return-void

    .line 340
    :cond_0
    iput p2, v2, Lcom/policydm/eng/core/XDMUicResult;->result:I

    .line 342
    invoke-static {}, Lcom/policydm/eng/core/XDMUic;->xdmUicCreateResult()Lcom/policydm/eng/core/XDMUicResult;

    move-result-object v1

    .line 343
    invoke-static {v1, v2}, Lcom/policydm/eng/core/XDMUic;->xdmUicCopyResult(Lcom/policydm/eng/core/XDMUicResult;Lcom/policydm/eng/core/XDMUicResult;)Lcom/policydm/eng/core/XDMUicResult;

    move-result-object v1

    .line 345
    invoke-static {v2}, Lcom/policydm/eng/core/XDMUic;->xdmUicFreeResult(Lcom/policydm/eng/core/XDMUicResult;)Ljava/lang/Object;

    .line 346
    invoke-static {v0}, Lcom/policydm/eng/core/XDMUic;->xdmUicFreeUicOption(Lcom/policydm/eng/core/XDMUicOption;)Lcom/policydm/eng/core/XDMUicOption;

    .line 347
    sput-object v4, Lcom/policydm/ui/XUIDialogActivity;->m_UicResult:Ljava/lang/Object;

    .line 348
    const/4 v0, 0x0

    .line 350
    const/16 v3, 0x15

    invoke-static {v3, v1, v4}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected xuiDlgUicInputResult(Ljava/lang/Object;I)V
    .locals 7
    .param p1, "pThis"    # Ljava/lang/Object;
    .param p2, "StateCode"    # I

    .prologue
    const/16 v6, 0x10

    const/4 v5, 0x0

    .line 355
    move-object v0, p1

    check-cast v0, Lcom/policydm/eng/core/XDMUicOption;

    .line 356
    .local v0, "pUicOption":Lcom/policydm/eng/core/XDMUicOption;
    sget-object v2, Lcom/policydm/ui/XUIDialogActivity;->m_UicResult:Ljava/lang/Object;

    check-cast v2, Lcom/policydm/eng/core/XDMUicResult;

    .line 357
    .local v2, "uicResult":Lcom/policydm/eng/core/XDMUicResult;
    const/4 v1, 0x0

    .line 359
    .local v1, "pUicResult":Lcom/policydm/eng/core/XDMUicResult;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "StateCode :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 361
    sparse-switch p2, :sswitch_data_0

    .line 378
    iput v6, v2, Lcom/policydm/eng/core/XDMUicResult;->result:I

    .line 382
    :cond_0
    :goto_0
    invoke-static {}, Lcom/policydm/eng/core/XDMUic;->xdmUicCreateResult()Lcom/policydm/eng/core/XDMUicResult;

    move-result-object v1

    .line 383
    invoke-static {v1, v2}, Lcom/policydm/eng/core/XDMUic;->xdmUicCopyResult(Lcom/policydm/eng/core/XDMUicResult;Lcom/policydm/eng/core/XDMUicResult;)Lcom/policydm/eng/core/XDMUicResult;

    move-result-object v1

    .line 385
    const/16 v3, 0x15

    invoke-static {v3, v1, v5}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 386
    invoke-static {v2}, Lcom/policydm/eng/core/XDMUic;->xdmUicFreeResult(Lcom/policydm/eng/core/XDMUicResult;)Ljava/lang/Object;

    .line 387
    const/4 v2, 0x0

    .line 388
    sput-object v5, Lcom/policydm/ui/XUIDialogActivity;->m_UicResult:Ljava/lang/Object;

    .line 390
    invoke-static {v0}, Lcom/policydm/eng/core/XDMUic;->xdmUicFreeUicOption(Lcom/policydm/eng/core/XDMUicOption;)Lcom/policydm/eng/core/XDMUicOption;

    .line 391
    sput-object v5, Lcom/policydm/ui/XUIDialogActivity;->g_UicOption:Ljava/lang/Object;

    .line 392
    return-void

    .line 364
    :sswitch_0
    const/4 v3, 0x0

    iput v3, v2, Lcom/policydm/eng/core/XDMUicResult;->result:I

    .line 365
    iget-object v3, p0, Lcom/policydm/ui/XUIDialogActivity;->m_szResponseText:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 366
    iget-object v3, v2, Lcom/policydm/eng/core/XDMUicResult;->text:Lcom/policydm/eng/core/XDMText;

    iget-object v4, p0, Lcom/policydm/ui/XUIDialogActivity;->m_szResponseText:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/policydm/eng/core/XDMList;->xdmListAppendStrText(Lcom/policydm/eng/core/XDMText;Ljava/lang/String;)Lcom/policydm/eng/core/XDMText;

    move-result-object v3

    iput-object v3, v2, Lcom/policydm/eng/core/XDMUicResult;->text:Lcom/policydm/eng/core/XDMText;

    goto :goto_0

    .line 370
    :sswitch_1
    iput v6, v2, Lcom/policydm/eng/core/XDMUicResult;->result:I

    goto :goto_0

    .line 374
    :sswitch_2
    const/4 v3, 0x2

    iput v3, v2, Lcom/policydm/eng/core/XDMUicResult;->result:I

    goto :goto_0

    .line 361
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x2 -> :sswitch_2
        0x10 -> :sswitch_1
    .end sparse-switch
.end method
