.class Lcom/policydm/ui/XUIDialogActivity$4;
.super Ljava/lang/Object;
.source "XUIDialogActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/policydm/ui/XUIDialogActivity;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/policydm/ui/XUIDialogActivity;


# direct methods
.method constructor <init>(Lcom/policydm/ui/XUIDialogActivity;)V
    .locals 0

    .prologue
    .line 509
    iput-object p1, p0, Lcom/policydm/ui/XUIDialogActivity$4;->this$0:Lcom/policydm/ui/XUIDialogActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    const/16 v3, 0x9

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 512
    invoke-static {v1}, Lcom/policydm/ui/XUIAdapter;->xuiAdpSetUiMode(I)V

    .line 513
    invoke-static {v3}, Lcom/policydm/ui/XUINotificationManager;->xuiSetIndicator(I)V

    .line 515
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMOInitiatedType()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 517
    invoke-static {v1}, Lcom/policydm/ui/XUIAdapter;->xuiAdpSetUiMode(I)V

    .line 518
    invoke-static {v1}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOInitiatedType(I)V

    .line 525
    :goto_0
    invoke-static {v3}, Lcom/policydm/ui/XUINotificationManager;->xuiSetIndicator(I)V

    .line 526
    iget-object v0, p0, Lcom/policydm/ui/XUIDialogActivity$4;->this$0:Lcom/policydm/ui/XUIDialogActivity;

    const/16 v1, 0x7c

    invoke-virtual {v0, v1}, Lcom/policydm/ui/XUIDialogActivity;->removeDialog(I)V

    .line 527
    iget-object v0, p0, Lcom/policydm/ui/XUIDialogActivity$4;->this$0:Lcom/policydm/ui/XUIDialogActivity;

    invoke-virtual {v0}, Lcom/policydm/ui/XUIDialogActivity;->finish()V

    .line 528
    return-void

    .line 522
    :cond_0
    invoke-static {v2}, Lcom/policydm/db/XDB;->xdbSetWaitWifiFlag(Z)V

    goto :goto_0
.end method
