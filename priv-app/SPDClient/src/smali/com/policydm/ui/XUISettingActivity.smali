.class public Lcom/policydm/ui/XUISettingActivity;
.super Landroid/preference/PreferenceActivity;
.source "XUISettingActivity.java"


# static fields
.field private static final DIALOG_SELECT_PROFILE:I = 0x1


# instance fields
.field private m_DlgBuilderProfile:Landroid/app/AlertDialog$Builder;

.field private m_DlgNet:Landroid/app/Dialog;

.field private m_DlgProfile:Landroid/app/Dialog;

.field private m_PrefRoot:Landroid/preference/PreferenceScreen;

.field private m_PrefScreenNet:Landroid/preference/PreferenceScreen;

.field private m_PrefScreenProfile:Landroid/preference/PreferenceScreen;

.field private m_ProNetworkProfileList:Lcom/policydm/db/XDBNetworkProfileList;

.field private m_nSelectedProfileIndex:I

.field private m_szItemProfile:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 25
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 30
    iput-object v0, p0, Lcom/policydm/ui/XUISettingActivity;->m_DlgNet:Landroid/app/Dialog;

    .line 31
    iput-object v0, p0, Lcom/policydm/ui/XUISettingActivity;->m_DlgProfile:Landroid/app/Dialog;

    .line 32
    iput-object v0, p0, Lcom/policydm/ui/XUISettingActivity;->m_DlgBuilderProfile:Landroid/app/AlertDialog$Builder;

    .line 38
    const/4 v0, 0x0

    iput v0, p0, Lcom/policydm/ui/XUISettingActivity;->m_nSelectedProfileIndex:I

    return-void
.end method

.method static synthetic access$000(Lcom/policydm/ui/XUISettingActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/policydm/ui/XUISettingActivity;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/policydm/ui/XUISettingActivity;->xuiCallUiDmProfile()V

    return-void
.end method

.method static synthetic access$100(Lcom/policydm/ui/XUISettingActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/policydm/ui/XUISettingActivity;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/policydm/ui/XUISettingActivity;->xuiUpdateProfile()V

    return-void
.end method

.method static synthetic access$200(Lcom/policydm/ui/XUISettingActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/policydm/ui/XUISettingActivity;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/policydm/ui/XUISettingActivity;->xuiUpdateNetworkProfile()V

    return-void
.end method

.method static synthetic access$300(Lcom/policydm/ui/XUISettingActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/policydm/ui/XUISettingActivity;

    .prologue
    .line 25
    iget v0, p0, Lcom/policydm/ui/XUISettingActivity;->m_nSelectedProfileIndex:I

    return v0
.end method

.method static synthetic access$302(Lcom/policydm/ui/XUISettingActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/policydm/ui/XUISettingActivity;
    .param p1, "x1"    # I

    .prologue
    .line 25
    iput p1, p0, Lcom/policydm/ui/XUISettingActivity;->m_nSelectedProfileIndex:I

    return p1
.end method

.method static synthetic access$400(Lcom/policydm/ui/XUISettingActivity;)Landroid/app/Dialog;
    .locals 1
    .param p0, "x0"    # Lcom/policydm/ui/XUISettingActivity;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/policydm/ui/XUISettingActivity;->m_DlgNet:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic access$500(Lcom/policydm/ui/XUISettingActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/policydm/ui/XUISettingActivity;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/policydm/ui/XUISettingActivity;->xuiCallUiDmNetProfile()V

    return-void
.end method

.method private xuiCallUiDmNetProfile()V
    .locals 3

    .prologue
    .line 74
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/policydm/ui/XUINetProfileActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 75
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "profileIndex"

    iget v2, p0, Lcom/policydm/ui/XUISettingActivity;->m_nSelectedProfileIndex:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 76
    invoke-virtual {p0, v0}, Lcom/policydm/ui/XUISettingActivity;->startActivity(Landroid/content/Intent;)V

    .line 77
    return-void
.end method

.method private xuiCallUiDmProfile()V
    .locals 3

    .prologue
    .line 67
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/policydm/ui/XUIProfileActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 68
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "profileIndex"

    iget v2, p0, Lcom/policydm/ui/XUISettingActivity;->m_nSelectedProfileIndex:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 69
    invoke-virtual {p0, v0}, Lcom/policydm/ui/XUISettingActivity;->startActivity(Landroid/content/Intent;)V

    .line 70
    return-void
.end method

.method private xuiUpdateNetworkProfile()V
    .locals 2

    .prologue
    .line 51
    const-string v0, ""

    .line 53
    .local v0, "NetworkName":Ljava/lang/String;
    sget-object v1, Lcom/policydm/ui/XUIMainActivity;->g_UiNetInfo:Lcom/policydm/adapter/XDMTelephonyAdapter;

    if-nez v1, :cond_0

    .line 54
    invoke-static {}, Lcom/policydm/adapter/XDMNetworkAdapter;->xdbAdpGetProxyData()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/policydm/adapter/XDMTelephonyAdapter;

    sput-object v1, Lcom/policydm/ui/XUIMainActivity;->g_UiNetInfo:Lcom/policydm/adapter/XDMTelephonyAdapter;

    .line 56
    :cond_0
    sget-object v1, Lcom/policydm/ui/XUIMainActivity;->g_UiNetInfo:Lcom/policydm/adapter/XDMTelephonyAdapter;

    iget-object v1, v1, Lcom/policydm/adapter/XDMTelephonyAdapter;->szAccountName:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 57
    sget-object v1, Lcom/policydm/ui/XUIMainActivity;->g_UiNetInfo:Lcom/policydm/adapter/XDMTelephonyAdapter;

    iget-object v0, v1, Lcom/policydm/adapter/XDMTelephonyAdapter;->szAccountName:Ljava/lang/String;

    .line 59
    :cond_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 60
    const-string v0, "Unknown"

    .line 62
    :cond_2
    iget-object v1, p0, Lcom/policydm/ui/XUISettingActivity;->m_PrefScreenNet:Landroid/preference/PreferenceScreen;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->setTitle(Ljava/lang/CharSequence;)V

    .line 63
    return-void
.end method

.method private xuiUpdateProfile()V
    .locals 3

    .prologue
    .line 45
    iget-object v0, p0, Lcom/policydm/ui/XUISettingActivity;->m_PrefScreenProfile:Landroid/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/policydm/ui/XUISettingActivity;->m_szItemProfile:[Ljava/lang/String;

    iget v2, p0, Lcom/policydm/ui/XUISettingActivity;->m_nSelectedProfileIndex:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setTitle(Ljava/lang/CharSequence;)V

    .line 46
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 191
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 197
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    if-eqz v0, :cond_0

    .line 199
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    invoke-static {}, Lcom/policydm/db/XDBProfileAdp;->xdbGetProfileInfo()Lcom/policydm/db/XDBProfileInfo;

    move-result-object v1

    iput-object v1, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    .line 200
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/policydm/db/XDB;->xdbReadListInfo(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/policydm/db/XDBNetworkProfileList;

    iput-object v0, p0, Lcom/policydm/ui/XUISettingActivity;->m_ProNetworkProfileList:Lcom/policydm/db/XDBNetworkProfileList;

    .line 201
    iget-object v0, p0, Lcom/policydm/ui/XUISettingActivity;->m_ProNetworkProfileList:Lcom/policydm/db/XDBNetworkProfileList;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    if-eqz v0, :cond_0

    .line 203
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    iget-object v1, p0, Lcom/policydm/ui/XUISettingActivity;->m_ProNetworkProfileList:Lcom/policydm/db/XDBNetworkProfileList;

    iget-object v1, v1, Lcom/policydm/db/XDBNetworkProfileList;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    iput-object v1, v0, Lcom/policydm/db/XDBProfileInfo;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    .line 206
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/policydm/ui/XUISettingActivity;->m_DlgBuilderProfile:Landroid/app/AlertDialog$Builder;

    .line 207
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .param p1, "id"    # I

    .prologue
    .line 82
    packed-switch p1, :pswitch_data_0

    .line 126
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 85
    :pswitch_0
    iget-object v0, p0, Lcom/policydm/ui/XUISettingActivity;->m_DlgBuilderProfile:Landroid/app/AlertDialog$Builder;

    if-nez v0, :cond_0

    .line 86
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/policydm/ui/XUISettingActivity;->m_DlgBuilderProfile:Landroid/app/AlertDialog$Builder;

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/policydm/ui/XUISettingActivity;->m_DlgBuilderProfile:Landroid/app/AlertDialog$Builder;

    const v1, 0x7f080035

    invoke-virtual {p0, v1}, Lcom/policydm/ui/XUISettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/policydm/ui/XUISettingActivity;->m_szItemProfile:[Ljava/lang/String;

    iget v2, p0, Lcom/policydm/ui/XUISettingActivity;->m_nSelectedProfileIndex:I

    new-instance v3, Lcom/policydm/ui/XUISettingActivity$3;

    invoke-direct {v3, p0}, Lcom/policydm/ui/XUISettingActivity$3;-><init>(Lcom/policydm/ui/XUISettingActivity;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f080039

    new-instance v2, Lcom/policydm/ui/XUISettingActivity$2;

    invoke-direct {v2, p0}, Lcom/policydm/ui/XUISettingActivity$2;-><init>(Lcom/policydm/ui/XUISettingActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f08000f

    new-instance v2, Lcom/policydm/ui/XUISettingActivity$1;

    invoke-direct {v2, p0}, Lcom/policydm/ui/XUISettingActivity$1;-><init>(Lcom/policydm/ui/XUISettingActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/policydm/ui/XUISettingActivity;->m_DlgProfile:Landroid/app/Dialog;

    .line 120
    iget-object v0, p0, Lcom/policydm/ui/XUISettingActivity;->m_DlgProfile:Landroid/app/Dialog;

    goto :goto_0

    .line 82
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected onStart()V
    .locals 5

    .prologue
    .line 133
    sget-object v3, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v3, v3, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    iget-object v3, v3, Lcom/policydm/db/XDBProfileListInfo;->ProfileName:[Ljava/lang/String;

    iput-object v3, p0, Lcom/policydm/ui/XUISettingActivity;->m_szItemProfile:[Ljava/lang/String;

    .line 134
    sget-object v3, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v3, v3, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    iget v3, v3, Lcom/policydm/db/XDBProfileListInfo;->Profileindex:I

    iput v3, p0, Lcom/policydm/ui/XUISettingActivity;->m_nSelectedProfileIndex:I

    .line 136
    invoke-virtual {p0}, Lcom/policydm/ui/XUISettingActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v3

    invoke-virtual {v3, p0}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v3

    iput-object v3, p0, Lcom/policydm/ui/XUISettingActivity;->m_PrefRoot:Landroid/preference/PreferenceScreen;

    .line 138
    new-instance v2, Landroid/preference/PreferenceCategory;

    invoke-direct {v2, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 139
    .local v2, "launchPrefCatTitle":Landroid/preference/PreferenceCategory;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const v4, 0x7f080033

    invoke-virtual {p0, v4}, Lcom/policydm/ui/XUISettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const v4, 0x7f08003f

    invoke-virtual {p0, v4}, Lcom/policydm/ui/XUISettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 140
    iget-object v3, p0, Lcom/policydm/ui/XUISettingActivity;->m_PrefRoot:Landroid/preference/PreferenceScreen;

    invoke-virtual {v3, v2}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 143
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 144
    .local v0, "launchPrefCat":Landroid/preference/PreferenceCategory;
    const v3, 0x7f080035

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceCategory;->setTitle(I)V

    .line 145
    iget-object v3, p0, Lcom/policydm/ui/XUISettingActivity;->m_PrefRoot:Landroid/preference/PreferenceScreen;

    invoke-virtual {v3, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 147
    invoke-virtual {p0}, Lcom/policydm/ui/XUISettingActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v3

    invoke-virtual {v3, p0}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v3

    iput-object v3, p0, Lcom/policydm/ui/XUISettingActivity;->m_PrefScreenProfile:Landroid/preference/PreferenceScreen;

    .line 148
    iget-object v3, p0, Lcom/policydm/ui/XUISettingActivity;->m_PrefScreenProfile:Landroid/preference/PreferenceScreen;

    const-string v4, "screen_preference"

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->setKey(Ljava/lang/String;)V

    .line 149
    invoke-direct {p0}, Lcom/policydm/ui/XUISettingActivity;->xuiUpdateProfile()V

    .line 150
    iget-object v3, p0, Lcom/policydm/ui/XUISettingActivity;->m_PrefRoot:Landroid/preference/PreferenceScreen;

    iget-object v4, p0, Lcom/policydm/ui/XUISettingActivity;->m_PrefScreenProfile:Landroid/preference/PreferenceScreen;

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 152
    iget-object v3, p0, Lcom/policydm/ui/XUISettingActivity;->m_PrefScreenProfile:Landroid/preference/PreferenceScreen;

    new-instance v4, Lcom/policydm/ui/XUISettingActivity$4;

    invoke-direct {v4, p0}, Lcom/policydm/ui/XUISettingActivity$4;-><init>(Lcom/policydm/ui/XUISettingActivity;)V

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 166
    new-instance v1, Landroid/preference/PreferenceCategory;

    invoke-direct {v1, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 167
    .local v1, "launchPrefCatNet":Landroid/preference/PreferenceCategory;
    const v3, 0x7f08002d

    invoke-virtual {v1, v3}, Landroid/preference/PreferenceCategory;->setTitle(I)V

    .line 168
    iget-object v3, p0, Lcom/policydm/ui/XUISettingActivity;->m_PrefRoot:Landroid/preference/PreferenceScreen;

    invoke-virtual {v3, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 170
    invoke-virtual {p0}, Lcom/policydm/ui/XUISettingActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v3

    invoke-virtual {v3, p0}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v3

    iput-object v3, p0, Lcom/policydm/ui/XUISettingActivity;->m_PrefScreenNet:Landroid/preference/PreferenceScreen;

    .line 171
    iget-object v3, p0, Lcom/policydm/ui/XUISettingActivity;->m_PrefScreenNet:Landroid/preference/PreferenceScreen;

    const-string v4, "screen_preferencenet"

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->setKey(Ljava/lang/String;)V

    .line 172
    invoke-direct {p0}, Lcom/policydm/ui/XUISettingActivity;->xuiUpdateNetworkProfile()V

    .line 173
    iget-object v3, p0, Lcom/policydm/ui/XUISettingActivity;->m_PrefRoot:Landroid/preference/PreferenceScreen;

    iget-object v4, p0, Lcom/policydm/ui/XUISettingActivity;->m_PrefScreenNet:Landroid/preference/PreferenceScreen;

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 175
    iget-object v3, p0, Lcom/policydm/ui/XUISettingActivity;->m_PrefScreenNet:Landroid/preference/PreferenceScreen;

    new-instance v4, Lcom/policydm/ui/XUISettingActivity$5;

    invoke-direct {v4, p0}, Lcom/policydm/ui/XUISettingActivity$5;-><init>(Lcom/policydm/ui/XUISettingActivity;)V

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 184
    iget-object v3, p0, Lcom/policydm/ui/XUISettingActivity;->m_PrefRoot:Landroid/preference/PreferenceScreen;

    invoke-virtual {p0, v3}, Lcom/policydm/ui/XUISettingActivity;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    .line 185
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onStart()V

    .line 186
    return-void
.end method
