.class Lcom/policydm/ui/XUISettingActivity$2;
.super Ljava/lang/Object;
.source "XUISettingActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/policydm/ui/XUISettingActivity;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/policydm/ui/XUISettingActivity;


# direct methods
.method constructor <init>(Lcom/policydm/ui/XUISettingActivity;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/policydm/ui/XUISettingActivity$2;->this$0:Lcom/policydm/ui/XUISettingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 98
    iget-object v0, p0, Lcom/policydm/ui/XUISettingActivity$2;->this$0:Lcom/policydm/ui/XUISettingActivity;

    # invokes: Lcom/policydm/ui/XUISettingActivity;->xuiUpdateProfile()V
    invoke-static {v0}, Lcom/policydm/ui/XUISettingActivity;->access$100(Lcom/policydm/ui/XUISettingActivity;)V

    .line 99
    iget-object v0, p0, Lcom/policydm/ui/XUISettingActivity$2;->this$0:Lcom/policydm/ui/XUISettingActivity;

    # invokes: Lcom/policydm/ui/XUISettingActivity;->xuiUpdateNetworkProfile()V
    invoke-static {v0}, Lcom/policydm/ui/XUISettingActivity;->access$200(Lcom/policydm/ui/XUISettingActivity;)V

    .line 100
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    iget-object v1, p0, Lcom/policydm/ui/XUISettingActivity$2;->this$0:Lcom/policydm/ui/XUISettingActivity;

    # getter for: Lcom/policydm/ui/XUISettingActivity;->m_nSelectedProfileIndex:I
    invoke-static {v1}, Lcom/policydm/ui/XUISettingActivity;->access$300(Lcom/policydm/ui/XUISettingActivity;)I

    move-result v1

    iput v1, v0, Lcom/policydm/db/XDBProfileListInfo;->Profileindex:I

    .line 101
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    invoke-static {v0}, Lcom/policydm/db/XDBProfileListAdp;->xdbSetProflieList(Ljava/lang/Object;)V

    .line 102
    iget-object v0, p0, Lcom/policydm/ui/XUISettingActivity$2;->this$0:Lcom/policydm/ui/XUISettingActivity;

    # getter for: Lcom/policydm/ui/XUISettingActivity;->m_nSelectedProfileIndex:I
    invoke-static {v0}, Lcom/policydm/ui/XUISettingActivity;->access$300(Lcom/policydm/ui/XUISettingActivity;)I

    move-result v0

    invoke-static {v0}, Lcom/policydm/db/XDBProfileListAdp;->xdbSetProfileIndex(I)V

    .line 103
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    invoke-static {}, Lcom/policydm/db/XDBProfileAdp;->xdbGetProfileInfo()Lcom/policydm/db/XDBProfileInfo;

    move-result-object v1

    iput-object v1, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    .line 104
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v0, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    invoke-static {}, Lcom/policydm/db/XDBProfileAdp;->xdbGetConRef()Lcom/policydm/db/XDBInfoConRef;

    move-result-object v1

    iput-object v1, v0, Lcom/policydm/db/XDBProfileInfo;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    .line 106
    iget-object v0, p0, Lcom/policydm/ui/XUISettingActivity$2;->this$0:Lcom/policydm/ui/XUISettingActivity;

    # getter for: Lcom/policydm/ui/XUISettingActivity;->m_DlgNet:Landroid/app/Dialog;
    invoke-static {v0}, Lcom/policydm/ui/XUISettingActivity;->access$400(Lcom/policydm/ui/XUISettingActivity;)Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/policydm/ui/XUISettingActivity$2;->this$0:Lcom/policydm/ui/XUISettingActivity;

    # getter for: Lcom/policydm/ui/XUISettingActivity;->m_DlgNet:Landroid/app/Dialog;
    invoke-static {v0}, Lcom/policydm/ui/XUISettingActivity;->access$400(Lcom/policydm/ui/XUISettingActivity;)Landroid/app/Dialog;

    move-result-object v0

    const v1, 0x7f08002d

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(I)V

    .line 110
    :cond_0
    iget-object v0, p0, Lcom/policydm/ui/XUISettingActivity$2;->this$0:Lcom/policydm/ui/XUISettingActivity;

    invoke-virtual {v0}, Lcom/policydm/ui/XUISettingActivity;->onStart()V

    .line 111
    return-void
.end method
