.class Lcom/policydm/ui/XUIDialogActivity$3;
.super Ljava/lang/Object;
.source "XUIDialogActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/policydm/ui/XUIDialogActivity;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/policydm/ui/XUIDialogActivity;


# direct methods
.method constructor <init>(Lcom/policydm/ui/XUIDialogActivity;)V
    .locals 0

    .prologue
    .line 530
    iput-object p1, p0, Lcom/policydm/ui/XUIDialogActivity$3;->this$0:Lcom/policydm/ui/XUIDialogActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    const/16 v3, 0x9

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 533
    invoke-static {v1}, Lcom/policydm/ui/XUIAdapter;->xuiAdpSetUiMode(I)V

    .line 534
    invoke-static {v3}, Lcom/policydm/ui/XUINotificationManager;->xuiSetIndicator(I)V

    .line 536
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMOInitiatedType()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 538
    invoke-static {v1}, Lcom/policydm/ui/XUIAdapter;->xuiAdpSetUiMode(I)V

    .line 539
    invoke-static {v1}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOInitiatedType(I)V

    .line 546
    :goto_0
    invoke-static {v3}, Lcom/policydm/ui/XUINotificationManager;->xuiSetIndicator(I)V

    .line 547
    iget-object v0, p0, Lcom/policydm/ui/XUIDialogActivity$3;->this$0:Lcom/policydm/ui/XUIDialogActivity;

    const/16 v1, 0x7c

    invoke-virtual {v0, v1}, Lcom/policydm/ui/XUIDialogActivity;->removeDialog(I)V

    .line 548
    iget-object v0, p0, Lcom/policydm/ui/XUIDialogActivity$3;->this$0:Lcom/policydm/ui/XUIDialogActivity;

    invoke-virtual {v0}, Lcom/policydm/ui/XUIDialogActivity;->finish()V

    .line 549
    return-void

    .line 543
    :cond_0
    invoke-static {v2}, Lcom/policydm/db/XDB;->xdbSetWaitWifiFlag(Z)V

    goto :goto_0
.end method
