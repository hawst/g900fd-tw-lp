.class public Lcom/policydm/ui/XUINetProfileActivity;
.super Landroid/app/TabActivity;
.source "XUINetProfileActivity.java"


# static fields
.field private static final DIALOG_PROFILE_EDIT_YES_NO:I = 0x1

.field private static m_LocalNetworkProfileList_t:Lcom/policydm/db/XDBNetworkProfileList;

.field private static m_bUpdateState:Z

.field private static m_nNProfile:I

.field private static m_nProfile:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 39
    sput v0, Lcom/policydm/ui/XUINetProfileActivity;->m_nNProfile:I

    .line 40
    sput-boolean v0, Lcom/policydm/ui/XUINetProfileActivity;->m_bUpdateState:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Landroid/app/TabActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lcom/policydm/db/XDBNetworkProfileList;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/policydm/ui/XUINetProfileActivity;->m_LocalNetworkProfileList_t:Lcom/policydm/db/XDBNetworkProfileList;

    return-object v0
.end method

.method static synthetic access$100(Lcom/policydm/ui/XUINetProfileActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/policydm/ui/XUINetProfileActivity;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/policydm/ui/XUINetProfileActivity;->xuiCallUiDmProfile()V

    return-void
.end method

.method static synthetic access$200()I
    .locals 1

    .prologue
    .line 36
    sget v0, Lcom/policydm/ui/XUINetProfileActivity;->m_nNProfile:I

    return v0
.end method

.method static synthetic access$202(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 36
    sput p0, Lcom/policydm/ui/XUINetProfileActivity;->m_nNProfile:I

    return p0
.end method

.method static synthetic access$300(Lcom/policydm/ui/XUINetProfileActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/policydm/ui/XUINetProfileActivity;
    .param p1, "x1"    # I

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/policydm/ui/XUINetProfileActivity;->xuiCurrentTabSet(I)V

    return-void
.end method

.method public static getUpdateState()Z
    .locals 1

    .prologue
    .line 47
    sget-boolean v0, Lcom/policydm/ui/XUINetProfileActivity;->m_bUpdateState:Z

    return v0
.end method

.method public static setUpdateState(I)V
    .locals 1
    .param p0, "state"    # I

    .prologue
    .line 52
    if-nez p0, :cond_0

    .line 53
    const/4 v0, 0x0

    sput-boolean v0, Lcom/policydm/ui/XUINetProfileActivity;->m_bUpdateState:Z

    .line 56
    :goto_0
    return-void

    .line 55
    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/policydm/ui/XUINetProfileActivity;->m_bUpdateState:Z

    goto :goto_0
.end method

.method private xuiCallUiDmProfile()V
    .locals 3

    .prologue
    .line 223
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/policydm/ui/XUIProfileActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 224
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "profileIndex"

    sget v2, Lcom/policydm/ui/XUINetProfileActivity;->m_nProfile:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 225
    invoke-virtual {p0, v0}, Lcom/policydm/ui/XUINetProfileActivity;->startActivity(Landroid/content/Intent;)V

    .line 226
    invoke-virtual {p0}, Lcom/policydm/ui/XUINetProfileActivity;->finish()V

    .line 227
    return-void
.end method

.method private xuiCurrentTabSet(I)V
    .locals 12
    .param p1, "selectTap"    # I

    .prologue
    const v11, 0x1090009

    const v10, 0x1090008

    .line 61
    const v8, 0x7f0a0007

    invoke-virtual {p0, v8}, Lcom/policydm/ui/XUINetProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/EditText;

    .line 62
    .local v7, "text":Landroid/widget/EditText;
    sget-object v8, Lcom/policydm/ui/XUINetProfileActivity;->m_LocalNetworkProfileList_t:Lcom/policydm/db/XDBNetworkProfileList;

    iget-object v8, v8, Lcom/policydm/db/XDBNetworkProfileList;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    iget-object v8, v8, Lcom/policydm/db/XDBInfoConRef;->NAP:Lcom/policydm/db/XDBConRefNAP;

    iget-object v8, v8, Lcom/policydm/db/XDBConRefNAP;->NetworkProfileName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 63
    new-instance v8, Lcom/policydm/ui/XUINetProfileActivity$1;

    invoke-direct {v8, p0}, Lcom/policydm/ui/XUINetProfileActivity$1;-><init>(Lcom/policydm/ui/XUINetProfileActivity;)V

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 79
    const v8, 0x7f0a0009

    invoke-virtual {p0, v8}, Lcom/policydm/ui/XUINetProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .end local v7    # "text":Landroid/widget/EditText;
    check-cast v7, Landroid/widget/EditText;

    .line 80
    .restart local v7    # "text":Landroid/widget/EditText;
    sget-object v8, Lcom/policydm/ui/XUINetProfileActivity;->m_LocalNetworkProfileList_t:Lcom/policydm/db/XDBNetworkProfileList;

    iget-object v8, v8, Lcom/policydm/db/XDBNetworkProfileList;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    iget-object v8, v8, Lcom/policydm/db/XDBInfoConRef;->PX:Lcom/policydm/db/XDBConRefPX;

    iget-object v8, v8, Lcom/policydm/db/XDBConRefPX;->Addr:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 81
    new-instance v8, Lcom/policydm/ui/XUINetProfileActivity$2;

    invoke-direct {v8, p0}, Lcom/policydm/ui/XUINetProfileActivity$2;-><init>(Lcom/policydm/ui/XUINetProfileActivity;)V

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 97
    const v8, 0x7f0a000b

    invoke-virtual {p0, v8}, Lcom/policydm/ui/XUINetProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .end local v7    # "text":Landroid/widget/EditText;
    check-cast v7, Landroid/widget/EditText;

    .line 99
    .restart local v7    # "text":Landroid/widget/EditText;
    sget-object v8, Lcom/policydm/ui/XUINetProfileActivity;->m_LocalNetworkProfileList_t:Lcom/policydm/db/XDBNetworkProfileList;

    iget-object v8, v8, Lcom/policydm/db/XDBNetworkProfileList;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    iget-object v8, v8, Lcom/policydm/db/XDBInfoConRef;->PX:Lcom/policydm/db/XDBConRefPX;

    iget v8, v8, Lcom/policydm/db/XDBConRefPX;->nPortNbr:I

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    .line 100
    .local v6, "szPortNbr":Ljava/lang/String;
    invoke-virtual {v7, v6}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 101
    new-instance v8, Lcom/policydm/ui/XUINetProfileActivity$3;

    invoke-direct {v8, p0}, Lcom/policydm/ui/XUINetProfileActivity$3;-><init>(Lcom/policydm/ui/XUINetProfileActivity;)V

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 120
    const v8, 0x7f0a000d

    invoke-virtual {p0, v8}, Lcom/policydm/ui/XUINetProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .end local v7    # "text":Landroid/widget/EditText;
    check-cast v7, Landroid/widget/EditText;

    .line 121
    .restart local v7    # "text":Landroid/widget/EditText;
    sget-object v8, Lcom/policydm/ui/XUINetProfileActivity;->m_LocalNetworkProfileList_t:Lcom/policydm/db/XDBNetworkProfileList;

    iget-object v8, v8, Lcom/policydm/db/XDBNetworkProfileList;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    iget-object v8, v8, Lcom/policydm/db/XDBInfoConRef;->NAP:Lcom/policydm/db/XDBConRefNAP;

    iget-object v8, v8, Lcom/policydm/db/XDBConRefNAP;->Auth:Lcom/policydm/db/XDBConRefAuth;

    iget-object v8, v8, Lcom/policydm/db/XDBConRefAuth;->PAP_ID:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 122
    new-instance v8, Lcom/policydm/ui/XUINetProfileActivity$4;

    invoke-direct {v8, p0}, Lcom/policydm/ui/XUINetProfileActivity$4;-><init>(Lcom/policydm/ui/XUINetProfileActivity;)V

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 139
    const v8, 0x7f0a000f

    invoke-virtual {p0, v8}, Lcom/policydm/ui/XUINetProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .end local v7    # "text":Landroid/widget/EditText;
    check-cast v7, Landroid/widget/EditText;

    .line 140
    .restart local v7    # "text":Landroid/widget/EditText;
    sget-object v8, Lcom/policydm/ui/XUINetProfileActivity;->m_LocalNetworkProfileList_t:Lcom/policydm/db/XDBNetworkProfileList;

    iget-object v8, v8, Lcom/policydm/db/XDBNetworkProfileList;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    iget-object v8, v8, Lcom/policydm/db/XDBInfoConRef;->NAP:Lcom/policydm/db/XDBConRefNAP;

    iget-object v8, v8, Lcom/policydm/db/XDBConRefNAP;->Auth:Lcom/policydm/db/XDBConRefAuth;

    iget-object v8, v8, Lcom/policydm/db/XDBConRefAuth;->PAP_Secret:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 141
    new-instance v8, Lcom/policydm/ui/XUINetProfileActivity$5;

    invoke-direct {v8, p0}, Lcom/policydm/ui/XUINetProfileActivity$5;-><init>(Lcom/policydm/ui/XUINetProfileActivity;)V

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 157
    const v8, 0x7f0a0011

    invoke-virtual {p0, v8}, Lcom/policydm/ui/XUINetProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Spinner;

    .line 159
    .local v4, "spin":Landroid/widget/Spinner;
    const/4 v2, 0x0

    .line 160
    .local v2, "nApnType":I
    sget-object v8, Lcom/policydm/ui/XUIMainActivity;->g_UiNetInfo:Lcom/policydm/adapter/XDMTelephonyAdapter;

    iget-object v8, v8, Lcom/policydm/adapter/XDMTelephonyAdapter;->apntype:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 161
    const/4 v2, -0x1

    .line 172
    :goto_0
    const/high16 v8, 0x7f060000

    invoke-static {p0, v8, v10}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v0

    .line 173
    .local v0, "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    invoke-virtual {v0, v11}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 174
    invoke-virtual {v4, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 175
    invoke-virtual {v4, v2}, Landroid/widget/Spinner;->setSelection(I)V

    .line 176
    new-instance v8, Lcom/policydm/ui/XUINetProfileActivity$6;

    invoke-direct {v8, p0}, Lcom/policydm/ui/XUINetProfileActivity$6;-><init>(Lcom/policydm/ui/XUINetProfileActivity;)V

    invoke-virtual {v4, v8}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 197
    const v8, 0x7f0a0013

    invoke-virtual {p0, v8}, Lcom/policydm/ui/XUINetProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Spinner;

    .line 198
    .local v5, "spin1":Landroid/widget/Spinner;
    const/4 v3, 0x0

    .line 200
    .local v3, "nAuthType":I
    sget-object v8, Lcom/policydm/ui/XUIMainActivity;->g_UiNetInfo:Lcom/policydm/adapter/XDMTelephonyAdapter;

    iget-object v8, v8, Lcom/policydm/adapter/XDMTelephonyAdapter;->auth:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 201
    sget-object v8, Lcom/policydm/ui/XUIMainActivity;->g_UiNetInfo:Lcom/policydm/adapter/XDMTelephonyAdapter;

    iget-object v8, v8, Lcom/policydm/adapter/XDMTelephonyAdapter;->auth:Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 203
    :cond_0
    const v8, 0x7f060002

    invoke-static {p0, v8, v10}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v1

    .line 204
    .local v1, "adapter1":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    invoke-virtual {v1, v11}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 205
    invoke-virtual {v5, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 206
    add-int/lit8 v8, v3, -0x1

    invoke-virtual {v5, v8}, Landroid/widget/Spinner;->setSelection(I)V

    .line 207
    new-instance v8, Lcom/policydm/ui/XUINetProfileActivity$7;

    invoke-direct {v8, p0}, Lcom/policydm/ui/XUINetProfileActivity$7;-><init>(Lcom/policydm/ui/XUINetProfileActivity;)V

    invoke-virtual {v5, v8}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 218
    return-void

    .line 164
    .end local v0    # "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    .end local v1    # "adapter1":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    .end local v3    # "nAuthType":I
    .end local v5    # "spin1":Landroid/widget/Spinner;
    :cond_1
    const-string v8, "*"

    sget-object v9, Lcom/policydm/ui/XUIMainActivity;->g_UiNetInfo:Lcom/policydm/adapter/XDMTelephonyAdapter;

    iget-object v9, v9, Lcom/policydm/adapter/XDMTelephonyAdapter;->apntype:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_2

    .line 165
    const/4 v2, 0x0

    goto :goto_0

    .line 166
    :cond_2
    const-string v8, "default"

    sget-object v9, Lcom/policydm/ui/XUIMainActivity;->g_UiNetInfo:Lcom/policydm/adapter/XDMTelephonyAdapter;

    iget-object v9, v9, Lcom/policydm/adapter/XDMTelephonyAdapter;->apntype:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_3

    .line 167
    const/4 v2, 0x1

    goto :goto_0

    .line 169
    :cond_3
    const/4 v2, 0x2

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 310
    invoke-super {p0, p1}, Landroid/app/TabActivity;->onCreate(Landroid/os/Bundle;)V

    .line 311
    invoke-virtual {p0}, Lcom/policydm/ui/XUINetProfileActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "profileIndex"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    sput v3, Lcom/policydm/ui/XUINetProfileActivity;->m_nProfile:I

    .line 312
    sput-boolean v6, Lcom/policydm/ui/XUINetProfileActivity;->m_bUpdateState:Z

    .line 313
    const-wide/16 v3, 0x1

    invoke-static {v3, v4}, Lcom/policydm/db/XDBSqlQuery;->xdmDbExistsNetworkRow(J)Z

    move-result v3

    if-nez v3, :cond_0

    .line 315
    const-string v3, "Please Setting APN"

    invoke-static {p0, v3, v5}, Lcom/policydm/XDMApplication;->xdmShowToast(Landroid/content/Context;Ljava/lang/String;I)V

    .line 316
    invoke-virtual {p0}, Lcom/policydm/ui/XUINetProfileActivity;->finish()V

    .line 360
    :goto_0
    return-void

    .line 320
    :cond_0
    new-instance v3, Lcom/policydm/db/XDBNetworkProfileList;

    invoke-direct {v3}, Lcom/policydm/db/XDBNetworkProfileList;-><init>()V

    sput-object v3, Lcom/policydm/ui/XUINetProfileActivity;->m_LocalNetworkProfileList_t:Lcom/policydm/db/XDBNetworkProfileList;

    .line 321
    sget-object v3, Lcom/policydm/db/XDB;->NetProfileClass:Lcom/policydm/db/XDBNetworkProfileList;

    sput-object v3, Lcom/policydm/ui/XUINetProfileActivity;->m_LocalNetworkProfileList_t:Lcom/policydm/db/XDBNetworkProfileList;

    .line 323
    invoke-virtual {p0}, Lcom/policydm/ui/XUINetProfileActivity;->getTabHost()Landroid/widget/TabHost;

    move-result-object v1

    .line 324
    .local v1, "tabHost":Landroid/widget/TabHost;
    sget-object v3, Lcom/policydm/ui/XUIMainActivity;->g_UiNetInfo:Lcom/policydm/adapter/XDMTelephonyAdapter;

    iget-object v0, v3, Lcom/policydm/adapter/XDMTelephonyAdapter;->szAccountName:Ljava/lang/String;

    .line 326
    .local v0, "item":Ljava/lang/String;
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f030001

    invoke-virtual {v1}, Landroid/widget/TabHost;->getTabContentView()Landroid/widget/FrameLayout;

    move-result-object v5

    invoke-virtual {v3, v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 329
    const-string v3, "tab1"

    invoke-virtual {v1, v3}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v2

    .line 330
    .local v2, "ts1":Landroid/widget/TabHost$TabSpec;
    invoke-virtual {p0}, Lcom/policydm/ui/XUINetProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x108000c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)Landroid/widget/TabHost$TabSpec;

    .line 331
    const v3, 0x7f0a0005

    invoke-virtual {v2, v3}, Landroid/widget/TabHost$TabSpec;->setContent(I)Landroid/widget/TabHost$TabSpec;

    .line 332
    invoke-virtual {v1, v2}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    .line 334
    new-instance v3, Lcom/policydm/ui/XUINetProfileActivity$11;

    invoke-direct {v3, p0}, Lcom/policydm/ui/XUINetProfileActivity$11;-><init>(Lcom/policydm/ui/XUINetProfileActivity;)V

    invoke-virtual {v1, v3}, Landroid/widget/TabHost;->setOnTabChangedListener(Landroid/widget/TabHost$OnTabChangeListener;)V

    .line 357
    sget v3, Lcom/policydm/ui/XUINetProfileActivity;->m_nNProfile:I

    invoke-direct {p0, v3}, Lcom/policydm/ui/XUINetProfileActivity;->xuiCurrentTabSet(I)V

    .line 358
    sget v3, Lcom/policydm/ui/XUINetProfileActivity;->m_nNProfile:I

    invoke-virtual {v1, v3}, Landroid/widget/TabHost;->setCurrentTab(I)V

    goto :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 232
    packed-switch p1, :pswitch_data_0

    .line 274
    invoke-super {p0, p1}, Landroid/app/TabActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 235
    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x1080027

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1010355

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "PROFILE EDIT"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "Do you want Save and Close Network info ?"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/policydm/ui/XUINetProfileActivity$10;

    invoke-direct {v1, p0}, Lcom/policydm/ui/XUINetProfileActivity$10;-><init>(Lcom/policydm/ui/XUINetProfileActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f080057

    new-instance v2, Lcom/policydm/ui/XUINetProfileActivity$9;

    invoke-direct {v2, p0}, Lcom/policydm/ui/XUINetProfileActivity$9;-><init>(Lcom/policydm/ui/XUINetProfileActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f080006

    new-instance v2, Lcom/policydm/ui/XUINetProfileActivity$8;

    invoke-direct {v2, p0}, Lcom/policydm/ui/XUINetProfileActivity$8;-><init>(Lcom/policydm/ui/XUINetProfileActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 232
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v2, 0x0

    .line 301
    const-string v0, "SAVE"

    invoke-interface {p1, v2, v2, v2, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x108004e

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 302
    const/4 v0, 0x1

    const-string v1, "Revert"

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x108004c

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 303
    const/4 v0, 0x2

    const-string v1, "Edit Profile Info"

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x108003e

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 304
    invoke-super {p0, p1}, Landroid/app/TabActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 385
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 395
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/app/TabActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0

    .line 388
    :pswitch_0
    invoke-virtual {p0}, Lcom/policydm/ui/XUINetProfileActivity;->xuiNetProfileSave()V

    .line 389
    invoke-virtual {p0}, Lcom/policydm/ui/XUINetProfileActivity;->finish()V

    goto :goto_0

    .line 385
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 280
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 295
    :goto_0
    invoke-super {p0, p1}, Landroid/app/TabActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 283
    :pswitch_0
    invoke-virtual {p0}, Lcom/policydm/ui/XUINetProfileActivity;->xuiNetProfileSave()V

    .line 284
    invoke-virtual {p0}, Lcom/policydm/ui/XUINetProfileActivity;->finish()V

    goto :goto_0

    .line 287
    :pswitch_1
    invoke-virtual {p0}, Lcom/policydm/ui/XUINetProfileActivity;->finish()V

    goto :goto_0

    .line 290
    :pswitch_2
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/policydm/ui/XUINetProfileActivity;->showDialog(I)V

    goto :goto_0

    .line 280
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected xuiNetProfileSave()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 364
    const-string v0, ""

    .line 365
    .local v0, "proxyAddr":Ljava/lang/String;
    sget-object v1, Lcom/policydm/ui/XUINetProfileActivity;->m_LocalNetworkProfileList_t:Lcom/policydm/db/XDBNetworkProfileList;

    iget-object v1, v1, Lcom/policydm/db/XDBNetworkProfileList;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    iget-object v1, v1, Lcom/policydm/db/XDBInfoConRef;->PX:Lcom/policydm/db/XDBConRefPX;

    iget-object v0, v1, Lcom/policydm/db/XDBConRefPX;->Addr:Ljava/lang/String;

    .line 366
    if-eqz v0, :cond_0

    const-string v1, "0.0.0.0"

    invoke-virtual {v1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    .line 368
    sget-object v1, Lcom/policydm/ui/XUINetProfileActivity;->m_LocalNetworkProfileList_t:Lcom/policydm/db/XDBNetworkProfileList;

    iget-object v1, v1, Lcom/policydm/db/XDBNetworkProfileList;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    iput-boolean v3, v1, Lcom/policydm/db/XDBInfoConRef;->bProxyUse:Z

    .line 375
    :goto_0
    sget-object v1, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v1, v1, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    sget-object v2, Lcom/policydm/ui/XUINetProfileActivity;->m_LocalNetworkProfileList_t:Lcom/policydm/db/XDBNetworkProfileList;

    iget-object v2, v2, Lcom/policydm/db/XDBNetworkProfileList;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    iput-object v2, v1, Lcom/policydm/db/XDBProfileInfo;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    .line 377
    sget-object v1, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v1, v1, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    iget-object v1, v1, Lcom/policydm/db/XDBProfileInfo;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    invoke-static {v1}, Lcom/policydm/db/XDBProfileAdp;->xdbSetConRef(Lcom/policydm/db/XDBInfoConRef;)V

    .line 379
    const-string v1, "Saved"

    invoke-static {p0, v1, v3}, Lcom/policydm/XDMApplication;->xdmShowToast(Landroid/content/Context;Ljava/lang/String;I)V

    .line 380
    return-void

    .line 372
    :cond_0
    sget-object v1, Lcom/policydm/ui/XUINetProfileActivity;->m_LocalNetworkProfileList_t:Lcom/policydm/db/XDBNetworkProfileList;

    iget-object v1, v1, Lcom/policydm/db/XDBNetworkProfileList;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/policydm/db/XDBInfoConRef;->bProxyUse:Z

    goto :goto_0
.end method
