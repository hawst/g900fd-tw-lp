.class public Lcom/policydm/ui/XUIMainActivity;
.super Landroid/preference/PreferenceActivity;
.source "XUIMainActivity.java"

# interfaces
.implements Lcom/policydm/interfaces/XDMInterface;


# static fields
.field private static final KEY_DM_SETTING:Ljava/lang/String; = "settingdm"

.field private static final KEY_SOFTWARE_UPDATE:Ljava/lang/String; = "startdm"

.field public static g_UiNetInfo:Lcom/policydm/adapter/XDMTelephonyAdapter;

.field private static m_Builder:Landroid/app/AlertDialog$Builder;

.field private static m_ConnectAlertDialog:Landroid/app/AlertDialog;


# instance fields
.field private m_Context:Landroid/content/Context;

.field private m_szResponseText:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 49
    sput-object v0, Lcom/policydm/ui/XUIMainActivity;->m_Builder:Landroid/app/AlertDialog$Builder;

    .line 50
    sput-object v0, Lcom/policydm/ui/XUIMainActivity;->m_ConnectAlertDialog:Landroid/app/AlertDialog;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 51
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/ui/XUIMainActivity;->m_szResponseText:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/policydm/ui/XUIMainActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/policydm/ui/XUIMainActivity;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/policydm/ui/XUIMainActivity;->xuiCallUiDmSetting()V

    return-void
.end method

.method static synthetic access$100(Lcom/policydm/ui/XUIMainActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/policydm/ui/XUIMainActivity;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/policydm/ui/XUIMainActivity;->m_szResponseText:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/policydm/ui/XUIMainActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/policydm/ui/XUIMainActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/policydm/ui/XUIMainActivity;->m_szResponseText:Ljava/lang/String;

    return-object p1
.end method

.method private xuiCallUiDmSetting()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 168
    sget-boolean v2, Lcom/policydm/agent/XDMTask;->g_IsDMInitialized:Z

    if-nez v2, :cond_0

    .line 170
    const v2, 0x7f080048

    invoke-virtual {p0, v2}, Lcom/policydm/ui/XUIMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2, v3}, Lcom/policydm/XDMApplication;->xdmShowToast(Landroid/content/Context;Ljava/lang/String;I)V

    .line 186
    :goto_0
    return-void

    .line 174
    :cond_0
    invoke-static {}, Lcom/policydm/agent/XDMAgent;->xdmAgentGetSyncMode()I

    move-result v2

    if-lez v2, :cond_1

    .line 176
    const/4 v1, 0x0

    .line 177
    .local v1, "msg":Ljava/lang/String;
    const v2, 0x7f08000a

    invoke-virtual {p0, v2}, Lcom/policydm/ui/XUIMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 178
    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 179
    const v2, 0x7f080032

    invoke-virtual {p0, v2}, Lcom/policydm/ui/XUIMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 180
    invoke-static {p0, v1, v3}, Lcom/policydm/XDMApplication;->xdmShowToast(Landroid/content/Context;Ljava/lang/String;I)V

    goto :goto_0

    .line 184
    .end local v1    # "msg":Ljava/lang/String;
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/policydm/ui/XUISettingActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 185
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/policydm/ui/XUIMainActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private xuiInputCommandDialog()V
    .locals 7

    .prologue
    .line 190
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 191
    .local v0, "factory":Landroid/view/LayoutInflater;
    const v4, 0x7f030003

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 192
    .local v3, "textEntryView":Landroid/view/View;
    const v4, 0x7f0a0026

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 193
    .local v1, "msg":Landroid/widget/TextView;
    const v4, 0x7f0a0027

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    .line 195
    .local v2, "text":Landroid/widget/EditText;
    const-string v4, "Input command key string"

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 196
    new-instance v4, Lcom/policydm/ui/XUIMainActivity$3;

    invoke-direct {v4, p0}, Lcom/policydm/ui/XUIMainActivity$3;-><init>(Lcom/policydm/ui/XUIMainActivity;)V

    invoke-virtual {v2, v4}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 212
    new-instance v4, Landroid/app/AlertDialog$Builder;

    iget-object v5, p0, Lcom/policydm/ui/XUIMainActivity;->m_Context:Landroid/content/Context;

    invoke-direct {v4, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sput-object v4, Lcom/policydm/ui/XUIMainActivity;->m_Builder:Landroid/app/AlertDialog$Builder;

    .line 213
    sget-object v4, Lcom/policydm/ui/XUIMainActivity;->m_Builder:Landroid/app/AlertDialog$Builder;

    const v5, 0x7f080033

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 214
    sget-object v4, Lcom/policydm/ui/XUIMainActivity;->m_Builder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v4, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f080031

    new-instance v6, Lcom/policydm/ui/XUIMainActivity$4;

    invoke-direct {v6, p0}, Lcom/policydm/ui/XUIMainActivity$4;-><init>(Lcom/policydm/ui/XUIMainActivity;)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 221
    sget-object v4, Lcom/policydm/ui/XUIMainActivity;->m_Builder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    sput-object v4, Lcom/policydm/ui/XUIMainActivity;->m_ConnectAlertDialog:Landroid/app/AlertDialog;

    .line 222
    sget-object v4, Lcom/policydm/ui/XUIMainActivity;->m_ConnectAlertDialog:Landroid/app/AlertDialog;

    new-instance v5, Lcom/policydm/ui/XUIMainActivity$5;

    invoke-direct {v5, p0}, Lcom/policydm/ui/XUIMainActivity$5;-><init>(Lcom/policydm/ui/XUIMainActivity;)V

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 229
    sget-object v4, Lcom/policydm/ui/XUIMainActivity;->m_ConnectAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v4}, Landroid/app/AlertDialog;->show()V

    .line 230
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 65
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 67
    iput-object p0, p0, Lcom/policydm/ui/XUIMainActivity;->m_Context:Landroid/content/Context;

    .line 69
    new-instance v2, Lcom/policydm/adapter/XDMTelephonyAdapter;

    iget-object v3, p0, Lcom/policydm/ui/XUIMainActivity;->m_Context:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/policydm/adapter/XDMTelephonyAdapter;-><init>(Landroid/content/Context;)V

    sput-object v2, Lcom/policydm/ui/XUIMainActivity;->g_UiNetInfo:Lcom/policydm/adapter/XDMTelephonyAdapter;

    .line 70
    sget-object v2, Lcom/policydm/ui/XUIMainActivity;->g_UiNetInfo:Lcom/policydm/adapter/XDMTelephonyAdapter;

    iget-object v2, v2, Lcom/policydm/adapter/XDMTelephonyAdapter;->napAddr:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;

    if-nez v2, :cond_0

    .line 71
    sget-object v2, Lcom/policydm/ui/XUIMainActivity;->g_UiNetInfo:Lcom/policydm/adapter/XDMTelephonyAdapter;

    new-instance v3, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;

    invoke-direct {v3}, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;-><init>()V

    iput-object v3, v2, Lcom/policydm/adapter/XDMTelephonyAdapter;->napAddr:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;

    .line 73
    :cond_0
    sget-object v2, Lcom/policydm/ui/XUIMainActivity;->g_UiNetInfo:Lcom/policydm/adapter/XDMTelephonyAdapter;

    iget-object v2, v2, Lcom/policydm/adapter/XDMTelephonyAdapter;->authInfo:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMAuthInfo;

    if-nez v2, :cond_1

    .line 74
    sget-object v2, Lcom/policydm/ui/XUIMainActivity;->g_UiNetInfo:Lcom/policydm/adapter/XDMTelephonyAdapter;

    new-instance v3, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMAuthInfo;

    invoke-direct {v3}, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMAuthInfo;-><init>()V

    iput-object v3, v2, Lcom/policydm/adapter/XDMTelephonyAdapter;->authInfo:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMAuthInfo;

    .line 76
    :cond_1
    invoke-static {}, Lcom/policydm/adapter/XDMNetworkAdapter;->xdbAdpGetProxyData()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/policydm/adapter/XDMTelephonyAdapter;

    sput-object v2, Lcom/policydm/ui/XUIMainActivity;->g_UiNetInfo:Lcom/policydm/adapter/XDMTelephonyAdapter;

    .line 78
    const v2, 0x7f040001

    invoke-virtual {p0, v2}, Lcom/policydm/ui/XUIMainActivity;->addPreferencesFromResource(I)V

    .line 80
    const-string v2, "startdm"

    invoke-virtual {p0, v2}, Lcom/policydm/ui/XUIMainActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 81
    .local v1, "SwPref":Landroid/preference/Preference;
    if-eqz v1, :cond_2

    .line 83
    new-instance v2, Lcom/policydm/ui/XUIMainActivity$1;

    invoke-direct {v2, p0}, Lcom/policydm/ui/XUIMainActivity$1;-><init>(Lcom/policydm/ui/XUIMainActivity;)V

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 93
    :cond_2
    const-string v2, "settingdm"

    invoke-virtual {p0, v2}, Lcom/policydm/ui/XUIMainActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 94
    .local v0, "SettingPref":Landroid/preference/Preference;
    if-eqz v0, :cond_3

    .line 96
    new-instance v2, Lcom/policydm/ui/XUIMainActivity$2;

    invoke-direct {v2, p0}, Lcom/policydm/ui/XUIMainActivity$2;-><init>(Lcom/policydm/ui/XUIMainActivity;)V

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 106
    :cond_3
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/policydm/ui/XUIMainActivity;->setDefaultKeyMode(I)V

    .line 107
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v1, 0x0

    .line 112
    const-string v0, "Input MasterKey"

    invoke-interface {p1, v1, v1, v1, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x1080042

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 113
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 119
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 127
    :goto_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 122
    :pswitch_0
    invoke-direct {p0}, Lcom/policydm/ui/XUIMainActivity;->xuiInputCommandDialog()V

    goto :goto_0

    .line 119
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method protected onStart()V
    .locals 0

    .prologue
    .line 59
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onStart()V

    .line 60
    return-void
.end method

.method protected xuiCallUiDmStartDm()V
    .locals 6

    .prologue
    const v5, 0x7f080032

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 134
    const-string v2, ""

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 136
    sget-boolean v2, Lcom/policydm/agent/XDMTask;->g_IsDMInitialized:Z

    if-nez v2, :cond_0

    .line 138
    const v2, 0x7f080048

    invoke-virtual {p0, v2}, Lcom/policydm/ui/XUIMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2, v3}, Lcom/policydm/XDMApplication;->xdmShowToast(Landroid/content/Context;Ljava/lang/String;I)V

    .line 163
    :goto_0
    return-void

    .line 142
    :cond_0
    const/4 v0, 0x0

    .line 143
    .local v0, "msg":Ljava/lang/String;
    const v2, 0x7f08000a

    invoke-virtual {p0, v2}, Lcom/policydm/ui/XUIMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 144
    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 145
    invoke-virtual {p0, v5}, Lcom/policydm/ui/XUIMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 147
    invoke-static {}, Lcom/policydm/agent/XDMAgent;->xdmAgentGetSyncMode()I

    move-result v2

    if-lez v2, :cond_1

    .line 149
    invoke-static {p0, v0, v3}, Lcom/policydm/XDMApplication;->xdmShowToast(Landroid/content/Context;Ljava/lang/String;I)V

    goto :goto_0

    .line 153
    :cond_1
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMOStatus()I

    move-result v1

    .line 154
    .local v1, "nStatus":I
    if-eqz v1, :cond_2

    .line 156
    const-string v2, "FumoStatus not none!! return"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 160
    :cond_2
    invoke-virtual {p0, v5}, Lcom/policydm/ui/XUIMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2, v3}, Lcom/policydm/XDMApplication;->xdmShowToast(Landroid/content/Context;Ljava/lang/String;I)V

    .line 161
    invoke-static {v4}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOInitiatedType(I)V

    .line 162
    invoke-static {}, Lcom/policydm/db/XDBProfileListAdp;->xdbGetProflieIndex()I

    move-result v2

    invoke-static {v4, v2}, Lcom/policydm/ui/XUIAdapter;->xuiAdpUserInitiate(II)V

    goto :goto_0
.end method

.method protected xuiSelectTestMenu(Ljava/lang/String;)V
    .locals 3
    .param p1, "szInputKey"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 234
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "key event occured on Dialog : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 235
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 274
    :cond_0
    :goto_0
    return-void

    .line 238
    :cond_1
    const-string v0, "reset"

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_2

    .line 240
    invoke-static {}, Lcom/policydm/db/XDB;->xdbFullResetAll()V

    .line 241
    sget-object v0, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    invoke-static {}, Lcom/policydm/db/XDBProfileAdp;->xdbGetProfileInfo()Lcom/policydm/db/XDBProfileInfo;

    move-result-object v1

    iput-object v1, v0, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    .line 242
    invoke-static {v2}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOStatus(I)V

    .line 243
    invoke-static {v2}, Lcom/policydm/ui/XUIAdapter;->xuiAdpSetUiMode(I)V

    .line 244
    const-string v0, "Reset FFS Complete"

    invoke-static {p0, v0, v2}, Lcom/policydm/XDMApplication;->xdmShowToast(Landroid/content/Context;Ljava/lang/String;I)V

    goto :goto_0

    .line 246
    :cond_2
    const-string v0, "dev"

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_3

    .line 248
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GetOEMName :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetOEM()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 249
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GetFullDeviceID "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/policydm/adapter/XDMDevinfAdapter;->xdmDevAdpGetFullDeviceID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 250
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GetManufacturer :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetManufacturer()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 251
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GetModel :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetModel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 252
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GetTargetLanguage :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 253
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GetTelephonyMcc :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetTelephonyMcc()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 254
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GetTelephonyMnc :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetTelephonyMnc()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 255
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GetFirmwareVersion :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/policydm/adapter/XDMDevinfAdapter;->xdmDevAdpGetFirmwareVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 256
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GetSoftwareVersion :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetSwV()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 257
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GetTargetHwV :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetHwV()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 258
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GetIMSIFromSIM :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetIMSIFromSIM()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 259
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GetTargetSIMState :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetSIMState()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 260
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GetAvailableMemorySize :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetAvailableMemorySize(I)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 262
    :cond_3
    const-string v0, "wbxml"

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_4

    .line 264
    invoke-static {}, Lcom/policydm/agent/XDMDebug;->xdmSetWbxmlFileLogOnOff()V

    goto/16 :goto_0

    .line 266
    :cond_4
    const-string v0, "dump"

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_5

    .line 268
    invoke-static {}, Lcom/policydm/agent/XDMDebug;->xdmSetWbxmlDumpLogOnOff()V

    goto/16 :goto_0

    .line 270
    :cond_5
    const-string v0, "privatelog"

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 272
    invoke-static {}, Lcom/policydm/agent/XDMDebug;->xdmSetPrivateLogOnOff()V

    goto/16 :goto_0
.end method
