.class public Lcom/policydm/ui/XUIAdapter;
.super Ljava/lang/Object;
.source "XUIAdapter.java"

# interfaces
.implements Lcom/policydm/interfaces/XDMInterface;
.implements Lcom/policydm/interfaces/XEventInterface;
.implements Lcom/policydm/interfaces/XUIInterface;


# static fields
.field private static m_nDmUiMode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    sput v0, Lcom/policydm/ui/XUIAdapter;->m_nDmUiMode:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xuiAdpGetUiMode()I
    .locals 2

    .prologue
    .line 28
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "nDmUiMode : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/policydm/ui/XUIAdapter;->m_nDmUiMode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 29
    sget v0, Lcom/policydm/ui/XUIAdapter;->m_nDmUiMode:I

    return v0
.end method

.method public static xuiAdpRequestNoti(I)V
    .locals 4
    .param p0, "nMsgType"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 40
    const-string v0, ""

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 42
    invoke-static {}, Lcom/policydm/db/XDBProfileListAdp;->xdbGetNotiOpMode()I

    move-result v0

    if-eqz v0, :cond_1

    .line 44
    const-string v0, "SPD Noti CONFIG mode!!"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 45
    const/16 v0, 0x40

    invoke-static {v0, v3, v3}, Lcom/policydm/eng/core/XDMMsg;->xdmSendRCMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 88
    :cond_0
    :goto_0
    return-void

    .line 50
    :cond_1
    const-string v0, "SPD Noti DOWNLOAD mode!!"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 51
    invoke-static {v1}, Lcom/policydm/db/XDBProfileListAdp;->xdbSetProfileIndex(I)V

    .line 54
    packed-switch p0, :pswitch_data_0

    goto :goto_0

    .line 61
    :pswitch_0
    invoke-static {}, Lcom/policydm/db/XDB;->xdbCheckProfileListExist()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    invoke-static {v2}, Lcom/policydm/db/XDBProfileListAdp;->xdbSetNotiEvent(I)V

    .line 64
    invoke-static {}, Lcom/policydm/ui/XUIAdapter;->xuiAdpStartSession()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    invoke-static {v2}, Lcom/policydm/ui/XUIAdapter;->xuiAdpSetUiMode(I)V

    goto :goto_0

    .line 71
    :pswitch_1
    invoke-static {}, Lcom/policydm/db/XDB;->xdbCheckProfileListExist()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/policydm/db/XDBProfileListAdp;->xdbSetNotiEvent(I)V

    .line 74
    invoke-static {}, Lcom/policydm/ui/XUIAdapter;->xuiAdpStartSession()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 76
    invoke-static {v1}, Lcom/policydm/ui/XUIAdapter;->xuiAdpSetUiMode(I)V

    .line 77
    const/16 v0, 0x67

    invoke-static {v0}, Lcom/policydm/XDMApplication;->xdmSendMessageDmHandler(I)V

    goto :goto_0

    .line 81
    :cond_2
    const-string v0, "Unable to connect network"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 54
    nop

    :pswitch_data_0
    .packed-switch 0x8c
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static xuiAdpSetUiMode(I)V
    .locals 2
    .param p0, "nUiMode"    # I

    .prologue
    .line 34
    sput p0, Lcom/policydm/ui/XUIAdapter;->m_nDmUiMode:I

    .line 35
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "nDmUiMode : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/policydm/ui/XUIAdapter;->m_nDmUiMode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 36
    return-void
.end method

.method public static xuiAdpStartSession()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 92
    const-string v2, ""

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 93
    const/4 v0, 0x0

    .line 94
    .local v0, "nStatus":I
    invoke-static {v1}, Lcom/policydm/db/XDB;->xdbSetWaitWifiFlag(Z)V

    .line 96
    invoke-static {}, Lcom/policydm/adapter/XDMInitAdapter;->xdmInitAdpCheckNetworkReady()I

    move-result v0

    .line 97
    if-eqz v0, :cond_0

    .line 107
    :goto_0
    return v1

    .line 102
    :cond_0
    invoke-static {}, Lcom/policydm/tp/XTPAdapter;->xtpAdpResetWBXMLLog()V

    .line 103
    invoke-static {}, Lcom/policydm/polling/XPollingAgent;->xPollingReportReStartAlarm()V

    .line 105
    const/16 v1, 0x65

    invoke-static {v3, v1}, Lcom/policydm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    .line 106
    const/16 v1, 0xa

    invoke-static {v1, v3, v3}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 107
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static xuiAdpUserInitiate(I)V
    .locals 1
    .param p0, "initType"    # I

    .prologue
    .line 112
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/policydm/ui/XUIAdapter;->xuiAdpUserInitiate(II)V

    .line 113
    return-void
.end method

.method public static xuiAdpUserInitiate(II)V
    .locals 5
    .param p0, "initType"    # I
    .param p1, "index"    # I

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 117
    const-string v1, ""

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 119
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/policydm/db/XDB;->xdbSetWaitWifiFlag(Z)V

    .line 120
    sget-boolean v1, Lcom/policydm/agent/XDMTask;->g_IsDMInitialized:Z

    if-nez v1, :cond_1

    .line 122
    const-string v1, "DM Not Init"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 123
    const-string v1, "Unable to connect network"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 196
    :cond_0
    :goto_0
    return-void

    .line 127
    :cond_1
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMOStatus()I

    move-result v0

    .line 128
    .local v0, "nStatus":I
    if-nez v0, :cond_2

    invoke-static {}, Lcom/policydm/agent/XDMAgent;->xdmAgentGetSyncMode()I

    move-result v1

    if-eqz v1, :cond_3

    .line 130
    :cond_2
    const-string v1, "connection to server... please wait"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 135
    :cond_3
    invoke-static {p1}, Lcom/policydm/db/XDBProfileListAdp;->xdbSetProfileIndex(I)V

    .line 137
    invoke-static {}, Lcom/policydm/adapter/XDMInitAdapter;->xdmInitAdpCheckNetworkReady()I

    move-result v1

    if-nez v1, :cond_9

    .line 139
    invoke-static {}, Lcom/policydm/tp/XTPAdapter;->xtpAdpHttpCookieClear()V

    .line 140
    invoke-static {p0}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOInitiatedType(I)V

    .line 142
    invoke-static {}, Lcom/policydm/db/XDB;->xdbGetAutoUpdateFlag()Z

    move-result v1

    if-nez v1, :cond_4

    if-ne p0, v4, :cond_4

    .line 144
    const-string v1, "Auto update is false, Not start polling"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 148
    :cond_4
    invoke-static {}, Lcom/policydm/db/XDB;->xdbGetWifiOnlyFlag()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-static {}, Lcom/policydm/XDMApplication;->xdmProtoIsWIFIConnected()Z

    move-result v1

    if-nez v1, :cond_7

    .line 150
    if-ne p0, v3, :cond_5

    .line 152
    const/4 v1, 0x0

    const/16 v2, 0x7c

    invoke-static {v1, v2}, Lcom/policydm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    goto :goto_0

    .line 155
    :cond_5
    if-ne p0, v4, :cond_6

    .line 157
    const/16 v1, 0x8

    invoke-static {v1}, Lcom/policydm/ui/XUINotificationManager;->xuiSetIndicator(I)V

    goto :goto_0

    .line 162
    :cond_6
    const-string v1, "Wifi not connected, Not start polling"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 167
    :cond_7
    const-wide/16 v1, 0x0

    invoke-static {v1, v2}, Lcom/policydm/db/XDBProfileListAdp;->xdbSetNotiJobId(J)V

    .line 168
    const/16 v1, 0x9

    invoke-static {v1}, Lcom/policydm/ui/XUINotificationManager;->xuiSetIndicator(I)V

    .line 169
    invoke-static {}, Lcom/policydm/ui/XUIAdapter;->xuiAdpStartSession()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 171
    invoke-static {}, Lcom/policydm/ui/XUIAdapter;->xuiAdpGetUiMode()I

    move-result v1

    if-nez v1, :cond_0

    .line 173
    if-ne p0, v3, :cond_8

    .line 175
    const/16 v1, 0x67

    invoke-static {v1}, Lcom/policydm/XDMApplication;->xdmSendMessageDmHandler(I)V

    .line 176
    invoke-static {v3}, Lcom/policydm/ui/XUIAdapter;->xuiAdpSetUiMode(I)V

    goto :goto_0

    .line 180
    :cond_8
    invoke-static {v4}, Lcom/policydm/ui/XUIAdapter;->xuiAdpSetUiMode(I)V

    goto :goto_0

    .line 187
    :cond_9
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmProtoIsWIFIConnected()Z

    move-result v1

    if-nez v1, :cond_a

    invoke-static {}, Lcom/policydm/XDMApplication;->xdmProtoIsMobileDataConnected()Z

    move-result v1

    if-nez v1, :cond_a

    .line 189
    const-string v1, "Unable to connect network"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 193
    :cond_a
    const-string v1, "connection to server... please wait"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
