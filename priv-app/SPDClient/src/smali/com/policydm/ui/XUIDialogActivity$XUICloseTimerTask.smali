.class public Lcom/policydm/ui/XUIDialogActivity$XUICloseTimerTask;
.super Ljava/util/TimerTask;
.source "XUIDialogActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/policydm/ui/XUIDialogActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "XUICloseTimerTask"
.end annotation


# instance fields
.field SelectDialog:I

.field final synthetic this$0:Lcom/policydm/ui/XUIDialogActivity;


# direct methods
.method public constructor <init>(Lcom/policydm/ui/XUIDialogActivity;I)V
    .locals 1
    .param p2, "type"    # I

    .prologue
    .line 92
    iput-object p1, p0, Lcom/policydm/ui/XUIDialogActivity$XUICloseTimerTask;->this$0:Lcom/policydm/ui/XUIDialogActivity;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 89
    const/4 v0, 0x0

    iput v0, p0, Lcom/policydm/ui/XUIDialogActivity$XUICloseTimerTask;->SelectDialog:I

    .line 93
    iput p2, p0, Lcom/policydm/ui/XUIDialogActivity$XUICloseTimerTask;->SelectDialog:I

    .line 94
    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/16 v2, 0x10

    .line 105
    iget v0, p0, Lcom/policydm/ui/XUIDialogActivity$XUICloseTimerTask;->SelectDialog:I

    packed-switch v0, :pswitch_data_0

    .line 145
    :goto_0
    return-void

    .line 108
    :pswitch_0
    iget-object v0, p0, Lcom/policydm/ui/XUIDialogActivity$XUICloseTimerTask;->this$0:Lcom/policydm/ui/XUIDialogActivity;

    # invokes: Lcom/policydm/ui/XUIDialogActivity;->xuiDlgCPopupKillTimer()V
    invoke-static {v0}, Lcom/policydm/ui/XUIDialogActivity;->access$000(Lcom/policydm/ui/XUIDialogActivity;)V

    .line 109
    iget-object v0, p0, Lcom/policydm/ui/XUIDialogActivity$XUICloseTimerTask;->this$0:Lcom/policydm/ui/XUIDialogActivity;

    iget v1, p0, Lcom/policydm/ui/XUIDialogActivity$XUICloseTimerTask;->SelectDialog:I

    invoke-virtual {v0, v1}, Lcom/policydm/ui/XUIDialogActivity;->removeDialog(I)V

    .line 110
    iget-object v0, p0, Lcom/policydm/ui/XUIDialogActivity$XUICloseTimerTask;->this$0:Lcom/policydm/ui/XUIDialogActivity;

    # invokes: Lcom/policydm/ui/XUIDialogActivity;->xuiDlgUicDisplayResult()V
    invoke-static {v0}, Lcom/policydm/ui/XUIDialogActivity;->access$100(Lcom/policydm/ui/XUIDialogActivity;)V

    .line 111
    iget-object v0, p0, Lcom/policydm/ui/XUIDialogActivity$XUICloseTimerTask;->this$0:Lcom/policydm/ui/XUIDialogActivity;

    invoke-virtual {v0}, Lcom/policydm/ui/XUIDialogActivity;->finish()V

    goto :goto_0

    .line 115
    :pswitch_1
    iget-object v0, p0, Lcom/policydm/ui/XUIDialogActivity$XUICloseTimerTask;->this$0:Lcom/policydm/ui/XUIDialogActivity;

    # invokes: Lcom/policydm/ui/XUIDialogActivity;->xuiDlgCPopupKillTimer()V
    invoke-static {v0}, Lcom/policydm/ui/XUIDialogActivity;->access$000(Lcom/policydm/ui/XUIDialogActivity;)V

    .line 116
    iget-object v0, p0, Lcom/policydm/ui/XUIDialogActivity$XUICloseTimerTask;->this$0:Lcom/policydm/ui/XUIDialogActivity;

    iget v1, p0, Lcom/policydm/ui/XUIDialogActivity$XUICloseTimerTask;->SelectDialog:I

    invoke-virtual {v0, v1}, Lcom/policydm/ui/XUIDialogActivity;->removeDialog(I)V

    .line 117
    iget-object v0, p0, Lcom/policydm/ui/XUIDialogActivity$XUICloseTimerTask;->this$0:Lcom/policydm/ui/XUIDialogActivity;

    sget-object v1, Lcom/policydm/ui/XUIDialogActivity;->g_UicOption:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/policydm/ui/XUIDialogActivity;->xuiDlgUicConfirmResult(Ljava/lang/Object;I)V

    .line 118
    iget-object v0, p0, Lcom/policydm/ui/XUIDialogActivity$XUICloseTimerTask;->this$0:Lcom/policydm/ui/XUIDialogActivity;

    invoke-virtual {v0}, Lcom/policydm/ui/XUIDialogActivity;->finish()V

    goto :goto_0

    .line 122
    :pswitch_2
    iget-object v0, p0, Lcom/policydm/ui/XUIDialogActivity$XUICloseTimerTask;->this$0:Lcom/policydm/ui/XUIDialogActivity;

    # invokes: Lcom/policydm/ui/XUIDialogActivity;->xuiDlgCPopupKillTimer()V
    invoke-static {v0}, Lcom/policydm/ui/XUIDialogActivity;->access$000(Lcom/policydm/ui/XUIDialogActivity;)V

    .line 123
    iget-object v0, p0, Lcom/policydm/ui/XUIDialogActivity$XUICloseTimerTask;->this$0:Lcom/policydm/ui/XUIDialogActivity;

    iget v1, p0, Lcom/policydm/ui/XUIDialogActivity$XUICloseTimerTask;->SelectDialog:I

    invoke-virtual {v0, v1}, Lcom/policydm/ui/XUIDialogActivity;->removeDialog(I)V

    .line 124
    iget-object v0, p0, Lcom/policydm/ui/XUIDialogActivity$XUICloseTimerTask;->this$0:Lcom/policydm/ui/XUIDialogActivity;

    sget-object v1, Lcom/policydm/ui/XUIDialogActivity;->g_UicOption:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/policydm/ui/XUIDialogActivity;->xuiDlgUicInputResult(Ljava/lang/Object;I)V

    .line 125
    iget-object v0, p0, Lcom/policydm/ui/XUIDialogActivity$XUICloseTimerTask;->this$0:Lcom/policydm/ui/XUIDialogActivity;

    invoke-virtual {v0}, Lcom/policydm/ui/XUIDialogActivity;->finish()V

    goto :goto_0

    .line 130
    :pswitch_3
    iget-object v0, p0, Lcom/policydm/ui/XUIDialogActivity$XUICloseTimerTask;->this$0:Lcom/policydm/ui/XUIDialogActivity;

    # invokes: Lcom/policydm/ui/XUIDialogActivity;->xuiDlgCPopupKillTimer()V
    invoke-static {v0}, Lcom/policydm/ui/XUIDialogActivity;->access$000(Lcom/policydm/ui/XUIDialogActivity;)V

    .line 131
    iget-object v0, p0, Lcom/policydm/ui/XUIDialogActivity$XUICloseTimerTask;->this$0:Lcom/policydm/ui/XUIDialogActivity;

    iget v1, p0, Lcom/policydm/ui/XUIDialogActivity$XUICloseTimerTask;->SelectDialog:I

    invoke-virtual {v0, v1}, Lcom/policydm/ui/XUIDialogActivity;->removeDialog(I)V

    .line 132
    iget-object v0, p0, Lcom/policydm/ui/XUIDialogActivity$XUICloseTimerTask;->this$0:Lcom/policydm/ui/XUIDialogActivity;

    sget-object v1, Lcom/policydm/ui/XUIDialogActivity;->g_UicOption:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/policydm/ui/XUIDialogActivity;->xuiDlgUicChoiceResult(Ljava/lang/Object;I)V

    .line 133
    iget-object v0, p0, Lcom/policydm/ui/XUIDialogActivity$XUICloseTimerTask;->this$0:Lcom/policydm/ui/XUIDialogActivity;

    invoke-virtual {v0}, Lcom/policydm/ui/XUIDialogActivity;->finish()V

    goto :goto_0

    .line 137
    :pswitch_4
    iget-object v0, p0, Lcom/policydm/ui/XUIDialogActivity$XUICloseTimerTask;->this$0:Lcom/policydm/ui/XUIDialogActivity;

    # invokes: Lcom/policydm/ui/XUIDialogActivity;->xuiDlgCPopupKillTimer()V
    invoke-static {v0}, Lcom/policydm/ui/XUIDialogActivity;->access$000(Lcom/policydm/ui/XUIDialogActivity;)V

    .line 138
    iget-object v0, p0, Lcom/policydm/ui/XUIDialogActivity$XUICloseTimerTask;->this$0:Lcom/policydm/ui/XUIDialogActivity;

    iget v1, p0, Lcom/policydm/ui/XUIDialogActivity$XUICloseTimerTask;->SelectDialog:I

    invoke-virtual {v0, v1}, Lcom/policydm/ui/XUIDialogActivity;->removeDialog(I)V

    .line 139
    iget-object v0, p0, Lcom/policydm/ui/XUIDialogActivity$XUICloseTimerTask;->this$0:Lcom/policydm/ui/XUIDialogActivity;

    invoke-virtual {v0}, Lcom/policydm/ui/XUIDialogActivity;->finish()V

    goto :goto_0

    .line 105
    :pswitch_data_0
    .packed-switch 0x6f
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public scheduledExecutionTime()J
    .locals 2

    .prologue
    .line 99
    invoke-super {p0}, Ljava/util/TimerTask;->scheduledExecutionTime()J

    move-result-wide v0

    return-wide v0
.end method
