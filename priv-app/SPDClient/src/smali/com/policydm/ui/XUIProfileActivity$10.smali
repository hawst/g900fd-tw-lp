.class Lcom/policydm/ui/XUIProfileActivity$10;
.super Ljava/lang/Object;
.source "XUIProfileActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/policydm/ui/XUIProfileActivity;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/policydm/ui/XUIProfileActivity;


# direct methods
.method constructor <init>(Lcom/policydm/ui/XUIProfileActivity;)V
    .locals 0

    .prologue
    .line 237
    iput-object p1, p0, Lcom/policydm/ui/XUIProfileActivity$10;->this$0:Lcom/policydm/ui/XUIProfileActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 8
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 241
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v6, 0x3

    if-ge v1, v6, :cond_0

    .line 243
    const-string v3, ""

    .line 244
    .local v3, "pAddress":Ljava/lang/String;
    const-string v0, ""

    .line 245
    .local v0, "URL":Ljava/lang/String;
    const/4 v2, 0x0

    .line 246
    .local v2, "nPort":I
    const-string v4, ""

    .line 247
    .local v4, "pProtocol":Ljava/lang/String;
    const/4 v5, 0x0

    .line 248
    .local v5, "parser":Lcom/policydm/db/XDBUrlInfo;
    iget-object v6, p0, Lcom/policydm/ui/XUIProfileActivity$10;->this$0:Lcom/policydm/ui/XUIProfileActivity;

    # getter for: Lcom/policydm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/policydm/db/XDBProfileInfo;
    invoke-static {v6}, Lcom/policydm/ui/XUIProfileActivity;->access$000(Lcom/policydm/ui/XUIProfileActivity;)[Lcom/policydm/db/XDBProfileInfo;

    move-result-object v6

    aget-object v6, v6, v1

    iget-object v6, v6, Lcom/policydm/db/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    invoke-static {v6}, Lcom/policydm/tp/XTPHttpUtil;->xtpURLParser(Ljava/lang/String;)Lcom/policydm/db/XDBUrlInfo;

    move-result-object v5

    .line 249
    iget-object v0, v5, Lcom/policydm/db/XDBUrlInfo;->pURL:Ljava/lang/String;

    .line 250
    iget-object v3, v5, Lcom/policydm/db/XDBUrlInfo;->pAddress:Ljava/lang/String;

    .line 251
    iget v2, v5, Lcom/policydm/db/XDBUrlInfo;->nPort:I

    .line 252
    iget-object v4, v5, Lcom/policydm/db/XDBUrlInfo;->pProtocol:Ljava/lang/String;

    .line 253
    iget-object v6, p0, Lcom/policydm/ui/XUIProfileActivity$10;->this$0:Lcom/policydm/ui/XUIProfileActivity;

    # getter for: Lcom/policydm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/policydm/db/XDBProfileInfo;
    invoke-static {v6}, Lcom/policydm/ui/XUIProfileActivity;->access$000(Lcom/policydm/ui/XUIProfileActivity;)[Lcom/policydm/db/XDBProfileInfo;

    move-result-object v6

    aget-object v6, v6, v1

    iput-object v0, v6, Lcom/policydm/db/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    .line 254
    iget-object v6, p0, Lcom/policydm/ui/XUIProfileActivity$10;->this$0:Lcom/policydm/ui/XUIProfileActivity;

    # getter for: Lcom/policydm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/policydm/db/XDBProfileInfo;
    invoke-static {v6}, Lcom/policydm/ui/XUIProfileActivity;->access$000(Lcom/policydm/ui/XUIProfileActivity;)[Lcom/policydm/db/XDBProfileInfo;

    move-result-object v6

    aget-object v6, v6, v1

    iput-object v3, v6, Lcom/policydm/db/XDBProfileInfo;->ServerIP:Ljava/lang/String;

    .line 255
    iget-object v6, p0, Lcom/policydm/ui/XUIProfileActivity$10;->this$0:Lcom/policydm/ui/XUIProfileActivity;

    # getter for: Lcom/policydm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/policydm/db/XDBProfileInfo;
    invoke-static {v6}, Lcom/policydm/ui/XUIProfileActivity;->access$000(Lcom/policydm/ui/XUIProfileActivity;)[Lcom/policydm/db/XDBProfileInfo;

    move-result-object v6

    aget-object v6, v6, v1

    iput v2, v6, Lcom/policydm/db/XDBProfileInfo;->ServerPort:I

    .line 256
    iget-object v6, p0, Lcom/policydm/ui/XUIProfileActivity$10;->this$0:Lcom/policydm/ui/XUIProfileActivity;

    # getter for: Lcom/policydm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/policydm/db/XDBProfileInfo;
    invoke-static {v6}, Lcom/policydm/ui/XUIProfileActivity;->access$000(Lcom/policydm/ui/XUIProfileActivity;)[Lcom/policydm/db/XDBProfileInfo;

    move-result-object v6

    aget-object v6, v6, v1

    iput-object v4, v6, Lcom/policydm/db/XDBProfileInfo;->Protocol:Ljava/lang/String;

    .line 258
    iget-object v6, p0, Lcom/policydm/ui/XUIProfileActivity$10;->this$0:Lcom/policydm/ui/XUIProfileActivity;

    # getter for: Lcom/policydm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/policydm/db/XDBProfileInfo;
    invoke-static {v6}, Lcom/policydm/ui/XUIProfileActivity;->access$000(Lcom/policydm/ui/XUIProfileActivity;)[Lcom/policydm/db/XDBProfileInfo;

    move-result-object v6

    aget-object v6, v6, v1

    iget-object v7, p0, Lcom/policydm/ui/XUIProfileActivity$10;->this$0:Lcom/policydm/ui/XUIProfileActivity;

    # getter for: Lcom/policydm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/policydm/db/XDBProfileInfo;
    invoke-static {v7}, Lcom/policydm/ui/XUIProfileActivity;->access$000(Lcom/policydm/ui/XUIProfileActivity;)[Lcom/policydm/db/XDBProfileInfo;

    move-result-object v7

    aget-object v7, v7, v1

    iget-object v7, v7, Lcom/policydm/db/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    iput-object v7, v6, Lcom/policydm/db/XDBProfileInfo;->ServerUrl_Org:Ljava/lang/String;

    .line 259
    iget-object v6, p0, Lcom/policydm/ui/XUIProfileActivity$10;->this$0:Lcom/policydm/ui/XUIProfileActivity;

    # getter for: Lcom/policydm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/policydm/db/XDBProfileInfo;
    invoke-static {v6}, Lcom/policydm/ui/XUIProfileActivity;->access$000(Lcom/policydm/ui/XUIProfileActivity;)[Lcom/policydm/db/XDBProfileInfo;

    move-result-object v6

    aget-object v6, v6, v1

    iget-object v7, p0, Lcom/policydm/ui/XUIProfileActivity$10;->this$0:Lcom/policydm/ui/XUIProfileActivity;

    # getter for: Lcom/policydm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/policydm/db/XDBProfileInfo;
    invoke-static {v7}, Lcom/policydm/ui/XUIProfileActivity;->access$000(Lcom/policydm/ui/XUIProfileActivity;)[Lcom/policydm/db/XDBProfileInfo;

    move-result-object v7

    aget-object v7, v7, v1

    iget-object v7, v7, Lcom/policydm/db/XDBProfileInfo;->ServerIP:Ljava/lang/String;

    iput-object v7, v6, Lcom/policydm/db/XDBProfileInfo;->ServerIP_Org:Ljava/lang/String;

    .line 260
    iget-object v6, p0, Lcom/policydm/ui/XUIProfileActivity$10;->this$0:Lcom/policydm/ui/XUIProfileActivity;

    # getter for: Lcom/policydm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/policydm/db/XDBProfileInfo;
    invoke-static {v6}, Lcom/policydm/ui/XUIProfileActivity;->access$000(Lcom/policydm/ui/XUIProfileActivity;)[Lcom/policydm/db/XDBProfileInfo;

    move-result-object v6

    aget-object v6, v6, v1

    iget-object v7, p0, Lcom/policydm/ui/XUIProfileActivity$10;->this$0:Lcom/policydm/ui/XUIProfileActivity;

    # getter for: Lcom/policydm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/policydm/db/XDBProfileInfo;
    invoke-static {v7}, Lcom/policydm/ui/XUIProfileActivity;->access$000(Lcom/policydm/ui/XUIProfileActivity;)[Lcom/policydm/db/XDBProfileInfo;

    move-result-object v7

    aget-object v7, v7, v1

    iget v7, v7, Lcom/policydm/db/XDBProfileInfo;->ServerPort:I

    iput v7, v6, Lcom/policydm/db/XDBProfileInfo;->ServerPort_Org:I

    .line 261
    iget-object v6, p0, Lcom/policydm/ui/XUIProfileActivity$10;->this$0:Lcom/policydm/ui/XUIProfileActivity;

    # getter for: Lcom/policydm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/policydm/db/XDBProfileInfo;
    invoke-static {v6}, Lcom/policydm/ui/XUIProfileActivity;->access$000(Lcom/policydm/ui/XUIProfileActivity;)[Lcom/policydm/db/XDBProfileInfo;

    move-result-object v6

    aget-object v6, v6, v1

    iget-object v7, p0, Lcom/policydm/ui/XUIProfileActivity$10;->this$0:Lcom/policydm/ui/XUIProfileActivity;

    # getter for: Lcom/policydm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/policydm/db/XDBProfileInfo;
    invoke-static {v7}, Lcom/policydm/ui/XUIProfileActivity;->access$000(Lcom/policydm/ui/XUIProfileActivity;)[Lcom/policydm/db/XDBProfileInfo;

    move-result-object v7

    aget-object v7, v7, v1

    iget-object v7, v7, Lcom/policydm/db/XDBProfileInfo;->Protocol:Ljava/lang/String;

    iput-object v7, v6, Lcom/policydm/db/XDBProfileInfo;->Protocol_Org:Ljava/lang/String;

    .line 262
    const/4 v6, 0x1

    # setter for: Lcom/policydm/ui/XUIProfileActivity;->m_bRowState:Z
    invoke-static {v6}, Lcom/policydm/ui/XUIProfileActivity;->access$302(Z)Z

    .line 263
    # setter for: Lcom/policydm/ui/XUIProfileActivity;->m_nRow:I
    invoke-static {v1}, Lcom/policydm/ui/XUIProfileActivity;->access$402(I)I

    .line 265
    iget-object v6, p0, Lcom/policydm/ui/XUIProfileActivity$10;->this$0:Lcom/policydm/ui/XUIProfileActivity;

    # getter for: Lcom/policydm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/policydm/db/XDBProfileInfo;
    invoke-static {v6}, Lcom/policydm/ui/XUIProfileActivity;->access$000(Lcom/policydm/ui/XUIProfileActivity;)[Lcom/policydm/db/XDBProfileInfo;

    move-result-object v6

    aget-object v6, v6, v1

    invoke-static {v6}, Lcom/policydm/db/XDBProfileAdp;->xdbSetProfileInfo(Lcom/policydm/db/XDBProfileInfo;)Z

    .line 266
    sget-object v6, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v6, v6, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    iget-object v6, v6, Lcom/policydm/db/XDBProfileListInfo;->ProfileName:[Ljava/lang/String;

    iget-object v7, p0, Lcom/policydm/ui/XUIProfileActivity$10;->this$0:Lcom/policydm/ui/XUIProfileActivity;

    # getter for: Lcom/policydm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/policydm/db/XDBProfileInfo;
    invoke-static {v7}, Lcom/policydm/ui/XUIProfileActivity;->access$000(Lcom/policydm/ui/XUIProfileActivity;)[Lcom/policydm/db/XDBProfileInfo;

    move-result-object v7

    aget-object v7, v7, v1

    iget-object v7, v7, Lcom/policydm/db/XDBProfileInfo;->ProfileName:Ljava/lang/String;

    aput-object v7, v6, v1

    .line 268
    const/4 v0, 0x0

    .line 269
    const/4 v3, 0x0

    .line 241
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 274
    .end local v0    # "URL":Ljava/lang/String;
    .end local v2    # "nPort":I
    .end local v3    # "pAddress":Ljava/lang/String;
    .end local v4    # "pProtocol":Ljava/lang/String;
    .end local v5    # "parser":Lcom/policydm/db/XDBUrlInfo;
    :cond_0
    const/4 v6, 0x0

    # setter for: Lcom/policydm/ui/XUIProfileActivity;->m_bRowState:Z
    invoke-static {v6}, Lcom/policydm/ui/XUIProfileActivity;->access$302(Z)Z

    .line 275
    sget-object v6, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v6, v6, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    # getter for: Lcom/policydm/ui/XUIProfileActivity;->m_nSelectedIndex:I
    invoke-static {}, Lcom/policydm/ui/XUIProfileActivity;->access$100()I

    move-result v7

    iput v7, v6, Lcom/policydm/db/XDBProfileListInfo;->Profileindex:I

    .line 276
    sget-object v6, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v6, v6, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    invoke-static {v6}, Lcom/policydm/db/XDBProfileListAdp;->xdbSetProflieList(Ljava/lang/Object;)V

    .line 277
    # getter for: Lcom/policydm/ui/XUIProfileActivity;->m_nSelectedIndex:I
    invoke-static {}, Lcom/policydm/ui/XUIProfileActivity;->access$100()I

    move-result v6

    invoke-static {v6}, Lcom/policydm/db/XDBProfileListAdp;->xdbSetProfileIndex(I)V

    .line 278
    iget-object v6, p0, Lcom/policydm/ui/XUIProfileActivity$10;->this$0:Lcom/policydm/ui/XUIProfileActivity;

    # invokes: Lcom/policydm/ui/XUIProfileActivity;->xuiCallUiDmNetProfile()V
    invoke-static {v6}, Lcom/policydm/ui/XUIProfileActivity;->access$200(Lcom/policydm/ui/XUIProfileActivity;)V

    .line 279
    return-void
.end method
