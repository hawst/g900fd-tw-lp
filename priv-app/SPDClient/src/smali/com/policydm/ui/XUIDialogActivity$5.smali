.class Lcom/policydm/ui/XUIDialogActivity$5;
.super Ljava/lang/Object;
.source "XUIDialogActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/policydm/ui/XUIDialogActivity;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/policydm/ui/XUIDialogActivity;


# direct methods
.method constructor <init>(Lcom/policydm/ui/XUIDialogActivity;)V
    .locals 0

    .prologue
    .line 483
    iput-object p1, p0, Lcom/policydm/ui/XUIDialogActivity$5;->this$0:Lcom/policydm/ui/XUIDialogActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    const/4 v3, 0x0

    .line 486
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMOInitiatedType()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 488
    invoke-static {}, Lcom/policydm/db/XDBNotiAdp;->xdbNotiGetInfo()Lcom/policydm/db/XDBNotiInfo;

    move-result-object v0

    .line 489
    .local v0, "notiInfo":Lcom/policydm/db/XDBNotiInfo;
    const/4 v1, 0x5

    iput v1, v0, Lcom/policydm/db/XDBNotiInfo;->uiMode:I

    .line 490
    const/16 v1, 0x1f

    invoke-static {v1, v0, v3}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 504
    .end local v0    # "notiInfo":Lcom/policydm/db/XDBNotiInfo;
    :goto_0
    const/16 v1, 0x9

    invoke-static {v1}, Lcom/policydm/ui/XUINotificationManager;->xuiSetIndicator(I)V

    .line 505
    iget-object v1, p0, Lcom/policydm/ui/XUIDialogActivity$5;->this$0:Lcom/policydm/ui/XUIDialogActivity;

    const/16 v2, 0x7c

    invoke-virtual {v1, v2}, Lcom/policydm/ui/XUIDialogActivity;->removeDialog(I)V

    .line 506
    iget-object v1, p0, Lcom/policydm/ui/XUIDialogActivity$5;->this$0:Lcom/policydm/ui/XUIDialogActivity;

    invoke-virtual {v1}, Lcom/policydm/ui/XUIDialogActivity;->finish()V

    .line 507
    return-void

    .line 492
    :cond_0
    invoke-static {}, Lcom/policydm/ui/XUIAdapter;->xuiAdpStartSession()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 494
    const/16 v1, 0x65

    invoke-static {v3, v1}, Lcom/policydm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    .line 495
    const-wide/16 v1, 0x0

    invoke-static {v1, v2}, Lcom/policydm/db/XDBProfileListAdp;->xdbSetNotiJobId(J)V

    .line 496
    const/16 v1, 0x67

    invoke-static {v1}, Lcom/policydm/XDMApplication;->xdmSendMessageDmHandler(I)V

    .line 497
    invoke-static {}, Lcom/policydm/noti/XNOTIAdapter;->xnotiPushAdpDeleteAllNotiQueue()V

    goto :goto_0

    .line 501
    :cond_1
    const-string v1, "Unable to connect network"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method
