.class public Lcom/policydm/ui/XUIProfileActivity;
.super Landroid/app/TabActivity;
.source "XUIProfileActivity.java"

# interfaces
.implements Lcom/policydm/interfaces/XDMInterface;


# static fields
.field private static final DIALOG_NETWORK_PROFILE_EDIT_YES_NO:I = 0x1

.field private static m_bRowState:Z

.field private static m_nRow:I

.field private static m_nSelectedIndex:I


# instance fields
.field private m_LocalSyncDMInfo_t:[Lcom/policydm/db/XDBProfileInfo;

.field private m_TabHost:Landroid/widget/TabHost;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 43
    sput v0, Lcom/policydm/ui/XUIProfileActivity;->m_nSelectedIndex:I

    .line 45
    sput-boolean v0, Lcom/policydm/ui/XUIProfileActivity;->m_bRowState:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/app/TabActivity;-><init>()V

    return-void
.end method

.method private DrawTab()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const v7, 0x7f0a0015

    const v6, 0x108000c

    .line 356
    invoke-virtual {p0}, Lcom/policydm/ui/XUIProfileActivity;->getTabHost()Landroid/widget/TabHost;

    move-result-object v3

    iput-object v3, p0, Lcom/policydm/ui/XUIProfileActivity;->m_TabHost:Landroid/widget/TabHost;

    .line 357
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f030002

    iget-object v5, p0, Lcom/policydm/ui/XUIProfileActivity;->m_TabHost:Landroid/widget/TabHost;

    invoke-virtual {v5}, Landroid/widget/TabHost;->getTabContentView()Landroid/widget/FrameLayout;

    move-result-object v5

    invoke-virtual {v3, v4, v5, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 360
    iget-object v3, p0, Lcom/policydm/ui/XUIProfileActivity;->m_TabHost:Landroid/widget/TabHost;

    const-string v4, "tab1"

    invoke-virtual {v3, v4}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v0

    .line 361
    .local v0, "ts1":Landroid/widget/TabHost$TabSpec;
    iget-object v3, p0, Lcom/policydm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/policydm/db/XDBProfileInfo;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    iget-object v3, v3, Lcom/policydm/db/XDBProfileInfo;->ProfileName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/policydm/ui/XUIProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)Landroid/widget/TabHost$TabSpec;

    .line 362
    invoke-virtual {v0, v7}, Landroid/widget/TabHost$TabSpec;->setContent(I)Landroid/widget/TabHost$TabSpec;

    .line 363
    iget-object v3, p0, Lcom/policydm/ui/XUIProfileActivity;->m_TabHost:Landroid/widget/TabHost;

    invoke-virtual {v3, v0}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    .line 365
    iget-object v3, p0, Lcom/policydm/ui/XUIProfileActivity;->m_TabHost:Landroid/widget/TabHost;

    const-string v4, "tab2"

    invoke-virtual {v3, v4}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v1

    .line 366
    .local v1, "ts2":Landroid/widget/TabHost$TabSpec;
    iget-object v3, p0, Lcom/policydm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/policydm/db/XDBProfileInfo;

    aget-object v3, v3, v8

    iget-object v3, v3, Lcom/policydm/db/XDBProfileInfo;->ProfileName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/policydm/ui/XUIProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)Landroid/widget/TabHost$TabSpec;

    .line 367
    invoke-virtual {v1, v7}, Landroid/widget/TabHost$TabSpec;->setContent(I)Landroid/widget/TabHost$TabSpec;

    .line 368
    iget-object v3, p0, Lcom/policydm/ui/XUIProfileActivity;->m_TabHost:Landroid/widget/TabHost;

    invoke-virtual {v3, v1}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    .line 370
    iget-object v3, p0, Lcom/policydm/ui/XUIProfileActivity;->m_TabHost:Landroid/widget/TabHost;

    const-string v4, "tab3"

    invoke-virtual {v3, v4}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v2

    .line 371
    .local v2, "ts3":Landroid/widget/TabHost$TabSpec;
    iget-object v3, p0, Lcom/policydm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/policydm/db/XDBProfileInfo;

    const/4 v4, 0x2

    aget-object v3, v3, v4

    iget-object v3, v3, Lcom/policydm/db/XDBProfileInfo;->ProfileName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/policydm/ui/XUIProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)Landroid/widget/TabHost$TabSpec;

    .line 372
    invoke-virtual {v2, v7}, Landroid/widget/TabHost$TabSpec;->setContent(I)Landroid/widget/TabHost$TabSpec;

    .line 373
    iget-object v3, p0, Lcom/policydm/ui/XUIProfileActivity;->m_TabHost:Landroid/widget/TabHost;

    invoke-virtual {v3, v2}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    .line 375
    iget-object v3, p0, Lcom/policydm/ui/XUIProfileActivity;->m_TabHost:Landroid/widget/TabHost;

    new-instance v4, Lcom/policydm/ui/XUIProfileActivity$12;

    invoke-direct {v4, p0}, Lcom/policydm/ui/XUIProfileActivity$12;-><init>(Lcom/policydm/ui/XUIProfileActivity;)V

    invoke-virtual {v3, v4}, Landroid/widget/TabHost;->setOnTabChangedListener(Landroid/widget/TabHost$OnTabChangeListener;)V

    .line 396
    sget v3, Lcom/policydm/ui/XUIProfileActivity;->m_nSelectedIndex:I

    invoke-direct {p0, v3}, Lcom/policydm/ui/XUIProfileActivity;->xuiCurrentTabSet(I)V

    .line 397
    iget-object v3, p0, Lcom/policydm/ui/XUIProfileActivity;->m_TabHost:Landroid/widget/TabHost;

    sget v4, Lcom/policydm/ui/XUIProfileActivity;->m_nSelectedIndex:I

    invoke-virtual {v3, v4}, Landroid/widget/TabHost;->setCurrentTab(I)V

    .line 398
    return-void
.end method

.method static synthetic access$000(Lcom/policydm/ui/XUIProfileActivity;)[Lcom/policydm/db/XDBProfileInfo;
    .locals 1
    .param p0, "x0"    # Lcom/policydm/ui/XUIProfileActivity;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/policydm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/policydm/db/XDBProfileInfo;

    return-object v0
.end method

.method static synthetic access$100()I
    .locals 1

    .prologue
    .line 39
    sget v0, Lcom/policydm/ui/XUIProfileActivity;->m_nSelectedIndex:I

    return v0
.end method

.method static synthetic access$102(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 39
    sput p0, Lcom/policydm/ui/XUIProfileActivity;->m_nSelectedIndex:I

    return p0
.end method

.method static synthetic access$200(Lcom/policydm/ui/XUIProfileActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/policydm/ui/XUIProfileActivity;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/policydm/ui/XUIProfileActivity;->xuiCallUiDmNetProfile()V

    return-void
.end method

.method static synthetic access$302(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 39
    sput-boolean p0, Lcom/policydm/ui/XUIProfileActivity;->m_bRowState:Z

    return p0
.end method

.method static synthetic access$402(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 39
    sput p0, Lcom/policydm/ui/XUIProfileActivity;->m_nRow:I

    return p0
.end method

.method static synthetic access$500(Lcom/policydm/ui/XUIProfileActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/policydm/ui/XUIProfileActivity;
    .param p1, "x1"    # I

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/policydm/ui/XUIProfileActivity;->xuiCurrentTabSet(I)V

    return-void
.end method

.method private xuiCallUiDmNetProfile()V
    .locals 3

    .prologue
    .line 218
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/policydm/ui/XUINetProfileActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 219
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "profileIndex"

    sget v2, Lcom/policydm/ui/XUIProfileActivity;->m_nSelectedIndex:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 220
    invoke-virtual {p0, v0}, Lcom/policydm/ui/XUIProfileActivity;->startActivity(Landroid/content/Intent;)V

    .line 221
    invoke-virtual {p0}, Lcom/policydm/ui/XUIProfileActivity;->finish()V

    .line 222
    return-void
.end method

.method private xuiCurrentTabSet(I)V
    .locals 11
    .param p1, "iSelected"    # I

    .prologue
    const v10, 0x7f060001

    const v9, 0x1090009

    const v8, 0x1090008

    .line 67
    const v7, 0x7f0a0017

    invoke-virtual {p0, v7}, Lcom/policydm/ui/XUIProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/EditText;

    .line 68
    .local v6, "text":Landroid/widget/EditText;
    iget-object v7, p0, Lcom/policydm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/policydm/db/XDBProfileInfo;

    aget-object v7, v7, p1

    iget-object v7, v7, Lcom/policydm/db/XDBProfileInfo;->ProfileName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 69
    new-instance v7, Lcom/policydm/ui/XUIProfileActivity$1;

    invoke-direct {v7, p0}, Lcom/policydm/ui/XUIProfileActivity$1;-><init>(Lcom/policydm/ui/XUIProfileActivity;)V

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 85
    const v7, 0x7f0a0019

    invoke-virtual {p0, v7}, Lcom/policydm/ui/XUIProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .end local v6    # "text":Landroid/widget/EditText;
    check-cast v6, Landroid/widget/EditText;

    .line 86
    .restart local v6    # "text":Landroid/widget/EditText;
    iget-object v7, p0, Lcom/policydm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/policydm/db/XDBProfileInfo;

    aget-object v7, v7, p1

    iget-object v7, v7, Lcom/policydm/db/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 87
    new-instance v7, Lcom/policydm/ui/XUIProfileActivity$2;

    invoke-direct {v7, p0}, Lcom/policydm/ui/XUIProfileActivity$2;-><init>(Lcom/policydm/ui/XUIProfileActivity;)V

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 103
    const v7, 0x7f0a001b

    invoke-virtual {p0, v7}, Lcom/policydm/ui/XUIProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .end local v6    # "text":Landroid/widget/EditText;
    check-cast v6, Landroid/widget/EditText;

    .line 104
    .restart local v6    # "text":Landroid/widget/EditText;
    iget-object v7, p0, Lcom/policydm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/policydm/db/XDBProfileInfo;

    aget-object v7, v7, p1

    iget-object v7, v7, Lcom/policydm/db/XDBProfileInfo;->ServerID:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 105
    new-instance v7, Lcom/policydm/ui/XUIProfileActivity$3;

    invoke-direct {v7, p0}, Lcom/policydm/ui/XUIProfileActivity$3;-><init>(Lcom/policydm/ui/XUIProfileActivity;)V

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 121
    const v7, 0x7f0a001d

    invoke-virtual {p0, v7}, Lcom/policydm/ui/XUIProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .end local v6    # "text":Landroid/widget/EditText;
    check-cast v6, Landroid/widget/EditText;

    .line 122
    .restart local v6    # "text":Landroid/widget/EditText;
    iget-object v7, p0, Lcom/policydm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/policydm/db/XDBProfileInfo;

    aget-object v7, v7, p1

    iget-object v7, v7, Lcom/policydm/db/XDBProfileInfo;->ServerPwd:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 123
    new-instance v7, Lcom/policydm/ui/XUIProfileActivity$4;

    invoke-direct {v7, p0}, Lcom/policydm/ui/XUIProfileActivity$4;-><init>(Lcom/policydm/ui/XUIProfileActivity;)V

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 139
    const v7, 0x7f0a001f

    invoke-virtual {p0, v7}, Lcom/policydm/ui/XUIProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .end local v6    # "text":Landroid/widget/EditText;
    check-cast v6, Landroid/widget/EditText;

    .line 140
    .restart local v6    # "text":Landroid/widget/EditText;
    iget-object v7, p0, Lcom/policydm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/policydm/db/XDBProfileInfo;

    aget-object v7, v7, p1

    iget-object v7, v7, Lcom/policydm/db/XDBProfileInfo;->UserName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 141
    new-instance v7, Lcom/policydm/ui/XUIProfileActivity$5;

    invoke-direct {v7, p0}, Lcom/policydm/ui/XUIProfileActivity$5;-><init>(Lcom/policydm/ui/XUIProfileActivity;)V

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 157
    const v7, 0x7f0a0021

    invoke-virtual {p0, v7}, Lcom/policydm/ui/XUIProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .end local v6    # "text":Landroid/widget/EditText;
    check-cast v6, Landroid/widget/EditText;

    .line 158
    .restart local v6    # "text":Landroid/widget/EditText;
    iget-object v7, p0, Lcom/policydm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/policydm/db/XDBProfileInfo;

    aget-object v7, v7, p1

    iget-object v7, v7, Lcom/policydm/db/XDBProfileInfo;->Password:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 159
    new-instance v7, Lcom/policydm/ui/XUIProfileActivity$6;

    invoke-direct {v7, p0}, Lcom/policydm/ui/XUIProfileActivity$6;-><init>(Lcom/policydm/ui/XUIProfileActivity;)V

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 175
    const v7, 0x7f0a0023

    invoke-virtual {p0, v7}, Lcom/policydm/ui/XUIProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Spinner;

    .line 176
    .local v4, "spinClientAuth":Landroid/widget/Spinner;
    const/4 v1, 0x0

    .line 177
    .local v1, "nClientAuthType":I
    iget-object v7, p0, Lcom/policydm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/policydm/db/XDBProfileInfo;

    aget-object v7, v7, p1

    iget v7, v7, Lcom/policydm/db/XDBProfileInfo;->AuthType:I

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 179
    invoke-static {p0, v10, v8}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v0

    .line 180
    .local v0, "clientadapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    invoke-virtual {v0, v9}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 181
    invoke-virtual {v4, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 182
    add-int/lit8 v7, v1, 0x1

    invoke-virtual {v4, v7}, Landroid/widget/Spinner;->setSelection(I)V

    .line 183
    new-instance v7, Lcom/policydm/ui/XUIProfileActivity$7;

    invoke-direct {v7, p0}, Lcom/policydm/ui/XUIProfileActivity$7;-><init>(Lcom/policydm/ui/XUIProfileActivity;)V

    invoke-virtual {v4, v7}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 195
    const v7, 0x7f0a0025

    invoke-virtual {p0, v7}, Lcom/policydm/ui/XUIProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Spinner;

    .line 196
    .local v5, "spinServerAuth":Landroid/widget/Spinner;
    const/4 v2, 0x0

    .line 197
    .local v2, "nServerAuthType":I
    iget-object v7, p0, Lcom/policydm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/policydm/db/XDBProfileInfo;

    aget-object v7, v7, p1

    iget v7, v7, Lcom/policydm/db/XDBProfileInfo;->nServerAuthType:I

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 199
    invoke-static {p0, v10, v8}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v3

    .line 200
    .local v3, "serveradapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    invoke-virtual {v3, v9}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 201
    invoke-virtual {v5, v3}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 202
    add-int/lit8 v7, v2, 0x1

    invoke-virtual {v5, v7}, Landroid/widget/Spinner;->setSelection(I)V

    .line 203
    new-instance v7, Lcom/policydm/ui/XUIProfileActivity$8;

    invoke-direct {v7, p0}, Lcom/policydm/ui/XUIProfileActivity$8;-><init>(Lcom/policydm/ui/XUIProfileActivity;)V

    invoke-virtual {v5, v7}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 214
    return-void
.end method

.method public static xuiGetRow()I
    .locals 1

    .prologue
    .line 62
    sget v0, Lcom/policydm/ui/XUIProfileActivity;->m_nRow:I

    return v0
.end method

.method public static xuiGetRowState()Z
    .locals 1

    .prologue
    .line 51
    sget-boolean v0, Lcom/policydm/ui/XUIProfileActivity;->m_bRowState:Z

    return v0
.end method

.method public static xuiSetRow(I)I
    .locals 1
    .param p0, "Row"    # I

    .prologue
    .line 56
    sput p0, Lcom/policydm/ui/XUIProfileActivity;->m_nRow:I

    .line 57
    sget v0, Lcom/policydm/ui/XUIProfileActivity;->m_nRow:I

    return v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 340
    invoke-super {p0, p1}, Landroid/app/TabActivity;->onCreate(Landroid/os/Bundle;)V

    .line 341
    const-string v0, ""

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 342
    invoke-virtual {p0}, Lcom/policydm/ui/XUIProfileActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "profileIndex"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/policydm/ui/XUIProfileActivity;->m_nSelectedIndex:I

    .line 344
    iget-object v0, p0, Lcom/policydm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/policydm/db/XDBProfileInfo;

    if-nez v0, :cond_0

    .line 346
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/policydm/db/XDBProfileInfo;

    iput-object v0, p0, Lcom/policydm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/policydm/db/XDBProfileInfo;

    .line 347
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/policydm/db/XDB;->xdbReadListInfo(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/policydm/db/XDBProfileInfo;

    check-cast v0, [Lcom/policydm/db/XDBProfileInfo;

    iput-object v0, p0, Lcom/policydm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/policydm/db/XDBProfileInfo;

    .line 349
    iget-object v0, p0, Lcom/policydm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/policydm/db/XDBProfileInfo;

    if-eqz v0, :cond_0

    .line 350
    invoke-direct {p0}, Lcom/policydm/ui/XUIProfileActivity;->DrawTab()V

    .line 352
    :cond_0
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 227
    packed-switch p1, :pswitch_data_0

    .line 293
    invoke-super {p0, p1}, Landroid/app/TabActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 230
    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x1080027

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1010355

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "NETWORK PROFILE EDIT"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "Do you want Save and Close Profile info ?"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/policydm/ui/XUIProfileActivity$11;

    invoke-direct {v1, p0}, Lcom/policydm/ui/XUIProfileActivity$11;-><init>(Lcom/policydm/ui/XUIProfileActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f080057

    new-instance v2, Lcom/policydm/ui/XUIProfileActivity$10;

    invoke-direct {v2, p0}, Lcom/policydm/ui/XUIProfileActivity$10;-><init>(Lcom/policydm/ui/XUIProfileActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f080006

    new-instance v2, Lcom/policydm/ui/XUIProfileActivity$9;

    invoke-direct {v2, p0}, Lcom/policydm/ui/XUIProfileActivity$9;-><init>(Lcom/policydm/ui/XUIProfileActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 227
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v2, 0x0

    .line 325
    const-string v0, "SAVE"

    invoke-interface {p1, v2, v2, v2, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x108004e

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 326
    const/4 v0, 0x1

    const-string v1, "revert"

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x108004c

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 327
    const/4 v0, 0x2

    const-string v1, "Edit Network Info"

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x108003e

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 328
    invoke-super {p0, p1}, Landroid/app/TabActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 460
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 469
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/app/TabActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0

    .line 463
    :pswitch_0
    invoke-virtual {p0}, Lcom/policydm/ui/XUIProfileActivity;->xuiProfileSave()V

    .line 464
    invoke-virtual {p0}, Lcom/policydm/ui/XUIProfileActivity;->finish()V

    goto :goto_0

    .line 460
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 299
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 319
    :goto_0
    invoke-super {p0, p1}, Landroid/app/TabActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 302
    :pswitch_0
    invoke-virtual {p0}, Lcom/policydm/ui/XUIProfileActivity;->xuiProfileSave()V

    .line 303
    invoke-virtual {p0}, Lcom/policydm/ui/XUIProfileActivity;->finish()V

    goto :goto_0

    .line 307
    :pswitch_1
    invoke-virtual {p0}, Lcom/policydm/ui/XUIProfileActivity;->finish()V

    goto :goto_0

    .line 312
    :pswitch_2
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/policydm/ui/XUIProfileActivity;->showDialog(I)V

    goto :goto_0

    .line 299
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onStart()V
    .locals 0

    .prologue
    .line 334
    invoke-super {p0}, Landroid/app/TabActivity;->onStart()V

    .line 335
    return-void
.end method

.method protected xuiProfileSave()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 402
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v5, 0x3

    if-ge v0, v5, :cond_1

    .line 404
    const-string v2, ""

    .line 405
    .local v2, "pAddress":Ljava/lang/String;
    const/4 v1, 0x0

    .line 406
    .local v1, "nPort":I
    const-string v3, ""

    .line 407
    .local v3, "pProtocol":Ljava/lang/String;
    const/4 v4, 0x0

    .line 409
    .local v4, "parser":Lcom/policydm/db/XDBUrlInfo;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Tab : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 411
    iget-object v5, p0, Lcom/policydm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/policydm/db/XDBProfileInfo;

    aget-object v5, v5, v0

    iget-object v5, v5, Lcom/policydm/db/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    invoke-static {v5}, Lcom/policydm/tp/XTPHttpUtil;->xtpURLParser(Ljava/lang/String;)Lcom/policydm/db/XDBUrlInfo;

    move-result-object v4

    .line 412
    iget-object v2, v4, Lcom/policydm/db/XDBUrlInfo;->pAddress:Ljava/lang/String;

    .line 413
    iget v1, v4, Lcom/policydm/db/XDBUrlInfo;->nPort:I

    .line 414
    iget-object v3, v4, Lcom/policydm/db/XDBUrlInfo;->pProtocol:Ljava/lang/String;

    .line 415
    iget-object v5, p0, Lcom/policydm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/policydm/db/XDBProfileInfo;

    aget-object v5, v5, v0

    iget-object v6, v4, Lcom/policydm/db/XDBUrlInfo;->pURL:Ljava/lang/String;

    iput-object v6, v5, Lcom/policydm/db/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    .line 416
    iget-object v5, p0, Lcom/policydm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/policydm/db/XDBProfileInfo;

    aget-object v5, v5, v0

    iput-object v2, v5, Lcom/policydm/db/XDBProfileInfo;->ServerIP:Ljava/lang/String;

    .line 417
    iget-object v5, p0, Lcom/policydm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/policydm/db/XDBProfileInfo;

    aget-object v5, v5, v0

    iput v1, v5, Lcom/policydm/db/XDBProfileInfo;->ServerPort:I

    .line 418
    iget-object v5, p0, Lcom/policydm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/policydm/db/XDBProfileInfo;

    aget-object v5, v5, v0

    iput-object v3, v5, Lcom/policydm/db/XDBProfileInfo;->Protocol:Ljava/lang/String;

    .line 420
    iget-object v5, p0, Lcom/policydm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/policydm/db/XDBProfileInfo;

    aget-object v5, v5, v0

    iget-object v6, p0, Lcom/policydm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/policydm/db/XDBProfileInfo;

    aget-object v6, v6, v0

    iget-object v6, v6, Lcom/policydm/db/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    iput-object v6, v5, Lcom/policydm/db/XDBProfileInfo;->ServerUrl_Org:Ljava/lang/String;

    .line 421
    iget-object v5, p0, Lcom/policydm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/policydm/db/XDBProfileInfo;

    aget-object v5, v5, v0

    iget-object v6, p0, Lcom/policydm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/policydm/db/XDBProfileInfo;

    aget-object v6, v6, v0

    iget-object v6, v6, Lcom/policydm/db/XDBProfileInfo;->ServerIP:Ljava/lang/String;

    iput-object v6, v5, Lcom/policydm/db/XDBProfileInfo;->ServerIP_Org:Ljava/lang/String;

    .line 422
    iget-object v5, p0, Lcom/policydm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/policydm/db/XDBProfileInfo;

    aget-object v5, v5, v0

    iget-object v6, p0, Lcom/policydm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/policydm/db/XDBProfileInfo;

    aget-object v6, v6, v0

    iget v6, v6, Lcom/policydm/db/XDBProfileInfo;->ServerPort:I

    iput v6, v5, Lcom/policydm/db/XDBProfileInfo;->ServerPort_Org:I

    .line 423
    iget-object v5, p0, Lcom/policydm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/policydm/db/XDBProfileInfo;

    aget-object v5, v5, v0

    iget-object v6, p0, Lcom/policydm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/policydm/db/XDBProfileInfo;

    aget-object v6, v6, v0

    iget-object v6, v6, Lcom/policydm/db/XDBProfileInfo;->Protocol:Ljava/lang/String;

    iput-object v6, v5, Lcom/policydm/db/XDBProfileInfo;->Protocol_Org:Ljava/lang/String;

    .line 425
    const/4 v5, 0x1

    sput-boolean v5, Lcom/policydm/ui/XUIProfileActivity;->m_bRowState:Z

    .line 426
    sput v0, Lcom/policydm/ui/XUIProfileActivity;->m_nRow:I

    .line 428
    iget-object v5, p0, Lcom/policydm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/policydm/db/XDBProfileInfo;

    aget-object v5, v5, v0

    invoke-static {v5}, Lcom/policydm/db/XDBProfileAdp;->xdbSetProfileInfo(Lcom/policydm/db/XDBProfileInfo;)Z

    .line 430
    sget-object v5, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    if-eqz v5, :cond_0

    sget-object v5, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v5, v5, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    if-eqz v5, :cond_0

    .line 431
    sget-object v5, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v5, v5, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    iget-object v5, v5, Lcom/policydm/db/XDBProfileListInfo;->ProfileName:[Ljava/lang/String;

    iget-object v6, p0, Lcom/policydm/ui/XUIProfileActivity;->m_LocalSyncDMInfo_t:[Lcom/policydm/db/XDBProfileInfo;

    aget-object v6, v6, v0

    iget-object v6, v6, Lcom/policydm/db/XDBProfileInfo;->ProfileName:Ljava/lang/String;

    aput-object v6, v5, v0

    .line 433
    :cond_0
    const/4 v2, 0x0

    .line 402
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 437
    .end local v1    # "nPort":I
    .end local v2    # "pAddress":Ljava/lang/String;
    .end local v3    # "pProtocol":Ljava/lang/String;
    .end local v4    # "parser":Lcom/policydm/db/XDBUrlInfo;
    :cond_1
    sput-boolean v7, Lcom/policydm/ui/XUIProfileActivity;->m_bRowState:Z

    .line 438
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "iTapIndex="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget v6, Lcom/policydm/ui/XUIProfileActivity;->m_nSelectedIndex:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 440
    sget-object v5, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    if-eqz v5, :cond_2

    sget-object v5, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v5, v5, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    if-eqz v5, :cond_2

    sget-object v5, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v5, v5, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    if-nez v5, :cond_3

    .line 455
    :cond_2
    :goto_1
    return-void

    .line 444
    :cond_3
    const-string v5, "Save to database..."

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 445
    sget-object v5, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v5, v5, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    sget v6, Lcom/policydm/ui/XUIProfileActivity;->m_nSelectedIndex:I

    iput v6, v5, Lcom/policydm/db/XDBProfileListInfo;->Profileindex:I

    .line 447
    sget-object v5, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    invoke-static {}, Lcom/policydm/db/XDBProfileAdp;->xdbGetProfileInfo()Lcom/policydm/db/XDBProfileInfo;

    move-result-object v6

    iput-object v6, v5, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    .line 448
    sget-object v5, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v5, v5, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    if-eqz v5, :cond_4

    .line 449
    sget-object v5, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v5, v5, Lcom/policydm/db/XDBNvm;->NVMDMInfo:Lcom/policydm/db/XDBProfileInfo;

    invoke-static {}, Lcom/policydm/db/XDBProfileAdp;->xdbGetConRef()Lcom/policydm/db/XDBInfoConRef;

    move-result-object v6

    iput-object v6, v5, Lcom/policydm/db/XDBProfileInfo;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    .line 451
    :cond_4
    sget-object v5, Lcom/policydm/db/XDB;->XDMNvmClass:Lcom/policydm/db/XDBNvm;

    iget-object v5, v5, Lcom/policydm/db/XDBNvm;->tProfileList:Lcom/policydm/db/XDBProfileListInfo;

    invoke-static {v5}, Lcom/policydm/db/XDBProfileListAdp;->xdbSetProflieList(Ljava/lang/Object;)V

    .line 452
    sget v5, Lcom/policydm/ui/XUIProfileActivity;->m_nSelectedIndex:I

    invoke-static {v5}, Lcom/policydm/db/XDBProfileListAdp;->xdbSetProfileIndex(I)V

    .line 453
    const-string v5, "Saved"

    invoke-static {p0, v5, v7}, Lcom/policydm/XDMApplication;->xdmShowToast(Landroid/content/Context;Ljava/lang/String;I)V

    goto :goto_1
.end method
