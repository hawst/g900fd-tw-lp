.class public Lcom/policydm/spd/XSPDAgent;
.super Ljava/lang/Object;
.source "XSPDAgent.java"

# interfaces
.implements Lcom/policydm/interfaces/XSPDInterface;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xspdApplySPotaPolicy()I
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 19
    const/4 v2, 0x1

    .line 20
    .local v2, "result":I
    const/4 v0, 0x0

    .line 22
    .local v0, "bRet":Z
    new-instance v3, Lcom/policydm/adapter/XSPDAdapter;

    invoke-direct {v3}, Lcom/policydm/adapter/XSPDAdapter;-><init>()V

    .line 24
    .local v3, "spdadp":Lcom/policydm/adapter/XSPDAdapter;
    invoke-virtual {v3}, Lcom/policydm/adapter/XSPDAdapter;->xspdUnzipSEAndroidPolicy()Z

    move-result v0

    .line 25
    if-nez v0, :cond_0

    .line 27
    const-string v4, "Fail to unzip Policy"

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 28
    const/4 v2, -0x1

    .line 58
    .end local v2    # "result":I
    :goto_0
    return v2

    .line 31
    .restart local v2    # "result":I
    :cond_0
    invoke-virtual {v3}, Lcom/policydm/adapter/XSPDAdapter;->xspdVersionSEAndroidPolicy()Z

    move-result v0

    .line 32
    if-nez v0, :cond_1

    .line 34
    const-string v4, "Fail to version Policy"

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 35
    const/4 v2, -0x2

    goto :goto_0

    .line 38
    :cond_1
    invoke-virtual {v3}, Lcom/policydm/adapter/XSPDAdapter;->xspdVerifySEAndroidPolicy()Z

    move-result v0

    .line 39
    if-nez v0, :cond_2

    .line 41
    const-string v4, "Fail to verify Policy"

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 42
    const/4 v2, -0x3

    goto :goto_0

    .line 45
    :cond_2
    invoke-virtual {v3}, Lcom/policydm/adapter/XSPDAdapter;->xspdCopySEAndroidPolicy()Z

    move-result v0

    .line 46
    if-nez v0, :cond_3

    .line 48
    const-string v4, "Fail to copy Policy"

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 49
    const/4 v2, -0x4

    goto :goto_0

    .line 52
    :cond_3
    const-string v4, "selinux.reload_policy"

    const-string v5, "1"

    invoke-static {v4, v5}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    new-instance v1, Landroid/content/Intent;

    const-string v4, "com.samsung.spdsuccess"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 54
    .local v1, "intent":Landroid/content/Intent;
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 55
    const-string v4, "Success Apply Policy"

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 56
    invoke-static {v6}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetSyncMode(I)Z

    .line 57
    invoke-static {v6}, Lcom/policydm/XDMBroadcastReceiver;->xdmSetCheckedIntent(Z)V

    goto :goto_0
.end method

.method public static xspdDeviceReset()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 80
    const-string v0, "Device Register Reset!!"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 82
    invoke-static {v1}, Lcom/policydm/db/XDBSpdAdp;->xdbSetSpdDeviceCreate(I)V

    .line 83
    invoke-static {v1}, Lcom/policydm/db/XDBSpdAdp;->xdbSetSpdDeviceUpdate(I)V

    .line 84
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/policydm/db/XDBSpdAdp;->xdbSetSpdDeviceRegister(I)V

    .line 85
    return-void
.end method

.method public static xspdRemoveSPotaPolicy()V
    .locals 3

    .prologue
    .line 63
    const/4 v0, 0x0

    .line 65
    .local v0, "bRet":Z
    new-instance v1, Lcom/policydm/adapter/XSPDAdapter;

    invoke-direct {v1}, Lcom/policydm/adapter/XSPDAdapter;-><init>()V

    .line 67
    .local v1, "spdadp":Lcom/policydm/adapter/XSPDAdapter;
    invoke-virtual {v1}, Lcom/policydm/adapter/XSPDAdapter;->xspdRemoveSEAndroidPolicy()Z

    move-result v0

    .line 68
    if-nez v0, :cond_0

    .line 70
    const-string v2, "Fail to remove Policy"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 75
    :goto_0
    return-void

    .line 74
    :cond_0
    const-string v2, "Success Remove Policy"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0
.end method
