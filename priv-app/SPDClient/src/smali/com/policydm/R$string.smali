.class public final Lcom/policydm/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/policydm/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final WSS_STR_ACCEPT:I = 0x7f080000

.field public static final WSS_STR_ACCESS_NAME:I = 0x7f080001

.field public static final WSS_STR_APN_TYPE:I = 0x7f080002

.field public static final WSS_STR_AUTH_TYPE:I = 0x7f080003

.field public static final WSS_STR_AUTOMATIC_UPDATES:I = 0x7f080004

.field public static final WSS_STR_AUTOMATIC_UPDATE_NOTIFY:I = 0x7f080005

.field public static final WSS_STR_CANCEL:I = 0x7f080006

.field public static final WSS_STR_CLOSE:I = 0x7f080007

.field public static final WSS_STR_CONFIRM:I = 0x7f080008

.field public static final WSS_STR_CONNECTING_SERVER:I = 0x7f080009

.field public static final WSS_STR_CONNECTING_TO_SERVER:I = 0x7f08000a

.field public static final WSS_STR_CONNECT_TO_MOBILE_NETWORK:I = 0x7f08000b

.field public static final WSS_STR_CURRENT_VERSION:I = 0x7f08000c

.field public static final WSS_STR_DECLINE:I = 0x7f08000d

.field public static final WSS_STR_DEVICE_REGISTERED:I = 0x7f08000e

.field public static final WSS_STR_EDIT:I = 0x7f08000f

.field public static final WSS_STR_ENTERPRISE_MODE:I = 0x7f080010

.field public static final WSS_STR_EULA_ACCEPT:I = 0x7f080011

.field public static final WSS_STR_EULA_ACCEPT_2:I = 0x7f080012

.field public static final WSS_STR_EULA_ACCEPT_3:I = 0x7f080013

.field public static final WSS_STR_EULA_ACCEPT_OPEN:I = 0x7f080014

.field public static final WSS_STR_EULA_ACCEPT_OPEN_2:I = 0x7f080015

.field public static final WSS_STR_EULA_ACCEPT_OPEN_3:I = 0x7f080016

.field public static final WSS_STR_EULA_CONFIRM:I = 0x7f080017

.field public static final WSS_STR_EULA_CONFIRM_OPEN:I = 0x7f080018

.field public static final WSS_STR_EULA_CONFIRM_OPEN_ATT:I = 0x7f080019

.field public static final WSS_STR_EULA_CONFIRM_TAB:I = 0x7f08001a

.field public static final WSS_STR_EULA_DECLINE:I = 0x7f08001b

.field public static final WSS_STR_EULA_DECLINE_2:I = 0x7f08001c

.field public static final WSS_STR_EULA_DECLINE_3:I = 0x7f08001d

.field public static final WSS_STR_EULA_DECLINE_OPEN:I = 0x7f08001e

.field public static final WSS_STR_EULA_DECLINE_OPEN_2:I = 0x7f08001f

.field public static final WSS_STR_EULA_DECLINE_OPEN_3:I = 0x7f080020

.field public static final WSS_STR_EULA_DETAILS:I = 0x7f080021

.field public static final WSS_STR_EULA_DETAILS_OPEN:I = 0x7f080022

.field public static final WSS_STR_EULA_DETAILS_OPEN_ATT:I = 0x7f080023

.field public static final WSS_STR_EULA_DETAILS_TAB:I = 0x7f080024

.field public static final WSS_STR_LATEST_VERSION:I = 0x7f080025

.field public static final WSS_STR_LEARN_MORE:I = 0x7f080026

.field public static final WSS_STR_NETWORK_FAIL:I = 0x7f080027

.field public static final WSS_STR_NETWORK_FAIL_BODY:I = 0x7f080028

.field public static final WSS_STR_NETWORK_FAIL_BODY_CHINA:I = 0x7f080029

.field public static final WSS_STR_NETWORK_FAIL_BODY_OPEN:I = 0x7f08002a

.field public static final WSS_STR_NETWORK_FAIL_BODY_OPEN_CHINA:I = 0x7f08002b

.field public static final WSS_STR_NETWORK_FAIL_OPEN:I = 0x7f08002c

.field public static final WSS_STR_NETWORK_PROFILE:I = 0x7f08002d

.field public static final WSS_STR_NEXT:I = 0x7f08002e

.field public static final WSS_STR_NO:I = 0x7f08002f

.field public static final WSS_STR_NO_UPDATE:I = 0x7f080030

.field public static final WSS_STR_OK:I = 0x7f080031

.field public static final WSS_STR_PLZWAIT:I = 0x7f080032

.field public static final WSS_STR_POLICY_UPDATE:I = 0x7f080033

.field public static final WSS_STR_PORT:I = 0x7f080034

.field public static final WSS_STR_PROFILE_NAME:I = 0x7f080035

.field public static final WSS_STR_PROXY:I = 0x7f080036

.field public static final WSS_STR_PW:I = 0x7f080037

.field public static final WSS_STR_SEANDROID_MODE:I = 0x7f080038

.field public static final WSS_STR_SELECT:I = 0x7f080039

.field public static final WSS_STR_SERVER_ADDR:I = 0x7f08003a

.field public static final WSS_STR_SERVER_AUTHTYPE:I = 0x7f08003b

.field public static final WSS_STR_SERVER_ID:I = 0x7f08003c

.field public static final WSS_STR_SERVER_IP:I = 0x7f08003d

.field public static final WSS_STR_SERVER_PW:I = 0x7f08003e

.field public static final WSS_STR_SETTING:I = 0x7f08003f

.field public static final WSS_STR_SETTINGSummary:I = 0x7f080040

.field public static final WSS_STR_SPOTA_VERSION:I = 0x7f080041

.field public static final WSS_STR_START_NETWORK_WARNING:I = 0x7f080042

.field public static final WSS_STR_START_NETWORK_WARNING_CHINA:I = 0x7f080043

.field public static final WSS_STR_SUMMARY:I = 0x7f080044

.field public static final WSS_STR_SYSTEM_VERSION:I = 0x7f080045

.field public static final WSS_STR_TITLE:I = 0x7f080046

.field public static final WSS_STR_UIC_TITLE:I = 0x7f080047

.field public static final WSS_STR_UNABLE_NETWORK:I = 0x7f080048

.field public static final WSS_STR_UPDATE_FAIL:I = 0x7f080049

.field public static final WSS_STR_UPDATE_FAIL_BODY:I = 0x7f08004a

.field public static final WSS_STR_UPDATE_FAIL_BODY_OPEN:I = 0x7f08004b

.field public static final WSS_STR_UPDATE_FAIL_OPEN:I = 0x7f08004c

.field public static final WSS_STR_UPDATE_FAIL_SPACE:I = 0x7f08004d

.field public static final WSS_STR_UPDATE_FAIL_SPACE_BODY:I = 0x7f08004e

.field public static final WSS_STR_UPDATE_FAIL_SPACE_BODY_OPEN:I = 0x7f08004f

.field public static final WSS_STR_UPDATE_FAIL_TITLE:I = 0x7f080050

.field public static final WSS_STR_UPDATE_SUCCESS:I = 0x7f080051

.field public static final WSS_STR_UPDATE_SUCCESS_OPEN:I = 0x7f080052

.field public static final WSS_STR_UPDATE_SUCCESS_TITLE:I = 0x7f080053

.field public static final WSS_STR_UserID:I = 0x7f080054

.field public static final WSS_STR_UserName:I = 0x7f080055

.field public static final WSS_STR_UserPWD:I = 0x7f080056

.field public static final WSS_STR_YES:I = 0x7f080057


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
