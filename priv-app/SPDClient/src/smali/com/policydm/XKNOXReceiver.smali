.class public Lcom/policydm/XKNOXReceiver;
.super Landroid/content/BroadcastReceiver;
.source "XKNOXReceiver.java"

# interfaces
.implements Lcom/policydm/interfaces/XCommonInterface;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x7

    const/4 v5, 0x6

    const/4 v4, 0x5

    const/4 v3, 0x0

    .line 26
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v1

    if-eqz v1, :cond_1

    .line 28
    const-string v1, "User mode!! return"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 122
    :cond_0
    :goto_0
    return-void

    .line 31
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.policydm.intent.action.PULL_RECEIVE"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 33
    const-string v1, "XCOMMON_INTENT_PULL"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 34
    invoke-static {}, Lcom/policydm/XDMBroadcastReceiver;->xdmGetCheckedIntent()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {}, Lcom/policydm/ui/XUIAdapter;->xuiAdpGetUiMode()I

    move-result v1

    if-eqz v1, :cond_3

    .line 36
    :cond_2
    const-string v1, "Already received pull intent...return!!"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 41
    :cond_3
    invoke-static {}, Lcom/policydm/XDMBroadcastReceiver;->callPullStart()V

    .line 42
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/policydm/XDMBroadcastReceiver;->xdmSetCheckedIntent(Z)V

    .line 43
    const/4 v0, 0x0

    .line 44
    .local v0, "mCountDown":Landroid/os/CountDownTimer;
    new-instance v0, Lcom/policydm/XKNOXReceiver$1;

    .end local v0    # "mCountDown":Landroid/os/CountDownTimer;
    const-wide/16 v2, 0x1388

    const-wide/16 v4, 0x3e8

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/policydm/XKNOXReceiver$1;-><init>(Lcom/policydm/XKNOXReceiver;JJ)V

    .line 55
    .restart local v0    # "mCountDown":Landroid/os/CountDownTimer;
    invoke-virtual {v0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    goto :goto_0

    .line 58
    .end local v0    # "mCountDown":Landroid/os/CountDownTimer;
    :cond_4
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.policydm.intent.action.POLICY_ADMIN"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 60
    new-instance v6, Landroid/content/Intent;

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const-class v3, Lcom/policydm/ui/XUISPDActivity;

    invoke-direct {v6, v1, v2, p1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 61
    .local v6, "i":Landroid/content/Intent;
    const/high16 v1, 0x10000000

    invoke-virtual {v6, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 62
    invoke-virtual {p1, v6}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 65
    .end local v6    # "i":Landroid/content/Intent;
    :cond_5
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.policydm.intent.action.POLLING_TIME"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 67
    sget-boolean v1, Lcom/policydm/agent/XDMTask;->g_IsDMInitialized:Z

    if-nez v1, :cond_6

    .line 69
    invoke-static {v4}, Lcom/policydm/XDMBroadcastReceiver;->xdmNotInitSetResume(I)V

    goto :goto_0

    .line 73
    :cond_6
    invoke-static {v4}, Lcom/policydm/XDMBroadcastReceiver;->xdmExecResumeCase(I)V

    goto :goto_0

    .line 76
    :cond_7
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.policydm.intent.action.POLLING_REPORTTIME"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 78
    sget-boolean v1, Lcom/policydm/agent/XDMTask;->g_IsDMInitialized:Z

    if-nez v1, :cond_8

    .line 80
    invoke-static {v5}, Lcom/policydm/XDMBroadcastReceiver;->xdmNotInitSetResume(I)V

    goto/16 :goto_0

    .line 84
    :cond_8
    invoke-static {v5}, Lcom/policydm/XDMBroadcastReceiver;->xdmExecResumeCase(I)V

    goto/16 :goto_0

    .line 87
    :cond_9
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.policydm.intent.action.SPD_REGISTER"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 89
    sget-boolean v1, Lcom/policydm/agent/XDMTask;->g_IsDMInitialized:Z

    if-nez v1, :cond_a

    .line 91
    invoke-static {v7}, Lcom/policydm/XDMBroadcastReceiver;->xdmNotInitSetResume(I)V

    goto/16 :goto_0

    .line 95
    :cond_a
    invoke-static {v7}, Lcom/policydm/XDMBroadcastReceiver;->xdmExecResumeCase(I)V

    goto/16 :goto_0

    .line 98
    :cond_b
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.policydm.intent.action.EULA_TIME"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 100
    sget-boolean v1, Lcom/policydm/agent/XDMTask;->g_IsDMInitialized:Z

    if-nez v1, :cond_c

    .line 102
    invoke-static {v8}, Lcom/policydm/XDMBroadcastReceiver;->xdmNotInitSetResume(I)V

    goto/16 :goto_0

    .line 106
    :cond_c
    invoke-static {v8}, Lcom/policydm/XDMBroadcastReceiver;->xdmExecResumeCase(I)V

    goto/16 :goto_0

    .line 109
    :cond_d
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.policydm.intent.action.DELETE_NOTI"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 111
    invoke-static {v3}, Lcom/policydm/XDMApplication;->xdmSetNotibarState(I)V

    goto/16 :goto_0

    .line 113
    :cond_e
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.policydm.intent.action.EULA_AGREEMENT"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 115
    invoke-static {}, Lcom/policydm/db/XDB;->xdbIsAlreadyAgree()Z

    move-result v1

    if-nez v1, :cond_f

    .line 117
    const/4 v1, 0x2

    invoke-static {v1}, Lcom/policydm/db/XDBSpdAdp;->xdbSetSpdDeviceRegister(I)V

    .line 118
    const/16 v1, 0xcd

    invoke-static {v1}, Lcom/policydm/XDMApplication;->xdmSendMessageDmHandler(I)V

    .line 120
    :cond_f
    invoke-static {}, Lcom/policydm/polling/XPollingAgent;->xpollingDefaultPollingTime()V

    goto/16 :goto_0
.end method
