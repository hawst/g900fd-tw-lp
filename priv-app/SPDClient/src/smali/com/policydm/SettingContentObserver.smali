.class public Lcom/policydm/SettingContentObserver;
.super Landroid/database/ContentObserver;
.source "SettingContentObserver.java"


# instance fields
.field private mSPDRegisterUri:Landroid/net/Uri;

.field private mService:Lcom/policydm/ObserverService;

.field private mUriUpdate:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Lcom/policydm/ObserverService;Landroid/os/Handler;)V
    .locals 1
    .param p1, "service"    # Lcom/policydm/ObserverService;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 18
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 20
    iput-object p1, p0, Lcom/policydm/SettingContentObserver;->mService:Lcom/policydm/ObserverService;

    .line 21
    const-string v0, "security_update_db"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/policydm/SettingContentObserver;->mUriUpdate:Landroid/net/Uri;

    .line 22
    const-string v0, "SPD_REGISTERD"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/policydm/SettingContentObserver;->mSPDRegisterUri:Landroid/net/Uri;

    .line 23
    return-void
.end method


# virtual methods
.method public getSPDRegisterUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/policydm/SettingContentObserver;->mSPDRegisterUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getUpdateUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/policydm/SettingContentObserver;->mUriUpdate:Landroid/net/Uri;

    return-object v0
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .locals 1
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 38
    invoke-super {p0, p1, p2}, Landroid/database/ContentObserver;->onChange(ZLandroid/net/Uri;)V

    .line 39
    if-nez p2, :cond_1

    .line 53
    :cond_0
    :goto_0
    return-void

    .line 42
    :cond_1
    if-nez p1, :cond_0

    .line 44
    iget-object v0, p0, Lcom/policydm/SettingContentObserver;->mUriUpdate:Landroid/net/Uri;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/policydm/SettingContentObserver;->mUriUpdate:Landroid/net/Uri;

    invoke-virtual {p2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 46
    iget-object v0, p0, Lcom/policydm/SettingContentObserver;->mService:Lcom/policydm/ObserverService;

    invoke-virtual {v0}, Lcom/policydm/ObserverService;->autoUpdateChanged()V

    goto :goto_0

    .line 48
    :cond_2
    iget-object v0, p0, Lcom/policydm/SettingContentObserver;->mSPDRegisterUri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/policydm/SettingContentObserver;->mSPDRegisterUri:Landroid/net/Uri;

    invoke-virtual {p2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/policydm/SettingContentObserver;->mService:Lcom/policydm/ObserverService;

    invoke-virtual {v0}, Lcom/policydm/ObserverService;->spdRegisterChanged()V

    goto :goto_0
.end method
