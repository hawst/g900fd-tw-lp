.class public Lcom/policydm/XDMApplication$XDMPhoneStateListener;
.super Landroid/telephony/PhoneStateListener;
.source "XDMApplication.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/policydm/XDMApplication;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "XDMPhoneStateListener"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 510
    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public notifyListeners()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 578
    sget-object v1, Lcom/policydm/XDMApplication;->telephonyManager:Landroid/telephony/TelephonyManager;

    sget-object v2, Lcom/policydm/XDMApplication;->serviceState:Landroid/telephony/ServiceState;

    invoke-static {v1, v2}, Lcom/policydm/adapter/XDMTelephonyData;->xdmGetInstance(Landroid/telephony/TelephonyManager;Landroid/telephony/ServiceState;)Lcom/policydm/adapter/XDMTelephonyData;

    move-result-object v0

    .line 580
    .local v0, "telephony":Lcom/policydm/adapter/XDMTelephonyData;
    if-eqz v0, :cond_1

    .line 582
    iget v1, v0, Lcom/policydm/adapter/XDMTelephonyData;->networkType:I

    invoke-static {v1}, Lcom/policydm/XDMApplication;->xdmSetServiceType(I)I

    .line 583
    iget v1, v0, Lcom/policydm/adapter/XDMTelephonyData;->nSimState:I

    invoke-static {v1}, Lcom/policydm/XDMApplication;->xdmSetSimState(I)V

    .line 585
    iget v1, v0, Lcom/policydm/adapter/XDMTelephonyData;->networkType:I

    if-eqz v1, :cond_0

    .line 587
    sget-boolean v1, Lcom/policydm/agent/XDMTask;->g_IsSyncTaskInit:Z

    if-eqz v1, :cond_0

    sget-boolean v1, Lcom/policydm/agent/XDMTask;->g_IsDMInitialized:Z

    if-nez v1, :cond_0

    sget-boolean v1, Lcom/policydm/XDMApplication;->g_bIsInitializing:Z

    if-nez v1, :cond_0

    .line 589
    sput-boolean v3, Lcom/policydm/XDMApplication;->g_bIsInitializing:Z

    .line 590
    invoke-static {v3, v4, v4}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 595
    :cond_0
    # getter for: Lcom/policydm/XDMApplication;->m_bIsRoaming:Z
    invoke-static {}, Lcom/policydm/XDMApplication;->access$500()Z

    move-result v1

    if-nez v1, :cond_3

    iget-boolean v1, v0, Lcom/policydm/adapter/XDMTelephonyData;->isNetworkRoaming:Z

    if-eqz v1, :cond_3

    .line 597
    # setter for: Lcom/policydm/XDMApplication;->m_bIsRoaming:Z
    invoke-static {v3}, Lcom/policydm/XDMApplication;->access$502(Z)Z

    .line 598
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "going to Roaming Area - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/policydm/XDMApplication;->m_bIsRoaming:Z
    invoke-static {}, Lcom/policydm/XDMApplication;->access$500()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 607
    :cond_1
    :goto_0
    sget-object v1, Lcom/policydm/XDMApplication;->serviceState:Landroid/telephony/ServiceState;

    if-eqz v1, :cond_2

    sget-object v1, Lcom/policydm/XDMApplication;->serviceState:Landroid/telephony/ServiceState;

    invoke-virtual {v1}, Landroid/telephony/ServiceState;->getState()I

    move-result v1

    if-nez v1, :cond_2

    .line 609
    sget-boolean v1, Lcom/policydm/agent/XDMTask;->g_IsSyncTaskInit:Z

    if-eqz v1, :cond_2

    sget-boolean v1, Lcom/policydm/agent/XDMTask;->g_IsDMInitialized:Z

    if-nez v1, :cond_2

    sget-boolean v1, Lcom/policydm/XDMApplication;->g_bIsInitializing:Z

    if-nez v1, :cond_2

    .line 611
    invoke-static {v3, v4, v4}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 614
    :cond_2
    return-void

    .line 600
    :cond_3
    # getter for: Lcom/policydm/XDMApplication;->m_bIsRoaming:Z
    invoke-static {}, Lcom/policydm/XDMApplication;->access$500()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, v0, Lcom/policydm/adapter/XDMTelephonyData;->isNetworkRoaming:Z

    if-nez v1, :cond_1

    .line 602
    const/4 v1, 0x0

    # setter for: Lcom/policydm/XDMApplication;->m_bIsRoaming:Z
    invoke-static {v1}, Lcom/policydm/XDMApplication;->access$502(Z)Z

    .line 603
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "exit from Roaming Area - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/policydm/XDMApplication;->m_bIsRoaming:Z
    invoke-static {}, Lcom/policydm/XDMApplication;->access$500()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCallStateChanged(ILjava/lang/String;)V
    .locals 1
    .param p1, "state"    # I
    .param p2, "szIncomingNumber"    # Ljava/lang/String;

    .prologue
    .line 515
    const-string v0, ">>>>>>>>>>> onCallStateChanged"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 516
    sget-object v0, Lcom/policydm/XDMApplication;->telephonyManager:Landroid/telephony/TelephonyManager;

    if-nez v0, :cond_1

    .line 536
    :cond_0
    :goto_0
    return-void

    .line 519
    :cond_1
    if-nez p1, :cond_3

    .line 521
    sget v0, Lcom/policydm/XDMApplication;->g_nResumeStatus:I

    if-eqz v0, :cond_2

    .line 523
    const-string v0, "Run Resume Operation, Resume call CallStateChanged"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 524
    # invokes: Lcom/policydm/XDMApplication;->xdmResumeOperation()Z
    invoke-static {}, Lcom/policydm/XDMApplication;->access$400()Z

    .line 526
    :cond_2
    const-string v0, ">>>>>>>>>>> CALL_STATE_IDLE"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 528
    :cond_3
    const/4 v0, 0x1

    if-ne p1, v0, :cond_4

    .line 530
    const-string v0, ">>>>>>>>>>> CALL_STATE_RINGING"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 532
    :cond_4
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 534
    const-string v0, ">>>>>>>>>>> CALL_STATE_OFFHOOK"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onDataActivity(I)V
    .locals 0
    .param p1, "direction"    # I

    .prologue
    .line 574
    return-void
.end method

.method public onDataConnectionStateChanged(II)V
    .locals 1
    .param p1, "state"    # I
    .param p2, "networkType"    # I

    .prologue
    .line 551
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetDataState()I

    move-result v0

    if-eq v0, p1, :cond_0

    .line 553
    invoke-static {p1}, Lcom/policydm/XDMApplication;->xdmSetDataState(I)V

    .line 556
    :cond_0
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetServiceType()I

    move-result v0

    if-eq v0, p2, :cond_1

    .line 558
    invoke-virtual {p0}, Lcom/policydm/XDMApplication$XDMPhoneStateListener;->notifyListeners()V

    .line 561
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 563
    sget v0, Lcom/policydm/XDMApplication;->g_nResumeStatus:I

    if-eqz v0, :cond_2

    .line 565
    const-string v0, "Run Resume Operation, Resume call DataConnectionState)"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 566
    # invokes: Lcom/policydm/XDMApplication;->xdmResumeOperation()Z
    invoke-static {}, Lcom/policydm/XDMApplication;->access$400()Z

    .line 569
    :cond_2
    return-void
.end method

.method public onServiceStateChanged(Landroid/telephony/ServiceState;)V
    .locals 1
    .param p1, "state"    # Landroid/telephony/ServiceState;

    .prologue
    .line 541
    sget-object v0, Lcom/policydm/XDMApplication;->serviceState:Landroid/telephony/ServiceState;

    if-eq v0, p1, :cond_0

    .line 543
    sput-object p1, Lcom/policydm/XDMApplication;->serviceState:Landroid/telephony/ServiceState;

    .line 544
    invoke-virtual {p0}, Lcom/policydm/XDMApplication$XDMPhoneStateListener;->notifyListeners()V

    .line 546
    :cond_0
    return-void
.end method
