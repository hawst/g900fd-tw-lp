.class public Lcom/policydm/dlagent/XFOTADlAgent;
.super Ljava/lang/Object;
.source "XFOTADlAgent.java"

# interfaces
.implements Lcom/policydm/interfaces/XDLInterface;
.implements Lcom/policydm/interfaces/XDMInterface;


# static fields
.field private static final XDL_MAX_DOWNLOAD_SIZE:I = 0x1700

.field private static final XDM_HTTP_HEADER_MAX_SIZE:I = 0x300

.field public static m_HttpDLAdapter:Lcom/policydm/tp/XTPAdapter;

.field private static m_bFFSWriteStatus:Z

.field private static m_byReceiveBuffer:[B

.field private static m_nDLConnectRetryCount:I

.field private static m_nDLConnectRetryFailCount:I

.field private static m_nFileSaveIndix:I


# instance fields
.field public m_DlAgentHandler:Lcom/policydm/dlagent/XFOTADlAgentHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 20
    sput v0, Lcom/policydm/dlagent/XFOTADlAgent;->m_nDLConnectRetryCount:I

    .line 21
    sput v0, Lcom/policydm/dlagent/XFOTADlAgent;->m_nDLConnectRetryFailCount:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const/4 v0, 0x0

    sput-boolean v0, Lcom/policydm/dlagent/XFOTADlAgent;->m_bFFSWriteStatus:Z

    .line 35
    sget-object v0, Lcom/policydm/dlagent/XFOTADlAgent;->m_HttpDLAdapter:Lcom/policydm/tp/XTPAdapter;

    if-nez v0, :cond_0

    .line 37
    new-instance v0, Lcom/policydm/tp/XTPAdapter;

    invoke-direct {v0}, Lcom/policydm/tp/XTPAdapter;-><init>()V

    sput-object v0, Lcom/policydm/dlagent/XFOTADlAgent;->m_HttpDLAdapter:Lcom/policydm/tp/XTPAdapter;

    .line 39
    :cond_0
    return-void
.end method

.method public static xfotaDlAgentGetBuffer()[B
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lcom/policydm/dlagent/XFOTADlAgent;->m_byReceiveBuffer:[B

    return-object v0
.end method

.method public static xfotaDlAgentGetFileSaveIndex()I
    .locals 1

    .prologue
    .line 64
    sget v0, Lcom/policydm/dlagent/XFOTADlAgent;->m_nFileSaveIndix:I

    return v0
.end method

.method public static xfotaDlAgentGetHttpConStatus()I
    .locals 5

    .prologue
    .line 96
    const/4 v0, 0x0

    .line 98
    .local v0, "nFileId":I
    const-string v3, ""

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 100
    invoke-static {}, Lcom/policydm/db/XDB;->xdbGetFileIdFirmwareData()I

    move-result v0

    .line 101
    invoke-static {v0}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpGetFileSize(I)I

    move-result v1

    .line 102
    .local v1, "nOffset":I
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMOObjectSize()I

    move-result v2

    .line 104
    .local v2, "nTotalObjectSize":I
    if-ne v1, v2, :cond_0

    .line 106
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "offset = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "  TotalSize = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 107
    const/4 v3, 0x0

    .line 115
    :goto_0
    return v3

    .line 109
    :cond_0
    if-le v1, v2, :cond_1

    .line 111
    invoke-static {v0}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpDeleteFile(I)I

    .line 112
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "offset ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "  TotalSize = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 113
    const/4 v3, -0x1

    goto :goto_0

    .line 115
    :cond_1
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public static xfotaDlAgentGetHttpContentRange(Z)Ljava/lang/String;
    .locals 7
    .param p0, "nDownloadMode"    # Z

    .prologue
    .line 120
    const/4 v2, 0x0

    .line 121
    .local v2, "nOffset":I
    const/4 v0, 0x0

    .line 122
    .local v0, "nDownloadSize":I
    const/4 v3, 0x0

    .line 123
    .local v3, "nTotalObjectSize":I
    const/4 v1, 0x0

    .line 124
    .local v1, "nFileId":I
    const-string v4, ""

    .line 126
    .local v4, "szConLength":Ljava/lang/String;
    sget-boolean v5, Lcom/policydm/dlagent/XFOTADlAgent;->m_bFFSWriteStatus:Z

    if-eqz v5, :cond_0

    .line 128
    const/4 v5, 0x0

    sput-boolean v5, Lcom/policydm/dlagent/XFOTADlAgent;->m_bFFSWriteStatus:Z

    .line 131
    :cond_0
    invoke-static {}, Lcom/policydm/db/XDB;->xdbGetFileIdFirmwareData()I

    move-result v1

    .line 132
    invoke-static {v1}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpGetFileSize(I)I

    move-result v2

    .line 133
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMOObjectSize()I

    move-result v3

    .line 135
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "nOffset = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " nTotalObjectSize = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 137
    if-nez p0, :cond_2

    .line 139
    const/16 v5, 0x1700

    sub-int v6, v3, v2

    if-ge v5, v6, :cond_1

    .line 140
    add-int/lit16 v5, v2, 0x1700

    add-int/lit8 v0, v5, -0x1

    .line 144
    :goto_0
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    .line 145
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "offset = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " , downloadsize = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", TotalSize = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 153
    :goto_1
    return-object v4

    .line 142
    :cond_1
    add-int/lit8 v0, v3, -0x1

    goto :goto_0

    .line 149
    :cond_2
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    .line 150
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "offset = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", TotalSize = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static xfotaDlAgentGetWriteStatus()Z
    .locals 1

    .prologue
    .line 43
    sget-boolean v0, Lcom/policydm/dlagent/XFOTADlAgent;->m_bFFSWriteStatus:Z

    return v0
.end method

.method public static xfotaDlAgentInitBuffer()V
    .locals 1

    .prologue
    .line 54
    const/16 v0, 0x5301

    new-array v0, v0, [B

    sput-object v0, Lcom/policydm/dlagent/XFOTADlAgent;->m_byReceiveBuffer:[B

    .line 55
    return-void
.end method

.method public static xfotaDlAgentIsStatus()Z
    .locals 3

    .prologue
    .line 69
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMOStatus()I

    move-result v0

    .line 70
    .local v0, "nAgentStatus":I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "nAgentStatus = ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 72
    sparse-switch v0, :sswitch_data_0

    .line 83
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 78
    :sswitch_0
    const/4 v1, 0x1

    goto :goto_0

    .line 72
    nop

    :sswitch_data_0
    .sparse-switch
        0x14 -> :sswitch_0
        0x1e -> :sswitch_0
        0x28 -> :sswitch_0
        0xc8 -> :sswitch_0
    .end sparse-switch
.end method

.method public static xfotaDlAgentSetFileSaveIndex(I)V
    .locals 2
    .param p0, "nIndex"    # I

    .prologue
    .line 88
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "xfotaDlAgentSetFileSaveIndex : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 89
    sput p0, Lcom/policydm/dlagent/XFOTADlAgent;->m_nFileSaveIndix:I

    .line 90
    return-void
.end method

.method public static xfotaDlAgentSetWriteStatus(Z)Z
    .locals 1
    .param p0, "nStatus"    # Z

    .prologue
    .line 48
    sput-boolean p0, Lcom/policydm/dlagent/XFOTADlAgent;->m_bFFSWriteStatus:Z

    .line 49
    const/4 v0, 0x1

    return v0
.end method

.method public static xfotaDlTpGetRetryCount()I
    .locals 1

    .prologue
    .line 193
    sget v0, Lcom/policydm/dlagent/XFOTADlAgent;->m_nDLConnectRetryCount:I

    return v0
.end method

.method public static xfotaDlTpGetRetryFailCount()I
    .locals 2

    .prologue
    .line 198
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "xfotaDlTpGetRetryFailCount : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/policydm/dlagent/XFOTADlAgent;->m_nDLConnectRetryFailCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 199
    sget v0, Lcom/policydm/dlagent/XFOTADlAgent;->m_nDLConnectRetryFailCount:I

    return v0
.end method

.method public static xfotaDlTpSetRetryCount(I)V
    .locals 0
    .param p0, "nCnt"    # I

    .prologue
    .line 188
    sput p0, Lcom/policydm/dlagent/XFOTADlAgent;->m_nDLConnectRetryCount:I

    .line 189
    return-void
.end method

.method public static xfotaDlTpSetRetryFailCount(I)V
    .locals 2
    .param p0, "nCnt"    # I

    .prologue
    .line 204
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "xfotaDlTpGetRetryFailCount : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 205
    sput p0, Lcom/policydm/dlagent/XFOTADlAgent;->m_nDLConnectRetryFailCount:I

    .line 206
    return-void
.end method


# virtual methods
.method public xfotaDlTpCheckRetry()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 173
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ConntectRetryCount "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/policydm/dlagent/XFOTADlAgent;->m_nDLConnectRetryCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 174
    sget v1, Lcom/policydm/dlagent/XFOTADlAgent;->m_nDLConnectRetryCount:I

    const/4 v2, 0x5

    if-lt v1, v2, :cond_0

    .line 176
    invoke-static {}, Lcom/policydm/tp/XTPNetConnectTimer;->xtpNetConnEndTimer()V

    .line 177
    invoke-static {}, Lcom/policydm/tp/XTPNetRecvTimer;->xtpNetRecvEndTimer()V

    .line 178
    invoke-static {}, Lcom/policydm/tp/XTPNetSendTimer;->xtpEndTimer()V

    .line 179
    sput v0, Lcom/policydm/dlagent/XFOTADlAgent;->m_nDLConnectRetryCount:I

    .line 183
    :goto_0
    return v0

    .line 182
    :cond_0
    sget v0, Lcom/policydm/dlagent/XFOTADlAgent;->m_nDLConnectRetryCount:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/policydm/dlagent/XFOTADlAgent;->m_nDLConnectRetryCount:I

    .line 183
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public xfotaDlTpClose(I)V
    .locals 1
    .param p1, "appId"    # I

    .prologue
    .line 158
    sget-object v0, Lcom/policydm/dlagent/XFOTADlAgent;->m_HttpDLAdapter:Lcom/policydm/tp/XTPAdapter;

    invoke-virtual {v0, p1}, Lcom/policydm/tp/XTPAdapter;->xtpAdpClose(I)V

    .line 159
    return-void
.end method

.method public xfotaDlTpCloseNetWork(I)V
    .locals 1
    .param p1, "appId"    # I

    .prologue
    .line 168
    sget-object v0, Lcom/policydm/dlagent/XFOTADlAgent;->m_HttpDLAdapter:Lcom/policydm/tp/XTPAdapter;

    invoke-virtual {v0, p1}, Lcom/policydm/tp/XTPAdapter;->xtpAdpCloseNetWork(I)V

    .line 169
    return-void
.end method

.method public xfotaDlTpInit(I)I
    .locals 1
    .param p1, "appId"    # I

    .prologue
    .line 163
    sget-object v0, Lcom/policydm/dlagent/XFOTADlAgent;->m_HttpDLAdapter:Lcom/policydm/tp/XTPAdapter;

    invoke-virtual {v0, p1}, Lcom/policydm/tp/XTPAdapter;->xtpAdpInit(I)I

    move-result v0

    return v0
.end method
