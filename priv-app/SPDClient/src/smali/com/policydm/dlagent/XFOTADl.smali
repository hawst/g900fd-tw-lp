.class public Lcom/policydm/dlagent/XFOTADl;
.super Ljava/lang/Object;
.source "XFOTADl.java"

# interfaces
.implements Lcom/policydm/interfaces/XDLInterface;
.implements Lcom/policydm/interfaces/XEventInterface;
.implements Lcom/policydm/interfaces/XSPDInterface;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xfotaCheckDownloadTime()Z
    .locals 7

    .prologue
    const/4 v6, 0x6

    .line 114
    const/4 v0, 0x0

    .line 116
    .local v0, "bRet":Z
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 117
    .local v3, "currtime":Ljava/util/Calendar;
    const/16 v4, 0xb

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 118
    .local v1, "currHours":I
    const/16 v4, 0xc

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 120
    .local v2, "currMinutes":I
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Current Time :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 122
    const/4 v4, 0x2

    if-le v1, v4, :cond_0

    if-ge v1, v6, :cond_0

    .line 124
    const/4 v0, 0x1

    .line 142
    :goto_0
    return v0

    .line 126
    :cond_0
    if-ne v1, v6, :cond_2

    .line 128
    if-ltz v2, :cond_1

    const/16 v4, 0x28

    if-gt v2, v4, :cond_1

    .line 130
    const/4 v0, 0x1

    goto :goto_0

    .line 134
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 139
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static xfotaDownloadComplete()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 47
    const-string v1, ""

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 49
    invoke-static {}, Lcom/policydm/spd/XSPDAgent;->xspdApplySPotaPolicy()I

    move-result v0

    .line 51
    .local v0, "nRet":I
    if-ne v0, v2, :cond_0

    .line 53
    const/16 v1, 0x64

    invoke-static {v1}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOStatus(I)V

    .line 54
    const-string v1, "200"

    invoke-static {v1}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOResultCode(Ljava/lang/String;)V

    .line 70
    :goto_0
    const/16 v1, 0x3f

    invoke-static {v1, v3, v3}, Lcom/policydm/eng/core/XDMMsg;->xdmSendRCMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 71
    invoke-static {}, Lcom/policydm/spd/XSPDAgent;->xspdRemoveSPotaPolicy()V

    .line 72
    return v2

    .line 58
    :cond_0
    const/16 v1, 0x50

    invoke-static {v1}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOStatus(I)V

    .line 59
    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 60
    const-string v1, "503"

    invoke-static {v1}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOResultCode(Ljava/lang/String;)V

    goto :goto_0

    .line 61
    :cond_1
    const/4 v1, -0x2

    if-ne v0, v1, :cond_2

    .line 62
    const-string v1, "504"

    invoke-static {v1}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOResultCode(Ljava/lang/String;)V

    goto :goto_0

    .line 63
    :cond_2
    const/4 v1, -0x3

    if-ne v0, v1, :cond_3

    .line 64
    const-string v1, "505"

    invoke-static {v1}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOResultCode(Ljava/lang/String;)V

    goto :goto_0

    .line 65
    :cond_3
    const/4 v1, -0x4

    if-ne v0, v1, :cond_4

    .line 66
    const-string v1, "506"

    invoke-static {v1}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOResultCode(Ljava/lang/String;)V

    goto :goto_0

    .line 68
    :cond_4
    const-string v1, "600"

    invoke-static {v1}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOResultCode(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xfotaDownloadMemoryCheck(I)V
    .locals 3
    .param p0, "nDeltaDownState"    # I

    .prologue
    const/4 v2, 0x0

    .line 18
    const-string v1, ""

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 19
    sput p0, Lcom/policydm/dlagent/XFOTADlMemChk;->nDeltaDownState:I

    .line 21
    invoke-static {}, Lcom/policydm/dlagent/XFOTADlAgentHandler;->xfotaDlAgentHdlrCheckDeltaPkgSize()I

    move-result v0

    .line 23
    .local v0, "nRet":I
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 25
    const-string v1, "FFS memory Insufficient"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 26
    const-string v1, "501"

    invoke-static {v1}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOResultCode(Ljava/lang/String;)V

    .line 34
    :cond_0
    :goto_0
    if-eqz v0, :cond_2

    .line 36
    const/16 v1, 0x14

    invoke-static {v1}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOStatus(I)V

    .line 37
    const/16 v1, 0x3f

    invoke-static {v1, v2, v2}, Lcom/policydm/eng/core/XDMMsg;->xdmSendRCMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 43
    :goto_1
    return-void

    .line 28
    :cond_1
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 30
    const-string v1, "Device Encrypted"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 31
    const-string v1, "502"

    invoke-static {v1}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOResultCode(Ljava/lang/String;)V

    goto :goto_0

    .line 41
    :cond_2
    const/16 v1, 0x28

    invoke-static {v1, v2, v2}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public static xfotaEncryptionMemoryCheck(Lcom/policydm/eng/core/XDMDeviceEncrypt;I)V
    .locals 4
    .param p0, "devEncrypt"    # Lcom/policydm/eng/core/XDMDeviceEncrypt;
    .param p1, "nDataSize"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 77
    const-string v0, ""

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 79
    if-nez p0, :cond_1

    .line 81
    const-string v0, "wssdmDeviceEncrypt is null"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 110
    :cond_0
    :goto_0
    return-void

    .line 85
    :cond_1
    sput-object p0, Lcom/policydm/dlagent/XFOTADlMemChk;->devEncrypt:Lcom/policydm/eng/core/XDMDeviceEncrypt;

    .line 86
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "useInternal : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/policydm/dlagent/XFOTADlMemChk;->bUseInternal:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", useExternal : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/policydm/dlagent/XFOTADlMemChk;->bUseExternal:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 87
    iget-boolean v0, p0, Lcom/policydm/eng/core/XDMDeviceEncrypt;->bAllareaDecrypted:Z

    if-eqz v0, :cond_2

    .line 89
    const-string v0, "Device is all Area Decryption"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 90
    sput-boolean v2, Lcom/policydm/dlagent/XFOTADlMemChk;->bUseInternal:Z

    .line 91
    sput-boolean v2, Lcom/policydm/dlagent/XFOTADlMemChk;->bUseExternal:Z

    goto :goto_0

    .line 95
    :cond_2
    iget-boolean v0, p0, Lcom/policydm/eng/core/XDMDeviceEncrypt;->bAllareaEncrypted:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/policydm/eng/core/XDMDeviceEncrypt;->bInternalEncrypted:Z

    if-eqz v0, :cond_4

    .line 97
    :cond_3
    const-string v0, "Device is AllareaEncrypted or InternalEncrypted"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 98
    sput-boolean v3, Lcom/policydm/dlagent/XFOTADlMemChk;->bUseInternal:Z

    .line 99
    sput-boolean v3, Lcom/policydm/dlagent/XFOTADlMemChk;->bUseExternal:Z

    goto :goto_0

    .line 103
    :cond_4
    iget-boolean v0, p0, Lcom/policydm/eng/core/XDMDeviceEncrypt;->bSDEncrypted:Z

    if-eqz v0, :cond_0

    .line 105
    const-string v0, "Device is SDEncrypted"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 106
    sput-boolean v2, Lcom/policydm/dlagent/XFOTADlMemChk;->bUseInternal:Z

    .line 107
    sput-boolean v2, Lcom/policydm/dlagent/XFOTADlMemChk;->bUseExternal:Z

    goto :goto_0
.end method
