.class public Lcom/policydm/dlagent/XFOTADlAgentHandler;
.super Lcom/policydm/dlagent/XFOTADlAgent;
.source "XFOTADlAgentHandler.java"

# interfaces
.implements Lcom/policydm/interfaces/XDBInterface;
.implements Lcom/policydm/interfaces/XDMDefInterface;
.implements Lcom/policydm/interfaces/XEventInterface;
.implements Lcom/policydm/interfaces/XSPDInterface;
.implements Lcom/policydm/interfaces/XTPInterface;
.implements Lcom/policydm/interfaces/XUIInterface;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/policydm/dlagent/XFOTADlAgent;-><init>()V

    return-void
.end method

.method public static xfotaDlAgentHdlrCheckDeltaPkgSize()I
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 27
    const/4 v1, 0x0

    .line 28
    .local v1, "nObjectSize":I
    const/high16 v0, 0x500000

    .line 29
    .local v0, "nExtraSpace":I
    const/4 v2, 0x4

    .line 31
    .local v2, "nRet":I
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMOObjectSize()I

    move-result v1

    .line 32
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "FirmwareObjectSize:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 34
    sget v4, Lcom/policydm/dlagent/XFOTADlMemChk;->nDeltaDownState:I

    if-ne v4, v3, :cond_1

    .line 36
    add-int v4, v1, v0

    invoke-static {v4}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpFileFreeSizeCheck(I)I

    move-result v2

    .line 37
    if-eqz v2, :cond_1

    .line 39
    const/4 v4, 0x6

    if-ne v2, v4, :cond_0

    .line 41
    const/4 v3, 0x2

    .line 49
    :goto_0
    return v3

    .line 44
    :cond_0
    const-string v4, "FFS Free Space NOT ENOUGH"

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 49
    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method


# virtual methods
.method public xfotaDlAgentHdlrDownloadComplete()I
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 432
    const/4 v1, 0x0

    .line 434
    .local v1, "nRc":I
    const-string v5, ""

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 436
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 440
    .local v4, "pReceiveBuffer":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    sget-object v5, Lcom/policydm/dlagent/XFOTADlAgentHandler;->m_HttpDLAdapter:Lcom/policydm/tp/XTPAdapter;

    const/4 v6, 0x1

    invoke-virtual {v5, v4, v6}, Lcom/policydm/tp/XTPAdapter;->xtpAdpReceiveData(Ljava/io/ByteArrayOutputStream;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 449
    :goto_0
    const/16 v5, -0xf

    if-ne v1, v5, :cond_0

    .line 451
    const/16 v5, 0xf2

    const/4 v6, 0x0

    invoke-static {v5, v6}, Lcom/policydm/eng/core/XDMMsg;->xdmCreateAbortMessage(IZ)Lcom/policydm/eng/core/XDMAbortMsgParam;

    move-result-object v3

    .line 452
    .local v3, "pAbortParam":Lcom/policydm/eng/core/XDMAbortMsgParam;
    const/16 v5, 0x2c

    invoke-static {v5, v3, v7}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    move v2, v1

    .line 462
    .end local v1    # "nRc":I
    .end local v3    # "pAbortParam":Lcom/policydm/eng/core/XDMAbortMsgParam;
    .local v2, "nRc":I
    :goto_1
    return v2

    .line 442
    .end local v2    # "nRc":I
    .restart local v1    # "nRc":I
    :catch_0
    move-exception v0

    .line 444
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 445
    invoke-static {}, Lcom/policydm/tp/XTPNetRecvTimer;->xtpNetRecvEndTimer()V

    .line 446
    const/16 v1, -0xe

    goto :goto_0

    .line 455
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    if-eqz v1, :cond_1

    .line 457
    const/16 v5, 0x2f

    invoke-static {v5, v7, v7}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    move v2, v1

    .line 458
    .end local v1    # "nRc":I
    .restart local v2    # "nRc":I
    goto :goto_1

    .line 461
    .end local v2    # "nRc":I
    .restart local v1    # "nRc":I
    :cond_1
    invoke-virtual {p0}, Lcom/policydm/dlagent/XFOTADlAgentHandler;->xfotaDlAgentHdlrDownloadCompleteFumo()I

    move-result v1

    move v2, v1

    .line 462
    .end local v1    # "nRc":I
    .restart local v2    # "nRc":I
    goto :goto_1
.end method

.method public xfotaDlAgentHdlrDownloadCompleteFumo()I
    .locals 6

    .prologue
    const/16 v5, 0x2d

    const/4 v4, 0x0

    .line 467
    const/4 v1, 0x0

    .line 468
    .local v1, "nRc":I
    const/4 v0, 0x0

    .line 470
    .local v0, "nAgentStatus":I
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMOStatus()I

    move-result v0

    .line 471
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "nAgentStatus = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 473
    const/16 v2, 0x14

    if-ne v0, v2, :cond_0

    .line 475
    invoke-static {v5, v4, v4}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 476
    const/16 v2, 0xf1

    invoke-static {v2}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOStatus(I)V

    .line 478
    const/16 v2, 0x3f

    invoke-static {v2, v4, v4}, Lcom/policydm/eng/core/XDMMsg;->xdmSendRCMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 487
    :goto_0
    return v1

    .line 482
    :cond_0
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMODownloadMode(Ljava/lang/Boolean;)V

    .line 484
    invoke-static {v5, v4, v4}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 485
    const/16 v2, 0x97

    invoke-static {v4, v2}, Lcom/policydm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    goto :goto_0
.end method

.method public xfotaDlAgentHdlrDownloadProgress()I
    .locals 15

    .prologue
    const/16 v14, 0x3f

    const/16 v13, 0x14

    const/4 v12, 0x0

    .line 54
    const-string v9, ""

    .line 55
    .local v9, "szContentRange":Ljava/lang/String;
    const/4 v4, 0x0

    .line 56
    .local v4, "nRc":I
    const/4 v6, 0x0

    .line 59
    .local v6, "nStatus":I
    new-instance v8, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v8}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 63
    .local v8, "pReceiveBuffer":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    sget-object v10, Lcom/policydm/dlagent/XFOTADlAgentHandler;->m_HttpDLAdapter:Lcom/policydm/tp/XTPAdapter;

    const/4 v11, 0x1

    invoke-virtual {v10, v8, v11}, Lcom/policydm/tp/XTPAdapter;->xtpAdpReceiveData(Ljava/io/ByteArrayOutputStream;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 72
    :goto_0
    const/16 v10, -0xf

    if-ne v4, v10, :cond_0

    .line 74
    const/16 v10, 0xf2

    const/4 v11, 0x0

    invoke-static {v10, v11}, Lcom/policydm/eng/core/XDMMsg;->xdmCreateAbortMessage(IZ)Lcom/policydm/eng/core/XDMAbortMsgParam;

    move-result-object v7

    .line 75
    .local v7, "pAbortParam":Lcom/policydm/eng/core/XDMAbortMsgParam;
    const/16 v10, 0x2c

    invoke-static {v10, v7, v12}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    move v5, v4

    .line 139
    .end local v4    # "nRc":I
    .end local v7    # "pAbortParam":Lcom/policydm/eng/core/XDMAbortMsgParam;
    .local v5, "nRc":I
    :goto_1
    return v5

    .line 65
    .end local v5    # "nRc":I
    .restart local v4    # "nRc":I
    :catch_0
    move-exception v0

    .line 67
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 68
    invoke-static {}, Lcom/policydm/tp/XTPNetRecvTimer;->xtpNetRecvEndTimer()V

    .line 69
    const/16 v4, -0xe

    goto :goto_0

    .line 78
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const/16 v10, -0x12

    if-ne v4, v10, :cond_1

    .line 81
    invoke-static {}, Lcom/policydm/db/XDB;->xdbGetFileIdFirmwareData()I

    move-result v3

    .line 82
    .local v3, "nFUMOFileId":I
    invoke-static {v3}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpDeleteFile(I)I

    .line 84
    invoke-static {v13}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOStatus(I)V

    .line 85
    const-string v10, "501"

    invoke-static {v10}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOResultCode(Ljava/lang/String;)V

    .line 87
    invoke-static {v14, v12, v12}, Lcom/policydm/eng/core/XDMMsg;->xdmSendRCMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    move v5, v4

    .line 88
    .end local v4    # "nRc":I
    .restart local v5    # "nRc":I
    goto :goto_1

    .line 90
    .end local v3    # "nFUMOFileId":I
    .end local v5    # "nRc":I
    .restart local v4    # "nRc":I
    :cond_1
    const/16 v10, -0x13

    if-ne v4, v10, :cond_2

    .line 93
    invoke-static {}, Lcom/policydm/db/XDB;->xdbGetFileIdFirmwareData()I

    move-result v3

    .line 94
    .restart local v3    # "nFUMOFileId":I
    invoke-static {v3}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpDeleteFile(I)I

    .line 96
    invoke-static {v13}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOStatus(I)V

    .line 97
    const-string v10, "501"

    invoke-static {v10}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOResultCode(Ljava/lang/String;)V

    .line 99
    invoke-static {v14, v12, v12}, Lcom/policydm/eng/core/XDMMsg;->xdmSendRCMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    move v5, v4

    .line 100
    .end local v4    # "nRc":I
    .restart local v5    # "nRc":I
    goto :goto_1

    .line 102
    .end local v3    # "nFUMOFileId":I
    .end local v5    # "nRc":I
    .restart local v4    # "nRc":I
    :cond_2
    const/16 v10, -0x10

    if-ne v4, v10, :cond_3

    .line 105
    invoke-static {}, Lcom/policydm/db/XDB;->xdbGetFileIdFirmwareData()I

    move-result v3

    .line 106
    .restart local v3    # "nFUMOFileId":I
    invoke-static {v3}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpDeleteFile(I)I

    .line 108
    invoke-static {v13}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOStatus(I)V

    .line 109
    const-string v10, "507"

    invoke-static {v10}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOResultCode(Ljava/lang/String;)V

    .line 111
    invoke-static {v14, v12, v12}, Lcom/policydm/eng/core/XDMMsg;->xdmSendRCMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    move v5, v4

    .line 112
    .end local v4    # "nRc":I
    .restart local v5    # "nRc":I
    goto :goto_1

    .line 114
    .end local v3    # "nFUMOFileId":I
    .end local v5    # "nRc":I
    .restart local v4    # "nRc":I
    :cond_3
    if-eqz v4, :cond_5

    .line 116
    const/4 v1, 0x0

    .line 117
    .local v1, "nAgentStatus":I
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMOStatus()I

    move-result v1

    .line 118
    if-ne v1, v13, :cond_4

    move v5, v4

    .line 119
    .end local v4    # "nRc":I
    .restart local v5    # "nRc":I
    goto :goto_1

    .line 121
    .end local v5    # "nRc":I
    .restart local v4    # "nRc":I
    :cond_4
    const/16 v10, 0x2f

    invoke-static {v10, v12, v12}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    move v5, v4

    .line 122
    .end local v4    # "nRc":I
    .restart local v5    # "nRc":I
    goto :goto_1

    .line 125
    .end local v1    # "nAgentStatus":I
    .end local v5    # "nRc":I
    .restart local v4    # "nRc":I
    :cond_5
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMODownloadMode()Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 126
    .local v2, "nDownloadMode":Z
    if-eqz v2, :cond_7

    .line 127
    const-string v10, "nDownloadMode is TRUE"

    invoke-static {v10}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 131
    :goto_2
    invoke-static {}, Lcom/policydm/dlagent/XFOTADlAgentHandler;->xfotaDlAgentGetHttpConStatus()I

    move-result v6

    .line 132
    if-eqz v6, :cond_6

    .line 134
    invoke-static {v2}, Lcom/policydm/dlagent/XFOTADlAgentHandler;->xfotaDlAgentGetHttpContentRange(Z)Ljava/lang/String;

    move-result-object v9

    .line 137
    :cond_6
    invoke-virtual {p0, v6, v2, v9}, Lcom/policydm/dlagent/XFOTADlAgentHandler;->xfotaDlAgentHdlrDownloadProgressFumo(IZLjava/lang/String;)I

    move-result v4

    move v5, v4

    .line 139
    .end local v4    # "nRc":I
    .restart local v5    # "nRc":I
    goto/16 :goto_1

    .line 129
    .end local v5    # "nRc":I
    .restart local v4    # "nRc":I
    :cond_7
    const-string v10, "nDownloadMode is FALSE"

    invoke-static {v10}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public xfotaDlAgentHdlrDownloadProgressFumo(IZLjava/lang/String;)I
    .locals 12
    .param p1, "nStatus"    # I
    .param p2, "nDownloadMode"    # Z
    .param p3, "szContentRange"    # Ljava/lang/String;

    .prologue
    const/16 v11, 0x2e

    const/4 v3, 0x1

    const/4 v10, 0x0

    .line 144
    const-string v1, ""

    .line 145
    .local v1, "szResURL":Ljava/lang/String;
    const-string v2, ""

    .line 146
    .local v2, "szHmacData":Ljava/lang/String;
    const/4 v8, 0x0

    .line 148
    .local v8, "nRc":I
    const-string v0, ""

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 149
    if-nez p1, :cond_0

    .line 151
    const/16 v0, 0x28

    invoke-static {v0}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOStatus(I)V

    .line 153
    const/16 v0, 0x2d

    invoke-static {v0, v10, v10}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 154
    const/16 v0, 0x97

    invoke-static {v10, v0}, Lcom/policydm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    :goto_0
    move v9, v8

    .line 211
    .end local v8    # "nRc":I
    .local v9, "nRc":I
    :goto_1
    return v9

    .line 156
    .end local v9    # "nRc":I
    .restart local v8    # "nRc":I
    :cond_0
    if-ne p1, v3, :cond_4

    .line 158
    invoke-static {v1}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMODownloadAddr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 162
    if-eqz p2, :cond_1

    .line 163
    :try_start_0
    sget-object v0, Lcom/policydm/dlagent/XFOTADlAgentHandler;->m_HttpDLAdapter:Lcom/policydm/tp/XTPAdapter;

    const-string v4, "GET"

    const/4 v5, 0x1

    const/4 v6, 0x1

    move-object v3, p3

    invoke-virtual/range {v0 .. v6}, Lcom/policydm/tp/XTPAdapter;->xtpAdpSetHttpObj(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 178
    :goto_2
    :try_start_1
    sget-object v0, Lcom/policydm/dlagent/XFOTADlAgentHandler;->m_HttpDLAdapter:Lcom/policydm/tp/XTPAdapter;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-virtual {v0, v3, v4, v5}, Lcom/policydm/tp/XTPAdapter;->xtpAdpSendData([BII)I
    :try_end_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v8

    .line 186
    :goto_3
    if-nez v8, :cond_2

    .line 188
    const-string v0, "XEVENT_DL_CONTINUE"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 189
    const/16 v0, 0x2b

    invoke-static {v0, v10, v10}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 165
    :cond_1
    :try_start_2
    sget-object v0, Lcom/policydm/dlagent/XFOTADlAgentHandler;->m_HttpDLAdapter:Lcom/policydm/tp/XTPAdapter;

    const-string v4, "GET"

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v3, p3

    invoke-virtual/range {v0 .. v6}, Lcom/policydm/tp/XTPAdapter;->xtpAdpSetHttpObj(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)I
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    .line 167
    :catch_0
    move-exception v7

    .line 169
    .local v7, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v7}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 170
    invoke-static {}, Lcom/policydm/tp/XTPNetSendTimer;->xtpEndTimer()V

    .line 171
    const/16 v8, -0xd

    .line 172
    invoke-static {v11, v10, v10}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    move v9, v8

    .line 173
    .end local v8    # "nRc":I
    .restart local v9    # "nRc":I
    goto :goto_1

    .line 180
    .end local v7    # "e":Ljava/lang/NullPointerException;
    .end local v9    # "nRc":I
    .restart local v8    # "nRc":I
    :catch_1
    move-exception v7

    .line 182
    .local v7, "e":Ljava/net/SocketTimeoutException;
    invoke-virtual {v7}, Ljava/net/SocketTimeoutException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 183
    invoke-static {}, Lcom/policydm/tp/XTPNetSendTimer;->xtpEndTimer()V

    .line 184
    const/16 v8, -0xd

    goto :goto_3

    .line 191
    .end local v7    # "e":Ljava/net/SocketTimeoutException;
    :cond_2
    const/16 v0, -0xc

    if-ne v8, v0, :cond_3

    .line 193
    const-string v0, "XTP_RET_CONNECTION_FAIL"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 194
    const/16 v0, 0x29

    invoke-static {v0, v10, v10}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 199
    :cond_3
    invoke-static {v11, v10, v10}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 204
    :cond_4
    const/16 v8, -0xe

    .line 205
    const-string v0, "XDL_STATE_DOWNLOAD_FAILED"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 206
    const/16 v0, 0x14

    invoke-static {v0}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOStatus(I)V

    .line 207
    const-string v0, "507"

    invoke-static {v0}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOResultCode(Ljava/lang/String;)V

    .line 209
    const/16 v0, 0x3f

    invoke-static {v0, v10, v10}, Lcom/policydm/eng/core/XDMMsg;->xdmSendRCMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public xfotaDlAgentHdlrDownloadStart()I
    .locals 5

    .prologue
    .line 242
    const-string v3, ""

    .line 243
    .local v3, "szContentRange":Ljava/lang/String;
    const/4 v1, 0x0

    .line 247
    .local v1, "nRc":I
    const-string v4, ""

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 249
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMODownloadMode()Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 250
    .local v0, "nDownloadMode":Z
    invoke-static {}, Lcom/policydm/dlagent/XFOTADlAgentHandler;->xfotaDlAgentGetHttpConStatus()I

    move-result v2

    .line 251
    .local v2, "nStatus":I
    if-eqz v2, :cond_0

    .line 253
    invoke-static {v0}, Lcom/policydm/dlagent/XFOTADlAgentHandler;->xfotaDlAgentGetHttpContentRange(Z)Ljava/lang/String;

    move-result-object v3

    .line 256
    :cond_0
    invoke-virtual {p0, v2, v0, v3}, Lcom/policydm/dlagent/XFOTADlAgentHandler;->xfotaDlAgentHdlrDownloadStartFumo(IZLjava/lang/String;)I

    move-result v1

    .line 258
    return v1
.end method

.method public xfotaDlAgentHdlrDownloadStartFumo(IZLjava/lang/String;)I
    .locals 12
    .param p1, "nStatus"    # I
    .param p2, "nDownloadMode"    # Z
    .param p3, "szContentRange"    # Ljava/lang/String;

    .prologue
    const/16 v11, 0x2e

    const/4 v3, 0x1

    const/4 v10, 0x0

    .line 263
    const-string v1, ""

    .line 264
    .local v1, "szResURL":Ljava/lang/String;
    const-string v2, ""

    .line 265
    .local v2, "szHmacData":Ljava/lang/String;
    const/4 v8, 0x0

    .line 267
    .local v8, "nRc":I
    const-string v0, ""

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 268
    if-nez p1, :cond_0

    .line 270
    const/16 v0, 0x28

    invoke-static {v0}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOStatus(I)V

    .line 272
    const/16 v0, 0x2d

    invoke-static {v0, v10, v10}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 273
    const/16 v0, 0x97

    invoke-static {v10, v0}, Lcom/policydm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    :goto_0
    move v9, v8

    .line 329
    .end local v8    # "nRc":I
    .local v9, "nRc":I
    :goto_1
    return v9

    .line 275
    .end local v9    # "nRc":I
    .restart local v8    # "nRc":I
    :cond_0
    if-ne p1, v3, :cond_4

    .line 277
    invoke-static {v1}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMODownloadAddr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 281
    if-eqz p2, :cond_1

    .line 282
    :try_start_0
    sget-object v0, Lcom/policydm/dlagent/XFOTADlAgentHandler;->m_HttpDLAdapter:Lcom/policydm/tp/XTPAdapter;

    const-string v4, "GET"

    const/4 v5, 0x1

    const/4 v6, 0x1

    move-object v3, p3

    invoke-virtual/range {v0 .. v6}, Lcom/policydm/tp/XTPAdapter;->xtpAdpSetHttpObj(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 297
    :goto_2
    :try_start_1
    sget-object v0, Lcom/policydm/dlagent/XFOTADlAgentHandler;->m_HttpDLAdapter:Lcom/policydm/tp/XTPAdapter;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-virtual {v0, v3, v4, v5}, Lcom/policydm/tp/XTPAdapter;->xtpAdpSendData([BII)I
    :try_end_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v8

    .line 305
    :goto_3
    if-nez v8, :cond_2

    .line 307
    const/16 v0, 0x2b

    invoke-static {v0, v10, v10}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 319
    :goto_4
    const/16 v0, 0x1e

    invoke-static {v0}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOStatus(I)V

    goto :goto_0

    .line 284
    :cond_1
    :try_start_2
    sget-object v0, Lcom/policydm/dlagent/XFOTADlAgentHandler;->m_HttpDLAdapter:Lcom/policydm/tp/XTPAdapter;

    const-string v4, "GET"

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v3, p3

    invoke-virtual/range {v0 .. v6}, Lcom/policydm/tp/XTPAdapter;->xtpAdpSetHttpObj(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)I
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    .line 286
    :catch_0
    move-exception v7

    .line 288
    .local v7, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v7}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 289
    invoke-static {}, Lcom/policydm/tp/XTPNetSendTimer;->xtpEndTimer()V

    .line 290
    const/16 v8, -0xd

    .line 291
    invoke-static {v11, v10, v10}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    move v9, v8

    .line 292
    .end local v8    # "nRc":I
    .restart local v9    # "nRc":I
    goto :goto_1

    .line 299
    .end local v7    # "e":Ljava/lang/NullPointerException;
    .end local v9    # "nRc":I
    .restart local v8    # "nRc":I
    :catch_1
    move-exception v7

    .line 301
    .local v7, "e":Ljava/net/SocketTimeoutException;
    invoke-virtual {v7}, Ljava/net/SocketTimeoutException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 302
    invoke-static {}, Lcom/policydm/tp/XTPNetSendTimer;->xtpEndTimer()V

    .line 303
    const/16 v8, -0xd

    goto :goto_3

    .line 309
    .end local v7    # "e":Ljava/net/SocketTimeoutException;
    :cond_2
    const/16 v0, -0xc

    if-ne v8, v0, :cond_3

    .line 311
    const/16 v0, 0x29

    invoke-static {v0, v10, v10}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_4

    .line 316
    :cond_3
    invoke-static {v11, v10, v10}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_4

    .line 323
    :cond_4
    const-string v0, "XDL_STATE_DOWNLOAD_FAILED"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 324
    const/16 v0, 0x14

    invoke-static {v0}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOStatus(I)V

    .line 325
    const-string v0, "507"

    invoke-static {v0}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOResultCode(Ljava/lang/String;)V

    .line 327
    const/16 v0, 0x3f

    invoke-static {v0, v10, v10}, Lcom/policydm/eng/core/XDMMsg;->xdmSendRCMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public xfotaDlAgentHdlrDownloadTakeOver()I
    .locals 7

    .prologue
    .line 334
    const/4 v4, 0x0

    .line 335
    .local v4, "szContentRange":Ljava/lang/String;
    const/4 v3, 0x0

    .line 336
    .local v3, "nStatus":I
    const/4 v2, 0x0

    .line 339
    .local v2, "nRc":I
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMODownloadMode()Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 340
    .local v0, "bDownloadMode":Z
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "nDownloadMode = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 342
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMOStatus()I

    move-result v1

    .line 343
    .local v1, "nOrgStatus":I
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "fumo org status = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 345
    invoke-static {}, Lcom/policydm/dlagent/XFOTADlAgentHandler;->xfotaDlAgentGetHttpConStatus()I

    move-result v3

    .line 347
    if-eqz v3, :cond_0

    .line 349
    invoke-static {v0}, Lcom/policydm/dlagent/XFOTADlAgentHandler;->xfotaDlAgentGetHttpContentRange(Z)Ljava/lang/String;

    move-result-object v4

    .line 352
    :cond_0
    invoke-virtual {p0, v3, v0, v4}, Lcom/policydm/dlagent/XFOTADlAgentHandler;->xfotaDlAgentHdlrDownloadTakeOverFumo(IZLjava/lang/String;)I

    move-result v2

    .line 354
    return v2
.end method

.method public xfotaDlAgentHdlrDownloadTakeOverFumo(IZLjava/lang/String;)I
    .locals 11
    .param p1, "nStatus"    # I
    .param p2, "nDownloadMode"    # Z
    .param p3, "szContentRange"    # Ljava/lang/String;

    .prologue
    .line 359
    const-string v1, ""

    .line 360
    .local v1, "szResURL":Ljava/lang/String;
    const-string v2, ""

    .line 361
    .local v2, "szHmacData":Ljava/lang/String;
    const/4 v9, 0x0

    .line 364
    .local v9, "nRc":I
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMOStatus()I

    move-result v8

    .line 365
    .local v8, "nOrgStatus":I
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "fumo org status = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 367
    if-nez p1, :cond_0

    .line 369
    const/16 v0, 0x28

    invoke-static {v0}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOStatus(I)V

    .line 371
    const/16 v0, 0x2d

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 372
    const/4 v0, 0x0

    const/16 v3, 0x97

    invoke-static {v0, v3}, Lcom/policydm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    :goto_0
    move v10, v9

    .line 427
    .end local v9    # "nRc":I
    .local v10, "nRc":I
    :goto_1
    return v10

    .line 374
    .end local v10    # "nRc":I
    .restart local v9    # "nRc":I
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_4

    .line 376
    invoke-static {v1}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMODownloadAddr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 379
    if-eqz p2, :cond_1

    .line 380
    :try_start_0
    sget-object v0, Lcom/policydm/dlagent/XFOTADlAgentHandler;->m_HttpDLAdapter:Lcom/policydm/tp/XTPAdapter;

    const-string v4, "GET"

    const/4 v5, 0x1

    const/4 v6, 0x1

    move-object v3, p3

    invoke-virtual/range {v0 .. v6}, Lcom/policydm/tp/XTPAdapter;->xtpAdpSetHttpObj(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 395
    :goto_2
    :try_start_1
    sget-object v0, Lcom/policydm/dlagent/XFOTADlAgentHandler;->m_HttpDLAdapter:Lcom/policydm/tp/XTPAdapter;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-virtual {v0, v3, v4, v5}, Lcom/policydm/tp/XTPAdapter;->xtpAdpSendData([BII)I
    :try_end_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v9

    .line 403
    :goto_3
    if-nez v9, :cond_2

    .line 405
    const/16 v0, 0x2b

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 417
    :goto_4
    const/16 v0, 0x1e

    invoke-static {v0}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOStatus(I)V

    goto :goto_0

    .line 382
    :cond_1
    :try_start_2
    sget-object v0, Lcom/policydm/dlagent/XFOTADlAgentHandler;->m_HttpDLAdapter:Lcom/policydm/tp/XTPAdapter;

    const-string v4, "GET"

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v3, p3

    invoke-virtual/range {v0 .. v6}, Lcom/policydm/tp/XTPAdapter;->xtpAdpSetHttpObj(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)I
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    .line 384
    :catch_0
    move-exception v7

    .line 386
    .local v7, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v7}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 387
    invoke-static {}, Lcom/policydm/tp/XTPNetSendTimer;->xtpEndTimer()V

    .line 388
    const/16 v9, -0xd

    .line 389
    const/16 v0, 0x2e

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    move v10, v9

    .line 390
    .end local v9    # "nRc":I
    .restart local v10    # "nRc":I
    goto :goto_1

    .line 397
    .end local v7    # "e":Ljava/lang/NullPointerException;
    .end local v10    # "nRc":I
    .restart local v9    # "nRc":I
    :catch_1
    move-exception v7

    .line 399
    .local v7, "e":Ljava/net/SocketTimeoutException;
    invoke-virtual {v7}, Ljava/net/SocketTimeoutException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 400
    invoke-static {}, Lcom/policydm/tp/XTPNetSendTimer;->xtpEndTimer()V

    .line 401
    const/16 v9, -0xd

    goto :goto_3

    .line 407
    .end local v7    # "e":Ljava/net/SocketTimeoutException;
    :cond_2
    const/16 v0, -0xc

    if-ne v9, v0, :cond_3

    .line 409
    const/16 v0, 0x29

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_4

    .line 414
    :cond_3
    const/16 v0, 0x2e

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_4

    .line 421
    :cond_4
    const-string v0, "XDL_STATE_DOWNLOAD_FAILED"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 422
    const/16 v0, 0x14

    invoke-static {v0}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOStatus(I)V

    .line 423
    const-string v0, "507"

    invoke-static {v0}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOResultCode(Ljava/lang/String;)V

    .line 425
    const/16 v0, 0x3f

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, Lcom/policydm/eng/core/XDMMsg;->xdmSendRCMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method public xfotaDlAgentHdlrStartOMADLAgent(I)I
    .locals 13
    .param p1, "nEvent"    # I

    .prologue
    const/16 v12, 0x2e

    const/4 v5, 0x1

    const/4 v11, 0x0

    .line 492
    const-string v2, ""

    .line 493
    .local v2, "szHmacData":Ljava/lang/String;
    const-string v3, ""

    .line 495
    .local v3, "szContentRange":Ljava/lang/String;
    const/4 v9, 0x0

    .line 497
    .local v9, "rc":I
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMOStatus()I

    move-result v8

    .line 499
    .local v8, "nDLStatus":I
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "nEvent ["

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "] nAgentStatus["

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "]"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 501
    packed-switch p1, :pswitch_data_0

    .line 599
    :goto_0
    :sswitch_0
    invoke-static {}, Lcom/policydm/dlagent/XFOTADlAgentHandler;->xfotaDlAgentInitBuffer()V

    move v10, v9

    .line 600
    :goto_1
    return v10

    .line 505
    :pswitch_0
    sparse-switch v8, :sswitch_data_0

    goto :goto_0

    .line 509
    :sswitch_1
    const/4 v10, 0x0

    .line 510
    .local v10, "ret":I
    invoke-static {v5}, Lcom/policydm/db/XDB;->xdbGetServerUrl(I)Ljava/lang/String;

    move-result-object v1

    .line 514
    .local v1, "szResponsURL":Ljava/lang/String;
    :try_start_0
    sget-object v0, Lcom/policydm/dlagent/XFOTADlAgentHandler;->m_HttpDLAdapter:Lcom/policydm/tp/XTPAdapter;

    const-string v4, "GET"

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/policydm/tp/XTPAdapter;->xtpAdpSetHttpObj(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 527
    :try_start_1
    sget-object v0, Lcom/policydm/dlagent/XFOTADlAgentHandler;->m_HttpDLAdapter:Lcom/policydm/tp/XTPAdapter;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-virtual {v0, v4, v5, v6}, Lcom/policydm/tp/XTPAdapter;->xtpAdpSendData([BII)I
    :try_end_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v10

    .line 535
    :goto_2
    if-nez v10, :cond_0

    .line 537
    const/16 v0, 0x2b

    invoke-static {v0, v11, v11}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 516
    :catch_0
    move-exception v7

    .line 518
    .local v7, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v7}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 519
    invoke-static {}, Lcom/policydm/tp/XTPNetSendTimer;->xtpEndTimer()V

    .line 520
    const/16 v10, -0xd

    .line 521
    invoke-static {v12, v11, v11}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    .line 529
    .end local v7    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v7

    .line 531
    .local v7, "e":Ljava/net/SocketTimeoutException;
    invoke-virtual {v7}, Ljava/net/SocketTimeoutException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 532
    invoke-static {}, Lcom/policydm/tp/XTPNetSendTimer;->xtpEndTimer()V

    .line 533
    const/16 v10, -0xd

    goto :goto_2

    .line 539
    .end local v7    # "e":Ljava/net/SocketTimeoutException;
    :cond_0
    const/16 v0, -0xc

    if-ne v10, v0, :cond_1

    .line 541
    const/16 v0, 0x29

    invoke-static {v0, v11, v11}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 546
    :cond_1
    invoke-static {v12, v11, v11}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 551
    .end local v1    # "szResponsURL":Ljava/lang/String;
    .end local v10    # "ret":I
    :sswitch_2
    invoke-virtual {p0}, Lcom/policydm/dlagent/XFOTADlAgentHandler;->xfotaDlAgentHdlrDownloadStart()I

    goto :goto_0

    .line 556
    :sswitch_3
    invoke-virtual {p0}, Lcom/policydm/dlagent/XFOTADlAgentHandler;->xfotaDlAgentHdlrDownloadTakeOver()I

    move-result v9

    .line 557
    goto :goto_0

    .line 568
    :pswitch_1
    const-string v0, "XEVENT_DL_CONTINUE"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 569
    sparse-switch v8, :sswitch_data_1

    goto :goto_0

    .line 572
    :sswitch_4
    const-string v0, "XDL_STATE_IDLE_START"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 576
    :sswitch_5
    const-string v0, "XDL_STATE_DOWNLOAD_IN_PROGRESS"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 577
    invoke-virtual {p0}, Lcom/policydm/dlagent/XFOTADlAgentHandler;->xfotaDlAgentHdlrDownloadProgress()I

    goto :goto_0

    .line 581
    :sswitch_6
    const-string v0, "XDL_STATE_DOWNLOAD_COMPLETE"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 582
    invoke-virtual {p0}, Lcom/policydm/dlagent/XFOTADlAgentHandler;->xfotaDlAgentHdlrDownloadComplete()I

    goto :goto_0

    .line 586
    :sswitch_7
    const-string v0, "XDL_STATE_DOWNLOAD_IN_FAIL"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 587
    invoke-virtual {p0}, Lcom/policydm/dlagent/XFOTADlAgentHandler;->xfotaDlAgentHdlrDownloadComplete()I

    goto/16 :goto_0

    .line 501
    nop

    :pswitch_data_0
    .packed-switch 0x2a
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 505
    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_1
        0x14 -> :sswitch_0
        0x1e -> :sswitch_3
        0x28 -> :sswitch_3
        0xc8 -> :sswitch_2
    .end sparse-switch

    .line 569
    :sswitch_data_1
    .sparse-switch
        0xa -> :sswitch_4
        0x14 -> :sswitch_7
        0x1e -> :sswitch_5
        0x28 -> :sswitch_6
    .end sparse-switch
.end method

.method public xfotaDlAgentHdlrWriteFirmwareObject(I[B)I
    .locals 4
    .param p1, "nReceiveDataSize"    # I
    .param p2, "pRecv"    # [B

    .prologue
    .line 216
    const/4 v0, 0x0

    .line 217
    .local v0, "nFileId":I
    invoke-static {}, Lcom/policydm/db/XDB;->xdbGetFileIdFirmwareData()I

    move-result v0

    .line 219
    if-lez p1, :cond_2

    .line 221
    const/4 v1, 0x0

    .line 222
    .local v1, "nRet":I
    invoke-static {v0, p2}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpAppendDeltaFile(I[B)I

    move-result v1

    .line 224
    invoke-static {}, Lcom/policydm/dlagent/XFOTADlAgent;->xfotaDlAgentGetFileSaveIndex()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 226
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetExternalMemoryAvailable()Z

    move-result v2

    if-nez v2, :cond_0

    .line 227
    const/4 v1, 0x4

    .line 237
    .end local v1    # "nRet":I
    :goto_0
    return v1

    .line 230
    .restart local v1    # "nRet":I
    :cond_0
    if-eqz v1, :cond_1

    .line 232
    const-string v2, "FFS WRITE FAILED"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 235
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "FFS WRITE OK. dataSize = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 237
    .end local v1    # "nRet":I
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method
