.class public Lcom/policydm/agent/XDMAgentHandler;
.super Lcom/policydm/agent/XDMAgent;
.source "XDMAgentHandler.java"

# interfaces
.implements Lcom/policydm/interfaces/XUIInterface;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/policydm/agent/XDMAgent;-><init>()V

    return-void
.end method

.method public static xdmAgentStringToPcdata([C)Lcom/policydm/eng/parser/XDMParserPcdata;
    .locals 4
    .param p0, "str"    # [C

    .prologue
    .line 692
    new-instance v1, Lcom/policydm/eng/parser/XDMParserPcdata;

    invoke-direct {v1}, Lcom/policydm/eng/parser/XDMParserPcdata;-><init>()V

    .line 694
    .local v1, "o":Lcom/policydm/eng/parser/XDMParserPcdata;
    const/4 v2, 0x0

    iput v2, v1, Lcom/policydm/eng/parser/XDMParserPcdata;->type:I

    .line 695
    array-length v2, p0

    iput v2, v1, Lcom/policydm/eng/parser/XDMParserPcdata;->size:I

    .line 696
    array-length v2, p0

    new-array v2, v2, [C

    iput-object v2, v1, Lcom/policydm/eng/parser/XDMParserPcdata;->data:[C

    .line 697
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_0

    .line 698
    iget-object v2, v1, Lcom/policydm/eng/parser/XDMParserPcdata;->data:[C

    aget-char v3, p0, v0

    aput-char v3, v2, v0

    .line 697
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 699
    :cond_0
    return-object v1
.end method


# virtual methods
.method public xdmAgentGetUicOptionDRMultiChoice(Lcom/policydm/eng/core/XDMUicResult;)V
    .locals 12
    .param p1, "pData"    # Lcom/policydm/eng/core/XDMUicResult;

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 644
    invoke-virtual {p0}, Lcom/policydm/agent/XDMAgentHandler;->xdmAgentGetWorkSpace()Lcom/policydm/eng/core/XDMWorkspace;

    move-result-object v7

    .line 645
    .local v7, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    if-nez v7, :cond_1

    .line 647
    const-string v8, "ws is null"

    invoke-static {v8}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 687
    :cond_0
    return-void

    .line 651
    :cond_1
    const/4 v5, 0x0

    .line 652
    .local v5, "pUicResult":Lcom/policydm/eng/core/XDMUicResult;
    const/4 v4, 0x0

    .line 653
    .local v4, "pStartCharacter":[C
    const/4 v3, 0x0

    .line 655
    .local v3, "pEndCharacter":[C
    const/4 v1, 0x0

    .line 656
    .local v1, "ipStartCharacter":I
    const/4 v0, 0x0

    .line 657
    .local v0, "ipEndCharacter":I
    move-object v5, p1

    .line 659
    iget-object v8, v7, Lcom/policydm/eng/core/XDMWorkspace;->uicOption:Lcom/policydm/eng/core/XDMUicOption;

    iget-object v8, v8, Lcom/policydm/eng/core/XDMUicOption;->defaultResponse:Lcom/policydm/eng/core/XDMText;

    iget-object v8, v8, Lcom/policydm/eng/core/XDMText;->text:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    .line 660
    move-object v3, v4

    .line 663
    :goto_0
    array-length v8, v4

    if-ge v1, v8, :cond_0

    .line 665
    array-length v8, v3

    if-ge v0, v8, :cond_0

    .line 667
    aget-char v8, v3, v0

    const/16 v9, 0x2d

    if-eq v8, v9, :cond_2

    aget-char v8, v3, v0

    if-nez v8, :cond_3

    .line 669
    :cond_2
    new-array v6, v11, [C

    aput-char v10, v6, v10

    .line 672
    .local v6, "tmpBuf":[C
    invoke-static {v4}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v8

    sub-int v9, v0, v1

    invoke-virtual {v8, v10, v9, v6, v10}, Ljava/lang/String;->getChars(II[CI)V

    .line 673
    invoke-static {v6}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 675
    .local v2, "nIndex":I
    iget-object v8, v5, Lcom/policydm/eng/core/XDMUicResult;->MultiSelected:[I

    aput v11, v8, v2

    .line 677
    move-object v4, v3

    .line 679
    aget-char v8, v4, v1

    if-eqz v8, :cond_0

    .line 682
    add-int/lit8 v1, v1, 0x1

    .line 685
    .end local v2    # "nIndex":I
    .end local v6    # "tmpBuf":[C
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public xdmAgentHdlrAbortSession(I)V
    .locals 3
    .param p1, "nReason"    # I

    .prologue
    .line 635
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AbortReason=["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 638
    const/16 v1, 0xf3

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/policydm/eng/core/XDMMsg;->xdmCreateAbortMessage(IZ)Lcom/policydm/eng/core/XDMAbortMsgParam;

    move-result-object v0

    .line 639
    .local v0, "pAbortParam":Lcom/policydm/eng/core/XDMAbortMsgParam;
    const/16 v1, 0xe

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 640
    return-void
.end method

.method public xdmAgentHdlrContinueSession(ILjava/lang/Object;)V
    .locals 15
    .param p1, "nEvent"    # I
    .param p2, "pData"    # Ljava/lang/Object;

    .prologue
    .line 187
    const/4 v5, 0x0

    .line 189
    .local v5, "nRet":I
    const/4 v11, 0x0

    .line 190
    .local v11, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    const/4 v3, 0x0

    .line 192
    .local v3, "nAgentType":I
    const-string v12, ""

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 193
    invoke-virtual {p0}, Lcom/policydm/agent/XDMAgentHandler;->xdmAgentGetWorkSpace()Lcom/policydm/eng/core/XDMWorkspace;

    move-result-object v11

    .line 194
    if-nez v11, :cond_0

    .line 196
    const-string v12, "!ws WARNING"

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 199
    :cond_0
    sparse-switch p1, :sswitch_data_0

    .line 613
    :cond_1
    :goto_0
    return-void

    .line 203
    :sswitch_0
    invoke-static {}, Lcom/policydm/db/XDBAgentAdp;->xdbGetDmAgentType()I

    move-result v3

    .line 204
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "XEVENT_DM_START nAgentType : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 205
    const/4 v12, 0x1

    if-ne v3, v12, :cond_2

    .line 207
    invoke-virtual {p0}, Lcom/policydm/agent/XDMAgentHandler;->xdmAgnetHdlrContinueSessionFumoStart()V

    goto :goto_0

    .line 211
    :cond_2
    invoke-virtual {p0}, Lcom/policydm/agent/XDMAgentHandler;->xdmAgnetHdlrContinueSessionDmStart()V

    goto :goto_0

    .line 218
    :sswitch_1
    const-string v12, "XEVENT_DM_CONTINUE"

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 220
    if-nez v11, :cond_3

    .line 222
    const-string v12, "ws XEVENT_DM_CONTINUE WARNING"

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 225
    :cond_3
    sget-object v12, Lcom/policydm/interfaces/XDMInterface$XDMProcessingState;->XDM_PROC_NONE:Lcom/policydm/interfaces/XDMInterface$XDMProcessingState;

    iput-object v12, v11, Lcom/policydm/eng/core/XDMWorkspace;->procState:Lcom/policydm/interfaces/XDMInterface$XDMProcessingState;

    .line 229
    :try_start_0
    iget-object v12, v11, Lcom/policydm/eng/core/XDMWorkspace;->buf:Ljava/io/ByteArrayOutputStream;

    if-nez v12, :cond_4

    .line 231
    new-instance v12, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v12}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v12, v11, Lcom/policydm/eng/core/XDMWorkspace;->buf:Ljava/io/ByteArrayOutputStream;

    .line 233
    :cond_4
    iget-object v12, p0, Lcom/policydm/agent/XDMAgentHandler;->m_HttpDMAdapter:Lcom/policydm/tp/XTPAdapter;

    iget-object v13, v11, Lcom/policydm/eng/core/XDMWorkspace;->buf:Ljava/io/ByteArrayOutputStream;

    const/4 v14, 0x0

    invoke-virtual {v12, v13, v14}, Lcom/policydm/tp/XTPAdapter;->xtpAdpReceiveData(Ljava/io/ByteArrayOutputStream;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    .line 242
    :goto_1
    iget-object v12, v11, Lcom/policydm/eng/core/XDMWorkspace;->buf:Ljava/io/ByteArrayOutputStream;

    if-nez v12, :cond_5

    .line 244
    const/16 v12, 0x9

    invoke-virtual {p0, v12}, Lcom/policydm/agent/XDMAgentHandler;->xdmAgentHdlrAbortSession(I)V

    goto :goto_0

    .line 235
    :catch_0
    move-exception v0

    .line 237
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 238
    invoke-static {}, Lcom/policydm/tp/XTPNetRecvTimer;->xtpNetRecvEndTimer()V

    .line 239
    const/16 v5, -0xe

    goto :goto_1

    .line 247
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_5
    iget-object v12, p0, Lcom/policydm/agent/XDMAgentHandler;->m_HttpDMAdapter:Lcom/policydm/tp/XTPAdapter;

    invoke-virtual {v12}, Lcom/policydm/tp/XTPAdapter;->xtpAdpGetCurHMACData()Lcom/policydm/eng/core/XDMHmacData;

    move-result-object v12

    iput-object v12, v11, Lcom/policydm/eng/core/XDMWorkspace;->recvHmacData:Lcom/policydm/eng/core/XDMHmacData;

    .line 249
    const/16 v12, -0xf

    if-ne v5, v12, :cond_6

    .line 251
    const/16 v12, 0xf2

    const/4 v13, 0x0

    invoke-static {v12, v13}, Lcom/policydm/eng/core/XDMMsg;->xdmCreateAbortMessage(IZ)Lcom/policydm/eng/core/XDMAbortMsgParam;

    move-result-object v7

    .line 252
    .local v7, "pAbortParam":Lcom/policydm/eng/core/XDMAbortMsgParam;
    const/16 v12, 0xe

    const/4 v13, 0x0

    invoke-static {v12, v7, v13}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 255
    .end local v7    # "pAbortParam":Lcom/policydm/eng/core/XDMAbortMsgParam;
    :cond_6
    if-eqz v5, :cond_7

    .line 257
    const/16 v12, 0x11

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-static {v12, v13, v14}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 261
    :cond_7
    invoke-virtual {p0}, Lcom/policydm/agent/XDMAgentHandler;->xdmAgentStartMgmtSession()I

    move-result v5

    .line 262
    const/4 v12, -0x1

    if-ne v5, v12, :cond_8

    .line 264
    const-string v12, "XDM_RET_FAILED"

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 265
    const/16 v12, 0x9

    invoke-virtual {p0, v12}, Lcom/policydm/agent/XDMAgentHandler;->xdmAgentHdlrAbortSession(I)V

    goto/16 :goto_0

    .line 268
    :cond_8
    const/4 v12, -0x5

    if-ne v5, v12, :cond_9

    .line 270
    const-string v12, "XDM_RET_AUTH_MAX_ERROR"

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 271
    const/4 v12, 0x3

    invoke-virtual {p0, v12}, Lcom/policydm/agent/XDMAgentHandler;->xdmAgentHdlrAbortSession(I)V

    goto/16 :goto_0

    .line 274
    :cond_9
    const/4 v12, 0x3

    if-ne v5, v12, :cond_a

    .line 276
    const-string v12, "XDM_RET_ALERT_SESSION_ABORT"

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 277
    invoke-virtual {p0}, Lcom/policydm/agent/XDMAgentHandler;->xdmAgentHdlrDestroySession()V

    goto/16 :goto_0

    .line 280
    :cond_a
    const/4 v12, 0x5

    if-ne v5, v12, :cond_d

    .line 282
    const-string v12, "XDM_RET_FINISH"

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 283
    const-string v12, "no action command finish session"

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 285
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMOStatus()I

    move-result v6

    .line 286
    .local v6, "nStatus":I
    const/16 v12, 0x64

    if-eq v6, v12, :cond_b

    const/16 v12, 0x50

    if-eq v6, v12, :cond_b

    const/16 v12, 0xf1

    if-ne v6, v12, :cond_c

    .line 288
    :cond_b
    const-string v12, "FUMO nStatus"

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 289
    const/4 v12, 0x0

    invoke-static {v12}, Lcom/policydm/db/XDBAgentAdp;->xdbSetDmAgentType(I)V

    .line 290
    const/4 v12, 0x0

    invoke-static {v12}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOStatus(I)V

    .line 293
    :cond_c
    invoke-virtual {p0}, Lcom/policydm/agent/XDMAgentHandler;->xdmAgentHdlrDestroySession()V

    goto/16 :goto_0

    .line 295
    .end local v6    # "nStatus":I
    :cond_d
    const/4 v12, -0x4

    if-ne v5, v12, :cond_e

    .line 297
    const-string v12, "XDM_RET_PAUSED_BECAUSE_UIC_COMMAND"

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 298
    iget-object v12, v11, Lcom/policydm/eng/core/XDMWorkspace;->uicOption:Lcom/policydm/eng/core/XDMUicOption;

    invoke-virtual {p0, v12}, Lcom/policydm/agent/XDMAgentHandler;->xdmAgentHdlrlUicSendEvent(Lcom/policydm/eng/core/XDMUicOption;)V

    goto/16 :goto_0

    .line 300
    :cond_e
    const/4 v12, 0x2

    if-ne v5, v12, :cond_f

    .line 302
    const-string v12, "XDM_RET_EXEC_ALTERNATIVE"

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 303
    const-string v12, "Connect to the Contents Server"

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 304
    const/16 v12, 0xc8

    invoke-static {v12}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOStatus(I)V

    .line 305
    const/4 v12, 0x0

    const/16 v13, 0x96

    invoke-static {v12, v13}, Lcom/policydm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    .line 307
    const/16 v12, 0xf

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-static {v12, v13, v14}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 309
    :cond_f
    const/4 v12, 0x1

    if-eq v5, v12, :cond_1

    .line 313
    const/4 v12, 0x4

    if-ne v5, v12, :cond_10

    .line 315
    const/4 v12, 0x1

    invoke-static {v12}, Lcom/policydm/db/XDBProfileAdp;->xdbSetChangedProtocol(Z)V

    .line 316
    const/16 v12, 0xa

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-static {v12, v13, v14}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 318
    :cond_10
    const/16 v12, -0xc

    if-ne v5, v12, :cond_1

    .line 320
    const/16 v12, 0xb

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-static {v12, v13, v14}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 328
    :sswitch_2
    const-string v12, "XEVENT_UIC_RESPONSE"

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 329
    if-eqz v11, :cond_11

    if-nez p2, :cond_12

    .line 331
    :cond_11
    const-string v12, "XEVENT_UIC_RESPONSE WARNING!!!!!!!!!!!"

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_12
    move-object/from16 v8, p2

    .line 334
    check-cast v8, Lcom/policydm/eng/core/XDMUicResult;

    .line 336
    .local v8, "pUicResult":Lcom/policydm/eng/core/XDMUicResult;
    iget v12, v8, Lcom/policydm/eng/core/XDMUicResult;->UICType:I

    const/4 v13, 0x2

    if-ne v12, v13, :cond_19

    .line 338
    iget v12, v8, Lcom/policydm/eng/core/XDMUicResult;->result:I

    const/4 v13, 0x1

    if-ne v12, v13, :cond_14

    .line 340
    sget-object v12, Lcom/policydm/interfaces/XUICInterface$XUICFlag;->XUIC_TRUE:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    iput-object v12, v11, Lcom/policydm/eng/core/XDMWorkspace;->uicFlag:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    .line 519
    :goto_2
    iget-object v12, v11, Lcom/policydm/eng/core/XDMWorkspace;->uicOption:Lcom/policydm/eng/core/XDMUicOption;

    if-eqz v12, :cond_13

    .line 521
    iget-object v12, v11, Lcom/policydm/eng/core/XDMWorkspace;->uicOption:Lcom/policydm/eng/core/XDMUicOption;

    invoke-static {v12}, Lcom/policydm/eng/core/XDMUic;->xdmUicFreeUicOption(Lcom/policydm/eng/core/XDMUicOption;)Lcom/policydm/eng/core/XDMUicOption;

    .line 522
    const/4 v12, 0x0

    iput-object v12, v11, Lcom/policydm/eng/core/XDMWorkspace;->uicOption:Lcom/policydm/eng/core/XDMUicOption;

    .line 525
    :cond_13
    invoke-virtual {p0}, Lcom/policydm/agent/XDMAgentHandler;->xdmAgentStartMgmtSession()I

    move-result v5

    .line 527
    const/4 v12, -0x1

    if-ne v5, v12, :cond_2d

    .line 529
    const/16 v12, 0x9

    invoke-virtual {p0, v12}, Lcom/policydm/agent/XDMAgentHandler;->xdmAgentHdlrAbortSession(I)V

    goto/16 :goto_0

    .line 342
    :cond_14
    iget v12, v8, Lcom/policydm/eng/core/XDMUicResult;->result:I

    const/16 v13, 0x10

    if-ne v12, v13, :cond_18

    .line 344
    iget-object v12, v11, Lcom/policydm/eng/core/XDMWorkspace;->uicOption:Lcom/policydm/eng/core/XDMUicOption;

    iget-object v12, v12, Lcom/policydm/eng/core/XDMUicOption;->defaultResponse:Lcom/policydm/eng/core/XDMText;

    iget v12, v12, Lcom/policydm/eng/core/XDMText;->len:I

    if-eqz v12, :cond_17

    .line 346
    const-string v12, "1"

    iget-object v13, v11, Lcom/policydm/eng/core/XDMWorkspace;->uicOption:Lcom/policydm/eng/core/XDMUicOption;

    iget-object v13, v13, Lcom/policydm/eng/core/XDMUicOption;->defaultResponse:Lcom/policydm/eng/core/XDMText;

    iget-object v13, v13, Lcom/policydm/eng/core/XDMText;->text:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v12

    if-nez v12, :cond_15

    .line 348
    sget-object v12, Lcom/policydm/interfaces/XUICInterface$XUICFlag;->XUIC_TRUE:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    iput-object v12, v11, Lcom/policydm/eng/core/XDMWorkspace;->uicFlag:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    goto :goto_2

    .line 350
    :cond_15
    const-string v12, "0"

    iget-object v13, v11, Lcom/policydm/eng/core/XDMWorkspace;->uicOption:Lcom/policydm/eng/core/XDMUicOption;

    iget-object v13, v13, Lcom/policydm/eng/core/XDMUicOption;->defaultResponse:Lcom/policydm/eng/core/XDMText;

    iget-object v13, v13, Lcom/policydm/eng/core/XDMText;->text:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v12

    if-nez v12, :cond_16

    .line 353
    sget-object v12, Lcom/policydm/interfaces/XUICInterface$XUICFlag;->XUIC_TIMEOUT:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    iput-object v12, v11, Lcom/policydm/eng/core/XDMWorkspace;->uicFlag:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    goto :goto_2

    .line 358
    :cond_16
    const-string v12, "____UIC_TYPE_CONFIRM__&&__UIC_RESULT_TIMEOUT________"

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 359
    sget-object v12, Lcom/policydm/interfaces/XUICInterface$XUICFlag;->XUIC_FALSE:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    iput-object v12, v11, Lcom/policydm/eng/core/XDMWorkspace;->uicFlag:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    goto :goto_2

    .line 365
    :cond_17
    sget-object v12, Lcom/policydm/interfaces/XUICInterface$XUICFlag;->XUIC_CANCELED:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    iput-object v12, v11, Lcom/policydm/eng/core/XDMWorkspace;->uicFlag:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    goto :goto_2

    .line 372
    :cond_18
    sget-object v12, Lcom/policydm/interfaces/XUICInterface$XUICFlag;->XUIC_FALSE:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    iput-object v12, v11, Lcom/policydm/eng/core/XDMWorkspace;->uicFlag:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    goto :goto_2

    .line 375
    :cond_19
    iget v12, v8, Lcom/policydm/eng/core/XDMUicResult;->UICType:I

    const/4 v13, 0x3

    if-ne v12, v13, :cond_1e

    .line 377
    const/4 v2, 0x0

    .line 378
    .local v2, "item":Lcom/policydm/eng/parser/XDMParserItem;
    const/4 v1, 0x0

    .local v1, "h":Lcom/policydm/eng/core/XDMList;
    const/4 v10, 0x0

    .line 380
    .local v10, "t":Lcom/policydm/eng/core/XDMList;
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "______ UIC_TYPE_INPUT _______INPUT text :"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, v8, Lcom/policydm/eng/core/XDMUicResult;->text:Lcom/policydm/eng/core/XDMText;

    iget-object v13, v13, Lcom/policydm/eng/core/XDMText;->text:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " UIC Result :"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget v13, v8, Lcom/policydm/eng/core/XDMUicResult;->result:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 382
    iget v12, v8, Lcom/policydm/eng/core/XDMUicResult;->result:I

    if-nez v12, :cond_1a

    .line 384
    new-instance v2, Lcom/policydm/eng/parser/XDMParserItem;

    .end local v2    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    invoke-direct {v2}, Lcom/policydm/eng/parser/XDMParserItem;-><init>()V

    .line 386
    .restart local v2    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    iget-object v12, v8, Lcom/policydm/eng/core/XDMUicResult;->text:Lcom/policydm/eng/core/XDMText;

    iget-object v12, v12, Lcom/policydm/eng/core/XDMText;->text:Ljava/lang/String;

    invoke-virtual {v12}, Ljava/lang/String;->toCharArray()[C

    move-result-object v12

    invoke-static {v12}, Lcom/policydm/agent/XDMAgentHandler;->xdmAgentStringToPcdata([C)Lcom/policydm/eng/parser/XDMParserPcdata;

    move-result-object v12

    iput-object v12, v2, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    .line 387
    invoke-static {v1, v10, v2}, Lcom/policydm/eng/core/XDMList;->xdmListAppend(Lcom/policydm/eng/core/XDMList;Lcom/policydm/eng/core/XDMList;Ljava/lang/Object;)Lcom/policydm/eng/core/XDMList;

    move-result-object v1

    .line 388
    iput-object v1, v11, Lcom/policydm/eng/core/XDMWorkspace;->uicData:Lcom/policydm/eng/core/XDMList;

    .line 389
    sget-object v12, Lcom/policydm/interfaces/XUICInterface$XUICFlag;->XUIC_TRUE:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    iput-object v12, v11, Lcom/policydm/eng/core/XDMWorkspace;->uicFlag:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    goto/16 :goto_2

    .line 392
    :cond_1a
    iget v12, v8, Lcom/policydm/eng/core/XDMUicResult;->result:I

    const/4 v13, 0x2

    if-ne v12, v13, :cond_1b

    .line 394
    sget-object v12, Lcom/policydm/interfaces/XUICInterface$XUICFlag;->XUIC_CANCELED:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    iput-object v12, v11, Lcom/policydm/eng/core/XDMWorkspace;->uicFlag:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    goto/16 :goto_2

    .line 396
    :cond_1b
    iget v12, v8, Lcom/policydm/eng/core/XDMUicResult;->result:I

    const/16 v13, 0x10

    if-ne v12, v13, :cond_1d

    .line 398
    iget-object v12, v11, Lcom/policydm/eng/core/XDMWorkspace;->uicOption:Lcom/policydm/eng/core/XDMUicOption;

    iget-object v12, v12, Lcom/policydm/eng/core/XDMUicOption;->defaultResponse:Lcom/policydm/eng/core/XDMText;

    iget v12, v12, Lcom/policydm/eng/core/XDMText;->len:I

    if-eqz v12, :cond_1c

    const-string v12, "0"

    iget-object v13, v11, Lcom/policydm/eng/core/XDMWorkspace;->uicOption:Lcom/policydm/eng/core/XDMUicOption;

    iget-object v13, v13, Lcom/policydm/eng/core/XDMUicOption;->defaultResponse:Lcom/policydm/eng/core/XDMText;

    iget-object v13, v13, Lcom/policydm/eng/core/XDMText;->text:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v12

    if-eqz v12, :cond_1c

    .line 400
    new-instance v2, Lcom/policydm/eng/parser/XDMParserItem;

    .end local v2    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    invoke-direct {v2}, Lcom/policydm/eng/parser/XDMParserItem;-><init>()V

    .line 401
    .restart local v2    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    iget-object v12, v8, Lcom/policydm/eng/core/XDMUicResult;->text:Lcom/policydm/eng/core/XDMText;

    iget-object v13, v11, Lcom/policydm/eng/core/XDMWorkspace;->uicOption:Lcom/policydm/eng/core/XDMUicOption;

    iget-object v13, v13, Lcom/policydm/eng/core/XDMUicOption;->defaultResponse:Lcom/policydm/eng/core/XDMText;

    iget-object v13, v13, Lcom/policydm/eng/core/XDMText;->text:Ljava/lang/String;

    iput-object v13, v12, Lcom/policydm/eng/core/XDMText;->text:Ljava/lang/String;

    .line 402
    iget-object v12, v8, Lcom/policydm/eng/core/XDMUicResult;->text:Lcom/policydm/eng/core/XDMText;

    iget-object v12, v12, Lcom/policydm/eng/core/XDMText;->text:Ljava/lang/String;

    invoke-virtual {v12}, Ljava/lang/String;->toCharArray()[C

    move-result-object v12

    invoke-static {v12}, Lcom/policydm/agent/XDMAgentHandler;->xdmAgentStringToPcdata([C)Lcom/policydm/eng/parser/XDMParserPcdata;

    move-result-object v12

    iput-object v12, v2, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    .line 403
    invoke-static {v1, v10, v2}, Lcom/policydm/eng/core/XDMList;->xdmListAppend(Lcom/policydm/eng/core/XDMList;Lcom/policydm/eng/core/XDMList;Ljava/lang/Object;)Lcom/policydm/eng/core/XDMList;

    move-result-object v1

    .line 404
    iput-object v1, v11, Lcom/policydm/eng/core/XDMWorkspace;->uicData:Lcom/policydm/eng/core/XDMList;

    .line 405
    sget-object v12, Lcom/policydm/interfaces/XUICInterface$XUICFlag;->XUIC_TRUE:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    iput-object v12, v11, Lcom/policydm/eng/core/XDMWorkspace;->uicFlag:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    goto/16 :goto_2

    .line 410
    :cond_1c
    sget-object v12, Lcom/policydm/interfaces/XUICInterface$XUICFlag;->XUIC_TIMEOUT:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    iput-object v12, v11, Lcom/policydm/eng/core/XDMWorkspace;->uicFlag:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    goto/16 :goto_2

    .line 416
    :cond_1d
    sget-object v12, Lcom/policydm/interfaces/XUICInterface$XUICFlag;->XUIC_FALSE:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    iput-object v12, v11, Lcom/policydm/eng/core/XDMWorkspace;->uicFlag:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    goto/16 :goto_2

    .line 419
    .end local v1    # "h":Lcom/policydm/eng/core/XDMList;
    .end local v2    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    .end local v10    # "t":Lcom/policydm/eng/core/XDMList;
    :cond_1e
    iget v12, v8, Lcom/policydm/eng/core/XDMUicResult;->UICType:I

    const/4 v13, 0x4

    if-eq v12, v13, :cond_1f

    iget v12, v8, Lcom/policydm/eng/core/XDMUicResult;->UICType:I

    const/4 v13, 0x5

    if-ne v12, v13, :cond_2c

    .line 421
    :cond_1f
    const/4 v1, 0x0

    .restart local v1    # "h":Lcom/policydm/eng/core/XDMList;
    const/4 v10, 0x0

    .line 423
    .restart local v10    # "t":Lcom/policydm/eng/core/XDMList;
    iget v12, v8, Lcom/policydm/eng/core/XDMUicResult;->result:I

    const/4 v13, 0x4

    if-ne v12, v13, :cond_21

    .line 425
    new-instance v2, Lcom/policydm/eng/parser/XDMParserItem;

    invoke-direct {v2}, Lcom/policydm/eng/parser/XDMParserItem;-><init>()V

    .line 426
    .restart local v2    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    const-string v9, ""

    .line 428
    .local v9, "szDataText":Ljava/lang/String;
    iget v12, v8, Lcom/policydm/eng/core/XDMUicResult;->SingleSelected:I

    if-lez v12, :cond_20

    .line 429
    iget v12, v8, Lcom/policydm/eng/core/XDMUicResult;->SingleSelected:I

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    .line 431
    :cond_20
    invoke-virtual {v9}, Ljava/lang/String;->toCharArray()[C

    move-result-object v12

    invoke-static {v12}, Lcom/policydm/agent/XDMAgentHandler;->xdmAgentStringToPcdata([C)Lcom/policydm/eng/parser/XDMParserPcdata;

    move-result-object v12

    iput-object v12, v2, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    .line 432
    invoke-static {v1, v10, v2}, Lcom/policydm/eng/core/XDMList;->xdmListAppend(Lcom/policydm/eng/core/XDMList;Lcom/policydm/eng/core/XDMList;Ljava/lang/Object;)Lcom/policydm/eng/core/XDMList;

    move-result-object v1

    .line 433
    iput-object v1, v11, Lcom/policydm/eng/core/XDMWorkspace;->uicData:Lcom/policydm/eng/core/XDMList;

    .line 434
    sget-object v12, Lcom/policydm/interfaces/XUICInterface$XUICFlag;->XUIC_TRUE:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    iput-object v12, v11, Lcom/policydm/eng/core/XDMWorkspace;->uicFlag:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    goto/16 :goto_2

    .line 436
    .end local v2    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    .end local v9    # "szDataText":Ljava/lang/String;
    :cond_21
    iget v12, v8, Lcom/policydm/eng/core/XDMUicResult;->result:I

    const/4 v13, 0x5

    if-ne v12, v13, :cond_25

    .line 438
    const/4 v4, 0x0

    .line 440
    .local v4, "nCount":I
    const/4 v4, 0x0

    :goto_3
    iget v12, v8, Lcom/policydm/eng/core/XDMUicResult;->MenuNumbers:I

    if-ge v4, v12, :cond_23

    .line 442
    iget-object v12, v8, Lcom/policydm/eng/core/XDMUicResult;->MultiSelected:[I

    aget v12, v12, v4

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 443
    iget-object v12, v8, Lcom/policydm/eng/core/XDMUicResult;->MultiSelected:[I

    aget v12, v12, v4

    const/4 v13, 0x1

    if-ne v12, v13, :cond_22

    .line 445
    new-instance v2, Lcom/policydm/eng/parser/XDMParserItem;

    invoke-direct {v2}, Lcom/policydm/eng/parser/XDMParserItem;-><init>()V

    .line 446
    .restart local v2    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    const-string v9, ""

    .line 448
    .restart local v9    # "szDataText":Ljava/lang/String;
    add-int/lit8 v12, v4, 0x1

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    .line 449
    invoke-virtual {v9}, Ljava/lang/String;->toCharArray()[C

    move-result-object v12

    invoke-static {v12}, Lcom/policydm/agent/XDMAgentHandler;->xdmAgentStringToPcdata([C)Lcom/policydm/eng/parser/XDMParserPcdata;

    move-result-object v12

    iput-object v12, v2, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    .line 450
    invoke-static {v1, v10, v2}, Lcom/policydm/eng/core/XDMList;->xdmListAppend(Lcom/policydm/eng/core/XDMList;Lcom/policydm/eng/core/XDMList;Ljava/lang/Object;)Lcom/policydm/eng/core/XDMList;

    move-result-object v1

    .line 440
    .end local v2    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    .end local v9    # "szDataText":Ljava/lang/String;
    :cond_22
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 453
    :cond_23
    if-nez v1, :cond_24

    .line 455
    new-instance v2, Lcom/policydm/eng/parser/XDMParserItem;

    invoke-direct {v2}, Lcom/policydm/eng/parser/XDMParserItem;-><init>()V

    .line 456
    .restart local v2    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    invoke-static {v1, v10, v2}, Lcom/policydm/eng/core/XDMList;->xdmListAppend(Lcom/policydm/eng/core/XDMList;Lcom/policydm/eng/core/XDMList;Ljava/lang/Object;)Lcom/policydm/eng/core/XDMList;

    move-result-object v1

    .line 459
    .end local v2    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    :cond_24
    iput-object v1, v11, Lcom/policydm/eng/core/XDMWorkspace;->uicData:Lcom/policydm/eng/core/XDMList;

    .line 460
    sget-object v12, Lcom/policydm/interfaces/XUICInterface$XUICFlag;->XUIC_TRUE:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    iput-object v12, v11, Lcom/policydm/eng/core/XDMWorkspace;->uicFlag:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    goto/16 :goto_2

    .line 462
    .end local v4    # "nCount":I
    :cond_25
    iget v12, v8, Lcom/policydm/eng/core/XDMUicResult;->result:I

    const/4 v13, 0x2

    if-ne v12, v13, :cond_26

    .line 464
    sget-object v12, Lcom/policydm/interfaces/XUICInterface$XUICFlag;->XUIC_CANCELED:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    iput-object v12, v11, Lcom/policydm/eng/core/XDMWorkspace;->uicFlag:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    goto/16 :goto_2

    .line 466
    :cond_26
    iget v12, v8, Lcom/policydm/eng/core/XDMUicResult;->result:I

    const/16 v13, 0x10

    if-ne v12, v13, :cond_2b

    .line 468
    iget-object v12, v11, Lcom/policydm/eng/core/XDMWorkspace;->uicOption:Lcom/policydm/eng/core/XDMUicOption;

    iget-object v12, v12, Lcom/policydm/eng/core/XDMUicOption;->defaultResponse:Lcom/policydm/eng/core/XDMText;

    iget v12, v12, Lcom/policydm/eng/core/XDMText;->len:I

    if-eqz v12, :cond_2a

    const-string v12, "0"

    iget-object v13, v11, Lcom/policydm/eng/core/XDMWorkspace;->uicOption:Lcom/policydm/eng/core/XDMUicOption;

    iget-object v13, v13, Lcom/policydm/eng/core/XDMUicOption;->defaultResponse:Lcom/policydm/eng/core/XDMText;

    iget-object v13, v13, Lcom/policydm/eng/core/XDMText;->text:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v12

    if-eqz v12, :cond_2a

    .line 470
    iget v12, v8, Lcom/policydm/eng/core/XDMUicResult;->UICType:I

    const/4 v13, 0x4

    if-ne v12, v13, :cond_27

    .line 472
    new-instance v2, Lcom/policydm/eng/parser/XDMParserItem;

    invoke-direct {v2}, Lcom/policydm/eng/parser/XDMParserItem;-><init>()V

    .line 474
    .restart local v2    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    iget-object v12, v11, Lcom/policydm/eng/core/XDMWorkspace;->uicOption:Lcom/policydm/eng/core/XDMUicOption;

    iget-object v12, v12, Lcom/policydm/eng/core/XDMUicOption;->defaultResponse:Lcom/policydm/eng/core/XDMText;

    iget-object v12, v12, Lcom/policydm/eng/core/XDMText;->text:Ljava/lang/String;

    invoke-virtual {v12}, Ljava/lang/String;->toCharArray()[C

    move-result-object v12

    invoke-static {v12}, Lcom/policydm/agent/XDMAgentHandler;->xdmAgentStringToPcdata([C)Lcom/policydm/eng/parser/XDMParserPcdata;

    move-result-object v12

    iput-object v12, v2, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    .line 475
    invoke-static {v1, v10, v2}, Lcom/policydm/eng/core/XDMList;->xdmListAppend(Lcom/policydm/eng/core/XDMList;Lcom/policydm/eng/core/XDMList;Ljava/lang/Object;)Lcom/policydm/eng/core/XDMList;

    move-result-object v1

    .line 476
    iput-object v1, v11, Lcom/policydm/eng/core/XDMWorkspace;->uicData:Lcom/policydm/eng/core/XDMList;

    .line 477
    sget-object v12, Lcom/policydm/interfaces/XUICInterface$XUICFlag;->XUIC_TRUE:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    iput-object v12, v11, Lcom/policydm/eng/core/XDMWorkspace;->uicFlag:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    goto/16 :goto_2

    .line 482
    .end local v2    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    :cond_27
    const/4 v4, 0x0

    .line 484
    .restart local v4    # "nCount":I
    invoke-virtual {p0, v8}, Lcom/policydm/agent/XDMAgentHandler;->xdmAgentGetUicOptionDRMultiChoice(Lcom/policydm/eng/core/XDMUicResult;)V

    .line 486
    const/4 v4, 0x0

    :goto_4
    iget v12, v8, Lcom/policydm/eng/core/XDMUicResult;->MenuNumbers:I

    if-ge v4, v12, :cond_29

    .line 488
    iget-object v12, v8, Lcom/policydm/eng/core/XDMUicResult;->MultiSelected:[I

    aget v12, v12, v4

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 490
    iget-object v12, v8, Lcom/policydm/eng/core/XDMUicResult;->MultiSelected:[I

    aget v12, v12, v4

    const/4 v13, 0x1

    if-ne v12, v13, :cond_28

    .line 492
    new-instance v2, Lcom/policydm/eng/parser/XDMParserItem;

    invoke-direct {v2}, Lcom/policydm/eng/parser/XDMParserItem;-><init>()V

    .line 493
    .restart local v2    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    const-string v9, ""

    .line 494
    .restart local v9    # "szDataText":Ljava/lang/String;
    add-int/lit8 v12, v4, 0x1

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    .line 495
    invoke-virtual {v9}, Ljava/lang/String;->toCharArray()[C

    move-result-object v12

    invoke-static {v12}, Lcom/policydm/agent/XDMAgentHandler;->xdmAgentStringToPcdata([C)Lcom/policydm/eng/parser/XDMParserPcdata;

    move-result-object v12

    iput-object v12, v2, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    .line 496
    invoke-static {v1, v10, v2}, Lcom/policydm/eng/core/XDMList;->xdmListAppend(Lcom/policydm/eng/core/XDMList;Lcom/policydm/eng/core/XDMList;Ljava/lang/Object;)Lcom/policydm/eng/core/XDMList;

    move-result-object v1

    .line 486
    .end local v2    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    .end local v9    # "szDataText":Ljava/lang/String;
    :cond_28
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 499
    :cond_29
    iput-object v1, v11, Lcom/policydm/eng/core/XDMWorkspace;->uicData:Lcom/policydm/eng/core/XDMList;

    .line 500
    sget-object v12, Lcom/policydm/interfaces/XUICInterface$XUICFlag;->XUIC_TRUE:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    iput-object v12, v11, Lcom/policydm/eng/core/XDMWorkspace;->uicFlag:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    goto/16 :goto_2

    .line 506
    .end local v4    # "nCount":I
    :cond_2a
    sget-object v12, Lcom/policydm/interfaces/XUICInterface$XUICFlag;->XUIC_TIMEOUT:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    iput-object v12, v11, Lcom/policydm/eng/core/XDMWorkspace;->uicFlag:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    goto/16 :goto_2

    .line 511
    :cond_2b
    sget-object v12, Lcom/policydm/interfaces/XUICInterface$XUICFlag;->XUIC_FALSE:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    iput-object v12, v11, Lcom/policydm/eng/core/XDMWorkspace;->uicFlag:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    goto/16 :goto_2

    .line 516
    .end local v1    # "h":Lcom/policydm/eng/core/XDMList;
    .end local v10    # "t":Lcom/policydm/eng/core/XDMList;
    :cond_2c
    sget-object v12, Lcom/policydm/interfaces/XUICInterface$XUICFlag;->XUIC_NONE:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    iput-object v12, v11, Lcom/policydm/eng/core/XDMWorkspace;->uicFlag:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    goto/16 :goto_2

    .line 532
    :cond_2d
    const/4 v12, 0x3

    if-ne v5, v12, :cond_2e

    .line 534
    invoke-virtual {p0}, Lcom/policydm/agent/XDMAgentHandler;->xdmAgentHdlrDestroySession()V

    goto/16 :goto_0

    .line 537
    :cond_2e
    const/4 v12, 0x5

    if-ne v5, v12, :cond_2f

    .line 539
    const-string v12, "no action command finish session"

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 540
    invoke-virtual {p0}, Lcom/policydm/agent/XDMAgentHandler;->xdmAgentHdlrDestroySession()V

    goto/16 :goto_0

    .line 543
    :cond_2f
    const/4 v12, -0x5

    if-eq v5, v12, :cond_1

    .line 547
    const/4 v12, 0x4

    if-ne v5, v12, :cond_30

    .line 549
    const/4 v12, 0x1

    invoke-static {v12}, Lcom/policydm/db/XDBProfileAdp;->xdbSetChangedProtocol(Z)V

    .line 550
    const/16 v12, 0xa

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-static {v12, v13, v14}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 552
    :cond_30
    const/16 v12, -0xc

    if-ne v5, v12, :cond_1

    .line 554
    const/16 v12, 0xb

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-static {v12, v13, v14}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 560
    .end local v8    # "pUicResult":Lcom/policydm/eng/core/XDMUicResult;
    :sswitch_3
    invoke-static {}, Lcom/policydm/db/XDBAgentAdp;->xdbGetDmAgentType()I

    move-result v3

    .line 561
    const/4 v12, 0x1

    if-ne v3, v12, :cond_31

    .line 563
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMOStatus()I

    move-result v6

    .line 570
    .restart local v6    # "nStatus":I
    :goto_5
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "nStatus ["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "]"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 572
    if-nez v6, :cond_34

    .line 574
    invoke-static {}, Lcom/policydm/agent/XDMAgentHandler;->xdmAgentGetPendingStatus()Z

    move-result v12

    if-eqz v12, :cond_32

    .line 576
    const-string v12, "XDM_TASK_RETRY"

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 577
    invoke-static {}, Lcom/policydm/agent/XDMAgentHandler;->xdmAgentClose()I

    goto/16 :goto_0

    .line 567
    .end local v6    # "nStatus":I
    :cond_31
    const/4 v6, 0x0

    .restart local v6    # "nStatus":I
    goto :goto_5

    .line 581
    :cond_32
    invoke-static {}, Lcom/policydm/db/XDBProfileAdp;->xdbGetChangedProtocol()Z

    move-result v12

    if-eqz v12, :cond_33

    .line 583
    invoke-static {}, Lcom/policydm/db/XDB;->xdbSetBackUpServerUrl()V

    .line 590
    :goto_6
    const/4 v12, 0x0

    const/16 v13, 0x82

    invoke-static {v12, v13}, Lcom/policydm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    .line 592
    invoke-static {}, Lcom/policydm/noti/XNOTIAdapter;->xnotiPushAdpResetSessionSaveState()V

    .line 593
    const/4 v12, 0x0

    invoke-static {v12}, Lcom/policydm/db/XDBProfileListAdp;->xdbSetNotiEvent(I)V

    .line 596
    invoke-static {}, Lcom/policydm/noti/XNOTIAdapter;->xnotiPushAdpHandleNotiQueue()V

    .line 606
    :goto_7
    invoke-static {}, Lcom/policydm/agent/XDMAgentHandler;->xdmAgentClose()I

    goto/16 :goto_0

    .line 587
    :cond_33
    invoke-static {}, Lcom/policydm/tp/XTPAdapter;->xtpAdpHttpCookieClear()V

    goto :goto_6

    .line 598
    :cond_34
    const/16 v12, 0xa

    if-ne v6, v12, :cond_35

    .line 600
    const-string v12, "Case XEVENT_DM_FINISH XDL_STATE_IDLE_START"

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_7

    .line 604
    :cond_35
    const-string v12, "Case XEVENT_DM_FINISH BUT not FINISH STATUS"

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_7

    .line 199
    :sswitch_data_0
    .sparse-switch
        0xc -> :sswitch_0
        0xd -> :sswitch_1
        0xf -> :sswitch_3
        0x15 -> :sswitch_2
    .end sparse-switch
.end method

.method public xdmAgentHdlrDestroySession()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 629
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/policydm/agent/XDMAgentHandler;->xdmAgentSetSyncMode(I)Z

    .line 630
    const/16 v0, 0xf

    invoke-static {v0, v1, v1}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 631
    return-void
.end method

.method public xdmAgentHdlrlUicSendEvent(Lcom/policydm/eng/core/XDMUicOption;)V
    .locals 3
    .param p1, "pUicOption"    # Lcom/policydm/eng/core/XDMUicOption;

    .prologue
    .line 617
    const/4 v0, 0x0

    .line 619
    .local v0, "pUicOptionDest":Lcom/policydm/eng/core/XDMUicOption;
    const-string v1, ""

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 621
    invoke-static {}, Lcom/policydm/eng/core/XDMUic;->xdmUicCreateUicOption()Lcom/policydm/eng/core/XDMUicOption;

    move-result-object v0

    .line 622
    invoke-static {v0, p1}, Lcom/policydm/eng/core/XDMUic;->xdmUicCopyUicOption(Ljava/lang/Object;Ljava/lang/Object;)Lcom/policydm/eng/core/XDMUicOption;

    move-result-object v0

    .line 624
    const/16 v1, 0x14

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 625
    return-void
.end method

.method public xdmAgnetHdlrContinueSessionDmStart()V
    .locals 5

    .prologue
    const/16 v3, 0x9

    const/4 v4, 0x0

    .line 35
    const-string v2, ""

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 41
    invoke-virtual {p0}, Lcom/policydm/agent/XDMAgentHandler;->xdmAgentGetWorkSpace()Lcom/policydm/eng/core/XDMWorkspace;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-static {}, Lcom/policydm/db/XDBProfileAdp;->xdbGetChangedProtocol()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 43
    const-string v2, "xdbGetChangedProtocol, do not create new package"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 63
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/policydm/agent/XDMAgentHandler;->m_HttpDMAdapter:Lcom/policydm/tp/XTPAdapter;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/policydm/tp/XTPAdapter;->xtpAdpOpen(I)I
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 72
    .local v1, "nRet":I
    :goto_0
    if-eqz v1, :cond_4

    .line 74
    const/16 v2, 0xb

    invoke-static {v2, v4, v4}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 88
    :cond_1
    :goto_1
    return-void

    .line 47
    .end local v1    # "nRet":I
    :cond_2
    invoke-virtual {p0}, Lcom/policydm/agent/XDMAgentHandler;->xdmAgentStartSession()I

    move-result v1

    .line 48
    .restart local v1    # "nRet":I
    if-eqz v1, :cond_3

    .line 50
    invoke-virtual {p0, v3}, Lcom/policydm/agent/XDMAgentHandler;->xdmAgentHdlrAbortSession(I)V

    goto :goto_1

    .line 54
    :cond_3
    invoke-virtual {p0}, Lcom/policydm/agent/XDMAgentHandler;->xdmAgentCreatePackage()I

    move-result v2

    if-eqz v2, :cond_0

    .line 56
    invoke-virtual {p0, v3}, Lcom/policydm/agent/XDMAgentHandler;->xdmAgentHdlrAbortSession(I)V

    goto :goto_1

    .line 65
    .end local v1    # "nRet":I
    :catch_0
    move-exception v0

    .line 67
    .local v0, "e":Ljava/net/SocketTimeoutException;
    invoke-virtual {v0}, Ljava/net/SocketTimeoutException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 68
    invoke-static {}, Lcom/policydm/tp/XTPNetConnectTimer;->xtpNetConnEndTimer()V

    .line 69
    const/16 v1, -0xc

    .restart local v1    # "nRet":I
    goto :goto_0

    .line 78
    .end local v0    # "e":Ljava/net/SocketTimeoutException;
    :cond_4
    invoke-virtual {p0}, Lcom/policydm/agent/XDMAgentHandler;->xdmAgentSendPackage()I

    move-result v1

    .line 79
    if-nez v1, :cond_5

    .line 80
    const/16 v2, 0x66

    invoke-static {v4, v2}, Lcom/policydm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    .line 82
    :cond_5
    const/4 v2, 0x4

    if-ne v1, v2, :cond_1

    .line 84
    const/4 v2, 0x1

    invoke-static {v2}, Lcom/policydm/db/XDBProfileAdp;->xdbSetChangedProtocol(Z)V

    .line 85
    const/16 v2, 0xf

    invoke-static {v2, v4, v4}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 86
    const/16 v2, 0xa

    invoke-static {v2, v4, v4}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public xdmAgnetHdlrContinueSessionFumoStart()V
    .locals 10

    .prologue
    const/16 v9, 0x66

    const/16 v8, 0xb

    const/16 v7, 0x9

    const/4 v6, 0x0

    .line 93
    const/4 v1, 0x0

    .line 96
    .local v1, "nFileId":I
    const-string v4, ""

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 98
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMOStatus()I

    move-result v3

    .line 99
    .local v3, "nStatus":I
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "nStatus ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 101
    const/16 v4, 0x64

    if-eq v3, v4, :cond_0

    const/16 v4, 0x50

    if-eq v3, v4, :cond_0

    const/16 v4, 0xf1

    if-ne v3, v4, :cond_4

    .line 103
    :cond_0
    invoke-static {}, Lcom/policydm/db/XDB;->xdbGetFileIdFirmwareData()I

    move-result v1

    .line 104
    invoke-static {v1}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpDeleteFile(I)I

    .line 109
    :try_start_0
    iget-object v4, p0, Lcom/policydm/agent/XDMAgentHandler;->m_HttpDMAdapter:Lcom/policydm/tp/XTPAdapter;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/policydm/tp/XTPAdapter;->xtpAdpOpen(I)I
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 118
    .local v2, "nRet":I
    :goto_0
    if-eqz v2, :cond_2

    .line 120
    invoke-static {v8, v6, v6}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 183
    :cond_1
    :goto_1
    return-void

    .line 111
    .end local v2    # "nRet":I
    :catch_0
    move-exception v0

    .line 113
    .local v0, "e":Ljava/net/SocketTimeoutException;
    invoke-virtual {v0}, Ljava/net/SocketTimeoutException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 114
    invoke-static {}, Lcom/policydm/tp/XTPNetConnectTimer;->xtpNetConnEndTimer()V

    .line 115
    const/16 v2, -0xc

    .restart local v2    # "nRet":I
    goto :goto_0

    .line 124
    .end local v0    # "e":Ljava/net/SocketTimeoutException;
    :cond_2
    invoke-virtual {p0}, Lcom/policydm/agent/XDMAgentHandler;->xdmAgentSendDMReport()I

    move-result v2

    .line 125
    if-nez v2, :cond_3

    .line 126
    invoke-static {v6, v9}, Lcom/policydm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    .line 177
    :cond_3
    :goto_2
    const/4 v4, 0x4

    if-ne v2, v4, :cond_1

    .line 179
    const/4 v4, 0x1

    invoke-static {v4}, Lcom/policydm/db/XDBProfileAdp;->xdbSetChangedProtocol(Z)V

    .line 180
    const/16 v4, 0xf

    invoke-static {v4, v6, v6}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 181
    const/16 v4, 0xa

    invoke-static {v4, v6, v6}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    .line 134
    .end local v2    # "nRet":I
    :cond_4
    invoke-virtual {p0}, Lcom/policydm/agent/XDMAgentHandler;->xdmAgentGetWorkSpace()Lcom/policydm/eng/core/XDMWorkspace;

    move-result-object v4

    if-eqz v4, :cond_6

    invoke-static {}, Lcom/policydm/db/XDBProfileAdp;->xdbGetChangedProtocol()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 136
    const-string v4, "xdbGetChangedProtocol, do not create new package"

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 157
    :cond_5
    :try_start_1
    iget-object v4, p0, Lcom/policydm/agent/XDMAgentHandler;->m_HttpDMAdapter:Lcom/policydm/tp/XTPAdapter;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/policydm/tp/XTPAdapter;->xtpAdpOpen(I)I
    :try_end_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v2

    .line 166
    .restart local v2    # "nRet":I
    :goto_3
    if-eqz v2, :cond_8

    .line 168
    invoke-static {v8, v6, v6}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    .line 140
    .end local v2    # "nRet":I
    :cond_6
    invoke-virtual {p0}, Lcom/policydm/agent/XDMAgentHandler;->xdmAgentStartSession()I

    move-result v2

    .line 141
    .restart local v2    # "nRet":I
    if-eqz v2, :cond_7

    .line 143
    invoke-virtual {p0, v7}, Lcom/policydm/agent/XDMAgentHandler;->xdmAgentHdlrAbortSession(I)V

    goto :goto_1

    .line 147
    :cond_7
    invoke-virtual {p0}, Lcom/policydm/agent/XDMAgentHandler;->xdmAgentCreatePackage()I

    move-result v4

    if-eqz v4, :cond_5

    .line 149
    invoke-virtual {p0, v7}, Lcom/policydm/agent/XDMAgentHandler;->xdmAgentHdlrAbortSession(I)V

    goto :goto_1

    .line 159
    .end local v2    # "nRet":I
    :catch_1
    move-exception v0

    .line 161
    .restart local v0    # "e":Ljava/net/SocketTimeoutException;
    invoke-virtual {v0}, Ljava/net/SocketTimeoutException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 162
    invoke-static {}, Lcom/policydm/tp/XTPNetConnectTimer;->xtpNetConnEndTimer()V

    .line 163
    const/16 v2, -0xc

    .restart local v2    # "nRet":I
    goto :goto_3

    .line 172
    .end local v0    # "e":Ljava/net/SocketTimeoutException;
    :cond_8
    invoke-virtual {p0}, Lcom/policydm/agent/XDMAgentHandler;->xdmAgentSendPackage()I

    move-result v2

    .line 173
    if-nez v2, :cond_3

    .line 174
    invoke-static {v6, v9}, Lcom/policydm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    goto :goto_2
.end method
