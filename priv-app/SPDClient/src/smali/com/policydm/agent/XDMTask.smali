.class public Lcom/policydm/agent/XDMTask;
.super Ljava/lang/Object;
.source "XDMTask.java"

# interfaces
.implements Lcom/policydm/interfaces/XDLInterface;
.implements Lcom/policydm/interfaces/XDMInterface;
.implements Lcom/policydm/interfaces/XEventInterface;
.implements Lcom/policydm/interfaces/XSPDInterface;
.implements Lcom/policydm/interfaces/XTPInterface;
.implements Lcom/policydm/interfaces/XUIInterface;
.implements Ljava/lang/Runnable;


# static fields
.field public static g_IsDMInitialized:Z

.field public static g_IsSyncTaskInit:Z

.field public static g_hDmTask:Landroid/os/Handler;


# instance fields
.field public m_DlAgent:Lcom/policydm/dlagent/XFOTADlAgent;

.field public m_DmAgent:Lcom/policydm/agent/XDMAgent;

.field private m_bDBInit:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 53
    sput-boolean v0, Lcom/policydm/agent/XDMTask;->g_IsSyncTaskInit:Z

    .line 54
    sput-boolean v0, Lcom/policydm/agent/XDMTask;->g_IsDMInitialized:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object v0, p0, Lcom/policydm/agent/XDMTask;->m_DmAgent:Lcom/policydm/agent/XDMAgent;

    .line 57
    iput-object v0, p0, Lcom/policydm/agent/XDMTask;->m_DlAgent:Lcom/policydm/dlagent/XFOTADlAgent;

    .line 59
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/policydm/agent/XDMTask;->m_bDBInit:Z

    .line 63
    sget-boolean v0, Lcom/policydm/agent/XDMTask;->g_IsSyncTaskInit:Z

    if-nez v0, :cond_0

    .line 65
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 67
    :cond_0
    return-void
.end method

.method private xdmAgentTaskDBInit()Z
    .locals 3

    .prologue
    .line 96
    const/4 v0, 0x0

    .line 98
    .local v0, "bRtn":Z
    invoke-static {}, Lcom/policydm/adapter/XDMDevinfAdapter;->xdmDevAdpVerifyDeviceID()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 102
    :try_start_0
    invoke-static {}, Lcom/policydm/db/XDB;->xdbDMffs_Init()V

    .line 103
    const/4 v0, 0x1

    .line 104
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/policydm/agent/XDMTask;->m_bDBInit:Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 115
    :goto_0
    return v0

    .line 106
    :catch_0
    move-exception v1

    .line 108
    .local v1, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 113
    .end local v1    # "e":Ljava/lang/NullPointerException;
    :cond_0
    const-string v2, "DB Init Fail!!"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdmAgentTaskGetDmInitStatus()Z
    .locals 1

    .prologue
    .line 140
    sget-boolean v0, Lcom/policydm/agent/XDMTask;->g_IsDMInitialized:Z

    return v0
.end method

.method public static xdmAgentTaskSetDmInitStatus(Z)V
    .locals 0
    .param p0, "bInitState"    # Z

    .prologue
    .line 145
    sput-boolean p0, Lcom/policydm/agent/XDMTask;->g_IsDMInitialized:Z

    .line 146
    return-void
.end method


# virtual methods
.method public run()V
    .locals 1

    .prologue
    .line 71
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 73
    new-instance v0, Lcom/policydm/agent/XDMTask$1;

    invoke-direct {v0, p0}, Lcom/policydm/agent/XDMTask$1;-><init>(Lcom/policydm/agent/XDMTask;)V

    sput-object v0, Lcom/policydm/agent/XDMTask;->g_hDmTask:Landroid/os/Handler;

    .line 91
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 92
    return-void
.end method

.method public xdmAgentDlXXXFail()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 775
    const-string v0, "===xdmAgentDlXXXFail==="

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 776
    iget-object v0, p0, Lcom/policydm/agent/XDMTask;->m_DlAgent:Lcom/policydm/dlagent/XFOTADlAgent;

    if-eqz v0, :cond_0

    .line 778
    iget-object v0, p0, Lcom/policydm/agent/XDMTask;->m_DlAgent:Lcom/policydm/dlagent/XFOTADlAgent;

    invoke-virtual {v0, v1}, Lcom/policydm/dlagent/XFOTADlAgent;->xfotaDlTpClose(I)V

    .line 779
    iget-object v0, p0, Lcom/policydm/agent/XDMTask;->m_DlAgent:Lcom/policydm/dlagent/XFOTADlAgent;

    invoke-virtual {v0, v1}, Lcom/policydm/dlagent/XFOTADlAgent;->xfotaDlTpCloseNetWork(I)V

    .line 781
    :cond_0
    return-void
.end method

.method public xdmAgentDmXXXFail()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 765
    const-string v0, "===xdmAgentDmXXXFail==="

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 766
    iget-object v0, p0, Lcom/policydm/agent/XDMTask;->m_DmAgent:Lcom/policydm/agent/XDMAgent;

    if-eqz v0, :cond_0

    .line 768
    iget-object v0, p0, Lcom/policydm/agent/XDMTask;->m_DmAgent:Lcom/policydm/agent/XDMAgent;

    invoke-virtual {v0, v1}, Lcom/policydm/agent/XDMAgent;->xdmAgentTpClose(I)V

    .line 769
    iget-object v0, p0, Lcom/policydm/agent/XDMTask;->m_DmAgent:Lcom/policydm/agent/XDMAgent;

    invoke-virtual {v0, v1}, Lcom/policydm/agent/XDMAgent;->xdmAgentTpCloseNetwork(I)V

    .line 771
    :cond_0
    return-void
.end method

.method public xdmAgentTaskHandler(Landroid/os/Message;)Z
    .locals 33
    .param p1, "msg"    # Landroid/os/Message;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 150
    const/4 v8, 0x0

    .line 152
    .local v8, "msgItem":Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v30, v0

    if-nez v30, :cond_0

    .line 153
    const/16 v30, 0x1

    .line 760
    :goto_0
    return v30

    .line 155
    :cond_0
    move-object/from16 v0, p1

    iget-object v8, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .end local v8    # "msgItem":Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;
    check-cast v8, Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;

    .line 156
    .restart local v8    # "msgItem":Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;
    iget v0, v8, Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;->type:I

    move/from16 v30, v0

    packed-switch v30, :pswitch_data_0

    .line 759
    :cond_1
    :goto_1
    :pswitch_0
    const/16 p1, 0x0

    .line 760
    const/16 v30, 0x0

    goto :goto_0

    .line 159
    :pswitch_1
    const-string v30, "XEVENT_OS_INITIALIZED"

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 160
    const/16 v30, 0x0

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMAgent;->xdmAgentTpSetRetryCount(I)V

    .line 161
    const/16 v30, 0x0

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetSyncMode(I)Z

    .line 162
    invoke-virtual/range {p0 .. p0}, Lcom/policydm/agent/XDMTask;->xdmAgentTaskInit()V

    .line 163
    const/16 v30, 0x1

    const/16 v31, 0x0

    const/16 v32, 0x0

    invoke-static/range {v30 .. v32}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    .line 167
    :pswitch_2
    const-string v30, "XEVENT_DM_INIT"

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 168
    invoke-static {}, Lcom/policydm/adapter/XDMInitAdapter;->xdmInitAdpGetNetStatus()I

    move-result v30

    const/16 v31, -0x1

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_2

    .line 170
    const-string v30, "Network Status is not ready. DM Not Initialized"

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 171
    const/16 v30, 0x0

    sput-boolean v30, Lcom/policydm/XDMApplication;->g_bIsInitializing:Z

    goto :goto_1

    .line 175
    :cond_2
    sget-boolean v30, Lcom/policydm/agent/XDMTask;->g_IsDMInitialized:Z

    if-nez v30, :cond_1

    .line 177
    const/4 v5, 0x1

    .line 179
    .local v5, "bRet":Z
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/policydm/agent/XDMTask;->m_bDBInit:Z

    move/from16 v30, v0

    if-nez v30, :cond_3

    .line 180
    invoke-direct/range {p0 .. p0}, Lcom/policydm/agent/XDMTask;->xdmAgentTaskDBInit()Z

    move-result v5

    .line 182
    :cond_3
    if-nez v5, :cond_4

    .line 184
    const/16 v30, 0x0

    sput-boolean v30, Lcom/policydm/XDMApplication;->g_bIsInitializing:Z

    goto :goto_1

    .line 188
    :cond_4
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetNotibarState()I

    move-result v11

    .line 189
    .local v11, "nNotibar":I
    invoke-static {v11}, Lcom/policydm/ui/XUINotificationManager;->xuiSetIndicator(I)V

    .line 191
    invoke-static {}, Lcom/policydm/adapter/XDMInitAdapter;->xdmInitAdpEXTInit()Z

    move-result v30

    if-nez v30, :cond_5

    .line 193
    const-string v30, "XEVENT_DM_INIT : Not Initialized"

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 194
    const/16 v30, 0x0

    sput-boolean v30, Lcom/policydm/agent/XDMTask;->g_IsDMInitialized:Z

    .line 195
    const/16 v30, 0x0

    sput-boolean v30, Lcom/policydm/XDMApplication;->g_bIsInitializing:Z

    goto :goto_1

    .line 199
    :cond_5
    const-string v30, "XEVENT_DM_INIT : Initialized"

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 200
    const/16 v30, 0x1

    sput-boolean v30, Lcom/policydm/agent/XDMTask;->g_IsDMInitialized:Z

    .line 201
    const/16 v30, 0x0

    sput-boolean v30, Lcom/policydm/XDMApplication;->g_bIsInitializing:Z

    .line 203
    const-wide/16 v30, 0xbb8

    invoke-static/range {v30 .. v31}, Ljava/lang/Thread;->sleep(J)V

    .line 204
    invoke-static {}, Lcom/policydm/XDMBroadcastReceiver;->xdmGetResumeCase()I

    move-result v30

    invoke-static/range {v30 .. v30}, Lcom/policydm/XDMBroadcastReceiver;->xdmExecResumeCase(I)V

    goto/16 :goto_1

    .line 210
    .end local v5    # "bRet":Z
    .end local v11    # "nNotibar":I
    :pswitch_3
    const-string v30, "XEVENT_DM_CONNECT"

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 211
    sget-boolean v30, Lcom/policydm/agent/XDMTask;->g_IsDMInitialized:Z

    if-eqz v30, :cond_b

    .line 213
    const/16 v30, 0x0

    invoke-static/range {v30 .. v30}, Lcom/policydm/db/XDB;->xdbSetWaitWifiFlag(Z)V

    .line 215
    invoke-static {}, Lcom/policydm/dlagent/XFOTADlAgent;->xfotaDlAgentIsStatus()Z

    move-result v28

    .line 216
    .local v28, "rc":Z
    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "xfotaDlAgentIsStatus :"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 218
    if-nez v28, :cond_9

    .line 220
    invoke-static {}, Lcom/policydm/db/XDBProfileAdp;->xdbGetChangedProtocol()Z

    move-result v30

    if-eqz v30, :cond_6

    .line 222
    const-string v30, "XEVENT_DM_CONNECT : Changed Protocol"

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 235
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/agent/XDMTask;->m_DmAgent:Lcom/policydm/agent/XDMAgent;

    move-object/from16 v30, v0

    const/16 v31, 0x0

    invoke-virtual/range {v30 .. v31}, Lcom/policydm/agent/XDMAgent;->xdmAgentTpInit(I)I

    move-result v12

    .line 236
    .local v12, "nRet":I
    if-eqz v12, :cond_8

    .line 238
    const-string v30, "xdmAgentTpInit fail!!"

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 239
    const/16 v30, 0xb

    const/16 v31, 0x0

    const/16 v32, 0x0

    invoke-static/range {v30 .. v32}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 226
    .end local v12    # "nRet":I
    :cond_6
    invoke-static {}, Lcom/policydm/db/XDBProfileAdp;->xdbGetProfileInfo()Lcom/policydm/db/XDBProfileInfo;

    move-result-object v20

    .line 227
    .local v20, "pNvInfo":Lcom/policydm/db/XDBProfileInfo;
    if-nez v20, :cond_7

    .line 228
    new-instance v20, Lcom/policydm/db/XDBProfileInfo;

    .end local v20    # "pNvInfo":Lcom/policydm/db/XDBProfileInfo;
    invoke-direct/range {v20 .. v20}, Lcom/policydm/db/XDBProfileInfo;-><init>()V

    .line 229
    .restart local v20    # "pNvInfo":Lcom/policydm/db/XDBProfileInfo;
    :cond_7
    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/policydm/db/XDBProfileInfo;->ServerUrl_Org:Ljava/lang/String;

    move-object/from16 v30, v0

    invoke-static/range {v30 .. v30}, Lcom/policydm/db/XDBProfileAdp;->xdbSetServerUrl(Ljava/lang/String;)V

    .line 230
    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/policydm/db/XDBProfileInfo;->ServerIP_Org:Ljava/lang/String;

    move-object/from16 v30, v0

    invoke-static/range {v30 .. v30}, Lcom/policydm/db/XDBProfileAdp;->xdbSetServerAddress(Ljava/lang/String;)V

    .line 231
    move-object/from16 v0, v20

    iget v0, v0, Lcom/policydm/db/XDBProfileInfo;->ServerPort_Org:I

    move/from16 v30, v0

    invoke-static/range {v30 .. v30}, Lcom/policydm/db/XDBProfileAdp;->xdbSetServerPort(I)V

    .line 232
    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/policydm/db/XDBProfileInfo;->Protocol_Org:Ljava/lang/String;

    move-object/from16 v30, v0

    invoke-static/range {v30 .. v30}, Lcom/policydm/db/XDBProfileAdp;->xdbSetServerProtocol(Ljava/lang/String;)V

    goto :goto_2

    .line 243
    .end local v20    # "pNvInfo":Lcom/policydm/db/XDBProfileInfo;
    .restart local v12    # "nRet":I
    :cond_8
    const/16 v30, 0x12

    const/16 v31, 0x0

    const/16 v32, 0x0

    invoke-static/range {v30 .. v32}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 248
    .end local v12    # "nRet":I
    :cond_9
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMOStatus()I

    move-result v9

    .line 249
    .local v9, "nFumoStatus":I
    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "nFumoStatus : "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 251
    const/16 v30, 0xc8

    move/from16 v0, v30

    if-ne v9, v0, :cond_a

    .line 253
    const/16 v30, 0x0

    const/16 v31, 0x96

    invoke-static/range {v30 .. v31}, Lcom/policydm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    goto/16 :goto_1

    .line 257
    :cond_a
    const/16 v30, 0x28

    const/16 v31, 0x0

    const/16 v32, 0x0

    invoke-static/range {v30 .. v32}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 263
    .end local v9    # "nFumoStatus":I
    .end local v28    # "rc":Z
    :cond_b
    const-string v30, "XUI_DM_NOT_INIT"

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 264
    const/16 v30, 0x0

    const/16 v31, 0x68

    invoke-static/range {v30 .. v31}, Lcom/policydm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    goto/16 :goto_1

    .line 272
    :pswitch_4
    const-string v30, "XEVENT_DM_CONNECTFAIL/SENDFAIL/RECEIVEFAIL"

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 273
    invoke-virtual/range {p0 .. p0}, Lcom/policydm/agent/XDMTask;->xdmAgentDmXXXFail()V

    .line 274
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/agent/XDMTask;->m_DmAgent:Lcom/policydm/agent/XDMAgent;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/policydm/agent/XDMAgent;->xdmAgentTpCheckRetry()Z

    move-result v4

    .line 275
    .local v4, "bRc":Z
    if-eqz v4, :cond_c

    .line 277
    const-wide/16 v30, 0xdac

    invoke-static/range {v30 .. v31}, Ljava/lang/Thread;->sleep(J)V

    .line 278
    const/16 v30, 0xa

    const/16 v31, 0x0

    const/16 v32, 0x0

    invoke-static/range {v30 .. v32}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 282
    :cond_c
    invoke-static {}, Lcom/policydm/db/XDBProfileAdp;->xdbGetChangedProtocol()Z

    move-result v30

    if-eqz v30, :cond_d

    .line 284
    invoke-static {}, Lcom/policydm/db/XDB;->xdbSetBackUpServerUrl()V

    .line 286
    :cond_d
    const/16 v30, 0x0

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetSyncMode(I)Z

    .line 287
    const/16 v30, 0x0

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMAgent;->xdmAgentTpSetRetryCount(I)V

    .line 288
    const/16 v30, 0x0

    const/16 v31, 0x78

    invoke-static/range {v30 .. v31}, Lcom/policydm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    .line 290
    const/16 v30, 0x1

    sput v30, Lcom/policydm/XDMApplication;->g_nResumeStatus:I

    goto/16 :goto_1

    .line 295
    .end local v4    # "bRc":Z
    :pswitch_5
    const-string v30, "XEVENT_DM_START"

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 296
    const/16 v30, 0x1

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetSyncMode(I)Z

    .line 297
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/agent/XDMTask;->m_DmAgent:Lcom/policydm/agent/XDMAgent;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/policydm/agent/XDMAgent;->m_AgentHandler:Lcom/policydm/agent/XDMAgentHandler;

    move-object/from16 v30, v0

    const/16 v31, 0xc

    const/16 v32, 0x0

    invoke-virtual/range {v30 .. v32}, Lcom/policydm/agent/XDMAgentHandler;->xdmAgentHdlrContinueSession(ILjava/lang/Object;)V

    goto/16 :goto_1

    .line 301
    :pswitch_6
    const-string v30, "XEVENT_DM_CONTINUE"

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 302
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/agent/XDMTask;->m_DmAgent:Lcom/policydm/agent/XDMAgent;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/policydm/agent/XDMAgent;->m_AgentHandler:Lcom/policydm/agent/XDMAgentHandler;

    move-object/from16 v30, v0

    const/16 v31, 0xd

    const/16 v32, 0x0

    invoke-virtual/range {v30 .. v32}, Lcom/policydm/agent/XDMAgentHandler;->xdmAgentHdlrContinueSession(ILjava/lang/Object;)V

    goto/16 :goto_1

    .line 306
    :pswitch_7
    const-string v30, "XEVENT_DM_ABORT"

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 307
    iget-object v0, v8, Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;->param:Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;

    move-object/from16 v30, v0

    if-nez v30, :cond_e

    .line 309
    const-string v30, "param is null"

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 310
    const/16 v30, 0x0

    goto/16 :goto_0

    .line 313
    :cond_e
    invoke-static {}, Lcom/policydm/db/XDBProfileAdp;->xdbGetChangedProtocol()Z

    move-result v30

    if-eqz v30, :cond_f

    .line 315
    invoke-static {}, Lcom/policydm/db/XDB;->xdbSetBackUpServerUrl()V

    .line 318
    :cond_f
    iget-object v0, v8, Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;->param:Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;->param:Ljava/lang/Object;

    move-object/from16 v17, v0

    check-cast v17, Lcom/policydm/eng/core/XDMAbortMsgParam;

    .line 319
    .local v17, "pAbortParam":Lcom/policydm/eng/core/XDMAbortMsgParam;
    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "pAbortParam.abortCode:"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v17

    iget v0, v0, Lcom/policydm/eng/core/XDMAbortMsgParam;->abortCode:I

    move/from16 v31, v0

    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 321
    move-object/from16 v0, v17

    iget v0, v0, Lcom/policydm/eng/core/XDMAbortMsgParam;->abortCode:I

    move/from16 v30, v0

    const/16 v31, 0xf2

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_11

    .line 323
    const-string v30, "XEVENT_ABORT_HTTP_ERROR, not implement..."

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 324
    invoke-static {}, Lcom/policydm/agent/XDMAgentHandler;->xdmAgentClose()I

    .line 325
    invoke-virtual/range {p0 .. p0}, Lcom/policydm/agent/XDMTask;->xdmAgentDmXXXFail()V

    .line 327
    invoke-static {}, Lcom/policydm/db/XDBAgentAdp;->xdbGetDmAgentType()I

    move-result v30

    const/16 v31, 0x1

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_10

    .line 329
    const/16 v30, 0x0

    invoke-static/range {v30 .. v30}, Lcom/policydm/db/XDBAgentAdp;->xdbSetDmAgentType(I)V

    .line 330
    const/16 v30, 0x0

    invoke-static/range {v30 .. v30}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOStatus(I)V

    .line 340
    :cond_10
    :goto_3
    invoke-static {}, Lcom/policydm/tp/XTPAdapter;->xtpAdpHttpCookieClear()V

    .line 341
    const/16 v30, 0x0

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMAgent;->xdmAgentTpSetRetryCount(I)V

    .line 342
    move-object/from16 v0, v17

    iget v0, v0, Lcom/policydm/eng/core/XDMAbortMsgParam;->abortCode:I

    move/from16 v30, v0

    .line 345
    const/16 v30, 0x0

    const/16 v31, 0x7b

    invoke-static/range {v30 .. v31}, Lcom/policydm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    goto/16 :goto_1

    .line 335
    :cond_11
    const-string v30, "XEVENT_ABORT_HTTP_ERROR : ELSE"

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 336
    invoke-static {}, Lcom/policydm/agent/XDMAgentHandler;->xdmAgentClose()I

    .line 337
    invoke-virtual/range {p0 .. p0}, Lcom/policydm/agent/XDMTask;->xdmAgentDmXXXFail()V

    goto :goto_3

    .line 351
    .end local v17    # "pAbortParam":Lcom/policydm/eng/core/XDMAbortMsgParam;
    :pswitch_8
    const-string v30, "XEVENT_DM_FINISH"

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 352
    invoke-virtual/range {p0 .. p0}, Lcom/policydm/agent/XDMTask;->xdmAgentDmXXXFail()V

    .line 353
    const/16 v30, 0x0

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMAgent;->xdmAgentTpSetRetryCount(I)V

    .line 354
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/agent/XDMTask;->m_DmAgent:Lcom/policydm/agent/XDMAgent;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/policydm/agent/XDMAgent;->m_AgentHandler:Lcom/policydm/agent/XDMAgentHandler;

    move-object/from16 v30, v0

    const/16 v31, 0xf

    const/16 v32, 0x0

    invoke-virtual/range {v30 .. v32}, Lcom/policydm/agent/XDMAgentHandler;->xdmAgentHdlrContinueSession(ILjava/lang/Object;)V

    .line 355
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMOStatus()I

    move-result v30

    if-nez v30, :cond_1

    .line 357
    const/16 v30, 0x0

    invoke-static/range {v30 .. v30}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOInitiatedType(I)V

    goto/16 :goto_1

    .line 362
    :pswitch_9
    const-string v30, "XEVENT_DM_TCPIP_OPEN"

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 363
    const/16 v30, 0xc

    const/16 v31, 0x0

    const/16 v32, 0x0

    invoke-static/range {v30 .. v32}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 372
    :pswitch_a
    const-string v30, "XEVENT_DL_CONNECT"

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 373
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/agent/XDMTask;->m_DlAgent:Lcom/policydm/dlagent/XFOTADlAgent;

    move-object/from16 v30, v0

    const/16 v31, 0x1

    invoke-virtual/range {v30 .. v31}, Lcom/policydm/dlagent/XFOTADlAgent;->xfotaDlTpInit(I)I

    move-result v12

    .line 374
    .restart local v12    # "nRet":I
    const/16 v30, -0x11

    move/from16 v0, v30

    if-ne v12, v0, :cond_12

    .line 376
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMOStatus()I

    move-result v14

    .line 377
    .local v14, "nStatus":I
    const/16 v30, 0xa

    move/from16 v0, v30

    if-ne v14, v0, :cond_1

    .line 379
    const/16 v30, 0x2d

    const/16 v31, 0x0

    const/16 v32, 0x0

    invoke-static/range {v30 .. v32}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 380
    const/16 v30, 0xf1

    invoke-static/range {v30 .. v30}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOStatus(I)V

    .line 381
    const-string v30, "401"

    invoke-static/range {v30 .. v30}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOResultCode(Ljava/lang/String;)V

    .line 383
    const/16 v30, 0x3f

    const/16 v31, 0x0

    const/16 v32, 0x0

    invoke-static/range {v30 .. v32}, Lcom/policydm/eng/core/XDMMsg;->xdmSendRCMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 386
    .end local v14    # "nStatus":I
    :cond_12
    if-eqz v12, :cond_13

    .line 388
    const-string v30, "xfotaDlTpInit fail!"

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 389
    const/16 v30, 0x29

    const/16 v31, 0x0

    const/16 v32, 0x0

    invoke-static/range {v30 .. v32}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 393
    :cond_13
    const/16 v30, 0x1

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetSyncMode(I)Z

    .line 394
    const/16 v30, 0x30

    const/16 v31, 0x0

    const/16 v32, 0x0

    invoke-static/range {v30 .. v32}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 400
    .end local v12    # "nRet":I
    :pswitch_b
    const-string v30, "XEVENT_DL_CONNECTFAIL"

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 401
    invoke-virtual/range {p0 .. p0}, Lcom/policydm/agent/XDMTask;->xdmAgentDlXXXFail()V

    .line 402
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/agent/XDMTask;->m_DlAgent:Lcom/policydm/dlagent/XFOTADlAgent;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/policydm/dlagent/XFOTADlAgent;->xfotaDlTpCheckRetry()Z

    move-result v4

    .line 403
    .restart local v4    # "bRc":Z
    if-eqz v4, :cond_14

    .line 405
    const-wide/16 v30, 0xdac

    invoke-static/range {v30 .. v31}, Ljava/lang/Thread;->sleep(J)V

    .line 406
    const/16 v30, 0x28

    const/16 v31, 0x0

    const/16 v32, 0x0

    invoke-static/range {v30 .. v32}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 410
    :cond_14
    const/16 v30, 0x0

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetSyncMode(I)Z

    .line 411
    const/16 v30, 0x0

    invoke-static/range {v30 .. v30}, Lcom/policydm/dlagent/XFOTADlAgent;->xfotaDlTpSetRetryCount(I)V

    .line 412
    const/16 v30, 0x0

    const/16 v31, 0x99

    invoke-static/range {v30 .. v31}, Lcom/policydm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    .line 414
    const/16 v30, 0x2

    sput v30, Lcom/policydm/XDMApplication;->g_nResumeStatus:I

    .line 417
    invoke-static {}, Lcom/policydm/dlagent/XFOTADlAgent;->xfotaDlTpGetRetryFailCount()I

    move-result v13

    .line 418
    .local v13, "nRetryFailCnt":I
    const/16 v30, 0x1

    move/from16 v0, v30

    if-ge v13, v0, :cond_15

    .line 420
    add-int/lit8 v13, v13, 0x1

    .line 421
    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "XEVENT_DL_CONNECTFAIL nRetryFailCnt="

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 422
    invoke-static {v13}, Lcom/policydm/dlagent/XFOTADlAgent;->xfotaDlTpSetRetryFailCount(I)V

    goto/16 :goto_1

    .line 427
    :cond_15
    const-string v30, "XEVENT_DL_CONNECTFAIL nRetryFailCntMax OVER. Session reset"

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 428
    const/4 v13, 0x0

    .line 429
    invoke-static {v13}, Lcom/policydm/dlagent/XFOTADlAgent;->xfotaDlTpSetRetryFailCount(I)V

    .line 431
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMOStatus()I

    move-result v15

    .line 432
    .local v15, "nfumostatus":I
    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "Fumo Status = "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 433
    if-eqz v15, :cond_1

    .line 435
    const-string v30, "send generic alert for fail to download package"

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 438
    const/16 v30, 0xf1

    invoke-static/range {v30 .. v30}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOStatus(I)V

    .line 439
    const-string v30, "402"

    invoke-static/range {v30 .. v30}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOResultCode(Ljava/lang/String;)V

    .line 441
    const/16 v30, 0x3f

    const/16 v31, 0x0

    const/16 v32, 0x0

    invoke-static/range {v30 .. v32}, Lcom/policydm/eng/core/XDMMsg;->xdmSendRCMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 450
    .end local v4    # "bRc":Z
    .end local v13    # "nRetryFailCnt":I
    .end local v15    # "nfumostatus":I
    :pswitch_c
    const-string v30, "XEVENT_DL_SENDFAIL/RECEIVEFAIL"

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 451
    invoke-virtual/range {p0 .. p0}, Lcom/policydm/agent/XDMTask;->xdmAgentDlXXXFail()V

    .line 452
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/agent/XDMTask;->m_DlAgent:Lcom/policydm/dlagent/XFOTADlAgent;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/policydm/dlagent/XFOTADlAgent;->xfotaDlTpCheckRetry()Z

    move-result v4

    .line 453
    .restart local v4    # "bRc":Z
    if-eqz v4, :cond_16

    .line 455
    const/16 v30, 0x28

    const/16 v31, 0x0

    const/16 v32, 0x0

    invoke-static/range {v30 .. v32}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 459
    :cond_16
    const/16 v30, 0x0

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetSyncMode(I)Z

    .line 460
    const/16 v30, 0x0

    invoke-static/range {v30 .. v30}, Lcom/policydm/dlagent/XFOTADlAgent;->xfotaDlTpSetRetryCount(I)V

    .line 461
    const/16 v30, 0x0

    const/16 v31, 0x99

    invoke-static/range {v30 .. v31}, Lcom/policydm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    .line 463
    const/16 v30, 0x2

    sput v30, Lcom/policydm/XDMApplication;->g_nResumeStatus:I

    .line 466
    invoke-static {}, Lcom/policydm/dlagent/XFOTADlAgent;->xfotaDlTpGetRetryFailCount()I

    move-result v13

    .line 467
    .restart local v13    # "nRetryFailCnt":I
    const/16 v30, 0x1

    move/from16 v0, v30

    if-ge v13, v0, :cond_17

    .line 469
    add-int/lit8 v13, v13, 0x1

    .line 470
    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "XEVENT_DL_CONNECTFAIL nRetryFailCnt="

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 471
    invoke-static {v13}, Lcom/policydm/dlagent/XFOTADlAgent;->xfotaDlTpSetRetryFailCount(I)V

    goto/16 :goto_1

    .line 476
    :cond_17
    const-string v30, "XEVENT_DL_CONNECTFAIL nRetryFailCntMax OVER. Session reset"

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 477
    const/4 v13, 0x0

    .line 478
    invoke-static {v13}, Lcom/policydm/dlagent/XFOTADlAgent;->xfotaDlTpSetRetryFailCount(I)V

    .line 480
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMOStatus()I

    move-result v15

    .line 481
    .restart local v15    # "nfumostatus":I
    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "Fumo Status = "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 482
    if-eqz v15, :cond_1

    .line 484
    const-string v30, "send generic alert for fail to download package"

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 487
    const/16 v30, 0xf1

    invoke-static/range {v30 .. v30}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOStatus(I)V

    .line 488
    const-string v30, "404"

    invoke-static/range {v30 .. v30}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOResultCode(Ljava/lang/String;)V

    .line 490
    const/16 v30, 0x3f

    const/16 v31, 0x0

    const/16 v32, 0x0

    invoke-static/range {v30 .. v32}, Lcom/policydm/eng/core/XDMMsg;->xdmSendRCMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 498
    .end local v4    # "bRc":Z
    .end local v13    # "nRetryFailCnt":I
    .end local v15    # "nfumostatus":I
    :pswitch_d
    const-string v30, "XEVENT_DL_ABORT"

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 499
    iget-object v0, v8, Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;->param:Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;

    move-object/from16 v30, v0

    if-nez v30, :cond_18

    .line 500
    const/16 v30, 0x0

    goto/16 :goto_0

    .line 502
    :cond_18
    iget-object v0, v8, Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;->param:Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;->param:Ljava/lang/Object;

    move-object/from16 v18, v0

    check-cast v18, Lcom/policydm/eng/core/XDMAbortMsgParam;

    .line 503
    .local v18, "pDLAbortParam":Lcom/policydm/eng/core/XDMAbortMsgParam;
    move-object/from16 v0, v18

    iget v0, v0, Lcom/policydm/eng/core/XDMAbortMsgParam;->abortCode:I

    move/from16 v30, v0

    const/16 v31, 0xf2

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_1

    .line 505
    const-string v30, "XEVENT_ABORT_HTTP_ERROR, not implement..."

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 506
    invoke-static {}, Lcom/policydm/agent/XDMAgentHandler;->xdmAgentClose()I

    .line 507
    invoke-virtual/range {p0 .. p0}, Lcom/policydm/agent/XDMTask;->xdmAgentDlXXXFail()V

    .line 509
    const/16 v30, 0x0

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetSyncMode(I)Z

    .line 510
    const/16 v30, 0x0

    invoke-static/range {v30 .. v30}, Lcom/policydm/dlagent/XFOTADlAgent;->xfotaDlTpSetRetryCount(I)V

    .line 512
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMOStatus()I

    move-result v15

    .line 513
    .restart local v15    # "nfumostatus":I
    const/16 v30, 0x28

    move/from16 v0, v30

    if-ne v15, v0, :cond_19

    .line 515
    const/16 v30, 0x0

    const/16 v31, 0x97

    invoke-static/range {v30 .. v31}, Lcom/policydm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    goto/16 :goto_1

    .line 521
    :cond_19
    const-string v30, "send generic alert for fail to download package"

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 523
    const/16 v30, 0xf1

    invoke-static/range {v30 .. v30}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOStatus(I)V

    .line 524
    const-string v30, "403"

    invoke-static/range {v30 .. v30}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOResultCode(Ljava/lang/String;)V

    .line 525
    const/16 v30, 0x0

    const/16 v31, 0x99

    invoke-static/range {v30 .. v31}, Lcom/policydm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    .line 527
    const/16 v30, 0x3f

    const/16 v31, 0x0

    const/16 v32, 0x0

    invoke-static/range {v30 .. v32}, Lcom/policydm/eng/core/XDMMsg;->xdmSendRCMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 534
    .end local v15    # "nfumostatus":I
    .end local v18    # "pDLAbortParam":Lcom/policydm/eng/core/XDMAbortMsgParam;
    :pswitch_e
    const-string v30, "XEVENT_DL_TCPIP_OPEN"

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 535
    const/16 v29, 0x0

    .line 538
    .local v29, "ret":I
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/agent/XDMTask;->m_DlAgent:Lcom/policydm/dlagent/XFOTADlAgent;

    move-object/from16 v30, v0

    sget-object v30, Lcom/policydm/dlagent/XFOTADlAgent;->m_HttpDLAdapter:Lcom/policydm/tp/XTPAdapter;

    const/16 v31, 0x1

    invoke-virtual/range {v30 .. v31}, Lcom/policydm/tp/XTPAdapter;->xtpAdpOpen(I)I
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v29

    .line 546
    :goto_4
    if-nez v29, :cond_1a

    .line 547
    const/16 v30, 0x2a

    const/16 v31, 0x0

    const/16 v32, 0x0

    invoke-static/range {v30 .. v32}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 540
    :catch_0
    move-exception v6

    .line 542
    .local v6, "e":Ljava/net/SocketTimeoutException;
    invoke-virtual {v6}, Ljava/net/SocketTimeoutException;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 543
    invoke-static {}, Lcom/policydm/tp/XTPNetConnectTimer;->xtpNetConnEndTimer()V

    .line 544
    const/16 v29, -0xc

    goto :goto_4

    .line 549
    .end local v6    # "e":Ljava/net/SocketTimeoutException;
    :cond_1a
    const/16 v30, 0x29

    const/16 v31, 0x0

    const/16 v32, 0x0

    invoke-static/range {v30 .. v32}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 553
    .end local v29    # "ret":I
    :pswitch_f
    const-string v30, "XEVENT_DL_START"

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 554
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/agent/XDMTask;->m_DlAgent:Lcom/policydm/dlagent/XFOTADlAgent;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/policydm/dlagent/XFOTADlAgent;->m_DlAgentHandler:Lcom/policydm/dlagent/XFOTADlAgentHandler;

    move-object/from16 v30, v0

    const/16 v31, 0x2a

    invoke-virtual/range {v30 .. v31}, Lcom/policydm/dlagent/XFOTADlAgentHandler;->xfotaDlAgentHdlrStartOMADLAgent(I)I

    goto/16 :goto_1

    .line 558
    :pswitch_10
    const-string v30, "XEVENT_DL_CONTINUE!!"

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 559
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/agent/XDMTask;->m_DlAgent:Lcom/policydm/dlagent/XFOTADlAgent;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/policydm/dlagent/XFOTADlAgent;->m_DlAgentHandler:Lcom/policydm/dlagent/XFOTADlAgentHandler;

    move-object/from16 v30, v0

    const/16 v31, 0x2b

    invoke-virtual/range {v30 .. v31}, Lcom/policydm/dlagent/XFOTADlAgentHandler;->xfotaDlAgentHdlrStartOMADLAgent(I)I

    goto/16 :goto_1

    .line 563
    :pswitch_11
    const-string v30, "XEVENT_DL_FINISH"

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 564
    invoke-virtual/range {p0 .. p0}, Lcom/policydm/agent/XDMTask;->xdmAgentDlXXXFail()V

    .line 565
    const/16 v30, 0x0

    invoke-static/range {v30 .. v30}, Lcom/policydm/dlagent/XFOTADlAgent;->xfotaDlTpSetRetryCount(I)V

    .line 566
    const/16 v30, 0x0

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetSyncMode(I)Z

    .line 571
    const/16 v30, 0x0

    invoke-static/range {v30 .. v30}, Lcom/policydm/db/XDBProfileListAdp;->xdbSetNotiReSyncMode(I)Ljava/lang/Boolean;

    goto/16 :goto_1

    .line 576
    :pswitch_12
    const-string v30, "XEVENT_UIC_REQUEST"

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 577
    iget-object v0, v8, Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;->param:Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;->param:Ljava/lang/Object;

    move-object/from16 v23, v0

    check-cast v23, Lcom/policydm/eng/core/XDMUicOption;

    .line 578
    .local v23, "pSrcUICOption":Lcom/policydm/eng/core/XDMUicOption;
    invoke-static {}, Lcom/policydm/eng/core/XDMUic;->xdmUicGetResultKeep()Lcom/policydm/eng/core/XDMUicResult;

    move-result-object v26

    .line 579
    .local v26, "pUicResultKeep":Lcom/policydm/eng/core/XDMUicResult;
    invoke-static {}, Lcom/policydm/eng/core/XDMUic;->xdmUicCreateUicOption()Lcom/policydm/eng/core/XDMUicOption;

    move-result-object v24

    .line 580
    .local v24, "pUicOption":Lcom/policydm/eng/core/XDMUicOption;
    move-object/from16 v0, v24

    move-object/from16 v1, v23

    invoke-static {v0, v1}, Lcom/policydm/eng/core/XDMUic;->xdmUicCopyUicOption(Ljava/lang/Object;Ljava/lang/Object;)Lcom/policydm/eng/core/XDMUicOption;

    move-result-object v24

    .line 581
    const/16 v30, 0x6e

    const/16 v31, 0x0

    move/from16 v0, v30

    move-object/from16 v1, v24

    move-object/from16 v2, v31

    invoke-static {v0, v1, v2}, Lcom/policydm/eng/core/XDMMsg;->xdmSendUIMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 582
    invoke-static/range {v26 .. v26}, Lcom/policydm/eng/core/XDMUic;->xdmUicFreeResult(Lcom/policydm/eng/core/XDMUicResult;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 586
    .end local v23    # "pSrcUICOption":Lcom/policydm/eng/core/XDMUicOption;
    .end local v24    # "pUicOption":Lcom/policydm/eng/core/XDMUicOption;
    .end local v26    # "pUicResultKeep":Lcom/policydm/eng/core/XDMUicResult;
    :pswitch_13
    const-string v30, "XEVENT_UIC_RESPONSE"

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 587
    iget-object v0, v8, Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;->param:Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;->param:Ljava/lang/Object;

    move-object/from16 v25, v0

    check-cast v25, Lcom/policydm/eng/core/XDMUicResult;

    .line 588
    .local v25, "pUicResult":Lcom/policydm/eng/core/XDMUicResult;
    const/16 v30, 0x1

    move-object/from16 v0, v25

    move/from16 v1, v30

    invoke-static {v0, v1}, Lcom/policydm/eng/core/XDMUic;->xdmUicSetResultKeep(Lcom/policydm/eng/core/XDMUicResult;I)Lcom/policydm/eng/core/XDMUicResult;

    move-result-object v25

    .line 589
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/agent/XDMTask;->m_DmAgent:Lcom/policydm/agent/XDMAgent;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/policydm/agent/XDMAgent;->m_AgentHandler:Lcom/policydm/agent/XDMAgentHandler;

    move-object/from16 v30, v0

    const/16 v31, 0x15

    move-object/from16 v0, v30

    move/from16 v1, v31

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lcom/policydm/agent/XDMAgentHandler;->xdmAgentHdlrContinueSession(ILjava/lang/Object;)V

    goto/16 :goto_1

    .line 593
    .end local v25    # "pUicResult":Lcom/policydm/eng/core/XDMUicResult;
    :pswitch_14
    const-string v30, "XEVENT_NOTI_RECEIVED"

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 594
    const/16 v21, 0x0

    .line 595
    .local v21, "pPushMsg":Lcom/policydm/noti/XNOTIMessage;
    new-instance v19, Lcom/policydm/noti/XNOTIHandler;

    invoke-direct/range {v19 .. v19}, Lcom/policydm/noti/XNOTIHandler;-><init>()V

    .line 596
    .local v19, "pHandler":Lcom/policydm/noti/XNOTIHandler;
    new-instance v27, Lcom/policydm/noti/XNOTI;

    invoke-direct/range {v27 .. v27}, Lcom/policydm/noti/XNOTI;-><init>()V

    .line 597
    .local v27, "ptMsg":Lcom/policydm/noti/XNOTI;
    iget-object v0, v8, Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;->param:Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget-object v7, v0, Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;->param:Ljava/lang/Object;

    check-cast v7, Lcom/policydm/noti/XNOTIMessage;

    .line 599
    .local v7, "msg1":Lcom/policydm/noti/XNOTIMessage;
    invoke-static {}, Lcom/policydm/adapter/XDMDevinfAdapter;->xdmDevAdpVerifyDeviceID()Z

    move-result v30

    if-nez v30, :cond_1b

    .line 601
    invoke-static {}, Lcom/policydm/noti/XNOTIAdapter;->xnotiPushDataHandling()V

    goto/16 :goto_1

    .line 605
    :cond_1b
    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Lcom/policydm/noti/XNOTIHandler;->xnotiPushHdleMessageCopy(Ljava/lang/Object;)Lcom/policydm/noti/XNOTIMessage;

    move-result-object v21

    .line 606
    if-nez v21, :cond_1c

    .line 608
    const-string v30, "pPushMsg is NULL"

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 609
    invoke-static {}, Lcom/policydm/noti/XNOTIAdapter;->xnotiPushDataHandling()V

    goto/16 :goto_1

    .line 613
    :cond_1c
    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/policydm/noti/XNOTIHandler;->xnotiPushHdleMsgHandler(Lcom/policydm/noti/XNOTIMessage;)Lcom/policydm/noti/XNOTI;

    move-result-object v27

    .line 614
    if-nez v27, :cond_1d

    .line 616
    invoke-static {}, Lcom/policydm/noti/XNOTIAdapter;->xnotiPushDataHandling()V

    goto/16 :goto_1

    .line 620
    :cond_1d
    move-object/from16 v0, v27

    iget v0, v0, Lcom/policydm/noti/XNOTI;->appId:I

    move/from16 v30, v0

    packed-switch v30, :pswitch_data_1

    .line 707
    const-string v30, "Not Support Application"

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 623
    :pswitch_15
    const/4 v3, 0x1

    .line 625
    .local v3, "bNotiExecute":Z
    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/policydm/noti/XNOTI;->triggerHeader:Lcom/policydm/noti/XNOTITriggerheader;

    move-object/from16 v30, v0

    if-nez v30, :cond_1e

    .line 627
    const-string v30, "triggerHeader is NULL."

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 628
    invoke-static {}, Lcom/policydm/noti/XNOTIAdapter;->xnotiPushDataHandling()V

    goto/16 :goto_1

    .line 632
    :cond_1e
    new-instance v16, Lcom/policydm/db/XDBNotiInfo;

    invoke-direct/range {v16 .. v16}, Lcom/policydm/db/XDBNotiInfo;-><init>()V

    .line 633
    .local v16, "notiInfo":Lcom/policydm/db/XDBNotiInfo;
    const/16 v30, 0x0

    move/from16 v0, v30

    move-object/from16 v1, v16

    iput v0, v1, Lcom/policydm/db/XDBNotiInfo;->appId:I

    .line 634
    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/policydm/noti/XNOTI;->triggerHeader:Lcom/policydm/noti/XNOTITriggerheader;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget v0, v0, Lcom/policydm/noti/XNOTITriggerheader;->uiMode:I

    move/from16 v30, v0

    move/from16 v0, v30

    move-object/from16 v1, v16

    iput v0, v1, Lcom/policydm/db/XDBNotiInfo;->uiMode:I

    .line 635
    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/policydm/noti/XNOTI;->triggerHeader:Lcom/policydm/noti/XNOTITriggerheader;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/policydm/noti/XNOTITriggerheader;->m_szServerID:Ljava/lang/String;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/policydm/db/XDBNotiInfo;->m_szServerId:Ljava/lang/String;

    .line 636
    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/policydm/noti/XNOTI;->triggerHeader:Lcom/policydm/noti/XNOTITriggerheader;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/policydm/noti/XNOTITriggerheader;->m_szSessionID:Ljava/lang/String;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/policydm/db/XDBNotiInfo;->m_szSessionId:Ljava/lang/String;

    .line 637
    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/policydm/noti/XNOTI;->triggerBody:Lcom/policydm/noti/XNOTITriggerbody;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget v0, v0, Lcom/policydm/noti/XNOTITriggerbody;->opmode:I

    move/from16 v30, v0

    move/from16 v0, v30

    move-object/from16 v1, v16

    iput v0, v1, Lcom/policydm/db/XDBNotiInfo;->opMode:I

    .line 638
    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/policydm/noti/XNOTI;->triggerBody:Lcom/policydm/noti/XNOTITriggerbody;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget-wide v0, v0, Lcom/policydm/noti/XNOTITriggerbody;->pushjobId:J

    move-wide/from16 v30, v0

    move-wide/from16 v0, v30

    move-object/from16 v2, v16

    iput-wide v0, v2, Lcom/policydm/db/XDBNotiInfo;->jobId:J

    .line 640
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/policydm/db/XDBNotiInfo;->m_szServerId:Ljava/lang/String;

    move-object/from16 v30, v0

    invoke-static/range {v30 .. v30}, Lcom/policydm/db/XDB;->xdbCheckActiveProfileIndexByServerID(Ljava/lang/String;)Z

    move-result v30

    if-nez v30, :cond_1f

    .line 642
    const-string v30, "Not Active Profile Index By ServerID"

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 643
    invoke-static {}, Lcom/policydm/noti/XNOTIAdapter;->xnotiPushDataHandling()V

    goto/16 :goto_1

    .line 647
    :cond_1f
    const/16 v30, 0x0

    invoke-static/range {v30 .. v30}, Lcom/policydm/db/XDB;->xdbSetWaitWifiFlag(Z)V

    .line 648
    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/policydm/noti/XNOTI;->triggerBody:Lcom/policydm/noti/XNOTITriggerbody;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget v0, v0, Lcom/policydm/noti/XNOTITriggerbody;->opmode:I

    move/from16 v30, v0

    if-nez v30, :cond_20

    .line 650
    invoke-static {}, Lcom/policydm/db/XDB;->xdbGetAutoUpdateFlag()Z

    move-result v30

    if-nez v30, :cond_20

    .line 652
    const-string v30, "Auto update false, Not start push"

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 653
    invoke-static {}, Lcom/policydm/noti/XNOTIAdapter;->xnotiPushDataHandling()V

    goto/16 :goto_1

    .line 658
    :cond_20
    invoke-static {}, Lcom/policydm/db/XDBProfileListAdp;->xdbGetNotiSavedInfo()Lcom/policydm/db/XDBSessionSaveInfo;

    move-result-object v22

    .line 659
    .local v22, "pSessionSaveInfo":Lcom/policydm/db/XDBSessionSaveInfo;
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMOInitiatedType()I

    move-result v30

    if-eqz v30, :cond_23

    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMOStatus()I

    move-result v30

    if-nez v30, :cond_23

    invoke-static {}, Lcom/policydm/agent/XDMAgent;->xdmAgentGetSyncMode()I

    move-result v30

    if-nez v30, :cond_23

    .line 661
    invoke-static {}, Lcom/policydm/db/XDB;->xdbGetWifiOnlyFlag()Z

    move-result v30

    if-eqz v30, :cond_22

    invoke-static {}, Lcom/policydm/XDMApplication;->xdmProtoIsWIFIConnected()Z

    move-result v30

    if-nez v30, :cond_22

    .line 663
    invoke-static {}, Lcom/policydm/noti/XNOTIAdapter;->xnotiPushAdpDeleteAllNotiQueue()V

    .line 664
    invoke-static/range {v16 .. v16}, Lcom/policydm/db/XDBNotiAdp;->xdbNotiInsertInfo(Ljava/lang/Object;)V

    .line 665
    const/16 v30, 0x8

    invoke-static/range {v30 .. v30}, Lcom/policydm/ui/XUINotificationManager;->xuiSetIndicator(I)V

    .line 666
    const/16 v30, 0x3

    invoke-static/range {v30 .. v30}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOInitiatedType(I)V

    .line 667
    const/4 v3, 0x0

    .line 668
    invoke-static {}, Lcom/policydm/noti/XNOTIAdapter;->xnotiPushDataHandling()V

    .line 686
    :cond_21
    :goto_5
    if-eqz v3, :cond_1

    .line 688
    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/policydm/noti/XNOTI;->triggerBody:Lcom/policydm/noti/XNOTITriggerbody;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget v0, v0, Lcom/policydm/noti/XNOTITriggerbody;->opmode:I

    move/from16 v30, v0

    if-nez v30, :cond_26

    invoke-static {}, Lcom/policydm/db/XDB;->xdbGetWifiOnlyFlag()Z

    move-result v30

    if-eqz v30, :cond_26

    invoke-static {}, Lcom/policydm/XDMApplication;->xdmProtoIsWIFIConnected()Z

    move-result v30

    if-nez v30, :cond_26

    .line 690
    invoke-static {}, Lcom/policydm/noti/XNOTIAdapter;->xnotiPushAdpDeleteAllNotiQueue()V

    .line 691
    invoke-static/range {v16 .. v16}, Lcom/policydm/db/XDBNotiAdp;->xdbNotiInsertInfo(Ljava/lang/Object;)V

    .line 692
    const/16 v30, 0x8

    invoke-static/range {v30 .. v30}, Lcom/policydm/ui/XUINotificationManager;->xuiSetIndicator(I)V

    .line 693
    const/16 v30, 0x3

    invoke-static/range {v30 .. v30}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOInitiatedType(I)V

    .line 694
    invoke-static {}, Lcom/policydm/noti/XNOTIAdapter;->xnotiPushDataHandling()V

    goto/16 :goto_1

    .line 672
    :cond_22
    invoke-static {}, Lcom/policydm/noti/XNOTIAdapter;->xnotiPushAdpDeleteAllNotiQueue()V

    .line 673
    invoke-static {}, Lcom/policydm/ui/XUIDialogActivity;->xuiDlgRemove()V

    .line 674
    const/16 v30, 0x9

    invoke-static/range {v30 .. v30}, Lcom/policydm/ui/XUINotificationManager;->xuiSetIndicator(I)V

    .line 675
    const/4 v3, 0x1

    goto :goto_5

    .line 678
    :cond_23
    if-eqz v22, :cond_24

    move-object/from16 v0, v22

    iget v0, v0, Lcom/policydm/db/XDBSessionSaveInfo;->nSessionSaveState:I

    move/from16 v30, v0

    if-nez v30, :cond_25

    :cond_24
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMOInitiatedType()I

    move-result v30

    if-nez v30, :cond_25

    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMOStatus()I

    move-result v30

    if-eqz v30, :cond_21

    .line 680
    :cond_25
    invoke-static/range {v16 .. v16}, Lcom/policydm/db/XDBNotiAdp;->xdbNotiInsertInfo(Ljava/lang/Object;)V

    .line 681
    const-string v30, "Noti was saved"

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 682
    const/4 v3, 0x0

    .line 683
    invoke-static {}, Lcom/policydm/noti/XNOTIAdapter;->xnotiPushDataHandling()V

    goto :goto_5

    .line 698
    :cond_26
    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/policydm/noti/XNOTI;->triggerBody:Lcom/policydm/noti/XNOTITriggerbody;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget v0, v0, Lcom/policydm/noti/XNOTITriggerbody;->opmode:I

    move/from16 v30, v0

    if-nez v30, :cond_27

    .line 699
    const/16 v30, 0x9

    invoke-static/range {v30 .. v30}, Lcom/policydm/ui/XUINotificationManager;->xuiSetIndicator(I)V

    .line 700
    :cond_27
    const/16 v30, 0x1f

    const/16 v31, 0x0

    move/from16 v0, v30

    move-object/from16 v1, v16

    move-object/from16 v2, v31

    invoke-static {v0, v1, v2}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 713
    .end local v3    # "bNotiExecute":Z
    .end local v7    # "msg1":Lcom/policydm/noti/XNOTIMessage;
    .end local v16    # "notiInfo":Lcom/policydm/db/XDBNotiInfo;
    .end local v19    # "pHandler":Lcom/policydm/noti/XNOTIHandler;
    .end local v21    # "pPushMsg":Lcom/policydm/noti/XNOTIMessage;
    .end local v22    # "pSessionSaveInfo":Lcom/policydm/db/XDBSessionSaveInfo;
    .end local v27    # "ptMsg":Lcom/policydm/noti/XNOTI;
    :pswitch_16
    const-string v30, "XEVENT_NOTI_EXECUTE"

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 714
    iget-object v0, v8, Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;->param:Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;->param:Ljava/lang/Object;

    move-object/from16 v16, v0

    check-cast v16, Lcom/policydm/db/XDBNotiInfo;

    .line 716
    .restart local v16    # "notiInfo":Lcom/policydm/db/XDBNotiInfo;
    if-nez v16, :cond_28

    .line 718
    invoke-static {}, Lcom/policydm/noti/XNOTIAdapter;->xnotiPushDataHandling()V

    goto/16 :goto_1

    .line 722
    :cond_28
    move-object/from16 v0, v16

    iget v0, v0, Lcom/policydm/db/XDBNotiInfo;->appId:I

    move/from16 v30, v0

    packed-switch v30, :pswitch_data_2

    .line 736
    const-string v30, "Not Support Application"

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 740
    :goto_6
    invoke-static {}, Lcom/policydm/noti/XNOTIAdapter;->xnotiPushDataHandling()V

    goto/16 :goto_1

    .line 725
    :pswitch_17
    invoke-static {}, Lcom/policydm/noti/XNOTIAdapter;->xnotiPushAdpClearSessionStatus()V

    .line 726
    const/16 v30, 0x0

    invoke-static/range {v30 .. v30}, Lcom/policydm/agent/XDMAgent;->xdmAgentTpSetRetryCount(I)V

    .line 727
    move-object/from16 v0, v16

    iget v0, v0, Lcom/policydm/db/XDBNotiInfo;->appId:I

    move/from16 v30, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/policydm/db/XDBNotiInfo;->m_szSessionId:Ljava/lang/String;

    move-object/from16 v31, v0

    invoke-static/range {v30 .. v31}, Lcom/policydm/db/XDBProfileListAdp;->xdbSetSessionID(ILjava/lang/String;)V

    .line 728
    move-object/from16 v0, v16

    iget v0, v0, Lcom/policydm/db/XDBNotiInfo;->uiMode:I

    move/from16 v30, v0

    invoke-static/range {v30 .. v30}, Lcom/policydm/db/XDBProfileListAdp;->xdbSetNotiEvent(I)V

    .line 729
    move-object/from16 v0, v16

    iget v0, v0, Lcom/policydm/db/XDBNotiInfo;->opMode:I

    move/from16 v30, v0

    invoke-static/range {v30 .. v30}, Lcom/policydm/db/XDBProfileListAdp;->xdbSetNotiOpMode(I)V

    .line 730
    move-object/from16 v0, v16

    iget-wide v0, v0, Lcom/policydm/db/XDBNotiInfo;->jobId:J

    move-wide/from16 v30, v0

    invoke-static/range {v30 .. v31}, Lcom/policydm/db/XDBProfileListAdp;->xdbSetNotiJobId(J)V

    .line 731
    move-object/from16 v0, v16

    iget v0, v0, Lcom/policydm/db/XDBNotiInfo;->uiMode:I

    move/from16 v30, v0

    invoke-static/range {v30 .. v30}, Lcom/policydm/noti/XNOTIAdapter;->xnotiPushAdpProcessNotiMessage(I)V

    goto :goto_6

    .line 750
    .end local v16    # "notiInfo":Lcom/policydm/db/XDBNotiInfo;
    :pswitch_18
    const/4 v10, 0x0

    .line 751
    .local v10, "nID":I
    iget-object v0, v8, Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;->param:Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;->param:Ljava/lang/Object;

    move-object/from16 v30, v0

    check-cast v30, Ljava/lang/Integer;

    invoke-virtual/range {v30 .. v30}, Ljava/lang/Integer;->intValue()I

    move-result v10

    .line 752
    invoke-static {v10}, Lcom/policydm/noti/XNOTIAdapter;->xnotiPushAdpProcessWapPushMsg(I)V

    goto/16 :goto_1

    .line 156
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_4
        :pswitch_4
        :pswitch_9
        :pswitch_0
        :pswitch_12
        :pswitch_13
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_14
        :pswitch_16
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_18
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_a
        :pswitch_b
        :pswitch_f
        :pswitch_10
        :pswitch_d
        :pswitch_11
        :pswitch_c
        :pswitch_c
        :pswitch_e
    .end packed-switch

    .line 620
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_15
    .end packed-switch

    .line 722
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_17
    .end packed-switch
.end method

.method public xdmAgentTaskInit()V
    .locals 2

    .prologue
    .line 120
    sget-boolean v0, Lcom/policydm/agent/XDMTask;->g_IsSyncTaskInit:Z

    if-nez v0, :cond_0

    .line 122
    new-instance v0, Lcom/policydm/agent/XDMAgent;

    invoke-direct {v0}, Lcom/policydm/agent/XDMAgent;-><init>()V

    iput-object v0, p0, Lcom/policydm/agent/XDMTask;->m_DmAgent:Lcom/policydm/agent/XDMAgent;

    .line 123
    iget-object v0, p0, Lcom/policydm/agent/XDMTask;->m_DmAgent:Lcom/policydm/agent/XDMAgent;

    new-instance v1, Lcom/policydm/agent/XDMAgentHandler;

    invoke-direct {v1}, Lcom/policydm/agent/XDMAgentHandler;-><init>()V

    iput-object v1, v0, Lcom/policydm/agent/XDMAgent;->m_AgentHandler:Lcom/policydm/agent/XDMAgentHandler;

    .line 124
    new-instance v0, Lcom/policydm/dlagent/XFOTADlAgent;

    invoke-direct {v0}, Lcom/policydm/dlagent/XFOTADlAgent;-><init>()V

    iput-object v0, p0, Lcom/policydm/agent/XDMTask;->m_DlAgent:Lcom/policydm/dlagent/XFOTADlAgent;

    .line 125
    iget-object v0, p0, Lcom/policydm/agent/XDMTask;->m_DlAgent:Lcom/policydm/dlagent/XFOTADlAgent;

    new-instance v1, Lcom/policydm/dlagent/XFOTADlAgentHandler;

    invoke-direct {v1}, Lcom/policydm/dlagent/XFOTADlAgentHandler;-><init>()V

    iput-object v1, v0, Lcom/policydm/dlagent/XFOTADlAgent;->m_DlAgentHandler:Lcom/policydm/dlagent/XFOTADlAgentHandler;

    .line 127
    iget-object v0, p0, Lcom/policydm/agent/XDMTask;->m_DmAgent:Lcom/policydm/agent/XDMAgent;

    iget-object v0, v0, Lcom/policydm/agent/XDMAgent;->m_AgentHandler:Lcom/policydm/agent/XDMAgentHandler;

    iget-object v1, p0, Lcom/policydm/agent/XDMTask;->m_DmAgent:Lcom/policydm/agent/XDMAgent;

    iget-object v1, v1, Lcom/policydm/agent/XDMAgent;->m_HttpDMAdapter:Lcom/policydm/tp/XTPAdapter;

    iput-object v1, v0, Lcom/policydm/agent/XDMAgentHandler;->m_HttpDMAdapter:Lcom/policydm/tp/XTPAdapter;

    .line 128
    iget-object v0, p0, Lcom/policydm/agent/XDMTask;->m_DlAgent:Lcom/policydm/dlagent/XFOTADlAgent;

    iget-object v0, v0, Lcom/policydm/dlagent/XFOTADlAgent;->m_DlAgentHandler:Lcom/policydm/dlagent/XFOTADlAgentHandler;

    iget-object v0, p0, Lcom/policydm/agent/XDMTask;->m_DlAgent:Lcom/policydm/dlagent/XFOTADlAgent;

    sget-object v0, Lcom/policydm/dlagent/XFOTADlAgent;->m_HttpDLAdapter:Lcom/policydm/tp/XTPAdapter;

    sput-object v0, Lcom/policydm/dlagent/XFOTADlAgentHandler;->m_HttpDLAdapter:Lcom/policydm/tp/XTPAdapter;

    .line 131
    invoke-static {}, Lcom/policydm/db/XDB;->xdbInit()Z

    .line 132
    invoke-direct {p0}, Lcom/policydm/agent/XDMTask;->xdmAgentTaskDBInit()Z

    .line 134
    const/4 v0, 0x1

    sput-boolean v0, Lcom/policydm/agent/XDMTask;->g_IsSyncTaskInit:Z

    .line 136
    :cond_0
    return-void
.end method
