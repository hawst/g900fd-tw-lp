.class public Lcom/policydm/agent/XDMBuildCmd;
.super Lcom/policydm/agent/XDMHandleCmd;
.source "XDMBuildCmd.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/policydm/agent/XDMHandleCmd;-><init>()V

    return-void
.end method

.method public static xdmAgentBuildCmdAlert(Lcom/policydm/eng/core/XDMWorkspace;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserAlert;
    .locals 2
    .param p0, "ws"    # Lcom/policydm/eng/core/XDMWorkspace;
    .param p1, "szData"    # Ljava/lang/String;

    .prologue
    .line 138
    new-instance v0, Lcom/policydm/eng/parser/XDMParserAlert;

    invoke-direct {v0}, Lcom/policydm/eng/parser/XDMParserAlert;-><init>()V

    .line 139
    .local v0, "alertCmd":Lcom/policydm/eng/parser/XDMParserAlert;
    invoke-static {p0}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdGetCmdID(Lcom/policydm/eng/core/XDMWorkspace;)I

    move-result v1

    iput v1, v0, Lcom/policydm/eng/parser/XDMParserAlert;->cmdid:I

    .line 140
    iput-object p1, v0, Lcom/policydm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    .line 142
    return-object v0
.end method

.method public static xdmAgentBuildCmdDetailResults(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I[C)Lcom/policydm/eng/parser/XDMParserResults;
    .locals 12
    .param p0, "ws"    # Lcom/policydm/eng/core/XDMWorkspace;
    .param p1, "cmdRef"    # I
    .param p2, "szSource"    # Ljava/lang/String;
    .param p3, "szFormat"    # Ljava/lang/String;
    .param p4, "szType"    # Ljava/lang/String;
    .param p5, "size"    # I
    .param p6, "data"    # [C

    .prologue
    .line 446
    const/4 v7, 0x0

    .line 447
    .local v7, "results":Lcom/policydm/eng/parser/XDMParserResults;
    const/4 v3, 0x0

    .line 448
    .local v3, "item":Lcom/policydm/eng/parser/XDMParserItem;
    const/4 v4, 0x0

    .line 449
    .local v4, "meta":Lcom/policydm/eng/parser/XDMParserMeta;
    const/4 v1, 0x0

    .local v1, "h":Lcom/policydm/eng/core/XDMList;
    const/4 v9, 0x0

    .line 450
    .local v9, "t":Lcom/policydm/eng/core/XDMList;
    const/4 v5, 0x0

    .line 451
    .local v5, "node":Lcom/policydm/eng/core/XDMVnode;
    const/16 v10, 0x80

    new-array v6, v10, [C

    .line 454
    .local v6, "nodename":[C
    if-eqz p0, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 456
    :cond_0
    const-string v10, "ws or source is null"

    invoke-static {v10}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 457
    const/4 v10, 0x0

    .line 534
    :goto_0
    return-object v10

    .line 460
    :cond_1
    invoke-virtual {p2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    const/16 v11, 0x3f

    invoke-static {v10, v11, v6}, Lcom/policydm/eng/core/XDMMem;->xdmLibStrsplit([CC[C)Ljava/lang/String;

    .line 461
    invoke-static {v6}, Lcom/policydm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v8

    .line 462
    .local v8, "szNodeName1":Ljava/lang/String;
    iget-object v10, p0, Lcom/policydm/eng/core/XDMWorkspace;->om:Lcom/policydm/eng/core/XDMOmTree;

    invoke-static {v10, v8}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v5

    .line 463
    if-nez v5, :cond_2

    .line 465
    const-string v10, "Result node is null"

    invoke-static {v10}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 466
    const/4 v10, 0x0

    goto :goto_0

    .line 470
    :cond_2
    new-instance v7, Lcom/policydm/eng/parser/XDMParserResults;

    .end local v7    # "results":Lcom/policydm/eng/parser/XDMParserResults;
    invoke-direct {v7}, Lcom/policydm/eng/parser/XDMParserResults;-><init>()V

    .line 471
    .restart local v7    # "results":Lcom/policydm/eng/parser/XDMParserResults;
    new-instance v3, Lcom/policydm/eng/parser/XDMParserItem;

    .end local v3    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    invoke-direct {v3}, Lcom/policydm/eng/parser/XDMParserItem;-><init>()V

    .line 472
    .restart local v3    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    invoke-static {p0}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdGetCmdID(Lcom/policydm/eng/core/XDMWorkspace;)I

    move-result v10

    iput v10, v7, Lcom/policydm/eng/parser/XDMParserResults;->cmdid:I

    .line 473
    iget-object v10, p0, Lcom/policydm/eng/core/XDMWorkspace;->m_szMsgRef:Ljava/lang/String;

    iput-object v10, v7, Lcom/policydm/eng/parser/XDMParserResults;->m_szMsgRef:Ljava/lang/String;

    .line 474
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/policydm/eng/parser/XDMParserResults;->m_szCmdRef:Ljava/lang/String;

    .line 477
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_3

    .line 479
    iput-object p2, v3, Lcom/policydm/eng/parser/XDMParserItem;->m_szSource:Ljava/lang/String;

    .line 483
    :cond_3
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_4

    if-lez p5, :cond_8

    .line 485
    :cond_4
    new-instance v4, Lcom/policydm/eng/parser/XDMParserMeta;

    .end local v4    # "meta":Lcom/policydm/eng/parser/XDMParserMeta;
    invoke-direct {v4}, Lcom/policydm/eng/parser/XDMParserMeta;-><init>()V

    .line 486
    .restart local v4    # "meta":Lcom/policydm/eng/parser/XDMParserMeta;
    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_5

    .line 487
    move-object/from16 v0, p4

    iput-object v0, v4, Lcom/policydm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    .line 488
    :cond_5
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_6

    .line 489
    iput-object p3, v4, Lcom/policydm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    .line 490
    :cond_6
    if-lez p5, :cond_7

    .line 491
    move/from16 v0, p5

    iput v0, v4, Lcom/policydm/eng/parser/XDMParserMeta;->size:I

    .line 492
    :cond_7
    iput-object v4, v3, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    .line 496
    :cond_8
    if-eqz p6, :cond_d

    .line 498
    iget v10, v5, Lcom/policydm/eng/core/XDMVnode;->format:I

    const/4 v11, 0x2

    if-ne v10, v11, :cond_a

    .line 500
    new-instance v10, Lcom/policydm/eng/parser/XDMParserPcdata;

    invoke-direct {v10}, Lcom/policydm/eng/parser/XDMParserPcdata;-><init>()V

    iput-object v10, v3, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    .line 501
    iget-object v10, v3, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    const/4 v11, 0x1

    iput v11, v10, Lcom/policydm/eng/parser/XDMParserPcdata;->type:I

    .line 502
    iget-object v10, v3, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    move/from16 v0, p5

    new-array v11, v0, [C

    iput-object v11, v10, Lcom/policydm/eng/parser/XDMParserPcdata;->data:[C

    .line 503
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    move/from16 v0, p5

    if-ge v2, v0, :cond_9

    .line 504
    iget-object v10, v3, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    iget-object v10, v10, Lcom/policydm/eng/parser/XDMParserPcdata;->data:[C

    aget-char v11, p6, v2

    aput-char v11, v10, v2

    .line 503
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 506
    :cond_9
    iget-object v10, v3, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    move/from16 v0, p5

    iput v0, v10, Lcom/policydm/eng/parser/XDMParserPcdata;->size:I

    .line 530
    .end local v2    # "i":I
    :goto_2
    invoke-static {v1, v9, v3}, Lcom/policydm/eng/core/XDMList;->xdmListAppend(Lcom/policydm/eng/core/XDMList;Lcom/policydm/eng/core/XDMList;Ljava/lang/Object;)Lcom/policydm/eng/core/XDMList;

    move-result-object v1

    .line 532
    iput-object v1, v7, Lcom/policydm/eng/parser/XDMParserResults;->itemlist:Lcom/policydm/eng/core/XDMList;

    move-object v10, v7

    .line 534
    goto/16 :goto_0

    .line 510
    :cond_a
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_c

    const-string v10, "bin"

    invoke-virtual {v10, p3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v10

    if-nez v10, :cond_c

    .line 512
    new-instance v10, Lcom/policydm/eng/parser/XDMParserPcdata;

    invoke-direct {v10}, Lcom/policydm/eng/parser/XDMParserPcdata;-><init>()V

    iput-object v10, v3, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    .line 513
    iget-object v10, v3, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    move/from16 v0, p5

    new-array v11, v0, [C

    iput-object v11, v10, Lcom/policydm/eng/parser/XDMParserPcdata;->data:[C

    .line 515
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_3
    move/from16 v0, p5

    if-ge v2, v0, :cond_b

    .line 516
    iget-object v10, v3, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    iget-object v10, v10, Lcom/policydm/eng/parser/XDMParserPcdata;->data:[C

    aget-char v11, p6, v2

    aput-char v11, v10, v2

    .line 515
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 518
    :cond_b
    iget-object v10, v3, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    move/from16 v0, p5

    iput v0, v10, Lcom/policydm/eng/parser/XDMParserPcdata;->size:I

    goto :goto_2

    .line 522
    .end local v2    # "i":I
    :cond_c
    invoke-static/range {p6 .. p6}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentDataStString2Pcdata([C)Lcom/policydm/eng/parser/XDMParserPcdata;

    move-result-object v10

    iput-object v10, v3, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    goto :goto_2

    .line 528
    :cond_d
    const/4 v10, 0x0

    iput-object v10, v3, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    goto :goto_2
.end method

.method public static xdmAgentBuildCmdGenericAlert(Lcom/policydm/eng/core/XDMWorkspace;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserAlert;
    .locals 8
    .param p0, "ws"    # Lcom/policydm/eng/core/XDMWorkspace;
    .param p1, "szData"    # Ljava/lang/String;

    .prologue
    .line 149
    const/4 v3, 0x0

    .line 150
    .local v3, "item":Lcom/policydm/eng/parser/XDMParserItem;
    const/4 v1, 0x0

    .local v1, "head":Lcom/policydm/eng/core/XDMList;
    const/4 v5, 0x0

    .line 153
    .local v5, "tail":Lcom/policydm/eng/core/XDMList;
    new-instance v0, Lcom/policydm/eng/parser/XDMParserAlert;

    invoke-direct {v0}, Lcom/policydm/eng/parser/XDMParserAlert;-><init>()V

    .line 154
    .local v0, "alertCmd":Lcom/policydm/eng/parser/XDMParserAlert;
    invoke-static {p0}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdGetCmdID(Lcom/policydm/eng/core/XDMWorkspace;)I

    move-result v6

    iput v6, v0, Lcom/policydm/eng/parser/XDMParserAlert;->cmdid:I

    .line 155
    iput-object p1, v0, Lcom/policydm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    .line 157
    new-instance v3, Lcom/policydm/eng/parser/XDMParserItem;

    .end local v3    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    invoke-direct {v3}, Lcom/policydm/eng/parser/XDMParserItem;-><init>()V

    .line 158
    .restart local v3    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    new-instance v6, Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-direct {v6}, Lcom/policydm/eng/parser/XDMParserMeta;-><init>()V

    iput-object v6, v3, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    .line 159
    new-instance v6, Lcom/policydm/eng/parser/XDMParserPcdata;

    invoke-direct {v6}, Lcom/policydm/eng/parser/XDMParserPcdata;-><init>()V

    iput-object v6, v3, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    .line 161
    invoke-static {}, Lcom/policydm/agent/XDMAgent;->xdmAgentGetDefaultLocuri()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/policydm/eng/parser/XDMParserItem;->m_szSource:Ljava/lang/String;

    .line 162
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMOInitiatedType()I

    move-result v2

    .line 164
    .local v2, "initType":I
    const/4 v6, 0x1

    if-ne v2, v6, :cond_0

    .line 166
    iget-object v6, v3, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    const-string v7, "chr"

    iput-object v7, v6, Lcom/policydm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    .line 167
    iget-object v6, v3, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    const-string v7, "org.openmobilealliance.dm.policyupdate.userrequest"

    iput-object v7, v6, Lcom/policydm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    .line 168
    const-string v6, "ALERT_TYPE_USER_INIT"

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 180
    :goto_0
    const-string v4, "0"

    .line 181
    .local v4, "szAlertData":Ljava/lang/String;
    iget-object v6, v3, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v7

    iput-object v7, v6, Lcom/policydm/eng/parser/XDMParserPcdata;->data:[C

    .line 182
    iget-object v6, v3, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    const/4 v7, 0x0

    iput v7, v6, Lcom/policydm/eng/parser/XDMParserPcdata;->type:I

    .line 184
    invoke-static {v1, v5, v3}, Lcom/policydm/eng/core/XDMList;->xdmListAppend(Lcom/policydm/eng/core/XDMList;Lcom/policydm/eng/core/XDMList;Ljava/lang/Object;)Lcom/policydm/eng/core/XDMList;

    move-result-object v1

    .line 185
    iput-object v1, v0, Lcom/policydm/eng/parser/XDMParserAlert;->itemlist:Lcom/policydm/eng/core/XDMList;

    .line 187
    return-object v0

    .line 170
    .end local v4    # "szAlertData":Ljava/lang/String;
    :cond_0
    const/4 v6, 0x2

    if-ne v2, v6, :cond_1

    .line 172
    iget-object v6, v3, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    const-string v7, "chr"

    iput-object v7, v6, Lcom/policydm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    .line 173
    iget-object v6, v3, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    const-string v7, "org.openmobilealliance.dm.policyupdate.devicerequest"

    iput-object v7, v6, Lcom/policydm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    .line 174
    const-string v6, "ALERT_TYPE_DEV_INIT"

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 178
    :cond_1
    const-string v6, "Init no flag"

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdmAgentBuildCmdGenericAlertReport(Lcom/policydm/eng/core/XDMWorkspace;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserAlert;
    .locals 9
    .param p0, "ws"    # Lcom/policydm/eng/core/XDMWorkspace;
    .param p1, "szData"    # Ljava/lang/String;

    .prologue
    .line 193
    const/4 v1, 0x0

    .line 194
    .local v1, "Item":Lcom/policydm/eng/parser/XDMParserItem;
    const/4 v2, 0x0

    .local v2, "head":Lcom/policydm/eng/core/XDMList;
    const/4 v6, 0x0

    .line 195
    .local v6, "tail":Lcom/policydm/eng/core/XDMList;
    const/4 v5, 0x0

    .line 196
    .local v5, "szResult":Ljava/lang/String;
    const/4 v4, 0x0

    .line 197
    .local v4, "szCorrelator":Ljava/lang/String;
    const/4 v3, 0x0

    .line 199
    .local v3, "nAgentType":I
    new-instance v0, Lcom/policydm/eng/parser/XDMParserAlert;

    invoke-direct {v0}, Lcom/policydm/eng/parser/XDMParserAlert;-><init>()V

    .line 200
    .local v0, "Alert":Lcom/policydm/eng/parser/XDMParserAlert;
    invoke-static {p0}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdGetCmdID(Lcom/policydm/eng/core/XDMWorkspace;)I

    move-result v7

    iput v7, v0, Lcom/policydm/eng/parser/XDMParserAlert;->cmdid:I

    .line 201
    iput-object p1, v0, Lcom/policydm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    .line 203
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMOCorrelator()Ljava/lang/String;

    move-result-object v4

    .line 204
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 206
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_0

    .line 208
    iput-object v4, v0, Lcom/policydm/eng/parser/XDMParserAlert;->m_szCorrelator:Ljava/lang/String;

    .line 212
    :cond_0
    new-instance v1, Lcom/policydm/eng/parser/XDMParserItem;

    .end local v1    # "Item":Lcom/policydm/eng/parser/XDMParserItem;
    invoke-direct {v1}, Lcom/policydm/eng/parser/XDMParserItem;-><init>()V

    .line 213
    .restart local v1    # "Item":Lcom/policydm/eng/parser/XDMParserItem;
    new-instance v7, Lcom/policydm/eng/parser/XDMParserPcdata;

    invoke-direct {v7}, Lcom/policydm/eng/parser/XDMParserPcdata;-><init>()V

    iput-object v7, v1, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    .line 214
    iget-object v7, v1, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    const/4 v8, 0x0

    iput v8, v7, Lcom/policydm/eng/parser/XDMParserPcdata;->type:I

    .line 216
    invoke-static {}, Lcom/policydm/db/XDBAgentAdp;->xdbGetDmAgentType()I

    move-result v3

    .line 218
    const/4 v7, 0x1

    if-ne v3, v7, :cond_4

    .line 220
    invoke-static {}, Lcom/policydm/agent/XDMAgent;->xdmAgentGetDefaultLocuri()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v1, Lcom/policydm/eng/parser/XDMParserItem;->m_szSource:Ljava/lang/String;

    .line 221
    new-instance v7, Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-direct {v7}, Lcom/policydm/eng/parser/XDMParserMeta;-><init>()V

    iput-object v7, v1, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    .line 224
    iget-object v7, v1, Lcom/policydm/eng/parser/XDMParserItem;->m_szSource:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 226
    const-string v7, "Item.source is null"

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 233
    :cond_1
    :goto_0
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMOResultCode()Ljava/lang/String;

    move-result-object v5

    .line 234
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 236
    const-string v5, "0"

    .line 237
    iget-object v7, v1, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    invoke-virtual {v5}, Ljava/lang/String;->toCharArray()[C

    move-result-object v8

    iput-object v8, v7, Lcom/policydm/eng/parser/XDMParserPcdata;->data:[C

    .line 249
    :goto_1
    invoke-static {v2, v6, v1}, Lcom/policydm/eng/core/XDMList;->xdmListAppend(Lcom/policydm/eng/core/XDMList;Lcom/policydm/eng/core/XDMList;Ljava/lang/Object;)Lcom/policydm/eng/core/XDMList;

    move-result-object v2

    .line 250
    iput-object v2, v0, Lcom/policydm/eng/parser/XDMParserAlert;->itemlist:Lcom/policydm/eng/core/XDMList;

    .line 252
    return-object v0

    .line 228
    :cond_2
    iget-object v7, v1, Lcom/policydm/eng/parser/XDMParserItem;->m_szSource:Ljava/lang/String;

    const-string v8, "/DownloadAndUpdate"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 230
    iget-object v7, v1, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    const-string v8, "org.openmobilealliance.dm.policyupdate.downloadandupdate"

    iput-object v8, v7, Lcom/policydm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    goto :goto_0

    .line 241
    :cond_3
    iget-object v7, v1, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    invoke-virtual {v5}, Ljava/lang/String;->toCharArray()[C

    move-result-object v8

    iput-object v8, v7, Lcom/policydm/eng/parser/XDMParserPcdata;->data:[C

    goto :goto_1

    .line 246
    :cond_4
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "nAgentType : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static xdmAgentBuildCmdGetCmdID(Lcom/policydm/eng/core/XDMWorkspace;)I
    .locals 2
    .param p0, "ws"    # Lcom/policydm/eng/core/XDMWorkspace;

    .prologue
    .line 33
    const/4 v0, 0x0

    .line 34
    .local v0, "cmdid":I
    iget v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->cmdID:I

    .end local v0    # "cmdid":I
    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/policydm/eng/core/XDMWorkspace;->cmdID:I

    .line 36
    .restart local v0    # "cmdid":I
    return v0
.end method

.method public static xdmAgentBuildCmdParseTargetURI(Lcom/policydm/eng/core/XDMWorkspace;)V
    .locals 14
    .param p0, "ws"    # Lcom/policydm/eng/core/XDMWorkspace;

    .prologue
    const/16 v13, 0x50

    const/16 v12, 0x3f

    const/16 v11, 0x3a

    const/16 v10, 0x2f

    const/4 v9, 0x0

    .line 539
    iget-object v6, p0, Lcom/policydm/eng/core/XDMWorkspace;->m_szTargetURI:Ljava/lang/String;

    .line 541
    .local v6, "szTarget":Ljava/lang/String;
    const/4 v7, 0x5

    invoke-virtual {v6, v9, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    const-string v8, "https"

    invoke-virtual {v7, v8}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v7

    if-nez v7, :cond_1

    .line 543
    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 544
    .local v5, "szSub":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "target.substring "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 545
    invoke-virtual {v5, v11}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 546
    .local v1, "firstComma":I
    invoke-virtual {v5, v12}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    .line 547
    .local v2, "firstQuestion":I
    if-lez v2, :cond_0

    .line 548
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "https://"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v5, v9, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/policydm/eng/core/XDMWorkspace;->m_szHostname:Ljava/lang/String;

    .line 551
    :goto_0
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ws.hostname=> "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/policydm/eng/core/XDMWorkspace;->m_szHostname:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 552
    invoke-virtual {v5, v10}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    .line 553
    .local v3, "firstSlash":I
    add-int/lit8 v7, v1, 0x1

    invoke-virtual {v5, v7, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 557
    .local v4, "szPort":Ljava/lang/String;
    :try_start_0
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    iput v7, p0, Lcom/policydm/eng/core/XDMWorkspace;->port:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 563
    :goto_1
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ws.port"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/policydm/eng/core/XDMWorkspace;->port:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 589
    :goto_2
    return-void

    .line 550
    .end local v3    # "firstSlash":I
    .end local v4    # "szPort":Ljava/lang/String;
    :cond_0
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "https://"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/policydm/eng/core/XDMWorkspace;->m_szHostname:Ljava/lang/String;

    goto :goto_0

    .line 559
    .restart local v3    # "firstSlash":I
    .restart local v4    # "szPort":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 561
    .local v0, "e":Ljava/lang/NumberFormatException;
    iput v13, p0, Lcom/policydm/eng/core/XDMWorkspace;->port:I

    goto :goto_1

    .line 567
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    .end local v1    # "firstComma":I
    .end local v2    # "firstQuestion":I
    .end local v3    # "firstSlash":I
    .end local v4    # "szPort":Ljava/lang/String;
    .end local v5    # "szSub":Ljava/lang/String;
    :cond_1
    const/4 v7, 0x7

    invoke-virtual {v6, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 568
    .restart local v5    # "szSub":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "target.substring "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 569
    invoke-virtual {v5, v11}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 570
    .restart local v1    # "firstComma":I
    invoke-virtual {v5, v12}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    .line 571
    .restart local v2    # "firstQuestion":I
    if-lez v2, :cond_2

    .line 572
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "http://"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v5, v9, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/policydm/eng/core/XDMWorkspace;->m_szHostname:Ljava/lang/String;

    .line 575
    :goto_3
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ws.hostname => "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/policydm/eng/core/XDMWorkspace;->m_szHostname:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 576
    invoke-virtual {v5, v10}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    .line 577
    .restart local v3    # "firstSlash":I
    add-int/lit8 v7, v1, 0x1

    invoke-virtual {v5, v7, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 581
    .restart local v4    # "szPort":Ljava/lang/String;
    :try_start_1
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    iput v7, p0, Lcom/policydm/eng/core/XDMWorkspace;->port:I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    .line 587
    :goto_4
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ws.port => "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/policydm/eng/core/XDMWorkspace;->port:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 574
    .end local v3    # "firstSlash":I
    .end local v4    # "szPort":Ljava/lang/String;
    :cond_2
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "http://"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/policydm/eng/core/XDMWorkspace;->m_szHostname:Ljava/lang/String;

    goto :goto_3

    .line 583
    .restart local v3    # "firstSlash":I
    .restart local v4    # "szPort":Ljava/lang/String;
    :catch_1
    move-exception v0

    .line 585
    .restart local v0    # "e":Ljava/lang/NumberFormatException;
    iput v13, p0, Lcom/policydm/eng/core/XDMWorkspace;->port:I

    goto :goto_4
.end method

.method public static xdmAgentBuildCmdReplace(Lcom/policydm/eng/core/XDMWorkspace;Lcom/policydm/eng/core/XDMLinkedList;)Lcom/policydm/eng/parser/XDMParserReplace;
    .locals 6
    .param p0, "ws"    # Lcom/policydm/eng/core/XDMWorkspace;
    .param p1, "list"    # Lcom/policydm/eng/core/XDMLinkedList;

    .prologue
    .line 288
    const/4 v3, 0x0

    .line 290
    .local v3, "replaceCmd":Lcom/policydm/eng/parser/XDMParserReplace;
    const/4 v0, 0x0

    .local v0, "head":Lcom/policydm/eng/core/XDMList;
    const/4 v4, 0x0

    .line 292
    .local v4, "tail":Lcom/policydm/eng/core/XDMList;
    new-instance v3, Lcom/policydm/eng/parser/XDMParserReplace;

    .end local v3    # "replaceCmd":Lcom/policydm/eng/parser/XDMParserReplace;
    invoke-direct {v3}, Lcom/policydm/eng/parser/XDMParserReplace;-><init>()V

    .line 293
    .restart local v3    # "replaceCmd":Lcom/policydm/eng/parser/XDMParserReplace;
    invoke-static {p0}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdGetCmdID(Lcom/policydm/eng/core/XDMWorkspace;)I

    move-result v5

    iput v5, v3, Lcom/policydm/eng/parser/XDMParserReplace;->cmdid:I

    .line 295
    const/4 v5, 0x0

    invoke-static {p1, v5}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListSetCurrentObj(Lcom/policydm/eng/core/XDMLinkedList;I)V

    .line 296
    invoke-static {p1}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/policydm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/policydm/eng/parser/XDMParserItem;

    .line 297
    .local v1, "obj":Lcom/policydm/eng/parser/XDMParserItem;
    :goto_0
    if-eqz v1, :cond_1

    .line 299
    new-instance v2, Lcom/policydm/eng/parser/XDMParserItem;

    invoke-direct {v2}, Lcom/policydm/eng/parser/XDMParserItem;-><init>()V

    .line 300
    .local v2, "obj2":Lcom/policydm/eng/parser/XDMParserItem;
    invoke-static {v2, v1}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentDataStDuplItem(Lcom/policydm/eng/parser/XDMParserItem;Lcom/policydm/eng/parser/XDMParserItem;)V

    .line 301
    if-nez v0, :cond_0

    .line 302
    invoke-static {v0, v4, v2}, Lcom/policydm/eng/core/XDMList;->xdmListAppend(Lcom/policydm/eng/core/XDMList;Lcom/policydm/eng/core/XDMList;Ljava/lang/Object;)Lcom/policydm/eng/core/XDMList;

    move-result-object v0

    .line 305
    :goto_1
    invoke-static {p1}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/policydm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "obj":Lcom/policydm/eng/parser/XDMParserItem;
    check-cast v1, Lcom/policydm/eng/parser/XDMParserItem;

    .restart local v1    # "obj":Lcom/policydm/eng/parser/XDMParserItem;
    goto :goto_0

    .line 304
    :cond_0
    invoke-static {v0, v4, v2}, Lcom/policydm/eng/core/XDMList;->xdmListAppend(Lcom/policydm/eng/core/XDMList;Lcom/policydm/eng/core/XDMList;Ljava/lang/Object;)Lcom/policydm/eng/core/XDMList;

    goto :goto_1

    .line 307
    .end local v2    # "obj2":Lcom/policydm/eng/parser/XDMParserItem;
    :cond_1
    iput-object v0, v3, Lcom/policydm/eng/parser/XDMParserReplace;->itemlist:Lcom/policydm/eng/core/XDMList;

    .line 309
    return-object v3
.end method

.method public static xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;
    .locals 13
    .param p0, "ws"    # Lcom/policydm/eng/core/XDMWorkspace;
    .param p1, "cmdRef"    # I
    .param p2, "szCmd"    # Ljava/lang/String;
    .param p3, "szSource"    # Ljava/lang/String;
    .param p4, "szTarget"    # Ljava/lang/String;
    .param p5, "szData"    # Ljava/lang/String;

    .prologue
    .line 314
    const/4 v6, 0x0

    .line 319
    .local v6, "status":Lcom/policydm/eng/parser/XDMParserStatus;
    new-instance v6, Lcom/policydm/eng/parser/XDMParserStatus;

    .end local v6    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    invoke-direct {v6}, Lcom/policydm/eng/parser/XDMParserStatus;-><init>()V

    .line 321
    .restart local v6    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    invoke-static {p0}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdGetCmdID(Lcom/policydm/eng/core/XDMWorkspace;)I

    move-result v9

    iput v9, v6, Lcom/policydm/eng/parser/XDMParserStatus;->cmdid:I

    .line 322
    iget-object v9, p0, Lcom/policydm/eng/core/XDMWorkspace;->m_szMsgRef:Ljava/lang/String;

    iput-object v9, v6, Lcom/policydm/eng/parser/XDMParserStatus;->m_szMsgRef:Ljava/lang/String;

    .line 323
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v6, Lcom/policydm/eng/parser/XDMParserStatus;->m_szCmdRef:Ljava/lang/String;

    .line 324
    iput-object p2, v6, Lcom/policydm/eng/parser/XDMParserStatus;->m_szCmd:Ljava/lang/String;

    .line 325
    move-object/from16 v0, p5

    iput-object v0, v6, Lcom/policydm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    .line 327
    const/4 v3, 0x0

    .line 328
    .local v3, "head":Lcom/policydm/eng/core/XDMList;
    const/4 v8, 0x0

    .line 330
    .local v8, "tail":Lcom/policydm/eng/core/XDMList;
    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_8

    .line 332
    move-object/from16 v7, p4

    .line 333
    .local v7, "szBuf":Ljava/lang/String;
    invoke-static {v3, v8, v7}, Lcom/policydm/eng/core/XDMList;->xdmListAppend(Lcom/policydm/eng/core/XDMList;Lcom/policydm/eng/core/XDMList;Ljava/lang/Object;)Lcom/policydm/eng/core/XDMList;

    move-result-object v3

    .line 334
    iput-object v3, v6, Lcom/policydm/eng/parser/XDMParserStatus;->targetref:Lcom/policydm/eng/core/XDMList;

    .line 353
    .end local v7    # "szBuf":Ljava/lang/String;
    :cond_0
    :goto_0
    const/4 v3, 0x0

    .line 354
    const/4 v8, 0x0

    .line 355
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_b

    .line 357
    move-object/from16 v7, p3

    .line 358
    .restart local v7    # "szBuf":Ljava/lang/String;
    invoke-static {v3, v8, v7}, Lcom/policydm/eng/core/XDMList;->xdmListAppend(Lcom/policydm/eng/core/XDMList;Lcom/policydm/eng/core/XDMList;Ljava/lang/Object;)Lcom/policydm/eng/core/XDMList;

    move-result-object v3

    .line 359
    iput-object v3, v6, Lcom/policydm/eng/parser/XDMParserStatus;->sourceref:Lcom/policydm/eng/core/XDMList;

    .line 375
    .end local v7    # "szBuf":Ljava/lang/String;
    :cond_1
    :goto_1
    const-string v9, "407"

    move-object/from16 v0, p5

    invoke-virtual {v9, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v9

    if-eqz v9, :cond_2

    const-string v9, "401"

    move-object/from16 v0, p5

    invoke-virtual {v9, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v9

    if-nez v9, :cond_4

    :cond_2
    if-nez p1, :cond_4

    .line 377
    new-instance v1, Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-direct {v1}, Lcom/policydm/eng/parser/XDMParserMeta;-><init>()V

    .line 378
    .local v1, "chal":Lcom/policydm/eng/parser/XDMParserMeta;
    const-string v9, "b64"

    iput-object v9, v1, Lcom/policydm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    .line 379
    iget v9, p0, Lcom/policydm/eng/core/XDMWorkspace;->serverCredType:I

    if-nez v9, :cond_e

    .line 381
    const-string v9, "syncml:auth-basic"

    iput-object v9, v1, Lcom/policydm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    .line 382
    const-string v9, "XDM_CRED_TYPE_BASIC"

    invoke-static {v9}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 399
    :cond_3
    :goto_2
    iput-object v1, v6, Lcom/policydm/eng/parser/XDMParserStatus;->chal:Lcom/policydm/eng/parser/XDMParserMeta;

    .line 402
    .end local v1    # "chal":Lcom/policydm/eng/parser/XDMParserMeta;
    :cond_4
    iget-boolean v9, p0, Lcom/policydm/eng/core/XDMWorkspace;->sendChal:Z

    if-nez v9, :cond_6

    .line 404
    const-string v9, "212"

    move-object/from16 v0, p5

    invoke-virtual {v9, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v9

    if-nez v9, :cond_6

    if-nez p1, :cond_6

    .line 406
    iget v9, p0, Lcom/policydm/eng/core/XDMWorkspace;->serverCredType:I

    const/4 v10, 0x1

    if-ne v9, v10, :cond_6

    .line 408
    iget-object v9, v6, Lcom/policydm/eng/parser/XDMParserStatus;->chal:Lcom/policydm/eng/parser/XDMParserMeta;

    if-eqz v9, :cond_5

    .line 410
    iget-object v9, v6, Lcom/policydm/eng/parser/XDMParserStatus;->chal:Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-static {v9}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDeleteMeta(Ljava/lang/Object;)V

    .line 411
    const/4 v9, 0x0

    iput-object v9, v6, Lcom/policydm/eng/parser/XDMParserStatus;->chal:Lcom/policydm/eng/parser/XDMParserMeta;

    .line 414
    :cond_5
    new-instance v1, Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-direct {v1}, Lcom/policydm/eng/parser/XDMParserMeta;-><init>()V

    .line 415
    .restart local v1    # "chal":Lcom/policydm/eng/parser/XDMParserMeta;
    const-string v9, "b64"

    iput-object v9, v1, Lcom/policydm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    .line 416
    const-string v9, "syncml:auth-md5"

    iput-object v9, v1, Lcom/policydm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    .line 417
    iget-object v9, p0, Lcom/policydm/eng/core/XDMWorkspace;->serverNextNonce:[B

    invoke-static {v9}, Lcom/policydm/eng/core/XDMBase64;->xdmBase64Encode([B)[B

    move-result-object v2

    .line 418
    .local v2, "encoder":[B
    const/4 v9, 0x0

    iput-object v9, v1, Lcom/policydm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    .line 419
    new-instance v9, Ljava/lang/String;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v10

    invoke-direct {v9, v2, v10}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    iput-object v9, v1, Lcom/policydm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    .line 421
    iget-object v9, v1, Lcom/policydm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    invoke-static {v9}, Lcom/policydm/db/XDBProfileAdp;->xdbSetServerNonce(Ljava/lang/String;)Ljava/lang/String;

    .line 422
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "CRED_TYPE_MD5 serverNextNonce: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    new-instance v10, Ljava/lang/String;

    iget-object v11, p0, Lcom/policydm/eng/core/XDMWorkspace;->serverNextNonce:[B

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v12

    invoke-direct {v10, v11, v12}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "Encoded server nonce "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v1, Lcom/policydm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 423
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/policydm/eng/core/XDMWorkspace;->sendChal:Z

    .line 424
    iput-object v1, v6, Lcom/policydm/eng/parser/XDMParserStatus;->chal:Lcom/policydm/eng/parser/XDMParserMeta;

    .line 429
    .end local v1    # "chal":Lcom/policydm/eng/parser/XDMParserMeta;
    .end local v2    # "encoder":[B
    :cond_6
    iget v9, p0, Lcom/policydm/eng/core/XDMWorkspace;->serverCredType:I

    const/4 v10, 0x2

    if-ne v9, v10, :cond_7

    const-string v9, "200"

    move-object/from16 v0, p5

    invoke-virtual {v9, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v9

    if-nez v9, :cond_7

    if-nez p1, :cond_7

    .line 431
    new-instance v1, Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-direct {v1}, Lcom/policydm/eng/parser/XDMParserMeta;-><init>()V

    .line 432
    .restart local v1    # "chal":Lcom/policydm/eng/parser/XDMParserMeta;
    const-string v9, "b64"

    iput-object v9, v1, Lcom/policydm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    .line 433
    const-string v9, "syncml:auth-MAC"

    iput-object v9, v1, Lcom/policydm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    .line 435
    iget-object v9, p0, Lcom/policydm/eng/core/XDMWorkspace;->serverNextNonce:[B

    invoke-static {v9}, Lcom/policydm/eng/core/XDMBase64;->xdmBase64Encode([B)[B

    move-result-object v2

    .line 436
    .restart local v2    # "encoder":[B
    const/4 v9, 0x0

    iput-object v9, v1, Lcom/policydm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    .line 437
    new-instance v9, Ljava/lang/String;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v10

    invoke-direct {v9, v2, v10}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    iput-object v9, v1, Lcom/policydm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    .line 439
    iput-object v1, v6, Lcom/policydm/eng/parser/XDMParserStatus;->chal:Lcom/policydm/eng/parser/XDMParserMeta;

    .line 441
    .end local v1    # "chal":Lcom/policydm/eng/parser/XDMParserMeta;
    .end local v2    # "encoder":[B
    :cond_7
    return-object v6

    .line 336
    :cond_8
    iget-object v9, p0, Lcom/policydm/eng/core/XDMWorkspace;->targetRefList:Lcom/policydm/eng/core/XDMLinkedList;

    if-eqz v9, :cond_0

    .line 338
    iget-object v9, p0, Lcom/policydm/eng/core/XDMWorkspace;->targetRefList:Lcom/policydm/eng/core/XDMLinkedList;

    const/4 v10, 0x0

    invoke-static {v9, v10}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListSetCurrentObj(Lcom/policydm/eng/core/XDMLinkedList;I)V

    .line 339
    iget-object v9, p0, Lcom/policydm/eng/core/XDMWorkspace;->targetRefList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v9}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/policydm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/policydm/eng/parser/XDMParserItem;

    .line 340
    .local v4, "obj":Lcom/policydm/eng/parser/XDMParserItem;
    :goto_3
    if-eqz v4, :cond_a

    .line 342
    new-instance v5, Lcom/policydm/eng/parser/XDMParserItem;

    invoke-direct {v5}, Lcom/policydm/eng/parser/XDMParserItem;-><init>()V

    .line 343
    .local v5, "obj2":Lcom/policydm/eng/parser/XDMParserItem;
    invoke-static {v5, v4}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplItem(Lcom/policydm/eng/parser/XDMParserItem;Lcom/policydm/eng/parser/XDMParserItem;)V

    .line 344
    if-nez v3, :cond_9

    .line 345
    invoke-static {v3, v8, v5}, Lcom/policydm/eng/core/XDMList;->xdmListAppend(Lcom/policydm/eng/core/XDMList;Lcom/policydm/eng/core/XDMList;Ljava/lang/Object;)Lcom/policydm/eng/core/XDMList;

    move-result-object v3

    .line 348
    :goto_4
    iget-object v9, p0, Lcom/policydm/eng/core/XDMWorkspace;->targetRefList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v9}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/policydm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "obj":Lcom/policydm/eng/parser/XDMParserItem;
    check-cast v4, Lcom/policydm/eng/parser/XDMParserItem;

    .restart local v4    # "obj":Lcom/policydm/eng/parser/XDMParserItem;
    goto :goto_3

    .line 347
    :cond_9
    invoke-static {v3, v8, v5}, Lcom/policydm/eng/core/XDMList;->xdmListAppend(Lcom/policydm/eng/core/XDMList;Lcom/policydm/eng/core/XDMList;Ljava/lang/Object;)Lcom/policydm/eng/core/XDMList;

    goto :goto_4

    .line 350
    .end local v5    # "obj2":Lcom/policydm/eng/parser/XDMParserItem;
    :cond_a
    iput-object v3, v6, Lcom/policydm/eng/parser/XDMParserStatus;->targetref:Lcom/policydm/eng/core/XDMList;

    goto/16 :goto_0

    .line 361
    .end local v4    # "obj":Lcom/policydm/eng/parser/XDMParserItem;
    :cond_b
    iget-object v9, p0, Lcom/policydm/eng/core/XDMWorkspace;->sourceRefList:Lcom/policydm/eng/core/XDMLinkedList;

    if-eqz v9, :cond_1

    .line 363
    iget-object v9, p0, Lcom/policydm/eng/core/XDMWorkspace;->sourceRefList:Lcom/policydm/eng/core/XDMLinkedList;

    const/4 v10, 0x0

    invoke-static {v9, v10}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListSetCurrentObj(Lcom/policydm/eng/core/XDMLinkedList;I)V

    .line 364
    iget-object v9, p0, Lcom/policydm/eng/core/XDMWorkspace;->sourceRefList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v9}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/policydm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/policydm/eng/parser/XDMParserItem;

    .line 365
    .restart local v4    # "obj":Lcom/policydm/eng/parser/XDMParserItem;
    :goto_5
    if-eqz v4, :cond_d

    .line 367
    if-nez v3, :cond_c

    .line 368
    invoke-static {v3, v8, v4}, Lcom/policydm/eng/core/XDMList;->xdmListAppend(Lcom/policydm/eng/core/XDMList;Lcom/policydm/eng/core/XDMList;Ljava/lang/Object;)Lcom/policydm/eng/core/XDMList;

    move-result-object v3

    .line 371
    :goto_6
    iget-object v9, p0, Lcom/policydm/eng/core/XDMWorkspace;->sourceRefList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v9}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/policydm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "obj":Lcom/policydm/eng/parser/XDMParserItem;
    check-cast v4, Lcom/policydm/eng/parser/XDMParserItem;

    .restart local v4    # "obj":Lcom/policydm/eng/parser/XDMParserItem;
    goto :goto_5

    .line 370
    :cond_c
    invoke-static {v3, v8, v4}, Lcom/policydm/eng/core/XDMList;->xdmListAppend(Lcom/policydm/eng/core/XDMList;Lcom/policydm/eng/core/XDMList;Ljava/lang/Object;)Lcom/policydm/eng/core/XDMList;

    goto :goto_6

    .line 373
    :cond_d
    iput-object v3, v6, Lcom/policydm/eng/parser/XDMParserStatus;->sourceref:Lcom/policydm/eng/core/XDMList;

    goto/16 :goto_1

    .line 384
    .end local v4    # "obj":Lcom/policydm/eng/parser/XDMParserItem;
    .restart local v1    # "chal":Lcom/policydm/eng/parser/XDMParserMeta;
    :cond_e
    iget v9, p0, Lcom/policydm/eng/core/XDMWorkspace;->serverCredType:I

    const/4 v10, 0x1

    if-ne v9, v10, :cond_f

    .line 386
    const-string v9, "syncml:auth-md5"

    iput-object v9, v1, Lcom/policydm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    .line 387
    iget-object v9, p0, Lcom/policydm/eng/core/XDMWorkspace;->serverNextNonce:[B

    invoke-static {v9}, Lcom/policydm/eng/core/XDMBase64;->xdmBase64Encode([B)[B

    move-result-object v2

    .line 388
    .restart local v2    # "encoder":[B
    const/4 v9, 0x0

    iput-object v9, v1, Lcom/policydm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    .line 389
    new-instance v9, Ljava/lang/String;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v10

    invoke-direct {v9, v2, v10}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    iput-object v9, v1, Lcom/policydm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    .line 390
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "CRED_TYPE_MD5 serverNextNonce: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    new-instance v10, Ljava/lang/String;

    iget-object v11, p0, Lcom/policydm/eng/core/XDMWorkspace;->serverNextNonce:[B

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v12

    invoke-direct {v10, v11, v12}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", Encoded server nonce "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v1, Lcom/policydm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 392
    .end local v2    # "encoder":[B
    :cond_f
    iget v9, p0, Lcom/policydm/eng/core/XDMWorkspace;->serverCredType:I

    const/4 v10, 0x2

    if-ne v9, v10, :cond_3

    .line 394
    const-string v9, "syncml:auth-MAC"

    iput-object v9, v1, Lcom/policydm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    .line 395
    iget-object v9, p0, Lcom/policydm/eng/core/XDMWorkspace;->serverNextNonce:[B

    invoke-static {v9}, Lcom/policydm/eng/core/XDMBase64;->xdmBase64Encode([B)[B

    move-result-object v2

    .line 396
    .restart local v2    # "encoder":[B
    const/4 v9, 0x0

    iput-object v9, v1, Lcom/policydm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    .line 397
    new-instance v9, Ljava/lang/String;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v10

    invoke-direct {v9, v2, v10}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    iput-object v9, v1, Lcom/policydm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    goto/16 :goto_2
.end method

.method public static xdmAgentBuildCmdSyncHeader(Lcom/policydm/eng/core/XDMWorkspace;)Lcom/policydm/eng/core/XDMWorkspace;
    .locals 11
    .param p0, "ws"    # Lcom/policydm/eng/core/XDMWorkspace;

    .prologue
    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 42
    iget-object v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->syncHeader:Lcom/policydm/eng/parser/XDMParserSyncheader;

    if-nez v0, :cond_6

    .line 45
    new-instance v0, Lcom/policydm/eng/parser/XDMParserSyncheader;

    invoke-direct {v0}, Lcom/policydm/eng/parser/XDMParserSyncheader;-><init>()V

    iput-object v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->syncHeader:Lcom/policydm/eng/parser/XDMParserSyncheader;

    .line 46
    iget-object v9, p0, Lcom/policydm/eng/core/XDMWorkspace;->syncHeader:Lcom/policydm/eng/parser/XDMParserSyncheader;

    .line 48
    .local v9, "sh":Lcom/policydm/eng/parser/XDMParserSyncheader;
    iget-object v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->m_szHostname:Ljava/lang/String;

    iput-object v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->m_szTargetURI:Ljava/lang/String;

    .line 52
    const-string v0, "1.2"

    iput-object v0, v9, Lcom/policydm/eng/parser/XDMParserSyncheader;->m_szVerdtd:Ljava/lang/String;

    .line 53
    const-string v0, "DM/1.2"

    iput-object v0, v9, Lcom/policydm/eng/parser/XDMParserSyncheader;->m_szVerproto:Ljava/lang/String;

    .line 61
    iget-object v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->m_szSessionID:Ljava/lang/String;

    iput-object v0, v9, Lcom/policydm/eng/parser/XDMParserSyncheader;->m_szSessionId:Ljava/lang/String;

    .line 62
    iget v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->msgID:I

    iput v0, v9, Lcom/policydm/eng/parser/XDMParserSyncheader;->msgid:I

    .line 64
    iget-object v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->m_szTargetURI:Ljava/lang/String;

    iput-object v0, v9, Lcom/policydm/eng/parser/XDMParserSyncheader;->m_szTarget:Ljava/lang/String;

    .line 65
    iget-object v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->m_szSourceURI:Ljava/lang/String;

    iput-object v0, v9, Lcom/policydm/eng/parser/XDMParserSyncheader;->m_szSource:Ljava/lang/String;

    .line 66
    iget-object v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->m_szUserName:Ljava/lang/String;

    iput-object v0, v9, Lcom/policydm/eng/parser/XDMParserSyncheader;->m_szLocname:Ljava/lang/String;

    .line 68
    new-instance v8, Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-direct {v8}, Lcom/policydm/eng/parser/XDMParserMeta;-><init>()V

    .line 69
    .local v8, "meta":Lcom/policydm/eng/parser/XDMParserMeta;
    iget v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->maxMsgSize:I

    iput v0, v8, Lcom/policydm/eng/parser/XDMParserMeta;->maxmsgsize:I

    .line 70
    iget v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->maxObjSize:I

    iput v0, v8, Lcom/policydm/eng/parser/XDMParserMeta;->maxobjsize:I

    .line 72
    iput-object v8, v9, Lcom/policydm/eng/parser/XDMParserSyncheader;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    .line 86
    .end local v8    # "meta":Lcom/policydm/eng/parser/XDMParserMeta;
    :cond_0
    :goto_0
    iget-object v0, v9, Lcom/policydm/eng/parser/XDMParserSyncheader;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    if-eqz v0, :cond_1

    .line 88
    iput-object v5, v9, Lcom/policydm/eng/parser/XDMParserSyncheader;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    .line 91
    :cond_1
    iget v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->authState:I

    if-eq v0, v2, :cond_2

    iget v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->authState:I

    if-nez v0, :cond_3

    :cond_2
    iget v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->serverAuthState:I

    if-eq v0, v2, :cond_5

    .line 95
    :cond_3
    const/4 v10, 0x0

    .line 97
    .local v10, "szTmp":Ljava/lang/String;
    iget v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->credType:I

    invoke-static {v0}, Lcom/policydm/eng/core/XDMAuth;->xdmAuthCredType2String(I)Ljava/lang/String;

    move-result-object v10

    .line 98
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 100
    iget-object v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->m_szUserName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->m_szClientPW:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_7

    .line 102
    const/4 v7, 0x0

    .line 124
    .local v7, "cred":Lcom/policydm/eng/parser/XDMParserCred;
    :cond_4
    :goto_1
    iput-object v7, v9, Lcom/policydm/eng/parser/XDMParserSyncheader;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    .line 132
    .end local v7    # "cred":Lcom/policydm/eng/parser/XDMParserCred;
    .end local v10    # "szTmp":Ljava/lang/String;
    :cond_5
    :goto_2
    return-object p0

    .line 76
    .end local v9    # "sh":Lcom/policydm/eng/parser/XDMParserSyncheader;
    :cond_6
    iget-object v9, p0, Lcom/policydm/eng/core/XDMWorkspace;->syncHeader:Lcom/policydm/eng/parser/XDMParserSyncheader;

    .line 77
    .restart local v9    # "sh":Lcom/policydm/eng/parser/XDMParserSyncheader;
    iget v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->msgID:I

    iput v0, v9, Lcom/policydm/eng/parser/XDMParserSyncheader;->msgid:I

    .line 78
    iget-object v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->m_szTargetURI:Ljava/lang/String;

    iget-object v1, v9, Lcom/policydm/eng/parser/XDMParserSyncheader;->m_szTarget:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->m_szTargetURI:Ljava/lang/String;

    iput-object v0, v9, Lcom/policydm/eng/parser/XDMParserSyncheader;->m_szTarget:Ljava/lang/String;

    .line 81
    invoke-static {p0}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdParseTargetURI(Lcom/policydm/eng/core/XDMWorkspace;)V

    goto :goto_0

    .line 104
    .restart local v10    # "szTmp":Ljava/lang/String;
    :cond_7
    iget v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->credType:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_8

    .line 106
    new-instance v8, Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-direct {v8}, Lcom/policydm/eng/parser/XDMParserMeta;-><init>()V

    .line 108
    .restart local v8    # "meta":Lcom/policydm/eng/parser/XDMParserMeta;
    iget v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->credType:I

    invoke-static {v0}, Lcom/policydm/eng/core/XDMAuth;->xdmAuthCredType2String(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/policydm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    .line 109
    const-string v0, "b64"

    iput-object v0, v8, Lcom/policydm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    .line 110
    const/4 v7, 0x0

    .line 111
    .restart local v7    # "cred":Lcom/policydm/eng/parser/XDMParserCred;
    new-instance v7, Lcom/policydm/eng/parser/XDMParserCred;

    .end local v7    # "cred":Lcom/policydm/eng/parser/XDMParserCred;
    invoke-direct {v7}, Lcom/policydm/eng/parser/XDMParserCred;-><init>()V

    .line 112
    .restart local v7    # "cred":Lcom/policydm/eng/parser/XDMParserCred;
    iput-object v8, v7, Lcom/policydm/eng/parser/XDMParserCred;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    .line 114
    iget v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->credType:I

    iget-object v1, p0, Lcom/policydm/eng/core/XDMWorkspace;->m_szUserName:Ljava/lang/String;

    iget-object v2, p0, Lcom/policydm/eng/core/XDMWorkspace;->m_szClientPW:Ljava/lang/String;

    iget-object v3, p0, Lcom/policydm/eng/core/XDMWorkspace;->nextNonce:[B

    iget-object v4, p0, Lcom/policydm/eng/core/XDMWorkspace;->nextNonce:[B

    array-length v4, v4

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lcom/policydm/eng/core/XDMAuth;->xdmAuthMakeDigest(ILjava/lang/String;Ljava/lang/String;[BI[BI)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Lcom/policydm/eng/parser/XDMParserCred;->m_szData:Ljava/lang/String;

    .line 115
    iget-object v0, v7, Lcom/policydm/eng/parser/XDMParserCred;->m_szData:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 117
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "cred data = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v7, Lcom/policydm/eng/parser/XDMParserCred;->m_szData:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "credType = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/policydm/eng/core/XDMWorkspace;->credType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto :goto_1

    .line 122
    .end local v7    # "cred":Lcom/policydm/eng/parser/XDMParserCred;
    .end local v8    # "meta":Lcom/policydm/eng/parser/XDMParserMeta;
    :cond_8
    const/4 v7, 0x0

    .restart local v7    # "cred":Lcom/policydm/eng/parser/XDMParserCred;
    goto :goto_1

    .line 128
    .end local v7    # "cred":Lcom/policydm/eng/parser/XDMParserCred;
    :cond_9
    iput-object v5, v9, Lcom/policydm/eng/parser/XDMParserSyncheader;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    goto :goto_2
.end method

.method public static xdmAgentBuildCmdSynchrousGenericAlertReport(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;
    .locals 5
    .param p0, "ws"    # Lcom/policydm/eng/core/XDMWorkspace;
    .param p1, "cmdRef"    # I
    .param p2, "szCmd"    # Ljava/lang/String;
    .param p3, "szSource"    # Ljava/lang/String;
    .param p4, "szTarget"    # Ljava/lang/String;
    .param p5, "szData"    # Ljava/lang/String;

    .prologue
    .line 257
    const/4 v2, 0x0

    .line 258
    .local v2, "status":Lcom/policydm/eng/parser/XDMParserStatus;
    const/4 v1, 0x0

    .line 259
    .local v1, "item":Lcom/policydm/eng/parser/XDMParserItem;
    const/4 v0, 0x0

    .local v0, "head":Lcom/policydm/eng/core/XDMList;
    const/4 v3, 0x0

    .line 261
    .local v3, "tail":Lcom/policydm/eng/core/XDMList;
    new-instance v2, Lcom/policydm/eng/parser/XDMParserStatus;

    .end local v2    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    invoke-direct {v2}, Lcom/policydm/eng/parser/XDMParserStatus;-><init>()V

    .line 262
    .restart local v2    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    new-instance v1, Lcom/policydm/eng/parser/XDMParserItem;

    .end local v1    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    invoke-direct {v1}, Lcom/policydm/eng/parser/XDMParserItem;-><init>()V

    .line 264
    .restart local v1    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    invoke-static {p0}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdGetCmdID(Lcom/policydm/eng/core/XDMWorkspace;)I

    move-result v4

    iput v4, v2, Lcom/policydm/eng/parser/XDMParserStatus;->cmdid:I

    .line 265
    iget-object v4, p0, Lcom/policydm/eng/core/XDMWorkspace;->m_szMsgRef:Ljava/lang/String;

    iput-object v4, v2, Lcom/policydm/eng/parser/XDMParserStatus;->m_szMsgRef:Ljava/lang/String;

    .line 266
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/policydm/eng/parser/XDMParserStatus;->m_szCmdRef:Ljava/lang/String;

    .line 267
    iput-object p2, v2, Lcom/policydm/eng/parser/XDMParserStatus;->m_szCmd:Ljava/lang/String;

    .line 268
    iput-object p5, v2, Lcom/policydm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    .line 270
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 272
    iput-object p4, v1, Lcom/policydm/eng/parser/XDMParserItem;->m_szSource:Ljava/lang/String;

    .line 275
    :cond_0
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 277
    iput-object p3, v1, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    .line 280
    :cond_1
    invoke-static {v0, v3, v1}, Lcom/policydm/eng/core/XDMList;->xdmListAppend(Lcom/policydm/eng/core/XDMList;Lcom/policydm/eng/core/XDMList;Ljava/lang/Object;)Lcom/policydm/eng/core/XDMList;

    move-result-object v0

    .line 281
    iput-object v0, v2, Lcom/policydm/eng/parser/XDMParserStatus;->itemlist:Lcom/policydm/eng/core/XDMList;

    .line 283
    return-object v2
.end method
