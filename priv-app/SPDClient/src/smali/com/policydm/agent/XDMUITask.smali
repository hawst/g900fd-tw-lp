.class public Lcom/policydm/agent/XDMUITask;
.super Ljava/lang/Object;
.source "XDMUITask.java"

# interfaces
.implements Lcom/policydm/interfaces/XDMInterface;
.implements Lcom/policydm/interfaces/XUIInterface;
.implements Ljava/lang/Runnable;


# static fields
.field public static g_hDmUiTask:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 33
    return-void
.end method


# virtual methods
.method public run()V
    .locals 1

    .prologue
    .line 37
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 39
    new-instance v0, Lcom/policydm/agent/XDMUITask$1;

    invoke-direct {v0, p0}, Lcom/policydm/agent/XDMUITask$1;-><init>(Lcom/policydm/agent/XDMUITask;)V

    sput-object v0, Lcom/policydm/agent/XDMUITask;->g_hDmUiTask:Landroid/os/Handler;

    .line 49
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 50
    return-void
.end method

.method public xdmUIEvent(Landroid/os/Message;)Z
    .locals 9
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 54
    const/4 v1, 0x0

    .line 56
    .local v1, "msgItem":Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;
    iget-object v6, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-nez v6, :cond_0

    .line 206
    :goto_0
    return v4

    .line 59
    :cond_0
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .end local v1    # "msgItem":Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;
    check-cast v1, Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;

    .line 61
    .restart local v1    # "msgItem":Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;
    iget v6, v1, Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;->type:I

    sparse-switch v6, :sswitch_data_0

    .line 205
    :cond_1
    :goto_1
    const/4 p1, 0x0

    move v4, v5

    .line 206
    goto :goto_0

    .line 64
    :sswitch_0
    const/16 v4, 0x68

    invoke-static {v4}, Lcom/policydm/XDMApplication;->xdmSendMessageDmHandler(I)V

    goto :goto_1

    .line 68
    :sswitch_1
    const-string v6, "XUI_DM_FINISH"

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 69
    invoke-static {}, Lcom/policydm/ui/XUIAdapter;->xuiAdpGetUiMode()I

    move-result v6

    if-ne v6, v4, :cond_2

    .line 71
    invoke-static {v4}, Lcom/policydm/ui/XUINotificationManager;->xuiSetIndicator(I)V

    .line 73
    :cond_2
    invoke-static {}, Lcom/policydm/ui/XUIDialogActivity;->xuiDlgRemove()V

    .line 75
    invoke-static {v5}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOInitiatedType(I)V

    .line 76
    invoke-static {v5}, Lcom/policydm/ui/XUIAdapter;->xuiAdpSetUiMode(I)V

    .line 78
    invoke-static {}, Lcom/policydm/db/XDBSpdAdp;->xdbGetSpdPushRegId()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    invoke-static {}, Lcom/policydm/db/XDBSpdAdp;->xdbGetSpdDeviceUpdate()I

    move-result v6

    if-eq v6, v4, :cond_1

    .line 80
    :cond_3
    invoke-static {}, Lcom/policydm/XDMBroadcastReceiver;->callPushRegisterStart()V

    goto :goto_1

    .line 85
    :sswitch_2
    const-string v6, "XUI_DM_SYNC_ERROR"

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 86
    invoke-static {}, Lcom/policydm/ui/XUIAdapter;->xuiAdpGetUiMode()I

    move-result v6

    if-ne v6, v4, :cond_4

    .line 88
    invoke-static {v8}, Lcom/policydm/ui/XUINotificationManager;->xuiSetIndicator(I)V

    .line 90
    :cond_4
    invoke-static {}, Lcom/policydm/ui/XUIDialogActivity;->xuiDlgRemove()V

    .line 92
    invoke-static {}, Lcom/policydm/noti/XNOTIAdapter;->xnotiPushAdpClearSessionStatus()V

    .line 93
    invoke-static {}, Lcom/policydm/noti/XNOTIAdapter;->xnotiPushAdpHandleNotiQueue()V

    goto :goto_1

    .line 98
    :sswitch_3
    const-string v6, "XUI_DM_CONNECT_FAIL"

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 99
    invoke-static {}, Lcom/policydm/ui/XUIAdapter;->xuiAdpGetUiMode()I

    move-result v6

    if-ne v6, v4, :cond_5

    .line 101
    invoke-static {v7}, Lcom/policydm/ui/XUINotificationManager;->xuiSetIndicator(I)V

    .line 103
    :cond_5
    invoke-static {}, Lcom/policydm/ui/XUIDialogActivity;->xuiDlgRemove()V

    .line 105
    invoke-static {v5}, Lcom/policydm/ui/XUIAdapter;->xuiAdpSetUiMode(I)V

    goto :goto_1

    .line 111
    :sswitch_4
    const-string v6, "XUI_DM_ERROR"

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 112
    invoke-static {}, Lcom/policydm/ui/XUIAdapter;->xuiAdpGetUiMode()I

    move-result v6

    if-ne v6, v4, :cond_6

    .line 114
    invoke-static {v8}, Lcom/policydm/ui/XUINotificationManager;->xuiSetIndicator(I)V

    .line 116
    :cond_6
    invoke-static {}, Lcom/policydm/ui/XUIDialogActivity;->xuiDlgRemove()V

    .line 118
    invoke-static {v5}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOInitiatedType(I)V

    .line 119
    invoke-static {v5}, Lcom/policydm/ui/XUIAdapter;->xuiAdpSetUiMode(I)V

    goto :goto_1

    .line 125
    :sswitch_5
    const-string v4, "XUI_DM_SYNC_START/CONNECT"

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_1

    .line 130
    :sswitch_6
    const-string v6, "XUI_DL_CONNECT_FAIL"

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 131
    invoke-static {}, Lcom/policydm/ui/XUIAdapter;->xuiAdpGetUiMode()I

    move-result v6

    if-ne v6, v4, :cond_7

    .line 133
    invoke-static {v7}, Lcom/policydm/ui/XUINotificationManager;->xuiSetIndicator(I)V

    .line 135
    :cond_7
    invoke-static {}, Lcom/policydm/ui/XUIDialogActivity;->xuiDlgRemove()V

    .line 137
    invoke-static {v5}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOInitiatedType(I)V

    .line 138
    invoke-static {v5}, Lcom/policydm/ui/XUIAdapter;->xuiAdpSetUiMode(I)V

    goto/16 :goto_1

    .line 143
    :sswitch_7
    const-string v4, "XUI_DM_UIC_REQUEST"

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 145
    iget-object v4, v1, Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;->param:Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;

    iget-object v3, v4, Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;->param:Ljava/lang/Object;

    check-cast v3, Lcom/policydm/eng/core/XDMUicOption;

    .line 146
    .local v3, "uicOption":Lcom/policydm/eng/core/XDMUicOption;
    const/4 v2, 0x0

    .line 147
    .local v2, "pUicOption":Lcom/policydm/eng/core/XDMUicOption;
    invoke-static {}, Lcom/policydm/eng/core/XDMUic;->xdmUicCreateUicOption()Lcom/policydm/eng/core/XDMUicOption;

    move-result-object v2

    .line 148
    invoke-static {v2, v3}, Lcom/policydm/eng/core/XDMUic;->xdmUicCopyUicOption(Ljava/lang/Object;Ljava/lang/Object;)Lcom/policydm/eng/core/XDMUicOption;

    move-result-object v2

    .line 150
    sput-object v2, Lcom/policydm/ui/XUIDialogActivity;->g_UicOption:Ljava/lang/Object;

    .line 151
    const/16 v4, 0x6e

    invoke-static {v4}, Lcom/policydm/XDMApplication;->xdmSendMessageDmHandler(I)V

    goto/16 :goto_1

    .line 155
    .end local v2    # "pUicOption":Lcom/policydm/eng/core/XDMUicOption;
    .end local v3    # "uicOption":Lcom/policydm/eng/core/XDMUicOption;
    :sswitch_8
    const-string v4, "XUI_DM_NETWOR_WARNING"

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 156
    const/16 v4, 0x7c

    invoke-static {v4}, Lcom/policydm/XDMApplication;->xdmSendMessageDmHandler(I)V

    goto/16 :goto_1

    .line 160
    :sswitch_9
    const-string v6, "XUI_DL_DOWNLOAD_YES_NO"

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 161
    invoke-static {v4}, Lcom/policydm/dlagent/XFOTADl;->xfotaDownloadMemoryCheck(I)V

    goto/16 :goto_1

    .line 165
    :sswitch_a
    const-string v4, "XUI_DL_DOWNLOAD_IN_COMPLETE"

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 166
    invoke-static {}, Lcom/policydm/dlagent/XFOTADl;->xfotaDownloadComplete()Z

    goto/16 :goto_1

    .line 170
    :sswitch_b
    const-string v4, "XUI_DL_RESUME_DOWNLOAD"

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 171
    invoke-static {}, Lcom/policydm/ui/XUIAdapter;->xuiAdpStartSession()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 172
    invoke-static {v7}, Lcom/policydm/ui/XUIAdapter;->xuiAdpSetUiMode(I)V

    goto/16 :goto_1

    .line 180
    :sswitch_c
    invoke-static {}, Lcom/policydm/tp/XTPAdapter;->xtpAdpHttpCookieClear()V

    .line 181
    invoke-static {v5}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOInitiatedType(I)V

    .line 182
    invoke-static {v5}, Lcom/policydm/ui/XUIAdapter;->xuiAdpSetUiMode(I)V

    .line 183
    iget v4, v1, Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;->type:I

    invoke-static {v4}, Lcom/policydm/ui/XUIAdapter;->xuiAdpRequestNoti(I)V

    goto/16 :goto_1

    .line 187
    :sswitch_d
    const/4 v0, 0x0

    .line 188
    .local v0, "bStatus":Z
    invoke-static {}, Lcom/policydm/agent/XDMTask;->xdmAgentTaskGetDmInitStatus()Z

    move-result v0

    .line 189
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "XUI_DM_IDLE_STATE :"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 191
    if-eqz v0, :cond_8

    .line 193
    invoke-static {v5}, Lcom/policydm/noti/XNOTIAdapter;->xnotiPushAdpResumeNotiAction(I)V

    goto/16 :goto_1

    .line 197
    :cond_8
    const-string v4, "DM Not Initialized"

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 61
    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_d
        0x65 -> :sswitch_5
        0x66 -> :sswitch_5
        0x68 -> :sswitch_0
        0x6e -> :sswitch_7
        0x78 -> :sswitch_3
        0x7a -> :sswitch_4
        0x7b -> :sswitch_2
        0x7c -> :sswitch_8
        0x82 -> :sswitch_1
        0x8c -> :sswitch_c
        0x8d -> :sswitch_c
        0x8e -> :sswitch_c
        0x8f -> :sswitch_c
        0x90 -> :sswitch_c
        0x96 -> :sswitch_9
        0x97 -> :sswitch_a
        0x98 -> :sswitch_b
        0x99 -> :sswitch_6
    .end sparse-switch
.end method
