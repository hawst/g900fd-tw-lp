.class public Lcom/policydm/agent/XDMAgent;
.super Ljava/lang/Object;
.source "XDMAgent.java"

# interfaces
.implements Lcom/policydm/interfaces/XDLInterface;
.implements Lcom/policydm/interfaces/XDMDefInterface;
.implements Lcom/policydm/interfaces/XDMInterface;
.implements Lcom/policydm/interfaces/XEventInterface;
.implements Lcom/policydm/interfaces/XSPDInterface;
.implements Lcom/policydm/interfaces/XTPInterface;
.implements Lcom/policydm/interfaces/XUICInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/policydm/agent/XDMAgent$1;
    }
.end annotation


# static fields
.field private static final DEFAULT_NONCE:Ljava/lang/String; = "SamSungNextNonce="

.field private static final PACKAGE_SIZE_GAP:I = 0x80

.field private static g_AccName:Ljava/lang/String;

.field public static g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

.field private static m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

.field private static m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

.field private static m_bPendingStatus:Z

.field private static m_nConnectRetryCount:I

.field private static m_nDMSync:I


# instance fields
.field public m_AddCmd:Lcom/policydm/eng/parser/XDMParserAdd;

.field public m_AgentHandler:Lcom/policydm/agent/XDMAgentHandler;

.field public m_Alert:Lcom/policydm/eng/parser/XDMParserAlert;

.field public m_Atomic:Lcom/policydm/eng/parser/XDMParserAtomic;

.field public m_CopyCmd:Lcom/policydm/eng/parser/XDMParserCopy;

.field public m_DeleteCmd:Lcom/policydm/eng/parser/XDMParserDelete;

.field public m_Exec:Lcom/policydm/eng/parser/XDMParserExec;

.field public m_Get:Lcom/policydm/eng/parser/XDMParserGet;

.field public m_Header:Lcom/policydm/eng/parser/XDMParserSyncheader;

.field public m_HttpDMAdapter:Lcom/policydm/tp/XTPAdapter;

.field public m_ReplaceCmd:Lcom/policydm/eng/parser/XDMParserReplace;

.field public m_Sequence:Lcom/policydm/eng/parser/XDMParserSequence;

.field public m_Status:Lcom/policydm/eng/parser/XDMParserStatus;

.field public m_bInProgresscmd:Z

.field public m_szCmd:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 94
    const/4 v0, 0x0

    sput-object v0, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    .line 97
    sput v1, Lcom/policydm/agent/XDMAgent;->m_nDMSync:I

    .line 99
    sput-boolean v1, Lcom/policydm/agent/XDMAgent;->m_bPendingStatus:Z

    .line 101
    sput v1, Lcom/policydm/agent/XDMAgent;->m_nConnectRetryCount:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    iget-object v0, p0, Lcom/policydm/agent/XDMAgent;->m_HttpDMAdapter:Lcom/policydm/tp/XTPAdapter;

    if-nez v0, :cond_0

    .line 110
    new-instance v0, Lcom/policydm/tp/XTPAdapter;

    invoke-direct {v0}, Lcom/policydm/tp/XTPAdapter;-><init>()V

    iput-object v0, p0, Lcom/policydm/agent/XDMAgent;->m_HttpDMAdapter:Lcom/policydm/tp/XTPAdapter;

    .line 112
    :cond_0
    return-void
.end method

.method private xdmAgentCheckNonce(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "szNonce"    # Ljava/lang/String;

    .prologue
    .line 1668
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    move-object v1, p1

    .line 1680
    .end local p1    # "szNonce":Ljava/lang/String;
    .local v1, "szNonce":Ljava/lang/Object;
    :goto_0
    return-object v1

    .line 1674
    .end local v1    # "szNonce":Ljava/lang/Object;
    .restart local p1    # "szNonce":Ljava/lang/String;
    :cond_0
    const-string p1, "SamSungNextNonce="

    .line 1677
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/eng/core/XDMBase64;->xdmBase64Encode([B)[B

    move-result-object v0

    .line 1678
    .local v0, "encoder1":[B
    new-instance p1, Ljava/lang/String;

    .end local p1    # "szNonce":Ljava/lang/String;
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v2

    invoke-direct {p1, v0, v2}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .restart local p1    # "szNonce":Ljava/lang/String;
    move-object v1, p1

    .line 1680
    .restart local v1    # "szNonce":Ljava/lang/Object;
    goto :goto_0
.end method

.method public static xdmAgentClose()I
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 244
    sget-object v0, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    .line 246
    .local v0, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "inDMSync = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/policydm/agent/XDMAgent;->m_nDMSync:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 247
    sget v1, Lcom/policydm/agent/XDMAgent;->m_nDMSync:I

    if-lez v1, :cond_1

    .line 249
    if-eqz v0, :cond_0

    .line 251
    sget-boolean v1, Lcom/policydm/agent/XDMAgent;->m_bPendingStatus:Z

    if-eqz v1, :cond_2

    .line 253
    const-string v1, "Pending Status don\'t save"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 254
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->om:Lcom/policydm/eng/core/XDMOmTree;

    iget-object v1, v1, Lcom/policydm/eng/core/XDMOmTree;->vfs:Lcom/policydm/eng/core/XDMOmVfs;

    invoke-static {v1}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmVfsEnd(Lcom/policydm/eng/core/XDMOmVfs;)V

    .line 261
    :goto_0
    invoke-virtual {v0}, Lcom/policydm/eng/core/XDMWorkspace;->xdmFreeWorkSpace()V

    .line 262
    sput-object v4, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    .line 265
    :cond_0
    sput-object v4, Lcom/policydm/agent/XDMAgent;->g_AccName:Ljava/lang/String;

    .line 266
    invoke-static {v3}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetSyncMode(I)Z

    .line 268
    :cond_1
    return v3

    .line 258
    :cond_2
    const-string v1, "workspace save"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 259
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->om:Lcom/policydm/eng/core/XDMOmTree;

    invoke-static {v1}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmEnd(Lcom/policydm/eng/core/XDMOmTree;)I

    goto :goto_0
.end method

.method private xdmAgentCmdExecFumo(Lcom/policydm/eng/core/XDMWorkspace;Lcom/policydm/eng/parser/XDMParserExec;Lcom/policydm/eng/parser/XDMParserItem;)V
    .locals 6
    .param p1, "DmWorkspace"    # Lcom/policydm/eng/core/XDMWorkspace;
    .param p2, "exec"    # Lcom/policydm/eng/parser/XDMParserExec;
    .param p3, "DmParserItem"    # Lcom/policydm/eng/parser/XDMParserItem;

    .prologue
    .line 5101
    const-string v0, ""

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 5103
    const/16 v0, 0xa

    invoke-static {v0}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOStatus(I)V

    .line 5105
    if-eqz p2, :cond_0

    .line 5107
    iget v1, p2, Lcom/policydm/eng/parser/XDMParserExec;->cmdid:I

    const-string v2, "Exec"

    const/4 v3, 0x0

    iget-object v4, p3, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v5, "202"

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/policydm/agent/XDMAgent;->m_Status:Lcom/policydm/eng/parser/XDMParserStatus;

    .line 5108
    iget-object v0, p1, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    iget-object v1, p0, Lcom/policydm/agent/XDMAgent;->m_Status:Lcom/policydm/eng/parser/XDMParserStatus;

    invoke-static {v0, v1}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 5110
    :cond_0
    return-void
.end method

.method public static xdmAgentGetDefaultLocuri()Ljava/lang/String;
    .locals 3

    .prologue
    .line 7962
    const-string v1, "./SEAndroidPolicy"

    const-string v2, "/DownloadAndUpdate"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 7964
    .local v0, "szDefaultURI":Ljava/lang/String;
    return-object v0
.end method

.method public static xdmAgentGetOM()Lcom/policydm/eng/core/XDMOmTree;
    .locals 1

    .prologue
    .line 132
    sget-object v0, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    if-nez v0, :cond_0

    .line 134
    const-string v0, "dm_ws is NULL"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 135
    const/4 v0, 0x0

    .line 138
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    iget-object v0, v0, Lcom/policydm/eng/core/XDMWorkspace;->om:Lcom/policydm/eng/core/XDMOmTree;

    goto :goto_0
.end method

.method public static xdmAgentGetPendingStatus()Z
    .locals 1

    .prologue
    .line 7957
    sget-boolean v0, Lcom/policydm/agent/XDMAgent;->m_bPendingStatus:Z

    return v0
.end method

.method public static xdmAgentGetSyncMode()I
    .locals 3

    .prologue
    .line 143
    const/4 v0, 0x0

    .line 145
    .local v0, "nSync":I
    sget v0, Lcom/policydm/agent/XDMAgent;->m_nDMSync:I

    .line 146
    if-eqz v0, :cond_0

    .line 147
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "nSync = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 149
    :cond_0
    return v0
.end method

.method public static xdmAgentInit()I
    .locals 1

    .prologue
    .line 230
    new-instance v0, Lcom/policydm/eng/core/XDMWorkspace;

    invoke-direct {v0}, Lcom/policydm/eng/core/XDMWorkspace;-><init>()V

    sput-object v0, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    .line 231
    sget-object v0, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    if-nez v0, :cond_0

    .line 232
    const/4 v0, -0x1

    .line 239
    :goto_0
    return v0

    .line 234
    :cond_0
    const-string v0, "./SyncML/DMAcc"

    sput-object v0, Lcom/policydm/agent/XDMAgent;->g_AccName:Ljava/lang/String;

    .line 237
    new-instance v0, Lcom/policydm/eng/core/XDMAccXNode;

    invoke-direct {v0}, Lcom/policydm/eng/core/XDMAccXNode;-><init>()V

    sput-object v0, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    .line 239
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static xdmAgentInitParser(Lcom/policydm/eng/core/XDMWorkspace;Lcom/policydm/eng/parser/XDMParser;)I
    .locals 1
    .param p0, "ws"    # Lcom/policydm/eng/core/XDMWorkspace;
    .param p1, "p"    # Lcom/policydm/eng/parser/XDMParser;

    .prologue
    .line 116
    invoke-virtual {p1, p1, p0}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseInit(Lcom/policydm/eng/parser/XDMParser;Ljava/lang/Object;)V

    .line 117
    const/4 v0, 0x0

    return v0
.end method

.method public static xdmAgentMakeDefaultAcl(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;II)V
    .locals 4
    .param p0, "om"    # Lcom/policydm/eng/core/XDMOmTree;
    .param p1, "szPath"    # Ljava/lang/String;
    .param p2, "aclValue"    # I
    .param p3, "scope"    # I

    .prologue
    .line 2011
    const/4 v0, 0x0

    .line 2012
    .local v0, "acl":Lcom/policydm/eng/core/XDMOmAcl;
    const/4 v1, 0x0

    .line 2013
    .local v1, "item":Lcom/policydm/eng/core/XDMOmList;
    const/4 v2, 0x0

    .line 2015
    .local v2, "node":Lcom/policydm/eng/core/XDMVnode;
    invoke-static {p0, p1}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v2

    .line 2016
    if-eqz v2, :cond_1

    .line 2019
    if-eqz p2, :cond_0

    .line 2021
    iget-object v1, v2, Lcom/policydm/eng/core/XDMVnode;->acl:Lcom/policydm/eng/core/XDMOmList;

    .line 2022
    iget-object v0, v1, Lcom/policydm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    .end local v0    # "acl":Lcom/policydm/eng/core/XDMOmAcl;
    check-cast v0, Lcom/policydm/eng/core/XDMOmAcl;

    .line 2023
    .restart local v0    # "acl":Lcom/policydm/eng/core/XDMOmAcl;
    iput p2, v0, Lcom/policydm/eng/core/XDMOmAcl;->ac:I

    .line 2029
    :goto_0
    iput p3, v2, Lcom/policydm/eng/core/XDMVnode;->scope:I

    .line 2035
    :goto_1
    return-void

    .line 2027
    :cond_0
    const-string v3, "ACL is XDM_OMACL_NONE"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 2033
    :cond_1
    const-string v3, "Not Exist"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static xdmAgentParsingWbxml([B)I
    .locals 4
    .param p0, "buf"    # [B

    .prologue
    const/4 v3, 0x0

    .line 273
    sget-object v2, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    .line 274
    .local v2, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    const/4 v1, 0x0

    .line 276
    .local v1, "res":I
    iput-boolean v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->nextMsg:Z

    .line 277
    iput-boolean v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->endOfMsg:Z

    .line 278
    new-instance v0, Lcom/policydm/eng/parser/XDMParser;

    invoke-direct {v0, p0}, Lcom/policydm/eng/parser/XDMParser;-><init>([B)V

    .line 279
    .local v0, "p":Lcom/policydm/eng/parser/XDMParser;
    invoke-static {v2, v0}, Lcom/policydm/agent/XDMAgent;->xdmAgentInitParser(Lcom/policydm/eng/core/XDMWorkspace;Lcom/policydm/eng/parser/XDMParser;)I

    .line 280
    invoke-virtual {v0}, Lcom/policydm/eng/parser/XDMParser;->xdmParParse()I

    move-result v1

    .line 281
    if-eqz v1, :cond_0

    .line 283
    const/4 v3, -0x2

    .line 285
    :cond_0
    return v3
.end method

.method public static xdmAgentReMakeFwUpdateNode(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)I
    .locals 9
    .param p0, "om"    # Lcom/policydm/eng/core/XDMOmTree;
    .param p1, "szFumoNodePath"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x2

    .line 1686
    const/4 v1, 0x0

    .line 1687
    .local v1, "szFUMONode":Ljava/lang/String;
    const/4 v2, 0x0

    .line 1688
    .local v2, "szFUMOPackageNode":Ljava/lang/String;
    const/4 v4, 0x0

    .line 1689
    .local v4, "szTmpbuf":Ljava/lang/String;
    const/4 v5, 0x0

    .line 1691
    .local v5, "tmpbuf_2":[C
    const-string v1, ""

    .line 1692
    const-string v2, ""

    .line 1693
    const-string v4, ""

    .line 1694
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    new-array v5, v6, [C

    .line 1696
    move-object v3, p1

    .line 1697
    .local v3, "szPath":Ljava/lang/String;
    move-object v4, v3

    .line 1701
    :goto_0
    invoke-static {v3, v5}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmMakeParentPath(Ljava/lang/String;[C)V

    .line 1702
    invoke-static {v5}, Lcom/policydm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v4

    .line 1703
    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1705
    const-string v6, "/DownloadAndUpdate"

    invoke-virtual {v4, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    const-string v6, "/Ext"

    invoke-virtual {v4, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 1707
    invoke-static {p0, v4}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v6

    if-nez v6, :cond_0

    .line 1709
    const/16 v6, 0x18

    const/4 v7, 0x1

    invoke-static {p0, v4, v6, v7}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmProcessCmdImplicitAdd(Ljava/lang/Object;Ljava/lang/String;II)Z

    .line 1718
    :cond_0
    move-object v2, v4

    .line 1721
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/PkgName"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1722
    const/16 v0, 0x18

    .line 1723
    .local v0, "aclValue":I
    const-string v6, " "

    invoke-static {p0, v1, v6, v0, v8}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1725
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/PkgVersion"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1726
    const/16 v0, 0x18

    .line 1727
    const-string v6, " "

    invoke-static {p0, v1, v6, v0, v8}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1729
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/PkgSize"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1730
    const/16 v0, 0x18

    .line 1731
    const-string v6, " "

    invoke-static {p0, v1, v6, v0, v8}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1733
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/PkgDesc"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1734
    const/16 v0, 0x18

    .line 1735
    const-string v6, " "

    invoke-static {p0, v1, v6, v0, v8}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1737
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/DownloadAndUpdate"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1738
    const/16 v0, 0xc

    .line 1739
    invoke-static {p0, v1, v0, v8}, Lcom/policydm/agent/XDMAgent;->xdm_SET_OM_PATH(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1741
    const-string v6, "/PkgURL"

    invoke-virtual {v1, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1742
    const/16 v0, 0x18

    .line 1743
    const-string v6, " "

    invoke-static {p0, v1, v6, v0, v8}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1745
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/DownloadAndUpdate"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1746
    const-string v6, "/ProcessId"

    invoke-virtual {v1, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1747
    const/16 v0, 0x18

    .line 1748
    const-string v6, " "

    invoke-static {p0, v1, v6, v0, v8}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1750
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/DownloadAndUpdate"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1751
    const-string v6, "/ReportURL"

    invoke-virtual {v1, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1752
    const/16 v0, 0x18

    .line 1753
    const-string v6, " "

    invoke-static {p0, v1, v6, v0, v8}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1755
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/PushJobId"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1756
    const/16 v0, 0x18

    .line 1757
    const-string v6, " "

    invoke-static {p0, v1, v6, v0, v8}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1759
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/State"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1760
    const/16 v0, 0x8

    .line 1761
    const-string v6, " "

    invoke-static {p0, v1, v6, v0, v8}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1763
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/Ext"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1764
    const/16 v0, 0x8

    .line 1765
    const-string v6, " "

    invoke-static {p0, v1, v6, v0, v8}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1767
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "pFUMONode:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1768
    const/4 v6, 0x0

    return v6

    .line 1713
    .end local v0    # "aclValue":I
    :cond_1
    move-object v3, v4

    goto/16 :goto_0
.end method

.method public static xdmAgentSetOM(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 9
    .param p0, "szPath"    # Ljava/lang/String;
    .param p1, "data"    # Ljava/lang/Object;

    .prologue
    .line 2186
    sget-object v8, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    .line 2189
    .local v8, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    const/4 v2, 0x0

    .line 2191
    .local v2, "nLen":I
    if-nez p1, :cond_1

    .line 2216
    :cond_0
    :goto_0
    return-void

    .line 2196
    :cond_1
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    array-length v2, v0

    .line 2197
    if-gtz v2, :cond_2

    .line 2199
    iget-object v0, v8, Lcom/policydm/eng/core/XDMWorkspace;->om:Lcom/policydm/eng/core/XDMOmTree;

    const/4 v1, 0x1

    invoke-static {v0, p0, v1}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmDeleteImplicit(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;Z)I

    .line 2202
    :cond_2
    iget-object v0, v8, Lcom/policydm/eng/core/XDMWorkspace;->om:Lcom/policydm/eng/core/XDMOmTree;

    const/4 v3, 0x0

    move-object v1, p0

    move-object v4, p1

    move v5, v2

    invoke-static/range {v0 .. v5}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmWrite(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;IILjava/lang/Object;I)I

    .line 2204
    iget-object v0, v8, Lcom/policydm/eng/core/XDMWorkspace;->om:Lcom/policydm/eng/core/XDMOmTree;

    invoke-static {v0, p0}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v7

    .line 2205
    .local v7, "node":Lcom/policydm/eng/core/XDMVnode;
    if-eqz v7, :cond_0

    .line 2207
    iget-object v0, v7, Lcom/policydm/eng/core/XDMVnode;->type:Lcom/policydm/eng/core/XDMOmList;

    if-eqz v0, :cond_3

    .line 2208
    iget-object v0, v7, Lcom/policydm/eng/core/XDMVnode;->type:Lcom/policydm/eng/core/XDMOmList;

    invoke-static {v0}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmVfsDeleteMimeList(Lcom/policydm/eng/core/XDMOmList;)V

    .line 2210
    :cond_3
    new-instance v6, Lcom/policydm/eng/core/XDMOmList;

    invoke-direct {v6}, Lcom/policydm/eng/core/XDMOmList;-><init>()V

    .line 2211
    .local v6, "list":Lcom/policydm/eng/core/XDMOmList;
    const-string v0, "text/plain"

    iput-object v0, v6, Lcom/policydm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    .line 2212
    const/4 v0, 0x0

    iput-object v0, v6, Lcom/policydm/eng/core/XDMOmList;->next:Lcom/policydm/eng/core/XDMOmList;

    .line 2213
    iput-object v6, v7, Lcom/policydm/eng/core/XDMVnode;->type:Lcom/policydm/eng/core/XDMOmList;

    .line 2214
    const/4 v0, 0x4

    iput v0, v7, Lcom/policydm/eng/core/XDMVnode;->format:I

    goto :goto_0
.end method

.method public static xdmAgentSetOMAccBin(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;III)V
    .locals 6
    .param p0, "omt"    # Ljava/lang/Object;
    .param p1, "szPath"    # Ljava/lang/String;
    .param p2, "szData"    # Ljava/lang/String;
    .param p3, "size"    # I
    .param p4, "aclValue"    # I
    .param p5, "scope"    # I

    .prologue
    .line 2102
    move-object v1, p0

    check-cast v1, Lcom/policydm/eng/core/XDMOmTree;

    .line 2106
    .local v1, "om":Lcom/policydm/eng/core/XDMOmTree;
    invoke-static {v1, p1}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v0

    .line 2108
    .local v0, "node":Lcom/policydm/eng/core/XDMVnode;
    if-nez v0, :cond_1

    .line 2110
    invoke-static {p1, p2, p3}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMBin(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2111
    invoke-static {v1, p1, p4, p5}, Lcom/policydm/agent/XDMAgent;->xdmAgentMakeDefaultAcl(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 2128
    :cond_0
    :goto_0
    return-void

    .line 2115
    :cond_1
    iget v4, v0, Lcom/policydm/eng/core/XDMVnode;->size:I

    new-array v3, v4, [C

    .line 2116
    .local v3, "temp":[C
    const/4 v4, 0x0

    iget v5, v0, Lcom/policydm/eng/core/XDMVnode;->size:I

    invoke-static {v1, p1, v4, v3, v5}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmRead(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 2118
    invoke-static {v3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v2

    .line 2120
    .local v2, "szTmp":Ljava/lang/String;
    iget v4, v0, Lcom/policydm/eng/core/XDMVnode;->size:I

    if-eq p3, v4, :cond_2

    .line 2122
    invoke-static {p1, p2, p3}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMBin(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 2124
    :cond_2
    invoke-virtual {v2, p2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-eqz v4, :cond_0

    .line 2126
    invoke-static {p1, p2, p3}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMBin(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public static xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 6
    .param p0, "omt"    # Ljava/lang/Object;
    .param p1, "szPath"    # Ljava/lang/String;
    .param p2, "szData"    # Ljava/lang/String;
    .param p3, "aclValue"    # I
    .param p4, "scope"    # I

    .prologue
    .line 2039
    move-object v1, p0

    check-cast v1, Lcom/policydm/eng/core/XDMOmTree;

    .line 2044
    .local v1, "om":Lcom/policydm/eng/core/XDMOmTree;
    invoke-static {v1, p1}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v0

    .line 2045
    .local v0, "node":Lcom/policydm/eng/core/XDMVnode;
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2047
    const-string p2, ""

    .line 2050
    :cond_0
    if-nez v0, :cond_2

    .line 2052
    invoke-static {p1, p2}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOM(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2053
    invoke-static {v1, p1, p3, p4}, Lcom/policydm/agent/XDMAgent;->xdmAgentMakeDefaultAcl(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 2069
    :cond_1
    :goto_0
    return-void

    .line 2057
    :cond_2
    iget v4, v0, Lcom/policydm/eng/core/XDMVnode;->size:I

    new-array v3, v4, [C

    .line 2058
    .local v3, "temp":[C
    const/4 v4, 0x0

    iget v5, v0, Lcom/policydm/eng/core/XDMVnode;->size:I

    invoke-static {v1, p1, v4, v3, v5}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmRead(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 2060
    invoke-static {v3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v2

    .line 2061
    .local v2, "szTmp":Ljava/lang/String;
    iget v4, v0, Lcom/policydm/eng/core/XDMVnode;->size:I

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v5

    if-eq v4, v5, :cond_3

    .line 2063
    invoke-static {p1, p2}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOM(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 2065
    :cond_3
    invoke-virtual {v2, p2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-eqz v4, :cond_1

    .line 2067
    invoke-static {p1, p2}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOM(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static xdmAgentSetOMBin(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 8
    .param p0, "szPath"    # Ljava/lang/String;
    .param p1, "szData"    # Ljava/lang/String;
    .param p2, "datasize"    # I

    .prologue
    .line 2132
    sget-object v7, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    .line 2135
    .local v7, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    iget-object v0, v7, Lcom/policydm/eng/core/XDMWorkspace;->om:Lcom/policydm/eng/core/XDMOmTree;

    const/4 v3, 0x0

    move-object v1, p0

    move v2, p2

    move-object v4, p1

    move v5, p2

    invoke-static/range {v0 .. v5}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmWrite(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;IILjava/lang/Object;I)I

    .line 2136
    iget-object v0, v7, Lcom/policydm/eng/core/XDMWorkspace;->om:Lcom/policydm/eng/core/XDMOmTree;

    invoke-static {v0, p0}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v6

    .line 2138
    .local v6, "node":Lcom/policydm/eng/core/XDMVnode;
    if-eqz v6, :cond_1

    .line 2140
    iget-object v0, v6, Lcom/policydm/eng/core/XDMVnode;->type:Lcom/policydm/eng/core/XDMOmList;

    if-eqz v0, :cond_0

    .line 2142
    iget-object v0, v6, Lcom/policydm/eng/core/XDMVnode;->type:Lcom/policydm/eng/core/XDMOmList;

    invoke-static {v0}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmVfsDeleteMimeList(Lcom/policydm/eng/core/XDMOmList;)V

    .line 2144
    :cond_0
    const/4 v0, 0x0

    iput-object v0, v6, Lcom/policydm/eng/core/XDMVnode;->type:Lcom/policydm/eng/core/XDMOmList;

    .line 2145
    const/4 v0, 0x2

    iput v0, v6, Lcom/policydm/eng/core/XDMVnode;->format:I

    .line 2147
    :cond_1
    return-void
.end method

.method public static xdmAgentSetSyncMode(I)Z
    .locals 2
    .param p0, "nSync"    # I

    .prologue
    .line 154
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "nSync = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 155
    sput p0, Lcom/policydm/agent/XDMAgent;->m_nDMSync:I

    .line 157
    const/4 v0, 0x1

    return v0
.end method

.method public static xdmAgentSetXNodePath(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 4
    .param p0, "szPath"    # Ljava/lang/String;
    .param p1, "szTarget"    # Ljava/lang/String;
    .param p2, "bTndsFlag"    # Z

    .prologue
    .line 7690
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "target["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "parent["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 7692
    invoke-static {}, Lcom/policydm/agent/XDMAgent;->xdmAgentGetSyncMode()I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_7

    .line 7694
    new-instance v1, Lcom/policydm/eng/core/XDMAccXNode;

    invoke-direct {v1}, Lcom/policydm/eng/core/XDMAccXNode;-><init>()V

    sput-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    .line 7696
    const-string v1, "."

    invoke-virtual {v1, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "./DMAcc"

    invoke-virtual {v1, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1

    .line 7698
    :cond_0
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iput-object p0, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 7699
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    sget-object v2, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v2, Lcom/policydm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 7700
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    sget-object v2, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v2, Lcom/policydm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 7703
    :cond_1
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v0, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 7704
    .local v0, "szTmpBuf":Ljava/lang/String;
    const-string v1, "/ToConRef"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 7706
    invoke-virtual {p0, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_2

    .line 7708
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iput-object p0, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szToConRef:Ljava/lang/String;

    .line 7709
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    sget-object v2, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v2, Lcom/policydm/eng/core/XDMAccXNode;->m_szToConRef:Ljava/lang/String;

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szToConRef:Ljava/lang/String;

    .line 7710
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    sget-object v2, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v2, Lcom/policydm/eng/core/XDMAccXNode;->m_szToConRef:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szToConRef:Ljava/lang/String;

    .line 7713
    :cond_2
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v0, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 7714
    const-string v1, "/AppAddr"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 7715
    invoke-virtual {p0, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_3

    .line 7717
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iput-object p0, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szAppAddr:Ljava/lang/String;

    .line 7718
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    sget-object v2, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v2, Lcom/policydm/eng/core/XDMAccXNode;->m_szAppAddr:Ljava/lang/String;

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szAppAddr:Ljava/lang/String;

    .line 7719
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    sget-object v2, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v2, Lcom/policydm/eng/core/XDMAccXNode;->m_szAppAddr:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szAppAddr:Ljava/lang/String;

    .line 7722
    :cond_3
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v0, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szAppAddr:Ljava/lang/String;

    .line 7723
    const-string v1, "/Port"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 7724
    invoke-virtual {p0, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_4

    .line 7726
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iput-object p0, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szAppAddrPort:Ljava/lang/String;

    .line 7727
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    sget-object v2, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v2, Lcom/policydm/eng/core/XDMAccXNode;->m_szAppAddrPort:Ljava/lang/String;

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szAppAddrPort:Ljava/lang/String;

    .line 7728
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    sget-object v2, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v2, Lcom/policydm/eng/core/XDMAccXNode;->m_szAppAddrPort:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szAppAddrPort:Ljava/lang/String;

    .line 7731
    :cond_4
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v0, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 7732
    const-string v1, "/AppAuth"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 7734
    invoke-virtual {p0, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_5

    .line 7737
    const-string v1, "ClientSide"

    invoke-virtual {p1, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_6

    .line 7739
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iput-object p0, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 7740
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    sget-object v2, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v2, Lcom/policydm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 7741
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    sget-object v2, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v2, Lcom/policydm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 7851
    :cond_5
    :goto_0
    return-void

    .line 7745
    :cond_6
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iput-object p0, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    .line 7746
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    sget-object v2, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v2, Lcom/policydm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    .line 7747
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    sget-object v2, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v2, Lcom/policydm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    goto :goto_0

    .line 7753
    .end local v0    # "szTmpBuf":Ljava/lang/String;
    :cond_7
    if-eqz p2, :cond_e

    .line 7755
    const-string v1, "."

    invoke-virtual {v1, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_8

    const-string v1, "./DMAcc"

    invoke-virtual {v1, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_9

    .line 7757
    :cond_8
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iput-object p0, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 7758
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    sget-object v2, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v2, Lcom/policydm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 7759
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    sget-object v2, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v2, Lcom/policydm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 7762
    :cond_9
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v0, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 7763
    .restart local v0    # "szTmpBuf":Ljava/lang/String;
    const-string v1, "/ToConRef"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 7764
    invoke-virtual {p0, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_a

    .line 7766
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iput-object p0, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szToConRef:Ljava/lang/String;

    .line 7767
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    sget-object v2, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v2, Lcom/policydm/eng/core/XDMAccXNode;->m_szToConRef:Ljava/lang/String;

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szToConRef:Ljava/lang/String;

    .line 7768
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    sget-object v2, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v2, Lcom/policydm/eng/core/XDMAccXNode;->m_szToConRef:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szToConRef:Ljava/lang/String;

    .line 7771
    :cond_a
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v0, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 7772
    const-string v1, "/AppAddr"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 7773
    invoke-virtual {p0, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_b

    .line 7775
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iput-object p0, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szAppAddr:Ljava/lang/String;

    .line 7776
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    sget-object v2, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v2, Lcom/policydm/eng/core/XDMAccXNode;->m_szAppAddr:Ljava/lang/String;

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szAppAddr:Ljava/lang/String;

    .line 7777
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    sget-object v2, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v2, Lcom/policydm/eng/core/XDMAccXNode;->m_szAppAddr:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szAppAddr:Ljava/lang/String;

    .line 7780
    :cond_b
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v0, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szAppAddr:Ljava/lang/String;

    .line 7781
    const-string v1, "/Port"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 7782
    invoke-virtual {p0, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_c

    .line 7784
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iput-object p0, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szAppAddrPort:Ljava/lang/String;

    .line 7785
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    sget-object v2, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v2, Lcom/policydm/eng/core/XDMAccXNode;->m_szAppAddrPort:Ljava/lang/String;

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szAppAddrPort:Ljava/lang/String;

    .line 7786
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    sget-object v2, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v2, Lcom/policydm/eng/core/XDMAccXNode;->m_szAppAddrPort:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szAppAddrPort:Ljava/lang/String;

    .line 7789
    :cond_c
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v0, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 7790
    const-string v1, "/AppAuth"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 7791
    invoke-virtual {p0, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_5

    .line 7793
    const-string v1, "ClientSide"

    invoke-virtual {v1, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_d

    .line 7795
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iput-object p0, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 7796
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    sget-object v2, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v2, Lcom/policydm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 7797
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    sget-object v2, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v2, Lcom/policydm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    goto/16 :goto_0

    .line 7801
    :cond_d
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iput-object p0, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    .line 7802
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    sget-object v2, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v2, Lcom/policydm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    .line 7803
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    sget-object v2, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v2, Lcom/policydm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    goto/16 :goto_0

    .line 7809
    .end local v0    # "szTmpBuf":Ljava/lang/String;
    :cond_e
    const-string v1, "."

    invoke-virtual {v1, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_f

    const-string v1, "./DMAcc"

    invoke-virtual {v1, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_10

    .line 7811
    :cond_f
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iput-object p1, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 7814
    :cond_10
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v0, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 7815
    .restart local v0    # "szTmpBuf":Ljava/lang/String;
    const-string v1, "/ToConRef"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 7816
    invoke-virtual {p0, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_11

    .line 7818
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iput-object p1, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szToConRef:Ljava/lang/String;

    .line 7821
    :cond_11
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v0, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 7822
    const-string v1, "/AppAddr"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 7823
    invoke-virtual {p0, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_12

    .line 7825
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iput-object p1, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szAppAddr:Ljava/lang/String;

    .line 7828
    :cond_12
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v0, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szAppAddr:Ljava/lang/String;

    .line 7829
    const-string v1, "/Port"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 7830
    invoke-virtual {p0, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_13

    .line 7832
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iput-object p1, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szAppAddrPort:Ljava/lang/String;

    .line 7835
    :cond_13
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v0, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 7836
    const-string v1, "/AppAuth"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 7837
    invoke-virtual {p0, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_5

    .line 7840
    const-string v1, "ClientSide"

    invoke-virtual {v1, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_14

    .line 7842
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iput-object p1, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    goto/16 :goto_0

    .line 7846
    :cond_14
    sget-object v1, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iput-object p1, v1, Lcom/policydm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method public static xdmAgentTpSetRetryCount(I)V
    .locals 0
    .param p0, "nCnt"    # I

    .prologue
    .line 7952
    sput p0, Lcom/policydm/agent/XDMAgent;->m_nConnectRetryCount:I

    .line 7953
    return-void
.end method

.method public static xdm_SET_OM_PATH(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;II)V
    .locals 6
    .param p0, "om"    # Lcom/policydm/eng/core/XDMOmTree;
    .param p1, "szPath"    # Ljava/lang/String;
    .param p2, "aclValue"    # I
    .param p3, "scope"    # I

    .prologue
    const/4 v2, 0x0

    .line 1993
    invoke-static {p0, p1}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1995
    const-string v4, ""

    move-object v0, p0

    move-object v1, p1

    move v3, v2

    move v5, v2

    invoke-static/range {v0 .. v5}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmWrite(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;IILjava/lang/Object;I)I

    .line 1996
    invoke-static {p0, p1, p2, p3}, Lcom/policydm/agent/XDMAgent;->xdmAgentMakeDefaultAcl(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1998
    :cond_0
    return-void
.end method


# virtual methods
.method public xdmAgentAppendAclItem(Lcom/policydm/eng/core/XDMOmList;Ljava/lang/String;I)Lcom/policydm/eng/core/XDMOmList;
    .locals 11
    .param p1, "acllist"    # Lcom/policydm/eng/core/XDMOmList;
    .param p2, "szAclValue"    # Ljava/lang/String;
    .param p3, "aclflag"    # I

    .prologue
    .line 4265
    move-object v4, p2

    .line 4268
    .local v4, "szData":Ljava/lang/String;
    const/4 v1, 0x0

    .line 4270
    .local v1, "buf":[C
    const/4 v3, 0x0

    .line 4272
    .local v3, "found":Z
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    new-array v1, v7, [C

    .line 4274
    const-string v7, "*"

    invoke-virtual {v4, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 4275
    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v7

    const/16 v8, 0x2b

    invoke-static {v7, v8, v1}, Lcom/policydm/eng/core/XDMMem;->xdmLibStrsplit([CC[C)Ljava/lang/String;

    move-result-object v4

    .line 4281
    :goto_0
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 4284
    move-object v2, p1

    .line 4285
    .local v2, "cur":Lcom/policydm/eng/core/XDMOmList;
    :goto_1
    if-eqz v2, :cond_2

    iget-object v7, v2, Lcom/policydm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    if-eqz v7, :cond_2

    .line 4287
    iget-object v0, v2, Lcom/policydm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    check-cast v0, Lcom/policydm/eng/core/XDMOmAcl;

    .line 4289
    .local v0, "acl":Lcom/policydm/eng/core/XDMOmAcl;
    iget-object v7, v0, Lcom/policydm/eng/core/XDMOmAcl;->m_szServerid:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v7

    if-nez v7, :cond_0

    .line 4291
    const/4 v3, 0x1

    .line 4292
    iget v7, v0, Lcom/policydm/eng/core/XDMOmAcl;->ac:I

    or-int/2addr v7, p3

    iput v7, v0, Lcom/policydm/eng/core/XDMOmAcl;->ac:I

    .line 4295
    :cond_0
    iget-object v2, v2, Lcom/policydm/eng/core/XDMOmList;->next:Lcom/policydm/eng/core/XDMOmList;

    goto :goto_1

    .line 4278
    .end local v0    # "acl":Lcom/policydm/eng/core/XDMOmAcl;
    .end local v2    # "cur":Lcom/policydm/eng/core/XDMOmList;
    :cond_1
    const/4 v7, 0x0

    const/16 v8, 0x2a

    aput-char v8, v1, v7

    .line 4279
    const/4 v7, 0x1

    const/4 v8, 0x0

    aput-char v8, v1, v7

    goto :goto_0

    .line 4298
    .restart local v2    # "cur":Lcom/policydm/eng/core/XDMOmList;
    :cond_2
    if-nez v3, :cond_3

    .line 4300
    const/16 v7, 0x28

    new-array v6, v7, [C

    .line 4301
    .local v6, "tmp1":[C
    new-instance v0, Lcom/policydm/eng/core/XDMOmAcl;

    invoke-direct {v0}, Lcom/policydm/eng/core/XDMOmAcl;-><init>()V

    .line 4303
    .restart local v0    # "acl":Lcom/policydm/eng/core/XDMOmAcl;
    invoke-static {v1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v7

    const-string v8, "*"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 4304
    invoke-static {v1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/16 v9, 0x27

    const/4 v10, 0x0

    invoke-virtual {v7, v8, v9, v6, v10}, Ljava/lang/String;->getChars(II[CI)V

    .line 4310
    :goto_2
    invoke-static {v6}, Lcom/policydm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v0, Lcom/policydm/eng/core/XDMOmAcl;->m_szServerid:Ljava/lang/String;

    .line 4312
    iget v7, v0, Lcom/policydm/eng/core/XDMOmAcl;->ac:I

    or-int/2addr v7, p3

    iput v7, v0, Lcom/policydm/eng/core/XDMOmAcl;->ac:I

    .line 4313
    new-instance v5, Lcom/policydm/eng/core/XDMOmList;

    invoke-direct {v5}, Lcom/policydm/eng/core/XDMOmList;-><init>()V

    .line 4314
    .local v5, "tmp":Lcom/policydm/eng/core/XDMOmList;
    iput-object v0, v5, Lcom/policydm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    .line 4315
    const/4 v7, 0x0

    iput-object v7, v5, Lcom/policydm/eng/core/XDMOmList;->next:Lcom/policydm/eng/core/XDMOmList;

    .line 4316
    invoke-static {p1, v5}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmVfsAppendList(Lcom/policydm/eng/core/XDMOmList;Lcom/policydm/eng/core/XDMOmList;)Lcom/policydm/eng/core/XDMOmList;

    move-result-object p1

    .line 4318
    .end local v0    # "acl":Lcom/policydm/eng/core/XDMOmAcl;
    .end local v5    # "tmp":Lcom/policydm/eng/core/XDMOmList;
    .end local v6    # "tmp1":[C
    :cond_3
    const/4 v1, 0x0

    .line 4319
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    new-array v1, v7, [C

    .line 4320
    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v7

    const/16 v8, 0x2b

    invoke-static {v7, v8, v1}, Lcom/policydm/eng/core/XDMMem;->xdmLibStrsplit([CC[C)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 4307
    .restart local v0    # "acl":Lcom/policydm/eng/core/XDMOmAcl;
    .restart local v6    # "tmp1":[C
    :cond_4
    const/4 v7, 0x0

    const/16 v8, 0x2a

    aput-char v8, v6, v7

    .line 4308
    const/4 v7, 0x1

    const/4 v8, 0x0

    aput-char v8, v6, v7

    goto :goto_2

    .line 4322
    .end local v0    # "acl":Lcom/policydm/eng/core/XDMOmAcl;
    .end local v2    # "cur":Lcom/policydm/eng/core/XDMOmList;
    .end local v6    # "tmp1":[C
    :cond_5
    return-object p1
.end method

.method public xdmAgentClientInitPackage(Lcom/policydm/eng/core/XDMEncoder;)I
    .locals 7
    .param p1, "e"    # Lcom/policydm/eng/core/XDMEncoder;

    .prologue
    const/4 v3, -0x1

    const/4 v6, -0x3

    const/4 v4, 0x0

    .line 2220
    sget-object v2, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    .line 2225
    .local v2, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    const-string v5, ""

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 2226
    invoke-virtual {p0, p1}, Lcom/policydm/agent/XDMAgent;->xdmAgentCreatePackageStatus(Lcom/policydm/eng/core/XDMEncoder;)I

    move-result v1

    .line 2227
    .local v1, "res":I
    if-eqz v1, :cond_1

    .line 2229
    if-ne v1, v6, :cond_0

    .line 2231
    iput-boolean v4, v2, Lcom/policydm/eng/core/XDMWorkspace;->endOfMsg:Z

    .line 2293
    :goto_0
    return v3

    .line 2235
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "failed("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 2240
    :cond_1
    invoke-virtual {p0, p1}, Lcom/policydm/agent/XDMAgent;->xdmAgentCreatePackageResults(Lcom/policydm/eng/core/XDMEncoder;)I

    move-result v1

    .line 2241
    if-eqz v1, :cond_3

    .line 2243
    if-ne v1, v6, :cond_2

    .line 2245
    iput-boolean v4, v2, Lcom/policydm/eng/core/XDMWorkspace;->endOfMsg:Z

    goto :goto_0

    .line 2249
    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "failed("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 2254
    :cond_3
    invoke-static {}, Lcom/policydm/db/XDBProfileListAdp;->xdbGetNotiEvent()I

    move-result v0

    .line 2255
    .local v0, "nNotiEvent":I
    if-lez v0, :cond_4

    .line 2257
    const-string v5, "1200"

    invoke-virtual {p0, p1, v5}, Lcom/policydm/agent/XDMAgent;->xdmAgentCreatePackageAlert(Lcom/policydm/eng/core/XDMEncoder;Ljava/lang/String;)I

    move-result v1

    .line 2264
    :goto_1
    if-eqz v1, :cond_6

    .line 2266
    if-ne v1, v6, :cond_5

    .line 2268
    iput-boolean v4, v2, Lcom/policydm/eng/core/XDMWorkspace;->endOfMsg:Z

    goto :goto_0

    .line 2261
    :cond_4
    const-string v5, "1201"

    invoke-virtual {p0, p1, v5}, Lcom/policydm/agent/XDMAgent;->xdmAgentCreatePackageAlert(Lcom/policydm/eng/core/XDMEncoder;Ljava/lang/String;)I

    move-result v1

    goto :goto_1

    .line 2272
    :cond_5
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "failed("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 2277
    :cond_6
    invoke-virtual {p0, p1}, Lcom/policydm/agent/XDMAgent;->xdmAgentCreatePackageDevInfo(Lcom/policydm/eng/core/XDMEncoder;)I

    move-result v1

    .line 2278
    if-eqz v1, :cond_8

    .line 2280
    if-ne v1, v6, :cond_7

    .line 2282
    iput-boolean v4, v2, Lcom/policydm/eng/core/XDMWorkspace;->endOfMsg:Z

    goto/16 :goto_0

    .line 2286
    :cond_7
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "failed("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2291
    :cond_8
    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->endOfMsg:Z

    move v3, v4

    .line 2293
    goto/16 :goto_0
.end method

.method public xdmAgentCmdAdd(Lcom/policydm/eng/parser/XDMParserAdd;ZLcom/policydm/eng/parser/XDMParserStatus;)I
    .locals 33
    .param p1, "add"    # Lcom/policydm/eng/parser/XDMParserAdd;
    .param p2, "isAtomic"    # Z
    .param p3, "atomic_status"    # Lcom/policydm/eng/parser/XDMParserStatus;

    .prologue
    .line 5431
    sget-object v3, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    .line 5432
    .local v3, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    const/4 v14, 0x0

    .line 5433
    .local v14, "item":Lcom/policydm/eng/parser/XDMParserItem;
    const/16 v23, 0x0

    .line 5434
    .local v23, "status":Lcom/policydm/eng/parser/XDMParserStatus;
    iget-object v0, v3, Lcom/policydm/eng/core/XDMWorkspace;->om:Lcom/policydm/eng/core/XDMOmTree;

    move-object/from16 v18, v0

    .line 5435
    .local v18, "om":Lcom/policydm/eng/core/XDMOmTree;
    const/16 v24, 0x0

    .line 5436
    .local v24, "szBuf":Ljava/lang/String;
    const/16 v30, 0x0

    .line 5437
    .local v30, "szType":Ljava/lang/String;
    const/16 v13, 0xc

    .line 5439
    .local v13, "format":I
    const/4 v12, 0x0

    .line 5440
    .local v12, "cur":Lcom/policydm/eng/core/XDMList;
    const/16 v4, 0x100

    new-array v0, v4, [C

    move-object/from16 v32, v0

    .line 5441
    .local v32, "tmpbuf":[C
    const/16 v27, 0x0

    .line 5443
    .local v27, "szNodename":Ljava/lang/String;
    const/4 v9, 0x0

    .line 5444
    .local v9, "bufsize":I
    const/16 v22, 0x0

    .line 5446
    .local v22, "res":I
    const/16 v25, 0x0

    .line 5447
    .local v25, "szInbox":Ljava/lang/String;
    const/4 v15, 0x0

    .line 5449
    .local v15, "nFileId":I
    const/16 v28, 0x0

    .line 5450
    .local v28, "szOutBuf":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/policydm/agent/XDMAgent;->xdmAgentCmdUicAlert()Z

    move-result v21

    .line 5453
    .local v21, "process":Z
    invoke-static {}, Lcom/policydm/db/XDB;->xdbGetFileIdTNDS()I

    move-result v15

    .line 5455
    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/policydm/eng/parser/XDMParserAdd;->itemlist:Lcom/policydm/eng/core/XDMList;

    .line 5456
    :cond_0
    :goto_0
    if-eqz v12, :cond_41

    .line 5458
    const/16 v30, 0x0

    .line 5459
    const/16 v13, 0xc

    .line 5460
    iget-object v14, v12, Lcom/policydm/eng/core/XDMList;->item:Ljava/lang/Object;

    .end local v14    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    check-cast v14, Lcom/policydm/eng/parser/XDMParserItem;

    .line 5462
    .restart local v14    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    iget-object v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    if-nez v4, :cond_2

    .line 5464
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/policydm/eng/parser/XDMParserAdd;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    if-eqz v4, :cond_1

    .line 5465
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/policydm/eng/parser/XDMParserAdd;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iput-object v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    .line 5485
    :cond_1
    :goto_1
    iget v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->serverAuthState:I

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    .line 5487
    iget-object v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 5489
    move-object/from16 v0, p1

    iget v4, v0, Lcom/policydm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    iget-object v8, v3, Lcom/policydm/eng/core/XDMWorkspace;->m_szStatusReturnCode:Ljava/lang/String;

    invoke-static/range {v3 .. v8}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v23

    .line 5495
    :goto_2
    iget-object v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 5496
    iget-object v12, v12, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 5497
    goto :goto_0

    .line 5469
    :cond_2
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/policydm/eng/parser/XDMParserAdd;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    if-eqz v4, :cond_1

    .line 5471
    iget-object v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v4, v4, Lcom/policydm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 5473
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/policydm/eng/parser/XDMParserAdd;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v4, v4, Lcom/policydm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 5474
    iget-object v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/policydm/eng/parser/XDMParserAdd;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v5, v5, Lcom/policydm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    iput-object v5, v4, Lcom/policydm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    .line 5477
    :cond_3
    iget-object v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v4, v4, Lcom/policydm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 5479
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/policydm/eng/parser/XDMParserAdd;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v4, v4, Lcom/policydm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 5480
    iget-object v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/policydm/eng/parser/XDMParserAdd;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v5, v5, Lcom/policydm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    iput-object v5, v4, Lcom/policydm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    goto :goto_1

    .line 5493
    :cond_4
    move-object/from16 v0, p1

    iget v4, v0, Lcom/policydm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    const/4 v7, 0x0

    iget-object v8, v3, Lcom/policydm/eng/core/XDMWorkspace;->m_szStatusReturnCode:Ljava/lang/String;

    invoke-static/range {v3 .. v8}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v23

    goto :goto_2

    .line 5499
    :cond_5
    if-nez v21, :cond_8

    .line 5501
    iget v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->moredata:I

    if-lez v4, :cond_6

    .line 5503
    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 5506
    :cond_6
    iget-object v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 5508
    move-object/from16 v0, p1

    iget v4, v0, Lcom/policydm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "215"

    invoke-static/range {v3 .. v8}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v23

    .line 5515
    :goto_3
    iget-object v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 5516
    iget-object v12, v12, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 5517
    goto/16 :goto_0

    .line 5512
    :cond_7
    move-object/from16 v0, p1

    iget v4, v0, Lcom/policydm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-string v8, "215"

    invoke-static/range {v3 .. v8}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v23

    goto :goto_3

    .line 5521
    :cond_8
    if-eqz p2, :cond_10

    iget-object v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->atomicStep:Lcom/policydm/interfaces/XDMInterface$XDMAtomicStep;

    sget-object v5, Lcom/policydm/interfaces/XDMInterface$XDMAtomicStep;->XDM_ATOMIC_NONE:Lcom/policydm/interfaces/XDMInterface$XDMAtomicStep;

    if-eq v4, v5, :cond_10

    .line 5523
    iget-object v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->tmpItem:Lcom/policydm/eng/parser/XDMParserItem;

    if-eqz v4, :cond_c

    .line 5526
    iget-object v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->tmpItem:Lcom/policydm/eng/parser/XDMParserItem;

    invoke-virtual {v4, v14}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 5528
    iget-object v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_9

    .line 5530
    move-object/from16 v0, p1

    iget v4, v0, Lcom/policydm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "418"

    invoke-static/range {v3 .. v8}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v23

    .line 5537
    :goto_4
    sget-object v4, Lcom/policydm/interfaces/XDMInterface$XDMAtomicStep;->XDM_ATOMIC_STEP_NOT_EXEC:Lcom/policydm/interfaces/XDMInterface$XDMAtomicStep;

    iput-object v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->atomicStep:Lcom/policydm/interfaces/XDMInterface$XDMAtomicStep;

    .line 5538
    const/4 v4, 0x0

    iput-object v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->tmpItem:Lcom/policydm/eng/parser/XDMParserItem;

    .line 5577
    :goto_5
    iget-object v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 5578
    iget-object v12, v12, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 5579
    goto/16 :goto_0

    .line 5534
    :cond_9
    move-object/from16 v0, p1

    iget v4, v0, Lcom/policydm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-string v8, "418"

    invoke-static/range {v3 .. v8}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v23

    goto :goto_4

    .line 5542
    :cond_a
    iget-object v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_b

    .line 5544
    move-object/from16 v0, p1

    iget v4, v0, Lcom/policydm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "216"

    invoke-static/range {v3 .. v8}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v23

    goto :goto_5

    .line 5548
    :cond_b
    move-object/from16 v0, p1

    iget v4, v0, Lcom/policydm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-string v8, "216"

    invoke-static/range {v3 .. v8}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v23

    goto :goto_5

    .line 5554
    :cond_c
    iget-object v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->atomicStep:Lcom/policydm/interfaces/XDMInterface$XDMAtomicStep;

    sget-object v5, Lcom/policydm/interfaces/XDMInterface$XDMAtomicStep;->XDM_ATOMIC_STEP_NOT_EXEC:Lcom/policydm/interfaces/XDMInterface$XDMAtomicStep;

    if-ne v4, v5, :cond_e

    .line 5556
    iget-object v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_d

    .line 5558
    move-object/from16 v0, p1

    iget v4, v0, Lcom/policydm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "215"

    invoke-static/range {v3 .. v8}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v23

    goto :goto_5

    .line 5562
    :cond_d
    move-object/from16 v0, p1

    iget v4, v0, Lcom/policydm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-string v8, "215"

    invoke-static/range {v3 .. v8}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v23

    goto :goto_5

    .line 5567
    :cond_e
    iget-object v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_f

    .line 5569
    move-object/from16 v0, p1

    iget v4, v0, Lcom/policydm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "216"

    invoke-static/range {v3 .. v8}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v23

    goto/16 :goto_5

    .line 5573
    :cond_f
    move-object/from16 v0, p1

    iget v4, v0, Lcom/policydm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-string v8, "216"

    invoke-static/range {v3 .. v8}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v23

    goto/16 :goto_5

    .line 5582
    :cond_10
    iget-object v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_11

    .line 5584
    move-object/from16 v0, p1

    iget v4, v0, Lcom/policydm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-string v8, "403"

    invoke-static/range {v3 .. v8}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v23

    .line 5585
    iget-object v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 5586
    iget-object v12, v12, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 5588
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 5590
    const-string v4, "507"

    move-object/from16 v0, p3

    iput-object v4, v0, Lcom/policydm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    goto/16 :goto_0

    .line 5595
    :cond_11
    iget-object v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 5600
    invoke-static {}, Lcom/policydm/agent/XDMAgent;->xdmAgentGetSyncMode()I

    move-result v4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_14

    .line 5602
    iget-object v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-static {v0, v4}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v17

    .line 5603
    .local v17, "node":Lcom/policydm/eng/core/XDMVnode;
    if-eqz v17, :cond_15

    iget v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->moredata:I

    if-nez v4, :cond_15

    iget-boolean v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->dataBuffered:Z

    if-nez v4, :cond_15

    .line 5605
    const/16 v4, 0xa

    invoke-static {v4}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFGetMOPath(I)Ljava/lang/String;

    move-result-object v25

    .line 5606
    invoke-static/range {v25 .. v25}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_12

    .line 5608
    move-object/from16 v0, p1

    iget v4, v0, Lcom/policydm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "418"

    invoke-static/range {v3 .. v8}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v23

    .line 5609
    iget-object v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 5610
    iget-object v12, v12, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 5614
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 5616
    const-string v4, "507"

    move-object/from16 v0, p3

    iput-object v4, v0, Lcom/policydm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    goto/16 :goto_0

    .line 5623
    :cond_12
    iget-object v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v29

    .line 5624
    .local v29, "szTmp":Ljava/lang/String;
    move-object/from16 v0, v29

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-eqz v4, :cond_13

    .line 5627
    move-object/from16 v0, p1

    iget v4, v0, Lcom/policydm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "418"

    invoke-static/range {v3 .. v8}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v23

    .line 5628
    iget-object v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 5629
    iget-object v12, v12, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 5633
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 5635
    const-string v4, "507"

    move-object/from16 v0, p3

    iput-object v4, v0, Lcom/policydm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    goto/16 :goto_0

    .line 5643
    :cond_13
    move-object/from16 v0, p1

    iget v4, v0, Lcom/policydm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "418"

    invoke-static/range {v3 .. v8}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v23

    .line 5644
    iget-object v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 5645
    iget-object v12, v12, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 5646
    const-string v4, "node already Existed[418]"

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 5650
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 5652
    const-string v4, "507"

    move-object/from16 v0, p3

    iput-object v4, v0, Lcom/policydm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    goto/16 :goto_0

    .line 5661
    .end local v17    # "node":Lcom/policydm/eng/core/XDMVnode;
    .end local v29    # "szTmp":Ljava/lang/String;
    :cond_14
    iget-object v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    move-object/from16 v0, v32

    invoke-static {v4, v0}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmMakeParentPath(Ljava/lang/String;[C)V

    .line 5662
    invoke-static/range {v32 .. v32}, Lcom/policydm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v26

    .line 5663
    .local v26, "szName":Ljava/lang/String;
    iget-object v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const/4 v5, 0x0

    move-object/from16 v0, v26

    invoke-static {v0, v4, v5}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetXNodePath(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 5666
    .end local v26    # "szName":Ljava/lang/String;
    :cond_15
    iget-object v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    move-object/from16 v0, v32

    invoke-static {v4, v0}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmMakeParentPath(Ljava/lang/String;[C)V

    .line 5667
    invoke-static/range {v32 .. v32}, Lcom/policydm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v27

    .line 5669
    move-object/from16 v0, v18

    move-object/from16 v1, v27

    invoke-static {v0, v1}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v17

    .line 5670
    .restart local v17    # "node":Lcom/policydm/eng/core/XDMVnode;
    if-nez v17, :cond_17

    .line 5674
    const/4 v11, 0x0

    .line 5676
    .local v11, "bResultImplicitAdd":Z
    const/16 v4, 0x1b

    const/4 v5, 0x1

    move-object/from16 v0, v18

    move-object/from16 v1, v27

    invoke-static {v0, v1, v4, v5}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmProcessCmdImplicitAdd(Ljava/lang/Object;Ljava/lang/String;II)Z

    move-result v11

    .line 5678
    if-nez v11, :cond_0

    .line 5680
    iget v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->moredata:I

    if-lez v4, :cond_16

    .line 5682
    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 5685
    :cond_16
    move-object/from16 v0, p1

    iget v4, v0, Lcom/policydm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "500"

    invoke-static/range {v3 .. v8}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v23

    .line 5686
    iget-object v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 5687
    iget-object v12, v12, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 5689
    const-string v4, "Node depth is over 15  Command failed 500"

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 5691
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 5693
    const-string v4, "507"

    move-object/from16 v0, p3

    iput-object v4, v0, Lcom/policydm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    goto/16 :goto_0

    .line 5700
    .end local v11    # "bResultImplicitAdd":Z
    :cond_17
    const/4 v4, 0x1

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-static {v0, v1, v4}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmCheckAcl(Lcom/policydm/eng/core/XDMOmTree;Lcom/policydm/eng/core/XDMVnode;I)Z

    move-result v4

    if-nez v4, :cond_19

    .line 5703
    iget v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->moredata:I

    if-lez v4, :cond_18

    .line 5704
    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 5706
    :cond_18
    move-object/from16 v0, p1

    iget v4, v0, Lcom/policydm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "425"

    invoke-static/range {v3 .. v8}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v23

    .line 5707
    iget-object v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 5708
    iget-object v12, v12, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 5712
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 5714
    const-string v4, "507"

    move-object/from16 v0, p3

    iput-object v4, v0, Lcom/policydm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    goto/16 :goto_0

    .line 5721
    :cond_19
    iget-object v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    if-eqz v4, :cond_1b

    iget-object v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v4, v4, Lcom/policydm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1b

    const-string v4, "node"

    iget-object v5, v14, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v5, v5, Lcom/policydm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_1b

    .line 5723
    const/16 v24, 0x0

    .line 5724
    const/4 v9, 0x0

    .line 5725
    const/4 v4, 0x0

    iput v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->dataTotalSize:I

    .line 5726
    const/4 v13, 0x6

    .line 5727
    iget-object v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v4, v4, Lcom/policydm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1a

    .line 5729
    iget-object v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v0, v4, Lcom/policydm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    move-object/from16 v30, v0

    .line 5819
    :cond_1a
    :goto_6
    iget-boolean v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->nTNDSFlag:Z

    if-eqz v4, :cond_2d

    .line 5822
    invoke-static/range {v30 .. v30}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2d

    invoke-static/range {v24 .. v24}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2d

    .line 5824
    const-string v4, "application/vnd.syncml.dmtnds+xml"

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_29

    .line 5826
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v4

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v4

    invoke-static {v15, v4}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpAppendFile(I[B)I

    .line 5827
    const/16 v24, 0x0

    .line 5828
    const/16 v30, 0x0

    .line 5829
    iget v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->moredata:I

    if-nez v4, :cond_28

    .line 5831
    const/4 v4, 0x0

    iput v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->prevBufPos:I

    .line 5832
    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 5833
    const/4 v4, 0x0

    iput v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->dataTotalSize:I

    .line 5834
    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->nTNDSFlag:Z

    .line 5835
    move-object/from16 v0, v18

    invoke-static {v15, v0}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFCreateTNDSNodeFromFile(ILcom/policydm/eng/core/XDMOmTree;)I

    move-result v22

    .line 5837
    if-lez v22, :cond_27

    .line 5839
    move-object/from16 v0, p1

    iget v4, v0, Lcom/policydm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "200"

    invoke-static/range {v3 .. v8}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v23

    .line 5840
    iget-object v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 5841
    iget-object v12, v12, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 5842
    goto/16 :goto_0

    .line 5732
    :cond_1b
    iget-object v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    if-eqz v4, :cond_23

    .line 5734
    iget-object v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    if-eqz v4, :cond_1c

    iget-object v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    iget-object v4, v4, Lcom/policydm/eng/parser/XDMParserPcdata;->data:[C

    if-eqz v4, :cond_1c

    .line 5736
    iget-object v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    invoke-static {v4}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStGetString(Lcom/policydm/eng/parser/XDMParserPcdata;)Ljava/lang/String;

    move-result-object v24

    .line 5737
    invoke-static/range {v24 .. v24}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1f

    .line 5739
    iget-object v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    iget v9, v4, Lcom/policydm/eng/parser/XDMParserPcdata;->size:I

    .line 5740
    iget-object v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    iget-object v4, v4, Lcom/policydm/eng/parser/XDMParserPcdata;->data:[C

    invoke-static {v4}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v24

    .line 5747
    :cond_1c
    :goto_7
    iget-object v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget v4, v4, Lcom/policydm/eng/parser/XDMParserMeta;->size:I

    if-lez v4, :cond_20

    .line 5749
    iget-object v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget v4, v4, Lcom/policydm/eng/parser/XDMParserMeta;->size:I

    iput v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->dataTotalSize:I

    .line 5750
    const/4 v4, 0x0

    iput v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->prevBufPos:I

    .line 5772
    :cond_1d
    :goto_8
    iget-object v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v4, v4, Lcom/policydm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1e

    .line 5774
    iget-object v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v0, v4, Lcom/policydm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    move-object/from16 v30, v0

    .line 5776
    :cond_1e
    iget-object v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v4, v4, Lcom/policydm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1a

    .line 5778
    iget-object v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v4, v4, Lcom/policydm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    invoke-static {v4}, Lcom/policydm/eng/core/XDMOmList;->xdmOmGetFormatFromString(Ljava/lang/String;)I

    move-result v13

    goto/16 :goto_6

    .line 5744
    :cond_1f
    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->length()I

    move-result v9

    goto :goto_7

    .line 5752
    :cond_20
    iget v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->prevBufPos:I

    if-nez v4, :cond_1d

    .line 5756
    iget-object v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    if-eqz v4, :cond_22

    .line 5758
    iget-object v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    iget v4, v4, Lcom/policydm/eng/parser/XDMParserPcdata;->size:I

    if-lez v4, :cond_21

    .line 5760
    iget-object v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    iget v4, v4, Lcom/policydm/eng/parser/XDMParserPcdata;->size:I

    iput v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->dataTotalSize:I

    goto :goto_8

    .line 5764
    :cond_21
    iput v9, v3, Lcom/policydm/eng/core/XDMWorkspace;->dataTotalSize:I

    goto :goto_8

    .line 5769
    :cond_22
    iput v9, v3, Lcom/policydm/eng/core/XDMWorkspace;->dataTotalSize:I

    goto :goto_8

    .line 5784
    :cond_23
    iget-object v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    if-eqz v4, :cond_26

    iget-object v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    iget-object v4, v4, Lcom/policydm/eng/parser/XDMParserPcdata;->data:[C

    if-eqz v4, :cond_26

    .line 5786
    iget-object v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    invoke-static {v4}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStGetString(Lcom/policydm/eng/parser/XDMParserPcdata;)Ljava/lang/String;

    move-result-object v24

    .line 5787
    invoke-static/range {v24 .. v24}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_25

    .line 5789
    iget-object v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    iget v9, v4, Lcom/policydm/eng/parser/XDMParserPcdata;->size:I

    .line 5790
    iget-object v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    iget-object v4, v4, Lcom/policydm/eng/parser/XDMParserPcdata;->data:[C

    if-eqz v4, :cond_24

    .line 5791
    iget-object v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    iget-object v4, v4, Lcom/policydm/eng/parser/XDMParserPcdata;->data:[C

    invoke-static {v4}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v24

    .line 5800
    :goto_9
    iget-boolean v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->dataBuffered:Z

    if-nez v4, :cond_1a

    .line 5802
    iput v9, v3, Lcom/policydm/eng/core/XDMWorkspace;->dataTotalSize:I

    .line 5803
    const/16 v30, 0x0

    .line 5804
    const/16 v13, 0xc

    goto/16 :goto_6

    .line 5793
    :cond_24
    const/16 v24, 0x0

    goto :goto_9

    .line 5797
    :cond_25
    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->length()I

    move-result v9

    goto :goto_9

    .line 5809
    :cond_26
    const/16 v24, 0x0

    .line 5810
    const/4 v9, 0x0

    .line 5811
    const/4 v4, 0x0

    iput v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->dataTotalSize:I

    .line 5812
    const/16 v13, 0xc

    .line 5813
    const/16 v30, 0x0

    goto/16 :goto_6

    .line 5844
    :cond_27
    move-object/from16 v0, p1

    iget v4, v0, Lcom/policydm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "500"

    invoke-static/range {v3 .. v8}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v23

    .line 5845
    iget-object v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 5846
    iget-object v12, v12, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 5847
    goto/16 :goto_0

    .line 5850
    :cond_28
    move-object/from16 v0, p1

    iget v4, v0, Lcom/policydm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "213"

    invoke-static/range {v3 .. v8}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v23

    .line 5851
    iget-object v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 5852
    iget-object v12, v12, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 5853
    goto/16 :goto_0

    .line 5855
    :cond_29
    const-string v4, "application/vnd.syncml.dmtnds+wbxml"

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2d

    .line 5857
    const-string v4, "### SYNCML_MIME_TYPE_TNDS_WBXML ###"

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 5859
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v4

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v4

    invoke-static {v15, v4}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpAppendFile(I[B)I

    .line 5861
    iget v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->moredata:I

    if-nez v4, :cond_2c

    .line 5863
    const/16 v16, 0x0

    .line 5864
    .local v16, "nWbxmlDataLen":I
    const/16 v20, 0x0

    .line 5865
    .local v20, "pWbxmlData":[B
    const/16 v31, 0x0

    .line 5866
    .local v31, "szWbxml":Ljava/lang/String;
    const/4 v4, 0x0

    iput v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->prevBufPos:I

    .line 5867
    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 5868
    const/4 v4, 0x0

    iput v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->dataTotalSize:I

    .line 5869
    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->nTNDSFlag:Z

    .line 5872
    invoke-static {v15}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpGetFileSize(I)I

    move-result v16

    .line 5873
    move/from16 v0, v16

    new-array v0, v0, [B

    move-object/from16 v20, v0

    .line 5874
    const/4 v4, 0x0

    move/from16 v0, v16

    move-object/from16 v1, v20

    invoke-static {v15, v4, v0, v1}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpReadFile(III[B)Z

    .line 5875
    new-instance v31, Ljava/lang/String;

    .end local v31    # "szWbxml":Ljava/lang/String;
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v4

    move-object/from16 v0, v31

    move-object/from16 v1, v20

    invoke-direct {v0, v1, v4}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 5876
    .restart local v31    # "szWbxml":Ljava/lang/String;
    invoke-virtual/range {v31 .. v31}, Ljava/lang/String;->length()I

    move-result v4

    move-object/from16 v0, v31

    invoke-static {v0, v4}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmTndsWbxmlParse(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v28

    .line 5877
    const/16 v19, 0x0

    .line 5878
    .local v19, "outBufSize":I
    invoke-static/range {v28 .. v28}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2a

    .line 5880
    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->length()I

    move-result v19

    .line 5882
    :cond_2a
    move-object/from16 v0, v28

    move/from16 v1, v19

    move-object/from16 v2, v18

    invoke-static {v0, v1, v2}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFCreateTNDSNode(Ljava/lang/String;ILcom/policydm/eng/core/XDMOmTree;)I

    move-result v22

    .line 5883
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmTndsParseFinish()V

    .line 5885
    if-lez v22, :cond_2b

    .line 5887
    move-object/from16 v0, p1

    iget v4, v0, Lcom/policydm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "200"

    invoke-static/range {v3 .. v8}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v23

    .line 5888
    iget-object v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 5889
    iget-object v12, v12, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 5890
    goto/16 :goto_0

    .line 5892
    :cond_2b
    move-object/from16 v0, p1

    iget v4, v0, Lcom/policydm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "500"

    invoke-static/range {v3 .. v8}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v23

    .line 5893
    iget-object v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 5894
    iget-object v12, v12, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 5895
    goto/16 :goto_0

    .line 5897
    .end local v16    # "nWbxmlDataLen":I
    .end local v19    # "outBufSize":I
    .end local v20    # "pWbxmlData":[B
    .end local v31    # "szWbxml":Ljava/lang/String;
    :cond_2c
    move-object/from16 v0, p1

    iget v4, v0, Lcom/policydm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "213"

    invoke-static/range {v3 .. v8}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v23

    .line 5898
    iget-object v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 5899
    iget-object v12, v12, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 5900
    goto/16 :goto_0

    .line 5906
    :cond_2d
    invoke-static/range {v30 .. v30}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_36

    invoke-static/range {v24 .. v24}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_36

    .line 5908
    const-string v4, "application/vnd.syncml.dmtnds+xml"

    move-object/from16 v0, v30

    invoke-virtual {v4, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_31

    .line 5910
    iget v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->moredata:I

    if-lez v4, :cond_2f

    .line 5912
    iget v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->prevBufPos:I

    add-int/2addr v4, v9

    iput v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->prevBufPos:I

    .line 5913
    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 5915
    iget-boolean v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->nTNDSFlag:Z

    if-nez v4, :cond_2e

    .line 5917
    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->nTNDSFlag:Z

    .line 5918
    invoke-static {v15}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpDeleteFile(I)I

    .line 5921
    :cond_2e
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v4

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v4

    invoke-static {v15, v4}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpAppendFile(I[B)I

    .line 5923
    move-object/from16 v0, p1

    iget v4, v0, Lcom/policydm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "213"

    invoke-static/range {v3 .. v8}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v23

    .line 5924
    iget-object v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 5925
    iget-object v12, v12, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 5926
    goto/16 :goto_0

    .line 5928
    :cond_2f
    move-object/from16 v0, v24

    move-object/from16 v1, v18

    invoke-static {v0, v9, v1}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFCreateTNDSNode(Ljava/lang/String;ILcom/policydm/eng/core/XDMOmTree;)I

    move-result v22

    .line 5929
    if-lez v22, :cond_30

    .line 5931
    move-object/from16 v0, p1

    iget v4, v0, Lcom/policydm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "200"

    invoke-static/range {v3 .. v8}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v23

    .line 5932
    iget-object v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 5933
    iget-object v12, v12, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 5937
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/policydm/agent/XDMAgent;->xdmAgentGetAccountFromOM(Lcom/policydm/eng/core/XDMOmTree;)I

    move-result v22

    .line 5939
    goto/16 :goto_0

    .line 5942
    :cond_30
    const/16 v24, 0x0

    .line 5943
    const/16 v30, 0x0

    .line 5944
    move-object/from16 v0, p1

    iget v4, v0, Lcom/policydm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "500"

    invoke-static/range {v3 .. v8}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v23

    .line 5945
    iget-object v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 5946
    iget-object v12, v12, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 5947
    const-string v4, "Fail"

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 5948
    const/4 v4, -0x1

    .line 6122
    .end local v17    # "node":Lcom/policydm/eng/core/XDMVnode;
    :goto_a
    return v4

    .line 5950
    .restart local v17    # "node":Lcom/policydm/eng/core/XDMVnode;
    :cond_31
    const-string v4, "application/vnd.syncml.dmtnds+wbxml"

    move-object/from16 v0, v30

    invoke-virtual {v4, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_36

    .line 5952
    const-string v4, "### SYNCML_MIME_TYPE_TNDS_WBXML ###\n"

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 5953
    iget v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->moredata:I

    if-lez v4, :cond_33

    .line 5955
    iget v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->prevBufPos:I

    add-int/2addr v4, v9

    iput v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->prevBufPos:I

    .line 5956
    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 5958
    iget-boolean v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->nTNDSFlag:Z

    if-nez v4, :cond_32

    .line 5960
    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->nTNDSFlag:Z

    .line 5961
    invoke-static {v15}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpDeleteFile(I)I

    .line 5964
    :cond_32
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v4

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v4

    invoke-static {v15, v4}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpAppendFile(I[B)I

    .line 5965
    move-object/from16 v0, p1

    iget v4, v0, Lcom/policydm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "213"

    invoke-static/range {v3 .. v8}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v23

    .line 5966
    iget-object v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 5967
    iget-object v12, v12, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 5968
    goto/16 :goto_0

    .line 5971
    :cond_33
    move-object/from16 v0, v24

    invoke-static {v0, v9}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmTndsWbxmlParse(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v28

    .line 5972
    const/16 v19, 0x0

    .line 5973
    .restart local v19    # "outBufSize":I
    invoke-static/range {v28 .. v28}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_34

    .line 5975
    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->length()I

    move-result v19

    .line 5977
    :cond_34
    move-object/from16 v0, v28

    move/from16 v1, v19

    move-object/from16 v2, v18

    invoke-static {v0, v1, v2}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFCreateTNDSNode(Ljava/lang/String;ILcom/policydm/eng/core/XDMOmTree;)I

    move-result v22

    .line 5978
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmTndsParseFinish()V

    .line 5980
    if-lez v22, :cond_35

    .line 5982
    move-object/from16 v0, p1

    iget v4, v0, Lcom/policydm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "200"

    invoke-static/range {v3 .. v8}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v23

    .line 5983
    iget-object v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 5984
    iget-object v12, v12, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 5986
    invoke-static {}, Lcom/policydm/agent/XDMAgent;->xdmAgentGetSyncMode()I

    move-result v4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_0

    .line 5988
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/policydm/agent/XDMAgent;->xdmAgentGetAccountFromOM(Lcom/policydm/eng/core/XDMOmTree;)I

    move-result v22

    goto/16 :goto_0

    .line 5994
    :cond_35
    const/16 v24, 0x0

    .line 5995
    const/16 v30, 0x0

    .line 5996
    move-object/from16 v0, p1

    iget v4, v0, Lcom/policydm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "500"

    invoke-static/range {v3 .. v8}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v23

    .line 5997
    iget-object v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 5998
    iget-object v12, v12, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 5999
    const-string v4, "xdmAgentBuildCmdStatus : Warning!!!. Fail"

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 6000
    const/4 v4, -0x1

    goto/16 :goto_a

    .line 6006
    .end local v19    # "outBufSize":I
    :cond_36
    iget v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->dataTotalSize:I

    if-nez v4, :cond_39

    .line 6008
    iget-object v5, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    iget v6, v3, Lcom/policydm/eng/core/XDMWorkspace;->dataTotalSize:I

    const/4 v7, 0x0

    move-object/from16 v4, v18

    move-object/from16 v8, v24

    invoke-static/range {v4 .. v9}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmWrite(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;IILjava/lang/Object;I)I

    move-result v22

    .line 6012
    iget-object v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v4}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetAclDynamicFUMONode(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)I

    .line 6015
    const-string v4, "ADD (NO DATA)"

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 6016
    iget-object v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-static {v0, v4}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v17

    .line 6017
    if-eqz v17, :cond_37

    const/16 v4, 0xc

    if-eq v13, v4, :cond_37

    .line 6019
    move-object/from16 v0, v17

    iput v13, v0, Lcom/policydm/eng/core/XDMVnode;->format:I

    .line 6056
    :cond_37
    :goto_b
    if-gez v22, :cond_3c

    .line 6058
    invoke-static/range {v22 .. v22}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 6060
    iget v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->moredata:I

    if-lez v4, :cond_38

    .line 6062
    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 6066
    :cond_38
    move-object/from16 v0, p1

    iget v4, v0, Lcom/policydm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "500"

    invoke-static/range {v3 .. v8}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v23

    .line 6067
    iget-object v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 6069
    const/16 v24, 0x0

    .line 6070
    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 6071
    iget-object v12, v12, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 6073
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 6075
    const-string v4, "507"

    move-object/from16 v0, p3

    iput-object v4, v0, Lcom/policydm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    goto/16 :goto_0

    .line 6025
    :cond_39
    iget v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->prevBufPos:I

    if-nez v4, :cond_3b

    .line 6027
    iget-object v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->om:Lcom/policydm/eng/core/XDMOmTree;

    iget-object v4, v4, Lcom/policydm/eng/core/XDMOmTree;->vfs:Lcom/policydm/eng/core/XDMOmVfs;

    iget v5, v3, Lcom/policydm/eng/core/XDMWorkspace;->dataTotalSize:I

    invoke-static {v4, v5}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsGetFreeVaddr(Lcom/policydm/eng/core/XDMOmVfs;I)I

    move-result v10

    .line 6029
    .local v10, "addr":I
    if-gez v10, :cond_3b

    .line 6032
    move-object/from16 v0, p1

    iget v4, v0, Lcom/policydm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "420"

    invoke-static/range {v3 .. v8}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v23

    .line 6033
    iget-object v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 6034
    const-string v4, "ADD STATUS_DEVICE_FULL"

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 6036
    iget-object v12, v12, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 6038
    iget v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->moredata:I

    if-lez v4, :cond_3a

    .line 6040
    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 6044
    :cond_3a
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 6046
    const-string v4, "507"

    move-object/from16 v0, p3

    iput-object v4, v0, Lcom/policydm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    goto/16 :goto_0

    .line 6052
    .end local v10    # "addr":I
    :cond_3b
    iget-object v5, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    iget v6, v3, Lcom/policydm/eng/core/XDMWorkspace;->dataTotalSize:I

    iget v7, v3, Lcom/policydm/eng/core/XDMWorkspace;->prevBufPos:I

    move-object/from16 v4, v18

    move-object/from16 v8, v24

    invoke-static/range {v4 .. v9}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmWrite(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;IILjava/lang/Object;I)I

    move-result v22

    goto/16 :goto_b

    .line 6080
    :cond_3c
    iget-object v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-static {v0, v4}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v17

    .line 6081
    if-eqz v17, :cond_3f

    .line 6083
    invoke-static/range {v30 .. v30}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3e

    .line 6085
    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/policydm/eng/core/XDMVnode;->type:Lcom/policydm/eng/core/XDMOmList;

    if-eqz v4, :cond_3d

    .line 6087
    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/policydm/eng/core/XDMVnode;->type:Lcom/policydm/eng/core/XDMOmList;

    invoke-static {v4}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmVfsDeleteMimeList(Lcom/policydm/eng/core/XDMOmList;)V

    .line 6089
    :cond_3d
    new-instance v4, Lcom/policydm/eng/core/XDMOmList;

    invoke-direct {v4}, Lcom/policydm/eng/core/XDMOmList;-><init>()V

    move-object/from16 v0, v17

    iput-object v4, v0, Lcom/policydm/eng/core/XDMVnode;->type:Lcom/policydm/eng/core/XDMOmList;

    .line 6090
    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/policydm/eng/core/XDMVnode;->type:Lcom/policydm/eng/core/XDMOmList;

    move-object/from16 v0, v30

    iput-object v0, v4, Lcom/policydm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    .line 6091
    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/policydm/eng/core/XDMVnode;->type:Lcom/policydm/eng/core/XDMOmList;

    const/4 v5, 0x0

    iput-object v5, v4, Lcom/policydm/eng/core/XDMOmList;->next:Lcom/policydm/eng/core/XDMOmList;

    .line 6093
    :cond_3e
    const/16 v4, 0xc

    if-eq v13, v4, :cond_3f

    .line 6095
    move-object/from16 v0, v17

    iput v13, v0, Lcom/policydm/eng/core/XDMVnode;->format:I

    .line 6100
    :cond_3f
    const/16 v24, 0x0

    .line 6102
    iget v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->moredata:I

    if-nez v4, :cond_40

    .line 6104
    const/4 v4, 0x0

    iput v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->prevBufPos:I

    .line 6105
    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 6106
    const/4 v4, 0x0

    iput v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->dataTotalSize:I

    .line 6109
    move-object/from16 v0, p1

    iget v4, v0, Lcom/policydm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "200"

    invoke-static/range {v3 .. v8}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v23

    .line 6119
    :goto_c
    iget-object v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 6120
    iget-object v12, v12, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    goto/16 :goto_0

    .line 6113
    :cond_40
    iget v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->prevBufPos:I

    add-int/2addr v4, v9

    iput v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->prevBufPos:I

    .line 6114
    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 6117
    move-object/from16 v0, p1

    iget v4, v0, Lcom/policydm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v5, "Add"

    const/4 v6, 0x0

    iget-object v7, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "213"

    invoke-static/range {v3 .. v8}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v23

    goto :goto_c

    .line 6122
    .end local v17    # "node":Lcom/policydm/eng/core/XDMVnode;
    :cond_41
    const/4 v4, 0x0

    goto/16 :goto_a
.end method

.method public xdmAgentCmdAlert(Lcom/policydm/eng/parser/XDMParserAlert;Z)I
    .locals 12
    .param p1, "alert"    # Lcom/policydm/eng/parser/XDMParserAlert;
    .param p2, "isAtomic"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 5259
    sget-object v0, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    .line 5260
    .local v0, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    const/4 v10, 0x0

    .line 5261
    .local v10, "status":Lcom/policydm/eng/parser/XDMParserStatus;
    const/4 v7, 0x0

    .line 5262
    .local v7, "item":Lcom/policydm/eng/parser/XDMParserItem;
    const/4 v6, 0x0

    .line 5263
    .local v6, "cur":Lcom/policydm/eng/core/XDMList;
    const/4 v11, 0x0

    .line 5265
    .local v11, "szData":Ljava/lang/String;
    sget-object v1, Lcom/policydm/interfaces/XDMInterface$XDMProcessingState;->XDM_PROC_ALERT:Lcom/policydm/interfaces/XDMInterface$XDMProcessingState;

    iput-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->procState:Lcom/policydm/interfaces/XDMInterface$XDMProcessingState;

    .line 5267
    iget-object v1, p1, Lcom/policydm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 5269
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Code "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/policydm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 5277
    iget v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->serverAuthState:I

    if-eq v1, v4, :cond_2

    .line 5279
    iget v1, p1, Lcom/policydm/eng/parser/XDMParserAlert;->cmdid:I

    const-string v2, "Alert"

    iget-object v5, v0, Lcom/policydm/eng/core/XDMWorkspace;->m_szStatusReturnCode:Ljava/lang/String;

    move-object v4, v3

    invoke-static/range {v0 .. v5}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v10

    .line 5280
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v1, v10}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 5425
    :cond_0
    :goto_0
    sget-object v1, Lcom/policydm/interfaces/XDMInterface$XDMProcessingState;->XDM_PROC_NONE:Lcom/policydm/interfaces/XDMInterface$XDMProcessingState;

    iput-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->procState:Lcom/policydm/interfaces/XDMInterface$XDMProcessingState;

    .line 5426
    const/4 v1, 0x0

    :goto_1
    return v1

    .line 5273
    :cond_1
    const-string v1, "alert->data is NULL"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 5274
    const/4 v1, -0x1

    goto :goto_1

    .line 5283
    :cond_2
    if-eqz p2, :cond_3

    .line 5285
    iget v1, p1, Lcom/policydm/eng/parser/XDMParserAlert;->cmdid:I

    const-string v2, "Alert"

    const-string v5, "215"

    move-object v4, v3

    invoke-static/range {v0 .. v5}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v10

    .line 5286
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v1, v10}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto :goto_0

    .line 5288
    :cond_3
    const-string v1, "1222"

    iget-object v2, p1, Lcom/policydm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_4

    .line 5290
    iput-boolean v4, v0, Lcom/policydm/eng/core/XDMWorkspace;->nextMsg:Z

    .line 5291
    iget v1, p1, Lcom/policydm/eng/parser/XDMParserAlert;->cmdid:I

    const-string v2, "Alert"

    const-string v5, "200"

    move-object v4, v3

    invoke-static/range {v0 .. v5}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v10

    .line 5292
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v1, v10}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto :goto_0

    .line 5295
    :cond_4
    const-string v1, "1100"

    iget-object v2, p1, Lcom/policydm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "1101"

    iget-object v2, p1, Lcom/policydm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "1102"

    iget-object v2, p1, Lcom/policydm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "1103"

    iget-object v2, p1, Lcom/policydm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "1104"

    iget-object v2, p1, Lcom/policydm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_17

    .line 5299
    :cond_5
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->uicOption:Lcom/policydm/eng/core/XDMUicOption;

    if-eqz v1, :cond_6

    .line 5301
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->uicOption:Lcom/policydm/eng/core/XDMUicOption;

    invoke-static {v1}, Lcom/policydm/eng/core/XDMUic;->xdmUicFreeUicOption(Lcom/policydm/eng/core/XDMUicOption;)Lcom/policydm/eng/core/XDMUicOption;

    .line 5302
    iput-object v3, v0, Lcom/policydm/eng/core/XDMWorkspace;->uicOption:Lcom/policydm/eng/core/XDMUicOption;

    .line 5305
    :cond_6
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->uicOption:Lcom/policydm/eng/core/XDMUicOption;

    if-nez v1, :cond_7

    .line 5307
    invoke-static {}, Lcom/policydm/eng/core/XDMUic;->xdmUicCreateUicOption()Lcom/policydm/eng/core/XDMUicOption;

    move-result-object v1

    iput-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->uicOption:Lcom/policydm/eng/core/XDMUicOption;

    .line 5310
    :cond_7
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->uicOption:Lcom/policydm/eng/core/XDMUicOption;

    iget-object v2, p1, Lcom/policydm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    invoke-static {v2}, Lcom/policydm/eng/core/XDMUic;->xdmUicGetUicType(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Lcom/policydm/eng/core/XDMUicOption;->UICType:I

    .line 5312
    iget-object v6, p1, Lcom/policydm/eng/parser/XDMParserAlert;->itemlist:Lcom/policydm/eng/core/XDMList;

    .line 5313
    iget-object v7, v6, Lcom/policydm/eng/core/XDMList;->item:Ljava/lang/Object;

    .end local v7    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    check-cast v7, Lcom/policydm/eng/parser/XDMParserItem;

    .line 5317
    .restart local v7    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    iget-object v1, v7, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    if-eqz v1, :cond_11

    .line 5319
    iget-object v1, v7, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    invoke-static {v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStGetString(Lcom/policydm/eng/parser/XDMParserPcdata;)Ljava/lang/String;

    move-result-object v11

    .line 5320
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "str = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 5327
    :goto_2
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 5330
    const-string v1, "1100"

    iget-object v2, p1, Lcom/policydm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_8

    const-string v1, "1101"

    iget-object v2, p1, Lcom/policydm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_8

    const-string v1, "1102"

    iget-object v2, p1, Lcom/policydm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_8

    const-string v1, "1103"

    iget-object v2, p1, Lcom/policydm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_8

    const-string v1, "1104"

    iget-object v2, p1, Lcom/policydm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_12

    .line 5333
    :cond_8
    const-string v11, "MINDT=30"

    .line 5347
    :cond_9
    :goto_3
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->uicOption:Lcom/policydm/eng/core/XDMUicOption;

    invoke-static {v11, v1}, Lcom/policydm/eng/core/XDMUic;->xdmUicOptionProcess(Ljava/lang/String;Lcom/policydm/eng/core/XDMUicOption;)Ljava/lang/String;

    move-result-object v11

    .line 5348
    iget-object v6, v6, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 5350
    if-eqz v6, :cond_a

    .line 5352
    iget-object v7, v6, Lcom/policydm/eng/core/XDMList;->item:Ljava/lang/Object;

    .end local v7    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    check-cast v7, Lcom/policydm/eng/parser/XDMParserItem;

    .line 5355
    .restart local v7    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    :cond_a
    iget-object v1, v7, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    if-eqz v1, :cond_b

    .line 5357
    iget-object v1, v7, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    invoke-static {v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStGetString(Lcom/policydm/eng/parser/XDMParserPcdata;)Ljava/lang/String;

    move-result-object v11

    .line 5360
    :cond_b
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 5362
    iget-object v1, v7, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    if-eqz v1, :cond_c

    iget-object v1, v7, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    iget-object v1, v1, Lcom/policydm/eng/parser/XDMParserPcdata;->data:[C

    if-eqz v1, :cond_c

    .line 5364
    iget-object v1, v7, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    iget-object v1, v1, Lcom/policydm/eng/parser/XDMParserPcdata;->data:[C

    invoke-static {v1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v11

    .line 5368
    :cond_c
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_d

    .line 5370
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->uicOption:Lcom/policydm/eng/core/XDMUicOption;

    iget-object v2, v0, Lcom/policydm/eng/core/XDMWorkspace;->uicOption:Lcom/policydm/eng/core/XDMUicOption;

    iget-object v2, v2, Lcom/policydm/eng/core/XDMUicOption;->text:Lcom/policydm/eng/core/XDMText;

    invoke-static {v2, v11}, Lcom/policydm/eng/core/XDMList;->xdmListAppendStrText(Lcom/policydm/eng/core/XDMText;Ljava/lang/String;)Lcom/policydm/eng/core/XDMText;

    move-result-object v2

    iput-object v2, v1, Lcom/policydm/eng/core/XDMUicOption;->text:Lcom/policydm/eng/core/XDMText;

    .line 5374
    :cond_d
    const-string v1, "1103"

    iget-object v2, p1, Lcom/policydm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_e

    const-string v1, "1104"

    iget-object v2, p1, Lcom/policydm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_15

    .line 5376
    :cond_e
    const/4 v8, 0x0

    .line 5377
    .local v8, "iuicMenu":I
    if-eqz v6, :cond_14

    .line 5379
    iget-object v6, v6, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    move v9, v8

    .line 5380
    .end local v8    # "iuicMenu":I
    .local v9, "iuicMenu":I
    :goto_4
    if-eqz v6, :cond_13

    .line 5382
    iget-object v7, v6, Lcom/policydm/eng/core/XDMList;->item:Ljava/lang/Object;

    .end local v7    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    check-cast v7, Lcom/policydm/eng/parser/XDMParserItem;

    .line 5384
    .restart local v7    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    iget-object v1, v7, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    if-eqz v1, :cond_f

    .line 5386
    iget-object v1, v7, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    invoke-static {v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStGetString(Lcom/policydm/eng/parser/XDMParserPcdata;)Ljava/lang/String;

    move-result-object v11

    .line 5389
    :cond_f
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 5391
    iget-object v1, v7, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    if-eqz v1, :cond_10

    iget-object v1, v7, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    iget-object v1, v1, Lcom/policydm/eng/parser/XDMParserPcdata;->data:[C

    if-eqz v1, :cond_10

    .line 5393
    iget-object v1, v7, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    iget-object v1, v1, Lcom/policydm/eng/parser/XDMParserPcdata;->data:[C

    invoke-static {v1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v11

    .line 5397
    :cond_10
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_18

    .line 5399
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->uicOption:Lcom/policydm/eng/core/XDMUicOption;

    iget-object v1, v1, Lcom/policydm/eng/core/XDMUicOption;->uicMenuList:[Ljava/lang/String;

    add-int/lit8 v8, v9, 0x1

    .end local v9    # "iuicMenu":I
    .restart local v8    # "iuicMenu":I
    aput-object v11, v1, v9

    .line 5402
    :goto_5
    iget-object v6, v6, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    move v9, v8

    .end local v8    # "iuicMenu":I
    .restart local v9    # "iuicMenu":I
    goto :goto_4

    .line 5324
    .end local v9    # "iuicMenu":I
    :cond_11
    const-string v1, "str = NULL"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 5338
    :cond_12
    iget-object v1, v7, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    if-eqz v1, :cond_9

    iget-object v1, v7, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    iget-object v1, v1, Lcom/policydm/eng/parser/XDMParserPcdata;->data:[C

    if-eqz v1, :cond_9

    .line 5342
    iget-object v1, v7, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    iget-object v1, v1, Lcom/policydm/eng/parser/XDMParserPcdata;->data:[C

    invoke-static {v1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v11

    goto/16 :goto_3

    .restart local v9    # "iuicMenu":I
    :cond_13
    move v8, v9

    .line 5405
    .end local v9    # "iuicMenu":I
    .restart local v8    # "iuicMenu":I
    :cond_14
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->uicOption:Lcom/policydm/eng/core/XDMUicOption;

    iput v8, v1, Lcom/policydm/eng/core/XDMUicOption;->uicMenuNumbers:I

    .line 5408
    .end local v8    # "iuicMenu":I
    :cond_15
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->uicOption:Lcom/policydm/eng/core/XDMUicOption;

    iget v2, v0, Lcom/policydm/eng/core/XDMWorkspace;->appId:I

    iput v2, v1, Lcom/policydm/eng/core/XDMUicOption;->appId:I

    .line 5410
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->uicAlert:Lcom/policydm/eng/parser/XDMParserAlert;

    if-eqz v1, :cond_16

    .line 5412
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->uicAlert:Lcom/policydm/eng/parser/XDMParserAlert;

    invoke-static {v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDeleteAlert(Ljava/lang/Object;)V

    .line 5414
    :cond_16
    new-instance v1, Lcom/policydm/eng/parser/XDMParserAlert;

    invoke-direct {v1}, Lcom/policydm/eng/parser/XDMParserAlert;-><init>()V

    iput-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->uicAlert:Lcom/policydm/eng/parser/XDMParserAlert;

    .line 5415
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->uicAlert:Lcom/policydm/eng/parser/XDMParserAlert;

    invoke-static {v1, p1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplAlert(Lcom/policydm/eng/parser/XDMParserAlert;Lcom/policydm/eng/parser/XDMParserAlert;)V

    .line 5417
    const/4 v1, -0x4

    goto/16 :goto_1

    .line 5419
    :cond_17
    const-string v1, "1223"

    iget-object v2, p1, Lcom/policydm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    .line 5421
    iput v4, v0, Lcom/policydm/eng/core/XDMWorkspace;->sessionAbort:I

    .line 5422
    const/4 v1, 0x3

    goto/16 :goto_1

    .restart local v9    # "iuicMenu":I
    :cond_18
    move v8, v9

    .end local v9    # "iuicMenu":I
    .restart local v8    # "iuicMenu":I
    goto :goto_5
.end method

.method public xdmAgentCmdAtomic(Lcom/policydm/eng/parser/XDMParserAtomic;)I
    .locals 4
    .param p1, "atomic"    # Lcom/policydm/eng/parser/XDMParserAtomic;

    .prologue
    const/4 v2, -0x1

    .line 7631
    sget-object v1, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    .line 7634
    .local v1, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    if-eqz p1, :cond_0

    iget-object v3, p1, Lcom/policydm/eng/parser/XDMParserAtomic;->itemlist:Lcom/policydm/eng/core/XDMLinkedList;

    if-eqz v3, :cond_0

    .line 7636
    iget-object v3, p1, Lcom/policydm/eng/parser/XDMParserAtomic;->itemlist:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-virtual {p0, p1, v3}, Lcom/policydm/agent/XDMAgent;->xdmAgentCmdAtomicBlock(Lcom/policydm/eng/parser/XDMParserAtomic;Lcom/policydm/eng/core/XDMLinkedList;)I

    move-result v0

    .line 7640
    .local v0, "res":I
    if-gez v0, :cond_1

    .line 7645
    .end local v0    # "res":I
    :cond_0
    :goto_0
    return v2

    .line 7644
    .restart local v0    # "res":I
    :cond_1
    iget v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->numAction:I

    add-int/2addr v2, v0

    iput v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->numAction:I

    .line 7645
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public xdmAgentCmdAtomicBlock(Lcom/policydm/eng/parser/XDMParserAtomic;Lcom/policydm/eng/core/XDMLinkedList;)I
    .locals 13
    .param p1, "atomic"    # Lcom/policydm/eng/parser/XDMParserAtomic;
    .param p2, "list"    # Lcom/policydm/eng/core/XDMLinkedList;

    .prologue
    .line 2606
    sget-object v0, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    .line 2608
    .local v0, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    const/4 v8, 0x0

    .line 2609
    .local v8, "item":Lcom/policydm/eng/parser/XDMParserItem;
    const/4 v12, 0x0

    .line 2610
    .local v12, "status":Lcom/policydm/eng/parser/XDMParserStatus;
    const/4 v7, 0x1

    .line 2612
    .local v7, "isProcess":Z
    const/4 v10, 0x0

    .line 2613
    .local v10, "r":I
    const/4 v9, 0x1

    .line 2615
    .local v9, "num":I
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->tmpItem:Lcom/policydm/eng/parser/XDMParserItem;

    .line 2616
    const/4 v1, 0x0

    invoke-static {p2, v1}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListSetCurrentObj(Lcom/policydm/eng/core/XDMLinkedList;I)V

    .line 2617
    invoke-static {p2}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/policydm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/policydm/agent/XDMAgent;

    .line 2619
    .local v6, "cmd":Lcom/policydm/agent/XDMAgent;
    :goto_0
    if-eqz v6, :cond_1

    .line 2621
    invoke-virtual {p0, v6}, Lcom/policydm/agent/XDMAgent;->xdmAgentVefifyAtomicCmd(Lcom/policydm/agent/XDMAgent;)Z

    move-result v11

    .line 2622
    .local v11, "res":Z
    if-nez v11, :cond_0

    .line 2624
    const/4 v7, 0x0

    .line 2626
    :cond_0
    invoke-static {p2}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/policydm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "cmd":Lcom/policydm/agent/XDMAgent;
    check-cast v6, Lcom/policydm/agent/XDMAgent;

    .restart local v6    # "cmd":Lcom/policydm/agent/XDMAgent;
    goto :goto_0

    .line 2629
    .end local v11    # "res":Z
    :cond_1
    if-eqz v7, :cond_2

    .line 2631
    iget v1, p1, Lcom/policydm/eng/parser/XDMParserAtomic;->cmdid:I

    const-string v2, "Atomic"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "200"

    invoke-static/range {v0 .. v5}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v12

    .line 2632
    sget-object v1, Lcom/policydm/interfaces/XDMInterface$XDMAtomicStep;->XDM_ATOMIC_NONE:Lcom/policydm/interfaces/XDMInterface$XDMAtomicStep;

    iput-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->atomicStep:Lcom/policydm/interfaces/XDMInterface$XDMAtomicStep;

    .line 2640
    :goto_1
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v1, v12}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 2642
    const/4 v1, 0x0

    invoke-static {p2, v1}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListSetCurrentObj(Lcom/policydm/eng/core/XDMLinkedList;I)V

    .line 2643
    invoke-static {p2}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/policydm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "cmd":Lcom/policydm/agent/XDMAgent;
    check-cast v6, Lcom/policydm/agent/XDMAgent;

    .line 2644
    .restart local v6    # "cmd":Lcom/policydm/agent/XDMAgent;
    :goto_2
    if-eqz v6, :cond_22

    .line 2646
    iget-boolean v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->atomicFlag:Z

    if-eqz v1, :cond_f

    .line 2648
    const-string v1, "Get"

    iget-object v2, v6, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_4

    .line 2650
    iget-object v1, v6, Lcom/policydm/agent/XDMAgent;->m_Get:Lcom/policydm/eng/parser/XDMParserGet;

    iget-object v1, v1, Lcom/policydm/eng/parser/XDMParserGet;->itemlist:Lcom/policydm/eng/core/XDMList;

    iget-object v8, v1, Lcom/policydm/eng/core/XDMList;->item:Ljava/lang/Object;

    .end local v8    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    check-cast v8, Lcom/policydm/eng/parser/XDMParserItem;

    .line 2651
    .restart local v8    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    iget-object v1, v8, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2653
    iget-object v1, v6, Lcom/policydm/agent/XDMAgent;->m_Get:Lcom/policydm/eng/parser/XDMParserGet;

    iget v1, v1, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    const-string v2, "Get"

    const/4 v3, 0x0

    iget-object v4, v8, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v5, "215"

    invoke-static/range {v0 .. v5}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v12

    .line 2660
    :goto_3
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v1, v12}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 2837
    :goto_4
    invoke-static {p2}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/policydm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "cmd":Lcom/policydm/agent/XDMAgent;
    check-cast v6, Lcom/policydm/agent/XDMAgent;

    .restart local v6    # "cmd":Lcom/policydm/agent/XDMAgent;
    goto :goto_2

    .line 2636
    :cond_2
    iget v1, p1, Lcom/policydm/eng/parser/XDMParserAtomic;->cmdid:I

    const-string v2, "Atomic"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "507"

    invoke-static/range {v0 .. v5}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v12

    .line 2637
    sget-object v1, Lcom/policydm/interfaces/XDMInterface$XDMAtomicStep;->XDM_ATOMIC_STEP_ROLLBACK:Lcom/policydm/interfaces/XDMInterface$XDMAtomicStep;

    iput-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->atomicStep:Lcom/policydm/interfaces/XDMInterface$XDMAtomicStep;

    goto :goto_1

    .line 2657
    :cond_3
    iget-object v1, v6, Lcom/policydm/agent/XDMAgent;->m_Get:Lcom/policydm/eng/parser/XDMParserGet;

    iget v1, v1, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    const-string v2, "Get"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "404"

    invoke-static/range {v0 .. v5}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v12

    goto :goto_3

    .line 2662
    :cond_4
    const-string v1, "Exec"

    iget-object v2, v6, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_6

    .line 2664
    iget-object v1, v6, Lcom/policydm/agent/XDMAgent;->m_Exec:Lcom/policydm/eng/parser/XDMParserExec;

    iget-object v1, v1, Lcom/policydm/eng/parser/XDMParserExec;->itemlist:Lcom/policydm/eng/core/XDMList;

    iget-object v8, v1, Lcom/policydm/eng/core/XDMList;->item:Ljava/lang/Object;

    .end local v8    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    check-cast v8, Lcom/policydm/eng/parser/XDMParserItem;

    .line 2665
    .restart local v8    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    iget-object v1, v8, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 2667
    iget-object v1, v6, Lcom/policydm/agent/XDMAgent;->m_Exec:Lcom/policydm/eng/parser/XDMParserExec;

    iget v1, v1, Lcom/policydm/eng/parser/XDMParserExec;->cmdid:I

    const-string v2, "Exec"

    const/4 v3, 0x0

    iget-object v4, v8, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v5, "215"

    invoke-static/range {v0 .. v5}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v12

    .line 2674
    :goto_5
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v1, v12}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto :goto_4

    .line 2671
    :cond_5
    iget-object v1, v6, Lcom/policydm/agent/XDMAgent;->m_Exec:Lcom/policydm/eng/parser/XDMParserExec;

    iget v1, v1, Lcom/policydm/eng/parser/XDMParserExec;->cmdid:I

    const-string v2, "Exec"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "404"

    invoke-static/range {v0 .. v5}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v12

    goto :goto_5

    .line 2676
    :cond_6
    const-string v1, "Add"

    iget-object v2, v6, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_8

    .line 2678
    iget-object v1, v6, Lcom/policydm/agent/XDMAgent;->m_AddCmd:Lcom/policydm/eng/parser/XDMParserAdd;

    iget-object v1, v1, Lcom/policydm/eng/parser/XDMParserAdd;->itemlist:Lcom/policydm/eng/core/XDMList;

    iget-object v8, v1, Lcom/policydm/eng/core/XDMList;->item:Ljava/lang/Object;

    .end local v8    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    check-cast v8, Lcom/policydm/eng/parser/XDMParserItem;

    .line 2679
    .restart local v8    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    iget-object v1, v8, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 2681
    iget-object v1, v6, Lcom/policydm/agent/XDMAgent;->m_AddCmd:Lcom/policydm/eng/parser/XDMParserAdd;

    iget v1, v1, Lcom/policydm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v2, "Add"

    const/4 v3, 0x0

    iget-object v4, v8, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v5, "215"

    invoke-static/range {v0 .. v5}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v12

    .line 2688
    :goto_6
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v1, v12}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto/16 :goto_4

    .line 2685
    :cond_7
    iget-object v1, v6, Lcom/policydm/agent/XDMAgent;->m_AddCmd:Lcom/policydm/eng/parser/XDMParserAdd;

    iget v1, v1, Lcom/policydm/eng/parser/XDMParserAdd;->cmdid:I

    const-string v2, "Add"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "404"

    invoke-static/range {v0 .. v5}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v12

    goto :goto_6

    .line 2690
    :cond_8
    const-string v1, "Delete"

    iget-object v2, v6, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_a

    .line 2692
    iget-object v1, v6, Lcom/policydm/agent/XDMAgent;->m_DeleteCmd:Lcom/policydm/eng/parser/XDMParserDelete;

    iget-object v1, v1, Lcom/policydm/eng/parser/XDMParserDelete;->itemlist:Lcom/policydm/eng/core/XDMList;

    iget-object v8, v1, Lcom/policydm/eng/core/XDMList;->item:Ljava/lang/Object;

    .end local v8    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    check-cast v8, Lcom/policydm/eng/parser/XDMParserItem;

    .line 2694
    .restart local v8    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    iget-object v1, v8, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 2696
    iget-object v1, v6, Lcom/policydm/agent/XDMAgent;->m_DeleteCmd:Lcom/policydm/eng/parser/XDMParserDelete;

    iget v1, v1, Lcom/policydm/eng/parser/XDMParserDelete;->cmdid:I

    const-string v2, "Delete"

    const/4 v3, 0x0

    iget-object v4, v8, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v5, "215"

    invoke-static/range {v0 .. v5}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v12

    .line 2702
    :goto_7
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v1, v12}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto/16 :goto_4

    .line 2700
    :cond_9
    iget-object v1, v6, Lcom/policydm/agent/XDMAgent;->m_DeleteCmd:Lcom/policydm/eng/parser/XDMParserDelete;

    iget v1, v1, Lcom/policydm/eng/parser/XDMParserDelete;->cmdid:I

    const-string v2, "Delete"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "404"

    invoke-static/range {v0 .. v5}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v12

    goto :goto_7

    .line 2704
    :cond_a
    const-string v1, "Replace"

    iget-object v2, v6, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_c

    .line 2706
    iget-object v1, v6, Lcom/policydm/agent/XDMAgent;->m_ReplaceCmd:Lcom/policydm/eng/parser/XDMParserReplace;

    iget-object v1, v1, Lcom/policydm/eng/parser/XDMParserReplace;->itemlist:Lcom/policydm/eng/core/XDMList;

    iget-object v8, v1, Lcom/policydm/eng/core/XDMList;->item:Ljava/lang/Object;

    .end local v8    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    check-cast v8, Lcom/policydm/eng/parser/XDMParserItem;

    .line 2707
    .restart local v8    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    iget-object v1, v8, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 2709
    iget-object v1, v6, Lcom/policydm/agent/XDMAgent;->m_ReplaceCmd:Lcom/policydm/eng/parser/XDMParserReplace;

    iget v1, v1, Lcom/policydm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v2, "Replace"

    const/4 v3, 0x0

    iget-object v4, v8, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v5, "215"

    invoke-static/range {v0 .. v5}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v12

    .line 2715
    :goto_8
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v1, v12}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto/16 :goto_4

    .line 2713
    :cond_b
    iget-object v1, v6, Lcom/policydm/agent/XDMAgent;->m_ReplaceCmd:Lcom/policydm/eng/parser/XDMParserReplace;

    iget v1, v1, Lcom/policydm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v2, "Replace"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "404"

    invoke-static/range {v0 .. v5}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v12

    goto :goto_8

    .line 2717
    :cond_c
    const-string v1, "Copy"

    iget-object v2, v6, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_e

    .line 2719
    iget-object v1, v6, Lcom/policydm/agent/XDMAgent;->m_CopyCmd:Lcom/policydm/eng/parser/XDMParserCopy;

    iget-object v1, v1, Lcom/policydm/eng/parser/XDMParserCopy;->itemlist:Lcom/policydm/eng/core/XDMList;

    iget-object v8, v1, Lcom/policydm/eng/core/XDMList;->item:Ljava/lang/Object;

    .end local v8    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    check-cast v8, Lcom/policydm/eng/parser/XDMParserItem;

    .line 2720
    .restart local v8    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    iget-object v1, v8, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_d

    .line 2722
    iget-object v1, v6, Lcom/policydm/agent/XDMAgent;->m_CopyCmd:Lcom/policydm/eng/parser/XDMParserCopy;

    iget v1, v1, Lcom/policydm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v2, "Copy"

    const/4 v3, 0x0

    iget-object v4, v8, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v5, "215"

    invoke-static/range {v0 .. v5}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v12

    .line 2728
    :goto_9
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v1, v12}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto/16 :goto_4

    .line 2726
    :cond_d
    iget-object v1, v6, Lcom/policydm/agent/XDMAgent;->m_CopyCmd:Lcom/policydm/eng/parser/XDMParserCopy;

    iget v1, v1, Lcom/policydm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v2, "Copy"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "404"

    invoke-static/range {v0 .. v5}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v12

    goto :goto_9

    .line 2732
    :cond_e
    const-string v1, "unknown command"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 2737
    :cond_f
    const-string v1, "Get"

    iget-object v2, v6, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_11

    .line 2739
    iget-object v1, v6, Lcom/policydm/agent/XDMAgent;->m_Get:Lcom/policydm/eng/parser/XDMParserGet;

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, Lcom/policydm/agent/XDMAgent;->xdmAgentCmdGet(Lcom/policydm/eng/parser/XDMParserGet;Z)I

    move-result v10

    .line 2740
    if-eqz v10, :cond_10

    .line 2742
    const-string v1, "get failed"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 2743
    const/4 v9, -0x1

    .line 2841
    .end local v9    # "num":I
    :goto_a
    return v9

    .line 2745
    .restart local v9    # "num":I
    :cond_10
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_4

    .line 2747
    :cond_11
    const-string v1, "Exec"

    iget-object v2, v6, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_14

    .line 2749
    iget-object v1, v6, Lcom/policydm/agent/XDMAgent;->m_Exec:Lcom/policydm/eng/parser/XDMParserExec;

    invoke-virtual {p0, v1}, Lcom/policydm/agent/XDMAgent;->xdmAgentCmdExec(Lcom/policydm/eng/parser/XDMParserExec;)I

    move-result v10

    .line 2751
    const-string v1, "507"

    iget-object v2, v12, Lcom/policydm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_12

    .line 2753
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->atomicFlag:Z

    .line 2756
    :cond_12
    if-eqz v10, :cond_13

    .line 2758
    const-string v1, "exec failed"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 2759
    const/4 v9, -0x1

    goto :goto_a

    .line 2761
    :cond_13
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_4

    .line 2763
    :cond_14
    const-string v1, "Add"

    iget-object v2, v6, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_17

    .line 2765
    iget-object v1, v6, Lcom/policydm/agent/XDMAgent;->m_AddCmd:Lcom/policydm/eng/parser/XDMParserAdd;

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2, v12}, Lcom/policydm/agent/XDMAgent;->xdmAgentCmdAdd(Lcom/policydm/eng/parser/XDMParserAdd;ZLcom/policydm/eng/parser/XDMParserStatus;)I

    move-result v10

    .line 2767
    const-string v1, "507"

    iget-object v2, v12, Lcom/policydm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_15

    .line 2769
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->atomicFlag:Z

    .line 2772
    :cond_15
    if-eqz v10, :cond_16

    .line 2774
    const-string v1, "Add failed"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 2775
    const/4 v9, -0x1

    goto :goto_a

    .line 2777
    :cond_16
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_4

    .line 2779
    :cond_17
    const-string v1, "Delete"

    iget-object v2, v6, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1a

    .line 2781
    iget-object v1, v6, Lcom/policydm/agent/XDMAgent;->m_DeleteCmd:Lcom/policydm/eng/parser/XDMParserDelete;

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2, v12}, Lcom/policydm/agent/XDMAgent;->xdmAgentCmdDelete(Lcom/policydm/eng/parser/XDMParserDelete;ZLcom/policydm/eng/parser/XDMParserStatus;)I

    move-result v10

    .line 2783
    const-string v1, "507"

    iget-object v2, v12, Lcom/policydm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_18

    .line 2785
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->atomicFlag:Z

    .line 2788
    :cond_18
    if-eqz v10, :cond_19

    .line 2790
    const-string v1, "Delete failed"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 2791
    const/4 v9, -0x1

    goto :goto_a

    .line 2793
    :cond_19
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_4

    .line 2795
    :cond_1a
    const-string v1, "Replace"

    iget-object v2, v6, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1d

    .line 2797
    iget-object v1, v6, Lcom/policydm/agent/XDMAgent;->m_ReplaceCmd:Lcom/policydm/eng/parser/XDMParserReplace;

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2, v12}, Lcom/policydm/agent/XDMAgent;->xdmAgentCmdReplace(Lcom/policydm/eng/parser/XDMParserReplace;ZLcom/policydm/eng/parser/XDMParserStatus;)I

    move-result v10

    .line 2799
    const-string v1, "507"

    iget-object v2, v12, Lcom/policydm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1b

    .line 2801
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->atomicFlag:Z

    .line 2804
    :cond_1b
    if-eqz v10, :cond_1c

    .line 2806
    const-string v1, "Replace failed"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 2807
    const/4 v9, -0x1

    goto/16 :goto_a

    .line 2809
    :cond_1c
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_4

    .line 2811
    :cond_1d
    const-string v1, "Copy"

    iget-object v2, v6, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_20

    .line 2813
    iget-object v1, v6, Lcom/policydm/agent/XDMAgent;->m_CopyCmd:Lcom/policydm/eng/parser/XDMParserCopy;

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2, v12}, Lcom/policydm/agent/XDMAgent;->xdmAgentCmdCopy(Lcom/policydm/eng/parser/XDMParserCopy;ZLcom/policydm/eng/parser/XDMParserStatus;)I

    move-result v10

    .line 2815
    const-string v1, "507"

    iget-object v2, v12, Lcom/policydm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1e

    .line 2817
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->atomicFlag:Z

    .line 2820
    :cond_1e
    if-eqz v10, :cond_1f

    .line 2822
    const-string v1, "Copy failed"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 2823
    const/4 v9, -0x1

    goto/16 :goto_a

    .line 2825
    :cond_1f
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_4

    .line 2827
    :cond_20
    const-string v1, "Atomic_Start"

    iget-object v2, v6, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_21

    .line 2829
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->atomicFlag:Z

    .line 2830
    iget-object v1, v6, Lcom/policydm/agent/XDMAgent;->m_Atomic:Lcom/policydm/eng/parser/XDMParserAtomic;

    iget-object v2, v6, Lcom/policydm/agent/XDMAgent;->m_Atomic:Lcom/policydm/eng/parser/XDMParserAtomic;

    iget-object v2, v2, Lcom/policydm/eng/parser/XDMParserAtomic;->itemlist:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v1, v2}, Lcom/policydm/agent/XDMAgent;->xdmAgentCmdAtomicBlock(Lcom/policydm/eng/parser/XDMParserAtomic;Lcom/policydm/eng/core/XDMLinkedList;)I

    goto/16 :goto_4

    .line 2834
    :cond_21
    const-string v1, "unknown command"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 2839
    :cond_22
    iget-object v1, p1, Lcom/policydm/eng/parser/XDMParserAtomic;->itemlist:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v1}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListClearLinkedList(Lcom/policydm/eng/core/XDMLinkedList;)V

    goto/16 :goto_a
.end method

.method public xdmAgentCmdCopy(Lcom/policydm/eng/parser/XDMParserCopy;ZLcom/policydm/eng/parser/XDMParserStatus;)I
    .locals 28
    .param p1, "copy"    # Lcom/policydm/eng/parser/XDMParserCopy;
    .param p2, "isAtomic"    # Z
    .param p3, "atomic_status"    # Lcom/policydm/eng/parser/XDMParserStatus;

    .prologue
    .line 7090
    sget-object v2, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    .line 7091
    .local v2, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    iget-object v0, v2, Lcom/policydm/eng/core/XDMWorkspace;->om:Lcom/policydm/eng/core/XDMOmTree;

    move-object/from16 v16, v0

    .line 7092
    .local v16, "om":Lcom/policydm/eng/core/XDMOmTree;
    const/16 v21, 0x0

    .line 7093
    .local v21, "status":Lcom/policydm/eng/parser/XDMParserStatus;
    const/4 v14, 0x0

    .line 7094
    .local v14, "item":Lcom/policydm/eng/parser/XDMParserItem;
    const/4 v11, 0x0

    .line 7096
    .local v11, "cur":Lcom/policydm/eng/core/XDMList;
    const/16 v19, 0x0

    .line 7097
    .local v19, "sourcedata":[C
    const/16 v25, 0x0

    .line 7102
    .local v25, "targetdata":[C
    const/16 v3, 0x50

    new-array v0, v3, [C

    move-object/from16 v27, v0

    .line 7103
    .local v27, "tmpbuf":[C
    const/4 v8, 0x0

    .line 7104
    .local v8, "sourcesize":I
    const/16 v26, 0x0

    .line 7105
    .local v26, "targetsize":I
    const/4 v9, 0x0

    .line 7106
    .local v9, "bufsize":I
    const/4 v10, 0x0

    .line 7109
    .local v10, "bufsize1":I
    invoke-virtual/range {p0 .. p0}, Lcom/policydm/agent/XDMAgent;->xdmAgentCmdUicAlert()Z

    move-result v17

    .line 7110
    .local v17, "process":Z
    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/policydm/eng/parser/XDMParserCopy;->itemlist:Lcom/policydm/eng/core/XDMList;

    .line 7111
    :cond_0
    :goto_0
    if-eqz v11, :cond_21

    .line 7113
    const/16 v24, 0x0

    .line 7114
    .local v24, "szType":Ljava/lang/String;
    const/16 v12, 0xc

    .line 7115
    .local v12, "format":I
    iget-object v14, v11, Lcom/policydm/eng/core/XDMList;->item:Ljava/lang/Object;

    .end local v14    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    check-cast v14, Lcom/policydm/eng/parser/XDMParserItem;

    .line 7117
    .restart local v14    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    iget v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->serverAuthState:I

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    .line 7119
    iget-object v3, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 7121
    move-object/from16 v0, p1

    iget v3, v0, Lcom/policydm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v4, "Copy"

    const/4 v5, 0x0

    iget-object v6, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    iget-object v7, v2, Lcom/policydm/eng/core/XDMWorkspace;->m_szStatusReturnCode:Ljava/lang/String;

    invoke-static/range {v2 .. v7}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v21

    .line 7128
    :goto_1
    iget-object v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v21

    invoke-static {v3, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 7129
    iget-object v11, v11, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 7130
    goto :goto_0

    .line 7125
    :cond_1
    move-object/from16 v0, p1

    iget v3, v0, Lcom/policydm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v4, "Copy"

    const/4 v5, 0x0

    const/4 v6, 0x0

    iget-object v7, v2, Lcom/policydm/eng/core/XDMWorkspace;->m_szStatusReturnCode:Ljava/lang/String;

    invoke-static/range {v2 .. v7}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v21

    goto :goto_1

    .line 7132
    :cond_2
    if-nez v17, :cond_5

    .line 7135
    iget v3, v14, Lcom/policydm/eng/parser/XDMParserItem;->moredata:I

    if-lez v3, :cond_3

    .line 7137
    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 7140
    :cond_3
    iget-object v3, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 7142
    move-object/from16 v0, p1

    iget v3, v0, Lcom/policydm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v4, "Copy"

    const/4 v5, 0x0

    iget-object v6, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "215"

    invoke-static/range {v2 .. v7}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v21

    .line 7149
    :goto_2
    iget-object v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v21

    invoke-static {v3, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 7150
    iget-object v11, v11, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 7151
    goto :goto_0

    .line 7146
    :cond_4
    move-object/from16 v0, p1

    iget v3, v0, Lcom/policydm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v4, "Copy"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "215"

    invoke-static/range {v2 .. v7}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v21

    goto :goto_2

    .line 7155
    :cond_5
    if-eqz p2, :cond_d

    iget-object v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->atomicStep:Lcom/policydm/interfaces/XDMInterface$XDMAtomicStep;

    sget-object v4, Lcom/policydm/interfaces/XDMInterface$XDMAtomicStep;->XDM_ATOMIC_NONE:Lcom/policydm/interfaces/XDMInterface$XDMAtomicStep;

    if-eq v3, v4, :cond_d

    .line 7157
    iget-object v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->tmpItem:Lcom/policydm/eng/parser/XDMParserItem;

    if-eqz v3, :cond_9

    .line 7160
    iget-object v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->tmpItem:Lcom/policydm/eng/parser/XDMParserItem;

    invoke-virtual {v3, v14}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 7162
    iget-object v3, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 7164
    move-object/from16 v0, p1

    iget v3, v0, Lcom/policydm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v4, "Copy"

    const/4 v5, 0x0

    iget-object v6, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "418"

    invoke-static/range {v2 .. v7}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v21

    .line 7171
    :goto_3
    sget-object v3, Lcom/policydm/interfaces/XDMInterface$XDMAtomicStep;->XDM_ATOMIC_STEP_NOT_EXEC:Lcom/policydm/interfaces/XDMInterface$XDMAtomicStep;

    iput-object v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->atomicStep:Lcom/policydm/interfaces/XDMInterface$XDMAtomicStep;

    .line 7172
    const/4 v3, 0x0

    iput-object v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->tmpItem:Lcom/policydm/eng/parser/XDMParserItem;

    .line 7212
    :goto_4
    iget-object v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v21

    invoke-static {v3, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 7213
    iget-object v11, v11, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 7214
    goto/16 :goto_0

    .line 7168
    :cond_6
    move-object/from16 v0, p1

    iget v3, v0, Lcom/policydm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v4, "Copy"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "418"

    invoke-static/range {v2 .. v7}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v21

    goto :goto_3

    .line 7177
    :cond_7
    iget-object v3, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 7179
    move-object/from16 v0, p1

    iget v3, v0, Lcom/policydm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v4, "Copy"

    const/4 v5, 0x0

    iget-object v6, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "216"

    invoke-static/range {v2 .. v7}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v21

    goto :goto_4

    .line 7183
    :cond_8
    move-object/from16 v0, p1

    iget v3, v0, Lcom/policydm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v4, "Copy"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "216"

    invoke-static/range {v2 .. v7}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v21

    goto :goto_4

    .line 7189
    :cond_9
    iget-object v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->atomicStep:Lcom/policydm/interfaces/XDMInterface$XDMAtomicStep;

    sget-object v4, Lcom/policydm/interfaces/XDMInterface$XDMAtomicStep;->XDM_ATOMIC_STEP_NOT_EXEC:Lcom/policydm/interfaces/XDMInterface$XDMAtomicStep;

    if-ne v3, v4, :cond_b

    .line 7191
    iget-object v3, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_a

    .line 7193
    move-object/from16 v0, p1

    iget v3, v0, Lcom/policydm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v4, "Copy"

    const/4 v5, 0x0

    iget-object v6, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "215"

    invoke-static/range {v2 .. v7}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v21

    goto :goto_4

    .line 7197
    :cond_a
    move-object/from16 v0, p1

    iget v3, v0, Lcom/policydm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v4, "Copy"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "215"

    invoke-static/range {v2 .. v7}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v21

    goto :goto_4

    .line 7202
    :cond_b
    iget-object v3, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_c

    .line 7204
    move-object/from16 v0, p1

    iget v3, v0, Lcom/policydm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v4, "Copy"

    const/4 v5, 0x0

    iget-object v6, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "216"

    invoke-static/range {v2 .. v7}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v21

    goto/16 :goto_4

    .line 7208
    :cond_c
    move-object/from16 v0, p1

    iget v3, v0, Lcom/policydm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v4, "Copy"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "216"

    invoke-static/range {v2 .. v7}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v21

    goto/16 :goto_4

    .line 7217
    :cond_d
    iget-object v3, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 7219
    move-object/from16 v0, p1

    iget v3, v0, Lcom/policydm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v4, "Copy"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "404"

    invoke-static/range {v2 .. v7}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v21

    .line 7220
    iget-object v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v21

    invoke-static {v3, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 7221
    iget-object v11, v11, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 7222
    goto/16 :goto_0

    .line 7225
    :cond_e
    iget-object v3, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 7227
    move-object/from16 v0, p1

    iget v3, v0, Lcom/policydm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v4, "Copy"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "404"

    invoke-static/range {v2 .. v7}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v21

    .line 7228
    iget-object v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v21

    invoke-static {v3, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 7229
    iget-object v11, v11, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 7230
    goto/16 :goto_0

    .line 7233
    :cond_f
    iget-object v3, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szSource:Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-static {v0, v3}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v15

    .line 7235
    .local v15, "node":Lcom/policydm/eng/core/XDMVnode;
    if-nez v15, :cond_10

    .line 7237
    move-object/from16 v0, p1

    iget v3, v0, Lcom/policydm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v4, "Copy"

    const/4 v5, 0x0

    iget-object v6, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szSource:Ljava/lang/String;

    const-string v7, "215"

    invoke-static/range {v2 .. v7}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v21

    .line 7238
    iget-object v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v21

    invoke-static {v3, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 7239
    iget-object v11, v11, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 7243
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 7245
    const-string v3, "507"

    move-object/from16 v0, p3

    iput-object v3, v0, Lcom/policydm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    goto/16 :goto_0

    .line 7253
    :cond_10
    iget v9, v15, Lcom/policydm/eng/core/XDMVnode;->size:I

    .line 7254
    move v8, v9

    .line 7255
    new-array v0, v8, [C

    move-object/from16 v19, v0

    .line 7256
    move-object/from16 v0, v16

    iget-object v3, v0, Lcom/policydm/eng/core/XDMOmTree;->vfs:Lcom/policydm/eng/core/XDMOmVfs;

    move-object/from16 v0, v19

    invoke-static {v3, v15, v0}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsGetData(Lcom/policydm/eng/core/XDMOmVfs;Lcom/policydm/eng/core/XDMVnode;[C)I

    .line 7258
    iget v12, v15, Lcom/policydm/eng/core/XDMVnode;->format:I

    .line 7260
    iget-object v3, v15, Lcom/policydm/eng/core/XDMVnode;->type:Lcom/policydm/eng/core/XDMOmList;

    if-eqz v3, :cond_11

    iget-object v3, v15, Lcom/policydm/eng/core/XDMVnode;->type:Lcom/policydm/eng/core/XDMOmList;

    iget-object v3, v3, Lcom/policydm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    if-eqz v3, :cond_11

    .line 7262
    iget-object v3, v15, Lcom/policydm/eng/core/XDMVnode;->type:Lcom/policydm/eng/core/XDMOmList;

    iget-object v3, v3, Lcom/policydm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v24

    .line 7270
    :goto_5
    iget v0, v15, Lcom/policydm/eng/core/XDMVnode;->format:I

    move/from16 v20, v0

    .line 7273
    .local v20, "sourceformat":I
    iget-object v3, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-static {v0, v3}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v15

    .line 7274
    if-eqz v15, :cond_16

    iget v3, v14, Lcom/policydm/eng/parser/XDMParserItem;->moredata:I

    if-nez v3, :cond_16

    iget-boolean v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->dataBuffered:Z

    if-nez v3, :cond_16

    .line 7276
    iget v10, v15, Lcom/policydm/eng/core/XDMVnode;->size:I

    .line 7277
    move/from16 v26, v10

    .line 7279
    move/from16 v0, v26

    if-ge v0, v8, :cond_12

    .line 7281
    const/16 v25, 0x0

    .line 7282
    new-array v0, v8, [C

    move-object/from16 v25, v0

    .line 7283
    move v10, v9

    .line 7284
    move/from16 v26, v8

    .line 7292
    :goto_6
    iget v3, v15, Lcom/policydm/eng/core/XDMVnode;->format:I

    const/4 v4, 0x6

    if-ne v3, v4, :cond_13

    .line 7294
    move-object/from16 v0, p1

    iget v3, v0, Lcom/policydm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v4, "Copy"

    const/4 v5, 0x0

    iget-object v6, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szSource:Ljava/lang/String;

    const-string v7, "215"

    invoke-static/range {v2 .. v7}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v21

    .line 7295
    iget-object v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v21

    invoke-static {v3, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 7296
    iget-object v11, v11, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 7298
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 7300
    const-string v3, "507"

    move-object/from16 v0, p3

    iput-object v3, v0, Lcom/policydm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    goto/16 :goto_0

    .line 7266
    .end local v20    # "sourceformat":I
    :cond_11
    const/16 v24, 0x0

    goto :goto_5

    .line 7288
    .restart local v20    # "sourceformat":I
    :cond_12
    const/16 v25, 0x0

    .line 7289
    move/from16 v0, v26

    new-array v0, v0, [C

    move-object/from16 v25, v0

    goto :goto_6

    .line 7306
    :cond_13
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_7
    if-ge v13, v8, :cond_14

    .line 7307
    aget-char v3, v19, v13

    aput-char v3, v25, v13

    .line 7306
    add-int/lit8 v13, v13, 0x1

    goto :goto_7

    .line 7308
    :cond_14
    new-instance v22, Ljava/lang/String;

    move-object/from16 v0, v22

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    .line 7309
    .local v22, "szData":Ljava/lang/String;
    move-object/from16 v0, v16

    iget-object v3, v0, Lcom/policydm/eng/core/XDMOmTree;->vfs:Lcom/policydm/eng/core/XDMOmVfs;

    move-object/from16 v0, v22

    invoke-static {v3, v15, v0, v9}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsSetData(Lcom/policydm/eng/core/XDMOmVfs;Lcom/policydm/eng/core/XDMVnode;Ljava/lang/Object;I)I

    .line 7312
    iget v3, v14, Lcom/policydm/eng/parser/XDMParserItem;->moredata:I

    if-nez v3, :cond_15

    .line 7314
    const/4 v3, 0x0

    iput v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->prevBufPos:I

    .line 7315
    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 7316
    const/4 v3, 0x0

    iput v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->dataTotalSize:I

    .line 7319
    move-object/from16 v0, p1

    iget v3, v0, Lcom/policydm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v4, "Copy"

    const/4 v5, 0x0

    iget-object v6, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "200"

    invoke-static/range {v2 .. v7}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v21

    .line 7440
    .end local v13    # "i":I
    .end local v22    # "szData":Ljava/lang/String;
    :goto_8
    iget-object v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v21

    invoke-static {v3, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 7441
    iget-object v11, v11, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    goto/16 :goto_0

    .line 7323
    .restart local v13    # "i":I
    .restart local v22    # "szData":Ljava/lang/String;
    :cond_15
    iget v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->prevBufPos:I

    add-int/2addr v3, v10

    iput v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->prevBufPos:I

    .line 7324
    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 7327
    move-object/from16 v0, p1

    iget v3, v0, Lcom/policydm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v4, "Copy"

    const/4 v5, 0x0

    iget-object v6, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "213"

    invoke-static/range {v2 .. v7}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v21

    goto :goto_8

    .line 7332
    .end local v13    # "i":I
    .end local v22    # "szData":Ljava/lang/String;
    :cond_16
    iget-object v3, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    move-object/from16 v0, v27

    invoke-static {v3, v0}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmMakeParentPath(Ljava/lang/String;[C)V

    .line 7333
    invoke-static/range {v27 .. v27}, Lcom/policydm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v23

    .line 7335
    .local v23, "szPathName":Ljava/lang/String;
    move-object/from16 v0, v16

    move-object/from16 v1, v23

    invoke-static {v0, v1}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v15

    .line 7336
    if-nez v15, :cond_18

    .line 7339
    iget v3, v14, Lcom/policydm/eng/parser/XDMParserItem;->moredata:I

    if-lez v3, :cond_17

    .line 7341
    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 7344
    :cond_17
    move-object/from16 v0, p1

    iget v3, v0, Lcom/policydm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v4, "Copy"

    const/4 v5, 0x0

    iget-object v6, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "500"

    invoke-static/range {v2 .. v7}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v21

    .line 7345
    iget-object v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v21

    invoke-static {v3, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 7346
    iget-object v11, v11, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 7350
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 7352
    const-string v3, "507"

    move-object/from16 v0, p3

    iput-object v3, v0, Lcom/policydm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    goto/16 :goto_0

    .line 7359
    :cond_18
    const/4 v3, 0x1

    move-object/from16 v0, v16

    invoke-static {v0, v15, v3}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmCheckAcl(Lcom/policydm/eng/core/XDMOmTree;Lcom/policydm/eng/core/XDMVnode;I)Z

    move-result v3

    if-nez v3, :cond_1a

    .line 7362
    iget v3, v14, Lcom/policydm/eng/parser/XDMParserItem;->moredata:I

    if-lez v3, :cond_19

    .line 7364
    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 7366
    :cond_19
    move-object/from16 v0, p1

    iget v3, v0, Lcom/policydm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v4, "Copy"

    const/4 v5, 0x0

    iget-object v6, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "425"

    invoke-static/range {v2 .. v7}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v21

    .line 7367
    iget-object v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v21

    invoke-static {v3, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 7368
    iget-object v11, v11, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 7369
    goto/16 :goto_0

    .line 7372
    :cond_1a
    iput v9, v2, Lcom/policydm/eng/core/XDMWorkspace;->dataTotalSize:I

    .line 7373
    move/from16 v12, v20

    .line 7374
    const/16 v24, 0x0

    .line 7375
    iget-object v4, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    iget v5, v2, Lcom/policydm/eng/core/XDMWorkspace;->dataTotalSize:I

    const/4 v6, 0x0

    invoke-static/range {v19 .. v19}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v3, v16

    invoke-static/range {v3 .. v8}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmWrite(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;IILjava/lang/Object;I)I

    move-result v18

    .line 7378
    .local v18, "res":I
    if-gez v18, :cond_1c

    .line 7381
    iget v3, v14, Lcom/policydm/eng/parser/XDMParserItem;->moredata:I

    if-lez v3, :cond_1b

    .line 7383
    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 7387
    :cond_1b
    move-object/from16 v0, p1

    iget v3, v0, Lcom/policydm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v4, "Copy"

    const/4 v5, 0x0

    iget-object v6, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "500"

    invoke-static/range {v2 .. v7}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v21

    .line 7388
    iget-object v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v21

    invoke-static {v3, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 7390
    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 7391
    iget-object v11, v11, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 7395
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 7397
    const-string v3, "507"

    move-object/from16 v0, p3

    iput-object v3, v0, Lcom/policydm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    goto/16 :goto_0

    .line 7404
    :cond_1c
    iget-object v3, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-static {v0, v3}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v15

    .line 7405
    if-eqz v15, :cond_1f

    .line 7407
    invoke-static/range {v24 .. v24}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1e

    .line 7409
    iget-object v3, v15, Lcom/policydm/eng/core/XDMVnode;->type:Lcom/policydm/eng/core/XDMOmList;

    if-eqz v3, :cond_1d

    .line 7411
    iget-object v3, v15, Lcom/policydm/eng/core/XDMVnode;->type:Lcom/policydm/eng/core/XDMOmList;

    invoke-static {v3}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmVfsDeleteMimeList(Lcom/policydm/eng/core/XDMOmList;)V

    .line 7413
    :cond_1d
    new-instance v3, Lcom/policydm/eng/core/XDMOmList;

    invoke-direct {v3}, Lcom/policydm/eng/core/XDMOmList;-><init>()V

    iput-object v3, v15, Lcom/policydm/eng/core/XDMVnode;->type:Lcom/policydm/eng/core/XDMOmList;

    .line 7414
    iget-object v3, v15, Lcom/policydm/eng/core/XDMVnode;->type:Lcom/policydm/eng/core/XDMOmList;

    move-object/from16 v0, v24

    iput-object v0, v3, Lcom/policydm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    .line 7415
    iget-object v3, v15, Lcom/policydm/eng/core/XDMVnode;->type:Lcom/policydm/eng/core/XDMOmList;

    const/4 v4, 0x0

    iput-object v4, v3, Lcom/policydm/eng/core/XDMOmList;->next:Lcom/policydm/eng/core/XDMOmList;

    .line 7417
    :cond_1e
    iput v12, v15, Lcom/policydm/eng/core/XDMVnode;->format:I

    .line 7421
    :cond_1f
    iget v3, v14, Lcom/policydm/eng/parser/XDMParserItem;->moredata:I

    if-nez v3, :cond_20

    .line 7423
    const/4 v3, 0x0

    iput v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->prevBufPos:I

    .line 7424
    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 7425
    const/4 v3, 0x0

    iput v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->dataTotalSize:I

    .line 7428
    move-object/from16 v0, p1

    iget v3, v0, Lcom/policydm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v4, "Copy"

    const/4 v5, 0x0

    iget-object v6, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "200"

    invoke-static/range {v2 .. v7}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v21

    goto/16 :goto_8

    .line 7432
    :cond_20
    iget v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->prevBufPos:I

    add-int/2addr v3, v9

    iput v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->prevBufPos:I

    .line 7433
    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 7436
    move-object/from16 v0, p1

    iget v3, v0, Lcom/policydm/eng/parser/XDMParserCopy;->cmdid:I

    const-string v4, "Copy"

    const/4 v5, 0x0

    iget-object v6, v14, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "213"

    invoke-static/range {v2 .. v7}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v21

    goto/16 :goto_8

    .line 7443
    .end local v12    # "format":I
    .end local v15    # "node":Lcom/policydm/eng/core/XDMVnode;
    .end local v18    # "res":I
    .end local v20    # "sourceformat":I
    .end local v23    # "szPathName":Ljava/lang/String;
    .end local v24    # "szType":Ljava/lang/String;
    :cond_21
    const/4 v3, 0x0

    return v3
.end method

.method public xdmAgentCmdDelete(Lcom/policydm/eng/parser/XDMParserDelete;ZLcom/policydm/eng/parser/XDMParserStatus;)I
    .locals 14
    .param p1, "delcmd"    # Lcom/policydm/eng/parser/XDMParserDelete;
    .param p2, "isAtomic"    # Z
    .param p3, "atomic_status"    # Lcom/policydm/eng/parser/XDMParserStatus;

    .prologue
    .line 7448
    sget-object v1, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    .line 7449
    .local v1, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    iget-object v10, v1, Lcom/policydm/eng/core/XDMWorkspace;->om:Lcom/policydm/eng/core/XDMOmTree;

    .line 7450
    .local v10, "om":Lcom/policydm/eng/core/XDMOmTree;
    const/4 v13, 0x0

    .line 7451
    .local v13, "status":Lcom/policydm/eng/parser/XDMParserStatus;
    const/4 v8, 0x0

    .line 7452
    .local v8, "item":Lcom/policydm/eng/parser/XDMParserItem;
    const/4 v7, 0x0

    .line 7453
    .local v7, "cur":Lcom/policydm/eng/core/XDMList;
    const/4 v9, 0x0

    .line 7457
    .local v9, "node":Lcom/policydm/eng/core/XDMVnode;
    invoke-virtual {p0}, Lcom/policydm/agent/XDMAgent;->xdmAgentCmdUicAlert()Z

    move-result v11

    .line 7459
    .local v11, "process":Z
    iget-object v7, p1, Lcom/policydm/eng/parser/XDMParserDelete;->itemlist:Lcom/policydm/eng/core/XDMList;

    .line 7460
    :goto_0
    if-eqz v7, :cond_15

    .line 7462
    iget-object v8, v7, Lcom/policydm/eng/core/XDMList;->item:Ljava/lang/Object;

    .end local v8    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    check-cast v8, Lcom/policydm/eng/parser/XDMParserItem;

    .line 7463
    .restart local v8    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    const/4 v13, 0x0

    .line 7464
    iget v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->serverAuthState:I

    const/4 v3, 0x1

    if-eq v2, v3, :cond_1

    .line 7466
    iget-object v2, v8, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 7468
    iget v2, p1, Lcom/policydm/eng/parser/XDMParserDelete;->cmdid:I

    const-string v3, "Delete"

    const/4 v4, 0x0

    iget-object v5, v8, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    iget-object v6, v1, Lcom/policydm/eng/core/XDMWorkspace;->m_szStatusReturnCode:Ljava/lang/String;

    invoke-static/range {v1 .. v6}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v13

    .line 7474
    :goto_1
    iget-object v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v2, v13}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 7475
    iget-object v7, v7, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 7476
    goto :goto_0

    .line 7472
    :cond_0
    iget v2, p1, Lcom/policydm/eng/parser/XDMParserDelete;->cmdid:I

    const-string v3, "Delete"

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget-object v6, v1, Lcom/policydm/eng/core/XDMWorkspace;->m_szStatusReturnCode:Ljava/lang/String;

    invoke-static/range {v1 .. v6}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v13

    goto :goto_1

    .line 7479
    :cond_1
    iget-object v2, v8, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 7481
    iget-object v2, v8, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v10, v2}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v9

    .line 7484
    :cond_2
    if-nez v11, :cond_6

    .line 7486
    iget-object v2, v8, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 7488
    iget v2, p1, Lcom/policydm/eng/parser/XDMParserDelete;->cmdid:I

    const-string v3, "Delete"

    const/4 v4, 0x0

    iget-object v5, v8, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "215"

    invoke-static/range {v1 .. v6}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v13

    .line 7495
    :goto_2
    if-eqz p2, :cond_3

    if-eqz p3, :cond_3

    .line 7497
    const-string v2, "507"

    move-object/from16 v0, p3

    iput-object v2, v0, Lcom/policydm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    .line 7619
    :cond_3
    :goto_3
    if-eqz v13, :cond_4

    .line 7621
    iget-object v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v2, v13}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 7624
    :cond_4
    iget-object v7, v7, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    goto :goto_0

    .line 7492
    :cond_5
    iget v2, p1, Lcom/policydm/eng/parser/XDMParserDelete;->cmdid:I

    const-string v3, "Delete"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string v6, "215"

    invoke-static/range {v1 .. v6}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v13

    goto :goto_2

    .line 7501
    :cond_6
    if-eqz p2, :cond_e

    iget-object v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->atomicStep:Lcom/policydm/interfaces/XDMInterface$XDMAtomicStep;

    sget-object v3, Lcom/policydm/interfaces/XDMInterface$XDMAtomicStep;->XDM_ATOMIC_NONE:Lcom/policydm/interfaces/XDMInterface$XDMAtomicStep;

    if-eq v2, v3, :cond_e

    .line 7503
    iget-object v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->tmpItem:Lcom/policydm/eng/parser/XDMParserItem;

    if-eqz v2, :cond_a

    .line 7506
    iget-object v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->tmpItem:Lcom/policydm/eng/parser/XDMParserItem;

    invoke-virtual {v2, v8}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 7508
    iget-object v2, v8, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 7510
    iget v2, p1, Lcom/policydm/eng/parser/XDMParserDelete;->cmdid:I

    const-string v3, "Delete"

    const/4 v4, 0x0

    iget-object v5, v8, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "404"

    invoke-static/range {v1 .. v6}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v13

    .line 7516
    :goto_4
    sget-object v2, Lcom/policydm/interfaces/XDMInterface$XDMAtomicStep;->XDM_ATOMIC_STEP_NOT_EXEC:Lcom/policydm/interfaces/XDMInterface$XDMAtomicStep;

    iput-object v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->atomicStep:Lcom/policydm/interfaces/XDMInterface$XDMAtomicStep;

    .line 7517
    const/4 v2, 0x0

    iput-object v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->tmpItem:Lcom/policydm/eng/parser/XDMParserItem;

    goto :goto_3

    .line 7514
    :cond_7
    iget v2, p1, Lcom/policydm/eng/parser/XDMParserDelete;->cmdid:I

    const-string v3, "Delete"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string v6, "404"

    invoke-static/range {v1 .. v6}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v13

    goto :goto_4

    .line 7521
    :cond_8
    iget-object v2, v8, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 7523
    iget v2, p1, Lcom/policydm/eng/parser/XDMParserDelete;->cmdid:I

    const-string v3, "Delete"

    const/4 v4, 0x0

    iget-object v5, v8, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "216"

    invoke-static/range {v1 .. v6}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v13

    goto :goto_3

    .line 7527
    :cond_9
    iget v2, p1, Lcom/policydm/eng/parser/XDMParserDelete;->cmdid:I

    const-string v3, "Delete"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string v6, "216"

    invoke-static/range {v1 .. v6}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v13

    goto :goto_3

    .line 7533
    :cond_a
    iget-object v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->atomicStep:Lcom/policydm/interfaces/XDMInterface$XDMAtomicStep;

    sget-object v3, Lcom/policydm/interfaces/XDMInterface$XDMAtomicStep;->XDM_ATOMIC_STEP_NOT_EXEC:Lcom/policydm/interfaces/XDMInterface$XDMAtomicStep;

    if-ne v2, v3, :cond_c

    .line 7535
    iget-object v2, v8, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 7537
    iget v2, p1, Lcom/policydm/eng/parser/XDMParserDelete;->cmdid:I

    const-string v3, "Delete"

    const/4 v4, 0x0

    iget-object v5, v8, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "215"

    invoke-static/range {v1 .. v6}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v13

    goto/16 :goto_3

    .line 7541
    :cond_b
    iget v2, p1, Lcom/policydm/eng/parser/XDMParserDelete;->cmdid:I

    const-string v3, "Delete"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string v6, "215"

    invoke-static/range {v1 .. v6}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v13

    goto/16 :goto_3

    .line 7546
    :cond_c
    iget-object v2, v8, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_d

    .line 7548
    iget v2, p1, Lcom/policydm/eng/parser/XDMParserDelete;->cmdid:I

    const-string v3, "Delete"

    const/4 v4, 0x0

    iget-object v5, v8, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "216"

    invoke-static/range {v1 .. v6}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v13

    goto/16 :goto_3

    .line 7552
    :cond_d
    iget v2, p1, Lcom/policydm/eng/parser/XDMParserDelete;->cmdid:I

    const-string v3, "Delete"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string v6, "216"

    invoke-static/range {v1 .. v6}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v13

    goto/16 :goto_3

    .line 7557
    :cond_e
    if-nez v9, :cond_10

    .line 7559
    iget-object v2, v8, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_f

    .line 7561
    iget v2, p1, Lcom/policydm/eng/parser/XDMParserDelete;->cmdid:I

    const-string v3, "Delete"

    const/4 v4, 0x0

    iget-object v5, v8, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "404"

    invoke-static/range {v1 .. v6}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v13

    .line 7568
    :goto_5
    if-eqz p2, :cond_3

    if-eqz p3, :cond_3

    .line 7570
    const-string v2, "507"

    move-object/from16 v0, p3

    iput-object v2, v0, Lcom/policydm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    goto/16 :goto_3

    .line 7565
    :cond_f
    iget v2, p1, Lcom/policydm/eng/parser/XDMParserDelete;->cmdid:I

    const-string v3, "Delete"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string v6, "404"

    invoke-static/range {v1 .. v6}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v13

    goto :goto_5

    .line 7574
    :cond_10
    iget-object v2, v8, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-virtual {p0, v10, v2}, Lcom/policydm/agent/XDMAgent;->xdmAgentIsPermanentNode(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 7576
    iget v2, p1, Lcom/policydm/eng/parser/XDMParserDelete;->cmdid:I

    const-string v3, "Delete"

    const/4 v4, 0x0

    iget-object v5, v8, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "405"

    invoke-static/range {v1 .. v6}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v13

    .line 7578
    if-eqz p2, :cond_3

    if-eqz p3, :cond_3

    .line 7580
    const-string v2, "507"

    move-object/from16 v0, p3

    iput-object v2, v0, Lcom/policydm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    goto/16 :goto_3

    .line 7583
    :cond_11
    const/4 v2, 0x2

    invoke-static {v10, v9, v2}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmCheckAcl(Lcom/policydm/eng/core/XDMOmTree;Lcom/policydm/eng/core/XDMVnode;I)Z

    move-result v2

    if-nez v2, :cond_12

    .line 7585
    iget v2, p1, Lcom/policydm/eng/parser/XDMParserDelete;->cmdid:I

    const-string v3, "Delete"

    const/4 v4, 0x0

    iget-object v5, v8, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "425"

    invoke-static/range {v1 .. v6}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v13

    .line 7587
    if-eqz p2, :cond_3

    if-eqz p3, :cond_3

    .line 7589
    const-string v2, "507"

    move-object/from16 v0, p3

    iput-object v2, v0, Lcom/policydm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    goto/16 :goto_3

    .line 7592
    :cond_12
    iget-object v2, v10, Lcom/policydm/eng/core/XDMOmTree;->vfs:Lcom/policydm/eng/core/XDMOmVfs;

    iget-object v2, v2, Lcom/policydm/eng/core/XDMOmVfs;->root:Lcom/policydm/eng/core/XDMVnode;

    if-ne v9, v2, :cond_13

    .line 7594
    iget v2, p1, Lcom/policydm/eng/parser/XDMParserDelete;->cmdid:I

    const-string v3, "Delete"

    const/4 v4, 0x0

    iget-object v5, v8, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "405"

    invoke-static/range {v1 .. v6}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v13

    .line 7596
    if-eqz p2, :cond_3

    if-eqz p3, :cond_3

    .line 7598
    const-string v2, "507"

    move-object/from16 v0, p3

    iput-object v2, v0, Lcom/policydm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    goto/16 :goto_3

    .line 7603
    :cond_13
    iget-object v2, v8, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 7604
    iget-object v2, v8, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-static {v10, v2, v3}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmDelete(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;Z)I

    move-result v12

    .line 7605
    .local v12, "res":I
    if-gez v12, :cond_14

    .line 7607
    iget v2, p1, Lcom/policydm/eng/parser/XDMParserDelete;->cmdid:I

    const-string v3, "Delete"

    const/4 v4, 0x0

    iget-object v5, v8, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "405"

    invoke-static/range {v1 .. v6}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v13

    .line 7608
    if-eqz p2, :cond_3

    if-eqz p3, :cond_3

    .line 7610
    const-string v2, "507"

    move-object/from16 v0, p3

    iput-object v2, v0, Lcom/policydm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    goto/16 :goto_3

    .line 7615
    :cond_14
    iget v2, p1, Lcom/policydm/eng/parser/XDMParserDelete;->cmdid:I

    const-string v3, "Delete"

    const/4 v4, 0x0

    iget-object v5, v8, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "200"

    invoke-static/range {v1 .. v6}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v13

    goto/16 :goto_3

    .line 7626
    .end local v12    # "res":I
    :cond_15
    const/4 v2, 0x0

    return v2
.end method

.method public xdmAgentCmdExec(Lcom/policydm/eng/parser/XDMParserExec;)I
    .locals 12
    .param p1, "exec"    # Lcom/policydm/eng/parser/XDMParserExec;

    .prologue
    .line 5114
    sget-object v0, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    .line 5115
    .local v0, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    const/4 v7, 0x0

    .line 5116
    .local v7, "item":Lcom/policydm/eng/parser/XDMParserItem;
    const/4 v6, 0x0

    .line 5117
    .local v6, "cur":Lcom/policydm/eng/core/XDMList;
    const/4 v9, 0x0

    .line 5118
    .local v9, "node":Lcom/policydm/eng/core/XDMVnode;
    const/4 v11, 0x0

    .line 5119
    .local v11, "status":Lcom/policydm/eng/parser/XDMParserStatus;
    iget-object v10, v0, Lcom/policydm/eng/core/XDMWorkspace;->om:Lcom/policydm/eng/core/XDMOmTree;

    .line 5121
    .local v10, "om":Lcom/policydm/eng/core/XDMOmTree;
    iget-object v6, p1, Lcom/policydm/eng/parser/XDMParserExec;->itemlist:Lcom/policydm/eng/core/XDMList;

    .line 5123
    :goto_0
    if-eqz v6, :cond_d

    .line 5125
    iget-object v7, v6, Lcom/policydm/eng/core/XDMList;->item:Ljava/lang/Object;

    .end local v7    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    check-cast v7, Lcom/policydm/eng/parser/XDMParserItem;

    .line 5127
    .restart local v7    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    iget v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->serverAuthState:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    .line 5129
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOStatus(I)V

    .line 5130
    iget-object v1, v7, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 5132
    iget v1, p1, Lcom/policydm/eng/parser/XDMParserExec;->cmdid:I

    const-string v2, "Exec"

    const/4 v3, 0x0

    iget-object v4, v7, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    iget-object v5, v0, Lcom/policydm/eng/core/XDMWorkspace;->m_szStatusReturnCode:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v11

    .line 5139
    :goto_1
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v1, v11}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 5140
    iget-object v6, v6, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 5141
    goto :goto_0

    .line 5136
    :cond_0
    iget v1, p1, Lcom/policydm/eng/parser/XDMParserExec;->cmdid:I

    const-string v2, "Exec"

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v5, v0, Lcom/policydm/eng/core/XDMWorkspace;->m_szStatusReturnCode:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v11

    goto :goto_1

    .line 5144
    :cond_1
    iget-object v1, v7, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 5146
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOStatus(I)V

    .line 5147
    iget v1, p1, Lcom/policydm/eng/parser/XDMParserExec;->cmdid:I

    const-string v2, "Exec"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "404"

    invoke-static/range {v0 .. v5}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v11

    .line 5148
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v1, v11}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 5149
    iget-object v6, v6, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 5150
    goto :goto_0

    .line 5153
    :cond_2
    iget-object v1, v7, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v10, v1}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v9

    .line 5154
    if-nez v9, :cond_3

    .line 5156
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOStatus(I)V

    .line 5157
    iget v1, p1, Lcom/policydm/eng/parser/XDMParserExec;->cmdid:I

    const-string v2, "Exec"

    const/4 v3, 0x0

    iget-object v4, v7, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v5, "404"

    invoke-static/range {v0 .. v5}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v11

    .line 5158
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v1, v11}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 5159
    iget-object v6, v6, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 5160
    goto :goto_0

    .line 5163
    :cond_3
    iget-object v1, v7, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/policydm/agent/XDMAgent;->xdmAgentIsAccessibleNode(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 5165
    iget v1, p1, Lcom/policydm/eng/parser/XDMParserExec;->cmdid:I

    const-string v2, "Exec"

    const/4 v3, 0x0

    iget-object v4, v7, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v5, "405"

    invoke-static/range {v0 .. v5}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v11

    .line 5166
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v1, v11}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 5167
    iget-object v6, v6, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 5168
    goto/16 :goto_0

    .line 5171
    :cond_4
    iget-object v1, v7, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-virtual {p0, v10, v1}, Lcom/policydm/agent/XDMAgent;->xdmAgentIsPermanentNode(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 5173
    iget v1, p1, Lcom/policydm/eng/parser/XDMParserExec;->cmdid:I

    const-string v2, "Exec"

    const/4 v3, 0x0

    iget-object v4, v7, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v5, "405"

    invoke-static/range {v0 .. v5}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v11

    .line 5174
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v1, v11}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 5175
    iget-object v6, v6, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 5176
    goto/16 :goto_0

    .line 5180
    :cond_5
    const/4 v1, 0x4

    invoke-static {v10, v9, v1}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmCheckAcl(Lcom/policydm/eng/core/XDMOmTree;Lcom/policydm/eng/core/XDMVnode;I)Z

    move-result v1

    if-nez v1, :cond_6

    .line 5182
    iget v1, p1, Lcom/policydm/eng/parser/XDMParserExec;->cmdid:I

    const-string v2, "Exec"

    const/4 v3, 0x0

    iget-object v4, v7, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v5, "425"

    invoke-static/range {v0 .. v5}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v11

    .line 5183
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v1, v11}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 5184
    iget-object v6, v6, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 5185
    goto/16 :goto_0

    .line 5188
    :cond_6
    iget-object v1, v7, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 5190
    iget-object v1, v7, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 5194
    iget-object v1, v7, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-static {v10, v1, v2}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmCheckAclCurrentNode(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;I)Z

    move-result v1

    if-nez v1, :cond_7

    .line 5196
    iget v1, p1, Lcom/policydm/eng/parser/XDMParserExec;->cmdid:I

    const-string v2, "Exec"

    const/4 v3, 0x0

    iget-object v4, v7, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v5, "425"

    invoke-static/range {v0 .. v5}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v11

    .line 5197
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v1, v11}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 5198
    iget-object v6, v6, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 5199
    goto/16 :goto_0

    .line 5202
    :cond_7
    iget-object v1, v7, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v2, "/DownloadAndUpdate"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 5204
    const-string v1, "Node is not exsisted"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 5206
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOStatus(I)V

    .line 5207
    iget v1, p1, Lcom/policydm/eng/parser/XDMParserExec;->cmdid:I

    const-string v2, "Exec"

    const/4 v3, 0x0

    iget-object v4, v7, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v5, "406"

    invoke-static/range {v0 .. v5}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v11

    .line 5210
    sget-object v1, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    iget-object v1, v1, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v1, v11}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 5211
    iget-object v6, v6, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 5212
    goto/16 :goto_0

    .line 5216
    :cond_8
    iget-object v1, v7, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v2, "/DownloadAndUpdate"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 5218
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/policydm/db/XDBAgentAdp;->xdbSetDmAgentType(I)V

    .line 5236
    :goto_2
    iget-object v1, p1, Lcom/policydm/eng/parser/XDMParserExec;->m_szCorrelator:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 5238
    iget-object v1, p1, Lcom/policydm/eng/parser/XDMParserExec;->m_szCorrelator:Ljava/lang/String;

    invoke-static {v1}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOCorrelator(Ljava/lang/String;)V

    .line 5241
    :cond_9
    invoke-static {}, Lcom/policydm/db/XDBAgentAdp;->xdbGetDmAgentType()I

    move-result v8

    .line 5243
    .local v8, "nAgentType":I
    const/4 v1, 0x1

    if-ne v8, v1, :cond_c

    .line 5245
    invoke-direct {p0, v0, p1, v7}, Lcom/policydm/agent/XDMAgent;->xdmAgentCmdExecFumo(Lcom/policydm/eng/core/XDMWorkspace;Lcom/policydm/eng/parser/XDMParserExec;Lcom/policydm/eng/parser/XDMParserItem;)V

    .line 5252
    :goto_3
    iget-object v6, v6, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 5253
    goto/16 :goto_0

    .line 5222
    .end local v8    # "nAgentType":I
    :cond_a
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/policydm/db/XDBAgentAdp;->xdbSetDmAgentType(I)V

    goto :goto_2

    .line 5228
    :cond_b
    const-string v1, "Error item->target->pLocURI is NULL"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 5229
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOStatus(I)V

    .line 5230
    iget v1, p1, Lcom/policydm/eng/parser/XDMParserExec;->cmdid:I

    const-string v2, "Exec"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "403"

    invoke-static/range {v0 .. v5}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v11

    .line 5231
    sget-object v1, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    iget-object v1, v1, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v1, v11}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 5232
    iget-object v6, v6, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 5233
    goto/16 :goto_0

    .line 5249
    .restart local v8    # "nAgentType":I
    :cond_c
    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_3

    .line 5254
    .end local v8    # "nAgentType":I
    :cond_d
    const/4 v1, 0x0

    return v1
.end method

.method public xdmAgentCmdGet(Lcom/policydm/eng/parser/XDMParserGet;Z)I
    .locals 29
    .param p1, "get"    # Lcom/policydm/eng/parser/XDMParserGet;
    .param p2, "isAtomic"    # Z

    .prologue
    .line 2846
    sget-object v3, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    .line 2847
    .local v3, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    iget-object v0, v3, Lcom/policydm/eng/core/XDMWorkspace;->om:Lcom/policydm/eng/core/XDMOmTree;

    move-object/from16 v20, v0

    .line 2848
    .local v20, "om":Lcom/policydm/eng/core/XDMOmTree;
    const/16 v18, 0x0

    .line 2849
    .local v18, "item":Lcom/policydm/eng/parser/XDMParserItem;
    const/16 v25, 0x0

    .line 2850
    .local v25, "status":Lcom/policydm/eng/parser/XDMParserStatus;
    const/16 v24, 0x0

    .line 2851
    .local v24, "results":Lcom/policydm/eng/parser/XDMParserResults;
    const/4 v7, 0x0

    .line 2852
    .local v7, "szType":Ljava/lang/String;
    const/4 v6, 0x0

    .line 2853
    .local v6, "szFormat":Ljava/lang/String;
    const/16 v27, 0x0

    .line 2854
    .local v27, "szResultbuf":Ljava/lang/String;
    const/16 v4, 0x64

    new-array v15, v4, [Ljava/lang/String;

    .line 2855
    .local v15, "chlist":[Ljava/lang/String;
    const/16 v16, 0x0

    .line 2857
    .local v16, "cur":Lcom/policydm/eng/core/XDMList;
    const/16 v19, 0x0

    .line 2858
    .local v19, "node":Lcom/policydm/eng/core/XDMVnode;
    const/16 v22, 0x1

    .line 2859
    .local v22, "process":Z
    const/4 v14, 0x0

    .line 2861
    .local v14, "bufsize":I
    const/16 v21, 0x0

    .line 2863
    .local v21, "pData":[C
    invoke-virtual/range {p0 .. p0}, Lcom/policydm/agent/XDMAgent;->xdmAgentCmdUicAlert()Z

    move-result v22

    .line 2865
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/policydm/eng/parser/XDMParserGet;->itemlist:Lcom/policydm/eng/core/XDMList;

    move-object/from16 v16, v0

    move-object/from16 v26, v6

    .end local v6    # "szFormat":Ljava/lang/String;
    .local v26, "szFormat":Ljava/lang/String;
    move-object/from16 v28, v7

    .line 2866
    .end local v7    # "szType":Ljava/lang/String;
    .local v28, "szType":Ljava/lang/String;
    :goto_0
    if-eqz v16, :cond_17

    .line 2868
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/policydm/eng/core/XDMList;->item:Ljava/lang/Object;

    move-object/from16 v18, v0

    .end local v18    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    check-cast v18, Lcom/policydm/eng/parser/XDMParserItem;

    .line 2870
    .restart local v18    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    iget v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->serverAuthState:I

    const/4 v5, 0x1

    if-eq v4, v5, :cond_1

    .line 2872
    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2874
    move-object/from16 v0, p1

    iget v4, v0, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    const-string v5, "Get"

    const/4 v6, 0x0

    move-object/from16 v0, v18

    iget-object v7, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    iget-object v8, v3, Lcom/policydm/eng/core/XDMWorkspace;->m_szStatusReturnCode:Ljava/lang/String;

    invoke-static/range {v3 .. v8}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v25

    .line 2880
    :goto_1
    iget-object v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v25

    invoke-static {v4, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 2881
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    move-object/from16 v16, v0

    .line 2882
    goto :goto_0

    .line 2878
    :cond_0
    move-object/from16 v0, p1

    iget v4, v0, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    const-string v5, "Get"

    const/4 v6, 0x0

    const/4 v7, 0x0

    iget-object v8, v3, Lcom/policydm/eng/core/XDMWorkspace;->m_szStatusReturnCode:Ljava/lang/String;

    invoke-static/range {v3 .. v8}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v25

    goto :goto_1

    .line 2885
    :cond_1
    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2888
    move-object/from16 v0, p1

    iget v4, v0, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    const-string v5, "Get"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-string v8, "404"

    invoke-static/range {v3 .. v8}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v25

    .line 2889
    iget-object v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v25

    invoke-static {v4, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 2890
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    move-object/from16 v16, v0

    .line 2891
    goto :goto_0

    .line 2894
    :cond_2
    if-nez v22, :cond_5

    .line 2896
    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 2898
    move-object/from16 v0, p1

    iget v4, v0, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    const-string v5, "Get"

    const/4 v6, 0x0

    move-object/from16 v0, v18

    iget-object v7, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "215"

    invoke-static/range {v3 .. v8}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v25

    .line 2905
    :goto_2
    if-eqz v25, :cond_3

    .line 2907
    iget-object v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v25

    invoke-static {v4, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 2909
    :cond_3
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    move-object/from16 v16, v0

    .line 2910
    goto/16 :goto_0

    .line 2902
    :cond_4
    move-object/from16 v0, p1

    iget v4, v0, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    const-string v5, "Get"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-string v8, "215"

    invoke-static/range {v3 .. v8}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v25

    goto :goto_2

    .line 2913
    :cond_5
    if-eqz p2, :cond_8

    .line 2915
    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 2917
    move-object/from16 v0, p1

    iget v4, v0, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    const-string v5, "Get"

    move-object/from16 v0, v18

    iget-object v6, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "215"

    invoke-static/range {v3 .. v8}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v25

    .line 2924
    :goto_3
    if-eqz v25, :cond_6

    .line 2926
    iget-object v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v25

    invoke-static {v4, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 2928
    :cond_6
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    move-object/from16 v16, v0

    .line 2929
    goto/16 :goto_0

    .line 2921
    :cond_7
    move-object/from16 v0, p1

    iget v4, v0, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    const-string v5, "Get"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-string v8, "215"

    invoke-static/range {v3 .. v8}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v25

    goto :goto_3

    .line 2932
    :cond_8
    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v5, "?"

    invoke-static {v4, v5}, Lcom/policydm/eng/core/XDMMem;->xdmLibStrstr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_9

    .line 2934
    const-string v4, "Get"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, p1

    invoke-virtual {v0, v4, v1, v2}, Lcom/policydm/agent/XDMAgent;->xdmAgentCmdProp(Ljava/lang/String;Lcom/policydm/eng/parser/XDMParserItem;Ljava/lang/Object;)I

    move-result v23

    .line 2935
    .local v23, "res":I
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    move-object/from16 v16, v0

    .line 2936
    goto/16 :goto_0

    .line 2939
    .end local v23    # "res":I
    :cond_9
    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-static {v0, v4}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v19

    .line 2940
    if-nez v19, :cond_a

    .line 2942
    move-object/from16 v0, p1

    iget v4, v0, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    const-string v5, "Get"

    const/4 v6, 0x0

    move-object/from16 v0, v18

    iget-object v7, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "404"

    invoke-static/range {v3 .. v8}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v25

    .line 2943
    iget-object v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v25

    invoke-static {v4, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 2944
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    move-object/from16 v16, v0

    .line 2945
    goto/16 :goto_0

    .line 2948
    :cond_a
    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/policydm/agent/XDMAgent;->xdmAgentIsAccessibleNode(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_b

    .line 2950
    move-object/from16 v0, p1

    iget v4, v0, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    const-string v5, "Get"

    const/4 v6, 0x0

    move-object/from16 v0, v18

    iget-object v7, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "405"

    invoke-static/range {v3 .. v8}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v25

    .line 2951
    iget-object v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v25

    invoke-static {v4, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 2952
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    move-object/from16 v16, v0

    .line 2953
    goto/16 :goto_0

    .line 2955
    :cond_b
    const/16 v4, 0x8

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-static {v0, v1, v4}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmCheckAcl(Lcom/policydm/eng/core/XDMOmTree;Lcom/policydm/eng/core/XDMVnode;I)Z

    move-result v4

    if-nez v4, :cond_c

    .line 2957
    move-object/from16 v0, p1

    iget v4, v0, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    const-string v5, "Get"

    const/4 v6, 0x0

    move-object/from16 v0, v18

    iget-object v7, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "425"

    invoke-static/range {v3 .. v8}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v25

    .line 2958
    iget-object v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v25

    invoke-static {v4, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 2959
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    move-object/from16 v16, v0

    .line 2960
    goto/16 :goto_0

    .line 2963
    :cond_c
    move-object/from16 v0, v19

    iget v4, v0, Lcom/policydm/eng/core/XDMVnode;->vaddr:I

    if-gez v4, :cond_10

    move-object/from16 v0, v19

    iget v4, v0, Lcom/policydm/eng/core/XDMVnode;->size:I

    if-gtz v4, :cond_10

    .line 2965
    move-object/from16 v0, p1

    iget v4, v0, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    const-string v5, "Get"

    const/4 v6, 0x0

    move-object/from16 v0, v18

    iget-object v7, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "200"

    invoke-static/range {v3 .. v8}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v25

    .line 2966
    iget-object v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v25

    invoke-static {v4, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 2968
    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const/16 v5, 0x64

    move-object/from16 v0, v20

    invoke-static {v0, v4, v15, v5}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetChild(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;[Ljava/lang/String;I)I

    move-result v23

    .line 2972
    .restart local v23    # "res":I
    move-object/from16 v0, v19

    iget v4, v0, Lcom/policydm/eng/core/XDMVnode;->format:I

    invoke-static {v4}, Lcom/policydm/eng/core/XDMOmList;->xdmOmGetFormatString(I)Ljava/lang/String;

    move-result-object v6

    .line 2973
    .end local v26    # "szFormat":Ljava/lang/String;
    .restart local v6    # "szFormat":Ljava/lang/String;
    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/policydm/eng/core/XDMVnode;->type:Lcom/policydm/eng/core/XDMOmList;

    if-eqz v4, :cond_d

    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/policydm/eng/core/XDMVnode;->type:Lcom/policydm/eng/core/XDMOmList;

    iget-object v4, v4, Lcom/policydm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    if-eqz v4, :cond_d

    .line 2975
    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/policydm/eng/core/XDMVnode;->type:Lcom/policydm/eng/core/XDMOmList;

    iget-object v4, v4, Lcom/policydm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 2982
    .end local v28    # "szType":Ljava/lang/String;
    .restart local v7    # "szType":Ljava/lang/String;
    :goto_4
    if-lez v23, :cond_e

    .line 2984
    const/4 v4, 0x0

    aget-object v27, v15, v4

    .line 2985
    const/16 v17, 0x1

    .local v17, "i":I
    :goto_5
    move/from16 v0, v17

    move/from16 v1, v23

    if-ge v0, v1, :cond_f

    .line 2987
    const-string v4, "/"

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    .line 2988
    aget-object v4, v15, v17

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    .line 2985
    add-int/lit8 v17, v17, 0x1

    goto :goto_5

    .line 2979
    .end local v7    # "szType":Ljava/lang/String;
    .end local v17    # "i":I
    .restart local v28    # "szType":Ljava/lang/String;
    :cond_d
    const/4 v7, 0x0

    .end local v28    # "szType":Ljava/lang/String;
    .restart local v7    # "szType":Ljava/lang/String;
    goto :goto_4

    .line 2993
    :cond_e
    const-string v27, ""

    .line 2995
    :cond_f
    move-object/from16 v0, p1

    iget v4, v0, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    move-object/from16 v0, v18

    iget-object v5, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-virtual/range {v27 .. v27}, Ljava/lang/String;->toCharArray()[C

    move-result-object v9

    invoke-static/range {v3 .. v9}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdDetailResults(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I[C)Lcom/policydm/eng/parser/XDMParserResults;

    move-result-object v24

    .line 2996
    iget-object v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->resultsList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v24

    invoke-static {v4, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3051
    .end local v23    # "res":I
    :goto_6
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    move-object/from16 v16, v0

    move-object/from16 v26, v6

    .end local v6    # "szFormat":Ljava/lang/String;
    .restart local v26    # "szFormat":Ljava/lang/String;
    move-object/from16 v28, v7

    .end local v7    # "szType":Ljava/lang/String;
    .restart local v28    # "szType":Ljava/lang/String;
    goto/16 :goto_0

    .line 3000
    :cond_10
    move-object/from16 v0, v19

    iget v4, v0, Lcom/policydm/eng/core/XDMVnode;->size:I

    iget v5, v3, Lcom/policydm/eng/core/XDMWorkspace;->serverMaxObjSize:I

    if-le v4, v5, :cond_11

    .line 3002
    move-object/from16 v0, p1

    iget v9, v0, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    const-string v10, "Get"

    const/4 v11, 0x0

    move-object/from16 v0, v18

    iget-object v12, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v13, "413"

    move-object v8, v3

    invoke-static/range {v8 .. v13}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v25

    .line 3003
    iget-object v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v25

    invoke-static {v4, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3004
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    move-object/from16 v16, v0

    .line 3005
    goto/16 :goto_0

    .line 3008
    :cond_11
    move-object/from16 v0, v19

    iget v14, v0, Lcom/policydm/eng/core/XDMVnode;->size:I

    .line 3010
    move-object/from16 v0, v19

    iget v4, v0, Lcom/policydm/eng/core/XDMVnode;->format:I

    invoke-static {v4}, Lcom/policydm/eng/core/XDMOmList;->xdmOmGetFormatString(I)Ljava/lang/String;

    move-result-object v6

    .line 3012
    .end local v26    # "szFormat":Ljava/lang/String;
    .restart local v6    # "szFormat":Ljava/lang/String;
    move-object/from16 v0, v19

    iget v4, v0, Lcom/policydm/eng/core/XDMVnode;->size:I

    new-array v0, v4, [C

    move-object/from16 v21, v0

    .line 3013
    if-eqz v21, :cond_12

    move-object/from16 v0, v21

    array-length v4, v0

    if-nez v4, :cond_14

    .line 3015
    :cond_12
    move-object/from16 v0, p1

    iget v9, v0, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    const-string v10, "Get"

    const/4 v11, 0x0

    move-object/from16 v0, v18

    iget-object v12, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v13, "215"

    move-object v8, v3

    invoke-static/range {v8 .. v13}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v25

    .line 3016
    if-eqz v25, :cond_13

    .line 3018
    iget-object v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v25

    invoke-static {v4, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3020
    :cond_13
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    move-object/from16 v16, v0

    move-object/from16 v26, v6

    .line 3021
    .end local v6    # "szFormat":Ljava/lang/String;
    .restart local v26    # "szFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 3024
    .end local v26    # "szFormat":Ljava/lang/String;
    .restart local v6    # "szFormat":Ljava/lang/String;
    :cond_14
    move-object/from16 v0, p1

    iget v9, v0, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    const-string v10, "Get"

    const/4 v11, 0x0

    move-object/from16 v0, v18

    iget-object v12, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v13, "200"

    move-object v8, v3

    invoke-static/range {v8 .. v13}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v25

    .line 3026
    iget-object v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v25

    invoke-static {v4, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3028
    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/policydm/eng/core/XDMVnode;->type:Lcom/policydm/eng/core/XDMOmList;

    if-eqz v4, :cond_16

    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/policydm/eng/core/XDMVnode;->type:Lcom/policydm/eng/core/XDMOmList;

    iget-object v4, v4, Lcom/policydm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    if-eqz v4, :cond_16

    .line 3030
    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/policydm/eng/core/XDMVnode;->type:Lcom/policydm/eng/core/XDMOmList;

    iget-object v4, v4, Lcom/policydm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 3037
    .end local v28    # "szType":Ljava/lang/String;
    .restart local v7    # "szType":Ljava/lang/String;
    :goto_7
    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const/4 v5, 0x0

    move-object/from16 v0, v19

    iget v8, v0, Lcom/policydm/eng/core/XDMVnode;->size:I

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-static {v0, v4, v5, v1, v8}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmRead(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 3038
    move-object/from16 v0, p1

    iget v4, v0, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    move-object/from16 v0, v18

    iget-object v5, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    move v8, v14

    move-object/from16 v9, v21

    invoke-static/range {v3 .. v9}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdDetailResults(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I[C)Lcom/policydm/eng/parser/XDMParserResults;

    move-result-object v24

    .line 3040
    iget-object v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->resultsList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v24

    invoke-static {v4, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3042
    if-eqz v21, :cond_15

    .line 3044
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "item.target = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v18

    iget-object v5, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 3045
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "item.data = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Ljava/lang/String;

    move-object/from16 v0, v21

    invoke-direct {v5, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 3048
    :cond_15
    const/4 v6, 0x0

    .line 3049
    const/16 v21, 0x0

    goto/16 :goto_6

    .line 3034
    .end local v7    # "szType":Ljava/lang/String;
    .restart local v28    # "szType":Ljava/lang/String;
    :cond_16
    const/4 v7, 0x0

    .end local v28    # "szType":Ljava/lang/String;
    .restart local v7    # "szType":Ljava/lang/String;
    goto :goto_7

    .line 3053
    .end local v6    # "szFormat":Ljava/lang/String;
    .end local v7    # "szType":Ljava/lang/String;
    .restart local v26    # "szFormat":Ljava/lang/String;
    .restart local v28    # "szType":Ljava/lang/String;
    :cond_17
    const/4 v4, 0x0

    return v4
.end method

.method public xdmAgentCmdProp(Ljava/lang/String;Lcom/policydm/eng/parser/XDMParserItem;Ljava/lang/Object;)I
    .locals 15
    .param p1, "szCmd"    # Ljava/lang/String;
    .param p2, "item"    # Lcom/policydm/eng/parser/XDMParserItem;
    .param p3, "p"    # Ljava/lang/Object;

    .prologue
    .line 3114
    sget-object v1, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    .line 3116
    .local v1, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    const/4 v7, 0x0

    .line 3117
    .local v7, "nodename":[C
    const/4 v10, 0x0

    .line 3118
    .local v10, "prop":[C
    const-string v14, ""

    .line 3119
    .local v14, "szData":Ljava/lang/String;
    const/4 v12, 0x0

    .line 3121
    .local v12, "ret":I
    move-object/from16 v0, p2

    iget-object v14, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    .line 3123
    const-string v2, "Get"

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_3

    move-object/from16 v9, p3

    .line 3125
    check-cast v9, Lcom/policydm/eng/parser/XDMParserGet;

    .line 3127
    .local v9, "get":Lcom/policydm/eng/parser/XDMParserGet;
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3129
    const-string v2, "ptr is null"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 3130
    iget v2, v9, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    const/4 v4, 0x0

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "405"

    move-object/from16 v3, p1

    invoke-static/range {v1 .. v6}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v13

    .line 3131
    .local v13, "status":Lcom/policydm/eng/parser/XDMParserStatus;
    iget-object v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v2, v13}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3132
    const/4 v2, 0x0

    .line 3205
    .end local v9    # "get":Lcom/policydm/eng/parser/XDMParserGet;
    .end local v13    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    :goto_0
    return v2

    .line 3135
    .restart local v9    # "get":Lcom/policydm/eng/parser/XDMParserGet;
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ptr = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 3136
    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v2

    new-array v7, v2, [C

    .line 3137
    invoke-virtual {v14}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    const/16 v3, 0x3f

    invoke-static {v2, v3, v7}, Lcom/policydm/eng/core/XDMMem;->xdmLibStrsplit([CC[C)Ljava/lang/String;

    move-result-object v14

    .line 3138
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 3140
    const-string v2, "ptr is null"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 3141
    iget v2, v9, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    const/4 v4, 0x0

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "405"

    move-object/from16 v3, p1

    invoke-static/range {v1 .. v6}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v13

    .line 3142
    .restart local v13    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    iget-object v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v2, v13}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3143
    const/4 v2, 0x0

    goto :goto_0

    .line 3146
    .end local v13    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ptr = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 3147
    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v2

    new-array v10, v2, [C

    .line 3148
    invoke-virtual {v14}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    const/16 v3, 0x3d

    invoke-static {v2, v3, v10}, Lcom/policydm/eng/core/XDMMem;->xdmLibStrsplit([CC[C)Ljava/lang/String;

    move-result-object v14

    .line 3149
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 3151
    const-string v2, "ptr is null"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 3152
    iget v2, v9, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    const/4 v4, 0x0

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "405"

    move-object/from16 v3, p1

    invoke-static/range {v1 .. v6}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v13

    .line 3153
    .restart local v13    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    iget-object v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v2, v13}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3154
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 3157
    .end local v13    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ptr = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    move-object v2, p0

    move-object v3, v1

    move-object/from16 v4, p2

    move-object v5, v14

    move-object v6, v10

    move-object/from16 v8, p3

    .line 3158
    invoke-virtual/range {v2 .. v8}, Lcom/policydm/agent/XDMAgent;->xdmAgentCmdPropGet(Lcom/policydm/eng/core/XDMWorkspace;Lcom/policydm/eng/parser/XDMParserItem;Ljava/lang/String;[C[CLjava/lang/Object;)I

    move-result v12

    .end local v9    # "get":Lcom/policydm/eng/parser/XDMParserGet;
    :goto_1
    move v2, v12

    .line 3205
    goto/16 :goto_0

    .line 3160
    :cond_3
    const-string v2, "Replace"

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_7

    move-object/from16 v11, p3

    .line 3162
    check-cast v11, Lcom/policydm/eng/parser/XDMParserReplace;

    .line 3164
    .local v11, "replace":Lcom/policydm/eng/parser/XDMParserReplace;
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 3166
    const-string v2, "ptr is null"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 3167
    iget v2, v11, Lcom/policydm/eng/parser/XDMParserReplace;->cmdid:I

    const/4 v4, 0x0

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "405"

    move-object/from16 v3, p1

    invoke-static/range {v1 .. v6}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v13

    .line 3168
    .restart local v13    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    iget-object v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v2, v13}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3169
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 3172
    .end local v13    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    :cond_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ptr = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 3173
    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v2

    new-array v7, v2, [C

    .line 3174
    invoke-virtual {v14}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    const/16 v3, 0x3f

    invoke-static {v2, v3, v7}, Lcom/policydm/eng/core/XDMMem;->xdmLibStrsplit([CC[C)Ljava/lang/String;

    move-result-object v14

    .line 3175
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 3177
    const-string v2, "ptr is null"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 3178
    iget v2, v11, Lcom/policydm/eng/parser/XDMParserReplace;->cmdid:I

    const/4 v4, 0x0

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "405"

    move-object/from16 v3, p1

    invoke-static/range {v1 .. v6}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v13

    .line 3179
    .restart local v13    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    iget-object v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v2, v13}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3180
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 3182
    .end local v13    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    :cond_5
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ptr = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 3183
    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v2

    new-array v10, v2, [C

    .line 3184
    invoke-virtual {v14}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    const/16 v3, 0x3d

    invoke-static {v2, v3, v10}, Lcom/policydm/eng/core/XDMMem;->xdmLibStrsplit([CC[C)Ljava/lang/String;

    move-result-object v14

    .line 3185
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v10}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v14}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 3186
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 3188
    const-string v2, "ptr is null"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 3189
    iget v2, v11, Lcom/policydm/eng/parser/XDMParserReplace;->cmdid:I

    const/4 v4, 0x0

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "405"

    move-object/from16 v3, p1

    invoke-static/range {v1 .. v6}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v13

    .line 3190
    .restart local v13    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    iget-object v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v2, v13}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3191
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 3194
    .end local v13    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    :cond_6
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ptr = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    move-object v3, p0

    move-object v4, v1

    move-object/from16 v5, p2

    move-object v6, v14

    move-object/from16 v8, p3

    .line 3195
    invoke-virtual/range {v3 .. v8}, Lcom/policydm/agent/XDMAgent;->xdmAgentCmdPropReplace(Lcom/policydm/eng/core/XDMWorkspace;Lcom/policydm/eng/parser/XDMParserItem;Ljava/lang/String;[CLjava/lang/Object;)I

    move-result v12

    .line 3196
    goto/16 :goto_1

    .end local v11    # "replace":Lcom/policydm/eng/parser/XDMParserReplace;
    :cond_7
    move-object/from16 v9, p3

    .line 3199
    check-cast v9, Lcom/policydm/eng/parser/XDMParserGet;

    .line 3201
    .restart local v9    # "get":Lcom/policydm/eng/parser/XDMParserGet;
    iget v2, v9, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    const/4 v4, 0x0

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "405"

    move-object/from16 v3, p1

    invoke-static/range {v1 .. v6}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v13

    .line 3202
    .restart local v13    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    iget-object v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v2, v13}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3203
    const/4 v2, 0x0

    goto/16 :goto_0
.end method

.method public xdmAgentCmdPropGet(Lcom/policydm/eng/core/XDMWorkspace;Lcom/policydm/eng/parser/XDMParserItem;Ljava/lang/String;[C[CLjava/lang/Object;)I
    .locals 33
    .param p1, "ws"    # Lcom/policydm/eng/core/XDMWorkspace;
    .param p2, "item"    # Lcom/policydm/eng/parser/XDMParserItem;
    .param p3, "szData"    # Ljava/lang/String;
    .param p4, "prop"    # [C
    .param p5, "pNodeName"    # [C
    .param p6, "pkg"    # Ljava/lang/Object;

    .prologue
    .line 3210
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/policydm/eng/core/XDMWorkspace;->om:Lcom/policydm/eng/core/XDMOmTree;

    move-object/from16 v27, v0

    .local v27, "om":Lcom/policydm/eng/core/XDMOmTree;
    move-object/from16 v24, p6

    .line 3211
    check-cast v24, Lcom/policydm/eng/parser/XDMParserGet;

    .line 3215
    .local v24, "get":Lcom/policydm/eng/parser/XDMParserGet;
    const/16 v16, 0x0

    .line 3216
    .local v16, "szOutbuf":Ljava/lang/String;
    const/4 v11, 0x0

    .line 3217
    .local v11, "chreBuf":[C
    const/16 v25, 0x0

    .line 3218
    .local v25, "nFileId":I
    const/16 v29, 0x0

    .line 3221
    .local v29, "ret":Z
    invoke-static {}, Lcom/policydm/db/XDB;->xdbGetFileIdTNDS()I

    move-result v25

    .line 3223
    invoke-static/range {p5 .. p5}, Lcom/policydm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v31

    .line 3224
    .local v31, "szNodename":Ljava/lang/String;
    move-object/from16 v0, v27

    move-object/from16 v1, v31

    invoke-static {v0, v1}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v26

    .line 3226
    .local v26, "node":Lcom/policydm/eng/core/XDMVnode;
    if-nez v26, :cond_0

    .line 3228
    move-object/from16 v0, v24

    iget v6, v0, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    const-string v7, "Get"

    const/4 v8, 0x0

    move-object/from16 v0, p2

    iget-object v9, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v10, "404"

    move-object/from16 v5, p1

    invoke-static/range {v5 .. v10}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v30

    .line 3229
    .local v30, "status":Lcom/policydm/eng/parser/XDMParserStatus;
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v30

    invoke-static {v5, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3230
    const/4 v5, 0x0

    move-object/from16 v9, v16

    .line 3400
    .end local v16    # "szOutbuf":Ljava/lang/String;
    :goto_0
    return v5

    .line 3232
    .end local v30    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    .restart local v16    # "szOutbuf":Ljava/lang/String;
    :cond_0
    invoke-static/range {p4 .. p4}, Lcom/policydm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v32

    .line 3233
    .local v32, "szPropname":Ljava/lang/String;
    const-string v5, "list"

    move-object/from16 v0, v32

    invoke-virtual {v5, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_6

    .line 3236
    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/policydm/agent/XDMAgent;->xdmAgentIsAccessibleNode(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 3238
    move-object/from16 v0, v24

    iget v6, v0, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    const-string v7, "Get"

    const/4 v8, 0x0

    move-object/from16 v0, p2

    iget-object v9, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v10, "405"

    move-object/from16 v5, p1

    invoke-static/range {v5 .. v10}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v30

    .line 3239
    .restart local v30    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v30

    invoke-static {v5, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3240
    const/4 v5, 0x0

    move-object/from16 v9, v16

    .local v9, "szOutbuf":Ljava/lang/Object;
    goto :goto_0

    .line 3243
    .end local v9    # "szOutbuf":Ljava/lang/Object;
    .end local v30    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    :cond_1
    const/16 v5, 0x8

    move-object/from16 v0, v27

    move-object/from16 v1, v26

    invoke-static {v0, v1, v5}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmCheckAcl(Lcom/policydm/eng/core/XDMOmTree;Lcom/policydm/eng/core/XDMVnode;I)Z

    move-result v5

    if-nez v5, :cond_2

    .line 3245
    move-object/from16 v0, v24

    iget v6, v0, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    const-string v7, "Get"

    const/4 v8, 0x0

    move-object/from16 v0, p2

    iget-object v9, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v10, "425"

    move-object/from16 v5, p1

    invoke-static/range {v5 .. v10}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v30

    .line 3246
    .restart local v30    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v30

    invoke-static {v5, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3247
    const/4 v5, 0x0

    move-object/from16 v9, v16

    .restart local v9    # "szOutbuf":Ljava/lang/Object;
    goto :goto_0

    .line 3250
    .end local v9    # "szOutbuf":Ljava/lang/Object;
    .end local v30    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    :cond_2
    const-string v5, "Struct"

    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_4

    .line 3252
    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2, v5}, Lcom/policydm/agent/XDMAgent;->xdmAgentCmdPropGetStruct(Lcom/policydm/eng/parser/XDMParserGet;Lcom/policydm/eng/core/XDMVnode;Z)V

    .line 3273
    :cond_3
    :goto_1
    move-object/from16 v0, v24

    iget v6, v0, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    const-string v7, "Get"

    const/4 v8, 0x0

    move-object/from16 v0, p2

    iget-object v9, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v10, "200"

    move-object/from16 v5, p1

    invoke-static/range {v5 .. v10}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v30

    .line 3274
    .restart local v30    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v30

    invoke-static {v5, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3275
    const/4 v5, 0x0

    move-object/from16 v9, v16

    .restart local v9    # "szOutbuf":Ljava/lang/Object;
    goto/16 :goto_0

    .line 3254
    .end local v9    # "szOutbuf":Ljava/lang/Object;
    .end local v30    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    :cond_4
    const-string v5, "StructData"

    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_5

    .line 3256
    const/4 v5, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2, v5}, Lcom/policydm/agent/XDMAgent;->xdmAgentCmdPropGetStruct(Lcom/policydm/eng/parser/XDMParserGet;Lcom/policydm/eng/core/XDMVnode;Z)V

    goto :goto_1

    .line 3258
    :cond_5
    const-string v5, "TNDS"

    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 3262
    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move-object/from16 v2, v27

    move-object/from16 v3, v26

    move-object/from16 v4, p3

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/policydm/agent/XDMAgent;->xdmAgentCmdPropGetTnds(Lcom/policydm/eng/parser/XDMParserGet;Lcom/policydm/eng/core/XDMOmTree;Lcom/policydm/eng/core/XDMVnode;Ljava/lang/String;)Z

    move-result v29

    .line 3263
    if-nez v29, :cond_3

    .line 3265
    invoke-static/range {v25 .. v25}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpDeleteFile(I)I

    .line 3266
    sget-object v5, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    move-object/from16 v0, v24

    iget v6, v0, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    const-string v7, "Get"

    const/4 v8, 0x0

    move-object/from16 v0, p2

    iget-object v9, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v10, "404"

    invoke-static/range {v5 .. v10}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v30

    .line 3267
    .restart local v30    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    sget-object v5, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    iget-object v5, v5, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v30

    invoke-static {v5, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3268
    const/4 v5, 0x0

    move-object/from16 v9, v16

    .restart local v9    # "szOutbuf":Ljava/lang/Object;
    goto/16 :goto_0

    .line 3279
    .end local v9    # "szOutbuf":Ljava/lang/Object;
    .end local v30    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    :cond_6
    const/16 v5, 0x8

    move-object/from16 v0, v27

    move-object/from16 v1, v26

    invoke-static {v0, v1, v5}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmCheckAcl(Lcom/policydm/eng/core/XDMOmTree;Lcom/policydm/eng/core/XDMVnode;I)Z

    move-result v5

    if-nez v5, :cond_7

    .line 3281
    move-object/from16 v0, v24

    iget v6, v0, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    const-string v7, "Get"

    const/4 v8, 0x0

    move-object/from16 v0, p2

    iget-object v9, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v10, "425"

    move-object/from16 v5, p1

    invoke-static/range {v5 .. v10}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v30

    .line 3282
    .restart local v30    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v30

    invoke-static {v5, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3283
    const/4 v5, 0x0

    move-object/from16 v9, v16

    .restart local v9    # "szOutbuf":Ljava/lang/Object;
    goto/16 :goto_0

    .line 3286
    .end local v9    # "szOutbuf":Ljava/lang/Object;
    .end local v30    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    :cond_7
    const-string v5, "ACL"

    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_9

    .line 3288
    move-object/from16 v0, v24

    iget v6, v0, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    const-string v7, "Get"

    const/4 v8, 0x0

    move-object/from16 v0, p2

    iget-object v9, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v10, "200"

    move-object/from16 v5, p1

    invoke-static/range {v5 .. v10}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v30

    .line 3289
    .restart local v30    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v30

    invoke-static {v5, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3291
    move-object/from16 v0, v26

    iget-object v5, v0, Lcom/policydm/eng/core/XDMVnode;->acl:Lcom/policydm/eng/core/XDMOmList;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v5, v1}, Lcom/policydm/agent/XDMAgent;->xdmAgentGetAclStr(Lcom/policydm/eng/core/XDMOmList;Lcom/policydm/eng/parser/XDMParserItem;)Ljava/lang/String;

    move-result-object v9

    .line 3292
    .end local v16    # "szOutbuf":Ljava/lang/String;
    .local v9, "szOutbuf":Ljava/lang/String;
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_8

    .line 3293
    invoke-virtual {v9}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    .line 3296
    :cond_8
    move-object/from16 v0, v24

    iget v6, v0, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    move-object/from16 v0, p2

    iget-object v7, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "chr"

    const-string v9, "text/plain"

    .end local v9    # "szOutbuf":Ljava/lang/String;
    const/4 v10, 0x0

    move-object/from16 v5, p1

    invoke-static/range {v5 .. v11}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdDetailResults(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I[C)Lcom/policydm/eng/parser/XDMParserResults;

    move-result-object v28

    .line 3297
    .local v28, "results":Lcom/policydm/eng/parser/XDMParserResults;
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/policydm/eng/core/XDMWorkspace;->resultsList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v28

    invoke-static {v5, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3299
    const/4 v9, 0x0

    .line 3300
    .restart local v9    # "szOutbuf":Ljava/lang/String;
    const/4 v5, 0x0

    goto/16 :goto_0

    .line 3302
    .end local v9    # "szOutbuf":Ljava/lang/String;
    .end local v28    # "results":Lcom/policydm/eng/parser/XDMParserResults;
    .end local v30    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    .restart local v16    # "szOutbuf":Ljava/lang/String;
    :cond_9
    const-string v5, "Format"

    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_b

    .line 3304
    move-object/from16 v0, v26

    iget v5, v0, Lcom/policydm/eng/core/XDMVnode;->format:I

    invoke-static {v5}, Lcom/policydm/eng/core/XDMOmList;->xdmOmGetFormatString(I)Ljava/lang/String;

    move-result-object v9

    .line 3305
    .end local v16    # "szOutbuf":Ljava/lang/String;
    .restart local v9    # "szOutbuf":Ljava/lang/String;
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_a

    .line 3306
    invoke-virtual {v9}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    .line 3308
    :cond_a
    move-object/from16 v0, v24

    iget v6, v0, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    const-string v7, "Get"

    const/4 v8, 0x0

    move-object/from16 v0, p2

    iget-object v9, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    .end local v9    # "szOutbuf":Ljava/lang/String;
    const-string v10, "200"

    move-object/from16 v5, p1

    invoke-static/range {v5 .. v10}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v30

    .line 3309
    .restart local v30    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v30

    invoke-static {v5, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3312
    move-object/from16 v0, v24

    iget v6, v0, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    move-object/from16 v0, p2

    iget-object v7, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "chr"

    const-string v9, "text/plain"

    const/4 v10, 0x0

    move-object/from16 v5, p1

    invoke-static/range {v5 .. v11}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdDetailResults(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I[C)Lcom/policydm/eng/parser/XDMParserResults;

    move-result-object v28

    .line 3313
    .restart local v28    # "results":Lcom/policydm/eng/parser/XDMParserResults;
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/policydm/eng/core/XDMWorkspace;->resultsList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v28

    invoke-static {v5, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3315
    const/4 v9, 0x0

    .line 3400
    .end local v28    # "results":Lcom/policydm/eng/parser/XDMParserResults;
    :goto_2
    const/4 v5, 0x0

    goto/16 :goto_0

    .line 3317
    .end local v30    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    .restart local v16    # "szOutbuf":Ljava/lang/String;
    :cond_b
    const-string v5, "Type"

    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_e

    .line 3319
    move-object/from16 v0, v24

    iget v6, v0, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    const-string v7, "Get"

    const/4 v8, 0x0

    move-object/from16 v0, p2

    iget-object v9, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v10, "200"

    move-object/from16 v5, p1

    invoke-static/range {v5 .. v10}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v30

    .line 3320
    .restart local v30    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v30

    invoke-static {v5, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3322
    move-object/from16 v0, v26

    iget-object v5, v0, Lcom/policydm/eng/core/XDMVnode;->type:Lcom/policydm/eng/core/XDMOmList;

    if-eqz v5, :cond_d

    move-object/from16 v0, v26

    iget-object v5, v0, Lcom/policydm/eng/core/XDMVnode;->type:Lcom/policydm/eng/core/XDMOmList;

    iget-object v5, v5, Lcom/policydm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    if-eqz v5, :cond_d

    .line 3324
    move-object/from16 v0, v26

    iget-object v5, v0, Lcom/policydm/eng/core/XDMVnode;->type:Lcom/policydm/eng/core/XDMOmList;

    iget-object v5, v5, Lcom/policydm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 3325
    .end local v16    # "szOutbuf":Ljava/lang/String;
    .restart local v9    # "szOutbuf":Ljava/lang/String;
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_c

    .line 3326
    invoke-virtual {v9}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    .line 3328
    :cond_c
    move-object/from16 v0, v24

    iget v6, v0, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    move-object/from16 v0, p2

    iget-object v7, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v8, "chr"

    const/4 v10, 0x0

    move-object/from16 v5, p1

    invoke-static/range {v5 .. v11}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdDetailResults(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I[C)Lcom/policydm/eng/parser/XDMParserResults;

    move-result-object v28

    .line 3329
    .restart local v28    # "results":Lcom/policydm/eng/parser/XDMParserResults;
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/policydm/eng/core/XDMWorkspace;->resultsList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v28

    invoke-static {v5, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3331
    const/4 v9, 0x0

    goto :goto_2

    .line 3335
    .end local v9    # "szOutbuf":Ljava/lang/String;
    .end local v28    # "results":Lcom/policydm/eng/parser/XDMParserResults;
    .restart local v16    # "szOutbuf":Ljava/lang/String;
    :cond_d
    move-object/from16 v0, v24

    iget v13, v0, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    move-object/from16 v0, p2

    iget-object v14, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v15, ""

    const/16 v17, 0x0

    move-object/from16 v12, p1

    move-object/from16 v18, v11

    invoke-static/range {v12 .. v18}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdDetailResults(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I[C)Lcom/policydm/eng/parser/XDMParserResults;

    move-result-object v28

    .line 3336
    .restart local v28    # "results":Lcom/policydm/eng/parser/XDMParserResults;
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/policydm/eng/core/XDMWorkspace;->resultsList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v28

    invoke-static {v5, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    move-object/from16 v9, v16

    .local v9, "szOutbuf":Ljava/lang/Object;
    goto/16 :goto_2

    .line 3340
    .end local v9    # "szOutbuf":Ljava/lang/Object;
    .end local v28    # "results":Lcom/policydm/eng/parser/XDMParserResults;
    .end local v30    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    :cond_e
    const-string v5, "Size"

    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_11

    .line 3342
    move-object/from16 v0, v26

    iget v5, v0, Lcom/policydm/eng/core/XDMVnode;->vaddr:I

    if-ltz v5, :cond_10

    move-object/from16 v0, v26

    iget v5, v0, Lcom/policydm/eng/core/XDMVnode;->size:I

    if-lez v5, :cond_10

    .line 3344
    move-object/from16 v0, v24

    iget v0, v0, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    move/from16 v18, v0

    const-string v19, "Get"

    const/16 v20, 0x0

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    move-object/from16 v21, v0

    const-string v22, "200"

    move-object/from16 v17, p1

    invoke-static/range {v17 .. v22}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v30

    .line 3345
    .restart local v30    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v30

    invoke-static {v5, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3347
    move-object/from16 v0, v26

    iget v5, v0, Lcom/policydm/eng/core/XDMVnode;->size:I

    add-int/lit8 v5, v5, -0x1

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    .line 3348
    .end local v16    # "szOutbuf":Ljava/lang/String;
    .local v9, "szOutbuf":Ljava/lang/String;
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_f

    .line 3349
    invoke-virtual {v9}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    .line 3352
    :cond_f
    move-object/from16 v0, v24

    iget v0, v0, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    move-object/from16 v19, v0

    const-string v20, "chr"

    const-string v21, "text/plain"

    const/16 v22, 0x0

    move-object/from16 v17, p1

    move-object/from16 v23, v11

    invoke-static/range {v17 .. v23}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdDetailResults(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I[C)Lcom/policydm/eng/parser/XDMParserResults;

    move-result-object v28

    .line 3353
    .restart local v28    # "results":Lcom/policydm/eng/parser/XDMParserResults;
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/policydm/eng/core/XDMWorkspace;->resultsList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v28

    invoke-static {v5, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3355
    const/4 v9, 0x0

    goto/16 :goto_2

    .line 3359
    .end local v9    # "szOutbuf":Ljava/lang/String;
    .end local v28    # "results":Lcom/policydm/eng/parser/XDMParserResults;
    .end local v30    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    .restart local v16    # "szOutbuf":Ljava/lang/String;
    :cond_10
    move-object/from16 v0, v24

    iget v0, v0, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    move/from16 v18, v0

    const-string v19, "Get"

    const/16 v20, 0x0

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    move-object/from16 v21, v0

    const-string v22, "406"

    move-object/from16 v17, p1

    invoke-static/range {v17 .. v22}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v30

    .line 3360
    .restart local v30    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v30

    invoke-static {v5, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    move-object/from16 v9, v16

    .local v9, "szOutbuf":Ljava/lang/Object;
    goto/16 :goto_2

    .line 3363
    .end local v9    # "szOutbuf":Ljava/lang/Object;
    .end local v30    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    :cond_11
    const-string v5, "Name"

    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_13

    .line 3365
    move-object/from16 v0, v24

    iget v0, v0, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    move/from16 v18, v0

    const-string v19, "Get"

    const/16 v20, 0x0

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    move-object/from16 v21, v0

    const-string v22, "200"

    move-object/from16 v17, p1

    invoke-static/range {v17 .. v22}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v30

    .line 3366
    .restart local v30    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v30

    invoke-static {v5, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3368
    move-object/from16 v0, v26

    iget-object v9, v0, Lcom/policydm/eng/core/XDMVnode;->m_szName:Ljava/lang/String;

    .line 3369
    .end local v16    # "szOutbuf":Ljava/lang/String;
    .local v9, "szOutbuf":Ljava/lang/String;
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_12

    .line 3370
    invoke-virtual {v9}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    .line 3373
    :cond_12
    move-object/from16 v0, v24

    iget v0, v0, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    move-object/from16 v19, v0

    const-string v20, "chr"

    const-string v21, "text/plain"

    const/16 v22, 0x0

    move-object/from16 v17, p1

    move-object/from16 v23, v11

    invoke-static/range {v17 .. v23}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdDetailResults(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I[C)Lcom/policydm/eng/parser/XDMParserResults;

    move-result-object v28

    .line 3374
    .restart local v28    # "results":Lcom/policydm/eng/parser/XDMParserResults;
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/policydm/eng/core/XDMWorkspace;->resultsList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v28

    invoke-static {v5, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3376
    const/4 v9, 0x0

    goto/16 :goto_2

    .line 3378
    .end local v9    # "szOutbuf":Ljava/lang/String;
    .end local v28    # "results":Lcom/policydm/eng/parser/XDMParserResults;
    .end local v30    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    .restart local v16    # "szOutbuf":Ljava/lang/String;
    :cond_13
    const-string v5, "Title"

    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_15

    .line 3380
    move-object/from16 v0, v24

    iget v0, v0, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    move/from16 v18, v0

    const-string v19, "Get"

    const/16 v20, 0x0

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    move-object/from16 v21, v0

    const-string v22, "200"

    move-object/from16 v17, p1

    invoke-static/range {v17 .. v22}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v30

    .line 3381
    .restart local v30    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v30

    invoke-static {v5, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3383
    move-object/from16 v0, v26

    iget-object v9, v0, Lcom/policydm/eng/core/XDMVnode;->title:Ljava/lang/String;

    .line 3384
    .end local v16    # "szOutbuf":Ljava/lang/String;
    .restart local v9    # "szOutbuf":Ljava/lang/String;
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_14

    .line 3385
    invoke-virtual {v9}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    .line 3388
    :cond_14
    move-object/from16 v0, v24

    iget v0, v0, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    move-object/from16 v19, v0

    const-string v20, "chr"

    const-string v21, "text/plain"

    const/16 v22, 0x0

    move-object/from16 v17, p1

    move-object/from16 v23, v11

    invoke-static/range {v17 .. v23}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdDetailResults(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I[C)Lcom/policydm/eng/parser/XDMParserResults;

    move-result-object v28

    .line 3389
    .restart local v28    # "results":Lcom/policydm/eng/parser/XDMParserResults;
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/policydm/eng/core/XDMWorkspace;->resultsList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v28

    invoke-static {v5, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3391
    const/4 v9, 0x0

    goto/16 :goto_2

    .line 3395
    .end local v9    # "szOutbuf":Ljava/lang/String;
    .end local v28    # "results":Lcom/policydm/eng/parser/XDMParserResults;
    .end local v30    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    .restart local v16    # "szOutbuf":Ljava/lang/String;
    :cond_15
    move-object/from16 v0, v24

    iget v0, v0, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    move/from16 v18, v0

    const-string v19, "Get"

    const/16 v20, 0x0

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    move-object/from16 v21, v0

    const-string v22, "405"

    move-object/from16 v17, p1

    invoke-static/range {v17 .. v22}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v30

    .line 3396
    .restart local v30    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v30

    invoke-static {v5, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3397
    const/4 v5, 0x0

    move-object/from16 v9, v16

    .local v9, "szOutbuf":Ljava/lang/Object;
    goto/16 :goto_0
.end method

.method public xdmAgentCmdPropGetStruct(Lcom/policydm/eng/parser/XDMParserGet;Lcom/policydm/eng/core/XDMVnode;Z)V
    .locals 11
    .param p1, "get"    # Lcom/policydm/eng/parser/XDMParserGet;
    .param p2, "node"    # Lcom/policydm/eng/core/XDMVnode;
    .param p3, "makedata"    # Z

    .prologue
    .line 3411
    sget-object v0, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    .line 3412
    .local v0, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    iget-object v9, v0, Lcom/policydm/eng/core/XDMWorkspace;->om:Lcom/policydm/eng/core/XDMOmTree;

    .line 3414
    .local v9, "om":Lcom/policydm/eng/core/XDMOmTree;
    const-string v2, ""

    .line 3415
    .local v2, "szName":Ljava/lang/String;
    const-string v3, ""

    .line 3416
    .local v3, "szFormat":Ljava/lang/String;
    const-string v4, ""

    .line 3417
    .local v4, "szType":Ljava/lang/String;
    const/4 v6, 0x0

    .line 3418
    .local v6, "data":[C
    const/4 v8, 0x0

    .line 3420
    .local v8, "cur":Lcom/policydm/eng/core/XDMVnode;
    const/4 v5, 0x0

    .line 3422
    .local v5, "datasize":I
    if-nez p2, :cond_1

    .line 3475
    :cond_0
    return-void

    .line 3427
    :cond_1
    iget-object v8, p2, Lcom/policydm/eng/core/XDMVnode;->childlist:Lcom/policydm/eng/core/XDMVnode;

    .line 3429
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->om:Lcom/policydm/eng/core/XDMOmTree;

    invoke-virtual {p0, v1, p2}, Lcom/policydm/agent/XDMAgent;->xdmAgentGetPathFromNode(Lcom/policydm/eng/core/XDMOmTree;Lcom/policydm/eng/core/XDMVnode;)Ljava/lang/String;

    move-result-object v2

    .line 3431
    if-eqz p3, :cond_5

    .line 3433
    iget v1, p2, Lcom/policydm/eng/core/XDMVnode;->vaddr:I

    if-ltz v1, :cond_4

    iget v1, p2, Lcom/policydm/eng/core/XDMVnode;->size:I

    if-lez v1, :cond_4

    .line 3435
    iget v1, p2, Lcom/policydm/eng/core/XDMVnode;->format:I

    if-lez v1, :cond_3

    .line 3437
    iget v1, p2, Lcom/policydm/eng/core/XDMVnode;->format:I

    invoke-static {v1}, Lcom/policydm/eng/core/XDMOmList;->xdmOmGetFormatString(I)Ljava/lang/String;

    move-result-object v3

    .line 3444
    :goto_0
    iget v7, p2, Lcom/policydm/eng/core/XDMVnode;->size:I

    .line 3445
    .local v7, "bufsize":I
    new-array v6, v7, [C

    .line 3447
    const/4 v1, 0x0

    invoke-static {v9, v2, v1, v6, v7}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmRead(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 3448
    iget v5, p2, Lcom/policydm/eng/core/XDMVnode;->size:I

    .line 3461
    .end local v7    # "bufsize":I
    :goto_1
    iget-object v1, p2, Lcom/policydm/eng/core/XDMVnode;->m_szName:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/policydm/agent/XDMAgent;->xdmAgentIsAccessibleNode(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 3463
    iget v1, p1, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    invoke-static/range {v0 .. v6}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdDetailResults(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I[C)Lcom/policydm/eng/parser/XDMParserResults;

    move-result-object v10

    .line 3464
    .local v10, "results":Lcom/policydm/eng/parser/XDMParserResults;
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->resultsList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v1, v10}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3466
    .end local v10    # "results":Lcom/policydm/eng/parser/XDMParserResults;
    :cond_2
    const/4 v2, 0x0

    .line 3467
    const/4 v3, 0x0

    .line 3468
    const/4 v4, 0x0

    .line 3470
    :goto_2
    if-eqz v8, :cond_0

    .line 3472
    invoke-virtual {p0, p1, v8, p3}, Lcom/policydm/agent/XDMAgent;->xdmAgentCmdPropGetStruct(Lcom/policydm/eng/parser/XDMParserGet;Lcom/policydm/eng/core/XDMVnode;Z)V

    .line 3473
    iget-object v8, v8, Lcom/policydm/eng/core/XDMVnode;->next:Lcom/policydm/eng/core/XDMVnode;

    goto :goto_2

    .line 3441
    :cond_3
    const-string v3, ""

    goto :goto_0

    .line 3453
    :cond_4
    iget v1, p2, Lcom/policydm/eng/core/XDMVnode;->format:I

    invoke-static {v1}, Lcom/policydm/eng/core/XDMOmList;->xdmOmGetFormatString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 3458
    :cond_5
    iget v1, p2, Lcom/policydm/eng/core/XDMVnode;->format:I

    invoke-static {v1}, Lcom/policydm/eng/core/XDMOmList;->xdmOmGetFormatString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method

.method public xdmAgentCmdPropGetTnds(Lcom/policydm/eng/parser/XDMParserGet;Lcom/policydm/eng/core/XDMOmTree;Lcom/policydm/eng/core/XDMVnode;Ljava/lang/String;)Z
    .locals 23
    .param p1, "get"    # Lcom/policydm/eng/parser/XDMParserGet;
    .param p2, "om"    # Lcom/policydm/eng/core/XDMOmTree;
    .param p3, "node"    # Lcom/policydm/eng/core/XDMVnode;
    .param p4, "szPropertylist"    # Ljava/lang/String;

    .prologue
    .line 3842
    const-string v19, ""

    .line 3843
    .local v19, "szTemp":Ljava/lang/String;
    const-string v18, ""

    .line 3844
    .local v18, "szTag":Ljava/lang/String;
    const-string v5, ""

    .line 3845
    .local v5, "szName":Ljava/lang/String;
    const-string v6, ""

    .line 3846
    .local v6, "szFormat":Ljava/lang/String;
    const-string v17, ""

    .line 3847
    .local v17, "szData":Ljava/lang/String;
    const/4 v14, 0x0

    .line 3848
    .local v14, "nFlag":I
    const/4 v8, 0x0

    .line 3849
    .local v8, "nSize":I
    const-string v21, ""

    .line 3850
    .local v21, "szToken":Ljava/lang/String;
    const/4 v15, 0x0

    .line 3851
    .local v15, "ptr":[Ljava/lang/String;
    const/16 v16, 0x0

    .line 3852
    .local v16, "results":Lcom/policydm/eng/parser/XDMParserResults;
    sget-object v3, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    .line 3853
    .local v3, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    const/16 v22, 0x0

    .line 3856
    .local v22, "tempPath":[C
    const/4 v13, 0x0

    .line 3858
    .local v13, "nFileId":I
    const-string v4, ""

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 3859
    if-nez p3, :cond_0

    .line 3861
    const/4 v4, 0x0

    .line 3980
    :goto_0
    return v4

    .line 3864
    :cond_0
    move-object/from16 v0, p3

    iget-object v4, v0, Lcom/policydm/eng/core/XDMVnode;->childlist:Lcom/policydm/eng/core/XDMVnode;

    if-nez v4, :cond_1

    .line 3866
    const/4 v4, 0x0

    goto :goto_0

    .line 3869
    :cond_1
    invoke-static {}, Lcom/policydm/db/XDB;->xdbGetFileIdTNDS()I

    move-result v13

    .line 3871
    const-string v4, "\\+"

    move-object/from16 v0, p4

    invoke-virtual {v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v15

    .line 3873
    if-nez v15, :cond_2

    .line 3875
    const/4 v4, 0x0

    goto :goto_0

    .line 3878
    :cond_2
    const/4 v4, 0x0

    aget-object v21, v15, v4

    .line 3879
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "token : "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 3880
    array-length v4, v15

    const/4 v7, 0x1

    if-le v4, v7, :cond_7

    .line 3882
    const/4 v11, 0x1

    .line 3884
    .local v11, "i":I
    :goto_1
    array-length v4, v15

    if-ge v11, v4, :cond_8

    .line 3886
    const-string v4, "ACL"

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_4

    .line 3888
    or-int/lit8 v14, v14, 0x1

    .line 3903
    :cond_3
    :goto_2
    add-int/lit8 v12, v11, 0x1

    .end local v11    # "i":I
    .local v12, "i":I
    aget-object v21, v15, v11

    move v11, v12

    .end local v12    # "i":I
    .restart local v11    # "i":I
    goto :goto_1

    .line 3890
    :cond_4
    const-string v4, "Format"

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_5

    .line 3892
    or-int/lit8 v14, v14, 0x2

    goto :goto_2

    .line 3894
    :cond_5
    const-string v4, "Type"

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_6

    .line 3896
    or-int/lit8 v14, v14, 0x4

    goto :goto_2

    .line 3898
    :cond_6
    const-string v4, "Value"

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_3

    .line 3900
    or-int/lit8 v14, v14, 0x8

    goto :goto_2

    .line 3908
    .end local v11    # "i":I
    :cond_7
    const/16 v14, 0xf

    .line 3909
    const-string v4, "-"

    move-object/from16 v0, p4

    invoke-virtual {v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v15

    .line 3910
    if-eqz v15, :cond_8

    .line 3912
    const/4 v4, 0x0

    aget-object v21, v15, v4

    .line 3913
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "token : "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 3915
    const-string v4, "ACL"

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_9

    .line 3917
    and-int/lit16 v14, v14, 0xfe

    .line 3934
    :cond_8
    :goto_3
    invoke-static {v13}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpDeleteFile(I)I

    .line 3936
    sget-object v4, Lcom/policydm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    const/16 v7, 0x22

    aget-object v18, v4, v7

    .line 3937
    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 3939
    sget-object v4, Lcom/policydm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    const/4 v7, 0x0

    aget-object v18, v4, v7

    .line 3940
    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 3942
    sget-object v4, Lcom/policydm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    const/4 v7, 0x2

    aget-object v18, v4, v7

    .line 3943
    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 3944
    const-string v4, "1.2"

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 3946
    sget-object v4, Lcom/policydm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    const/4 v7, 0x3

    aget-object v18, v4, v7

    .line 3947
    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 3948
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v4

    invoke-static {v13, v4}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpAppendFile(I[B)I

    .line 3950
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lcom/policydm/agent/XDMAgent;->xdmAgentGetPathFromNode(Lcom/policydm/eng/core/XDMOmTree;Lcom/policydm/eng/core/XDMVnode;)Ljava/lang/String;

    move-result-object v20

    .line 3951
    .local v20, "szTempPath":Ljava/lang/String;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v4

    new-array v0, v4, [C

    move-object/from16 v22, v0

    .line 3952
    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmMakeParentPath(Ljava/lang/String;[C)V

    .line 3953
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "tempPath : "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static/range {v22 .. v22}, Lcom/policydm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 3954
    invoke-static/range {v22 .. v22}, Lcom/policydm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2, v14, v4}, Lcom/policydm/agent/XDMAgent;->xdmAgentMakeTndsSubTree(Lcom/policydm/eng/core/XDMOmTree;Lcom/policydm/eng/core/XDMVnode;ILjava/lang/String;)V

    .line 3956
    sget-object v4, Lcom/policydm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    const/4 v7, 0x1

    aget-object v18, v4, v7

    .line 3957
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v4

    invoke-static {v13, v4}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpAppendFile(I[B)I

    .line 3959
    sget-object v4, Lcom/policydm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    const/16 v7, 0x23

    aget-object v18, v4, v7

    .line 3960
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v4

    invoke-static {v13, v4}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpAppendFile(I[B)I

    .line 3962
    invoke-static {v13}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpGetFileSize(I)I

    move-result v8

    .line 3963
    const/4 v4, 0x0

    invoke-static {v13, v4, v8}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpReadFile(III)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [B

    move-object v10, v4

    check-cast v10, [B

    .line 3964
    .local v10, "bTemp":[B
    new-instance v17, Ljava/lang/String;

    .end local v17    # "szData":Ljava/lang/String;
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-direct {v0, v10, v4}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 3965
    .restart local v17    # "szData":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lcom/policydm/agent/XDMAgent;->xdmAgentGetPathFromNode(Lcom/policydm/eng/core/XDMOmTree;Lcom/policydm/eng/core/XDMVnode;)Ljava/lang/String;

    move-result-object v5

    .line 3966
    const/16 v4, 0x8

    invoke-static {v4}, Lcom/policydm/eng/core/XDMOmList;->xdmOmGetFormatString(I)Ljava/lang/String;

    move-result-object v6

    .line 3968
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "name : "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 3969
    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 3971
    const-string v4, "_____ TNDSResults File Read Error!"

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 3972
    move-object/from16 v0, p1

    iget v4, v0, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    const-string v7, "application/vnd.syncml.dmtnds+xml"

    const/4 v9, 0x0

    invoke-static/range {v3 .. v9}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdDetailResults(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I[C)Lcom/policydm/eng/parser/XDMParserResults;

    move-result-object v16

    .line 3978
    :goto_4
    iget-object v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->resultsList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v16

    invoke-static {v4, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3980
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 3919
    .end local v10    # "bTemp":[B
    .end local v20    # "szTempPath":Ljava/lang/String;
    :cond_9
    const-string v4, "Format"

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_a

    .line 3921
    and-int/lit16 v14, v14, 0xfd

    goto/16 :goto_3

    .line 3923
    :cond_a
    const-string v4, "Type"

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_b

    .line 3925
    and-int/lit16 v14, v14, 0xfb

    goto/16 :goto_3

    .line 3927
    :cond_b
    const-string v4, "Value"

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_8

    .line 3929
    and-int/lit16 v14, v14, 0xf7

    goto/16 :goto_3

    .line 3976
    .restart local v10    # "bTemp":[B
    .restart local v20    # "szTempPath":Ljava/lang/String;
    :cond_c
    move-object/from16 v0, p1

    iget v4, v0, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    const-string v7, "application/vnd.syncml.dmtnds+xml"

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->toCharArray()[C

    move-result-object v9

    invoke-static/range {v3 .. v9}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdDetailResults(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I[C)Lcom/policydm/eng/parser/XDMParserResults;

    move-result-object v16

    goto :goto_4
.end method

.method public xdmAgentCmdPropReplace(Lcom/policydm/eng/core/XDMWorkspace;Lcom/policydm/eng/parser/XDMParserItem;Ljava/lang/String;[CLjava/lang/Object;)I
    .locals 14
    .param p1, "ws"    # Lcom/policydm/eng/core/XDMWorkspace;
    .param p2, "item"    # Lcom/policydm/eng/parser/XDMParserItem;
    .param p3, "szData"    # Ljava/lang/String;
    .param p4, "pNodeName"    # [C
    .param p5, "pkg"    # Ljava/lang/Object;

    .prologue
    .line 3523
    iget-object v9, p1, Lcom/policydm/eng/core/XDMWorkspace;->om:Lcom/policydm/eng/core/XDMOmTree;

    .local v9, "om":Lcom/policydm/eng/core/XDMOmTree;
    move-object/from16 v10, p5

    .line 3524
    check-cast v10, Lcom/policydm/eng/parser/XDMParserReplace;

    .line 3527
    .local v10, "replace":Lcom/policydm/eng/parser/XDMParserReplace;
    const/4 v13, 0x0

    .line 3528
    .local v13, "szOutbuf":Ljava/lang/String;
    const/4 v7, 0x0

    .line 3530
    .local v7, "acllist":Lcom/policydm/eng/core/XDMOmList;
    invoke-static/range {p4 .. p4}, Lcom/policydm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v12

    .line 3531
    .local v12, "szNodeName":Ljava/lang/String;
    invoke-static {v9, v12}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v8

    .line 3532
    .local v8, "node":Lcom/policydm/eng/core/XDMVnode;
    if-nez v8, :cond_0

    .line 3534
    const-string v1, "!node"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 3536
    iget v2, v10, Lcom/policydm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v3, "Replace"

    const/4 v4, 0x0

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "404"

    move-object v1, p1

    invoke-static/range {v1 .. v6}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v11

    .line 3537
    .local v11, "status":Lcom/policydm/eng/parser/XDMParserStatus;
    iget-object v1, p1, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v1, v11}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3538
    const/4 v1, 0x0

    .line 3627
    :goto_0
    return v1

    .line 3541
    .end local v11    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    :cond_0
    invoke-virtual {p0, v9, v12}, Lcom/policydm/agent/XDMAgent;->xdmAgentIsPermanentNode(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3543
    iget v2, v10, Lcom/policydm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v3, "Replace"

    const/4 v4, 0x0

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "405"

    move-object v1, p1

    invoke-static/range {v1 .. v6}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v11

    .line 3544
    .restart local v11    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    iget-object v1, p1, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v1, v11}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3545
    const/4 v1, 0x0

    goto :goto_0

    .line 3551
    .end local v11    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    :cond_1
    const/16 v1, 0x10

    invoke-static {v9, v8, v1}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmCheckAcl(Lcom/policydm/eng/core/XDMOmTree;Lcom/policydm/eng/core/XDMVnode;I)Z

    move-result v1

    if-nez v1, :cond_2

    .line 3553
    const-string v1, "!XDM_OMACL_REPLACE"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 3555
    iget v2, v10, Lcom/policydm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v3, "Replace"

    const/4 v4, 0x0

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "425"

    move-object v1, p1

    invoke-static/range {v1 .. v6}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v11

    .line 3556
    .restart local v11    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    iget-object v1, p1, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v1, v11}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3557
    const/4 v1, 0x0

    goto :goto_0

    .line 3560
    .end local v11    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    :cond_2
    const-string v1, "ACL"

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_6

    .line 3562
    const-string v1, "ACL"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 3565
    iget v1, v8, Lcom/policydm/eng/core/XDMVnode;->format:I

    const/4 v2, 0x6

    if-eq v1, v2, :cond_3

    .line 3567
    iget-object v1, v8, Lcom/policydm/eng/core/XDMVnode;->ptParentNode:Lcom/policydm/eng/core/XDMVnode;

    const/16 v2, 0x10

    invoke-static {v9, v1, v2}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmCheckAcl(Lcom/policydm/eng/core/XDMOmTree;Lcom/policydm/eng/core/XDMVnode;I)Z

    move-result v1

    if-nez v1, :cond_3

    .line 3569
    iget v2, v10, Lcom/policydm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v3, "Replace"

    const/4 v4, 0x0

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "425"

    move-object v1, p1

    invoke-static/range {v1 .. v6}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v11

    .line 3570
    .restart local v11    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    iget-object v1, p1, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v1, v11}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3571
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "STATUS_COMMAND_NOT_ALLOWED="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static/range {p4 .. p4}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 3572
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 3575
    .end local v11    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    :cond_3
    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    invoke-static {v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStGetString(Lcom/policydm/eng/parser/XDMParserPcdata;)Ljava/lang/String;

    move-result-object v13

    .line 3576
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 3578
    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    iget-object v1, v1, Lcom/policydm/eng/parser/XDMParserPcdata;->data:[C

    invoke-static {v1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v13

    .line 3580
    :cond_4
    invoke-virtual {p0, v7, v13}, Lcom/policydm/agent/XDMAgent;->xdmAgentMakeAcl(Lcom/policydm/eng/core/XDMOmList;Ljava/lang/String;)Lcom/policydm/eng/core/XDMOmList;

    move-result-object v7

    .line 3581
    const/4 v13, 0x0

    .line 3582
    iget-object v1, v8, Lcom/policydm/eng/core/XDMVnode;->acl:Lcom/policydm/eng/core/XDMOmList;

    invoke-static {v1}, Lcom/policydm/eng/core/XDMOmList;->xdmOmDeleteAclList(Lcom/policydm/eng/core/XDMOmList;)V

    .line 3583
    iput-object v7, v8, Lcom/policydm/eng/core/XDMVnode;->acl:Lcom/policydm/eng/core/XDMOmList;

    .line 3585
    iget v2, v10, Lcom/policydm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v3, "Replace"

    const/4 v4, 0x0

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "200"

    move-object v1, p1

    invoke-static/range {v1 .. v6}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v11

    .line 3586
    .restart local v11    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    iget-object v1, p1, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v1, v11}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3627
    :cond_5
    :goto_1
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 3588
    .end local v11    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    :cond_6
    const-string v1, "Format"

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_7

    .line 3590
    iget v2, v10, Lcom/policydm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v3, "Replace"

    const/4 v4, 0x0

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "405"

    move-object v1, p1

    invoke-static/range {v1 .. v6}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v11

    .line 3591
    .restart local v11    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    iget-object v1, p1, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v1, v11}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto :goto_1

    .line 3593
    .end local v11    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    :cond_7
    const-string v1, "Type"

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_8

    .line 3595
    iget v2, v10, Lcom/policydm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v3, "Replace"

    const/4 v4, 0x0

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "405"

    move-object v1, p1

    invoke-static/range {v1 .. v6}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v11

    .line 3596
    .restart local v11    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    iget-object v1, p1, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v1, v11}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto :goto_1

    .line 3598
    .end local v11    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    :cond_8
    const-string v1, "Size"

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_9

    .line 3600
    iget v2, v10, Lcom/policydm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v3, "Replace"

    const/4 v4, 0x0

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "405"

    move-object v1, p1

    invoke-static/range {v1 .. v6}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v11

    .line 3601
    .restart local v11    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    iget-object v1, p1, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v1, v11}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto :goto_1

    .line 3603
    .end local v11    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    :cond_9
    const-string v1, "Name"

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_a

    .line 3605
    iget v2, v10, Lcom/policydm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v3, "Replace"

    const/4 v4, 0x0

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "405"

    move-object v1, p1

    invoke-static/range {v1 .. v6}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v11

    .line 3606
    .restart local v11    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    iget-object v1, p1, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v1, v11}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 3608
    .end local v11    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    :cond_a
    const-string v1, "Title"

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_b

    .line 3610
    iget v2, v10, Lcom/policydm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v3, "Replace"

    const/4 v4, 0x0

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "200"

    move-object v1, p1

    invoke-static/range {v1 .. v6}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v11

    .line 3611
    .restart local v11    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    iget-object v1, p1, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v1, v11}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3612
    const/4 v1, 0x0

    iput-object v1, v8, Lcom/policydm/eng/core/XDMVnode;->title:Ljava/lang/String;

    .line 3613
    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    invoke-static {v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStGetString(Lcom/policydm/eng/parser/XDMParserPcdata;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v8, Lcom/policydm/eng/core/XDMVnode;->title:Ljava/lang/String;

    .line 3614
    iget-object v1, v8, Lcom/policydm/eng/core/XDMVnode;->title:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 3616
    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    if-eqz v1, :cond_5

    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    iget-object v1, v1, Lcom/policydm/eng/parser/XDMParserPcdata;->data:[C

    if-eqz v1, :cond_5

    .line 3618
    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    iget-object v1, v1, Lcom/policydm/eng/parser/XDMParserPcdata;->data:[C

    invoke-static {v1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v8, Lcom/policydm/eng/core/XDMVnode;->title:Ljava/lang/String;

    goto/16 :goto_1

    .line 3624
    .end local v11    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    :cond_b
    iget v2, v10, Lcom/policydm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v3, "Replace"

    const/4 v4, 0x0

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v6, "405"

    move-object v1, p1

    invoke-static/range {v1 .. v6}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v11

    .line 3625
    .restart local v11    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    iget-object v1, p1, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v1, v11}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto/16 :goto_1
.end method

.method public xdmAgentCmdReplace(Lcom/policydm/eng/parser/XDMParserReplace;ZLcom/policydm/eng/parser/XDMParserStatus;)I
    .locals 21
    .param p1, "replace"    # Lcom/policydm/eng/parser/XDMParserReplace;
    .param p2, "isAtomic"    # Z
    .param p3, "atomic_status"    # Lcom/policydm/eng/parser/XDMParserStatus;

    .prologue
    .line 6502
    sget-object v2, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    .line 6503
    .local v2, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    iget-object v15, v2, Lcom/policydm/eng/core/XDMWorkspace;->om:Lcom/policydm/eng/core/XDMOmTree;

    .line 6506
    .local v15, "om":Lcom/policydm/eng/core/XDMOmTree;
    const/16 v20, 0x0

    .line 6507
    .local v20, "szType":Ljava/lang/String;
    const/16 v19, 0x0

    .line 6510
    .local v19, "szBuf":Ljava/lang/String;
    const/16 v10, 0xc

    .line 6513
    .local v10, "format":I
    const/4 v8, 0x0

    .line 6514
    .local v8, "bufsize":I
    const/16 v17, 0x0

    .line 6515
    .local v17, "res":I
    const/4 v13, 0x0

    .line 6517
    .local v13, "nFileId":I
    invoke-virtual/range {p0 .. p0}, Lcom/policydm/agent/XDMAgent;->xdmAgentCmdUicAlert()Z

    move-result v16

    .line 6520
    .local v16, "process":Z
    invoke-static {}, Lcom/policydm/db/XDB;->xdbGetFileIdTNDS()I

    move-result v13

    .line 6522
    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/policydm/eng/parser/XDMParserReplace;->itemlist:Lcom/policydm/eng/core/XDMList;

    .line 6523
    .local v9, "cur":Lcom/policydm/eng/core/XDMList;
    :goto_0
    if-eqz v9, :cond_3e

    .line 6525
    iget-object v11, v9, Lcom/policydm/eng/core/XDMList;->item:Ljava/lang/Object;

    check-cast v11, Lcom/policydm/eng/parser/XDMParserItem;

    .line 6527
    .local v11, "item":Lcom/policydm/eng/parser/XDMParserItem;
    iget-object v3, v11, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    if-nez v3, :cond_1

    .line 6529
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/policydm/eng/parser/XDMParserReplace;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    if-eqz v3, :cond_0

    .line 6530
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/policydm/eng/parser/XDMParserReplace;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iput-object v3, v11, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    .line 6550
    :cond_0
    :goto_1
    const/16 v18, 0x0

    .line 6552
    .local v18, "status":Lcom/policydm/eng/parser/XDMParserStatus;
    iget v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->serverAuthState:I

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    .line 6554
    iget-object v3, v11, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 6556
    move-object/from16 v0, p1

    iget v3, v0, Lcom/policydm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    iget-object v6, v11, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    iget-object v7, v2, Lcom/policydm/eng/core/XDMWorkspace;->m_szStatusReturnCode:Ljava/lang/String;

    invoke-static/range {v2 .. v7}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v18

    .line 6563
    :goto_2
    iget-object v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v18

    invoke-static {v3, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 6564
    iget-object v9, v9, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 6565
    goto :goto_0

    .line 6534
    .end local v18    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    :cond_1
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/policydm/eng/parser/XDMParserReplace;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    if-eqz v3, :cond_0

    .line 6536
    iget-object v3, v11, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v3, v3, Lcom/policydm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 6538
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/policydm/eng/parser/XDMParserReplace;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v3, v3, Lcom/policydm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 6539
    iget-object v3, v11, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/policydm/eng/parser/XDMParserReplace;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v4, v4, Lcom/policydm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    iput-object v4, v3, Lcom/policydm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    .line 6542
    :cond_2
    iget-object v3, v11, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v3, v3, Lcom/policydm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 6544
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/policydm/eng/parser/XDMParserReplace;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v3, v3, Lcom/policydm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 6545
    iget-object v3, v11, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/policydm/eng/parser/XDMParserReplace;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v4, v4, Lcom/policydm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    iput-object v4, v3, Lcom/policydm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    goto :goto_1

    .line 6560
    .restart local v18    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    :cond_3
    move-object/from16 v0, p1

    iget v3, v0, Lcom/policydm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    const/4 v6, 0x0

    iget-object v7, v2, Lcom/policydm/eng/core/XDMWorkspace;->m_szStatusReturnCode:Ljava/lang/String;

    invoke-static/range {v2 .. v7}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v18

    goto :goto_2

    .line 6567
    :cond_4
    iget-object v3, v11, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    if-eqz v3, :cond_5

    .line 6569
    iget-object v3, v11, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    iget v3, v3, Lcom/policydm/eng/parser/XDMParserPcdata;->size:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 6573
    :cond_5
    if-eqz p2, :cond_e

    iget-object v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->atomicStep:Lcom/policydm/interfaces/XDMInterface$XDMAtomicStep;

    sget-object v4, Lcom/policydm/interfaces/XDMInterface$XDMAtomicStep;->XDM_ATOMIC_NONE:Lcom/policydm/interfaces/XDMInterface$XDMAtomicStep;

    if-eq v3, v4, :cond_e

    .line 6575
    const/16 v18, 0x0

    .line 6576
    iget-object v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->tmpItem:Lcom/policydm/eng/parser/XDMParserItem;

    if-eqz v3, :cond_a

    .line 6579
    iget-object v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->tmpItem:Lcom/policydm/eng/parser/XDMParserItem;

    invoke-virtual {v3, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 6581
    iget-object v3, v11, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 6583
    move-object/from16 v0, p1

    iget v3, v0, Lcom/policydm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    iget-object v6, v11, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "404"

    invoke-static/range {v2 .. v7}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v18

    .line 6589
    :goto_3
    sget-object v3, Lcom/policydm/interfaces/XDMInterface$XDMAtomicStep;->XDM_ATOMIC_STEP_NOT_EXEC:Lcom/policydm/interfaces/XDMInterface$XDMAtomicStep;

    iput-object v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->atomicStep:Lcom/policydm/interfaces/XDMInterface$XDMAtomicStep;

    .line 6590
    const/4 v3, 0x0

    iput-object v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->tmpItem:Lcom/policydm/eng/parser/XDMParserItem;

    .line 6630
    :goto_4
    if-eqz v18, :cond_6

    .line 6632
    iget-object v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v18

    invoke-static {v3, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 6634
    :cond_6
    iget-object v9, v9, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 6635
    goto/16 :goto_0

    .line 6587
    :cond_7
    move-object/from16 v0, p1

    iget v3, v0, Lcom/policydm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "404"

    invoke-static/range {v2 .. v7}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v18

    goto :goto_3

    .line 6594
    :cond_8
    iget-object v3, v11, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_9

    .line 6596
    move-object/from16 v0, p1

    iget v3, v0, Lcom/policydm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    iget-object v6, v11, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "216"

    invoke-static/range {v2 .. v7}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v18

    goto :goto_4

    .line 6600
    :cond_9
    move-object/from16 v0, p1

    iget v3, v0, Lcom/policydm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "216"

    invoke-static/range {v2 .. v7}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v18

    goto :goto_4

    .line 6606
    :cond_a
    iget-object v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->atomicStep:Lcom/policydm/interfaces/XDMInterface$XDMAtomicStep;

    sget-object v4, Lcom/policydm/interfaces/XDMInterface$XDMAtomicStep;->XDM_ATOMIC_STEP_NOT_EXEC:Lcom/policydm/interfaces/XDMInterface$XDMAtomicStep;

    if-ne v3, v4, :cond_c

    .line 6608
    iget-object v3, v11, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_b

    .line 6610
    move-object/from16 v0, p1

    iget v3, v0, Lcom/policydm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    iget-object v6, v11, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "215"

    invoke-static/range {v2 .. v7}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v18

    goto :goto_4

    .line 6614
    :cond_b
    move-object/from16 v0, p1

    iget v3, v0, Lcom/policydm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "215"

    invoke-static/range {v2 .. v7}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v18

    goto :goto_4

    .line 6619
    :cond_c
    iget-object v3, v11, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_d

    .line 6621
    move-object/from16 v0, p1

    iget v3, v0, Lcom/policydm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    iget-object v6, v11, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "216"

    invoke-static/range {v2 .. v7}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v18

    goto/16 :goto_4

    .line 6625
    :cond_d
    move-object/from16 v0, p1

    iget v3, v0, Lcom/policydm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "216"

    invoke-static/range {v2 .. v7}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v18

    goto/16 :goto_4

    .line 6638
    :cond_e
    iget-object v3, v11, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 6640
    move-object/from16 v0, p1

    iget v3, v0, Lcom/policydm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "404"

    invoke-static/range {v2 .. v7}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v18

    .line 6641
    iget-object v9, v9, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 6642
    goto/16 :goto_0

    .line 6646
    :cond_f
    iget-object v3, v11, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v4, "?"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_10

    .line 6648
    const-string v3, "Replace"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v3, v11, v1}, Lcom/policydm/agent/XDMAgent;->xdmAgentCmdProp(Ljava/lang/String;Lcom/policydm/eng/parser/XDMParserItem;Ljava/lang/Object;)I

    move-result v17

    .line 6649
    iget-object v9, v9, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 6650
    goto/16 :goto_0

    .line 6653
    :cond_10
    iget-object v3, v11, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v15, v3}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v14

    .line 6654
    .local v14, "node":Lcom/policydm/eng/core/XDMVnode;
    if-nez v16, :cond_13

    .line 6656
    move-object/from16 v0, p1

    iget v3, v0, Lcom/policydm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    iget-object v6, v11, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "215"

    invoke-static/range {v2 .. v7}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v18

    .line 6660
    if-eqz p2, :cond_11

    if-eqz p3, :cond_11

    .line 6662
    const-string v3, "507"

    move-object/from16 v0, p3

    iput-object v3, v0, Lcom/policydm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    .line 6666
    :cond_11
    if-eqz v18, :cond_12

    .line 6668
    iget-object v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v18

    invoke-static {v3, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 6670
    :cond_12
    iget-object v9, v9, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 6671
    goto/16 :goto_0

    .line 6673
    :cond_13
    if-nez v14, :cond_1f

    .line 6675
    const-string v3, "node == null(not exist)"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 6676
    iget-object v3, v11, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v4, "/DownloadAndUpdate/PkgURL"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_14

    iget-object v3, v11, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v4, "/Ext"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1c

    .line 6678
    :cond_14
    iget-object v3, v11, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v15, v3}, Lcom/policydm/agent/XDMAgent;->xdmAgentReMakeFwUpdateNode(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)I

    .line 6736
    :cond_15
    const-string v3, "else"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 6737
    iget-object v3, v11, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 6739
    iget-object v3, v11, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    if-eqz v3, :cond_29

    .line 6741
    iget-object v3, v11, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v3, v3, Lcom/policydm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_16

    .line 6743
    iget-object v3, v11, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v0, v3, Lcom/policydm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    move-object/from16 v20, v0

    .line 6745
    :cond_16
    iget-object v3, v11, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v3, v3, Lcom/policydm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_17

    .line 6747
    iget-object v3, v11, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v3, v3, Lcom/policydm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    invoke-static {v3}, Lcom/policydm/eng/core/XDMOmList;->xdmOmGetFormatFromString(Ljava/lang/String;)I

    move-result v10

    .line 6750
    :cond_17
    iget-object v3, v11, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget v3, v3, Lcom/policydm/eng/parser/XDMParserMeta;->size:I

    if-lez v3, :cond_25

    .line 6752
    iget-object v3, v11, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget v3, v3, Lcom/policydm/eng/parser/XDMParserMeta;->size:I

    iput v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->dataTotalSize:I

    .line 6753
    const/4 v3, 0x0

    iput v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->prevBufPos:I

    .line 6767
    :cond_18
    :goto_5
    iget-object v3, v11, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    if-eqz v3, :cond_28

    iget-object v3, v11, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    iget-object v3, v3, Lcom/policydm/eng/parser/XDMParserPcdata;->data:[C

    if-eqz v3, :cond_28

    .line 6769
    iget-object v3, v11, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    invoke-static {v3}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStGetString(Lcom/policydm/eng/parser/XDMParserPcdata;)Ljava/lang/String;

    move-result-object v19

    .line 6770
    invoke-static/range {v19 .. v19}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_27

    .line 6772
    iget-object v3, v11, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    iget v8, v3, Lcom/policydm/eng/parser/XDMParserPcdata;->size:I

    .line 6773
    iget-object v3, v11, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    iget-object v3, v3, Lcom/policydm/eng/parser/XDMParserPcdata;->data:[C

    invoke-static {v3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v19

    .line 6807
    :cond_19
    :goto_6
    iget-boolean v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->nTNDSFlag:Z

    if-eqz v3, :cond_2d

    .line 6809
    const-string v3, "REPLACE ws.nTNDSFlag = true"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 6811
    invoke-static/range {v19 .. v19}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1a

    .line 6813
    const-string v19, ""

    .line 6815
    :cond_1a
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v3

    invoke-static {v13, v3}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpAppendFile(I[B)I

    .line 6816
    iget v3, v11, Lcom/policydm/eng/parser/XDMParserItem;->moredata:I

    if-nez v3, :cond_2c

    .line 6818
    const/4 v3, 0x0

    iput v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->prevBufPos:I

    .line 6819
    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 6820
    const/4 v3, 0x0

    iput v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->dataTotalSize:I

    .line 6821
    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->nTNDSFlag:Z

    .line 6822
    iget-object v3, v11, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const/4 v4, 0x1

    invoke-static {v15, v3, v4}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmDeleteImplicit(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;Z)I

    move-result v17

    .line 6823
    if-ltz v17, :cond_1b

    .line 6825
    invoke-static {v13, v15}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFCreateTNDSNodeFromFile(ILcom/policydm/eng/core/XDMOmTree;)I

    move-result v17

    .line 6828
    :cond_1b
    if-lez v17, :cond_2b

    .line 6830
    move-object/from16 v0, p1

    iget v3, v0, Lcom/policydm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    iget-object v6, v11, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "200"

    invoke-static/range {v2 .. v7}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v18

    .line 6831
    iget-object v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v18

    invoke-static {v3, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 6832
    iget-object v9, v9, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 6833
    goto/16 :goto_0

    .line 6682
    :cond_1c
    move-object/from16 v0, p1

    iget v3, v0, Lcom/policydm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    iget-object v6, v11, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "404"

    invoke-static/range {v2 .. v7}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v18

    .line 6684
    if-eqz p2, :cond_1d

    if-eqz p3, :cond_1d

    .line 6685
    const-string v3, "507"

    move-object/from16 v0, p3

    iput-object v3, v0, Lcom/policydm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    .line 6688
    :cond_1d
    if-eqz v18, :cond_1e

    .line 6689
    iget-object v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v18

    invoke-static {v3, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 6691
    :cond_1e
    iget-object v9, v9, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 6692
    goto/16 :goto_0

    .line 6695
    :cond_1f
    iget-object v3, v11, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v15, v3}, Lcom/policydm/agent/XDMAgent;->xdmAgentIsPermanentNode(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_22

    .line 6697
    const-string v3, "Fail"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 6698
    move-object/from16 v0, p1

    iget v3, v0, Lcom/policydm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    iget-object v6, v11, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "405"

    invoke-static/range {v2 .. v7}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v18

    .line 6702
    if-eqz p2, :cond_20

    if-eqz p3, :cond_20

    .line 6704
    const-string v3, "507"

    move-object/from16 v0, p3

    iput-object v3, v0, Lcom/policydm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    .line 6708
    :cond_20
    if-eqz v18, :cond_21

    .line 6710
    iget-object v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v18

    invoke-static {v3, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 6712
    :cond_21
    iget-object v9, v9, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 6713
    goto/16 :goto_0

    .line 6715
    :cond_22
    const/16 v3, 0x10

    invoke-static {v15, v14, v3}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmCheckAcl(Lcom/policydm/eng/core/XDMOmTree;Lcom/policydm/eng/core/XDMVnode;I)Z

    move-result v3

    if-nez v3, :cond_15

    .line 6717
    const-string v3, "xdmOmCheckAcl Fail"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 6718
    move-object/from16 v0, p1

    iget v3, v0, Lcom/policydm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    iget-object v6, v11, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "425"

    invoke-static/range {v2 .. v7}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v18

    .line 6722
    if-eqz p2, :cond_23

    if-eqz p3, :cond_23

    .line 6724
    const-string v3, "507"

    move-object/from16 v0, p3

    iput-object v3, v0, Lcom/policydm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    .line 6728
    :cond_23
    if-eqz v18, :cond_24

    .line 6730
    iget-object v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v18

    invoke-static {v3, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 6732
    :cond_24
    iget-object v9, v9, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 6733
    goto/16 :goto_0

    .line 6755
    :cond_25
    iget v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->prevBufPos:I

    if-nez v3, :cond_18

    .line 6757
    iget-object v3, v11, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    if-eqz v3, :cond_26

    .line 6759
    iget-object v3, v11, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    iget v3, v3, Lcom/policydm/eng/parser/XDMParserPcdata;->size:I

    iput v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->dataTotalSize:I

    goto/16 :goto_5

    .line 6763
    :cond_26
    const/4 v3, 0x0

    iput v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->dataTotalSize:I

    goto/16 :goto_5

    .line 6777
    :cond_27
    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v8

    goto/16 :goto_6

    .line 6782
    :cond_28
    const/4 v8, 0x0

    .line 6783
    const/16 v19, 0x0

    .line 6784
    const/4 v3, 0x0

    iput v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->dataTotalSize:I

    .line 6785
    const-string v3, "REPLACE ( no item->data)"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto/16 :goto_6

    .line 6788
    :cond_29
    iget-object v3, v11, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    if-eqz v3, :cond_19

    iget-object v3, v11, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    iget-object v3, v3, Lcom/policydm/eng/parser/XDMParserPcdata;->data:[C

    if-eqz v3, :cond_19

    .line 6790
    iget-object v3, v11, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    invoke-static {v3}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStGetString(Lcom/policydm/eng/parser/XDMParserPcdata;)Ljava/lang/String;

    move-result-object v19

    .line 6791
    invoke-static/range {v19 .. v19}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2a

    .line 6793
    iget-object v3, v11, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    iget v8, v3, Lcom/policydm/eng/parser/XDMParserPcdata;->size:I

    .line 6794
    iget-object v3, v11, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    iget-object v3, v3, Lcom/policydm/eng/parser/XDMParserPcdata;->data:[C

    invoke-static {v3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v19

    .line 6800
    :goto_7
    iget-boolean v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->dataBuffered:Z

    if-nez v3, :cond_19

    .line 6802
    iput v8, v2, Lcom/policydm/eng/core/XDMWorkspace;->dataTotalSize:I

    goto/16 :goto_6

    .line 6798
    :cond_2a
    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v8

    goto :goto_7

    .line 6835
    :cond_2b
    move-object/from16 v0, p1

    iget v3, v0, Lcom/policydm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    iget-object v6, v11, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "500"

    invoke-static/range {v2 .. v7}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v18

    .line 6836
    iget-object v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v18

    invoke-static {v3, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 6837
    iget-object v9, v9, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 6838
    goto/16 :goto_0

    .line 6841
    :cond_2c
    move-object/from16 v0, p1

    iget v3, v0, Lcom/policydm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    iget-object v6, v11, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "213"

    invoke-static/range {v2 .. v7}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v18

    .line 6842
    iget-object v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v18

    invoke-static {v3, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 6843
    iget-object v9, v9, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 6844
    goto/16 :goto_0

    .line 6847
    :cond_2d
    invoke-static/range {v20 .. v20}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_33

    .line 6849
    const-string v3, "application/vnd.syncml.dmtnds+xml"

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_32

    .line 6851
    iget v3, v11, Lcom/policydm/eng/parser/XDMParserItem;->moredata:I

    if-lez v3, :cond_30

    .line 6853
    iget v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->prevBufPos:I

    add-int/2addr v3, v8

    iput v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->prevBufPos:I

    .line 6854
    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 6856
    iget-boolean v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->nTNDSFlag:Z

    if-nez v3, :cond_2e

    .line 6858
    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->nTNDSFlag:Z

    .line 6859
    invoke-static {v13}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpDeleteFile(I)I

    .line 6862
    :cond_2e
    invoke-static/range {v19 .. v19}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2f

    .line 6864
    const-string v19, ""

    .line 6866
    :cond_2f
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v3

    invoke-static {v13, v3}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpAppendFile(I[B)I

    .line 6868
    move-object/from16 v0, p1

    iget v3, v0, Lcom/policydm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    iget-object v6, v11, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "213"

    invoke-static/range {v2 .. v7}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v18

    .line 6869
    iget-object v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v18

    invoke-static {v3, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 6870
    iget-object v9, v9, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 6871
    goto/16 :goto_0

    .line 6873
    :cond_30
    iget-object v3, v11, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const/4 v4, 0x1

    invoke-static {v15, v3, v4}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmDeleteImplicit(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;Z)I

    move-result v17

    .line 6874
    if-ltz v17, :cond_31

    .line 6876
    move-object/from16 v0, v19

    invoke-static {v0, v8, v15}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFCreateTNDSNode(Ljava/lang/String;ILcom/policydm/eng/core/XDMOmTree;)I

    move-result v17

    .line 6878
    if-lez v17, :cond_31

    .line 6880
    move-object/from16 v0, p1

    iget v3, v0, Lcom/policydm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    iget-object v6, v11, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "200"

    invoke-static/range {v2 .. v7}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v18

    .line 6881
    iget-object v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v18

    invoke-static {v3, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 6882
    iget-object v9, v9, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 6883
    goto/16 :goto_0

    .line 6887
    :cond_31
    move-object/from16 v0, p1

    iget v3, v0, Lcom/policydm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    iget-object v6, v11, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "500"

    invoke-static/range {v2 .. v7}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v18

    .line 6888
    iget-object v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v18

    invoke-static {v3, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 6889
    iget-object v9, v9, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 6890
    const-string v3, "Delete Fail.\n"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 6891
    const/4 v3, -0x1

    .line 6994
    .end local v11    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    .end local v14    # "node":Lcom/policydm/eng/core/XDMVnode;
    .end local v18    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    :goto_8
    return v3

    .line 6893
    .restart local v11    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    .restart local v14    # "node":Lcom/policydm/eng/core/XDMVnode;
    .restart local v18    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    :cond_32
    const-string v3, "application/vnd.syncml.dmtnds+wbxml"

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_33

    .line 6895
    move-object/from16 v0, p1

    iget v3, v0, Lcom/policydm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    iget-object v6, v11, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "406"

    invoke-static/range {v2 .. v7}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v18

    .line 6896
    iget-object v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v18

    invoke-static {v3, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 6897
    iget-object v9, v9, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 6898
    const-string v3, "Not Support TNDS with WBXML Type.\n"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 6899
    const/4 v3, -0x1

    goto :goto_8

    .line 6904
    :cond_33
    iget-object v3, v11, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v2, v3, v1}, Lcom/policydm/agent/XDMAgent;->xdmAgentVerifyPolicyNode(Lcom/policydm/eng/core/XDMWorkspace;Ljava/lang/String;Ljava/lang/String;)I

    move-result v17

    .line 6906
    if-lez v17, :cond_37

    .line 6908
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "item.m_szTarget="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v11, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 6909
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bufsize : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ws.dataTotalSize : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v2, Lcom/policydm/eng/core/XDMWorkspace;->dataTotalSize:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 6910
    iget-object v4, v11, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    iget v5, v2, Lcom/policydm/eng/core/XDMWorkspace;->dataTotalSize:I

    iget v6, v2, Lcom/policydm/eng/core/XDMWorkspace;->prevBufPos:I

    move-object v3, v15

    move-object/from16 v7, v19

    invoke-static/range {v3 .. v8}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmWrite(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;IILjava/lang/Object;I)I

    move-result v17

    .line 6919
    :goto_9
    invoke-static/range {v19 .. v19}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_38

    .line 6921
    const/16 v19, 0x0

    .line 6929
    :cond_34
    :goto_a
    if-gez v17, :cond_39

    .line 6932
    move-object/from16 v0, p1

    iget v3, v0, Lcom/policydm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    iget-object v6, v11, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "500"

    invoke-static/range {v2 .. v7}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v18

    .line 6936
    if-eqz p2, :cond_35

    if-eqz p3, :cond_35

    .line 6938
    const-string v3, "507"

    move-object/from16 v0, p3

    iput-object v3, v0, Lcom/policydm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    .line 6987
    :cond_35
    :goto_b
    if-eqz v18, :cond_36

    .line 6989
    iget-object v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v0, v18

    invoke-static {v3, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 6991
    :cond_36
    iget-object v9, v9, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    goto/16 :goto_0

    .line 6914
    :cond_37
    iget-object v3, v11, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 6915
    const/16 v17, -0x1

    goto :goto_9

    .line 6923
    :cond_38
    if-eqz v14, :cond_34

    .line 6925
    const/4 v3, -0x1

    iput v3, v14, Lcom/policydm/eng/core/XDMVnode;->vaddr:I

    .line 6926
    const/4 v3, 0x0

    iput v3, v14, Lcom/policydm/eng/core/XDMVnode;->size:I

    goto :goto_a

    .line 6945
    :cond_39
    iget-object v3, v11, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v15, v3}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v14

    .line 6946
    if-eqz v14, :cond_3c

    .line 6948
    invoke-static/range {v20 .. v20}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3b

    .line 6950
    iget-object v3, v14, Lcom/policydm/eng/core/XDMVnode;->type:Lcom/policydm/eng/core/XDMOmList;

    if-eqz v3, :cond_3a

    .line 6952
    iget-object v3, v14, Lcom/policydm/eng/core/XDMVnode;->type:Lcom/policydm/eng/core/XDMOmList;

    invoke-static {v3}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmVfsDeleteMimeList(Lcom/policydm/eng/core/XDMOmList;)V

    .line 6954
    :cond_3a
    new-instance v12, Lcom/policydm/eng/core/XDMOmList;

    invoke-direct {v12}, Lcom/policydm/eng/core/XDMOmList;-><init>()V

    .line 6955
    .local v12, "list":Lcom/policydm/eng/core/XDMOmList;
    move-object/from16 v0, v20

    iput-object v0, v12, Lcom/policydm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    .line 6956
    const/4 v3, 0x0

    iput-object v3, v12, Lcom/policydm/eng/core/XDMOmList;->next:Lcom/policydm/eng/core/XDMOmList;

    .line 6957
    iput-object v12, v14, Lcom/policydm/eng/core/XDMVnode;->type:Lcom/policydm/eng/core/XDMOmList;

    .line 6960
    .end local v12    # "list":Lcom/policydm/eng/core/XDMOmList;
    :cond_3b
    if-lez v10, :cond_3c

    .line 6962
    iput v10, v14, Lcom/policydm/eng/core/XDMVnode;->format:I

    .line 6968
    :cond_3c
    iget v3, v11, Lcom/policydm/eng/parser/XDMParserItem;->moredata:I

    if-nez v3, :cond_3d

    .line 6970
    const/4 v3, 0x0

    iput v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->prevBufPos:I

    .line 6971
    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 6972
    const/4 v3, 0x0

    iput v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->dataTotalSize:I

    .line 6975
    move-object/from16 v0, p1

    iget v3, v0, Lcom/policydm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    iget-object v6, v11, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "200"

    invoke-static/range {v2 .. v7}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v18

    goto :goto_b

    .line 6979
    :cond_3d
    iget v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->prevBufPos:I

    add-int/2addr v3, v8

    iput v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->prevBufPos:I

    .line 6980
    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 6983
    move-object/from16 v0, p1

    iget v3, v0, Lcom/policydm/eng/parser/XDMParserReplace;->cmdid:I

    const-string v4, "Replace"

    const/4 v5, 0x0

    iget-object v6, v11, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    const-string v7, "213"

    invoke-static/range {v2 .. v7}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v18

    goto :goto_b

    .line 6994
    .end local v11    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    .end local v14    # "node":Lcom/policydm/eng/core/XDMVnode;
    .end local v18    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    :cond_3e
    const/4 v3, 0x0

    goto/16 :goto_8
.end method

.method public xdmAgentCmdSequence(Lcom/policydm/eng/parser/XDMParserSequence;)I
    .locals 10
    .param p1, "sequence"    # Lcom/policydm/eng/parser/XDMParserSequence;

    .prologue
    const/4 v3, 0x0

    const/4 v9, -0x4

    const/4 v8, -0x1

    .line 7650
    sget-object v0, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    .line 7652
    .local v0, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    const/4 v6, 0x0

    .line 7654
    .local v6, "res":I
    if-nez p1, :cond_0

    move v1, v8

    .line 7684
    :goto_0
    return v1

    .line 7659
    :cond_0
    iget-boolean v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->IsSequenceProcessing:Z

    if-nez v1, :cond_1

    .line 7661
    iget v1, p1, Lcom/policydm/eng/parser/XDMParserSequence;->cmdid:I

    const-string v2, "Sequence"

    const-string v5, "200"

    move-object v4, v3

    invoke-static/range {v0 .. v5}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v7

    .line 7662
    .local v7, "status":Lcom/policydm/eng/parser/XDMParserStatus;
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v1, v7}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 7664
    .end local v7    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    :cond_1
    iget-object v1, p1, Lcom/policydm/eng/parser/XDMParserSequence;->itemlist:Lcom/policydm/eng/core/XDMLinkedList;

    if-eqz v1, :cond_2

    .line 7666
    iget-object v1, p1, Lcom/policydm/eng/parser/XDMParserSequence;->itemlist:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v1}, Lcom/policydm/agent/XDMAgent;->xdmAgentCmdSequenceBlock(Lcom/policydm/eng/core/XDMLinkedList;)I

    move-result v6

    .line 7673
    if-ne v6, v9, :cond_3

    move v1, v9

    .line 7675
    goto :goto_0

    :cond_2
    move v1, v8

    .line 7670
    goto :goto_0

    .line 7678
    :cond_3
    if-gez v6, :cond_4

    move v1, v8

    .line 7680
    goto :goto_0

    :cond_4
    move v1, v6

    .line 7684
    goto :goto_0
.end method

.method public xdmAgentCmdSequenceBlock(Lcom/policydm/eng/core/XDMLinkedList;)I
    .locals 9
    .param p1, "list"    # Lcom/policydm/eng/core/XDMLinkedList;

    .prologue
    const/4 v8, 0x0

    const/4 v5, -0x4

    .line 4710
    sget-object v4, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    .line 4712
    .local v4, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    const/4 v3, 0x0

    .line 4713
    .local v3, "status":Lcom/policydm/eng/parser/XDMParserStatus;
    const/4 v2, 0x0

    .line 4714
    .local v2, "res":I
    const/4 v1, 0x0

    .line 4716
    .local v1, "isAtomic":Z
    invoke-static {p1, v8}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListSetCurrentObj(Lcom/policydm/eng/core/XDMLinkedList;I)V

    .line 4717
    invoke-static {p1}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/policydm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/policydm/agent/XDMAgent;

    .line 4719
    .local v0, "cmditem":Lcom/policydm/agent/XDMAgent;
    if-eqz v0, :cond_0

    .line 4720
    const/4 v6, 0x1

    iput-boolean v6, v4, Lcom/policydm/eng/core/XDMWorkspace;->IsSequenceProcessing:Z

    .line 4721
    :cond_0
    if-eqz v0, :cond_9

    .line 4726
    const-string v6, "Get"

    iget-object v7, v0, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-eqz v6, :cond_1

    const-string v6, "Exec"

    iget-object v7, v0, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-eqz v6, :cond_1

    const-string v6, "Alert"

    iget-object v7, v0, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-eqz v6, :cond_1

    const-string v6, "Add"

    iget-object v7, v0, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-eqz v6, :cond_1

    const-string v6, "Replace"

    iget-object v7, v0, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-eqz v6, :cond_1

    const-string v6, "Copy"

    iget-object v7, v0, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-eqz v6, :cond_1

    const-string v6, "Delete"

    iget-object v7, v0, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_2

    .line 4729
    :cond_1
    iget v6, v4, Lcom/policydm/eng/core/XDMWorkspace;->numAction:I

    add-int/lit8 v6, v6, 0x1

    iput v6, v4, Lcom/policydm/eng/core/XDMWorkspace;->numAction:I

    .line 4732
    :cond_2
    const-string v6, "Atomic_Start"

    iget-object v7, v0, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_4

    .line 4734
    const/4 v1, 0x1

    .line 4735
    const-string v6, "Atomic_Start"

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 4737
    iget-boolean v6, v4, Lcom/policydm/eng/core/XDMWorkspace;->inAtomicCmd:Z

    if-eqz v6, :cond_3

    .line 4739
    iget-object v6, v0, Lcom/policydm/agent/XDMAgent;->m_Atomic:Lcom/policydm/eng/parser/XDMParserAtomic;

    invoke-virtual {p0, v6}, Lcom/policydm/agent/XDMAgent;->xdmAgentCmdAtomic(Lcom/policydm/eng/parser/XDMParserAtomic;)I

    move-result v2

    .line 4745
    :goto_0
    const/4 v1, 0x0

    .line 4764
    :goto_1
    invoke-static {p1}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/policydm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "cmditem":Lcom/policydm/agent/XDMAgent;
    check-cast v0, Lcom/policydm/agent/XDMAgent;

    .line 4765
    .restart local v0    # "cmditem":Lcom/policydm/agent/XDMAgent;
    invoke-static {p1}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListRemoveObjAtFirst(Lcom/policydm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    .line 4767
    if-ne v2, v5, :cond_7

    .line 4771
    iput-object p1, v4, Lcom/policydm/eng/core/XDMWorkspace;->sequenceList:Lcom/policydm/eng/core/XDMLinkedList;

    .line 4789
    :goto_2
    return v5

    .line 4743
    :cond_3
    invoke-virtual {p0, v0, v1, v3}, Lcom/policydm/agent/XDMAgent;->xdmAgentVerifyCmd(Lcom/policydm/agent/XDMAgent;ZLcom/policydm/eng/parser/XDMParserStatus;)I

    move-result v2

    goto :goto_0

    .line 4747
    :cond_4
    const-string v6, "Sequence_Start"

    iget-object v7, v0, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_6

    .line 4749
    const-string v6, "Sequence_Start"

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 4750
    iget-boolean v6, v4, Lcom/policydm/eng/core/XDMWorkspace;->inSequenceCmd:Z

    if-eqz v6, :cond_5

    .line 4752
    iget-object v6, v0, Lcom/policydm/agent/XDMAgent;->m_Sequence:Lcom/policydm/eng/parser/XDMParserSequence;

    invoke-virtual {p0, v6}, Lcom/policydm/agent/XDMAgent;->xdmAgentCmdSequence(Lcom/policydm/eng/parser/XDMParserSequence;)I

    move-result v2

    goto :goto_1

    .line 4756
    :cond_5
    invoke-virtual {p0, v0, v1, v3}, Lcom/policydm/agent/XDMAgent;->xdmAgentVerifyCmd(Lcom/policydm/agent/XDMAgent;ZLcom/policydm/eng/parser/XDMParserStatus;)I

    move-result v2

    goto :goto_1

    .line 4761
    :cond_6
    invoke-virtual {p0, v0, v1, v3}, Lcom/policydm/agent/XDMAgent;->xdmAgentVerifyCmd(Lcom/policydm/agent/XDMAgent;ZLcom/policydm/eng/parser/XDMParserStatus;)I

    move-result v2

    goto :goto_1

    .line 4774
    :cond_7
    const/4 v6, 0x3

    if-ne v2, v6, :cond_8

    move v5, v2

    .line 4776
    goto :goto_2

    .line 4778
    :cond_8
    if-eqz v2, :cond_0

    .line 4780
    const-string v5, "Processing failed"

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 4781
    const/4 v5, -0x1

    goto :goto_2

    .line 4787
    :cond_9
    invoke-static {p1}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListClearLinkedList(Lcom/policydm/eng/core/XDMLinkedList;)V

    .line 4788
    iput-boolean v8, v4, Lcom/policydm/eng/core/XDMWorkspace;->IsSequenceProcessing:Z

    move v5, v2

    .line 4789
    goto :goto_2
.end method

.method public xdmAgentCmdStatus(Lcom/policydm/eng/parser/XDMParserStatus;)I
    .locals 11
    .param p1, "status"    # Lcom/policydm/eng/parser/XDMParserStatus;

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 4898
    sget-object v5, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    .line 4901
    .local v5, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    const/4 v0, 0x0

    .line 4903
    .local v0, "dValue":[B
    const-string v6, "401"

    iget-object v7, p1, Lcom/policydm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_1

    .line 4905
    const/4 v6, -0x7

    iput v6, v5, Lcom/policydm/eng/core/XDMWorkspace;->authState:I

    .line 4906
    const-string v6, "Client invalid credential(401)"

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 4907
    const-string v6, "SyncHdr"

    iget-object v7, p1, Lcom/policydm/eng/parser/XDMParserStatus;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p1, Lcom/policydm/eng/parser/XDMParserStatus;->chal:Lcom/policydm/eng/parser/XDMParserMeta;

    if-nez v6, :cond_0

    .line 4909
    const-string v6, "SyncHdr Status 401. and No Chal"

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 4910
    const/4 v6, 0x3

    iput v6, v5, Lcom/policydm/eng/core/XDMWorkspace;->authCount:I

    .line 4948
    :cond_0
    :goto_0
    iget-object v6, p1, Lcom/policydm/eng/parser/XDMParserStatus;->chal:Lcom/policydm/eng/parser/XDMParserMeta;

    if-eqz v6, :cond_9

    .line 4950
    const-string v6, "syncml:auth-md5"

    iget-object v7, p1, Lcom/policydm/eng/parser/XDMParserStatus;->chal:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v7, v7, Lcom/policydm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_b

    .line 4952
    iput v8, v5, Lcom/policydm/eng/core/XDMWorkspace;->credType:I

    .line 4953
    iget v6, v5, Lcom/policydm/eng/core/XDMWorkspace;->credType:I

    invoke-static {v6}, Lcom/policydm/db/XDBProfileAdp;->xdbSetAuthType(I)V

    .line 4955
    const-string v6, "b64"

    iget-object v7, p1, Lcom/policydm/eng/parser/XDMParserStatus;->chal:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v7, v7, Lcom/policydm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_a

    .line 4965
    sget-object v6, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v6, Lcom/policydm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 4966
    .local v2, "szAccBuf":Ljava/lang/String;
    const-string v6, "/AAuthData"

    invoke-virtual {v2, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 4968
    iget-object v6, p1, Lcom/policydm/eng/parser/XDMParserStatus;->chal:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v6, v6, Lcom/policydm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    invoke-virtual {p0, v2, v6}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMB64(Ljava/lang/String;Ljava/lang/Object;)V

    .line 4970
    iget-object v6, p1, Lcom/policydm/eng/parser/XDMParserStatus;->chal:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v6, v6, Lcom/policydm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v6

    invoke-static {v6}, Lcom/policydm/eng/core/XDMBase64;->xdmBase64Decode([B)[B

    move-result-object v0

    .line 4971
    if-eqz v0, :cond_7

    .line 4973
    array-length v6, v0

    new-array v6, v6, [B

    iput-object v6, v5, Lcom/policydm/eng/core/XDMWorkspace;->nextNonce:[B

    .line 4974
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v6, v0

    if-ge v1, v6, :cond_8

    .line 4975
    iget-object v6, v5, Lcom/policydm/eng/core/XDMWorkspace;->nextNonce:[B

    aget-byte v7, v0, v1

    aput-byte v7, v6, v1

    .line 4974
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 4913
    .end local v1    # "i":I
    .end local v2    # "szAccBuf":Ljava/lang/String;
    :cond_1
    const-string v6, "212"

    iget-object v7, p1, Lcom/policydm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_2

    .line 4915
    iput v8, v5, Lcom/policydm/eng/core/XDMWorkspace;->authState:I

    goto :goto_0

    .line 4917
    :cond_2
    const-string v6, "200"

    iget-object v7, p1, Lcom/policydm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_5

    const-string v6, "SyncHdr"

    iget-object v7, p1, Lcom/policydm/eng/parser/XDMParserStatus;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_5

    .line 4919
    iget v6, v5, Lcom/policydm/eng/core/XDMWorkspace;->credType:I

    if-ne v6, v10, :cond_4

    .line 4921
    iget-object v6, p1, Lcom/policydm/eng/parser/XDMParserStatus;->chal:Lcom/policydm/eng/parser/XDMParserMeta;

    if-eqz v6, :cond_3

    .line 4923
    iput v8, v5, Lcom/policydm/eng/core/XDMWorkspace;->authState:I

    .line 4930
    :cond_3
    :goto_2
    const-string v6, "Client Authrization Accepted (Catch 200)"

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 4928
    :cond_4
    iput v8, v5, Lcom/policydm/eng/core/XDMWorkspace;->authState:I

    goto :goto_2

    .line 4932
    :cond_5
    const-string v6, "213"

    iget-object v7, p1, Lcom/policydm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_0

    .line 4934
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "received Status \'buffered\' cmd "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p1, Lcom/policydm/eng/parser/XDMParserStatus;->m_szCmdRef:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 4936
    iget-object v6, v5, Lcom/policydm/eng/core/XDMWorkspace;->tempResults:Lcom/policydm/eng/parser/XDMParserResults;

    if-eqz v6, :cond_6

    .line 4938
    new-instance v4, Lcom/policydm/eng/parser/XDMParserResults;

    invoke-direct {v4}, Lcom/policydm/eng/parser/XDMParserResults;-><init>()V

    .line 4939
    .local v4, "tmp":Lcom/policydm/eng/parser/XDMParserResults;
    iget-object v6, v5, Lcom/policydm/eng/core/XDMWorkspace;->tempResults:Lcom/policydm/eng/parser/XDMParserResults;

    invoke-static {v4, v6}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplResults(Lcom/policydm/eng/parser/XDMParserResults;Lcom/policydm/eng/parser/XDMParserResults;)V

    .line 4940
    iget-object v6, v5, Lcom/policydm/eng/core/XDMWorkspace;->resultsList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v6, v4}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtFirst(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 4944
    .end local v4    # "tmp":Lcom/policydm/eng/parser/XDMParserResults;
    :cond_6
    const-string v6, "can\'t find cached results can\'t send multi-messaged"

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 4979
    .restart local v2    # "szAccBuf":Ljava/lang/String;
    :cond_7
    const-string v6, "########## dValue is NULL"

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 4982
    :cond_8
    new-instance v3, Ljava/lang/String;

    iget-object v6, v5, Lcom/policydm/eng/core/XDMWorkspace;->nextNonce:[B

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v7

    invoke-direct {v3, v6, v7}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 4983
    .local v3, "szTemp":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "receive nextNonce:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p1, Lcom/policydm/eng/parser/XDMParserStatus;->chal:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v7, v7, Lcom/policydm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "B64 decode String(ws.nextNonce):"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 5002
    .end local v3    # "szTemp":Ljava/lang/String;
    :goto_3
    iget-object v6, p1, Lcom/policydm/eng/parser/XDMParserStatus;->chal:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v6, v6, Lcom/policydm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    invoke-static {v6}, Lcom/policydm/db/XDBProfileAdp;->xdbSetClientNonce(Ljava/lang/String;)Ljava/lang/String;

    .line 5011
    sget-object v6, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v6, Lcom/policydm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 5012
    const-string v6, "/AAuthPref"

    invoke-virtual {v2, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 5014
    iget-object v6, p1, Lcom/policydm/eng/parser/XDMParserStatus;->chal:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v6, v6, Lcom/policydm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    invoke-static {v2, v6}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOM(Ljava/lang/String;Ljava/lang/Object;)V

    .line 5096
    .end local v2    # "szAccBuf":Ljava/lang/String;
    :cond_9
    :goto_4
    return v9

    .line 4988
    :cond_a
    const-string v6, "!B64"

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 4996
    sget-object v6, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v6, Lcom/policydm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 4997
    .restart local v2    # "szAccBuf":Ljava/lang/String;
    const-string v6, "/AAuthData"

    invoke-virtual {v2, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 4999
    iget-object v6, p1, Lcom/policydm/eng/parser/XDMParserStatus;->chal:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v6, v6, Lcom/policydm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    invoke-static {v2, v6}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOM(Ljava/lang/String;Ljava/lang/Object;)V

    .line 5000
    iget-object v6, p1, Lcom/policydm/eng/parser/XDMParserStatus;->chal:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v6, v6, Lcom/policydm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v6

    iput-object v6, v5, Lcom/policydm/eng/core/XDMWorkspace;->nextNonce:[B

    goto :goto_3

    .line 5016
    .end local v2    # "szAccBuf":Ljava/lang/String;
    :cond_b
    const-string v6, "syncml:auth-MAC"

    iget-object v7, p1, Lcom/policydm/eng/parser/XDMParserStatus;->chal:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v7, v7, Lcom/policydm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_f

    .line 5018
    iput v10, v5, Lcom/policydm/eng/core/XDMWorkspace;->credType:I

    .line 5019
    iget v6, v5, Lcom/policydm/eng/core/XDMWorkspace;->credType:I

    invoke-static {v6}, Lcom/policydm/db/XDBProfileAdp;->xdbSetAuthType(I)V

    .line 5021
    const-string v6, "b64"

    iget-object v7, p1, Lcom/policydm/eng/parser/XDMParserStatus;->chal:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v7, v7, Lcom/policydm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_e

    .line 5030
    sget-object v6, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v6, Lcom/policydm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 5031
    .restart local v2    # "szAccBuf":Ljava/lang/String;
    const-string v6, "/AAuthData"

    invoke-virtual {v2, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 5033
    iget-object v6, p1, Lcom/policydm/eng/parser/XDMParserStatus;->chal:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v6, v6, Lcom/policydm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    invoke-virtual {p0, v2, v6}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMB64(Ljava/lang/String;Ljava/lang/Object;)V

    .line 5034
    iget-object v6, p1, Lcom/policydm/eng/parser/XDMParserStatus;->chal:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v6, v6, Lcom/policydm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v6

    invoke-static {v6}, Lcom/policydm/eng/core/XDMBase64;->xdmBase64Decode([B)[B

    move-result-object v0

    .line 5035
    if-eqz v0, :cond_c

    .line 5037
    array-length v6, v0

    new-array v6, v6, [B

    iput-object v6, v5, Lcom/policydm/eng/core/XDMWorkspace;->nextNonce:[B

    .line 5038
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_5
    array-length v6, v0

    if-ge v1, v6, :cond_d

    .line 5039
    iget-object v6, v5, Lcom/policydm/eng/core/XDMWorkspace;->nextNonce:[B

    aget-byte v7, v0, v1

    aput-byte v7, v6, v1

    .line 5038
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 5043
    .end local v1    # "i":I
    :cond_c
    const-string v6, "########## dValue is NULL"

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 5045
    :cond_d
    new-instance v3, Ljava/lang/String;

    iget-object v6, v5, Lcom/policydm/eng/core/XDMWorkspace;->nextNonce:[B

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v7

    invoke-direct {v3, v6, v7}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 5046
    .restart local v3    # "szTemp":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "B64 decode nextNonce"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 5063
    .end local v3    # "szTemp":Ljava/lang/String;
    :goto_6
    iget-object v6, p1, Lcom/policydm/eng/parser/XDMParserStatus;->chal:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v6, v6, Lcom/policydm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    invoke-static {v6}, Lcom/policydm/db/XDBProfileAdp;->xdbSetClientNonce(Ljava/lang/String;)Ljava/lang/String;

    .line 5072
    sget-object v6, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v6, Lcom/policydm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 5073
    const-string v6, "/AAuthPref"

    invoke-virtual {v2, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 5075
    iget-object v6, p1, Lcom/policydm/eng/parser/XDMParserStatus;->chal:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v6, v6, Lcom/policydm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    invoke-static {v2, v6}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOM(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_4

    .line 5057
    .end local v2    # "szAccBuf":Ljava/lang/String;
    :cond_e
    sget-object v6, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v6, Lcom/policydm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 5058
    .restart local v2    # "szAccBuf":Ljava/lang/String;
    const-string v6, "/AAuthData"

    invoke-virtual {v2, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 5060
    iget-object v6, p1, Lcom/policydm/eng/parser/XDMParserStatus;->chal:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v6, v6, Lcom/policydm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    invoke-static {v2, v6}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOM(Ljava/lang/String;Ljava/lang/Object;)V

    .line 5061
    iget-object v6, p1, Lcom/policydm/eng/parser/XDMParserStatus;->chal:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v6, v6, Lcom/policydm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v6

    iput-object v6, v5, Lcom/policydm/eng/core/XDMWorkspace;->nextNonce:[B

    goto :goto_6

    .line 5077
    .end local v2    # "szAccBuf":Ljava/lang/String;
    :cond_f
    const-string v6, "syncml:auth-basic"

    iget-object v7, p1, Lcom/policydm/eng/parser/XDMParserStatus;->chal:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v7, v7, Lcom/policydm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_9

    .line 5080
    iput v9, v5, Lcom/policydm/eng/core/XDMWorkspace;->credType:I

    .line 5081
    iget v6, v5, Lcom/policydm/eng/core/XDMWorkspace;->credType:I

    invoke-static {v6}, Lcom/policydm/db/XDBProfileAdp;->xdbSetAuthType(I)V

    .line 5090
    sget-object v6, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v6, Lcom/policydm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 5091
    .restart local v2    # "szAccBuf":Ljava/lang/String;
    const-string v6, "/AAuthPref"

    invoke-virtual {v2, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 5093
    iget-object v6, p1, Lcom/policydm/eng/parser/XDMParserStatus;->chal:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v6, v6, Lcom/policydm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    invoke-static {v2, v6}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOM(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_4
.end method

.method public xdmAgentCmdSyncHeader(Lcom/policydm/eng/parser/XDMParserSyncheader;)I
    .locals 8
    .param p1, "synchdr"    # Lcom/policydm/eng/parser/XDMParserSyncheader;

    .prologue
    const/4 v7, 0x2

    const/4 v5, 0x1

    const/4 v1, 0x0

    const/16 v4, -0x9

    const/high16 v3, 0x100000

    .line 4794
    sget-object v0, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    .line 4797
    .local v0, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    iget v2, p1, Lcom/policydm/eng/parser/XDMParserSyncheader;->msgid:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/policydm/eng/core/XDMWorkspace;->m_szMsgRef:Ljava/lang/String;

    .line 4799
    iget-object v2, p1, Lcom/policydm/eng/parser/XDMParserSyncheader;->m_szRespUri:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 4801
    iget-object v2, p1, Lcom/policydm/eng/parser/XDMParserSyncheader;->m_szRespUri:Ljava/lang/String;

    iput-object v2, v0, Lcom/policydm/eng/core/XDMWorkspace;->m_szTargetURI:Ljava/lang/String;

    .line 4806
    :cond_0
    iget-object v2, p1, Lcom/policydm/eng/parser/XDMParserSyncheader;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    if-eqz v2, :cond_6

    .line 4808
    iget-object v2, p1, Lcom/policydm/eng/parser/XDMParserSyncheader;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget v2, v2, Lcom/policydm/eng/parser/XDMParserMeta;->maxobjsize:I

    if-lez v2, :cond_4

    .line 4810
    iget-object v2, p1, Lcom/policydm/eng/parser/XDMParserSyncheader;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget v2, v2, Lcom/policydm/eng/parser/XDMParserMeta;->maxobjsize:I

    iput v2, v0, Lcom/policydm/eng/core/XDMWorkspace;->serverMaxObjSize:I

    .line 4811
    iget v2, v0, Lcom/policydm/eng/core/XDMWorkspace;->serverMaxObjSize:I

    if-gtz v2, :cond_3

    .line 4813
    iput v3, v0, Lcom/policydm/eng/core/XDMWorkspace;->serverMaxObjSize:I

    .line 4825
    :cond_1
    :goto_0
    iget-object v2, p1, Lcom/policydm/eng/parser/XDMParserSyncheader;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget v2, v2, Lcom/policydm/eng/parser/XDMParserMeta;->maxmsgsize:I

    if-lez v2, :cond_5

    .line 4827
    iget-object v2, p1, Lcom/policydm/eng/parser/XDMParserSyncheader;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget v2, v2, Lcom/policydm/eng/parser/XDMParserMeta;->maxmsgsize:I

    iput v2, v0, Lcom/policydm/eng/core/XDMWorkspace;->serverMaxMsgSize:I

    .line 4840
    :goto_1
    iget v2, v0, Lcom/policydm/eng/core/XDMWorkspace;->serverAuthState:I

    if-eq v2, v5, :cond_2

    .line 4842
    iget v2, v0, Lcom/policydm/eng/core/XDMWorkspace;->serverCredType:I

    if-eq v2, v7, :cond_a

    .line 4844
    iget-object v2, p1, Lcom/policydm/eng/parser/XDMParserSyncheader;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    if-eqz v2, :cond_7

    .line 4846
    invoke-virtual {p0, p1}, Lcom/policydm/agent/XDMAgent;->xdmAgentVerifyServerAuth(Lcom/policydm/eng/parser/XDMParserSyncheader;)I

    move-result v2

    iput v2, v0, Lcom/policydm/eng/core/XDMWorkspace;->serverAuthState:I

    .line 4868
    :cond_2
    :goto_2
    iget v2, v0, Lcom/policydm/eng/core/XDMWorkspace;->serverAuthState:I

    if-ne v2, v5, :cond_c

    .line 4870
    iget v2, v0, Lcom/policydm/eng/core/XDMWorkspace;->serverCredType:I

    if-ne v2, v7, :cond_b

    .line 4872
    const-string v2, "200"

    iput-object v2, v0, Lcom/policydm/eng/core/XDMWorkspace;->m_szStatusReturnCode:Ljava/lang/String;

    .line 4878
    :goto_3
    iget v2, v0, Lcom/policydm/eng/core/XDMWorkspace;->serverCredType:I

    invoke-static {v2}, Lcom/policydm/db/XDBProfileAdp;->xdbSetServerAuthType(I)V

    .line 4890
    :goto_4
    const-string v2, "SyncHdr"

    iget-object v3, v0, Lcom/policydm/eng/core/XDMWorkspace;->m_szHostname:Ljava/lang/String;

    iget-object v4, v0, Lcom/policydm/eng/core/XDMWorkspace;->m_szSourceURI:Ljava/lang/String;

    iget-object v5, v0, Lcom/policydm/eng/core/XDMWorkspace;->m_szStatusReturnCode:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v6

    .line 4891
    .local v6, "status":Lcom/policydm/eng/parser/XDMParserStatus;
    iget-object v2, v0, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v2, v6}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 4893
    return v1

    .line 4815
    .end local v6    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    :cond_3
    iget v2, v0, Lcom/policydm/eng/core/XDMWorkspace;->serverMaxObjSize:I

    if-le v2, v3, :cond_1

    .line 4817
    iput v3, v0, Lcom/policydm/eng/core/XDMWorkspace;->serverMaxObjSize:I

    goto :goto_0

    .line 4822
    :cond_4
    iput v3, v0, Lcom/policydm/eng/core/XDMWorkspace;->serverMaxObjSize:I

    goto :goto_0

    .line 4831
    :cond_5
    const/16 v2, 0x1400

    iput v2, v0, Lcom/policydm/eng/core/XDMWorkspace;->serverMaxMsgSize:I

    goto :goto_1

    .line 4836
    :cond_6
    iput v3, v0, Lcom/policydm/eng/core/XDMWorkspace;->serverMaxObjSize:I

    .line 4837
    const/16 v2, 0x1400

    iput v2, v0, Lcom/policydm/eng/core/XDMWorkspace;->serverMaxMsgSize:I

    goto :goto_1

    .line 4850
    :cond_7
    const-string v2, "Not Used Server Authentication"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 4851
    iget v2, v0, Lcom/policydm/eng/core/XDMWorkspace;->serverAuthState:I

    const/4 v3, -0x7

    if-eq v2, v3, :cond_8

    iget v2, v0, Lcom/policydm/eng/core/XDMWorkspace;->serverAuthState:I

    if-ne v2, v4, :cond_9

    .line 4853
    :cond_8
    const/4 v2, -0x1

    iput v2, v0, Lcom/policydm/eng/core/XDMWorkspace;->serverAuthState:I

    goto :goto_2

    .line 4858
    :cond_9
    iput v4, v0, Lcom/policydm/eng/core/XDMWorkspace;->serverAuthState:I

    goto :goto_2

    .line 4864
    :cond_a
    invoke-virtual {p0, p1}, Lcom/policydm/agent/XDMAgent;->xdmAgentVerifyServerAuth(Lcom/policydm/eng/parser/XDMParserSyncheader;)I

    move-result v2

    iput v2, v0, Lcom/policydm/eng/core/XDMWorkspace;->serverAuthState:I

    goto :goto_2

    .line 4876
    :cond_b
    const-string v2, "212"

    iput-object v2, v0, Lcom/policydm/eng/core/XDMWorkspace;->m_szStatusReturnCode:Ljava/lang/String;

    goto :goto_3

    .line 4881
    :cond_c
    iget v2, v0, Lcom/policydm/eng/core/XDMWorkspace;->serverAuthState:I

    if-ne v2, v4, :cond_d

    .line 4883
    const-string v2, "407"

    iput-object v2, v0, Lcom/policydm/eng/core/XDMWorkspace;->m_szStatusReturnCode:Ljava/lang/String;

    goto :goto_4

    .line 4887
    :cond_d
    const-string v2, "401"

    iput-object v2, v0, Lcom/policydm/eng/core/XDMWorkspace;->m_szStatusReturnCode:Ljava/lang/String;

    goto :goto_4
.end method

.method public xdmAgentCmdUicAlert()Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 3058
    sget-object v0, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    .line 3059
    .local v0, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    const/4 v6, 0x0

    .line 3061
    .local v6, "status":Lcom/policydm/eng/parser/XDMParserStatus;
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->uicAlert:Lcom/policydm/eng/parser/XDMParserAlert;

    if-eqz v1, :cond_2

    .line 3063
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->uicFlag:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    sget-object v2, Lcom/policydm/interfaces/XUICInterface$XUICFlag;->XUIC_TRUE:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    if-eq v1, v2, :cond_0

    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->uicFlag:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    sget-object v2, Lcom/policydm/interfaces/XUICInterface$XUICFlag;->XUIC_NONE:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    if-ne v1, v2, :cond_4

    .line 3065
    :cond_0
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->uicAlert:Lcom/policydm/eng/parser/XDMParserAlert;

    iget v1, v1, Lcom/policydm/eng/parser/XDMParserAlert;->cmdid:I

    const-string v2, "Alert"

    const-string v5, "200"

    move-object v4, v3

    invoke-static/range {v0 .. v5}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v6

    .line 3066
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->uicData:Lcom/policydm/eng/core/XDMList;

    if-eqz v1, :cond_1

    .line 3068
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->uicData:Lcom/policydm/eng/core/XDMList;

    iput-object v1, v6, Lcom/policydm/eng/parser/XDMParserStatus;->itemlist:Lcom/policydm/eng/core/XDMList;

    .line 3095
    :cond_1
    :goto_0
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v1, v6}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 3096
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->uicAlert:Lcom/policydm/eng/parser/XDMParserAlert;

    invoke-static {v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDeleteAlert(Ljava/lang/Object;)V

    .line 3097
    iput-object v3, v0, Lcom/policydm/eng/core/XDMWorkspace;->uicData:Lcom/policydm/eng/core/XDMList;

    .line 3098
    iput-object v3, v0, Lcom/policydm/eng/core/XDMWorkspace;->uicAlert:Lcom/policydm/eng/parser/XDMParserAlert;

    .line 3099
    const/4 v6, 0x0

    .line 3102
    :cond_2
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->uicFlag:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    sget-object v2, Lcom/policydm/interfaces/XUICInterface$XUICFlag;->XUIC_TRUE:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    if-eq v1, v2, :cond_3

    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->uicFlag:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    sget-object v2, Lcom/policydm/interfaces/XUICInterface$XUICFlag;->XUIC_NONE:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    if-ne v1, v2, :cond_7

    .line 3104
    :cond_3
    const/4 v1, 0x1

    .line 3108
    :goto_1
    return v1

    .line 3071
    :cond_4
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->uicFlag:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    sget-object v2, Lcom/policydm/interfaces/XUICInterface$XUICFlag;->XUIC_FALSE:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    if-ne v1, v2, :cond_5

    .line 3073
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->uicAlert:Lcom/policydm/eng/parser/XDMParserAlert;

    iget v1, v1, Lcom/policydm/eng/parser/XDMParserAlert;->cmdid:I

    const-string v2, "Alert"

    const-string v5, "304"

    move-object v4, v3

    invoke-static/range {v0 .. v5}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v6

    .line 3074
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->uicData:Lcom/policydm/eng/core/XDMList;

    if-eqz v1, :cond_1

    .line 3076
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->uicData:Lcom/policydm/eng/core/XDMList;

    iput-object v1, v6, Lcom/policydm/eng/parser/XDMParserStatus;->itemlist:Lcom/policydm/eng/core/XDMList;

    goto :goto_0

    .line 3079
    :cond_5
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->uicFlag:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    sget-object v2, Lcom/policydm/interfaces/XUICInterface$XUICFlag;->XUIC_CANCELED:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    if-ne v1, v2, :cond_6

    .line 3081
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->uicAlert:Lcom/policydm/eng/parser/XDMParserAlert;

    iget v1, v1, Lcom/policydm/eng/parser/XDMParserAlert;->cmdid:I

    const-string v2, "Alert"

    const-string v5, "214"

    move-object v4, v3

    invoke-static/range {v0 .. v5}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v6

    .line 3082
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->uicData:Lcom/policydm/eng/core/XDMList;

    if-eqz v1, :cond_1

    .line 3084
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->uicData:Lcom/policydm/eng/core/XDMList;

    iput-object v1, v6, Lcom/policydm/eng/parser/XDMParserStatus;->itemlist:Lcom/policydm/eng/core/XDMList;

    goto :goto_0

    .line 3089
    :cond_6
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->uicAlert:Lcom/policydm/eng/parser/XDMParserAlert;

    iget v1, v1, Lcom/policydm/eng/parser/XDMParserAlert;->cmdid:I

    const-string v2, "Alert"

    const-string v5, "215"

    move-object v4, v3

    invoke-static/range {v0 .. v5}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdStatus(Lcom/policydm/eng/core/XDMWorkspace;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserStatus;

    move-result-object v6

    .line 3090
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->uicData:Lcom/policydm/eng/core/XDMList;

    if-eqz v1, :cond_1

    .line 3092
    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->uicData:Lcom/policydm/eng/core/XDMList;

    iput-object v1, v6, Lcom/policydm/eng/parser/XDMParserStatus;->itemlist:Lcom/policydm/eng/core/XDMList;

    goto :goto_0

    .line 3108
    :cond_7
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public xdmAgentCreatePackage()I
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, -0x3

    const/4 v6, -0x1

    const/4 v9, 0x0

    .line 674
    const/16 v0, 0x6a

    .line 676
    .local v0, "WBXML_CHARSET_UTF8":I
    sget-object v5, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    .line 678
    .local v5, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    const/4 v3, 0x0

    .line 679
    .local v3, "res":I
    iget-object v1, v5, Lcom/policydm/eng/core/XDMWorkspace;->e:Lcom/policydm/eng/core/XDMEncoder;

    .line 681
    .local v1, "e":Lcom/policydm/eng/core/XDMEncoder;
    iget-object v7, v5, Lcom/policydm/eng/core/XDMWorkspace;->dmState:Lcom/policydm/interfaces/XDMInterface$XDMSyncMLState;

    sget-object v8, Lcom/policydm/interfaces/XDMInterface$XDMSyncMLState;->XDM_STATE_INIT:Lcom/policydm/interfaces/XDMInterface$XDMSyncMLState;

    if-ne v7, v8, :cond_2

    .line 683
    invoke-virtual {p0}, Lcom/policydm/agent/XDMAgent;->xdmAgentLoadWorkSpace()I

    move-result v3

    .line 684
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMOInitiatedType()I

    move-result v7

    if-eqz v7, :cond_1

    .line 685
    sget-object v7, Lcom/policydm/interfaces/XDMInterface$XDMSyncMLState;->XDM_STATE_GENERIC_ALERT:Lcom/policydm/interfaces/XDMInterface$XDMSyncMLState;

    iput-object v7, v5, Lcom/policydm/eng/core/XDMWorkspace;->dmState:Lcom/policydm/interfaces/XDMInterface$XDMSyncMLState;

    .line 693
    :cond_0
    :goto_0
    if-eqz v3, :cond_4

    .line 695
    const-string v7, "xdmAgentCreatePackage failed"

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 837
    :goto_1
    return v6

    .line 687
    :cond_1
    sget-object v7, Lcom/policydm/interfaces/XDMInterface$XDMSyncMLState;->XDM_STATE_CLIENT_INIT_MGMT:Lcom/policydm/interfaces/XDMInterface$XDMSyncMLState;

    iput-object v7, v5, Lcom/policydm/eng/core/XDMWorkspace;->dmState:Lcom/policydm/interfaces/XDMInterface$XDMSyncMLState;

    goto :goto_0

    .line 689
    :cond_2
    iget-object v7, v5, Lcom/policydm/eng/core/XDMWorkspace;->dmState:Lcom/policydm/interfaces/XDMInterface$XDMSyncMLState;

    sget-object v8, Lcom/policydm/interfaces/XDMInterface$XDMSyncMLState;->XDM_STATE_GENERIC_ALERT_REPORT:Lcom/policydm/interfaces/XDMInterface$XDMSyncMLState;

    if-eq v7, v8, :cond_3

    iget-object v7, v5, Lcom/policydm/eng/core/XDMWorkspace;->dmState:Lcom/policydm/interfaces/XDMInterface$XDMSyncMLState;

    sget-object v8, Lcom/policydm/interfaces/XDMInterface$XDMSyncMLState;->XDM_STATE_ABORT_ALERT:Lcom/policydm/interfaces/XDMInterface$XDMSyncMLState;

    if-ne v7, v8, :cond_0

    .line 691
    :cond_3
    invoke-virtual {p0}, Lcom/policydm/agent/XDMAgent;->xdmAgentLoadWorkSpace()I

    move-result v3

    goto :goto_0

    .line 699
    :cond_4
    iget-object v7, v5, Lcom/policydm/eng/core/XDMWorkspace;->buf:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 700
    iget-object v7, v5, Lcom/policydm/eng/core/XDMWorkspace;->buf:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1, v7}, Lcom/policydm/eng/core/XDMEncoder;->xdmEncInit(Ljava/io/ByteArrayOutputStream;)V

    .line 702
    const-string v4, "-//SYNCML//DTD SyncML 1.2//EN"

    .line 706
    .local v4, "szWbxmlStrTbl":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v1, v9, v0, v4, v7}, Lcom/policydm/eng/core/XDMEncoder;->xdmEncStartSyncml(IILjava/lang/String;I)I

    .line 707
    invoke-static {v5}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdSyncHeader(Lcom/policydm/eng/core/XDMWorkspace;)Lcom/policydm/eng/core/XDMWorkspace;

    move-result-object v5

    .line 708
    iget-object v7, v5, Lcom/policydm/eng/core/XDMWorkspace;->syncHeader:Lcom/policydm/eng/parser/XDMParserSyncheader;

    invoke-virtual {v1, v7}, Lcom/policydm/eng/core/XDMEncoder;->xdmEncAddSyncHeader(Lcom/policydm/eng/parser/XDMParserSyncheader;)I

    .line 709
    invoke-virtual {v1}, Lcom/policydm/eng/core/XDMEncoder;->xdmEncStartSyncbody()I

    .line 711
    sget-object v7, Lcom/policydm/agent/XDMAgent$1;->$SwitchMap$com$policydm$interfaces$XDMInterface$XDMSyncMLState:[I

    iget-object v8, v5, Lcom/policydm/eng/core/XDMWorkspace;->dmState:Lcom/policydm/interfaces/XDMInterface$XDMSyncMLState;

    invoke-virtual {v8}, Lcom/policydm/interfaces/XDMInterface$XDMSyncMLState;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_0

    .line 829
    :goto_2
    iget-boolean v6, v5, Lcom/policydm/eng/core/XDMWorkspace;->dataBuffered:Z

    if-nez v6, :cond_5

    iget-boolean v6, v5, Lcom/policydm/eng/core/XDMWorkspace;->sendRemain:Z

    if-eqz v6, :cond_6

    .line 831
    :cond_5
    iput-boolean v9, v5, Lcom/policydm/eng/core/XDMWorkspace;->endOfMsg:Z

    .line 834
    :cond_6
    iget-boolean v6, v5, Lcom/policydm/eng/core/XDMWorkspace;->endOfMsg:Z

    invoke-virtual {v1, v6}, Lcom/policydm/eng/core/XDMEncoder;->xdmEncEndSyncbody(Z)I

    .line 835
    invoke-virtual {v1}, Lcom/policydm/eng/core/XDMEncoder;->xdmEncEndSyncml()I

    move v6, v3

    .line 837
    goto :goto_1

    .line 714
    :pswitch_0
    const-string v6, "XDM_STATE_CLIENT_INIT_MGMT"

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 715
    invoke-virtual {p0, v1}, Lcom/policydm/agent/XDMAgent;->xdmAgentClientInitPackage(Lcom/policydm/eng/core/XDMEncoder;)I

    goto :goto_2

    .line 718
    :pswitch_1
    const-string v6, "XDM_STATE_PROCESSING"

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 719
    invoke-virtual {p0, v1}, Lcom/policydm/agent/XDMAgent;->xdmAgentMgmtPackage(Lcom/policydm/eng/core/XDMEncoder;)I

    goto :goto_2

    .line 722
    :pswitch_2
    const-string v7, "XDM_STATE_GENERIC_ALERT"

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 723
    invoke-virtual {p0, v1}, Lcom/policydm/agent/XDMAgent;->xdmAgentClientInitPackage(Lcom/policydm/eng/core/XDMEncoder;)I

    move-result v3

    .line 725
    if-eqz v3, :cond_8

    .line 727
    if-ne v3, v10, :cond_7

    .line 729
    iput-boolean v9, v5, Lcom/policydm/eng/core/XDMWorkspace;->endOfMsg:Z

    goto :goto_1

    .line 733
    :cond_7
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "failed(%d)"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 738
    :cond_8
    const-string v7, "1226"

    invoke-virtual {p0, v1, v7}, Lcom/policydm/agent/XDMAgent;->xdmAgentCreatePackageGenericAlert(Lcom/policydm/eng/core/XDMEncoder;Ljava/lang/String;)I

    move-result v3

    .line 739
    if-eqz v3, :cond_a

    .line 741
    if-ne v3, v10, :cond_9

    .line 743
    iput-boolean v9, v5, Lcom/policydm/eng/core/XDMWorkspace;->endOfMsg:Z

    goto/16 :goto_1

    .line 747
    :cond_9
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "failed(%d)"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 751
    :cond_a
    iput-boolean v11, v5, Lcom/policydm/eng/core/XDMWorkspace;->endOfMsg:Z

    goto :goto_2

    .line 754
    :pswitch_3
    const-string v7, "XDM_STATE_GENERIC_ALERT_REPORT"

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 755
    invoke-virtual {p0, v1}, Lcom/policydm/agent/XDMAgent;->xdmAgentClientInitPackage(Lcom/policydm/eng/core/XDMEncoder;)I

    move-result v3

    .line 757
    if-eqz v3, :cond_c

    .line 759
    if-ne v3, v10, :cond_b

    .line 761
    iput-boolean v9, v5, Lcom/policydm/eng/core/XDMWorkspace;->endOfMsg:Z

    goto/16 :goto_1

    .line 765
    :cond_b
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "failed(%d)"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 770
    :cond_c
    const-string v7, "1226"

    invoke-virtual {p0, v1, v7}, Lcom/policydm/agent/XDMAgent;->xdmAgentCreatePackageReportGenericAlert(Lcom/policydm/eng/core/XDMEncoder;Ljava/lang/String;)I

    move-result v3

    .line 771
    if-eqz v3, :cond_e

    .line 773
    if-ne v3, v10, :cond_d

    .line 775
    iput-boolean v9, v5, Lcom/policydm/eng/core/XDMWorkspace;->endOfMsg:Z

    goto/16 :goto_1

    .line 779
    :cond_d
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "failed(%d)"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 783
    :cond_e
    iput-boolean v11, v5, Lcom/policydm/eng/core/XDMWorkspace;->endOfMsg:Z

    goto/16 :goto_2

    .line 786
    :pswitch_4
    const-string v7, "XDM_STATE_ABORT_ALERT"

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 787
    invoke-static {}, Lcom/policydm/db/XDBProfileListAdp;->xdbGetNotiEvent()I

    move-result v2

    .line 788
    .local v2, "nNotiEvent":I
    if-lez v2, :cond_f

    .line 790
    const-string v7, "1200"

    invoke-virtual {p0, v1, v7}, Lcom/policydm/agent/XDMAgent;->xdmAgentCreatePackageAlert(Lcom/policydm/eng/core/XDMEncoder;Ljava/lang/String;)I

    move-result v3

    .line 796
    :goto_3
    if-eqz v3, :cond_11

    .line 798
    if-ne v3, v10, :cond_10

    .line 800
    iput-boolean v9, v5, Lcom/policydm/eng/core/XDMWorkspace;->endOfMsg:Z

    goto/16 :goto_1

    .line 794
    :cond_f
    const-string v7, "1201"

    invoke-virtual {p0, v1, v7}, Lcom/policydm/agent/XDMAgent;->xdmAgentCreatePackageAlert(Lcom/policydm/eng/core/XDMEncoder;Ljava/lang/String;)I

    move-result v3

    goto :goto_3

    .line 804
    :cond_10
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "failed(%d)"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 808
    :cond_11
    const-string v7, "1223"

    invoke-virtual {p0, v1, v7}, Lcom/policydm/agent/XDMAgent;->xdmAgentCreatePackageAlert(Lcom/policydm/eng/core/XDMEncoder;Ljava/lang/String;)I

    move-result v3

    .line 810
    if-eqz v3, :cond_13

    .line 812
    if-ne v3, v10, :cond_12

    .line 814
    iput-boolean v9, v5, Lcom/policydm/eng/core/XDMWorkspace;->endOfMsg:Z

    goto/16 :goto_1

    .line 818
    :cond_12
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "failed(%d)"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 823
    :cond_13
    iput-boolean v11, v5, Lcom/policydm/eng/core/XDMWorkspace;->endOfMsg:Z

    goto/16 :goto_2

    .line 711
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public xdmAgentCreatePackageAlert(Lcom/policydm/eng/core/XDMEncoder;Ljava/lang/String;)I
    .locals 3
    .param p1, "e"    # Lcom/policydm/eng/core/XDMEncoder;
    .param p2, "szData"    # Ljava/lang/String;

    .prologue
    .line 2458
    const/4 v0, 0x0

    .line 2459
    .local v0, "alert":Lcom/policydm/eng/parser/XDMParserAlert;
    sget-object v1, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    .line 2461
    .local v1, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    invoke-static {v1, p2}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdAlert(Lcom/policydm/eng/core/XDMWorkspace;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserAlert;

    move-result-object v0

    .line 2462
    invoke-virtual {p1, v0}, Lcom/policydm/eng/core/XDMEncoder;->xdmEncAddAlert(Lcom/policydm/eng/parser/XDMParserAlert;)I

    .line 2463
    invoke-static {v0}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDeleteAlert(Ljava/lang/Object;)V

    .line 2465
    const/4 v2, 0x0

    return v2
.end method

.method xdmAgentCreatePackageDevInfo(Lcom/policydm/eng/core/XDMEncoder;)I
    .locals 7
    .param p1, "e"    # Lcom/policydm/eng/core/XDMEncoder;

    .prologue
    .line 2470
    sget-object v4, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    .line 2471
    .local v4, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    const/4 v3, 0x0

    .line 2472
    .local v3, "rep":Lcom/policydm/eng/parser/XDMParserReplace;
    const/4 v0, 0x0

    .line 2473
    .local v0, "list":Lcom/policydm/eng/core/XDMLinkedList;
    iget-object v2, v4, Lcom/policydm/eng/core/XDMWorkspace;->om:Lcom/policydm/eng/core/XDMOmTree;

    .line 2476
    .local v2, "om":Lcom/policydm/eng/core/XDMOmTree;
    invoke-static {}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListCreateLinkedList()Lcom/policydm/eng/core/XDMLinkedList;

    move-result-object v0

    .line 2477
    const-string v5, "./DevInfo/Lang"

    invoke-static {v2, v5}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v1

    .line 2478
    .local v1, "node":Lcom/policydm/eng/core/XDMVnode;
    if-eqz v1, :cond_0

    .line 2479
    const-string v5, "./DevInfo/Lang"

    iget v6, v1, Lcom/policydm/eng/core/XDMVnode;->size:I

    invoke-virtual {p0, v2, v0, v5, v6}, Lcom/policydm/agent/XDMAgent;->xdmAgent_MAKE_REP_ITEM(Lcom/policydm/eng/core/XDMOmTree;Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/String;I)V

    .line 2481
    :cond_0
    const-string v5, "./DevInfo/DmV"

    invoke-static {v2, v5}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v1

    .line 2482
    if-eqz v1, :cond_1

    .line 2483
    const-string v5, "./DevInfo/DmV"

    iget v6, v1, Lcom/policydm/eng/core/XDMVnode;->size:I

    invoke-virtual {p0, v2, v0, v5, v6}, Lcom/policydm/agent/XDMAgent;->xdmAgent_MAKE_REP_ITEM(Lcom/policydm/eng/core/XDMOmTree;Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/String;I)V

    .line 2485
    :cond_1
    const-string v5, "./DevInfo/Mod"

    invoke-static {v2, v5}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v1

    .line 2486
    if-eqz v1, :cond_2

    .line 2487
    const-string v5, "./DevInfo/Mod"

    iget v6, v1, Lcom/policydm/eng/core/XDMVnode;->size:I

    invoke-virtual {p0, v2, v0, v5, v6}, Lcom/policydm/agent/XDMAgent;->xdmAgent_MAKE_REP_ITEM(Lcom/policydm/eng/core/XDMOmTree;Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/String;I)V

    .line 2489
    :cond_2
    const-string v5, "./DevInfo/Man"

    invoke-static {v2, v5}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v1

    .line 2490
    if-eqz v1, :cond_3

    .line 2491
    const-string v5, "./DevInfo/Man"

    iget v6, v1, Lcom/policydm/eng/core/XDMVnode;->size:I

    invoke-virtual {p0, v2, v0, v5, v6}, Lcom/policydm/agent/XDMAgent;->xdmAgent_MAKE_REP_ITEM(Lcom/policydm/eng/core/XDMOmTree;Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/String;I)V

    .line 2493
    :cond_3
    const-string v5, "./DevInfo/DevId"

    invoke-static {v2, v5}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v1

    .line 2494
    if-eqz v1, :cond_4

    .line 2495
    const-string v5, "./DevInfo/DevId"

    iget v6, v1, Lcom/policydm/eng/core/XDMVnode;->size:I

    invoke-virtual {p0, v2, v0, v5, v6}, Lcom/policydm/agent/XDMAgent;->xdmAgent_MAKE_REP_ITEM(Lcom/policydm/eng/core/XDMOmTree;Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/String;I)V

    .line 2497
    :cond_4
    const-string v5, "./DevDetail/FwV"

    invoke-static {v2, v5}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v1

    .line 2498
    if-eqz v1, :cond_5

    .line 2499
    const-string v5, "./DevDetail/FwV"

    iget v6, v1, Lcom/policydm/eng/core/XDMVnode;->size:I

    invoke-virtual {p0, v2, v0, v5, v6}, Lcom/policydm/agent/XDMAgent;->xdmAgent_MAKE_REP_ITEM(Lcom/policydm/eng/core/XDMOmTree;Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/String;I)V

    .line 2501
    :cond_5
    const-string v5, "./DevDetail/Ext/PolicyVersion"

    invoke-static {v2, v5}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v1

    .line 2502
    if-eqz v1, :cond_6

    .line 2503
    const-string v5, "./DevDetail/Ext/PolicyVersion"

    iget v6, v1, Lcom/policydm/eng/core/XDMVnode;->size:I

    invoke-virtual {p0, v2, v0, v5, v6}, Lcom/policydm/agent/XDMAgent;->xdmAgent_MAKE_REP_ITEM(Lcom/policydm/eng/core/XDMOmTree;Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/String;I)V

    .line 2505
    :cond_6
    const-string v5, "./DevDetail/Ext/CustCode"

    invoke-static {v2, v5}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v1

    .line 2506
    if-eqz v1, :cond_7

    .line 2507
    const-string v5, "./DevDetail/Ext/CustCode"

    iget v6, v1, Lcom/policydm/eng/core/XDMVnode;->size:I

    invoke-virtual {p0, v2, v0, v5, v6}, Lcom/policydm/agent/XDMAgent;->xdmAgent_MAKE_REP_ITEM(Lcom/policydm/eng/core/XDMOmTree;Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/String;I)V

    .line 2509
    :cond_7
    const-string v5, "./DevDetail/Ext/AndroidV"

    invoke-static {v2, v5}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v1

    .line 2510
    if-eqz v1, :cond_8

    .line 2511
    const-string v5, "./DevDetail/Ext/AndroidV"

    iget v6, v1, Lcom/policydm/eng/core/XDMVnode;->size:I

    invoke-virtual {p0, v2, v0, v5, v6}, Lcom/policydm/agent/XDMAgent;->xdmAgent_MAKE_REP_ITEM(Lcom/policydm/eng/core/XDMOmTree;Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/String;I)V

    .line 2513
    :cond_8
    const-string v5, "./DevDetail/Ext/ClientV"

    invoke-static {v2, v5}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v1

    .line 2514
    if-eqz v1, :cond_9

    .line 2515
    const-string v5, "./DevDetail/Ext/ClientV"

    iget v6, v1, Lcom/policydm/eng/core/XDMVnode;->size:I

    invoke-virtual {p0, v2, v0, v5, v6}, Lcom/policydm/agent/XDMAgent;->xdmAgent_MAKE_REP_ITEM(Lcom/policydm/eng/core/XDMOmTree;Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/String;I)V

    .line 2517
    :cond_9
    const-string v5, "./DevDetail/Ext/MCC"

    invoke-static {v2, v5}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v1

    .line 2518
    if-eqz v1, :cond_a

    .line 2519
    const-string v5, "./DevDetail/Ext/MCC"

    iget v6, v1, Lcom/policydm/eng/core/XDMVnode;->size:I

    invoke-virtual {p0, v2, v0, v5, v6}, Lcom/policydm/agent/XDMAgent;->xdmAgent_MAKE_REP_ITEM(Lcom/policydm/eng/core/XDMOmTree;Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/String;I)V

    .line 2521
    :cond_a
    const-string v5, "./DevDetail/Ext/MNC"

    invoke-static {v2, v5}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v1

    .line 2522
    if-eqz v1, :cond_b

    .line 2523
    const-string v5, "./DevDetail/Ext/MNC"

    iget v6, v1, Lcom/policydm/eng/core/XDMVnode;->size:I

    invoke-virtual {p0, v2, v0, v5, v6}, Lcom/policydm/agent/XDMAgent;->xdmAgent_MAKE_REP_ITEM(Lcom/policydm/eng/core/XDMOmTree;Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/String;I)V

    .line 2525
    :cond_b
    const-string v5, "./DevDetail/Ext/TMCC"

    invoke-static {v2, v5}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v1

    .line 2526
    if-eqz v1, :cond_c

    .line 2527
    const-string v5, "./DevDetail/Ext/TMCC"

    iget v6, v1, Lcom/policydm/eng/core/XDMVnode;->size:I

    invoke-virtual {p0, v2, v0, v5, v6}, Lcom/policydm/agent/XDMAgent;->xdmAgent_MAKE_REP_ITEM(Lcom/policydm/eng/core/XDMOmTree;Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/String;I)V

    .line 2529
    :cond_c
    const-string v5, "./DevDetail/Ext/TMNC"

    invoke-static {v2, v5}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v1

    .line 2530
    if-eqz v1, :cond_d

    .line 2531
    const-string v5, "./DevDetail/Ext/TMNC"

    iget v6, v1, Lcom/policydm/eng/core/XDMVnode;->size:I

    invoke-virtual {p0, v2, v0, v5, v6}, Lcom/policydm/agent/XDMAgent;->xdmAgent_MAKE_REP_ITEM(Lcom/policydm/eng/core/XDMOmTree;Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/String;I)V

    .line 2533
    :cond_d
    const-string v5, "./SEAndroidPolicy/PushJobId"

    invoke-static {v2, v5}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v1

    .line 2534
    if-eqz v1, :cond_e

    .line 2535
    const-string v5, "./SEAndroidPolicy/PushJobId"

    iget v6, v1, Lcom/policydm/eng/core/XDMVnode;->size:I

    invoke-virtual {p0, v2, v0, v5, v6}, Lcom/policydm/agent/XDMAgent;->xdmAgent_MAKE_REP_ITEM(Lcom/policydm/eng/core/XDMOmTree;Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/String;I)V

    .line 2537
    :cond_e
    invoke-static {v4, v0}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdReplace(Lcom/policydm/eng/core/XDMWorkspace;Lcom/policydm/eng/core/XDMLinkedList;)Lcom/policydm/eng/parser/XDMParserReplace;

    move-result-object v3

    .line 2539
    invoke-static {v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListFreeLinkedList(Lcom/policydm/eng/core/XDMLinkedList;)V

    .line 2540
    invoke-virtual {p1, v3}, Lcom/policydm/eng/core/XDMEncoder;->xdmEncAddReplace(Lcom/policydm/eng/parser/XDMParserReplace;)I

    .line 2541
    invoke-static {v3}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDeleteReplace(Ljava/lang/Object;)V

    .line 2543
    const/4 v5, 0x0

    return v5
.end method

.method public xdmAgentCreatePackageGenericAlert(Lcom/policydm/eng/core/XDMEncoder;Ljava/lang/String;)I
    .locals 3
    .param p1, "e"    # Lcom/policydm/eng/core/XDMEncoder;
    .param p2, "szData"    # Ljava/lang/String;

    .prologue
    .line 2563
    sget-object v1, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    .line 2565
    .local v1, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    const-string v2, ""

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 2566
    invoke-static {v1, p2}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdGenericAlert(Lcom/policydm/eng/core/XDMWorkspace;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserAlert;

    move-result-object v0

    .line 2567
    .local v0, "alert":Lcom/policydm/eng/parser/XDMParserAlert;
    invoke-virtual {p1, v0}, Lcom/policydm/eng/core/XDMEncoder;->xdmEncAddAlert(Lcom/policydm/eng/parser/XDMParserAlert;)I

    .line 2568
    invoke-static {v0}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDeleteAlert(Ljava/lang/Object;)V

    .line 2570
    const/4 v2, 0x0

    return v2
.end method

.method public xdmAgentCreatePackageReportGenericAlert(Lcom/policydm/eng/core/XDMEncoder;Ljava/lang/String;)I
    .locals 3
    .param p1, "e"    # Lcom/policydm/eng/core/XDMEncoder;
    .param p2, "szData"    # Ljava/lang/String;

    .prologue
    .line 2576
    sget-object v1, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    .line 2578
    .local v1, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    invoke-static {v1, p2}, Lcom/policydm/agent/XDMBuildCmd;->xdmAgentBuildCmdGenericAlertReport(Lcom/policydm/eng/core/XDMWorkspace;Ljava/lang/String;)Lcom/policydm/eng/parser/XDMParserAlert;

    move-result-object v0

    .line 2579
    .local v0, "alert":Lcom/policydm/eng/parser/XDMParserAlert;
    invoke-virtual {p1, v0}, Lcom/policydm/eng/core/XDMEncoder;->xdmEncAddAlert(Lcom/policydm/eng/parser/XDMParserAlert;)I

    .line 2580
    invoke-static {v0}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDeleteAlert(Ljava/lang/Object;)V

    .line 2582
    const/4 v2, 0x0

    return v2
.end method

.method public xdmAgentCreatePackageResults(Lcom/policydm/eng/core/XDMEncoder;)I
    .locals 22
    .param p1, "e"    # Lcom/policydm/eng/core/XDMEncoder;

    .prologue
    .line 2319
    sget-object v19, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    .line 2320
    .local v19, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    sget-object v20, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    move-object/from16 v0, v20

    iget-object v11, v0, Lcom/policydm/eng/core/XDMWorkspace;->om:Lcom/policydm/eng/core/XDMOmTree;

    .line 2321
    .local v11, "om":Lcom/policydm/eng/core/XDMOmTree;
    const/4 v15, 0x0

    .line 2327
    .local v15, "results":Lcom/policydm/eng/parser/XDMParserResults;
    const/4 v13, 0x0

    .line 2328
    .local v13, "partialsize":I
    const/4 v10, 0x0

    .line 2329
    .local v10, "offset":I
    const/4 v12, 0x0

    .line 2330
    .local v12, "partialsend":Z
    const/4 v2, 0x0

    .line 2332
    .local v2, "buf":[C
    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/policydm/eng/core/XDMWorkspace;->resultsList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-static/range {v20 .. v21}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListSetCurrentObj(Lcom/policydm/eng/core/XDMLinkedList;I)V

    .line 2333
    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/policydm/eng/core/XDMWorkspace;->resultsList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/policydm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v15

    .end local v15    # "results":Lcom/policydm/eng/parser/XDMParserResults;
    check-cast v15, Lcom/policydm/eng/parser/XDMParserResults;

    .line 2335
    .restart local v15    # "results":Lcom/policydm/eng/parser/XDMParserResults;
    if-nez v15, :cond_1

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/policydm/eng/core/XDMWorkspace;->dmState:Lcom/policydm/interfaces/XDMInterface$XDMSyncMLState;

    move-object/from16 v20, v0

    sget-object v21, Lcom/policydm/interfaces/XDMInterface$XDMSyncMLState;->XDM_STATE_CLIENT_INIT_MGMT:Lcom/policydm/interfaces/XDMInterface$XDMSyncMLState;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    if-eq v0, v1, :cond_1

    move-object/from16 v0, v19

    iget-boolean v0, v0, Lcom/policydm/eng/core/XDMWorkspace;->nextMsg:Z

    move/from16 v20, v0

    if-eqz v20, :cond_1

    .line 2337
    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/policydm/eng/core/XDMWorkspace;->resultsList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListClearLinkedList(Lcom/policydm/eng/core/XDMLinkedList;)V

    .line 2338
    const/16 v20, 0x0

    .line 2453
    :goto_0
    return v20

    .line 2367
    .local v4, "cmdsize":I
    .local v5, "datasize":I
    .local v7, "item":Lcom/policydm/eng/parser/XDMParserItem;
    .local v9, "node":Lcom/policydm/eng/core/XDMVnode;
    .local v14, "remainsize":I
    .local v18, "usedsize":I
    :cond_0
    if-lez v5, :cond_4

    if-ge v14, v4, :cond_4

    .line 2369
    move v13, v5

    .line 2376
    :goto_1
    if-lez v13, :cond_8

    .line 2378
    new-array v2, v13, [C

    .line 2379
    iget-object v0, v7, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    move-object/from16 v20, v0

    if-eqz v20, :cond_6

    .line 2381
    iget-object v0, v7, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/policydm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v20

    if-nez v20, :cond_6

    .line 2385
    const-string v20, "application/vnd.syncml.dmtnds+xml"

    iget-object v0, v7, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/policydm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v20

    if-nez v20, :cond_6

    .line 2387
    const/4 v8, 0x0

    .line 2388
    .local v8, "nFileId":I
    new-array v3, v13, [B

    .line 2389
    .local v3, "buftmp":[B
    invoke-static {}, Lcom/policydm/db/XDB;->xdbGetFileIdTNDS()I

    move-result v8

    .line 2390
    invoke-static {v8, v10, v13, v3}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpReadFile(III[B)Z

    .line 2391
    new-instance v16, Ljava/lang/String;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v20

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-direct {v0, v3, v1}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 2392
    .local v16, "szData1":Ljava/lang/String;
    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->toCharArray()[C

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStString2Pcdata([C)Lcom/policydm/eng/parser/XDMParserPcdata;

    move-result-object v20

    move-object/from16 v0, v20

    iput-object v0, v7, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    .line 2393
    if-eqz v12, :cond_5

    .line 2395
    const/16 v20, 0x1

    move/from16 v0, v20

    iput v0, v7, Lcom/policydm/eng/parser/XDMParserItem;->moredata:I

    .line 2396
    move-object/from16 v0, v19

    iget v0, v0, Lcom/policydm/eng/core/XDMWorkspace;->sendPos:I

    move/from16 v20, v0

    add-int v20, v20, v13

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput v0, v1, Lcom/policydm/eng/core/XDMWorkspace;->sendPos:I

    .line 2403
    :goto_2
    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lcom/policydm/eng/core/XDMEncoder;->xdmEncAddResults(Lcom/policydm/eng/parser/XDMParserResults;)I

    .line 2404
    move-object/from16 v17, v15

    .line 2405
    .local v17, "tmp":Lcom/policydm/eng/parser/XDMParserResults;
    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/policydm/eng/core/XDMWorkspace;->resultsList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/policydm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v15

    .end local v15    # "results":Lcom/policydm/eng/parser/XDMParserResults;
    check-cast v15, Lcom/policydm/eng/parser/XDMParserResults;

    .line 2406
    .restart local v15    # "results":Lcom/policydm/eng/parser/XDMParserResults;
    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/policydm/eng/core/XDMWorkspace;->resultsList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListRemoveObjAtFirst(Lcom/policydm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    .line 2407
    invoke-static/range {v17 .. v17}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDeleteResults(Ljava/lang/Object;)V

    .line 2409
    const/4 v2, 0x0

    .line 2340
    .end local v3    # "buftmp":[B
    .end local v4    # "cmdsize":I
    .end local v5    # "datasize":I
    .end local v7    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    .end local v8    # "nFileId":I
    .end local v9    # "node":Lcom/policydm/eng/core/XDMVnode;
    .end local v14    # "remainsize":I
    .end local v16    # "szData1":Ljava/lang/String;
    .end local v17    # "tmp":Lcom/policydm/eng/parser/XDMParserResults;
    .end local v18    # "usedsize":I
    :cond_1
    :goto_3
    if-eqz v15, :cond_b

    .line 2342
    iget-object v0, v15, Lcom/policydm/eng/parser/XDMParserResults;->itemlist:Lcom/policydm/eng/core/XDMList;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v7, v0, Lcom/policydm/eng/core/XDMList;->item:Ljava/lang/Object;

    check-cast v7, Lcom/policydm/eng/parser/XDMParserItem;

    .line 2343
    .restart local v7    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    iget-object v0, v7, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    move-object/from16 v20, v0

    if-eqz v20, :cond_3

    .line 2345
    iget-object v0, v7, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/policydm/eng/parser/XDMParserMeta;->size:I

    move/from16 v20, v0

    if-lez v20, :cond_2

    .line 2347
    iget-object v0, v7, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/policydm/eng/parser/XDMParserMeta;->size:I

    move/from16 v20, v0

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 2358
    .restart local v5    # "datasize":I
    :goto_4
    iget-object v0, v7, Lcom/policydm/eng/parser/XDMParserItem;->m_szSource:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-static {v11, v0}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v9

    .line 2359
    .restart local v9    # "node":Lcom/policydm/eng/core/XDMVnode;
    invoke-static/range {p1 .. p1}, Lcom/policydm/eng/core/XDMEncoder;->xdmEncGetBufferSize(Lcom/policydm/eng/core/XDMEncoder;)I

    move-result v18

    .line 2360
    .restart local v18    # "usedsize":I
    add-int/lit16 v4, v5, 0x80

    .line 2361
    .restart local v4    # "cmdsize":I
    move-object/from16 v0, v19

    iget v0, v0, Lcom/policydm/eng/core/XDMWorkspace;->maxMsgSize:I

    move/from16 v20, v0

    sub-int v14, v20, v18

    .line 2363
    .restart local v14    # "remainsize":I
    const/16 v20, 0x80

    move/from16 v0, v20

    if-ge v14, v0, :cond_0

    .line 2365
    const/16 v20, -0x3

    goto/16 :goto_0

    .line 2351
    .end local v4    # "cmdsize":I
    .end local v5    # "datasize":I
    .end local v9    # "node":Lcom/policydm/eng/core/XDMVnode;
    .end local v14    # "remainsize":I
    .end local v18    # "usedsize":I
    :cond_2
    const/4 v5, 0x0

    .restart local v5    # "datasize":I
    goto :goto_4

    .line 2356
    .end local v5    # "datasize":I
    :cond_3
    const/4 v5, 0x0

    .restart local v5    # "datasize":I
    goto :goto_4

    .line 2373
    .restart local v4    # "cmdsize":I
    .restart local v9    # "node":Lcom/policydm/eng/core/XDMVnode;
    .restart local v14    # "remainsize":I
    .restart local v18    # "usedsize":I
    :cond_4
    const/4 v13, 0x0

    goto/16 :goto_1

    .line 2400
    .restart local v3    # "buftmp":[B
    .restart local v8    # "nFileId":I
    .restart local v16    # "szData1":Ljava/lang/String;
    :cond_5
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput v0, v1, Lcom/policydm/eng/core/XDMWorkspace;->sendPos:I

    goto/16 :goto_2

    .line 2415
    .end local v3    # "buftmp":[B
    .end local v8    # "nFileId":I
    .end local v16    # "szData1":Ljava/lang/String;
    :cond_6
    iget-object v0, v7, Lcom/policydm/eng/parser/XDMParserItem;->m_szSource:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-static {v11, v0, v10, v2, v13}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmRead(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 2417
    if-eqz v9, :cond_9

    iget v0, v9, Lcom/policydm/eng/core/XDMVnode;->format:I

    move/from16 v20, v0

    const/16 v21, 0x2

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_9

    .line 2419
    new-instance v20, Lcom/policydm/eng/parser/XDMParserPcdata;

    invoke-direct/range {v20 .. v20}, Lcom/policydm/eng/parser/XDMParserPcdata;-><init>()V

    move-object/from16 v0, v20

    iput-object v0, v7, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    .line 2420
    iget-object v0, v7, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, v20

    iput v0, v1, Lcom/policydm/eng/parser/XDMParserPcdata;->type:I

    .line 2421
    iget-object v0, v7, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    move-object/from16 v20, v0

    new-array v0, v13, [C

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    iput-object v0, v1, Lcom/policydm/eng/parser/XDMParserPcdata;->data:[C

    .line 2423
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_5
    if-ge v6, v13, :cond_7

    .line 2424
    iget-object v0, v7, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/policydm/eng/parser/XDMParserPcdata;->data:[C

    move-object/from16 v20, v0

    aget-char v21, v2, v6

    aput-char v21, v20, v6

    .line 2423
    add-int/lit8 v6, v6, 0x1

    goto :goto_5

    .line 2425
    :cond_7
    iget-object v0, v7, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iput v13, v0, Lcom/policydm/eng/parser/XDMParserPcdata;->size:I

    .line 2433
    .end local v6    # "i":I
    :cond_8
    :goto_6
    if-eqz v12, :cond_a

    .line 2435
    const/16 v20, 0x1

    move/from16 v0, v20

    iput v0, v7, Lcom/policydm/eng/parser/XDMParserItem;->moredata:I

    .line 2436
    move-object/from16 v0, v19

    iget v0, v0, Lcom/policydm/eng/core/XDMWorkspace;->sendPos:I

    move/from16 v20, v0

    add-int v20, v20, v13

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput v0, v1, Lcom/policydm/eng/core/XDMWorkspace;->sendPos:I

    .line 2443
    :goto_7
    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lcom/policydm/eng/core/XDMEncoder;->xdmEncAddResults(Lcom/policydm/eng/parser/XDMParserResults;)I

    .line 2444
    move-object/from16 v17, v15

    .line 2445
    .restart local v17    # "tmp":Lcom/policydm/eng/parser/XDMParserResults;
    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/policydm/eng/core/XDMWorkspace;->resultsList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/policydm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v15

    .end local v15    # "results":Lcom/policydm/eng/parser/XDMParserResults;
    check-cast v15, Lcom/policydm/eng/parser/XDMParserResults;

    .line 2446
    .restart local v15    # "results":Lcom/policydm/eng/parser/XDMParserResults;
    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/policydm/eng/core/XDMWorkspace;->resultsList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListRemoveObjAtFirst(Lcom/policydm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    .line 2447
    invoke-static/range {v17 .. v17}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDeleteResults(Ljava/lang/Object;)V

    .line 2449
    const/4 v2, 0x0

    goto/16 :goto_3

    .line 2429
    .end local v17    # "tmp":Lcom/policydm/eng/parser/XDMParserResults;
    :cond_9
    invoke-static {v2}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStString2Pcdata([C)Lcom/policydm/eng/parser/XDMParserPcdata;

    move-result-object v20

    move-object/from16 v0, v20

    iput-object v0, v7, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    goto :goto_6

    .line 2440
    :cond_a
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput v0, v1, Lcom/policydm/eng/core/XDMWorkspace;->sendPos:I

    goto :goto_7

    .line 2451
    .end local v4    # "cmdsize":I
    .end local v5    # "datasize":I
    .end local v7    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    .end local v9    # "node":Lcom/policydm/eng/core/XDMVnode;
    .end local v14    # "remainsize":I
    .end local v18    # "usedsize":I
    :cond_b
    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/policydm/eng/core/XDMWorkspace;->resultsList:Lcom/policydm/eng/core/XDMLinkedList;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListClearLinkedList(Lcom/policydm/eng/core/XDMLinkedList;)V

    .line 2453
    const/16 v20, 0x0

    goto/16 :goto_0
.end method

.method public xdmAgentCreatePackageStatus(Lcom/policydm/eng/core/XDMEncoder;)I
    .locals 5
    .param p1, "e"    # Lcom/policydm/eng/core/XDMEncoder;

    .prologue
    const/4 v4, 0x0

    .line 2298
    const/4 v0, 0x0

    .line 2300
    .local v0, "status":Lcom/policydm/eng/parser/XDMParserStatus;
    sget-object v2, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    .line 2301
    .local v2, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    iget-object v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v3, v4}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListSetCurrentObj(Lcom/policydm/eng/core/XDMLinkedList;I)V

    .line 2303
    iget-object v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v3}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/policydm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    check-cast v0, Lcom/policydm/eng/parser/XDMParserStatus;

    .line 2304
    .restart local v0    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    :goto_0
    if-eqz v0, :cond_0

    .line 2306
    invoke-virtual {p1, v0}, Lcom/policydm/eng/core/XDMEncoder;->xdmEncAddStatus(Lcom/policydm/eng/parser/XDMParserStatus;)I

    .line 2307
    move-object v1, v0

    .line 2308
    .local v1, "tmp":Lcom/policydm/eng/parser/XDMParserStatus;
    iget-object v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v3}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/policydm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    check-cast v0, Lcom/policydm/eng/parser/XDMParserStatus;

    .line 2309
    .restart local v0    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    iget-object v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v3}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListRemoveObjAtFirst(Lcom/policydm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    .line 2310
    invoke-static {v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDeleteStatus(Ljava/lang/Object;)V

    goto :goto_0

    .line 2312
    .end local v1    # "tmp":Lcom/policydm/eng/parser/XDMParserStatus;
    :cond_0
    iget-object v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v3}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListClearLinkedList(Lcom/policydm/eng/core/XDMLinkedList;)V

    .line 2314
    return v4
.end method

.method public xdmAgentGetAccountFromOM(Lcom/policydm/eng/core/XDMOmTree;)I
    .locals 14
    .param p1, "om"    # Lcom/policydm/eng/core/XDMOmTree;

    .prologue
    .line 6127
    const/4 v7, 0x0

    .line 6128
    .local v7, "pProfileInfo":Lcom/policydm/db/XDBProfileInfo;
    const/4 v6, 0x0

    .line 6131
    .local v6, "node":Lcom/policydm/eng/core/XDMVnode;
    const/4 v1, 0x0

    .line 6132
    .local v1, "TmpBuf":[C
    const/4 v10, 0x0

    .line 6133
    .local v10, "szAddrURI":Ljava/lang/String;
    const/4 v11, 0x0

    .line 6134
    .local v11, "szPortNum":Ljava/lang/String;
    const/4 v9, 0x0

    .line 6135
    .local v9, "szAccTmpBuf":Ljava/lang/String;
    const/16 v12, 0x100

    new-array v0, v12, [C

    .line 6137
    .local v0, "ServerUrl":[C
    const/4 v3, 0x0

    .line 6140
    .local v3, "authType":I
    new-instance v7, Lcom/policydm/db/XDBProfileInfo;

    .end local v7    # "pProfileInfo":Lcom/policydm/db/XDBProfileInfo;
    invoke-direct {v7}, Lcom/policydm/db/XDBProfileInfo;-><init>()V

    .line 6142
    .restart local v7    # "pProfileInfo":Lcom/policydm/db/XDBProfileInfo;
    sget-object v12, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v12, v12, Lcom/policydm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 6144
    invoke-static {}, Lcom/policydm/agent/XDMAgent;->xdmAgentClose()I

    .line 6145
    const/4 v12, -0x1

    .line 6497
    :goto_0
    return v12

    .line 6147
    :cond_0
    sget-object v12, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v12, v12, Lcom/policydm/eng/core/XDMAccXNode;->m_szAppAddr:Ljava/lang/String;

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 6149
    invoke-static {}, Lcom/policydm/agent/XDMAgent;->xdmAgentClose()I

    .line 6150
    const/4 v12, -0x1

    goto :goto_0

    .line 6152
    :cond_1
    sget-object v12, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v12, v12, Lcom/policydm/eng/core/XDMAccXNode;->m_szAppAddrPort:Ljava/lang/String;

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 6154
    invoke-static {}, Lcom/policydm/agent/XDMAgent;->xdmAgentClose()I

    .line 6155
    const/4 v12, -0x1

    goto :goto_0

    .line 6157
    :cond_2
    sget-object v12, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v12, v12, Lcom/policydm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 6159
    invoke-static {}, Lcom/policydm/agent/XDMAgent;->xdmAgentClose()I

    .line 6160
    const/4 v12, -0x1

    goto :goto_0

    .line 6162
    :cond_3
    sget-object v12, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v12, v12, Lcom/policydm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 6164
    invoke-static {}, Lcom/policydm/agent/XDMAgent;->xdmAgentClose()I

    .line 6165
    const/4 v12, -0x1

    goto :goto_0

    .line 6167
    :cond_4
    sget-object v12, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v12, v12, Lcom/policydm/eng/core/XDMAccXNode;->m_szToConRef:Ljava/lang/String;

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 6169
    invoke-static {}, Lcom/policydm/agent/XDMAgent;->xdmAgentClose()I

    .line 6170
    const/4 v12, -0x1

    goto :goto_0

    .line 6173
    :cond_5
    sget-object v12, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v9, v12, Lcom/policydm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 6174
    const-string v12, "/AppId"

    invoke-virtual {v9, v12}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 6175
    invoke-static {p1, v9}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v6

    .line 6176
    if-eqz v6, :cond_6

    .line 6178
    iget v8, v6, Lcom/policydm/eng/core/XDMVnode;->size:I

    .line 6184
    .local v8, "size":I
    :goto_1
    new-array v1, v8, [C

    .line 6185
    const/4 v12, 0x0

    invoke-static {p1, v9, v12, v1, v8}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmRead(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 6186
    invoke-static {v1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v7, Lcom/policydm/db/XDBProfileInfo;->AppID:Ljava/lang/String;

    .line 6188
    sget-object v12, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v9, v12, Lcom/policydm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 6189
    const-string v12, "/ServerID"

    invoke-virtual {v9, v12}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 6190
    invoke-static {p1, v9}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v6

    .line 6191
    if-eqz v6, :cond_7

    .line 6193
    iget v8, v6, Lcom/policydm/eng/core/XDMVnode;->size:I

    .line 6199
    :goto_2
    new-array v1, v8, [C

    .line 6200
    const/4 v12, 0x0

    invoke-static {p1, v9, v12, v1, v8}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmRead(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 6201
    invoke-static {v1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v7, Lcom/policydm/db/XDBProfileInfo;->ServerID:Ljava/lang/String;

    .line 6202
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "get DM informations from OM...ServerId : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {v1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 6204
    sget-object v12, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v9, v12, Lcom/policydm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 6205
    const-string v12, "/Name"

    invoke-virtual {v9, v12}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 6206
    invoke-static {p1, v9}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v6

    .line 6207
    if-eqz v6, :cond_8

    .line 6209
    iget v12, v6, Lcom/policydm/eng/core/XDMVnode;->size:I

    new-array v1, v12, [C

    .line 6210
    const/4 v12, 0x0

    iget v13, v6, Lcom/policydm/eng/core/XDMVnode;->size:I

    invoke-static {p1, v9, v12, v1, v13}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmRead(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 6211
    invoke-static {v1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v7, Lcom/policydm/db/XDBProfileInfo;->ProfileName:Ljava/lang/String;

    .line 6220
    :goto_3
    sget-object v12, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v9, v12, Lcom/policydm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 6221
    const-string v12, "/PrefConRef"

    invoke-virtual {v9, v12}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 6222
    invoke-static {p1, v9}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v6

    .line 6223
    if-eqz v6, :cond_9

    .line 6225
    iget v8, v6, Lcom/policydm/eng/core/XDMVnode;->size:I

    .line 6231
    :goto_4
    new-array v1, v8, [C

    .line 6232
    const/4 v12, 0x0

    invoke-static {p1, v9, v12, v1, v8}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmRead(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 6233
    invoke-static {v1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v7, Lcom/policydm/db/XDBProfileInfo;->PrefConRef:Ljava/lang/String;

    .line 6235
    sget-object v12, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v9, v12, Lcom/policydm/eng/core/XDMAccXNode;->m_szAppAddr:Ljava/lang/String;

    .line 6236
    const-string v12, "/Addr"

    invoke-virtual {v9, v12}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 6237
    invoke-static {p1, v9}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v6

    .line 6238
    if-eqz v6, :cond_a

    .line 6240
    iget v8, v6, Lcom/policydm/eng/core/XDMVnode;->size:I

    .line 6246
    :goto_5
    new-array v1, v8, [C

    .line 6247
    const/4 v12, 0x0

    invoke-static {p1, v9, v12, v1, v8}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmRead(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 6248
    invoke-static {v1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v10

    .line 6249
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "get DM informations from OM...AddURI : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 6251
    sget-object v12, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v9, v12, Lcom/policydm/eng/core/XDMAccXNode;->m_szAppAddrPort:Ljava/lang/String;

    .line 6252
    const-string v12, "/PortNbr"

    invoke-virtual {v9, v12}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 6253
    invoke-static {p1, v9}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v6

    .line 6254
    if-eqz v6, :cond_b

    .line 6256
    iget v8, v6, Lcom/policydm/eng/core/XDMVnode;->size:I

    .line 6262
    :goto_6
    new-array v1, v8, [C

    .line 6263
    const/4 v12, 0x0

    invoke-static {p1, v9, v12, v1, v8}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmRead(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 6264
    invoke-static {v1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v11

    .line 6266
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v12

    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v13

    invoke-static {v12, v13}, Lcom/policydm/db/XDB;->xdbDoDMBootStrapURI([C[C)[C

    move-result-object v0

    .line 6267
    if-nez v0, :cond_c

    .line 6268
    const/4 v12, -0x1

    goto/16 :goto_0

    .line 6182
    .end local v8    # "size":I
    :cond_6
    const/4 v8, 0x0

    .restart local v8    # "size":I
    goto/16 :goto_1

    .line 6197
    :cond_7
    const/4 v8, 0x0

    goto/16 :goto_2

    .line 6215
    :cond_8
    iget-object v12, v7, Lcom/policydm/db/XDBProfileInfo;->ServerID:Ljava/lang/String;

    iput-object v12, v7, Lcom/policydm/db/XDBProfileInfo;->ProfileName:Ljava/lang/String;

    .line 6216
    const/16 v2, 0x1b

    .line 6217
    .local v2, "aclValue":I
    iget-object v12, v7, Lcom/policydm/db/XDBProfileInfo;->ProfileName:Ljava/lang/String;

    const/4 v13, 0x2

    invoke-static {p1, v9, v12, v2, v13}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    goto/16 :goto_3

    .line 6229
    .end local v2    # "aclValue":I
    :cond_9
    const/4 v8, 0x0

    goto :goto_4

    .line 6244
    :cond_a
    const/4 v8, 0x0

    goto :goto_5

    .line 6260
    :cond_b
    const/4 v8, 0x0

    goto :goto_6

    .line 6269
    :cond_c
    new-instance v12, Ljava/lang/String;

    invoke-direct {v12, v0}, Ljava/lang/String;-><init>([C)V

    iput-object v12, v7, Lcom/policydm/db/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    .line 6270
    iget-object v12, v7, Lcom/policydm/db/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    invoke-static {v12}, Lcom/policydm/tp/XTPHttpUtil;->xtpURLParser(Ljava/lang/String;)Lcom/policydm/db/XDBUrlInfo;

    move-result-object v5

    .line 6271
    .local v5, "getParser":Lcom/policydm/db/XDBUrlInfo;
    iget-object v12, v5, Lcom/policydm/db/XDBUrlInfo;->pURL:Ljava/lang/String;

    iput-object v12, v7, Lcom/policydm/db/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    .line 6272
    iget-object v12, v5, Lcom/policydm/db/XDBUrlInfo;->pAddress:Ljava/lang/String;

    iput-object v12, v7, Lcom/policydm/db/XDBProfileInfo;->ServerIP:Ljava/lang/String;

    .line 6273
    iget-object v12, v5, Lcom/policydm/db/XDBUrlInfo;->pPath:Ljava/lang/String;

    iput-object v12, v7, Lcom/policydm/db/XDBProfileInfo;->ServerPath:Ljava/lang/String;

    .line 6274
    iget v12, v5, Lcom/policydm/db/XDBUrlInfo;->nPort:I

    iput v12, v7, Lcom/policydm/db/XDBProfileInfo;->ServerPort:I

    .line 6275
    iget-object v12, v5, Lcom/policydm/db/XDBUrlInfo;->pProtocol:Ljava/lang/String;

    iput-object v12, v7, Lcom/policydm/db/XDBProfileInfo;->Protocol:Ljava/lang/String;

    .line 6277
    iget-object v12, v7, Lcom/policydm/db/XDBProfileInfo;->ServerUrl:Ljava/lang/String;

    iput-object v12, v7, Lcom/policydm/db/XDBProfileInfo;->ServerUrl_Org:Ljava/lang/String;

    .line 6278
    iget-object v12, v7, Lcom/policydm/db/XDBProfileInfo;->ServerIP:Ljava/lang/String;

    iput-object v12, v7, Lcom/policydm/db/XDBProfileInfo;->ServerIP_Org:Ljava/lang/String;

    .line 6279
    iget-object v12, v7, Lcom/policydm/db/XDBProfileInfo;->ServerPath:Ljava/lang/String;

    iput-object v12, v7, Lcom/policydm/db/XDBProfileInfo;->ServerPath_Org:Ljava/lang/String;

    .line 6280
    iget-object v12, v7, Lcom/policydm/db/XDBProfileInfo;->Protocol:Ljava/lang/String;

    iput-object v12, v7, Lcom/policydm/db/XDBProfileInfo;->Protocol_Org:Ljava/lang/String;

    .line 6281
    iget v12, v7, Lcom/policydm/db/XDBProfileInfo;->ServerPort:I

    iput v12, v7, Lcom/policydm/db/XDBProfileInfo;->ServerPort_Org:I

    .line 6282
    const/4 v12, 0x0

    iput-boolean v12, v7, Lcom/policydm/db/XDBProfileInfo;->bChangedProtocol:Z

    .line 6284
    sget-object v12, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v9, v12, Lcom/policydm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 6285
    const-string v12, "/AAuthLevel"

    invoke-virtual {v9, v12}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 6286
    invoke-static {p1, v9}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v6

    .line 6287
    if-eqz v6, :cond_e

    .line 6289
    iget v8, v6, Lcom/policydm/eng/core/XDMVnode;->size:I

    .line 6295
    :goto_7
    new-array v1, v8, [C

    .line 6296
    const/4 v12, 0x0

    invoke-static {p1, v9, v12, v1, v8}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmRead(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 6297
    invoke-static {v1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v7, Lcom/policydm/db/XDBProfileInfo;->AuthLevel:Ljava/lang/String;

    .line 6299
    sget-object v12, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v9, v12, Lcom/policydm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 6300
    const-string v12, "/AAuthType"

    invoke-virtual {v9, v12}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 6301
    invoke-static {p1, v9}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v6

    .line 6302
    if-eqz v6, :cond_f

    .line 6304
    iget v8, v6, Lcom/policydm/eng/core/XDMVnode;->size:I

    .line 6310
    :goto_8
    new-array v1, v8, [C

    .line 6311
    const/4 v12, 0x0

    invoke-static {p1, v9, v12, v1, v8}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmRead(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 6313
    const-string v12, "DIGEST"

    invoke-static {v1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v12

    if-nez v12, :cond_10

    .line 6315
    const/4 v3, 0x1

    .line 6333
    :goto_9
    iput v3, v7, Lcom/policydm/db/XDBProfileInfo;->AuthType:I

    .line 6335
    sget-object v12, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v9, v12, Lcom/policydm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 6336
    const-string v12, "/AAuthName"

    invoke-virtual {v9, v12}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 6337
    invoke-static {p1, v9}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v6

    .line 6338
    if-eqz v6, :cond_14

    .line 6340
    iget v8, v6, Lcom/policydm/eng/core/XDMVnode;->size:I

    .line 6346
    :goto_a
    new-array v1, v8, [C

    .line 6347
    const/4 v12, 0x0

    invoke-static {p1, v9, v12, v1, v8}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmRead(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 6348
    invoke-static {v1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v7, Lcom/policydm/db/XDBProfileInfo;->UserName:Ljava/lang/String;

    .line 6350
    sget-object v12, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v9, v12, Lcom/policydm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 6351
    const-string v12, "/AAuthSecret"

    invoke-virtual {v9, v12}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 6352
    invoke-static {p1, v9}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v6

    .line 6353
    if-eqz v6, :cond_15

    .line 6355
    iget v8, v6, Lcom/policydm/eng/core/XDMVnode;->size:I

    .line 6361
    :goto_b
    new-array v1, v8, [C

    .line 6362
    const/4 v12, 0x0

    invoke-static {p1, v9, v12, v1, v8}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmRead(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 6363
    invoke-static {v1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v7, Lcom/policydm/db/XDBProfileInfo;->Password:Ljava/lang/String;

    .line 6365
    sget-object v12, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v9, v12, Lcom/policydm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 6366
    const-string v12, "/AAuthData"

    invoke-virtual {v9, v12}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 6367
    invoke-static {p1, v9}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v6

    .line 6368
    if-eqz v6, :cond_16

    .line 6370
    iget v8, v6, Lcom/policydm/eng/core/XDMVnode;->size:I

    .line 6371
    iget v4, v6, Lcom/policydm/eng/core/XDMVnode;->format:I

    .line 6378
    .local v4, "format":I
    :goto_c
    new-array v1, v8, [C

    .line 6379
    const/4 v12, 0x0

    invoke-static {p1, v9, v12, v1, v8}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmRead(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 6380
    invoke-static {v1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v7, Lcom/policydm/db/XDBProfileInfo;->ClientNonce:Ljava/lang/String;

    .line 6381
    iput v4, v7, Lcom/policydm/db/XDBProfileInfo;->ClientNonceFormat:I

    .line 6383
    sget-object v12, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v9, v12, Lcom/policydm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    .line 6384
    const-string v12, "/AAuthLevel"

    invoke-virtual {v9, v12}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 6385
    invoke-static {p1, v9}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v6

    .line 6386
    if-eqz v6, :cond_17

    .line 6388
    iget v8, v6, Lcom/policydm/eng/core/XDMVnode;->size:I

    .line 6394
    :goto_d
    new-array v1, v8, [C

    .line 6395
    const/4 v12, 0x0

    invoke-static {p1, v9, v12, v1, v8}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmRead(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 6396
    invoke-static {v1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v7, Lcom/policydm/db/XDBProfileInfo;->ServerAuthLevel:Ljava/lang/String;

    .line 6398
    sget-object v12, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v9, v12, Lcom/policydm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    .line 6399
    const-string v12, "/AAuthType"

    invoke-virtual {v9, v12}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 6400
    invoke-static {p1, v9}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v6

    .line 6401
    if-eqz v6, :cond_18

    .line 6403
    iget v8, v6, Lcom/policydm/eng/core/XDMVnode;->size:I

    .line 6409
    :goto_e
    new-array v1, v8, [C

    .line 6410
    const/4 v12, 0x0

    invoke-static {p1, v9, v12, v1, v8}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmRead(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 6412
    const-string v12, "DIGEST"

    invoke-static {v1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v12

    if-nez v12, :cond_19

    .line 6414
    const/4 v3, 0x1

    .line 6432
    :goto_f
    iput v3, v7, Lcom/policydm/db/XDBProfileInfo;->nServerAuthType:I

    .line 6434
    sget-object v12, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v9, v12, Lcom/policydm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    .line 6435
    const-string v12, "/AAuthName"

    invoke-virtual {v9, v12}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 6436
    invoke-static {p1, v9}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v6

    .line 6437
    if-eqz v6, :cond_1d

    .line 6439
    iget v8, v6, Lcom/policydm/eng/core/XDMVnode;->size:I

    .line 6445
    :goto_10
    new-array v1, v8, [C

    .line 6446
    const/4 v12, 0x0

    invoke-static {p1, v9, v12, v1, v8}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmRead(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 6447
    invoke-static {v1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v7, Lcom/policydm/db/XDBProfileInfo;->ServerID:Ljava/lang/String;

    .line 6449
    sget-object v12, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v9, v12, Lcom/policydm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    .line 6450
    const-string v12, "/AAuthSecret"

    invoke-virtual {v9, v12}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 6451
    invoke-static {p1, v9}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v6

    .line 6452
    if-eqz v6, :cond_1e

    .line 6454
    iget v8, v6, Lcom/policydm/eng/core/XDMVnode;->size:I

    .line 6460
    :goto_11
    new-array v1, v8, [C

    .line 6461
    const/4 v12, 0x0

    invoke-static {p1, v9, v12, v1, v8}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmRead(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 6462
    invoke-static {v1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v7, Lcom/policydm/db/XDBProfileInfo;->ServerPwd:Ljava/lang/String;

    .line 6464
    sget-object v12, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v9, v12, Lcom/policydm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    .line 6465
    const-string v12, "/AAuthData"

    invoke-virtual {v9, v12}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 6466
    invoke-static {p1, v9}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v6

    .line 6467
    if-eqz v6, :cond_1f

    .line 6469
    iget v8, v6, Lcom/policydm/eng/core/XDMVnode;->size:I

    .line 6470
    iget v4, v6, Lcom/policydm/eng/core/XDMVnode;->format:I

    .line 6477
    :goto_12
    new-array v1, v8, [C

    .line 6478
    const/4 v12, 0x0

    invoke-static {p1, v9, v12, v1, v8}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmRead(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 6479
    invoke-static {v1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v7, Lcom/policydm/db/XDBProfileInfo;->ServerNonce:Ljava/lang/String;

    .line 6480
    iput v4, v7, Lcom/policydm/db/XDBProfileInfo;->ServerNonceFormat:I

    .line 6483
    sget-object v12, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v9, v12, Lcom/policydm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 6484
    const-string v12, "/Ext"

    invoke-virtual {v9, v12}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 6485
    invoke-static {p1, v9}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v6

    .line 6486
    if-eqz v6, :cond_d

    .line 6494
    :cond_d
    const/4 v9, 0x0

    .line 6495
    const/4 v7, 0x0

    .line 6496
    const/4 v12, 0x0

    sput-object v12, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeTndsInfo:Lcom/policydm/eng/core/XDMAccXNode;

    .line 6497
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 6293
    .end local v4    # "format":I
    :cond_e
    const/4 v8, 0x0

    goto/16 :goto_7

    .line 6308
    :cond_f
    const/4 v8, 0x0

    goto/16 :goto_8

    .line 6317
    :cond_10
    const-string v12, "BASIC"

    invoke-static {v1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v12

    if-nez v12, :cond_11

    .line 6319
    const/4 v3, 0x0

    goto/16 :goto_9

    .line 6321
    :cond_11
    const-string v12, "HMAC"

    invoke-static {v1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v12

    if-nez v12, :cond_12

    .line 6323
    const/4 v3, 0x2

    goto/16 :goto_9

    .line 6325
    :cond_12
    const-string v12, "DIGEST"

    invoke-static {v1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v12

    if-nez v12, :cond_13

    .line 6327
    const/4 v3, 0x1

    goto/16 :goto_9

    .line 6331
    :cond_13
    const/4 v3, 0x0

    goto/16 :goto_9

    .line 6344
    :cond_14
    const/4 v8, 0x0

    goto/16 :goto_a

    .line 6359
    :cond_15
    const/4 v8, 0x0

    goto/16 :goto_b

    .line 6375
    :cond_16
    const/4 v8, 0x0

    .line 6376
    const/16 v4, 0xc

    .restart local v4    # "format":I
    goto/16 :goto_c

    .line 6392
    :cond_17
    const/4 v8, 0x0

    goto/16 :goto_d

    .line 6407
    :cond_18
    const/4 v8, 0x0

    goto/16 :goto_e

    .line 6416
    :cond_19
    const-string v12, "BASIC"

    invoke-static {v1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v12

    if-nez v12, :cond_1a

    .line 6418
    const/4 v3, 0x0

    goto/16 :goto_f

    .line 6420
    :cond_1a
    const-string v12, "HMAC"

    invoke-static {v1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v12

    if-nez v12, :cond_1b

    .line 6422
    const/4 v3, 0x2

    goto/16 :goto_f

    .line 6424
    :cond_1b
    const-string v12, "DIGEST"

    invoke-static {v1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v12

    if-nez v12, :cond_1c

    .line 6426
    const/4 v3, 0x1

    goto/16 :goto_f

    .line 6430
    :cond_1c
    const/4 v3, 0x0

    goto/16 :goto_f

    .line 6443
    :cond_1d
    const/4 v8, 0x0

    goto/16 :goto_10

    .line 6458
    :cond_1e
    const/4 v8, 0x0

    goto/16 :goto_11

    .line 6474
    :cond_1f
    const/4 v8, 0x0

    .line 6475
    const/16 v4, 0xc

    goto/16 :goto_12
.end method

.method public xdmAgentGetAclStr(Lcom/policydm/eng/core/XDMOmList;Lcom/policydm/eng/parser/XDMParserItem;)Ljava/lang/String;
    .locals 13
    .param p1, "acllist"    # Lcom/policydm/eng/core/XDMOmList;
    .param p2, "item"    # Lcom/policydm/eng/parser/XDMParserItem;

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 3986
    const/4 v6, 0x5

    new-array v1, v6, [Ljava/lang/String;

    const-string v6, "\u0000"

    aput-object v6, v1, v8

    const-string v6, "\u0000"

    aput-object v6, v1, v9

    const-string v6, "\u0000"

    aput-object v6, v1, v10

    const-string v6, "\u0000"

    aput-object v6, v1, v11

    const-string v6, "\u0000"

    aput-object v6, v1, v12

    .line 3992
    .local v1, "buf":[Ljava/lang/String;
    move-object v2, p1

    .line 3994
    .local v2, "cur":Lcom/policydm/eng/core/XDMOmList;
    if-nez p1, :cond_0

    .line 3995
    const/4 v5, 0x0

    .line 4203
    :goto_0
    return-object v5

    .line 3997
    :cond_0
    const-string v4, "\u0000"

    .line 4004
    .local v4, "szOutbuf":Ljava/lang/String;
    iget-object v6, p2, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    if-nez v6, :cond_6

    .line 4005
    const/4 v6, 0x0

    iput-object v6, p2, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    .line 4010
    :goto_1
    if-eqz v2, :cond_d

    .line 4012
    iget-object v0, v2, Lcom/policydm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    check-cast v0, Lcom/policydm/eng/core/XDMOmAcl;

    .line 4013
    .local v0, "acl":Lcom/policydm/eng/core/XDMOmAcl;
    iget v6, v0, Lcom/policydm/eng/core/XDMOmAcl;->ac:I

    and-int/lit8 v6, v6, 0x1

    if-lez v6, :cond_1

    .line 4017
    aget-object v6, v1, v8

    invoke-virtual {v6, v8}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-nez v6, :cond_8

    .line 4018
    iget-object v6, v0, Lcom/policydm/eng/core/XDMOmAcl;->m_szServerid:Ljava/lang/String;

    aput-object v6, v1, v8

    .line 4022
    :cond_1
    :goto_2
    iget v6, v0, Lcom/policydm/eng/core/XDMOmAcl;->ac:I

    and-int/lit8 v6, v6, 0x2

    if-lez v6, :cond_2

    .line 4025
    aget-object v6, v1, v9

    invoke-virtual {v6, v8}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-nez v6, :cond_9

    .line 4026
    iget-object v6, v0, Lcom/policydm/eng/core/XDMOmAcl;->m_szServerid:Ljava/lang/String;

    aput-object v6, v1, v9

    .line 4030
    :cond_2
    :goto_3
    iget v6, v0, Lcom/policydm/eng/core/XDMOmAcl;->ac:I

    and-int/lit8 v6, v6, 0x4

    if-lez v6, :cond_3

    .line 4033
    aget-object v6, v1, v10

    invoke-virtual {v6, v8}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-nez v6, :cond_a

    .line 4034
    iget-object v6, v0, Lcom/policydm/eng/core/XDMOmAcl;->m_szServerid:Ljava/lang/String;

    aput-object v6, v1, v10

    .line 4038
    :cond_3
    :goto_4
    iget v6, v0, Lcom/policydm/eng/core/XDMOmAcl;->ac:I

    and-int/lit8 v6, v6, 0x8

    if-lez v6, :cond_4

    .line 4041
    aget-object v6, v1, v11

    invoke-virtual {v6, v8}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-nez v6, :cond_b

    .line 4042
    iget-object v6, v0, Lcom/policydm/eng/core/XDMOmAcl;->m_szServerid:Ljava/lang/String;

    aput-object v6, v1, v11

    .line 4046
    :cond_4
    :goto_5
    iget v6, v0, Lcom/policydm/eng/core/XDMOmAcl;->ac:I

    and-int/lit8 v6, v6, 0x10

    if-lez v6, :cond_5

    .line 4049
    aget-object v6, v1, v12

    invoke-virtual {v6, v8}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-nez v6, :cond_c

    .line 4050
    iget-object v6, v0, Lcom/policydm/eng/core/XDMOmAcl;->m_szServerid:Ljava/lang/String;

    aput-object v6, v1, v12

    .line 4054
    :cond_5
    :goto_6
    iget-object v2, v2, Lcom/policydm/eng/core/XDMOmList;->next:Lcom/policydm/eng/core/XDMOmList;

    goto :goto_1

    .line 4006
    .end local v0    # "acl":Lcom/policydm/eng/core/XDMOmAcl;
    :cond_6
    iget-object v6, p2, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v6, v6, Lcom/policydm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 4007
    iget-object v6, p2, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    const/4 v7, 0x0

    iput-object v7, v6, Lcom/policydm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    goto :goto_1

    .line 4009
    :cond_7
    const-string v6, "item->meta !NULL"

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_1

    .line 4020
    .restart local v0    # "acl":Lcom/policydm/eng/core/XDMOmAcl;
    :cond_8
    aget-object v6, v1, v8

    iget-object v7, v0, Lcom/policydm/eng/core/XDMOmAcl;->m_szServerid:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v1, v8

    goto :goto_2

    .line 4028
    :cond_9
    aget-object v6, v1, v9

    iget-object v7, v0, Lcom/policydm/eng/core/XDMOmAcl;->m_szServerid:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v1, v9

    goto :goto_3

    .line 4036
    :cond_a
    aget-object v6, v1, v10

    iget-object v7, v0, Lcom/policydm/eng/core/XDMOmAcl;->m_szServerid:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v1, v10

    goto :goto_4

    .line 4044
    :cond_b
    aget-object v6, v1, v11

    iget-object v7, v0, Lcom/policydm/eng/core/XDMOmAcl;->m_szServerid:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v1, v11

    goto :goto_5

    .line 4052
    :cond_c
    aget-object v6, v1, v12

    iget-object v7, v0, Lcom/policydm/eng/core/XDMOmAcl;->m_szServerid:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v1, v12

    goto :goto_6

    .line 4057
    .end local v0    # "acl":Lcom/policydm/eng/core/XDMOmAcl;
    :cond_d
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_7
    const/4 v6, 0x5

    if-ge v3, v6, :cond_2d

    .line 4059
    if-nez v3, :cond_f

    aget-object v6, v1, v3

    invoke-virtual {v6, v8}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-eqz v6, :cond_f

    .line 4061
    invoke-virtual {v4, v8}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-eqz v6, :cond_e

    .line 4063
    iget-object v6, p2, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    if-nez v6, :cond_19

    .line 4065
    const-string v6, "&"

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 4081
    :cond_e
    :goto_8
    invoke-virtual {v4, v8}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-eqz v6, :cond_1c

    .line 4082
    const-string v6, "Add="

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 4086
    :cond_f
    :goto_9
    if-ne v3, v9, :cond_11

    aget-object v6, v1, v3

    invoke-virtual {v6, v8}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-eqz v6, :cond_11

    .line 4088
    invoke-virtual {v4, v8}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-eqz v6, :cond_10

    .line 4090
    iget-object v6, p2, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    if-nez v6, :cond_1d

    .line 4092
    const-string v6, "&"

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 4108
    :cond_10
    :goto_a
    invoke-virtual {v4, v8}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-eqz v6, :cond_20

    .line 4109
    const-string v6, "Delete="

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 4113
    :cond_11
    :goto_b
    if-ne v3, v10, :cond_13

    aget-object v6, v1, v3

    invoke-virtual {v6, v8}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-eqz v6, :cond_13

    .line 4115
    invoke-virtual {v4, v8}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-eqz v6, :cond_12

    .line 4117
    iget-object v6, p2, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    if-nez v6, :cond_21

    .line 4119
    const-string v6, "&"

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 4135
    :cond_12
    :goto_c
    invoke-virtual {v4, v8}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-eqz v6, :cond_24

    .line 4136
    const-string v6, "Exec="

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 4140
    :cond_13
    :goto_d
    if-ne v3, v11, :cond_15

    aget-object v6, v1, v3

    invoke-virtual {v6, v8}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-eqz v6, :cond_15

    .line 4142
    invoke-virtual {v4, v8}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-eqz v6, :cond_14

    .line 4144
    iget-object v6, p2, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    if-nez v6, :cond_25

    .line 4146
    const-string v6, "&"

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 4162
    :cond_14
    :goto_e
    invoke-virtual {v4, v8}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-eqz v6, :cond_28

    .line 4163
    const-string v6, "Get="

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 4167
    :cond_15
    :goto_f
    if-ne v3, v12, :cond_17

    aget-object v6, v1, v3

    invoke-virtual {v6, v8}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-eqz v6, :cond_17

    .line 4169
    invoke-virtual {v4, v8}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-eqz v6, :cond_16

    .line 4171
    iget-object v6, p2, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    if-nez v6, :cond_29

    .line 4173
    const-string v6, "&"

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 4189
    :cond_16
    :goto_10
    invoke-virtual {v4, v8}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-eqz v6, :cond_2c

    .line 4190
    const-string v6, "Replace="

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 4195
    :cond_17
    :goto_11
    aget-object v6, v1, v3

    invoke-virtual {v6, v8}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-eqz v6, :cond_18

    .line 4197
    aget-object v6, v1, v3

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 4057
    :cond_18
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_7

    .line 4067
    :cond_19
    iget-object v6, p2, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v6, v6, Lcom/policydm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1a

    .line 4069
    const-string v6, "&"

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_8

    .line 4071
    :cond_1a
    const-string v6, "xml"

    iget-object v7, p2, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v7, v7, Lcom/policydm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_1b

    .line 4073
    const-string v6, "&amp;"

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_8

    .line 4077
    :cond_1b
    const-string v6, "&"

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_8

    .line 4084
    :cond_1c
    const-string v4, "Add="

    goto/16 :goto_9

    .line 4094
    :cond_1d
    iget-object v6, p2, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v6, v6, Lcom/policydm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1e

    .line 4096
    const-string v6, "&"

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_a

    .line 4098
    :cond_1e
    const-string v6, "xml"

    iget-object v7, p2, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v7, v7, Lcom/policydm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_1f

    .line 4100
    const-string v6, "&amp;"

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_a

    .line 4104
    :cond_1f
    const-string v6, "&"

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_a

    .line 4111
    :cond_20
    const-string v4, "Delete="

    goto/16 :goto_b

    .line 4121
    :cond_21
    iget-object v6, p2, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v6, v6, Lcom/policydm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_22

    .line 4123
    const-string v6, "&"

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_c

    .line 4125
    :cond_22
    const-string v6, "xml"

    iget-object v7, p2, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v7, v7, Lcom/policydm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_23

    .line 4127
    const-string v6, "&amp;"

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_c

    .line 4131
    :cond_23
    const-string v6, "&"

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_c

    .line 4138
    :cond_24
    const-string v4, "Exec="

    goto/16 :goto_d

    .line 4148
    :cond_25
    iget-object v6, p2, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v6, v6, Lcom/policydm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_26

    .line 4150
    const-string v6, "&"

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_e

    .line 4152
    :cond_26
    const-string v6, "xml"

    iget-object v7, p2, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v7, v7, Lcom/policydm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_27

    .line 4154
    const-string v6, "&amp;"

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_e

    .line 4158
    :cond_27
    const-string v6, "&"

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_e

    .line 4165
    :cond_28
    const-string v4, "Get="

    goto/16 :goto_f

    .line 4175
    :cond_29
    iget-object v6, p2, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v6, v6, Lcom/policydm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2a

    .line 4177
    const-string v6, "&"

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_10

    .line 4179
    :cond_2a
    const-string v6, "xml"

    iget-object v7, p2, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v7, v7, Lcom/policydm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_2b

    .line 4181
    const-string v6, "&amp;"

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_10

    .line 4185
    :cond_2b
    const-string v6, "&"

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_10

    .line 4192
    :cond_2c
    const-string v4, "Replace="

    goto/16 :goto_11

    .line 4201
    :cond_2d
    move-object v5, v4

    .line 4203
    .local v5, "szTmp":Ljava/lang/String;
    goto/16 :goto_0
.end method

.method public xdmAgentGetPathFromNode(Lcom/policydm/eng/core/XDMOmTree;Lcom/policydm/eng/core/XDMVnode;)Ljava/lang/String;
    .locals 9
    .param p1, "om"    # Lcom/policydm/eng/core/XDMOmTree;
    .param p2, "node"    # Lcom/policydm/eng/core/XDMVnode;

    .prologue
    .line 3486
    const/16 v7, 0xa

    new-array v0, v7, [Ljava/lang/String;

    .line 3487
    .local v0, "buf":[Ljava/lang/String;
    const/4 v3, 0x0

    .line 3488
    .local v3, "level":I
    move-object v1, p2

    .line 3489
    .local v1, "cur":Lcom/policydm/eng/core/XDMVnode;
    const/4 v4, 0x0

    .line 3491
    .local v4, "szName":Ljava/lang/String;
    iget-object v7, p1, Lcom/policydm/eng/core/XDMOmTree;->vfs:Lcom/policydm/eng/core/XDMOmVfs;

    iget-object v8, p1, Lcom/policydm/eng/core/XDMOmTree;->vfs:Lcom/policydm/eng/core/XDMOmVfs;

    iget-object v8, v8, Lcom/policydm/eng/core/XDMOmVfs;->root:Lcom/policydm/eng/core/XDMVnode;

    invoke-static {v7, v8, p2}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmVfsGetParent(Lcom/policydm/eng/core/XDMOmVfs;Lcom/policydm/eng/core/XDMVnode;Lcom/policydm/eng/core/XDMVnode;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v1

    .line 3492
    :goto_0
    if-eqz v1, :cond_0

    iget-object v7, p1, Lcom/policydm/eng/core/XDMOmTree;->vfs:Lcom/policydm/eng/core/XDMOmVfs;

    iget-object v7, v7, Lcom/policydm/eng/core/XDMOmVfs;->root:Lcom/policydm/eng/core/XDMVnode;

    if-eq v1, v7, :cond_0

    .line 3494
    iget-object v7, v1, Lcom/policydm/eng/core/XDMVnode;->m_szName:Ljava/lang/String;

    aput-object v7, v0, v3

    .line 3495
    add-int/lit8 v3, v3, 0x1

    .line 3496
    iget-object v7, p1, Lcom/policydm/eng/core/XDMOmTree;->vfs:Lcom/policydm/eng/core/XDMOmVfs;

    iget-object v8, p1, Lcom/policydm/eng/core/XDMOmTree;->vfs:Lcom/policydm/eng/core/XDMOmVfs;

    iget-object v8, v8, Lcom/policydm/eng/core/XDMOmVfs;->root:Lcom/policydm/eng/core/XDMVnode;

    invoke-static {v7, v8, v1}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmVfsGetParent(Lcom/policydm/eng/core/XDMOmVfs;Lcom/policydm/eng/core/XDMVnode;Lcom/policydm/eng/core/XDMVnode;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v1

    goto :goto_0

    .line 3499
    :cond_0
    if-nez v1, :cond_1

    .line 3501
    const-string v6, "."

    .line 3502
    .local v6, "szOutbuf":Ljava/lang/String;
    move-object v4, v6

    move-object v5, v4

    .line 3518
    .end local v4    # "szName":Ljava/lang/String;
    .local v5, "szName":Ljava/lang/String;
    :goto_1
    return-object v5

    .line 3506
    .end local v5    # "szName":Ljava/lang/String;
    .end local v6    # "szOutbuf":Ljava/lang/String;
    .restart local v4    # "szName":Ljava/lang/String;
    :cond_1
    const-string v6, "./"

    .line 3507
    .restart local v6    # "szOutbuf":Ljava/lang/String;
    add-int/lit8 v2, v3, -0x1

    .local v2, "i":I
    :goto_2
    if-ltz v2, :cond_2

    .line 3509
    aget-object v7, v0, v2

    invoke-virtual {v6, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 3510
    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 3507
    add-int/lit8 v2, v2, -0x1

    goto :goto_2

    .line 3512
    :cond_2
    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v7

    if-nez v7, :cond_3

    const-string v7, "./"

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v7

    if-eqz v7, :cond_4

    .line 3514
    :cond_3
    iget-object v7, p2, Lcom/policydm/eng/core/XDMVnode;->m_szName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 3517
    :cond_4
    move-object v4, v6

    move-object v5, v4

    .line 3518
    .end local v4    # "szName":Ljava/lang/String;
    .restart local v5    # "szName":Ljava/lang/String;
    goto :goto_1
.end method

.method public xdmAgentGetWorkSpace()Lcom/policydm/eng/core/XDMWorkspace;
    .locals 1

    .prologue
    .line 122
    sget-object v0, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    if-nez v0, :cond_0

    .line 124
    const-string v0, "dm_ws is NULL"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 125
    const/4 v0, 0x0

    .line 127
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    goto :goto_0
.end method

.method public xdmAgentHandleCmd()I
    .locals 12

    .prologue
    const/4 v6, -0x4

    const/4 v11, 0x3

    const/4 v10, 0x1

    const/4 v7, 0x0

    .line 4492
    sget-object v5, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    .line 4494
    .local v5, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    const/4 v2, 0x0

    .line 4495
    .local v2, "list":Lcom/policydm/eng/core/XDMLinkedList;
    const/4 v4, 0x0

    .line 4496
    .local v4, "status":Lcom/policydm/eng/parser/XDMParserStatus;
    const/4 v3, 0x0

    .line 4497
    .local v3, "res":I
    const/4 v1, 0x0

    .line 4499
    .local v1, "isAtomic":Z
    iget-object v8, v5, Lcom/policydm/eng/core/XDMWorkspace;->procState:Lcom/policydm/interfaces/XDMInterface$XDMProcessingState;

    sget-object v9, Lcom/policydm/interfaces/XDMInterface$XDMProcessingState;->XDM_PROC_NONE:Lcom/policydm/interfaces/XDMInterface$XDMProcessingState;

    if-ne v8, v9, :cond_0

    .line 4501
    iput v7, v5, Lcom/policydm/eng/core/XDMWorkspace;->procStep:I

    .line 4502
    iput v10, v5, Lcom/policydm/eng/core/XDMWorkspace;->cmdID:I

    .line 4505
    :cond_0
    :goto_0
    iget v8, v5, Lcom/policydm/eng/core/XDMWorkspace;->procStep:I

    if-eq v8, v11, :cond_f

    .line 4508
    iget-boolean v8, v5, Lcom/policydm/eng/core/XDMWorkspace;->IsSequenceProcessing:Z

    if-nez v8, :cond_7

    .line 4510
    iget-object v2, v5, Lcom/policydm/eng/core/XDMWorkspace;->list:Lcom/policydm/eng/core/XDMLinkedList;

    .line 4511
    invoke-static {v2, v7}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListSetCurrentObj(Lcom/policydm/eng/core/XDMLinkedList;I)V

    .line 4512
    invoke-static {v2}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/policydm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/policydm/agent/XDMAgent;

    .line 4544
    .local v0, "cmditem":Lcom/policydm/agent/XDMAgent;
    :cond_1
    :goto_1
    if-nez v0, :cond_2

    .line 4546
    iget-object v8, v5, Lcom/policydm/eng/core/XDMWorkspace;->uicAlert:Lcom/policydm/eng/parser/XDMParserAlert;

    if-eqz v8, :cond_f

    .line 4548
    invoke-virtual {p0}, Lcom/policydm/agent/XDMAgent;->xdmAgentCmdUicAlert()Z

    .line 4557
    :cond_2
    if-eqz v0, :cond_e

    .line 4559
    const-string v8, "Get"

    iget-object v9, v0, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-eqz v8, :cond_3

    const-string v8, "Exec"

    iget-object v9, v0, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-eqz v8, :cond_3

    const-string v8, "Alert"

    iget-object v9, v0, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-eqz v8, :cond_3

    const-string v8, "Add"

    iget-object v9, v0, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-eqz v8, :cond_3

    const-string v8, "Replace"

    iget-object v9, v0, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-eqz v8, :cond_3

    const-string v8, "Copy"

    iget-object v9, v0, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-eqz v8, :cond_3

    const-string v8, "Delete"

    iget-object v9, v0, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-eqz v8, :cond_3

    const-string v8, "Atomic_Start"

    iget-object v9, v0, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-eqz v8, :cond_3

    const-string v8, "Sequence_Start"

    iget-object v9, v0, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_4

    .line 4562
    :cond_3
    iget v8, v5, Lcom/policydm/eng/core/XDMWorkspace;->numAction:I

    add-int/lit8 v8, v8, 0x1

    iput v8, v5, Lcom/policydm/eng/core/XDMWorkspace;->numAction:I

    .line 4564
    :cond_4
    iget-object v8, v0, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-static {v8}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 4565
    iget-object v8, v0, Lcom/policydm/agent/XDMAgent;->m_Atomic:Lcom/policydm/eng/parser/XDMParserAtomic;

    if-eqz v8, :cond_9

    .line 4567
    iput-boolean v10, v5, Lcom/policydm/eng/core/XDMWorkspace;->inAtomicCmd:Z

    .line 4568
    const/4 v1, 0x1

    .line 4575
    :cond_5
    :goto_2
    invoke-virtual {p0, v0, v1, v4}, Lcom/policydm/agent/XDMAgent;->xdmAgentVerifyCmd(Lcom/policydm/agent/XDMAgent;ZLcom/policydm/eng/parser/XDMParserStatus;)I

    move-result v3

    .line 4578
    iget-boolean v8, v5, Lcom/policydm/eng/core/XDMWorkspace;->IsSequenceProcessing:Z

    if-nez v8, :cond_6

    .line 4580
    invoke-static {v2}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/policydm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "cmditem":Lcom/policydm/agent/XDMAgent;
    check-cast v0, Lcom/policydm/agent/XDMAgent;

    .line 4581
    .restart local v0    # "cmditem":Lcom/policydm/agent/XDMAgent;
    invoke-static {v2}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListRemoveObjAtFirst(Lcom/policydm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    .line 4586
    :cond_6
    if-ne v3, v6, :cond_a

    .line 4588
    const-string v7, "XDM_RET_PAUSED_BECAUSE_UIC_COMMAND"

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 4617
    .end local v0    # "cmditem":Lcom/policydm/agent/XDMAgent;
    :goto_3
    return v6

    .line 4516
    :cond_7
    iget-object v2, v5, Lcom/policydm/eng/core/XDMWorkspace;->sequenceList:Lcom/policydm/eng/core/XDMLinkedList;

    .line 4518
    invoke-static {v2, v7}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListGetObj(Lcom/policydm/eng/core/XDMLinkedList;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/policydm/agent/XDMAgent;

    .line 4519
    .restart local v0    # "cmditem":Lcom/policydm/agent/XDMAgent;
    if-eqz v0, :cond_8

    .line 4521
    iget-object v8, v5, Lcom/policydm/eng/core/XDMWorkspace;->sequenceList:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v8}, Lcom/policydm/agent/XDMAgent;->xdmAgentCmdSequenceBlock(Lcom/policydm/eng/core/XDMLinkedList;)I

    .line 4529
    :goto_4
    iget-boolean v8, v5, Lcom/policydm/eng/core/XDMWorkspace;->IsSequenceProcessing:Z

    if-nez v8, :cond_1

    .line 4531
    iget-object v2, v5, Lcom/policydm/eng/core/XDMWorkspace;->list:Lcom/policydm/eng/core/XDMLinkedList;

    .line 4532
    invoke-static {v2, v7}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListSetCurrentObj(Lcom/policydm/eng/core/XDMLinkedList;I)V

    .line 4533
    invoke-static {v2}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/policydm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "cmditem":Lcom/policydm/agent/XDMAgent;
    check-cast v0, Lcom/policydm/agent/XDMAgent;

    .line 4537
    .restart local v0    # "cmditem":Lcom/policydm/agent/XDMAgent;
    invoke-static {v2}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/policydm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "cmditem":Lcom/policydm/agent/XDMAgent;
    check-cast v0, Lcom/policydm/agent/XDMAgent;

    .line 4538
    .restart local v0    # "cmditem":Lcom/policydm/agent/XDMAgent;
    invoke-static {v2}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListRemoveObjAtFirst(Lcom/policydm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 4525
    :cond_8
    iput-boolean v7, v5, Lcom/policydm/eng/core/XDMWorkspace;->IsSequenceProcessing:Z

    goto :goto_4

    .line 4570
    :cond_9
    iget-object v8, v0, Lcom/policydm/agent/XDMAgent;->m_Sequence:Lcom/policydm/eng/parser/XDMParserSequence;

    if-eqz v8, :cond_5

    .line 4572
    iput-boolean v10, v5, Lcom/policydm/eng/core/XDMWorkspace;->inSequenceCmd:Z

    goto :goto_2

    .line 4593
    :cond_a
    iput-boolean v7, v5, Lcom/policydm/eng/core/XDMWorkspace;->atomicFlag:Z

    .line 4594
    iput-boolean v7, v5, Lcom/policydm/eng/core/XDMWorkspace;->inAtomicCmd:Z

    .line 4595
    iput-boolean v7, v5, Lcom/policydm/eng/core/XDMWorkspace;->inSequenceCmd:Z

    .line 4598
    const/4 v8, 0x2

    if-eq v3, v8, :cond_b

    if-ne v3, v10, :cond_c

    .line 4600
    :cond_b
    iput v11, v5, Lcom/policydm/eng/core/XDMWorkspace;->procStep:I

    .line 4601
    iput v7, v5, Lcom/policydm/eng/core/XDMWorkspace;->numAction:I

    move v6, v3

    .line 4602
    goto :goto_3

    .line 4604
    :cond_c
    if-ne v3, v11, :cond_d

    move v6, v3

    .line 4606
    goto :goto_3

    .line 4608
    :cond_d
    if-eqz v3, :cond_2

    .line 4610
    const-string v6, "Processing failed"

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 4611
    const/4 v6, -0x1

    goto :goto_3

    .line 4614
    :cond_e
    invoke-static {v2}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListClearLinkedList(Lcom/policydm/eng/core/XDMLinkedList;)V

    goto/16 :goto_0

    .line 4616
    .end local v0    # "cmditem":Lcom/policydm/agent/XDMAgent;
    :cond_f
    iget-object v6, v5, Lcom/policydm/eng/core/XDMWorkspace;->list:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v6}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListClearLinkedList(Lcom/policydm/eng/core/XDMLinkedList;)V

    move v6, v7

    .line 4617
    goto :goto_3
.end method

.method public xdmAgentIsAccessibleNode(Ljava/lang/String;)Z
    .locals 4
    .param p1, "szPath"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 162
    const/4 v0, 0x0

    .line 188
    .local v0, "szInbox":Ljava/lang/String;
    const-string v3, "/AAuthSecret"

    invoke-static {p1, v3}, Lcom/policydm/eng/core/XDMMem;->xdmLibStrstr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 209
    :cond_0
    :goto_0
    return v1

    .line 193
    :cond_1
    const-string v3, "/AAuthData"

    invoke-static {p1, v3}, Lcom/policydm/eng/core/XDMMem;->xdmLibStrstr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 198
    const/16 v3, 0xa

    invoke-static {v3}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFGetMOPath(I)Ljava/lang/String;

    move-result-object v0

    .line 199
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    move v1, v2

    .line 201
    goto :goto_0

    .line 204
    :cond_2
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-static {p1, v0, v3}, Lcom/policydm/eng/core/XDMMem;->xdmLibStrncmp(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v3

    if-eqz v3, :cond_0

    move v1, v2

    .line 209
    goto :goto_0
.end method

.method public xdmAgentIsPermanentNode(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Z
    .locals 3
    .param p1, "om"    # Lcom/policydm/eng/core/XDMOmTree;
    .param p2, "szPath"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 216
    invoke-static {p1, p2}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v0

    .line 218
    .local v0, "node":Lcom/policydm/eng/core/XDMVnode;
    if-eqz v0, :cond_0

    .line 220
    iget v2, v0, Lcom/policydm/eng/core/XDMVnode;->scope:I

    if-ne v2, v1, :cond_0

    .line 225
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public xdmAgentLibMakeSessionID()Ljava/lang/String;
    .locals 8

    .prologue
    .line 603
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 607
    .local v0, "data":Ljava/util/Calendar;
    const/16 v4, 0xc

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    .line 608
    .local v2, "szNowData":Ljava/lang/String;
    const/16 v4, 0xd

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 610
    .local v1, "second":I
    const-string v4, "%x%x"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 611
    .local v3, "szSessionid":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Make sessionid ="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 613
    return-object v3
.end method

.method public xdmAgentLoadWorkSpace()I
    .locals 15

    .prologue
    const/4 v14, 0x1

    const/4 v10, -0x1

    const/4 v11, 0x0

    .line 844
    sget-object v9, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    .line 845
    .local v9, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    if-nez v9, :cond_1

    .line 1091
    :cond_0
    :goto_0
    return v10

    .line 848
    :cond_1
    iget-object v6, v9, Lcom/policydm/eng/core/XDMWorkspace;->om:Lcom/policydm/eng/core/XDMOmTree;

    .line 849
    .local v6, "om":Lcom/policydm/eng/core/XDMOmTree;
    const/4 v1, 0x0

    .line 850
    .local v1, "dValue":[B
    const-string v7, ""

    .line 852
    .local v7, "szAccBuf":Ljava/lang/String;
    const/4 v5, 0x0

    .line 853
    .local v5, "node":Lcom/policydm/eng/core/XDMVnode;
    const/4 v3, 0x0

    .line 855
    .local v3, "nReSyncMode":I
    sget-object v12, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v7, v12, Lcom/policydm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 856
    const-string v12, "/AAuthName"

    invoke-virtual {v7, v12}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 857
    invoke-static {v6, v7}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v5

    .line 858
    if-eqz v5, :cond_0

    .line 863
    iget v12, v5, Lcom/policydm/eng/core/XDMVnode;->size:I

    new-array v0, v12, [C

    .line 864
    .local v0, "buf":[C
    iget v12, v5, Lcom/policydm/eng/core/XDMVnode;->size:I

    invoke-static {v6, v7, v11, v0, v12}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmRead(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 865
    invoke-static {v0}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v9, Lcom/policydm/eng/core/XDMWorkspace;->m_szUserName:Ljava/lang/String;

    .line 867
    sget-object v12, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v7, v12, Lcom/policydm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 868
    const-string v12, "/AAuthSecret"

    invoke-virtual {v7, v12}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 869
    invoke-static {v6, v7}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v5

    .line 870
    if-eqz v5, :cond_0

    .line 874
    iget v12, v5, Lcom/policydm/eng/core/XDMVnode;->size:I

    new-array v0, v12, [C

    .line 875
    iget v12, v5, Lcom/policydm/eng/core/XDMVnode;->size:I

    invoke-static {v6, v7, v11, v0, v12}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmRead(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 876
    invoke-static {v0}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v9, Lcom/policydm/eng/core/XDMWorkspace;->m_szClientPW:Ljava/lang/String;

    .line 878
    sget-object v12, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v7, v12, Lcom/policydm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 879
    const-string v12, "/AAuthType"

    invoke-virtual {v7, v12}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 880
    invoke-static {v6, v7}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v5

    .line 881
    if-eqz v5, :cond_0

    .line 885
    iget v12, v5, Lcom/policydm/eng/core/XDMVnode;->size:I

    new-array v0, v12, [C

    .line 886
    iget v12, v5, Lcom/policydm/eng/core/XDMVnode;->size:I

    invoke-static {v6, v7, v11, v0, v12}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmRead(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 888
    invoke-static {}, Lcom/policydm/db/XDBProfileListAdp;->xdbGetNotiReSyncMode()I

    move-result v3

    .line 890
    if-ne v3, v14, :cond_4

    .line 893
    const-string v12, "DIGEST"

    invoke-static {v12}, Lcom/policydm/eng/core/XDMAuth;->xdmAuthAAuthtring2Type(Ljava/lang/String;)I

    move-result v12

    iput v12, v9, Lcom/policydm/eng/core/XDMWorkspace;->credType:I

    .line 903
    :goto_1
    sget-object v12, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v7, v12, Lcom/policydm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    .line 904
    const-string v12, "/AAuthType"

    invoke-virtual {v7, v12}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 905
    invoke-static {v6, v7}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v5

    .line 906
    if-eqz v5, :cond_0

    .line 910
    iget v12, v5, Lcom/policydm/eng/core/XDMVnode;->size:I

    new-array v0, v12, [C

    .line 911
    iget v12, v5, Lcom/policydm/eng/core/XDMVnode;->size:I

    invoke-static {v6, v7, v11, v0, v12}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmRead(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 912
    invoke-static {}, Lcom/policydm/db/XDBProfileListAdp;->xdbGetNotiReSyncMode()I

    move-result v3

    .line 914
    if-ne v3, v14, :cond_5

    .line 917
    const-string v12, "DIGEST"

    invoke-static {v12}, Lcom/policydm/eng/core/XDMAuth;->xdmAuthAAuthtring2Type(Ljava/lang/String;)I

    move-result v12

    iput v12, v9, Lcom/policydm/eng/core/XDMWorkspace;->serverCredType:I

    .line 930
    :cond_2
    :goto_2
    sget-object v12, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v7, v12, Lcom/policydm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 931
    const-string v12, "/ServerID"

    invoke-virtual {v7, v12}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 932
    invoke-static {v6, v7}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v5

    .line 933
    if-eqz v5, :cond_0

    .line 937
    iget v12, v5, Lcom/policydm/eng/core/XDMVnode;->size:I

    new-array v0, v12, [C

    .line 938
    iget v12, v5, Lcom/policydm/eng/core/XDMVnode;->size:I

    invoke-static {v6, v7, v11, v0, v12}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmRead(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 939
    invoke-static {v0}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v9, Lcom/policydm/eng/core/XDMWorkspace;->m_szServerID:Ljava/lang/String;

    .line 941
    sget-object v12, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v7, v12, Lcom/policydm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    .line 942
    const-string v12, "/AAuthSecret"

    invoke-virtual {v7, v12}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 943
    invoke-static {v6, v7}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v5

    .line 944
    if-eqz v5, :cond_0

    .line 948
    iget v12, v5, Lcom/policydm/eng/core/XDMVnode;->size:I

    new-array v0, v12, [C

    .line 949
    iget v12, v5, Lcom/policydm/eng/core/XDMVnode;->size:I

    invoke-static {v6, v7, v11, v0, v12}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmRead(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 950
    invoke-static {v0}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v9, Lcom/policydm/eng/core/XDMWorkspace;->m_szServerPW:Ljava/lang/String;

    .line 952
    if-ne v3, v14, :cond_6

    .line 954
    iget-object v12, v9, Lcom/policydm/eng/core/XDMWorkspace;->nextNonce:[B

    aput-byte v11, v12, v11

    .line 955
    iget-object v12, v9, Lcom/policydm/eng/core/XDMWorkspace;->nextNonce:[B

    aput-byte v11, v12, v14

    .line 956
    iget-object v12, v9, Lcom/policydm/eng/core/XDMWorkspace;->nextNonce:[B

    const/4 v13, 0x2

    aput-byte v11, v12, v13

    .line 957
    iget-object v12, v9, Lcom/policydm/eng/core/XDMWorkspace;->nextNonce:[B

    const/4 v13, 0x3

    aput-byte v11, v12, v13

    .line 1006
    :cond_3
    :goto_3
    sget-object v12, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v7, v12, Lcom/policydm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    .line 1007
    const-string v12, "/AAuthData"

    invoke-virtual {v7, v12}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1008
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Server szAccBuf) :"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 1009
    invoke-static {v6, v7}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v5

    .line 1010
    if-eqz v5, :cond_0

    .line 1012
    iget v12, v5, Lcom/policydm/eng/core/XDMVnode;->size:I

    if-lez v12, :cond_b

    .line 1014
    const/4 v0, 0x0

    .line 1015
    iget v12, v5, Lcom/policydm/eng/core/XDMVnode;->size:I

    new-array v0, v12, [C

    .line 1016
    iget v12, v5, Lcom/policydm/eng/core/XDMVnode;->size:I

    invoke-static {v6, v7, v11, v0, v12}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmRead(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 1022
    :goto_4
    if-eqz v0, :cond_e

    .line 1024
    iget v12, v5, Lcom/policydm/eng/core/XDMVnode;->format:I

    if-eq v12, v14, :cond_c

    .line 1026
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_5
    iget v12, v5, Lcom/policydm/eng/core/XDMVnode;->size:I

    if-ge v2, v12, :cond_e

    .line 1027
    iget-object v12, v9, Lcom/policydm/eng/core/XDMWorkspace;->serverNextNonce:[B

    aget-char v13, v0, v2

    int-to-byte v13, v13

    aput-byte v13, v12, v2

    .line 1026
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 899
    .end local v2    # "i":I
    :cond_4
    invoke-static {v0}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v8

    .line 900
    .local v8, "szTmp":Ljava/lang/String;
    invoke-static {v8}, Lcom/policydm/eng/core/XDMAuth;->xdmAuthAAuthtring2Type(Ljava/lang/String;)I

    move-result v12

    iput v12, v9, Lcom/policydm/eng/core/XDMWorkspace;->credType:I

    goto/16 :goto_1

    .line 923
    .end local v8    # "szTmp":Ljava/lang/String;
    :cond_5
    invoke-static {v0}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v8

    .line 924
    .restart local v8    # "szTmp":Ljava/lang/String;
    invoke-static {v8}, Lcom/policydm/eng/core/XDMAuth;->xdmAuthAAuthtring2Type(Ljava/lang/String;)I

    move-result v12

    iput v12, v9, Lcom/policydm/eng/core/XDMWorkspace;->serverCredType:I

    .line 925
    iget v12, v9, Lcom/policydm/eng/core/XDMWorkspace;->serverCredType:I

    if-ne v12, v10, :cond_2

    .line 927
    iget v12, v9, Lcom/policydm/eng/core/XDMWorkspace;->credType:I

    iput v12, v9, Lcom/policydm/eng/core/XDMWorkspace;->serverCredType:I

    goto/16 :goto_2

    .line 961
    .end local v8    # "szTmp":Ljava/lang/String;
    :cond_6
    sget-object v12, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v7, v12, Lcom/policydm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 962
    const-string v12, "/AAuthData"

    invoke-virtual {v7, v12}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 963
    invoke-static {v6, v7}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v5

    .line 964
    if-eqz v5, :cond_0

    .line 966
    iget v12, v5, Lcom/policydm/eng/core/XDMVnode;->size:I

    if-lez v12, :cond_7

    .line 968
    iget v12, v5, Lcom/policydm/eng/core/XDMVnode;->size:I

    new-array v0, v12, [C

    .line 969
    iget v12, v5, Lcom/policydm/eng/core/XDMVnode;->size:I

    invoke-static {v6, v7, v11, v0, v12}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmRead(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 975
    :goto_6
    if-eqz v0, :cond_3

    .line 977
    iget v12, v5, Lcom/policydm/eng/core/XDMVnode;->format:I

    if-eq v12, v14, :cond_9

    .line 979
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_7
    iget v12, v5, Lcom/policydm/eng/core/XDMVnode;->size:I

    if-ge v2, v12, :cond_8

    .line 980
    iget-object v12, v9, Lcom/policydm/eng/core/XDMWorkspace;->nextNonce:[B

    aget-char v13, v0, v2

    int-to-byte v13, v13

    aput-byte v13, v12, v2

    .line 979
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 973
    .end local v2    # "i":I
    :cond_7
    const/4 v0, 0x0

    goto :goto_6

    .line 982
    .restart local v2    # "i":I
    :cond_8
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "node->size = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget v13, v5, Lcom/policydm/eng/core/XDMVnode;->size:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 986
    .end local v2    # "i":I
    :cond_9
    new-instance v12, Ljava/lang/String;

    invoke-direct {v12, v0}, Ljava/lang/String;-><init>([C)V

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v12

    invoke-static {v12}, Lcom/policydm/eng/core/XDMBase64;->xdmBase64Decode([B)[B

    move-result-object v1

    .line 987
    if-eqz v1, :cond_a

    .line 989
    array-length v12, v1

    new-array v12, v12, [B

    iput-object v12, v9, Lcom/policydm/eng/core/XDMWorkspace;->nextNonce:[B

    .line 990
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_8
    array-length v12, v1

    if-ge v2, v12, :cond_3

    .line 991
    iget-object v12, v9, Lcom/policydm/eng/core/XDMWorkspace;->nextNonce:[B

    aget-byte v13, v1, v2

    aput-byte v13, v12, v2

    .line 990
    add-int/lit8 v2, v2, 0x1

    goto :goto_8

    .line 995
    .end local v2    # "i":I
    :cond_a
    const-string v12, "dValue is NULL"

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1020
    :cond_b
    const/4 v0, 0x0

    goto/16 :goto_4

    .line 1031
    :cond_c
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Server Next Noncenew String(buf) :"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    new-instance v13, Ljava/lang/String;

    invoke-direct {v13, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 1032
    const/4 v1, 0x0

    .line 1033
    new-instance v12, Ljava/lang/String;

    invoke-direct {v12, v0}, Ljava/lang/String;-><init>([C)V

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v12

    invoke-static {v12}, Lcom/policydm/eng/core/XDMBase64;->xdmBase64Decode([B)[B

    move-result-object v1

    .line 1034
    if-eqz v1, :cond_d

    .line 1036
    array-length v12, v1

    new-array v12, v12, [B

    iput-object v12, v9, Lcom/policydm/eng/core/XDMWorkspace;->serverNextNonce:[B

    .line 1037
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_9
    array-length v12, v1

    if-ge v2, v12, :cond_e

    .line 1038
    iget-object v12, v9, Lcom/policydm/eng/core/XDMWorkspace;->serverNextNonce:[B

    aget-byte v13, v1, v2

    aput-byte v13, v12, v2

    .line 1037
    add-int/lit8 v2, v2, 0x1

    goto :goto_9

    .line 1042
    .end local v2    # "i":I
    :cond_d
    const-string v12, "dValue is NULL"

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1051
    :cond_e
    sget-object v12, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v7, v12, Lcom/policydm/eng/core/XDMAccXNode;->m_szAppAddr:Ljava/lang/String;

    .line 1052
    const-string v12, "/Addr"

    invoke-virtual {v7, v12}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1053
    invoke-static {v6, v7}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v5

    .line 1054
    if-eqz v5, :cond_0

    .line 1058
    iget v12, v5, Lcom/policydm/eng/core/XDMVnode;->size:I

    new-array v0, v12, [C

    .line 1059
    iget v12, v5, Lcom/policydm/eng/core/XDMVnode;->size:I

    invoke-static {v6, v7, v11, v0, v12}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmRead(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 1060
    invoke-static {v0}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v9, Lcom/policydm/eng/core/XDMWorkspace;->m_szHostname:Ljava/lang/String;

    .line 1062
    sget-object v12, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v7, v12, Lcom/policydm/eng/core/XDMAccXNode;->m_szAppAddrPort:Ljava/lang/String;

    .line 1063
    const-string v12, "/PortNbr"

    invoke-virtual {v7, v12}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1064
    invoke-static {v6, v7}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v5

    .line 1065
    if-eqz v5, :cond_0

    .line 1069
    iget v12, v5, Lcom/policydm/eng/core/XDMVnode;->size:I

    new-array v0, v12, [C

    .line 1070
    iget v12, v5, Lcom/policydm/eng/core/XDMVnode;->size:I

    invoke-static {v6, v7, v11, v0, v12}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmRead(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 1073
    :try_start_0
    invoke-static {v0}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    iput v12, v9, Lcom/policydm/eng/core/XDMWorkspace;->port:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1080
    :goto_a
    const-string v12, "./DevInfo/DevId"

    invoke-static {v6, v12}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v5

    .line 1081
    if-eqz v5, :cond_0

    .line 1085
    iget v10, v5, Lcom/policydm/eng/core/XDMVnode;->size:I

    new-array v0, v10, [C

    .line 1086
    const-string v10, "./DevInfo/DevId"

    iget v12, v5, Lcom/policydm/eng/core/XDMVnode;->size:I

    invoke-static {v6, v10, v11, v0, v12}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmRead(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 1088
    invoke-static {v0}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, Lcom/policydm/eng/core/XDMWorkspace;->m_szSourceURI:Ljava/lang/String;

    .line 1089
    const/4 v0, 0x0

    move v10, v11

    .line 1091
    goto/16 :goto_0

    .line 1075
    :catch_0
    move-exception v4

    .line 1077
    .local v4, "ne":Ljava/lang/NumberFormatException;
    invoke-virtual {v4}, Ljava/lang/NumberFormatException;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_a
.end method

.method public xdmAgentMakeAcl(Lcom/policydm/eng/core/XDMOmList;Ljava/lang/String;)Lcom/policydm/eng/core/XDMOmList;
    .locals 12
    .param p1, "acllist"    # Lcom/policydm/eng/core/XDMOmList;
    .param p2, "szAcl"    # Ljava/lang/String;

    .prologue
    const/16 v11, 0x26

    const/4 v10, 0x4

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 4208
    const/4 v0, 0x0

    .line 4209
    .local v0, "buf":[C
    const/4 v1, 0x0

    .line 4210
    .local v1, "subbuf":[C
    move-object v3, p2

    .line 4213
    .local v3, "szData":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    new-array v0, v5, [C

    .line 4214
    invoke-virtual {v3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v5

    invoke-static {v5, v11, v0}, Lcom/policydm/eng/core/XDMMem;->xdmLibStrsplit([CC[C)Ljava/lang/String;

    move-result-object v3

    .line 4215
    :goto_0
    if-eqz v0, :cond_2

    .line 4217
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 4218
    array-length v5, v0

    add-int/lit8 v5, v5, -0x1

    aput-char v7, v0, v5

    .line 4219
    :cond_0
    invoke-static {v0}, Lcom/policydm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v4

    .line 4220
    .local v4, "szSub":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    new-array v1, v5, [C

    .line 4221
    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v5

    const/16 v6, 0x3d

    invoke-static {v5, v6, v1}, Lcom/policydm/eng/core/XDMMem;->xdmLibStrsplit([CC[C)Ljava/lang/String;

    move-result-object v4

    .line 4222
    invoke-static {v1}, Lcom/policydm/eng/core/XDMMem;->xdmLibCharToString([C)Ljava/lang/String;

    move-result-object v2

    .line 4224
    .local v2, "szCmd":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 4226
    const-string v5, "Add"

    invoke-virtual {v5, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_3

    .line 4228
    invoke-virtual {p0, p1, v4, v8}, Lcom/policydm/agent/XDMAgent;->xdmAgentAppendAclItem(Lcom/policydm/eng/core/XDMOmList;Ljava/lang/String;I)Lcom/policydm/eng/core/XDMOmList;

    move-result-object p1

    .line 4248
    :cond_1
    :goto_1
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 4260
    .end local v2    # "szCmd":Ljava/lang/String;
    .end local v4    # "szSub":Ljava/lang/String;
    :cond_2
    return-object p1

    .line 4230
    .restart local v2    # "szCmd":Ljava/lang/String;
    .restart local v4    # "szSub":Ljava/lang/String;
    :cond_3
    const-string v5, "Delete"

    invoke-virtual {v5, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_4

    .line 4232
    invoke-virtual {p0, p1, v4, v9}, Lcom/policydm/agent/XDMAgent;->xdmAgentAppendAclItem(Lcom/policydm/eng/core/XDMOmList;Ljava/lang/String;I)Lcom/policydm/eng/core/XDMOmList;

    move-result-object p1

    goto :goto_1

    .line 4234
    :cond_4
    const-string v5, "Replace"

    invoke-virtual {v5, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_5

    .line 4236
    const/16 v5, 0x10

    invoke-virtual {p0, p1, v4, v5}, Lcom/policydm/agent/XDMAgent;->xdmAgentAppendAclItem(Lcom/policydm/eng/core/XDMOmList;Ljava/lang/String;I)Lcom/policydm/eng/core/XDMOmList;

    move-result-object p1

    goto :goto_1

    .line 4238
    :cond_5
    const-string v5, "Get"

    invoke-virtual {v5, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_6

    .line 4240
    const/16 v5, 0x8

    invoke-virtual {p0, p1, v4, v5}, Lcom/policydm/agent/XDMAgent;->xdmAgentAppendAclItem(Lcom/policydm/eng/core/XDMOmList;Ljava/lang/String;I)Lcom/policydm/eng/core/XDMOmList;

    move-result-object p1

    goto :goto_1

    .line 4242
    :cond_6
    const-string v5, "Exec"

    invoke-virtual {v5, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_1

    .line 4244
    invoke-virtual {p0, p1, v4, v10}, Lcom/policydm/agent/XDMAgent;->xdmAgentAppendAclItem(Lcom/policydm/eng/core/XDMOmList;Ljava/lang/String;I)Lcom/policydm/eng/core/XDMOmList;

    move-result-object p1

    goto :goto_1

    .line 4250
    :cond_7
    invoke-virtual {v3, v7}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x61

    if-ne v5, v6, :cond_8

    invoke-virtual {v3, v8}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x6d

    if-ne v5, v6, :cond_8

    invoke-virtual {v3, v9}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x70

    if-ne v5, v6, :cond_8

    const/4 v5, 0x3

    invoke-virtual {v3, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x3b

    if-ne v5, v6, :cond_8

    .line 4253
    invoke-virtual {v3, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 4256
    :cond_8
    const/4 v0, 0x0

    .line 4257
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    new-array v0, v5, [C

    .line 4258
    invoke-virtual {v3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v5

    invoke-static {v5, v11, v0}, Lcom/policydm/eng/core/XDMMem;->xdmLibStrsplit([CC[C)Ljava/lang/String;

    move-result-object v3

    .line 4259
    goto/16 :goto_0
.end method

.method public xdmAgentMakeDevDetailNode()I
    .locals 12

    .prologue
    const/4 v11, 0x1

    .line 1817
    sget-object v8, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    .line 1818
    .local v8, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    iget-object v2, v8, Lcom/policydm/eng/core/XDMWorkspace;->om:Lcom/policydm/eng/core/XDMOmTree;

    .line 1821
    .local v2, "om":Lcom/policydm/eng/core/XDMOmTree;
    const-string v5, ""

    .line 1822
    .local v5, "szHwVersion":Ljava/lang/String;
    const-string v7, ""

    .line 1823
    .local v7, "szSwVersion":Ljava/lang/String;
    const-string v4, ""

    .line 1824
    .local v4, "szFwVersion":Ljava/lang/String;
    const-string v6, ""

    .line 1825
    .local v6, "szOEMName":Ljava/lang/String;
    const-string v3, ""

    .line 1827
    .local v3, "szData":Ljava/lang/String;
    const/16 v0, 0x8

    .line 1829
    .local v0, "aclValue":I
    const-string v9, "./DevDetail"

    invoke-static {v2, v9, v0, v11}, Lcom/policydm/agent/XDMAgent;->xdm_SET_OM_PATH(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1831
    const/16 v0, 0x8

    .line 1832
    const-string v9, "./DevDetail/URI"

    invoke-static {v2, v9, v0, v11}, Lcom/policydm/agent/XDMAgent;->xdm_SET_OM_PATH(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1834
    const/16 v0, 0x8

    .line 1835
    const-string v9, "./DevDetail/URI/MaxDepth"

    const-string v10, "0"

    invoke-static {v2, v9, v10, v0, v11}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1837
    const/16 v0, 0x8

    .line 1838
    const-string v9, "./DevDetail/URI/MaxTotLen"

    const-string v10, "0"

    invoke-static {v2, v9, v10, v0, v11}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1840
    const/16 v0, 0x8

    .line 1841
    const-string v9, "./DevDetail/URI/MaxSegLen"

    const-string v10, "0"

    invoke-static {v2, v9, v10, v0, v11}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1843
    const/16 v0, 0x8

    .line 1844
    const-string v9, "./DevDetail/DevTyp"

    const-string v10, "phone"

    invoke-static {v2, v9, v10, v0, v11}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1846
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetOEM()Ljava/lang/String;

    move-result-object v6

    .line 1847
    const/16 v0, 0x8

    .line 1848
    const-string v9, "./DevDetail/OEM"

    invoke-static {v2, v9, v6, v0, v11}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1850
    invoke-static {}, Lcom/policydm/adapter/XDMDevinfAdapter;->xdmDevAdpGetFirmwareVersion()Ljava/lang/String;

    move-result-object v4

    .line 1851
    const/16 v0, 0x8

    .line 1852
    const-string v9, "./DevDetail/FwV"

    invoke-static {v2, v9, v4, v0, v11}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1854
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetSwV()Ljava/lang/String;

    move-result-object v7

    .line 1855
    const/16 v0, 0x8

    .line 1856
    const-string v9, "./DevDetail/SwV"

    invoke-static {v2, v9, v7, v0, v11}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1858
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetHwV()Ljava/lang/String;

    move-result-object v5

    .line 1859
    const/16 v0, 0x8

    .line 1860
    const-string v9, "./DevDetail/HwV"

    invoke-static {v2, v9, v5, v0, v11}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1862
    const/16 v0, 0x8

    .line 1863
    const-string v9, "./DevDetail/LrgObj"

    const-string v10, "false"

    invoke-static {v2, v9, v10, v0, v11}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1864
    const-string v9, "./DevDetail/LrgObj"

    invoke-static {v2, v9}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v1

    .line 1865
    .local v1, "node":Lcom/policydm/eng/core/XDMVnode;
    if-eqz v1, :cond_0

    .line 1867
    const/4 v9, 0x3

    iput v9, v1, Lcom/policydm/eng/core/XDMVnode;->format:I

    .line 1868
    const/4 v9, 0x0

    iput-object v9, v1, Lcom/policydm/eng/core/XDMVnode;->type:Lcom/policydm/eng/core/XDMOmList;

    .line 1872
    :cond_0
    const/16 v0, 0xa

    .line 1873
    const-string v9, "./DevDetail/Ext"

    invoke-static {v2, v9, v0, v11}, Lcom/policydm/agent/XDMAgent;->xdm_SET_OM_PATH(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1876
    invoke-static {}, Lcom/policydm/adapter/XSPDAdapter;->xspdGetLatestPolicyVersion()Ljava/lang/String;

    move-result-object v3

    .line 1877
    const/16 v0, 0x8

    .line 1878
    const-string v9, "./DevDetail/Ext/PolicyVersion"

    invoke-static {v2, v9, v3, v0, v11}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1881
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetCustomCode()Ljava/lang/String;

    move-result-object v3

    .line 1882
    const/16 v0, 0x8

    .line 1883
    const-string v9, "./DevDetail/Ext/CustCode"

    invoke-static {v2, v9, v3, v0, v11}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1886
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetOSVersion()Ljava/lang/String;

    move-result-object v3

    .line 1887
    const/16 v0, 0x8

    .line 1888
    const-string v9, "./DevDetail/Ext/AndroidV"

    invoke-static {v2, v9, v3, v0, v11}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1891
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetReleaseVer()Ljava/lang/String;

    move-result-object v3

    .line 1892
    const/16 v0, 0x8

    .line 1893
    const-string v9, "./DevDetail/Ext/ClientV"

    invoke-static {v2, v9, v3, v0, v11}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1896
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetDeviceMcc()Ljava/lang/String;

    move-result-object v3

    .line 1897
    const/16 v0, 0x8

    .line 1898
    const-string v9, "./DevDetail/Ext/MCC"

    invoke-static {v2, v9, v3, v0, v11}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1901
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetDeviceMnc()Ljava/lang/String;

    move-result-object v3

    .line 1902
    const/16 v0, 0x8

    .line 1903
    const-string v9, "./DevDetail/Ext/MNC"

    invoke-static {v2, v9, v3, v0, v11}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1906
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetTelephonyMcc()Ljava/lang/String;

    move-result-object v3

    .line 1907
    const/16 v0, 0x8

    .line 1908
    const-string v9, "./DevDetail/Ext/TMCC"

    invoke-static {v2, v9, v3, v0, v11}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1911
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetTelephonyMnc()Ljava/lang/String;

    move-result-object v3

    .line 1912
    const/16 v0, 0x8

    .line 1913
    const-string v9, "./DevDetail/Ext/TMNC"

    invoke-static {v2, v9, v3, v0, v11}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1915
    const/4 v5, 0x0

    .line 1916
    const/4 v7, 0x0

    .line 1917
    const/4 v4, 0x0

    .line 1918
    const/4 v6, 0x0

    .line 1920
    const/4 v9, 0x0

    return v9
.end method

.method public xdmAgentMakeDevInfoNode()I
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 1773
    sget-object v4, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    .line 1774
    .local v4, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    iget-object v3, v4, Lcom/policydm/eng/core/XDMWorkspace;->om:Lcom/policydm/eng/core/XDMOmTree;

    .line 1777
    .local v3, "om":Lcom/policydm/eng/core/XDMOmTree;
    const/16 v0, 0xb

    .line 1778
    .local v0, "aclValue":I
    const-string v5, "./DevInfo"

    invoke-static {v3, v5, v0, v7}, Lcom/policydm/agent/XDMAgent;->xdm_SET_OM_PATH(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1780
    const/16 v0, 0x8

    .line 1781
    const-string v5, "./DevInfo/DevId"

    invoke-static {}, Lcom/policydm/adapter/XDMDevinfAdapter;->xdmDevAdpGetFullDeviceID()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v5, v6, v0, v7}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1783
    const/16 v0, 0xb

    .line 1784
    const-string v5, "./DevInfo/Man"

    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetManufacturer()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v5, v6, v0, v7}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1786
    const/16 v0, 0xb

    .line 1787
    const-string v5, "./DevInfo/Mod"

    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetModel()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v5, v6, v0, v7}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1789
    const/16 v0, 0xb

    .line 1797
    const-string v5, "./DevInfo/DmV"

    const-string v6, " 1.2"

    invoke-static {v3, v5, v6, v0, v7}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1800
    const/16 v0, 0xb

    .line 1801
    const-string v5, "./DevInfo/Lang"

    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetLanguage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v5, v6, v0, v7}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1804
    const/16 v0, 0xb

    .line 1805
    const-string v5, "./SEAndroidPolicy"

    invoke-static {v3, v5, v0, v7}, Lcom/policydm/agent/XDMAgent;->xdm_SET_OM_PATH(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1807
    invoke-static {}, Lcom/policydm/db/XDBProfileListAdp;->xdbGetNotiJobId()J

    move-result-wide v1

    .line 1808
    .local v1, "nJobId":J
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "nJobId = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 1809
    const/16 v0, 0x8

    .line 1810
    const-string v5, "./SEAndroidPolicy/PushJobId"

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v5, v6, v0, v7}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1812
    const/4 v5, 0x0

    return v5
.end method

.method public xdmAgentMakeFwUpdateNode()I
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x2

    .line 1925
    sget-object v6, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    .line 1926
    .local v6, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    iget-object v1, v6, Lcom/policydm/eng/core/XDMWorkspace;->om:Lcom/policydm/eng/core/XDMOmTree;

    .line 1930
    .local v1, "om":Lcom/policydm/eng/core/XDMOmTree;
    const-string v4, ""

    .line 1932
    .local v4, "szFUMOPackageNode":Ljava/lang/String;
    const-string v7, "Initialize"

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1934
    const-string v5, "./SEAndroidPolicy"

    .line 1935
    .local v5, "szFUMORoot":Ljava/lang/String;
    const/16 v0, 0x8

    .line 1936
    .local v0, "aclValue":I
    invoke-static {v1, v5, v0, v9}, Lcom/policydm/agent/XDMAgent;->xdm_SET_OM_PATH(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1938
    move-object v4, v5

    .line 1940
    const-string v2, "pFUMOPackageNode :"

    .line 1941
    .local v2, "szDbg":Ljava/lang/String;
    invoke-virtual {v2, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1942
    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1943
    const/16 v0, 0x19

    .line 1944
    invoke-static {v1, v4, v0, v9}, Lcom/policydm/agent/XDMAgent;->xdm_SET_OM_PATH(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1946
    const-string v7, "/PkgName"

    invoke-virtual {v4, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1947
    .local v3, "szFUMONode":Ljava/lang/String;
    const/16 v0, 0x18

    .line 1948
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMOPkgName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v3, v7, v0, v8}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1950
    const-string v7, "/PkgVersion"

    invoke-virtual {v4, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1951
    const/16 v0, 0x18

    .line 1952
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMOPkgVersion()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v3, v7, v0, v8}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1954
    const-string v7, "/PkgSize"

    invoke-virtual {v4, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1955
    const/16 v0, 0x18

    .line 1956
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMOObjectSize()I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v3, v7, v0, v8}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1958
    const-string v7, "/PkgDesc"

    invoke-virtual {v4, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1959
    const/16 v0, 0x18

    .line 1960
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMODescription()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v3, v7, v0, v8}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1962
    const-string v7, "/DownloadAndUpdate"

    invoke-virtual {v4, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1963
    const/16 v0, 0xc

    .line 1964
    invoke-static {v1, v3, v0, v8}, Lcom/policydm/agent/XDMAgent;->xdm_SET_OM_PATH(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1966
    const-string v7, "/PkgURL"

    invoke-virtual {v3, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1967
    const/16 v0, 0x18

    .line 1968
    const-string v7, " "

    invoke-static {v1, v3, v7, v0, v8}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1970
    const-string v7, "/DownloadAndUpdate"

    invoke-virtual {v4, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1971
    const-string v7, "/ProcessId"

    invoke-virtual {v3, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1972
    const/16 v0, 0x18

    .line 1973
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMOProcessId()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v3, v7, v0, v8}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1975
    const-string v7, "/DownloadAndUpdate"

    invoke-virtual {v4, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1976
    const-string v7, "/ReportURL"

    invoke-virtual {v3, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1977
    const/16 v0, 0x18

    .line 1978
    const-string v7, " "

    invoke-static {v1, v3, v7, v0, v8}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1980
    const-string v7, "/State"

    invoke-virtual {v4, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1981
    const/16 v0, 0x8

    .line 1982
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMOStatus()I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v3, v7, v0, v8}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1984
    const-string v7, "/Ext"

    invoke-virtual {v4, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1985
    const/16 v0, 0x8

    .line 1986
    const-string v7, " "

    invoke-static {v1, v3, v7, v0, v8}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1988
    const/4 v7, 0x0

    return v7
.end method

.method public xdmAgentMakeNode()I
    .locals 3

    .prologue
    .line 650
    sget-object v1, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    .line 651
    .local v1, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    iget-object v0, v1, Lcom/policydm/eng/core/XDMWorkspace;->om:Lcom/policydm/eng/core/XDMOmTree;

    .line 653
    .local v0, "om":Lcom/policydm/eng/core/XDMOmTree;
    invoke-static {v0}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmInit(Lcom/policydm/eng/core/XDMOmTree;)I

    move-result v2

    if-eqz v2, :cond_0

    .line 655
    const/4 v2, -0x1

    .line 669
    :goto_0
    return v2

    .line 658
    :cond_0
    const-string v2, "*"

    invoke-static {v0, v2}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmSetServerId(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)I

    .line 660
    invoke-virtual {p0}, Lcom/policydm/agent/XDMAgent;->xdmAgentMakeSyncMLNode()V

    .line 661
    invoke-virtual {p0}, Lcom/policydm/agent/XDMAgent;->xdmAgentMakeDevInfoNode()I

    .line 662
    invoke-virtual {p0}, Lcom/policydm/agent/XDMAgent;->xdmAgentMakeDevDetailNode()I

    .line 666
    invoke-virtual {p0}, Lcom/policydm/agent/XDMAgent;->xdmAgentMakeFwUpdateNode()I

    .line 669
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public xdmAgentMakeSyncMLNode()V
    .locals 11

    .prologue
    const/4 v9, 0x0

    const/4 v10, 0x1

    const/4 v5, 0x2

    .line 1350
    sget-object v8, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    .line 1351
    .local v8, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    iget-object v1, v8, Lcom/policydm/eng/core/XDMWorkspace;->om:Lcom/policydm/eng/core/XDMOmTree;

    .line 1353
    .local v1, "om":Lcom/policydm/eng/core/XDMOmTree;
    const-string v2, ""

    .line 1354
    .local v2, "szAccBuf":Ljava/lang/String;
    const/4 v6, 0x0

    .line 1355
    .local v6, "authType":I
    const-string v3, ""

    .line 1359
    .local v3, "szTempBuf":Ljava/lang/String;
    const/16 v4, 0x9

    .line 1360
    .local v4, "aclValue":I
    const-string v0, "."

    invoke-static {v1, v0, v4, v10}, Lcom/policydm/agent/XDMAgent;->xdmAgentMakeDefaultAcl(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1363
    const/16 v4, 0x1b

    .line 1364
    const-string v0, "./SyncML"

    invoke-static {v1, v0, v4, v10}, Lcom/policydm/agent/XDMAgent;->xdm_SET_OM_PATH(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1367
    const/16 v4, 0x1b

    .line 1368
    const-string v0, "."

    invoke-static {v1, v0, v4, v10}, Lcom/policydm/agent/XDMAgent;->xdm_SET_OM_PATH(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1370
    const/16 v4, 0x1b

    .line 1371
    const-string v0, "./DMAcc"

    invoke-static {v1, v0, v4, v10}, Lcom/policydm/agent/XDMAgent;->xdm_SET_OM_PATH(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1374
    const/16 v4, 0x1b

    .line 1375
    const-string v0, "./SyncML/Con"

    invoke-static {v1, v0, v4, v10}, Lcom/policydm/agent/XDMAgent;->xdm_SET_OM_PATH(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1377
    const/16 v4, 0x1b

    .line 1378
    sget-object v0, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v0, v0, Lcom/policydm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    invoke-static {v1, v0, v4, v5}, Lcom/policydm/agent/XDMAgent;->xdm_SET_OM_PATH(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1380
    const-string v3, "w7"

    .line 1381
    sget-object v0, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/policydm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 1382
    const-string v0, "/AppId"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1383
    invoke-static {v1, v2, v3, v4, v5}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1385
    invoke-static {}, Lcom/policydm/db/XDBProfileAdp;->xdbGetServerID()Ljava/lang/String;

    move-result-object v3

    .line 1386
    sget-object v0, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/policydm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 1387
    const-string v0, "/ServerID"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1388
    const/16 v4, 0x1b

    .line 1389
    invoke-static {v1, v2, v3, v4, v5}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1391
    invoke-static {}, Lcom/policydm/db/XDBProfileAdp;->xdbGetProfileName()Ljava/lang/String;

    move-result-object v3

    .line 1392
    const/16 v4, 0x1b

    .line 1393
    sget-object v0, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/policydm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 1394
    const-string v0, "/Name"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1395
    invoke-static {v1, v2, v3, v4, v5}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1397
    invoke-static {}, Lcom/policydm/db/XDBProfileAdp;->xdbGetPrefConRef()Ljava/lang/String;

    move-result-object v3

    .line 1398
    const/16 v4, 0x1b

    .line 1399
    sget-object v0, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/policydm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 1400
    const-string v0, "/PrefConRef"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1401
    invoke-static {v1, v2, v3, v4, v5}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1403
    const/16 v4, 0x1b

    .line 1404
    sget-object v0, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/policydm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 1405
    const-string v0, "/ToConRef"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1406
    invoke-static {v1, v2, v4, v5}, Lcom/policydm/agent/XDMAgent;->xdm_SET_OM_PATH(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1408
    const/16 v4, 0x1b

    .line 1409
    sget-object v0, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/policydm/eng/core/XDMAccXNode;->m_szToConRef:Ljava/lang/String;

    .line 1410
    invoke-static {v1, v2, v4, v5}, Lcom/policydm/agent/XDMAgent;->xdm_SET_OM_PATH(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1412
    const/16 v4, 0x1b

    .line 1413
    sget-object v0, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/policydm/eng/core/XDMAccXNode;->m_szToConRef:Ljava/lang/String;

    .line 1414
    const-string v0, "/ConRef"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1415
    const-string v0, "dataProxy"

    invoke-static {v1, v2, v0, v4, v5}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1417
    const/16 v4, 0x1b

    .line 1418
    sget-object v0, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/policydm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 1419
    const-string v0, "/AppAddr"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1420
    invoke-static {v1, v2, v4, v5}, Lcom/policydm/agent/XDMAgent;->xdm_SET_OM_PATH(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1422
    const/16 v4, 0x1b

    .line 1423
    sget-object v0, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/policydm/eng/core/XDMAccXNode;->m_szAppAddr:Ljava/lang/String;

    .line 1424
    invoke-static {v1, v2, v4, v5}, Lcom/policydm/agent/XDMAgent;->xdm_SET_OM_PATH(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1426
    invoke-static {v9}, Lcom/policydm/db/XDB;->xdbGetServerUrl(I)Ljava/lang/String;

    move-result-object v3

    .line 1428
    const/16 v4, 0x1b

    .line 1429
    sget-object v0, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/policydm/eng/core/XDMAccXNode;->m_szAppAddr:Ljava/lang/String;

    .line 1430
    const-string v0, "/Addr"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1431
    invoke-static {v1, v2, v3, v4, v5}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1433
    const/16 v4, 0x1b

    .line 1434
    sget-object v0, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/policydm/eng/core/XDMAccXNode;->m_szAppAddr:Ljava/lang/String;

    .line 1435
    const-string v0, "/AddrType"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1436
    const-string v0, "URI"

    invoke-static {v1, v2, v0, v4, v5}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1438
    const/16 v4, 0x1b

    .line 1439
    sget-object v0, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/policydm/eng/core/XDMAccXNode;->m_szAppAddr:Ljava/lang/String;

    .line 1440
    const-string v0, "/Port"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1441
    invoke-static {v1, v2, v4, v5}, Lcom/policydm/agent/XDMAgent;->xdm_SET_OM_PATH(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1443
    const/16 v4, 0x1b

    .line 1444
    sget-object v0, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/policydm/eng/core/XDMAccXNode;->m_szAppAddrPort:Ljava/lang/String;

    .line 1445
    invoke-static {v1, v2, v4, v5}, Lcom/policydm/agent/XDMAgent;->xdm_SET_OM_PATH(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1447
    invoke-static {v9}, Lcom/policydm/db/XDB;->xdbGetServerPort(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 1448
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "ServerPort = "

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 1449
    const/16 v4, 0x1b

    .line 1450
    sget-object v0, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/policydm/eng/core/XDMAccXNode;->m_szAppAddrPort:Ljava/lang/String;

    .line 1451
    const-string v0, "/PortNbr"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1452
    invoke-static {v1, v2, v3, v4, v5}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1454
    invoke-static {}, Lcom/policydm/db/XDBProfileAdp;->xdbGetAuthType()I

    move-result v6

    .line 1455
    const/16 v4, 0x1b

    .line 1456
    sget-object v0, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/policydm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 1457
    const-string v0, "/AAuthPref"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1458
    invoke-static {v6}, Lcom/policydm/eng/core/XDMAuth;->xdmAuthCredType2String(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0, v4, v5}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1460
    const/16 v4, 0x1b

    .line 1461
    sget-object v0, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/policydm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 1462
    const-string v0, "/AppAuth"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1463
    invoke-static {v1, v2, v4, v5}, Lcom/policydm/agent/XDMAgent;->xdm_SET_OM_PATH(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1465
    const/16 v4, 0x1b

    .line 1466
    sget-object v0, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/policydm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 1467
    invoke-static {v1, v2, v4, v5}, Lcom/policydm/agent/XDMAgent;->xdm_SET_OM_PATH(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1469
    invoke-static {}, Lcom/policydm/db/XDBProfileAdp;->xdbGetAuthLevel()Ljava/lang/String;

    move-result-object v3

    .line 1470
    const/16 v4, 0x1b

    .line 1471
    sget-object v0, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/policydm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 1472
    const-string v0, "/AAuthLevel"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1473
    invoke-static {v1, v2, v3, v4, v5}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1475
    invoke-static {}, Lcom/policydm/db/XDBProfileAdp;->xdbGetAuthType()I

    move-result v6

    .line 1476
    const/16 v4, 0x1b

    .line 1477
    sget-object v0, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/policydm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 1478
    const-string v0, "/AAuthType"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1479
    invoke-static {v6}, Lcom/policydm/eng/core/XDMAuth;->xdmAuthAAuthType2String(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0, v4, v5}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1481
    invoke-static {}, Lcom/policydm/db/XDBProfileAdp;->xdbGetUsername()Ljava/lang/String;

    move-result-object v3

    .line 1482
    const/16 v4, 0x1b

    .line 1483
    sget-object v0, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/policydm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 1484
    const-string v0, "/AAuthName"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1485
    invoke-static {v1, v2, v3, v4, v5}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1487
    invoke-static {}, Lcom/policydm/db/XDBProfileAdp;->xdbGetClientPassword()Ljava/lang/String;

    move-result-object v3

    .line 1488
    const/16 v4, 0x1b

    .line 1489
    sget-object v0, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/policydm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 1490
    const-string v0, "/AAuthSecret"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1491
    invoke-static {v1, v2, v3, v4, v5}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1493
    invoke-static {}, Lcom/policydm/db/XDBProfileAdp;->xdbGetClientNonce()Ljava/lang/String;

    move-result-object v3

    .line 1494
    invoke-direct {p0, v3}, Lcom/policydm/agent/XDMAgent;->xdmAgentCheckNonce(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1496
    const/16 v4, 0x1b

    .line 1497
    sget-object v0, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/policydm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 1498
    const-string v0, "/AAuthData"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    .line 1499
    invoke-virtual/range {v0 .. v5}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccB64(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1501
    const/16 v4, 0x1b

    .line 1502
    sget-object v0, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/policydm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    .line 1503
    invoke-static {v1, v2, v4, v5}, Lcom/policydm/agent/XDMAgent;->xdm_SET_OM_PATH(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1505
    invoke-static {}, Lcom/policydm/db/XDBProfileAdp;->xdbGetServerAuthLevel()Ljava/lang/String;

    move-result-object v3

    .line 1506
    const/16 v4, 0x1b

    .line 1507
    sget-object v0, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/policydm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    .line 1508
    const-string v0, "/AAuthLevel"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1509
    invoke-static {v1, v2, v3, v4, v5}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1511
    invoke-static {}, Lcom/policydm/db/XDBProfileAdp;->xdbGetServerAuthType()I

    move-result v6

    .line 1512
    const/16 v4, 0x1b

    .line 1513
    sget-object v0, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/policydm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    .line 1514
    const-string v0, "/AAuthType"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1515
    invoke-static {v6}, Lcom/policydm/eng/core/XDMAuth;->xdmAuthAAuthType2String(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0, v4, v5}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1517
    invoke-static {}, Lcom/policydm/db/XDBProfileAdp;->xdbGetServerID()Ljava/lang/String;

    move-result-object v3

    .line 1518
    const/16 v4, 0x1b

    .line 1519
    sget-object v0, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/policydm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    .line 1520
    const-string v0, "/AAuthName"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1521
    invoke-static {v1, v2, v3, v4, v5}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1523
    invoke-static {}, Lcom/policydm/db/XDBProfileAdp;->xdbGetServerPassword()Ljava/lang/String;

    move-result-object v3

    .line 1524
    const/16 v4, 0x1b

    .line 1525
    sget-object v0, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/policydm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    .line 1526
    const-string v0, "/AAuthSecret"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1527
    invoke-static {v1, v2, v3, v4, v5}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1529
    invoke-static {}, Lcom/policydm/db/XDBProfileAdp;->xdbGetServerNonce()Ljava/lang/String;

    move-result-object v3

    .line 1530
    invoke-direct {p0, v3}, Lcom/policydm/agent/XDMAgent;->xdmAgentCheckNonce(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1531
    const/16 v4, 0x1b

    .line 1532
    sget-object v0, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/policydm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    .line 1533
    const-string v0, "/AAuthData"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    .line 1534
    invoke-virtual/range {v0 .. v5}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccB64(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1536
    const/16 v4, 0x1b

    .line 1537
    sget-object v0, Lcom/policydm/agent/XDMAgent;->m_DmAccXNodeInfo:Lcom/policydm/eng/core/XDMAccXNode;

    iget-object v2, v0, Lcom/policydm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 1538
    const-string v0, "/Ext"

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1539
    const-string v0, " "

    invoke-static {v1, v2, v0, v4, v5}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMAccStr(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1541
    const/16 v0, 0xa

    invoke-static {v0}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFGetMOPath(I)Ljava/lang/String;

    move-result-object v7

    .line 1542
    .local v7, "szInbox":Ljava/lang/String;
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1544
    const/16 v4, 0x1b

    .line 1545
    invoke-static {v1, v7, v4, v10}, Lcom/policydm/agent/XDMAgent;->xdm_SET_OM_PATH(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1663
    :cond_0
    return-void
.end method

.method public xdmAgentMakeTndsSubTree(Lcom/policydm/eng/core/XDMOmTree;Lcom/policydm/eng/core/XDMVnode;ILjava/lang/String;)V
    .locals 20
    .param p1, "om"    # Lcom/policydm/eng/core/XDMOmTree;
    .param p2, "node"    # Lcom/policydm/eng/core/XDMVnode;
    .param p3, "nFlag"    # I
    .param p4, "szPath"    # Ljava/lang/String;

    .prologue
    .line 3632
    const/4 v11, 0x0

    .line 3633
    .local v11, "nLen":I
    const/4 v5, 0x0

    .line 3634
    .local v5, "ac":I
    const/16 v16, 0x0

    .line 3635
    .local v16, "szTag":Ljava/lang/String;
    const/4 v12, 0x0

    .line 3636
    .local v12, "szData":Ljava/lang/String;
    const/4 v13, 0x0

    .line 3637
    .local v13, "szFormat":Ljava/lang/String;
    const/4 v14, 0x0

    .line 3638
    .local v14, "szNodeProperty":Ljava/lang/String;
    const/16 v17, 0x0

    .line 3639
    .local v17, "szType":Ljava/lang/String;
    const/4 v15, 0x0

    .line 3641
    .local v15, "szNodeUri":Ljava/lang/String;
    const/4 v8, 0x0

    .line 3642
    .local v8, "cur":Lcom/policydm/eng/core/XDMVnode;
    const/4 v6, 0x0

    .line 3643
    .local v6, "acl":Lcom/policydm/eng/core/XDMOmAcl;
    new-instance v9, Lcom/policydm/eng/core/XDMOmList;

    invoke-direct {v9}, Lcom/policydm/eng/core/XDMOmList;-><init>()V

    .line 3644
    .local v9, "list":Lcom/policydm/eng/core/XDMOmList;
    const/4 v10, 0x0

    .line 3646
    .local v10, "nFileId":I
    if-nez p2, :cond_0

    .line 3838
    :goto_0
    return-void

    .line 3650
    :cond_0
    move-object/from16 v0, p2

    iget-object v8, v0, Lcom/policydm/eng/core/XDMVnode;->childlist:Lcom/policydm/eng/core/XDMVnode;

    .line 3651
    invoke-static {}, Lcom/policydm/db/XDB;->xdbGetFileIdTNDS()I

    move-result v10

    .line 3653
    sget-object v18, Lcom/policydm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    const/16 v19, 0x4

    aget-object v16, v18, v19

    .line 3655
    move-object/from16 v14, v16

    .line 3657
    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_1

    .line 3659
    sget-object v18, Lcom/policydm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    const/16 v19, 0x8

    aget-object v16, v18, v19

    .line 3660
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "szTag : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 3661
    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 3662
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "szPath : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 3663
    move-object/from16 v0, p4

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 3664
    sget-object v18, Lcom/policydm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    const/16 v19, 0x9

    aget-object v16, v18, v19

    .line 3665
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "szTag"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 3666
    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 3668
    :cond_1
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/policydm/eng/core/XDMVnode;->m_szName:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_2

    .line 3670
    sget-object v18, Lcom/policydm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    const/16 v19, 0x6

    aget-object v16, v18, v19

    .line 3671
    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 3672
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/policydm/eng/core/XDMVnode;->m_szName:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 3673
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "node.name : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/policydm/eng/core/XDMVnode;->m_szName:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 3674
    sget-object v18, Lcom/policydm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    const/16 v19, 0x7

    aget-object v16, v18, v19

    .line 3675
    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 3678
    :cond_2
    move-object/from16 v0, p2

    iget v0, v0, Lcom/policydm/eng/core/XDMVnode;->format:I

    move/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/policydm/eng/core/XDMOmList;->xdmOmGetFormatString(I)Ljava/lang/String;

    move-result-object v13

    .line 3680
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_3

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/policydm/eng/core/XDMVnode;->acl:Lcom/policydm/eng/core/XDMOmList;

    move-object/from16 v18, v0

    if-nez v18, :cond_3

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/policydm/eng/core/XDMVnode;->type:Lcom/policydm/eng/core/XDMOmList;

    move-object/from16 v18, v0

    if-eqz v18, :cond_c

    .line 3682
    :cond_3
    sget-object v18, Lcom/policydm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    const/16 v19, 0xc

    aget-object v16, v18, v19

    .line 3683
    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 3685
    and-int/lit8 v18, p3, 0x2

    const/16 v19, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_4

    .line 3687
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_4

    .line 3689
    sget-object v18, Lcom/policydm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    const/16 v19, 0x10

    aget-object v16, v18, v19

    .line 3690
    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 3691
    const-string v18, "<"

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 3692
    invoke-virtual {v14, v13}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 3693
    const-string v18, "/>"

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 3694
    sget-object v18, Lcom/policydm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    const/16 v19, 0x11

    aget-object v16, v18, v19

    .line 3695
    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 3699
    :cond_4
    and-int/lit8 v18, p3, 0x4

    const/16 v19, 0x4

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_5

    .line 3701
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/policydm/eng/core/XDMVnode;->type:Lcom/policydm/eng/core/XDMOmList;

    move-object/from16 v18, v0

    if-eqz v18, :cond_5

    .line 3703
    move-object/from16 v0, p2

    iget-object v9, v0, Lcom/policydm/eng/core/XDMVnode;->type:Lcom/policydm/eng/core/XDMOmList;

    .line 3704
    iget-object v0, v9, Lcom/policydm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    move-object/from16 v17, v0

    .end local v17    # "szType":Ljava/lang/String;
    check-cast v17, Ljava/lang/String;

    .line 3706
    .restart local v17    # "szType":Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_5

    .line 3708
    sget-object v18, Lcom/policydm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    const/16 v19, 0x12

    aget-object v16, v18, v19

    .line 3709
    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 3710
    const-string v18, "<MIME>"

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 3711
    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 3712
    const-string v18, "</MIME>"

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 3713
    sget-object v18, Lcom/policydm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    const/16 v19, 0x13

    aget-object v16, v18, v19

    .line 3714
    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 3718
    :cond_5
    const/4 v9, 0x0

    .line 3720
    and-int/lit8 v18, p3, 0x1

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_b

    .line 3722
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/policydm/eng/core/XDMVnode;->acl:Lcom/policydm/eng/core/XDMOmList;

    move-object/from16 v18, v0

    if-eqz v18, :cond_b

    .line 3724
    move-object/from16 v0, p2

    iget-object v9, v0, Lcom/policydm/eng/core/XDMVnode;->acl:Lcom/policydm/eng/core/XDMOmList;

    .line 3725
    iget-object v6, v9, Lcom/policydm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    .end local v6    # "acl":Lcom/policydm/eng/core/XDMOmAcl;
    check-cast v6, Lcom/policydm/eng/core/XDMOmAcl;

    .line 3727
    .restart local v6    # "acl":Lcom/policydm/eng/core/XDMOmAcl;
    if-eqz v6, :cond_b

    .line 3729
    iget v5, v6, Lcom/policydm/eng/core/XDMOmAcl;->ac:I

    .line 3730
    if-eqz v5, :cond_b

    .line 3732
    const/4 v4, 0x0

    .line 3733
    .local v4, "IsOtherACL":Z
    sget-object v18, Lcom/policydm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    const/16 v19, 0xe

    aget-object v16, v18, v19

    .line 3734
    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 3735
    and-int/lit8 v18, v5, 0x1

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_6

    .line 3737
    const-string v18, "Add=*"

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 3738
    const/4 v4, 0x1

    .line 3740
    :cond_6
    and-int/lit8 v18, v5, 0x2

    const/16 v19, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_7

    .line 3742
    if-eqz v4, :cond_e

    .line 3744
    const-string v18, "&amp;Delete=*"

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 3752
    :cond_7
    :goto_1
    and-int/lit8 v18, v5, 0x4

    const/16 v19, 0x4

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_8

    .line 3754
    if-eqz v4, :cond_f

    .line 3756
    const-string v18, "&amp;Exec=*"

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 3764
    :cond_8
    :goto_2
    and-int/lit8 v18, v5, 0x8

    const/16 v19, 0x8

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_9

    .line 3766
    if-eqz v4, :cond_10

    .line 3768
    const-string v18, "&amp;Get=*"

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 3776
    :cond_9
    :goto_3
    and-int/lit8 v18, v5, 0x10

    const/16 v19, 0x10

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_a

    .line 3778
    if-eqz v4, :cond_11

    .line 3780
    const-string v18, "&amp;Replace=*"

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 3788
    :cond_a
    :goto_4
    sget-object v18, Lcom/policydm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    const/16 v19, 0xf

    aget-object v16, v18, v19

    .line 3789
    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 3795
    .end local v4    # "IsOtherACL":Z
    :cond_b
    sget-object v18, Lcom/policydm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    const/16 v19, 0xd

    aget-object v16, v18, v19

    .line 3796
    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 3799
    :cond_c
    and-int/lit8 v18, p3, 0x8

    const/16 v19, 0x8

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_13

    .line 3801
    move-object/from16 v0, p2

    iget v0, v0, Lcom/policydm/eng/core/XDMVnode;->size:I

    move/from16 v18, v0

    if-lez v18, :cond_12

    .line 3803
    sget-object v18, Lcom/policydm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    const/16 v19, 0xa

    aget-object v16, v18, v19

    .line 3804
    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 3805
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v10, v0}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpAppendFile(I[B)I

    .line 3807
    const-string v12, ""

    .line 3808
    invoke-virtual/range {p0 .. p2}, Lcom/policydm/agent/XDMAgent;->xdmAgentGetPathFromNode(Lcom/policydm/eng/core/XDMOmTree;Lcom/policydm/eng/core/XDMVnode;)Ljava/lang/String;

    move-result-object v15

    .line 3809
    move-object/from16 v0, p2

    iget v0, v0, Lcom/policydm/eng/core/XDMVnode;->size:I

    move/from16 v18, v0

    move/from16 v0, v18

    new-array v7, v0, [C

    .line 3810
    .local v7, "cTemp":[C
    const/16 v18, 0x0

    move-object/from16 v0, p2

    iget v0, v0, Lcom/policydm/eng/core/XDMVnode;->size:I

    move/from16 v19, v0

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v0, v15, v1, v7, v2}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmRead(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    move-result v11

    .line 3811
    invoke-static {v7}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v12

    .line 3812
    if-lez v11, :cond_d

    .line 3814
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v12, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v10, v0}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpAppendFile(I[B)I

    .line 3816
    :cond_d
    sget-object v18, Lcom/policydm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    const/16 v19, 0xb

    aget-object v16, v18, v19

    .line 3817
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v18

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v10, v0}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpAppendFile(I[B)I

    .line 3829
    .end local v7    # "cTemp":[C
    :goto_5
    if-eqz v8, :cond_14

    .line 3831
    const/16 v18, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p3

    move-object/from16 v3, v18

    invoke-virtual {v0, v1, v8, v2, v3}, Lcom/policydm/agent/XDMAgent;->xdmAgentMakeTndsSubTree(Lcom/policydm/eng/core/XDMOmTree;Lcom/policydm/eng/core/XDMVnode;ILjava/lang/String;)V

    .line 3832
    iget-object v8, v8, Lcom/policydm/eng/core/XDMVnode;->next:Lcom/policydm/eng/core/XDMVnode;

    goto :goto_5

    .line 3748
    .restart local v4    # "IsOtherACL":Z
    :cond_e
    const-string v18, "Delete=*"

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 3749
    const/4 v4, 0x1

    goto/16 :goto_1

    .line 3760
    :cond_f
    const-string v18, "Exec=*"

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 3761
    const/4 v4, 0x1

    goto/16 :goto_2

    .line 3772
    :cond_10
    const-string v18, "Get=*"

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 3773
    const/4 v4, 0x1

    goto/16 :goto_3

    .line 3784
    :cond_11
    const-string v18, "Replace=*"

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 3785
    const/4 v4, 0x1

    goto/16 :goto_4

    .line 3821
    .end local v4    # "IsOtherACL":Z
    :cond_12
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v10, v0}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpAppendFile(I[B)I

    goto :goto_5

    .line 3826
    :cond_13
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v10, v0}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpAppendFile(I[B)I

    goto :goto_5

    .line 3835
    :cond_14
    sget-object v18, Lcom/policydm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    const/16 v19, 0x5

    aget-object v16, v18, v19

    .line 3837
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v18

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v10, v0}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpAppendFile(I[B)I

    goto/16 :goto_0
.end method

.method public xdmAgentMgmtPackage(Lcom/policydm/eng/core/XDMEncoder;)I
    .locals 6
    .param p1, "e"    # Lcom/policydm/eng/core/XDMEncoder;

    .prologue
    const/4 v2, -0x1

    const/4 v5, -0x3

    const/4 v3, 0x0

    .line 1296
    sget-object v1, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    .line 1298
    .local v1, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    iget-boolean v4, v1, Lcom/policydm/eng/core/XDMWorkspace;->dataBuffered:Z

    if-eqz v4, :cond_1

    .line 1300
    const-string v4, "1222"

    invoke-virtual {p0, p1, v4}, Lcom/policydm/agent/XDMAgent;->xdmAgentCreatePackageAlert(Lcom/policydm/eng/core/XDMEncoder;Ljava/lang/String;)I

    move-result v0

    .line 1301
    .local v0, "res":I
    if-eqz v0, :cond_1

    .line 1303
    if-ne v0, v5, :cond_0

    .line 1305
    iput-boolean v3, v1, Lcom/policydm/eng/core/XDMWorkspace;->endOfMsg:Z

    .line 1343
    :goto_0
    return v2

    .line 1309
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "failed = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 1314
    .end local v0    # "res":I
    :cond_1
    invoke-virtual {p0, p1}, Lcom/policydm/agent/XDMAgent;->xdmAgentCreatePackageStatus(Lcom/policydm/eng/core/XDMEncoder;)I

    move-result v0

    .line 1315
    .restart local v0    # "res":I
    if-eqz v0, :cond_3

    .line 1317
    if-ne v0, v5, :cond_2

    .line 1319
    iput-boolean v3, v1, Lcom/policydm/eng/core/XDMWorkspace;->endOfMsg:Z

    goto :goto_0

    .line 1323
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "failed = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 1327
    :cond_3
    invoke-virtual {p0, p1}, Lcom/policydm/agent/XDMAgent;->xdmAgentCreatePackageResults(Lcom/policydm/eng/core/XDMEncoder;)I

    move-result v0

    .line 1328
    if-eqz v0, :cond_5

    .line 1330
    if-ne v0, v5, :cond_4

    .line 1332
    iput-boolean v3, v1, Lcom/policydm/eng/core/XDMWorkspace;->endOfMsg:Z

    goto :goto_0

    .line 1336
    :cond_4
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "failed = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 1341
    :cond_5
    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->endOfMsg:Z

    move v2, v3

    .line 1343
    goto :goto_0
.end method

.method public xdmAgentSendDMReport()I
    .locals 14

    .prologue
    const/16 v13, 0x10

    const/4 v12, 0x0

    const/4 v11, 0x0

    .line 542
    const-string v2, ""

    .line 543
    .local v2, "szHmacData":Ljava/lang/String;
    const-string v3, ""

    .line 544
    .local v3, "szContentRange":Ljava/lang/String;
    const/4 v8, 0x0

    .line 546
    .local v8, "ret":I
    invoke-static {v12}, Lcom/policydm/db/XDB;->xdbGetServerUrl(I)Ljava/lang/String;

    move-result-object v1

    .line 550
    .local v1, "szResURL":Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/policydm/agent/XDMAgent;->m_HttpDMAdapter:Lcom/policydm/tp/XTPAdapter;

    const-string v4, "POST"

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/policydm/tp/XTPAdapter;->xtpAdpSetHttpObj(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v8

    .line 561
    const/4 v0, 0x4

    if-ne v8, v0, :cond_0

    .line 563
    const/4 v0, 0x1

    sput-boolean v0, Lcom/policydm/agent/XDMAgent;->m_bPendingStatus:Z

    :goto_0
    move v9, v8

    .line 598
    .end local v8    # "ret":I
    .local v9, "ret":I
    :goto_1
    return v9

    .line 552
    .end local v9    # "ret":I
    .restart local v8    # "ret":I
    :catch_0
    move-exception v7

    .line 554
    .local v7, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v7}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 555
    invoke-static {}, Lcom/policydm/tp/XTPNetSendTimer;->xtpEndTimer()V

    .line 556
    const/16 v8, -0xd

    .line 557
    invoke-static {v13, v11, v11}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    move v9, v8

    .line 558
    .end local v8    # "ret":I
    .restart local v9    # "ret":I
    goto :goto_1

    .line 567
    .end local v7    # "e":Ljava/lang/NullPointerException;
    .end local v9    # "ret":I
    .restart local v8    # "ret":I
    :cond_0
    sget-boolean v0, Lcom/policydm/agent/XDMAgent;->m_bPendingStatus:Z

    if-eqz v0, :cond_1

    .line 569
    sput-boolean v12, Lcom/policydm/agent/XDMAgent;->m_bPendingStatus:Z

    .line 572
    :cond_1
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMOResultCode()Ljava/lang/String;

    move-result-object v10

    .line 576
    .local v10, "szSendData":Ljava/lang/String;
    :try_start_1
    iget-object v0, p0, Lcom/policydm/agent/XDMAgent;->m_HttpDMAdapter:Lcom/policydm/tp/XTPAdapter;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v4

    invoke-virtual {v10, v4}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v4

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v5

    const/4 v6, 0x0

    invoke-virtual {v0, v4, v5, v6}, Lcom/policydm/tp/XTPAdapter;->xtpAdpSendData([BII)I
    :try_end_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v8

    .line 584
    :goto_2
    if-nez v8, :cond_2

    .line 586
    const/16 v0, 0xd

    invoke-static {v0, v11, v11}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 578
    :catch_1
    move-exception v7

    .line 580
    .local v7, "e":Ljava/net/SocketTimeoutException;
    invoke-virtual {v7}, Ljava/net/SocketTimeoutException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 581
    invoke-static {}, Lcom/policydm/tp/XTPNetSendTimer;->xtpEndTimer()V

    .line 582
    const/16 v8, -0xd

    goto :goto_2

    .line 588
    .end local v7    # "e":Ljava/net/SocketTimeoutException;
    :cond_2
    const/16 v0, -0xc

    if-ne v8, v0, :cond_3

    .line 590
    const/16 v0, 0xb

    invoke-static {v0, v11, v11}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 595
    :cond_3
    invoke-static {v13, v11, v11}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public xdmAgentSendPackage()I
    .locals 17

    .prologue
    .line 424
    const-string v3, ""

    .line 425
    .local v3, "szHmacData":Ljava/lang/String;
    const-string v4, ""

    .line 426
    .local v4, "szContentRange":Ljava/lang/String;
    sget-object v16, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    .line 427
    .local v16, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    const/4 v13, 0x0

    .line 429
    .local v13, "ret":I
    move-object/from16 v0, v16

    iget v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->credType:I

    if-eqz v1, :cond_0

    move-object/from16 v0, v16

    iget v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->credType:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_5

    .line 433
    :cond_0
    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/policydm/agent/XDMAgent;->m_HttpDMAdapter:Lcom/policydm/tp/XTPAdapter;

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/policydm/eng/core/XDMWorkspace;->m_szTargetURI:Ljava/lang/String;

    const-string v5, "POST"

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v1 .. v7}, Lcom/policydm/tp/XTPAdapter;->xtpAdpSetHttpObj(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v13

    .line 444
    const/4 v1, 0x4

    if-ne v13, v1, :cond_1

    .line 446
    const/4 v1, 0x1

    sput-boolean v1, Lcom/policydm/agent/XDMAgent;->m_bPendingStatus:Z

    :goto_0
    move v14, v13

    .line 537
    .end local v13    # "ret":I
    .local v14, "ret":I
    :goto_1
    return v14

    .line 435
    .end local v14    # "ret":I
    .restart local v13    # "ret":I
    :catch_0
    move-exception v12

    .line 437
    .local v12, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v12}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 438
    invoke-static {}, Lcom/policydm/tp/XTPNetSendTimer;->xtpEndTimer()V

    .line 439
    const/16 v13, -0xd

    .line 440
    const/16 v1, 0x10

    const/4 v2, 0x0

    const/4 v5, 0x0

    invoke-static {v1, v2, v5}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    move v14, v13

    .line 441
    .end local v13    # "ret":I
    .restart local v14    # "ret":I
    goto :goto_1

    .line 450
    .end local v12    # "e":Ljava/lang/NullPointerException;
    .end local v14    # "ret":I
    .restart local v13    # "ret":I
    :cond_1
    sget-boolean v1, Lcom/policydm/agent/XDMAgent;->m_bPendingStatus:Z

    if-eqz v1, :cond_2

    .line 452
    const/4 v1, 0x0

    sput-boolean v1, Lcom/policydm/agent/XDMAgent;->m_bPendingStatus:Z

    .line 457
    :cond_2
    :try_start_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/policydm/agent/XDMAgent;->m_HttpDMAdapter:Lcom/policydm/tp/XTPAdapter;

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/policydm/eng/core/XDMWorkspace;->buf:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    move-object/from16 v0, v16

    iget-object v5, v0, Lcom/policydm/eng/core/XDMWorkspace;->buf:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v5

    const/4 v6, 0x0

    invoke-virtual {v1, v2, v5, v6}, Lcom/policydm/tp/XTPAdapter;->xtpAdpSendData([BII)I
    :try_end_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v13

    .line 465
    :goto_2
    if-nez v13, :cond_3

    .line 467
    const/16 v1, 0xd

    const/4 v2, 0x0

    const/4 v5, 0x0

    invoke-static {v1, v2, v5}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 459
    :catch_1
    move-exception v12

    .line 461
    .local v12, "e":Ljava/net/SocketTimeoutException;
    invoke-virtual {v12}, Ljava/net/SocketTimeoutException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 462
    invoke-static {}, Lcom/policydm/tp/XTPNetSendTimer;->xtpEndTimer()V

    .line 463
    const/16 v13, -0xd

    goto :goto_2

    .line 469
    .end local v12    # "e":Ljava/net/SocketTimeoutException;
    :cond_3
    const/16 v1, -0xc

    if-ne v13, v1, :cond_4

    .line 471
    const/16 v1, 0xb

    const/4 v2, 0x0

    const/4 v5, 0x0

    invoke-static {v1, v2, v5}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 476
    :cond_4
    const/16 v1, 0x10

    const/4 v2, 0x0

    const/4 v5, 0x0

    invoke-static {v1, v2, v5}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 483
    :cond_5
    invoke-static {}, Lcom/policydm/eng/core/XDMWbxmlEncoder;->xdmWbxEncGetBufferSize()I

    move-result v11

    .line 484
    .local v11, "len":I
    move-object/from16 v0, v16

    iget v5, v0, Lcom/policydm/eng/core/XDMWorkspace;->credType:I

    move-object/from16 v0, v16

    iget-object v6, v0, Lcom/policydm/eng/core/XDMWorkspace;->m_szUserName:Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v7, v0, Lcom/policydm/eng/core/XDMWorkspace;->m_szClientPW:Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v8, v0, Lcom/policydm/eng/core/XDMWorkspace;->nextNonce:[B

    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->nextNonce:[B

    array-length v9, v1

    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->buf:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v10

    invoke-static/range {v5 .. v11}, Lcom/policydm/eng/core/XDMAuth;->xdmAuthMakeDigest(ILjava/lang/String;Ljava/lang/String;[BI[BI)Ljava/lang/String;

    move-result-object v15

    .line 486
    .local v15, "szMac":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "algorithm=MD5, username=\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/policydm/eng/core/XDMWorkspace;->m_szUserName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mac="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 490
    :try_start_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/policydm/agent/XDMAgent;->m_HttpDMAdapter:Lcom/policydm/tp/XTPAdapter;

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/policydm/eng/core/XDMWorkspace;->m_szTargetURI:Ljava/lang/String;

    const-string v5, "POST"

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v1 .. v7}, Lcom/policydm/tp/XTPAdapter;->xtpAdpSetHttpObj(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)I
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_2

    move-result v13

    .line 501
    const/4 v1, 0x4

    if-ne v13, v1, :cond_6

    .line 503
    const/4 v1, 0x1

    sput-boolean v1, Lcom/policydm/agent/XDMAgent;->m_bPendingStatus:Z

    goto/16 :goto_0

    .line 492
    :catch_2
    move-exception v12

    .line 494
    .local v12, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v12}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 495
    invoke-static {}, Lcom/policydm/tp/XTPNetSendTimer;->xtpEndTimer()V

    .line 496
    const/16 v13, -0xd

    .line 497
    const/16 v1, 0x10

    const/4 v2, 0x0

    const/4 v5, 0x0

    invoke-static {v1, v2, v5}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    move v14, v13

    .line 498
    .end local v13    # "ret":I
    .restart local v14    # "ret":I
    goto/16 :goto_1

    .line 507
    .end local v12    # "e":Ljava/lang/NullPointerException;
    .end local v14    # "ret":I
    .restart local v13    # "ret":I
    :cond_6
    sget-boolean v1, Lcom/policydm/agent/XDMAgent;->m_bPendingStatus:Z

    if-eqz v1, :cond_7

    .line 509
    const/4 v1, 0x0

    sput-boolean v1, Lcom/policydm/agent/XDMAgent;->m_bPendingStatus:Z

    .line 514
    :cond_7
    :try_start_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/policydm/agent/XDMAgent;->m_HttpDMAdapter:Lcom/policydm/tp/XTPAdapter;

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/policydm/eng/core/XDMWorkspace;->buf:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    move-object/from16 v0, v16

    iget-object v5, v0, Lcom/policydm/eng/core/XDMWorkspace;->e:Lcom/policydm/eng/core/XDMEncoder;

    invoke-static {v5}, Lcom/policydm/eng/core/XDMEncoder;->xdmEncGetBufferSize(Lcom/policydm/eng/core/XDMEncoder;)I

    move-result v5

    const/4 v6, 0x0

    invoke-virtual {v1, v2, v5, v6}, Lcom/policydm/tp/XTPAdapter;->xtpAdpSendData([BII)I
    :try_end_3
    .catch Ljava/net/SocketTimeoutException; {:try_start_3 .. :try_end_3} :catch_3

    move-result v13

    .line 522
    :goto_3
    if-nez v13, :cond_8

    .line 524
    const/16 v1, 0xd

    const/4 v2, 0x0

    const/4 v5, 0x0

    invoke-static {v1, v2, v5}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 516
    :catch_3
    move-exception v12

    .line 518
    .local v12, "e":Ljava/net/SocketTimeoutException;
    invoke-virtual {v12}, Ljava/net/SocketTimeoutException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 519
    invoke-static {}, Lcom/policydm/tp/XTPNetSendTimer;->xtpEndTimer()V

    .line 520
    const/16 v13, -0xd

    goto :goto_3

    .line 526
    .end local v12    # "e":Ljava/net/SocketTimeoutException;
    :cond_8
    const/16 v1, -0xc

    if-ne v13, v1, :cond_9

    .line 528
    const/16 v1, 0xb

    const/4 v2, 0x0

    const/4 v5, 0x0

    invoke-static {v1, v2, v5}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 533
    :cond_9
    const/16 v1, 0x10

    const/4 v2, 0x0

    const/4 v5, 0x0

    invoke-static {v1, v2, v5}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method public xdmAgentSetAclDynamicFUMONode(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)I
    .locals 4
    .param p1, "ptOm"    # Lcom/policydm/eng/core/XDMOmTree;
    .param p2, "szFumoNodePath"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x2

    .line 7857
    const/4 v0, 0x0

    .line 7858
    .local v0, "aclValue":I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "target path["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 7860
    const-string v1, "/PkgName"

    invoke-virtual {p2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 7862
    const/16 v0, 0x18

    .line 7863
    invoke-static {p1, p2, v0, v3}, Lcom/policydm/agent/XDMAgent;->xdmAgentMakeDefaultAcl(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 7916
    :cond_0
    :goto_0
    const/4 v1, 0x0

    return v1

    .line 7865
    :cond_1
    const-string v1, "/PkgVersion"

    invoke-virtual {p2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 7867
    const/16 v0, 0x18

    .line 7868
    invoke-static {p1, p2, v0, v3}, Lcom/policydm/agent/XDMAgent;->xdmAgentMakeDefaultAcl(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    goto :goto_0

    .line 7870
    :cond_2
    const-string v1, "/PkgSize"

    invoke-virtual {p2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 7872
    const/16 v0, 0x18

    .line 7873
    invoke-static {p1, p2, v0, v3}, Lcom/policydm/agent/XDMAgent;->xdmAgentMakeDefaultAcl(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    goto :goto_0

    .line 7875
    :cond_3
    const-string v1, "/PkgDesc"

    invoke-virtual {p2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 7877
    const/16 v0, 0x18

    .line 7878
    invoke-static {p1, p2, v0, v3}, Lcom/policydm/agent/XDMAgent;->xdmAgentMakeDefaultAcl(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    goto :goto_0

    .line 7880
    :cond_4
    const-string v1, "/DownloadAndUpdate"

    invoke-virtual {p2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 7882
    const/16 v0, 0xc

    .line 7883
    invoke-static {p1, p2, v0, v3}, Lcom/policydm/agent/XDMAgent;->xdmAgentMakeDefaultAcl(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    goto :goto_0

    .line 7885
    :cond_5
    const-string v1, "/PkgURL"

    invoke-virtual {p2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 7887
    const/16 v0, 0x18

    .line 7888
    invoke-static {p1, p2, v0, v3}, Lcom/policydm/agent/XDMAgent;->xdmAgentMakeDefaultAcl(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    goto :goto_0

    .line 7890
    :cond_6
    const-string v1, "/ProcessId"

    invoke-virtual {p2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 7892
    const/16 v0, 0x18

    .line 7893
    invoke-static {p1, p2, v0, v3}, Lcom/policydm/agent/XDMAgent;->xdmAgentMakeDefaultAcl(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    goto :goto_0

    .line 7895
    :cond_7
    const-string v1, "/ReportURL"

    invoke-virtual {p2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 7897
    const/16 v0, 0x18

    .line 7898
    invoke-static {p1, p2, v0, v3}, Lcom/policydm/agent/XDMAgent;->xdmAgentMakeDefaultAcl(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    goto :goto_0

    .line 7900
    :cond_8
    const-string v1, "/PushJobId"

    invoke-virtual {p2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 7902
    const/16 v0, 0x18

    .line 7903
    invoke-static {p1, p2, v0, v3}, Lcom/policydm/agent/XDMAgent;->xdmAgentMakeDefaultAcl(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    goto :goto_0

    .line 7905
    :cond_9
    const-string v1, "/State"

    invoke-virtual {p2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 7907
    const/16 v0, 0x8

    .line 7908
    invoke-static {p1, p2, v0, v3}, Lcom/policydm/agent/XDMAgent;->xdmAgentMakeDefaultAcl(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    goto :goto_0

    .line 7910
    :cond_a
    const-string v1, "/Ext"

    invoke-virtual {p2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7912
    const/16 v0, 0x8

    .line 7913
    invoke-static {p1, p2, v0, v3}, Lcom/policydm/agent/XDMAgent;->xdmAgentMakeDefaultAcl(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    goto/16 :goto_0
.end method

.method public xdmAgentSetOMAccB64(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 6
    .param p1, "omt"    # Ljava/lang/Object;
    .param p2, "szPath"    # Ljava/lang/String;
    .param p3, "szData"    # Ljava/lang/String;
    .param p4, "aclValue"    # I
    .param p5, "scope"    # I

    .prologue
    .line 2073
    move-object v1, p1

    check-cast v1, Lcom/policydm/eng/core/XDMOmTree;

    .line 2078
    .local v1, "om":Lcom/policydm/eng/core/XDMOmTree;
    invoke-static {v1, p2}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v0

    .line 2079
    .local v0, "node":Lcom/policydm/eng/core/XDMVnode;
    if-nez v0, :cond_1

    .line 2081
    invoke-virtual {p0, p2, p3}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMB64(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2082
    invoke-static {v1, p2, p4, p5}, Lcom/policydm/agent/XDMAgent;->xdmAgentMakeDefaultAcl(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 2098
    :cond_0
    :goto_0
    return-void

    .line 2086
    :cond_1
    iget v4, v0, Lcom/policydm/eng/core/XDMVnode;->size:I

    new-array v3, v4, [C

    .line 2087
    .local v3, "temp":[C
    const/4 v4, 0x0

    iget v5, v0, Lcom/policydm/eng/core/XDMVnode;->size:I

    invoke-static {v1, p2, v4, v3, v5}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmRead(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    .line 2089
    invoke-static {v3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v2

    .line 2090
    .local v2, "szTmp":Ljava/lang/String;
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v4

    iget v5, v0, Lcom/policydm/eng/core/XDMVnode;->size:I

    if-eq v4, v5, :cond_2

    .line 2092
    invoke-virtual {p0, p2, p3}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMB64(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 2094
    :cond_2
    invoke-virtual {v2, p3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-eqz v4, :cond_0

    .line 2096
    invoke-virtual {p0, p2, p3}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOMB64(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public xdmAgentSetOMB64(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 10
    .param p1, "szPath"    # Ljava/lang/String;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v9, 0x1

    .line 2151
    sget-object v8, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    .line 2154
    .local v8, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    const/4 v2, 0x0

    .line 2156
    .local v2, "nLen":I
    if-nez p2, :cond_1

    .line 2158
    const-string v0, "data is NULL"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 2182
    :cond_0
    :goto_0
    return-void

    .line 2162
    :cond_1
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    array-length v2, v0

    .line 2163
    if-gtz v2, :cond_2

    .line 2165
    iget-object v0, v8, Lcom/policydm/eng/core/XDMWorkspace;->om:Lcom/policydm/eng/core/XDMOmTree;

    invoke-static {v0, p1, v9}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmDeleteImplicit(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;Z)I

    .line 2166
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "The ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] node is 0 length"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 2169
    :cond_2
    iget-object v0, v8, Lcom/policydm/eng/core/XDMWorkspace;->om:Lcom/policydm/eng/core/XDMOmTree;

    const/4 v3, 0x0

    move-object v1, p1

    move-object v4, p2

    move v5, v2

    invoke-static/range {v0 .. v5}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmWrite(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;IILjava/lang/Object;I)I

    .line 2170
    iget-object v0, v8, Lcom/policydm/eng/core/XDMWorkspace;->om:Lcom/policydm/eng/core/XDMOmTree;

    invoke-static {v0, p1}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v7

    .line 2171
    .local v7, "node":Lcom/policydm/eng/core/XDMVnode;
    if-eqz v7, :cond_0

    .line 2173
    iget-object v0, v7, Lcom/policydm/eng/core/XDMVnode;->type:Lcom/policydm/eng/core/XDMOmList;

    if-eqz v0, :cond_3

    .line 2174
    iget-object v0, v7, Lcom/policydm/eng/core/XDMVnode;->type:Lcom/policydm/eng/core/XDMOmList;

    invoke-static {v0}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmVfsDeleteMimeList(Lcom/policydm/eng/core/XDMOmList;)V

    .line 2176
    :cond_3
    new-instance v6, Lcom/policydm/eng/core/XDMOmList;

    invoke-direct {v6}, Lcom/policydm/eng/core/XDMOmList;-><init>()V

    .line 2177
    .local v6, "list":Lcom/policydm/eng/core/XDMOmList;
    const-string v0, "text/plain"

    iput-object v0, v6, Lcom/policydm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    .line 2178
    const/4 v0, 0x0

    iput-object v0, v6, Lcom/policydm/eng/core/XDMOmList;->next:Lcom/policydm/eng/core/XDMOmList;

    .line 2179
    iput-object v6, v7, Lcom/policydm/eng/core/XDMVnode;->type:Lcom/policydm/eng/core/XDMOmList;

    .line 2180
    iput v9, v7, Lcom/policydm/eng/core/XDMVnode;->format:I

    goto :goto_0
.end method

.method public xdmAgentStartMgmtSession()I
    .locals 10

    .prologue
    const/16 v5, -0xc

    const/4 v9, 0x1

    const/4 v4, -0x1

    const/4 v8, 0x0

    .line 4327
    sget-object v3, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    .line 4328
    .local v3, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    const/4 v2, 0x0

    .line 4330
    .local v2, "res":I
    if-nez v3, :cond_0

    .line 4332
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Parsing package failed Abort session "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 4486
    :goto_0
    return v4

    .line 4336
    :cond_0
    iget-object v6, v3, Lcom/policydm/eng/core/XDMWorkspace;->procState:Lcom/policydm/interfaces/XDMInterface$XDMProcessingState;

    sget-object v7, Lcom/policydm/interfaces/XDMInterface$XDMProcessingState;->XDM_PROC_NONE:Lcom/policydm/interfaces/XDMInterface$XDMProcessingState;

    if-ne v6, v7, :cond_1

    .line 4338
    iput v8, v3, Lcom/policydm/eng/core/XDMWorkspace;->numAction:I

    .line 4339
    iget-object v6, v3, Lcom/policydm/eng/core/XDMWorkspace;->buf:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    invoke-static {v6}, Lcom/policydm/agent/XDMAgent;->xdmAgentParsingWbxml([B)I

    move-result v2

    .line 4341
    if-eqz v2, :cond_1

    .line 4343
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Parsing package failed Abort session"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 4348
    :cond_1
    invoke-virtual {p0}, Lcom/policydm/agent/XDMAgent;->xdmAgentHandleCmd()I

    move-result v2

    .line 4350
    sget-object v6, Lcom/policydm/interfaces/XUICInterface$XUICFlag;->XUIC_TRUE:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    iput-object v6, v3, Lcom/policydm/eng/core/XDMWorkspace;->uicFlag:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    .line 4352
    sparse-switch v2, :sswitch_data_0

    .line 4367
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Handling Commands failed Abort session "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 4355
    :sswitch_0
    const-string v4, "Handling Paused  Processing UIC Command"

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    move v4, v2

    .line 4356
    goto :goto_0

    .line 4359
    :sswitch_1
    const-string v4, "XDM_RET_ALERT_SESSION_ABORT"

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 4360
    invoke-static {v8}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOStatus(I)V

    move v4, v2

    .line 4361
    goto :goto_0

    .line 4371
    :sswitch_2
    iget-object v6, v3, Lcom/policydm/eng/core/XDMWorkspace;->dmState:Lcom/policydm/interfaces/XDMInterface$XDMSyncMLState;

    sget-object v7, Lcom/policydm/interfaces/XDMInterface$XDMSyncMLState;->XDM_STATE_FINISH:Lcom/policydm/interfaces/XDMInterface$XDMSyncMLState;

    if-eq v6, v7, :cond_2

    .line 4373
    iget v6, v3, Lcom/policydm/eng/core/XDMWorkspace;->msgID:I

    add-int/lit8 v6, v6, 0x1

    iput v6, v3, Lcom/policydm/eng/core/XDMWorkspace;->msgID:I

    .line 4376
    :cond_2
    iget v6, v3, Lcom/policydm/eng/core/XDMWorkspace;->authState:I

    if-ne v6, v9, :cond_3

    iget v6, v3, Lcom/policydm/eng/core/XDMWorkspace;->serverAuthState:I

    if-eq v6, v9, :cond_9

    .line 4378
    :cond_3
    iget v6, v3, Lcom/policydm/eng/core/XDMWorkspace;->authCount:I

    add-int/lit8 v6, v6, 0x1

    iput v6, v3, Lcom/policydm/eng/core/XDMWorkspace;->authCount:I

    .line 4379
    iget v6, v3, Lcom/policydm/eng/core/XDMWorkspace;->authCount:I

    const/4 v7, 0x3

    if-lt v6, v7, :cond_4

    .line 4381
    iput v8, v3, Lcom/policydm/eng/core/XDMWorkspace;->authCount:I

    .line 4382
    const/4 v4, -0x8

    iput v4, v3, Lcom/policydm/eng/core/XDMWorkspace;->serverAuthState:I

    .line 4383
    const-string v4, "Authentification Failed Abort"

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 4384
    invoke-static {v8}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOInitiatedType(I)V

    .line 4385
    const/4 v4, -0x5

    goto/16 :goto_0

    .line 4388
    :cond_4
    iget v6, v3, Lcom/policydm/eng/core/XDMWorkspace;->authState:I

    if-nez v6, :cond_6

    .line 4390
    sget-object v6, Lcom/policydm/interfaces/XDMInterface$XDMSyncMLState;->XDM_STATE_PROCESSING:Lcom/policydm/interfaces/XDMInterface$XDMSyncMLState;

    iput-object v6, v3, Lcom/policydm/eng/core/XDMWorkspace;->dmState:Lcom/policydm/interfaces/XDMInterface$XDMSyncMLState;

    .line 4401
    :cond_5
    :goto_1
    invoke-virtual {p0}, Lcom/policydm/agent/XDMAgent;->xdmAgentCreatePackage()I

    move-result v6

    if-eqz v6, :cond_7

    .line 4403
    const-string v5, "failed"

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 4395
    :cond_6
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Authentification Retry...ws->dmState = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v3, Lcom/policydm/eng/core/XDMWorkspace;->dmState:Lcom/policydm/interfaces/XDMInterface$XDMSyncMLState;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 4396
    iget-object v6, v3, Lcom/policydm/eng/core/XDMWorkspace;->dmState:Lcom/policydm/interfaces/XDMInterface$XDMSyncMLState;

    sget-object v7, Lcom/policydm/interfaces/XDMInterface$XDMSyncMLState;->XDM_STATE_CLIENT_INIT_MGMT:Lcom/policydm/interfaces/XDMInterface$XDMSyncMLState;

    if-eq v6, v7, :cond_5

    iget-object v6, v3, Lcom/policydm/eng/core/XDMWorkspace;->dmState:Lcom/policydm/interfaces/XDMInterface$XDMSyncMLState;

    sget-object v7, Lcom/policydm/interfaces/XDMInterface$XDMSyncMLState;->XDM_STATE_GENERIC_ALERT:Lcom/policydm/interfaces/XDMInterface$XDMSyncMLState;

    if-eq v6, v7, :cond_5

    iget-object v6, v3, Lcom/policydm/eng/core/XDMWorkspace;->dmState:Lcom/policydm/interfaces/XDMInterface$XDMSyncMLState;

    sget-object v7, Lcom/policydm/interfaces/XDMInterface$XDMSyncMLState;->XDM_STATE_GENERIC_ALERT_REPORT:Lcom/policydm/interfaces/XDMInterface$XDMSyncMLState;

    if-eq v6, v7, :cond_5

    .line 4398
    sget-object v6, Lcom/policydm/interfaces/XDMInterface$XDMSyncMLState;->XDM_STATE_PROCESSING:Lcom/policydm/interfaces/XDMInterface$XDMSyncMLState;

    iput-object v6, v3, Lcom/policydm/eng/core/XDMWorkspace;->dmState:Lcom/policydm/interfaces/XDMInterface$XDMSyncMLState;

    goto :goto_1

    .line 4408
    :cond_7
    sget-object v4, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v4, v4, v8

    iget v4, v4, Lcom/policydm/tp/XTPHttpObj;->nHttpConnection:I

    if-ne v4, v9, :cond_8

    .line 4413
    :try_start_0
    iget-object v4, p0, Lcom/policydm/agent/XDMAgent;->m_HttpDMAdapter:Lcom/policydm/tp/XTPAdapter;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Lcom/policydm/tp/XTPAdapter;->xtpAdpOpen(I)I
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 4422
    :goto_2
    if-eqz v2, :cond_8

    move v4, v5

    .line 4424
    goto/16 :goto_0

    .line 4415
    :catch_0
    move-exception v0

    .line 4417
    .local v0, "e":Ljava/net/SocketTimeoutException;
    invoke-virtual {v0}, Ljava/net/SocketTimeoutException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 4418
    invoke-static {}, Lcom/policydm/tp/XTPNetConnectTimer;->xtpNetConnEndTimer()V

    .line 4419
    const/16 v2, -0xc

    goto :goto_2

    .line 4428
    .end local v0    # "e":Ljava/net/SocketTimeoutException;
    :cond_8
    invoke-virtual {p0}, Lcom/policydm/agent/XDMAgent;->xdmAgentSendPackage()I

    move-result v2

    move v4, v2

    .line 4429
    goto/16 :goto_0

    .line 4433
    :cond_9
    sget-object v6, Lcom/policydm/interfaces/XDMInterface$XDMSyncMLState;->XDM_STATE_PROCESSING:Lcom/policydm/interfaces/XDMInterface$XDMSyncMLState;

    iput-object v6, v3, Lcom/policydm/eng/core/XDMWorkspace;->dmState:Lcom/policydm/interfaces/XDMInterface$XDMSyncMLState;

    .line 4434
    iput v8, v3, Lcom/policydm/eng/core/XDMWorkspace;->authCount:I

    .line 4435
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "total action commands = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v3, Lcom/policydm/eng/core/XDMWorkspace;->numAction:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 4437
    iget v6, v3, Lcom/policydm/eng/core/XDMWorkspace;->numAction:I

    if-nez v6, :cond_b

    iget-boolean v6, v3, Lcom/policydm/eng/core/XDMWorkspace;->isFinal:Z

    if-eqz v6, :cond_b

    .line 4441
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMOStatus()I

    move-result v1

    .line 4442
    .local v1, "nAgentStatus":I
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "nStatus :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 4444
    const/16 v4, 0xa

    if-ne v1, v4, :cond_a

    .line 4446
    const-string v4, "DownloadAndUpdate Start"

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 4447
    const/4 v4, 0x2

    goto/16 :goto_0

    .line 4452
    :cond_a
    invoke-static {v8}, Lcom/policydm/db/XDBProfileListAdp;->xdbSetNotiReSyncMode(I)Ljava/lang/Boolean;

    .line 4455
    const/4 v4, 0x5

    goto/16 :goto_0

    .line 4458
    .end local v1    # "nAgentStatus":I
    :cond_b
    invoke-virtual {p0}, Lcom/policydm/agent/XDMAgent;->xdmAgentCreatePackage()I

    move-result v2

    .line 4459
    if-gez v2, :cond_c

    .line 4461
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "xdmAgentCreatePackage failed "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 4466
    :cond_c
    sget-object v4, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v4, v4, v8

    iget v4, v4, Lcom/policydm/tp/XTPHttpObj;->nHttpConnection:I

    if-ne v4, v9, :cond_d

    .line 4470
    :try_start_1
    iget-object v4, p0, Lcom/policydm/agent/XDMAgent;->m_HttpDMAdapter:Lcom/policydm/tp/XTPAdapter;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Lcom/policydm/tp/XTPAdapter;->xtpAdpOpen(I)I
    :try_end_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v2

    .line 4479
    :goto_3
    if-eqz v2, :cond_d

    move v4, v5

    .line 4481
    goto/16 :goto_0

    .line 4472
    :catch_1
    move-exception v0

    .line 4474
    .restart local v0    # "e":Ljava/net/SocketTimeoutException;
    invoke-virtual {v0}, Ljava/net/SocketTimeoutException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 4475
    invoke-static {}, Lcom/policydm/tp/XTPNetConnectTimer;->xtpNetConnEndTimer()V

    .line 4476
    const/16 v2, -0xc

    goto :goto_3

    .line 4485
    .end local v0    # "e":Ljava/net/SocketTimeoutException;
    :cond_d
    invoke-virtual {p0}, Lcom/policydm/agent/XDMAgent;->xdmAgentSendPackage()I

    move-result v2

    move v4, v2

    .line 4486
    goto/16 :goto_0

    .line 4352
    :sswitch_data_0
    .sparse-switch
        -0x4 -> :sswitch_0
        0x0 -> :sswitch_2
        0x3 -> :sswitch_1
    .end sparse-switch
.end method

.method public xdmAgentStartSession()I
    .locals 4

    .prologue
    .line 618
    const/4 v1, 0x0

    .line 619
    .local v1, "ret":I
    const/4 v0, 0x0

    .line 622
    .local v0, "nNotiEvent":I
    const-string v3, ""

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 624
    invoke-static {}, Lcom/policydm/agent/XDMAgent;->xdmAgentInit()I

    move-result v3

    if-eqz v3, :cond_0

    .line 626
    const/4 v3, -0x1

    .line 645
    :goto_0
    return v3

    .line 629
    :cond_0
    sget-object v2, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    .line 630
    .local v2, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    invoke-static {}, Lcom/policydm/db/XDBProfileListAdp;->xdbGetNotiEvent()I

    move-result v0

    .line 631
    if-lez v0, :cond_2

    sget-boolean v3, Lcom/policydm/agent/XDMAgent;->m_bPendingStatus:Z

    if-nez v3, :cond_2

    .line 633
    iget v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->appId:I

    invoke-static {v3}, Lcom/policydm/db/XDBProfileListAdp;->xdbGetSessionID(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->m_szSessionID:Ljava/lang/String;

    .line 640
    :goto_1
    invoke-virtual {p0}, Lcom/policydm/agent/XDMAgent;->xdmAgentMakeNode()I

    move-result v3

    if-eqz v3, :cond_1

    .line 642
    const/4 v1, -0x1

    :cond_1
    move v3, v1

    .line 645
    goto :goto_0

    .line 637
    :cond_2
    invoke-virtual {p0}, Lcom/policydm/agent/XDMAgent;->xdmAgentLibMakeSessionID()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->m_szSessionID:Ljava/lang/String;

    goto :goto_1
.end method

.method public xdmAgentTpCheckRetry()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 7936
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ConntectRetryCount "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/policydm/agent/XDMAgent;->m_nConnectRetryCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 7938
    sget v1, Lcom/policydm/agent/XDMAgent;->m_nConnectRetryCount:I

    const/4 v2, 0x3

    if-lt v1, v2, :cond_0

    .line 7940
    invoke-static {}, Lcom/policydm/tp/XTPNetConnectTimer;->xtpNetConnEndTimer()V

    .line 7941
    invoke-static {}, Lcom/policydm/tp/XTPNetRecvTimer;->xtpNetRecvEndTimer()V

    .line 7942
    invoke-static {}, Lcom/policydm/tp/XTPNetSendTimer;->xtpEndTimer()V

    .line 7943
    sput v0, Lcom/policydm/agent/XDMAgent;->m_nConnectRetryCount:I

    .line 7947
    :goto_0
    return v0

    .line 7946
    :cond_0
    sget v0, Lcom/policydm/agent/XDMAgent;->m_nConnectRetryCount:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/policydm/agent/XDMAgent;->m_nConnectRetryCount:I

    .line 7947
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public xdmAgentTpClose(I)V
    .locals 1
    .param p1, "appId"    # I

    .prologue
    .line 7921
    iget-object v0, p0, Lcom/policydm/agent/XDMAgent;->m_HttpDMAdapter:Lcom/policydm/tp/XTPAdapter;

    invoke-virtual {v0, p1}, Lcom/policydm/tp/XTPAdapter;->xtpAdpClose(I)V

    .line 7922
    return-void
.end method

.method public xdmAgentTpCloseNetwork(I)V
    .locals 1
    .param p1, "appId"    # I

    .prologue
    .line 7931
    iget-object v0, p0, Lcom/policydm/agent/XDMAgent;->m_HttpDMAdapter:Lcom/policydm/tp/XTPAdapter;

    invoke-virtual {v0, p1}, Lcom/policydm/tp/XTPAdapter;->xtpAdpCloseNetWork(I)V

    .line 7932
    return-void
.end method

.method public xdmAgentTpInit(I)I
    .locals 1
    .param p1, "appId"    # I

    .prologue
    .line 7926
    iget-object v0, p0, Lcom/policydm/agent/XDMAgent;->m_HttpDMAdapter:Lcom/policydm/tp/XTPAdapter;

    invoke-virtual {v0, p1}, Lcom/policydm/tp/XTPAdapter;->xtpAdpInit(I)I

    move-result v0

    return v0
.end method

.method public xdmAgentVefifyAtomicCmd(Lcom/policydm/agent/XDMAgent;)Z
    .locals 4
    .param p1, "cmd"    # Lcom/policydm/agent/XDMAgent;

    .prologue
    const/4 v1, 0x0

    .line 2587
    const/4 v0, 0x1

    .line 2589
    .local v0, "res":Z
    const-string v2, "Atomic_Start"

    iget-object v3, p1, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 2600
    .end local v0    # "res":Z
    :goto_0
    return v0

    .line 2593
    .restart local v0    # "res":Z
    :cond_0
    const-string v2, "GET"

    iget-object v3, p1, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_1

    move v0, v1

    .line 2595
    goto :goto_0

    .line 2599
    :cond_1
    const-string v1, ""

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public xdmAgentVerifyCmd(Lcom/policydm/agent/XDMAgent;ZLcom/policydm/eng/parser/XDMParserStatus;)I
    .locals 7
    .param p1, "cmd"    # Lcom/policydm/agent/XDMAgent;
    .param p2, "isAtomic"    # Z
    .param p3, "atomic_status"    # Lcom/policydm/eng/parser/XDMParserStatus;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 4622
    sget-object v2, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    .line 4623
    .local v2, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    const/4 v1, 0x0

    .line 4625
    .local v1, "res":I
    const-string v3, "SyncHdr"

    iget-object v4, p1, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_0

    .line 4627
    iget-object v3, p1, Lcom/policydm/agent/XDMAgent;->m_Header:Lcom/policydm/eng/parser/XDMParserSyncheader;

    invoke-virtual {p0, v3}, Lcom/policydm/agent/XDMAgent;->xdmAgentCmdSyncHeader(Lcom/policydm/eng/parser/XDMParserSyncheader;)I

    move-result v1

    :goto_0
    move v3, v1

    .line 4705
    :goto_1
    return v3

    .line 4629
    :cond_0
    const-string v3, "Status"

    iget-object v4, p1, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_1

    .line 4631
    iget-object v3, p1, Lcom/policydm/agent/XDMAgent;->m_Status:Lcom/policydm/eng/parser/XDMParserStatus;

    invoke-virtual {p0, v3}, Lcom/policydm/agent/XDMAgent;->xdmAgentCmdStatus(Lcom/policydm/eng/parser/XDMParserStatus;)I

    move-result v1

    goto :goto_0

    .line 4633
    :cond_1
    const-string v3, "Get"

    iget-object v4, p1, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_2

    .line 4635
    iget-object v3, p1, Lcom/policydm/agent/XDMAgent;->m_Get:Lcom/policydm/eng/parser/XDMParserGet;

    invoke-virtual {p0, v3, p2}, Lcom/policydm/agent/XDMAgent;->xdmAgentCmdGet(Lcom/policydm/eng/parser/XDMParserGet;Z)I

    move-result v1

    goto :goto_0

    .line 4637
    :cond_2
    const-string v3, "Exec"

    iget-object v4, p1, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_3

    .line 4639
    iget-object v3, p1, Lcom/policydm/agent/XDMAgent;->m_Exec:Lcom/policydm/eng/parser/XDMParserExec;

    invoke-virtual {p0, v3}, Lcom/policydm/agent/XDMAgent;->xdmAgentCmdExec(Lcom/policydm/eng/parser/XDMParserExec;)I

    move-result v1

    goto :goto_0

    .line 4641
    :cond_3
    const-string v3, "Alert"

    iget-object v4, p1, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_4

    .line 4643
    iget-object v3, p1, Lcom/policydm/agent/XDMAgent;->m_Alert:Lcom/policydm/eng/parser/XDMParserAlert;

    invoke-virtual {p0, v3, p2}, Lcom/policydm/agent/XDMAgent;->xdmAgentCmdAlert(Lcom/policydm/eng/parser/XDMParserAlert;Z)I

    move-result v1

    goto :goto_0

    .line 4645
    :cond_4
    const-string v3, "Add"

    iget-object v4, p1, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_5

    .line 4647
    iget-object v3, p1, Lcom/policydm/agent/XDMAgent;->m_AddCmd:Lcom/policydm/eng/parser/XDMParserAdd;

    invoke-virtual {p0, v3, p2, p3}, Lcom/policydm/agent/XDMAgent;->xdmAgentCmdAdd(Lcom/policydm/eng/parser/XDMParserAdd;ZLcom/policydm/eng/parser/XDMParserStatus;)I

    move-result v1

    goto :goto_0

    .line 4649
    :cond_5
    const-string v3, "Replace"

    iget-object v4, p1, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_6

    .line 4651
    iget-object v3, p1, Lcom/policydm/agent/XDMAgent;->m_ReplaceCmd:Lcom/policydm/eng/parser/XDMParserReplace;

    invoke-virtual {p0, v3, p2, p3}, Lcom/policydm/agent/XDMAgent;->xdmAgentCmdReplace(Lcom/policydm/eng/parser/XDMParserReplace;ZLcom/policydm/eng/parser/XDMParserStatus;)I

    move-result v1

    goto :goto_0

    .line 4653
    :cond_6
    const-string v3, "Copy"

    iget-object v4, p1, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_7

    .line 4655
    iget-object v3, p1, Lcom/policydm/agent/XDMAgent;->m_CopyCmd:Lcom/policydm/eng/parser/XDMParserCopy;

    invoke-virtual {p0, v3, p2, p3}, Lcom/policydm/agent/XDMAgent;->xdmAgentCmdCopy(Lcom/policydm/eng/parser/XDMParserCopy;ZLcom/policydm/eng/parser/XDMParserStatus;)I

    move-result v1

    goto :goto_0

    .line 4657
    :cond_7
    const-string v3, "Delete"

    iget-object v4, p1, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_8

    .line 4659
    iget-object v3, p1, Lcom/policydm/agent/XDMAgent;->m_DeleteCmd:Lcom/policydm/eng/parser/XDMParserDelete;

    invoke-virtual {p0, v3, p2, p3}, Lcom/policydm/agent/XDMAgent;->xdmAgentCmdDelete(Lcom/policydm/eng/parser/XDMParserDelete;ZLcom/policydm/eng/parser/XDMParserStatus;)I

    move-result v1

    goto/16 :goto_0

    .line 4661
    :cond_8
    const-string v3, "Atomic_Start"

    iget-object v4, p1, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_a

    .line 4663
    iput-boolean v6, v2, Lcom/policydm/eng/core/XDMWorkspace;->inAtomicCmd:Z

    .line 4664
    iput-boolean v5, v2, Lcom/policydm/eng/core/XDMWorkspace;->atomicFlag:Z

    .line 4668
    :try_start_0
    iget-object v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->om:Lcom/policydm/eng/core/XDMOmTree;

    iget-object v3, v3, Lcom/policydm/eng/core/XDMOmTree;->vfs:Lcom/policydm/eng/core/XDMOmVfs;

    invoke-static {v3}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmVfsSaveFs(Lcom/policydm/eng/core/XDMOmVfs;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4674
    :goto_2
    iget-object v3, p1, Lcom/policydm/agent/XDMAgent;->m_Atomic:Lcom/policydm/eng/parser/XDMParserAtomic;

    invoke-virtual {p0, v3}, Lcom/policydm/agent/XDMAgent;->xdmAgentCmdAtomic(Lcom/policydm/eng/parser/XDMParserAtomic;)I

    move-result v1

    .line 4675
    iget-boolean v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->atomicFlag:Z

    if-eqz v3, :cond_9

    .line 4677
    const/4 v3, 0x0

    iput-object v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->om:Lcom/policydm/eng/core/XDMOmTree;

    .line 4678
    new-instance v3, Lcom/policydm/eng/core/XDMOmTree;

    invoke-direct {v3}, Lcom/policydm/eng/core/XDMOmTree;-><init>()V

    iput-object v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->om:Lcom/policydm/eng/core/XDMOmTree;

    .line 4679
    iget-object v3, v2, Lcom/policydm/eng/core/XDMWorkspace;->om:Lcom/policydm/eng/core/XDMOmTree;

    iget-object v3, v3, Lcom/policydm/eng/core/XDMOmTree;->vfs:Lcom/policydm/eng/core/XDMOmVfs;

    invoke-static {v3}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmVfsInit(Lcom/policydm/eng/core/XDMOmVfs;)I

    .line 4682
    :cond_9
    iput-boolean v5, v2, Lcom/policydm/eng/core/XDMWorkspace;->inAtomicCmd:Z

    goto/16 :goto_0

    .line 4670
    :catch_0
    move-exception v0

    .line 4672
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_2

    .line 4684
    .end local v0    # "e":Ljava/io/IOException;
    :cond_a
    const-string v3, "Sequence_Start"

    iget-object v4, p1, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_c

    .line 4686
    iput-boolean v6, v2, Lcom/policydm/eng/core/XDMWorkspace;->inSequenceCmd:Z

    .line 4687
    iget-object v3, p1, Lcom/policydm/agent/XDMAgent;->m_Sequence:Lcom/policydm/eng/parser/XDMParserSequence;

    invoke-virtual {p0, v3}, Lcom/policydm/agent/XDMAgent;->xdmAgentCmdSequence(Lcom/policydm/eng/parser/XDMParserSequence;)I

    move-result v1

    .line 4689
    const/4 v3, -0x4

    if-ne v1, v3, :cond_b

    move v3, v1

    .line 4691
    goto/16 :goto_1

    .line 4695
    :cond_b
    iput-boolean v5, v2, Lcom/policydm/eng/core/XDMWorkspace;->inSequenceCmd:Z

    goto/16 :goto_0

    .line 4701
    :cond_c
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown Command"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 4702
    const/4 v3, -0x6

    goto/16 :goto_1
.end method

.method public xdmAgentVerifyPolicyNode(Lcom/policydm/eng/core/XDMWorkspace;Ljava/lang/String;Ljava/lang/String;)I
    .locals 4
    .param p1, "ws"    # Lcom/policydm/eng/core/XDMWorkspace;
    .param p2, "szPath"    # Ljava/lang/String;
    .param p3, "szData"    # Ljava/lang/String;

    .prologue
    const/4 v2, -0x1

    .line 6999
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMOInfo()Lcom/policydm/db/XDBFumoInfo;

    move-result-object v1

    .line 7000
    .local v1, "fumoinfo":Lcom/policydm/db/XDBFumoInfo;
    const-string v3, ""

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 7002
    const-string v3, "/PkgName"

    invoke-virtual {p2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 7004
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 7085
    :cond_0
    :goto_0
    return v2

    .line 7008
    :cond_1
    iput-object p3, v1, Lcom/policydm/db/XDBFumoInfo;->m_szPkgName:Ljava/lang/String;

    .line 7009
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SEAndoridPolicy PkgName : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 7084
    :cond_2
    :goto_1
    invoke-static {v1}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOInfo(Ljava/lang/Object;)V

    .line 7085
    const/4 v2, 0x1

    goto :goto_0

    .line 7011
    :cond_3
    const-string v3, "/PkgVersion"

    invoke-virtual {p2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 7013
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 7017
    iput-object p3, v1, Lcom/policydm/db/XDBFumoInfo;->m_szPkgVer:Ljava/lang/String;

    .line 7018
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SEAndoridPolicy PkgVersion : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto :goto_1

    .line 7020
    :cond_4
    const-string v3, "/PkgSize"

    invoke-virtual {p2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 7022
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 7026
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v1, Lcom/policydm/db/XDBFumoInfo;->m_nPkgSize:I

    .line 7027
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SEAndoridPolicy  PkgSize: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v1, Lcom/policydm/db/XDBFumoInfo;->m_nPkgSize:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto :goto_1

    .line 7029
    :cond_5
    const-string v3, "/PkgDesc"

    invoke-virtual {p2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 7031
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 7035
    iput-object p3, v1, Lcom/policydm/db/XDBFumoInfo;->m_szPkgDesc:Ljava/lang/String;

    .line 7036
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SEAndoridPolicy PkgDesc : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v1, Lcom/policydm/db/XDBFumoInfo;->m_szPkgDesc:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 7038
    :cond_6
    const-string v3, "/PkgURL"

    invoke-virtual {p2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 7040
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 7042
    const-string v3, "SEAndoridPolicy PkgURL fail"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 7046
    :cond_7
    invoke-static {p3}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOObjectServerUrl(Ljava/lang/String;)Z

    move-result v0

    .line 7047
    .local v0, "bret":Z
    if-nez v0, :cond_8

    .line 7049
    const-string v3, "wrong URL"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 7053
    :cond_8
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SEAndoridPolicy PkgURL : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v1, Lcom/policydm/db/XDBFumoInfo;->m_szObjectDownloadUrl:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 7055
    .end local v0    # "bret":Z
    :cond_9
    const-string v3, "/ProcessId"

    invoke-virtual {p2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 7057
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 7061
    iput-object p3, v1, Lcom/policydm/db/XDBFumoInfo;->m_szProcessId:Ljava/lang/String;

    .line 7062
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SEAndoridPolicy ProcessId : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v1, Lcom/policydm/db/XDBFumoInfo;->m_szProcessId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 7064
    :cond_a
    const-string v3, "/ReportURL"

    invoke-virtual {p2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 7066
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 7068
    const-string v3, "SEAndoridPolicy ReportURL fail"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 7072
    :cond_b
    iput-object p3, v1, Lcom/policydm/db/XDBFumoInfo;->m_szReportUrl:Ljava/lang/String;

    .line 7073
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SEAndoridPolicy ReportURL : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v1, Lcom/policydm/db/XDBFumoInfo;->m_szReportUrl:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 7075
    :cond_c
    const-string v3, "/State"

    invoke-virtual {p2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 7077
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 7081
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v1, Lcom/policydm/db/XDBFumoInfo;->m_nStatus:I

    .line 7082
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SEAndoridPolicy Status : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v1, Lcom/policydm/db/XDBFumoInfo;->m_nStatus:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public xdmAgentVerifyServerAuth(Lcom/policydm/eng/parser/XDMParserSyncheader;)I
    .locals 13
    .param p1, "syncHeader"    # Lcom/policydm/eng/parser/XDMParserSyncheader;

    .prologue
    const/4 v5, 0x0

    const/4 v12, -0x1

    const/4 v6, 0x0

    const/16 v0, -0x9

    .line 290
    sget-object v11, Lcom/policydm/agent/XDMAgent;->g_DmWs:Lcom/policydm/eng/core/XDMWorkspace;

    .line 291
    .local v11, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    const/4 v10, 0x0

    .line 292
    .local v10, "szKey":Ljava/lang/String;
    iget-object v7, p1, Lcom/policydm/eng/parser/XDMParserSyncheader;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    .line 293
    .local v7, "cred":Lcom/policydm/eng/parser/XDMParserCred;
    const/4 v9, 0x1

    .line 294
    .local v9, "ret":I
    iget-object v8, v11, Lcom/policydm/eng/core/XDMWorkspace;->recvHmacData:Lcom/policydm/eng/core/XDMHmacData;

    .line 296
    .local v8, "pHMAC":Lcom/policydm/eng/core/XDMHmacData;
    const-string v1, ""

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 298
    iget-object v1, v11, Lcom/policydm/eng/core/XDMWorkspace;->m_szServerID:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 300
    const-string v1, "ServerID is null"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 301
    iget v1, v11, Lcom/policydm/eng/core/XDMWorkspace;->serverAuthState:I

    const/4 v2, -0x7

    if-eq v1, v2, :cond_0

    iget v1, v11, Lcom/policydm/eng/core/XDMWorkspace;->serverAuthState:I

    if-ne v1, v0, :cond_1

    .line 302
    :cond_0
    const/4 v9, -0x1

    :goto_0
    move v0, v9

    .line 418
    :goto_1
    return v0

    .line 304
    :cond_1
    const/16 v9, -0x9

    goto :goto_0

    .line 309
    :cond_2
    iget v1, v11, Lcom/policydm/eng/core/XDMWorkspace;->serverCredType:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_a

    .line 311
    if-nez v8, :cond_3

    .line 313
    const-string v1, "HAMC is null"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto :goto_1

    .line 317
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "algorighm : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v8, Lcom/policydm/eng/core/XDMHmacData;->m_szHmacAlgorithm:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 319
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "digest : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v8, Lcom/policydm/eng/core/XDMHmacData;->m_szHmacDigest:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 321
    iget-object v1, v8, Lcom/policydm/eng/core/XDMHmacData;->m_szHmacAlgorithm:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, v8, Lcom/policydm/eng/core/XDMHmacData;->m_szHmacUserName:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, v8, Lcom/policydm/eng/core/XDMHmacData;->m_szHmacDigest:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 323
    :cond_4
    const-string v1, "Any of MAC data is empty"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto :goto_1

    .line 326
    :cond_5
    const-string v1, "MD5"

    iget-object v2, v8, Lcom/policydm/eng/core/XDMHmacData;->m_szHmacAlgorithm:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_6

    .line 328
    const-string v1, "State No Credential"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto :goto_1

    .line 333
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "credtype:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, v11, Lcom/policydm/eng/core/XDMWorkspace;->serverCredType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", nextNonce:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v1, Ljava/lang/String;

    iget-object v2, v11, Lcom/policydm/eng/core/XDMWorkspace;->serverNextNonce:[B

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 334
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "httpContentLength:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, v8, Lcom/policydm/eng/core/XDMHmacData;->httpContentLength:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 336
    iget v0, v11, Lcom/policydm/eng/core/XDMWorkspace;->serverCredType:I

    iget-object v1, v11, Lcom/policydm/eng/core/XDMWorkspace;->m_szServerID:Ljava/lang/String;

    iget-object v2, v11, Lcom/policydm/eng/core/XDMWorkspace;->m_szServerPW:Ljava/lang/String;

    iget-object v3, v11, Lcom/policydm/eng/core/XDMWorkspace;->serverNextNonce:[B

    iget-object v4, v11, Lcom/policydm/eng/core/XDMWorkspace;->serverNextNonce:[B

    array-length v4, v4

    iget-object v5, v11, Lcom/policydm/eng/core/XDMWorkspace;->buf:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    iget v6, v8, Lcom/policydm/eng/core/XDMHmacData;->httpContentLength:I

    invoke-static/range {v0 .. v6}, Lcom/policydm/eng/core/XDMAuth;->xdmAuthMakeDigest(ILjava/lang/String;Ljava/lang/String;[BI[BI)Ljava/lang/String;

    move-result-object v10

    .line 337
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 339
    const-string v0, "key is null"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    move v0, v12

    .line 340
    goto/16 :goto_1

    .line 343
    :cond_7
    iget-object v0, v8, Lcom/policydm/eng/core/XDMHmacData;->m_szHmacDigest:Ljava/lang/String;

    invoke-virtual {v10, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_8

    .line 345
    const-string v0, "key and pHMAC.hamcDigest not equal"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    move v0, v12

    .line 346
    goto/16 :goto_1

    .line 348
    :cond_8
    const/4 v9, 0x1

    :cond_9
    :goto_2
    move v0, v9

    .line 418
    goto/16 :goto_1

    .line 352
    :cond_a
    iget-object v0, v7, Lcom/policydm/eng/parser/XDMParserCred;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    if-nez v0, :cond_b

    .line 354
    const/16 v9, -0x9

    goto :goto_2

    .line 358
    :cond_b
    iget-object v0, v7, Lcom/policydm/eng/parser/XDMParserCred;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v0, v0, Lcom/policydm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    invoke-static {v0}, Lcom/policydm/eng/core/XDMAuth;->xdmAuthCredString2Type(Ljava/lang/String;)I

    move-result v0

    iget v1, v11, Lcom/policydm/eng/core/XDMWorkspace;->serverCredType:I

    if-ne v0, v1, :cond_11

    .line 360
    const-string v0, "syncml:auth-md5"

    iget-object v1, v7, Lcom/policydm/eng/parser/XDMParserCred;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v1, v1, Lcom/policydm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_e

    .line 362
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CRED_TYPE_MD5 ws.serverCredType : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, v11, Lcom/policydm/eng/core/XDMWorkspace;->serverCredType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 363
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CRED_TYPE_MD5 ws.serverID : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v11, Lcom/policydm/eng/core/XDMWorkspace;->m_szServerID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 364
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CRED_TYPE_MD5 ws.serverPW : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v11, Lcom/policydm/eng/core/XDMWorkspace;->m_szServerPW:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 366
    iget v0, v11, Lcom/policydm/eng/core/XDMWorkspace;->serverCredType:I

    iget-object v1, v11, Lcom/policydm/eng/core/XDMWorkspace;->m_szServerID:Ljava/lang/String;

    iget-object v2, v11, Lcom/policydm/eng/core/XDMWorkspace;->m_szServerPW:Ljava/lang/String;

    iget-object v3, v11, Lcom/policydm/eng/core/XDMWorkspace;->serverNextNonce:[B

    iget-object v4, v11, Lcom/policydm/eng/core/XDMWorkspace;->serverNextNonce:[B

    array-length v4, v4

    invoke-static/range {v0 .. v6}, Lcom/policydm/eng/core/XDMAuth;->xdmAuthMakeDigest(ILjava/lang/String;Ljava/lang/String;[BI[BI)Ljava/lang/String;

    move-result-object v10

    .line 367
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 369
    const-string v0, "key is null"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 370
    const/4 v9, -0x1

    goto/16 :goto_2

    .line 374
    :cond_c
    iget-object v0, v7, Lcom/policydm/eng/parser/XDMParserCred;->m_szData:Ljava/lang/String;

    invoke-virtual {v10, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_d

    .line 376
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "key.compareTo(cred.data) != 0 key= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " cred.data= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v7, Lcom/policydm/eng/parser/XDMParserCred;->m_szData:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 377
    const-string v0, "key and cred.data not equal"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 378
    const/4 v9, -0x1

    goto/16 :goto_2

    .line 382
    :cond_d
    const/4 v9, 0x1

    goto/16 :goto_2

    .line 386
    :cond_e
    const-string v0, "syncml:auth-basic"

    iget-object v1, v7, Lcom/policydm/eng/parser/XDMParserCred;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v1, v1, Lcom/policydm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_9

    .line 388
    iget v0, v11, Lcom/policydm/eng/core/XDMWorkspace;->serverCredType:I

    iget-object v1, v11, Lcom/policydm/eng/core/XDMWorkspace;->m_szServerID:Ljava/lang/String;

    iget-object v2, v11, Lcom/policydm/eng/core/XDMWorkspace;->m_szServerPW:Ljava/lang/String;

    const-string v3, ""

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v3

    move v4, v6

    invoke-static/range {v0 .. v6}, Lcom/policydm/eng/core/XDMAuth;->xdmAuthMakeDigest(ILjava/lang/String;Ljava/lang/String;[BI[BI)Ljava/lang/String;

    move-result-object v10

    .line 390
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 392
    const-string v0, "key is null"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 393
    const/4 v9, -0x1

    goto/16 :goto_2

    .line 397
    :cond_f
    iget-object v0, v7, Lcom/policydm/eng/parser/XDMParserCred;->m_szData:Ljava/lang/String;

    invoke-virtual {v10, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_10

    .line 399
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "key.compareTo(cred.data) != 0 key= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " cred.data= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v7, Lcom/policydm/eng/parser/XDMParserCred;->m_szData:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 400
    const-string v0, "key and cred.data not equal"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 401
    const/4 v9, -0x1

    goto/16 :goto_2

    .line 405
    :cond_10
    const/4 v9, 0x1

    goto/16 :goto_2

    .line 412
    :cond_11
    const-string v0, "server auth type is mismatch"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 413
    const/4 v9, -0x1

    goto/16 :goto_2
.end method

.method public xdmAgent_MAKE_REP_ITEM(Lcom/policydm/eng/core/XDMOmTree;Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/String;I)V
    .locals 3
    .param p1, "om"    # Lcom/policydm/eng/core/XDMOmTree;
    .param p2, "list"    # Lcom/policydm/eng/core/XDMLinkedList;
    .param p3, "szPath"    # Ljava/lang/String;
    .param p4, "node_size"    # I

    .prologue
    .line 2548
    new-array v0, p4, [C

    .line 2550
    .local v0, "buf":[C
    const/4 v2, 0x0

    invoke-static {p1, p3, v2, v0, p4}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmRead(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;I[CI)I

    move-result v2

    if-gez v2, :cond_0

    .line 2552
    const-string v2, "xdmOmRead failed"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 2554
    :cond_0
    new-instance v1, Lcom/policydm/eng/parser/XDMParserItem;

    invoke-direct {v1}, Lcom/policydm/eng/parser/XDMParserItem;-><init>()V

    .line 2555
    .local v1, "item":Lcom/policydm/eng/parser/XDMParserItem;
    iput-object p3, v1, Lcom/policydm/eng/parser/XDMParserItem;->m_szSource:Ljava/lang/String;

    .line 2556
    invoke-static {v0}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStString2Pcdata([C)Lcom/policydm/eng/parser/XDMParserPcdata;

    move-result-object v2

    iput-object v2, v1, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    .line 2557
    invoke-static {p2, v1}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 2558
    return-void
.end method

.method public xdm_SET_OM_STR(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 1
    .param p1, "om"    # Lcom/policydm/eng/core/XDMOmTree;
    .param p2, "szPath"    # Ljava/lang/String;
    .param p3, "szData"    # Ljava/lang/String;
    .param p4, "aclValue"    # I
    .param p5, "scope"    # I

    .prologue
    .line 2002
    invoke-static {p1, p2}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2004
    invoke-static {p2, p3}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetOM(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2005
    invoke-static {p1, p2, p4, p5}, Lcom/policydm/agent/XDMAgent;->xdmAgentMakeDefaultAcl(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 2007
    :cond_0
    return-void
.end method
