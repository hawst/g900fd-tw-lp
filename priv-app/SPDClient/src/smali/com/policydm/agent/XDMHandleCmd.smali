.class public Lcom/policydm/agent/XDMHandleCmd;
.super Ljava/lang/Object;
.source "XDMHandleCmd.java"

# interfaces
.implements Lcom/policydm/interfaces/XDMInterface;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xdmAgentDataStDeleteAlert(Ljava/lang/Object;)V
    .locals 3
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x0

    .line 1431
    move-object v0, p0

    check-cast v0, Lcom/policydm/eng/parser/XDMParserAlert;

    .line 1432
    .local v0, "alert":Lcom/policydm/eng/parser/XDMParserAlert;
    if-nez v0, :cond_0

    .line 1451
    :goto_0
    return-void

    .line 1436
    :cond_0
    const/4 v1, 0x0

    iput v1, v0, Lcom/policydm/eng/parser/XDMParserAlert;->cmdid:I

    .line 1438
    iput-object v2, v0, Lcom/policydm/eng/parser/XDMParserAlert;->m_szCorrelator:Ljava/lang/String;

    .line 1440
    iget-object v1, v0, Lcom/policydm/eng/parser/XDMParserAlert;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    if-eqz v1, :cond_1

    .line 1442
    iget-object v1, v0, Lcom/policydm/eng/parser/XDMParserAlert;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    invoke-static {v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDeleteCred(Ljava/lang/Object;)V

    .line 1444
    :cond_1
    iput-object v2, v0, Lcom/policydm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    .line 1446
    iget-object v1, v0, Lcom/policydm/eng/parser/XDMParserAlert;->itemlist:Lcom/policydm/eng/core/XDMList;

    if-eqz v1, :cond_2

    .line 1448
    iget-object v1, v0, Lcom/policydm/eng/parser/XDMParserAlert;->itemlist:Lcom/policydm/eng/core/XDMList;

    invoke-static {v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDeleteItemlist(Ljava/lang/Object;)V

    .line 1450
    :cond_2
    const/4 v0, 0x0

    .line 1451
    goto :goto_0
.end method

.method public static xdmAgentDataStDeleteCred(Ljava/lang/Object;)V
    .locals 2
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    .line 1391
    move-object v0, p0

    check-cast v0, Lcom/policydm/eng/parser/XDMParserCred;

    .line 1392
    .local v0, "cred":Lcom/policydm/eng/parser/XDMParserCred;
    if-nez v0, :cond_0

    .line 1403
    :goto_0
    return-void

    .line 1396
    :cond_0
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/policydm/eng/parser/XDMParserCred;->m_szData:Ljava/lang/String;

    .line 1398
    iget-object v1, v0, Lcom/policydm/eng/parser/XDMParserCred;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    if-eqz v1, :cond_1

    .line 1400
    iget-object v1, v0, Lcom/policydm/eng/parser/XDMParserCred;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-static {v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDeleteMeta(Ljava/lang/Object;)V

    .line 1402
    :cond_1
    const/4 v0, 0x0

    .line 1403
    goto :goto_0
.end method

.method public static xdmAgentDataStDeleteElelist(Ljava/lang/Object;)V
    .locals 4
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    .line 1411
    move-object v1, p0

    check-cast v1, Lcom/policydm/eng/core/XDMList;

    .line 1414
    .local v1, "h":Lcom/policydm/eng/core/XDMList;
    move-object v0, v1

    .line 1415
    .local v0, "curr":Lcom/policydm/eng/core/XDMList;
    :goto_0
    if-eqz v0, :cond_0

    .line 1417
    move-object v2, v0

    .line 1418
    .local v2, "tmp":Lcom/policydm/eng/core/XDMList;
    iget-object v0, v0, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 1420
    const/4 v3, 0x0

    iput-object v3, v2, Lcom/policydm/eng/core/XDMList;->item:Ljava/lang/Object;

    .line 1421
    const/4 v2, 0x0

    goto :goto_0

    .line 1423
    .end local v2    # "tmp":Lcom/policydm/eng/core/XDMList;
    :cond_0
    return-void
.end method

.method public static xdmAgentDataStDeleteItem(Ljava/lang/Object;)V
    .locals 3
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x0

    .line 1322
    move-object v0, p0

    check-cast v0, Lcom/policydm/eng/parser/XDMParserItem;

    .line 1323
    .local v0, "item":Lcom/policydm/eng/parser/XDMParserItem;
    if-nez v0, :cond_0

    .line 1343
    :goto_0
    return-void

    .line 1328
    :cond_0
    iget-object v1, v0, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    if-eqz v1, :cond_1

    .line 1330
    iget-object v1, v0, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    invoke-static {v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDeletePcdata(Ljava/lang/Object;)V

    .line 1333
    :cond_1
    iput-object v2, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szSource:Ljava/lang/String;

    .line 1334
    iput-object v2, v0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    .line 1336
    iget-object v1, v0, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    if-eqz v1, :cond_2

    .line 1338
    iget-object v1, v0, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-static {v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDeleteMeta(Ljava/lang/Object;)V

    .line 1341
    :cond_2
    const/4 v0, 0x0

    .line 1343
    goto :goto_0
.end method

.method public static xdmAgentDataStDeleteItemlist(Ljava/lang/Object;)V
    .locals 4
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    .line 1300
    move-object v1, p0

    check-cast v1, Lcom/policydm/eng/core/XDMList;

    .line 1301
    .local v1, "header":Lcom/policydm/eng/core/XDMList;
    move-object v0, v1

    .line 1304
    .local v0, "curr":Lcom/policydm/eng/core/XDMList;
    :goto_0
    if-eqz v0, :cond_0

    .line 1306
    move-object v2, v0

    .line 1307
    .local v2, "tmp":Lcom/policydm/eng/core/XDMList;
    iget-object v0, v0, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 1309
    iget-object v3, v2, Lcom/policydm/eng/core/XDMList;->item:Ljava/lang/Object;

    invoke-static {v3}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDeleteItem(Ljava/lang/Object;)V

    .line 1310
    const/4 v2, 0x0

    goto :goto_0

    .line 1313
    .end local v2    # "tmp":Lcom/policydm/eng/core/XDMList;
    :cond_0
    const/4 v1, 0x0

    .line 1314
    return-void
.end method

.method public static xdmAgentDataStDeleteMeta(Ljava/lang/Object;)V
    .locals 4
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1233
    move-object v0, p0

    check-cast v0, Lcom/policydm/eng/parser/XDMParserMeta;

    .line 1234
    .local v0, "meta":Lcom/policydm/eng/parser/XDMParserMeta;
    if-nez v0, :cond_0

    .line 1257
    :goto_0
    return-void

    .line 1238
    :cond_0
    iget-object v1, v0, Lcom/policydm/eng/parser/XDMParserMeta;->anchor:Lcom/policydm/eng/parser/XDMParserAnchor;

    if-eqz v1, :cond_1

    .line 1240
    iget-object v1, v0, Lcom/policydm/eng/parser/XDMParserMeta;->anchor:Lcom/policydm/eng/parser/XDMParserAnchor;

    invoke-static {v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDeleteMetinfAnchor(Ljava/lang/Object;)V

    .line 1242
    :cond_1
    iget-object v1, v0, Lcom/policydm/eng/parser/XDMParserMeta;->mem:Lcom/policydm/eng/parser/XDMParserMem;

    if-eqz v1, :cond_2

    .line 1244
    iget-object v1, v0, Lcom/policydm/eng/parser/XDMParserMeta;->mem:Lcom/policydm/eng/parser/XDMParserMem;

    invoke-static {v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDeleteMetinfMem(Ljava/lang/Object;)V

    .line 1247
    :cond_2
    iput-object v2, v0, Lcom/policydm/eng/parser/XDMParserMeta;->m_szEmi:Ljava/lang/String;

    .line 1248
    iput-object v2, v0, Lcom/policydm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    .line 1249
    iput-object v2, v0, Lcom/policydm/eng/parser/XDMParserMeta;->m_szMark:Ljava/lang/String;

    .line 1250
    iput v3, v0, Lcom/policydm/eng/parser/XDMParserMeta;->maxmsgsize:I

    .line 1251
    iput v3, v0, Lcom/policydm/eng/parser/XDMParserMeta;->maxobjsize:I

    .line 1252
    iput-object v2, v0, Lcom/policydm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    .line 1253
    iput v3, v0, Lcom/policydm/eng/parser/XDMParserMeta;->size:I

    .line 1254
    iput-object v2, v0, Lcom/policydm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    .line 1255
    iput-object v2, v0, Lcom/policydm/eng/parser/XDMParserMeta;->m_szVersion:Ljava/lang/String;

    .line 1256
    const/4 v0, 0x0

    .line 1257
    goto :goto_0
.end method

.method public static xdmAgentDataStDeleteMetinfAnchor(Ljava/lang/Object;)V
    .locals 2
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 1265
    move-object v0, p0

    check-cast v0, Lcom/policydm/eng/parser/XDMParserAnchor;

    .line 1266
    .local v0, "anchor":Lcom/policydm/eng/parser/XDMParserAnchor;
    if-nez v0, :cond_0

    .line 1274
    :goto_0
    return-void

    .line 1271
    :cond_0
    iput-object v1, v0, Lcom/policydm/eng/parser/XDMParserAnchor;->m_szLast:Ljava/lang/String;

    .line 1272
    iput-object v1, v0, Lcom/policydm/eng/parser/XDMParserAnchor;->m_szNext:Ljava/lang/String;

    .line 1273
    const/4 v0, 0x0

    .line 1274
    goto :goto_0
.end method

.method public static xdmAgentDataStDeleteMetinfMem(Ljava/lang/Object;)V
    .locals 2
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 1282
    move-object v0, p0

    check-cast v0, Lcom/policydm/eng/parser/XDMParserMem;

    .line 1283
    .local v0, "mem":Lcom/policydm/eng/parser/XDMParserMem;
    if-nez v0, :cond_0

    .line 1292
    :goto_0
    return-void

    .line 1288
    :cond_0
    iput v1, v0, Lcom/policydm/eng/parser/XDMParserMem;->free:I

    .line 1289
    iput v1, v0, Lcom/policydm/eng/parser/XDMParserMem;->freeid:I

    .line 1290
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/policydm/eng/parser/XDMParserMem;->m_szShared:Ljava/lang/String;

    .line 1291
    const/4 v0, 0x0

    .line 1292
    goto :goto_0
.end method

.method public static xdmAgentDataStDeletePcdata(Ljava/lang/Object;)V
    .locals 2
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    .line 1351
    move-object v0, p0

    check-cast v0, Lcom/policydm/eng/parser/XDMParserPcdata;

    .line 1352
    .local v0, "pcdata":Lcom/policydm/eng/parser/XDMParserPcdata;
    if-nez v0, :cond_0

    .line 1364
    :goto_0
    return-void

    .line 1357
    :cond_0
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/policydm/eng/parser/XDMParserPcdata;->data:[C

    .line 1358
    iget-object v1, v0, Lcom/policydm/eng/parser/XDMParserPcdata;->anchor:Lcom/policydm/eng/parser/XDMParserAnchor;

    if-eqz v1, :cond_1

    .line 1360
    iget-object v1, v0, Lcom/policydm/eng/parser/XDMParserPcdata;->anchor:Lcom/policydm/eng/parser/XDMParserAnchor;

    invoke-static {v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDeleteMetinfAnchor(Ljava/lang/Object;)V

    .line 1363
    :cond_1
    const/4 v0, 0x0

    .line 1364
    goto :goto_0
.end method

.method public static xdmAgentDataStDeleteReplace(Ljava/lang/Object;)V
    .locals 2
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    .line 1459
    move-object v0, p0

    check-cast v0, Lcom/policydm/eng/parser/XDMParserReplace;

    .line 1460
    .local v0, "rep":Lcom/policydm/eng/parser/XDMParserReplace;
    if-nez v0, :cond_0

    .line 1479
    :goto_0
    return-void

    .line 1465
    :cond_0
    iget-object v1, v0, Lcom/policydm/eng/parser/XDMParserReplace;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    if-eqz v1, :cond_1

    .line 1467
    iget-object v1, v0, Lcom/policydm/eng/parser/XDMParserReplace;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    invoke-static {v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDeleteCred(Ljava/lang/Object;)V

    .line 1469
    :cond_1
    iget-object v1, v0, Lcom/policydm/eng/parser/XDMParserReplace;->itemlist:Lcom/policydm/eng/core/XDMList;

    if-eqz v1, :cond_2

    .line 1471
    iget-object v1, v0, Lcom/policydm/eng/parser/XDMParserReplace;->itemlist:Lcom/policydm/eng/core/XDMList;

    invoke-static {v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDeleteItemlist(Ljava/lang/Object;)V

    .line 1473
    :cond_2
    iget-object v1, v0, Lcom/policydm/eng/parser/XDMParserReplace;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    if-eqz v1, :cond_3

    .line 1475
    iget-object v1, v0, Lcom/policydm/eng/parser/XDMParserReplace;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-static {v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDeleteMeta(Ljava/lang/Object;)V

    .line 1477
    :cond_3
    const/4 v1, 0x0

    iput v1, v0, Lcom/policydm/eng/parser/XDMParserReplace;->cmdid:I

    .line 1478
    const/4 v0, 0x0

    .line 1479
    goto :goto_0
.end method

.method public static xdmAgentDataStDeleteResults(Ljava/lang/Object;)V
    .locals 3
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x0

    .line 1504
    move-object v0, p0

    check-cast v0, Lcom/policydm/eng/parser/XDMParserResults;

    .line 1505
    .local v0, "results":Lcom/policydm/eng/parser/XDMParserResults;
    if-nez v0, :cond_0

    .line 1526
    :goto_0
    return-void

    .line 1510
    :cond_0
    const/4 v1, 0x0

    iput v1, v0, Lcom/policydm/eng/parser/XDMParserResults;->cmdid:I

    .line 1511
    iput-object v2, v0, Lcom/policydm/eng/parser/XDMParserResults;->m_szMsgRef:Ljava/lang/String;

    .line 1512
    iput-object v2, v0, Lcom/policydm/eng/parser/XDMParserResults;->m_szCmdRef:Ljava/lang/String;

    .line 1514
    iget-object v1, v0, Lcom/policydm/eng/parser/XDMParserResults;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    if-eqz v1, :cond_1

    .line 1516
    iget-object v1, v0, Lcom/policydm/eng/parser/XDMParserResults;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-static {v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDeleteMeta(Ljava/lang/Object;)V

    .line 1518
    :cond_1
    iput-object v2, v0, Lcom/policydm/eng/parser/XDMParserResults;->m_szTargetRef:Ljava/lang/String;

    .line 1519
    iput-object v2, v0, Lcom/policydm/eng/parser/XDMParserResults;->m_szSourceRef:Ljava/lang/String;

    .line 1521
    iget-object v1, v0, Lcom/policydm/eng/parser/XDMParserResults;->itemlist:Lcom/policydm/eng/core/XDMList;

    if-eqz v1, :cond_2

    .line 1523
    iget-object v1, v0, Lcom/policydm/eng/parser/XDMParserResults;->itemlist:Lcom/policydm/eng/core/XDMList;

    invoke-static {v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDeleteItemlist(Ljava/lang/Object;)V

    .line 1525
    :cond_2
    const/4 v0, 0x0

    .line 1526
    goto :goto_0
.end method

.method public static xdmAgentDataStDeleteStatus(Ljava/lang/Object;)V
    .locals 3
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x0

    .line 1191
    move-object v0, p0

    check-cast v0, Lcom/policydm/eng/parser/XDMParserStatus;

    .line 1192
    .local v0, "status":Lcom/policydm/eng/parser/XDMParserStatus;
    if-nez v0, :cond_0

    .line 1225
    :goto_0
    return-void

    .line 1197
    :cond_0
    iget-object v1, v0, Lcom/policydm/eng/parser/XDMParserStatus;->chal:Lcom/policydm/eng/parser/XDMParserMeta;

    if-eqz v1, :cond_1

    .line 1199
    iget-object v1, v0, Lcom/policydm/eng/parser/XDMParserStatus;->chal:Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-static {v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDeleteMeta(Ljava/lang/Object;)V

    .line 1201
    :cond_1
    iget-object v1, v0, Lcom/policydm/eng/parser/XDMParserStatus;->itemlist:Lcom/policydm/eng/core/XDMList;

    if-eqz v1, :cond_2

    .line 1203
    iget-object v1, v0, Lcom/policydm/eng/parser/XDMParserStatus;->itemlist:Lcom/policydm/eng/core/XDMList;

    invoke-static {v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDeleteItemlist(Ljava/lang/Object;)V

    .line 1205
    :cond_2
    iget-object v1, v0, Lcom/policydm/eng/parser/XDMParserStatus;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    if-eqz v1, :cond_3

    .line 1207
    iget-object v1, v0, Lcom/policydm/eng/parser/XDMParserStatus;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    invoke-static {v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDeleteCred(Ljava/lang/Object;)V

    .line 1209
    :cond_3
    iput-object v2, v0, Lcom/policydm/eng/parser/XDMParserStatus;->m_szCmd:Ljava/lang/String;

    .line 1210
    const/4 v1, 0x0

    iput v1, v0, Lcom/policydm/eng/parser/XDMParserStatus;->cmdid:I

    .line 1211
    iput-object v2, v0, Lcom/policydm/eng/parser/XDMParserStatus;->m_szCmdRef:Ljava/lang/String;

    .line 1212
    iput-object v2, v0, Lcom/policydm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    .line 1213
    iput-object v2, v0, Lcom/policydm/eng/parser/XDMParserStatus;->m_szMsgRef:Ljava/lang/String;

    .line 1215
    iget-object v1, v0, Lcom/policydm/eng/parser/XDMParserStatus;->sourceref:Lcom/policydm/eng/core/XDMList;

    if-eqz v1, :cond_4

    .line 1217
    iget-object v1, v0, Lcom/policydm/eng/parser/XDMParserStatus;->sourceref:Lcom/policydm/eng/core/XDMList;

    invoke-static {v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDeleteElelist(Ljava/lang/Object;)V

    .line 1219
    :cond_4
    iget-object v1, v0, Lcom/policydm/eng/parser/XDMParserStatus;->targetref:Lcom/policydm/eng/core/XDMList;

    if-eqz v1, :cond_5

    .line 1221
    iget-object v1, v0, Lcom/policydm/eng/parser/XDMParserStatus;->targetref:Lcom/policydm/eng/core/XDMList;

    invoke-static {v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDeleteElelist(Ljava/lang/Object;)V

    .line 1223
    :cond_5
    const/4 v0, 0x0

    .line 1225
    goto :goto_0
.end method

.method public static xdmAgentDataStDeleteTarget(Ljava/lang/Object;)V
    .locals 2
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 1372
    move-object v0, p0

    check-cast v0, Lcom/policydm/eng/parser/XDMParserTarget;

    .line 1374
    .local v0, "pTarget":Lcom/policydm/eng/parser/XDMParserTarget;
    if-nez v0, :cond_0

    .line 1383
    :goto_0
    return-void

    .line 1379
    :cond_0
    iput-object v1, v0, Lcom/policydm/eng/parser/XDMParserTarget;->m_szLocURI:Ljava/lang/String;

    .line 1380
    iput-object v1, v0, Lcom/policydm/eng/parser/XDMParserTarget;->m_szLocName:Ljava/lang/String;

    .line 1382
    const/4 v0, 0x0

    .line 1383
    goto :goto_0
.end method

.method public static xdmAgentDataStDuplAlert(Lcom/policydm/eng/parser/XDMParserAlert;Lcom/policydm/eng/parser/XDMParserAlert;)V
    .locals 2
    .param p0, "dest"    # Lcom/policydm/eng/parser/XDMParserAlert;
    .param p1, "src"    # Lcom/policydm/eng/parser/XDMParserAlert;

    .prologue
    .line 665
    if-nez p1, :cond_1

    .line 691
    :cond_0
    :goto_0
    return-void

    .line 670
    :cond_1
    iget v0, p1, Lcom/policydm/eng/parser/XDMParserAlert;->cmdid:I

    if-lez v0, :cond_2

    .line 672
    iget v0, p1, Lcom/policydm/eng/parser/XDMParserAlert;->cmdid:I

    iput v0, p0, Lcom/policydm/eng/parser/XDMParserAlert;->cmdid:I

    .line 674
    :cond_2
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserAlert;->m_szCorrelator:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 676
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserAlert;->m_szCorrelator:Ljava/lang/String;

    iput-object v0, p0, Lcom/policydm/eng/parser/XDMParserAlert;->m_szCorrelator:Ljava/lang/String;

    .line 678
    :cond_3
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserAlert;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    if-eqz v0, :cond_4

    .line 680
    new-instance v0, Lcom/policydm/eng/parser/XDMParserCred;

    invoke-direct {v0}, Lcom/policydm/eng/parser/XDMParserCred;-><init>()V

    iput-object v0, p0, Lcom/policydm/eng/parser/XDMParserAlert;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    .line 681
    iget-object v0, p0, Lcom/policydm/eng/parser/XDMParserAlert;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    iget-object v1, p1, Lcom/policydm/eng/parser/XDMParserAlert;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    invoke-static {v0, v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplCred(Lcom/policydm/eng/parser/XDMParserCred;Lcom/policydm/eng/parser/XDMParserCred;)V

    .line 683
    :cond_4
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 685
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    iput-object v0, p0, Lcom/policydm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    .line 687
    :cond_5
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserAlert;->itemlist:Lcom/policydm/eng/core/XDMList;

    if-eqz v0, :cond_0

    .line 689
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserAlert;->itemlist:Lcom/policydm/eng/core/XDMList;

    invoke-static {v0}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplItemlist(Lcom/policydm/eng/core/XDMList;)Lcom/policydm/eng/core/XDMList;

    move-result-object v0

    iput-object v0, p0, Lcom/policydm/eng/parser/XDMParserAlert;->itemlist:Lcom/policydm/eng/core/XDMList;

    goto :goto_0
.end method

.method public static xdmAgentDataStDuplCred(Lcom/policydm/eng/parser/XDMParserCred;Lcom/policydm/eng/parser/XDMParserCred;)V
    .locals 2
    .param p0, "dest"    # Lcom/policydm/eng/parser/XDMParserCred;
    .param p1, "src"    # Lcom/policydm/eng/parser/XDMParserCred;

    .prologue
    .line 846
    if-nez p1, :cond_1

    .line 859
    :cond_0
    :goto_0
    return-void

    .line 850
    :cond_1
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserCred;->m_szData:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 852
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserCred;->m_szData:Ljava/lang/String;

    iput-object v0, p0, Lcom/policydm/eng/parser/XDMParserCred;->m_szData:Ljava/lang/String;

    .line 854
    :cond_2
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserCred;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    if-eqz v0, :cond_0

    .line 856
    new-instance v0, Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-direct {v0}, Lcom/policydm/eng/parser/XDMParserMeta;-><init>()V

    iput-object v0, p0, Lcom/policydm/eng/parser/XDMParserCred;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    .line 857
    iget-object v0, p0, Lcom/policydm/eng/parser/XDMParserCred;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v1, p1, Lcom/policydm/eng/parser/XDMParserCred;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-static {v0, v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplMeta(Lcom/policydm/eng/parser/XDMParserMeta;Lcom/policydm/eng/parser/XDMParserMeta;)V

    goto :goto_0
.end method

.method public static xdmAgentDataStDuplItem(Lcom/policydm/eng/parser/XDMParserItem;Lcom/policydm/eng/parser/XDMParserItem;)V
    .locals 2
    .param p0, "dest"    # Lcom/policydm/eng/parser/XDMParserItem;
    .param p1, "src"    # Lcom/policydm/eng/parser/XDMParserItem;

    .prologue
    .line 956
    if-nez p1, :cond_0

    .line 981
    :goto_0
    return-void

    .line 961
    :cond_0
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 963
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    iput-object v0, p0, Lcom/policydm/eng/parser/XDMParserItem;->m_szTarget:Ljava/lang/String;

    .line 965
    :cond_1
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserItem;->m_szSource:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 967
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserItem;->m_szSource:Ljava/lang/String;

    iput-object v0, p0, Lcom/policydm/eng/parser/XDMParserItem;->m_szSource:Ljava/lang/String;

    .line 969
    :cond_2
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    if-eqz v0, :cond_3

    .line 971
    new-instance v0, Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-direct {v0}, Lcom/policydm/eng/parser/XDMParserMeta;-><init>()V

    iput-object v0, p0, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    .line 972
    iget-object v0, p0, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v1, p1, Lcom/policydm/eng/parser/XDMParserItem;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-static {v0, v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplMeta(Lcom/policydm/eng/parser/XDMParserMeta;Lcom/policydm/eng/parser/XDMParserMeta;)V

    .line 974
    :cond_3
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    if-eqz v0, :cond_4

    .line 976
    new-instance v0, Lcom/policydm/eng/parser/XDMParserPcdata;

    invoke-direct {v0}, Lcom/policydm/eng/parser/XDMParserPcdata;-><init>()V

    iput-object v0, p0, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    .line 977
    iget-object v0, p0, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    iget-object v1, p1, Lcom/policydm/eng/parser/XDMParserItem;->data:Lcom/policydm/eng/parser/XDMParserPcdata;

    invoke-static {v0, v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplPcdata(Lcom/policydm/eng/parser/XDMParserPcdata;Lcom/policydm/eng/parser/XDMParserPcdata;)V

    .line 979
    :cond_4
    iget v0, p1, Lcom/policydm/eng/parser/XDMParserItem;->moredata:I

    iput v0, p0, Lcom/policydm/eng/parser/XDMParserItem;->moredata:I

    goto :goto_0
.end method

.method public static xdmAgentDataStDuplItemlist(Lcom/policydm/eng/core/XDMList;)Lcom/policydm/eng/core/XDMList;
    .locals 7
    .param p0, "src"    # Lcom/policydm/eng/core/XDMList;

    .prologue
    .line 928
    move-object v0, p0

    .line 931
    .local v0, "curr":Lcom/policydm/eng/core/XDMList;
    const/4 v1, 0x0

    .local v1, "head":Lcom/policydm/eng/core/XDMList;
    const/4 v3, 0x0

    .line 933
    .local v3, "tail":Lcom/policydm/eng/core/XDMList;
    :goto_0
    if-eqz v0, :cond_1

    .line 935
    move-object v4, v0

    .line 936
    .local v4, "tmp":Lcom/policydm/eng/core/XDMList;
    iget-object v0, v0, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 938
    new-instance v2, Lcom/policydm/eng/parser/XDMParserItem;

    invoke-direct {v2}, Lcom/policydm/eng/parser/XDMParserItem;-><init>()V

    .local v2, "item":Lcom/policydm/eng/parser/XDMParserItem;
    move-object v5, v2

    .line 939
    check-cast v5, Lcom/policydm/eng/parser/XDMParserItem;

    iget-object v6, v4, Lcom/policydm/eng/core/XDMList;->item:Ljava/lang/Object;

    check-cast v6, Lcom/policydm/eng/parser/XDMParserItem;

    invoke-static {v5, v6}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplItem(Lcom/policydm/eng/parser/XDMParserItem;Lcom/policydm/eng/parser/XDMParserItem;)V

    .line 940
    if-nez v1, :cond_0

    .line 941
    invoke-static {v1, v3, v2}, Lcom/policydm/eng/core/XDMList;->xdmListAppend(Lcom/policydm/eng/core/XDMList;Lcom/policydm/eng/core/XDMList;Ljava/lang/Object;)Lcom/policydm/eng/core/XDMList;

    move-result-object v1

    goto :goto_0

    .line 943
    :cond_0
    invoke-static {v1, v3, v2}, Lcom/policydm/eng/core/XDMList;->xdmListAppend(Lcom/policydm/eng/core/XDMList;Lcom/policydm/eng/core/XDMList;Ljava/lang/Object;)Lcom/policydm/eng/core/XDMList;

    goto :goto_0

    .line 946
    .end local v2    # "item":Lcom/policydm/eng/parser/XDMParserItem;
    .end local v4    # "tmp":Lcom/policydm/eng/core/XDMList;
    :cond_1
    return-object v1
.end method

.method public static xdmAgentDataStDuplMeta(Lcom/policydm/eng/parser/XDMParserMeta;Lcom/policydm/eng/parser/XDMParserMeta;)V
    .locals 2
    .param p0, "dest"    # Lcom/policydm/eng/parser/XDMParserMeta;
    .param p1, "src"    # Lcom/policydm/eng/parser/XDMParserMeta;

    .prologue
    .line 868
    if-nez p1, :cond_1

    .line 919
    :cond_0
    :goto_0
    return-void

    .line 873
    :cond_1
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 875
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    iput-object v0, p0, Lcom/policydm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    .line 877
    :cond_2
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 879
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    iput-object v0, p0, Lcom/policydm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    .line 881
    :cond_3
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserMeta;->m_szMark:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 883
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserMeta;->m_szMark:Ljava/lang/String;

    iput-object v0, p0, Lcom/policydm/eng/parser/XDMParserMeta;->m_szMark:Ljava/lang/String;

    .line 885
    :cond_4
    iget v0, p1, Lcom/policydm/eng/parser/XDMParserMeta;->size:I

    if-lez v0, :cond_5

    .line 887
    iget v0, p1, Lcom/policydm/eng/parser/XDMParserMeta;->size:I

    iput v0, p0, Lcom/policydm/eng/parser/XDMParserMeta;->size:I

    .line 889
    :cond_5
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 891
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    iput-object v0, p0, Lcom/policydm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    .line 893
    :cond_6
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserMeta;->m_szVersion:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 895
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserMeta;->m_szVersion:Ljava/lang/String;

    iput-object v0, p0, Lcom/policydm/eng/parser/XDMParserMeta;->m_szVersion:Ljava/lang/String;

    .line 897
    :cond_7
    iget v0, p1, Lcom/policydm/eng/parser/XDMParserMeta;->maxmsgsize:I

    if-lez v0, :cond_8

    .line 899
    iget v0, p1, Lcom/policydm/eng/parser/XDMParserMeta;->maxmsgsize:I

    iput v0, p0, Lcom/policydm/eng/parser/XDMParserMeta;->maxmsgsize:I

    .line 901
    :cond_8
    iget v0, p1, Lcom/policydm/eng/parser/XDMParserMeta;->maxobjsize:I

    if-lez v0, :cond_9

    .line 903
    iget v0, p1, Lcom/policydm/eng/parser/XDMParserMeta;->maxobjsize:I

    iput v0, p0, Lcom/policydm/eng/parser/XDMParserMeta;->maxobjsize:I

    .line 905
    :cond_9
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserMeta;->mem:Lcom/policydm/eng/parser/XDMParserMem;

    if-eqz v0, :cond_a

    .line 907
    new-instance v0, Lcom/policydm/eng/parser/XDMParserMem;

    invoke-direct {v0}, Lcom/policydm/eng/parser/XDMParserMem;-><init>()V

    iput-object v0, p0, Lcom/policydm/eng/parser/XDMParserMeta;->mem:Lcom/policydm/eng/parser/XDMParserMem;

    .line 908
    iget-object v0, p0, Lcom/policydm/eng/parser/XDMParserMeta;->mem:Lcom/policydm/eng/parser/XDMParserMem;

    iget-object v1, p1, Lcom/policydm/eng/parser/XDMParserMeta;->mem:Lcom/policydm/eng/parser/XDMParserMem;

    invoke-static {v0, v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplMetinfMem(Lcom/policydm/eng/parser/XDMParserMem;Lcom/policydm/eng/parser/XDMParserMem;)V

    .line 910
    :cond_a
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserMeta;->m_szEmi:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 912
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserMeta;->m_szEmi:Ljava/lang/String;

    iput-object v0, p0, Lcom/policydm/eng/parser/XDMParserMeta;->m_szEmi:Ljava/lang/String;

    .line 914
    :cond_b
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserMeta;->anchor:Lcom/policydm/eng/parser/XDMParserAnchor;

    if-eqz v0, :cond_0

    .line 916
    new-instance v0, Lcom/policydm/eng/parser/XDMParserAnchor;

    invoke-direct {v0}, Lcom/policydm/eng/parser/XDMParserAnchor;-><init>()V

    iput-object v0, p0, Lcom/policydm/eng/parser/XDMParserMeta;->anchor:Lcom/policydm/eng/parser/XDMParserAnchor;

    .line 917
    iget-object v0, p0, Lcom/policydm/eng/parser/XDMParserMeta;->anchor:Lcom/policydm/eng/parser/XDMParserAnchor;

    iget-object v1, p1, Lcom/policydm/eng/parser/XDMParserMeta;->anchor:Lcom/policydm/eng/parser/XDMParserAnchor;

    invoke-static {v0, v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplMetinfAnchor(Lcom/policydm/eng/parser/XDMParserAnchor;Lcom/policydm/eng/parser/XDMParserAnchor;)V

    goto/16 :goto_0
.end method

.method public static xdmAgentDataStDuplMetinfAnchor(Lcom/policydm/eng/parser/XDMParserAnchor;Lcom/policydm/eng/parser/XDMParserAnchor;)V
    .locals 1
    .param p0, "dest"    # Lcom/policydm/eng/parser/XDMParserAnchor;
    .param p1, "src"    # Lcom/policydm/eng/parser/XDMParserAnchor;

    .prologue
    .line 1050
    if-nez p1, :cond_1

    .line 1063
    :cond_0
    :goto_0
    return-void

    .line 1055
    :cond_1
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserAnchor;->m_szLast:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1057
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserAnchor;->m_szLast:Ljava/lang/String;

    iput-object v0, p0, Lcom/policydm/eng/parser/XDMParserAnchor;->m_szLast:Ljava/lang/String;

    .line 1059
    :cond_2
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserAnchor;->m_szNext:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1061
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserAnchor;->m_szNext:Ljava/lang/String;

    iput-object v0, p0, Lcom/policydm/eng/parser/XDMParserAnchor;->m_szNext:Ljava/lang/String;

    goto :goto_0
.end method

.method public static xdmAgentDataStDuplMetinfMem(Lcom/policydm/eng/parser/XDMParserMem;Lcom/policydm/eng/parser/XDMParserMem;)V
    .locals 1
    .param p0, "dest"    # Lcom/policydm/eng/parser/XDMParserMem;
    .param p1, "src"    # Lcom/policydm/eng/parser/XDMParserMem;

    .prologue
    .line 1025
    if-nez p1, :cond_1

    .line 1041
    :cond_0
    :goto_0
    return-void

    .line 1029
    :cond_1
    iget v0, p1, Lcom/policydm/eng/parser/XDMParserMem;->free:I

    if-lez v0, :cond_2

    .line 1031
    iget v0, p1, Lcom/policydm/eng/parser/XDMParserMem;->free:I

    iput v0, p0, Lcom/policydm/eng/parser/XDMParserMem;->free:I

    .line 1033
    :cond_2
    iget v0, p1, Lcom/policydm/eng/parser/XDMParserMem;->freeid:I

    if-lez v0, :cond_3

    .line 1035
    iget v0, p1, Lcom/policydm/eng/parser/XDMParserMem;->freeid:I

    iput v0, p0, Lcom/policydm/eng/parser/XDMParserMem;->freeid:I

    .line 1037
    :cond_3
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserMem;->m_szShared:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1039
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserMem;->m_szShared:Ljava/lang/String;

    iput-object v0, p0, Lcom/policydm/eng/parser/XDMParserMem;->m_szShared:Ljava/lang/String;

    goto :goto_0
.end method

.method public static xdmAgentDataStDuplPcdata(Lcom/policydm/eng/parser/XDMParserPcdata;Lcom/policydm/eng/parser/XDMParserPcdata;)V
    .locals 3
    .param p0, "dest"    # Lcom/policydm/eng/parser/XDMParserPcdata;
    .param p1, "src"    # Lcom/policydm/eng/parser/XDMParserPcdata;

    .prologue
    .line 990
    if-nez p1, :cond_1

    .line 1016
    :cond_0
    :goto_0
    return-void

    .line 995
    :cond_1
    iget v1, p1, Lcom/policydm/eng/parser/XDMParserPcdata;->type:I

    iput v1, p0, Lcom/policydm/eng/parser/XDMParserPcdata;->type:I

    .line 996
    iget v1, p0, Lcom/policydm/eng/parser/XDMParserPcdata;->type:I

    if-nez v1, :cond_3

    .line 998
    iget-object v1, p1, Lcom/policydm/eng/parser/XDMParserPcdata;->data:[C

    iput-object v1, p0, Lcom/policydm/eng/parser/XDMParserPcdata;->data:[C

    .line 999
    iget v1, p1, Lcom/policydm/eng/parser/XDMParserPcdata;->size:I

    iput v1, p0, Lcom/policydm/eng/parser/XDMParserPcdata;->size:I

    .line 1011
    :cond_2
    :goto_1
    iget-object v1, p1, Lcom/policydm/eng/parser/XDMParserPcdata;->anchor:Lcom/policydm/eng/parser/XDMParserAnchor;

    if-eqz v1, :cond_0

    .line 1013
    new-instance v1, Lcom/policydm/eng/parser/XDMParserAnchor;

    invoke-direct {v1}, Lcom/policydm/eng/parser/XDMParserAnchor;-><init>()V

    iput-object v1, p0, Lcom/policydm/eng/parser/XDMParserPcdata;->anchor:Lcom/policydm/eng/parser/XDMParserAnchor;

    .line 1014
    iget-object v1, p0, Lcom/policydm/eng/parser/XDMParserPcdata;->anchor:Lcom/policydm/eng/parser/XDMParserAnchor;

    iget-object v2, p1, Lcom/policydm/eng/parser/XDMParserPcdata;->anchor:Lcom/policydm/eng/parser/XDMParserAnchor;

    invoke-static {v1, v2}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplMetinfAnchor(Lcom/policydm/eng/parser/XDMParserAnchor;Lcom/policydm/eng/parser/XDMParserAnchor;)V

    goto :goto_0

    .line 1003
    :cond_3
    iget-object v1, p1, Lcom/policydm/eng/parser/XDMParserPcdata;->data:[C

    if-eqz v1, :cond_2

    .line 1005
    iget v1, p1, Lcom/policydm/eng/parser/XDMParserPcdata;->size:I

    new-array v1, v1, [C

    iput-object v1, p0, Lcom/policydm/eng/parser/XDMParserPcdata;->data:[C

    .line 1006
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    iget v1, p1, Lcom/policydm/eng/parser/XDMParserPcdata;->size:I

    if-ge v0, v1, :cond_4

    .line 1007
    iget-object v1, p0, Lcom/policydm/eng/parser/XDMParserPcdata;->data:[C

    iget-object v2, p1, Lcom/policydm/eng/parser/XDMParserPcdata;->data:[C

    aget-char v2, v2, v0

    aput-char v2, v1, v0

    .line 1006
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1008
    :cond_4
    iget v1, p1, Lcom/policydm/eng/parser/XDMParserPcdata;->size:I

    iput v1, p0, Lcom/policydm/eng/parser/XDMParserPcdata;->size:I

    goto :goto_1
.end method

.method public static xdmAgentDataStDuplResults(Lcom/policydm/eng/parser/XDMParserResults;Lcom/policydm/eng/parser/XDMParserResults;)V
    .locals 2
    .param p0, "dest"    # Lcom/policydm/eng/parser/XDMParserResults;
    .param p1, "src"    # Lcom/policydm/eng/parser/XDMParserResults;

    .prologue
    .line 1561
    if-nez p1, :cond_1

    .line 1595
    :cond_0
    :goto_0
    return-void

    .line 1566
    :cond_1
    iget v0, p1, Lcom/policydm/eng/parser/XDMParserResults;->cmdid:I

    if-lez v0, :cond_2

    .line 1568
    iget v0, p1, Lcom/policydm/eng/parser/XDMParserResults;->cmdid:I

    iput v0, p0, Lcom/policydm/eng/parser/XDMParserResults;->cmdid:I

    .line 1570
    :cond_2
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserResults;->m_szMsgRef:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1572
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserResults;->m_szMsgRef:Ljava/lang/String;

    iput-object v0, p0, Lcom/policydm/eng/parser/XDMParserResults;->m_szMsgRef:Ljava/lang/String;

    .line 1574
    :cond_3
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserResults;->m_szCmdRef:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1576
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserResults;->m_szCmdRef:Ljava/lang/String;

    iput-object v0, p0, Lcom/policydm/eng/parser/XDMParserResults;->m_szCmdRef:Ljava/lang/String;

    .line 1578
    :cond_4
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserResults;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    if-eqz v0, :cond_5

    .line 1580
    new-instance v0, Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-direct {v0}, Lcom/policydm/eng/parser/XDMParserMeta;-><init>()V

    iput-object v0, p0, Lcom/policydm/eng/parser/XDMParserResults;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    .line 1581
    iget-object v0, p0, Lcom/policydm/eng/parser/XDMParserResults;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v1, p1, Lcom/policydm/eng/parser/XDMParserResults;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-static {v0, v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplMeta(Lcom/policydm/eng/parser/XDMParserMeta;Lcom/policydm/eng/parser/XDMParserMeta;)V

    .line 1583
    :cond_5
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserResults;->m_szTargetRef:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 1585
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserResults;->m_szTargetRef:Ljava/lang/String;

    iput-object v0, p0, Lcom/policydm/eng/parser/XDMParserResults;->m_szTargetRef:Ljava/lang/String;

    .line 1587
    :cond_6
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserResults;->m_szSourceRef:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 1589
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserResults;->m_szSourceRef:Ljava/lang/String;

    iput-object v0, p0, Lcom/policydm/eng/parser/XDMParserResults;->m_szSourceRef:Ljava/lang/String;

    .line 1591
    :cond_7
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserResults;->itemlist:Lcom/policydm/eng/core/XDMList;

    if-eqz v0, :cond_0

    .line 1593
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserResults;->itemlist:Lcom/policydm/eng/core/XDMList;

    invoke-static {v0}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplItemlist(Lcom/policydm/eng/core/XDMList;)Lcom/policydm/eng/core/XDMList;

    move-result-object v0

    iput-object v0, p0, Lcom/policydm/eng/parser/XDMParserResults;->itemlist:Lcom/policydm/eng/core/XDMList;

    goto :goto_0
.end method

.method public static xdmAgentDataStGetString(Lcom/policydm/eng/parser/XDMParserPcdata;)Ljava/lang/String;
    .locals 3
    .param p0, "pcdata"    # Lcom/policydm/eng/parser/XDMParserPcdata;

    .prologue
    const/4 v1, 0x0

    .line 1535
    const-string v0, ""

    .line 1537
    .local v0, "szData":Ljava/lang/String;
    if-nez p0, :cond_1

    .line 1551
    :cond_0
    :goto_0
    return-object v1

    .line 1541
    :cond_1
    iget v2, p0, Lcom/policydm/eng/parser/XDMParserPcdata;->type:I

    if-nez v2, :cond_0

    .line 1546
    iget-object v2, p0, Lcom/policydm/eng/parser/XDMParserPcdata;->data:[C

    if-eqz v2, :cond_0

    .line 1549
    iget-object v1, p0, Lcom/policydm/eng/parser/XDMParserPcdata;->data:[C

    invoke-static {v1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 1551
    goto :goto_0
.end method

.method public static xdmAgentDataStString2Pcdata([C)Lcom/policydm/eng/parser/XDMParserPcdata;
    .locals 4
    .param p0, "str"    # [C

    .prologue
    .line 1488
    new-instance v1, Lcom/policydm/eng/parser/XDMParserPcdata;

    invoke-direct {v1}, Lcom/policydm/eng/parser/XDMParserPcdata;-><init>()V

    .line 1490
    .local v1, "o":Lcom/policydm/eng/parser/XDMParserPcdata;
    const/4 v2, 0x0

    iput v2, v1, Lcom/policydm/eng/parser/XDMParserPcdata;->type:I

    .line 1491
    array-length v2, p0

    iput v2, v1, Lcom/policydm/eng/parser/XDMParserPcdata;->size:I

    .line 1492
    array-length v2, p0

    new-array v2, v2, [C

    iput-object v2, v1, Lcom/policydm/eng/parser/XDMParserPcdata;->data:[C

    .line 1493
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_0

    .line 1494
    iget-object v2, v1, Lcom/policydm/eng/parser/XDMParserPcdata;->data:[C

    aget-char v3, p0, v0

    aput-char v3, v2, v0

    .line 1493
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1495
    :cond_0
    return-object v1
.end method


# virtual methods
.method public xdmAgentDataStDeleteSequence(Ljava/lang/Object;)V
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 1148
    move-object v1, p1

    check-cast v1, Lcom/policydm/eng/parser/XDMParserSequence;

    .line 1149
    .local v1, "sequence":Lcom/policydm/eng/parser/XDMParserSequence;
    const/4 v0, 0x0

    .line 1151
    .local v0, "cmd":Lcom/policydm/agent/XDMAgent;
    if-nez v1, :cond_0

    .line 1183
    :goto_0
    return-void

    .line 1154
    :cond_0
    iput v4, v1, Lcom/policydm/eng/parser/XDMParserSequence;->cmdid:I

    .line 1155
    iget-object v2, v1, Lcom/policydm/eng/parser/XDMParserSequence;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    if-eqz v2, :cond_1

    .line 1157
    iget-object v2, v1, Lcom/policydm/eng/parser/XDMParserSequence;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iput-object v3, v2, Lcom/policydm/eng/parser/XDMParserMeta;->anchor:Lcom/policydm/eng/parser/XDMParserAnchor;

    .line 1158
    iget-object v2, v1, Lcom/policydm/eng/parser/XDMParserSequence;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iput-object v3, v2, Lcom/policydm/eng/parser/XDMParserMeta;->m_szEmi:Ljava/lang/String;

    .line 1159
    iget-object v2, v1, Lcom/policydm/eng/parser/XDMParserSequence;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iput-object v3, v2, Lcom/policydm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    .line 1160
    iget-object v2, v1, Lcom/policydm/eng/parser/XDMParserSequence;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iput-object v3, v2, Lcom/policydm/eng/parser/XDMParserMeta;->m_szMark:Ljava/lang/String;

    .line 1161
    iget-object v2, v1, Lcom/policydm/eng/parser/XDMParserSequence;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iput v4, v2, Lcom/policydm/eng/parser/XDMParserMeta;->maxmsgsize:I

    .line 1162
    iget-object v2, v1, Lcom/policydm/eng/parser/XDMParserSequence;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iput v4, v2, Lcom/policydm/eng/parser/XDMParserMeta;->maxobjsize:I

    .line 1163
    iget-object v2, v1, Lcom/policydm/eng/parser/XDMParserSequence;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iput-object v3, v2, Lcom/policydm/eng/parser/XDMParserMeta;->mem:Lcom/policydm/eng/parser/XDMParserMem;

    .line 1164
    iget-object v2, v1, Lcom/policydm/eng/parser/XDMParserSequence;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iput-object v3, v2, Lcom/policydm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    .line 1165
    iget-object v2, v1, Lcom/policydm/eng/parser/XDMParserSequence;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iput v4, v2, Lcom/policydm/eng/parser/XDMParserMeta;->size:I

    .line 1166
    iget-object v2, v1, Lcom/policydm/eng/parser/XDMParserSequence;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iput-object v3, v2, Lcom/policydm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    .line 1167
    iget-object v2, v1, Lcom/policydm/eng/parser/XDMParserSequence;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iput-object v3, v2, Lcom/policydm/eng/parser/XDMParserMeta;->m_szVersion:Ljava/lang/String;

    .line 1168
    iput-object v3, v1, Lcom/policydm/eng/parser/XDMParserSequence;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    .line 1171
    :cond_1
    iget-object v2, v1, Lcom/policydm/eng/parser/XDMParserSequence;->itemlist:Lcom/policydm/eng/core/XDMLinkedList;

    if-eqz v2, :cond_2

    .line 1173
    iget-object v2, v1, Lcom/policydm/eng/parser/XDMParserSequence;->itemlist:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v2, v4}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListSetCurrentObj(Lcom/policydm/eng/core/XDMLinkedList;I)V

    .line 1174
    iget-object v2, v1, Lcom/policydm/eng/parser/XDMParserSequence;->itemlist:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v2}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/policydm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "cmd":Lcom/policydm/agent/XDMAgent;
    check-cast v0, Lcom/policydm/agent/XDMAgent;

    .line 1176
    .restart local v0    # "cmd":Lcom/policydm/agent/XDMAgent;
    :goto_1
    if-eqz v0, :cond_2

    .line 1178
    iget-object v2, v1, Lcom/policydm/eng/parser/XDMParserSequence;->itemlist:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v2}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListRemoveObjAtFirst(Lcom/policydm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    .line 1179
    iget-object v2, v1, Lcom/policydm/eng/parser/XDMParserSequence;->itemlist:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v2}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/policydm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "cmd":Lcom/policydm/agent/XDMAgent;
    check-cast v0, Lcom/policydm/agent/XDMAgent;

    .restart local v0    # "cmd":Lcom/policydm/agent/XDMAgent;
    goto :goto_1

    .line 1182
    :cond_2
    const/4 v1, 0x0

    .line 1183
    goto :goto_0
.end method

.method public xdmAgentDataStDuplAdd(Lcom/policydm/eng/parser/XDMParserAdd;Lcom/policydm/eng/parser/XDMParserAdd;)V
    .locals 2
    .param p1, "dest"    # Lcom/policydm/eng/parser/XDMParserAdd;
    .param p2, "src"    # Lcom/policydm/eng/parser/XDMParserAdd;

    .prologue
    .line 700
    if-nez p2, :cond_1

    .line 723
    :cond_0
    :goto_0
    return-void

    .line 705
    :cond_1
    iget v0, p2, Lcom/policydm/eng/parser/XDMParserAdd;->cmdid:I

    if-lez v0, :cond_2

    .line 707
    iget v0, p2, Lcom/policydm/eng/parser/XDMParserAdd;->cmdid:I

    iput v0, p1, Lcom/policydm/eng/parser/XDMParserAdd;->cmdid:I

    .line 709
    :cond_2
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserAdd;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    if-eqz v0, :cond_3

    .line 711
    new-instance v0, Lcom/policydm/eng/parser/XDMParserCred;

    invoke-direct {v0}, Lcom/policydm/eng/parser/XDMParserCred;-><init>()V

    iput-object v0, p1, Lcom/policydm/eng/parser/XDMParserAdd;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    .line 712
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserAdd;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    iget-object v1, p2, Lcom/policydm/eng/parser/XDMParserAdd;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    invoke-static {v0, v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplCred(Lcom/policydm/eng/parser/XDMParserCred;Lcom/policydm/eng/parser/XDMParserCred;)V

    .line 714
    :cond_3
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserAdd;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    if-eqz v0, :cond_4

    .line 716
    new-instance v0, Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-direct {v0}, Lcom/policydm/eng/parser/XDMParserMeta;-><init>()V

    iput-object v0, p1, Lcom/policydm/eng/parser/XDMParserAdd;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    .line 717
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserAdd;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v1, p2, Lcom/policydm/eng/parser/XDMParserAdd;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-static {v0, v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplMeta(Lcom/policydm/eng/parser/XDMParserMeta;Lcom/policydm/eng/parser/XDMParserMeta;)V

    .line 719
    :cond_4
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserAdd;->itemlist:Lcom/policydm/eng/core/XDMList;

    if-eqz v0, :cond_0

    .line 721
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserAdd;->itemlist:Lcom/policydm/eng/core/XDMList;

    invoke-static {v0}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplItemlist(Lcom/policydm/eng/core/XDMList;)Lcom/policydm/eng/core/XDMList;

    move-result-object v0

    iput-object v0, p1, Lcom/policydm/eng/parser/XDMParserAdd;->itemlist:Lcom/policydm/eng/core/XDMList;

    goto :goto_0
.end method

.method public xdmAgentDataStDuplAtomic(Lcom/policydm/eng/parser/XDMParserAtomic;Lcom/policydm/eng/parser/XDMParserAtomic;)V
    .locals 3
    .param p1, "dest"    # Lcom/policydm/eng/parser/XDMParserAtomic;
    .param p2, "src"    # Lcom/policydm/eng/parser/XDMParserAtomic;

    .prologue
    .line 1072
    const/4 v0, 0x0

    .line 1073
    .local v0, "cmd":Lcom/policydm/agent/XDMAgent;
    if-nez p2, :cond_1

    .line 1101
    :cond_0
    :goto_0
    return-void

    .line 1078
    :cond_1
    iget v1, p2, Lcom/policydm/eng/parser/XDMParserAtomic;->cmdid:I

    if-lez v1, :cond_2

    .line 1080
    iget v1, p2, Lcom/policydm/eng/parser/XDMParserAtomic;->cmdid:I

    iput v1, p1, Lcom/policydm/eng/parser/XDMParserAtomic;->cmdid:I

    .line 1082
    :cond_2
    iget-object v1, p2, Lcom/policydm/eng/parser/XDMParserAtomic;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    if-eqz v1, :cond_3

    .line 1084
    new-instance v1, Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-direct {v1}, Lcom/policydm/eng/parser/XDMParserMeta;-><init>()V

    iput-object v1, p1, Lcom/policydm/eng/parser/XDMParserAtomic;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    .line 1085
    iget-object v1, p1, Lcom/policydm/eng/parser/XDMParserAtomic;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v2, p2, Lcom/policydm/eng/parser/XDMParserAtomic;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-static {v1, v2}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplMeta(Lcom/policydm/eng/parser/XDMParserMeta;Lcom/policydm/eng/parser/XDMParserMeta;)V

    .line 1087
    :cond_3
    iget-object v1, p2, Lcom/policydm/eng/parser/XDMParserAtomic;->itemlist:Lcom/policydm/eng/core/XDMLinkedList;

    if-eqz v1, :cond_4

    .line 1089
    iget-object v1, p2, Lcom/policydm/eng/parser/XDMParserAtomic;->itemlist:Lcom/policydm/eng/core/XDMLinkedList;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListSetCurrentObj(Lcom/policydm/eng/core/XDMLinkedList;I)V

    .line 1090
    iget-object v1, p2, Lcom/policydm/eng/parser/XDMParserAtomic;->itemlist:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v1}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/policydm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "cmd":Lcom/policydm/agent/XDMAgent;
    check-cast v0, Lcom/policydm/agent/XDMAgent;

    .line 1091
    .restart local v0    # "cmd":Lcom/policydm/agent/XDMAgent;
    :goto_1
    if-eqz v0, :cond_0

    .line 1093
    iget-object v1, p1, Lcom/policydm/eng/parser/XDMParserAtomic;->itemlist:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v1, v0}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 1094
    iget-object v1, p2, Lcom/policydm/eng/parser/XDMParserAtomic;->itemlist:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v1}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/policydm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "cmd":Lcom/policydm/agent/XDMAgent;
    check-cast v0, Lcom/policydm/agent/XDMAgent;

    .restart local v0    # "cmd":Lcom/policydm/agent/XDMAgent;
    goto :goto_1

    .line 1099
    :cond_4
    invoke-static {}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListCreateLinkedList()Lcom/policydm/eng/core/XDMLinkedList;

    move-result-object v1

    iput-object v1, p1, Lcom/policydm/eng/parser/XDMParserAtomic;->itemlist:Lcom/policydm/eng/core/XDMLinkedList;

    goto :goto_0
.end method

.method public xdmAgentDataStDuplCopy(Lcom/policydm/eng/parser/XDMParserCopy;Lcom/policydm/eng/parser/XDMParserCopy;)V
    .locals 2
    .param p1, "dest"    # Lcom/policydm/eng/parser/XDMParserCopy;
    .param p2, "src"    # Lcom/policydm/eng/parser/XDMParserCopy;

    .prologue
    .line 764
    if-nez p2, :cond_1

    .line 787
    :cond_0
    :goto_0
    return-void

    .line 769
    :cond_1
    iget v0, p2, Lcom/policydm/eng/parser/XDMParserCopy;->cmdid:I

    if-lez v0, :cond_2

    .line 771
    iget v0, p2, Lcom/policydm/eng/parser/XDMParserCopy;->cmdid:I

    iput v0, p1, Lcom/policydm/eng/parser/XDMParserCopy;->cmdid:I

    .line 773
    :cond_2
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserCopy;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    if-eqz v0, :cond_3

    .line 775
    new-instance v0, Lcom/policydm/eng/parser/XDMParserCred;

    invoke-direct {v0}, Lcom/policydm/eng/parser/XDMParserCred;-><init>()V

    iput-object v0, p1, Lcom/policydm/eng/parser/XDMParserCopy;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    .line 776
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserCopy;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    iget-object v1, p2, Lcom/policydm/eng/parser/XDMParserCopy;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    invoke-static {v0, v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplCred(Lcom/policydm/eng/parser/XDMParserCred;Lcom/policydm/eng/parser/XDMParserCred;)V

    .line 778
    :cond_3
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserCopy;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    if-eqz v0, :cond_4

    .line 780
    new-instance v0, Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-direct {v0}, Lcom/policydm/eng/parser/XDMParserMeta;-><init>()V

    iput-object v0, p1, Lcom/policydm/eng/parser/XDMParserCopy;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    .line 781
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserCopy;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v1, p2, Lcom/policydm/eng/parser/XDMParserCopy;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-static {v0, v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplMeta(Lcom/policydm/eng/parser/XDMParserMeta;Lcom/policydm/eng/parser/XDMParserMeta;)V

    .line 783
    :cond_4
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserCopy;->itemlist:Lcom/policydm/eng/core/XDMList;

    if-eqz v0, :cond_0

    .line 785
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserCopy;->itemlist:Lcom/policydm/eng/core/XDMList;

    invoke-static {v0}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplItemlist(Lcom/policydm/eng/core/XDMList;)Lcom/policydm/eng/core/XDMList;

    move-result-object v0

    iput-object v0, p1, Lcom/policydm/eng/parser/XDMParserCopy;->itemlist:Lcom/policydm/eng/core/XDMList;

    goto :goto_0
.end method

.method public xdmAgentDataStDuplDelete(Lcom/policydm/eng/parser/XDMParserDelete;Lcom/policydm/eng/parser/XDMParserDelete;)V
    .locals 2
    .param p1, "dest"    # Lcom/policydm/eng/parser/XDMParserDelete;
    .param p2, "src"    # Lcom/policydm/eng/parser/XDMParserDelete;

    .prologue
    .line 796
    if-nez p2, :cond_1

    .line 819
    :cond_0
    :goto_0
    return-void

    .line 801
    :cond_1
    iget v0, p2, Lcom/policydm/eng/parser/XDMParserDelete;->cmdid:I

    if-lez v0, :cond_2

    .line 803
    iget v0, p2, Lcom/policydm/eng/parser/XDMParserDelete;->cmdid:I

    iput v0, p1, Lcom/policydm/eng/parser/XDMParserDelete;->cmdid:I

    .line 805
    :cond_2
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserDelete;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    if-eqz v0, :cond_3

    .line 807
    new-instance v0, Lcom/policydm/eng/parser/XDMParserCred;

    invoke-direct {v0}, Lcom/policydm/eng/parser/XDMParserCred;-><init>()V

    iput-object v0, p1, Lcom/policydm/eng/parser/XDMParserDelete;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    .line 808
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserDelete;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    iget-object v1, p2, Lcom/policydm/eng/parser/XDMParserDelete;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    invoke-static {v0, v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplCred(Lcom/policydm/eng/parser/XDMParserCred;Lcom/policydm/eng/parser/XDMParserCred;)V

    .line 810
    :cond_3
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserDelete;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    if-eqz v0, :cond_4

    .line 812
    new-instance v0, Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-direct {v0}, Lcom/policydm/eng/parser/XDMParserMeta;-><init>()V

    iput-object v0, p1, Lcom/policydm/eng/parser/XDMParserDelete;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    .line 813
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserDelete;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v1, p2, Lcom/policydm/eng/parser/XDMParserDelete;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-static {v0, v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplMeta(Lcom/policydm/eng/parser/XDMParserMeta;Lcom/policydm/eng/parser/XDMParserMeta;)V

    .line 815
    :cond_4
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserDelete;->itemlist:Lcom/policydm/eng/core/XDMList;

    if-eqz v0, :cond_0

    .line 817
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserDelete;->itemlist:Lcom/policydm/eng/core/XDMList;

    invoke-static {v0}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplItemlist(Lcom/policydm/eng/core/XDMList;)Lcom/policydm/eng/core/XDMList;

    move-result-object v0

    iput-object v0, p1, Lcom/policydm/eng/parser/XDMParserDelete;->itemlist:Lcom/policydm/eng/core/XDMList;

    goto :goto_0
.end method

.method public xdmAgentDataStDuplElelist(Lcom/policydm/eng/core/XDMList;)Lcom/policydm/eng/core/XDMList;
    .locals 2
    .param p1, "src"    # Lcom/policydm/eng/core/XDMList;

    .prologue
    .line 828
    move-object v0, p1

    .line 829
    .local v0, "curr":Lcom/policydm/eng/core/XDMList;
    const/4 v1, 0x0

    .line 831
    .local v1, "head":Lcom/policydm/eng/core/XDMList;
    :goto_0
    if-eqz v0, :cond_0

    .line 833
    iget-object v0, v0, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    goto :goto_0

    .line 836
    :cond_0
    return-object v1
.end method

.method public xdmAgentDataStDuplExec(Lcom/policydm/eng/parser/XDMParserExec;Lcom/policydm/eng/parser/XDMParserExec;)V
    .locals 2
    .param p1, "dest"    # Lcom/policydm/eng/parser/XDMParserExec;
    .param p2, "src"    # Lcom/policydm/eng/parser/XDMParserExec;

    .prologue
    .line 635
    if-nez p2, :cond_1

    .line 656
    :cond_0
    :goto_0
    return-void

    .line 639
    :cond_1
    iget v0, p2, Lcom/policydm/eng/parser/XDMParserExec;->cmdid:I

    if-lez v0, :cond_2

    .line 641
    iget v0, p2, Lcom/policydm/eng/parser/XDMParserExec;->cmdid:I

    iput v0, p1, Lcom/policydm/eng/parser/XDMParserExec;->cmdid:I

    .line 643
    :cond_2
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserExec;->m_szCorrelator:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 645
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserExec;->m_szCorrelator:Ljava/lang/String;

    iput-object v0, p1, Lcom/policydm/eng/parser/XDMParserExec;->m_szCorrelator:Ljava/lang/String;

    .line 647
    :cond_3
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserExec;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    if-eqz v0, :cond_4

    .line 649
    new-instance v0, Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-direct {v0}, Lcom/policydm/eng/parser/XDMParserMeta;-><init>()V

    iput-object v0, p1, Lcom/policydm/eng/parser/XDMParserExec;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    .line 650
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserExec;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v1, p2, Lcom/policydm/eng/parser/XDMParserExec;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-static {v0, v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplMeta(Lcom/policydm/eng/parser/XDMParserMeta;Lcom/policydm/eng/parser/XDMParserMeta;)V

    .line 652
    :cond_4
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserExec;->itemlist:Lcom/policydm/eng/core/XDMList;

    if-eqz v0, :cond_0

    .line 654
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserExec;->itemlist:Lcom/policydm/eng/core/XDMList;

    invoke-static {v0}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplItemlist(Lcom/policydm/eng/core/XDMList;)Lcom/policydm/eng/core/XDMList;

    move-result-object v0

    iput-object v0, p1, Lcom/policydm/eng/parser/XDMParserExec;->itemlist:Lcom/policydm/eng/core/XDMList;

    goto :goto_0
.end method

.method public xdmAgentDataStDuplGet(Lcom/policydm/eng/parser/XDMParserGet;Lcom/policydm/eng/parser/XDMParserGet;)V
    .locals 2
    .param p1, "dest"    # Lcom/policydm/eng/parser/XDMParserGet;
    .param p2, "src"    # Lcom/policydm/eng/parser/XDMParserGet;

    .prologue
    .line 599
    if-nez p2, :cond_1

    .line 626
    :cond_0
    :goto_0
    return-void

    .line 604
    :cond_1
    iget v0, p2, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    if-lez v0, :cond_2

    .line 606
    iget v0, p2, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    iput v0, p1, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    .line 608
    :cond_2
    iget v0, p2, Lcom/policydm/eng/parser/XDMParserGet;->lang:I

    if-lez v0, :cond_3

    .line 610
    iget v0, p2, Lcom/policydm/eng/parser/XDMParserGet;->lang:I

    iput v0, p1, Lcom/policydm/eng/parser/XDMParserGet;->lang:I

    .line 612
    :cond_3
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserGet;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    if-eqz v0, :cond_4

    .line 614
    new-instance v0, Lcom/policydm/eng/parser/XDMParserCred;

    invoke-direct {v0}, Lcom/policydm/eng/parser/XDMParserCred;-><init>()V

    iput-object v0, p1, Lcom/policydm/eng/parser/XDMParserGet;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    .line 615
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserGet;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    iget-object v1, p2, Lcom/policydm/eng/parser/XDMParserGet;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    invoke-static {v0, v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplCred(Lcom/policydm/eng/parser/XDMParserCred;Lcom/policydm/eng/parser/XDMParserCred;)V

    .line 617
    :cond_4
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserGet;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    if-eqz v0, :cond_5

    .line 619
    new-instance v0, Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-direct {v0}, Lcom/policydm/eng/parser/XDMParserMeta;-><init>()V

    iput-object v0, p1, Lcom/policydm/eng/parser/XDMParserGet;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    .line 620
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserGet;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v1, p2, Lcom/policydm/eng/parser/XDMParserGet;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-static {v0, v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplMeta(Lcom/policydm/eng/parser/XDMParserMeta;Lcom/policydm/eng/parser/XDMParserMeta;)V

    .line 622
    :cond_5
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserGet;->itemlist:Lcom/policydm/eng/core/XDMList;

    if-eqz v0, :cond_0

    .line 624
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserGet;->itemlist:Lcom/policydm/eng/core/XDMList;

    invoke-static {v0}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplItemlist(Lcom/policydm/eng/core/XDMList;)Lcom/policydm/eng/core/XDMList;

    move-result-object v0

    iput-object v0, p1, Lcom/policydm/eng/parser/XDMParserGet;->itemlist:Lcom/policydm/eng/core/XDMList;

    goto :goto_0
.end method

.method public xdmAgentDataStDuplReplace(Lcom/policydm/eng/parser/XDMParserReplace;Lcom/policydm/eng/parser/XDMParserReplace;)V
    .locals 2
    .param p1, "dest"    # Lcom/policydm/eng/parser/XDMParserReplace;
    .param p2, "src"    # Lcom/policydm/eng/parser/XDMParserReplace;

    .prologue
    .line 732
    if-nez p2, :cond_1

    .line 755
    :cond_0
    :goto_0
    return-void

    .line 737
    :cond_1
    iget v0, p2, Lcom/policydm/eng/parser/XDMParserReplace;->cmdid:I

    if-lez v0, :cond_2

    .line 739
    iget v0, p2, Lcom/policydm/eng/parser/XDMParserReplace;->cmdid:I

    iput v0, p1, Lcom/policydm/eng/parser/XDMParserReplace;->cmdid:I

    .line 741
    :cond_2
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserReplace;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    if-eqz v0, :cond_3

    .line 743
    new-instance v0, Lcom/policydm/eng/parser/XDMParserCred;

    invoke-direct {v0}, Lcom/policydm/eng/parser/XDMParserCred;-><init>()V

    iput-object v0, p1, Lcom/policydm/eng/parser/XDMParserReplace;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    .line 744
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserReplace;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    iget-object v1, p2, Lcom/policydm/eng/parser/XDMParserReplace;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    invoke-static {v0, v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplCred(Lcom/policydm/eng/parser/XDMParserCred;Lcom/policydm/eng/parser/XDMParserCred;)V

    .line 746
    :cond_3
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserReplace;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    if-eqz v0, :cond_4

    .line 748
    new-instance v0, Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-direct {v0}, Lcom/policydm/eng/parser/XDMParserMeta;-><init>()V

    iput-object v0, p1, Lcom/policydm/eng/parser/XDMParserReplace;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    .line 749
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserReplace;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v1, p2, Lcom/policydm/eng/parser/XDMParserReplace;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-static {v0, v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplMeta(Lcom/policydm/eng/parser/XDMParserMeta;Lcom/policydm/eng/parser/XDMParserMeta;)V

    .line 751
    :cond_4
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserReplace;->itemlist:Lcom/policydm/eng/core/XDMList;

    if-eqz v0, :cond_0

    .line 753
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserReplace;->itemlist:Lcom/policydm/eng/core/XDMList;

    invoke-static {v0}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplItemlist(Lcom/policydm/eng/core/XDMList;)Lcom/policydm/eng/core/XDMList;

    move-result-object v0

    iput-object v0, p1, Lcom/policydm/eng/parser/XDMParserReplace;->itemlist:Lcom/policydm/eng/core/XDMList;

    goto :goto_0
.end method

.method public xdmAgentDataStDuplSequence(Lcom/policydm/eng/parser/XDMParserSequence;Lcom/policydm/eng/parser/XDMParserSequence;)V
    .locals 3
    .param p1, "dest"    # Lcom/policydm/eng/parser/XDMParserSequence;
    .param p2, "src"    # Lcom/policydm/eng/parser/XDMParserSequence;

    .prologue
    .line 1110
    const/4 v0, 0x0

    .line 1112
    .local v0, "cmd":Lcom/policydm/agent/XDMAgent;
    if-nez p2, :cond_1

    .line 1140
    :cond_0
    :goto_0
    return-void

    .line 1117
    :cond_1
    iget v1, p2, Lcom/policydm/eng/parser/XDMParserSequence;->cmdid:I

    if-lez v1, :cond_2

    .line 1119
    iget v1, p2, Lcom/policydm/eng/parser/XDMParserSequence;->cmdid:I

    iput v1, p1, Lcom/policydm/eng/parser/XDMParserSequence;->cmdid:I

    .line 1121
    :cond_2
    iget-object v1, p2, Lcom/policydm/eng/parser/XDMParserSequence;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    if-eqz v1, :cond_3

    .line 1123
    new-instance v1, Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-direct {v1}, Lcom/policydm/eng/parser/XDMParserMeta;-><init>()V

    iput-object v1, p1, Lcom/policydm/eng/parser/XDMParserSequence;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    .line 1124
    iget-object v1, p1, Lcom/policydm/eng/parser/XDMParserSequence;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v2, p2, Lcom/policydm/eng/parser/XDMParserSequence;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-static {v1, v2}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplMeta(Lcom/policydm/eng/parser/XDMParserMeta;Lcom/policydm/eng/parser/XDMParserMeta;)V

    .line 1126
    :cond_3
    iget-object v1, p2, Lcom/policydm/eng/parser/XDMParserSequence;->itemlist:Lcom/policydm/eng/core/XDMLinkedList;

    if-eqz v1, :cond_4

    .line 1128
    iget-object v1, p2, Lcom/policydm/eng/parser/XDMParserSequence;->itemlist:Lcom/policydm/eng/core/XDMLinkedList;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListSetCurrentObj(Lcom/policydm/eng/core/XDMLinkedList;I)V

    .line 1129
    iget-object v1, p2, Lcom/policydm/eng/parser/XDMParserSequence;->itemlist:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v1}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/policydm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "cmd":Lcom/policydm/agent/XDMAgent;
    check-cast v0, Lcom/policydm/agent/XDMAgent;

    .line 1130
    .restart local v0    # "cmd":Lcom/policydm/agent/XDMAgent;
    :goto_1
    if-eqz v0, :cond_0

    .line 1132
    iget-object v1, p1, Lcom/policydm/eng/parser/XDMParserSequence;->itemlist:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v1, v0}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 1133
    iget-object v1, p2, Lcom/policydm/eng/parser/XDMParserSequence;->itemlist:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v1}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/policydm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "cmd":Lcom/policydm/agent/XDMAgent;
    check-cast v0, Lcom/policydm/agent/XDMAgent;

    .restart local v0    # "cmd":Lcom/policydm/agent/XDMAgent;
    goto :goto_1

    .line 1138
    :cond_4
    invoke-static {}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListCreateLinkedList()Lcom/policydm/eng/core/XDMLinkedList;

    move-result-object v1

    iput-object v1, p1, Lcom/policydm/eng/parser/XDMParserSequence;->itemlist:Lcom/policydm/eng/core/XDMLinkedList;

    goto :goto_0
.end method

.method public xdmAgentDataStDuplStatus(Lcom/policydm/eng/parser/XDMParserStatus;Lcom/policydm/eng/parser/XDMParserStatus;)V
    .locals 2
    .param p1, "dest"    # Lcom/policydm/eng/parser/XDMParserStatus;
    .param p2, "src"    # Lcom/policydm/eng/parser/XDMParserStatus;

    .prologue
    .line 544
    if-nez p2, :cond_1

    .line 590
    :cond_0
    :goto_0
    return-void

    .line 548
    :cond_1
    iget v0, p2, Lcom/policydm/eng/parser/XDMParserStatus;->cmdid:I

    if-lez v0, :cond_2

    .line 550
    iget v0, p2, Lcom/policydm/eng/parser/XDMParserStatus;->cmdid:I

    iput v0, p1, Lcom/policydm/eng/parser/XDMParserStatus;->cmdid:I

    .line 552
    :cond_2
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserStatus;->m_szMsgRef:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 554
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserStatus;->m_szMsgRef:Ljava/lang/String;

    iput-object v0, p1, Lcom/policydm/eng/parser/XDMParserStatus;->m_szMsgRef:Ljava/lang/String;

    .line 556
    :cond_3
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserStatus;->m_szCmdRef:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 558
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserStatus;->m_szCmdRef:Ljava/lang/String;

    iput-object v0, p1, Lcom/policydm/eng/parser/XDMParserStatus;->m_szCmdRef:Ljava/lang/String;

    .line 560
    :cond_4
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserStatus;->m_szCmd:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 562
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserStatus;->m_szCmd:Ljava/lang/String;

    iput-object v0, p1, Lcom/policydm/eng/parser/XDMParserStatus;->m_szCmd:Ljava/lang/String;

    .line 564
    :cond_5
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserStatus;->targetref:Lcom/policydm/eng/core/XDMList;

    if-eqz v0, :cond_6

    .line 566
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserStatus;->targetref:Lcom/policydm/eng/core/XDMList;

    iput-object v0, p1, Lcom/policydm/eng/parser/XDMParserStatus;->targetref:Lcom/policydm/eng/core/XDMList;

    .line 568
    :cond_6
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserStatus;->sourceref:Lcom/policydm/eng/core/XDMList;

    if-eqz v0, :cond_7

    .line 570
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserStatus;->sourceref:Lcom/policydm/eng/core/XDMList;

    iput-object v0, p1, Lcom/policydm/eng/parser/XDMParserStatus;->sourceref:Lcom/policydm/eng/core/XDMList;

    .line 572
    :cond_7
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserStatus;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    if-eqz v0, :cond_8

    .line 574
    new-instance v0, Lcom/policydm/eng/parser/XDMParserCred;

    invoke-direct {v0}, Lcom/policydm/eng/parser/XDMParserCred;-><init>()V

    iput-object v0, p1, Lcom/policydm/eng/parser/XDMParserStatus;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    .line 575
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserStatus;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    iget-object v1, p2, Lcom/policydm/eng/parser/XDMParserStatus;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    invoke-static {v0, v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplCred(Lcom/policydm/eng/parser/XDMParserCred;Lcom/policydm/eng/parser/XDMParserCred;)V

    .line 577
    :cond_8
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserStatus;->chal:Lcom/policydm/eng/parser/XDMParserMeta;

    if-eqz v0, :cond_9

    .line 579
    new-instance v0, Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-direct {v0}, Lcom/policydm/eng/parser/XDMParserMeta;-><init>()V

    iput-object v0, p1, Lcom/policydm/eng/parser/XDMParserStatus;->chal:Lcom/policydm/eng/parser/XDMParserMeta;

    .line 580
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserStatus;->chal:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v1, p2, Lcom/policydm/eng/parser/XDMParserStatus;->chal:Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-static {v0, v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplMeta(Lcom/policydm/eng/parser/XDMParserMeta;Lcom/policydm/eng/parser/XDMParserMeta;)V

    .line 582
    :cond_9
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 584
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    iput-object v0, p1, Lcom/policydm/eng/parser/XDMParserStatus;->m_szData:Ljava/lang/String;

    .line 586
    :cond_a
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserStatus;->itemlist:Lcom/policydm/eng/core/XDMList;

    if-eqz v0, :cond_0

    .line 588
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserStatus;->itemlist:Lcom/policydm/eng/core/XDMList;

    invoke-static {v0}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplItemlist(Lcom/policydm/eng/core/XDMList;)Lcom/policydm/eng/core/XDMList;

    move-result-object v0

    iput-object v0, p1, Lcom/policydm/eng/parser/XDMParserStatus;->itemlist:Lcom/policydm/eng/core/XDMList;

    goto :goto_0
.end method

.method public xdmAgentDataStDuplSyncHeader(Lcom/policydm/eng/parser/XDMParserSyncheader;Lcom/policydm/eng/parser/XDMParserSyncheader;)V
    .locals 2
    .param p1, "dest"    # Lcom/policydm/eng/parser/XDMParserSyncheader;
    .param p2, "src"    # Lcom/policydm/eng/parser/XDMParserSyncheader;

    .prologue
    .line 493
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserSyncheader;->m_szVerdtd:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 495
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserSyncheader;->m_szVerdtd:Ljava/lang/String;

    iput-object v0, p1, Lcom/policydm/eng/parser/XDMParserSyncheader;->m_szVerdtd:Ljava/lang/String;

    .line 497
    :cond_0
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserSyncheader;->m_szVerproto:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 499
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserSyncheader;->m_szVerproto:Ljava/lang/String;

    iput-object v0, p1, Lcom/policydm/eng/parser/XDMParserSyncheader;->m_szVerproto:Ljava/lang/String;

    .line 501
    :cond_1
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserSyncheader;->m_szSessionId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 503
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserSyncheader;->m_szSessionId:Ljava/lang/String;

    iput-object v0, p1, Lcom/policydm/eng/parser/XDMParserSyncheader;->m_szSessionId:Ljava/lang/String;

    .line 505
    :cond_2
    iget v0, p2, Lcom/policydm/eng/parser/XDMParserSyncheader;->msgid:I

    if-lez v0, :cond_3

    .line 507
    iget v0, p2, Lcom/policydm/eng/parser/XDMParserSyncheader;->msgid:I

    iput v0, p1, Lcom/policydm/eng/parser/XDMParserSyncheader;->msgid:I

    .line 509
    :cond_3
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserSyncheader;->m_szTarget:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 511
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserSyncheader;->m_szTarget:Ljava/lang/String;

    iput-object v0, p1, Lcom/policydm/eng/parser/XDMParserSyncheader;->m_szTarget:Ljava/lang/String;

    .line 513
    :cond_4
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserSyncheader;->m_szSource:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 515
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserSyncheader;->m_szSource:Ljava/lang/String;

    iput-object v0, p1, Lcom/policydm/eng/parser/XDMParserSyncheader;->m_szSource:Ljava/lang/String;

    .line 517
    :cond_5
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserSyncheader;->m_szLocname:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 519
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserSyncheader;->m_szLocname:Ljava/lang/String;

    iput-object v0, p1, Lcom/policydm/eng/parser/XDMParserSyncheader;->m_szLocname:Ljava/lang/String;

    .line 521
    :cond_6
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserSyncheader;->m_szRespUri:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 523
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserSyncheader;->m_szRespUri:Ljava/lang/String;

    iput-object v0, p1, Lcom/policydm/eng/parser/XDMParserSyncheader;->m_szRespUri:Ljava/lang/String;

    .line 525
    :cond_7
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserSyncheader;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    if-eqz v0, :cond_8

    .line 527
    new-instance v0, Lcom/policydm/eng/parser/XDMParserCred;

    invoke-direct {v0}, Lcom/policydm/eng/parser/XDMParserCred;-><init>()V

    iput-object v0, p1, Lcom/policydm/eng/parser/XDMParserSyncheader;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    .line 528
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserSyncheader;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    iget-object v1, p2, Lcom/policydm/eng/parser/XDMParserSyncheader;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    invoke-static {v0, v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplCred(Lcom/policydm/eng/parser/XDMParserCred;Lcom/policydm/eng/parser/XDMParserCred;)V

    .line 530
    :cond_8
    iget-object v0, p2, Lcom/policydm/eng/parser/XDMParserSyncheader;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    if-eqz v0, :cond_9

    .line 532
    new-instance v0, Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-direct {v0}, Lcom/policydm/eng/parser/XDMParserMeta;-><init>()V

    iput-object v0, p1, Lcom/policydm/eng/parser/XDMParserSyncheader;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    .line 533
    iget-object v0, p1, Lcom/policydm/eng/parser/XDMParserSyncheader;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iget-object v1, p2, Lcom/policydm/eng/parser/XDMParserSyncheader;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-static {v0, v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplMeta(Lcom/policydm/eng/parser/XDMParserMeta;Lcom/policydm/eng/parser/XDMParserMeta;)V

    .line 535
    :cond_9
    return-void
.end method

.method public xdmAgentHdlCmdAdd(Ljava/lang/Object;Lcom/policydm/eng/parser/XDMParserAdd;)V
    .locals 3
    .param p1, "userdata"    # Ljava/lang/Object;
    .param p2, "addCmd"    # Lcom/policydm/eng/parser/XDMParserAdd;

    .prologue
    .line 167
    move-object v1, p1

    check-cast v1, Lcom/policydm/eng/core/XDMWorkspace;

    .line 170
    .local v1, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    new-instance v0, Lcom/policydm/agent/XDMAgent;

    invoke-direct {v0}, Lcom/policydm/agent/XDMAgent;-><init>()V

    .line 171
    .local v0, "agent":Lcom/policydm/agent/XDMAgent;
    new-instance v2, Lcom/policydm/eng/parser/XDMParserAdd;

    invoke-direct {v2}, Lcom/policydm/eng/parser/XDMParserAdd;-><init>()V

    iput-object v2, v0, Lcom/policydm/agent/XDMAgent;->m_AddCmd:Lcom/policydm/eng/parser/XDMParserAdd;

    .line 172
    iget-object v2, v0, Lcom/policydm/agent/XDMAgent;->m_AddCmd:Lcom/policydm/eng/parser/XDMParserAdd;

    invoke-virtual {p0, v2, p2}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplAdd(Lcom/policydm/eng/parser/XDMParserAdd;Lcom/policydm/eng/parser/XDMParserAdd;)V

    .line 174
    const-string v2, "Add"

    iput-object v2, v0, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    .line 175
    iget-boolean v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->inAtomicCmd:Z

    if-nez v2, :cond_0

    iget-boolean v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->inSequenceCmd:Z

    if-eqz v2, :cond_1

    .line 177
    :cond_0
    iget-object v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->list:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v0, v2}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentHdlCmdAddSelectedAgent(Lcom/policydm/agent/XDMAgent;Lcom/policydm/eng/core/XDMLinkedList;)V

    .line 183
    :goto_0
    return-void

    .line 181
    :cond_1
    iget-object v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->list:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v2, v0}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public xdmAgentHdlCmdAddSelectedAgent(Lcom/policydm/agent/XDMAgent;Lcom/policydm/eng/core/XDMLinkedList;)V
    .locals 3
    .param p1, "agent"    # Lcom/policydm/agent/XDMAgent;
    .param p2, "curlocate"    # Lcom/policydm/eng/core/XDMLinkedList;

    .prologue
    .line 344
    const/4 v0, 0x0

    .line 346
    .local v0, "cmdagent":Lcom/policydm/agent/XDMAgent;
    iget v1, p2, Lcom/policydm/eng/core/XDMLinkedList;->count:I

    add-int/lit8 v1, v1, -0x1

    invoke-static {p2, v1}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListGetObj(Lcom/policydm/eng/core/XDMLinkedList;I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "cmdagent":Lcom/policydm/agent/XDMAgent;
    check-cast v0, Lcom/policydm/agent/XDMAgent;

    .line 347
    .restart local v0    # "cmdagent":Lcom/policydm/agent/XDMAgent;
    if-eqz v0, :cond_a

    .line 349
    iget-boolean v1, v0, Lcom/policydm/agent/XDMAgent;->m_bInProgresscmd:Z

    if-eqz v1, :cond_4

    .line 351
    iget-object v1, v0, Lcom/policydm/agent/XDMAgent;->m_Atomic:Lcom/policydm/eng/parser/XDMParserAtomic;

    if-eqz v1, :cond_1

    .line 353
    iget-object v1, v0, Lcom/policydm/agent/XDMAgent;->m_Atomic:Lcom/policydm/eng/parser/XDMParserAtomic;

    iget-object v1, v1, Lcom/policydm/eng/parser/XDMParserAtomic;->itemlist:Lcom/policydm/eng/core/XDMLinkedList;

    if-eqz v1, :cond_0

    .line 355
    iget-object v1, v0, Lcom/policydm/agent/XDMAgent;->m_Atomic:Lcom/policydm/eng/parser/XDMParserAtomic;

    iget-object v1, v1, Lcom/policydm/eng/parser/XDMParserAtomic;->itemlist:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-virtual {p0, p1, v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentHdlCmdAddSelectedAgent(Lcom/policydm/agent/XDMAgent;Lcom/policydm/eng/core/XDMLinkedList;)V

    .line 412
    :goto_0
    return-void

    .line 359
    :cond_0
    iget-object v1, v0, Lcom/policydm/agent/XDMAgent;->m_Atomic:Lcom/policydm/eng/parser/XDMParserAtomic;

    invoke-static {}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListCreateLinkedList()Lcom/policydm/eng/core/XDMLinkedList;

    move-result-object v2

    iput-object v2, v1, Lcom/policydm/eng/parser/XDMParserAtomic;->itemlist:Lcom/policydm/eng/core/XDMLinkedList;

    .line 360
    iget-object v1, v0, Lcom/policydm/agent/XDMAgent;->m_Atomic:Lcom/policydm/eng/parser/XDMParserAtomic;

    iget-object v1, v1, Lcom/policydm/eng/parser/XDMParserAtomic;->itemlist:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-virtual {p0, p1, v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentHdlCmdAddSelectedAgent(Lcom/policydm/agent/XDMAgent;Lcom/policydm/eng/core/XDMLinkedList;)V

    goto :goto_0

    .line 363
    :cond_1
    iget-object v1, v0, Lcom/policydm/agent/XDMAgent;->m_Sequence:Lcom/policydm/eng/parser/XDMParserSequence;

    if-eqz v1, :cond_3

    .line 365
    iget-object v1, v0, Lcom/policydm/agent/XDMAgent;->m_Sequence:Lcom/policydm/eng/parser/XDMParserSequence;

    iget-object v1, v1, Lcom/policydm/eng/parser/XDMParserSequence;->itemlist:Lcom/policydm/eng/core/XDMLinkedList;

    if-eqz v1, :cond_2

    .line 367
    iget-object v1, v0, Lcom/policydm/agent/XDMAgent;->m_Sequence:Lcom/policydm/eng/parser/XDMParserSequence;

    iget-object v1, v1, Lcom/policydm/eng/parser/XDMParserSequence;->itemlist:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-virtual {p0, p1, v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentHdlCmdAddSelectedAgent(Lcom/policydm/agent/XDMAgent;Lcom/policydm/eng/core/XDMLinkedList;)V

    goto :goto_0

    .line 371
    :cond_2
    iget-object v1, v0, Lcom/policydm/agent/XDMAgent;->m_Sequence:Lcom/policydm/eng/parser/XDMParserSequence;

    invoke-static {}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListCreateLinkedList()Lcom/policydm/eng/core/XDMLinkedList;

    move-result-object v2

    iput-object v2, v1, Lcom/policydm/eng/parser/XDMParserSequence;->itemlist:Lcom/policydm/eng/core/XDMLinkedList;

    .line 372
    iget-object v1, v0, Lcom/policydm/agent/XDMAgent;->m_Sequence:Lcom/policydm/eng/parser/XDMParserSequence;

    iget-object v1, v1, Lcom/policydm/eng/parser/XDMParserSequence;->itemlist:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-virtual {p0, p1, v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentHdlCmdAddSelectedAgent(Lcom/policydm/agent/XDMAgent;Lcom/policydm/eng/core/XDMLinkedList;)V

    goto :goto_0

    .line 377
    :cond_3
    invoke-virtual {p0, p2, p1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto :goto_0

    .line 383
    :cond_4
    const-string v1, "Atomic_Start"

    iget-object v2, p1, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "Sequence_Start"

    iget-object v2, p1, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_6

    .line 385
    :cond_5
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/policydm/agent/XDMAgent;->m_bInProgresscmd:Z

    .line 387
    :cond_6
    iget-object v1, v0, Lcom/policydm/agent/XDMAgent;->m_Atomic:Lcom/policydm/eng/parser/XDMParserAtomic;

    if-eqz v1, :cond_8

    .line 389
    iget-object v1, v0, Lcom/policydm/agent/XDMAgent;->m_Atomic:Lcom/policydm/eng/parser/XDMParserAtomic;

    iget-object p2, v1, Lcom/policydm/eng/parser/XDMParserAtomic;->itemlist:Lcom/policydm/eng/core/XDMLinkedList;

    .line 396
    :cond_7
    :goto_1
    if-eqz p2, :cond_9

    .line 398
    invoke-virtual {p0, p2, p1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto :goto_0

    .line 391
    :cond_8
    iget-object v1, v0, Lcom/policydm/agent/XDMAgent;->m_Sequence:Lcom/policydm/eng/parser/XDMParserSequence;

    if-eqz v1, :cond_7

    .line 393
    iget-object v1, v0, Lcom/policydm/agent/XDMAgent;->m_Sequence:Lcom/policydm/eng/parser/XDMParserSequence;

    iget-object p2, v1, Lcom/policydm/eng/parser/XDMParserSequence;->itemlist:Lcom/policydm/eng/core/XDMLinkedList;

    goto :goto_1

    .line 402
    :cond_9
    invoke-static {}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListCreateLinkedList()Lcom/policydm/eng/core/XDMLinkedList;

    move-result-object p2

    .line 403
    invoke-virtual {p0, p2, p1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto :goto_0

    .line 409
    :cond_a
    invoke-static {}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListCreateLinkedList()Lcom/policydm/eng/core/XDMLinkedList;

    move-result-object p2

    .line 410
    invoke-virtual {p0, p2, p1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public xdmAgentHdlCmdAlert(Ljava/lang/Object;Lcom/policydm/eng/parser/XDMParserAlert;)V
    .locals 3
    .param p1, "userdata"    # Ljava/lang/Object;
    .param p2, "alert"    # Lcom/policydm/eng/parser/XDMParserAlert;

    .prologue
    .line 142
    move-object v1, p1

    check-cast v1, Lcom/policydm/eng/core/XDMWorkspace;

    .line 145
    .local v1, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    new-instance v0, Lcom/policydm/agent/XDMAgent;

    invoke-direct {v0}, Lcom/policydm/agent/XDMAgent;-><init>()V

    .line 146
    .local v0, "agent":Lcom/policydm/agent/XDMAgent;
    new-instance v2, Lcom/policydm/eng/parser/XDMParserAlert;

    invoke-direct {v2}, Lcom/policydm/eng/parser/XDMParserAlert;-><init>()V

    iput-object v2, v0, Lcom/policydm/agent/XDMAgent;->m_Alert:Lcom/policydm/eng/parser/XDMParserAlert;

    .line 147
    iget-object v2, v0, Lcom/policydm/agent/XDMAgent;->m_Alert:Lcom/policydm/eng/parser/XDMParserAlert;

    invoke-static {v2, p2}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplAlert(Lcom/policydm/eng/parser/XDMParserAlert;Lcom/policydm/eng/parser/XDMParserAlert;)V

    .line 149
    const-string v2, "Alert"

    iput-object v2, v0, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    .line 150
    iget-boolean v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->inAtomicCmd:Z

    if-nez v2, :cond_0

    iget-boolean v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->inSequenceCmd:Z

    if-eqz v2, :cond_1

    .line 152
    :cond_0
    iget-object v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->list:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v0, v2}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentHdlCmdAddSelectedAgent(Lcom/policydm/agent/XDMAgent;Lcom/policydm/eng/core/XDMLinkedList;)V

    .line 158
    :goto_0
    return-void

    .line 156
    :cond_1
    iget-object v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->list:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v2, v0}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public xdmAgentHdlCmdAtomicEnd(Ljava/lang/Object;)V
    .locals 3
    .param p1, "userdata"    # Ljava/lang/Object;

    .prologue
    .line 295
    move-object v1, p1

    check-cast v1, Lcom/policydm/eng/core/XDMWorkspace;

    .line 296
    .local v1, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    const/4 v0, 0x0

    .line 297
    .local v0, "locateagent":Lcom/policydm/agent/XDMAgent;
    iget-boolean v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->inAtomicCmd:Z

    if-nez v2, :cond_0

    iget-boolean v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->inSequenceCmd:Z

    if-eqz v2, :cond_1

    .line 299
    :cond_0
    iget-object v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->list:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v0, v2}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentHdlCmdLocateSelectedAgent(Lcom/policydm/agent/XDMAgent;Lcom/policydm/eng/core/XDMLinkedList;)V

    .line 300
    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->inAtomicCmd:Z

    .line 302
    :cond_1
    return-void
.end method

.method public xdmAgentHdlCmdAtomicStart(Ljava/lang/Object;Lcom/policydm/eng/parser/XDMParserAtomic;)V
    .locals 3
    .param p1, "userdata"    # Ljava/lang/Object;
    .param p2, "atomic"    # Lcom/policydm/eng/parser/XDMParserAtomic;

    .prologue
    .line 268
    move-object v1, p1

    check-cast v1, Lcom/policydm/eng/core/XDMWorkspace;

    .line 271
    .local v1, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    new-instance v0, Lcom/policydm/agent/XDMAgent;

    invoke-direct {v0}, Lcom/policydm/agent/XDMAgent;-><init>()V

    .line 272
    .local v0, "agent":Lcom/policydm/agent/XDMAgent;
    new-instance v2, Lcom/policydm/eng/parser/XDMParserAtomic;

    invoke-direct {v2}, Lcom/policydm/eng/parser/XDMParserAtomic;-><init>()V

    iput-object v2, v0, Lcom/policydm/agent/XDMAgent;->m_Atomic:Lcom/policydm/eng/parser/XDMParserAtomic;

    .line 273
    iget-object v2, v0, Lcom/policydm/agent/XDMAgent;->m_Atomic:Lcom/policydm/eng/parser/XDMParserAtomic;

    invoke-virtual {p0, v2, p2}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplAtomic(Lcom/policydm/eng/parser/XDMParserAtomic;Lcom/policydm/eng/parser/XDMParserAtomic;)V

    .line 274
    new-instance v2, Lcom/policydm/eng/parser/XDMParserAtomic;

    invoke-direct {v2}, Lcom/policydm/eng/parser/XDMParserAtomic;-><init>()V

    iput-object v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->atomic:Lcom/policydm/eng/parser/XDMParserAtomic;

    .line 275
    iget-object v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->atomic:Lcom/policydm/eng/parser/XDMParserAtomic;

    invoke-virtual {p0, v2, p2}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplAtomic(Lcom/policydm/eng/parser/XDMParserAtomic;Lcom/policydm/eng/parser/XDMParserAtomic;)V

    .line 277
    const-string v2, "Atomic_Start"

    iput-object v2, v0, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    .line 278
    iget-boolean v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->inAtomicCmd:Z

    if-nez v2, :cond_0

    iget-boolean v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->inSequenceCmd:Z

    if-eqz v2, :cond_1

    .line 280
    :cond_0
    iget-object v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->list:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v0, v2}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentHdlCmdAddSelectedAgent(Lcom/policydm/agent/XDMAgent;Lcom/policydm/eng/core/XDMLinkedList;)V

    .line 287
    :goto_0
    return-void

    .line 284
    :cond_1
    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->inAtomicCmd:Z

    .line 285
    iget-object v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->list:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v2, v0}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public xdmAgentHdlCmdCopy(Ljava/lang/Object;Lcom/policydm/eng/parser/XDMParserCopy;)V
    .locals 3
    .param p1, "userdata"    # Ljava/lang/Object;
    .param p2, "copyCmd"    # Lcom/policydm/eng/parser/XDMParserCopy;

    .prologue
    .line 217
    move-object v1, p1

    check-cast v1, Lcom/policydm/eng/core/XDMWorkspace;

    .line 220
    .local v1, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    new-instance v0, Lcom/policydm/agent/XDMAgent;

    invoke-direct {v0}, Lcom/policydm/agent/XDMAgent;-><init>()V

    .line 221
    .local v0, "agent":Lcom/policydm/agent/XDMAgent;
    new-instance v2, Lcom/policydm/eng/parser/XDMParserCopy;

    invoke-direct {v2}, Lcom/policydm/eng/parser/XDMParserCopy;-><init>()V

    iput-object v2, v0, Lcom/policydm/agent/XDMAgent;->m_CopyCmd:Lcom/policydm/eng/parser/XDMParserCopy;

    .line 223
    iget-object v2, v0, Lcom/policydm/agent/XDMAgent;->m_CopyCmd:Lcom/policydm/eng/parser/XDMParserCopy;

    invoke-virtual {p0, v2, p2}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplCopy(Lcom/policydm/eng/parser/XDMParserCopy;Lcom/policydm/eng/parser/XDMParserCopy;)V

    .line 225
    const-string v2, "Copy"

    iput-object v2, v0, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    .line 226
    iget-boolean v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->inAtomicCmd:Z

    if-nez v2, :cond_0

    iget-boolean v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->inSequenceCmd:Z

    if-eqz v2, :cond_1

    .line 228
    :cond_0
    iget-object v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->list:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v0, v2}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentHdlCmdAddSelectedAgent(Lcom/policydm/agent/XDMAgent;Lcom/policydm/eng/core/XDMLinkedList;)V

    .line 234
    :goto_0
    return-void

    .line 232
    :cond_1
    iget-object v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->list:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v2, v0}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public xdmAgentHdlCmdDelete(Ljava/lang/Object;Lcom/policydm/eng/parser/XDMParserDelete;)V
    .locals 3
    .param p1, "userdata"    # Ljava/lang/Object;
    .param p2, "deleteCmd"    # Lcom/policydm/eng/parser/XDMParserDelete;

    .prologue
    .line 243
    move-object v1, p1

    check-cast v1, Lcom/policydm/eng/core/XDMWorkspace;

    .line 246
    .local v1, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    new-instance v0, Lcom/policydm/agent/XDMAgent;

    invoke-direct {v0}, Lcom/policydm/agent/XDMAgent;-><init>()V

    .line 247
    .local v0, "agent":Lcom/policydm/agent/XDMAgent;
    new-instance v2, Lcom/policydm/eng/parser/XDMParserDelete;

    invoke-direct {v2}, Lcom/policydm/eng/parser/XDMParserDelete;-><init>()V

    iput-object v2, v0, Lcom/policydm/agent/XDMAgent;->m_DeleteCmd:Lcom/policydm/eng/parser/XDMParserDelete;

    .line 248
    iget-object v2, v0, Lcom/policydm/agent/XDMAgent;->m_DeleteCmd:Lcom/policydm/eng/parser/XDMParserDelete;

    invoke-virtual {p0, v2, p2}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplDelete(Lcom/policydm/eng/parser/XDMParserDelete;Lcom/policydm/eng/parser/XDMParserDelete;)V

    .line 250
    const-string v2, "Delete"

    iput-object v2, v0, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    .line 251
    iget-boolean v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->inAtomicCmd:Z

    if-nez v2, :cond_0

    iget-boolean v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->inSequenceCmd:Z

    if-eqz v2, :cond_1

    .line 253
    :cond_0
    iget-object v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->list:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v0, v2}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentHdlCmdAddSelectedAgent(Lcom/policydm/agent/XDMAgent;Lcom/policydm/eng/core/XDMLinkedList;)V

    .line 259
    :goto_0
    return-void

    .line 257
    :cond_1
    iget-object v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->list:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v2, v0}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public xdmAgentHdlCmdExec(Ljava/lang/Object;Lcom/policydm/eng/parser/XDMParserExec;)V
    .locals 3
    .param p1, "userdata"    # Ljava/lang/Object;
    .param p2, "exec"    # Lcom/policydm/eng/parser/XDMParserExec;

    .prologue
    .line 116
    move-object v1, p1

    check-cast v1, Lcom/policydm/eng/core/XDMWorkspace;

    .line 119
    .local v1, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    new-instance v0, Lcom/policydm/agent/XDMAgent;

    invoke-direct {v0}, Lcom/policydm/agent/XDMAgent;-><init>()V

    .line 120
    .local v0, "agent":Lcom/policydm/agent/XDMAgent;
    new-instance v2, Lcom/policydm/eng/parser/XDMParserExec;

    invoke-direct {v2}, Lcom/policydm/eng/parser/XDMParserExec;-><init>()V

    iput-object v2, v0, Lcom/policydm/agent/XDMAgent;->m_Exec:Lcom/policydm/eng/parser/XDMParserExec;

    .line 122
    iget-object v2, v0, Lcom/policydm/agent/XDMAgent;->m_Exec:Lcom/policydm/eng/parser/XDMParserExec;

    invoke-virtual {p0, v2, p2}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplExec(Lcom/policydm/eng/parser/XDMParserExec;Lcom/policydm/eng/parser/XDMParserExec;)V

    .line 124
    const-string v2, "Exec"

    iput-object v2, v0, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    .line 125
    iget-boolean v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->inAtomicCmd:Z

    if-nez v2, :cond_0

    iget-boolean v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->inSequenceCmd:Z

    if-eqz v2, :cond_1

    .line 127
    :cond_0
    iget-object v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->list:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v0, v2}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentHdlCmdAddSelectedAgent(Lcom/policydm/agent/XDMAgent;Lcom/policydm/eng/core/XDMLinkedList;)V

    .line 133
    :goto_0
    return-void

    .line 131
    :cond_1
    iget-object v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->list:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v2, v0}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public xdmAgentHdlCmdGet(Ljava/lang/Object;Lcom/policydm/eng/parser/XDMParserGet;)V
    .locals 3
    .param p1, "userdata"    # Ljava/lang/Object;
    .param p2, "get"    # Lcom/policydm/eng/parser/XDMParserGet;

    .prologue
    .line 91
    move-object v1, p1

    check-cast v1, Lcom/policydm/eng/core/XDMWorkspace;

    .line 94
    .local v1, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    new-instance v0, Lcom/policydm/agent/XDMAgent;

    invoke-direct {v0}, Lcom/policydm/agent/XDMAgent;-><init>()V

    .line 95
    .local v0, "agent":Lcom/policydm/agent/XDMAgent;
    new-instance v2, Lcom/policydm/eng/parser/XDMParserGet;

    invoke-direct {v2}, Lcom/policydm/eng/parser/XDMParserGet;-><init>()V

    iput-object v2, v0, Lcom/policydm/agent/XDMAgent;->m_Get:Lcom/policydm/eng/parser/XDMParserGet;

    .line 96
    iget-object v2, v0, Lcom/policydm/agent/XDMAgent;->m_Get:Lcom/policydm/eng/parser/XDMParserGet;

    invoke-virtual {p0, v2, p2}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplGet(Lcom/policydm/eng/parser/XDMParserGet;Lcom/policydm/eng/parser/XDMParserGet;)V

    .line 98
    const-string v2, "Get"

    iput-object v2, v0, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    .line 99
    iget-boolean v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->inAtomicCmd:Z

    if-nez v2, :cond_0

    iget-boolean v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->inSequenceCmd:Z

    if-eqz v2, :cond_1

    .line 101
    :cond_0
    iget-object v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->list:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v0, v2}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentHdlCmdAddSelectedAgent(Lcom/policydm/agent/XDMAgent;Lcom/policydm/eng/core/XDMLinkedList;)V

    .line 107
    :goto_0
    return-void

    .line 105
    :cond_1
    iget-object v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->list:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v2, v0}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public xdmAgentHdlCmdLocateSelectedAgent(Lcom/policydm/agent/XDMAgent;Lcom/policydm/eng/core/XDMLinkedList;)V
    .locals 2
    .param p1, "agent"    # Lcom/policydm/agent/XDMAgent;
    .param p2, "curlocate"    # Lcom/policydm/eng/core/XDMLinkedList;

    .prologue
    .line 421
    const/4 v0, 0x0

    .line 423
    .local v0, "cmdagent":Lcom/policydm/agent/XDMAgent;
    iget v1, p2, Lcom/policydm/eng/core/XDMLinkedList;->count:I

    add-int/lit8 v1, v1, -0x1

    invoke-static {p2, v1}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListGetObj(Lcom/policydm/eng/core/XDMLinkedList;I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "cmdagent":Lcom/policydm/agent/XDMAgent;
    check-cast v0, Lcom/policydm/agent/XDMAgent;

    .line 424
    .restart local v0    # "cmdagent":Lcom/policydm/agent/XDMAgent;
    if-eqz v0, :cond_0

    .line 426
    iget-boolean v1, v0, Lcom/policydm/agent/XDMAgent;->m_bInProgresscmd:Z

    if-eqz v1, :cond_2

    .line 428
    move-object p1, v0

    .line 429
    iget-object v1, v0, Lcom/policydm/agent/XDMAgent;->m_Atomic:Lcom/policydm/eng/parser/XDMParserAtomic;

    if-eqz v1, :cond_1

    .line 431
    iget-object v1, v0, Lcom/policydm/agent/XDMAgent;->m_Atomic:Lcom/policydm/eng/parser/XDMParserAtomic;

    iget-object v1, v1, Lcom/policydm/eng/parser/XDMParserAtomic;->itemlist:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-virtual {p0, p1, v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentHdlCmdLocateSelectedAgent(Lcom/policydm/agent/XDMAgent;Lcom/policydm/eng/core/XDMLinkedList;)V

    .line 446
    :cond_0
    :goto_0
    return-void

    .line 433
    :cond_1
    iget-object v1, v0, Lcom/policydm/agent/XDMAgent;->m_Sequence:Lcom/policydm/eng/parser/XDMParserSequence;

    if-eqz v1, :cond_0

    .line 435
    iget-object v1, v0, Lcom/policydm/agent/XDMAgent;->m_Sequence:Lcom/policydm/eng/parser/XDMParserSequence;

    iget-object v1, v1, Lcom/policydm/eng/parser/XDMParserSequence;->itemlist:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-virtual {p0, p1, v1}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentHdlCmdLocateSelectedAgent(Lcom/policydm/agent/XDMAgent;Lcom/policydm/eng/core/XDMLinkedList;)V

    goto :goto_0

    .line 440
    :cond_2
    if-eqz p1, :cond_0

    .line 442
    const/4 v1, 0x0

    iput-boolean v1, p1, Lcom/policydm/agent/XDMAgent;->m_bInProgresscmd:Z

    goto :goto_0
.end method

.method public xdmAgentHdlCmdReplace(Ljava/lang/Object;Lcom/policydm/eng/parser/XDMParserReplace;)V
    .locals 3
    .param p1, "userdata"    # Ljava/lang/Object;
    .param p2, "replaceCmd"    # Lcom/policydm/eng/parser/XDMParserReplace;

    .prologue
    .line 192
    move-object v1, p1

    check-cast v1, Lcom/policydm/eng/core/XDMWorkspace;

    .line 195
    .local v1, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    new-instance v0, Lcom/policydm/agent/XDMAgent;

    invoke-direct {v0}, Lcom/policydm/agent/XDMAgent;-><init>()V

    .line 196
    .local v0, "agent":Lcom/policydm/agent/XDMAgent;
    new-instance v2, Lcom/policydm/eng/parser/XDMParserReplace;

    invoke-direct {v2}, Lcom/policydm/eng/parser/XDMParserReplace;-><init>()V

    iput-object v2, v0, Lcom/policydm/agent/XDMAgent;->m_ReplaceCmd:Lcom/policydm/eng/parser/XDMParserReplace;

    .line 197
    iget-object v2, v0, Lcom/policydm/agent/XDMAgent;->m_ReplaceCmd:Lcom/policydm/eng/parser/XDMParserReplace;

    invoke-virtual {p0, v2, p2}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplReplace(Lcom/policydm/eng/parser/XDMParserReplace;Lcom/policydm/eng/parser/XDMParserReplace;)V

    .line 199
    const-string v2, "Replace"

    iput-object v2, v0, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    .line 200
    iget-boolean v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->inAtomicCmd:Z

    if-nez v2, :cond_0

    iget-boolean v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->inSequenceCmd:Z

    if-eqz v2, :cond_1

    .line 202
    :cond_0
    iget-object v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->list:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v0, v2}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentHdlCmdAddSelectedAgent(Lcom/policydm/agent/XDMAgent;Lcom/policydm/eng/core/XDMLinkedList;)V

    .line 208
    :goto_0
    return-void

    .line 206
    :cond_1
    iget-object v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->list:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v2, v0}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public xdmAgentHdlCmdSequenceEnd(Ljava/lang/Object;)V
    .locals 3
    .param p1, "userdata"    # Ljava/lang/Object;

    .prologue
    .line 454
    move-object v1, p1

    check-cast v1, Lcom/policydm/eng/core/XDMWorkspace;

    .line 455
    .local v1, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    const/4 v0, 0x0

    .line 456
    .local v0, "locateagent":Lcom/policydm/agent/XDMAgent;
    iget-boolean v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->inSequenceCmd:Z

    if-nez v2, :cond_0

    iget-boolean v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->inAtomicCmd:Z

    if-eqz v2, :cond_1

    .line 458
    :cond_0
    iget-object v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->list:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v0, v2}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentHdlCmdLocateSelectedAgent(Lcom/policydm/agent/XDMAgent;Lcom/policydm/eng/core/XDMLinkedList;)V

    .line 459
    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->inSequenceCmd:Z

    .line 461
    :cond_1
    return-void
.end method

.method public xdmAgentHdlCmdSequenceStart(Ljava/lang/Object;Lcom/policydm/eng/parser/XDMParserSequence;)V
    .locals 3
    .param p1, "userdata"    # Ljava/lang/Object;
    .param p2, "sequence"    # Lcom/policydm/eng/parser/XDMParserSequence;

    .prologue
    .line 311
    move-object v1, p1

    check-cast v1, Lcom/policydm/eng/core/XDMWorkspace;

    .line 314
    .local v1, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    new-instance v0, Lcom/policydm/agent/XDMAgent;

    invoke-direct {v0}, Lcom/policydm/agent/XDMAgent;-><init>()V

    .line 315
    .local v0, "agent":Lcom/policydm/agent/XDMAgent;
    new-instance v2, Lcom/policydm/eng/parser/XDMParserSequence;

    invoke-direct {v2}, Lcom/policydm/eng/parser/XDMParserSequence;-><init>()V

    iput-object v2, v0, Lcom/policydm/agent/XDMAgent;->m_Sequence:Lcom/policydm/eng/parser/XDMParserSequence;

    .line 316
    iget-object v2, v0, Lcom/policydm/agent/XDMAgent;->m_Sequence:Lcom/policydm/eng/parser/XDMParserSequence;

    invoke-virtual {p0, v2, p2}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplSequence(Lcom/policydm/eng/parser/XDMParserSequence;Lcom/policydm/eng/parser/XDMParserSequence;)V

    .line 317
    iget-object v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->sequence:Lcom/policydm/eng/parser/XDMParserSequence;

    if-eqz v2, :cond_0

    .line 319
    iget-object v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->sequence:Lcom/policydm/eng/parser/XDMParserSequence;

    invoke-virtual {p0, v2}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDeleteSequence(Ljava/lang/Object;)V

    .line 321
    :cond_0
    new-instance v2, Lcom/policydm/eng/parser/XDMParserSequence;

    invoke-direct {v2}, Lcom/policydm/eng/parser/XDMParserSequence;-><init>()V

    iput-object v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->sequence:Lcom/policydm/eng/parser/XDMParserSequence;

    .line 322
    iget-object v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->sequence:Lcom/policydm/eng/parser/XDMParserSequence;

    invoke-virtual {p0, v2, p2}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplSequence(Lcom/policydm/eng/parser/XDMParserSequence;Lcom/policydm/eng/parser/XDMParserSequence;)V

    .line 324
    const-string v2, "Sequence_Start"

    iput-object v2, v0, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    .line 326
    iget-boolean v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->inAtomicCmd:Z

    if-nez v2, :cond_1

    iget-boolean v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->inSequenceCmd:Z

    if-eqz v2, :cond_2

    .line 328
    :cond_1
    iget-object v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->list:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v0, v2}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentHdlCmdAddSelectedAgent(Lcom/policydm/agent/XDMAgent;Lcom/policydm/eng/core/XDMLinkedList;)V

    .line 335
    :goto_0
    return-void

    .line 332
    :cond_2
    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->inSequenceCmd:Z

    .line 333
    iget-object v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->list:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v2, v0}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public xdmAgentHdlCmdStatus(Ljava/lang/Object;Lcom/policydm/eng/parser/XDMParserStatus;)V
    .locals 3
    .param p1, "userdata"    # Ljava/lang/Object;
    .param p2, "status"    # Lcom/policydm/eng/parser/XDMParserStatus;

    .prologue
    .line 64
    move-object v1, p1

    check-cast v1, Lcom/policydm/eng/core/XDMWorkspace;

    .line 67
    .local v1, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    new-instance v0, Lcom/policydm/agent/XDMAgent;

    invoke-direct {v0}, Lcom/policydm/agent/XDMAgent;-><init>()V

    .line 68
    .local v0, "agent":Lcom/policydm/agent/XDMAgent;
    new-instance v2, Lcom/policydm/eng/parser/XDMParserStatus;

    invoke-direct {v2}, Lcom/policydm/eng/parser/XDMParserStatus;-><init>()V

    iput-object v2, v0, Lcom/policydm/agent/XDMAgent;->m_Status:Lcom/policydm/eng/parser/XDMParserStatus;

    .line 69
    iget-object v2, v0, Lcom/policydm/agent/XDMAgent;->m_Status:Lcom/policydm/eng/parser/XDMParserStatus;

    invoke-virtual {p0, v2, p2}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplStatus(Lcom/policydm/eng/parser/XDMParserStatus;Lcom/policydm/eng/parser/XDMParserStatus;)V

    .line 71
    const-string v2, "Status"

    iput-object v2, v0, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    .line 73
    iget-boolean v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->inAtomicCmd:Z

    if-nez v2, :cond_0

    iget-boolean v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->inSequenceCmd:Z

    if-eqz v2, :cond_1

    .line 75
    :cond_0
    iget-object v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->list:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v0, v2}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentHdlCmdAddSelectedAgent(Lcom/policydm/agent/XDMAgent;Lcom/policydm/eng/core/XDMLinkedList;)V

    .line 82
    :goto_0
    return-void

    .line 79
    :cond_1
    iget-object v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->list:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v2, v0}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public xdmAgentHdlCmdSyncHdr(Ljava/lang/Object;Lcom/policydm/eng/parser/XDMParserSyncheader;)V
    .locals 3
    .param p1, "userdata"    # Ljava/lang/Object;
    .param p2, "header"    # Lcom/policydm/eng/parser/XDMParserSyncheader;

    .prologue
    .line 39
    move-object v1, p1

    check-cast v1, Lcom/policydm/eng/core/XDMWorkspace;

    .line 42
    .local v1, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    new-instance v0, Lcom/policydm/agent/XDMAgent;

    invoke-direct {v0}, Lcom/policydm/agent/XDMAgent;-><init>()V

    .line 43
    .local v0, "agent":Lcom/policydm/agent/XDMAgent;
    new-instance v2, Lcom/policydm/eng/parser/XDMParserSyncheader;

    invoke-direct {v2}, Lcom/policydm/eng/parser/XDMParserSyncheader;-><init>()V

    iput-object v2, v0, Lcom/policydm/agent/XDMAgent;->m_Header:Lcom/policydm/eng/parser/XDMParserSyncheader;

    .line 44
    iget-object v2, v0, Lcom/policydm/agent/XDMAgent;->m_Header:Lcom/policydm/eng/parser/XDMParserSyncheader;

    invoke-virtual {p0, v2, p2}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentDataStDuplSyncHeader(Lcom/policydm/eng/parser/XDMParserSyncheader;Lcom/policydm/eng/parser/XDMParserSyncheader;)V

    .line 46
    const-string v2, "SyncHdr"

    iput-object v2, v0, Lcom/policydm/agent/XDMAgent;->m_szCmd:Ljava/lang/String;

    .line 47
    iget-boolean v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->inAtomicCmd:Z

    if-nez v2, :cond_0

    iget-boolean v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->inSequenceCmd:Z

    if-eqz v2, :cond_1

    .line 49
    :cond_0
    iget-object v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->list:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v0, v2}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentHdlCmdAddSelectedAgent(Lcom/policydm/agent/XDMAgent;Lcom/policydm/eng/core/XDMLinkedList;)V

    .line 55
    :goto_0
    return-void

    .line 53
    :cond_1
    iget-object v2, v1, Lcom/policydm/eng/core/XDMWorkspace;->list:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-virtual {p0, v2, v0}, Lcom/policydm/agent/XDMHandleCmd;->xdmAgentListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public xdmAgentListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V
    .locals 3
    .param p1, "list"    # Lcom/policydm/eng/core/XDMLinkedList;
    .param p2, "obj"    # Ljava/lang/Object;

    .prologue
    .line 470
    iget-object v1, p1, Lcom/policydm/eng/core/XDMLinkedList;->top:Lcom/policydm/eng/core/XDMNode;

    .line 471
    .local v1, "top":Lcom/policydm/eng/core/XDMNode;
    new-instance v0, Lcom/policydm/eng/core/XDMNode;

    invoke-direct {v0}, Lcom/policydm/eng/core/XDMNode;-><init>()V

    .line 473
    .local v0, "node":Lcom/policydm/eng/core/XDMNode;
    invoke-static {v0, p2}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListBindObjectToNode(Lcom/policydm/eng/core/XDMNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 475
    if-eqz v0, :cond_0

    .line 477
    iput-object v1, v0, Lcom/policydm/eng/core/XDMNode;->next:Lcom/policydm/eng/core/XDMNode;

    .line 478
    iget-object v2, v1, Lcom/policydm/eng/core/XDMNode;->previous:Lcom/policydm/eng/core/XDMNode;

    iput-object v2, v0, Lcom/policydm/eng/core/XDMNode;->previous:Lcom/policydm/eng/core/XDMNode;

    .line 479
    iget-object v2, v1, Lcom/policydm/eng/core/XDMNode;->previous:Lcom/policydm/eng/core/XDMNode;

    iput-object v0, v2, Lcom/policydm/eng/core/XDMNode;->next:Lcom/policydm/eng/core/XDMNode;

    .line 480
    iput-object v0, v1, Lcom/policydm/eng/core/XDMNode;->previous:Lcom/policydm/eng/core/XDMNode;

    .line 482
    iget v2, p1, Lcom/policydm/eng/core/XDMLinkedList;->count:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p1, Lcom/policydm/eng/core/XDMLinkedList;->count:I

    .line 484
    :cond_0
    return-void
.end method
