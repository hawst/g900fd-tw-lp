.class final enum Lcom/policydm/adapter/XSPDAdapter$HASH_PREFIX;
.super Ljava/lang/Enum;
.source "XSPDAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/policydm/adapter/XSPDAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "HASH_PREFIX"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/policydm/adapter/XSPDAdapter$HASH_PREFIX;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/policydm/adapter/XSPDAdapter$HASH_PREFIX;

.field public static final enum MP:Lcom/policydm/adapter/XSPDAdapter$HASH_PREFIX;

.field public static final enum MV:Lcom/policydm/adapter/XSPDAdapter$HASH_PREFIX;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 44
    new-instance v0, Lcom/policydm/adapter/XSPDAdapter$HASH_PREFIX;

    const-string v1, "MP"

    invoke-direct {v0, v1, v2}, Lcom/policydm/adapter/XSPDAdapter$HASH_PREFIX;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/policydm/adapter/XSPDAdapter$HASH_PREFIX;->MP:Lcom/policydm/adapter/XSPDAdapter$HASH_PREFIX;

    new-instance v0, Lcom/policydm/adapter/XSPDAdapter$HASH_PREFIX;

    const-string v1, "MV"

    invoke-direct {v0, v1, v3}, Lcom/policydm/adapter/XSPDAdapter$HASH_PREFIX;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/policydm/adapter/XSPDAdapter$HASH_PREFIX;->MV:Lcom/policydm/adapter/XSPDAdapter$HASH_PREFIX;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/policydm/adapter/XSPDAdapter$HASH_PREFIX;

    sget-object v1, Lcom/policydm/adapter/XSPDAdapter$HASH_PREFIX;->MP:Lcom/policydm/adapter/XSPDAdapter$HASH_PREFIX;

    aput-object v1, v0, v2

    sget-object v1, Lcom/policydm/adapter/XSPDAdapter$HASH_PREFIX;->MV:Lcom/policydm/adapter/XSPDAdapter$HASH_PREFIX;

    aput-object v1, v0, v3

    sput-object v0, Lcom/policydm/adapter/XSPDAdapter$HASH_PREFIX;->$VALUES:[Lcom/policydm/adapter/XSPDAdapter$HASH_PREFIX;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/policydm/adapter/XSPDAdapter$HASH_PREFIX;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 44
    const-class v0, Lcom/policydm/adapter/XSPDAdapter$HASH_PREFIX;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/policydm/adapter/XSPDAdapter$HASH_PREFIX;

    return-object v0
.end method

.method public static values()[Lcom/policydm/adapter/XSPDAdapter$HASH_PREFIX;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/policydm/adapter/XSPDAdapter$HASH_PREFIX;->$VALUES:[Lcom/policydm/adapter/XSPDAdapter$HASH_PREFIX;

    invoke-virtual {v0}, [Lcom/policydm/adapter/XSPDAdapter$HASH_PREFIX;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/policydm/adapter/XSPDAdapter$HASH_PREFIX;

    return-object v0
.end method
