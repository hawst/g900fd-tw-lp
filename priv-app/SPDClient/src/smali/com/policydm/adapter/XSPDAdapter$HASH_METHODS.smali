.class final enum Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;
.super Ljava/lang/Enum;
.source "XSPDAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/policydm/adapter/XSPDAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "HASH_METHODS"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;

.field public static final enum MD5:Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;

.field public static final enum NONE:Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;

.field public static final enum SHA_1:Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;

.field public static final enum SHA_256:Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 45
    new-instance v0, Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;

    const-string v1, "MD5"

    invoke-direct {v0, v1, v2}, Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;->MD5:Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;

    new-instance v0, Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;

    const-string v1, "SHA_1"

    invoke-direct {v0, v1, v3}, Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;->SHA_1:Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;

    new-instance v0, Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;

    const-string v1, "SHA_256"

    invoke-direct {v0, v1, v4}, Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;->SHA_256:Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;

    new-instance v0, Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v5}, Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;->NONE:Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;

    sget-object v1, Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;->MD5:Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;

    aput-object v1, v0, v2

    sget-object v1, Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;->SHA_1:Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;

    aput-object v1, v0, v3

    sget-object v1, Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;->SHA_256:Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;

    aput-object v1, v0, v4

    sget-object v1, Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;->NONE:Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;

    aput-object v1, v0, v5

    sput-object v0, Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;->$VALUES:[Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 45
    const-class v0, Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;

    return-object v0
.end method

.method public static values()[Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;->$VALUES:[Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;

    invoke-virtual {v0}, [Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;

    return-object v0
.end method
