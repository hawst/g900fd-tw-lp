.class public Lcom/policydm/adapter/XSecureKeyLoader;
.super Ljava/lang/Object;
.source "XSecureKeyLoader.java"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 13
    :try_start_0
    const-string v1, "spdkeygen"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 19
    .local v0, "t":Ljava/lang/Throwable;
    :goto_0
    return-void

    .line 15
    .end local v0    # "t":Ljava/lang/Throwable;
    :catch_0
    move-exception v0

    .line 17
    .restart local v0    # "t":Ljava/lang/Throwable;
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static native getSPDkey()Ljava/lang/String;
.end method

.method public static native verifySignature([B[B[B)Z
.end method
