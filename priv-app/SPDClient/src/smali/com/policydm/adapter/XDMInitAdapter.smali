.class public Lcom/policydm/adapter/XDMInitAdapter;
.super Ljava/lang/Object;
.source "XDMInitAdapter.java"

# interfaces
.implements Lcom/policydm/interfaces/XDLInterface;
.implements Lcom/policydm/interfaces/XDMInterface;
.implements Lcom/policydm/interfaces/XEventInterface;
.implements Lcom/policydm/interfaces/XUIInterface;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xdmGetDataStateString(I)Ljava/lang/String;
    .locals 1
    .param p0, "nstate"    # I

    .prologue
    .line 164
    packed-switch p0, :pswitch_data_0

    .line 175
    const-string v0, "Network unknown"

    :goto_0
    return-object v0

    .line 167
    :pswitch_0
    const-string v0, "Network Status is DATA CONNECTED!"

    goto :goto_0

    .line 169
    :pswitch_1
    const-string v0, "Network Status is DATA CONNECTING!"

    goto :goto_0

    .line 171
    :pswitch_2
    const-string v0, "Network Status is DATA DISCONNECTED!"

    goto :goto_0

    .line 173
    :pswitch_3
    const-string v0, "Network Status is DATA SUSPENDED!"

    goto :goto_0

    .line 164
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static xdmInitAdpCheckDownloadResume()V
    .locals 3

    .prologue
    .line 149
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmProtoIsWIFIConnected()Z

    move-result v0

    .line 151
    .local v0, "bWificonnected":Z
    sget-boolean v1, Lcom/policydm/adapter/XDMFeature;->XDM_FEATURE_WIFI_ONLY_MODEL:Z

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    .line 153
    const-string v1, "WIFI_ONLY_MODEL, WIFI Disconnected"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 154
    const/4 v1, 0x2

    sput v1, Lcom/policydm/XDMApplication;->g_nResumeStatus:I

    .line 160
    :goto_0
    return-void

    .line 158
    :cond_0
    const/4 v1, 0x0

    const/16 v2, 0x98

    invoke-static {v1, v2}, Lcom/policydm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    goto :goto_0
.end method

.method public static xdmInitAdpCheckNetworkReady()I
    .locals 2

    .prologue
    .line 79
    invoke-static {}, Lcom/policydm/agent/XDMAgent;->xdmAgentGetSyncMode()I

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    const-string v0, "XDM_NETWORK_STATE_SYNCML_USE"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 82
    const/4 v0, 0x2

    .line 90
    :goto_0
    return v0

    .line 85
    :cond_0
    invoke-static {}, Lcom/policydm/adapter/XDMInitAdapter;->xdmInitAdpGetNetStatus()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 87
    const/4 v0, 0x1

    goto :goto_0

    .line 90
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static xdmInitAdpEXTInit()Z
    .locals 7

    .prologue
    const/16 v6, 0x64

    const/16 v3, 0x3f

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 95
    invoke-static {v5}, Lcom/policydm/agent/XDMTask;->xdmAgentTaskSetDmInitStatus(Z)V

    .line 97
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMOStatus()I

    move-result v0

    .line 98
    .local v0, "nStatus":I
    if-eq v0, v6, :cond_0

    const/16 v2, 0x50

    if-ne v0, v2, :cond_2

    .line 100
    :cond_0
    invoke-static {}, Lcom/policydm/XDMBroadcastReceiver;->xdmSPDFirstCheckOption()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 101
    invoke-static {v3, v4, v4}, Lcom/policydm/eng/core/XDMMsg;->xdmSendRCMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 140
    :cond_1
    :goto_0
    invoke-static {}, Lcom/policydm/polling/XPollingAgent;->xpollingCheckVersionInfo()Z

    .line 141
    invoke-static {}, Lcom/policydm/polling/XPollingAgent;->xPollingReportReStartAlarm()V

    .line 144
    return v5

    .line 103
    :cond_2
    const/16 v2, 0xc8

    if-eq v0, v2, :cond_3

    const/16 v2, 0x1e

    if-eq v0, v2, :cond_3

    const/16 v2, 0x28

    if-eq v0, v2, :cond_3

    const/16 v2, 0xa

    if-ne v0, v2, :cond_4

    .line 105
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "FUMO Status is ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]. FUMO Resume Event Send"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 106
    invoke-static {}, Lcom/policydm/XDMBroadcastReceiver;->xdmSPDFirstCheckOption()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 107
    invoke-static {}, Lcom/policydm/adapter/XDMInitAdapter;->xdmInitAdpCheckDownloadResume()V

    goto :goto_0

    .line 109
    :cond_4
    const/16 v2, 0x14

    if-eq v0, v2, :cond_5

    const/16 v2, 0xf1

    if-ne v0, v2, :cond_6

    .line 111
    :cond_5
    invoke-static {}, Lcom/policydm/XDMBroadcastReceiver;->xdmSPDFirstCheckOption()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 112
    invoke-static {v3, v4, v4}, Lcom/policydm/eng/core/XDMMsg;->xdmSendRCMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 114
    :cond_6
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMOInitiatedType()I

    move-result v2

    if-eqz v2, :cond_8

    .line 116
    const-string v2, "InitiatedType not none"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 118
    invoke-static {}, Lcom/policydm/db/XDB;->xdbGetWaitWifiFlag()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/policydm/XDMApplication;->xdmProtoIsWIFIConnected()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 120
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMOInitiatedType()I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_7

    .line 122
    invoke-static {}, Lcom/policydm/db/XDBNotiAdp;->xdbNotiGetInfo()Lcom/policydm/db/XDBNotiInfo;

    move-result-object v1

    .line 123
    .local v1, "notiInfo":Lcom/policydm/db/XDBNotiInfo;
    const/4 v2, 0x5

    iput v2, v1, Lcom/policydm/db/XDBNotiInfo;->uiMode:I

    .line 124
    const/16 v2, 0x1f

    invoke-static {v2, v1, v4}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 126
    .end local v1    # "notiInfo":Lcom/policydm/db/XDBNotiInfo;
    :cond_7
    invoke-static {}, Lcom/policydm/ui/XUIAdapter;->xuiAdpStartSession()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 128
    const/16 v2, 0x65

    invoke-static {v4, v2}, Lcom/policydm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    .line 129
    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Lcom/policydm/db/XDBProfileListAdp;->xdbSetNotiJobId(J)V

    .line 130
    const/16 v2, 0x67

    invoke-static {v2}, Lcom/policydm/XDMApplication;->xdmSendMessageDmHandler(I)V

    .line 131
    invoke-static {}, Lcom/policydm/noti/XNOTIAdapter;->xnotiPushAdpDeleteAllNotiQueue()V

    goto/16 :goto_0

    .line 137
    :cond_8
    invoke-static {v4, v6}, Lcom/policydm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    goto/16 :goto_0
.end method

.method public static xdmInitAdpGetNetStatus()I
    .locals 4

    .prologue
    .line 30
    const/4 v0, 0x0

    .line 32
    .local v0, "nret":I
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmProtoIsWIFIConnected()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {}, Lcom/policydm/XDMApplication;->xdmProtoIsMobileDataConnected()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 50
    :cond_0
    :goto_0
    return v0

    .line 37
    :cond_1
    sget-boolean v2, Lcom/policydm/adapter/XDMFeature;->XDM_FEATURE_WIFI_ONLY_MODEL:Z

    if-nez v2, :cond_2

    .line 39
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetDataState()I

    move-result v1

    .line 40
    .local v1, "state":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SIM : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v1}, Lcom/policydm/adapter/XDMInitAdapter;->xdmGetDataStateString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 41
    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    .line 43
    const/4 v0, -0x1

    goto :goto_0

    .line 48
    .end local v1    # "state":I
    :cond_2
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static xdmInitAdpGetSIMLockState()Z
    .locals 5

    .prologue
    const/4 v4, 0x5

    .line 55
    const/4 v1, 0x0

    .line 57
    .local v1, "bret":Z
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetSIMState()I

    move-result v0

    .line 59
    .local v0, "SIMState":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SIMState : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 60
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "xdmGetSimState() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetSimState()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 62
    if-ne v0, v4, :cond_0

    .line 64
    const/4 v1, 0x1

    .line 67
    :cond_0
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetSimState()I

    move-result v2

    if-eq v2, v4, :cond_1

    .line 69
    const-string v2, "SIM Status is not WORKING"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 70
    const/4 v1, 0x0

    .line 73
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bret : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 74
    return v1
.end method
