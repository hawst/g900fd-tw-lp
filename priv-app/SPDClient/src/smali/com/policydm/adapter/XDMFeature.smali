.class public Lcom/policydm/adapter/XDMFeature;
.super Ljava/lang/Object;
.source "XDMFeature.java"

# interfaces
.implements Lcom/policydm/interfaces/XDMExternalInterface;


# static fields
.field public static XDM_FEATURE_BEARER_SETTING_STATUS:Z

.field public static XDM_FEATURE_DATA_ONLY_MODEL:Z

.field public static XDM_FEATURE_WIFI_ONLY_MODEL:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 9
    sput-boolean v0, Lcom/policydm/adapter/XDMFeature;->XDM_FEATURE_WIFI_ONLY_MODEL:Z

    .line 10
    sput-boolean v0, Lcom/policydm/adapter/XDMFeature;->XDM_FEATURE_DATA_ONLY_MODEL:Z

    .line 11
    sput-boolean v0, Lcom/policydm/adapter/XDMFeature;->XDM_FEATURE_BEARER_SETTING_STATUS:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getConfigDevicePreId()Ljava/lang/String;
    .locals 4

    .prologue
    .line 27
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v2

    const-string v3, "CscFeature_SyncML_ConfigDevicePreId"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 28
    .local v0, "cscFeature":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cscFeature : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 34
    .end local v0    # "cscFeature":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 31
    .restart local v0    # "cscFeature":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 33
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "can not use cscFeature"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 34
    const-string v0, ""

    goto :goto_0
.end method

.method public static xdmInitialize()V
    .locals 2

    .prologue
    .line 15
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetCheckWifiOnlyModel()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 17
    const/4 v0, 0x1

    sput-boolean v0, Lcom/policydm/adapter/XDMFeature;->XDM_FEATURE_WIFI_ONLY_MODEL:Z

    .line 20
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "XDM_FEATURE_WIFI_ONLY_MODEL : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/policydm/adapter/XDMFeature;->XDM_FEATURE_WIFI_ONLY_MODEL:Z

    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 21
    return-void
.end method
