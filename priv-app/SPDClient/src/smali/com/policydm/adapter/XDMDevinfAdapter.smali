.class public Lcom/policydm/adapter/XDMDevinfAdapter;
.super Ljava/lang/Object;
.source "XDMDevinfAdapter.java"

# interfaces
.implements Lcom/policydm/interfaces/XDMDefInterface;


# static fields
.field private static final DEFAULT_DEVID_VALUE:Ljava/lang/String; = "Default"

.field private static final DEFAULT_MANUFACTURER:Ljava/lang/String; = "SAMSUNG"

.field private static final DEFAULT_MODEL_NAME:Ljava/lang/String; = "GT-I9100"

.field private static final DEFAULT_NULL_DEVID_VALUE:Ljava/lang/String; = "000000000000000"

.field private static final DEFAULT_NULL_DEVID_VALUE2:Ljava/lang/String; = "B0000000"

.field public static preferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xdmDevAdpGetDeviceID()Ljava/lang/String;
    .locals 2

    .prologue
    .line 24
    const-string v0, ""

    .line 30
    .local v0, "szDeviceId":Ljava/lang/String;
    sget-boolean v1, Lcom/policydm/adapter/XDMFeature;->XDM_FEATURE_WIFI_ONLY_MODEL:Z

    if-eqz v1, :cond_2

    .line 32
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetTWID()Ljava/lang/String;

    move-result-object v0

    .line 39
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "Default"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "000000000000000"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "B0000000"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 40
    :cond_0
    invoke-static {}, Lcom/policydm/adapter/XDMDevinfAdapter;->xdmDevAdpRetryGetDeviceID()Ljava/lang/String;

    move-result-object v0

    .line 42
    :cond_1
    return-object v0

    .line 36
    :cond_2
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetDevID()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static xdmDevAdpGetFirmwareVersion()Ljava/lang/String;
    .locals 5

    .prologue
    .line 140
    const-string v1, "%s/%s/%s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetFwV()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetCscV()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetPhoneV()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 142
    .local v0, "szFirmwareVer":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 145
    :goto_0
    return-object v0

    .line 143
    :cond_0
    const-string v0, "I9300XX"

    goto :goto_0
.end method

.method public static xdmDevAdpGetFullDeviceID()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 81
    const-string v1, ""

    .line 82
    .local v1, "szDeviceID":Ljava/lang/String;
    const-string v0, ""

    .line 84
    .local v0, "preId":Ljava/lang/String;
    sget-boolean v2, Lcom/policydm/adapter/XDMFeature;->XDM_FEATURE_WIFI_ONLY_MODEL:Z

    if-eqz v2, :cond_0

    .line 86
    const-string v2, "TWID:%s"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {}, Lcom/policydm/adapter/XDMDevinfAdapter;->xdmDevAdpGetDeviceID()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 93
    :goto_0
    return-object v1

    .line 90
    :cond_0
    invoke-static {}, Lcom/policydm/adapter/XDMDevinfAdapter;->xdmMakePreID()Ljava/lang/String;

    move-result-object v0

    .line 91
    const-string v2, "%s:%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-static {}, Lcom/policydm/adapter/XDMDevinfAdapter;->xdmDevAdpGetDeviceID()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static xdmDevAdpGetHttpUserAgent()Ljava/lang/String;
    .locals 7

    .prologue
    .line 150
    const-string v2, ""

    .line 151
    .local v2, "szUserAgent":Ljava/lang/String;
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetManufacturer()Ljava/lang/String;

    move-result-object v0

    .line 152
    .local v0, "szManufacturer":Ljava/lang/String;
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetModel()Ljava/lang/String;

    move-result-object v1

    .line 154
    .local v1, "szModelName":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 159
    :goto_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 164
    :goto_1
    const-string v3, "%s %s %s"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v5, 0x1

    aput-object v1, v4, v5

    const/4 v5, 0x2

    const-string v6, "SyncML_DM Client"

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 165
    return-object v2

    .line 156
    :cond_0
    const-string v0, "SAMSUNG"

    goto :goto_0

    .line 161
    :cond_1
    const-string v1, "GT-I9100"

    goto :goto_1
.end method

.method public static xdmDevAdpRetryGetDeviceID()Ljava/lang/String;
    .locals 5

    .prologue
    .line 47
    const-string v2, ""

    .line 49
    .local v2, "szDeviceId":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/16 v3, 0xa

    if-ge v1, v3, :cond_2

    .line 53
    const-wide/16 v3, 0x3e8

    :try_start_0
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 61
    :goto_1
    sget-boolean v3, Lcom/policydm/adapter/XDMFeature;->XDM_FEATURE_WIFI_ONLY_MODEL:Z

    if-eqz v3, :cond_1

    .line 63
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetTWID()Ljava/lang/String;

    move-result-object v2

    .line 70
    :goto_2
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "Default"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "000000000000000"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "B0000000"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 71
    :cond_0
    const-string v2, "000000000000000"

    .line 49
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 55
    :catch_0
    move-exception v0

    .line 57
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 67
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_1
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetDevID()Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 76
    :cond_2
    return-object v2
.end method

.method public static xdmDevAdpVerifyDeviceID()Z
    .locals 2

    .prologue
    .line 130
    invoke-static {}, Lcom/policydm/adapter/XDMDevinfAdapter;->xdmDevAdpGetDeviceID()Ljava/lang/String;

    move-result-object v0

    .line 132
    .local v0, "szDeviceId":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "000000000000000"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "B0000000"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "Default"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 133
    :cond_0
    const/4 v1, 0x0

    .line 135
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static xdmMakePreID()Ljava/lang/String;
    .locals 5

    .prologue
    .line 98
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "config"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    sput-object v2, Lcom/policydm/adapter/XDMDevinfAdapter;->preferences:Landroid/content/SharedPreferences;

    .line 99
    sget-object v2, Lcom/policydm/adapter/XDMDevinfAdapter;->preferences:Landroid/content/SharedPreferences;

    const-string v3, "devicePrefix"

    const-string v4, ""

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 100
    .local v1, "preId":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 102
    invoke-static {}, Lcom/policydm/adapter/XDMFeature;->getConfigDevicePreId()Ljava/lang/String;

    move-result-object v1

    .line 103
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "first time decide prefix : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 104
    const-string v2, "VZW"

    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetCustomCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmMEID()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 106
    :cond_0
    const-string v1, "MEID"

    .line 121
    :goto_0
    sget-object v2, Lcom/policydm/adapter/XDMDevinfAdapter;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 122
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "devicePrefix"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 123
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 125
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_1
    return-object v1

    .line 110
    :cond_2
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetPhoneType()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 117
    const-string v1, "IMEI"

    goto :goto_0

    .line 113
    :pswitch_0
    const-string v1, "MEID"

    .line 114
    goto :goto_0

    .line 110
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method
