.class public Lcom/policydm/adapter/XSPDAdapter;
.super Ljava/lang/Object;
.source "XSPDAdapter.java"

# interfaces
.implements Lcom/policydm/interfaces/XSPDInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;,
        Lcom/policydm/adapter/XSPDAdapter$HASH_PREFIX;
    }
.end annotation


# static fields
.field private static final FILE_CONTEXTS:Ljava/lang/String; = "/file_contexts"

.field private static final MAC_PERMISSIONS:Ljava/lang/String; = "/mac_permissions.xml"

.field private static final POLICY_FILE_PREFIX:Ljava/lang/String; = "SEPF"

.field private static final PROPERTY_CONTEXTS:Ljava/lang/String; = "/property_contexts"

.field private static PolicyFileList:[Ljava/lang/String; = null

.field private static final SEAPP_CONTEXTS:Ljava/lang/String; = "/seapp_contexts"

.field private static final SECURITY_DIR_PATH:Ljava/lang/String; = "data/security"

.field private static final SEPOLICY:Ljava/lang/String; = "/sepolicy"

.field private static final SEPOLICY_VERSION:Ljava/lang/String; = "/sepolicy_version"

.field private static final SEPOLICY_VERSION_SIGN:Ljava/lang/String; = "/sepolicy_version_sign"

.field private static final SERVICE_CONTEXTS:Ljava/lang/String; = "/service_contexts"

.field private static final SPOTA_DIR_PATH:Ljava/lang/String; = "data/security/spota"

.field private static final VERSION_FILE:Ljava/lang/String; = "VE="


# instance fields
.field private final PUBLIC_KEY:Ljava/lang/String;

.field private final PUBLIC_KEY_43:Ljava/lang/String;

.field private final PUBLIC_KEY_ENG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 50
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "/sepolicy"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "/seapp_contexts"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "/property_contexts"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "/file_contexts"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "/service_contexts"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "/mac_permissions.xml"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "/sepolicy_version"

    aput-object v2, v0, v1

    sput-object v0, Lcom/policydm/adapter/XSPDAdapter;->PolicyFileList:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    const-string v0, "public_cert.pem"

    iput-object v0, p0, Lcom/policydm/adapter/XSPDAdapter;->PUBLIC_KEY:Ljava/lang/String;

    .line 55
    const-string v0, "public_cert_eng.pem"

    iput-object v0, p0, Lcom/policydm/adapter/XSPDAdapter;->PUBLIC_KEY_ENG:Ljava/lang/String;

    .line 56
    const-string v0, "public_cert_43.pem"

    iput-object v0, p0, Lcom/policydm/adapter/XSPDAdapter;->PUBLIC_KEY_43:Ljava/lang/String;

    return-void
.end method

.method public static varargs closeQuietly([Ljava/io/Closeable;)V
    .locals 5
    .param p0, "closeables"    # [Ljava/io/Closeable;

    .prologue
    .line 224
    move-object v0, p0

    .local v0, "arr$":[Ljava/io/Closeable;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v1, v0, v3

    .line 225
    .local v1, "c":Ljava/io/Closeable;
    if-nez v1, :cond_0

    .line 224
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 227
    :cond_0
    :try_start_0
    invoke-interface {v1}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 228
    :catch_0
    move-exception v2

    .line 229
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 232
    .end local v1    # "c":Ljava/io/Closeable;
    .end local v2    # "e":Ljava/io/IOException;
    :cond_1
    return-void
.end method

.method private xspdCheckPolicyFiles()Z
    .locals 4

    .prologue
    .line 372
    const/4 v0, 0x1

    .line 374
    .local v0, "bRet":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v2, Lcom/policydm/adapter/XSPDAdapter;->PolicyFileList:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 376
    sget-object v2, Lcom/policydm/adapter/XDMTargetAdapter;->POLICY_DIR_PATH:Ljava/lang/String;

    sget-object v3, Lcom/policydm/adapter/XSPDAdapter;->PolicyFileList:[Ljava/lang/String;

    aget-object v3, v3, v1

    invoke-static {v2, v3}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpFileExists(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 378
    const/4 v0, 0x0

    .line 382
    :cond_0
    return v0

    .line 374
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static xspdCompareHashValue([Ljava/lang/String;Ljava/lang/String;)Z
    .locals 10
    .param p0, "receivedHashList"    # [Ljava/lang/String;
    .param p1, "hashMethod"    # Ljava/lang/String;

    .prologue
    .line 387
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 388
    .local v1, "fileHashList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    sget-object v0, Lcom/policydm/adapter/XSPDAdapter;->PolicyFileList:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v6, :cond_0

    aget-object v2, v0, v4

    .line 389
    .local v2, "filename":Ljava/lang/String;
    sget-object v8, Lcom/policydm/adapter/XDMTargetAdapter;->POLICY_DIR_PATH:Ljava/lang/String;

    invoke-static {v8, v2, p1}, Lcom/policydm/db/XDBFile;->xdbFileGetHash(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 390
    .local v3, "hash":Ljava/lang/String;
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 388
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 394
    .end local v2    # "filename":Ljava/lang/String;
    .end local v3    # "hash":Ljava/lang/String;
    :cond_0
    new-instance v7, Ljava/util/ArrayList;

    invoke-static {p0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 395
    .local v7, "rHashList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v5, 0x0

    .local v5, "idx":I
    :goto_1
    sget-object v8, Lcom/policydm/adapter/XSPDAdapter;->PolicyFileList:[Ljava/lang/String;

    array-length v8, v8

    add-int/lit8 v8, v8, -0x1

    if-ge v5, v8, :cond_2

    .line 396
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 395
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 397
    :cond_1
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "No such hash of "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/policydm/adapter/XSPDAdapter;->PolicyFileList:[Ljava/lang/String;

    aget-object v9, v9, v5

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " in sepolicy_version file"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 398
    const/4 v8, 0x0

    .line 400
    :goto_2
    return v8

    :cond_2
    const/4 v8, 0x1

    goto :goto_2
.end method

.method public static xspdGetDefaultPolicyVersion()Ljava/lang/String;
    .locals 2

    .prologue
    .line 211
    const/4 v0, 0x0

    .line 212
    .local v0, "policyVersion":Ljava/lang/String;
    const-string v0, "SEPF"

    .line 213
    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 214
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetModel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 215
    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 216
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetOSVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 217
    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 218
    const-string v1, "0000"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 220
    return-object v0
.end method

.method public static xspdGetLatestPolicyVersion()Ljava/lang/String;
    .locals 16

    .prologue
    const/16 v15, 0x10

    const/4 v14, 0x3

    const/4 v13, 0x2

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 281
    const-string v0, ""

    .line 284
    .local v0, "latestPolicyVer":Ljava/lang/String;
    invoke-static {}, Lcom/policydm/adapter/XSPDAdapter;->xspdGetSystemPolicyVersion()Ljava/lang/String;

    move-result-object v7

    .line 285
    .local v7, "systemPolicyVer":Ljava/lang/String;
    invoke-static {}, Lcom/policydm/adapter/XSPDAdapter;->xspdGetSPotaPolicyVersion()Ljava/lang/String;

    move-result-object v5

    .line 287
    .local v5, "spotaPolicyVer":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "systemPolicyVer = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 288
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "spotaPolicyVer = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 290
    const-string v9, "_"

    invoke-virtual {v7, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 291
    .local v8, "systemPver":[Ljava/lang/String;
    const-string v9, "_"

    invoke-virtual {v5, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 293
    .local v6, "spotaPver":[Ljava/lang/String;
    aget-object v9, v8, v11

    aget-object v10, v6, v11

    invoke-virtual {v9, v10}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v9

    if-eqz v9, :cond_1

    .line 295
    const-string v9, "latest Policy version check error. arg1"

    invoke-static {v9}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 321
    .end local v7    # "systemPolicyVer":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v7

    .line 299
    .restart local v7    # "systemPolicyVer":Ljava/lang/String;
    :cond_1
    aget-object v9, v8, v12

    aget-object v10, v6, v12

    invoke-virtual {v9, v10}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v9

    if-eqz v9, :cond_2

    .line 301
    const-string v9, "latest Policy version check error. arg2"

    invoke-static {v9}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 305
    :cond_2
    aget-object v9, v8, v13

    aget-object v10, v6, v13

    invoke-virtual {v9, v10}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v9

    if-nez v9, :cond_0

    .line 307
    aget-object v9, v8, v14

    invoke-static {v9, v15}, Ljava/lang/Long;->valueOf(Ljava/lang/String;I)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    .line 308
    .local v3, "nSysVersion":J
    aget-object v9, v6, v14

    invoke-static {v9, v15}, Ljava/lang/Long;->valueOf(Ljava/lang/String;I)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    .line 310
    .local v1, "nSPotaVersion":J
    cmp-long v9, v3, v1

    if-lez v9, :cond_3

    .line 311
    move-object v0, v7

    .line 320
    :goto_1
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "latestPolicyVer = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    move-object v7, v0

    .line 321
    goto :goto_0

    .line 313
    :cond_3
    move-object v0, v5

    goto :goto_1
.end method

.method private xspdGetPemPublicKey()[B
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 326
    const-string v5, "public_cert_43.pem"

    .line 327
    .local v5, "pemKey":Ljava/lang/String;
    const-string v7, "4.4"

    const-string v8, "ro.build.version.release"

    invoke-static {v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v7

    if-gtz v7, :cond_0

    .line 328
    const-string v5, "public_cert.pem"

    .line 329
    const-string v7, "eng"

    const-string v8, "ro.build.type"

    invoke-static {v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    const-string v5, "public_cert_eng.pem"

    .line 332
    :cond_0
    const/4 v3, 0x0

    .line 333
    .local v3, "is":Ljava/io/InputStream;
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 335
    .local v0, "am":Landroid/content/res/AssetManager;
    const/4 v7, 0x3

    :try_start_0
    invoke-virtual {v0, v5, v7}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;I)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v3

    .line 342
    :goto_0
    const/4 v1, 0x0

    .line 345
    .local v1, "cert_key":[B
    :try_start_1
    invoke-virtual {v3}, Ljava/io/InputStream;->available()I

    move-result v4

    .line 346
    .local v4, "length":I
    new-array v1, v4, [B

    .line 347
    invoke-virtual {v3, v1}, Ljava/io/InputStream;->read([B)I

    move-result v6

    .line 348
    .local v6, "read":I
    if-eq v6, v4, :cond_1

    .line 350
    const-string v7, "Pem read error."

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 352
    :cond_1
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 364
    new-array v7, v10, [Ljava/io/Closeable;

    aput-object v3, v7, v9

    invoke-static {v7}, Lcom/policydm/adapter/XSPDAdapter;->closeQuietly([Ljava/io/Closeable;)V

    .line 367
    .end local v4    # "length":I
    .end local v6    # "read":I
    :goto_1
    return-object v1

    .line 336
    .end local v1    # "cert_key":[B
    :catch_0
    move-exception v2

    .line 337
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 338
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 339
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 354
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    .restart local v1    # "cert_key":[B
    :catch_2
    move-exception v2

    .line 356
    .local v2, "e":Ljava/io/IOException;
    :try_start_2
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 364
    new-array v7, v10, [Ljava/io/Closeable;

    aput-object v3, v7, v9

    invoke-static {v7}, Lcom/policydm/adapter/XSPDAdapter;->closeQuietly([Ljava/io/Closeable;)V

    goto :goto_1

    .line 358
    .end local v2    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v2

    .line 360
    .local v2, "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 364
    new-array v7, v10, [Ljava/io/Closeable;

    aput-object v3, v7, v9

    invoke-static {v7}, Lcom/policydm/adapter/XSPDAdapter;->closeQuietly([Ljava/io/Closeable;)V

    goto :goto_1

    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v7

    new-array v8, v10, [Ljava/io/Closeable;

    aput-object v3, v8, v9

    invoke-static {v8}, Lcom/policydm/adapter/XSPDAdapter;->closeQuietly([Ljava/io/Closeable;)V

    throw v7
.end method

.method public static xspdGetPolicyMode()Ljava/lang/String;
    .locals 3

    .prologue
    .line 80
    const-string v0, ""

    .line 82
    .local v0, "policymode":Ljava/lang/String;
    invoke-static {}, Lcom/policydm/adapter/XSPDAdapter;->xspdIsEnforcedMode()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 84
    invoke-static {}, Lcom/policydm/adapter/XSPDAdapter;->xspdIsEnterprisePolicy()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 85
    const-string v0, "enterprise"

    .line 94
    :goto_0
    return-object v0

    .line 87
    :cond_0
    const-string v0, "enforcing"

    goto :goto_0

    .line 89
    :cond_1
    invoke-static {}, Lcom/policydm/db/XDBSpdAdp;->xdbGetSpdPolicyMode()Ljava/lang/String;

    move-result-object v1

    const-string v2, "enforcing"

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_2

    .line 90
    const-string v0, "enforcing"

    goto :goto_0

    .line 92
    :cond_2
    const-string v0, "permissive"

    goto :goto_0
.end method

.method public static xspdGetSPotaFileHash()Ljava/util/ArrayList;
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 236
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 237
    .local v8, "hashList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {}, Lcom/policydm/adapter/XSPDAdapter$HASH_PREFIX;->values()[Lcom/policydm/adapter/XSPDAdapter$HASH_PREFIX;

    move-result-object v19

    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    new-array v9, v0, [Ljava/lang/String;

    .line 238
    .local v9, "hashMethods":[Ljava/lang/String;
    const/16 v16, 0x0

    .line 240
    .local v16, "reader":Ljava/io/BufferedReader;
    new-instance v15, Ljava/util/HashMap;

    invoke-direct {v15}, Ljava/util/HashMap;-><init>()V

    .line 241
    .local v15, "prefixMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-static {}, Lcom/policydm/adapter/XSPDAdapter$HASH_PREFIX;->values()[Lcom/policydm/adapter/XSPDAdapter$HASH_PREFIX;

    move-result-object v2

    .local v2, "arr$":[Lcom/policydm/adapter/XSPDAdapter$HASH_PREFIX;
    array-length v12, v2

    .local v12, "len$":I
    const/4 v11, 0x0

    .local v11, "i$":I
    :goto_0
    if-ge v11, v12, :cond_0

    aget-object v14, v2, v11

    .line 242
    .local v14, "p":Lcom/policydm/adapter/XSPDAdapter$HASH_PREFIX;
    invoke-virtual {v14}, Lcom/policydm/adapter/XSPDAdapter$HASH_PREFIX;->name()Ljava/lang/String;

    move-result-object v19

    invoke-virtual {v14}, Lcom/policydm/adapter/XSPDAdapter$HASH_PREFIX;->ordinal()I

    move-result v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v15, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 241
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 245
    .end local v14    # "p":Lcom/policydm/adapter/XSPDAdapter$HASH_PREFIX;
    :cond_0
    :try_start_0
    new-instance v6, Ljava/io/File;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v20, Lcom/policydm/adapter/XDMTargetAdapter;->POLICY_DIR_PATH:Ljava/lang/String;

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "/sepolicy_version"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-direct {v6, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 246
    .local v6, "file":Ljava/io/File;
    new-instance v7, Ljava/io/FileReader;

    invoke-direct {v7, v6}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    .line 247
    .local v7, "fr":Ljava/io/FileReader;
    new-instance v17, Ljava/io/BufferedReader;

    move-object/from16 v0, v17

    invoke-direct {v0, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 249
    .end local v16    # "reader":Ljava/io/BufferedReader;
    .local v17, "reader":Ljava/io/BufferedReader;
    const/4 v13, 0x0

    .line 250
    .local v13, "line":Ljava/lang/String;
    :cond_1
    :goto_1
    :try_start_1
    invoke-virtual/range {v17 .. v17}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_3

    .line 251
    const-string v19, "="

    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 252
    .local v3, "code":[Ljava/lang/String;
    array-length v0, v3

    move/from16 v19, v0

    const/16 v20, 0x2

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_1

    .line 253
    const/16 v19, 0x0

    aget-object v10, v3, v19

    .line 254
    .local v10, "header":Ljava/lang/String;
    const/16 v19, 0x1

    aget-object v4, v3, v19

    .line 256
    .local v4, "data":Ljava/lang/String;
    invoke-virtual {v15, v10}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_2

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    .line 260
    .end local v3    # "code":[Ljava/lang/String;
    .end local v4    # "data":Ljava/lang/String;
    .end local v10    # "header":Ljava/lang/String;
    :catch_0
    move-exception v5

    move-object/from16 v16, v17

    .line 262
    .end local v6    # "file":Ljava/io/File;
    .end local v7    # "fr":Ljava/io/FileReader;
    .end local v13    # "line":Ljava/lang/String;
    .end local v17    # "reader":Ljava/io/BufferedReader;
    .local v5, "e":Ljava/io/FileNotFoundException;
    .restart local v16    # "reader":Ljava/io/BufferedReader;
    :goto_2
    :try_start_2
    invoke-virtual {v5}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 270
    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [Ljava/io/Closeable;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    aput-object v16, v19, v20

    invoke-static/range {v19 .. v19}, Lcom/policydm/adapter/XSPDAdapter;->closeQuietly([Ljava/io/Closeable;)V

    .line 273
    .end local v5    # "e":Ljava/io/FileNotFoundException;
    :goto_3
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 274
    .local v18, "ret":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[Ljava/lang/String;>;"
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v19

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 275
    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 276
    return-object v18

    .line 257
    .end local v16    # "reader":Ljava/io/BufferedReader;
    .end local v18    # "ret":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[Ljava/lang/String;>;"
    .restart local v3    # "code":[Ljava/lang/String;
    .restart local v4    # "data":Ljava/lang/String;
    .restart local v6    # "file":Ljava/io/File;
    .restart local v7    # "fr":Ljava/io/FileReader;
    .restart local v10    # "header":Ljava/lang/String;
    .restart local v13    # "line":Ljava/lang/String;
    .restart local v17    # "reader":Ljava/io/BufferedReader;
    :cond_2
    :try_start_3
    invoke-virtual {v15, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Integer;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v19

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v9, v19
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    .line 264
    .end local v3    # "code":[Ljava/lang/String;
    .end local v4    # "data":Ljava/lang/String;
    .end local v10    # "header":Ljava/lang/String;
    :catch_1
    move-exception v5

    move-object/from16 v16, v17

    .line 266
    .end local v6    # "file":Ljava/io/File;
    .end local v7    # "fr":Ljava/io/FileReader;
    .end local v13    # "line":Ljava/lang/String;
    .end local v17    # "reader":Ljava/io/BufferedReader;
    .local v5, "e":Ljava/io/IOException;
    .restart local v16    # "reader":Ljava/io/BufferedReader;
    :goto_4
    :try_start_4
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 270
    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [Ljava/io/Closeable;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    aput-object v16, v19, v20

    invoke-static/range {v19 .. v19}, Lcom/policydm/adapter/XSPDAdapter;->closeQuietly([Ljava/io/Closeable;)V

    goto :goto_3

    .end local v5    # "e":Ljava/io/IOException;
    .end local v16    # "reader":Ljava/io/BufferedReader;
    .restart local v6    # "file":Ljava/io/File;
    .restart local v7    # "fr":Ljava/io/FileReader;
    .restart local v13    # "line":Ljava/lang/String;
    .restart local v17    # "reader":Ljava/io/BufferedReader;
    :cond_3
    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [Ljava/io/Closeable;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    aput-object v17, v19, v20

    invoke-static/range {v19 .. v19}, Lcom/policydm/adapter/XSPDAdapter;->closeQuietly([Ljava/io/Closeable;)V

    move-object/from16 v16, v17

    .line 271
    .end local v17    # "reader":Ljava/io/BufferedReader;
    .restart local v16    # "reader":Ljava/io/BufferedReader;
    goto :goto_3

    .line 270
    .end local v6    # "file":Ljava/io/File;
    .end local v7    # "fr":Ljava/io/FileReader;
    .end local v13    # "line":Ljava/lang/String;
    :catchall_0
    move-exception v19

    :goto_5
    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/io/Closeable;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v16, v20, v21

    invoke-static/range {v20 .. v20}, Lcom/policydm/adapter/XSPDAdapter;->closeQuietly([Ljava/io/Closeable;)V

    throw v19

    .end local v16    # "reader":Ljava/io/BufferedReader;
    .restart local v6    # "file":Ljava/io/File;
    .restart local v7    # "fr":Ljava/io/FileReader;
    .restart local v13    # "line":Ljava/lang/String;
    .restart local v17    # "reader":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v19

    move-object/from16 v16, v17

    .end local v17    # "reader":Ljava/io/BufferedReader;
    .restart local v16    # "reader":Ljava/io/BufferedReader;
    goto :goto_5

    .line 264
    .end local v6    # "file":Ljava/io/File;
    .end local v7    # "fr":Ljava/io/FileReader;
    .end local v13    # "line":Ljava/lang/String;
    :catch_2
    move-exception v5

    goto :goto_4

    .line 260
    :catch_3
    move-exception v5

    goto/16 :goto_2
.end method

.method public static xspdGetSPotaPolicyVersion()Ljava/lang/String;
    .locals 9

    .prologue
    .line 156
    const/4 v5, 0x0

    .line 157
    .local v5, "policyVersion":Ljava/lang/String;
    const/4 v2, 0x0

    .line 160
    .local v2, "fr":Ljava/io/FileReader;
    :try_start_0
    new-instance v1, Ljava/io/File;

    const-string v7, "data/security/spota/sepolicy_version"

    invoke-direct {v1, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 161
    .local v1, "file":Ljava/io/File;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 163
    new-instance v3, Ljava/io/FileReader;

    invoke-direct {v3, v1}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 164
    .end local v2    # "fr":Ljava/io/FileReader;
    .local v3, "fr":Ljava/io/FileReader;
    :try_start_1
    new-instance v6, Ljava/io/BufferedReader;

    invoke-direct {v6, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 166
    .local v6, "reader":Ljava/io/BufferedReader;
    const/4 v4, 0x0

    .line 168
    .local v4, "line":Ljava/lang/String;
    :cond_0
    invoke-virtual {v6}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 170
    const-string v7, "VE="

    invoke-virtual {v4, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 172
    move-object v5, v4

    .line 173
    const-string v7, "VE="

    const-string v8, ""

    invoke-virtual {v5, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 177
    :cond_1
    invoke-virtual {v6}, Ljava/io/BufferedReader;->close()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    move-object v2, v3

    .line 190
    .end local v1    # "file":Ljava/io/File;
    .end local v3    # "fr":Ljava/io/FileReader;
    .end local v4    # "line":Ljava/lang/String;
    .end local v6    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "fr":Ljava/io/FileReader;
    :cond_2
    :goto_0
    if-eqz v2, :cond_3

    .line 192
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 200
    :cond_3
    :goto_1
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 202
    invoke-static {}, Lcom/policydm/adapter/XSPDAdapter;->xspdGetDefaultPolicyVersion()Ljava/lang/String;

    move-result-object v5

    .line 206
    :cond_4
    return-object v5

    .line 180
    :catch_0
    move-exception v0

    .line 182
    .local v0, "e":Ljava/io/FileNotFoundException;
    :goto_2
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 184
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v0

    .line 186
    .local v0, "e":Ljava/io/IOException;
    :goto_3
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 195
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 197
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 184
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "fr":Ljava/io/FileReader;
    .restart local v1    # "file":Ljava/io/File;
    .restart local v3    # "fr":Ljava/io/FileReader;
    :catch_3
    move-exception v0

    move-object v2, v3

    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v2    # "fr":Ljava/io/FileReader;
    goto :goto_3

    .line 180
    .end local v2    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    :catch_4
    move-exception v0

    move-object v2, v3

    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v2    # "fr":Ljava/io/FileReader;
    goto :goto_2
.end method

.method public static xspdGetSystemPolicyVersion()Ljava/lang/String;
    .locals 9

    .prologue
    .line 100
    const/4 v5, 0x0

    .line 101
    .local v5, "policyVersion":Ljava/lang/String;
    const/4 v2, 0x0

    .line 105
    .local v2, "fr":Ljava/io/FileReader;
    :try_start_0
    new-instance v1, Ljava/io/File;

    const-string v7, "/sepolicy_version"

    invoke-direct {v1, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 107
    .local v1, "file":Ljava/io/File;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 109
    new-instance v3, Ljava/io/FileReader;

    invoke-direct {v3, v1}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 110
    .end local v2    # "fr":Ljava/io/FileReader;
    .local v3, "fr":Ljava/io/FileReader;
    :try_start_1
    new-instance v6, Ljava/io/BufferedReader;

    invoke-direct {v6, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 112
    .local v6, "reader":Ljava/io/BufferedReader;
    const/4 v4, 0x0

    .line 114
    .local v4, "line":Ljava/lang/String;
    :cond_0
    invoke-virtual {v6}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 116
    const-string v7, "VE="

    invoke-virtual {v4, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 118
    move-object v5, v4

    .line 119
    const-string v7, "VE="

    const-string v8, ""

    invoke-virtual {v5, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 124
    :cond_1
    invoke-virtual {v6}, Ljava/io/BufferedReader;->close()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    move-object v2, v3

    .line 137
    .end local v1    # "file":Ljava/io/File;
    .end local v3    # "fr":Ljava/io/FileReader;
    .end local v4    # "line":Ljava/lang/String;
    .end local v6    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "fr":Ljava/io/FileReader;
    :cond_2
    :goto_0
    if-eqz v2, :cond_3

    .line 138
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 144
    :cond_3
    :goto_1
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 146
    invoke-static {}, Lcom/policydm/adapter/XSPDAdapter;->xspdGetDefaultPolicyVersion()Ljava/lang/String;

    move-result-object v5

    .line 150
    :cond_4
    return-object v5

    .line 127
    :catch_0
    move-exception v0

    .line 129
    .local v0, "e":Ljava/io/FileNotFoundException;
    :goto_2
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 131
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v0

    .line 133
    .local v0, "e":Ljava/io/IOException;
    :goto_3
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 140
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 142
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 131
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "fr":Ljava/io/FileReader;
    .restart local v1    # "file":Ljava/io/File;
    .restart local v3    # "fr":Ljava/io/FileReader;
    :catch_3
    move-exception v0

    move-object v2, v3

    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v2    # "fr":Ljava/io/FileReader;
    goto :goto_3

    .line 127
    .end local v2    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    :catch_4
    move-exception v0

    move-object v2, v3

    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v2    # "fr":Ljava/io/FileReader;
    goto :goto_2
.end method

.method public static xspdIsEnforcedMode()Z
    .locals 1

    .prologue
    .line 60
    invoke-static {}, Landroid/os/SELinux;->isSELinuxEnforced()Z

    move-result v0

    return v0
.end method

.method public static xspdIsEnterprisePolicy()Z
    .locals 4

    .prologue
    .line 65
    const/4 v0, 0x0

    .line 67
    .local v0, "bRet":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v2, Lcom/policydm/adapter/XSPDAdapter;->PolicyFileList:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_0

    .line 69
    const-string v2, "data/security"

    sget-object v3, Lcom/policydm/adapter/XSPDAdapter;->PolicyFileList:[Ljava/lang/String;

    aget-object v3, v3, v1

    invoke-static {v2, v3}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpFileExists(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 71
    const/4 v0, 0x1

    .line 75
    :cond_0
    return v0

    .line 67
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static xspdIsSuperKeyEnabled()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 600
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "elm_skey_activation"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 604
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private xspdVerifySignature([B[B[B)Z
    .locals 2
    .param p1, "dest"    # [B
    .param p2, "src"    # [B
    .param p3, "pubKey"    # [B

    .prologue
    const/4 v1, 0x0

    .line 586
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 594
    :cond_0
    :goto_0
    return v1

    .line 589
    :cond_1
    :try_start_0
    invoke-static {p1, p2, p3}, Lcom/policydm/adapter/XSecureKeyLoader;->verifySignature([B[B[B)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 590
    :catch_0
    move-exception v0

    .line 591
    .local v0, "t":Ljava/lang/Throwable;
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public xspdCopySEAndroidPolicy()Z
    .locals 5

    .prologue
    .line 543
    const/4 v0, 0x1

    .line 545
    .local v0, "bRet":Z
    const/4 v1, 0x0

    .local v1, "idx":I
    :goto_0
    sget-object v2, Lcom/policydm/adapter/XSPDAdapter;->PolicyFileList:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 547
    sget-object v2, Lcom/policydm/adapter/XDMTargetAdapter;->POLICY_DIR_PATH:Ljava/lang/String;

    const-string v3, "data/security/spota"

    sget-object v4, Lcom/policydm/adapter/XSPDAdapter;->PolicyFileList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-static {v2, v3, v4}, Lcom/policydm/db/XDBFile;->xdbFileCopy(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 549
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "policy file copy fail. idx="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 550
    const/4 v0, 0x0

    .line 554
    :cond_0
    if-eqz v0, :cond_1

    .line 556
    sget-object v2, Lcom/policydm/adapter/XDMTargetAdapter;->POLICY_DIR_PATH:Ljava/lang/String;

    const-string v3, "data/security/spota"

    const-string v4, "/sepolicy_version_sign"

    invoke-static {v2, v3, v4}, Lcom/policydm/db/XDBFile;->xdbFileCopy(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 558
    :cond_1
    return v0

    .line 545
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public xspdRemoveSEAndroidPolicy()Z
    .locals 6

    .prologue
    .line 563
    const/4 v0, 0x1

    .line 565
    .local v0, "bRet":Z
    const-string v1, ""

    .line 567
    .local v1, "filepath":Ljava/lang/String;
    invoke-static {}, Lcom/policydm/db/XDB;->xdbGetFileIdFirmwareData()I

    move-result v3

    .line 568
    .local v3, "nFUMOFileId":I
    invoke-static {v3}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpDeleteFile(I)I

    .line 570
    const/4 v2, 0x0

    .local v2, "idx":I
    :goto_0
    sget-object v4, Lcom/policydm/adapter/XSPDAdapter;->PolicyFileList:[Ljava/lang/String;

    array-length v4, v4

    if-ge v2, v4, :cond_0

    .line 572
    sget-object v1, Lcom/policydm/adapter/XDMTargetAdapter;->POLICY_DIR_PATH:Ljava/lang/String;

    .line 573
    sget-object v4, Lcom/policydm/adapter/XSPDAdapter;->PolicyFileList:[Ljava/lang/String;

    aget-object v4, v4, v2

    invoke-virtual {v1, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 574
    invoke-static {v1}, Lcom/policydm/db/XDBFile;->xdbFileRemove(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 576
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "policy file remove fail. idx="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 577
    const/4 v0, 0x0

    .line 581
    :cond_0
    return v0

    .line 570
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public xspdUnzipSEAndroidPolicy()Z
    .locals 7

    .prologue
    .line 405
    new-instance v4, Ljava/io/File;

    const-string v5, "data/data/com.policydm/SE_policy_file.zip"

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 406
    .local v4, "zipFile":Ljava/io/File;
    sget-object v3, Lcom/policydm/adapter/XDMTargetAdapter;->POLICY_DIR_PATH:Ljava/lang/String;

    .line 407
    .local v3, "targetDirPath":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 409
    .local v2, "targetDir":Ljava/io/File;
    const/4 v0, 0x0

    .line 410
    .local v0, "keydata":Ljava/lang/String;
    const-string v5, "4.5"

    const-string v6, "ro.build.version.release"

    invoke-static {v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-lez v5, :cond_0

    .line 414
    :try_start_0
    invoke-static {}, Lcom/policydm/adapter/XSecureKeyLoader;->getSPDkey()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 421
    :cond_0
    :goto_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "spdkey = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 422
    invoke-static {v4, v2, v0}, Lcom/policydm/db/XDBFile;->xdbFileUnzip(Ljava/io/File;Ljava/io/File;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 424
    const-string v5, "success unzip file"

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 425
    invoke-direct {p0}, Lcom/policydm/adapter/XSPDAdapter;->xspdCheckPolicyFiles()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 427
    const/4 v5, 0x1

    .line 430
    :goto_1
    return v5

    .line 416
    :catch_0
    move-exception v1

    .line 418
    .local v1, "t":Ljava/lang/Throwable;
    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    .line 430
    .end local v1    # "t":Ljava/lang/Throwable;
    :cond_1
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public xspdVerifySEAndroidPolicy()Z
    .locals 15

    .prologue
    .line 500
    invoke-static {}, Lcom/policydm/adapter/XSPDAdapter;->xspdGetSPotaFileHash()Ljava/util/ArrayList;

    move-result-object v10

    .line 503
    .local v10, "ret":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[Ljava/lang/String;>;"
    const/4 v12, 0x0

    invoke-virtual {v10, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/String;

    .line 506
    .local v6, "hashList":[Ljava/lang/String;
    const/4 v12, 0x1

    invoke-virtual {v10, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Ljava/lang/String;

    .line 509
    .local v7, "hashMethods":[Ljava/lang/String;
    const-string v12, "4.5"

    const-string v13, "ro.build.version.release"

    invoke-static {v13}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v12

    if-lez v12, :cond_0

    .line 510
    const/4 v12, 0x2

    new-array v7, v12, [Ljava/lang/String;

    .end local v7    # "hashMethods":[Ljava/lang/String;
    const/4 v12, 0x0

    sget-object v13, Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;->MD5:Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;

    invoke-virtual {v13}, Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;->name()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v7, v12

    const/4 v12, 0x1

    sget-object v13, Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;->NONE:Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;

    invoke-virtual {v13}, Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;->name()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v7, v12

    .line 512
    .restart local v7    # "hashMethods":[Ljava/lang/String;
    :cond_0
    move-object v0, v7

    .local v0, "arr$":[Ljava/lang/String;
    array-length v9, v0

    .local v9, "len$":I
    const/4 v8, 0x0

    .local v8, "i$":I
    :goto_0
    if-ge v8, v9, :cond_1

    aget-object v5, v0, v8

    .line 514
    .local v5, "hash":Ljava/lang/String;
    const/16 v12, 0x2d

    const/16 v13, 0x5f

    :try_start_0
    invoke-virtual {v5, v12, v13}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;->valueOf(Ljava/lang/String;)Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 512
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 515
    :catch_0
    move-exception v4

    .line 516
    .local v4, "e":Ljava/lang/IllegalArgumentException;
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Not support hashing method - "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 517
    const/4 v12, 0x0

    .line 538
    .end local v4    # "e":Ljava/lang/IllegalArgumentException;
    .end local v5    # "hash":Ljava/lang/String;
    :goto_1
    return v12

    .line 521
    :cond_1
    const/4 v2, 0x0

    .line 522
    .local v2, "bVarifySign":Z
    const/4 v1, 0x0

    .line 523
    .local v1, "bCompareHash":Z
    const/4 v11, 0x0

    .line 524
    .local v11, "src":[B
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v13, Lcom/policydm/adapter/XDMTargetAdapter;->POLICY_DIR_PATH:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "/sepolicy_version_sign"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/policydm/db/XDBFile;->xdbFileRead(Ljava/lang/String;)[B

    move-result-object v3

    .line 526
    .local v3, "dest":[B
    const/4 v12, 0x1

    aget-object v12, v7, v12

    sget-object v13, Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;->NONE:Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;

    invoke-virtual {v13}, Lcom/policydm/adapter/XSPDAdapter$HASH_METHODS;->name()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v12

    if-nez v12, :cond_3

    .line 527
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v13, Lcom/policydm/adapter/XDMTargetAdapter;->POLICY_DIR_PATH:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "/sepolicy_version"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/policydm/db/XDBFile;->xdbFileRead(Ljava/lang/String;)[B

    move-result-object v11

    .line 531
    :goto_2
    invoke-direct {p0}, Lcom/policydm/adapter/XSPDAdapter;->xspdGetPemPublicKey()[B

    move-result-object v12

    invoke-direct {p0, v3, v11, v12}, Lcom/policydm/adapter/XSPDAdapter;->xspdVerifySignature([B[B[B)Z

    move-result v2

    .line 532
    if-nez v2, :cond_4

    const-string v12, "invalid signature"

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 535
    :goto_3
    const/4 v12, 0x0

    aget-object v12, v7, v12

    invoke-static {v6, v12}, Lcom/policydm/adapter/XSPDAdapter;->xspdCompareHashValue([Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    .line 536
    if-nez v1, :cond_2

    const-string v12, "not equal hash list"

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 538
    :cond_2
    if-eqz v2, :cond_5

    if-eqz v1, :cond_5

    const/4 v12, 0x1

    goto :goto_1

    .line 529
    :cond_3
    sget-object v12, Lcom/policydm/adapter/XDMTargetAdapter;->POLICY_DIR_PATH:Ljava/lang/String;

    const-string v13, "/sepolicy_version"

    const/4 v14, 0x1

    aget-object v14, v7, v14

    invoke-static {v12, v13, v14}, Lcom/policydm/db/XDBFile;->xdbFileGetHash(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->getBytes()[B

    move-result-object v11

    goto :goto_2

    .line 533
    :cond_4
    const-string v12, "verified signature"

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_3

    .line 538
    :cond_5
    const/4 v12, 0x0

    goto :goto_1
.end method

.method public xspdVersionSEAndroidPolicy()Z
    .locals 12

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 435
    const/4 v5, 0x0

    .line 436
    .local v5, "policyVersion":Ljava/lang/String;
    const/4 v2, 0x0

    .line 439
    .local v2, "fr":Ljava/io/FileReader;
    :try_start_0
    new-instance v1, Ljava/io/File;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v11, Lcom/policydm/adapter/XDMTargetAdapter;->POLICY_DIR_PATH:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/sepolicy_version"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v1, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 440
    .local v1, "file":Ljava/io/File;
    new-instance v3, Ljava/io/FileReader;

    invoke-direct {v3, v1}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 441
    .end local v2    # "fr":Ljava/io/FileReader;
    .local v3, "fr":Ljava/io/FileReader;
    :try_start_1
    new-instance v6, Ljava/io/BufferedReader;

    invoke-direct {v6, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 443
    .local v6, "reader":Ljava/io/BufferedReader;
    const/4 v4, 0x0

    .line 445
    .local v4, "line":Ljava/lang/String;
    :cond_0
    invoke-virtual {v6}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 447
    const-string v10, "VE="

    invoke-virtual {v4, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 449
    move-object v5, v4

    .line 450
    const-string v10, "VE="

    const-string v11, ""

    invoke-virtual {v5, v10, v11}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 454
    :cond_1
    invoke-virtual {v6}, Ljava/io/BufferedReader;->close()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    move-object v2, v3

    .line 466
    .end local v1    # "file":Ljava/io/File;
    .end local v3    # "fr":Ljava/io/FileReader;
    .end local v4    # "line":Ljava/lang/String;
    .end local v6    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "fr":Ljava/io/FileReader;
    :goto_0
    if-eqz v2, :cond_2

    .line 467
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 473
    :cond_2
    :goto_1
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 475
    const-string v9, "sepolicy_version VE is empty"

    invoke-static {v9}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 495
    :goto_2
    return v8

    .line 456
    :catch_0
    move-exception v0

    .line 458
    .local v0, "e":Ljava/io/FileNotFoundException;
    :goto_3
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 460
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v0

    .line 462
    .local v0, "e":Ljava/io/IOException;
    :goto_4
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 469
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 471
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 479
    .end local v0    # "e":Ljava/io/IOException;
    :cond_3
    const-string v10, "_"

    invoke-virtual {v5, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 480
    .local v7, "sepolicy_version":[Ljava/lang/String;
    aget-object v10, v7, v8

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_4

    aget-object v10, v7, v8

    const-string v11, "SEPF"

    invoke-virtual {v10, v11}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v10

    if-eqz v10, :cond_5

    .line 482
    :cond_4
    const-string v9, "sepolicy_version[0] value is wrong"

    invoke-static {v9}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto :goto_2

    .line 485
    :cond_5
    aget-object v10, v7, v9

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_6

    aget-object v10, v7, v9

    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetModel()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v10

    if-eqz v10, :cond_7

    .line 487
    :cond_6
    const-string v9, "sepolicy_version[1] value is wrong"

    invoke-static {v9}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto :goto_2

    .line 490
    :cond_7
    const/4 v10, 0x2

    aget-object v10, v7, v10

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_8

    const/4 v10, 0x3

    aget-object v10, v7, v10

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 492
    :cond_8
    const-string v9, "sepolicy_version[2] or sepolicy_version[3] value is wrong"

    invoke-static {v9}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto :goto_2

    :cond_9
    move v8, v9

    .line 495
    goto :goto_2

    .line 460
    .end local v2    # "fr":Ljava/io/FileReader;
    .end local v7    # "sepolicy_version":[Ljava/lang/String;
    .restart local v1    # "file":Ljava/io/File;
    .restart local v3    # "fr":Ljava/io/FileReader;
    :catch_3
    move-exception v0

    move-object v2, v3

    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v2    # "fr":Ljava/io/FileReader;
    goto :goto_4

    .line 456
    .end local v2    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    :catch_4
    move-exception v0

    move-object v2, v3

    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v2    # "fr":Ljava/io/FileReader;
    goto :goto_3
.end method
