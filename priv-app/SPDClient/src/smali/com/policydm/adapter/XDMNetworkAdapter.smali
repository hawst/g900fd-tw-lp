.class public Lcom/policydm/adapter/XDMNetworkAdapter;
.super Ljava/lang/Object;
.source "XDMNetworkAdapter.java"


# static fields
.field private static final DEFAULT_NULL_ADDRESS:Ljava/lang/String; = "0.0.0.0"

.field private static final DEFAULT_PORT:Ljava/lang/String; = "8080"

.field private static final GPRS_APN1:Ljava/lang/String; = "epc.tmobile.com"

.field private static final NET_PROFILE_NAME1:Ljava/lang/String; = "Production"

.field private static final PROXY_ADDRESS1:Ljava/lang/String; = "0.0.0.0"

.field private static final PROXY_PORT1:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xdbAdpGetProxyData()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 53
    new-instance v2, Lcom/policydm/adapter/XDMTelephonyAdapter;

    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/policydm/adapter/XDMTelephonyAdapter;-><init>(Landroid/content/Context;)V

    .line 54
    .local v2, "netInfo":Lcom/policydm/adapter/XDMTelephonyAdapter;
    iget-object v3, v2, Lcom/policydm/adapter/XDMTelephonyAdapter;->napAddr:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;

    if-nez v3, :cond_0

    .line 55
    new-instance v3, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;

    invoke-direct {v3}, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;-><init>()V

    iput-object v3, v2, Lcom/policydm/adapter/XDMTelephonyAdapter;->napAddr:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;

    .line 56
    :cond_0
    iget-object v3, v2, Lcom/policydm/adapter/XDMTelephonyAdapter;->authInfo:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMAuthInfo;

    if-nez v3, :cond_1

    .line 57
    new-instance v3, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMAuthInfo;

    invoke-direct {v3}, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMAuthInfo;-><init>()V

    iput-object v3, v2, Lcom/policydm/adapter/XDMTelephonyAdapter;->authInfo:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMAuthInfo;

    .line 59
    :cond_1
    sget-boolean v3, Lcom/policydm/adapter/XDMFeature;->XDM_FEATURE_WIFI_ONLY_MODEL:Z

    if-nez v3, :cond_2

    .line 61
    invoke-static {v2}, Lcom/policydm/tp/XTPAdapter;->xtpAdpNetGetProfileData(Lcom/policydm/adapter/XDMTelephonyAdapter;)V

    .line 62
    invoke-static {}, Lcom/policydm/db/XDBProfileAdp;->xdbGetConRef()Lcom/policydm/db/XDBInfoConRef;

    move-result-object v0

    .line 66
    .local v0, "conref":Lcom/policydm/db/XDBInfoConRef;
    :try_start_0
    invoke-static {}, Lcom/policydm/adapter/XDMTelephonyAdapter;->xdmAgentAppGetExistApn()Z

    move-result v3

    if-nez v3, :cond_3

    .line 68
    invoke-static {}, Lcom/policydm/db/XDBSqlQuery;->xdmDbResetNetworkTable()V

    .line 91
    .end local v0    # "conref":Lcom/policydm/db/XDBInfoConRef;
    :cond_2
    :goto_0
    return-object v2

    .line 71
    .restart local v0    # "conref":Lcom/policydm/db/XDBInfoConRef;
    :cond_3
    invoke-static {}, Lcom/policydm/adapter/XDMTelephonyAdapter;->xdmAgentAppGetExistApn()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 73
    if-eqz v0, :cond_2

    .line 75
    iget-object v3, v0, Lcom/policydm/db/XDBInfoConRef;->NAP:Lcom/policydm/db/XDBConRefNAP;

    iget-object v4, v2, Lcom/policydm/adapter/XDMTelephonyAdapter;->napAddr:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;

    iget-object v4, v4, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;->m_szApn:Ljava/lang/String;

    iput-object v4, v3, Lcom/policydm/db/XDBConRefNAP;->NetworkProfileName:Ljava/lang/String;

    .line 76
    iget-object v3, v0, Lcom/policydm/db/XDBInfoConRef;->NAP:Lcom/policydm/db/XDBConRefNAP;

    iget-object v4, v2, Lcom/policydm/adapter/XDMTelephonyAdapter;->szAccountName:Ljava/lang/String;

    iput-object v4, v3, Lcom/policydm/db/XDBConRefNAP;->Addr:Ljava/lang/String;

    .line 77
    iget-object v3, v0, Lcom/policydm/db/XDBInfoConRef;->PX:Lcom/policydm/db/XDBConRefPX;

    iget-object v4, v2, Lcom/policydm/adapter/XDMTelephonyAdapter;->napAddr:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;

    iget-object v4, v4, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;->m_szPrimaryProxyAddr:Ljava/lang/String;

    iput-object v4, v3, Lcom/policydm/db/XDBConRefPX;->Addr:Ljava/lang/String;

    .line 78
    iget-object v3, v0, Lcom/policydm/db/XDBInfoConRef;->PX:Lcom/policydm/db/XDBConRefPX;

    iget-object v4, v2, Lcom/policydm/adapter/XDMTelephonyAdapter;->napAddr:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;

    iget v4, v4, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;->nPrimary_proxy_port:I

    iput v4, v3, Lcom/policydm/db/XDBConRefPX;->nPortNbr:I

    .line 79
    iget-object v3, v0, Lcom/policydm/db/XDBInfoConRef;->NAP:Lcom/policydm/db/XDBConRefNAP;

    iget-object v3, v3, Lcom/policydm/db/XDBConRefNAP;->Auth:Lcom/policydm/db/XDBConRefAuth;

    iget-object v4, v2, Lcom/policydm/adapter/XDMTelephonyAdapter;->authInfo:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMAuthInfo;

    iget-object v4, v4, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMAuthInfo;->szId:Ljava/lang/String;

    iput-object v4, v3, Lcom/policydm/db/XDBConRefAuth;->PAP_ID:Ljava/lang/String;

    .line 80
    iget-object v3, v0, Lcom/policydm/db/XDBInfoConRef;->NAP:Lcom/policydm/db/XDBConRefNAP;

    iget-object v3, v3, Lcom/policydm/db/XDBConRefNAP;->Auth:Lcom/policydm/db/XDBConRefAuth;

    iget-object v4, v2, Lcom/policydm/adapter/XDMTelephonyAdapter;->authInfo:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMAuthInfo;

    iget-object v4, v4, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMAuthInfo;->szPasswd:Ljava/lang/String;

    iput-object v4, v3, Lcom/policydm/db/XDBConRefAuth;->PAP_Secret:Ljava/lang/String;

    .line 82
    invoke-static {v0}, Lcom/policydm/db/XDBProfileAdp;->xdbSetConRef(Lcom/policydm/db/XDBInfoConRef;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 86
    :catch_0
    move-exception v1

    .line 88
    .local v1, "ex":Ljava/lang/NullPointerException;
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdbAdpInitNetProfile(Ljava/lang/Object;)V
    .locals 5
    .param p0, "NVMDMInfo"    # Ljava/lang/Object;

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x1

    .line 27
    move-object v0, p0

    check-cast v0, Lcom/policydm/db/XDBProfileInfo;

    .line 29
    .local v0, "pProfileInfo":Lcom/policydm/db/XDBProfileInfo;
    const-string v1, "DM Profile"

    iput-object v1, v0, Lcom/policydm/db/XDBProfileInfo;->NetworkConnName:Ljava/lang/String;

    .line 30
    iget-object v1, v0, Lcom/policydm/db/XDBProfileInfo;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    iput-boolean v3, v1, Lcom/policydm/db/XDBInfoConRef;->Active:Z

    .line 31
    iget-object v1, v0, Lcom/policydm/db/XDBProfileInfo;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    iget-object v1, v1, Lcom/policydm/db/XDBInfoConRef;->NAP:Lcom/policydm/db/XDBConRefNAP;

    const/16 v2, 0x11

    iput v2, v1, Lcom/policydm/db/XDBConRefNAP;->nBearer:I

    .line 32
    iget-object v1, v0, Lcom/policydm/db/XDBProfileInfo;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    iget-object v1, v1, Lcom/policydm/db/XDBInfoConRef;->NAP:Lcom/policydm/db/XDBConRefNAP;

    iput v4, v1, Lcom/policydm/db/XDBConRefNAP;->nAddrType:I

    .line 34
    iget-object v1, v0, Lcom/policydm/db/XDBProfileInfo;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    iget-object v1, v1, Lcom/policydm/db/XDBInfoConRef;->NAP:Lcom/policydm/db/XDBConRefNAP;

    const-string v2, "epc.tmobile.com"

    iput-object v2, v1, Lcom/policydm/db/XDBConRefNAP;->Addr:Ljava/lang/String;

    .line 35
    iget-object v1, v0, Lcom/policydm/db/XDBProfileInfo;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    iget-object v1, v1, Lcom/policydm/db/XDBInfoConRef;->PX:Lcom/policydm/db/XDBConRefPX;

    const/4 v2, 0x0

    iput v2, v1, Lcom/policydm/db/XDBConRefPX;->nPortNbr:I

    .line 36
    iget-object v1, v0, Lcom/policydm/db/XDBProfileInfo;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    iget-object v1, v1, Lcom/policydm/db/XDBInfoConRef;->PX:Lcom/policydm/db/XDBConRefPX;

    iput v3, v1, Lcom/policydm/db/XDBConRefPX;->nAddrType:I

    .line 37
    iget-object v1, v0, Lcom/policydm/db/XDBProfileInfo;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    iget-object v1, v1, Lcom/policydm/db/XDBInfoConRef;->PX:Lcom/policydm/db/XDBConRefPX;

    const-string v2, "0.0.0.0"

    iput-object v2, v1, Lcom/policydm/db/XDBConRefPX;->Addr:Ljava/lang/String;

    .line 38
    iget-object v1, v0, Lcom/policydm/db/XDBProfileInfo;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    iput v4, v1, Lcom/policydm/db/XDBInfoConRef;->nService:I

    .line 39
    iget-object v1, v0, Lcom/policydm/db/XDBProfileInfo;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    iput-boolean v3, v1, Lcom/policydm/db/XDBInfoConRef;->bProxyUse:Z

    .line 40
    iget-object v1, v0, Lcom/policydm/db/XDBProfileInfo;->ConRef:Lcom/policydm/db/XDBInfoConRef;

    iget-object v1, v1, Lcom/policydm/db/XDBInfoConRef;->NAP:Lcom/policydm/db/XDBConRefNAP;

    const-string v2, "Production"

    iput-object v2, v1, Lcom/policydm/db/XDBConRefNAP;->NetworkProfileName:Ljava/lang/String;

    .line 48
    return-void
.end method
