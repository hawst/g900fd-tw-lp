.class public Lcom/policydm/adapter/XDMTelephonyData;
.super Ljava/lang/Object;
.source "XDMTelephonyData.java"


# instance fields
.field public isNetworkRoaming:Z

.field public nSimState:I

.field public networkType:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-boolean v0, p0, Lcom/policydm/adapter/XDMTelephonyData;->isNetworkRoaming:Z

    .line 17
    iput v0, p0, Lcom/policydm/adapter/XDMTelephonyData;->networkType:I

    .line 18
    const/4 v0, -0x1

    iput v0, p0, Lcom/policydm/adapter/XDMTelephonyData;->nSimState:I

    .line 19
    return-void
.end method

.method public static xdmGetInstance(Landroid/telephony/TelephonyManager;Landroid/telephony/ServiceState;)Lcom/policydm/adapter/XDMTelephonyData;
    .locals 2
    .param p0, "telephonyManager"    # Landroid/telephony/TelephonyManager;
    .param p1, "serviceState"    # Landroid/telephony/ServiceState;

    .prologue
    .line 30
    if-nez p0, :cond_0

    .line 32
    const-string v1, "telephonyManager is null, return."

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 33
    const/4 v0, 0x0

    .line 47
    :goto_0
    return-object v0

    .line 36
    :cond_0
    new-instance v0, Lcom/policydm/adapter/XDMTelephonyData;

    invoke-direct {v0}, Lcom/policydm/adapter/XDMTelephonyData;-><init>()V

    .line 38
    .local v0, "wstelephony":Lcom/policydm/adapter/XDMTelephonyData;
    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v1

    iput v1, v0, Lcom/policydm/adapter/XDMTelephonyData;->nSimState:I

    .line 39
    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v1

    iput v1, v0, Lcom/policydm/adapter/XDMTelephonyData;->networkType:I

    .line 40
    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z

    move-result v1

    iput-boolean v1, v0, Lcom/policydm/adapter/XDMTelephonyData;->isNetworkRoaming:Z

    goto :goto_0
.end method
