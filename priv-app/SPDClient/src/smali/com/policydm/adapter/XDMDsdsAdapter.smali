.class public Lcom/policydm/adapter/XDMDsdsAdapter;
.super Ljava/lang/Object;
.source "XDMDsdsAdapter.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xdmDsdsGetTargetDevID()Ljava/lang/String;
    .locals 14

    .prologue
    .line 13
    const-string v9, ""

    .line 16
    .local v9, "szDeviceId":Ljava/lang/String;
    :try_start_0
    const-string v10, "try dsds"

    invoke-static {v10}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 17
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    .line 23
    .local v3, "cloader":Ljava/lang/ClassLoader;
    const-string v10, "com.android.plugin.PlugInServiceManager"

    invoke-virtual {v3, v10}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    .line 24
    .local v2, "cPlugInServiceManager":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v10, "com.android.plugin.dsds.PlugInDsdsService"

    invoke-virtual {v3, v10}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 26
    .local v1, "cPlugInDsdsService":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v10, "getService"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Class;

    const/4 v12, 0x0

    const-class v13, Ljava/lang/String;

    aput-object v13, v11, v12

    invoke-virtual {v2, v10, v11}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v7

    .line 27
    .local v7, "mGetService":Ljava/lang/reflect/Method;
    const-string v10, "getDeviceId"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Class;

    const/4 v12, 0x0

    sget-object v13, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v13, v11, v12

    invoke-virtual {v1, v10, v11}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v6

    .line 28
    .local v6, "mGetDeviceId":Ljava/lang/reflect/Method;
    const-string v10, "NAME"

    invoke-virtual {v1, v10}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v4

    .line 30
    .local v4, "fNAME":Ljava/lang/reflect/Field;
    const/4 v11, 0x0

    const/4 v10, 0x1

    new-array v12, v10, [Ljava/lang/Object;

    const/4 v13, 0x0

    const/4 v10, 0x0

    invoke-virtual {v4, v10}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    aput-object v10, v12, v13

    invoke-virtual {v7, v11, v12}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    .line 31
    .local v8, "objDsds":Ljava/lang/Object;
    if-eqz v8, :cond_0

    .line 32
    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v6, v8, v10}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    move-object v0, v10

    check-cast v0, Ljava/lang/String;

    move-object v9, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 38
    .end local v1    # "cPlugInDsdsService":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v2    # "cPlugInServiceManager":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v3    # "cloader":Ljava/lang/ClassLoader;
    .end local v4    # "fNAME":Ljava/lang/reflect/Field;
    .end local v6    # "mGetDeviceId":Ljava/lang/reflect/Method;
    .end local v7    # "mGetService":Ljava/lang/reflect/Method;
    .end local v8    # "objDsds":Ljava/lang/Object;
    :cond_0
    :goto_0
    return-object v9

    .line 34
    :catch_0
    move-exception v5

    .line 36
    .local v5, "ignore":Ljava/lang/Exception;
    const-string v10, "not dsds"

    invoke-static {v10}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto :goto_0
.end method
