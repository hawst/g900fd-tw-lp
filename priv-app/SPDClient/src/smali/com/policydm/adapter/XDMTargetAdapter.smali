.class public Lcom/policydm/adapter/XDMTargetAdapter;
.super Ljava/lang/Object;
.source "XDMTargetAdapter.java"

# interfaces
.implements Lcom/policydm/interfaces/XDMDefInterface;
.implements Lcom/policydm/interfaces/XDMInterface;


# static fields
.field public static DATA_DIR_PATH:Ljava/lang/String; = null

.field public static EXTERNAL_DIR_PATH:Ljava/lang/String; = null

.field public static EXTERNAL_SD_DIR_PATH:Ljava/lang/String; = null

.field private static final FILE_PATH_CSC_VERSION:Ljava/lang/String; = "system/CSCVersion.txt"

.field public static POLICY_DIR_PATH:Ljava/lang/String; = null

.field private static final PROP_BUILD_CSC_VERSION:Ljava/lang/String; = "ril.official_cscver"

.field private static final PROP_BUILD_HW_VERSION:Ljava/lang/String; = "ro.build.HW"

.field private static final PROP_BUILD_OS_VERSION:Ljava/lang/String; = "ro.build.version.release"

.field private static final PROP_BUILD_PDA_VERSION:Ljava/lang/String; = "ro.build.PDA"

.field private static final PROP_BUILD_PHONE_VERSION:Ljava/lang/String; = "ril.sw_ver"

.field private static final PROP_BUILD_SERIAL_NUMBER:Ljava/lang/String; = "ril.serialnumber"

.field private static final PROP_CSC_COUNTRYISO_CODE:Ljava/lang/String; = "ro.csc.countryiso_code"

.field private static final PROP_CSC_SALES_CODE:Ljava/lang/String; = "ro.csc.sales_code"

.field private static final PROP_CSC_SALES_CODE2:Ljava/lang/String; = "ril.sales_code"

.field private static final PROP_DEVICE_DECRYPTION_SUPPORT:Ljava/lang/String; = "ro.crypto.support"

.field private static final PROP_DEVICE_ENCRYPTION_STATE:Ljava/lang/String; = "ro.crypto.state"

.field private static final PROP_PERSIST_SYS_COUNTRY:Ljava/lang/String; = "persist.sys.country"

.field private static final PROP_PERSIST_SYS_LANGUAGE:Ljava/lang/String; = "persist.sys.language"

.field private static final PROP_PRODUCT_LOCALE_LANGUAGE:Ljava/lang/String; = "ro.product.locale.language"

.field private static final PROP_PRODUCT_LOCALE_REGION:Ljava/lang/String; = "ro.product.locale.region"

.field private static final PROP_PRODUCT_MANUFACTURER:Ljava/lang/String; = "ro.product.manufacturer"

.field private static final PROP_PRODUCT_MODEL:Ljava/lang/String; = "ro.product.model"

.field private static final PROP_PRODUCT_OEM:Ljava/lang/String; = "ro.product.OEM"

.field private static final PROP_SIM_OPERATOR_ALPHA:Ljava/lang/String; = "gsm.sim.operator.alpha"

.field private static final PROP_TWID:Ljava/lang/String; = "ro.serialno"

.field public static g_bExternalSDStorageAvailable:Z

.field public static g_bExternalStorageAvailable:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 57
    const-string v0, "data"

    sput-object v0, Lcom/policydm/adapter/XDMTargetAdapter;->DATA_DIR_PATH:Ljava/lang/String;

    .line 58
    sput-object v2, Lcom/policydm/adapter/XDMTargetAdapter;->EXTERNAL_DIR_PATH:Ljava/lang/String;

    .line 59
    sput-object v2, Lcom/policydm/adapter/XDMTargetAdapter;->EXTERNAL_SD_DIR_PATH:Ljava/lang/String;

    .line 60
    const-string v0, ""

    sput-object v0, Lcom/policydm/adapter/XDMTargetAdapter;->POLICY_DIR_PATH:Ljava/lang/String;

    .line 62
    sput-boolean v1, Lcom/policydm/adapter/XDMTargetAdapter;->g_bExternalStorageAvailable:Z

    .line 63
    sput-boolean v1, Lcom/policydm/adapter/XDMTargetAdapter;->g_bExternalSDStorageAvailable:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isHmodel()Z
    .locals 6

    .prologue
    .line 934
    const/16 v4, 0x8

    new-array v0, v4, [Ljava/lang/String;

    .line 935
    .local v0, "hList":[Ljava/lang/String;
    const/4 v2, 0x0

    .line 936
    .local v2, "isH":Z
    const-string v4, "ro.build.product"

    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    .line 938
    .local v3, "lowerProductName":Ljava/lang/String;
    const/4 v4, 0x0

    const-string v5, "hlte"

    aput-object v5, v0, v4

    .line 939
    const/4 v4, 0x1

    const-string v5, "ha3g"

    aput-object v5, v0, v4

    .line 940
    const/4 v4, 0x2

    const-string v5, "h3g"

    aput-object v5, v0, v4

    .line 941
    const/4 v4, 0x3

    const-string v5, "htdlte"

    aput-object v5, v0, v4

    .line 942
    const/4 v4, 0x4

    const-string v5, "hvolte"

    aput-object v5, v0, v4

    .line 943
    const/4 v4, 0x5

    const-string v5, "hllte"

    aput-object v5, v0, v4

    .line 944
    const/4 v4, 0x6

    const-string v5, "hl3g"

    aput-object v5, v0, v4

    .line 945
    const/4 v4, 0x7

    const-string v5, "frescolte"

    aput-object v5, v0, v4

    .line 947
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v4, v0

    if-ge v1, v4, :cond_1

    .line 949
    aget-object v4, v0, v1

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 951
    const/4 v2, 0x1

    .line 947
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 955
    :cond_1
    return v2
.end method

.method public static isJmodel()Z
    .locals 6

    .prologue
    .line 960
    const/16 v4, 0xb

    new-array v2, v4, [Ljava/lang/String;

    .line 961
    .local v2, "jList":[Ljava/lang/String;
    const/4 v1, 0x0

    .line 962
    .local v1, "isJ":Z
    const-string v4, "ro.build.product"

    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    .line 964
    .local v3, "lowerProductName":Ljava/lang/String;
    const/4 v4, 0x0

    const-string v5, "ja3g"

    aput-object v5, v2, v4

    .line 965
    const/4 v4, 0x1

    const-string v5, "jalte"

    aput-object v5, v2, v4

    .line 966
    const/4 v4, 0x2

    const-string v5, "jflte"

    aput-object v5, v2, v4

    .line 967
    const/4 v4, 0x3

    const-string v5, "jftdd"

    aput-object v5, v2, v4

    .line 968
    const/4 v4, 0x4

    const-string v5, "jfve3g"

    aput-object v5, v2, v4

    .line 969
    const/4 v4, 0x5

    const-string v5, "jfvelte"

    aput-object v5, v2, v4

    .line 970
    const/4 v4, 0x6

    const-string v5, "jfvolte"

    aput-object v5, v2, v4

    .line 971
    const/4 v4, 0x7

    const-string v5, "jfwifi"

    aput-object v5, v2, v4

    .line 972
    const/16 v4, 0x8

    const-string v5, "jactivelte"

    aput-object v5, v2, v4

    .line 973
    const/16 v4, 0x9

    const-string v5, "jf3g"

    aput-object v5, v2, v4

    .line 974
    const/16 v4, 0xa

    const-string v5, "ks01"

    aput-object v5, v2, v4

    .line 976
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, v2

    if-ge v0, v4, :cond_1

    .line 978
    aget-object v4, v2, v0

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 980
    const/4 v1, 0x1

    .line 976
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 984
    :cond_1
    return v1
.end method

.method public static isKmodel()Z
    .locals 6

    .prologue
    .line 909
    const/4 v4, 0x7

    new-array v2, v4, [Ljava/lang/String;

    .line 910
    .local v2, "kList":[Ljava/lang/String;
    const/4 v1, 0x0

    .line 911
    .local v1, "isK":Z
    const-string v4, "ro.build.product"

    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v4, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    .line 913
    .local v3, "lowerProductName":Ljava/lang/String;
    const/4 v4, 0x0

    const-string v5, "k3g"

    aput-object v5, v2, v4

    .line 914
    const/4 v4, 0x1

    const-string v5, "kactivelte"

    aput-object v5, v2, v4

    .line 915
    const/4 v4, 0x2

    const-string v5, "kalte"

    aput-object v5, v2, v4

    .line 916
    const/4 v4, 0x3

    const-string v5, "klte"

    aput-object v5, v2, v4

    .line 917
    const/4 v4, 0x4

    const-string v5, "kminilte"

    aput-object v5, v2, v4

    .line 918
    const/4 v4, 0x5

    const-string v5, "kq3g"

    aput-object v5, v2, v4

    .line 919
    const/4 v4, 0x6

    const-string v5, "kqlte"

    aput-object v5, v2, v4

    .line 921
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, v2

    if-ge v0, v4, :cond_1

    .line 923
    aget-object v4, v2, v0

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 925
    const/4 v1, 0x1

    .line 921
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 929
    :cond_1
    return v1
.end method

.method public static xdmCheckCHNProduct()Z
    .locals 3

    .prologue
    .line 784
    const/4 v0, 0x0

    .line 785
    .local v0, "bRet":Z
    const-string v2, "ro.csc.country_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 786
    .local v1, "country_code":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 788
    const-string v2, "CHINA"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "China"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "china"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 790
    :cond_0
    const/4 v0, 0x1

    .line 793
    :cond_1
    return v0
.end method

.method public static xdmCheckNAProduct()Z
    .locals 3

    .prologue
    .line 770
    const/4 v0, 0x0

    .line 771
    .local v0, "bRet":Z
    const-string v2, "ro.csc.country_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 772
    .local v1, "country_code":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 774
    const-string v2, "USA"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 776
    const/4 v0, 0x1

    .line 779
    :cond_0
    return v0
.end method

.method public static xdmEulaModel()Z
    .locals 3

    .prologue
    .line 835
    const-string v0, "VZW/ATT/AIO"

    .line 837
    .local v0, "SALESCODE_EULA_LIST":Ljava/lang/String;
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetCustomCode()Ljava/lang/String;

    move-result-object v1

    .line 838
    .local v1, "salesCode":Ljava/lang/String;
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 839
    const/4 v2, 0x1

    .line 841
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static xdmGetAvailableMemorySize(I)J
    .locals 8
    .param p0, "nMemoryArea"    # I

    .prologue
    .line 604
    const-string v5, ""

    .line 606
    .local v5, "szPath":Ljava/lang/String;
    invoke-static {p0}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetStoragePath(I)Ljava/lang/String;

    move-result-object v5

    .line 607
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 608
    const-wide/16 v6, 0x0

    .line 613
    :goto_0
    return-wide v6

    .line 610
    :cond_0
    new-instance v4, Landroid/os/StatFs;

    invoke-direct {v4, v5}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 611
    .local v4, "stat":Landroid/os/StatFs;
    invoke-virtual {v4}, Landroid/os/StatFs;->getBlockSize()I

    move-result v6

    int-to-long v2, v6

    .line 612
    .local v2, "blockSize":J
    invoke-virtual {v4}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v6

    int-to-long v0, v6

    .line 613
    .local v0, "availableBlocks":J
    mul-long v6, v0, v2

    goto :goto_0
.end method

.method private static xdmGetCharToHex(C)C
    .locals 1
    .param p0, "ch"    # C

    .prologue
    .line 464
    const/16 v0, 0x30

    if-lt p0, v0, :cond_0

    const/16 v0, 0x39

    if-gt p0, v0, :cond_0

    .line 465
    add-int/lit8 v0, p0, -0x30

    int-to-char v0, v0

    .line 471
    :goto_0
    return v0

    .line 466
    :cond_0
    const/16 v0, 0x41

    if-lt p0, v0, :cond_1

    const/16 v0, 0x46

    if-gt p0, v0, :cond_1

    .line 467
    add-int/lit8 v0, p0, -0x41

    add-int/lit8 v0, v0, 0xa

    int-to-char v0, v0

    goto :goto_0

    .line 468
    :cond_1
    const/16 v0, 0x61

    if-lt p0, v0, :cond_2

    const/16 v0, 0x66

    if-gt p0, v0, :cond_2

    .line 469
    add-int/lit8 v0, p0, -0x61

    add-int/lit8 v0, v0, 0xa

    int-to-char v0, v0

    goto :goto_0

    .line 471
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static xdmGetCheckWifiOnlyModel()Z
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 706
    const/4 v0, 0x0

    .line 709
    .local v0, "bRet":Z
    :try_start_0
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v5

    const-string v6, "connectivity"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 710
    .local v1, "cm":Landroid/net/ConnectivityManager;
    if-nez v1, :cond_0

    .line 712
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    const/16 v5, 0xa

    if-ge v3, v5, :cond_0

    .line 716
    const-wide/16 v5, 0x3e8

    :try_start_1
    invoke-static {v5, v6}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 723
    :goto_1
    :try_start_2
    const-string v5, "connectivity is null, retry..."

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 724
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v5

    const-string v6, "connectivity"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "cm":Landroid/net/ConnectivityManager;
    check-cast v1, Landroid/net/ConnectivityManager;

    .line 725
    .restart local v1    # "cm":Landroid/net/ConnectivityManager;
    if-eqz v1, :cond_2

    .line 730
    .end local v3    # "i":I
    :cond_0
    if-eqz v1, :cond_1

    .line 732
    const/4 v5, 0x0

    invoke-virtual {v1, v5}, Landroid/net/ConnectivityManager;->isNetworkSupported(I)Z

    move-result v5

    if-nez v5, :cond_3

    .line 733
    const/4 v0, 0x1

    .line 738
    :cond_1
    :goto_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "isWifiOnly : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 744
    .end local v0    # "bRet":Z
    .end local v1    # "cm":Landroid/net/ConnectivityManager;
    :goto_3
    return v0

    .line 718
    .restart local v0    # "bRet":Z
    .restart local v1    # "cm":Landroid/net/ConnectivityManager;
    .restart local v3    # "i":I
    :catch_0
    move-exception v2

    .line 720
    .local v2, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 741
    .end local v1    # "cm":Landroid/net/ConnectivityManager;
    .end local v2    # "e":Ljava/lang/InterruptedException;
    .end local v3    # "i":I
    :catch_1
    move-exception v2

    .line 743
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move v0, v4

    .line 744
    goto :goto_3

    .line 712
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v1    # "cm":Landroid/net/ConnectivityManager;
    .restart local v3    # "i":I
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 735
    .end local v3    # "i":I
    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public static xdmGetDeviceDecryptionSupport()Z
    .locals 4

    .prologue
    .line 750
    const/4 v0, 0x0

    .line 752
    .local v0, "bRet":Z
    const-string v2, ""

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 753
    const-string v2, "ro.crypto.support"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 754
    .local v1, "cryptoSupport":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 756
    const-string v2, "CryptoSupport is Empty"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 757
    const/4 v0, 0x0

    .line 765
    .end local v0    # "bRet":Z
    :goto_0
    return v0

    .line 760
    .restart local v0    # "bRet":Z
    :cond_0
    const-string v2, "recovery_mount"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 762
    const/4 v0, 0x1

    .line 764
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DecryptionSupport Ret : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdmGetExternalMemoryAvailable()Z
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 542
    const-string v4, ""

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 544
    sget-boolean v4, Lcom/policydm/adapter/XDMTargetAdapter;->g_bExternalStorageAvailable:Z

    if-nez v4, :cond_2

    .line 546
    const-string v4, "storage"

    invoke-static {v4}, Lcom/policydm/XDMApplication;->xdmGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/storage/StorageManager;

    .line 547
    .local v1, "mStorageManager":Landroid/os/storage/StorageManager;
    if-nez v1, :cond_1

    .line 549
    const-string v4, "mStorageManager is null!!"

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 570
    :cond_0
    :goto_0
    return v3

    .line 553
    :cond_1
    const/4 v4, 0x2

    invoke-static {v4}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetStoragePath(I)Ljava/lang/String;

    move-result-object v2

    .line 554
    .local v2, "szPath":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 559
    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "External memory State : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1, v2}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 560
    invoke-virtual {v1, v2}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "mounted"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    sput-boolean v3, Lcom/policydm/adapter/XDMTargetAdapter;->g_bExternalStorageAvailable:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 569
    :cond_2
    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bExternalStorageAvailable ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-boolean v4, Lcom/policydm/adapter/XDMTargetAdapter;->g_bExternalStorageAvailable:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 570
    sget-boolean v3, Lcom/policydm/adapter/XDMTargetAdapter;->g_bExternalStorageAvailable:Z

    goto :goto_0

    .line 562
    :catch_0
    move-exception v0

    .line 564
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static xdmGetNetPinFormIMSI()Ljava/lang/String;
    .locals 13

    .prologue
    const/4 v10, 0x0

    .line 410
    const-string v7, ""

    .line 411
    .local v7, "szIMSI":Ljava/lang/String;
    const-string v9, ""

    .line 412
    .local v9, "szPadIMSI":Ljava/lang/String;
    const/4 v3, 0x0

    .line 413
    .local v3, "nPadIMSILen":I
    const/4 v2, 0x0

    .line 415
    .local v2, "i":I
    const/4 v6, 0x0

    .line 416
    .local v6, "pNetPin":[C
    const/4 v8, 0x0

    .line 418
    .local v8, "szNetPin":Ljava/lang/String;
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetIMSIFromSIM()Ljava/lang/String;

    move-result-object v7

    .line 419
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 459
    :cond_0
    :goto_0
    return-object v10

    .line 425
    :cond_1
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v3

    .line 426
    rem-int/lit8 v11, v3, 0x2

    if-eqz v11, :cond_2

    .line 428
    add-int/lit8 v3, v3, 0x1

    .line 429
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "9"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 437
    :goto_1
    div-int/lit8 v11, v3, 0x2

    new-array v6, v11, [C

    .line 438
    if-eqz v6, :cond_0

    .line 444
    const/4 v2, 0x0

    :goto_2
    div-int/lit8 v10, v3, 0x2

    if-ge v2, v10, :cond_3

    .line 446
    mul-int/lit8 v10, v2, 0x2

    add-int/lit8 v10, v10, 0x1

    invoke-virtual {v9, v10}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 447
    .local v4, "nTemp1":C
    mul-int/lit8 v10, v2, 0x2

    invoke-virtual {v9, v10}, Ljava/lang/String;->charAt(I)C

    move-result v5

    .line 449
    .local v5, "nTemp2":C
    invoke-static {v4}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetCharToHex(C)C

    move-result v10

    shl-int/lit8 v10, v10, 0x4

    int-to-char v0, v10

    .line 450
    .local v0, "a":C
    invoke-static {v5}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetCharToHex(C)C

    move-result v1

    .line 451
    .local v1, "b":C
    or-int v10, v0, v1

    int-to-char v10, v10

    aput-char v10, v6, v2

    .line 454
    const/4 v4, 0x0

    .line 455
    const/4 v5, 0x0

    .line 444
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 433
    .end local v0    # "a":C
    .end local v1    # "b":C
    .end local v4    # "nTemp1":C
    .end local v5    # "nTemp2":C
    :cond_2
    add-int/lit8 v3, v3, 0x2

    .line 434
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "1"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "F"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    goto :goto_1

    .line 458
    :cond_3
    invoke-static {v6}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v8

    move-object v10, v8

    .line 459
    goto :goto_0
.end method

.method private static xdmGetSDCardMemoryPath(I)Ljava/lang/String;
    .locals 7
    .param p0, "nMemoryArea"    # I

    .prologue
    .line 575
    const-string v3, ""

    .line 577
    .local v3, "szPath":Ljava/lang/String;
    const-string v5, "storage"

    invoke-static {v5}, Lcom/policydm/XDMApplication;->xdmGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/storage/StorageManager;

    .line 578
    .local v1, "mStorageManager":Landroid/os/storage/StorageManager;
    if-nez v1, :cond_0

    .line 580
    const-string v5, "mStorageManager is null!!"

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move-object v4, v3

    .line 599
    .end local v3    # "szPath":Ljava/lang/String;
    .local v4, "szPath":Ljava/lang/String;
    :goto_0
    return-object v4

    .line 587
    .end local v4    # "szPath":Ljava/lang/String;
    .restart local v3    # "szPath":Ljava/lang/String;
    :cond_0
    :try_start_0
    invoke-virtual {v1}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    move-result-object v2

    .line 588
    .local v2, "mStorageVolumes":[Landroid/os/storage/StorageVolume;
    const/4 v5, 0x2

    if-ne p0, v5, :cond_2

    .line 589
    const/4 v5, 0x0

    aget-object v5, v2, v5

    invoke-virtual {v5}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 598
    .end local v2    # "mStorageVolumes":[Landroid/os/storage/StorageVolume;
    :cond_1
    :goto_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SD Card memory Path : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    move-object v4, v3

    .line 599
    .end local v3    # "szPath":Ljava/lang/String;
    .restart local v4    # "szPath":Ljava/lang/String;
    goto :goto_0

    .line 590
    .end local v4    # "szPath":Ljava/lang/String;
    .restart local v2    # "mStorageVolumes":[Landroid/os/storage/StorageVolume;
    .restart local v3    # "szPath":Ljava/lang/String;
    :cond_2
    const/4 v5, 0x3

    if-ne p0, v5, :cond_1

    .line 591
    const/4 v5, 0x1

    :try_start_1
    aget-object v5, v2, v5

    invoke-virtual {v5}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v3

    goto :goto_1

    .line 593
    .end local v2    # "mStorageVolumes":[Landroid/os/storage/StorageVolume;
    :catch_0
    move-exception v0

    .line 595
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private static xdmGetStoragePath(I)Ljava/lang/String;
    .locals 4
    .param p0, "nMemoryArea"    # I

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x2

    .line 632
    const-string v0, ""

    .line 634
    .local v0, "szPath":Ljava/lang/String;
    const/4 v1, 0x1

    if-ne p0, v1, :cond_2

    .line 636
    sget-object v0, Lcom/policydm/adapter/XDMTargetAdapter;->DATA_DIR_PATH:Ljava/lang/String;

    .line 653
    :cond_0
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 655
    const-string v1, "getStoragePath is empty"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 657
    :cond_1
    return-object v0

    .line 638
    :cond_2
    if-ne p0, v2, :cond_4

    .line 640
    sget-object v1, Lcom/policydm/adapter/XDMTargetAdapter;->EXTERNAL_DIR_PATH:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 641
    invoke-static {v2}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetSDCardMemoryPath(I)Ljava/lang/String;

    move-result-object v0

    .end local v0    # "szPath":Ljava/lang/String;
    sput-object v0, Lcom/policydm/adapter/XDMTargetAdapter;->EXTERNAL_DIR_PATH:Ljava/lang/String;

    .restart local v0    # "szPath":Ljava/lang/String;
    goto :goto_0

    .line 643
    :cond_3
    sget-object v0, Lcom/policydm/adapter/XDMTargetAdapter;->EXTERNAL_DIR_PATH:Ljava/lang/String;

    goto :goto_0

    .line 645
    :cond_4
    if-ne p0, v3, :cond_0

    .line 647
    sget-object v1, Lcom/policydm/adapter/XDMTargetAdapter;->EXTERNAL_SD_DIR_PATH:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 648
    invoke-static {v3}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetSDCardMemoryPath(I)Ljava/lang/String;

    move-result-object v0

    .end local v0    # "szPath":Ljava/lang/String;
    sput-object v0, Lcom/policydm/adapter/XDMTargetAdapter;->EXTERNAL_SD_DIR_PATH:Ljava/lang/String;

    .restart local v0    # "szPath":Ljava/lang/String;
    goto :goto_0

    .line 650
    :cond_5
    sget-object v0, Lcom/policydm/adapter/XDMTargetAdapter;->EXTERNAL_SD_DIR_PATH:Ljava/lang/String;

    goto :goto_0
.end method

.method public static xdmGetSystemCscFile()Ljava/lang/String;
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    .line 662
    const/4 v2, 0x0

    .line 663
    .local v2, "f":Ljava/io/RandomAccessFile;
    new-instance v4, Ljava/io/File;

    const-string v8, "system/CSCVersion.txt"

    invoke-direct {v4, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 664
    .local v4, "file":Ljava/io/File;
    const/4 v0, 0x0

    .line 665
    .local v0, "buffer":[B
    const-string v6, ""

    .line 669
    .local v6, "szRet":Ljava/lang/String;
    const/4 v5, 0x0

    .line 670
    .local v5, "nRead":I
    :try_start_0
    new-instance v3, Ljava/io/RandomAccessFile;

    const-string v8, "r"

    invoke-direct {v3, v4, v8}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 672
    .end local v2    # "f":Ljava/io/RandomAccessFile;
    .local v3, "f":Ljava/io/RandomAccessFile;
    :try_start_1
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v8

    cmp-long v8, v8, v10

    if-lez v8, :cond_0

    .line 674
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v8

    long-to-int v8, v8

    add-int/lit8 v8, v8, -0x1

    new-array v0, v8, [B

    .line 675
    const-wide/16 v8, 0x0

    invoke-virtual {v3, v8, v9}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 677
    invoke-virtual {v3, v0}, Ljava/io/RandomAccessFile;->read([B)I

    move-result v5

    .line 678
    if-lez v5, :cond_0

    .line 680
    new-instance v7, Ljava/lang/String;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v8

    invoke-direct {v7, v0, v8}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .end local v6    # "szRet":Ljava/lang/String;
    .local v7, "szRet":Ljava/lang/String;
    move-object v6, v7

    .line 692
    .end local v7    # "szRet":Ljava/lang/String;
    .restart local v6    # "szRet":Ljava/lang/String;
    :cond_0
    if-eqz v3, :cond_1

    .line 693
    :try_start_2
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_1
    move-object v2, v3

    .line 701
    .end local v3    # "f":Ljava/io/RandomAccessFile;
    .restart local v2    # "f":Ljava/io/RandomAccessFile;
    :cond_2
    :goto_0
    return-object v6

    .line 695
    .end local v2    # "f":Ljava/io/RandomAccessFile;
    .restart local v3    # "f":Ljava/io/RandomAccessFile;
    :catch_0
    move-exception v1

    .line 697
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move-object v2, v3

    .line 699
    .end local v3    # "f":Ljava/io/RandomAccessFile;
    .restart local v2    # "f":Ljava/io/RandomAccessFile;
    goto :goto_0

    .line 684
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 686
    .local v1, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_3
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 692
    if-eqz v2, :cond_2

    .line 693
    :try_start_4
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 695
    :catch_2
    move-exception v1

    .line 697
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 690
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    .line 692
    :goto_2
    if-eqz v2, :cond_3

    .line 693
    :try_start_5
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 698
    :cond_3
    :goto_3
    throw v8

    .line 695
    :catch_3
    move-exception v1

    .line 697
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_3

    .line 690
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "f":Ljava/io/RandomAccessFile;
    .restart local v3    # "f":Ljava/io/RandomAccessFile;
    :catchall_1
    move-exception v8

    move-object v2, v3

    .end local v3    # "f":Ljava/io/RandomAccessFile;
    .restart local v2    # "f":Ljava/io/RandomAccessFile;
    goto :goto_2

    .line 684
    .end local v2    # "f":Ljava/io/RandomAccessFile;
    .restart local v3    # "f":Ljava/io/RandomAccessFile;
    :catch_4
    move-exception v1

    move-object v2, v3

    .end local v3    # "f":Ljava/io/RandomAccessFile;
    .restart local v2    # "f":Ljava/io/RandomAccessFile;
    goto :goto_1
.end method

.method public static xdmGetTargetCountry()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 161
    const-string v1, "ro.csc.countryiso_code"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 162
    .local v0, "szCountry":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 164
    const-string v1, "persist.sys.country"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 165
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 167
    const-string v1, "ro.product.locale.region"

    const-string v2, "US"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 170
    :cond_0
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static xdmGetTargetCscV()Ljava/lang/String;
    .locals 3

    .prologue
    .line 288
    const-string v1, "ril.official_cscver"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 289
    .local v0, "szCsc":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "read csc: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 290
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 292
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetSystemCscFile()Ljava/lang/String;

    move-result-object v0

    .line 293
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "read file csc: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 295
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 297
    const-string v0, ""

    .line 298
    const-string v1, "csc file is Unknown"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 301
    :cond_0
    return-object v0
.end method

.method public static xdmGetTargetCustomCode()Ljava/lang/String;
    .locals 3

    .prologue
    .line 328
    const-string v1, "ro.csc.sales_code"

    const-string v2, "none"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 329
    .local v0, "szSalesCode":Ljava/lang/String;
    const-string v1, "none"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 330
    const-string v1, "ril.sales_code"

    const-string v2, "XEU"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 332
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getSalesCode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 333
    return-object v0
.end method

.method public static xdmGetTargetDevID()Ljava/lang/String;
    .locals 5

    .prologue
    .line 67
    const-string v1, ""

    .line 68
    .local v1, "szDeviceId":Ljava/lang/String;
    invoke-static {}, Lcom/policydm/adapter/XDMDsdsAdapter;->xdmDsdsGetTargetDevID()Ljava/lang/String;

    move-result-object v1

    .line 70
    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 72
    const/4 v2, 0x0

    .line 75
    .local v2, "tm":Landroid/telephony/TelephonyManager;
    :try_start_0
    const-string v3, "try duos"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 76
    invoke-static {}, Landroid/telephony/TelephonyManager;->getFirst()Landroid/telephony/TelephonyManager;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 83
    :goto_0
    if-nez v2, :cond_0

    .line 84
    const-string v3, "phone"

    invoke-static {v3}, Lcom/policydm/XDMApplication;->xdmGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "tm":Landroid/telephony/TelephonyManager;
    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 86
    .restart local v2    # "tm":Landroid/telephony/TelephonyManager;
    :cond_0
    if-eqz v2, :cond_3

    .line 88
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    .line 89
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_4

    .line 90
    :cond_1
    const-string v1, ""

    .line 101
    :cond_2
    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getDeviceID : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 104
    .end local v2    # "tm":Landroid/telephony/TelephonyManager;
    :cond_3
    return-object v1

    .line 78
    .restart local v2    # "tm":Landroid/telephony/TelephonyManager;
    :catch_0
    move-exception v0

    .line 80
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "not duos"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto :goto_0

    .line 93
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_4
    invoke-static {}, Lcom/policydm/adapter/XDMDevinfAdapter;->xdmMakePreID()Ljava/lang/String;

    move-result-object v3

    const-string v4, "MEID"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 95
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0xf

    if-lt v3, v4, :cond_2

    .line 97
    const/4 v3, 0x0

    const/16 v4, 0xe

    invoke-static {v1, v3, v4}, Landroid/text/TextUtils;->substring(Ljava/lang/CharSequence;II)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method public static xdmGetTargetDeviceMcc()Ljava/lang/String;
    .locals 5

    .prologue
    .line 204
    const-string v0, ""

    .line 205
    .local v0, "mMobileCountryCode":Ljava/lang/String;
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetCountry()Ljava/lang/String;

    move-result-object v0

    .line 206
    sget-boolean v3, Lcom/policydm/adapter/XDMFeature;->XDM_FEATURE_WIFI_ONLY_MODEL:Z

    if-nez v3, :cond_1

    .line 208
    const-string v3, "phone"

    invoke-static {v3}, Lcom/policydm/XDMApplication;->xdmGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 209
    .local v2, "tm":Landroid/telephony/TelephonyManager;
    if-nez v2, :cond_0

    .line 220
    .end local v2    # "tm":Landroid/telephony/TelephonyManager;
    :goto_0
    return-object v0

    .line 213
    .restart local v2    # "tm":Landroid/telephony/TelephonyManager;
    :cond_0
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v1

    .line 214
    .local v1, "operator":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 216
    const/4 v3, 0x0

    const/4 v4, 0x3

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 219
    .end local v1    # "operator":Ljava/lang/String;
    .end local v2    # "tm":Landroid/telephony/TelephonyManager;
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Device MobileCountryCode: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdmGetTargetDeviceMnc()Ljava/lang/String;
    .locals 5

    .prologue
    .line 244
    const-string v0, ""

    .line 246
    .local v0, "mMobileNetworkCode":Ljava/lang/String;
    sget-boolean v3, Lcom/policydm/adapter/XDMFeature;->XDM_FEATURE_WIFI_ONLY_MODEL:Z

    if-eqz v3, :cond_1

    .line 258
    :cond_0
    :goto_0
    return-object v0

    .line 249
    :cond_1
    const-string v3, "phone"

    invoke-static {v3}, Lcom/policydm/XDMApplication;->xdmGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 250
    .local v2, "tm":Landroid/telephony/TelephonyManager;
    if-eqz v2, :cond_0

    .line 252
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v1

    .line 253
    .local v1, "operator":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 255
    const/4 v3, 0x3

    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 256
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Device MobileNetworkCode: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdmGetTargetDeviceSerialNum()Ljava/lang/String;
    .locals 3

    .prologue
    .line 320
    const-string v1, "ril.serialnumber"

    const-string v2, ""

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 322
    .local v0, "szSerialNum":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DeviceSerialNum = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 323
    return-object v0
.end method

.method public static xdmGetTargetFwV()Ljava/lang/String;
    .locals 1

    .prologue
    .line 283
    const-string v0, "ro.build.PDA"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static xdmGetTargetHwV()Ljava/lang/String;
    .locals 1

    .prologue
    .line 278
    const-string v0, "ro.build.HW"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static xdmGetTargetIMSIFromSIM()Ljava/lang/String;
    .locals 3

    .prologue
    .line 378
    const-string v0, ""

    .line 379
    .local v0, "szBuff":Ljava/lang/String;
    const-string v2, "phone"

    invoke-static {v2}, Lcom/policydm/XDMApplication;->xdmGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 380
    .local v1, "tm":Landroid/telephony/TelephonyManager;
    if-nez v1, :cond_1

    .line 382
    const-string v2, "TelephonyManager is null!!"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 383
    const/4 v0, 0x0

    .line 393
    .end local v0    # "szBuff":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 386
    .restart local v0    # "szBuff":Ljava/lang/String;
    :cond_1
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    move-result-object v0

    .line 388
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 390
    const-string v0, ""

    goto :goto_0
.end method

.method public static xdmGetTargetLang()Ljava/lang/String;
    .locals 3

    .prologue
    .line 175
    const-string v1, "persist.sys.language"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 176
    .local v0, "lang":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 178
    const-string v1, "ro.product.locale.language"

    const-string v2, "en"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 180
    :cond_0
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static xdmGetTargetLanguage()Ljava/lang/String;
    .locals 8

    .prologue
    .line 147
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 148
    .local v0, "configuration":Landroid/content/res/Configuration;
    iget-object v4, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    .line 149
    .local v2, "szLanguage":Ljava/lang/String;
    iget-object v4, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    .line 150
    .local v1, "szCountry":Ljava/lang/String;
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "%s-%s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v2, v6, v7

    const/4 v7, 0x1

    aput-object v1, v6, v7

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 151
    .local v3, "szRet":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "language : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 153
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 156
    :goto_0
    return-object v3

    .line 154
    :cond_0
    const-string v3, "en-us"

    goto :goto_0
.end method

.method public static xdmGetTargetMSISDN()Ljava/lang/String;
    .locals 2

    .prologue
    .line 398
    const-string v1, "phone"

    invoke-static {v1}, Lcom/policydm/XDMApplication;->xdmGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 399
    .local v0, "tm":Landroid/telephony/TelephonyManager;
    if-nez v0, :cond_0

    .line 401
    const-string v1, "TelephonyManager is null!!"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 402
    const/4 v1, 0x0

    .line 405
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static xdmGetTargetManufacturer()Ljava/lang/String;
    .locals 2

    .prologue
    .line 126
    const-string v1, "ro.product.manufacturer"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 127
    .local v0, "szMan":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 130
    :goto_0
    return-object v0

    .line 128
    :cond_0
    const-string v0, "Samsung"

    goto :goto_0
.end method

.method public static xdmGetTargetModel()Ljava/lang/String;
    .locals 3

    .prologue
    .line 135
    const-string v1, "ro.product.model"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 136
    .local v0, "szModel":Ljava/lang/String;
    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 138
    const/4 v1, 0x0

    const/16 v2, 0x2f

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 140
    :cond_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 142
    :goto_0
    return-object v0

    .line 141
    :cond_1
    const-string v0, "GT-I9070"

    goto :goto_0
.end method

.method public static xdmGetTargetOEM()Ljava/lang/String;
    .locals 2

    .prologue
    .line 263
    const-string v1, "ro.product.OEM"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 265
    .local v0, "szOEM":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 268
    :goto_0
    return-object v0

    .line 266
    :cond_0
    const-string v0, "Samsung Electronics."

    goto :goto_0
.end method

.method public static xdmGetTargetOSVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 338
    const-string v0, "ro.build.version.release"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static xdmGetTargetOperatorAlphaFromSIM()Ljava/lang/String;
    .locals 3

    .prologue
    .line 363
    const-string v0, ""

    .line 365
    .local v0, "szOperator":Ljava/lang/String;
    const-string v1, "gsm.sim.operator.alpha"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 367
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pszBuff: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 368
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 370
    const/4 v0, 0x0

    .line 373
    :cond_0
    return-object v0
.end method

.method public static xdmGetTargetPhoneType()I
    .locals 2

    .prologue
    .line 114
    const-string v1, "phone"

    invoke-static {v1}, Lcom/policydm/XDMApplication;->xdmGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 115
    .local v0, "tm":Landroid/telephony/TelephonyManager;
    if-nez v0, :cond_0

    .line 117
    const-string v1, "tm == null"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 118
    const/4 v1, 0x0

    .line 121
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v1

    goto :goto_0
.end method

.method public static xdmGetTargetPhoneV()Ljava/lang/String;
    .locals 1

    .prologue
    .line 306
    const-string v0, "ril.sw_ver"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static xdmGetTargetSIMOperator()Ljava/lang/String;
    .locals 4

    .prologue
    .line 343
    const-string v0, ""

    .line 344
    .local v0, "szOperator":Ljava/lang/String;
    const-string v2, "phone"

    invoke-static {v2}, Lcom/policydm/XDMApplication;->xdmGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 345
    .local v1, "tm":Landroid/telephony/TelephonyManager;
    if-nez v1, :cond_0

    .line 347
    const-string v2, "TelephonyManager is null!!"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 348
    const/4 v0, 0x0

    .line 358
    .end local v0    # "szOperator":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 351
    .restart local v0    # "szOperator":Ljava/lang/String;
    :cond_0
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v0

    .line 352
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 354
    const-string v0, ""

    .line 357
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SIMOperator = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdmGetTargetSIMState()I
    .locals 2

    .prologue
    .line 311
    const-string v1, "phone"

    invoke-static {v1}, Lcom/policydm/XDMApplication;->xdmGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 312
    .local v0, "tm":Landroid/telephony/TelephonyManager;
    if-nez v0, :cond_0

    .line 313
    const/4 v1, 0x0

    .line 315
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v1

    goto :goto_0
.end method

.method public static xdmGetTargetSwV()Ljava/lang/String;
    .locals 1

    .prologue
    .line 273
    const-string v0, "ro.build.PDA"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static xdmGetTargetTWID()Ljava/lang/String;
    .locals 2

    .prologue
    .line 109
    const-string v0, "ro.serialno"

    const-string v1, "Default"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static xdmGetTargetTelephonyMcc()Ljava/lang/String;
    .locals 5

    .prologue
    .line 185
    const-string v0, ""

    .line 187
    .local v0, "mMobileCountryCode":Ljava/lang/String;
    sget-boolean v3, Lcom/policydm/adapter/XDMFeature;->XDM_FEATURE_WIFI_ONLY_MODEL:Z

    if-eqz v3, :cond_1

    .line 199
    :cond_0
    :goto_0
    return-object v0

    .line 190
    :cond_1
    const-string v3, "phone"

    invoke-static {v3}, Lcom/policydm/XDMApplication;->xdmGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 191
    .local v2, "tm":Landroid/telephony/TelephonyManager;
    if-eqz v2, :cond_0

    .line 193
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v1

    .line 194
    .local v1, "operator":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 196
    const/4 v3, 0x0

    const/4 v4, 0x3

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 198
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Telephony MobileCountryCode: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdmGetTargetTelephonyMnc()Ljava/lang/String;
    .locals 5

    .prologue
    .line 225
    const-string v0, ""

    .line 227
    .local v0, "mMobileNetworkCode":Ljava/lang/String;
    sget-boolean v3, Lcom/policydm/adapter/XDMFeature;->XDM_FEATURE_WIFI_ONLY_MODEL:Z

    if-eqz v3, :cond_1

    .line 239
    :cond_0
    :goto_0
    return-object v0

    .line 230
    :cond_1
    const-string v3, "phone"

    invoke-static {v3}, Lcom/policydm/XDMApplication;->xdmGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 231
    .local v2, "tm":Landroid/telephony/TelephonyManager;
    if-eqz v2, :cond_0

    .line 233
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v1

    .line 234
    .local v1, "operator":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 236
    const/4 v3, 0x3

    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 237
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Telephony MobileNetworkCode: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdmGetTotalMemorySize(I)J
    .locals 8
    .param p0, "nMemoryArea"    # I

    .prologue
    .line 618
    const-string v3, ""

    .line 620
    .local v3, "szPath":Ljava/lang/String;
    invoke-static {p0}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetStoragePath(I)Ljava/lang/String;

    move-result-object v3

    .line 621
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 622
    const-wide/16 v6, 0x0

    .line 627
    :goto_0
    return-wide v6

    .line 624
    :cond_0
    new-instance v2, Landroid/os/StatFs;

    invoke-direct {v2, v3}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 625
    .local v2, "stat":Landroid/os/StatFs;
    invoke-virtual {v2}, Landroid/os/StatFs;->getBlockSize()I

    move-result v6

    int-to-long v0, v6

    .line 626
    .local v0, "blockSize":J
    invoke-virtual {v2}, Landroid/os/StatFs;->getBlockCount()I

    move-result v6

    int-to-long v4, v6

    .line 627
    .local v4, "totalBlocks":J
    mul-long v6, v4, v0

    goto :goto_0
.end method

.method public static xdmInitExternalStorageState()V
    .locals 8

    .prologue
    .line 476
    const-string v6, ""

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 478
    const-string v6, "storage"

    invoke-static {v6}, Lcom/policydm/XDMApplication;->xdmGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/storage/StorageManager;

    .line 479
    .local v2, "mStorageManager":Landroid/os/storage/StorageManager;
    if-nez v2, :cond_0

    .line 481
    const-string v6, "mStorageManager is null!!"

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 538
    :goto_0
    return-void

    .line 486
    :cond_0
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    .line 487
    .local v4, "szDataDir":Ljava/lang/String;
    sget-object v6, Lcom/policydm/adapter/XDMTargetAdapter;->DATA_DIR_PATH:Ljava/lang/String;

    invoke-virtual {v6, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-eqz v6, :cond_1

    .line 489
    sput-object v4, Lcom/policydm/adapter/XDMTargetAdapter;->DATA_DIR_PATH:Ljava/lang/String;

    .line 495
    :cond_1
    :try_start_0
    invoke-virtual {v2}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    move-result-object v3

    .line 498
    .local v3, "mStorageVolumes":[Landroid/os/storage/StorageVolume;
    sget-object v6, Lcom/policydm/adapter/XDMTargetAdapter;->EXTERNAL_DIR_PATH:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 499
    const/4 v6, 0x0

    aget-object v6, v3, v6

    invoke-virtual {v6}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v6

    sput-object v6, Lcom/policydm/adapter/XDMTargetAdapter;->EXTERNAL_DIR_PATH:Ljava/lang/String;

    .line 501
    :cond_2
    const/4 v6, 0x0

    aget-object v6, v3, v6

    invoke-virtual {v6}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "mounted"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 502
    const/4 v6, 0x1

    sput-boolean v6, Lcom/policydm/adapter/XDMTargetAdapter;->g_bExternalStorageAvailable:Z

    .line 507
    :goto_1
    sget-object v6, Lcom/policydm/adapter/XDMTargetAdapter;->EXTERNAL_SD_DIR_PATH:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 508
    const/4 v6, 0x1

    aget-object v6, v3, v6

    invoke-virtual {v6}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v6

    sput-object v6, Lcom/policydm/adapter/XDMTargetAdapter;->EXTERNAL_SD_DIR_PATH:Ljava/lang/String;

    .line 510
    :cond_3
    const/4 v6, 0x1

    aget-object v6, v3, v6

    invoke-virtual {v6}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "mounted"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 511
    const/4 v6, 0x1

    sput-boolean v6, Lcom/policydm/adapter/XDMTargetAdapter;->g_bExternalSDStorageAvailable:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 521
    .end local v3    # "mStorageVolumes":[Landroid/os/storage/StorageVolume;
    :goto_2
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    .line 522
    .local v0, "cacheDir":Ljava/io/File;
    if-eqz v0, :cond_4

    .line 524
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    .line 525
    .local v5, "szPolicyDir":Ljava/lang/String;
    sget-object v6, Lcom/policydm/adapter/XDMTargetAdapter;->POLICY_DIR_PATH:Ljava/lang/String;

    invoke-virtual {v6, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-eqz v6, :cond_4

    .line 527
    sput-object v5, Lcom/policydm/adapter/XDMTargetAdapter;->POLICY_DIR_PATH:Ljava/lang/String;

    .line 531
    .end local v5    # "szPolicyDir":Ljava/lang/String;
    :cond_4
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "DATA_DIR_PATH ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/policydm/adapter/XDMTargetAdapter;->DATA_DIR_PATH:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 532
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "EXTERNAL_DIR_PATH ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/policydm/adapter/XDMTargetAdapter;->EXTERNAL_DIR_PATH:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 533
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "EXTERNAL_SD_DIR_PATH ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/policydm/adapter/XDMTargetAdapter;->EXTERNAL_SD_DIR_PATH:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 534
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "POLICY_DIR_PATH ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/policydm/adapter/XDMTargetAdapter;->POLICY_DIR_PATH:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 536
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "bExternalStorageAvailable ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-boolean v7, Lcom/policydm/adapter/XDMTargetAdapter;->g_bExternalStorageAvailable:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 537
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "bExternalSDStorageAvailable ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-boolean v7, Lcom/policydm/adapter/XDMTargetAdapter;->g_bExternalSDStorageAvailable:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 504
    .end local v0    # "cacheDir":Ljava/io/File;
    .restart local v3    # "mStorageVolumes":[Landroid/os/storage/StorageVolume;
    :cond_5
    const/4 v6, 0x0

    :try_start_1
    sput-boolean v6, Lcom/policydm/adapter/XDMTargetAdapter;->g_bExternalStorageAvailable:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    .line 515
    .end local v3    # "mStorageVolumes":[Landroid/os/storage/StorageVolume;
    :catch_0
    move-exception v1

    .line 517
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 513
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v3    # "mStorageVolumes":[Landroid/os/storage/StorageVolume;
    :cond_6
    const/4 v6, 0x0

    :try_start_2
    sput-boolean v6, Lcom/policydm/adapter/XDMTargetAdapter;->g_bExternalSDStorageAvailable:Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_2
.end method

.method public static xdmKorModel()Z
    .locals 3

    .prologue
    .line 824
    const-string v0, "SKT/SKC/SKO/KTT/KTC/KTO/LGT/LUC/LUO"

    .line 826
    .local v0, "SALESCODE_KOREA_LIST":Ljava/lang/String;
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetCustomCode()Ljava/lang/String;

    move-result-object v1

    .line 827
    .local v1, "salesCode":Ljava/lang/String;
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 828
    const/4 v2, 0x1

    .line 830
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static xdmMEID()Z
    .locals 6

    .prologue
    .line 846
    const/16 v4, 0x30

    new-array v1, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "SM-N9009"

    aput-object v5, v1, v4

    const/4 v4, 0x1

    const-string v5, "SCH-R530C"

    aput-object v5, v1, v4

    const/4 v4, 0x2

    const-string v5, "SCH-R530X"

    aput-object v5, v1, v4

    const/4 v4, 0x3

    const-string v5, "SM-G7109"

    aput-object v5, v1, v4

    const/4 v4, 0x4

    const-string v5, "SCH-R530M"

    aput-object v5, v1, v4

    const/4 v4, 0x5

    const-string v5, "SCL23"

    aput-object v5, v1, v4

    const/4 v4, 0x6

    const-string v5, "SM-G900R4"

    aput-object v5, v1, v4

    const/4 v4, 0x7

    const-string v5, "SM-G900P"

    aput-object v5, v1, v4

    const/16 v4, 0x8

    const-string v5, "SCH-J003"

    aput-object v5, v1, v4

    const/16 v4, 0x9

    const-string v5, "SM-G900V"

    aput-object v5, v1, v4

    const/16 v4, 0xa

    const-string v5, "SM-T527P"

    aput-object v5, v1, v4

    const/16 v4, 0xb

    const-string v5, "SCH-R950"

    aput-object v5, v1, v4

    const/16 v4, 0xc

    const-string v5, "SPH-L520"

    aput-object v5, v1, v4

    const/16 v4, 0xd

    const-string v5, "SCH-I545"

    aput-object v5, v1, v4

    const/16 v4, 0xe

    const-string v5, "SCH-R890"

    aput-object v5, v1, v4

    const/16 v4, 0xf

    const-string v5, "SM-T337V"

    aput-object v5, v1, v4

    const/16 v4, 0x10

    const-string v5, "SM-T537V"

    aput-object v5, v1, v4

    const/16 v4, 0x11

    const-string v5, "SM-T217S"

    aput-object v5, v1, v4

    const/16 v4, 0x12

    const-string v5, "SCH-I605"

    aput-object v5, v1, v4

    const/16 v4, 0x13

    const-string v5, "SCH-I535"

    aput-object v5, v1, v4

    const/16 v4, 0x14

    const-string v5, "SCH-R970X"

    aput-object v5, v1, v4

    const/16 v4, 0x15

    const-string v5, "SPH-L900"

    aput-object v5, v1, v4

    const/16 v4, 0x16

    const-string v5, "SCH-R970C"

    aput-object v5, v1, v4

    const/16 v4, 0x17

    const-string v5, "SM-N900R4"

    aput-object v5, v1, v4

    const/16 v4, 0x18

    const-string v5, "SM-N900P"

    aput-object v5, v1, v4

    const/16 v4, 0x19

    const-string v5, "SM-P605V"

    aput-object v5, v1, v4

    const/16 v4, 0x1a

    const-string v5, "SM-P905V"

    aput-object v5, v1, v4

    const/16 v4, 0x1b

    const-string v5, "SCH-I435"

    aput-object v5, v1, v4

    const/16 v4, 0x1c

    const-string v5, "SPH-L720"

    aput-object v5, v1, v4

    const/16 v4, 0x1d

    const-string v5, "SCH-L710"

    aput-object v5, v1, v4

    const/16 v4, 0x1e

    const-string v5, "SCL22"

    aput-object v5, v1, v4

    const/16 v4, 0x1f

    const-string v5, "SCH-R970"

    aput-object v5, v1, v4

    const/16 v4, 0x20

    const-string v5, "SPH-L710"

    aput-object v5, v1, v4

    const/16 v4, 0x21

    const-string v5, "SHV-E300L"

    aput-object v5, v1, v4

    const/16 v4, 0x22

    const-string v5, "SCH-R530U"

    aput-object v5, v1, v4

    const/16 v4, 0x23

    const-string v5, "SM-N900V"

    aput-object v5, v1, v4

    const/16 v4, 0x24

    const-string v5, "SPH-L720T"

    aput-object v5, v1, v4

    const/16 v4, 0x25

    const-string v5, "SCH-I545L"

    aput-object v5, v1, v4

    const/16 v4, 0x26

    const-string v5, "SHV-E210L"

    aput-object v5, v1, v4

    const/16 v4, 0x27

    const-string v5, "SHV-E250L"

    aput-object v5, v1, v4

    const/16 v4, 0x28

    const-string v5, "SM-G9009D"

    aput-object v5, v1, v4

    const/16 v4, 0x29

    const-string v5, "SCH-I435L"

    aput-object v5, v1, v4

    const/16 v4, 0x2a

    const-string v5, "SCH-I925"

    aput-object v5, v1, v4

    const/16 v4, 0x2b

    const-string v5, "SCH-R960"

    aput-object v5, v1, v4

    const/16 v4, 0x2c

    const-string v5, "SM-G860P"

    aput-object v5, v1, v4

    const/16 v4, 0x2d

    const-string v5, "SM-G900R6"

    aput-object v5, v1, v4

    const/16 v4, 0x2e

    const-string v5, "SPH-L710T"

    aput-object v5, v1, v4

    const/16 v4, 0x2f

    const-string v5, "SPH-L600"

    aput-object v5, v1, v4

    .line 895
    .local v1, "SALESCODE_MEID_LIST":[Ljava/lang/String;
    const/4 v3, 0x0

    .line 896
    .local v3, "isMEID":Z
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetModel()Ljava/lang/String;

    move-result-object v0

    .line 897
    .local v0, "MEID":Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v4, v1

    if-ge v2, v4, :cond_1

    .line 899
    aget-object v4, v1, v2

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 901
    const/4 v3, 0x1

    .line 897
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 904
    :cond_1
    return v3
.end method

.method public static xdmTablet()Z
    .locals 2

    .prologue
    .line 798
    const-string v1, "ro.build.characteristics"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 799
    .local v0, "deviceType":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, "tablet"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static xdmUseSPP()Z
    .locals 3

    .prologue
    .line 813
    const-string v0, "CTC/CHN/CHM/CHU/CHC"

    .line 815
    .local v0, "SALESCODE_CHINA_LIST":Ljava/lang/String;
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetCustomCode()Ljava/lang/String;

    move-result-object v1

    .line 816
    .local v1, "salesCode":Ljava/lang/String;
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 817
    const/4 v2, 0x1

    .line 819
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static xdmWhite()Z
    .locals 2

    .prologue
    .line 804
    const-string v1, "ro.build.scafe.cream"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 805
    .local v0, "deviceType":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, "black"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 806
    const/4 v1, 0x0

    .line 808
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method
