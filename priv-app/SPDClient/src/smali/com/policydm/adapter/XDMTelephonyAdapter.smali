.class public Lcom/policydm/adapter/XDMTelephonyAdapter;
.super Ljava/lang/Object;
.source "XDMTelephonyAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;,
        Lcom/policydm/adapter/XDMTelephonyAdapter$XDMAuthInfo;
    }
.end annotation


# static fields
.field private static final APN_CONTENT_URI:Landroid/net/Uri;

.field private static final APN_PROJECTION:[Ljava/lang/String;

.field private static final PREFERAPN_URI:Landroid/net/Uri;

.field private static context:Landroid/content/Context;

.field public static existApn:Z

.field private static final searchProjection:[Ljava/lang/String;

.field private static selectedAPNID:I


# instance fields
.field public apntype:Ljava/lang/String;

.field public auth:Ljava/lang/String;

.field public authInfo:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMAuthInfo;

.field public napAddr:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;

.field public szAccountName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 15
    const/16 v0, 0xf

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "name"

    aput-object v1, v0, v4

    const-string v1, "apn"

    aput-object v1, v0, v5

    const-string v1, "proxy"

    aput-object v1, v0, v6

    const/4 v1, 0x4

    const-string v2, "port"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "user"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "server"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "password"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "mmsc"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "mcc"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "mnc"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "numeric"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "mmsproxy"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "mmsport"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "authtype"

    aput-object v2, v0, v1

    sput-object v0, Lcom/policydm/adapter/XDMTelephonyAdapter;->APN_PROJECTION:[Ljava/lang/String;

    .line 35
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "name"

    aput-object v1, v0, v4

    const-string v1, "apn"

    aput-object v1, v0, v5

    sput-object v0, Lcom/policydm/adapter/XDMTelephonyAdapter;->searchProjection:[Ljava/lang/String;

    .line 38
    const-string v0, "content://telephony/carriers"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/policydm/adapter/XDMTelephonyAdapter;->APN_CONTENT_URI:Landroid/net/Uri;

    .line 39
    const-string v0, "content://telephony/carriers/preferapn"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/policydm/adapter/XDMTelephonyAdapter;->PREFERAPN_URI:Landroid/net/Uri;

    .line 43
    sput v4, Lcom/policydm/adapter/XDMTelephonyAdapter;->selectedAPNID:I

    .line 53
    sput-boolean v3, Lcom/policydm/adapter/XDMTelephonyAdapter;->existApn:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "pcontext"    # Landroid/content/Context;

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    sget-object v0, Lcom/policydm/adapter/XDMTelephonyAdapter;->context:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 84
    sput-object p1, Lcom/policydm/adapter/XDMTelephonyAdapter;->context:Landroid/content/Context;

    .line 85
    :cond_0
    return-void
.end method

.method public static xdmAgentAppGetExistApn()Z
    .locals 1

    .prologue
    .line 89
    sget-boolean v0, Lcom/policydm/adapter/XDMTelephonyAdapter;->existApn:Z

    return v0
.end method

.method private xdmAgentAppSaveNetInfo(Lcom/policydm/adapter/XDMTelephonyAdapter;)V
    .locals 10
    .param p1, "netInfo"    # Lcom/policydm/adapter/XDMTelephonyAdapter;

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v9, 0x1

    .line 295
    sget v5, Lcom/policydm/adapter/XDMTelephonyAdapter;->selectedAPNID:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 296
    .local v3, "szParamIndex":Ljava/lang/String;
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 298
    .local v4, "values":Landroid/content/ContentValues;
    iget-object v5, p1, Lcom/policydm/adapter/XDMTelephonyAdapter;->szAccountName:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 299
    sget-object v5, Lcom/policydm/adapter/XDMTelephonyAdapter;->APN_PROJECTION:[Ljava/lang/String;

    aget-object v5, v5, v9

    iget-object v6, p1, Lcom/policydm/adapter/XDMTelephonyAdapter;->szAccountName:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    :cond_0
    iget-object v5, p1, Lcom/policydm/adapter/XDMTelephonyAdapter;->napAddr:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;

    if-eqz v5, :cond_7

    .line 303
    iget-object v5, p1, Lcom/policydm/adapter/XDMTelephonyAdapter;->napAddr:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;

    iget-object v5, v5, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;->m_szApn:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 304
    sget-object v5, Lcom/policydm/adapter/XDMTelephonyAdapter;->APN_PROJECTION:[Ljava/lang/String;

    const/4 v6, 0x2

    aget-object v5, v5, v6

    iget-object v6, p1, Lcom/policydm/adapter/XDMTelephonyAdapter;->napAddr:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;

    iget-object v6, v6, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;->m_szApn:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    :cond_1
    iget-object v5, p1, Lcom/policydm/adapter/XDMTelephonyAdapter;->napAddr:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;

    iget-object v5, v5, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;->m_szPrimaryProxyAddr:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 308
    iget-object v5, p1, Lcom/policydm/adapter/XDMTelephonyAdapter;->napAddr:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;

    iget-object v5, v5, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;->m_szPrimaryProxyAddr:Ljava/lang/String;

    const-string v6, "0.0.0.0"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 310
    sget-object v5, Lcom/policydm/adapter/XDMTelephonyAdapter;->APN_PROJECTION:[Ljava/lang/String;

    aget-object v5, v5, v7

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    :goto_0
    iget-object v5, p1, Lcom/policydm/adapter/XDMTelephonyAdapter;->napAddr:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;

    iget v5, v5, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;->nPrimary_proxy_port:I

    if-nez v5, :cond_6

    .line 318
    sget-object v5, Lcom/policydm/adapter/XDMTelephonyAdapter;->APN_PROJECTION:[Ljava/lang/String;

    aget-object v5, v5, v8

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    :cond_2
    :goto_1
    iget-object v5, p1, Lcom/policydm/adapter/XDMTelephonyAdapter;->authInfo:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMAuthInfo;

    if-eqz v5, :cond_8

    .line 333
    iget-object v5, p1, Lcom/policydm/adapter/XDMTelephonyAdapter;->authInfo:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMAuthInfo;

    iget-object v5, v5, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMAuthInfo;->szId:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 334
    sget-object v5, Lcom/policydm/adapter/XDMTelephonyAdapter;->APN_PROJECTION:[Ljava/lang/String;

    const/4 v6, 0x5

    aget-object v5, v5, v6

    iget-object v6, p1, Lcom/policydm/adapter/XDMTelephonyAdapter;->authInfo:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMAuthInfo;

    iget-object v6, v6, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMAuthInfo;->szId:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    :cond_3
    iget-object v5, p1, Lcom/policydm/adapter/XDMTelephonyAdapter;->authInfo:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMAuthInfo;

    iget-object v5, v5, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMAuthInfo;->szPasswd:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 336
    sget-object v5, Lcom/policydm/adapter/XDMTelephonyAdapter;->APN_PROJECTION:[Ljava/lang/String;

    const/4 v6, 0x7

    aget-object v5, v5, v6

    iget-object v6, p1, Lcom/policydm/adapter/XDMTelephonyAdapter;->authInfo:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMAuthInfo;

    iget-object v6, v6, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMAuthInfo;->szPasswd:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    :cond_4
    :goto_2
    iget-object v5, p1, Lcom/policydm/adapter/XDMTelephonyAdapter;->apntype:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_9

    .line 344
    sget-object v5, Lcom/policydm/adapter/XDMTelephonyAdapter;->APN_PROJECTION:[Ljava/lang/String;

    const/16 v6, 0xe

    aget-object v5, v5, v6

    iget-object v6, p1, Lcom/policydm/adapter/XDMTelephonyAdapter;->apntype:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    :goto_3
    :try_start_0
    sget-object v5, Lcom/policydm/adapter/XDMTelephonyAdapter;->APN_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 353
    .local v1, "netInfoUri":Landroid/net/Uri;
    sget-object v5, Lcom/policydm/adapter/XDMTelephonyAdapter;->context:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v5, v1, v4, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 354
    .local v2, "ret":I
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "update completed "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " accounts"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 361
    sget-object v5, Lcom/policydm/adapter/XDMTelephonyAdapter;->context:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/policydm/adapter/XDMTelephonyAdapter;->PREFERAPN_URI:Landroid/net/Uri;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v4, v7, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 362
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "update completed "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " accounts"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 370
    .end local v1    # "netInfoUri":Landroid/net/Uri;
    .end local v2    # "ret":I
    :goto_4
    sput v9, Lcom/policydm/adapter/XDMTelephonyAdapter;->selectedAPNID:I

    .line 371
    return-void

    .line 314
    :cond_5
    sget-object v5, Lcom/policydm/adapter/XDMTelephonyAdapter;->APN_PROJECTION:[Ljava/lang/String;

    aget-object v5, v5, v7

    iget-object v6, p1, Lcom/policydm/adapter/XDMTelephonyAdapter;->napAddr:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;

    iget-object v6, v6, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;->m_szPrimaryProxyAddr:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 322
    :cond_6
    sget-object v5, Lcom/policydm/adapter/XDMTelephonyAdapter;->APN_PROJECTION:[Ljava/lang/String;

    aget-object v5, v5, v8

    iget-object v6, p1, Lcom/policydm/adapter/XDMTelephonyAdapter;->napAddr:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;

    iget v6, v6, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;->nPrimary_proxy_port:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_1

    .line 328
    :cond_7
    const-string v5, "netInfo.napAddr is Null"

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 340
    :cond_8
    const-string v5, "netInfo.authInfo is Null"

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 347
    :cond_9
    const-string v5, "netInfo.apntype is Null"

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 364
    :catch_0
    move-exception v0

    .line 366
    .local v0, "e":Ljava/lang/Exception;
    const-string v5, "Can\'t update DB"

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 367
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_4
.end method

.method public static xdmAgentAppSetExistApn(Z)V
    .locals 0
    .param p0, "bexist"    # Z

    .prologue
    .line 94
    sput-boolean p0, Lcom/policydm/adapter/XDMTelephonyAdapter;->existApn:Z

    .line 95
    return-void
.end method


# virtual methods
.method public xdmAgentAppGetNetInfo(Lcom/policydm/adapter/XDMTelephonyAdapter;)V
    .locals 16
    .param p1, "netInfo"    # Lcom/policydm/adapter/XDMTelephonyAdapter;

    .prologue
    .line 99
    const/4 v7, 0x0

    .line 100
    .local v7, "_cNetInfo":Landroid/database/Cursor;
    const/4 v8, 0x0

    .line 101
    .local v8, "cursor":Landroid/database/Cursor;
    const/4 v9, 0x0

    .line 105
    .local v9, "cursor1":Landroid/database/Cursor;
    :try_start_0
    sget-object v1, Lcom/policydm/adapter/XDMTelephonyAdapter;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/policydm/adapter/XDMTelephonyAdapter;->PREFERAPN_URI:Landroid/net/Uri;

    sget-object v3, Lcom/policydm/adapter/XDMTelephonyAdapter;->searchProjection:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 106
    if-eqz v8, :cond_16

    .line 108
    const/4 v11, 0x1

    .line 109
    .local v11, "pos":I
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/policydm/adapter/XDMTelephonyAdapter;->xdmAgentAppSetExistApn(Z)V

    .line 111
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-nez v1, :cond_15

    .line 114
    const-string v1, "there is no enabled apn in PREFERAPN_URI!!!!!!!"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 115
    sget-object v1, Lcom/policydm/adapter/XDMTelephonyAdapter;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/policydm/adapter/XDMTelephonyAdapter;->APN_CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    const-string v4, "current =\'1\'"

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 116
    if-eqz v9, :cond_6

    .line 118
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_12

    .line 120
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 122
    const-string v1, "_id"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 123
    sput v11, Lcom/policydm/adapter/XDMTelephonyAdapter;->selectedAPNID:I

    .line 124
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "there is no enabled apn in PREFERAPN_URI selectedAPNID="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/policydm/adapter/XDMTelephonyAdapter;->selectedAPNID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 125
    sget-object v1, Lcom/policydm/adapter/XDMTelephonyAdapter;->APN_PROJECTION:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    iput-object v1, v0, Lcom/policydm/adapter/XDMTelephonyAdapter;->szAccountName:Ljava/lang/String;

    .line 128
    sget-object v1, Lcom/policydm/adapter/XDMTelephonyAdapter;->APN_PROJECTION:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 129
    .local v12, "szTmpAPN":Ljava/lang/String;
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 131
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/policydm/adapter/XDMTelephonyAdapter;->napAddr:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;

    if-nez v1, :cond_0

    .line 133
    new-instance v1, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;

    invoke-direct {v1}, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;-><init>()V

    move-object/from16 v0, p1

    iput-object v1, v0, Lcom/policydm/adapter/XDMTelephonyAdapter;->napAddr:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;

    .line 135
    :cond_0
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/policydm/adapter/XDMTelephonyAdapter;->napAddr:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;

    iput-object v12, v1, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;->m_szApn:Ljava/lang/String;

    .line 139
    :cond_1
    sget-object v1, Lcom/policydm/adapter/XDMTelephonyAdapter;->APN_PROJECTION:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 140
    .local v13, "szTmpProxyAddr":Ljava/lang/String;
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 142
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/policydm/adapter/XDMTelephonyAdapter;->napAddr:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;

    if-nez v1, :cond_2

    .line 144
    new-instance v1, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;

    invoke-direct {v1}, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;-><init>()V

    move-object/from16 v0, p1

    iput-object v1, v0, Lcom/policydm/adapter/XDMTelephonyAdapter;->napAddr:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;

    .line 147
    :cond_2
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/policydm/adapter/XDMTelephonyAdapter;->napAddr:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;

    invoke-static {v13}, Lcom/policydm/tp/XTPHttpUtil;->xtpCheckValidIPAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;->m_szPrimaryProxyAddr:Ljava/lang/String;

    .line 148
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "proxy addr is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/policydm/adapter/XDMTelephonyAdapter;->napAddr:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;

    iget-object v2, v2, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;->m_szPrimaryProxyAddr:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 151
    sget-object v1, Lcom/policydm/adapter/XDMTelephonyAdapter;->APN_PROJECTION:[Ljava/lang/String;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 152
    .local v15, "tmpProxyPort":Ljava/lang/String;
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/policydm/adapter/XDMTelephonyAdapter;->napAddr:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;

    const/4 v2, 0x0

    iput v2, v1, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;->nPrimary_proxy_port:I

    .line 153
    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 155
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/policydm/adapter/XDMTelephonyAdapter;->napAddr:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v1, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;->nPrimary_proxy_port:I

    .line 156
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "proxy port is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/policydm/adapter/XDMTelephonyAdapter;->napAddr:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;

    iget v2, v2, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;->nPrimary_proxy_port:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 161
    .end local v15    # "tmpProxyPort":Ljava/lang/String;
    :cond_3
    sget-object v1, Lcom/policydm/adapter/XDMTelephonyAdapter;->APN_PROJECTION:[Ljava/lang/String;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 162
    .local v14, "szTmpUsername":Ljava/lang/String;
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 164
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/policydm/adapter/XDMTelephonyAdapter;->authInfo:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMAuthInfo;

    if-nez v1, :cond_4

    .line 166
    new-instance v1, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMAuthInfo;

    invoke-direct {v1}, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMAuthInfo;-><init>()V

    move-object/from16 v0, p1

    iput-object v1, v0, Lcom/policydm/adapter/XDMTelephonyAdapter;->authInfo:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMAuthInfo;

    .line 168
    :cond_4
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/policydm/adapter/XDMTelephonyAdapter;->authInfo:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMAuthInfo;

    iput-object v14, v1, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMAuthInfo;->szId:Ljava/lang/String;

    .line 171
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/policydm/adapter/XDMTelephonyAdapter;->authInfo:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMAuthInfo;

    sget-object v2, Lcom/policydm/adapter/XDMTelephonyAdapter;->APN_PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x7

    aget-object v2, v2, v3

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMAuthInfo;->szPasswd:Ljava/lang/String;

    .line 174
    :cond_5
    sget-object v1, Lcom/policydm/adapter/XDMTelephonyAdapter;->APN_PROJECTION:[Ljava/lang/String;

    const/16 v2, 0xe

    aget-object v1, v1, v2

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    iput-object v1, v0, Lcom/policydm/adapter/XDMTelephonyAdapter;->apntype:Ljava/lang/String;

    .line 187
    .end local v12    # "szTmpAPN":Ljava/lang/String;
    .end local v13    # "szTmpProxyAddr":Ljava/lang/String;
    .end local v14    # "szTmpUsername":Ljava/lang/String;
    :cond_6
    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 189
    sget-object v1, Lcom/policydm/adapter/XDMTelephonyAdapter;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/policydm/adapter/XDMTelephonyAdapter;->APN_CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_id =\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 191
    if-eqz v7, :cond_e

    .line 193
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_e

    .line 195
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_d

    .line 197
    sput v11, Lcom/policydm/adapter/XDMTelephonyAdapter;->selectedAPNID:I

    .line 198
    sget-object v1, Lcom/policydm/adapter/XDMTelephonyAdapter;->APN_PROJECTION:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    iput-object v1, v0, Lcom/policydm/adapter/XDMTelephonyAdapter;->szAccountName:Ljava/lang/String;

    .line 201
    sget-object v1, Lcom/policydm/adapter/XDMTelephonyAdapter;->APN_PROJECTION:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 202
    .restart local v12    # "szTmpAPN":Ljava/lang/String;
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 204
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/policydm/adapter/XDMTelephonyAdapter;->napAddr:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;

    if-nez v1, :cond_7

    .line 206
    new-instance v1, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;

    invoke-direct {v1}, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;-><init>()V

    move-object/from16 v0, p1

    iput-object v1, v0, Lcom/policydm/adapter/XDMTelephonyAdapter;->napAddr:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;

    .line 208
    :cond_7
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/policydm/adapter/XDMTelephonyAdapter;->napAddr:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;

    iput-object v12, v1, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;->m_szApn:Ljava/lang/String;

    .line 212
    :cond_8
    sget-object v1, Lcom/policydm/adapter/XDMTelephonyAdapter;->APN_PROJECTION:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 213
    .restart local v13    # "szTmpProxyAddr":Ljava/lang/String;
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 215
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/policydm/adapter/XDMTelephonyAdapter;->napAddr:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;

    if-nez v1, :cond_9

    .line 217
    new-instance v1, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;

    invoke-direct {v1}, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;-><init>()V

    move-object/from16 v0, p1

    iput-object v1, v0, Lcom/policydm/adapter/XDMTelephonyAdapter;->napAddr:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;

    .line 221
    :cond_9
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/policydm/adapter/XDMTelephonyAdapter;->napAddr:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;

    invoke-static {v13}, Lcom/policydm/tp/XTPHttpUtil;->xtpCheckValidIPAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;->m_szPrimaryProxyAddr:Ljava/lang/String;

    .line 222
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "proxy addr is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/policydm/adapter/XDMTelephonyAdapter;->napAddr:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;

    iget-object v2, v2, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;->m_szPrimaryProxyAddr:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 225
    sget-object v1, Lcom/policydm/adapter/XDMTelephonyAdapter;->APN_PROJECTION:[Ljava/lang/String;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 226
    .restart local v15    # "tmpProxyPort":Ljava/lang/String;
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/policydm/adapter/XDMTelephonyAdapter;->napAddr:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;

    const/4 v2, 0x0

    iput v2, v1, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;->nPrimary_proxy_port:I

    .line 227
    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 229
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/policydm/adapter/XDMTelephonyAdapter;->napAddr:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v1, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;->nPrimary_proxy_port:I

    .line 230
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "proxy port is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/policydm/adapter/XDMTelephonyAdapter;->napAddr:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;

    iget v2, v2, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMConnectSetting;->nPrimary_proxy_port:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 235
    .end local v15    # "tmpProxyPort":Ljava/lang/String;
    :cond_a
    sget-object v1, Lcom/policydm/adapter/XDMTelephonyAdapter;->APN_PROJECTION:[Ljava/lang/String;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 236
    .restart local v14    # "szTmpUsername":Ljava/lang/String;
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 238
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/policydm/adapter/XDMTelephonyAdapter;->authInfo:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMAuthInfo;

    if-nez v1, :cond_b

    .line 240
    new-instance v1, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMAuthInfo;

    invoke-direct {v1}, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMAuthInfo;-><init>()V

    move-object/from16 v0, p1

    iput-object v1, v0, Lcom/policydm/adapter/XDMTelephonyAdapter;->authInfo:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMAuthInfo;

    .line 242
    :cond_b
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/policydm/adapter/XDMTelephonyAdapter;->authInfo:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMAuthInfo;

    iput-object v14, v1, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMAuthInfo;->szId:Ljava/lang/String;

    .line 245
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/policydm/adapter/XDMTelephonyAdapter;->authInfo:Lcom/policydm/adapter/XDMTelephonyAdapter$XDMAuthInfo;

    sget-object v2, Lcom/policydm/adapter/XDMTelephonyAdapter;->APN_PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x7

    aget-object v2, v2, v3

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/policydm/adapter/XDMTelephonyAdapter$XDMAuthInfo;->szPasswd:Ljava/lang/String;

    .line 248
    :cond_c
    sget-object v1, Lcom/policydm/adapter/XDMTelephonyAdapter;->APN_PROJECTION:[Ljava/lang/String;

    const/16 v2, 0xe

    aget-object v1, v1, v2

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    iput-object v1, v0, Lcom/policydm/adapter/XDMTelephonyAdapter;->apntype:Ljava/lang/String;

    .line 250
    .end local v12    # "szTmpAPN":Ljava/lang/String;
    .end local v13    # "szTmpProxyAddr":Ljava/lang/String;
    .end local v14    # "szTmpUsername":Ljava/lang/String;
    :cond_d
    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 266
    .end local v11    # "pos":I
    :cond_e
    :goto_1
    if-eqz v7, :cond_f

    .line 267
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 269
    :cond_f
    if-eqz v8, :cond_10

    .line 270
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 272
    :cond_10
    if-eqz v9, :cond_11

    .line 273
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 275
    :cond_11
    :goto_2
    return-void

    .line 179
    .restart local v11    # "pos":I
    :cond_12
    :try_start_1
    const-string v1, "there is no enabled apn!!!!!!!"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 259
    .end local v11    # "pos":I
    :catch_0
    move-exception v10

    .line 261
    .local v10, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    :try_start_2
    invoke-static {v1}, Lcom/policydm/adapter/XDMTelephonyAdapter;->xdmAgentAppSetExistApn(Z)V

    .line 262
    invoke-virtual {v10}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 266
    if-eqz v7, :cond_13

    .line 267
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 269
    :cond_13
    if-eqz v8, :cond_14

    .line 270
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 272
    :cond_14
    if-eqz v9, :cond_11

    .line 273
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 185
    .end local v10    # "e":Ljava/lang/Exception;
    .restart local v11    # "pos":I
    :cond_15
    :try_start_3
    const-string v1, "_id"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    goto/16 :goto_0

    .line 256
    .end local v11    # "pos":I
    :cond_16
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/policydm/adapter/XDMTelephonyAdapter;->xdmAgentAppSetExistApn(Z)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 266
    :catchall_0
    move-exception v1

    if-eqz v7, :cond_17

    .line 267
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 269
    :cond_17
    if-eqz v8, :cond_18

    .line 270
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 272
    :cond_18
    if-eqz v9, :cond_19

    .line 273
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_19
    throw v1
.end method

.method public xdmAgentAppProtoSetAccount(ILcom/policydm/adapter/XDMTelephonyAdapter;)Z
    .locals 1
    .param p1, "protoAccount"    # I
    .param p2, "netInfo"    # Lcom/policydm/adapter/XDMTelephonyAdapter;

    .prologue
    .line 285
    invoke-direct {p0, p2}, Lcom/policydm/adapter/XDMTelephonyAdapter;->xdmAgentAppSaveNetInfo(Lcom/policydm/adapter/XDMTelephonyAdapter;)V

    .line 286
    const/4 v0, 0x1

    return v0
.end method
