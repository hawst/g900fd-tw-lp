.class public Lcom/policydm/XDMBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "XDMBroadcastReceiver.java"

# interfaces
.implements Lcom/policydm/interfaces/XCommonInterface;
.implements Lcom/policydm/interfaces/XEventInterface;
.implements Lcom/policydm/interfaces/XUIInterface;


# static fields
.field public static bAlreadyReceivedIntent:Z

.field public static bStartDeviceRegister:Z

.field public static g_ResumeCase:I

.field public static m_bTestDevice:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 38
    sput v0, Lcom/policydm/XDMBroadcastReceiver;->g_ResumeCase:I

    .line 39
    sput-boolean v0, Lcom/policydm/XDMBroadcastReceiver;->bStartDeviceRegister:Z

    .line 40
    sput-boolean v0, Lcom/policydm/XDMBroadcastReceiver;->bAlreadyReceivedIntent:Z

    .line 42
    sput-boolean v0, Lcom/policydm/XDMBroadcastReceiver;->m_bTestDevice:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method public static callPollingStart()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 177
    sget-boolean v0, Lcom/policydm/agent/XDMTask;->g_IsDMInitialized:Z

    if-nez v0, :cond_0

    .line 179
    invoke-static {v1}, Lcom/policydm/XDMBroadcastReceiver;->xdmNotInitSetResume(I)V

    .line 195
    :goto_0
    return-void

    .line 183
    :cond_0
    invoke-static {}, Lcom/policydm/adapter/XSPDAdapter;->xspdIsSuperKeyEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 185
    const-string v0, "xspdIsSuperKeyEnabled()"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 188
    :cond_1
    invoke-static {}, Lcom/policydm/db/XDB;->xdbGetAutoUpdateFlag()Z

    move-result v0

    if-nez v0, :cond_2

    .line 190
    const-string v0, "AUTO_UPDATE is unset"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 193
    :cond_2
    invoke-static {v1}, Lcom/policydm/XDMBroadcastReceiver;->xdmExecResumeCase(I)V

    goto :goto_0
.end method

.method public static callPullStart()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 153
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmProtoIsWIFIConnected()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/policydm/XDMApplication;->xdmProtoIsMobileDataConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 155
    const/16 v0, 0x78

    invoke-static {v0}, Lcom/policydm/XDMApplication;->xdmSendMessageDmHandler(I)V

    .line 173
    :goto_0
    return-void

    .line 159
    :cond_0
    invoke-static {}, Lcom/policydm/adapter/XSPDAdapter;->xspdIsSuperKeyEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 161
    const-string v0, "xspdIsSuperKeyEnabled()"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 165
    :cond_1
    sget-boolean v0, Lcom/policydm/agent/XDMTask;->g_IsDMInitialized:Z

    if-nez v0, :cond_2

    .line 167
    invoke-static {v1}, Lcom/policydm/XDMBroadcastReceiver;->xdmNotInitSetResume(I)V

    goto :goto_0

    .line 171
    :cond_2
    invoke-static {v1}, Lcom/policydm/XDMBroadcastReceiver;->xdmExecResumeCase(I)V

    goto :goto_0
.end method

.method public static callPushIntent([B)V
    .locals 3
    .param p0, "data"    # [B

    .prologue
    const/4 v2, 0x3

    .line 114
    const-string v1, ""

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 116
    move-object v0, p0

    .line 118
    .local v0, "inbox":[B
    if-nez v0, :cond_1

    .line 120
    const-string v1, "PushData is null."

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 149
    :cond_0
    :goto_0
    return-void

    .line 124
    :cond_1
    invoke-static {}, Lcom/policydm/adapter/XSPDAdapter;->xspdIsSuperKeyEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 126
    const-string v1, "xspdIsSuperKeyEnabled()"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 130
    :cond_2
    invoke-static {v0}, Lcom/policydm/eng/core/XDMBase64;->xdmBase64Decode([B)[B

    move-result-object v0

    .line 131
    if-eqz v0, :cond_0

    .line 133
    const/4 v1, 0x2

    invoke-static {v1, v0}, Lcom/policydm/noti/XNOTIAdapter;->xnotiAddPushDataQueue(I[B)V

    .line 135
    sget-boolean v1, Lcom/policydm/agent/XDMTask;->g_IsDMInitialized:Z

    if-nez v1, :cond_3

    .line 137
    invoke-static {v2}, Lcom/policydm/XDMBroadcastReceiver;->xdmNotInitSetResume(I)V

    goto :goto_0

    .line 141
    :cond_3
    invoke-static {}, Lcom/policydm/noti/XNOTIAdapter;->xnotiGetNotiProcessing()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 143
    const-string v1, "Noti Processing..."

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 146
    :cond_4
    invoke-static {v2}, Lcom/policydm/XDMBroadcastReceiver;->xdmExecResumeCase(I)V

    goto :goto_0
.end method

.method public static callPushRegisterStart()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 484
    invoke-static {}, Lcom/policydm/XDMBroadcastReceiver;->getStartDeviceRegister()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 486
    const-string v0, "Already Device Registering... return"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 500
    :goto_0
    return-void

    .line 490
    :cond_0
    invoke-static {v1}, Lcom/policydm/XDMBroadcastReceiver;->setStartDeviceRegister(Z)V

    .line 491
    const/4 v0, 0x7

    invoke-static {v0}, Lcom/policydm/db/XDBSpdAdp;->xdbSetSpdState(I)V

    .line 492
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmUseSPP()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 494
    invoke-static {v1}, Lcom/policydm/push/XPushProcess;->sendMessage(I)V

    goto :goto_0

    .line 498
    :cond_1
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/policydm/push/XPushProcess;->sendMessage(I)V

    goto :goto_0
.end method

.method public static callRegisterStart()V
    .locals 2

    .prologue
    const/4 v1, 0x7

    .line 463
    sget-boolean v0, Lcom/policydm/agent/XDMTask;->g_IsDMInitialized:Z

    if-nez v0, :cond_1

    .line 465
    invoke-static {v1}, Lcom/policydm/XDMBroadcastReceiver;->xdmNotInitSetResume(I)V

    .line 480
    :cond_0
    :goto_0
    return-void

    .line 469
    :cond_1
    invoke-static {}, Lcom/policydm/XDMBroadcastReceiver;->xdmSPDFirstCheckOption()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 472
    invoke-static {}, Lcom/policydm/adapter/XSPDAdapter;->xspdIsSuperKeyEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 474
    const-string v0, "xspdIsSuperKeyEnabled()"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 478
    :cond_2
    invoke-static {v1}, Lcom/policydm/XDMBroadcastReceiver;->xdmExecResumeCase(I)V

    goto :goto_0
.end method

.method private static checkSettingValue()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    .line 526
    const-string v1, "GT-I9506"

    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetModel()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 528
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "spd_client_setting_check"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v3, :cond_1

    .line 530
    invoke-static {}, Lcom/policydm/db/XDB;->xdbGetAutoUpdateFlag()Z

    move-result v1

    if-nez v1, :cond_0

    .line 532
    invoke-static {v4}, Lcom/policydm/db/XDB;->xdbSetAutoUpdateFlag(Z)V

    .line 536
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "spd_client_setting_check"

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 544
    .local v0, "e":Ljava/lang/Exception;
    :cond_1
    :goto_0
    return-void

    .line 538
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_0
    move-exception v0

    .line 540
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getStartDeviceRegister()Z
    .locals 1

    .prologue
    .line 504
    sget-boolean v0, Lcom/policydm/XDMBroadcastReceiver;->bStartDeviceRegister:Z

    return v0
.end method

.method private static processEULAAgreement()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    .line 439
    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbSetSPDRegisterStatus(I)V

    .line 441
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "security_update_db"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 442
    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbSetAutoUpdateFlag(Z)V

    .line 443
    :cond_0
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "use_wifi_only_db"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v2, :cond_1

    .line 444
    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbSetWifiOnlyFlag(Z)V

    .line 445
    :cond_1
    invoke-static {}, Lcom/policydm/XDMBroadcastReceiver;->checkSettingValue()V

    .line 446
    const/4 v0, 0x7

    invoke-static {v0}, Lcom/policydm/ui/XUINotificationManager;->xuiSetIndicator(I)V

    .line 447
    invoke-static {}, Lcom/policydm/XDMBroadcastReceiver;->sendRegisterIntent()V

    .line 448
    return-void
.end method

.method private static sendRegisterIntent()V
    .locals 2

    .prologue
    .line 452
    invoke-static {}, Lcom/policydm/XDMBroadcastReceiver;->getStartDeviceRegister()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 454
    const-string v1, "Already Device Registering... return"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 459
    .local v0, "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 457
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.policydm.intent.action.SPD_REGISTER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 458
    .restart local v0    # "intent":Landroid/content/Intent;
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public static setStartDeviceRegister(Z)V
    .locals 0
    .param p0, "bStart"    # Z

    .prologue
    .line 509
    sput-boolean p0, Lcom/policydm/XDMBroadcastReceiver;->bStartDeviceRegister:Z

    .line 510
    return-void
.end method

.method public static xdmExecResumeCase(I)V
    .locals 9
    .param p0, "nResumeCase"    # I

    .prologue
    const/4 v8, 0x6

    const/4 v7, 0x3

    const/4 v6, 0x0

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 234
    const-string v3, ""

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 236
    invoke-static {}, Lcom/policydm/XDMBroadcastReceiver;->xdmSPDFirstCheckOption()Z

    move-result v3

    if-nez v3, :cond_1

    .line 238
    if-ne p0, v5, :cond_0

    .line 239
    const/16 v3, 0x7a

    invoke-static {v3}, Lcom/policydm/XDMApplication;->xdmSendMessageDmHandler(I)V

    .line 435
    :cond_0
    :goto_0
    return-void

    .line 243
    :cond_1
    invoke-static {}, Lcom/policydm/adapter/XSPDAdapter;->xspdIsSuperKeyEnabled()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 245
    const-string v3, "xspdIsSuperKeyEnabled()"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 249
    :cond_2
    if-ne p0, v4, :cond_9

    .line 251
    const-string v3, "Start resumecase for INIT"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 253
    invoke-static {}, Lcom/policydm/db/XDB;->xdbIsAlreadyAgree()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 256
    invoke-static {}, Lcom/policydm/db/XDBSpdAdp;->xdbGetSpdDeviceRegister()I

    move-result v3

    if-eq v3, v4, :cond_3

    .line 258
    invoke-static {}, Lcom/policydm/XDMBroadcastReceiver;->sendRegisterIntent()V

    goto :goto_0

    .line 262
    :cond_3
    invoke-static {}, Lcom/policydm/db/XDBSpdAdp;->xdbGetSpdPushRegId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    invoke-static {}, Lcom/policydm/db/XDBSpdAdp;->xdbGetSpdDeviceUpdate()I

    move-result v3

    if-eq v3, v4, :cond_5

    .line 264
    :cond_4
    invoke-static {}, Lcom/policydm/XDMBroadcastReceiver;->callPushRegisterStart()V

    goto :goto_0

    .line 268
    :cond_5
    sget-boolean v3, Lcom/policydm/agent/XDMTask;->g_IsDMInitialized:Z

    if-eqz v3, :cond_6

    invoke-static {}, Lcom/policydm/XDMApplication;->xdmProtoIsWIFIConnected()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 270
    invoke-static {}, Lcom/policydm/db/XDB;->xdbGetWaitWifiFlag()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 272
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMOInitiatedType()I

    move-result v3

    if-ne v3, v7, :cond_7

    .line 274
    invoke-static {}, Lcom/policydm/db/XDBNotiAdp;->xdbNotiGetInfo()Lcom/policydm/db/XDBNotiInfo;

    move-result-object v0

    .line 275
    .local v0, "notiInfo":Lcom/policydm/db/XDBNotiInfo;
    const/4 v3, 0x5

    iput v3, v0, Lcom/policydm/db/XDBNotiInfo;->uiMode:I

    .line 276
    const/16 v3, 0x1f

    invoke-static {v3, v0, v6}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 434
    .end local v0    # "notiInfo":Lcom/policydm/db/XDBNotiInfo;
    :cond_6
    :goto_1
    const/4 v3, 0x0

    invoke-static {v3}, Lcom/policydm/XDMBroadcastReceiver;->xdmSetResumeCase(I)V

    goto :goto_0

    .line 278
    :cond_7
    invoke-static {}, Lcom/policydm/ui/XUIAdapter;->xuiAdpStartSession()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 280
    const/16 v3, 0x65

    invoke-static {v6, v3}, Lcom/policydm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    .line 281
    const-wide/16 v3, 0x0

    invoke-static {v3, v4}, Lcom/policydm/db/XDBProfileListAdp;->xdbSetNotiJobId(J)V

    .line 282
    const/16 v3, 0x67

    invoke-static {v3}, Lcom/policydm/XDMApplication;->xdmSendMessageDmHandler(I)V

    .line 283
    invoke-static {}, Lcom/policydm/noti/XNOTIAdapter;->xnotiPushAdpDeleteAllNotiQueue()V

    goto :goto_1

    .line 288
    :cond_8
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmInitializing()V

    goto :goto_1

    .line 292
    :cond_9
    if-ne p0, v5, :cond_c

    .line 294
    const-string v3, "Start resumecase for PULL"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 296
    invoke-static {}, Lcom/policydm/db/XDBSpdAdp;->xdbGetSpdDeviceRegister()I

    move-result v3

    if-ne v3, v4, :cond_a

    .line 298
    invoke-static {v4}, Lcom/policydm/ui/XUIAdapter;->xuiAdpUserInitiate(I)V

    goto :goto_1

    .line 302
    :cond_a
    invoke-static {}, Lcom/policydm/db/XDB;->xdbIsAlreadyAgree()Z

    move-result v3

    if-nez v3, :cond_b

    .line 304
    invoke-static {v5}, Lcom/policydm/db/XDBSpdAdp;->xdbSetSpdDeviceRegister(I)V

    .line 305
    const/16 v3, 0xc8

    invoke-static {v3}, Lcom/policydm/XDMApplication;->xdmSendMessageDmHandler(I)V

    goto :goto_1

    .line 309
    :cond_b
    const-string v3, "Go SPD Device Registraion!!"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 310
    const/16 v3, 0x3c

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v3, v4, v6}, Lcom/policydm/eng/core/XDMMsg;->xdmSendRCMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    .line 314
    :cond_c
    if-ne p0, v7, :cond_e

    .line 316
    const-string v3, "Start resumecase for PUSH"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 318
    invoke-static {}, Lcom/policydm/db/XDBSpdAdp;->xdbGetSpdDeviceRegister()I

    move-result v3

    if-eq v3, v4, :cond_d

    sget-boolean v3, Lcom/policydm/XDMBroadcastReceiver;->m_bTestDevice:Z

    if-nez v3, :cond_d

    .line 320
    const-string v3, "Device is not Registerd"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 323
    :cond_d
    invoke-static {}, Lcom/policydm/noti/XNOTIAdapter;->xnotiPushDataHandling()V

    goto :goto_1

    .line 325
    :cond_e
    const/4 v3, 0x4

    if-ne p0, v3, :cond_10

    .line 327
    const-string v3, "Start resumecase for POLLING"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 329
    invoke-static {}, Lcom/policydm/db/XDBSpdAdp;->xdbGetSpdDeviceCreate()I

    move-result v3

    if-ne v3, v4, :cond_f

    .line 331
    invoke-static {v5}, Lcom/policydm/ui/XUIAdapter;->xuiAdpUserInitiate(I)V

    goto :goto_1

    .line 335
    :cond_f
    invoke-static {}, Lcom/policydm/db/XDB;->xdbIsAlreadyAgree()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 337
    const-string v3, "Go SPD Device Registraion!!"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 338
    const/16 v3, 0x3c

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v3, v4, v6}, Lcom/policydm/eng/core/XDMMsg;->xdmSendRCMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 342
    :cond_10
    const/4 v3, 0x7

    if-ne p0, v3, :cond_12

    .line 344
    const-string v3, "Start resumecase for REGISTER"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 346
    invoke-static {}, Lcom/policydm/db/XDBSpdAdp;->xdbGetSpdDeviceRegister()I

    move-result v3

    if-eq v3, v4, :cond_11

    .line 348
    const-string v3, "Go SPD Device Registraion!!"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 349
    const/16 v3, 0x3c

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v3, v4, v6}, Lcom/policydm/eng/core/XDMMsg;->xdmSendRCMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 353
    :cond_11
    const-string v3, "REGISTER is already started."

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 356
    :cond_12
    const/4 v3, 0x5

    if-ne p0, v3, :cond_17

    .line 358
    const-string v3, "Start resumecase for POLLINGTIME"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 360
    const/4 v3, 0x0

    invoke-static {v3}, Lcom/policydm/XDMApplication;->xdmStopAlarm(I)V

    .line 362
    invoke-static {}, Lcom/policydm/db/XDBSpdAdp;->xdbGetSpdEulaTime()J

    move-result-wide v1

    .line 363
    .local v1, "time":J
    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-nez v3, :cond_13

    .line 365
    invoke-static {}, Lcom/policydm/db/XDB;->xdbGetEULAAgreement()Z

    move-result v3

    if-eqz v3, :cond_14

    .line 367
    invoke-static {}, Lcom/policydm/XDMBroadcastReceiver;->processEULAAgreement()V

    .line 380
    :cond_13
    :goto_2
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmProtoIsWIFIConnected()Z

    move-result v3

    if-nez v3, :cond_15

    invoke-static {}, Lcom/policydm/XDMApplication;->xdmProtoIsMobileDataConnected()Z

    move-result v3

    if-nez v3, :cond_15

    .line 382
    const-string v3, "network is not ready"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 383
    invoke-static {}, Lcom/policydm/polling/XPollingAgent;->xpollingDefaultPollingTime()V

    goto/16 :goto_1

    .line 371
    :cond_14
    invoke-static {}, Lcom/policydm/db/XDB;->xdbIsAlreadyAgree()Z

    move-result v3

    if-nez v3, :cond_13

    .line 373
    invoke-static {v8}, Lcom/policydm/ui/XUINotificationManager;->xuiSetIndicator(I)V

    .line 374
    const-wide/16 v3, -0x1

    invoke-static {v3, v4}, Lcom/policydm/db/XDBSpdAdp;->xdbSetSpdEulaTime(J)V

    goto :goto_2

    .line 385
    :cond_15
    invoke-static {}, Lcom/policydm/db/XDB;->xdbGetAutoUpdateFlag()Z

    move-result v3

    if-nez v3, :cond_16

    .line 387
    const-string v3, "AUTO_UPDATE is unset"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 388
    invoke-static {}, Lcom/policydm/polling/XPollingAgent;->xpollingDefaultPollingTime()V

    goto/16 :goto_1

    .line 392
    :cond_16
    invoke-static {}, Lcom/policydm/polling/XPollingAgent;->xpollingCheckVersionInfo()Z

    goto/16 :goto_1

    .line 395
    .end local v1    # "time":J
    :cond_17
    if-ne p0, v8, :cond_1b

    .line 397
    const-string v3, "Start resumecase for POLLING REPORTTIME"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 399
    invoke-static {}, Lcom/policydm/polling/XPollingAgent;->xPollingReportReStartAlarm()V

    .line 400
    invoke-static {}, Lcom/policydm/db/XDBSpdAdp;->xdbGetSpdDeviceRegister()I

    move-result v3

    if-ne v3, v4, :cond_1a

    .line 402
    invoke-static {}, Lcom/policydm/adapter/XSPDAdapter;->xspdIsSuperKeyEnabled()Z

    move-result v3

    if-eqz v3, :cond_18

    .line 404
    const-string v3, "xspdIsSuperKeyEnabled()"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 406
    :cond_18
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmProtoIsWIFIConnected()Z

    move-result v3

    if-nez v3, :cond_19

    invoke-static {}, Lcom/policydm/XDMApplication;->xdmProtoIsMobileDataConnected()Z

    move-result v3

    if-nez v3, :cond_19

    .line 408
    const-string v3, "network is not ready"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 412
    :cond_19
    const/16 v3, 0x3d

    invoke-static {v3, v6, v6}, Lcom/policydm/eng/core/XDMMsg;->xdmSendRCMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 417
    :cond_1a
    const-string v3, "Device is not Registerd. restart polling report time"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 420
    :cond_1b
    const/16 v3, 0x8

    if-ne p0, v3, :cond_6

    .line 422
    const-string v3, "Start resumecase for EULATIME"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 423
    invoke-static {v5}, Lcom/policydm/XDMApplication;->xdmStopAlarm(I)V

    .line 425
    invoke-static {}, Lcom/policydm/db/XDB;->xdbIsAlreadyAgree()Z

    move-result v3

    if-nez v3, :cond_6

    .line 427
    const/4 v3, 0x7

    invoke-static {v3}, Lcom/policydm/ui/XUINotificationManager;->xuiSetIndicator(I)V

    .line 428
    invoke-static {v8}, Lcom/policydm/ui/XUINotificationManager;->xuiSetIndicator(I)V

    .line 429
    invoke-static {v5}, Lcom/policydm/db/XDBSpdAdp;->xdbSetSpdDeviceRegister(I)V

    .line 430
    const-wide/16 v3, 0x0

    invoke-static {v5, v3, v4}, Lcom/policydm/XDMApplication;->xdmStartAlarm(IJ)V

    goto/16 :goto_1
.end method

.method public static xdmGetCheckedIntent()Z
    .locals 2

    .prologue
    .line 520
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "b_AlreadyReceivedIntent : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/policydm/XDMBroadcastReceiver;->bAlreadyReceivedIntent:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 521
    sget-boolean v0, Lcom/policydm/XDMBroadcastReceiver;->bAlreadyReceivedIntent:Z

    return v0
.end method

.method public static xdmGetResumeCase()I
    .locals 1

    .prologue
    .line 224
    sget v0, Lcom/policydm/XDMBroadcastReceiver;->g_ResumeCase:I

    return v0
.end method

.method public static xdmNotInitSetResume(I)V
    .locals 1
    .param p0, "nResumeCase"    # I

    .prologue
    .line 217
    const-string v0, "DM Not Init"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 219
    invoke-static {p0}, Lcom/policydm/XDMBroadcastReceiver;->xdmSetResumeCase(I)V

    .line 220
    return-void
.end method

.method public static xdmSPDFirstCheckOption()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x1

    .line 199
    invoke-static {}, Lcom/policydm/adapter/XSPDAdapter;->xspdGetPolicyMode()Ljava/lang/String;

    move-result-object v1

    const-string v2, "enforcing"

    if-eq v1, v2, :cond_1

    .line 201
    const-string v1, "Policymode is not enforcing!! return"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 202
    invoke-static {}, Lcom/policydm/db/XDBSpdAdp;->xdbGetSpdDeviceRegister()I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 204
    invoke-static {}, Lcom/policydm/adapter/XSPDAdapter;->xspdIsSuperKeyEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 206
    const-string v0, "but device registerd. go device update"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 207
    const/16 v0, 0x3d

    invoke-static {v0, v3, v3}, Lcom/policydm/eng/core/XDMMsg;->xdmSendRCMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 210
    :cond_0
    const/4 v0, 0x0

    .line 212
    :cond_1
    return v0
.end method

.method public static xdmSetCheckedIntent(Z)V
    .locals 2
    .param p0, "bIntent"    # Z

    .prologue
    .line 514
    sput-boolean p0, Lcom/policydm/XDMBroadcastReceiver;->bAlreadyReceivedIntent:Z

    .line 515
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "b_AlreadyReceivedIntent : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/policydm/XDMBroadcastReceiver;->bAlreadyReceivedIntent:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 516
    return-void
.end method

.method public static xdmSetResumeCase(I)V
    .locals 0
    .param p0, "nCase"    # I

    .prologue
    .line 229
    sput p0, Lcom/policydm/XDMBroadcastReceiver;->g_ResumeCase:I

    .line 230
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, -0x1

    const/4 v4, 0x1

    .line 47
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 48
    .local v0, "action":Ljava/lang/String;
    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 50
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v2

    if-eqz v2, :cond_1

    .line 52
    const-string v2, "User mode!! return"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 110
    :cond_0
    :goto_0
    return-void

    .line 56
    :cond_1
    const-string v2, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 58
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "use_wifi_only_db"

    invoke-static {v2, v3, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v5, :cond_2

    .line 60
    invoke-static {v4}, Lcom/policydm/db/XDB;->xdbSetWifiOnlyFlag(Z)V

    .line 62
    :cond_2
    invoke-static {}, Lcom/policydm/db/XDB;->xdbIsAlreadyAgree()Z

    move-result v2

    if-nez v2, :cond_3

    .line 64
    invoke-static {}, Lcom/policydm/XDMBroadcastReceiver;->xdmSPDFirstCheckOption()Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    .line 71
    :cond_3
    invoke-static {}, Lcom/policydm/XDMBroadcastReceiver;->checkSettingValue()V

    goto :goto_0

    .line 74
    :cond_4
    const-string v2, "com.sec.android.app.secsetupwizard.SETUPWIZARD_COMPLETE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 76
    invoke-static {}, Lcom/policydm/db/XDB;->xdbGetEULAAgreement()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 78
    invoke-static {}, Lcom/policydm/XDMBroadcastReceiver;->processEULAAgreement()V

    goto :goto_0

    .line 81
    :cond_5
    const-string v2, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 83
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetNotibarState()I

    move-result v1

    .line 84
    .local v1, "nNotibar":I
    invoke-static {v1}, Lcom/policydm/ui/XUINotificationManager;->xuiSetIndicator(I)V

    goto :goto_0

    .line 86
    .end local v1    # "nNotibar":I
    :cond_6
    const-string v2, "android.net.ConnectivityManager.CONNECTIVITY_ACTION"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    const-string v2, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 88
    :cond_7
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmProtoIsWIFIConnected()Z

    move-result v2

    if-nez v2, :cond_8

    invoke-static {}, Lcom/policydm/XDMApplication;->xdmProtoIsMobileDataConnected()Z

    move-result v2

    if-nez v2, :cond_8

    .line 90
    const-string v2, "network is unserviceable"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 91
    const/4 v2, 0x0

    sput v2, Lcom/policydm/XDMApplication;->nCurrentNetwork:I

    .line 92
    invoke-static {}, Lcom/policydm/XDMApplication;->isNetworkChanged()Z

    goto :goto_0

    .line 96
    :cond_8
    invoke-static {}, Lcom/policydm/XDMApplication;->isNetworkChanged()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 98
    sget-boolean v2, Lcom/policydm/agent/XDMTask;->g_IsDMInitialized:Z

    if-nez v2, :cond_9

    .line 100
    invoke-static {v4}, Lcom/policydm/XDMBroadcastReceiver;->xdmNotInitSetResume(I)V

    .line 101
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmInitializing()V

    goto/16 :goto_0

    .line 105
    :cond_9
    invoke-static {v4}, Lcom/policydm/XDMBroadcastReceiver;->xdmExecResumeCase(I)V

    goto/16 :goto_0
.end method
