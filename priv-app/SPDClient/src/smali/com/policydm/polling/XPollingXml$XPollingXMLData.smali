.class public Lcom/policydm/polling/XPollingXml$XPollingXMLData;
.super Ljava/lang/Object;
.source "XPollingXml.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/policydm/polling/XPollingXml;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "XPollingXMLData"
.end annotation


# instance fields
.field androidver:[Ljava/lang/String;

.field filename:Ljava/lang/String;

.field orgurl:Ljava/lang/String;

.field period:Ljava/lang/String;

.field periodunit:Ljava/lang/String;

.field policyver:[Ljava/lang/String;

.field range:Ljava/lang/String;

.field reportperiod:Ljava/lang/String;

.field reportrange:Ljava/lang/String;

.field reporttime:Ljava/lang/String;

.field time:Ljava/lang/String;

.field url:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object v0, p0, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->androidver:[Ljava/lang/String;

    .line 40
    iput-object v0, p0, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->policyver:[Ljava/lang/String;

    .line 41
    iput-object v0, p0, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->url:Ljava/lang/String;

    .line 42
    iput-object v0, p0, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->orgurl:Ljava/lang/String;

    .line 43
    iput-object v0, p0, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->filename:Ljava/lang/String;

    .line 44
    iput-object v0, p0, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->periodunit:Ljava/lang/String;

    .line 45
    iput-object v0, p0, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->period:Ljava/lang/String;

    .line 46
    iput-object v0, p0, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->time:Ljava/lang/String;

    .line 47
    iput-object v0, p0, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->range:Ljava/lang/String;

    .line 48
    iput-object v0, p0, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->reportperiod:Ljava/lang/String;

    .line 49
    iput-object v0, p0, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->reporttime:Ljava/lang/String;

    .line 50
    iput-object v0, p0, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->reportrange:Ljava/lang/String;

    return-void
.end method
