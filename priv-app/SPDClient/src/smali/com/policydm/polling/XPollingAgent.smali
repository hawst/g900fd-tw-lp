.class public Lcom/policydm/polling/XPollingAgent;
.super Ljava/lang/Object;
.source "XPollingAgent.java"

# interfaces
.implements Lcom/policydm/interfaces/XCommonInterface;
.implements Lcom/policydm/interfaces/XEventInterface;
.implements Lcom/policydm/interfaces/XPollingInterface;


# instance fields
.field private mFilename:Ljava/lang/String;

.field private mOrgUrl:Ljava/lang/String;

.field private mPeriod:I

.field private mPeriodUnit:Ljava/lang/String;

.field private mRange:I

.field private mReportPeriod:I

.field private mReportRange:I

.field private mReportTime:I

.field private mTime:I

.field private mUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xPollingReportReStartAlarm()V
    .locals 4

    .prologue
    .line 311
    const-string v3, ""

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 313
    new-instance v2, Lcom/policydm/polling/XPollingAgent;

    invoke-direct {v2}, Lcom/policydm/polling/XPollingAgent;-><init>()V

    .line 314
    .local v2, "pollingAgent":Lcom/policydm/polling/XPollingAgent;
    invoke-virtual {v2}, Lcom/policydm/polling/XPollingAgent;->xpollingNextReportTime()J

    move-result-wide v0

    .line 316
    .local v0, "NextReportAlarm":J
    invoke-static {v0, v1}, Lcom/policydm/db/XDBPollingAdp;->xdbSetPollingStatusReportTime(J)V

    .line 317
    const/4 v3, 0x1

    invoke-static {v3, v0, v1}, Lcom/policydm/XDMApplication;->xdmStartAlarm(IJ)V

    .line 318
    return-void
.end method

.method public static xpollingCheckTimer()Z
    .locals 9

    .prologue
    .line 265
    const/4 v0, 0x0

    .line 266
    .local v0, "bRtn":Z
    const-wide/16 v1, 0x0

    .line 267
    .local v1, "currentTime":J
    const-wide/16 v4, 0x0

    .line 268
    .local v4, "pollingTime":J
    new-instance v3, Lcom/policydm/db/XDBPollingInfo;

    invoke-direct {v3}, Lcom/policydm/db/XDBPollingInfo;-><init>()V

    .line 270
    .local v3, "pollingInfo":Lcom/policydm/db/XDBPollingInfo;
    const-string v6, ""

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 272
    invoke-static {}, Lcom/policydm/db/XDBPollingAdp;->xdbGetPollingInfo()Lcom/policydm/db/XDBPollingInfo;

    move-result-object v3

    .line 273
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 275
    if-eqz v3, :cond_2

    .line 277
    iget-wide v4, v3, Lcom/policydm/db/XDBPollingInfo;->m_nextpollingtime:J

    .line 279
    const-wide/16 v6, 0x0

    cmp-long v6, v4, v6

    if-nez v6, :cond_0

    .line 281
    invoke-static {}, Lcom/policydm/polling/XPollingAgent;->xpollingDefaultPollingTime()V

    .line 282
    const/4 v0, 0x1

    .line 306
    .end local v0    # "bRtn":Z
    :goto_0
    return v0

    .line 284
    .restart local v0    # "bRtn":Z
    :cond_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "savedpollingtime : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v4, v5}, Lcom/policydm/XDMApplication;->xdmDisplayAlarmTime(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 285
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "currentTime : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v1, v2}, Lcom/policydm/XDMApplication;->xdmDisplayAlarmTime(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 287
    cmp-long v6, v4, v1

    if-lez v6, :cond_1

    .line 289
    const-string v6, "Restart Timer.."

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 290
    const/4 v6, 0x0

    iget-wide v7, v3, Lcom/policydm/db/XDBPollingInfo;->m_nextpollingtime:J

    invoke-static {v6, v7, v8}, Lcom/policydm/XDMApplication;->xdmStartAlarm(IJ)V

    .line 292
    invoke-static {v3}, Lcom/policydm/db/XDBPollingAdp;->xdbSetPollingInfo(Lcom/policydm/db/XDBPollingInfo;)V

    .line 293
    const/4 v0, 0x1

    goto :goto_0

    .line 297
    :cond_1
    invoke-static {}, Lcom/policydm/polling/XPollingAgent;->xpollingDefaultPollingTime()V

    goto :goto_0

    .line 302
    :cond_2
    invoke-static {}, Lcom/policydm/polling/XPollingAgent;->xpollingDefaultPollingTime()V

    .line 303
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static xpollingCheckVersionInfo()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 71
    const-string v1, ""

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 73
    invoke-static {}, Lcom/policydm/polling/XPollingAgent;->xpollingCheckTimer()Z

    move-result v1

    if-nez v1, :cond_0

    .line 75
    invoke-static {}, Lcom/policydm/adapter/XSPDAdapter;->xspdIsSuperKeyEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 77
    invoke-static {}, Lcom/policydm/polling/XPollingAgent;->xpollingDefaultPollingTime()V

    .line 78
    const-string v1, " xspdIsSuperKeyEnabled()"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 85
    :cond_0
    :goto_0
    return v0

    .line 81
    :cond_1
    invoke-static {}, Lcom/policydm/XDMBroadcastReceiver;->xdmSPDFirstCheckOption()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 82
    const/16 v0, 0x41

    invoke-static {v0, v2, v2}, Lcom/policydm/eng/core/XDMMsg;->xdmSendRCMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 83
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static xpollingDefaultPollingTime()V
    .locals 11

    .prologue
    .line 322
    const-wide/16 v3, 0x0

    .line 323
    .local v3, "defaultpollingTime":J
    const-string v8, ""

    invoke-static {v8}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 327
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 328
    .local v1, "currentTime":J
    new-instance v7, Ljava/util/Random;

    invoke-direct {v7}, Ljava/util/Random;-><init>()V

    .line 329
    .local v7, "rnd":Ljava/util/Random;
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    .line 330
    .local v0, "c":Ljava/util/GregorianCalendar;
    invoke-virtual {v0, v1, v2}, Ljava/util/GregorianCalendar;->setTimeInMillis(J)V

    .line 331
    const/4 v8, 0x5

    const/4 v9, 0x1

    invoke-virtual {v0, v8, v9}, Ljava/util/GregorianCalendar;->add(II)V

    .line 332
    const/16 v8, 0xb

    const/16 v9, 0x9

    invoke-virtual {v7, v9}, Ljava/util/Random;->nextInt(I)I

    move-result v9

    add-int/lit8 v9, v9, 0xf

    invoke-virtual {v0, v8, v9}, Ljava/util/GregorianCalendar;->set(II)V

    .line 333
    const/16 v8, 0xc

    const/16 v9, 0x3c

    invoke-virtual {v7, v9}, Ljava/util/Random;->nextInt(I)I

    move-result v9

    invoke-virtual {v0, v8, v9}, Ljava/util/GregorianCalendar;->set(II)V

    .line 334
    const/16 v8, 0xd

    const/16 v9, 0x3c

    invoke-virtual {v7, v9}, Ljava/util/Random;->nextInt(I)I

    move-result v9

    invoke-virtual {v0, v8, v9}, Ljava/util/GregorianCalendar;->set(II)V

    .line 335
    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTimeInMillis()J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v3

    .line 342
    .end local v0    # "c":Ljava/util/GregorianCalendar;
    .end local v1    # "currentTime":J
    .end local v7    # "rnd":Ljava/util/Random;
    :goto_0
    new-instance v6, Lcom/policydm/db/XDBPollingInfo;

    invoke-direct {v6}, Lcom/policydm/db/XDBPollingInfo;-><init>()V

    .line 343
    .local v6, "pollingInfo":Lcom/policydm/db/XDBPollingInfo;
    invoke-static {}, Lcom/policydm/db/XDBPollingAdp;->xdbGetPollingInfo()Lcom/policydm/db/XDBPollingInfo;

    move-result-object v6

    .line 345
    iput-wide v3, v6, Lcom/policydm/db/XDBPollingInfo;->m_nextpollingtime:J

    .line 346
    const/4 v8, 0x0

    iget-wide v9, v6, Lcom/policydm/db/XDBPollingInfo;->m_nextpollingtime:J

    invoke-static {v8, v9, v10}, Lcom/policydm/XDMApplication;->xdmStartAlarm(IJ)V

    .line 347
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Next Polling Time:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-wide v9, v6, Lcom/policydm/db/XDBPollingInfo;->m_nextpollingtime:J

    invoke-static {v9, v10}, Lcom/policydm/XDMApplication;->xdmDisplayAlarmTime(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 349
    invoke-static {v6}, Lcom/policydm/db/XDBPollingAdp;->xdbSetPollingInfo(Lcom/policydm/db/XDBPollingInfo;)V

    .line 350
    return-void

    .line 337
    .end local v6    # "pollingInfo":Lcom/policydm/db/XDBPollingInfo;
    :catch_0
    move-exception v5

    .line 339
    .local v5, "e":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static xpollingMakeOrgUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 49
    invoke-static {}, Lcom/policydm/db/XDBPollingAdp;->xdbGetPollingOrgVersionUrl()Ljava/lang/String;

    move-result-object v0

    .line 50
    .local v0, "pollingurl":Ljava/lang/String;
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetModel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 51
    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 52
    invoke-static {}, Lcom/policydm/db/XDBPollingAdp;->xdbGetPollingVersionFile()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 54
    return-object v0
.end method

.method public static xpollingMakeUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 59
    invoke-static {}, Lcom/policydm/db/XDBPollingAdp;->xdbGetPollingVersionUrl()Ljava/lang/String;

    move-result-object v0

    .line 60
    .local v0, "pollingurl":Ljava/lang/String;
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetModel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 61
    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 62
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetCustomCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 63
    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 64
    invoke-static {}, Lcom/policydm/db/XDBPollingAdp;->xdbGetPollingVersionFile()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 66
    return-object v0
.end method

.method public static xpollingUpdateInfo()V
    .locals 2

    .prologue
    .line 38
    invoke-static {}, Lcom/policydm/db/XDBPollingAdp;->xdbGetPollingInfo()Lcom/policydm/db/XDBPollingInfo;

    move-result-object v0

    .line 40
    .local v0, "pollingInfo":Lcom/policydm/db/XDBPollingInfo;
    invoke-static {}, Lcom/policydm/spd/XSPDXml;->getResponseConfigPollingFileName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/policydm/db/XDBPollingInfo;->m_szFileName:Ljava/lang/String;

    .line 41
    invoke-static {}, Lcom/policydm/spd/XSPDXml;->getResponseConfigPollingOrgServerUrl()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/policydm/db/XDBPollingInfo;->m_szOrgVersionUrl:Ljava/lang/String;

    .line 42
    invoke-static {}, Lcom/policydm/spd/XSPDXml;->getResponseConfigPollingServerUrl()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/policydm/db/XDBPollingInfo;->m_szVersionUrl:Ljava/lang/String;

    .line 44
    invoke-static {v0}, Lcom/policydm/db/XDBPollingAdp;->xdbSetPollingInfo(Lcom/policydm/db/XDBPollingInfo;)V

    .line 45
    return-void
.end method


# virtual methods
.method public xpollingNextPollingTime()J
    .locals 14

    .prologue
    const-wide/16 v12, 0x3e8

    const-wide/16 v10, 0x3c

    .line 173
    const-wide/16 v4, 0x0

    .line 177
    .local v4, "nextpollingTime":J
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 178
    .local v1, "currentTime":J
    new-instance v7, Ljava/util/Random;

    invoke-direct {v7}, Ljava/util/Random;-><init>()V

    .line 179
    .local v7, "rnd":Ljava/util/Random;
    const-string v8, "hour"

    iget-object v9, p0, Lcom/policydm/polling/XPollingAgent;->mPeriodUnit:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 181
    iget v8, p0, Lcom/policydm/polling/XPollingAgent;->mPeriod:I

    int-to-long v8, v8

    mul-long/2addr v8, v10

    mul-long/2addr v8, v10

    mul-long/2addr v8, v12

    add-long v4, v1, v8

    .line 204
    :goto_0
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Next Polling Time:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {v4, v5}, Lcom/policydm/XDMApplication;->xdmDisplayAlarmTime(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 210
    .end local v1    # "currentTime":J
    .end local v7    # "rnd":Ljava/util/Random;
    :goto_1
    return-wide v4

    .line 183
    .restart local v1    # "currentTime":J
    .restart local v7    # "rnd":Ljava/util/Random;
    :cond_0
    const-string v8, "min"

    iget-object v9, p0, Lcom/policydm/polling/XPollingAgent;->mPeriodUnit:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 185
    iget v8, p0, Lcom/policydm/polling/XPollingAgent;->mPeriod:I

    int-to-long v8, v8

    mul-long/2addr v8, v10

    mul-long/2addr v8, v12

    add-long v4, v1, v8

    goto :goto_0

    .line 189
    :cond_1
    iget v6, p0, Lcom/policydm/polling/XPollingAgent;->mRange:I

    .line 190
    .local v6, "range":I
    if-nez v6, :cond_2

    .line 192
    const-string v8, "Range can\'t be zero. set to default range, 1"

    invoke-static {v8}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 193
    const/4 v6, 0x1

    .line 195
    :cond_2
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    .line 196
    .local v0, "c":Ljava/util/GregorianCalendar;
    invoke-virtual {v0, v1, v2}, Ljava/util/GregorianCalendar;->setTimeInMillis(J)V

    .line 197
    const/4 v8, 0x5

    iget v9, p0, Lcom/policydm/polling/XPollingAgent;->mPeriod:I

    invoke-virtual {v0, v8, v9}, Ljava/util/GregorianCalendar;->add(II)V

    .line 198
    const/16 v8, 0xb

    iget v9, p0, Lcom/policydm/polling/XPollingAgent;->mTime:I

    invoke-virtual {v7, v6}, Ljava/util/Random;->nextInt(I)I

    move-result v10

    add-int/2addr v9, v10

    invoke-virtual {v0, v8, v9}, Ljava/util/GregorianCalendar;->set(II)V

    .line 199
    const/16 v8, 0xc

    const/16 v9, 0x3c

    invoke-virtual {v7, v9}, Ljava/util/Random;->nextInt(I)I

    move-result v9

    invoke-virtual {v0, v8, v9}, Ljava/util/GregorianCalendar;->set(II)V

    .line 200
    const/16 v8, 0xd

    const/16 v9, 0x3c

    invoke-virtual {v7, v9}, Ljava/util/Random;->nextInt(I)I

    move-result v9

    invoke-virtual {v0, v8, v9}, Ljava/util/GregorianCalendar;->set(II)V

    .line 201
    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTimeInMillis()J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v4

    goto :goto_0

    .line 206
    .end local v0    # "c":Ljava/util/GregorianCalendar;
    .end local v1    # "currentTime":J
    .end local v6    # "range":I
    .end local v7    # "rnd":Ljava/util/Random;
    :catch_0
    move-exception v3

    .line 208
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public xpollingNextReportTime()J
    .locals 14

    .prologue
    .line 215
    const-wide/16 v7, 0x0

    .line 217
    .local v7, "nextreportTime":J
    iget v4, p0, Lcom/policydm/polling/XPollingAgent;->mReportPeriod:I

    .line 218
    .local v4, "nReportPeriod":I
    iget v6, p0, Lcom/policydm/polling/XPollingAgent;->mReportTime:I

    .line 219
    .local v6, "nReportTime":I
    iget v5, p0, Lcom/policydm/polling/XPollingAgent;->mReportRange:I

    .line 221
    .local v5, "nReportRange":I
    if-nez v4, :cond_0

    .line 222
    invoke-static {}, Lcom/policydm/db/XDBPollingAdp;->xdbGetPollingReportPeriod()I

    move-result v4

    .line 223
    :cond_0
    if-nez v6, :cond_1

    .line 224
    invoke-static {}, Lcom/policydm/db/XDBPollingAdp;->xdbGetPollingReportTime()I

    move-result v6

    .line 225
    :cond_1
    if-nez v5, :cond_2

    .line 226
    invoke-static {}, Lcom/policydm/db/XDBPollingAdp;->xdbGetPollingReportRange()I

    move-result v5

    .line 230
    :cond_2
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 232
    .local v1, "currentTime":J
    new-instance v9, Lcom/policydm/db/XDBPollingInfo;

    invoke-direct {v9}, Lcom/policydm/db/XDBPollingInfo;-><init>()V

    .line 233
    .local v9, "pollingInfo":Lcom/policydm/db/XDBPollingInfo;
    invoke-static {}, Lcom/policydm/db/XDBPollingAdp;->xdbGetPollingInfo()Lcom/policydm/db/XDBPollingInfo;

    move-result-object v9

    .line 235
    iget-wide v12, v9, Lcom/policydm/db/XDBPollingInfo;->m_nextreporttime:J

    cmp-long v12, v1, v12

    if-gez v12, :cond_3

    .line 237
    iget-wide v12, v9, Lcom/policydm/db/XDBPollingInfo;->m_nextreporttime:J

    .line 260
    .end local v1    # "currentTime":J
    .end local v9    # "pollingInfo":Lcom/policydm/db/XDBPollingInfo;
    :goto_0
    return-wide v12

    .line 239
    .restart local v1    # "currentTime":J
    .restart local v9    # "pollingInfo":Lcom/policydm/db/XDBPollingInfo;
    :cond_3
    new-instance v11, Ljava/util/Random;

    invoke-direct {v11}, Ljava/util/Random;-><init>()V

    .line 240
    .local v11, "rnd":Ljava/util/Random;
    move v10, v5

    .line 241
    .local v10, "range":I
    if-nez v10, :cond_4

    .line 243
    const-string v12, "Range can\'t be zero. set to default range, 1"

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 244
    const/4 v10, 0x1

    .line 246
    :cond_4
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    .line 247
    .local v0, "c":Ljava/util/GregorianCalendar;
    invoke-virtual {v0, v1, v2}, Ljava/util/GregorianCalendar;->setTimeInMillis(J)V

    .line 248
    const/4 v12, 0x5

    invoke-virtual {v0, v12, v4}, Ljava/util/GregorianCalendar;->add(II)V

    .line 249
    const/16 v12, 0xb

    invoke-virtual {v11, v10}, Ljava/util/Random;->nextInt(I)I

    move-result v13

    add-int/2addr v13, v6

    invoke-virtual {v0, v12, v13}, Ljava/util/GregorianCalendar;->set(II)V

    .line 250
    const/16 v12, 0xc

    const/16 v13, 0x3c

    invoke-virtual {v11, v13}, Ljava/util/Random;->nextInt(I)I

    move-result v13

    invoke-virtual {v0, v12, v13}, Ljava/util/GregorianCalendar;->set(II)V

    .line 251
    const/16 v12, 0xd

    const/16 v13, 0x3c

    invoke-virtual {v11, v13}, Ljava/util/Random;->nextInt(I)I

    move-result v13

    invoke-virtual {v0, v12, v13}, Ljava/util/GregorianCalendar;->set(II)V

    .line 252
    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v7

    .line 254
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Next StatusReport Time:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {v7, v8}, Lcom/policydm/XDMApplication;->xdmDisplayAlarmTime(J)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v0    # "c":Ljava/util/GregorianCalendar;
    .end local v1    # "currentTime":J
    .end local v9    # "pollingInfo":Lcom/policydm/db/XDBPollingInfo;
    .end local v10    # "range":I
    .end local v11    # "rnd":Ljava/util/Random;
    :goto_1
    move-wide v12, v7

    .line 260
    goto :goto_0

    .line 256
    :catch_0
    move-exception v3

    .line 258
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public xpollingSaveXMLData(Lcom/policydm/polling/XPollingXml$XPollingXMLData;)V
    .locals 5
    .param p1, "xmldata"    # Lcom/policydm/polling/XPollingXml$XPollingXMLData;

    .prologue
    .line 90
    iget-object v1, p1, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->url:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 91
    invoke-static {}, Lcom/policydm/db/XDBPollingAdp;->xdbGetPollingVersionUrl()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/policydm/polling/XPollingAgent;->mUrl:Ljava/lang/String;

    .line 95
    :goto_0
    iget-object v1, p1, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->orgurl:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 96
    invoke-static {}, Lcom/policydm/db/XDBPollingAdp;->xdbGetPollingOrgVersionUrl()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/policydm/polling/XPollingAgent;->mOrgUrl:Ljava/lang/String;

    .line 100
    :goto_1
    iget-object v1, p1, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->filename:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 101
    invoke-static {}, Lcom/policydm/db/XDBPollingAdp;->xdbGetPollingVersionFile()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/policydm/polling/XPollingAgent;->mFilename:Ljava/lang/String;

    .line 105
    :goto_2
    iget-object v1, p1, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->periodunit:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 106
    invoke-static {}, Lcom/policydm/db/XDBPollingAdp;->xdbGetPollingPeriodUnit()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/policydm/polling/XPollingAgent;->mPeriodUnit:Ljava/lang/String;

    .line 110
    :goto_3
    iget-object v1, p1, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->period:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-gtz v1, :cond_4

    .line 111
    invoke-static {}, Lcom/policydm/db/XDBPollingAdp;->xdbGetPollingPeriod()I

    move-result v1

    iput v1, p0, Lcom/policydm/polling/XPollingAgent;->mPeriod:I

    .line 115
    :goto_4
    iget-object v1, p1, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->time:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-gtz v1, :cond_5

    .line 116
    invoke-static {}, Lcom/policydm/db/XDBPollingAdp;->xdbGetPollingTime()I

    move-result v1

    iput v1, p0, Lcom/policydm/polling/XPollingAgent;->mTime:I

    .line 120
    :goto_5
    iget-object v1, p1, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->range:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-gtz v1, :cond_6

    .line 121
    invoke-static {}, Lcom/policydm/db/XDBPollingAdp;->xdbGetPollingRange()I

    move-result v1

    iput v1, p0, Lcom/policydm/polling/XPollingAgent;->mRange:I

    .line 125
    :goto_6
    iget-object v1, p1, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->reportperiod:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-gtz v1, :cond_7

    .line 126
    invoke-static {}, Lcom/policydm/db/XDBPollingAdp;->xdbGetPollingReportPeriod()I

    move-result v1

    iput v1, p0, Lcom/policydm/polling/XPollingAgent;->mReportPeriod:I

    .line 130
    :goto_7
    iget-object v1, p1, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->reporttime:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-gtz v1, :cond_8

    .line 131
    invoke-static {}, Lcom/policydm/db/XDBPollingAdp;->xdbGetPollingReportTime()I

    move-result v1

    iput v1, p0, Lcom/policydm/polling/XPollingAgent;->mReportTime:I

    .line 135
    :goto_8
    iget-object v1, p1, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->reportrange:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-gtz v1, :cond_9

    .line 136
    invoke-static {}, Lcom/policydm/db/XDBPollingAdp;->xdbGetPollingReportRange()I

    move-result v1

    iput v1, p0, Lcom/policydm/polling/XPollingAgent;->mReportRange:I

    .line 141
    :goto_9
    new-instance v0, Lcom/policydm/db/XDBPollingInfo;

    invoke-direct {v0}, Lcom/policydm/db/XDBPollingInfo;-><init>()V

    .line 142
    .local v0, "pollingInfo":Lcom/policydm/db/XDBPollingInfo;
    invoke-static {}, Lcom/policydm/db/XDBPollingAdp;->xdbGetPollingInfo()Lcom/policydm/db/XDBPollingInfo;

    move-result-object v0

    .line 144
    iget-object v1, p0, Lcom/policydm/polling/XPollingAgent;->mOrgUrl:Ljava/lang/String;

    iput-object v1, v0, Lcom/policydm/db/XDBPollingInfo;->m_szOrgVersionUrl:Ljava/lang/String;

    .line 145
    iget-object v1, p0, Lcom/policydm/polling/XPollingAgent;->mUrl:Ljava/lang/String;

    iput-object v1, v0, Lcom/policydm/db/XDBPollingInfo;->m_szVersionUrl:Ljava/lang/String;

    .line 146
    iget-object v1, p0, Lcom/policydm/polling/XPollingAgent;->mFilename:Ljava/lang/String;

    iput-object v1, v0, Lcom/policydm/db/XDBPollingInfo;->m_szFileName:Ljava/lang/String;

    .line 147
    iget-object v1, p0, Lcom/policydm/polling/XPollingAgent;->mPeriodUnit:Ljava/lang/String;

    iput-object v1, v0, Lcom/policydm/db/XDBPollingInfo;->m_szPeriodUnit:Ljava/lang/String;

    .line 148
    iget v1, p0, Lcom/policydm/polling/XPollingAgent;->mPeriod:I

    iput v1, v0, Lcom/policydm/db/XDBPollingInfo;->nPeriod:I

    .line 149
    iget v1, p0, Lcom/policydm/polling/XPollingAgent;->mTime:I

    iput v1, v0, Lcom/policydm/db/XDBPollingInfo;->nTime:I

    .line 150
    iget v1, p0, Lcom/policydm/polling/XPollingAgent;->mRange:I

    iput v1, v0, Lcom/policydm/db/XDBPollingInfo;->nRange:I

    .line 151
    iget v1, p0, Lcom/policydm/polling/XPollingAgent;->mReportPeriod:I

    iput v1, v0, Lcom/policydm/db/XDBPollingInfo;->nReportPeriod:I

    .line 152
    iget v1, p0, Lcom/policydm/polling/XPollingAgent;->mReportTime:I

    iput v1, v0, Lcom/policydm/db/XDBPollingInfo;->nReportTime:I

    .line 153
    iget v1, p0, Lcom/policydm/polling/XPollingAgent;->mReportRange:I

    iput v1, v0, Lcom/policydm/db/XDBPollingInfo;->nReportRange:I

    .line 155
    invoke-virtual {p0}, Lcom/policydm/polling/XPollingAgent;->xpollingNextPollingTime()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/policydm/db/XDBPollingInfo;->m_nextpollingtime:J

    .line 156
    const/4 v1, 0x0

    iget-wide v2, v0, Lcom/policydm/db/XDBPollingInfo;->m_nextpollingtime:J

    invoke-static {v1, v2, v3}, Lcom/policydm/XDMApplication;->xdmStartAlarm(IJ)V

    .line 158
    iget-wide v1, v0, Lcom/policydm/db/XDBPollingInfo;->m_nextreporttime:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-nez v1, :cond_a

    .line 160
    invoke-virtual {p0}, Lcom/policydm/polling/XPollingAgent;->xpollingNextReportTime()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/policydm/db/XDBPollingInfo;->m_nextreporttime:J

    .line 161
    const/4 v1, 0x1

    iget-wide v2, v0, Lcom/policydm/db/XDBPollingInfo;->m_nextreporttime:J

    invoke-static {v1, v2, v3}, Lcom/policydm/XDMApplication;->xdmStartAlarm(IJ)V

    .line 168
    :goto_a
    invoke-static {v0}, Lcom/policydm/db/XDBPollingAdp;->xdbSetPollingInfo(Lcom/policydm/db/XDBPollingInfo;)V

    .line 169
    return-void

    .line 93
    .end local v0    # "pollingInfo":Lcom/policydm/db/XDBPollingInfo;
    :cond_0
    iget-object v1, p1, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->url:Ljava/lang/String;

    iput-object v1, p0, Lcom/policydm/polling/XPollingAgent;->mUrl:Ljava/lang/String;

    goto/16 :goto_0

    .line 98
    :cond_1
    iget-object v1, p1, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->orgurl:Ljava/lang/String;

    iput-object v1, p0, Lcom/policydm/polling/XPollingAgent;->mOrgUrl:Ljava/lang/String;

    goto/16 :goto_1

    .line 103
    :cond_2
    iget-object v1, p1, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->filename:Ljava/lang/String;

    iput-object v1, p0, Lcom/policydm/polling/XPollingAgent;->mFilename:Ljava/lang/String;

    goto/16 :goto_2

    .line 108
    :cond_3
    iget-object v1, p1, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->periodunit:Ljava/lang/String;

    iput-object v1, p0, Lcom/policydm/polling/XPollingAgent;->mPeriodUnit:Ljava/lang/String;

    goto/16 :goto_3

    .line 113
    :cond_4
    iget-object v1, p1, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->period:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, Lcom/policydm/polling/XPollingAgent;->mPeriod:I

    goto/16 :goto_4

    .line 118
    :cond_5
    iget-object v1, p1, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->time:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, Lcom/policydm/polling/XPollingAgent;->mTime:I

    goto/16 :goto_5

    .line 123
    :cond_6
    iget-object v1, p1, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->range:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, Lcom/policydm/polling/XPollingAgent;->mRange:I

    goto/16 :goto_6

    .line 128
    :cond_7
    iget-object v1, p1, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->reportperiod:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, Lcom/policydm/polling/XPollingAgent;->mReportPeriod:I

    goto/16 :goto_7

    .line 133
    :cond_8
    iget-object v1, p1, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->reporttime:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, Lcom/policydm/polling/XPollingAgent;->mReportTime:I

    goto/16 :goto_8

    .line 138
    :cond_9
    iget-object v1, p1, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->reportrange:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, Lcom/policydm/polling/XPollingAgent;->mReportRange:I

    goto/16 :goto_9

    .line 165
    .restart local v0    # "pollingInfo":Lcom/policydm/db/XDBPollingInfo;
    :cond_a
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Next StatusReport Time:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, v0, Lcom/policydm/db/XDBPollingInfo;->m_nextreporttime:J

    invoke-static {v2, v3}, Lcom/policydm/XDMApplication;->xdmDisplayAlarmTime(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto/16 :goto_a
.end method
