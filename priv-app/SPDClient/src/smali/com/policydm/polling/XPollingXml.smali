.class public Lcom/policydm/polling/XPollingXml;
.super Ljava/lang/Object;
.source "XPollingXml.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/policydm/polling/XPollingXml$XPollingXMLData;
    }
.end annotation


# static fields
.field private static final XPOLLINT_TAG_ANDROID_VERSION:Ljava/lang/String; = "androidVer"

.field private static final XPOLLINT_TAG_FILENAME:Ljava/lang/String; = "fileName"

.field private static final XPOLLINT_TAG_ORGURL:Ljava/lang/String; = "orgUrl"

.field private static final XPOLLINT_TAG_PERIOD:Ljava/lang/String; = "period"

.field private static final XPOLLINT_TAG_PERIOD_UNIT:Ljava/lang/String; = "periodUnit"

.field private static final XPOLLINT_TAG_POLICY_VERSION:Ljava/lang/String; = "policyVer"

.field private static final XPOLLINT_TAG_POLLING:Ljava/lang/String; = "polling"

.field private static final XPOLLINT_TAG_RANGE:Ljava/lang/String; = "range"

.field private static final XPOLLINT_TAG_STATUS_REPORT:Ljava/lang/String; = "statusReport"

.field private static final XPOLLINT_TAG_TIME:Ljava/lang/String; = "time"

.field private static final XPOLLINT_TAG_URL:Ljava/lang/String; = "url"

.field private static final XPOLLINT_TAG_VERSION:Ljava/lang/String; = "version"


# instance fields
.field pollingAgent:Lcom/policydm/polling/XPollingAgent;

.field pollingxml:Lcom/policydm/polling/XPollingXml$XPollingXMLData;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object v0, p0, Lcom/policydm/polling/XPollingXml;->pollingAgent:Lcom/policydm/polling/XPollingAgent;

    .line 35
    iput-object v0, p0, Lcom/policydm/polling/XPollingXml;->pollingxml:Lcom/policydm/polling/XPollingXml$XPollingXMLData;

    .line 37
    return-void
.end method


# virtual methods
.method public getParsingVersionInfo(Ljava/io/InputStream;)Z
    .locals 28
    .param p1, "content"    # Ljava/io/InputStream;

    .prologue
    .line 55
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/polling/XPollingXml;->pollingAgent:Lcom/policydm/polling/XPollingAgent;

    move-object/from16 v25, v0

    if-nez v25, :cond_0

    .line 56
    new-instance v25, Lcom/policydm/polling/XPollingAgent;

    invoke-direct/range {v25 .. v25}, Lcom/policydm/polling/XPollingAgent;-><init>()V

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/policydm/polling/XPollingXml;->pollingAgent:Lcom/policydm/polling/XPollingAgent;

    .line 57
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/polling/XPollingXml;->pollingxml:Lcom/policydm/polling/XPollingXml$XPollingXMLData;

    move-object/from16 v25, v0

    if-nez v25, :cond_1

    .line 58
    new-instance v25, Lcom/policydm/polling/XPollingXml$XPollingXMLData;

    invoke-direct/range {v25 .. v25}, Lcom/policydm/polling/XPollingXml$XPollingXMLData;-><init>()V

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/policydm/polling/XPollingXml;->pollingxml:Lcom/policydm/polling/XPollingXml$XPollingXMLData;

    .line 60
    :cond_1
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmGetTargetOSVersion()Ljava/lang/String;

    move-result-object v17

    .line 61
    .local v17, "osVer":Ljava/lang/String;
    invoke-static {}, Lcom/policydm/adapter/XSPDAdapter;->xspdGetLatestPolicyVersion()Ljava/lang/String;

    move-result-object v18

    .line 63
    .local v18, "policyVer":Ljava/lang/String;
    const/4 v4, 0x0

    .line 66
    .local v4, "bPolicyUpdate":Z
    if-eqz p1, :cond_8

    .line 68
    :try_start_0
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v8

    .line 69
    .local v8, "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    invoke-virtual {v8}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v5

    .line 70
    .local v5, "builder":Ljavax/xml/parsers/DocumentBuilder;
    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/InputStream;)Lorg/w3c/dom/Document;

    move-result-object v6

    .line 72
    .local v6, "doc":Lorg/w3c/dom/Document;
    if-eqz v6, :cond_7

    .line 74
    invoke-interface {v6}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v20

    .line 75
    .local v20, "root":Lorg/w3c/dom/Element;
    if-eqz v20, :cond_6

    .line 77
    const-string v25, "url"

    move-object/from16 v0, v20

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v22

    .line 78
    .local v22, "url":Lorg/w3c/dom/NodeList;
    const-string v25, "orgUrl"

    move-object/from16 v0, v20

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v16

    .line 79
    .local v16, "orgurl":Lorg/w3c/dom/NodeList;
    const-string v25, "fileName"

    move-object/from16 v0, v20

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v9

    .line 81
    .local v9, "filename":Lorg/w3c/dom/NodeList;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/polling/XPollingXml;->pollingxml:Lcom/policydm/polling/XPollingXml$XPollingXMLData;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move-object/from16 v0, v22

    move/from16 v1, v26

    invoke-interface {v0, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v26

    invoke-interface/range {v26 .. v26}, Lorg/w3c/dom/Node;->getTextContent()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->url:Ljava/lang/String;

    .line 82
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/polling/XPollingXml;->pollingxml:Lcom/policydm/polling/XPollingXml$XPollingXMLData;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v26

    invoke-interface {v0, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v26

    invoke-interface/range {v26 .. v26}, Lorg/w3c/dom/Node;->getTextContent()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->orgurl:Ljava/lang/String;

    .line 83
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/polling/XPollingXml;->pollingxml:Lcom/policydm/polling/XPollingXml$XPollingXMLData;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move/from16 v0, v26

    invoke-interface {v9, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v26

    invoke-interface/range {v26 .. v26}, Lorg/w3c/dom/Node;->getTextContent()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->filename:Ljava/lang/String;

    .line 85
    const-string v25, "version"

    move-object/from16 v0, v20

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v23

    .line 87
    .local v23, "version":Lorg/w3c/dom/NodeList;
    if-eqz v23, :cond_3

    .line 89
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/polling/XPollingXml;->pollingxml:Lcom/policydm/polling/XPollingXml$XPollingXMLData;

    move-object/from16 v25, v0

    invoke-interface/range {v23 .. v23}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v26

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->androidver:[Ljava/lang/String;

    .line 90
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/polling/XPollingXml;->pollingxml:Lcom/policydm/polling/XPollingXml$XPollingXMLData;

    move-object/from16 v25, v0

    invoke-interface/range {v23 .. v23}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v26

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->policyver:[Ljava/lang/String;

    .line 92
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    invoke-interface/range {v23 .. v23}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v25

    move/from16 v0, v25

    if-ge v10, v0, :cond_3

    .line 94
    move-object/from16 v0, v23

    invoke-interface {v0, v10}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v24

    check-cast v24, Lorg/w3c/dom/Element;

    .line 95
    .local v24, "versionElement":Lorg/w3c/dom/Element;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/polling/XPollingXml;->pollingxml:Lcom/policydm/polling/XPollingXml$XPollingXMLData;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->androidver:[Ljava/lang/String;

    move-object/from16 v25, v0

    const-string v26, "androidVer"

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    aput-object v26, v25, v10

    .line 96
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/polling/XPollingXml;->pollingxml:Lcom/policydm/polling/XPollingXml$XPollingXMLData;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->policyver:[Ljava/lang/String;

    move-object/from16 v25, v0

    const-string v26, "policyVer"

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    aput-object v26, v25, v10

    .line 97
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "androidver : "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/polling/XPollingXml;->pollingxml:Lcom/policydm/polling/XPollingXml$XPollingXMLData;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->androidver:[Ljava/lang/String;

    move-object/from16 v26, v0

    aget-object v26, v26, v10

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v25 .. v25}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 98
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "policyver : "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/polling/XPollingXml;->pollingxml:Lcom/policydm/polling/XPollingXml$XPollingXMLData;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->policyver:[Ljava/lang/String;

    move-object/from16 v26, v0

    aget-object v26, v26, v10

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v25 .. v25}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 100
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/polling/XPollingXml;->pollingxml:Lcom/policydm/polling/XPollingXml$XPollingXMLData;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->androidver:[Ljava/lang/String;

    move-object/from16 v25, v0

    aget-object v25, v25, v10

    if-eqz v25, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/polling/XPollingXml;->pollingxml:Lcom/policydm/polling/XPollingXml$XPollingXMLData;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->androidver:[Ljava/lang/String;

    move-object/from16 v25, v0

    aget-object v25, v25, v10

    move-object/from16 v0, v17

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v25

    if-nez v25, :cond_2

    .line 102
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/polling/XPollingXml;->pollingxml:Lcom/policydm/polling/XPollingXml$XPollingXMLData;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->policyver:[Ljava/lang/String;

    move-object/from16 v25, v0

    aget-object v25, v25, v10

    const-string v26, "_"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 103
    .local v3, "SerPolicyVer":[Ljava/lang/String;
    const-string v25, "_"

    move-object/from16 v0, v18

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 105
    .local v2, "DevPolicyVer":[Ljava/lang/String;
    const/16 v25, 0x3

    aget-object v25, v3, v25

    const/16 v26, 0x10

    invoke-static/range {v25 .. v26}, Ljava/lang/Long;->valueOf(Ljava/lang/String;I)Ljava/lang/Long;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    .line 106
    .local v14, "nSerVersion":J
    const/16 v25, 0x3

    aget-object v25, v2, v25

    const/16 v26, 0x10

    invoke-static/range {v25 .. v26}, Ljava/lang/Long;->valueOf(Ljava/lang/String;I)Ljava/lang/Long;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    .line 108
    .local v12, "nDevVersion":J
    cmp-long v25, v14, v12

    if-lez v25, :cond_2

    .line 109
    const/4 v4, 0x1

    .line 92
    .end local v2    # "DevPolicyVer":[Ljava/lang/String;
    .end local v3    # "SerPolicyVer":[Ljava/lang/String;
    .end local v12    # "nDevVersion":J
    .end local v14    # "nSerVersion":J
    :cond_2
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_0

    .line 114
    .end local v10    # "i":I
    .end local v24    # "versionElement":Lorg/w3c/dom/Element;
    :cond_3
    const-string v25, "polling"

    move-object/from16 v0, v20

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v19

    .line 115
    .local v19, "polling":Lorg/w3c/dom/NodeList;
    const/4 v10, 0x0

    .restart local v10    # "i":I
    :goto_1
    invoke-interface/range {v19 .. v19}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v25

    move/from16 v0, v25

    if-ge v10, v0, :cond_4

    .line 117
    move-object/from16 v0, v19

    invoke-interface {v0, v10}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v11

    check-cast v11, Lorg/w3c/dom/Element;

    .line 119
    .local v11, "info":Lorg/w3c/dom/Element;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/polling/XPollingXml;->pollingxml:Lcom/policydm/polling/XPollingXml$XPollingXMLData;

    move-object/from16 v25, v0

    const-string v26, "periodUnit"

    move-object/from16 v0, v26

    invoke-interface {v11, v0}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v26

    const/16 v27, 0x0

    invoke-interface/range {v26 .. v27}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v26

    invoke-interface/range {v26 .. v26}, Lorg/w3c/dom/Node;->getTextContent()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->periodunit:Ljava/lang/String;

    .line 120
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/polling/XPollingXml;->pollingxml:Lcom/policydm/polling/XPollingXml$XPollingXMLData;

    move-object/from16 v25, v0

    const-string v26, "period"

    move-object/from16 v0, v26

    invoke-interface {v11, v0}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v26

    const/16 v27, 0x0

    invoke-interface/range {v26 .. v27}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v26

    invoke-interface/range {v26 .. v26}, Lorg/w3c/dom/Node;->getTextContent()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->period:Ljava/lang/String;

    .line 121
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/polling/XPollingXml;->pollingxml:Lcom/policydm/polling/XPollingXml$XPollingXMLData;

    move-object/from16 v25, v0

    const-string v26, "time"

    move-object/from16 v0, v26

    invoke-interface {v11, v0}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v26

    const/16 v27, 0x0

    invoke-interface/range {v26 .. v27}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v26

    invoke-interface/range {v26 .. v26}, Lorg/w3c/dom/Node;->getTextContent()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->time:Ljava/lang/String;

    .line 122
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/polling/XPollingXml;->pollingxml:Lcom/policydm/polling/XPollingXml$XPollingXMLData;

    move-object/from16 v25, v0

    const-string v26, "range"

    move-object/from16 v0, v26

    invoke-interface {v11, v0}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v26

    const/16 v27, 0x0

    invoke-interface/range {v26 .. v27}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v26

    invoke-interface/range {v26 .. v26}, Lorg/w3c/dom/Node;->getTextContent()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->range:Ljava/lang/String;

    .line 115
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_1

    .line 125
    .end local v11    # "info":Lorg/w3c/dom/Element;
    :cond_4
    const-string v25, "statusReport"

    move-object/from16 v0, v20

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v21

    .line 126
    .local v21, "statusReport":Lorg/w3c/dom/NodeList;
    const/4 v10, 0x0

    :goto_2
    invoke-interface/range {v21 .. v21}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v25

    move/from16 v0, v25

    if-ge v10, v0, :cond_5

    .line 128
    move-object/from16 v0, v21

    invoke-interface {v0, v10}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v11

    check-cast v11, Lorg/w3c/dom/Element;

    .line 130
    .restart local v11    # "info":Lorg/w3c/dom/Element;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/polling/XPollingXml;->pollingxml:Lcom/policydm/polling/XPollingXml$XPollingXMLData;

    move-object/from16 v25, v0

    const-string v26, "period"

    move-object/from16 v0, v26

    invoke-interface {v11, v0}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v26

    const/16 v27, 0x0

    invoke-interface/range {v26 .. v27}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v26

    invoke-interface/range {v26 .. v26}, Lorg/w3c/dom/Node;->getTextContent()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->reportperiod:Ljava/lang/String;

    .line 131
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/polling/XPollingXml;->pollingxml:Lcom/policydm/polling/XPollingXml$XPollingXMLData;

    move-object/from16 v25, v0

    const-string v26, "time"

    move-object/from16 v0, v26

    invoke-interface {v11, v0}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v26

    const/16 v27, 0x0

    invoke-interface/range {v26 .. v27}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v26

    invoke-interface/range {v26 .. v26}, Lorg/w3c/dom/Node;->getTextContent()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->reporttime:Ljava/lang/String;

    .line 132
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/polling/XPollingXml;->pollingxml:Lcom/policydm/polling/XPollingXml$XPollingXMLData;

    move-object/from16 v25, v0

    const-string v26, "range"

    move-object/from16 v0, v26

    invoke-interface {v11, v0}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v26

    const/16 v27, 0x0

    invoke-interface/range {v26 .. v27}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v26

    invoke-interface/range {v26 .. v26}, Lorg/w3c/dom/Node;->getTextContent()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->reportrange:Ljava/lang/String;

    .line 126
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 135
    .end local v11    # "info":Lorg/w3c/dom/Element;
    :cond_5
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "periodunit "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/polling/XPollingXml;->pollingxml:Lcom/policydm/polling/XPollingXml$XPollingXMLData;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->periodunit:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v25 .. v25}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 136
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "period "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/polling/XPollingXml;->pollingxml:Lcom/policydm/polling/XPollingXml$XPollingXMLData;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->period:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v25 .. v25}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 137
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "time "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/polling/XPollingXml;->pollingxml:Lcom/policydm/polling/XPollingXml$XPollingXMLData;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->time:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v25 .. v25}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 138
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "range "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/polling/XPollingXml;->pollingxml:Lcom/policydm/polling/XPollingXml$XPollingXMLData;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->range:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v25 .. v25}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 139
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "reportperiod "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/polling/XPollingXml;->pollingxml:Lcom/policydm/polling/XPollingXml$XPollingXMLData;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->reportperiod:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v25 .. v25}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 140
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "rReporttime "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/polling/XPollingXml;->pollingxml:Lcom/policydm/polling/XPollingXml$XPollingXMLData;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->reporttime:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v25 .. v25}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 141
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "reportrange "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/polling/XPollingXml;->pollingxml:Lcom/policydm/polling/XPollingXml$XPollingXMLData;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/policydm/polling/XPollingXml$XPollingXMLData;->reportrange:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v25 .. v25}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 143
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/polling/XPollingXml;->pollingAgent:Lcom/policydm/polling/XPollingAgent;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/polling/XPollingXml;->pollingxml:Lcom/policydm/polling/XPollingXml$XPollingXMLData;

    move-object/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Lcom/policydm/polling/XPollingAgent;->xpollingSaveXMLData(Lcom/policydm/polling/XPollingXml$XPollingXMLData;)V

    .line 179
    .end local v5    # "builder":Ljavax/xml/parsers/DocumentBuilder;
    .end local v6    # "doc":Lorg/w3c/dom/Document;
    .end local v8    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    .end local v9    # "filename":Lorg/w3c/dom/NodeList;
    .end local v10    # "i":I
    .end local v16    # "orgurl":Lorg/w3c/dom/NodeList;
    .end local v19    # "polling":Lorg/w3c/dom/NodeList;
    .end local v20    # "root":Lorg/w3c/dom/Element;
    .end local v21    # "statusReport":Lorg/w3c/dom/NodeList;
    .end local v22    # "url":Lorg/w3c/dom/NodeList;
    .end local v23    # "version":Lorg/w3c/dom/NodeList;
    :goto_3
    return v4

    .line 147
    .restart local v5    # "builder":Ljavax/xml/parsers/DocumentBuilder;
    .restart local v6    # "doc":Lorg/w3c/dom/Document;
    .restart local v8    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    .restart local v20    # "root":Lorg/w3c/dom/Element;
    :cond_6
    invoke-static {}, Lcom/policydm/polling/XPollingAgent;->xpollingDefaultPollingTime()V

    .line 148
    const-string v25, "root is null"

    invoke-static/range {v25 .. v25}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V
    :try_end_0
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xml/sax/SAXException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_3

    .line 163
    .end local v5    # "builder":Ljavax/xml/parsers/DocumentBuilder;
    .end local v6    # "doc":Lorg/w3c/dom/Document;
    .end local v8    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    .end local v20    # "root":Lorg/w3c/dom/Element;
    :catch_0
    move-exception v7

    .line 165
    .local v7, "e":Ljavax/xml/parsers/ParserConfigurationException;
    invoke-static {}, Lcom/policydm/polling/XPollingAgent;->xpollingDefaultPollingTime()V

    .line 166
    invoke-virtual {v7}, Ljavax/xml/parsers/ParserConfigurationException;->printStackTrace()V

    goto :goto_3

    .line 153
    .end local v7    # "e":Ljavax/xml/parsers/ParserConfigurationException;
    .restart local v5    # "builder":Ljavax/xml/parsers/DocumentBuilder;
    .restart local v6    # "doc":Lorg/w3c/dom/Document;
    .restart local v8    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    :cond_7
    :try_start_1
    invoke-static {}, Lcom/policydm/polling/XPollingAgent;->xpollingDefaultPollingTime()V

    .line 154
    const-string v25, "doc is null"

    invoke-static/range {v25 .. v25}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V
    :try_end_1
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/xml/sax/SAXException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_3

    .line 168
    .end local v5    # "builder":Ljavax/xml/parsers/DocumentBuilder;
    .end local v6    # "doc":Lorg/w3c/dom/Document;
    .end local v8    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    :catch_1
    move-exception v7

    .line 170
    .local v7, "e":Lorg/xml/sax/SAXException;
    invoke-static {}, Lcom/policydm/polling/XPollingAgent;->xpollingDefaultPollingTime()V

    .line 171
    invoke-virtual {v7}, Lorg/xml/sax/SAXException;->printStackTrace()V

    goto :goto_3

    .line 159
    .end local v7    # "e":Lorg/xml/sax/SAXException;
    :cond_8
    :try_start_2
    invoke-static {}, Lcom/policydm/polling/XPollingAgent;->xpollingDefaultPollingTime()V

    .line 160
    const-string v25, "InputStream is null"

    invoke-static/range {v25 .. v25}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V
    :try_end_2
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lorg/xml/sax/SAXException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_3

    .line 173
    :catch_2
    move-exception v7

    .line 175
    .local v7, "e":Ljava/io/IOException;
    invoke-static {}, Lcom/policydm/polling/XPollingAgent;->xpollingDefaultPollingTime()V

    .line 176
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3
.end method
