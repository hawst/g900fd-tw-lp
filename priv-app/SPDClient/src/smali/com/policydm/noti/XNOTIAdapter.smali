.class public Lcom/policydm/noti/XNOTIAdapter;
.super Lcom/policydm/agent/XDMAgent;
.source "XNOTIAdapter.java"

# interfaces
.implements Lcom/policydm/interfaces/XNOTIInterface;
.implements Lcom/policydm/interfaces/XUIInterface;
.implements Ljava/io/Serializable;


# static fields
.field private static final XNOTI_RETRY_COUNT_MAX:I = 0x5

.field private static m_NotiProcessing:Z = false

.field private static m_PtWapPush:Lcom/policydm/noti/XNOTIWapPush; = null

.field private static m_PushDataQueue:Ljava/util/LinkedList; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/policydm/noti/XNOTIData;",
            ">;"
        }
    .end annotation
.end field

.field private static m_szPushDate:Ljava/lang/String; = null

.field private static final serialVersionUID:J = 0x1L


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 28
    sput-object v0, Lcom/policydm/noti/XNOTIAdapter;->m_PtWapPush:Lcom/policydm/noti/XNOTIWapPush;

    .line 29
    sput-object v0, Lcom/policydm/noti/XNOTIAdapter;->m_szPushDate:Ljava/lang/String;

    .line 30
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/policydm/noti/XNOTIAdapter;->m_PushDataQueue:Ljava/util/LinkedList;

    .line 31
    const/4 v0, 0x0

    sput-boolean v0, Lcom/policydm/noti/XNOTIAdapter;->m_NotiProcessing:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/policydm/agent/XDMAgent;-><init>()V

    .line 39
    return-void
.end method

.method public static xnotiAddPushDataQueue(I[B)V
    .locals 4
    .param p0, "type"    # I
    .param p1, "pushData"    # [B

    .prologue
    const/4 v3, 0x0

    .line 205
    if-nez p1, :cond_0

    .line 207
    const-string v2, "gPushData  Uri is null"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 221
    :goto_0
    return-void

    .line 211
    :cond_0
    array-length v2, p1

    new-array v0, v2, [B

    .line 212
    .local v0, "pushDataCopy":[B
    array-length v2, p1

    invoke-static {p1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 214
    new-instance v1, Lcom/policydm/noti/XNOTIData;

    invoke-direct {v1, p0, v0}, Lcom/policydm/noti/XNOTIData;-><init>(I[B)V

    .line 216
    .local v1, "wssPushData":Lcom/policydm/noti/XNOTIData;
    sget-object v3, Lcom/policydm/noti/XNOTIAdapter;->m_PushDataQueue:Ljava/util/LinkedList;

    monitor-enter v3

    .line 218
    :try_start_0
    sget-object v2, Lcom/policydm/noti/XNOTIAdapter;->m_PushDataQueue:Ljava/util/LinkedList;

    invoke-virtual {v2, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 219
    const-string v2, "mPushDataQueue add"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 220
    monitor-exit v3

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public static xnotiGetNotiProcessing()Z
    .locals 1

    .prologue
    .line 194
    sget-boolean v0, Lcom/policydm/noti/XNOTIAdapter;->m_NotiProcessing:Z

    return v0
.end method

.method public static xnotiIPPushDataReceive([B)V
    .locals 2
    .param p0, "pushData"    # [B

    .prologue
    .line 274
    if-nez p0, :cond_0

    .line 276
    const-string v1, "pushData is null"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 283
    :goto_0
    return-void

    .line 280
    :cond_0
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/policydm/noti/XNOTIAdapter;->xnotiSetNotiProcessing(Z)V

    .line 281
    new-instance v0, Lcom/policydm/noti/XNOTIAdapter;

    invoke-direct {v0}, Lcom/policydm/noti/XNOTIAdapter;-><init>()V

    .line 282
    .local v0, "pushAdp":Lcom/policydm/noti/XNOTIAdapter;
    array-length v1, p0

    invoke-virtual {v0, p0, v1}, Lcom/policydm/noti/XNOTIAdapter;->xnotiIpPushAdpReceiveMsg([BI)Z

    goto :goto_0
.end method

.method public static xnotiPushAdpClearSessionStatus()V
    .locals 3

    .prologue
    .line 287
    const-string v2, ""

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 288
    const/4 v1, 0x0

    .line 292
    .local v1, "nFileId":I
    const/4 v2, 0x0

    :try_start_0
    invoke-static {v2}, Lcom/policydm/ui/XUIAdapter;->xuiAdpSetUiMode(I)V

    .line 293
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOStatus(I)V

    .line 294
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOInitiatedType(I)V

    .line 295
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/policydm/db/XDBProfileListAdp;->xdbSetNotiEvent(I)V

    .line 297
    invoke-static {}, Lcom/policydm/db/XDB;->xdbSetBackUpServerUrl()V

    .line 298
    invoke-static {}, Lcom/policydm/noti/XNOTIAdapter;->xnotiPushAdpResetSessionSaveState()V

    .line 300
    invoke-static {}, Lcom/policydm/db/XDB;->xdbGetFileIdFirmwareData()I

    move-result v1

    .line 301
    const/4 v2, 0x0

    invoke-static {v2, v1}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpFileExists(Ljava/lang/String;I)I

    move-result v2

    if-nez v2, :cond_0

    .line 303
    invoke-static {v1}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpDeleteFile(I)I

    .line 306
    :cond_0
    const/4 v2, 0x0

    sput v2, Lcom/policydm/XDMApplication;->g_nResumeStatus:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 312
    :goto_0
    return-void

    .line 308
    :catch_0
    move-exception v0

    .line 310
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xnotiPushAdpDeleteAllNotiQueue()V
    .locals 2

    .prologue
    .line 396
    .local v0, "notiInfo":Lcom/policydm/db/XDBNotiInfo;
    :cond_0
    :goto_0
    invoke-static {}, Lcom/policydm/db/XDBNotiAdp;->xdbNotiExistInfo()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 398
    invoke-static {}, Lcom/policydm/db/XDBNotiAdp;->xdbNotiGetInfo()Lcom/policydm/db/XDBNotiInfo;

    move-result-object v0

    .line 399
    iget-object v1, v0, Lcom/policydm/db/XDBNotiInfo;->m_szSessionId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 400
    iget-object v1, v0, Lcom/policydm/db/XDBNotiInfo;->m_szSessionId:Ljava/lang/String;

    invoke-static {v1}, Lcom/policydm/db/XDBNotiAdp;->xdbNotiDeleteSessionId(Ljava/lang/String;)V

    goto :goto_0

    .line 402
    :cond_1
    return-void
.end method

.method public static xnotiPushAdpExcuteResumeNoti(I)I
    .locals 5
    .param p0, "nAppId"    # I

    .prologue
    const/4 v4, 0x2

    .line 412
    invoke-static {}, Lcom/policydm/db/XDBProfileListAdp;->xdbGetNotiSavedInfo()Lcom/policydm/db/XDBSessionSaveInfo;

    move-result-object v1

    .line 413
    .local v1, "pSessionSaveInfo":Lcom/policydm/db/XDBSessionSaveInfo;
    if-nez v1, :cond_0

    .line 415
    const-string v2, "Get Noti Info File Read Error"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 416
    const/4 v2, -0x1

    .line 467
    :goto_0
    return v2

    .line 419
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "nSessionSaveState:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v1, Lcom/policydm/db/XDBSessionSaveInfo;->nSessionSaveState:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 420
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "nNotiUiEvent:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v1, Lcom/policydm/db/XDBSessionSaveInfo;->nNotiUiEvent:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 421
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "nNotiRetryCount:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v1, Lcom/policydm/db/XDBSessionSaveInfo;->nNotiRetryCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 423
    iget v2, v1, Lcom/policydm/db/XDBSessionSaveInfo;->nSessionSaveState:I

    const/4 v3, 0x1

    if-eq v2, v3, :cond_1

    iget v2, v1, Lcom/policydm/db/XDBSessionSaveInfo;->nSessionSaveState:I

    if-ne v2, v4, :cond_6

    .line 425
    :cond_1
    iget v2, v1, Lcom/policydm/db/XDBSessionSaveInfo;->nNotiRetryCount:I

    const/4 v3, 0x5

    if-lt v2, v3, :cond_2

    .line 427
    const-string v2, "Noti Retry Count MAX. All Clear"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 429
    invoke-static {}, Lcom/policydm/noti/XNOTIAdapter;->xnotiPushAdpClearSessionStatus()V

    .line 430
    invoke-static {}, Lcom/policydm/noti/XNOTIAdapter;->xnotiPushAdpHandleNotiQueue()V

    .line 467
    :goto_1
    const/4 v2, 0x0

    goto :goto_0

    .line 434
    :cond_2
    const-string v2, "Current NOTI SAVED State"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 436
    iget v2, v1, Lcom/policydm/db/XDBSessionSaveInfo;->nNotiUiEvent:I

    const/4 v3, 0x4

    if-ne v2, v3, :cond_4

    .line 438
    invoke-static {}, Lcom/policydm/ui/XUIAdapter;->xuiAdpStartSession()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 440
    invoke-static {v4}, Lcom/policydm/ui/XUIAdapter;->xuiAdpSetUiMode(I)V

    .line 456
    :cond_3
    :goto_2
    iget v2, v1, Lcom/policydm/db/XDBSessionSaveInfo;->nSessionSaveState:I

    iget v3, v1, Lcom/policydm/db/XDBSessionSaveInfo;->nNotiUiEvent:I

    iget v4, v1, Lcom/policydm/db/XDBSessionSaveInfo;->nNotiRetryCount:I

    add-int/lit8 v4, v4, 0x1

    invoke-static {v2, v3, v4}, Lcom/policydm/db/XDBProfileListAdp;->xdbSetSessionSaveStatus(III)Z

    goto :goto_1

    .line 443
    :cond_4
    iget v2, v1, Lcom/policydm/db/XDBSessionSaveInfo;->nNotiUiEvent:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_5

    .line 445
    invoke-static {}, Lcom/policydm/ui/XUIAdapter;->xuiAdpStartSession()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 447
    invoke-static {v4}, Lcom/policydm/ui/XUIAdapter;->xuiAdpSetUiMode(I)V

    goto :goto_2

    .line 452
    :cond_5
    iget v2, v1, Lcom/policydm/db/XDBSessionSaveInfo;->nNotiUiEvent:I

    invoke-static {v2}, Lcom/policydm/db/XDBProfileListAdp;->xdbSetNotiEvent(I)V

    .line 453
    iget v2, v1, Lcom/policydm/db/XDBSessionSaveInfo;->nNotiUiEvent:I

    invoke-static {v2}, Lcom/policydm/noti/XNOTIAdapter;->xnotiPushAdpSelectNotiEvt(I)I

    move-result v0

    .line 454
    .local v0, "nMessage":I
    const/4 v2, 0x0

    invoke-static {v2, v0}, Lcom/policydm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    goto :goto_2

    .line 461
    .end local v0    # "nMessage":I
    :cond_6
    const-string v2, "Current NOTI NOT SAVED State. EXIT."

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 463
    invoke-static {}, Lcom/policydm/noti/XNOTIAdapter;->xnotiPushAdpClearSessionStatus()V

    .line 464
    invoke-static {}, Lcom/policydm/noti/XNOTIAdapter;->xnotiPushAdpHandleNotiQueue()V

    goto :goto_1
.end method

.method public static xnotiPushAdpFreePushData()V
    .locals 1

    .prologue
    .line 155
    const/4 v0, 0x0

    sput-object v0, Lcom/policydm/noti/XNOTIAdapter;->m_PtWapPush:Lcom/policydm/noti/XNOTIWapPush;

    .line 156
    return-void
.end method

.method public static xnotiPushAdpHandleNotiQueue()V
    .locals 4

    .prologue
    .line 365
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/policydm/db/XDBProfileListAdp;->xdbGetSessionID(I)Ljava/lang/String;

    move-result-object v1

    .line 367
    .local v1, "szSessionID":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 369
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Delete Noti Msg sessionId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 370
    invoke-static {v1}, Lcom/policydm/db/XDBNotiAdp;->xdbNotiDeleteSessionId(Ljava/lang/String;)V

    .line 373
    :cond_0
    invoke-static {}, Lcom/policydm/db/XDBNotiAdp;->xdbNotiExistInfo()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 376
    const-string v2, "Next Noti Msg Execute"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 378
    invoke-static {}, Lcom/policydm/db/XDBNotiAdp;->xdbNotiGetInfo()Lcom/policydm/db/XDBNotiInfo;

    move-result-object v0

    .line 380
    .local v0, "notiInfo":Lcom/policydm/db/XDBNotiInfo;
    invoke-static {}, Lcom/policydm/db/XDB;->xdbGetWifiOnlyFlag()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {}, Lcom/policydm/XDMApplication;->xdmProtoIsWIFIConnected()Z

    move-result v2

    if-nez v2, :cond_2

    iget v2, v0, Lcom/policydm/db/XDBNotiInfo;->opMode:I

    if-nez v2, :cond_2

    .line 382
    invoke-static {}, Lcom/policydm/noti/XNOTIAdapter;->xnotiPushAdpDeleteAllNotiQueue()V

    .line 383
    invoke-static {v0}, Lcom/policydm/db/XDBNotiAdp;->xdbNotiInsertInfo(Ljava/lang/Object;)V

    .line 384
    const/16 v2, 0x8

    invoke-static {v2}, Lcom/policydm/ui/XUINotificationManager;->xuiSetIndicator(I)V

    .line 385
    const/4 v2, 0x3

    invoke-static {v2}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOInitiatedType(I)V

    .line 392
    .end local v0    # "notiInfo":Lcom/policydm/db/XDBNotiInfo;
    :cond_1
    :goto_0
    return-void

    .line 389
    .restart local v0    # "notiInfo":Lcom/policydm/db/XDBNotiInfo;
    :cond_2
    const/16 v2, 0x1f

    const/4 v3, 0x0

    invoke-static {v2, v0, v3}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static xnotiPushAdpProcessNotiMessage(I)V
    .locals 4
    .param p0, "nNotiUiEvent"    # I

    .prologue
    .line 487
    const/4 v1, 0x0

    .line 488
    .local v1, "nNetworkState":I
    const/16 v0, 0x8c

    .line 490
    .local v0, "nMessage":I
    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v2, p0, v3}, Lcom/policydm/db/XDBProfileListAdp;->xdbSetSessionSaveStatus(III)Z

    .line 491
    invoke-static {}, Lcom/policydm/adapter/XDMInitAdapter;->xdmInitAdpCheckNetworkReady()I

    move-result v1

    .line 493
    if-nez v1, :cond_0

    .line 495
    invoke-static {p0}, Lcom/policydm/noti/XNOTIAdapter;->xnotiPushAdpSelectNotiEvt(I)I

    move-result v0

    .line 496
    const/4 v2, 0x0

    invoke-static {v2, v0}, Lcom/policydm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    .line 504
    :goto_0
    return-void

    .line 501
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "nNetworkState is Used."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 502
    invoke-static {p0}, Lcom/policydm/noti/XNOTIAdapter;->xnotiPushAdpSuspendNotiAction(I)V

    goto :goto_0
.end method

.method public static xnotiPushAdpProcessWapPushMsg(I)V
    .locals 5
    .param p0, "nId"    # I

    .prologue
    .line 165
    const/4 v0, 0x0

    .line 166
    .local v0, "nID":I
    const/4 v2, 0x0

    .line 167
    .local v2, "ptWapPush":Lcom/policydm/noti/XNOTIWapPush;
    const/4 v1, 0x0

    .line 168
    .local v1, "ptNoti":Lcom/policydm/noti/XNOTIMessage;
    move v0, p0

    .line 170
    invoke-static {v0}, Lcom/policydm/noti/XNOTIAdapter;->xnotiPushAdpReadPushData(I)Lcom/policydm/noti/XNOTIWapPush;

    move-result-object v2

    .line 171
    if-nez v2, :cond_0

    .line 190
    :goto_0
    return-void

    .line 176
    :cond_0
    packed-switch v0, :pswitch_data_0

    .line 187
    const-string v3, "Not Support Content Type"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 179
    :pswitch_0
    new-instance v1, Lcom/policydm/noti/XNOTIMessage;

    .end local v1    # "ptNoti":Lcom/policydm/noti/XNOTIMessage;
    invoke-direct {v1}, Lcom/policydm/noti/XNOTIMessage;-><init>()V

    .line 180
    .restart local v1    # "ptNoti":Lcom/policydm/noti/XNOTIMessage;
    const/4 v3, 0x1

    iput v3, v1, Lcom/policydm/noti/XNOTIMessage;->push_type:I

    .line 181
    iget-object v3, v2, Lcom/policydm/noti/XNOTIWapPush;->pBody:[B

    iput-object v3, v1, Lcom/policydm/noti/XNOTIMessage;->pData:[B

    .line 182
    iget-object v3, v2, Lcom/policydm/noti/XNOTIWapPush;->tWapPushInfo:Lcom/policydm/noti/XNOTIWapPushInfo;

    iget v3, v3, Lcom/policydm/noti/XNOTIWapPushInfo;->nBodyLen:I

    iput v3, v1, Lcom/policydm/noti/XNOTIMessage;->dataSize:I

    .line 183
    const/16 v3, 0x1e

    const/4 v4, 0x0

    invoke-static {v3, v1, v4}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 176
    nop

    :pswitch_data_0
    .packed-switch 0x201
        :pswitch_0
    .end packed-switch
.end method

.method public static xnotiPushAdpReadPushData(I)Lcom/policydm/noti/XNOTIWapPush;
    .locals 1
    .param p0, "nID"    # I

    .prologue
    .line 129
    sget-object v0, Lcom/policydm/noti/XNOTIAdapter;->m_PtWapPush:Lcom/policydm/noti/XNOTIWapPush;

    return-object v0
.end method

.method public static xnotiPushAdpResetSessionSaveState()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 472
    invoke-static {}, Lcom/policydm/db/XDBProfileListAdp;->xdbGetNotiSavedInfo()Lcom/policydm/db/XDBSessionSaveInfo;

    move-result-object v0

    .line 474
    .local v0, "pSessionSaveInfo":Lcom/policydm/db/XDBSessionSaveInfo;
    if-eqz v0, :cond_0

    iget v1, v0, Lcom/policydm/db/XDBSessionSaveInfo;->nSessionSaveState:I

    if-eqz v1, :cond_0

    .line 476
    invoke-static {v2, v2, v2}, Lcom/policydm/db/XDBProfileListAdp;->xdbSetSessionSaveStatus(III)Z

    .line 478
    :cond_0
    const/4 v0, 0x0

    .line 479
    return-void
.end method

.method public static xnotiPushAdpResumeNotiAction(I)V
    .locals 3
    .param p0, "nAppId"    # I

    .prologue
    .line 512
    const/4 v1, 0x0

    .line 513
    .local v1, "nNetworkState":I
    const/4 v0, 0x0

    .line 515
    .local v0, "nEvent":I
    invoke-static {}, Lcom/policydm/adapter/XDMInitAdapter;->xdmInitAdpCheckNetworkReady()I

    move-result v1

    .line 516
    if-eqz v1, :cond_0

    .line 518
    invoke-static {}, Lcom/policydm/db/XDBProfileListAdp;->xdbGetNotiEvent()I

    move-result v0

    .line 519
    packed-switch v0, :pswitch_data_0

    .line 528
    const-string v2, "CAN NOT EXCUTE Noti Resume. EXIT"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 534
    :goto_0
    :pswitch_0
    return-void

    .line 533
    :cond_0
    invoke-static {p0}, Lcom/policydm/noti/XNOTIAdapter;->xnotiPushAdpExcuteResumeNoti(I)I

    goto :goto_0

    .line 519
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static xnotiPushAdpSelectNotiEvt(I)I
    .locals 1
    .param p0, "nEvent"    # I

    .prologue
    .line 321
    const/4 v0, 0x0

    .line 322
    .local v0, "nMessage":I
    packed-switch p0, :pswitch_data_0

    .line 340
    const/16 v0, 0x8c

    .line 343
    :goto_0
    return v0

    .line 325
    :pswitch_0
    const/16 v0, 0x8c

    .line 326
    goto :goto_0

    .line 328
    :pswitch_1
    const/16 v0, 0x8d

    .line 329
    goto :goto_0

    .line 331
    :pswitch_2
    const/16 v0, 0x8e

    .line 332
    goto :goto_0

    .line 334
    :pswitch_3
    const/16 v0, 0x8f

    .line 335
    goto :goto_0

    .line 337
    :pswitch_4
    const/16 v0, 0x90

    .line 338
    goto :goto_0

    .line 322
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static xnotiPushAdpSuspendNotiAction(I)V
    .locals 2
    .param p0, "nNotiUiEvent"    # I

    .prologue
    const/4 v1, 0x0

    .line 352
    const-string v0, ""

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 353
    const/4 v0, 0x1

    invoke-static {v0, p0, v1}, Lcom/policydm/db/XDBProfileListAdp;->xdbSetSessionSaveStatus(III)Z

    .line 354
    invoke-static {v1}, Lcom/policydm/db/XDBProfileListAdp;->xdbSetNotiEvent(I)V

    .line 355
    return-void
.end method

.method public static xnotiPushAdpWritePushData(Lcom/policydm/noti/XNOTIWapPush;I)I
    .locals 1
    .param p0, "pWapPush"    # Lcom/policydm/noti/XNOTIWapPush;
    .param p1, "nID"    # I

    .prologue
    .line 114
    if-nez p0, :cond_0

    .line 115
    const/4 v0, 0x1

    .line 119
    :goto_0
    return v0

    .line 117
    :cond_0
    sput-object p0, Lcom/policydm/noti/XNOTIAdapter;->m_PtWapPush:Lcom/policydm/noti/XNOTIWapPush;

    .line 119
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static xnotiPushDataHandling()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 225
    const/4 v1, 0x0

    .line 226
    .local v1, "wssPushData":Lcom/policydm/noti/XNOTIData;
    sget-object v3, Lcom/policydm/noti/XNOTIAdapter;->m_PushDataQueue:Ljava/util/LinkedList;

    monitor-enter v3

    .line 228
    :try_start_0
    sget-object v2, Lcom/policydm/noti/XNOTIAdapter;->m_PushDataQueue:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 230
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/policydm/noti/XNOTIAdapter;->xnotiSetNotiProcessing(Z)V

    .line 231
    const-string v2, "mPushDataQueue is empty. return"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 232
    monitor-exit v3

    .line 257
    :cond_0
    :goto_0
    return-void

    .line 235
    :cond_1
    sget-object v2, Lcom/policydm/noti/XNOTIAdapter;->m_PushDataQueue:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/policydm/noti/XNOTIData;

    move-object v1, v0

    .line 236
    const-string v2, "mPushDataQueue poll"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 237
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 239
    if-eqz v1, :cond_0

    .line 241
    iget v2, v1, Lcom/policydm/noti/XNOTIData;->type:I

    packed-switch v2, :pswitch_data_0

    .line 252
    invoke-static {v4}, Lcom/policydm/noti/XNOTIAdapter;->xnotiSetNotiProcessing(Z)V

    .line 253
    const-string v2, "PUSH_TYPE is not exist"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 237
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 244
    :pswitch_0
    iget-object v2, v1, Lcom/policydm/noti/XNOTIData;->pushData:[B

    invoke-static {v2}, Lcom/policydm/noti/XNOTIAdapter;->xnotiPushDataReceive([B)V

    goto :goto_0

    .line 248
    :pswitch_1
    iget-object v2, v1, Lcom/policydm/noti/XNOTIData;->pushData:[B

    invoke-static {v2}, Lcom/policydm/noti/XNOTIAdapter;->xnotiIPPushDataReceive([B)V

    goto :goto_0

    .line 241
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static xnotiPushDataReceive([B)V
    .locals 3
    .param p0, "pushData"    # [B

    .prologue
    .line 261
    if-nez p0, :cond_0

    .line 263
    const-string v1, "pushData is null"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 270
    :goto_0
    return-void

    .line 267
    :cond_0
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/policydm/noti/XNOTIAdapter;->xnotiSetNotiProcessing(Z)V

    .line 268
    new-instance v0, Lcom/policydm/noti/XNOTIAdapter;

    invoke-direct {v0}, Lcom/policydm/noti/XNOTIAdapter;-><init>()V

    .line 269
    .local v0, "pushAdp":Lcom/policydm/noti/XNOTIAdapter;
    array-length v1, p0

    const/4 v2, 0x0

    invoke-virtual {v0, p0, v1, v2}, Lcom/policydm/noti/XNOTIAdapter;->xnotiPushAdpReceiveMsg([BILjava/lang/String;)Z

    goto :goto_0
.end method

.method public static xnotiPushGetDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 147
    sget-object v0, Lcom/policydm/noti/XNOTIAdapter;->m_szPushDate:Ljava/lang/String;

    return-object v0
.end method

.method public static xnotiPushSetDate(Ljava/lang/String;)V
    .locals 0
    .param p0, "szDate"    # Ljava/lang/String;

    .prologue
    .line 138
    sput-object p0, Lcom/policydm/noti/XNOTIAdapter;->m_szPushDate:Ljava/lang/String;

    .line 139
    return-void
.end method

.method public static xnotiSetNotiProcessing(Z)V
    .locals 2
    .param p0, "notiProcessing"    # Z

    .prologue
    .line 199
    sput-boolean p0, Lcom/policydm/noti/XNOTIAdapter;->m_NotiProcessing:Z

    .line 200
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "notiProcessing : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 201
    return-void
.end method


# virtual methods
.method public xnotiIpPushAdpReceiveMsg([BI)Z
    .locals 4
    .param p1, "pBody"    # [B
    .param p2, "nBodySize"    # I

    .prologue
    const/4 v3, 0x1

    .line 95
    const/4 v0, 0x0

    .line 97
    .local v0, "ptNoti":Lcom/policydm/noti/XNOTIMessage;
    new-instance v0, Lcom/policydm/noti/XNOTIMessage;

    .end local v0    # "ptNoti":Lcom/policydm/noti/XNOTIMessage;
    invoke-direct {v0}, Lcom/policydm/noti/XNOTIMessage;-><init>()V

    .line 98
    .restart local v0    # "ptNoti":Lcom/policydm/noti/XNOTIMessage;
    iput v3, v0, Lcom/policydm/noti/XNOTIMessage;->push_type:I

    .line 99
    iput-object p1, v0, Lcom/policydm/noti/XNOTIMessage;->pBody:[B

    .line 100
    iput p2, v0, Lcom/policydm/noti/XNOTIMessage;->bodySize:I

    .line 102
    const/16 v1, 0x1e

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 103
    return v3
.end method

.method public xnotiPushAdpReceiveMsg([BILjava/lang/String;)Z
    .locals 12
    .param p1, "pMsg"    # [B
    .param p2, "nMsgLen"    # I
    .param p3, "szDate"    # Ljava/lang/String;

    .prologue
    const/4 v11, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    .line 43
    const/4 v5, 0x0

    .line 44
    .local v5, "ptWapPush":Lcom/policydm/noti/XNOTIWapPush;
    const/4 v4, 0x0

    .line 45
    .local v4, "ptNoti":Lcom/policydm/noti/XNOTIMessage;
    const/4 v1, 0x0

    .line 46
    .local v1, "eRet":I
    const/16 v2, 0x204

    .line 48
    .local v2, "nID":I
    const/4 v0, 0x1

    .line 50
    .local v0, "bRet":Z
    invoke-static {p1, p2}, Lcom/policydm/noti/XNOTIHandler;->xnotiPushHdlrParsingWSPHeader([BI)Lcom/policydm/noti/XNOTIWapPush;

    move-result-object v5

    .line 51
    if-nez v5, :cond_0

    .line 53
    const-string v7, "xnotiPushHdlrParsingWSPHeader Error"

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 90
    :goto_0
    return v6

    .line 57
    :cond_0
    iget-object v8, v5, Lcom/policydm/noti/XNOTIWapPush;->tWapPushInfo:Lcom/policydm/noti/XNOTIWapPushInfo;

    iget v8, v8, Lcom/policydm/noti/XNOTIWapPushInfo;->nContentType:I

    packed-switch v8, :pswitch_data_0

    .line 70
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Not Support Content Type :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "0x%x"

    new-array v7, v7, [Ljava/lang/Object;

    iget-object v10, v5, Lcom/policydm/noti/XNOTIWapPush;->tWapPushInfo:Lcom/policydm/noti/XNOTIWapPushInfo;

    iget v10, v10, Lcom/policydm/noti/XNOTIWapPushInfo;->nContentType:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v7, v6

    invoke-static {v9, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 74
    const/16 v6, 0x204

    if-ne v2, v6, :cond_1

    .line 76
    const/4 v0, 0x0

    move v6, v0

    .line 77
    goto :goto_0

    .line 60
    :pswitch_0
    const/16 v2, 0x201

    .line 61
    new-instance v4, Lcom/policydm/noti/XNOTIMessage;

    .end local v4    # "ptNoti":Lcom/policydm/noti/XNOTIMessage;
    invoke-direct {v4}, Lcom/policydm/noti/XNOTIMessage;-><init>()V

    .line 62
    .restart local v4    # "ptNoti":Lcom/policydm/noti/XNOTIMessage;
    iput v7, v4, Lcom/policydm/noti/XNOTIMessage;->push_type:I

    .line 63
    new-array v6, p2, [B

    iput-object v6, v4, Lcom/policydm/noti/XNOTIMessage;->pData:[B

    .line 64
    iput p2, v4, Lcom/policydm/noti/XNOTIMessage;->dataSize:I

    .line 65
    iput-object p1, v4, Lcom/policydm/noti/XNOTIMessage;->pData:[B

    .line 66
    const/16 v6, 0x1e

    invoke-static {v6, v4, v11}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    move v6, v7

    .line 67
    goto :goto_0

    .line 80
    :cond_1
    invoke-static {p3}, Lcom/policydm/noti/XNOTIAdapter;->xnotiPushSetDate(Ljava/lang/String;)V

    .line 81
    invoke-static {v5, v2}, Lcom/policydm/noti/XNOTIAdapter;->xnotiPushAdpWritePushData(Lcom/policydm/noti/XNOTIWapPush;I)I

    move-result v1

    .line 82
    if-eqz v1, :cond_2

    .line 84
    const/4 v0, 0x0

    move v6, v0

    .line 85
    goto :goto_0

    .line 88
    :cond_2
    move v3, v2

    .line 89
    .local v3, "pSendId":I
    const/16 v6, 0x24

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static {v6, v7, v11}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    move v6, v0

    .line 90
    goto :goto_0

    .line 57
    nop

    :pswitch_data_0
    .packed-switch 0x44
        :pswitch_0
    .end packed-switch
.end method
