.class public Lcom/policydm/noti/XNOTIHandler;
.super Ljava/lang/Object;
.source "XNOTIHandler.java"

# interfaces
.implements Lcom/policydm/interfaces/XDMDefInterface;
.implements Lcom/policydm/interfaces/XDMInterface;
.implements Lcom/policydm/interfaces/XNOTIInterface;


# static fields
.field public static g_tNotiMsg:[Lcom/policydm/noti/XNOTI;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-array v1, v3, [Lcom/policydm/noti/XNOTI;

    sput-object v1, Lcom/policydm/noti/XNOTIHandler;->g_tNotiMsg:[Lcom/policydm/noti/XNOTI;

    .line 35
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_0

    .line 37
    sget-object v1, Lcom/policydm/noti/XNOTIHandler;->g_tNotiMsg:[Lcom/policydm/noti/XNOTI;

    new-instance v2, Lcom/policydm/noti/XNOTI;

    invoke-direct {v2}, Lcom/policydm/noti/XNOTI;-><init>()V

    aput-object v2, v1, v0

    .line 35
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 39
    :cond_0
    return-void
.end method

.method public static final XDM_TO_HEX(I)B
    .locals 2
    .param p0, "val"    # I

    .prologue
    .line 25
    const/16 v1, 0x9

    if-le p0, v1, :cond_0

    add-int/lit8 v1, p0, -0xa

    add-int/lit8 v1, v1, 0x41

    :goto_0
    int-to-byte v0, v1

    .line 26
    .local v0, "ret":B
    return v0

    .line 25
    .end local v0    # "ret":B
    :cond_0
    add-int/lit8 v1, p0, 0x30

    goto :goto_0
.end method

.method public static xnotiPushHdleCompareNotiDigest([BILcom/policydm/noti/XNOTI;I)I
    .locals 8
    .param p0, "pBody"    # [B
    .param p1, "nBodyLen"    # I
    .param p2, "pNotiMsg"    # Lcom/policydm/noti/XNOTI;
    .param p3, "nAppId"    # I

    .prologue
    const/4 v5, -0x1

    .line 118
    const/4 v3, 0x0

    .line 120
    .local v3, "szDigest":Ljava/lang/String;
    iget-object v6, p2, Lcom/policydm/noti/XNOTI;->triggerHeader:Lcom/policydm/noti/XNOTITriggerheader;

    if-nez v6, :cond_0

    .line 122
    const-string v6, "triggerHeader is NULL"

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 156
    :goto_0
    return v5

    .line 126
    :cond_0
    iget-object v6, p2, Lcom/policydm/noti/XNOTI;->triggerHeader:Lcom/policydm/noti/XNOTITriggerheader;

    iget-object v6, v6, Lcom/policydm/noti/XNOTITriggerheader;->m_szServerID:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 128
    const-string v6, "pServerID is NULL"

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 132
    :cond_1
    add-int/lit8 v1, p1, -0x10

    .line 133
    .local v1, "len":I
    new-array v2, v1, [B

    .line 134
    .local v2, "packetbody":[B
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_2

    .line 136
    aget-byte v6, p0, v0

    aput-byte v6, v2, v0

    .line 134
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 139
    :cond_2
    iget-object v6, p2, Lcom/policydm/noti/XNOTI;->triggerHeader:Lcom/policydm/noti/XNOTITriggerheader;

    iget-object v6, v6, Lcom/policydm/noti/XNOTITriggerheader;->m_szServerID:Ljava/lang/String;

    const/4 v7, 0x3

    invoke-static {v6, v7, v2, v1}, Lcom/policydm/db/XDB;->xdbGetNotiDigest(Ljava/lang/String;I[BI)Ljava/lang/String;

    move-result-object v3

    .line 141
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 143
    const-string v6, "pDigest is NULL "

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 147
    :cond_3
    new-instance v4, Ljava/lang/String;

    iget-object v6, p2, Lcom/policydm/noti/XNOTI;->digestdata:[B

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v7

    invoke-direct {v4, v6, v7}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 148
    .local v4, "szDigestdata":Ljava/lang/String;
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 150
    const-string v6, "Compare Fail"

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 155
    :cond_4
    const-string v5, "Compare Success"

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 156
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public static xnotiPushHdleInitNotiMsg(Lcom/policydm/noti/XNOTI;I)V
    .locals 0
    .param p0, "pNotiMsg"    # Lcom/policydm/noti/XNOTI;
    .param p1, "appId"    # I

    .prologue
    .line 97
    iput p1, p0, Lcom/policydm/noti/XNOTI;->appId:I

    .line 98
    return-void
.end method

.method public static xnotiPushHdleMessageFree(Lcom/policydm/noti/XNOTIMessage;)V
    .locals 1
    .param p0, "pObj"    # Lcom/policydm/noti/XNOTIMessage;

    .prologue
    const/4 v0, 0x0

    .line 43
    if-nez p0, :cond_0

    .line 50
    :goto_0
    return-void

    .line 46
    :cond_0
    iput-object v0, p0, Lcom/policydm/noti/XNOTIMessage;->pData:[B

    .line 48
    iput-object v0, p0, Lcom/policydm/noti/XNOTIMessage;->pBody:[B

    .line 49
    const/4 p0, 0x0

    .line 50
    goto :goto_0
.end method

.method public static xnotiPushHdleParsingSyncNoti([BII)I
    .locals 11
    .param p0, "pPushBody"    # [B
    .param p1, "bodySize"    # I
    .param p2, "nAppId"    # I

    .prologue
    const/4 v8, -0x1

    .line 270
    const/4 v6, 0x0

    .line 271
    .local v6, "pNotiMsg":Lcom/policydm/noti/XNOTI;
    const/4 v5, 0x0

    .line 272
    .local v5, "notiHeaderLen":I
    const/4 v4, 0x0

    .line 273
    .local v4, "notiBodyLen":I
    const/4 v2, 0x0

    .line 274
    .local v2, "nRet":I
    const-string v9, ""

    invoke-static {v9}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 276
    sget-object v9, Lcom/policydm/noti/XNOTIHandler;->g_tNotiMsg:[Lcom/policydm/noti/XNOTI;

    aget-object v6, v9, p2

    .line 277
    invoke-static {p0}, Lcom/policydm/noti/XNOTIHandler;->xnotiPushHdleParsingSyncNotiDigest([B)[B

    move-result-object v9

    iput-object v9, v6, Lcom/policydm/noti/XNOTI;->digestdata:[B

    .line 279
    array-length v9, p0

    add-int/lit8 v9, v9, -0x10

    new-array v7, v9, [B

    .line 280
    .local v7, "tmp":[B
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v9, p0

    add-int/lit8 v9, v9, -0x10

    if-ge v1, v9, :cond_0

    .line 282
    add-int/lit8 v9, v1, 0x10

    aget-byte v9, p0, v9

    aput-byte v9, v7, v1

    .line 280
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 285
    :cond_0
    invoke-static {v7, v6}, Lcom/policydm/noti/XNOTIHandler;->xnotiPushHdleParsingSyncNotiHeader([BLcom/policydm/noti/XNOTI;)I

    move-result v5

    .line 286
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "bodySize["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "], notiHeaderLen["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "] DigestLength["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const/16 v10, 0x10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "]"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 287
    if-nez v5, :cond_2

    .line 289
    const-string v9, "notiHeaderLen is 0"

    invoke-static {v9}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 319
    :cond_1
    :goto_1
    return v8

    .line 292
    :cond_2
    invoke-static {p0, p1, v6, p2}, Lcom/policydm/noti/XNOTIHandler;->xnotiPushHdleCompareNotiDigest([BILcom/policydm/noti/XNOTI;I)I

    move-result v2

    .line 296
    if-eqz v2, :cond_3

    .line 298
    invoke-static {p0, p1, v6}, Lcom/policydm/noti/XNOTIHandler;->xnotiReSyncCompare([BILcom/policydm/noti/XNOTI;)I

    move-result v2

    .line 299
    if-nez v2, :cond_1

    .line 306
    :cond_3
    add-int/lit8 v8, v5, 0x10

    if-ge v8, p1, :cond_5

    .line 308
    add-int/lit8 v8, p1, -0x10

    sub-int v4, v8, v5

    .line 310
    array-length v8, p0

    add-int/lit8 v9, v5, 0x10

    sub-int v3, v8, v9

    .line 311
    .local v3, "nlen":I
    new-array v0, v3, [B

    .line 312
    .local v0, "body":[B
    const/4 v1, 0x0

    :goto_2
    if-ge v1, v3, :cond_4

    .line 314
    add-int/lit8 v8, v5, 0x10

    add-int/2addr v8, v1

    aget-byte v8, p0, v8

    aput-byte v8, v0, v1

    .line 312
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 316
    :cond_4
    invoke-static {v0, v4, v6}, Lcom/policydm/noti/XNOTIHandler;->xnotiPushHdleParsingSyncNotiBody([BILcom/policydm/noti/XNOTI;)V

    .line 319
    .end local v0    # "body":[B
    .end local v3    # "nlen":I
    :cond_5
    const/4 v8, 0x0

    goto :goto_1
.end method

.method private static xnotiPushHdleParsingSyncNotiBody([BILcom/policydm/noti/XNOTI;)V
    .locals 7
    .param p0, "pPushBody"    # [B
    .param p1, "notiBodyLen"    # I
    .param p2, "pNotiMsg"    # Lcom/policydm/noti/XNOTI;

    .prologue
    const/16 v6, 0x8

    .line 239
    const-string v4, ""

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 243
    if-eqz p0, :cond_1

    .line 245
    :try_start_0
    iget-object v4, p2, Lcom/policydm/noti/XNOTI;->triggerBody:Lcom/policydm/noti/XNOTITriggerbody;

    const/4 v5, 0x0

    aget-byte v5, p0, v5

    shr-int/lit8 v5, v5, 0x4

    iput v5, v4, Lcom/policydm/noti/XNOTITriggerbody;->opmode:I

    .line 247
    const/16 v4, 0x8

    invoke-static {v4}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 248
    .local v0, "byte_buf":Ljava/nio/ByteBuffer;
    const/16 v4, 0x8

    new-array v1, v4, [B

    .line 249
    .local v1, "change":[B
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v6, :cond_0

    .line 251
    aget-byte v4, p0, v3

    shl-int/lit8 v4, v4, 0x4

    int-to-byte v4, v4

    and-int/lit16 v4, v4, 0xf0

    add-int/lit8 v5, v3, 0x1

    aget-byte v5, p0, v5

    shr-int/lit8 v5, v5, 0x4

    int-to-byte v5, v5

    and-int/lit8 v5, v5, 0xf

    or-int/2addr v4, v5

    int-to-byte v4, v4

    aput-byte v4, v1, v3

    .line 249
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 254
    :cond_0
    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 255
    sget-object v4, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 256
    iget-object v4, p2, Lcom/policydm/noti/XNOTI;->triggerBody:Lcom/policydm/noti/XNOTITriggerbody;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v5

    iput-wide v5, v4, Lcom/policydm/noti/XNOTITriggerbody;->pushjobId:J

    .line 258
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "opmode="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p2, Lcom/policydm/noti/XNOTI;->triggerBody:Lcom/policydm/noti/XNOTITriggerbody;

    iget v5, v5, Lcom/policydm/noti/XNOTITriggerbody;->opmode:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 259
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "pushjobId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p2, Lcom/policydm/noti/XNOTI;->triggerBody:Lcom/policydm/noti/XNOTITriggerbody;

    iget-wide v5, v5, Lcom/policydm/noti/XNOTITriggerbody;->pushjobId:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 266
    .end local v0    # "byte_buf":Ljava/nio/ByteBuffer;
    .end local v1    # "change":[B
    .end local v3    # "i":I
    :cond_1
    :goto_1
    return-void

    .line 262
    :catch_0
    move-exception v2

    .line 264
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static xnotiPushHdleParsingSyncNotiDigest([B)[B
    .locals 5
    .param p0, "pPushBody"    # [B

    .prologue
    const/16 v4, 0x10

    .line 162
    const/4 v1, 0x0

    .line 163
    .local v1, "szDigest":[B
    if-eqz p0, :cond_1

    .line 166
    new-array v1, v4, [B

    .line 167
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v4, :cond_0

    .line 169
    aget-byte v3, p0, v0

    aput-byte v3, v1, v0

    .line 167
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move-object v2, v1

    .line 174
    .end local v0    # "i":I
    .end local v1    # "szDigest":[B
    .local v2, "szDigest":[B
    :goto_1
    return-object v2

    .end local v2    # "szDigest":[B
    .restart local v1    # "szDigest":[B
    :cond_1
    move-object v2, v1

    .end local v1    # "szDigest":[B
    .restart local v2    # "szDigest":[B
    goto :goto_1
.end method

.method public static xnotiPushHdleParsingSyncNotiHeader([BLcom/policydm/noti/XNOTI;)I
    .locals 12
    .param p0, "pPushBody2"    # [B
    .param p1, "pNotiMsg"    # Lcom/policydm/noti/XNOTI;

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 179
    const/4 v4, 0x0

    .line 182
    .local v4, "len_noti_header":I
    :try_start_0
    array-length v9, p0

    new-array v5, v9, [B

    .line 183
    .local v5, "pPushBody":[B
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v9, p0

    if-ge v1, v9, :cond_0

    .line 185
    aget-byte v9, p0, v1

    aput-byte v9, v5, v1

    .line 183
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 188
    :cond_0
    if-eqz v5, :cond_1

    .line 190
    iget-object v9, p1, Lcom/policydm/noti/XNOTI;->triggerHeader:Lcom/policydm/noti/XNOTITriggerheader;

    const/4 v10, 0x0

    aget-byte v10, v5, v10

    shl-int/lit8 v10, v10, 0x8

    const/4 v11, 0x1

    aget-byte v11, v5, v11

    and-int/lit16 v11, v11, 0xc0

    or-int/2addr v10, v11

    shr-int/lit8 v10, v10, 0x6

    iput v10, v9, Lcom/policydm/noti/XNOTITriggerheader;->version:I

    .line 191
    const/4 v9, 0x1

    aget-byte v9, v5, v9

    and-int/lit8 v9, v9, 0x30

    shr-int/lit8 v9, v9, 0x4

    packed-switch v9, :pswitch_data_0

    .line 203
    iget-object v9, p1, Lcom/policydm/noti/XNOTI;->triggerHeader:Lcom/policydm/noti/XNOTITriggerheader;

    const/4 v10, 0x4

    iput v10, v9, Lcom/policydm/noti/XNOTITriggerheader;->uiMode:I

    .line 207
    :goto_1
    const/4 v9, 0x1

    aget-byte v9, v5, v9

    and-int/lit8 v9, v9, 0x8

    shr-int/lit8 v2, v9, 0x3

    .line 208
    .local v2, "init":I
    iget-object v9, p1, Lcom/policydm/noti/XNOTI;->triggerHeader:Lcom/policydm/noti/XNOTITriggerheader;

    if-lez v2, :cond_2

    :goto_2
    iput v7, v9, Lcom/policydm/noti/XNOTITriggerheader;->initiator:I

    .line 209
    iget-object v7, p1, Lcom/policydm/noti/XNOTI;->triggerHeader:Lcom/policydm/noti/XNOTITriggerheader;

    const/4 v8, 0x1

    aget-byte v8, v5, v8

    and-int/lit8 v8, v8, 0x7

    const/4 v9, 0x2

    aget-byte v9, v5, v9

    or-int/2addr v8, v9

    const/4 v9, 0x3

    aget-byte v9, v5, v9

    or-int/2addr v8, v9

    const/4 v9, 0x4

    aget-byte v9, v5, v9

    or-int/2addr v8, v9

    iput v8, v7, Lcom/policydm/noti/XNOTITriggerheader;->future:I

    .line 210
    iget-object v7, p1, Lcom/policydm/noti/XNOTI;->triggerHeader:Lcom/policydm/noti/XNOTITriggerheader;

    const/4 v8, 0x5

    const/4 v9, 0x2

    invoke-static {v5, v8, v9}, Lcom/policydm/eng/core/XDMMem;->xdmLibToHexString([BII)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/policydm/noti/XNOTITriggerheader;->m_szSessionID:Ljava/lang/String;

    .line 211
    iget-object v7, p1, Lcom/policydm/noti/XNOTI;->triggerHeader:Lcom/policydm/noti/XNOTITriggerheader;

    const/4 v8, 0x7

    aget-byte v8, v5, v8

    iput v8, v7, Lcom/policydm/noti/XNOTITriggerheader;->lenServerID:I

    .line 213
    add-int/lit8 v4, v4, 0x8

    .line 216
    iget-object v7, p1, Lcom/policydm/noti/XNOTI;->triggerHeader:Lcom/policydm/noti/XNOTITriggerheader;

    iget v7, v7, Lcom/policydm/noti/XNOTITriggerheader;->lenServerID:I

    new-array v6, v7, [B

    .line 217
    .local v6, "tmp":[B
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_3
    iget-object v7, p1, Lcom/policydm/noti/XNOTI;->triggerHeader:Lcom/policydm/noti/XNOTITriggerheader;

    iget v7, v7, Lcom/policydm/noti/XNOTITriggerheader;->lenServerID:I

    if-ge v3, v7, :cond_3

    .line 219
    add-int/lit8 v7, v3, 0x8

    aget-byte v7, p0, v7

    aput-byte v7, v6, v3

    .line 217
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 194
    .end local v2    # "init":I
    .end local v3    # "j":I
    .end local v6    # "tmp":[B
    :pswitch_0
    iget-object v9, p1, Lcom/policydm/noti/XNOTI;->triggerHeader:Lcom/policydm/noti/XNOTITriggerheader;

    const/4 v10, 0x1

    iput v10, v9, Lcom/policydm/noti/XNOTITriggerheader;->uiMode:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 228
    .end local v1    # "i":I
    .end local v5    # "pPushBody":[B
    :catch_0
    move-exception v0

    .line 230
    .local v0, "e":Ljava/lang/Exception;
    const/4 v4, 0x0

    .line 231
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 234
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_4
    return v4

    .line 197
    .restart local v1    # "i":I
    .restart local v5    # "pPushBody":[B
    :pswitch_1
    :try_start_1
    iget-object v9, p1, Lcom/policydm/noti/XNOTI;->triggerHeader:Lcom/policydm/noti/XNOTITriggerheader;

    const/4 v10, 0x2

    iput v10, v9, Lcom/policydm/noti/XNOTITriggerheader;->uiMode:I

    goto :goto_1

    .line 200
    :pswitch_2
    iget-object v9, p1, Lcom/policydm/noti/XNOTI;->triggerHeader:Lcom/policydm/noti/XNOTITriggerheader;

    const/4 v10, 0x3

    iput v10, v9, Lcom/policydm/noti/XNOTITriggerheader;->uiMode:I

    goto :goto_1

    .restart local v2    # "init":I
    :cond_2
    move v7, v8

    .line 208
    goto :goto_2

    .line 221
    .restart local v3    # "j":I
    .restart local v6    # "tmp":[B
    :cond_3
    iget-object v7, p1, Lcom/policydm/noti/XNOTI;->triggerHeader:Lcom/policydm/noti/XNOTITriggerheader;

    new-instance v8, Ljava/lang/String;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v9

    invoke-direct {v8, v6, v9}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    iput-object v8, v7, Lcom/policydm/noti/XNOTITriggerheader;->m_szServerID:Ljava/lang/String;

    .line 222
    iget-object v7, p1, Lcom/policydm/noti/XNOTI;->triggerHeader:Lcom/policydm/noti/XNOTITriggerheader;

    iget v7, v7, Lcom/policydm/noti/XNOTITriggerheader;->lenServerID:I

    add-int/lit8 v4, v7, 0x8

    .line 223
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ServerID="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p1, Lcom/policydm/noti/XNOTI;->triggerHeader:Lcom/policydm/noti/XNOTITriggerheader;

    iget-object v8, v8, Lcom/policydm/noti/XNOTITriggerheader;->m_szServerID:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 224
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "SessionID="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p1, Lcom/policydm/noti/XNOTI;->triggerHeader:Lcom/policydm/noti/XNOTITriggerheader;

    iget-object v8, v8, Lcom/policydm/noti/XNOTITriggerheader;->m_szSessionID:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 225
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "UI Mode="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p1, Lcom/policydm/noti/XNOTI;->triggerHeader:Lcom/policydm/noti/XNOTITriggerheader;

    iget v8, v8, Lcom/policydm/noti/XNOTITriggerheader;->uiMode:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_4

    .line 191
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static xnotiPushHdleWSPHeader([BI)I
    .locals 4
    .param p0, "buf"    # [B
    .param p1, "len"    # I

    .prologue
    .line 79
    const/4 v1, 0x0

    .line 80
    .local v1, "lenWSP":I
    const/4 v0, 0x0

    .line 82
    .local v0, "LenHeader":I
    const/4 v2, 0x1

    aget-byte v2, p0, v2

    and-int/lit16 v2, v2, 0xff

    const/4 v3, 0x6

    if-ne v2, v3, :cond_0

    .line 84
    const/4 v2, 0x2

    aget-byte v1, p0, v2

    .line 85
    add-int/lit8 v0, v1, 0x3

    .line 87
    if-ge v0, p1, :cond_0

    move v2, v0

    .line 92
    :goto_0
    return v2

    :cond_0
    const/4 v2, -0x1

    goto :goto_0
.end method

.method public static xnotiPushHdlrParsingWSPHeader([BI)Lcom/policydm/noti/XNOTIWapPush;
    .locals 24
    .param p0, "szMsg"    # [B
    .param p1, "nMsgLen"    # I

    .prologue
    .line 410
    const/4 v14, 0x0

    .line 411
    .local v14, "ptMsg":Lcom/policydm/noti/XNOTIWapPush;
    const/4 v11, 0x0

    .line 412
    .local v11, "nLoc":I
    const/16 v20, 0x0

    .line 413
    .local v20, "value":I
    if-eqz p0, :cond_0

    move-object/from16 v0, p0

    array-length v0, v0

    move/from16 v21, v0

    if-nez v21, :cond_1

    .line 415
    :cond_0
    const-string v21, "pData is NULL"

    invoke-static/range {v21 .. v21}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 416
    const/16 v21, 0x0

    .line 597
    :goto_0
    return-object v21

    .line 419
    :cond_1
    const/4 v11, 0x1

    .line 420
    aget-byte v21, p0, v11

    move/from16 v0, v21

    and-int/lit16 v0, v0, 0xff

    move/from16 v20, v0

    const/16 v21, 0x6

    move/from16 v0, v20

    move/from16 v1, v21

    if-eq v0, v1, :cond_2

    .line 422
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Not Support PDU Type="

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 423
    const/16 v21, 0x0

    goto :goto_0

    .line 426
    :cond_2
    new-instance v14, Lcom/policydm/noti/XNOTIWapPush;

    .end local v14    # "ptMsg":Lcom/policydm/noti/XNOTIWapPush;
    invoke-direct {v14}, Lcom/policydm/noti/XNOTIWapPush;-><init>()V

    .line 427
    .restart local v14    # "ptMsg":Lcom/policydm/noti/XNOTIWapPush;
    const/4 v11, 0x2

    .line 428
    iget-object v0, v14, Lcom/policydm/noti/XNOTIWapPush;->tWapPushInfo:Lcom/policydm/noti/XNOTIWapPushInfo;

    move-object/from16 v21, v0

    aget-byte v22, p0, v11

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/policydm/noti/XNOTIWapPushInfo;->nHeaderLen:I

    .line 429
    add-int/lit8 v11, v11, 0x1

    .line 430
    aget-byte v21, p0, v11

    move/from16 v0, v21

    and-int/lit16 v0, v0, 0xff

    move/from16 v20, v0

    const/16 v21, 0x1f

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_3

    .line 432
    add-int/lit8 v11, v11, 0x2

    .line 435
    :cond_3
    aget-byte v21, p0, v11

    move/from16 v0, v21

    and-int/lit16 v0, v0, 0xff

    move/from16 v20, v0

    const/16 v21, 0x20

    move/from16 v0, v20

    move/from16 v1, v21

    if-lt v0, v1, :cond_b

    aget-byte v21, p0, v11

    move/from16 v0, v21

    and-int/lit16 v0, v0, 0xff

    move/from16 v20, v0

    const/16 v21, 0x80

    move/from16 v0, v20

    move/from16 v1, v21

    if-ge v0, v1, :cond_b

    .line 438
    const/4 v7, 0x0

    .local v7, "i":I
    const/4 v2, 0x0

    .local v2, "Loc":I
    const/4 v8, 0x0

    .line 439
    .local v8, "j":I
    move v2, v11

    .line 440
    :goto_1
    aget-byte v21, p0, v11

    if-eqz v21, :cond_4

    .line 442
    add-int/lit8 v7, v7, 0x1

    .line 443
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 445
    :cond_4
    new-array v5, v7, [B

    .line 446
    .local v5, "Mime":[B
    :goto_2
    aget-byte v19, p0, v2

    .local v19, "txt":B
    if-eqz v19, :cond_5

    .line 448
    aput-byte v19, v5, v8

    .line 449
    add-int/lit8 v8, v8, 0x1

    .line 450
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 452
    :cond_5
    new-instance v18, Ljava/lang/String;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v21

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-direct {v0, v5, v1}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 453
    .local v18, "szWbxml":Ljava/lang/String;
    const-string v21, "application/vnd.syncml.notification"

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_7

    .line 455
    iget-object v0, v14, Lcom/policydm/noti/XNOTIWapPush;->tWapPushInfo:Lcom/policydm/noti/XNOTIWapPushInfo;

    move-object/from16 v21, v0

    const/16 v22, 0x36

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/policydm/noti/XNOTIWapPushInfo;->nContentType:I

    .line 474
    :goto_3
    add-int/lit8 v11, v11, 0x1

    .line 509
    .end local v2    # "Loc":I
    .end local v5    # "Mime":[B
    .end local v7    # "i":I
    .end local v8    # "j":I
    .end local v18    # "szWbxml":Ljava/lang/String;
    .end local v19    # "txt":B
    :goto_4
    aget-byte v21, p0, v11

    if-nez v21, :cond_6

    .line 511
    add-int/lit8 v11, v11, 0x1

    .line 514
    :cond_6
    const-string v21, "SEC"

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    move/from16 v0, v21

    new-array v6, v0, [B

    .line 515
    .local v6, "a":[B
    const-string v21, "SEC"

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v15

    .line 516
    .local v15, "strLen":I
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_5
    if-ge v7, v15, :cond_10

    .line 518
    add-int v21, v11, v7

    aget-byte v21, p0, v21

    aput-byte v21, v6, v7

    .line 516
    add-int/lit8 v7, v7, 0x1

    goto :goto_5

    .line 457
    .end local v6    # "a":[B
    .end local v15    # "strLen":I
    .restart local v2    # "Loc":I
    .restart local v5    # "Mime":[B
    .restart local v8    # "j":I
    .restart local v18    # "szWbxml":Ljava/lang/String;
    .restart local v19    # "txt":B
    :cond_7
    const-string v21, "application/vnd.wap.connectivity-xml"

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_8

    .line 459
    iget-object v0, v14, Lcom/policydm/noti/XNOTIWapPush;->tWapPushInfo:Lcom/policydm/noti/XNOTIWapPushInfo;

    move-object/from16 v21, v0

    const/16 v22, 0x35

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/policydm/noti/XNOTIWapPushInfo;->nContentType:I

    goto :goto_3

    .line 461
    :cond_8
    const-string v21, "application/vnd.syncml.notification"

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_9

    .line 463
    iget-object v0, v14, Lcom/policydm/noti/XNOTIWapPush;->tWapPushInfo:Lcom/policydm/noti/XNOTIWapPushInfo;

    move-object/from16 v21, v0

    const/16 v22, 0x44

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/policydm/noti/XNOTIWapPushInfo;->nContentType:I

    goto :goto_3

    .line 465
    :cond_9
    const-string v21, "application/vnd.syncml.dm+wbxml"

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_a

    .line 467
    iget-object v0, v14, Lcom/policydm/noti/XNOTIWapPush;->tWapPushInfo:Lcom/policydm/noti/XNOTIWapPushInfo;

    move-object/from16 v21, v0

    const/16 v22, 0x42

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/policydm/noti/XNOTIWapPushInfo;->nContentType:I

    goto :goto_3

    .line 471
    :cond_a
    const-string v21, "Not Support Content Type"

    invoke-static/range {v21 .. v21}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 472
    const/16 v21, 0x0

    goto/16 :goto_0

    .line 478
    .end local v2    # "Loc":I
    .end local v5    # "Mime":[B
    .end local v7    # "i":I
    .end local v8    # "j":I
    .end local v18    # "szWbxml":Ljava/lang/String;
    .end local v19    # "txt":B
    :cond_b
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Content Value:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    aget-byte v22, p0, v11

    invoke-static/range {v22 .. v22}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 479
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Mask Value:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const/16 v22, 0x3

    aget-byte v22, p0, v22

    and-int/lit8 v22, v22, 0x7f

    invoke-static/range {v22 .. v22}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 481
    aget-byte v21, p0, v11

    and-int/lit8 v21, v21, 0x7f

    const/16 v22, 0x36

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_c

    .line 483
    iget-object v0, v14, Lcom/policydm/noti/XNOTIWapPush;->tWapPushInfo:Lcom/policydm/noti/XNOTIWapPushInfo;

    move-object/from16 v21, v0

    aget-byte v22, p0, v11

    and-int/lit8 v22, v22, 0x7f

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/policydm/noti/XNOTIWapPushInfo;->nContentType:I

    .line 484
    const-string v21, "Content Type: XNOTI_MIMETYPE_WAP_CONNECTIVITY_WBXML"

    invoke-static/range {v21 .. v21}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 506
    :goto_6
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_4

    .line 486
    :cond_c
    aget-byte v21, p0, v11

    and-int/lit8 v21, v21, 0x7f

    const/16 v22, 0x35

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_d

    .line 488
    iget-object v0, v14, Lcom/policydm/noti/XNOTIWapPush;->tWapPushInfo:Lcom/policydm/noti/XNOTIWapPushInfo;

    move-object/from16 v21, v0

    aget-byte v22, p0, v11

    and-int/lit8 v22, v22, 0x7f

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/policydm/noti/XNOTIWapPushInfo;->nContentType:I

    .line 489
    const-string v21, "Content Type: XNOTI_MIMETYPE_WAP_CONNECTIVITY_XML"

    invoke-static/range {v21 .. v21}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto :goto_6

    .line 491
    :cond_d
    aget-byte v21, p0, v11

    and-int/lit8 v21, v21, 0x7f

    const/16 v22, 0x44

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_e

    .line 493
    iget-object v0, v14, Lcom/policydm/noti/XNOTIWapPush;->tWapPushInfo:Lcom/policydm/noti/XNOTIWapPushInfo;

    move-object/from16 v21, v0

    aget-byte v22, p0, v11

    and-int/lit8 v22, v22, 0x7f

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/policydm/noti/XNOTIWapPushInfo;->nContentType:I

    .line 494
    const-string v21, "Content Type: XNOTI_MIMETYPE_SYNCML_DM_NOTI"

    invoke-static/range {v21 .. v21}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto :goto_6

    .line 496
    :cond_e
    aget-byte v21, p0, v11

    and-int/lit8 v21, v21, 0x7f

    const/16 v22, 0x42

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_f

    .line 498
    iget-object v0, v14, Lcom/policydm/noti/XNOTIWapPush;->tWapPushInfo:Lcom/policydm/noti/XNOTIWapPushInfo;

    move-object/from16 v21, v0

    aget-byte v22, p0, v11

    and-int/lit8 v22, v22, 0x7f

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/policydm/noti/XNOTIWapPushInfo;->nContentType:I

    .line 499
    const-string v21, "Content Type: XNOTI_MIMETYPE_SYNCML_DM_WBXML"

    invoke-static/range {v21 .. v21}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto :goto_6

    .line 503
    :cond_f
    const-string v21, "Not Support Content Type"

    invoke-static/range {v21 .. v21}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 504
    const/16 v21, 0x0

    goto/16 :goto_0

    .line 520
    .restart local v6    # "a":[B
    .restart local v7    # "i":I
    .restart local v15    # "strLen":I
    :cond_10
    new-instance v17, Ljava/lang/String;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-direct {v0, v6, v1}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 522
    .local v17, "szSec":Ljava/lang/String;
    aget-byte v21, p0, v11

    move/from16 v0, v21

    and-int/lit16 v0, v0, 0xff

    move/from16 v20, v0

    const/16 v21, 0x91

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_13

    .line 524
    add-int/lit8 v11, v11, 0x1

    .line 525
    iget-object v0, v14, Lcom/policydm/noti/XNOTIWapPush;->tWapPushInfo:Lcom/policydm/noti/XNOTIWapPushInfo;

    move-object/from16 v21, v0

    aget-byte v22, p0, v11

    move/from16 v0, v22

    and-int/lit16 v0, v0, 0xff

    move/from16 v22, v0

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/policydm/noti/XNOTIWapPushInfo;->nSEC:I

    .line 526
    add-int/lit8 v11, v11, 0x1

    .line 535
    :cond_11
    :goto_7
    aget-byte v21, p0, v11

    if-nez v21, :cond_12

    .line 537
    add-int/lit8 v11, v11, 0x1

    .line 539
    :cond_12
    const-string v21, "MAC"

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    move/from16 v0, v21

    new-array v13, v0, [B

    .line 540
    .local v13, "nMac":[B
    const-string v21, "MAC"

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v15

    .line 541
    const/4 v7, 0x0

    :goto_8
    if-ge v7, v15, :cond_14

    .line 543
    add-int v21, v11, v7

    aget-byte v21, p0, v21

    aput-byte v21, v13, v7

    .line 541
    add-int/lit8 v7, v7, 0x1

    goto :goto_8

    .line 528
    .end local v13    # "nMac":[B
    :cond_13
    const-string v21, "SEC"

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_11

    .line 530
    const-string v21, "SEC"

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    add-int v21, v21, v11

    add-int/lit8 v11, v21, 0x1

    .line 531
    iget-object v0, v14, Lcom/policydm/noti/XNOTIWapPush;->tWapPushInfo:Lcom/policydm/noti/XNOTIWapPushInfo;

    move-object/from16 v21, v0

    aget-byte v22, p0, v11

    and-int/lit8 v22, v22, 0xf

    move/from16 v0, v22

    or-int/lit16 v0, v0, 0x80

    move/from16 v22, v0

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/policydm/noti/XNOTIWapPushInfo;->nSEC:I

    .line 532
    add-int/lit8 v11, v11, 0x1

    goto :goto_7

    .line 545
    .restart local v13    # "nMac":[B
    :cond_14
    new-instance v16, Ljava/lang/String;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v21

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-direct {v0, v13, v1}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 546
    .local v16, "szMac":Ljava/lang/String;
    aget-byte v21, p0, v11

    move/from16 v0, v21

    and-int/lit16 v0, v0, 0xff

    move/from16 v20, v0

    const/16 v21, 0x92

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_18

    .line 549
    const/4 v7, 0x0

    const/4 v2, 0x0

    .restart local v2    # "Loc":I
    const/4 v8, 0x0

    .line 550
    .restart local v8    # "j":I
    add-int/lit8 v11, v11, 0x1

    .line 551
    move v2, v11

    .line 552
    :goto_9
    add-int/lit8 v12, v11, 0x1

    .end local v11    # "nLoc":I
    .local v12, "nLoc":I
    aget-byte v21, p0, v11

    if-eqz v21, :cond_15

    .line 554
    add-int/lit8 v7, v7, 0x1

    move v11, v12

    .end local v12    # "nLoc":I
    .restart local v11    # "nLoc":I
    goto :goto_9

    .line 556
    .end local v11    # "nLoc":I
    .restart local v12    # "nLoc":I
    :cond_15
    new-array v4, v7, [B

    .line 557
    .local v4, "MacValue":[B
    :goto_a
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "Loc":I
    .local v3, "Loc":I
    aget-byte v9, p0, v2

    .local v9, "msg":B
    if-eqz v9, :cond_16

    .line 559
    aput-byte v9, v4, v8

    .line 560
    add-int/lit8 v8, v8, 0x1

    move v2, v3

    .end local v3    # "Loc":I
    .restart local v2    # "Loc":I
    goto :goto_a

    .line 563
    .end local v2    # "Loc":I
    .restart local v3    # "Loc":I
    :cond_16
    iget-object v0, v14, Lcom/policydm/noti/XNOTIWapPush;->tWapPushInfo:Lcom/policydm/noti/XNOTIWapPushInfo;

    move-object/from16 v21, v0

    array-length v0, v4

    move/from16 v22, v0

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/policydm/noti/XNOTIWapPushInfo;->nMACLen:I

    .line 564
    iget-object v0, v14, Lcom/policydm/noti/XNOTIWapPush;->tWapPushInfo:Lcom/policydm/noti/XNOTIWapPushInfo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iput-object v4, v0, Lcom/policydm/noti/XNOTIWapPushInfo;->szMAC:[B

    .line 565
    add-int/lit8 v11, v12, 0x1

    .line 590
    .end local v3    # "Loc":I
    .end local v4    # "MacValue":[B
    .end local v8    # "j":I
    .end local v9    # "msg":B
    .end local v12    # "nLoc":I
    .restart local v11    # "nLoc":I
    :cond_17
    :goto_b
    iget-object v0, v14, Lcom/policydm/noti/XNOTIWapPush;->tWapPushInfo:Lcom/policydm/noti/XNOTIWapPushInfo;

    move-object/from16 v21, v0

    add-int/lit8 v22, p1, -0x2

    iget-object v0, v14, Lcom/policydm/noti/XNOTIWapPush;->tWapPushInfo:Lcom/policydm/noti/XNOTIWapPushInfo;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Lcom/policydm/noti/XNOTIWapPushInfo;->nHeaderLen:I

    move/from16 v23, v0

    sub-int v22, v22, v23

    add-int/lit8 v22, v22, -0x1

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/policydm/noti/XNOTIWapPushInfo;->nBodyLen:I

    .line 591
    iget-object v0, v14, Lcom/policydm/noti/XNOTIWapPush;->tWapPushInfo:Lcom/policydm/noti/XNOTIWapPushInfo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/policydm/noti/XNOTIWapPushInfo;->nHeaderLen:I

    move/from16 v21, v0

    add-int/lit8 v21, v21, 0x2

    add-int/lit8 v11, v21, 0x1

    .line 592
    iget-object v0, v14, Lcom/policydm/noti/XNOTIWapPush;->tWapPushInfo:Lcom/policydm/noti/XNOTIWapPushInfo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/policydm/noti/XNOTIWapPushInfo;->nBodyLen:I

    move/from16 v21, v0

    move/from16 v0, v21

    new-array v0, v0, [B

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iput-object v0, v14, Lcom/policydm/noti/XNOTIWapPush;->pBody:[B

    .line 593
    const/4 v10, 0x0

    .local v10, "n":I
    :goto_c
    iget-object v0, v14, Lcom/policydm/noti/XNOTIWapPush;->tWapPushInfo:Lcom/policydm/noti/XNOTIWapPushInfo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/policydm/noti/XNOTIWapPushInfo;->nBodyLen:I

    move/from16 v21, v0

    move/from16 v0, v21

    if-ge v10, v0, :cond_1b

    .line 595
    iget-object v0, v14, Lcom/policydm/noti/XNOTIWapPush;->pBody:[B

    move-object/from16 v21, v0

    add-int/lit8 v12, v11, 0x1

    .end local v11    # "nLoc":I
    .restart local v12    # "nLoc":I
    aget-byte v22, p0, v11

    aput-byte v22, v21, v10

    .line 593
    add-int/lit8 v10, v10, 0x1

    move v11, v12

    .end local v12    # "nLoc":I
    .restart local v11    # "nLoc":I
    goto :goto_c

    .line 567
    .end local v10    # "n":I
    :cond_18
    const-string v21, "MAC"

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_17

    .line 570
    const/4 v7, 0x0

    const/4 v2, 0x0

    .restart local v2    # "Loc":I
    const/4 v8, 0x0

    .line 571
    .restart local v8    # "j":I
    const-string v21, "MAC"

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    add-int v21, v21, v11

    add-int/lit8 v11, v21, 0x1

    .line 573
    move v2, v11

    .line 574
    :goto_d
    add-int/lit8 v12, v11, 0x1

    .end local v11    # "nLoc":I
    .restart local v12    # "nLoc":I
    aget-byte v21, p0, v11

    if-eqz v21, :cond_19

    .line 576
    add-int/lit8 v7, v7, 0x1

    move v11, v12

    .end local v12    # "nLoc":I
    .restart local v11    # "nLoc":I
    goto :goto_d

    .line 578
    .end local v11    # "nLoc":I
    .restart local v12    # "nLoc":I
    :cond_19
    new-array v4, v7, [B

    .line 579
    .restart local v4    # "MacValue":[B
    :goto_e
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "Loc":I
    .restart local v3    # "Loc":I
    aget-byte v9, p0, v2

    .restart local v9    # "msg":B
    if-eqz v9, :cond_1a

    .line 581
    aput-byte v9, v4, v8

    .line 582
    add-int/lit8 v8, v8, 0x1

    move v2, v3

    .end local v3    # "Loc":I
    .restart local v2    # "Loc":I
    goto :goto_e

    .line 585
    .end local v2    # "Loc":I
    .restart local v3    # "Loc":I
    :cond_1a
    iget-object v0, v14, Lcom/policydm/noti/XNOTIWapPush;->tWapPushInfo:Lcom/policydm/noti/XNOTIWapPushInfo;

    move-object/from16 v21, v0

    array-length v0, v4

    move/from16 v22, v0

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/policydm/noti/XNOTIWapPushInfo;->nMACLen:I

    .line 586
    iget-object v0, v14, Lcom/policydm/noti/XNOTIWapPush;->tWapPushInfo:Lcom/policydm/noti/XNOTIWapPushInfo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iput-object v4, v0, Lcom/policydm/noti/XNOTIWapPushInfo;->szMAC:[B

    .line 587
    add-int/lit8 v11, v12, 0x1

    .end local v12    # "nLoc":I
    .restart local v11    # "nLoc":I
    goto/16 :goto_b

    .end local v3    # "Loc":I
    .end local v4    # "MacValue":[B
    .end local v8    # "j":I
    .end local v9    # "msg":B
    .restart local v10    # "n":I
    :cond_1b
    move-object/from16 v21, v14

    .line 597
    goto/16 :goto_0
.end method

.method public static xnotiReSyncCompare([BILcom/policydm/noti/XNOTI;)I
    .locals 13
    .param p0, "pBody"    # [B
    .param p1, "nBodyLen"    # I
    .param p2, "msg"    # Lcom/policydm/noti/XNOTI;

    .prologue
    const/4 v12, -0x1

    .line 602
    const/4 v8, 0x0

    .line 603
    .local v8, "nRet":I
    move-object v9, p2

    .line 604
    .local v9, "pNotiMsg":Lcom/policydm/noti/XNOTI;
    const/4 v10, 0x0

    .line 605
    .local v10, "pNvInfo":Lcom/policydm/db/XDBProfileInfo;
    const/4 v11, 0x0

    .line 606
    .local v11, "szDigest":Ljava/lang/String;
    const/4 v0, 0x4

    new-array v3, v0, [B

    fill-array-data v3, :array_0

    .line 608
    .local v3, "szReSyncNonce":[B
    const/4 v7, 0x0

    .line 610
    .local v7, "bResyncModeRet":Z
    invoke-static {}, Lcom/policydm/db/XDBResyncModeAdp;->xdbGetNonceResync()Z

    move-result v0

    if-nez v0, :cond_0

    .line 612
    const-string v0, "nonce resync SKIP!!!"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 613
    const/4 v0, 0x0

    .line 656
    :goto_0
    return v0

    .line 616
    :cond_0
    if-eqz p2, :cond_1

    iget-object v0, p2, Lcom/policydm/noti/XNOTI;->triggerHeader:Lcom/policydm/noti/XNOTITriggerheader;

    if-nez v0, :cond_2

    :cond_1
    move v0, v12

    .line 618
    goto :goto_0

    .line 620
    :cond_2
    iget-object v0, p2, Lcom/policydm/noti/XNOTI;->triggerHeader:Lcom/policydm/noti/XNOTITriggerheader;

    iget-object v0, v0, Lcom/policydm/noti/XNOTITriggerheader;->m_szServerID:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v12

    .line 622
    goto :goto_0

    .line 625
    :cond_3
    invoke-static {}, Lcom/policydm/db/XDBProfileAdp;->xdbGetProfileInfo()Lcom/policydm/db/XDBProfileInfo;

    move-result-object v10

    .line 626
    if-nez v10, :cond_4

    .line 628
    const-string v0, "DM Info is NULL "

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move v0, v12

    .line 629
    goto :goto_0

    .line 631
    :cond_4
    const/4 v0, 0x3

    iget-object v1, v10, Lcom/policydm/db/XDBProfileInfo;->ServerID:Ljava/lang/String;

    iget-object v2, v10, Lcom/policydm/db/XDBProfileInfo;->ServerPwd:Ljava/lang/String;

    array-length v4, v3

    add-int/lit8 v6, p1, -0x10

    move-object v5, p0

    invoke-static/range {v0 .. v6}, Lcom/policydm/eng/core/XDMAuth;->xdmAuthMakeDigest(ILjava/lang/String;Ljava/lang/String;[BI[BI)Ljava/lang/String;

    move-result-object v11

    .line 633
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 635
    const-string v0, "Digest is NULL "

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move v0, v12

    .line 636
    goto :goto_0

    .line 639
    :cond_5
    invoke-static {v11}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 640
    new-instance v0, Ljava/lang/String;

    iget-object v1, v9, Lcom/policydm/noti/XNOTI;->digestdata:[B

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    invoke-virtual {v11, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_7

    .line 642
    const-string v0, "Fail"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 643
    const/4 v8, -0x1

    :cond_6
    :goto_1
    move v0, v8

    .line 656
    goto :goto_0

    .line 647
    :cond_7
    const-string v0, "xnotiReSyncCompare Success"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 648
    const/4 v8, 0x0

    .line 649
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/policydm/db/XDBProfileListAdp;->xdbSetNotiReSyncMode(I)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    .line 650
    if-nez v7, :cond_6

    .line 652
    const-string v0, "Fail"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 653
    const/4 v8, -0x1

    goto :goto_1

    .line 606
    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method


# virtual methods
.method public xnotiPushHdleFreeNotiMsg(Lcom/policydm/noti/XNOTI;)V
    .locals 2
    .param p1, "pNotiMsg"    # Lcom/policydm/noti/XNOTI;

    .prologue
    const/4 v1, 0x0

    .line 102
    if-nez p1, :cond_0

    .line 104
    const-string v0, "pNotiMsg is NULL"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 114
    :goto_0
    return-void

    .line 108
    :cond_0
    iget-object v0, p1, Lcom/policydm/noti/XNOTI;->triggerHeader:Lcom/policydm/noti/XNOTITriggerheader;

    if-eqz v0, :cond_1

    .line 110
    iget-object v0, p1, Lcom/policydm/noti/XNOTI;->triggerHeader:Lcom/policydm/noti/XNOTITriggerheader;

    iput-object v1, v0, Lcom/policydm/noti/XNOTITriggerheader;->m_szServerID:Ljava/lang/String;

    .line 111
    iput-object v1, p1, Lcom/policydm/noti/XNOTI;->triggerHeader:Lcom/policydm/noti/XNOTITriggerheader;

    .line 113
    :cond_1
    const/4 p1, 0x0

    .line 114
    goto :goto_0
.end method

.method public xnotiPushHdleMessageCopy(Ljava/lang/Object;)Lcom/policydm/noti/XNOTIMessage;
    .locals 3
    .param p1, "source"    # Ljava/lang/Object;

    .prologue
    .line 54
    move-object v1, p1

    check-cast v1, Lcom/policydm/noti/XNOTIMessage;

    .line 55
    .local v1, "pSrc":Lcom/policydm/noti/XNOTIMessage;
    new-instance v0, Lcom/policydm/noti/XNOTIMessage;

    invoke-direct {v0}, Lcom/policydm/noti/XNOTIMessage;-><init>()V

    .line 57
    .local v0, "pDst":Lcom/policydm/noti/XNOTIMessage;
    iget-object v2, v1, Lcom/policydm/noti/XNOTIMessage;->pData:[B

    if-eqz v2, :cond_1

    .line 59
    iget-object v2, v1, Lcom/policydm/noti/XNOTIMessage;->pData:[B

    iput-object v2, v0, Lcom/policydm/noti/XNOTIMessage;->pData:[B

    .line 60
    iget v2, v1, Lcom/policydm/noti/XNOTIMessage;->dataSize:I

    iput v2, v0, Lcom/policydm/noti/XNOTIMessage;->dataSize:I

    .line 68
    :cond_0
    :goto_0
    iget v2, v1, Lcom/policydm/noti/XNOTIMessage;->bodySize:I

    iput v2, v0, Lcom/policydm/noti/XNOTIMessage;->bodySize:I

    .line 69
    iget v2, v1, Lcom/policydm/noti/XNOTIMessage;->mime_type:I

    iput v2, v0, Lcom/policydm/noti/XNOTIMessage;->mime_type:I

    .line 70
    iget v2, v1, Lcom/policydm/noti/XNOTIMessage;->push_type:I

    iput v2, v0, Lcom/policydm/noti/XNOTIMessage;->push_type:I

    .line 71
    iget v2, v1, Lcom/policydm/noti/XNOTIMessage;->push_status:I

    iput v2, v0, Lcom/policydm/noti/XNOTIMessage;->push_status:I

    .line 72
    iget v2, v1, Lcom/policydm/noti/XNOTIMessage;->appId:I

    iput v2, v0, Lcom/policydm/noti/XNOTIMessage;->appId:I

    .line 74
    return-object v0

    .line 62
    :cond_1
    iget-object v2, v1, Lcom/policydm/noti/XNOTIMessage;->pBody:[B

    if-eqz v2, :cond_0

    .line 64
    iget v2, v1, Lcom/policydm/noti/XNOTIMessage;->bodySize:I

    new-array v2, v2, [B

    iput-object v2, v0, Lcom/policydm/noti/XNOTIMessage;->pData:[B

    .line 65
    iget-object v2, v1, Lcom/policydm/noti/XNOTIMessage;->pBody:[B

    iput-object v2, v0, Lcom/policydm/noti/XNOTIMessage;->pBody:[B

    goto :goto_0
.end method

.method public xnotiPushHdleMsgHandler(Lcom/policydm/noti/XNOTIMessage;)Lcom/policydm/noti/XNOTI;
    .locals 4
    .param p1, "pPushMsg"    # Lcom/policydm/noti/XNOTIMessage;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 380
    new-instance v0, Lcom/policydm/noti/XNOTI;

    invoke-direct {v0}, Lcom/policydm/noti/XNOTI;-><init>()V

    .line 382
    .local v0, "ptMsg":Lcom/policydm/noti/XNOTI;
    const-string v1, ""

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 383
    iget v1, p1, Lcom/policydm/noti/XNOTIMessage;->push_type:I

    packed-switch v1, :pswitch_data_0

    .line 399
    :pswitch_0
    const-string v1, "Not Support Push Type"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 403
    :goto_0
    invoke-static {p1}, Lcom/policydm/noti/XNOTIHandler;->xnotiPushHdleMessageFree(Lcom/policydm/noti/XNOTIMessage;)V

    .line 405
    return-object v0

    .line 386
    :pswitch_1
    iput v2, p1, Lcom/policydm/noti/XNOTIMessage;->appId:I

    .line 387
    iput v3, p1, Lcom/policydm/noti/XNOTIMessage;->mime_type:I

    .line 388
    const-string v1, "XNOTI_MIME_CONTENT_TYPE_DM "

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 389
    invoke-virtual {p0, p1}, Lcom/policydm/noti/XNOTIHandler;->xnotiPushHdleNotiMsgHandler(Lcom/policydm/noti/XNOTIMessage;)Lcom/policydm/noti/XNOTI;

    move-result-object v0

    .line 390
    goto :goto_0

    .line 393
    :pswitch_2
    iput v2, p1, Lcom/policydm/noti/XNOTIMessage;->appId:I

    .line 394
    iput v3, p1, Lcom/policydm/noti/XNOTIMessage;->mime_type:I

    .line 395
    const-string v1, "XNOTI_MIME_CONTENT_TYPE_DM "

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 383
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public xnotiPushHdleNotiMsgHandler(Lcom/policydm/noti/XNOTIMessage;)Lcom/policydm/noti/XNOTI;
    .locals 11
    .param p1, "pPushMsg"    # Lcom/policydm/noti/XNOTIMessage;

    .prologue
    const/4 v8, 0x0

    .line 324
    const/4 v2, 0x0

    .line 325
    .local v2, "appId":I
    const/4 v6, 0x0

    .line 327
    .local v6, "nRet":I
    if-nez p1, :cond_0

    .line 329
    const-string v9, "pPushMsg is NULL"

    invoke-static {v9}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 374
    :goto_0
    return-object v8

    .line 333
    :cond_0
    iget v2, p1, Lcom/policydm/noti/XNOTIMessage;->appId:I

    .line 334
    sget-object v9, Lcom/policydm/noti/XNOTIHandler;->g_tNotiMsg:[Lcom/policydm/noti/XNOTI;

    aget-object v9, v9, v2

    invoke-static {v9, v2}, Lcom/policydm/noti/XNOTIHandler;->xnotiPushHdleInitNotiMsg(Lcom/policydm/noti/XNOTI;I)V

    .line 335
    sget-object v9, Lcom/policydm/noti/XNOTIHandler;->g_tNotiMsg:[Lcom/policydm/noti/XNOTI;

    aget-object v9, v9, v2

    iget v10, p1, Lcom/policydm/noti/XNOTIMessage;->mime_type:I

    iput v10, v9, Lcom/policydm/noti/XNOTI;->mimeType:I

    .line 337
    iget-object v9, p1, Lcom/policydm/noti/XNOTIMessage;->pData:[B

    if-eqz v9, :cond_2

    .line 339
    iget-object v9, p1, Lcom/policydm/noti/XNOTIMessage;->pData:[B

    const/4 v10, 0x0

    aget-byte v3, v9, v10

    .line 340
    .local v3, "data":B
    if-eqz v3, :cond_2

    .line 342
    const/4 v1, 0x0

    .line 343
    .local v1, "PushDataLength":I
    const/4 v0, 0x0

    .line 344
    .local v0, "LenHeader":I
    const-string v9, "header and body"

    invoke-static {v9}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 346
    iget v1, p1, Lcom/policydm/noti/XNOTIMessage;->dataSize:I

    .line 348
    iget-object v9, p1, Lcom/policydm/noti/XNOTIMessage;->pData:[B

    invoke-static {v9, v1}, Lcom/policydm/noti/XNOTIHandler;->xnotiPushHdleWSPHeader([BI)I

    move-result v0

    .line 349
    const/4 v9, -0x1

    if-eq v0, v9, :cond_2

    .line 351
    sub-int v5, v1, v0

    .line 352
    .local v5, "len":I
    new-array v7, v5, [B

    .line 353
    .local v7, "ptrPushBody":[B
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    if-ge v4, v5, :cond_1

    .line 355
    iget-object v9, p1, Lcom/policydm/noti/XNOTIMessage;->pData:[B

    add-int v10, v0, v4

    aget-byte v9, v9, v10

    aput-byte v9, v7, v4

    .line 353
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 358
    :cond_1
    iput-object v8, p1, Lcom/policydm/noti/XNOTIMessage;->pBody:[B

    .line 359
    iput v5, p1, Lcom/policydm/noti/XNOTIMessage;->bodySize:I

    .line 360
    iput-object v7, p1, Lcom/policydm/noti/XNOTIMessage;->pBody:[B

    .line 361
    iput-object v8, p1, Lcom/policydm/noti/XNOTIMessage;->pData:[B

    .line 366
    .end local v0    # "LenHeader":I
    .end local v1    # "PushDataLength":I
    .end local v3    # "data":B
    .end local v4    # "i":I
    .end local v5    # "len":I
    .end local v7    # "ptrPushBody":[B
    :cond_2
    iget-object v9, p1, Lcom/policydm/noti/XNOTIMessage;->pBody:[B

    iget v10, p1, Lcom/policydm/noti/XNOTIMessage;->bodySize:I

    invoke-static {v9, v10, v2}, Lcom/policydm/noti/XNOTIHandler;->xnotiPushHdleParsingSyncNoti([BII)I

    move-result v6

    .line 367
    if-nez v6, :cond_3

    .line 369
    sget-object v8, Lcom/policydm/noti/XNOTIHandler;->g_tNotiMsg:[Lcom/policydm/noti/XNOTI;

    aget-object v8, v8, v2

    goto :goto_0

    .line 373
    :cond_3
    sget-object v9, Lcom/policydm/noti/XNOTIHandler;->g_tNotiMsg:[Lcom/policydm/noti/XNOTI;

    aget-object v9, v9, v2

    invoke-virtual {p0, v9}, Lcom/policydm/noti/XNOTIHandler;->xnotiPushHdleFreeNotiMsg(Lcom/policydm/noti/XNOTI;)V

    goto :goto_0
.end method
