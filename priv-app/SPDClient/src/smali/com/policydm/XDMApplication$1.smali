.class Lcom/policydm/XDMApplication$1;
.super Landroid/os/Handler;
.source "XDMApplication.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/policydm/XDMApplication;->onCreate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/policydm/XDMApplication;


# direct methods
.method constructor <init>(Lcom/policydm/XDMApplication;)V
    .locals 0

    .prologue
    .line 141
    iput-object p1, p0, Lcom/policydm/XDMApplication$1;->this$0:Lcom/policydm/XDMApplication;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v3, 0x1

    .line 145
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 147
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 185
    :cond_0
    :goto_0
    # getter for: Lcom/policydm/XDMApplication;->m_hDmHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/policydm/XDMApplication;->access$300()Landroid/os/Handler;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 186
    return-void

    .line 160
    :sswitch_0
    iget-object v0, p0, Lcom/policydm/XDMApplication$1;->this$0:Lcom/policydm/XDMApplication;

    iget v1, p1, Landroid/os/Message;->what:I

    # invokes: Lcom/policydm/XDMApplication;->xdmCallUiDialogActivity(I)V
    invoke-static {v0, v1}, Lcom/policydm/XDMApplication;->access$000(Lcom/policydm/XDMApplication;I)V

    goto :goto_0

    .line 164
    :sswitch_1
    # getter for: Lcom/policydm/XDMApplication;->m_Context:Landroid/content/Context;
    invoke-static {}, Lcom/policydm/XDMApplication;->access$100()Landroid/content/Context;

    move-result-object v0

    # getter for: Lcom/policydm/XDMApplication;->m_Context:Landroid/content/Context;
    invoke-static {}, Lcom/policydm/XDMApplication;->access$100()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f080048

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/policydm/XDMApplication;->xdmShowToast(Landroid/content/Context;Ljava/lang/String;I)V

    goto :goto_0

    .line 168
    :sswitch_2
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetTopActivityName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "XUIEulaActivity"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/policydm/XDMApplication$1;->this$0:Lcom/policydm/XDMApplication;

    const/4 v1, 0x0

    # invokes: Lcom/policydm/XDMApplication;->xdmCallUiEulaActivity(I)V
    invoke-static {v0, v1}, Lcom/policydm/XDMApplication;->access$200(Lcom/policydm/XDMApplication;I)V

    goto :goto_0

    .line 173
    :sswitch_3
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetTopActivityName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "XUIEulaActivity"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/policydm/XDMApplication$1;->this$0:Lcom/policydm/XDMApplication;

    # invokes: Lcom/policydm/XDMApplication;->xdmCallUiEulaActivity(I)V
    invoke-static {v0, v3}, Lcom/policydm/XDMApplication;->access$200(Lcom/policydm/XDMApplication;I)V

    goto :goto_0

    .line 178
    :sswitch_4
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetTopActivityName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "XUIEulaActivity"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/policydm/XDMApplication$1;->this$0:Lcom/policydm/XDMApplication;

    const/4 v1, 0x2

    # invokes: Lcom/policydm/XDMApplication;->xdmCallUiEulaActivity(I)V
    invoke-static {v0, v1}, Lcom/policydm/XDMApplication;->access$200(Lcom/policydm/XDMApplication;I)V

    goto :goto_0

    .line 147
    :sswitch_data_0
    .sparse-switch
        0x67 -> :sswitch_0
        0x68 -> :sswitch_1
        0x6e -> :sswitch_0
        0x78 -> :sswitch_0
        0x79 -> :sswitch_0
        0x7a -> :sswitch_0
        0x7c -> :sswitch_0
        0x82 -> :sswitch_0
        0xc8 -> :sswitch_2
        0xc9 -> :sswitch_3
        0xca -> :sswitch_4
        0xcb -> :sswitch_0
        0xcc -> :sswitch_0
        0xcd -> :sswitch_0
        0xce -> :sswitch_0
    .end sparse-switch
.end method
