.class public Lcom/policydm/push/XSPPResult;
.super Ljava/lang/Object;
.source "XSPPResult.java"


# instance fields
.field protected bSuccess:Z

.field protected mError:I

.field protected mPushID:Ljava/lang/String;


# direct methods
.method public constructor <init>(Z)V
    .locals 2
    .param p1, "success"    # Z

    .prologue
    const/4 v1, 0x0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    iput-boolean v1, p0, Lcom/policydm/push/XSPPResult;->bSuccess:Z

    .line 8
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/push/XSPPResult;->mPushID:Ljava/lang/String;

    .line 9
    iput v1, p0, Lcom/policydm/push/XSPPResult;->mError:I

    .line 13
    iput-boolean p1, p0, Lcom/policydm/push/XSPPResult;->bSuccess:Z

    .line 14
    return-void
.end method


# virtual methods
.method public getError()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/policydm/push/XSPPResult;->mError:I

    return v0
.end method

.method public getPushID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/policydm/push/XSPPResult;->mPushID:Ljava/lang/String;

    return-object v0
.end method

.method public isSuccess()Z
    .locals 1

    .prologue
    .line 21
    iget-boolean v0, p0, Lcom/policydm/push/XSPPResult;->bSuccess:Z

    return v0
.end method

.method public setError(I)V
    .locals 2
    .param p1, "error"    # I

    .prologue
    .line 42
    iput p1, p0, Lcom/policydm/push/XSPPResult;->mError:I

    .line 43
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SPP Register error: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 44
    return-void
.end method

.method public setPushID(Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/policydm/push/XSPPResult;->mPushID:Ljava/lang/String;

    .line 27
    return-void
.end method
