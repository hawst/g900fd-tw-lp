.class Lcom/policydm/push/XPushProcess$7;
.super Landroid/os/AsyncTask;
.source "XPushProcess.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/policydm/push/XPushProcess;->sendGCMResult(Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/policydm/push/XPushProcess;


# direct methods
.method constructor <init>(Lcom/policydm/push/XPushProcess;)V
    .locals 0

    .prologue
    .line 462
    iput-object p1, p0, Lcom/policydm/push/XPushProcess$7;->this$0:Lcom/policydm/push/XPushProcess;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 462
    check-cast p1, [Ljava/lang/String;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/policydm/push/XPushProcess$7;->doInBackground([Ljava/lang/String;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Void;
    .locals 5
    .param p1, "params"    # [Ljava/lang/String;

    .prologue
    .line 466
    const/4 v1, 0x0

    .line 467
    .local v1, "isSuccess":Z
    const/4 v3, 0x0

    aget-object v2, p1, v3

    .line 468
    .local v2, "regID":Ljava/lang/String;
    const/4 v3, 0x1

    aget-object v0, p1, v3

    .line 470
    .local v0, "error":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 472
    iget-object v3, p0, Lcom/policydm/push/XPushProcess$7;->this$0:Lcom/policydm/push/XPushProcess;

    iget-boolean v3, v3, Lcom/policydm/push/XPushProcess;->bRegMode:Z

    if-eqz v3, :cond_1

    .line 474
    const-string v3, "GCM Push registration Success"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 475
    const/4 v1, 0x1

    .line 484
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/policydm/push/XPushProcess$7;->this$0:Lcom/policydm/push/XPushProcess;

    new-instance v4, Lcom/policydm/push/XGCMResult;

    invoke-direct {v4, v1}, Lcom/policydm/push/XGCMResult;-><init>(Z)V

    iput-object v4, v3, Lcom/policydm/push/XPushProcess;->mGCMResult:Lcom/policydm/push/XGCMResult;

    .line 485
    iget-object v3, p0, Lcom/policydm/push/XPushProcess$7;->this$0:Lcom/policydm/push/XPushProcess;

    iget-object v3, v3, Lcom/policydm/push/XPushProcess;->mGCMResult:Lcom/policydm/push/XGCMResult;

    invoke-virtual {v3}, Lcom/policydm/push/XGCMResult;->isSuccess()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 486
    iget-object v3, p0, Lcom/policydm/push/XPushProcess$7;->this$0:Lcom/policydm/push/XPushProcess;

    iget-object v3, v3, Lcom/policydm/push/XPushProcess;->mGCMResult:Lcom/policydm/push/XGCMResult;

    invoke-virtual {v3, v2}, Lcom/policydm/push/XGCMResult;->setPushID(Ljava/lang/String;)V

    .line 489
    :goto_1
    const/4 v3, 0x0

    return-object v3

    .line 477
    :cond_1
    iget-object v3, p0, Lcom/policydm/push/XPushProcess$7;->this$0:Lcom/policydm/push/XPushProcess;

    iget-boolean v3, v3, Lcom/policydm/push/XPushProcess;->bRegMode:Z

    if-nez v3, :cond_0

    .line 479
    const-string v3, "GCM Push unregistration Success"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 480
    const/4 v1, 0x1

    goto :goto_0

    .line 488
    :cond_2
    iget-object v3, p0, Lcom/policydm/push/XPushProcess$7;->this$0:Lcom/policydm/push/XPushProcess;

    iget-object v3, v3, Lcom/policydm/push/XPushProcess;->mGCMResult:Lcom/policydm/push/XGCMResult;

    invoke-virtual {v3, v0}, Lcom/policydm/push/XGCMResult;->setError(Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 462
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/policydm/push/XPushProcess$7;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 6
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    const/16 v5, 0x7a

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 495
    iget-object v1, p0, Lcom/policydm/push/XPushProcess$7;->this$0:Lcom/policydm/push/XPushProcess;

    iget-object v1, v1, Lcom/policydm/push/XPushProcess;->mGCMResult:Lcom/policydm/push/XGCMResult;

    if-eqz v1, :cond_2

    .line 497
    iget-object v1, p0, Lcom/policydm/push/XPushProcess$7;->this$0:Lcom/policydm/push/XPushProcess;

    iget-object v1, v1, Lcom/policydm/push/XPushProcess;->mGCMResult:Lcom/policydm/push/XGCMResult;

    invoke-virtual {v1}, Lcom/policydm/push/XGCMResult;->isSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 499
    iget-object v1, p0, Lcom/policydm/push/XPushProcess$7;->this$0:Lcom/policydm/push/XPushProcess;

    iget-object v1, v1, Lcom/policydm/push/XPushProcess;->mGCMResult:Lcom/policydm/push/XGCMResult;

    invoke-virtual {v1}, Lcom/policydm/push/XGCMResult;->getPushID()Ljava/lang/String;

    move-result-object v0

    .line 500
    .local v0, "regiId":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "gcm id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 501
    invoke-static {v0}, Lcom/policydm/db/XDBSpdAdp;->xdbSetSpdPushRegId(Ljava/lang/String;)V

    .line 503
    invoke-static {v3}, Lcom/policydm/XDMBroadcastReceiver;->setStartDeviceRegister(Z)V

    .line 504
    const/16 v1, 0x3e

    invoke-static {v1, v4, v4}, Lcom/policydm/eng/core/XDMMsg;->xdmSendRCMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 505
    sput v3, Lcom/policydm/push/XPushProcess;->retryCount:I

    .line 535
    .end local v0    # "regiId":Ljava/lang/String;
    :goto_0
    return-void

    .line 509
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "gcm error : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/policydm/push/XPushProcess$7;->this$0:Lcom/policydm/push/XPushProcess;

    iget-object v2, v2, Lcom/policydm/push/XPushProcess;->mGCMResult:Lcom/policydm/push/XGCMResult;

    invoke-virtual {v2}, Lcom/policydm/push/XGCMResult;->getError()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 510
    invoke-static {v3}, Lcom/policydm/db/XDBSpdAdp;->xdbSetSpdState(I)V

    .line 511
    invoke-static {v4, v5}, Lcom/policydm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    .line 512
    iget-object v1, p0, Lcom/policydm/push/XPushProcess$7;->this$0:Lcom/policydm/push/XPushProcess;

    iget-boolean v1, v1, Lcom/policydm/push/XPushProcess;->bRegMode:Z

    if-eqz v1, :cond_1

    sget v1, Lcom/policydm/push/XPushProcess;->retryCount:I

    const/4 v2, 0x3

    if-ge v1, v2, :cond_1

    .line 514
    sget v1, Lcom/policydm/push/XPushProcess;->retryCount:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/policydm/push/XPushProcess;->retryCount:I

    .line 515
    iget-object v1, p0, Lcom/policydm/push/XPushProcess$7;->this$0:Lcom/policydm/push/XPushProcess;

    iget-object v1, v1, Lcom/policydm/push/XPushProcess;->mGCMResult:Lcom/policydm/push/XGCMResult;

    invoke-virtual {v1}, Lcom/policydm/push/XGCMResult;->setNextRetry()V

    goto :goto_0

    .line 519
    :cond_1
    sput v3, Lcom/policydm/push/XPushProcess;->retryCount:I

    goto :goto_0

    .line 525
    :cond_2
    const-string v1, "result is Null"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 526
    invoke-static {v3}, Lcom/policydm/db/XDBSpdAdp;->xdbSetSpdState(I)V

    .line 527
    invoke-static {v4, v5}, Lcom/policydm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    goto :goto_0
.end method
