.class public Lcom/policydm/push/XPushProcess;
.super Ljava/lang/Object;
.source "XPushProcess.java"

# interfaces
.implements Lcom/policydm/interfaces/XDMInterface;
.implements Lcom/policydm/interfaces/XEventInterface;
.implements Lcom/policydm/interfaces/XPushInterface;
.implements Lcom/policydm/interfaces/XUIInterface;
.implements Ljava/lang/Runnable;


# static fields
.field public static final PUSH_MSG_GCM_REGI_PROCESS:I = 0x3

.field public static final PUSH_MSG_GCM_UNREGI_PROCESS:I = 0x4

.field public static final PUSH_MSG_SPP_REGI_PROCESS:I = 0x1

.field public static final PUSH_MSG_SPP_UNREGI_PROCESS:I = 0x2

.field private static PushHanlder:Landroid/os/Handler;

.field private static mPushProcess:Lcom/policydm/push/XPushProcess;

.field protected static retryCount:I


# instance fields
.field protected bRegMode:Z

.field protected mGCMRespReceiver:Lcom/policydm/push/XGCMRespReceiver;

.field protected mGCMResult:Lcom/policydm/push/XGCMResult;

.field protected mSPPRespReceiver:Lcom/policydm/push/XSPPRespReceiver;

.field protected mSPPResult:Lcom/policydm/push/XSPPResult;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    sput-object v0, Lcom/policydm/push/XPushProcess;->mPushProcess:Lcom/policydm/push/XPushProcess;

    .line 43
    const/4 v0, 0x0

    sput v0, Lcom/policydm/push/XPushProcess;->retryCount:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object v0, p0, Lcom/policydm/push/XPushProcess;->mSPPRespReceiver:Lcom/policydm/push/XSPPRespReceiver;

    .line 31
    iput-object v0, p0, Lcom/policydm/push/XPushProcess;->mSPPResult:Lcom/policydm/push/XSPPResult;

    .line 32
    iput-object v0, p0, Lcom/policydm/push/XPushProcess;->mGCMRespReceiver:Lcom/policydm/push/XGCMRespReceiver;

    .line 33
    iput-object v0, p0, Lcom/policydm/push/XPushProcess;->mGCMResult:Lcom/policydm/push/XGCMResult;

    .line 42
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/policydm/push/XPushProcess;->bRegMode:Z

    .line 47
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 48
    return-void
.end method

.method static synthetic access$000(Lcom/policydm/push/XPushProcess;I)V
    .locals 0
    .param p0, "x0"    # Lcom/policydm/push/XPushProcess;
    .param p1, "x1"    # I

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/policydm/push/XPushProcess;->processMessage(I)V

    return-void
.end method

.method private callGCMRegiProcess()V
    .locals 3

    .prologue
    .line 313
    invoke-static {}, Lcom/policydm/push/XPushProcess;->getPush()Lcom/policydm/push/XPushProcess;

    move-result-object v0

    .line 314
    .local v0, "gcm":Lcom/policydm/push/XPushProcess;
    new-instance v2, Lcom/policydm/push/XPushProcess$4;

    invoke-direct {v2, p0}, Lcom/policydm/push/XPushProcess$4;-><init>(Lcom/policydm/push/XPushProcess;)V

    invoke-virtual {v0, v2}, Lcom/policydm/push/XPushProcess;->setGCMReceiver(Lcom/policydm/push/XGCMRespReceiver;)V

    .line 356
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/policydm/push/XPushProcess;->XGCMRegistration(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 357
    .local v1, "pushID":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 359
    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/policydm/push/XPushProcess;->sendGCMResult(Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    :cond_0
    return-void
.end method

.method private callGCMUnRegiProcess()V
    .locals 2

    .prologue
    .line 366
    invoke-static {}, Lcom/policydm/push/XPushProcess;->getPush()Lcom/policydm/push/XPushProcess;

    move-result-object v0

    .line 367
    .local v0, "gcm":Lcom/policydm/push/XPushProcess;
    new-instance v1, Lcom/policydm/push/XPushProcess$5;

    invoke-direct {v1, p0}, Lcom/policydm/push/XPushProcess$5;-><init>(Lcom/policydm/push/XPushProcess;)V

    invoke-virtual {v0, v1}, Lcom/policydm/push/XPushProcess;->setGCMReceiver(Lcom/policydm/push/XGCMRespReceiver;)V

    .line 382
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/policydm/push/XPushProcess;->XGCMUnregistration(Landroid/content/Context;)Ljava/lang/String;

    .line 383
    return-void
.end method

.method private callSPPRegiProcess()V
    .locals 2

    .prologue
    .line 243
    invoke-static {}, Lcom/policydm/push/XPushProcess;->getPush()Lcom/policydm/push/XPushProcess;

    move-result-object v0

    .line 244
    .local v0, "spp":Lcom/policydm/push/XPushProcess;
    new-instance v1, Lcom/policydm/push/XPushProcess$2;

    invoke-direct {v1, p0}, Lcom/policydm/push/XPushProcess$2;-><init>(Lcom/policydm/push/XPushProcess;)V

    invoke-virtual {v0, v1}, Lcom/policydm/push/XPushProcess;->setSPPReceiver(Lcom/policydm/push/XSPPRespReceiver;)V

    .line 285
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/policydm/push/XPushProcess;->XSPPRegistration(Landroid/content/Context;)V

    .line 286
    return-void
.end method

.method private callSPPUnRegiProcess()V
    .locals 2

    .prologue
    .line 291
    invoke-static {}, Lcom/policydm/push/XPushProcess;->getPush()Lcom/policydm/push/XPushProcess;

    move-result-object v0

    .line 292
    .local v0, "spp":Lcom/policydm/push/XPushProcess;
    new-instance v1, Lcom/policydm/push/XPushProcess$3;

    invoke-direct {v1, p0}, Lcom/policydm/push/XPushProcess$3;-><init>(Lcom/policydm/push/XPushProcess;)V

    invoke-virtual {v0, v1}, Lcom/policydm/push/XPushProcess;->setSPPReceiver(Lcom/policydm/push/XSPPRespReceiver;)V

    .line 307
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/policydm/push/XPushProcess;->XSPPUnregistration(Landroid/content/Context;)V

    .line 308
    return-void
.end method

.method public static getPush()Lcom/policydm/push/XPushProcess;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/policydm/push/XPushProcess;->mPushProcess:Lcom/policydm/push/XPushProcess;

    if-nez v0, :cond_0

    .line 53
    new-instance v0, Lcom/policydm/push/XPushProcess;

    invoke-direct {v0}, Lcom/policydm/push/XPushProcess;-><init>()V

    sput-object v0, Lcom/policydm/push/XPushProcess;->mPushProcess:Lcom/policydm/push/XPushProcess;

    .line 54
    :cond_0
    sget-object v0, Lcom/policydm/push/XPushProcess;->mPushProcess:Lcom/policydm/push/XPushProcess;

    return-object v0
.end method

.method private processMessage(I)V
    .locals 0
    .param p1, "what"    # I

    .prologue
    .line 123
    packed-switch p1, :pswitch_data_0

    .line 140
    :goto_0
    return-void

    .line 126
    :pswitch_0
    invoke-direct {p0}, Lcom/policydm/push/XPushProcess;->callSPPRegiProcess()V

    goto :goto_0

    .line 129
    :pswitch_1
    invoke-direct {p0}, Lcom/policydm/push/XPushProcess;->callSPPUnRegiProcess()V

    goto :goto_0

    .line 132
    :pswitch_2
    invoke-direct {p0}, Lcom/policydm/push/XPushProcess;->callGCMRegiProcess()V

    goto :goto_0

    .line 135
    :pswitch_3
    invoke-direct {p0}, Lcom/policydm/push/XPushProcess;->callGCMUnRegiProcess()V

    goto :goto_0

    .line 123
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static sendMessage(I)V
    .locals 2
    .param p0, "what"    # I

    .prologue
    .line 109
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 110
    .local v0, "msg":Landroid/os/Message;
    iput p0, v0, Landroid/os/Message;->what:I

    .line 111
    sget-object v1, Lcom/policydm/push/XPushProcess;->PushHanlder:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 113
    sget-object v1, Lcom/policydm/push/XPushProcess;->PushHanlder:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 119
    :goto_0
    return-void

    .line 117
    :cond_0
    const-string v1, "PushHanlder is null!!"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public XGCMRegistration(Landroid/content/Context;)Ljava/lang/String;
    .locals 5
    .param p1, "mContext"    # Landroid/content/Context;

    .prologue
    .line 195
    const/4 v1, 0x0

    .line 198
    .local v1, "pushID":Ljava/lang/String;
    :try_start_0
    const-string v2, "register GCM server"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 199
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/policydm/push/XPushProcess;->bRegMode:Z

    .line 200
    invoke-static {p1}, Lcom/google/android/gcm/GCMRegistrar;->checkDevice(Landroid/content/Context;)V

    .line 201
    invoke-static {p1}, Lcom/google/android/gcm/GCMRegistrar;->checkManifest(Landroid/content/Context;)V

    .line 202
    invoke-static {p1}, Lcom/google/android/gcm/GCMRegistrar;->getRegistrationId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 203
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 205
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "96666210016"

    aput-object v4, v2, v3

    invoke-static {p1, v2}, Lcom/google/android/gcm/GCMRegistrar;->register(Landroid/content/Context;[Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 212
    :cond_0
    :goto_0
    return-object v1

    .line 208
    :catch_0
    move-exception v0

    .line 210
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public XGCMUnregistration(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p1, "mContext"    # Landroid/content/Context;

    .prologue
    .line 220
    const/4 v1, 0x0

    .line 223
    .local v1, "pushID":Ljava/lang/String;
    :try_start_0
    const-string v2, "unregister GCM server"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 224
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/policydm/push/XPushProcess;->bRegMode:Z

    .line 225
    invoke-static {p1}, Lcom/google/android/gcm/GCMRegistrar;->checkDevice(Landroid/content/Context;)V

    .line 226
    invoke-static {p1}, Lcom/google/android/gcm/GCMRegistrar;->checkManifest(Landroid/content/Context;)V

    .line 227
    invoke-static {p1}, Lcom/google/android/gcm/GCMRegistrar;->getRegistrationId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 228
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 230
    invoke-static {p1}, Lcom/google/android/gcm/GCMRegistrar;->unregister(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 237
    :cond_0
    :goto_0
    return-object v1

    .line 233
    :catch_0
    move-exception v0

    .line 235
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public XSPPRegistration(Landroid/content/Context;)V
    .locals 5
    .param p1, "mContext"    # Landroid/content/Context;

    .prologue
    .line 149
    :try_start_0
    const-string v3, "register SPP server"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 150
    const-string v3, "alarm"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 151
    .local v0, "actMgr":Landroid/app/AlarmManager;
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/policydm/push/XPushProcess;->bRegMode:Z

    .line 152
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.sec.spp.action.SPP_REQUEST"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 153
    .local v2, "i":Landroid/content/Intent;
    const-string v3, "reqType"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 154
    const-string v3, "appId"

    const-string v4, "e668374785e8ac2a"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 155
    const-string v3, "userdata"

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 156
    const/16 v3, 0x20

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 160
    invoke-virtual {p1, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 166
    .end local v0    # "actMgr":Landroid/app/AlarmManager;
    .end local v2    # "i":Landroid/content/Intent;
    :goto_0
    return-void

    .line 162
    :catch_0
    move-exception v1

    .line 164
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public XSPPUnregistration(Landroid/content/Context;)V
    .locals 4
    .param p1, "mContext"    # Landroid/content/Context;

    .prologue
    .line 175
    :try_start_0
    const-string v2, "unregister SPP server"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 176
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/policydm/push/XPushProcess;->bRegMode:Z

    .line 177
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.spp.action.SPP_REQUEST"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 178
    .local v1, "i":Landroid/content/Intent;
    const-string v2, "reqType"

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 179
    const-string v2, "appId"

    const-string v3, "e668374785e8ac2a"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 180
    const-string v2, "userdata"

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 181
    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 182
    invoke-virtual {p1, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 188
    .end local v1    # "i":Landroid/content/Intent;
    :goto_0
    return-void

    .line 184
    :catch_0
    move-exception v0

    .line 186
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public getGCMReceiver()Lcom/policydm/push/XGCMRespReceiver;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/policydm/push/XPushProcess;->mGCMRespReceiver:Lcom/policydm/push/XGCMRespReceiver;

    return-object v0
.end method

.method public getSPPReceiver()Lcom/policydm/push/XSPPRespReceiver;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/policydm/push/XPushProcess;->mSPPRespReceiver:Lcom/policydm/push/XSPPRespReceiver;

    return-object v0
.end method

.method public run()V
    .locals 1

    .prologue
    .line 93
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 95
    new-instance v0, Lcom/policydm/push/XPushProcess$1;

    invoke-direct {v0, p0}, Lcom/policydm/push/XPushProcess$1;-><init>(Lcom/policydm/push/XPushProcess;)V

    sput-object v0, Lcom/policydm/push/XPushProcess;->PushHanlder:Landroid/os/Handler;

    .line 104
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 105
    return-void
.end method

.method public sendGCMResult(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "regId"    # Ljava/lang/String;
    .param p2, "errorMsg"    # Ljava/lang/String;

    .prologue
    .line 461
    new-instance v0, Lcom/policydm/push/XPushProcess$7;

    invoke-direct {v0, p0}, Lcom/policydm/push/XPushProcess$7;-><init>(Lcom/policydm/push/XPushProcess;)V

    .line 537
    .local v0, "task":Landroid/os/AsyncTask;, "Landroid/os/AsyncTask<Ljava/lang/String;Ljava/lang/Void;Ljava/lang/Void;>;"
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 538
    return-void
.end method

.method public sendSPPResult(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 390
    new-instance v0, Lcom/policydm/push/XPushProcess$6;

    invoke-direct {v0, p0}, Lcom/policydm/push/XPushProcess$6;-><init>(Lcom/policydm/push/XPushProcess;)V

    .line 453
    .local v0, "task":Landroid/os/AsyncTask;, "Landroid/os/AsyncTask<Landroid/content/Intent;Ljava/lang/Void;Ljava/lang/Void;>;"
    const/4 v1, 0x1

    new-array v1, v1, [Landroid/content/Intent;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 454
    return-void
.end method

.method public setGCMReceiver(Lcom/policydm/push/XGCMRespReceiver;)V
    .locals 0
    .param p1, "observer"    # Lcom/policydm/push/XGCMRespReceiver;

    .prologue
    .line 86
    iput-object p1, p0, Lcom/policydm/push/XPushProcess;->mGCMRespReceiver:Lcom/policydm/push/XGCMRespReceiver;

    .line 87
    return-void
.end method

.method public setSPPReceiver(Lcom/policydm/push/XSPPRespReceiver;)V
    .locals 0
    .param p1, "observer"    # Lcom/policydm/push/XSPPRespReceiver;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/policydm/push/XPushProcess;->mSPPRespReceiver:Lcom/policydm/push/XSPPRespReceiver;

    .line 71
    return-void
.end method
