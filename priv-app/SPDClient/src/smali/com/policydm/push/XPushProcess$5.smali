.class Lcom/policydm/push/XPushProcess$5;
.super Ljava/lang/Object;
.source "XPushProcess.java"

# interfaces
.implements Lcom/policydm/push/XGCMRespReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/policydm/push/XPushProcess;->callGCMUnRegiProcess()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/policydm/push/XPushProcess;


# direct methods
.method constructor <init>(Lcom/policydm/push/XPushProcess;)V
    .locals 0

    .prologue
    .line 368
    iput-object p1, p0, Lcom/policydm/push/XPushProcess$5;->this$0:Lcom/policydm/push/XPushProcess;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onXGCMResponse(Lcom/policydm/push/XGCMResult;)V
    .locals 2
    .param p1, "result"    # Lcom/policydm/push/XGCMResult;

    .prologue
    .line 372
    if-eqz p1, :cond_0

    .line 374
    invoke-virtual {p1}, Lcom/policydm/push/XGCMResult;->isSuccess()Z

    move-result v0

    if-nez v0, :cond_0

    .line 376
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "gcm error : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/policydm/push/XGCMResult;->getError()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 379
    :cond_0
    invoke-static {}, Lcom/policydm/push/XPushProcess;->getPush()Lcom/policydm/push/XPushProcess;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/policydm/push/XPushProcess;->setGCMReceiver(Lcom/policydm/push/XGCMRespReceiver;)V

    .line 380
    return-void
.end method
