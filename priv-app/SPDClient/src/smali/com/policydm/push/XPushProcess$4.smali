.class Lcom/policydm/push/XPushProcess$4;
.super Ljava/lang/Object;
.source "XPushProcess.java"

# interfaces
.implements Lcom/policydm/push/XGCMRespReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/policydm/push/XPushProcess;->callGCMRegiProcess()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/policydm/push/XPushProcess;


# direct methods
.method constructor <init>(Lcom/policydm/push/XPushProcess;)V
    .locals 0

    .prologue
    .line 315
    iput-object p1, p0, Lcom/policydm/push/XPushProcess$4;->this$0:Lcom/policydm/push/XPushProcess;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onXGCMResponse(Lcom/policydm/push/XGCMResult;)V
    .locals 6
    .param p1, "result"    # Lcom/policydm/push/XGCMResult;

    .prologue
    const/16 v5, 0x7a

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 319
    if-eqz p1, :cond_2

    .line 321
    invoke-virtual {p1}, Lcom/policydm/push/XGCMResult;->isSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 323
    invoke-virtual {p1}, Lcom/policydm/push/XGCMResult;->getPushID()Ljava/lang/String;

    move-result-object v0

    .line 324
    .local v0, "regiId":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "gcm id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 325
    invoke-static {v0}, Lcom/policydm/db/XDBSpdAdp;->xdbSetSpdPushRegId(Ljava/lang/String;)V

    .line 327
    invoke-static {v3}, Lcom/policydm/XDMBroadcastReceiver;->setStartDeviceRegister(Z)V

    .line 328
    const/16 v1, 0x3e

    invoke-static {v1, v4, v4}, Lcom/policydm/eng/core/XDMMsg;->xdmSendRCMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 329
    sput v3, Lcom/policydm/push/XPushProcess;->retryCount:I

    .line 353
    .end local v0    # "regiId":Ljava/lang/String;
    :goto_0
    invoke-static {}, Lcom/policydm/push/XPushProcess;->getPush()Lcom/policydm/push/XPushProcess;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/policydm/push/XPushProcess;->setGCMReceiver(Lcom/policydm/push/XGCMRespReceiver;)V

    .line 354
    return-void

    .line 333
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "gcm error : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/policydm/push/XGCMResult;->getError()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 334
    invoke-static {v3}, Lcom/policydm/db/XDBSpdAdp;->xdbSetSpdState(I)V

    .line 335
    invoke-static {v4, v5}, Lcom/policydm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    .line 336
    iget-object v1, p0, Lcom/policydm/push/XPushProcess$4;->this$0:Lcom/policydm/push/XPushProcess;

    iget-boolean v1, v1, Lcom/policydm/push/XPushProcess;->bRegMode:Z

    if-eqz v1, :cond_1

    sget v1, Lcom/policydm/push/XPushProcess;->retryCount:I

    const/4 v2, 0x3

    if-ge v1, v2, :cond_1

    .line 338
    sget v1, Lcom/policydm/push/XPushProcess;->retryCount:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/policydm/push/XPushProcess;->retryCount:I

    .line 339
    iget-object v1, p0, Lcom/policydm/push/XPushProcess$4;->this$0:Lcom/policydm/push/XPushProcess;

    iget-object v1, v1, Lcom/policydm/push/XPushProcess;->mGCMResult:Lcom/policydm/push/XGCMResult;

    invoke-virtual {v1}, Lcom/policydm/push/XGCMResult;->setNextRetry()V

    goto :goto_0

    .line 343
    :cond_1
    sput v3, Lcom/policydm/push/XPushProcess;->retryCount:I

    goto :goto_0

    .line 349
    :cond_2
    const-string v1, "result is Null"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 350
    invoke-static {v3}, Lcom/policydm/db/XDBSpdAdp;->xdbSetSpdState(I)V

    .line 351
    invoke-static {v4, v5}, Lcom/policydm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    goto :goto_0
.end method
