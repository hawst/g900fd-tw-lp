.class Lcom/policydm/push/XPushProcess$6;
.super Landroid/os/AsyncTask;
.source "XPushProcess.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/policydm/push/XPushProcess;->sendSPPResult(Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Landroid/content/Intent;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/policydm/push/XPushProcess;


# direct methods
.method constructor <init>(Lcom/policydm/push/XPushProcess;)V
    .locals 0

    .prologue
    .line 391
    iput-object p1, p0, Lcom/policydm/push/XPushProcess$6;->this$0:Lcom/policydm/push/XPushProcess;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 391
    check-cast p1, [Landroid/content/Intent;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/policydm/push/XPushProcess$6;->doInBackground([Landroid/content/Intent;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Landroid/content/Intent;)Ljava/lang/Void;
    .locals 8
    .param p1, "params"    # [Landroid/content/Intent;

    .prologue
    const/4 v7, 0x0

    .line 395
    const/4 v1, 0x0

    .line 396
    .local v1, "isSuccess":Z
    aget-object v4, p1, v7

    const-string v5, "com.sec.spp.Status"

    const/4 v6, 0x1

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 397
    .local v3, "state":I
    aget-object v4, p1, v7

    const-string v5, "Error"

    invoke-virtual {v4, v5, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 398
    .local v0, "error":I
    aget-object v4, p1, v7

    const-string v5, "RegistrationID"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 400
    .local v2, "regID":Ljava/lang/String;
    if-nez v3, :cond_1

    iget-object v4, p0, Lcom/policydm/push/XPushProcess$6;->this$0:Lcom/policydm/push/XPushProcess;

    iget-boolean v4, v4, Lcom/policydm/push/XPushProcess;->bRegMode:Z

    if-eqz v4, :cond_1

    .line 402
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 404
    const-string v4, "SPP Push registration Success"

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 405
    const/4 v1, 0x1

    .line 414
    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/policydm/push/XPushProcess$6;->this$0:Lcom/policydm/push/XPushProcess;

    new-instance v5, Lcom/policydm/push/XSPPResult;

    invoke-direct {v5, v1}, Lcom/policydm/push/XSPPResult;-><init>(Z)V

    iput-object v5, v4, Lcom/policydm/push/XPushProcess;->mSPPResult:Lcom/policydm/push/XSPPResult;

    .line 415
    iget-object v4, p0, Lcom/policydm/push/XPushProcess$6;->this$0:Lcom/policydm/push/XPushProcess;

    iget-object v4, v4, Lcom/policydm/push/XPushProcess;->mSPPResult:Lcom/policydm/push/XSPPResult;

    invoke-virtual {v4}, Lcom/policydm/push/XSPPResult;->isSuccess()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 416
    iget-object v4, p0, Lcom/policydm/push/XPushProcess$6;->this$0:Lcom/policydm/push/XPushProcess;

    iget-object v4, v4, Lcom/policydm/push/XPushProcess;->mSPPResult:Lcom/policydm/push/XSPPResult;

    invoke-virtual {v4, v2}, Lcom/policydm/push/XSPPResult;->setPushID(Ljava/lang/String;)V

    .line 419
    :goto_1
    const/4 v4, 0x0

    return-object v4

    .line 408
    :cond_1
    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    iget-object v4, p0, Lcom/policydm/push/XPushProcess$6;->this$0:Lcom/policydm/push/XPushProcess;

    iget-boolean v4, v4, Lcom/policydm/push/XPushProcess;->bRegMode:Z

    if-nez v4, :cond_0

    .line 410
    const-string v4, "SPP Push unregistration Success"

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 411
    const/4 v1, 0x1

    goto :goto_0

    .line 418
    :cond_2
    iget-object v4, p0, Lcom/policydm/push/XPushProcess$6;->this$0:Lcom/policydm/push/XPushProcess;

    iget-object v4, v4, Lcom/policydm/push/XPushProcess;->mSPPResult:Lcom/policydm/push/XSPPResult;

    invoke-virtual {v4, v0}, Lcom/policydm/push/XSPPResult;->setError(I)V

    goto :goto_1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 391
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/policydm/push/XPushProcess$6;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 6
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    const/16 v5, 0x7a

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 425
    iget-object v1, p0, Lcom/policydm/push/XPushProcess$6;->this$0:Lcom/policydm/push/XPushProcess;

    iget-object v1, v1, Lcom/policydm/push/XPushProcess;->mSPPResult:Lcom/policydm/push/XSPPResult;

    if-eqz v1, :cond_1

    .line 427
    iget-object v1, p0, Lcom/policydm/push/XPushProcess$6;->this$0:Lcom/policydm/push/XPushProcess;

    iget-object v1, v1, Lcom/policydm/push/XPushProcess;->mSPPResult:Lcom/policydm/push/XSPPResult;

    invoke-virtual {v1}, Lcom/policydm/push/XSPPResult;->isSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 429
    iget-object v1, p0, Lcom/policydm/push/XPushProcess$6;->this$0:Lcom/policydm/push/XPushProcess;

    iget-object v1, v1, Lcom/policydm/push/XPushProcess;->mSPPResult:Lcom/policydm/push/XSPPResult;

    invoke-virtual {v1}, Lcom/policydm/push/XSPPResult;->getPushID()Ljava/lang/String;

    move-result-object v0

    .line 430
    .local v0, "regiId":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "spp id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 431
    invoke-static {v0}, Lcom/policydm/db/XDBSpdAdp;->xdbSetSpdPushRegId(Ljava/lang/String;)V

    .line 433
    invoke-static {v4}, Lcom/policydm/XDMBroadcastReceiver;->setStartDeviceRegister(Z)V

    .line 434
    const/16 v1, 0x3e

    invoke-static {v1, v3, v3}, Lcom/policydm/eng/core/XDMMsg;->xdmSendRCMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 451
    .end local v0    # "regiId":Ljava/lang/String;
    :goto_0
    return-void

    .line 438
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "spp error : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/policydm/push/XPushProcess$6;->this$0:Lcom/policydm/push/XPushProcess;

    iget-object v2, v2, Lcom/policydm/push/XPushProcess;->mSPPResult:Lcom/policydm/push/XSPPResult;

    invoke-virtual {v2}, Lcom/policydm/push/XSPPResult;->getError()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 439
    invoke-static {v4}, Lcom/policydm/db/XDBSpdAdp;->xdbSetSpdState(I)V

    .line 440
    invoke-static {v3, v5}, Lcom/policydm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    goto :goto_0

    .line 445
    :cond_1
    const-string v1, "result is Null"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 446
    invoke-static {v4}, Lcom/policydm/db/XDBSpdAdp;->xdbSetSpdState(I)V

    .line 447
    invoke-static {v3, v5}, Lcom/policydm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    goto :goto_0
.end method
