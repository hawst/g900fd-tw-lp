.class public Lcom/policydm/push/XGCMResult;
.super Ljava/lang/Object;
.source "XGCMResult.java"


# static fields
.field public static final REGISTER_ERROR:Ljava/lang/String; = "PHONE_REGISTRATION_ERROR"

.field public static final SERVICE_ERROR:Ljava/lang/String; = "SERVICE_NOT_AVAILABLE"


# instance fields
.field protected bSuccess:Z

.field protected mErrorMsg:Ljava/lang/String;

.field protected mPushID:Ljava/lang/String;


# direct methods
.method public constructor <init>(Z)V
    .locals 1
    .param p1, "success"    # Z

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/policydm/push/XGCMResult;->bSuccess:Z

    .line 17
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/push/XGCMResult;->mPushID:Ljava/lang/String;

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/push/XGCMResult;->mErrorMsg:Ljava/lang/String;

    .line 23
    iput-boolean p1, p0, Lcom/policydm/push/XGCMResult;->bSuccess:Z

    .line 24
    return-void
.end method


# virtual methods
.method public getError()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/policydm/push/XGCMResult;->mErrorMsg:Ljava/lang/String;

    return-object v0
.end method

.method public getPushID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/policydm/push/XGCMResult;->mPushID:Ljava/lang/String;

    return-object v0
.end method

.method public isSuccess()Z
    .locals 1

    .prologue
    .line 31
    iget-boolean v0, p0, Lcom/policydm/push/XGCMResult;->bSuccess:Z

    return v0
.end method

.method public setError(Ljava/lang/String;)V
    .locals 2
    .param p1, "error"    # Ljava/lang/String;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/policydm/push/XGCMResult;->mErrorMsg:Ljava/lang/String;

    .line 53
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GCM Register error: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 54
    return-void
.end method

.method public setNextRetry()V
    .locals 2

    .prologue
    .line 66
    const-string v0, "SERVICE_NOT_AVAILABLE"

    iget-object v1, p0, Lcom/policydm/push/XGCMResult;->mErrorMsg:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "PHONE_REGISTRATION_ERROR"

    iget-object v1, p0, Lcom/policydm/push/XGCMResult;->mErrorMsg:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 68
    :cond_0
    const-string v0, "need to register GCM next time"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 69
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/policydm/push/XPushProcess;->sendMessage(I)V

    .line 71
    :cond_1
    return-void
.end method

.method public setPushID(Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/policydm/push/XGCMResult;->mPushID:Ljava/lang/String;

    .line 37
    return-void
.end method
