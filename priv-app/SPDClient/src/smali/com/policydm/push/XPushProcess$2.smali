.class Lcom/policydm/push/XPushProcess$2;
.super Ljava/lang/Object;
.source "XPushProcess.java"

# interfaces
.implements Lcom/policydm/push/XSPPRespReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/policydm/push/XPushProcess;->callSPPRegiProcess()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/policydm/push/XPushProcess;


# direct methods
.method constructor <init>(Lcom/policydm/push/XPushProcess;)V
    .locals 0

    .prologue
    .line 245
    iput-object p1, p0, Lcom/policydm/push/XPushProcess$2;->this$0:Lcom/policydm/push/XPushProcess;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onXSPPResponse(Lcom/policydm/push/XSPPResult;)V
    .locals 6
    .param p1, "result"    # Lcom/policydm/push/XSPPResult;

    .prologue
    const/16 v5, 0x7a

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 249
    if-eqz p1, :cond_2

    .line 251
    invoke-virtual {p1}, Lcom/policydm/push/XSPPResult;->isSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 253
    invoke-virtual {p1}, Lcom/policydm/push/XSPPResult;->getPushID()Ljava/lang/String;

    move-result-object v0

    .line 254
    .local v0, "regiId":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "spp id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 255
    invoke-static {v0}, Lcom/policydm/db/XDBSpdAdp;->xdbSetSpdPushRegId(Ljava/lang/String;)V

    .line 257
    invoke-static {v4}, Lcom/policydm/XDMBroadcastReceiver;->setStartDeviceRegister(Z)V

    .line 258
    const/16 v1, 0x3e

    invoke-static {v1, v3, v3}, Lcom/policydm/eng/core/XDMMsg;->xdmSendRCMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 282
    .end local v0    # "regiId":Ljava/lang/String;
    :goto_0
    invoke-static {}, Lcom/policydm/push/XPushProcess;->getPush()Lcom/policydm/push/XPushProcess;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/policydm/push/XPushProcess;->setSPPReceiver(Lcom/policydm/push/XSPPRespReceiver;)V

    .line 283
    return-void

    .line 262
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "spp error : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/policydm/push/XSPPResult;->getError()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 263
    invoke-static {v4}, Lcom/policydm/db/XDBSpdAdp;->xdbSetSpdState(I)V

    .line 264
    invoke-static {v3, v5}, Lcom/policydm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    .line 265
    iget-object v1, p0, Lcom/policydm/push/XPushProcess$2;->this$0:Lcom/policydm/push/XPushProcess;

    iget-boolean v1, v1, Lcom/policydm/push/XPushProcess;->bRegMode:Z

    if-eqz v1, :cond_1

    sget v1, Lcom/policydm/push/XPushProcess;->retryCount:I

    const/4 v2, 0x3

    if-ge v1, v2, :cond_1

    .line 267
    sget v1, Lcom/policydm/push/XPushProcess;->retryCount:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/policydm/push/XPushProcess;->retryCount:I

    .line 268
    iget-object v1, p0, Lcom/policydm/push/XPushProcess$2;->this$0:Lcom/policydm/push/XPushProcess;

    iget-object v1, v1, Lcom/policydm/push/XPushProcess;->mGCMResult:Lcom/policydm/push/XGCMResult;

    invoke-virtual {v1}, Lcom/policydm/push/XGCMResult;->setNextRetry()V

    goto :goto_0

    .line 272
    :cond_1
    sput v4, Lcom/policydm/push/XPushProcess;->retryCount:I

    goto :goto_0

    .line 278
    :cond_2
    const-string v1, "result is Null"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 279
    invoke-static {v4}, Lcom/policydm/db/XDBSpdAdp;->xdbSetSpdState(I)V

    .line 280
    invoke-static {v3, v5}, Lcom/policydm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    goto :goto_0
.end method
