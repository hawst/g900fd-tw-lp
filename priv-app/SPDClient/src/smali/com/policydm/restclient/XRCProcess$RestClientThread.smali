.class public Lcom/policydm/restclient/XRCProcess$RestClientThread;
.super Ljava/lang/Thread;
.source "XRCProcess.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/policydm/restclient/XRCProcess;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "RestClientThread"
.end annotation


# instance fields
.field arg1:I

.field state:I

.field final synthetic this$0:Lcom/policydm/restclient/XRCProcess;

.field url:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/policydm/restclient/XRCProcess;Ljava/lang/String;II)V
    .locals 2
    .param p2, "reqUrl"    # Ljava/lang/String;
    .param p3, "devState"    # I
    .param p4, "arg1"    # I

    .prologue
    const/4 v1, 0x0

    .line 119
    iput-object p1, p0, Lcom/policydm/restclient/XRCProcess$RestClientThread;->this$0:Lcom/policydm/restclient/XRCProcess;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 114
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/policydm/restclient/XRCProcess$RestClientThread;->url:Ljava/lang/String;

    .line 115
    iput v1, p0, Lcom/policydm/restclient/XRCProcess$RestClientThread;->state:I

    .line 116
    iput v1, p0, Lcom/policydm/restclient/XRCProcess$RestClientThread;->arg1:I

    .line 120
    iput-object p2, p0, Lcom/policydm/restclient/XRCProcess$RestClientThread;->url:Ljava/lang/String;

    .line 121
    iput p3, p0, Lcom/policydm/restclient/XRCProcess$RestClientThread;->state:I

    .line 122
    iput p4, p0, Lcom/policydm/restclient/XRCProcess$RestClientThread;->arg1:I

    .line 123
    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 129
    const/4 v1, 0x1

    :try_start_0
    # setter for: Lcom/policydm/restclient/XRCProcess;->mThread:Z
    invoke-static {v1}, Lcom/policydm/restclient/XRCProcess;->access$002(Z)Z

    .line 130
    iget-object v1, p0, Lcom/policydm/restclient/XRCProcess$RestClientThread;->this$0:Lcom/policydm/restclient/XRCProcess;

    iget-object v2, p0, Lcom/policydm/restclient/XRCProcess$RestClientThread;->url:Ljava/lang/String;

    iget v3, p0, Lcom/policydm/restclient/XRCProcess$RestClientThread;->state:I

    iget v4, p0, Lcom/policydm/restclient/XRCProcess$RestClientThread;->arg1:I

    # invokes: Lcom/policydm/restclient/XRCProcess;->RestClientProcess(Ljava/lang/String;II)V
    invoke-static {v1, v2, v3, v4}, Lcom/policydm/restclient/XRCProcess;->access$100(Lcom/policydm/restclient/XRCProcess;Ljava/lang/String;II)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 140
    :goto_0
    return-void

    .line 132
    :catch_0
    move-exception v0

    .line 134
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 136
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 138
    .local v0, "e":Ljava/lang/Throwable;
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method
