.class public Lcom/policydm/restclient/XRCProcess;
.super Ljava/lang/Object;
.source "XRCProcess.java"

# interfaces
.implements Lcom/policydm/interfaces/XDMInterface;
.implements Lcom/policydm/interfaces/XEventInterface;
.implements Lcom/policydm/interfaces/XSPDInterface;
.implements Lcom/policydm/interfaces/XTPInterface;
.implements Lcom/policydm/interfaces/XUIInterface;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/policydm/restclient/XRCProcess$RestClientThread;,
        Lcom/policydm/restclient/XRCProcess$HttpMethod;
    }
.end annotation


# static fields
.field private static RestClientThread:Lcom/policydm/restclient/XRCProcess$RestClientThread;

.field public static hRestClient:Landroid/os/Handler;

.field private static httpclient:Lorg/apache/http/impl/client/DefaultHttpClient;

.field private static mThread:Z


# instance fields
.field private final MaxRetryCnt:I

.field private final mConnTimeOut:I

.field private mPollingXML:Lcom/policydm/polling/XPollingXml;

.field private mSPDXML:Lcom/policydm/spd/XSPDXml;

.field private final mSockTimeOut:I

.field private m_bufDebug:Ljava/io/ByteArrayOutputStream;

.field private nRecvCount:I

.field private nRetryCnt:I

.field private out:Ljava/lang/StringBuffer;

.field private reqXML:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 75
    const/4 v0, 0x0

    sput-boolean v0, Lcom/policydm/restclient/XRCProcess;->mThread:Z

    .line 78
    sput-object v1, Lcom/policydm/restclient/XRCProcess;->httpclient:Lorg/apache/http/impl/client/DefaultHttpClient;

    .line 79
    sput-object v1, Lcom/policydm/restclient/XRCProcess;->RestClientThread:Lcom/policydm/restclient/XRCProcess$RestClientThread;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/16 v0, 0x7530

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput-object v1, p0, Lcom/policydm/restclient/XRCProcess;->reqXML:Ljava/lang/String;

    .line 81
    iput-object v1, p0, Lcom/policydm/restclient/XRCProcess;->mSPDXML:Lcom/policydm/spd/XSPDXml;

    .line 82
    iput-object v1, p0, Lcom/policydm/restclient/XRCProcess;->mPollingXML:Lcom/policydm/polling/XPollingXml;

    .line 84
    iput v0, p0, Lcom/policydm/restclient/XRCProcess;->mConnTimeOut:I

    .line 85
    iput v0, p0, Lcom/policydm/restclient/XRCProcess;->mSockTimeOut:I

    .line 87
    iput v2, p0, Lcom/policydm/restclient/XRCProcess;->nRetryCnt:I

    .line 88
    const/4 v0, 0x2

    iput v0, p0, Lcom/policydm/restclient/XRCProcess;->MaxRetryCnt:I

    .line 91
    iput-object v1, p0, Lcom/policydm/restclient/XRCProcess;->out:Ljava/lang/StringBuffer;

    .line 92
    iput v2, p0, Lcom/policydm/restclient/XRCProcess;->nRecvCount:I

    .line 106
    new-instance v0, Lcom/policydm/spd/XSPDXml;

    invoke-direct {v0}, Lcom/policydm/spd/XSPDXml;-><init>()V

    iput-object v0, p0, Lcom/policydm/restclient/XRCProcess;->mSPDXML:Lcom/policydm/spd/XSPDXml;

    .line 107
    new-instance v0, Lcom/policydm/polling/XPollingXml;

    invoke-direct {v0}, Lcom/policydm/polling/XPollingXml;-><init>()V

    iput-object v0, p0, Lcom/policydm/restclient/XRCProcess;->mPollingXML:Lcom/policydm/polling/XPollingXml;

    .line 109
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 110
    return-void
.end method

.method private RestClientProcess(Ljava/lang/String;II)V
    .locals 17
    .param p1, "szUrl"    # Ljava/lang/String;
    .param p2, "nState"    # I
    .param p3, "arg1"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 201
    const/4 v6, 0x0

    .line 202
    .local v6, "instream":Ljava/io/InputStream;
    const/4 v11, 0x0

    .line 203
    .local v11, "response":Lorg/apache/http/HttpResponse;
    const/4 v12, 0x0

    .line 205
    .local v12, "statusCode":I
    invoke-static/range {p2 .. p2}, Lcom/policydm/db/XDBSpdAdp;->xdbSetSpdState(I)V

    .line 208
    :goto_0
    sget-boolean v14, Lcom/policydm/restclient/XRCProcess;->mThread:Z

    if-eqz v14, :cond_12

    .line 210
    const/4 v14, 0x1

    move/from16 v0, p2

    if-ne v0, v14, :cond_3

    .line 212
    invoke-static {}, Lcom/policydm/db/XDBSpdAdp;->xdbGetSpdDeviceRegister()I

    move-result v14

    const/4 v15, 0x1

    if-ne v14, v15, :cond_2

    .line 214
    const/4 v14, 0x2

    move/from16 v0, p3

    if-eq v0, v14, :cond_0

    if-nez p3, :cond_2

    .line 216
    :cond_0
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/policydm/restclient/XRCProcess;->callRestClientFinish(I)V

    .line 523
    :cond_1
    :goto_1
    return-void

    .line 220
    :cond_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/policydm/restclient/XRCProcess;->mSPDXML:Lcom/policydm/spd/XSPDXml;

    const/4 v15, 0x1

    invoke-virtual {v14, v15}, Lcom/policydm/spd/XSPDXml;->getXML(I)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/policydm/restclient/XRCProcess;->reqXML:Ljava/lang/String;

    .line 221
    sget-object v14, Lcom/policydm/restclient/XRCProcess$HttpMethod;->POST:Lcom/policydm/restclient/XRCProcess$HttpMethod;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/policydm/restclient/XRCProcess;->reqXML:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    invoke-direct {v0, v14, v1, v15, v2}, Lcom/policydm/restclient/XRCProcess;->execute(Lcom/policydm/restclient/XRCProcess$HttpMethod;Ljava/lang/String;Ljava/lang/String;I)Lorg/apache/http/HttpResponse;

    move-result-object v11

    .line 288
    :goto_2
    if-nez v11, :cond_14

    .line 290
    const-wide/16 v14, 0x1f4

    invoke-static {v14, v15}, Landroid/os/SystemClock;->sleep(J)V

    goto :goto_0

    .line 223
    :cond_3
    const/4 v14, 0x2

    move/from16 v0, p2

    if-ne v0, v14, :cond_4

    .line 225
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/policydm/restclient/XRCProcess;->mSPDXML:Lcom/policydm/spd/XSPDXml;

    const/4 v15, 0x2

    invoke-virtual {v14, v15}, Lcom/policydm/spd/XSPDXml;->getXML(I)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/policydm/restclient/XRCProcess;->reqXML:Ljava/lang/String;

    .line 226
    sget-object v14, Lcom/policydm/restclient/XRCProcess$HttpMethod;->PUT:Lcom/policydm/restclient/XRCProcess$HttpMethod;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/policydm/restclient/XRCProcess;->reqXML:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    invoke-direct {v0, v14, v1, v15, v2}, Lcom/policydm/restclient/XRCProcess;->execute(Lcom/policydm/restclient/XRCProcess$HttpMethod;Ljava/lang/String;Ljava/lang/String;I)Lorg/apache/http/HttpResponse;

    move-result-object v11

    goto :goto_2

    .line 228
    :cond_4
    const/4 v14, 0x3

    move/from16 v0, p2

    if-ne v0, v14, :cond_7

    .line 230
    invoke-static {}, Lcom/policydm/db/XDBSpdAdp;->xdbGetSpdPushRegId()Ljava/lang/String;

    move-result-object v8

    .line 231
    .local v8, "pushRegId":Ljava/lang/String;
    if-eqz v8, :cond_5

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_5

    const-string v14, "null"

    invoke-virtual {v8, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 233
    :cond_5
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/policydm/restclient/XRCProcess;->callRestClientFinish(I)V

    goto :goto_1

    .line 236
    :cond_6
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/policydm/restclient/XRCProcess;->mSPDXML:Lcom/policydm/spd/XSPDXml;

    const/4 v15, 0x3

    invoke-virtual {v14, v15}, Lcom/policydm/spd/XSPDXml;->getXML(I)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/policydm/restclient/XRCProcess;->reqXML:Ljava/lang/String;

    .line 237
    sget-object v14, Lcom/policydm/restclient/XRCProcess$HttpMethod;->PUT:Lcom/policydm/restclient/XRCProcess$HttpMethod;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/policydm/restclient/XRCProcess;->reqXML:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    invoke-direct {v0, v14, v1, v15, v2}, Lcom/policydm/restclient/XRCProcess;->execute(Lcom/policydm/restclient/XRCProcess$HttpMethod;Ljava/lang/String;Ljava/lang/String;I)Lorg/apache/http/HttpResponse;

    move-result-object v11

    .line 238
    goto :goto_2

    .line 239
    .end local v8    # "pushRegId":Ljava/lang/String;
    :cond_7
    const/4 v14, 0x4

    move/from16 v0, p2

    if-ne v0, v14, :cond_f

    .line 241
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMOResultCode()Ljava/lang/String;

    move-result-object v13

    .line 242
    .local v13, "szResult":Ljava/lang/String;
    invoke-static {}, Lcom/policydm/ui/XUIAdapter;->xuiAdpGetUiMode()I

    move-result v14

    const/4 v15, 0x1

    if-ne v14, v15, :cond_8

    .line 244
    const-string v14, "200"

    invoke-virtual {v14, v13}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v14

    if-nez v14, :cond_a

    .line 246
    const/4 v14, 0x5

    invoke-static {v14}, Lcom/policydm/ui/XUINotificationManager;->xuiSetIndicator(I)V

    .line 261
    :goto_3
    const/4 v14, 0x0

    invoke-static {v14}, Lcom/policydm/ui/XUIAdapter;->xuiAdpSetUiMode(I)V

    .line 263
    :cond_8
    invoke-static {}, Lcom/policydm/ui/XUIDialogActivity;->xuiDlgRemove()V

    .line 265
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMOProcessId()Ljava/lang/String;

    move-result-object v7

    .line 266
    .local v7, "processingId":Ljava/lang/String;
    if-eqz v7, :cond_9

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_9

    const-string v14, "null"

    invoke-virtual {v7, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_9

    if-eqz v13, :cond_9

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_9

    const-string v14, "null"

    invoke-virtual {v13, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v14

    if-eqz v14, :cond_e

    .line 268
    :cond_9
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/policydm/restclient/XRCProcess;->callRestClientFinish(I)V

    goto/16 :goto_1

    .line 248
    .end local v7    # "processingId":Ljava/lang/String;
    :cond_a
    const-string v14, "401"

    invoke-virtual {v14, v13}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v14

    if-eqz v14, :cond_b

    const-string v14, "402"

    invoke-virtual {v14, v13}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v14

    if-eqz v14, :cond_b

    const-string v14, "403"

    invoke-virtual {v14, v13}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v14

    if-eqz v14, :cond_b

    const-string v14, "404"

    invoke-virtual {v14, v13}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v14

    if-nez v14, :cond_c

    .line 250
    :cond_b
    const/4 v14, 0x2

    invoke-static {v14}, Lcom/policydm/ui/XUINotificationManager;->xuiSetIndicator(I)V

    goto :goto_3

    .line 252
    :cond_c
    const-string v14, "501"

    invoke-virtual {v14, v13}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v14

    if-nez v14, :cond_d

    .line 254
    const/4 v14, 0x4

    invoke-static {v14}, Lcom/policydm/ui/XUINotificationManager;->xuiSetIndicator(I)V

    goto :goto_3

    .line 258
    :cond_d
    const/4 v14, 0x3

    invoke-static {v14}, Lcom/policydm/ui/XUINotificationManager;->xuiSetIndicator(I)V

    goto :goto_3

    .line 271
    .restart local v7    # "processingId":Ljava/lang/String;
    :cond_e
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/policydm/restclient/XRCProcess;->mSPDXML:Lcom/policydm/spd/XSPDXml;

    invoke-virtual {v14, v7, v13}, Lcom/policydm/spd/XSPDXml;->getResultXML(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/policydm/restclient/XRCProcess;->reqXML:Ljava/lang/String;

    .line 272
    sget-object v14, Lcom/policydm/restclient/XRCProcess$HttpMethod;->POST:Lcom/policydm/restclient/XRCProcess$HttpMethod;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/policydm/restclient/XRCProcess;->reqXML:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    invoke-direct {v0, v14, v1, v15, v2}, Lcom/policydm/restclient/XRCProcess;->execute(Lcom/policydm/restclient/XRCProcess$HttpMethod;Ljava/lang/String;Ljava/lang/String;I)Lorg/apache/http/HttpResponse;

    move-result-object v11

    .line 273
    goto/16 :goto_2

    .line 274
    .end local v7    # "processingId":Ljava/lang/String;
    .end local v13    # "szResult":Ljava/lang/String;
    :cond_f
    const/4 v14, 0x5

    move/from16 v0, p2

    if-ne v0, v14, :cond_10

    .line 276
    sget-object v14, Lcom/policydm/restclient/XRCProcess$HttpMethod;->GET:Lcom/policydm/restclient/XRCProcess$HttpMethod;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    invoke-direct {v0, v14, v1, v15, v2}, Lcom/policydm/restclient/XRCProcess;->execute(Lcom/policydm/restclient/XRCProcess$HttpMethod;Ljava/lang/String;Ljava/lang/String;I)Lorg/apache/http/HttpResponse;

    move-result-object v11

    goto/16 :goto_2

    .line 278
    :cond_10
    const/4 v14, 0x6

    move/from16 v0, p2

    if-ne v0, v14, :cond_11

    .line 280
    sget-object v14, Lcom/policydm/restclient/XRCProcess$HttpMethod;->GET:Lcom/policydm/restclient/XRCProcess$HttpMethod;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    invoke-direct {v0, v14, v1, v15, v2}, Lcom/policydm/restclient/XRCProcess;->execute(Lcom/policydm/restclient/XRCProcess$HttpMethod;Ljava/lang/String;Ljava/lang/String;I)Lorg/apache/http/HttpResponse;

    move-result-object v11

    goto/16 :goto_2

    .line 284
    :cond_11
    const-string v14, "XSPD State not defined"

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 297
    :cond_12
    const/4 v14, 0x6

    move/from16 v0, p2

    if-ne v0, v14, :cond_13

    .line 299
    const-string v14, "Fail get version.xml, Set default polling time"

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 300
    invoke-static {}, Lcom/policydm/polling/XPollingAgent;->xpollingDefaultPollingTime()V

    .line 303
    :cond_13
    const/4 v14, 0x4

    move/from16 v0, p2

    if-eq v0, v14, :cond_1

    const/4 v14, 0x5

    move/from16 v0, p2

    if-eq v0, v14, :cond_1

    .line 304
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/policydm/restclient/XRCProcess;->callRestClientFinish(I)V

    goto/16 :goto_1

    .line 311
    :cond_14
    if-eqz v11, :cond_18

    .line 312
    :try_start_0
    invoke-interface {v11}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v14

    invoke-interface {v14}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v12

    .line 324
    sget-boolean v14, Lcom/policydm/restclient/XRCProcess;->mThread:Z

    if-eqz v14, :cond_33

    .line 326
    const/16 v14, 0xc8

    if-ne v12, v14, :cond_1f

    .line 328
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "HTTP status is "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 329
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput v14, v0, Lcom/policydm/restclient/XRCProcess;->nRetryCnt:I

    .line 331
    const/4 v14, 0x1

    move/from16 v0, p2

    if-ne v0, v14, :cond_1b

    .line 333
    invoke-virtual/range {p0 .. p0}, Lcom/policydm/restclient/XRCProcess;->getInputStream()Ljava/io/InputStream;

    move-result-object v6

    .line 334
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/policydm/restclient/XRCProcess;->mSPDXML:Lcom/policydm/spd/XSPDXml;

    invoke-virtual {v14, v6}, Lcom/policydm/spd/XSPDXml;->getParsingRespConfig(Ljava/io/InputStream;)V

    .line 335
    invoke-static {}, Lcom/policydm/polling/XPollingAgent;->xpollingUpdateInfo()V

    .line 337
    const/4 v14, 0x1

    invoke-static {v14}, Lcom/policydm/db/XDBSpdAdp;->xdbSetSpdDeviceCreate(I)V

    .line 339
    const/4 v14, 0x0

    invoke-static {v14}, Lcom/policydm/XDMBroadcastReceiver;->setStartDeviceRegister(Z)V

    .line 340
    const/4 v14, 0x7

    invoke-static {v14}, Lcom/policydm/ui/XUINotificationManager;->xuiSetIndicator(I)V

    .line 342
    const/4 v14, 0x2

    move/from16 v0, p3

    if-eq v0, v14, :cond_15

    if-nez p3, :cond_19

    .line 344
    :cond_15
    invoke-static {}, Lcom/policydm/db/XDBPollingAdp;->xdbGetPollingStatusReportTime()J

    move-result-wide v9

    .line 345
    .local v9, "reporttime":J
    const-wide/16 v14, 0x0

    cmp-long v14, v9, v14

    if-eqz v14, :cond_16

    .line 347
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "report polling time : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-static {v9, v10}, Lcom/policydm/XDMApplication;->xdmDisplayAlarmTime(J)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 348
    invoke-static {}, Lcom/policydm/polling/XPollingAgent;->xPollingReportReStartAlarm()V

    .line 350
    :cond_16
    invoke-static {}, Lcom/policydm/XDMBroadcastReceiver;->callPushRegisterStart()V

    .line 433
    .end local v9    # "reporttime":J
    :cond_17
    :goto_4
    move-object/from16 v0, p0

    iget v14, v0, Lcom/policydm/restclient/XRCProcess;->nRetryCnt:I

    if-eqz v14, :cond_2d

    move-object/from16 v0, p0

    iget v14, v0, Lcom/policydm/restclient/XRCProcess;->nRetryCnt:I

    const/4 v15, 0x2

    if-gt v14, v15, :cond_2d

    .line 435
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "signature is invalid!! try retry. nRetryCnt="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget v15, v0, Lcom/policydm/restclient/XRCProcess;->nRetryCnt:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 437
    const/4 v14, 0x2

    move/from16 v0, p2

    if-ne v0, v14, :cond_24

    .line 438
    const/16 v14, 0x3d

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-static/range {v14 .. v16}, Lcom/policydm/eng/core/XDMMsg;->xdmSendRCMessage(ILjava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 519
    :catch_0
    move-exception v4

    .line 521
    .local v4, "e":Ljava/lang/Throwable;
    invoke-virtual {v4}, Ljava/lang/Throwable;->printStackTrace()V

    goto/16 :goto_1

    .line 315
    .end local v4    # "e":Ljava/lang/Throwable;
    :cond_18
    :try_start_1
    const-string v14, "statusCode is null !!"

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 316
    const/4 v14, 0x6

    move/from16 v0, p2

    if-ne v0, v14, :cond_1

    .line 318
    const-string v14, "Fail get version.xml, Set default polling time"

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 319
    invoke-static {}, Lcom/policydm/polling/XPollingAgent;->xpollingDefaultPollingTime()V

    goto/16 :goto_1

    .line 352
    :cond_19
    const/4 v14, 0x1

    move/from16 v0, p3

    if-ne v0, v14, :cond_1a

    .line 354
    const/4 v14, 0x2

    invoke-static {v14}, Lcom/policydm/ui/XUIAdapter;->xuiAdpUserInitiate(I)V

    goto :goto_4

    .line 356
    :cond_1a
    const/4 v14, 0x3

    move/from16 v0, p3

    if-ne v0, v14, :cond_17

    .line 358
    const/4 v14, 0x1

    invoke-static {v14}, Lcom/policydm/ui/XUIAdapter;->xuiAdpUserInitiate(I)V

    goto :goto_4

    .line 361
    :cond_1b
    const/4 v14, 0x2

    move/from16 v0, p2

    if-ne v0, v14, :cond_1c

    .line 363
    invoke-static {}, Lcom/policydm/adapter/XSPDAdapter;->xspdGetPolicyMode()Ljava/lang/String;

    move-result-object v14

    const-string v15, "enforcing"

    if-eq v14, v15, :cond_17

    .line 365
    invoke-static {}, Lcom/policydm/spd/XSPDAgent;->xspdDeviceReset()V

    .line 366
    invoke-static {}, Lcom/policydm/polling/XPollingAgent;->xPollingReportReStartAlarm()V

    goto :goto_4

    .line 369
    :cond_1c
    const/4 v14, 0x3

    move/from16 v0, p2

    if-ne v0, v14, :cond_1d

    .line 371
    const/4 v14, 0x1

    invoke-static {v14}, Lcom/policydm/db/XDBSpdAdp;->xdbSetSpdDeviceUpdate(I)V

    .line 373
    const/4 v14, 0x1

    invoke-static {v14}, Lcom/policydm/db/XDBSpdAdp;->xdbSetSpdDeviceRegister(I)V

    .line 374
    const/4 v14, 0x1

    invoke-static {v14}, Lcom/policydm/db/XDB;->xdbSetSPDRegisterStatus(I)V

    .line 376
    const/4 v14, 0x0

    invoke-static {v14}, Lcom/policydm/XDMBroadcastReceiver;->setStartDeviceRegister(Z)V

    goto/16 :goto_4

    .line 378
    :cond_1d
    const/4 v14, 0x5

    move/from16 v0, p2

    if-ne v0, v14, :cond_1e

    .line 380
    invoke-virtual/range {p0 .. p0}, Lcom/policydm/restclient/XRCProcess;->getInputStream()Ljava/io/InputStream;

    move-result-object v6

    .line 381
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/policydm/restclient/XRCProcess;->mSPDXML:Lcom/policydm/spd/XSPDXml;

    invoke-virtual {v14, v6}, Lcom/policydm/spd/XSPDXml;->getParsingRespConfig(Ljava/io/InputStream;)V

    .line 382
    invoke-static {}, Lcom/policydm/polling/XPollingAgent;->xpollingUpdateInfo()V

    goto/16 :goto_4

    .line 384
    :cond_1e
    const/4 v14, 0x6

    move/from16 v0, p2

    if-ne v0, v14, :cond_17

    .line 386
    invoke-virtual/range {p0 .. p0}, Lcom/policydm/restclient/XRCProcess;->getInputStream()Ljava/io/InputStream;

    move-result-object v6

    .line 387
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/policydm/restclient/XRCProcess;->mPollingXML:Lcom/policydm/polling/XPollingXml;

    invoke-virtual {v14, v6}, Lcom/policydm/polling/XPollingXml;->getParsingVersionInfo(Ljava/io/InputStream;)Z

    move-result v3

    .line 388
    .local v3, "bPolicyUpdate":Z
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "bPolicyUpdate is "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 389
    if-eqz v3, :cond_17

    .line 391
    invoke-static {}, Lcom/policydm/XDMBroadcastReceiver;->callPollingStart()V

    goto/16 :goto_4

    .line 397
    .end local v3    # "bPolicyUpdate":Z
    :cond_1f
    const/4 v14, 0x0

    invoke-static {v14}, Lcom/policydm/XDMBroadcastReceiver;->setStartDeviceRegister(Z)V

    .line 398
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "HTTP status is "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 399
    const/4 v14, 0x6

    move/from16 v0, p2

    if-eq v0, v14, :cond_23

    .line 401
    invoke-virtual/range {p0 .. p0}, Lcom/policydm/restclient/XRCProcess;->getInputStream()Ljava/io/InputStream;

    move-result-object v6

    .line 402
    if-eqz v6, :cond_22

    .line 404
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/policydm/restclient/XRCProcess;->mSPDXML:Lcom/policydm/spd/XSPDXml;

    invoke-virtual {v14, v6}, Lcom/policydm/spd/XSPDXml;->getParsingRespErr(Ljava/io/InputStream;)V

    .line 406
    invoke-static {}, Lcom/policydm/spd/XSPDXml;->getResponseErrorCode()Ljava/lang/String;

    move-result-object v5

    .line 407
    .local v5, "errCode":Ljava/lang/String;
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "SPD errCode is "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 409
    const/16 v14, 0x191

    if-ne v12, v14, :cond_20

    const-string v14, "2001"

    invoke-virtual {v14, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v14

    if-nez v14, :cond_20

    .line 411
    invoke-static {}, Lcom/policydm/spd/XSPDAgent;->xspdDeviceReset()V

    goto/16 :goto_4

    .line 413
    :cond_20
    const/16 v14, 0x190

    if-ne v12, v14, :cond_21

    const-string v14, "1102"

    invoke-virtual {v14, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v14

    if-nez v14, :cond_21

    .line 415
    move-object/from16 v0, p0

    iget v14, v0, Lcom/policydm/restclient/XRCProcess;->nRetryCnt:I

    add-int/lit8 v14, v14, 0x1

    move-object/from16 v0, p0

    iput v14, v0, Lcom/policydm/restclient/XRCProcess;->nRetryCnt:I

    goto/16 :goto_4

    .line 419
    :cond_21
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput v14, v0, Lcom/policydm/restclient/XRCProcess;->nRetryCnt:I

    goto/16 :goto_4

    .line 424
    .end local v5    # "errCode":Ljava/lang/String;
    :cond_22
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput v14, v0, Lcom/policydm/restclient/XRCProcess;->nRetryCnt:I

    goto/16 :goto_4

    .line 429
    :cond_23
    invoke-static {}, Lcom/policydm/polling/XPollingAgent;->xpollingDefaultPollingTime()V

    goto/16 :goto_4

    .line 439
    :cond_24
    const/4 v14, 0x3

    move/from16 v0, p2

    if-ne v0, v14, :cond_25

    .line 440
    const/16 v14, 0x3e

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-static/range {v14 .. v16}, Lcom/policydm/eng/core/XDMMsg;->xdmSendRCMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 441
    :cond_25
    const/4 v14, 0x4

    move/from16 v0, p2

    if-ne v0, v14, :cond_26

    .line 442
    const/16 v14, 0x3f

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-static/range {v14 .. v16}, Lcom/policydm/eng/core/XDMMsg;->xdmSendRCMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 443
    :cond_26
    const/4 v14, 0x5

    move/from16 v0, p2

    if-ne v0, v14, :cond_27

    .line 444
    const/16 v14, 0x40

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-static/range {v14 .. v16}, Lcom/policydm/eng/core/XDMMsg;->xdmSendRCMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 447
    :cond_27
    const-string v14, "cannot retry rc state. go finish"

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 448
    const/16 v14, 0xc8

    if-eq v12, v14, :cond_28

    invoke-static {}, Lcom/policydm/ui/XUIAdapter;->xuiAdpGetUiMode()I

    move-result v14

    const/4 v15, 0x1

    if-ne v14, v15, :cond_28

    .line 450
    const/4 v14, 0x4

    move/from16 v0, p2

    if-ne v0, v14, :cond_29

    .line 452
    const/4 v14, 0x0

    const/16 v15, 0x7a

    invoke-static {v14, v15}, Lcom/policydm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    .line 477
    :cond_28
    :goto_5
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/policydm/restclient/XRCProcess;->callRestClientFinish(I)V

    goto/16 :goto_1

    .line 456
    :cond_29
    const/4 v14, 0x1

    move/from16 v0, p2

    if-ne v0, v14, :cond_2c

    .line 458
    invoke-static {}, Lcom/policydm/spd/XSPDXml;->getResponseErrorCode()Ljava/lang/String;

    move-result-object v5

    .line 459
    .restart local v5    # "errCode":Ljava/lang/String;
    const-string v14, "2002"

    invoke-virtual {v14, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v14

    if-eqz v14, :cond_2a

    const-string v14, "2003"

    invoke-virtual {v14, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v14

    if-eqz v14, :cond_2a

    const-string v14, "2004"

    invoke-virtual {v14, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v14

    if-eqz v14, :cond_2a

    const-string v14, "2011"

    invoke-virtual {v14, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v14

    if-nez v14, :cond_2b

    .line 464
    :cond_2a
    const/4 v14, 0x0

    const/16 v15, 0x82

    invoke-static {v14, v15}, Lcom/policydm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    goto :goto_5

    .line 468
    :cond_2b
    const/4 v14, 0x0

    const/16 v15, 0x78

    invoke-static {v14, v15}, Lcom/policydm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    goto :goto_5

    .line 473
    .end local v5    # "errCode":Ljava/lang/String;
    :cond_2c
    const/4 v14, 0x0

    const/16 v15, 0x78

    invoke-static {v14, v15}, Lcom/policydm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    goto :goto_5

    .line 482
    :cond_2d
    const/16 v14, 0xc8

    if-eq v12, v14, :cond_2e

    invoke-static {}, Lcom/policydm/ui/XUIAdapter;->xuiAdpGetUiMode()I

    move-result v14

    const/4 v15, 0x1

    if-ne v14, v15, :cond_2e

    .line 484
    const/4 v14, 0x4

    move/from16 v0, p2

    if-ne v0, v14, :cond_2f

    .line 511
    :cond_2e
    :goto_6
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/policydm/restclient/XRCProcess;->callRestClientFinish(I)V

    goto/16 :goto_1

    .line 490
    :cond_2f
    const/4 v14, 0x1

    move/from16 v0, p2

    if-ne v0, v14, :cond_32

    .line 492
    invoke-static {}, Lcom/policydm/spd/XSPDXml;->getResponseErrorCode()Ljava/lang/String;

    move-result-object v5

    .line 493
    .restart local v5    # "errCode":Ljava/lang/String;
    const-string v14, "2002"

    invoke-virtual {v14, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v14

    if-eqz v14, :cond_30

    const-string v14, "2003"

    invoke-virtual {v14, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v14

    if-eqz v14, :cond_30

    const-string v14, "2004"

    invoke-virtual {v14, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v14

    if-eqz v14, :cond_30

    const-string v14, "2011"

    invoke-virtual {v14, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v14

    if-nez v14, :cond_31

    .line 498
    :cond_30
    const/4 v14, 0x0

    const/16 v15, 0x82

    invoke-static {v14, v15}, Lcom/policydm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    goto :goto_6

    .line 502
    :cond_31
    const/4 v14, 0x0

    const/16 v15, 0x78

    invoke-static {v14, v15}, Lcom/policydm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    goto :goto_6

    .line 507
    .end local v5    # "errCode":Ljava/lang/String;
    :cond_32
    const/4 v14, 0x0

    const/16 v15, 0x78

    invoke-static {v14, v15}, Lcom/policydm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    goto :goto_6

    .line 516
    :cond_33
    const-string v14, "mThread is false !!!!!"

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1
.end method

.method static synthetic access$002(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 73
    sput-boolean p0, Lcom/policydm/restclient/XRCProcess;->mThread:Z

    return p0
.end method

.method static synthetic access$100(Lcom/policydm/restclient/XRCProcess;Ljava/lang/String;II)V
    .locals 0
    .param p0, "x0"    # Lcom/policydm/restclient/XRCProcess;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I
    .param p3, "x3"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 73
    invoke-direct {p0, p1, p2, p3}, Lcom/policydm/restclient/XRCProcess;->RestClientProcess(Ljava/lang/String;II)V

    return-void
.end method

.method private callRestClientFinish(I)V
    .locals 3
    .param p1, "nSPDState"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 931
    const-string v0, ""

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 933
    iput v1, p0, Lcom/policydm/restclient/XRCProcess;->nRetryCnt:I

    .line 934
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/policydm/restclient/XRCProcess;->reqXML:Ljava/lang/String;

    .line 936
    if-ne p1, v2, :cond_0

    invoke-static {}, Lcom/policydm/db/XDBSpdAdp;->xdbGetSpdDeviceRegister()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 963
    :goto_0
    return-void

    .line 941
    :cond_0
    const/4 v0, 0x4

    if-ne p1, v0, :cond_3

    .line 943
    const-string v0, ""

    invoke-static {v0}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOResultCode(Ljava/lang/String;)V

    .line 944
    const-string v0, ""

    invoke-static {v0}, Lcom/policydm/db/XDBFumoAdp;->xdbSetFUMOProcessId(Ljava/lang/String;)V

    .line 945
    const-string v0, ""

    invoke-static {v0}, Lcom/policydm/db/XDBFumoAdp;->xdbSetReportUrl(Ljava/lang/String;)Z

    .line 947
    invoke-static {}, Lcom/policydm/noti/XNOTIAdapter;->xnotiPushAdpClearSessionStatus()V

    .line 948
    invoke-static {}, Lcom/policydm/noti/XNOTIAdapter;->xnotiPushAdpHandleNotiQueue()V

    .line 950
    invoke-static {}, Lcom/policydm/db/XDBSpdAdp;->xdbGetSpdPushRegId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/policydm/db/XDBSpdAdp;->xdbGetSpdDeviceUpdate()I

    move-result v0

    if-eq v0, v2, :cond_2

    .line 952
    :cond_1
    invoke-static {}, Lcom/policydm/XDMBroadcastReceiver;->callPushRegisterStart()V

    .line 961
    :cond_2
    :goto_1
    invoke-static {v1}, Lcom/policydm/XDMBroadcastReceiver;->xdmSetCheckedIntent(Z)V

    .line 962
    invoke-static {v1}, Lcom/policydm/db/XDBSpdAdp;->xdbSetSpdState(I)V

    goto :goto_0

    .line 955
    :cond_3
    const/4 v0, 0x5

    if-ne p1, v0, :cond_2

    .line 957
    invoke-static {}, Lcom/policydm/noti/XNOTIAdapter;->xnotiPushAdpClearSessionStatus()V

    .line 958
    invoke-static {}, Lcom/policydm/noti/XNOTIAdapter;->xnotiPushAdpHandleNotiQueue()V

    goto :goto_1
.end method

.method private callRestClientThread(II)V
    .locals 5
    .param p1, "nState"    # I
    .param p2, "arg1"    # I

    .prologue
    .line 145
    const-string v2, ""

    .line 147
    .local v2, "szReqURL":Ljava/lang/String;
    const/4 v3, 0x6

    if-ne p1, v3, :cond_0

    .line 149
    invoke-static {}, Lcom/policydm/polling/XPollingAgent;->xpollingMakeUrl()Ljava/lang/String;

    move-result-object v2

    .line 181
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "szReqURL = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 185
    :try_start_0
    new-instance v3, Lcom/policydm/restclient/XRCProcess$RestClientThread;

    invoke-direct {v3, p0, v2, p1, p2}, Lcom/policydm/restclient/XRCProcess$RestClientThread;-><init>(Lcom/policydm/restclient/XRCProcess;Ljava/lang/String;II)V

    sput-object v3, Lcom/policydm/restclient/XRCProcess;->RestClientThread:Lcom/policydm/restclient/XRCProcess$RestClientThread;

    .line 186
    sget-object v3, Lcom/policydm/restclient/XRCProcess;->RestClientThread:Lcom/policydm/restclient/XRCProcess$RestClientThread;

    invoke-virtual {v3}, Lcom/policydm/restclient/XRCProcess$RestClientThread;->start()V

    .line 187
    sget-object v3, Lcom/policydm/restclient/XRCProcess;->RestClientThread:Lcom/policydm/restclient/XRCProcess$RestClientThread;

    invoke-virtual {v3}, Lcom/policydm/restclient/XRCProcess$RestClientThread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 197
    :goto_1
    return-void

    .line 151
    :cond_0
    const/4 v3, 0x4

    if-ne p1, v3, :cond_2

    .line 153
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetReportURL()Ljava/lang/String;

    move-result-object v2

    .line 154
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 155
    const-string v3, "/service/policy/result"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 157
    :cond_1
    const-string v3, "/service/policy/result"

    invoke-static {v3}, Lcom/policydm/db/XDB;->xdbSPDGetServerURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 161
    :cond_2
    const/4 v1, 0x0

    .line 162
    .local v1, "szPath":Ljava/lang/String;
    const/4 v3, 0x1

    if-ne p1, v3, :cond_4

    .line 164
    const-string v1, "/service/policy/device"

    .line 178
    :cond_3
    :goto_2
    invoke-static {v1}, Lcom/policydm/db/XDB;->xdbSPDGetServerURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 166
    :cond_4
    const/4 v3, 0x2

    if-ne p1, v3, :cond_5

    .line 168
    const-string v1, "/service/policy/device"

    goto :goto_2

    .line 170
    :cond_5
    const/4 v3, 0x3

    if-ne p1, v3, :cond_6

    .line 172
    const-string v1, "/service/policy/device/push"

    goto :goto_2

    .line 174
    :cond_6
    const/4 v3, 0x5

    if-ne p1, v3, :cond_3

    .line 176
    const-string v1, "/service/policy/config"

    goto :goto_2

    .line 189
    .end local v1    # "szPath":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 191
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    .line 193
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v0

    .line 195
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method private execute(Lcom/policydm/restclient/XRCProcess$HttpMethod;Ljava/lang/String;Ljava/lang/String;I)Lorg/apache/http/HttpResponse;
    .locals 21
    .param p1, "method"    # Lcom/policydm/restclient/XRCProcess$HttpMethod;
    .param p2, "reqUrl"    # Ljava/lang/String;
    .param p3, "reqBody"    # Ljava/lang/String;
    .param p4, "nState"    # I

    .prologue
    .line 621
    const/4 v12, 0x0

    .line 622
    .local v12, "request":Lorg/apache/http/client/methods/HttpUriRequest;
    const/4 v13, 0x0

    .line 624
    .local v13, "response":Lorg/apache/http/HttpResponse;
    const-string v16, ""

    invoke-static/range {v16 .. v16}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 626
    sget-object v16, Lcom/policydm/restclient/XRCProcess$HttpMethod;->GET:Lcom/policydm/restclient/XRCProcess$HttpMethod;

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    if-ne v0, v1, :cond_4

    .line 628
    new-instance v12, Lorg/apache/http/client/methods/HttpGet;

    .end local v12    # "request":Lorg/apache/http/client/methods/HttpUriRequest;
    move-object/from16 v0, p2

    invoke-direct {v12, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 649
    .restart local v12    # "request":Lorg/apache/http/client/methods/HttpUriRequest;
    :goto_0
    const-string v16, "User-Agent"

    invoke-static {}, Lcom/policydm/adapter/XDMDevinfAdapter;->xdmDevAdpGetHttpUserAgent()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-interface {v12, v0, v1}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 651
    const-string v16, "Connection"

    const-string v17, "Keep-Alive"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-interface {v12, v0, v1}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 652
    const-string v16, "Content-Type"

    const-string v17, "application/xml"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-interface {v12, v0, v1}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 653
    const-string v16, "Accept"

    const-string v17, "application/xml"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-interface {v12, v0, v1}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 654
    const/16 v16, 0x6

    move/from16 v0, p4

    move/from16 v1, v16

    if-eq v0, v1, :cond_0

    .line 656
    const-string v16, "Authorization"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p4

    invoke-virtual {v0, v1, v2}, Lcom/policydm/restclient/XRCProcess;->setAuthHeader(Lcom/policydm/restclient/XRCProcess$HttpMethod;I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-interface {v12, v0, v1}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 660
    :cond_0
    if-eqz p3, :cond_1

    .line 664
    :try_start_0
    sget-object v16, Lcom/policydm/restclient/XRCProcess$HttpMethod;->POST:Lcom/policydm/restclient/XRCProcess$HttpMethod;

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    if-ne v0, v1, :cond_8

    .line 666
    move-object v0, v12

    check-cast v0, Lorg/apache/http/client/methods/HttpPost;

    move-object/from16 v16, v0

    new-instance v17, Lorg/apache/http/entity/StringEntity;

    const-string v18, "UTF-8"

    move-object/from16 v0, v17

    move-object/from16 v1, p3

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {v16 .. v17}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 683
    :cond_1
    :goto_1
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v12}, Lcom/policydm/restclient/XRCProcess;->getRequestPacket(Lcom/policydm/restclient/XRCProcess$HttpMethod;Lorg/apache/http/client/methods/HttpUriRequest;)V

    .line 686
    new-instance v8, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v8}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 688
    .local v8, "httpParams":Lorg/apache/http/params/HttpParams;
    const/16 v16, 0x7530

    move/from16 v0, v16

    invoke-static {v8, v0}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 689
    const/16 v16, 0x7530

    move/from16 v0, v16

    invoke-static {v8, v0}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 692
    const-string v16, "http.proxyHost"

    invoke-static/range {v16 .. v16}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 693
    .local v10, "proxyHostName":Ljava/lang/String;
    const-string v16, "http.proxyPort"

    invoke-static/range {v16 .. v16}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 694
    .local v11, "proxyHostPort":Ljava/lang/String;
    if-eqz v10, :cond_2

    if-eqz v11, :cond_2

    .line 696
    new-instance v9, Lorg/apache/http/HttpHost;

    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v16

    move/from16 v0, v16

    invoke-direct {v9, v10, v0}, Lorg/apache/http/HttpHost;-><init>(Ljava/lang/String;I)V

    .line 697
    .local v9, "proxyHost":Lorg/apache/http/HttpHost;
    invoke-static {v8, v9}, Lorg/apache/http/conn/params/ConnRouteParams;->setDefaultProxy(Lorg/apache/http/params/HttpParams;Lorg/apache/http/HttpHost;)V

    .line 701
    .end local v9    # "proxyHost":Lorg/apache/http/HttpHost;
    :cond_2
    invoke-static/range {p2 .. p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 702
    .local v7, "hostUri":Landroid/net/Uri;
    invoke-virtual {v7}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v4

    .line 703
    .local v4, "hostName":Ljava/lang/String;
    invoke-virtual {v7}, Landroid/net/Uri;->getPort()I

    move-result v5

    .line 704
    .local v5, "hostPort":I
    invoke-virtual {v7}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v6

    .line 706
    .local v6, "hostScheme":Ljava/lang/String;
    const/16 v16, -0x1

    move/from16 v0, v16

    if-ne v5, v0, :cond_3

    .line 708
    const-string v16, "https"

    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v16

    const/16 v17, 0x1

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_a

    .line 710
    const/16 v5, 0x1bb

    .line 717
    :cond_3
    :goto_2
    new-instance v15, Lorg/apache/http/HttpHost;

    invoke-direct {v15, v4, v5, v6}, Lorg/apache/http/HttpHost;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 720
    .local v15, "targetHost":Lorg/apache/http/HttpHost;
    new-instance v16, Lorg/apache/http/impl/client/DefaultHttpClient;

    move-object/from16 v0, v16

    invoke-direct {v0, v8}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/params/HttpParams;)V

    sput-object v16, Lcom/policydm/restclient/XRCProcess;->httpclient:Lorg/apache/http/impl/client/DefaultHttpClient;

    .line 721
    sget-object v16, Lcom/policydm/restclient/XRCProcess;->httpclient:Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-virtual/range {v16 .. v16}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Lorg/apache/http/conn/ClientConnectionManager;->getSchemeRegistry()Lorg/apache/http/conn/scheme/SchemeRegistry;

    move-result-object v16

    new-instance v17, Lorg/apache/http/conn/scheme/Scheme;

    const-string v18, "SSLSocketFactory"

    invoke-static {}, Lorg/apache/http/conn/ssl/SSLSocketFactory;->getSocketFactory()Lorg/apache/http/conn/ssl/SSLSocketFactory;

    move-result-object v19

    const/16 v20, 0x1bb

    invoke-direct/range {v17 .. v20}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual/range {v16 .. v17}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 722
    sget-object v16, Lcom/policydm/restclient/XRCProcess;->httpclient:Lorg/apache/http/impl/client/DefaultHttpClient;

    new-instance v17, Lcom/policydm/restclient/XRCProcess$1;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/policydm/restclient/XRCProcess$1;-><init>(Lcom/policydm/restclient/XRCProcess;)V

    invoke-virtual/range {v16 .. v17}, Lorg/apache/http/impl/client/DefaultHttpClient;->addResponseInterceptor(Lorg/apache/http/HttpResponseInterceptor;)V

    .line 746
    :try_start_1
    sget-object v16, Lcom/policydm/restclient/XRCProcess;->httpclient:Lorg/apache/http/impl/client/DefaultHttpClient;

    move-object/from16 v0, v16

    invoke-virtual {v0, v15, v12}, Lorg/apache/http/impl/client/DefaultHttpClient;->execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;)Lorg/apache/http/HttpResponse;
    :try_end_1
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v13

    .line 786
    const/16 v16, 0x0

    sput-object v16, Lcom/policydm/restclient/XRCProcess;->RestClientThread:Lcom/policydm/restclient/XRCProcess$RestClientThread;

    .line 787
    const/16 v16, 0x0

    sput-object v16, Lcom/policydm/restclient/XRCProcess;->httpclient:Lorg/apache/http/impl/client/DefaultHttpClient;

    :goto_3
    move-object v14, v13

    .line 789
    .end local v4    # "hostName":Ljava/lang/String;
    .end local v5    # "hostPort":I
    .end local v6    # "hostScheme":Ljava/lang/String;
    .end local v7    # "hostUri":Landroid/net/Uri;
    .end local v8    # "httpParams":Lorg/apache/http/params/HttpParams;
    .end local v10    # "proxyHostName":Ljava/lang/String;
    .end local v11    # "proxyHostPort":Ljava/lang/String;
    .end local v13    # "response":Lorg/apache/http/HttpResponse;
    .end local v15    # "targetHost":Lorg/apache/http/HttpHost;
    .local v14, "response":Lorg/apache/http/HttpResponse;
    :goto_4
    return-object v14

    .line 630
    .end local v14    # "response":Lorg/apache/http/HttpResponse;
    .restart local v13    # "response":Lorg/apache/http/HttpResponse;
    :cond_4
    sget-object v16, Lcom/policydm/restclient/XRCProcess$HttpMethod;->DELETE:Lcom/policydm/restclient/XRCProcess$HttpMethod;

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    if-ne v0, v1, :cond_5

    .line 632
    new-instance v12, Lorg/apache/http/client/methods/HttpDelete;

    .end local v12    # "request":Lorg/apache/http/client/methods/HttpUriRequest;
    move-object/from16 v0, p2

    invoke-direct {v12, v0}, Lorg/apache/http/client/methods/HttpDelete;-><init>(Ljava/lang/String;)V

    .restart local v12    # "request":Lorg/apache/http/client/methods/HttpUriRequest;
    goto/16 :goto_0

    .line 634
    :cond_5
    sget-object v16, Lcom/policydm/restclient/XRCProcess$HttpMethod;->PUT:Lcom/policydm/restclient/XRCProcess$HttpMethod;

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    if-ne v0, v1, :cond_6

    .line 636
    new-instance v12, Lorg/apache/http/client/methods/HttpPut;

    .end local v12    # "request":Lorg/apache/http/client/methods/HttpUriRequest;
    move-object/from16 v0, p2

    invoke-direct {v12, v0}, Lorg/apache/http/client/methods/HttpPut;-><init>(Ljava/lang/String;)V

    .restart local v12    # "request":Lorg/apache/http/client/methods/HttpUriRequest;
    goto/16 :goto_0

    .line 638
    :cond_6
    sget-object v16, Lcom/policydm/restclient/XRCProcess$HttpMethod;->POST:Lcom/policydm/restclient/XRCProcess$HttpMethod;

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    if-ne v0, v1, :cond_7

    .line 640
    new-instance v12, Lorg/apache/http/client/methods/HttpPost;

    .end local v12    # "request":Lorg/apache/http/client/methods/HttpUriRequest;
    move-object/from16 v0, p2

    invoke-direct {v12, v0}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .restart local v12    # "request":Lorg/apache/http/client/methods/HttpUriRequest;
    goto/16 :goto_0

    .line 644
    :cond_7
    const-string v16, "Invalid HTTP method"

    invoke-static/range {v16 .. v16}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move-object v14, v13

    .line 645
    .end local v13    # "response":Lorg/apache/http/HttpResponse;
    .restart local v14    # "response":Lorg/apache/http/HttpResponse;
    goto :goto_4

    .line 668
    .end local v14    # "response":Lorg/apache/http/HttpResponse;
    .restart local v13    # "response":Lorg/apache/http/HttpResponse;
    :cond_8
    :try_start_2
    sget-object v16, Lcom/policydm/restclient/XRCProcess$HttpMethod;->PUT:Lcom/policydm/restclient/XRCProcess$HttpMethod;

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    if-ne v0, v1, :cond_9

    .line 670
    move-object v0, v12

    check-cast v0, Lorg/apache/http/client/methods/HttpPut;

    move-object/from16 v16, v0

    new-instance v17, Lorg/apache/http/entity/StringEntity;

    const-string v18, "UTF-8"

    move-object/from16 v0, v17

    move-object/from16 v1, p3

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {v16 .. v17}, Lorg/apache/http/client/methods/HttpPut;->setEntity(Lorg/apache/http/HttpEntity;)V
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_1

    .line 677
    :catch_0
    move-exception v3

    .line 679
    .local v3, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v3}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 680
    invoke-virtual {v3}, Ljava/io/UnsupportedEncodingException;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 674
    .end local v3    # "e":Ljava/io/UnsupportedEncodingException;
    :cond_9
    :try_start_3
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Invalid body by HTTP method: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_1

    .line 714
    .restart local v4    # "hostName":Ljava/lang/String;
    .restart local v5    # "hostPort":I
    .restart local v6    # "hostScheme":Ljava/lang/String;
    .restart local v7    # "hostUri":Landroid/net/Uri;
    .restart local v8    # "httpParams":Lorg/apache/http/params/HttpParams;
    .restart local v10    # "proxyHostName":Ljava/lang/String;
    .restart local v11    # "proxyHostPort":Ljava/lang/String;
    :cond_a
    const/16 v5, 0x50

    goto/16 :goto_2

    .line 748
    .restart local v15    # "targetHost":Lorg/apache/http/HttpHost;
    :catch_1
    move-exception v3

    .line 750
    .local v3, "e":Lorg/apache/http/client/ClientProtocolException;
    const/16 v16, 0x0

    :try_start_4
    sput-boolean v16, Lcom/policydm/restclient/XRCProcess;->mThread:Z

    .line 751
    const/16 v16, 0x0

    invoke-static/range {v16 .. v16}, Lcom/policydm/XDMBroadcastReceiver;->setStartDeviceRegister(Z)V

    .line 752
    const/16 v16, 0x0

    const/16 v17, 0x78

    invoke-static/range {v16 .. v17}, Lcom/policydm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    .line 753
    const/16 v16, 0x3

    sput v16, Lcom/policydm/XDMApplication;->g_nResumeStatus:I

    .line 754
    invoke-virtual {v3}, Lorg/apache/http/client/ClientProtocolException;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 755
    invoke-virtual {v3}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 786
    const/16 v16, 0x0

    sput-object v16, Lcom/policydm/restclient/XRCProcess;->RestClientThread:Lcom/policydm/restclient/XRCProcess$RestClientThread;

    .line 787
    const/16 v16, 0x0

    sput-object v16, Lcom/policydm/restclient/XRCProcess;->httpclient:Lorg/apache/http/impl/client/DefaultHttpClient;

    goto/16 :goto_3

    .line 757
    .end local v3    # "e":Lorg/apache/http/client/ClientProtocolException;
    :catch_2
    move-exception v3

    .line 759
    .local v3, "e":Ljava/io/IOException;
    const/16 v16, 0x0

    :try_start_5
    sput-boolean v16, Lcom/policydm/restclient/XRCProcess;->mThread:Z

    .line 760
    const/16 v16, 0x0

    invoke-static/range {v16 .. v16}, Lcom/policydm/XDMBroadcastReceiver;->setStartDeviceRegister(Z)V

    .line 761
    const/16 v16, 0x0

    const/16 v17, 0x78

    invoke-static/range {v16 .. v17}, Lcom/policydm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    .line 762
    const/16 v16, 0x3

    sput v16, Lcom/policydm/XDMApplication;->g_nResumeStatus:I

    .line 763
    invoke-virtual {v3}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 764
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 786
    const/16 v16, 0x0

    sput-object v16, Lcom/policydm/restclient/XRCProcess;->RestClientThread:Lcom/policydm/restclient/XRCProcess$RestClientThread;

    .line 787
    const/16 v16, 0x0

    sput-object v16, Lcom/policydm/restclient/XRCProcess;->httpclient:Lorg/apache/http/impl/client/DefaultHttpClient;

    goto/16 :goto_3

    .line 766
    .end local v3    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v3

    .line 768
    .local v3, "e":Ljava/lang/NullPointerException;
    const/16 v16, 0x0

    :try_start_6
    sput-boolean v16, Lcom/policydm/restclient/XRCProcess;->mThread:Z

    .line 769
    const/16 v16, 0x0

    invoke-static/range {v16 .. v16}, Lcom/policydm/XDMBroadcastReceiver;->setStartDeviceRegister(Z)V

    .line 770
    const/16 v16, 0x0

    const/16 v17, 0x78

    invoke-static/range {v16 .. v17}, Lcom/policydm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    .line 771
    const/16 v16, 0x3

    sput v16, Lcom/policydm/XDMApplication;->g_nResumeStatus:I

    .line 772
    invoke-virtual {v3}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 773
    invoke-virtual {v3}, Ljava/lang/NullPointerException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 786
    const/16 v16, 0x0

    sput-object v16, Lcom/policydm/restclient/XRCProcess;->RestClientThread:Lcom/policydm/restclient/XRCProcess$RestClientThread;

    .line 787
    const/16 v16, 0x0

    sput-object v16, Lcom/policydm/restclient/XRCProcess;->httpclient:Lorg/apache/http/impl/client/DefaultHttpClient;

    goto/16 :goto_3

    .line 775
    .end local v3    # "e":Ljava/lang/NullPointerException;
    :catch_4
    move-exception v3

    .line 777
    .local v3, "e":Ljava/lang/Exception;
    const/16 v16, 0x0

    :try_start_7
    sput-boolean v16, Lcom/policydm/restclient/XRCProcess;->mThread:Z

    .line 778
    const/16 v16, 0x0

    invoke-static/range {v16 .. v16}, Lcom/policydm/XDMBroadcastReceiver;->setStartDeviceRegister(Z)V

    .line 779
    const/16 v16, 0x0

    const/16 v17, 0x78

    invoke-static/range {v16 .. v17}, Lcom/policydm/eng/core/XDMEvent;->XDMSetEvent(Ljava/lang/Object;I)V

    .line 780
    const/16 v16, 0x3

    sput v16, Lcom/policydm/XDMApplication;->g_nResumeStatus:I

    .line 781
    invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 782
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 786
    const/16 v16, 0x0

    sput-object v16, Lcom/policydm/restclient/XRCProcess;->RestClientThread:Lcom/policydm/restclient/XRCProcess$RestClientThread;

    .line 787
    const/16 v16, 0x0

    sput-object v16, Lcom/policydm/restclient/XRCProcess;->httpclient:Lorg/apache/http/impl/client/DefaultHttpClient;

    goto/16 :goto_3

    .line 786
    .end local v3    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v16

    const/16 v17, 0x0

    sput-object v17, Lcom/policydm/restclient/XRCProcess;->RestClientThread:Lcom/policydm/restclient/XRCProcess$RestClientThread;

    .line 787
    const/16 v17, 0x0

    sput-object v17, Lcom/policydm/restclient/XRCProcess;->httpclient:Lorg/apache/http/impl/client/DefaultHttpClient;

    throw v16
.end method

.method private getPacket(Ljava/lang/String;Lcom/policydm/restclient/XRCProcess$HttpMethod;Ljava/net/URI;[Lorg/apache/http/Header;Lorg/apache/http/HttpEntity;)V
    .locals 12
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "method"    # Lcom/policydm/restclient/XRCProcess$HttpMethod;
    .param p3, "uri"    # Ljava/net/URI;
    .param p4, "header"    # [Lorg/apache/http/Header;
    .param p5, "httpEntity"    # Lorg/apache/http/HttpEntity;

    .prologue
    .line 979
    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    .line 981
    .local v7, "sb":Ljava/lang/StringBuffer;
    const-string v8, "\r\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 982
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "-----------Device RestClient "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "-----------"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 983
    const-string v8, "\r\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 985
    if-eqz p2, :cond_0

    .line 987
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 990
    :cond_0
    if-eqz p3, :cond_1

    .line 992
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lorg/apache/http/HttpVersion;->HTTP_1_1:Lorg/apache/http/HttpVersion;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 993
    const-string v8, "\r\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 996
    :cond_1
    move-object/from16 v0, p4

    .local v0, "arr$":[Lorg/apache/http/Header;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v5, :cond_3

    aget-object v2, v0, v3

    .line 998
    .local v2, "element":Lorg/apache/http/Header;
    if-eqz v2, :cond_2

    .line 1000
    invoke-virtual {v7, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 1001
    const-string v8, "\r\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 996
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1005
    .end local v2    # "element":Lorg/apache/http/Header;
    :cond_3
    new-instance v6, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v6}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 1006
    .local v6, "os":Ljava/io/ByteArrayOutputStream;
    const/4 v4, 0x0

    .line 1009
    .local v4, "is":Ljava/io/InputStream;
    if-eqz p5, :cond_5

    .line 1011
    :try_start_0
    invoke-interface/range {p5 .. p5}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v4

    .line 1013
    invoke-virtual {p0, v4}, Lcom/policydm/restclient/XRCProcess;->inputStream2ByteArrOutStream(Ljava/io/InputStream;)Ljava/io/ByteArrayOutputStream;

    move-result-object v6

    if-eqz v6, :cond_5

    .line 1015
    invoke-static {}, Lcom/policydm/agent/XDMDebug;->xdmGetWbxmlFileStatus()Z

    move-result v8

    if-eqz v8, :cond_5

    .line 1017
    iget-object v8, p0, Lcom/policydm/restclient/XRCProcess;->m_bufDebug:Ljava/io/ByteArrayOutputStream;

    if-nez v8, :cond_4

    .line 1018
    new-instance v8, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v8}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v8, p0, Lcom/policydm/restclient/XRCProcess;->m_bufDebug:Ljava/io/ByteArrayOutputStream;

    .line 1020
    :cond_4
    iget-object v8, p0, Lcom/policydm/restclient/XRCProcess;->m_bufDebug:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v8}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 1021
    iget-object v8, p0, Lcom/policydm/restclient/XRCProcess;->m_bufDebug:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v11

    invoke-virtual {v8, v9, v10, v11}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 1022
    if-eqz p1, :cond_5

    const-string v8, "response"

    invoke-virtual {v8, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_5

    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v8

    if-eqz v8, :cond_5

    .line 1024
    iget v8, p0, Lcom/policydm/restclient/XRCProcess;->nRecvCount:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/policydm/restclient/XRCProcess;->nRecvCount:I

    .line 1025
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "data/data/com.policydm/recv"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/policydm/restclient/XRCProcess;->nRecvCount:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".xml"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/policydm/restclient/XRCProcess;->m_bufDebug:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v9}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v9

    invoke-static {v8, v9}, Lcom/policydm/agent/XDMDebug;->xdmWriteFile(Ljava/lang/String;[B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1037
    :cond_5
    if-eqz v6, :cond_6

    .line 1041
    :try_start_1
    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1049
    :cond_6
    :goto_1
    if-eqz v4, :cond_7

    .line 1053
    :try_start_2
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 1061
    :cond_7
    :goto_2
    if-eqz p5, :cond_8

    .line 1065
    :try_start_3
    invoke-interface/range {p5 .. p5}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_8

    .line 1074
    :cond_8
    :goto_3
    const-string v8, "---------------- End ----------------"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1075
    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 1077
    return-void

    .line 1043
    :catch_0
    move-exception v1

    .line 1045
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 1055
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 1057
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 1031
    .end local v1    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v1

    .line 1033
    .local v1, "e":Ljava/lang/Exception;
    :try_start_4
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1037
    if-eqz v6, :cond_9

    .line 1041
    :try_start_5
    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    .line 1049
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_9
    :goto_4
    if-eqz v4, :cond_a

    .line 1053
    :try_start_6
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    .line 1061
    :cond_a
    :goto_5
    if-eqz p5, :cond_8

    .line 1065
    :try_start_7
    invoke-interface/range {p5 .. p5}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    goto :goto_3

    .line 1067
    :catch_3
    move-exception v8

    goto :goto_3

    .line 1043
    .restart local v1    # "e":Ljava/lang/Exception;
    :catch_4
    move-exception v1

    .line 1045
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 1055
    .end local v1    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v1

    .line 1057
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 1037
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    if-eqz v6, :cond_b

    .line 1041
    :try_start_8
    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6

    .line 1049
    :cond_b
    :goto_6
    if-eqz v4, :cond_c

    .line 1053
    :try_start_9
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_7

    .line 1061
    :cond_c
    :goto_7
    if-eqz p5, :cond_d

    .line 1065
    :try_start_a
    invoke-interface/range {p5 .. p5}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_9

    .line 1070
    :cond_d
    :goto_8
    throw v8

    .line 1043
    :catch_6
    move-exception v1

    .line 1045
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 1055
    .end local v1    # "e":Ljava/io/IOException;
    :catch_7
    move-exception v1

    .line 1057
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 1067
    .end local v1    # "e":Ljava/io/IOException;
    :catch_8
    move-exception v8

    goto :goto_3

    :catch_9
    move-exception v9

    goto :goto_8
.end method

.method private makeSignature(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .locals 16
    .param p1, "szNonce"    # Ljava/lang/String;
    .param p2, "szQuery"    # Ljava/lang/String;
    .param p3, "szBody"    # Ljava/lang/String;
    .param p4, "nState"    # I

    .prologue
    .line 551
    const/4 v13, 0x0

    .line 552
    .local v13, "szSignature":Ljava/lang/String;
    const/4 v10, 0x0

    .line 553
    .local v10, "szCredData1":Ljava/lang/String;
    const/4 v11, 0x0

    .line 556
    .local v11, "szCredData2":Ljava/lang/String;
    new-instance v2, Lcom/policydm/adapter/XSPDSingature;

    invoke-direct {v2}, Lcom/policydm/adapter/XSPDSingature;-><init>()V

    .line 558
    .local v2, "data":Lcom/policydm/adapter/XSPDSingature;
    const-string v8, "1m553ne3y9"

    .line 559
    .local v8, "serverid":Ljava/lang/String;
    invoke-static {}, Lcom/policydm/db/XDBProfileAdp;->xdbGetServerPassword()Ljava/lang/String;

    move-result-object v9

    .line 560
    .local v9, "serverpw":Ljava/lang/String;
    invoke-static {}, Lcom/policydm/adapter/XDMDevinfAdapter;->xdmDevAdpGetFullDeviceID()Ljava/lang/String;

    move-result-object v3

    .line 561
    .local v3, "deviceid":Ljava/lang/String;
    invoke-static {}, Lcom/policydm/db/XDBProfileAdp;->xdbGetClientPassword()Ljava/lang/String;

    move-result-object v4

    .line 562
    .local v4, "devicepw":Ljava/lang/String;
    const-string v12, "/service/policy/device"

    .line 563
    .local v12, "szPath":Ljava/lang/String;
    const/4 v14, 0x1

    move/from16 v0, p4

    if-ne v0, v14, :cond_1

    .line 565
    const-string v12, "/service/policy/device"

    .line 584
    :cond_0
    :goto_0
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "serverid="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 585
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "serverpw="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 586
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "deviceid="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 587
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "devicepw="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 588
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Nonce="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 589
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "path="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 590
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "query="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 591
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "szBody="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p3

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 593
    invoke-virtual {v2, v8, v9, v3, v4}, Lcom/policydm/adapter/XSPDSingature;->SignatureCred1(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v6

    .line 594
    .local v6, "md5digest1":[B
    if-nez v6, :cond_5

    .line 596
    const/4 v14, 0x0

    .line 616
    :goto_1
    return-object v14

    .line 567
    .end local v6    # "md5digest1":[B
    :cond_1
    const/4 v14, 0x2

    move/from16 v0, p4

    if-ne v0, v14, :cond_2

    .line 569
    const-string v12, "/service/policy/device"

    goto/16 :goto_0

    .line 571
    :cond_2
    const/4 v14, 0x3

    move/from16 v0, p4

    if-ne v0, v14, :cond_3

    .line 573
    const-string v12, "/service/policy/device/push"

    goto/16 :goto_0

    .line 575
    :cond_3
    const/4 v14, 0x4

    move/from16 v0, p4

    if-ne v0, v14, :cond_4

    .line 577
    const-string v12, "/service/policy/result"

    goto/16 :goto_0

    .line 579
    :cond_4
    const/4 v14, 0x5

    move/from16 v0, p4

    if-ne v0, v14, :cond_0

    .line 581
    const-string v12, "/service/policy/config"

    goto/16 :goto_0

    .line 598
    .restart local v6    # "md5digest1":[B
    :cond_5
    new-instance v10, Ljava/lang/String;

    .end local v10    # "szCredData1":Ljava/lang/String;
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v14

    invoke-direct {v10, v6, v14}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 599
    .restart local v10    # "szCredData1":Ljava/lang/String;
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "szCredData1="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 601
    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v2, v12, v0, v1}, Lcom/policydm/adapter/XSPDSingature;->SignatureCred2(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v7

    .line 602
    .local v7, "md5digest2":[B
    if-nez v7, :cond_6

    .line 604
    const/4 v14, 0x0

    goto :goto_1

    .line 606
    :cond_6
    new-instance v11, Ljava/lang/String;

    .end local v11    # "szCredData2":Ljava/lang/String;
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v14

    invoke-direct {v11, v7, v14}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 607
    .restart local v11    # "szCredData2":Ljava/lang/String;
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "szCredData2="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 609
    move-object/from16 v0, p1

    invoke-virtual {v2, v10, v0, v11}, Lcom/policydm/adapter/XSPDSingature;->Signature(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v5

    .line 610
    .local v5, "md5digest":[B
    if-nez v5, :cond_7

    .line 612
    const/4 v14, 0x0

    goto :goto_1

    .line 614
    :cond_7
    new-instance v13, Ljava/lang/String;

    .end local v13    # "szSignature":Ljava/lang/String;
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v14

    invoke-direct {v13, v5, v14}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 615
    .restart local v13    # "szSignature":Ljava/lang/String;
    invoke-static {v13}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    move-object v14, v13

    .line 616
    goto/16 :goto_1
.end method

.method public static sendMessage(I)V
    .locals 2
    .param p0, "what"    # I

    .prologue
    .line 917
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 918
    .local v0, "msg":Landroid/os/Message;
    iput p0, v0, Landroid/os/Message;->what:I

    .line 919
    sget-object v1, Lcom/policydm/restclient/XRCProcess;->hRestClient:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 921
    sget-object v1, Lcom/policydm/restclient/XRCProcess;->hRestClient:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 927
    :goto_0
    return-void

    .line 925
    :cond_0
    const-string v1, "hRestClient is null!!"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public getInputStream()Ljava/io/InputStream;
    .locals 3

    .prologue
    .line 1107
    iget-object v2, p0, Lcom/policydm/restclient/XRCProcess;->out:Ljava/lang/StringBuffer;

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1108
    .local v1, "outis":Ljava/lang/String;
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 1110
    .local v0, "inputStream":Ljava/io/InputStream;
    return-object v0
.end method

.method public getRequestPacket(Lcom/policydm/restclient/XRCProcess$HttpMethod;Lorg/apache/http/client/methods/HttpUriRequest;)V
    .locals 6
    .param p1, "method"    # Lcom/policydm/restclient/XRCProcess$HttpMethod;
    .param p2, "request"    # Lorg/apache/http/client/methods/HttpUriRequest;

    .prologue
    .line 967
    const-string v1, "request"

    invoke-interface {p2}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v3

    invoke-interface {p2}, Lorg/apache/http/client/methods/HttpUriRequest;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/policydm/restclient/XRCProcess;->getPacket(Ljava/lang/String;Lcom/policydm/restclient/XRCProcess$HttpMethod;Ljava/net/URI;[Lorg/apache/http/Header;Lorg/apache/http/HttpEntity;)V

    .line 968
    return-void
.end method

.method public getResponsePacket(Lorg/apache/http/HttpResponse;)V
    .locals 6
    .param p1, "response"    # Lorg/apache/http/HttpResponse;

    .prologue
    const/4 v2, 0x0

    .line 973
    const-string v1, "response"

    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v4

    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v5

    move-object v0, p0

    move-object v3, v2

    invoke-direct/range {v0 .. v5}, Lcom/policydm/restclient/XRCProcess;->getPacket(Ljava/lang/String;Lcom/policydm/restclient/XRCProcess$HttpMethod;Ljava/net/URI;[Lorg/apache/http/Header;Lorg/apache/http/HttpEntity;)V

    .line 974
    return-void
.end method

.method public inputStream2ByteArrOutStream(Ljava/io/InputStream;)Ljava/io/ByteArrayOutputStream;
    .locals 8
    .param p1, "is"    # Ljava/io/InputStream;

    .prologue
    const/4 v7, 0x0

    .line 1082
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 1083
    .local v0, "baos":Ljava/io/ByteArrayOutputStream;
    const/16 v4, 0x400

    new-array v1, v4, [B

    .line 1084
    .local v1, "byteBuffer":[B
    iput-object v7, p0, Lcom/policydm/restclient/XRCProcess;->out:Ljava/lang/StringBuffer;

    .line 1085
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v4, p0, Lcom/policydm/restclient/XRCProcess;->out:Ljava/lang/StringBuffer;

    .line 1086
    const/4 v3, 0x0

    .line 1090
    .local v3, "nLength":I
    :goto_0
    :try_start_0
    invoke-virtual {p1, v1}, Ljava/io/InputStream;->read([B)I

    move-result v3

    if-lez v3, :cond_0

    .line 1092
    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 1093
    iget-object v4, p0, Lcom/policydm/restclient/XRCProcess;->out:Ljava/lang/StringBuffer;

    new-instance v5, Ljava/lang/String;

    const/4 v6, 0x0

    invoke-direct {v5, v1, v6, v3}, Ljava/lang/String;-><init>([BII)V

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1096
    :catch_0
    move-exception v2

    .line 1098
    .local v2, "e":Ljava/io/IOException;
    iput-object v7, p0, Lcom/policydm/restclient/XRCProcess;->out:Ljava/lang/StringBuffer;

    .line 1099
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 1102
    .end local v2    # "e":Ljava/io/IOException;
    :cond_0
    return-object v0
.end method

.method public run()V
    .locals 1

    .prologue
    .line 794
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 796
    new-instance v0, Lcom/policydm/restclient/XRCProcess$2;

    invoke-direct {v0, p0}, Lcom/policydm/restclient/XRCProcess$2;-><init>(Lcom/policydm/restclient/XRCProcess;)V

    sput-object v0, Lcom/policydm/restclient/XRCProcess;->hRestClient:Landroid/os/Handler;

    .line 813
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 814
    return-void
.end method

.method public setAuthHeader(Lcom/policydm/restclient/XRCProcess$HttpMethod;I)Ljava/lang/String;
    .locals 8
    .param p1, "method"    # Lcom/policydm/restclient/XRCProcess$HttpMethod;
    .param p2, "nState"    # I

    .prologue
    .line 527
    const-string v0, "SPD-AUTH"

    .line 528
    .local v0, "auth_id":Ljava/lang/String;
    const-string v4, "1m553ne3y9"

    .line 529
    .local v4, "server_id":Ljava/lang/String;
    invoke-static {}, Lcom/policydm/db/XDBSpdAdp;->xdbGetSpdServerNonce()Ljava/lang/String;

    move-result-object v1

    .line 530
    .local v1, "nonce":Ljava/lang/String;
    const/4 v2, 0x0

    .line 532
    .local v2, "query":Ljava/lang/String;
    sget-object v6, Lcom/policydm/restclient/XRCProcess$HttpMethod;->POST:Lcom/policydm/restclient/XRCProcess$HttpMethod;

    if-ne p1, v6, :cond_1

    const/4 v6, 0x1

    if-ne p2, v6, :cond_1

    .line 534
    const/4 v1, 0x0

    .line 540
    :cond_0
    :goto_0
    iget-object v6, p0, Lcom/policydm/restclient/XRCProcess;->reqXML:Ljava/lang/String;

    invoke-direct {p0, v1, v2, v6, p2}, Lcom/policydm/restclient/XRCProcess;->makeSignature(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    .line 542
    .local v5, "signature":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 543
    .local v3, "sbAuth":Ljava/lang/StringBuilder;
    const-string v6, "auth_identifier="

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "server_id="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "device_id="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Lcom/policydm/adapter/XDMDevinfAdapter;->xdmDevAdpGetFullDeviceID()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "signature="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 546
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6

    .line 536
    .end local v3    # "sbAuth":Ljava/lang/StringBuilder;
    .end local v5    # "signature":Ljava/lang/String;
    :cond_1
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 538
    const/4 v1, 0x0

    goto/16 :goto_0
.end method

.method public xspdHandler(Landroid/os/Message;)Z
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x2

    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 818
    const/4 v1, 0x0

    .line 820
    .local v1, "msgItem":Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-nez v2, :cond_0

    move v2, v3

    .line 912
    :goto_0
    return v2

    .line 823
    :cond_0
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .end local v1    # "msgItem":Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;
    check-cast v1, Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;

    .line 825
    .restart local v1    # "msgItem":Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;
    iget v2, v1, Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;->type:I

    packed-switch v2, :pswitch_data_0

    :goto_1
    move v2, v4

    .line 912
    goto :goto_0

    .line 829
    :pswitch_0
    const/4 v0, 0x0

    .line 830
    .local v0, "arg":I
    iget-object v2, v1, Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;->param:Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;

    if-eqz v2, :cond_1

    .line 832
    iget-object v2, v1, Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;->param:Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;

    iget-object v2, v2, Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;->param:Ljava/lang/Object;

    if-eqz v2, :cond_1

    .line 833
    iget-object v2, v1, Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;->param:Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;

    iget-object v2, v2, Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;->param:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 836
    :cond_1
    invoke-static {}, Lcom/policydm/XDMBroadcastReceiver;->getStartDeviceRegister()Z

    move-result v2

    if-eqz v2, :cond_2

    if-eq v0, v3, :cond_2

    if-eq v0, v5, :cond_2

    .line 838
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmUseSPP()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 840
    const-string v2, "Waiting for SPP Push update..."

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 848
    :cond_2
    invoke-static {}, Lcom/policydm/db/XDBSpdAdp;->xdbGetSpdDeviceCreate()I

    move-result v2

    if-ne v2, v3, :cond_8

    invoke-static {}, Lcom/policydm/db/XDBSpdAdp;->xdbGetSpdDeviceRegister()I

    move-result v2

    if-eq v2, v3, :cond_8

    .line 850
    const-string v2, "Device Registering, skip Device Create"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 851
    invoke-static {}, Lcom/policydm/db/XDBSpdAdp;->xdbGetSpdPushRegId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-static {}, Lcom/policydm/db/XDBSpdAdp;->xdbGetSpdDeviceUpdate()I

    move-result v2

    if-eq v2, v3, :cond_7

    .line 853
    :cond_3
    if-ne v0, v3, :cond_5

    .line 855
    invoke-static {v6}, Lcom/policydm/ui/XUIAdapter;->xuiAdpUserInitiate(I)V

    goto :goto_1

    .line 844
    :cond_4
    const-string v2, "Already Device Registering... return"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 857
    :cond_5
    if-ne v0, v5, :cond_6

    .line 859
    invoke-static {v3}, Lcom/policydm/ui/XUIAdapter;->xuiAdpUserInitiate(I)V

    goto :goto_1

    .line 863
    :cond_6
    invoke-static {}, Lcom/policydm/XDMBroadcastReceiver;->callPushRegisterStart()V

    goto :goto_1

    .line 868
    :cond_7
    const/16 v2, 0x3e

    invoke-static {v2, v7, v7}, Lcom/policydm/eng/core/XDMMsg;->xdmSendRCMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    .line 872
    :cond_8
    const-string v2, "XEVENT_RC_DEVICE_CREATE"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 873
    invoke-static {v3}, Lcom/policydm/XDMBroadcastReceiver;->setStartDeviceRegister(Z)V

    .line 874
    invoke-direct {p0, v3, v0}, Lcom/policydm/restclient/XRCProcess;->callRestClientThread(II)V

    goto :goto_1

    .line 878
    .end local v0    # "arg":I
    :pswitch_1
    const-string v2, "XEVENT_RC_DEVICE_UPDATE"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 879
    invoke-direct {p0, v6, v4}, Lcom/policydm/restclient/XRCProcess;->callRestClientThread(II)V

    goto :goto_1

    .line 883
    :pswitch_2
    const-string v2, "XEVENT_RC_PUSH_UPDATE"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 884
    invoke-static {}, Lcom/policydm/XDMBroadcastReceiver;->getStartDeviceRegister()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 886
    const-string v2, "Already Device Registering... return"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 889
    :cond_9
    invoke-static {v3}, Lcom/policydm/XDMBroadcastReceiver;->setStartDeviceRegister(Z)V

    .line 890
    invoke-direct {p0, v5, v4}, Lcom/policydm/restclient/XRCProcess;->callRestClientThread(II)V

    goto/16 :goto_1

    .line 894
    :pswitch_3
    const-string v2, "XEVENT_RC_DEVICE_REPORT"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 895
    const/4 v2, 0x4

    invoke-direct {p0, v2, v4}, Lcom/policydm/restclient/XRCProcess;->callRestClientThread(II)V

    goto/16 :goto_1

    .line 899
    :pswitch_4
    const-string v2, "XEVENT_RC_GET_POLLINGINFO"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 900
    const/4 v2, 0x5

    invoke-direct {p0, v2, v4}, Lcom/policydm/restclient/XRCProcess;->callRestClientThread(II)V

    goto/16 :goto_1

    .line 904
    :pswitch_5
    const-string v2, "XEVENT_RC_GET_VERSION"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 905
    const/4 v2, 0x6

    invoke-direct {p0, v2, v4}, Lcom/policydm/restclient/XRCProcess;->callRestClientThread(II)V

    goto/16 :goto_1

    .line 825
    :pswitch_data_0
    .packed-switch 0x3c
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
