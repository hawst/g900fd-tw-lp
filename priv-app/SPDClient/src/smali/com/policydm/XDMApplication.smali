.class public Lcom/policydm/XDMApplication;
.super Landroid/app/Application;
.source "XDMApplication.java"

# interfaces
.implements Lcom/policydm/interfaces/XCommonInterface;
.implements Lcom/policydm/interfaces/XDLInterface;
.implements Lcom/policydm/interfaces/XDMInterface;
.implements Lcom/policydm/interfaces/XEventInterface;
.implements Lcom/policydm/interfaces/XUIInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/policydm/XDMApplication$XDMPhoneStateListener;
    }
.end annotation


# static fields
.field private static final SPD_NOTIBAR_KEY:Ljava/lang/String; = "Notibar"

.field private static final SPD_SHARED_PREF_KEY:Ljava/lang/String; = "SPDSharedPref"

.field public static g_PushTask:Lcom/policydm/push/XPushProcess;

.field public static g_RestClientTask:Lcom/policydm/restclient/XRCProcess;

.field public static g_Task:Lcom/policydm/agent/XDMTask;

.field public static g_UITask:Lcom/policydm/agent/XDMUITask;

.field public static g_bIsInitializing:Z

.field public static g_nResumeStatus:I

.field public static mDataState:I

.field private static m_Context:Landroid/content/Context;

.field private static m_ToastAlreadyAdded:Landroid/widget/Toast;

.field private static m_WakeLock:Landroid/os/PowerManager$WakeLock;

.field private static m_bIsRoaming:Z

.field private static m_hDmHandler:Landroid/os/Handler;

.field private static m_szCurrentReleaseVer:Ljava/lang/String;

.field public static nCurrentNetwork:I

.field public static nPrevNetwork:I

.field public static nServiceType:I

.field public static nSimStatus:I

.field public static phoneListener:Lcom/policydm/XDMApplication$XDMPhoneStateListener;

.field public static serviceState:Landroid/telephony/ServiceState;

.field public static telephonyManager:Landroid/telephony/TelephonyManager;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 59
    sput-object v1, Lcom/policydm/XDMApplication;->g_Task:Lcom/policydm/agent/XDMTask;

    .line 60
    sput-object v1, Lcom/policydm/XDMApplication;->g_UITask:Lcom/policydm/agent/XDMUITask;

    .line 61
    sput-object v1, Lcom/policydm/XDMApplication;->g_PushTask:Lcom/policydm/push/XPushProcess;

    .line 62
    sput-object v1, Lcom/policydm/XDMApplication;->g_RestClientTask:Lcom/policydm/restclient/XRCProcess;

    .line 66
    sput-object v1, Lcom/policydm/XDMApplication;->serviceState:Landroid/telephony/ServiceState;

    .line 68
    const/4 v0, -0x1

    sput v0, Lcom/policydm/XDMApplication;->nSimStatus:I

    .line 69
    sput v2, Lcom/policydm/XDMApplication;->mDataState:I

    .line 70
    sput v2, Lcom/policydm/XDMApplication;->nCurrentNetwork:I

    .line 71
    sput v2, Lcom/policydm/XDMApplication;->nPrevNetwork:I

    .line 73
    sput v2, Lcom/policydm/XDMApplication;->g_nResumeStatus:I

    .line 74
    sput-boolean v2, Lcom/policydm/XDMApplication;->g_bIsInitializing:Z

    .line 81
    sput-object v1, Lcom/policydm/XDMApplication;->m_hDmHandler:Landroid/os/Handler;

    .line 83
    sput-boolean v2, Lcom/policydm/XDMApplication;->m_bIsRoaming:Z

    .line 85
    sput-object v1, Lcom/policydm/XDMApplication;->m_ToastAlreadyAdded:Landroid/widget/Toast;

    .line 86
    const-string v0, ""

    sput-object v0, Lcom/policydm/XDMApplication;->m_szCurrentReleaseVer:Ljava/lang/String;

    .line 88
    sput-object v1, Lcom/policydm/XDMApplication;->m_WakeLock:Landroid/os/PowerManager$WakeLock;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    .line 510
    return-void
.end method

.method static synthetic access$000(Lcom/policydm/XDMApplication;I)V
    .locals 0
    .param p0, "x0"    # Lcom/policydm/XDMApplication;
    .param p1, "x1"    # I

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/policydm/XDMApplication;->xdmCallUiDialogActivity(I)V

    return-void
.end method

.method static synthetic access$100()Landroid/content/Context;
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lcom/policydm/XDMApplication;->m_Context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/policydm/XDMApplication;I)V
    .locals 0
    .param p0, "x0"    # Lcom/policydm/XDMApplication;
    .param p1, "x1"    # I

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/policydm/XDMApplication;->xdmCallUiEulaActivity(I)V

    return-void
.end method

.method static synthetic access$300()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lcom/policydm/XDMApplication;->m_hDmHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$400()Z
    .locals 1

    .prologue
    .line 57
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmResumeOperation()Z

    move-result v0

    return v0
.end method

.method static synthetic access$500()Z
    .locals 1

    .prologue
    .line 57
    sget-boolean v0, Lcom/policydm/XDMApplication;->m_bIsRoaming:Z

    return v0
.end method

.method static synthetic access$502(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 57
    sput-boolean p0, Lcom/policydm/XDMApplication;->m_bIsRoaming:Z

    return p0
.end method

.method public static isNetworkChanged()Z
    .locals 2

    .prologue
    .line 622
    sget v0, Lcom/policydm/XDMApplication;->nPrevNetwork:I

    sget v1, Lcom/policydm/XDMApplication;->nCurrentNetwork:I

    if-eq v0, v1, :cond_0

    .line 624
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "a previous network is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/policydm/XDMApplication;->nPrevNetwork:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", current network is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/policydm/XDMApplication;->nCurrentNetwork:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 626
    const-string v0, "network changed.... "

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 627
    sget v0, Lcom/policydm/XDMApplication;->nCurrentNetwork:I

    sput v0, Lcom/policydm/XDMApplication;->nPrevNetwork:I

    .line 628
    const/4 v0, 0x1

    .line 635
    :goto_0
    return v0

    .line 632
    :cond_0
    const-string v0, "network not changed.... "

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 635
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private xdmCallUiDialogActivity(I)V
    .locals 5
    .param p1, "id"    # I

    .prologue
    .line 640
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "id:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 641
    new-instance v0, Landroid/content/Intent;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    sget-object v3, Lcom/policydm/XDMApplication;->m_Context:Landroid/content/Context;

    const-class v4, Lcom/policydm/ui/XUIDialogActivity;

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 642
    .local v0, "DialIntent":Landroid/content/Intent;
    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 643
    invoke-virtual {p0, v0}, Lcom/policydm/XDMApplication;->startActivity(Landroid/content/Intent;)V

    .line 644
    return-void
.end method

.method private xdmCallUiEulaActivity(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 648
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/policydm/ui/XUIEulaActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 649
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "EULA_TYPE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 650
    const/high16 v1, 0x34000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 651
    invoke-virtual {p0, v0}, Lcom/policydm/XDMApplication;->startActivity(Landroid/content/Intent;)V

    .line 652
    return-void
.end method

.method public static xdmCheckIdleScreen()Z
    .locals 6

    .prologue
    .line 473
    const/4 v0, 0x0

    .line 477
    .local v0, "bRet":Z
    :try_start_0
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetTopPackageName()Ljava/lang/String;

    move-result-object v3

    .line 478
    .local v3, "szTopPackage":Ljava/lang/String;
    const-string v4, "android"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "settings"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-eqz v4, :cond_0

    .line 480
    const/4 v0, 0x1

    .line 504
    .end local v3    # "szTopPackage":Ljava/lang/String;
    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Idle Screen : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 506
    return v0

    .line 482
    .restart local v3    # "szTopPackage":Ljava/lang/String;
    :cond_0
    :try_start_1
    sget-object v4, Lcom/policydm/XDMApplication;->m_Context:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 484
    const/4 v0, 0x1

    goto :goto_0

    .line 488
    :cond_1
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetHomeScreenList()Ljava/util/ArrayList;

    move-result-object v2

    .line 489
    .local v2, "homeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v4

    if-eqz v4, :cond_2

    .line 491
    const/4 v0, 0x1

    goto :goto_0

    .line 495
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 499
    .end local v2    # "homeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v3    # "szTopPackage":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 501
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdmDbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 251
    if-eqz p0, :cond_0

    .line 252
    :try_start_0
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 258
    :cond_0
    :goto_0
    return-void

    .line 254
    :catch_0
    move-exception v0

    .line 256
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdmDbGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .locals 5

    .prologue
    .line 215
    const/4 v2, 0x0

    .line 216
    .local v2, "mDatabaseHelper":Lcom/policydm/db/XDBSqlHelper;
    const/4 v0, 0x0

    .line 219
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    new-instance v3, Lcom/policydm/db/XDBSqlHelper;

    sget-object v4, Lcom/policydm/XDMApplication;->m_Context:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/policydm/db/XDBSqlHelper;-><init>(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 220
    .end local v2    # "mDatabaseHelper":Lcom/policydm/db/XDBSqlHelper;
    .local v3, "mDatabaseHelper":Lcom/policydm/db/XDBSqlHelper;
    :try_start_1
    invoke-virtual {v3}, Lcom/policydm/db/XDBSqlHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    move-object v2, v3

    .line 227
    .end local v3    # "mDatabaseHelper":Lcom/policydm/db/XDBSqlHelper;
    .restart local v2    # "mDatabaseHelper":Lcom/policydm/db/XDBSqlHelper;
    :goto_0
    return-object v0

    .line 222
    :catch_0
    move-exception v1

    .line 224
    .local v1, "e":Ljava/lang/Exception;
    :goto_1
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 222
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "mDatabaseHelper":Lcom/policydm/db/XDBSqlHelper;
    .restart local v3    # "mDatabaseHelper":Lcom/policydm/db/XDBSqlHelper;
    :catch_1
    move-exception v1

    move-object v2, v3

    .end local v3    # "mDatabaseHelper":Lcom/policydm/db/XDBSqlHelper;
    .restart local v2    # "mDatabaseHelper":Lcom/policydm/db/XDBSqlHelper;
    goto :goto_1
.end method

.method public static xdmDbGetWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .locals 5

    .prologue
    .line 232
    const/4 v2, 0x0

    .line 233
    .local v2, "mDatabaseHelper":Lcom/policydm/db/XDBSqlHelper;
    const/4 v0, 0x0

    .line 236
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    new-instance v3, Lcom/policydm/db/XDBSqlHelper;

    sget-object v4, Lcom/policydm/XDMApplication;->m_Context:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/policydm/db/XDBSqlHelper;-><init>(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 237
    .end local v2    # "mDatabaseHelper":Lcom/policydm/db/XDBSqlHelper;
    .local v3, "mDatabaseHelper":Lcom/policydm/db/XDBSqlHelper;
    :try_start_1
    invoke-virtual {v3}, Lcom/policydm/db/XDBSqlHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    move-object v2, v3

    .line 244
    .end local v3    # "mDatabaseHelper":Lcom/policydm/db/XDBSqlHelper;
    .restart local v2    # "mDatabaseHelper":Lcom/policydm/db/XDBSqlHelper;
    :goto_0
    return-object v0

    .line 239
    :catch_0
    move-exception v1

    .line 241
    .local v1, "e":Ljava/lang/Exception;
    :goto_1
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 239
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "mDatabaseHelper":Lcom/policydm/db/XDBSqlHelper;
    .restart local v3    # "mDatabaseHelper":Lcom/policydm/db/XDBSqlHelper;
    :catch_1
    move-exception v1

    move-object v2, v3

    .end local v3    # "mDatabaseHelper":Lcom/policydm/db/XDBSqlHelper;
    .restart local v2    # "mDatabaseHelper":Lcom/policydm/db/XDBSqlHelper;
    goto :goto_1
.end method

.method public static xdmDisplayAlarmTime(J)Ljava/lang/String;
    .locals 4
    .param p0, "time"    # J

    .prologue
    .line 971
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy/MM/dd/HH:mm:ss"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 972
    .local v0, "df":Ljava/text/DateFormat;
    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 974
    .local v1, "sztime":Ljava/lang/String;
    return-object v1
.end method

.method public static xdmEulaReStartAlarm()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 942
    const-string v6, ""

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 944
    const-string v6, "alarm"

    invoke-static {v6}, Lcom/policydm/XDMApplication;->xdmGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/AlarmManager;

    .line 945
    .local v2, "am":Landroid/app/AlarmManager;
    if-nez v2, :cond_1

    .line 947
    const-string v6, "AlarmManager is null!!"

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 967
    :cond_0
    :goto_0
    return-void

    .line 951
    :cond_1
    invoke-static {}, Lcom/policydm/db/XDBSpdAdp;->xdbGetSpdEulaTime()J

    move-result-wide v0

    .line 952
    .local v0, "alarmtime":J
    const-wide/16 v6, 0x0

    cmp-long v6, v0, v6

    if-eqz v6, :cond_0

    .line 954
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 955
    .local v3, "currentTime":J
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "alarmtime:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v0, v1}, Lcom/policydm/XDMApplication;->xdmDisplayAlarmTime(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 956
    cmp-long v6, v3, v0

    if-lez v6, :cond_2

    .line 958
    const-wide/16 v6, 0x7530

    add-long v0, v3, v6

    .line 959
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "currentTime:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v3, v4}, Lcom/policydm/XDMApplication;->xdmDisplayAlarmTime(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 961
    :cond_2
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v6

    new-instance v7, Landroid/content/Intent;

    const-string v8, "com.policydm.intent.action.EULA_TIME"

    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v6, v9, v7, v9}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    .line 962
    .local v5, "sender":Landroid/app/PendingIntent;
    invoke-static {v0, v1}, Lcom/policydm/db/XDBSpdAdp;->xdbSetSpdEulaTime(J)V

    .line 963
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Eula Time:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v0, v1}, Lcom/policydm/XDMApplication;->xdmDisplayAlarmTime(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 965
    invoke-virtual {v2, v9, v0, v1, v5}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    goto :goto_0
.end method

.method public static xdmGetContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 210
    sget-object v0, Lcom/policydm/XDMApplication;->m_Context:Landroid/content/Context;

    return-object v0
.end method

.method public static xdmGetDataState()I
    .locals 1

    .prologue
    .line 306
    sget v0, Lcom/policydm/XDMApplication;->mDataState:I

    return v0
.end method

.method public static xdmGetHomeScreenList()Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 447
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 451
    .local v3, "homeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :try_start_0
    sget-object v7, Lcom/policydm/XDMApplication;->m_Context:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    .line 452
    .local v6, "pm":Landroid/content/pm/PackageManager;
    new-instance v2, Landroid/content/Intent;

    const-string v7, "android.intent.action.MAIN"

    invoke-direct {v2, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 453
    .local v2, "homeIntent":Landroid/content/Intent;
    const-string v7, "android.intent.category.HOME"

    invoke-virtual {v2, v7}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 455
    const/4 v7, 0x1

    invoke-virtual {v6, v2, v7}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 457
    .local v1, "homeApps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v7

    if-ge v4, v7, :cond_0

    .line 459
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/pm/ResolveInfo;

    .line 460
    .local v5, "info":Landroid/content/pm/ResolveInfo;
    iget-object v7, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v7, v7, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 457
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 463
    .end local v1    # "homeApps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v2    # "homeIntent":Landroid/content/Intent;
    .end local v4    # "i":I
    .end local v5    # "info":Landroid/content/pm/ResolveInfo;
    .end local v6    # "pm":Landroid/content/pm/PackageManager;
    :catch_0
    move-exception v0

    .line 465
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 468
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    return-object v3
.end method

.method public static xdmGetNotibarState()I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 871
    sget-object v2, Lcom/policydm/XDMApplication;->m_Context:Landroid/content/Context;

    const-string v3, "SPDSharedPref"

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 872
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v2, "Notibar"

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 874
    .local v1, "state":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "get NotibarState : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 876
    return v1
.end method

.method public static xdmGetReleaseVer()Ljava/lang/String;
    .locals 1

    .prologue
    .line 766
    sget-object v0, Lcom/policydm/XDMApplication;->m_szCurrentReleaseVer:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 768
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmSetReleaseVer()V

    .line 770
    :cond_0
    sget-object v0, Lcom/policydm/XDMApplication;->m_szCurrentReleaseVer:Ljava/lang/String;

    return-object v0
.end method

.method public static xdmGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;
    .locals 5
    .param p0, "szServiceName"    # Ljava/lang/String;

    .prologue
    .line 775
    const/4 v2, 0x0

    .line 778
    .local v2, "manager":Ljava/lang/Object;
    :try_start_0
    sget-object v3, Lcom/policydm/XDMApplication;->m_Context:Landroid/content/Context;

    invoke-virtual {v3, p0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 779
    if-nez v2, :cond_0

    .line 781
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/16 v3, 0xa

    if-ge v1, v3, :cond_0

    .line 785
    const-wide/16 v3, 0x3e8

    :try_start_1
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 792
    :goto_1
    :try_start_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is null, retry..."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 793
    sget-object v3, Lcom/policydm/XDMApplication;->m_Context:Landroid/content/Context;

    invoke-virtual {v3, p0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 794
    if-eqz v2, :cond_1

    .line 804
    .end local v1    # "i":I
    .end local v2    # "manager":Ljava/lang/Object;
    :cond_0
    :goto_2
    return-object v2

    .line 787
    .restart local v1    # "i":I
    .restart local v2    # "manager":Ljava/lang/Object;
    :catch_0
    move-exception v0

    .line 789
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 799
    .end local v0    # "e":Ljava/lang/InterruptedException;
    .end local v1    # "i":I
    .end local v2    # "manager":Ljava/lang/Object;
    :catch_1
    move-exception v0

    .line 801
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_2

    .line 781
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "i":I
    .restart local v2    # "manager":Ljava/lang/Object;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static xdmGetServiceType()I
    .locals 1

    .prologue
    .line 277
    sget v0, Lcom/policydm/XDMApplication;->nServiceType:I

    return v0
.end method

.method public static xdmGetSimState()I
    .locals 1

    .prologue
    .line 296
    sget v0, Lcom/policydm/XDMApplication;->nSimStatus:I

    return v0
.end method

.method public static xdmGetTopActivityName()Ljava/lang/String;
    .locals 7

    .prologue
    .line 373
    const-string v4, "NoActivity"

    .line 377
    .local v4, "szActivityName":Ljava/lang/String;
    :try_start_0
    const-string v5, "activity"

    invoke-static {v5}, Lcom/policydm/XDMApplication;->xdmGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 378
    .local v0, "activityManager":Landroid/app/ActivityManager;
    if-nez v0, :cond_0

    .line 380
    const-string v5, "activityManager is null!!"

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 405
    .end local v0    # "activityManager":Landroid/app/ActivityManager;
    :goto_0
    return-object v4

    .line 384
    .restart local v0    # "activityManager":Landroid/app/ActivityManager;
    :cond_0
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v3

    .line 385
    .local v3, "runningTasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    if-nez v3, :cond_1

    .line 387
    const-string v5, "runningTasks is null!!"

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 399
    .end local v0    # "activityManager":Landroid/app/ActivityManager;
    .end local v3    # "runningTasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    :catch_0
    move-exception v2

    .line 401
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 404
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "TopActivity : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 391
    .restart local v0    # "activityManager":Landroid/app/ActivityManager;
    .restart local v3    # "runningTasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    :cond_1
    const/4 v5, 0x0

    :try_start_1
    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v1, v5, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    .line 392
    .local v1, "componentName":Landroid/content/ComponentName;
    if-nez v1, :cond_2

    .line 394
    const-string v5, "componentName is null!!"

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 397
    :cond_2
    invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v4

    goto :goto_1
.end method

.method public static xdmGetTopPackageName()Ljava/lang/String;
    .locals 7

    .prologue
    .line 410
    const-string v4, "NoPackage"

    .line 414
    .local v4, "szPackageName":Ljava/lang/String;
    :try_start_0
    const-string v5, "activity"

    invoke-static {v5}, Lcom/policydm/XDMApplication;->xdmGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 415
    .local v0, "activityManager":Landroid/app/ActivityManager;
    if-nez v0, :cond_0

    .line 417
    const-string v5, "activityManager is null!!"

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 442
    .end local v0    # "activityManager":Landroid/app/ActivityManager;
    :goto_0
    return-object v4

    .line 421
    .restart local v0    # "activityManager":Landroid/app/ActivityManager;
    :cond_0
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v3

    .line 422
    .local v3, "runningTasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    if-nez v3, :cond_1

    .line 424
    const-string v5, "runningTasks is null!!"

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 436
    .end local v0    # "activityManager":Landroid/app/ActivityManager;
    .end local v3    # "runningTasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    :catch_0
    move-exception v2

    .line 438
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 441
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "TopPackageName : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 428
    .restart local v0    # "activityManager":Landroid/app/ActivityManager;
    .restart local v3    # "runningTasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    :cond_1
    const/4 v5, 0x0

    :try_start_1
    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v1, v5, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    .line 429
    .local v1, "componentName":Landroid/content/ComponentName;
    if-nez v1, :cond_2

    .line 431
    const-string v5, "componentName is null!!"

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 434
    :cond_2
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v4

    goto :goto_1
.end method

.method public static xdmInitializing()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 262
    sget-boolean v0, Lcom/policydm/agent/XDMTask;->g_IsSyncTaskInit:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Lcom/policydm/agent/XDMTask;->g_IsDMInitialized:Z

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/policydm/XDMApplication;->g_bIsInitializing:Z

    if-nez v0, :cond_1

    .line 264
    sput-boolean v1, Lcom/policydm/XDMApplication;->g_bIsInitializing:Z

    .line 265
    const-string v0, "----------- XEVENT_DM_INIT WIFI ok"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 266
    invoke-static {v1, v2, v2}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 273
    :cond_0
    :goto_0
    return-void

    .line 268
    :cond_1
    sget v0, Lcom/policydm/XDMApplication;->g_nResumeStatus:I

    if-eqz v0, :cond_0

    .line 270
    const-string v0, "Run Resume Operation, Resume call WIFIConnect"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 271
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmResumeOperation()Z

    goto :goto_0
.end method

.method public static xdmProtoIsMobileDataConnected()Z
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 693
    const/4 v0, 0x0

    .line 696
    .local v0, "bRet":Z
    :try_start_0
    sget-object v5, Lcom/policydm/XDMApplication;->m_Context:Landroid/content/Context;

    const-string v6, "connectivity"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 697
    .local v1, "cm":Landroid/net/ConnectivityManager;
    if-nez v1, :cond_0

    .line 699
    const-string v5, "ConnectivityManager is null!!"

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 724
    :goto_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Mobile Data Connected : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 730
    .end local v0    # "bRet":Z
    .end local v1    # "cm":Landroid/net/ConnectivityManager;
    :goto_1
    return v0

    .line 703
    .restart local v0    # "bRet":Z
    .restart local v1    # "cm":Landroid/net/ConnectivityManager;
    :cond_0
    const/4 v5, 0x0

    invoke-virtual {v1, v5}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v3

    .line 705
    .local v3, "mobileInfo":Landroid/net/NetworkInfo;
    if-nez v3, :cond_1

    .line 707
    const-string v5, "moblie data info is null!!"

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 727
    .end local v1    # "cm":Landroid/net/ConnectivityManager;
    .end local v3    # "mobileInfo":Landroid/net/NetworkInfo;
    :catch_0
    move-exception v2

    .line 729
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move v0, v4

    .line 730
    goto :goto_1

    .line 711
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v1    # "cm":Landroid/net/ConnectivityManager;
    .restart local v3    # "mobileInfo":Landroid/net/NetworkInfo;
    :cond_1
    :try_start_1
    invoke-virtual {v3}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 713
    const/4 v5, 0x1

    sput v5, Lcom/policydm/XDMApplication;->nCurrentNetwork:I

    .line 714
    const-string v5, "a Current network is Mobile Data"

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 715
    const/4 v0, 0x1

    goto :goto_0

    .line 719
    :cond_2
    const-string v5, "Mobile Data DisConnected"

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public static xdmProtoIsWIFIConnected()Z
    .locals 4

    .prologue
    .line 656
    const/4 v0, 0x0

    .line 658
    .local v0, "bWifiConn":Z
    const-string v3, "connectivity"

    invoke-static {v3}, Lcom/policydm/XDMApplication;->xdmGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 659
    .local v1, "connectivityManager":Landroid/net/ConnectivityManager;
    if-nez v1, :cond_0

    .line 661
    const-string v3, "connectivityManager is null!!"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 662
    const/4 v0, 0x0

    .line 687
    .end local v0    # "bWifiConn":Z
    :goto_0
    return v0

    .line 666
    .restart local v0    # "bWifiConn":Z
    :cond_0
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    .line 667
    .local v2, "wifiInfo":Landroid/net/NetworkInfo;
    if-nez v2, :cond_1

    .line 669
    const-string v3, "WifiInfo is null!!"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 670
    const/4 v0, 0x0

    goto :goto_0

    .line 674
    :cond_1
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 676
    const/4 v3, 0x2

    sput v3, Lcom/policydm/XDMApplication;->nCurrentNetwork:I

    .line 677
    const-string v3, "a Current network is WiFi"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 678
    const/4 v0, 0x1

    goto :goto_0

    .line 682
    :cond_2
    const-string v3, "WiFi DisConnected"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 683
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static xdmResumeOperation()Z
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v7, 0x0

    .line 316
    const-string v5, ""

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 318
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmProtoIsWIFIConnected()Z

    move-result v0

    .line 319
    .local v0, "bWificonnected":Z
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMOStatus()I

    move-result v1

    .line 320
    .local v1, "nFumoStatus":I
    invoke-static {}, Lcom/policydm/db/XDBSpdAdp;->xdbGetSpdState()I

    move-result v2

    .line 322
    .local v2, "nSpdStatus":I
    invoke-static {}, Lcom/policydm/adapter/XDMInitAdapter;->xdmInitAdpGetNetStatus()I

    move-result v5

    const/4 v6, -0x1

    if-ne v5, v6, :cond_0

    .line 324
    const-string v4, "already connect to network, return"

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 368
    :goto_0
    return v3

    .line 327
    :cond_0
    sget-boolean v5, Lcom/policydm/adapter/XDMFeature;->XDM_FEATURE_WIFI_ONLY_MODEL:Z

    if-eqz v5, :cond_1

    if-nez v0, :cond_1

    .line 329
    const-string v4, "WIFI_ONLY_MODEL but Wi-Fi Disconnected, return"

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 333
    :cond_1
    sget v5, Lcom/policydm/XDMApplication;->g_nResumeStatus:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_3

    .line 335
    sparse-switch v1, :sswitch_data_0

    .line 366
    :cond_2
    :goto_1
    sput v3, Lcom/policydm/XDMApplication;->g_nResumeStatus:I

    move v3, v4

    .line 368
    goto :goto_0

    .line 341
    :sswitch_0
    const/16 v5, 0x28

    invoke-static {v5, v7, v7}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    .line 347
    :cond_3
    sget v5, Lcom/policydm/XDMApplication;->g_nResumeStatus:I

    if-ne v5, v4, :cond_4

    .line 349
    packed-switch v1, :pswitch_data_0

    .line 355
    invoke-static {v3}, Lcom/policydm/noti/XNOTIAdapter;->xnotiPushAdpResumeNotiAction(I)V

    goto :goto_1

    .line 352
    :pswitch_0
    const/16 v5, 0xa

    invoke-static {v5, v7, v7}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    .line 359
    :cond_4
    sget v5, Lcom/policydm/XDMApplication;->g_nResumeStatus:I

    const/4 v6, 0x3

    if-ne v5, v6, :cond_2

    .line 361
    const/4 v5, 0x4

    if-ne v2, v5, :cond_5

    .line 362
    const/16 v5, 0x3f

    invoke-static {v5, v7, v7}, Lcom/policydm/eng/core/XDMMsg;->xdmSendRCMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    .line 363
    :cond_5
    const/4 v5, 0x5

    if-ne v2, v5, :cond_2

    .line 364
    const/16 v5, 0x40

    invoke-static {v5, v7, v7}, Lcom/policydm/eng/core/XDMMsg;->xdmSendRCMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    .line 335
    nop

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0x1e -> :sswitch_0
        0x28 -> :sswitch_0
        0xc8 -> :sswitch_0
    .end sparse-switch

    .line 349
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
    .end packed-switch
.end method

.method public static xdmSendMessageDmHandler(I)V
    .locals 1
    .param p0, "nEvent"    # I

    .prologue
    .line 853
    sget-object v0, Lcom/policydm/XDMApplication;->m_hDmHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 854
    sget-object v0, Lcom/policydm/XDMApplication;->m_hDmHandler:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 857
    :goto_0
    return-void

    .line 856
    :cond_0
    const-string v0, "m_hDmHandler is null!!"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdmSetDataState(I)V
    .locals 0
    .param p0, "state"    # I

    .prologue
    .line 311
    sput p0, Lcom/policydm/XDMApplication;->mDataState:I

    .line 312
    return-void
.end method

.method public static xdmSetNotibarState(I)V
    .locals 5
    .param p0, "state"    # I

    .prologue
    .line 861
    sget-object v2, Lcom/policydm/XDMApplication;->m_Context:Landroid/content/Context;

    const-string v3, "SPDSharedPref"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 862
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 863
    .local v0, "ed":Landroid/content/SharedPreferences$Editor;
    const-string v2, "Notibar"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 864
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 866
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "set NotibarState : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 867
    return-void
.end method

.method public static xdmSetReleaseVer()V
    .locals 1

    .prologue
    .line 747
    sget-object v0, Lcom/policydm/XDMApplication;->m_Context:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 748
    sget-object v0, Lcom/policydm/XDMApplication;->m_Context:Landroid/content/Context;

    invoke-static {v0}, Lcom/policydm/XDMApplication;->xdmSetReleaseVer(Landroid/content/Context;)V

    .line 749
    :cond_0
    return-void
.end method

.method public static xdmSetReleaseVer(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 755
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 756
    .local v1, "pi":Landroid/content/pm/PackageInfo;
    iget-object v2, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    sput-object v2, Lcom/policydm/XDMApplication;->m_szCurrentReleaseVer:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 762
    .end local v1    # "pi":Landroid/content/pm/PackageInfo;
    :goto_0
    return-void

    .line 758
    :catch_0
    move-exception v0

    .line 760
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdmSetServiceType(I)I
    .locals 2
    .param p0, "Type"    # I

    .prologue
    .line 282
    const/4 v0, 0x0

    .line 283
    .local v0, "ret":I
    sget v1, Lcom/policydm/XDMApplication;->nServiceType:I

    if-eq v1, p0, :cond_0

    .line 285
    sput p0, Lcom/policydm/XDMApplication;->nServiceType:I

    .line 291
    :goto_0
    return v0

    .line 289
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static xdmSetSimState(I)V
    .locals 0
    .param p0, "state"    # I

    .prologue
    .line 301
    sput p0, Lcom/policydm/XDMApplication;->nSimStatus:I

    .line 302
    return-void
.end method

.method public static xdmShowToast(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "szText"    # Ljava/lang/String;
    .param p2, "duration"    # I

    .prologue
    .line 736
    sget-object v1, Lcom/policydm/XDMApplication;->m_ToastAlreadyAdded:Landroid/widget/Toast;

    if-nez v1, :cond_0

    .line 738
    new-instance v0, Landroid/view/ContextThemeWrapper;

    const/high16 v1, 0x7f090000

    invoke-direct {v0, p0, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 739
    .local v0, "themeContext":Landroid/content/Context;
    const/4 v1, 0x0

    invoke-static {v0, v1, p2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    sput-object v1, Lcom/policydm/XDMApplication;->m_ToastAlreadyAdded:Landroid/widget/Toast;

    .line 741
    .end local v0    # "themeContext":Landroid/content/Context;
    :cond_0
    sget-object v1, Lcom/policydm/XDMApplication;->m_ToastAlreadyAdded:Landroid/widget/Toast;

    invoke-virtual {v1, p1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    .line 742
    sget-object v1, Lcom/policydm/XDMApplication;->m_ToastAlreadyAdded:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 743
    return-void
.end method

.method public static xdmStartAlarm(IJ)V
    .locals 7
    .param p0, "nID"    # I
    .param p1, "alarmtime"    # J

    .prologue
    const/4 v6, 0x0

    .line 881
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Alarm ID: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 883
    const-string v2, "alarm"

    invoke-static {v2}, Lcom/policydm/XDMApplication;->xdmGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 884
    .local v0, "am":Landroid/app/AlarmManager;
    if-nez v0, :cond_0

    .line 886
    const-string v2, "AlarmManager is null!!"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 909
    :goto_0
    return-void

    .line 890
    :cond_0
    const/4 v1, 0x0

    .line 891
    .local v1, "sender":Landroid/app/PendingIntent;
    if-nez p0, :cond_2

    .line 893
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.policydm.intent.action.POLLING_TIME"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v6, v3, v6}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 908
    :cond_1
    :goto_1
    invoke-virtual {v0, v6, p1, p2, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    goto :goto_0

    .line 895
    :cond_2
    const/4 v2, 0x1

    if-ne p0, v2, :cond_3

    .line 897
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.policydm.intent.action.POLLING_REPORTTIME"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v6, v3, v6}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    goto :goto_1

    .line 899
    :cond_3
    const/4 v2, 0x2

    if-ne p0, v2, :cond_1

    .line 901
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/32 v4, 0xa4cb800

    add-long p1, v2, v4

    .line 902
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.policydm.intent.action.EULA_TIME"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v6, v3, v6}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 904
    invoke-static {p1, p2}, Lcom/policydm/db/XDBSpdAdp;->xdbSetSpdEulaTime(J)V

    .line 905
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Eula Time:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1, p2}, Lcom/policydm/XDMApplication;->xdmDisplayAlarmTime(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static xdmStopAlarm(I)V
    .locals 7
    .param p0, "nID"    # I

    .prologue
    const/high16 v6, 0x10000000

    const/4 v5, 0x0

    .line 913
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Alarm ID: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 915
    const-string v2, "alarm"

    invoke-static {v2}, Lcom/policydm/XDMApplication;->xdmGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 916
    .local v0, "am":Landroid/app/AlarmManager;
    if-nez v0, :cond_0

    .line 918
    const-string v2, "AlarmManager is null!!"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 938
    :goto_0
    return-void

    .line 922
    :cond_0
    const/4 v1, 0x0

    .line 923
    .local v1, "sender":Landroid/app/PendingIntent;
    if-nez p0, :cond_2

    .line 925
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.policydm.intent.action.POLLING_TIME"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v5, v3, v6}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 937
    :cond_1
    :goto_1
    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    goto :goto_0

    .line 927
    :cond_2
    const/4 v2, 0x1

    if-ne p0, v2, :cond_3

    .line 929
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.policydm.intent.action.POLLING_REPORTTIME"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v5, v3, v6}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    goto :goto_1

    .line 931
    :cond_3
    const/4 v2, 0x2

    if-ne p0, v2, :cond_1

    .line 933
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.policydm.intent.action.EULA_TIME"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v5, v3, v6}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 934
    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Lcom/policydm/db/XDBSpdAdp;->xdbSetSpdEulaTime(J)V

    goto :goto_1
.end method

.method public static xdmWakeLockAcquire()V
    .locals 4

    .prologue
    .line 809
    const-string v2, ""

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 812
    :try_start_0
    sget-object v2, Lcom/policydm/XDMApplication;->m_WakeLock:Landroid/os/PowerManager$WakeLock;

    if-nez v2, :cond_0

    .line 814
    const-string v2, "m_WakeLock is acquire!!"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 815
    const-string v2, "power"

    invoke-static {v2}, Lcom/policydm/XDMApplication;->xdmGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    .line 816
    .local v1, "pm":Landroid/os/PowerManager;
    if-nez v1, :cond_1

    .line 818
    const-string v2, "PowerManager is null!!"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 831
    :cond_0
    :goto_0
    return-void

    .line 822
    :cond_1
    const/4 v2, 0x1

    const-string v3, "wakeLock"

    invoke-virtual {v1, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    sput-object v2, Lcom/policydm/XDMApplication;->m_WakeLock:Landroid/os/PowerManager$WakeLock;

    .line 823
    sget-object v2, Lcom/policydm/XDMApplication;->m_WakeLock:Landroid/os/PowerManager$WakeLock;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 824
    sget-object v2, Lcom/policydm/XDMApplication;->m_WakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->acquire()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 827
    :catch_0
    move-exception v0

    .line 829
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xdmWakeLockRelease()V
    .locals 2

    .prologue
    .line 835
    const-string v1, ""

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 838
    :try_start_0
    sget-object v1, Lcom/policydm/XDMApplication;->m_WakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_0

    .line 840
    const-string v1, "m_WakeLock is release!!"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 841
    sget-object v1, Lcom/policydm/XDMApplication;->m_WakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 842
    const/4 v1, 0x0

    sput-object v1, Lcom/policydm/XDMApplication;->m_WakeLock:Landroid/os/PowerManager$WakeLock;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 849
    .local v0, "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    return-void

    .line 845
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_0
    move-exception v0

    .line 847
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method protected attachBaseContext(Landroid/content/Context;)V
    .locals 0
    .param p1, "base"    # Landroid/content/Context;

    .prologue
    .line 204
    invoke-super {p0, p1}, Landroid/app/Application;->attachBaseContext(Landroid/content/Context;)V

    .line 205
    sput-object p1, Lcom/policydm/XDMApplication;->m_Context:Landroid/content/Context;

    .line 206
    return-void
.end method

.method public onCreate()V
    .locals 4

    .prologue
    .line 96
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 98
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v1

    if-eqz v1, :cond_0

    .line 100
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "User mode!! id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 199
    :goto_0
    return-void

    .line 104
    :cond_0
    sput-object p0, Lcom/policydm/XDMApplication;->m_Context:Landroid/content/Context;

    .line 106
    new-instance v1, Lcom/policydm/agent/XDMTask;

    invoke-direct {v1}, Lcom/policydm/agent/XDMTask;-><init>()V

    sput-object v1, Lcom/policydm/XDMApplication;->g_Task:Lcom/policydm/agent/XDMTask;

    .line 107
    new-instance v1, Lcom/policydm/agent/XDMUITask;

    invoke-direct {v1}, Lcom/policydm/agent/XDMUITask;-><init>()V

    sput-object v1, Lcom/policydm/XDMApplication;->g_UITask:Lcom/policydm/agent/XDMUITask;

    .line 108
    new-instance v1, Lcom/policydm/push/XPushProcess;

    invoke-direct {v1}, Lcom/policydm/push/XPushProcess;-><init>()V

    sput-object v1, Lcom/policydm/XDMApplication;->g_PushTask:Lcom/policydm/push/XPushProcess;

    .line 109
    new-instance v1, Lcom/policydm/restclient/XRCProcess;

    invoke-direct {v1}, Lcom/policydm/restclient/XRCProcess;-><init>()V

    sput-object v1, Lcom/policydm/XDMApplication;->g_RestClientTask:Lcom/policydm/restclient/XRCProcess;

    .line 111
    sget-object v1, Lcom/policydm/XDMApplication;->m_Context:Landroid/content/Context;

    invoke-static {v1}, Lcom/policydm/XDMApplication;->xdmSetReleaseVer(Landroid/content/Context;)V

    .line 112
    invoke-static {}, Lcom/policydm/adapter/XDMFeature;->xdmInitialize()V

    .line 116
    :try_start_0
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmWakeLockAcquire()V

    .line 118
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/policydm/eng/core/XDMMsg;->xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 119
    invoke-static {p0}, Lcom/policydm/ui/XUINotificationManager;->xuiNotiInitialize(Landroid/content/Context;)V

    .line 121
    const-string v1, "phone"

    invoke-static {v1}, Lcom/policydm/XDMApplication;->xdmGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    sput-object v1, Lcom/policydm/XDMApplication;->telephonyManager:Landroid/telephony/TelephonyManager;

    .line 122
    sget-object v1, Lcom/policydm/XDMApplication;->telephonyManager:Landroid/telephony/TelephonyManager;

    if-nez v1, :cond_1

    .line 124
    const-string v1, "TelephonyManager is null!!"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 195
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmWakeLockRelease()V

    goto :goto_0

    .line 128
    :cond_1
    :try_start_1
    new-instance v1, Lcom/policydm/XDMApplication$XDMPhoneStateListener;

    invoke-direct {v1}, Lcom/policydm/XDMApplication$XDMPhoneStateListener;-><init>()V

    sput-object v1, Lcom/policydm/XDMApplication;->phoneListener:Lcom/policydm/XDMApplication$XDMPhoneStateListener;

    .line 129
    sget-object v1, Lcom/policydm/XDMApplication;->telephonyManager:Landroid/telephony/TelephonyManager;

    sget-object v2, Lcom/policydm/XDMApplication;->phoneListener:Lcom/policydm/XDMApplication$XDMPhoneStateListener;

    const/16 v3, 0x1f1

    invoke-virtual {v1, v2, v3}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 136
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmGetServiceType()I

    move-result v1

    invoke-static {v1}, Lcom/policydm/XDMApplication;->xdmSetServiceType(I)I

    .line 138
    invoke-static {}, Lcom/policydm/adapter/XDMTargetAdapter;->xdmInitExternalStorageState()V

    .line 140
    new-instance v1, Lcom/policydm/XDMApplication$1;

    invoke-direct {v1, p0}, Lcom/policydm/XDMApplication$1;-><init>(Lcom/policydm/XDMApplication;)V

    sput-object v1, Lcom/policydm/XDMApplication;->m_hDmHandler:Landroid/os/Handler;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 195
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmWakeLockRelease()V

    .line 198
    :goto_1
    const-string v1, "DMApplication Start !"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_0

    .line 189
    :catch_0
    move-exception v0

    .line 191
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 195
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmWakeLockRelease()V

    goto :goto_1

    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    invoke-static {}, Lcom/policydm/XDMApplication;->xdmWakeLockRelease()V

    throw v1
.end method
