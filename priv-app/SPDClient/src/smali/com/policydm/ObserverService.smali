.class public Lcom/policydm/ObserverService;
.super Landroid/app/Service;
.source "ObserverService.java"


# instance fields
.field private m_SettingObserver:Lcom/policydm/SettingContentObserver;

.field private m_hSettingHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 15
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 17
    iput-object v0, p0, Lcom/policydm/ObserverService;->m_SettingObserver:Lcom/policydm/SettingContentObserver;

    .line 18
    iput-object v0, p0, Lcom/policydm/ObserverService;->m_hSettingHandler:Landroid/os/Handler;

    return-void
.end method


# virtual methods
.method public autoUpdateChanged()V
    .locals 1

    .prologue
    .line 46
    const-string v0, ""

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 47
    invoke-static {}, Lcom/policydm/db/XDB;->xdbGetAutoUpdateFlag()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 49
    invoke-static {}, Lcom/policydm/db/XDB;->xdbIsAlreadyAgree()Z

    move-result v0

    if-nez v0, :cond_0

    .line 51
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/policydm/db/XDBSpdAdp;->xdbSetSpdDeviceRegister(I)V

    .line 52
    const/16 v0, 0xcd

    invoke-static {v0}, Lcom/policydm/XDMApplication;->xdmSendMessageDmHandler(I)V

    .line 54
    :cond_0
    invoke-static {}, Lcom/policydm/polling/XPollingAgent;->xpollingDefaultPollingTime()V

    .line 56
    :cond_1
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 41
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 23
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 24
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/policydm/ObserverService;->m_hSettingHandler:Landroid/os/Handler;

    .line 25
    new-instance v0, Lcom/policydm/SettingContentObserver;

    iget-object v1, p0, Lcom/policydm/ObserverService;->m_hSettingHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/policydm/SettingContentObserver;-><init>(Lcom/policydm/ObserverService;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/policydm/ObserverService;->m_SettingObserver:Lcom/policydm/SettingContentObserver;

    .line 27
    invoke-virtual {p0}, Lcom/policydm/ObserverService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/policydm/ObserverService;->m_SettingObserver:Lcom/policydm/SettingContentObserver;

    invoke-virtual {v1}, Lcom/policydm/SettingContentObserver;->getUpdateUri()Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/policydm/ObserverService;->m_SettingObserver:Lcom/policydm/SettingContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 28
    invoke-virtual {p0}, Lcom/policydm/ObserverService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/policydm/ObserverService;->m_SettingObserver:Lcom/policydm/SettingContentObserver;

    invoke-virtual {v1}, Lcom/policydm/SettingContentObserver;->getSPDRegisterUri()Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/policydm/ObserverService;->m_SettingObserver:Lcom/policydm/SettingContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 29
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 34
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 35
    invoke-virtual {p0}, Lcom/policydm/ObserverService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/policydm/ObserverService;->m_SettingObserver:Lcom/policydm/SettingContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 36
    return-void
.end method

.method public spdRegisterChanged()V
    .locals 1

    .prologue
    .line 60
    const-string v0, ""

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 61
    invoke-static {}, Lcom/policydm/db/XDB;->xdbIsAlreadyAgree()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    invoke-virtual {p0}, Lcom/policydm/ObserverService;->stopSelf()V

    .line 65
    :cond_0
    return-void
.end method
