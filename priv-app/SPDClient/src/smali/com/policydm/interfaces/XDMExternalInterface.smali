.class public interface abstract Lcom/policydm/interfaces/XDMExternalInterface;
.super Ljava/lang/Object;
.source "XDMExternalInterface.java"


# static fields
.field public static final SETTING_PROVIDER_SPD_AUTOUPDATE:Ljava/lang/String; = "security_update_db"

.field public static final SETTING_PROVIDER_SPD_AUTOUPDATE_FAIL:Ljava/lang/String; = "SPD_autoupdate_fail"

.field public static final SETTING_PROVIDER_SPD_EULA_AGREE:Ljava/lang/String; = "samsung_eula_agree"

.field public static final SETTING_PROVIDER_SPD_REGISTERD_URL:Ljava/lang/String; = "SPD_REGISTERD"

.field public static final SETTING_PROVIDER_SPD_WAITWIFI:Ljava/lang/String; = "use_Wait_Wifi_db"

.field public static final SETTING_PROVIDER_SPD_WIFIONLY:Ljava/lang/String; = "use_wifi_only_db"

.field public static final XDM_BEARER_OTA_PREFER:I = 0x2

.field public static final XDM_BEARER_WIFI_ONLY:I = 0x1
