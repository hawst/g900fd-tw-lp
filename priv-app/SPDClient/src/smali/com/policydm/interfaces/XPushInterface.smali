.class public interface abstract Lcom/policydm/interfaces/XPushInterface;
.super Ljava/lang/Object;
.source "XPushInterface.java"


# static fields
.field public static final NOTIFICATION_ACK_RESULT_ACTION:Ljava/lang/String; = "com.sec.spp.NotificationAckResultAction"

.field public static final PUSH_REGISTRATION_CHANGED_ACTION:Ljava/lang/String; = "com.sec.spp.RegistrationChangedAction"

.field public static final PUSH_SERVICE_REQUEST:Ljava/lang/String; = "com.sec.spp.action.SPP_REQUEST"

.field public static final SERVICE_ABNORMALLY_STOPPED_ACTION:Ljava/lang/String; = "com.sec.spp.ServiceAbnormallyStoppedAction"

.field public static final SERVICE_DATA_DELETED_ACTION:Ljava/lang/String; = "com.sec.spp.ServiceDataDeletedAction"

.field public static final STATE_DEREGISTRATION_FAIL:I = 0x3

.field public static final STATE_DEREGISTRATION_SUCCESS:I = 0x2

.field public static final STATE_REGISTRATION_FAIL:I = 0x1

.field public static final STATE_REGISTRATION_SUCCESS:I = 0x0

.field public static final XPUSH_EXTRA_ACKRESULT:Ljava/lang/String; = "ackResult"

.field public static final XPUSH_EXTRA_APPID:Ljava/lang/String; = "appId"

.field public static final XPUSH_EXTRA_ERROR:Ljava/lang/String; = "Error"

.field public static final XPUSH_EXTRA_MSG:Ljava/lang/String; = "msg"

.field public static final XPUSH_EXTRA_NOTIID:Ljava/lang/String; = "notificationId"

.field public static final XPUSH_EXTRA_REGID:Ljava/lang/String; = "RegistrationID"

.field public static final XPUSH_EXTRA_REQTYPE:Ljava/lang/String; = "reqType"

.field public static final XPUSH_EXTRA_STATUS:Ljava/lang/String; = "com.sec.spp.Status"

.field public static final XPUSH_EXTRA_USERDATA:Ljava/lang/String; = "userdata"

.field public static final XPUSH_GCM_APPID:Ljava/lang/String; = "96666210016"

.field public static final XPUSH_REQ_TYPE_REGISTRATION:I = 0x1

.field public static final XPUSH_REQ_TYPE_UNREGISTRATION:I = 0x2

.field public static final XPUSH_SPP_APPID:Ljava/lang/String; = "e668374785e8ac2a"

.field public static final XPUSH_SPP_PACKAGENAME:Ljava/lang/String; = "com.sec.spp.push"
