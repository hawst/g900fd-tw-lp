.class public interface abstract Lcom/policydm/interfaces/XUIInterface;
.super Ljava/lang/Object;
.source "XUIInterface.java"


# static fields
.field public static final ACTIVITY_NAME_DIALOG:Ljava/lang/String; = "XUIDialogActivity"

.field public static final ACTIVITY_NAME_EULA:Ljava/lang/String; = "XUIEulaActivity"

.field public static final PACKAGE_NAME_ANDROID:Ljava/lang/String; = "android"

.field public static final PACKAGE_NAME_SETTING:Ljava/lang/String; = "settings"

.field public static final XUI_AUTOUPDATE_CONFIRM:I = 0xcd

.field public static final XUI_DL_CONNECT_FAIL:I = 0x99

.field public static final XUI_DL_DOWNLOAD_IN_COMPLETE:I = 0x97

.field public static final XUI_DL_DOWNLOAD_YES_NO:I = 0x96

.field public static final XUI_DL_RECEIVE_FAIL:I = 0x9b

.field public static final XUI_DL_RESUME_DOWNLOAD:I = 0x98

.field public static final XUI_DL_SEND_FAIL:I = 0x9a

.field public static final XUI_DM_CONNECT:I = 0x66

.field public static final XUI_DM_CONNECTING:I = 0x67

.field public static final XUI_DM_CONNECT_FAIL:I = 0x78

.field public static final XUI_DM_ERROR:I = 0x7a

.field public static final XUI_DM_FINISH:I = 0x82

.field public static final XUI_DM_IDLE_STATE:I = 0x64

.field public static final XUI_DM_MEMORY_FULL:I = 0x79

.field public static final XUI_DM_NONE:I = 0x0

.field public static final XUI_DM_NOTI_BACKGROUND:I = 0x8d

.field public static final XUI_DM_NOTI_FOREGROUND:I = 0x90

.field public static final XUI_DM_NOTI_INFORMATIVE:I = 0x8e

.field public static final XUI_DM_NOTI_INTERACTIVE:I = 0x8f

.field public static final XUI_DM_NOTI_NOT_SPECIFIED:I = 0x8c

.field public static final XUI_DM_NOT_INIT:I = 0x68

.field public static final XUI_DM_START_NETWORK_WARNING:I = 0x7c

.field public static final XUI_DM_SYNC_ERROR:I = 0x7b

.field public static final XUI_DM_SYNC_START:I = 0x65

.field public static final XUI_DM_UIC_REQUEST:I = 0x6e

.field public static final XUI_EULA_ACCEPT:I = 0xcb

.field public static final XUI_EULA_CONFIRM:I = 0xce

.field public static final XUI_EULA_DECLINE:I = 0xcc

.field public static final XUI_EULA_START_POLLING:I = 0xca

.field public static final XUI_EULA_START_PULL:I = 0xc8

.field public static final XUI_EULA_START_SETTING:I = 0xc9

.field public static final XUI_INDICATOR_CONNECT_FAIL:I = 0x2

.field public static final XUI_INDICATOR_ERROR:I = 0x3

.field public static final XUI_INDICATOR_EULA_OFF:I = 0x7

.field public static final XUI_INDICATOR_EULA_ON:I = 0x6

.field public static final XUI_INDICATOR_FINISH:I = 0x1

.field public static final XUI_INDICATOR_MEMORY_FULL:I = 0x4

.field public static final XUI_INDICATOR_NETWORK_WARNING:I = 0x8

.field public static final XUI_INDICATOR_NETWORK_WARNING_OFF:I = 0x9

.field public static final XUI_INDICATOR_OFF:I = 0x0

.field public static final XUI_INDICATOR_SUCCESS:I = 0x5
