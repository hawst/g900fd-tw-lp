.class public interface abstract Lcom/policydm/interfaces/XEventInterface;
.super Ljava/lang/Object;
.source "XEventInterface.java"


# static fields
.field public static final XEVENT_ABORT_HTTP_ERROR:I = 0xf2

.field public static final XEVENT_ABORT_SYNCDM_ERROR:I = 0xf3

.field public static final XEVENT_ABORT_USER_REQ:I = 0xf1

.field public static final XEVENT_DL_ABORT:I = 0x2c

.field public static final XEVENT_DL_CONNECT:I = 0x28

.field public static final XEVENT_DL_CONNECTFAIL:I = 0x29

.field public static final XEVENT_DL_CONTINUE:I = 0x2b

.field public static final XEVENT_DL_FINISH:I = 0x2d

.field public static final XEVENT_DL_RECEIVEFAIL:I = 0x2f

.field public static final XEVENT_DL_SENDFAIL:I = 0x2e

.field public static final XEVENT_DL_START:I = 0x2a

.field public static final XEVENT_DL_TCPIP_OPEN:I = 0x30

.field public static final XEVENT_DM_ABORT:I = 0xe

.field public static final XEVENT_DM_CONNECT:I = 0xa

.field public static final XEVENT_DM_CONNECTFAIL:I = 0xb

.field public static final XEVENT_DM_CONTINUE:I = 0xd

.field public static final XEVENT_DM_FINISH:I = 0xf

.field public static final XEVENT_DM_INIT:I = 0x1

.field public static final XEVENT_DM_RECEIVEFAIL:I = 0x11

.field public static final XEVENT_DM_SENDFAIL:I = 0x10

.field public static final XEVENT_DM_START:I = 0xc

.field public static final XEVENT_DM_TCPIP_OPEN:I = 0x12

.field public static final XEVENT_NOTI_BACKGROUND:I = 0x21

.field public static final XEVENT_NOTI_EXECUTE:I = 0x1f

.field public static final XEVENT_NOTI_INFORMATIVE:I = 0x22

.field public static final XEVENT_NOTI_INTERACTIVE:I = 0x23

.field public static final XEVENT_NOTI_NOT_SPECIFIED:I = 0x20

.field public static final XEVENT_NOTI_RECEIVED:I = 0x1e

.field public static final XEVENT_OS_INITIALIZED:I = 0x0

.field public static final XEVENT_RC_DEVICE_CREATE:I = 0x3c

.field public static final XEVENT_RC_DEVICE_REPORT:I = 0x3f

.field public static final XEVENT_RC_DEVICE_UPDATE:I = 0x3d

.field public static final XEVENT_RC_GET_POLLINGINFO:I = 0x40

.field public static final XEVENT_RC_GET_VERSION:I = 0x41

.field public static final XEVENT_RC_PUSH_UPDATE:I = 0x3e

.field public static final XEVENT_UIC_REQUEST:I = 0x14

.field public static final XEVENT_UIC_RESPONSE:I = 0x15

.field public static final XEVENT_WAP_PUSH_RECEIVE:I = 0x24
