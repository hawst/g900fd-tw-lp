.class public interface abstract Lcom/policydm/interfaces/XDBInterface;
.super Ljava/lang/Object;
.source "XDBInterface.java"


# static fields
.field public static final E2P_SYNCML_SIM_IMSI:I = 0x78

.field public static final E2P_XDM_ACCNODE_IDX:I = 0x64

.field public static final E2P_XDM_ACCNODE_MAX:I = 0x69

.field public static final E2P_XDM_ACCXNODE_INFO1:I = 0x64

.field public static final E2P_XDM_ACCXNODE_INFO2:I = 0x65

.field public static final E2P_XDM_ACCXNODE_INFO3:I = 0x66

.field public static final E2P_XDM_ACCXNODE_INFO4:I = 0x67

.field public static final E2P_XDM_ACCXNODE_INFO5:I = 0x68

.field public static final E2P_XDM_AGENT_IDX:I = 0x6e

.field public static final E2P_XDM_AGENT_INFO:I = 0x6e

.field public static final E2P_XDM_AGENT_INFO_AGENT_TYPE:I = 0x6f

.field public static final E2P_XDM_AGENT_MAX:I = 0x70

.field public static final E2P_XDM_APPID:I = 0x3a

.field public static final E2P_XDM_AUTHLEVEL:I = 0x3b

.field public static final E2P_XDM_AUTHTYPE:I = 0x3d

.field public static final E2P_XDM_CHANGED_PROTOCOL:I = 0x39

.field public static final E2P_XDM_CLIENTNONCE:I = 0x43

.field public static final E2P_XDM_INFO:I = 0x32

.field public static final E2P_XDM_INFO_CONREF:I = 0x47

.field public static final E2P_XDM_INFO_IDX:I = 0x32

.field public static final E2P_XDM_INFO_MAX:I = 0x48

.field public static final E2P_XDM_INFO_PROFILENAME:I = 0x45

.field public static final E2P_XDM_MAGIC:I = 0x33

.field public static final E2P_XDM_NETWORKCONNNAME:I = 0x1

.field public static final E2P_XDM_NOTI_EVENT:I = 0x8

.field public static final E2P_XDM_NOTI_JOBID:I = 0xa

.field public static final E2P_XDM_NOTI_NOTI_RESYNC_MODE:I = 0xb

.field public static final E2P_XDM_NOTI_OPMODE:I = 0x9

.field public static final E2P_XDM_NOTI_SAVED_INFO:I = 0xe

.field public static final E2P_XDM_PASSWORD:I = 0x40

.field public static final E2P_XDM_PREFCONREF:I = 0x46

.field public static final E2P_XDM_PROFILE:I = 0x0

.field public static final E2P_XDM_PROFILENAME1:I = 0x4

.field public static final E2P_XDM_PROFILENAME2:I = 0x5

.field public static final E2P_XDM_PROFILENAME3:I = 0x6

.field public static final E2P_XDM_PROFILE_IDX:I = 0x0

.field public static final E2P_XDM_PROFILE_INDEX:I = 0x3

.field public static final E2P_XDM_PROFILE_MAGIC:I = 0xd

.field public static final E2P_XDM_PROFILE_MAX:I = 0x11

.field public static final E2P_XDM_PROTOCOL:I = 0x34

.field public static final E2P_XDM_PROXY_PROFILE_INDEX:I = 0x2

.field public static final E2P_XDM_RESYNC_IDX:I = 0x96

.field public static final E2P_XDM_RESYNC_MAX:I = 0x97

.field public static final E2P_XDM_RESYNC_MODE:I = 0x96

.field public static final E2P_XDM_SERVERAUTHLEVEL:I = 0x3c

.field public static final E2P_XDM_SERVERID:I = 0x41

.field public static final E2P_XDM_SERVERIP:I = 0x36

.field public static final E2P_XDM_SERVERNONCE:I = 0x44

.field public static final E2P_XDM_SERVERPASSWORD:I = 0x42

.field public static final E2P_XDM_SERVERPATH:I = 0x37

.field public static final E2P_XDM_SERVERPORT:I = 0x38

.field public static final E2P_XDM_SERVERURL:I = 0x35

.field public static final E2P_XDM_SERVER_AUTHTYPE:I = 0x3e

.field public static final E2P_XDM_SESSIONID:I = 0x7

.field public static final E2P_XDM_SIM_IDX:I = 0x78

.field public static final E2P_XDM_SIM_MAX:I = 0x79

.field public static final E2P_XDM_SKIP_DEV_DISCOVERY:I = 0xc

.field public static final E2P_XDM_UIC_RESULT_KEEP:I = 0xf

.field public static final E2P_XDM_UIC_RESULT_KEEP_FLAG:I = 0x10

.field public static final E2P_XDM_USERNAME:I = 0x3f

.field public static final E2P_XFOTA_CORRELATOR:I = 0xdc

.field public static final E2P_XFOTA_DOWNLOADMODE:I = 0xdb

.field public static final E2P_XFOTA_DOWNLOAD_RESULTCODE:I = 0xde

.field public static final E2P_XFOTA_IDX:I = 0xc8

.field public static final E2P_XFOTA_INFO:I = 0xc8

.field public static final E2P_XFOTA_INITIATED_TYPE:I = 0xdf

.field public static final E2P_XFOTA_MAX:I = 0xe1

.field public static final E2P_XFOTA_OBJECTDOWNLOADIP:I = 0xcf

.field public static final E2P_XFOTA_OBJECTDOWNLOADPORT:I = 0xd0

.field public static final E2P_XFOTA_OBJECTDOWNLOADPROTOCOL:I = 0xcd

.field public static final E2P_XFOTA_OBJECTDOWNLOADURL:I = 0xce

.field public static final E2P_XFOTA_OBJECTSIZE:I = 0xd9

.field public static final E2P_XFOTA_PKGDESC:I = 0xd7

.field public static final E2P_XFOTA_PKGNAME:I = 0xd5

.field public static final E2P_XFOTA_PKGVERSION:I = 0xd6

.field public static final E2P_XFOTA_PROCESSID:I = 0xd8

.field public static final E2P_XFOTA_PROTOCOL:I = 0xc9

.field public static final E2P_XFOTA_REPORTURL:I = 0xe0

.field public static final E2P_XFOTA_RESULTCODE:I = 0xdd

.field public static final E2P_XFOTA_SERVERIP:I = 0xcb

.field public static final E2P_XFOTA_SERVERPORT:I = 0xcc

.field public static final E2P_XFOTA_SERVERURL:I = 0xca

.field public static final E2P_XFOTA_STATUS:I = 0xda

.field public static final E2P_XFOTA_STATUSNOTIFYIP:I = 0xd3

.field public static final E2P_XFOTA_STATUSNOTIFYPORT:I = 0xd4

.field public static final E2P_XFOTA_STATUSNOTIFYPROTOCOL:I = 0xd1

.field public static final E2P_XFOTA_STATUSNOTIFYURL:I = 0xd2

.field public static final E2P_XNOTI_APPID:I = 0x12d

.field public static final E2P_XNOTI_IDX:I = 0x12c

.field public static final E2P_XNOTI_INFO:I = 0x12c

.field public static final E2P_XNOTI_JOBID:I = 0x132

.field public static final E2P_XNOTI_MAX:I = 0x133

.field public static final E2P_XNOTI_OPMODE:I = 0x131

.field public static final E2P_XNOTI_SERVERID:I = 0x130

.field public static final E2P_XNOTI_SESSIONID:I = 0x12f

.field public static final E2P_XNOTI_UIMODE:I = 0x12e

.field public static final E2P_XPOLLING_FILENAME:I = 0x85

.field public static final E2P_XPOLLING_IDX:I = 0x82

.field public static final E2P_XPOLLING_INFO:I = 0x82

.field public static final E2P_XPOLLING_MAX:I = 0x8f

.field public static final E2P_XPOLLING_NEXTPOLLINGTIME:I = 0x8d

.field public static final E2P_XPOLLING_NEXTREPORTTIME:I = 0x8e

.field public static final E2P_XPOLLING_ORGVERSION_URL:I = 0x83

.field public static final E2P_XPOLLING_PERIOD:I = 0x87

.field public static final E2P_XPOLLING_PERIODUNIT:I = 0x86

.field public static final E2P_XPOLLING_RANGE:I = 0x89

.field public static final E2P_XPOLLING_REPORT_PERIOD:I = 0x8a

.field public static final E2P_XPOLLING_REPORT_RANGE:I = 0x8c

.field public static final E2P_XPOLLING_REPORT_TIME:I = 0x8b

.field public static final E2P_XPOLLING_TIME:I = 0x88

.field public static final E2P_XPOLLING_VERSION_URL:I = 0x84

.field public static final E2P_XSPD_DEVICE_CREATE:I = 0x192

.field public static final E2P_XSPD_DEVICE_REGISTER:I = 0x191

.field public static final E2P_XSPD_DEVICE_UPDATE:I = 0x193

.field public static final E2P_XSPD_EULA_TIME:I = 0x199

.field public static final E2P_XSPD_IDX:I = 0x190

.field public static final E2P_XSPD_INFO:I = 0x190

.field public static final E2P_XSPD_MAX:I = 0x19a

.field public static final E2P_XSPD_POLICY_MODE:I = 0x198

.field public static final E2P_XSPD_PUSH_REGID:I = 0x195

.field public static final E2P_XSPD_PUSH_TYPE:I = 0x194

.field public static final E2P_XSPD_SERVERNONCE:I = 0x197

.field public static final E2P_XSPD_STATE:I = 0x196

.field public static final XDB_FS_ERR_BAD_PARAM:I = 0x2

.field public static final XDB_FS_ERR_FILE_NOT_FOUND:I = 0x3

.field public static final XDB_FS_ERR_NO_MEM_READY:I = 0x4

.field public static final XDB_FS_ERR_STORAGE_ENCRYPTED:I = 0x6

.field public static final XDB_FS_FAIL:I = 0x5

.field public static final XDB_FS_OK:I
