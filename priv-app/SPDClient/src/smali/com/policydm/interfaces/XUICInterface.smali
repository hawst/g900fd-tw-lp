.class public interface abstract Lcom/policydm/interfaces/XUICInterface;
.super Ljava/lang/Object;
.source "XUICInterface.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/policydm/interfaces/XUICInterface$XUICFlag;
    }
.end annotation


# static fields
.field public static final XUIC_ECHOTYPE_PASSWORD:I = 0x2

.field public static final XUIC_ECHOTYPE_TEXT:I = 0x1

.field public static final XUIC_INPUTTYPE_ALPHANUMERIC:I = 0x1

.field public static final XUIC_INPUTTYPE_DATE:I = 0x3

.field public static final XUIC_INPUTTYPE_IPADDRESS:I = 0x6

.field public static final XUIC_INPUTTYPE_NUMERIC:I = 0x2

.field public static final XUIC_INPUTTYPE_PHONENUBMER:I = 0x5

.field public static final XUIC_INPUTTYPE_TIME:I = 0x4

.field public static final XUIC_MAX_CHOICE_MENU:I = 0x20

.field public static final XUIC_MAX_TITLE_SIZE:I = 0x80

.field public static final XUIC_MAX_USERINPUT_SIZE:I = 0x80

.field public static final XUIC_PROGRTYPE_END:I = 0x2

.field public static final XUIC_PROGRTYPE_START:I = 0x0

.field public static final XUIC_PROGRTYPE_UPDATA:I = 0x1

.field public static final XUIC_RESULT_MULTI_CHOICE:I = 0x5

.field public static final XUIC_RESULT_NOT_EXCUTED:I = 0x11

.field public static final XUIC_RESULT_OK:I = 0x0

.field public static final XUIC_RESULT_REJECT:I = 0x2

.field public static final XUIC_RESULT_SINGLE_CHOICE:I = 0x4

.field public static final XUIC_RESULT_TIMEOUT:I = 0x10

.field public static final XUIC_RESULT_YES:I = 0x1

.field public static final XUIC_SAVE_NONE:I = 0x0

.field public static final XUIC_SAVE_OK:I = 0x1

.field public static final XUIC_TYPE_CONFIRM:I = 0x2

.field public static final XUIC_TYPE_DISP:I = 0x1

.field public static final XUIC_TYPE_INPUT:I = 0x3

.field public static final XUIC_TYPE_MULTI_CHOICE:I = 0x5

.field public static final XUIC_TYPE_NONE:I = 0x0

.field public static final XUIC_TYPE_PROGR:I = 0x6

.field public static final XUIC_TYPE_SINGLE_CHOICE:I = 0x4
