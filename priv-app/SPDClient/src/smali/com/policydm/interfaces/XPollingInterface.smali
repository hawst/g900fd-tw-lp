.class public interface abstract Lcom/policydm/interfaces/XPollingInterface;
.super Ljava/lang/Object;
.source "XPollingInterface.java"


# static fields
.field public static final XPOLLING_FILE_NAME:Ljava/lang/String; = "version.xml"

.field public static final XPOLLING_MAIN_SERVERURL:Ljava/lang/String; = "http://svc-cf.spd.samsungdm.com/"

.field public static final XPOLLING_PERIODUNIT_DEFAULT:Ljava/lang/String; = "day"

.field public static final XPOLLING_PERIOD_DEFAULT:I = 0x1

.field public static final XPOLLING_RANGE_DEFAULT:I = 0x9

.field public static final XPOLLING_REPORT_PERIOD_DEFAULT:I = 0x1e

.field public static final XPOLLING_SUB_SERVERURL:Ljava/lang/String; = "http://svc-org.spd.samsungdm.com/"

.field public static final XPOLLING_TIME_DEFAULT:I = 0xf
