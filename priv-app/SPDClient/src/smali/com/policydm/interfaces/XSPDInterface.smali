.class public interface abstract Lcom/policydm/interfaces/XSPDInterface;
.super Ljava/lang/Object;
.source "XSPDInterface.java"


# static fields
.field public static final XSPD_AUTH_HEADER_AUTH_ID:Ljava/lang/String; = "auth_identifier="

.field public static final XSPD_AUTH_HEADER_DEVICE_ID:Ljava/lang/String; = "device_id="

.field public static final XSPD_AUTH_HEADER_SERVER_ID:Ljava/lang/String; = "server_id="

.field public static final XSPD_AUTH_HEADER_SIGNATURE:Ljava/lang/String; = "signature="

.field public static final XSPD_ERROR_ACCESS_DENIED:Ljava/lang/String; = "3000"

.field public static final XSPD_ERROR_ACCESS_DENIED_INVALID_VALUE:Ljava/lang/String; = "3001"

.field public static final XSPD_ERROR_ALERT:Ljava/lang/String; = "4000"

.field public static final XSPD_ERROR_ALERT_RECEIVED_WARNING:Ljava/lang/String; = "4001"

.field public static final XSPD_ERROR_EXTERNAL_SYSTEM_ERROR:Ljava/lang/String; = "9000"

.field public static final XSPD_ERROR_INVALID_BODY_XML_UNMARSHAL:Ljava/lang/String; = "1200"

.field public static final XSPD_ERROR_INVALID_HEADER:Ljava/lang/String; = "1100"

.field public static final XSPD_ERROR_INVALID_HEADER_INVALID_VALUE:Ljava/lang/String; = "1102"

.field public static final XSPD_ERROR_INVALID_HEADER_MANDATORY_EMPTY:Ljava/lang/String; = "1101"

.field public static final XSPD_ERROR_INVALID_PARAMETER:Ljava/lang/String; = "1000"

.field public static final XSPD_ERROR_INVALID_PARAMETER_INVALID_VALUE:Ljava/lang/String; = "1002"

.field public static final XSPD_ERROR_INVALID_PARAMETER_MANDATORY_EMPTY:Ljava/lang/String; = "1001"

.field public static final XSPD_ERROR_PERMISSION_DENIED:Ljava/lang/String; = "2000"

.field public static final XSPD_ERROR_PERMISSION_DENIED_NOTFOUND_DEVICE:Ljava/lang/String; = "2001"

.field public static final XSPD_ERROR_PERMISSION_DENIED_NOTFOUND_MODEL:Ljava/lang/String; = "2002"

.field public static final XSPD_ERROR_PERMISSION_DENIED_NOTFOUND_OSVERSION:Ljava/lang/String; = "2004"

.field public static final XSPD_ERROR_PERMISSION_DENIED_NOTFOUND_SALESCODE:Ljava/lang/String; = "2003"

.field public static final XSPD_ERROR_PERMISSION_DENIED_NOTSUPPORT_MODELCC:Ljava/lang/String; = "2011"

.field public static final XSPD_ERROR_UNKNOWN_ERROR:Ljava/lang/String; = "5000"

.field public static final XSPD_EULA_CHECK_TIME:I = 0xa4cb800

.field public static final XSPD_EULA_TYPE_POLLING:I = 0x2

.field public static final XSPD_EULA_TYPE_PULL:I = 0x0

.field public static final XSPD_EULA_TYPE_SETTING:I = 0x1

.field public static final XSPD_GENERIC_BAD_URL:Ljava/lang/String; = "401"

.field public static final XSPD_GENERIC_DOWNLOAD_FAILED_COPY:Ljava/lang/String; = "506"

.field public static final XSPD_GENERIC_DOWNLOAD_FAILED_ENCRYPT:Ljava/lang/String; = "502"

.field public static final XSPD_GENERIC_DOWNLOAD_FAILED_MEMORY_FULL:Ljava/lang/String; = "501"

.field public static final XSPD_GENERIC_DOWNLOAD_FAILED_NETWORK:Ljava/lang/String; = "404"

.field public static final XSPD_GENERIC_DOWNLOAD_FAILED_SIZE_ERROR:Ljava/lang/String; = "507"

.field public static final XSPD_GENERIC_DOWNLOAD_FAILED_UNZIP:Ljava/lang/String; = "503"

.field public static final XSPD_GENERIC_DOWNLOAD_FAILED_VERIFY:Ljava/lang/String; = "505"

.field public static final XSPD_GENERIC_DOWNLOAD_FAILED_VERSION:Ljava/lang/String; = "504"

.field public static final XSPD_GENERIC_SERVER_CONNECT_FAIL:Ljava/lang/String; = "402"

.field public static final XSPD_GENERIC_SERVER_ERROR:Ljava/lang/String; = "403"

.field public static final XSPD_GENERIC_SUCCESSFUL:Ljava/lang/String; = "200"

.field public static final XSPD_GENERIC_UNKNOWN_ERROR:Ljava/lang/String; = "600"

.field public static final XSPD_GET_READ_PATH:Ljava/lang/String; = "/service/policy/config"

.field public static final XSPD_NONE:I = 0x0

.field public static final XSPD_NOTI_JOBID_LENGTH:I = 0x40

.field public static final XSPD_NOTI_OPMODE_CONFIG:I = 0x1

.field public static final XSPD_NOTI_OPMODE_DOWNLOAD:I = 0x0

.field public static final XSPD_OK:I = 0x1

.field public static final XSPD_POLICY_MODE_ENFORCING:Ljava/lang/String; = "enforcing"

.field public static final XSPD_POLICY_MODE_ENTERPRISE:Ljava/lang/String; = "enterprise"

.field public static final XSPD_POLICY_MODE_IDX_ENFORCING:I = 0x0

.field public static final XSPD_POLICY_MODE_IDX_ENTERPRISE:I = 0x2

.field public static final XSPD_POLICY_MODE_IDX_PERMISSIVE:I = 0x1

.field public static final XSPD_POLICY_MODE_PERMISSIVE:Ljava/lang/String; = "permissive"

.field public static final XSPD_POST_CREATE_PATH:Ljava/lang/String; = "/service/policy/device"

.field public static final XSPD_POST_RESULT_PATH:Ljava/lang/String; = "/service/policy/result"

.field public static final XSPD_PUSH_TYPE_GCM:Ljava/lang/String; = "GCM"

.field public static final XSPD_PUSH_TYPE_SPP:Ljava/lang/String; = "SPP"

.field public static final XSPD_PUT_PUSH_PATH:Ljava/lang/String; = "/service/policy/device/push"

.field public static final XSPD_PUT_UPDATE_PATH:Ljava/lang/String; = "/service/policy/device"

.field public static final XSPD_RET_COPY_FAIL:I = -0x4

.field public static final XSPD_RET_FAIL:I = 0x0

.field public static final XSPD_RET_SUCCESS:I = 0x1

.field public static final XSPD_RET_UNZIP_FAIL:I = -0x1

.field public static final XSPD_RET_VALID_FAIL:I = -0x3

.field public static final XSPD_RET_VERSION_FAIL:I = -0x2

.field public static final XSPD_SEAP_DOWNLOADANDUPDATE_PATH:Ljava/lang/String; = "/DownloadAndUpdate"

.field public static final XSPD_SEAP_EXT_PATH:Ljava/lang/String; = "/Ext"

.field public static final XSPD_SEAP_PATH:Ljava/lang/String; = "./SEAndroidPolicy"

.field public static final XSPD_SEAP_PKGDESC_PATH:Ljava/lang/String; = "/PkgDesc"

.field public static final XSPD_SEAP_PKGNAME_PATH:Ljava/lang/String; = "/PkgName"

.field public static final XSPD_SEAP_PKGSIZE_PATH:Ljava/lang/String; = "/PkgSize"

.field public static final XSPD_SEAP_PKGURL_PATH:Ljava/lang/String; = "/PkgURL"

.field public static final XSPD_SEAP_PKGVERSION_PATH:Ljava/lang/String; = "/PkgVersion"

.field public static final XSPD_SEAP_PRECESSID_PATH:Ljava/lang/String; = "/ProcessId"

.field public static final XSPD_SEAP_PUSHJOBID_PATH:Ljava/lang/String; = "/PushJobId"

.field public static final XSPD_SEAP_REPORTURL_PATH:Ljava/lang/String; = "/ReportURL"

.field public static final XSPD_SEAP_STATE_PATH:Ljava/lang/String; = "/State"

.field public static final XSPD_STATE_DEVICE_CREATE:I = 0x1

.field public static final XSPD_STATE_DEVICE_RESULT:I = 0x4

.field public static final XSPD_STATE_DEVICE_UPDATE:I = 0x2

.field public static final XSPD_STATE_GET_DEVICE:I = 0x5

.field public static final XSPD_STATE_GET_PUSHID:I = 0x7

.field public static final XSPD_STATE_GET_VERSION:I = 0x6

.field public static final XSPD_STATE_NONE:I = 0x0

.field public static final XSPD_STATE_PUSH_UPDATE:I = 0x3

.field public static final XSPD_TIME_1_DAY:I = 0x5265c00

.field public static final XSPD_TRY:I = 0x2
