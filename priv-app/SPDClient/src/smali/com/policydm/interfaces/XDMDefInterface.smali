.class public interface abstract Lcom/policydm/interfaces/XDMDefInterface;
.super Ljava/lang/Object;
.source "XDMDefInterface.java"


# static fields
.field public static final XDM_FOTA:Z = true

.field public static final XDM_NONE_PROXY:Z = false

.field public static final XDM_POLLING:Z = true

.field public static final XDM_SIMULATOR_TEST:Z = false

.field public static final XDM_VERSION_V11:Z = false

.field public static final XDM_VERSION_V12:Z = true

.field public static final XDM_VERSION_V12_NONCE_RESYNC:Z
