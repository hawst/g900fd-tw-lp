.class public interface abstract Lcom/policydm/interfaces/XCommonInterface;
.super Ljava/lang/Object;
.source "XCommonInterface.java"


# static fields
.field public static final XCOMMON_ALARM_EULA_TIME:I = 0x2

.field public static final XCOMMON_ALARM_POLLING_TIME:I = 0x0

.field public static final XCOMMON_ALARM_REPORT_TIME:I = 0x1

.field public static final XCOMMON_INTENT_CONNECTIVITY_ACTION:Ljava/lang/String; = "android.net.ConnectivityManager.CONNECTIVITY_ACTION"

.field public static final XCOMMON_INTENT_CONNECTIVITY_CHANGE:Ljava/lang/String; = "android.net.conn.CONNECTIVITY_CHANGE"

.field public static final XCOMMON_INTENT_DELETE_NOTI:Ljava/lang/String; = "com.policydm.intent.action.DELETE_NOTI"

.field public static final XCOMMON_INTENT_EULA_AGREEMENT:Ljava/lang/String; = "com.policydm.intent.action.EULA_AGREEMENT"

.field public static final XCOMMON_INTENT_EULA_TIME:Ljava/lang/String; = "com.policydm.intent.action.EULA_TIME"

.field public static final XCOMMON_INTENT_POLICY_ADMIN:Ljava/lang/String; = "com.policydm.intent.action.POLICY_ADMIN"

.field public static final XCOMMON_INTENT_POLLING_REPORTTIME:Ljava/lang/String; = "com.policydm.intent.action.POLLING_REPORTTIME"

.field public static final XCOMMON_INTENT_POLLING_TIME:Ljava/lang/String; = "com.policydm.intent.action.POLLING_TIME"

.field public static final XCOMMON_INTENT_PULL:Ljava/lang/String; = "com.policydm.intent.action.PULL_RECEIVE"

.field public static final XCOMMON_INTENT_RESUME_EULA_TIME:I = 0x8

.field public static final XCOMMON_INTENT_RESUME_INIT:I = 0x1

.field public static final XCOMMON_INTENT_RESUME_NONE:I = 0x0

.field public static final XCOMMON_INTENT_RESUME_POLLING:I = 0x4

.field public static final XCOMMON_INTENT_RESUME_POLLING_REPORTTIME:I = 0x6

.field public static final XCOMMON_INTENT_RESUME_POLLING_TIME:I = 0x5

.field public static final XCOMMON_INTENT_RESUME_PULL:I = 0x2

.field public static final XCOMMON_INTENT_RESUME_PUSH:I = 0x3

.field public static final XCOMMON_INTENT_RESUME_REGISTER:I = 0x7

.field public static final XCOMMON_INTENT_SETUPWIZARD_COMPLETE:Ljava/lang/String; = "com.sec.android.app.secsetupwizard.SETUPWIZARD_COMPLETE"

.field public static final XCOMMON_INTENT_SPD_REGISTER:Ljava/lang/String; = "com.policydm.intent.action.SPD_REGISTER"

.field public static final XCOMMON_NETWORK_DATA:I = 0x1

.field public static final XCOMMON_NETWORK_NONE:I = 0x0

.field public static final XCOMMON_NETWORK_WIFI:I = 0x2
