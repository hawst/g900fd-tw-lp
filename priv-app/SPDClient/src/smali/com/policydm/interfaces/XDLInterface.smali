.class public interface abstract Lcom/policydm/interfaces/XDLInterface;
.super Ljava/lang/Object;
.source "XDLInterface.java"


# static fields
.field public static final XDL_CHECK_DOWNLOAD_AFTER:I = 0x2

.field public static final XDL_CHECK_DOWNLOAD_BEFORE:I = 0x1

.field public static final XDL_DOWNLOADANDUPDATE_PATH:Ljava/lang/String; = "/DownloadAndUpdate"

.field public static final XDL_DOWNLOAD_PATH:Ljava/lang/String; = "/Download"

.field public static final XDL_EXT:Ljava/lang/String; = "/Ext"

.field public static final XDL_GENERIC_AUTHENTICATION_FAILURE:Ljava/lang/String; = "406"

.field public static final XDL_GENERIC_BAD_URL:Ljava/lang/String; = "411"

.field public static final XDL_GENERIC_CLIENT_ERROR:Ljava/lang/String; = "400"

.field public static final XDL_GENERIC_CORRUPTED_FW_UP:Ljava/lang/String; = "402"

.field public static final XDL_GENERIC_DOWNLOAD_FAILED_NETWORK:Ljava/lang/String; = "503"

.field public static final XDL_GENERIC_DOWNLOAD_FAILED_OUT_MEMORY:Ljava/lang/String; = "501"

.field public static final XDL_GENERIC_FAILED_FW_UP_VALIDATION:Ljava/lang/String; = "404"

.field public static final XDL_GENERIC_NOT_ACCEPTABLE:Ljava/lang/String; = "405"

.field public static final XDL_GENERIC_NOT_IMPLEMENTED:Ljava/lang/String; = "408"

.field public static final XDL_GENERIC_PACKAGE_MISMATCH:Ljava/lang/String; = "403"

.field public static final XDL_GENERIC_REQUEST_TIME_OUT:Ljava/lang/String; = "407"

.field public static final XDL_GENERIC_SERVER_ERROR:Ljava/lang/String; = "500"

.field public static final XDL_GENERIC_SERVER_UNAVAILABLE:Ljava/lang/String; = "412"

.field public static final XDL_GENERIC_SUCCESSFUL:Ljava/lang/String; = "200"

.field public static final XDL_GENERIC_SUCCESSFUL_VENDOR_SPECIFIED:Ljava/lang/String; = "250"

.field public static final XDL_GENERIC_UNDEFINED_ERROR:Ljava/lang/String; = "409"

.field public static final XDL_GENERIC_UPDATE_FAILED:Ljava/lang/String; = "410"

.field public static final XDL_GENERIC_UPDATE_FAILED_OUT_MEMORY:Ljava/lang/String; = "502"

.field public static final XDL_GENERIC_USER_CANCELED_DOWNLOAD:Ljava/lang/String; = "401"

.field public static final XDL_MEMORY_ENCRYPTED:I = 0x2

.field public static final XDL_MEMORY_INSUFFICIENT:I = 0x1

.field public static final XDL_PATH:Ljava/lang/String; = "./FUMO"

.field public static final XDL_PKGDATA_PATH:Ljava/lang/String; = "/PkgData"

.field public static final XDL_PKGNAME_PATH:Ljava/lang/String; = "/PkgName"

.field public static final XDL_PKGURL_PATH:Ljava/lang/String; = "/PkgURL"

.field public static final XDL_PKGVERSION_PATH:Ljava/lang/String; = "/PkgVersion"

.field public static final XDL_RET_CONTINUE:I = 0x1

.field public static final XDL_RET_FAILED:I = -0x1

.field public static final XDL_RET_OK:I = 0x0

.field public static final XDL_SPACE_FREE_SIZE_5M:I = 0x500000

.field public static final XDL_STATE_DOWNLOAD_COMPLETE:I = 0x28

.field public static final XDL_STATE_DOWNLOAD_DESCRIPTOR:I = 0xc8

.field public static final XDL_STATE_DOWNLOAD_FAILED:I = 0x14

.field public static final XDL_STATE_DOWNLOAD_FAILED_REPORTING:I = 0xf1

.field public static final XDL_STATE_DOWNLOAD_IN_CANCEL:I = 0xe6

.field public static final XDL_STATE_DOWNLOAD_IN_PROGRESS:I = 0x1e

.field public static final XDL_STATE_IDLE_START:I = 0xa

.field public static final XDL_STATE_NONE:I = 0x0

.field public static final XDL_STATE_PATH:Ljava/lang/String; = "/State"

.field public static final XDL_STATE_POSTPONE_TO_DOWNLOAD:I = 0xdd

.field public static final XDL_STATE_POSTPONE_TO_UPDATE:I = 0xdc

.field public static final XDL_STATE_READY_TO_UPDATE:I = 0x32

.field public static final XDL_STATE_START_TO_UPDATE:I = 0xd2

.field public static final XDL_STATE_UPDATE_FAILED_HAVEDATA:I = 0x46

.field public static final XDL_STATE_UPDATE_FAILED_NODATA:I = 0x50

.field public static final XDL_STATE_UPDATE_IN_PROGRESS:I = 0x3c

.field public static final XDL_STATE_UPDATE_SUCCESSFUL_HAVEDATA:I = 0x5a

.field public static final XDL_STATE_UPDATE_SUCCESSFUL_NODATA:I = 0x64

.field public static final XDL_STATE_USER_CANCEL_REPORTING:I = 0xf0

.field public static final XDL_UPDATE_PATH:Ljava/lang/String; = "/Update"
