.class public Lcom/policydm/eng/parser/XDMParserGet;
.super Lcom/policydm/agent/XDMHandleCmd;
.source "XDMParserGet.java"

# interfaces
.implements Lcom/policydm/eng/core/XDMWbxml;


# instance fields
.field public cmdid:I

.field public cred:Lcom/policydm/eng/parser/XDMParserCred;

.field public is_noresp:I

.field public itemlist:Lcom/policydm/eng/core/XDMList;

.field public lang:I

.field public meta:Lcom/policydm/eng/parser/XDMParserMeta;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/policydm/agent/XDMHandleCmd;-><init>()V

    .line 17
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/policydm/eng/parser/XDMParserGet;->itemlist:Lcom/policydm/eng/core/XDMList;

    .line 10
    return-void
.end method


# virtual methods
.method public xdmParParseGet(Lcom/policydm/eng/parser/XDMParser;)I
    .locals 5
    .param p1, "p"    # Lcom/policydm/eng/parser/XDMParser;

    .prologue
    const/4 v3, 0x0

    .line 26
    const/4 v1, -0x1

    .line 27
    .local v1, "id":I
    const/4 v2, 0x0

    .line 29
    .local v2, "res":I
    const/16 v4, 0x13

    invoke-virtual {p1, v4}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseCheckElement(I)I

    move-result v2

    .line 30
    if-eqz v2, :cond_1

    move v3, v2

    .line 119
    :cond_0
    :goto_0
    return v3

    .line 35
    :cond_1
    invoke-virtual {p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseZeroBitTagCheck()I

    move-result v2

    .line 36
    const/16 v4, 0x8

    if-eq v2, v4, :cond_0

    .line 40
    if-eqz v2, :cond_2

    .line 42
    const-string v3, "not WBXML_ERR_OK"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move v3, v2

    .line 43
    goto :goto_0

    .line 50
    :cond_2
    :try_start_0
    invoke-virtual {p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseCurrentElement()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 57
    :goto_1
    const/4 v4, 0x1

    if-ne v1, v4, :cond_3

    .line 59
    invoke-virtual {p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseReadElement()I

    move-result v1

    .line 118
    iget-object v4, p1, Lcom/policydm/eng/parser/XDMParser;->userdata:Ljava/lang/Object;

    invoke-virtual {p0, v4, p0}, Lcom/policydm/eng/parser/XDMParserGet;->xdmAgentHdlCmdGet(Ljava/lang/Object;Lcom/policydm/eng/parser/XDMParserGet;)V

    goto :goto_0

    .line 52
    :catch_0
    move-exception v0

    .line 54
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 63
    .end local v0    # "e":Ljava/io/IOException;
    :cond_3
    sparse-switch v1, :sswitch_data_0

    .line 109
    const/4 v2, 0x2

    .line 112
    :goto_2
    if-eqz v2, :cond_2

    move v3, v2

    .line 114
    goto :goto_0

    .line 66
    :sswitch_0
    invoke-virtual {p1, v1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseElement(I)I

    move-result v2

    .line 67
    iget-object v4, p1, Lcom/policydm/eng/parser/XDMParser;->m_szParserElement:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/policydm/eng/parser/XDMParserGet;->cmdid:I

    goto :goto_2

    .line 71
    :sswitch_1
    invoke-virtual {p1, v1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseBlankElement(I)I

    move-result v4

    iput v4, p0, Lcom/policydm/eng/parser/XDMParserGet;->is_noresp:I

    goto :goto_2

    .line 75
    :sswitch_2
    invoke-virtual {p1, v1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseElement(I)I

    move-result v2

    .line 76
    iget-object v4, p1, Lcom/policydm/eng/parser/XDMParser;->m_szParserElement:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/policydm/eng/parser/XDMParserGet;->lang:I

    goto :goto_2

    .line 80
    :sswitch_3
    new-instance v4, Lcom/policydm/eng/parser/XDMParserCred;

    invoke-direct {v4}, Lcom/policydm/eng/parser/XDMParserCred;-><init>()V

    iput-object v4, p0, Lcom/policydm/eng/parser/XDMParserGet;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    .line 81
    iget-object v4, p0, Lcom/policydm/eng/parser/XDMParserGet;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    invoke-virtual {v4, p1}, Lcom/policydm/eng/parser/XDMParserCred;->xdmParParseCred(Lcom/policydm/eng/parser/XDMParser;)I

    move-result v2

    .line 82
    iget-object v4, p1, Lcom/policydm/eng/parser/XDMParser;->Cred:Lcom/policydm/eng/parser/XDMParserCred;

    iput-object v4, p0, Lcom/policydm/eng/parser/XDMParserGet;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    goto :goto_2

    .line 86
    :sswitch_4
    new-instance v4, Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-direct {v4}, Lcom/policydm/eng/parser/XDMParserMeta;-><init>()V

    iput-object v4, p0, Lcom/policydm/eng/parser/XDMParserGet;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    .line 87
    iget-object v4, p0, Lcom/policydm/eng/parser/XDMParserGet;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-virtual {v4, p1}, Lcom/policydm/eng/parser/XDMParserMeta;->xdmParParseMeta(Lcom/policydm/eng/parser/XDMParser;)I

    move-result v2

    .line 88
    iget-object v4, p1, Lcom/policydm/eng/parser/XDMParser;->Meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iput-object v4, p0, Lcom/policydm/eng/parser/XDMParserGet;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    goto :goto_2

    .line 92
    :sswitch_5
    iget-object v4, p0, Lcom/policydm/eng/parser/XDMParserGet;->itemlist:Lcom/policydm/eng/core/XDMList;

    invoke-virtual {p1, v4}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseItemlist(Lcom/policydm/eng/core/XDMList;)Lcom/policydm/eng/core/XDMList;

    move-result-object v4

    iput-object v4, p0, Lcom/policydm/eng/parser/XDMParserGet;->itemlist:Lcom/policydm/eng/core/XDMList;

    goto :goto_2

    .line 96
    :sswitch_6
    invoke-virtual {p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseReadElement()I

    move-result v1

    .line 97
    invoke-virtual {p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseReadElement()I

    move-result v1

    .line 99
    if-eqz v2, :cond_4

    move v3, v2

    .line 101
    goto/16 :goto_0

    .line 104
    :cond_4
    iput v1, p1, Lcom/policydm/eng/parser/XDMParser;->codePage:I

    goto :goto_2

    .line 63
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_6
        0xb -> :sswitch_0
        0xe -> :sswitch_3
        0x14 -> :sswitch_5
        0x15 -> :sswitch_2
        0x1a -> :sswitch_4
        0x1d -> :sswitch_1
    .end sparse-switch
.end method
