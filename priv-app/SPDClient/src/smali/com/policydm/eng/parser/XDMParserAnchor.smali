.class public Lcom/policydm/eng/parser/XDMParserAnchor;
.super Ljava/lang/Object;
.source "XDMParserAnchor.java"

# interfaces
.implements Lcom/policydm/eng/core/XDMWbxml;


# instance fields
.field public m_szLast:Ljava/lang/String;

.field public m_szNext:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public xdmParGetMetinfAnchor()Lcom/policydm/eng/parser/XDMParserAnchor;
    .locals 2

    .prologue
    .line 94
    new-instance v0, Lcom/policydm/eng/parser/XDMParserAnchor;

    invoke-direct {v0}, Lcom/policydm/eng/parser/XDMParserAnchor;-><init>()V

    .line 96
    .local v0, "anchor":Lcom/policydm/eng/parser/XDMParserAnchor;
    iget-object v1, p0, Lcom/policydm/eng/parser/XDMParserAnchor;->m_szLast:Ljava/lang/String;

    iput-object v1, v0, Lcom/policydm/eng/parser/XDMParserAnchor;->m_szLast:Ljava/lang/String;

    .line 97
    iget-object v1, p0, Lcom/policydm/eng/parser/XDMParserAnchor;->m_szNext:Ljava/lang/String;

    iput-object v1, v0, Lcom/policydm/eng/parser/XDMParserAnchor;->m_szNext:Ljava/lang/String;

    .line 99
    return-object v0
.end method

.method public xdmParParseAnchor(Lcom/policydm/eng/parser/XDMParser;)I
    .locals 4
    .param p1, "p"    # Lcom/policydm/eng/parser/XDMParser;

    .prologue
    .line 20
    const/4 v1, -0x1

    .line 21
    .local v1, "id":I
    const/4 v2, 0x0

    .line 22
    .local v2, "res":I
    const-string v3, "xdmParParseAnchor"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 24
    const/4 v3, 0x5

    invoke-virtual {p1, v3}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseCheckElement(I)I

    move-result v2

    .line 25
    if-eqz v2, :cond_0

    move v3, v2

    .line 85
    :goto_0
    return v3

    .line 30
    :cond_0
    invoke-virtual {p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseZeroBitTagCheck()I

    move-result v2

    .line 31
    const/16 v3, 0x8

    if-ne v2, v3, :cond_1

    .line 33
    const/4 v3, 0x0

    goto :goto_0

    .line 35
    :cond_1
    if-eqz v2, :cond_2

    .line 37
    const-string v3, "not WBXML_ERR_OK"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move v3, v2

    .line 38
    goto :goto_0

    .line 41
    :cond_2
    invoke-virtual {p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseSkipLiteralElement()I

    move-result v2

    .line 42
    if-eqz v2, :cond_3

    move v3, v2

    .line 44
    goto :goto_0

    .line 51
    :cond_3
    :try_start_0
    invoke-virtual {p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseCurrentElement()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 58
    :goto_1
    const/4 v3, 0x1

    if-ne v1, v3, :cond_4

    .line 60
    invoke-virtual {p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseReadElement()I

    move-result v1

    move v3, v2

    .line 85
    goto :goto_0

    .line 53
    :catch_0
    move-exception v0

    .line 55
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 64
    .end local v0    # "e":Ljava/io/IOException;
    :cond_4
    sparse-switch v1, :sswitch_data_0

    .line 77
    const/4 v2, 0x2

    .line 80
    :goto_2
    if-eqz v2, :cond_3

    move v3, v2

    .line 82
    goto :goto_0

    .line 67
    :sswitch_0
    invoke-virtual {p1, v1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseElement(I)I

    move-result v2

    .line 68
    iget-object v3, p1, Lcom/policydm/eng/parser/XDMParser;->m_szParserElement:Ljava/lang/String;

    iput-object v3, p0, Lcom/policydm/eng/parser/XDMParserAnchor;->m_szLast:Ljava/lang/String;

    goto :goto_2

    .line 72
    :sswitch_1
    invoke-virtual {p1, v1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseElement(I)I

    move-result v2

    .line 73
    iget-object v3, p1, Lcom/policydm/eng/parser/XDMParser;->m_szParserElement:Ljava/lang/String;

    iput-object v3, p0, Lcom/policydm/eng/parser/XDMParserAnchor;->m_szNext:Ljava/lang/String;

    goto :goto_2

    .line 64
    nop

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0xf -> :sswitch_1
    .end sparse-switch
.end method
