.class public Lcom/policydm/eng/parser/XDMParserMeta;
.super Ljava/lang/Object;
.source "XDMParserMeta.java"

# interfaces
.implements Lcom/policydm/eng/core/XDMWbxml;


# instance fields
.field public anchor:Lcom/policydm/eng/parser/XDMParserAnchor;

.field public m_szEmi:Ljava/lang/String;

.field public m_szFormat:Ljava/lang/String;

.field public m_szMark:Ljava/lang/String;

.field public m_szNextNonce:Ljava/lang/String;

.field public m_szType:Ljava/lang/String;

.field public m_szVersion:Ljava/lang/String;

.field public maxmsgsize:I

.field public maxobjsize:I

.field public mem:Lcom/policydm/eng/parser/XDMParserMem;

.field public size:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public xdmParParseMeta(Lcom/policydm/eng/parser/XDMParser;)I
    .locals 6
    .param p1, "p"    # Lcom/policydm/eng/parser/XDMParser;

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x1

    .line 29
    const/4 v1, -0x1

    .line 30
    .local v1, "id":I
    const/4 v2, 0x0

    .line 32
    .local v2, "res":I
    const/16 v4, 0x1a

    invoke-virtual {p1, v4}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseCheckElement(I)I

    move-result v2

    .line 33
    if-eqz v2, :cond_1

    move v3, v2

    .line 160
    :cond_0
    :goto_0
    return v3

    .line 38
    :cond_1
    invoke-virtual {p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseZeroBitTagCheck()I

    move-result v2

    .line 39
    const/16 v4, 0x8

    if-eq v2, v4, :cond_0

    .line 43
    if-eqz v2, :cond_2

    move v3, v2

    .line 45
    goto :goto_0

    .line 50
    :cond_2
    :try_start_0
    invoke-virtual {p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseCurrentElement()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 57
    :goto_1
    if-ne v1, v5, :cond_3

    .line 59
    invoke-virtual {p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseReadElement()I

    move-result v1

    move v3, v2

    .line 60
    goto :goto_0

    .line 52
    :catch_0
    move-exception v0

    .line 54
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 64
    .end local v0    # "e":Ljava/io/IOException;
    :cond_3
    invoke-virtual {p1, v3}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseCheckElement(I)I

    move-result v2

    .line 65
    if-eqz v2, :cond_4

    move v3, v2

    .line 67
    goto :goto_0

    .line 70
    :cond_4
    invoke-virtual {p1, v5}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseCheckElement(I)I

    move-result v2

    .line 71
    if-eqz v2, :cond_5

    move v3, v2

    .line 73
    goto :goto_0

    .line 76
    :cond_5
    iput v5, p1, Lcom/policydm/eng/parser/XDMParser;->codePage:I

    .line 82
    :cond_6
    :try_start_1
    invoke-virtual {p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseCurrentElement()I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v1

    .line 89
    :goto_2
    if-ne v1, v5, :cond_7

    .line 91
    invoke-virtual {p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseReadElement()I

    move-result v1

    .line 159
    iput-object p0, p1, Lcom/policydm/eng/parser/XDMParser;->Meta:Lcom/policydm/eng/parser/XDMParserMeta;

    move v3, v2

    .line 160
    goto :goto_0

    .line 84
    :catch_1
    move-exception v0

    .line 86
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_2

    .line 96
    .end local v0    # "e":Ljava/io/IOException;
    :cond_7
    packed-switch v1, :pswitch_data_0

    .line 150
    :pswitch_0
    const/4 v2, 0x2

    .line 153
    :goto_3
    if-eqz v2, :cond_6

    move v3, v2

    .line 155
    goto :goto_0

    .line 99
    :pswitch_1
    invoke-virtual {p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseReadElement()I

    move-result v1

    .line 100
    invoke-virtual {p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseReadElement()I

    move-result v1

    .line 102
    iput v1, p1, Lcom/policydm/eng/parser/XDMParser;->codePage:I

    goto :goto_3

    .line 106
    :pswitch_2
    invoke-virtual {p1, v1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseElement(I)I

    move-result v2

    .line 107
    iget-object v3, p1, Lcom/policydm/eng/parser/XDMParser;->m_szParserElement:Ljava/lang/String;

    iput-object v3, p0, Lcom/policydm/eng/parser/XDMParserMeta;->m_szType:Ljava/lang/String;

    goto :goto_3

    .line 110
    :pswitch_3
    invoke-virtual {p1, v1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseElement(I)I

    move-result v2

    .line 111
    iget-object v3, p1, Lcom/policydm/eng/parser/XDMParser;->m_szParserElement:Ljava/lang/String;

    iput-object v3, p0, Lcom/policydm/eng/parser/XDMParserMeta;->m_szFormat:Ljava/lang/String;

    goto :goto_3

    .line 114
    :pswitch_4
    invoke-virtual {p1, v1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseElement(I)I

    move-result v2

    .line 115
    iget-object v3, p1, Lcom/policydm/eng/parser/XDMParser;->m_szParserElement:Ljava/lang/String;

    iput-object v3, p0, Lcom/policydm/eng/parser/XDMParserMeta;->m_szMark:Ljava/lang/String;

    goto :goto_3

    .line 118
    :pswitch_5
    invoke-virtual {p1, v1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseElement(I)I

    move-result v2

    .line 119
    iget-object v3, p1, Lcom/policydm/eng/parser/XDMParser;->m_szParserElement:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iput v3, p0, Lcom/policydm/eng/parser/XDMParserMeta;->size:I

    goto :goto_3

    .line 122
    :pswitch_6
    invoke-virtual {p1, v1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseElement(I)I

    move-result v2

    .line 123
    iget-object v3, p1, Lcom/policydm/eng/parser/XDMParser;->m_szParserElement:Ljava/lang/String;

    iput-object v3, p0, Lcom/policydm/eng/parser/XDMParserMeta;->m_szNextNonce:Ljava/lang/String;

    goto :goto_3

    .line 126
    :pswitch_7
    invoke-virtual {p1, v1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseElement(I)I

    move-result v2

    .line 127
    iget-object v3, p1, Lcom/policydm/eng/parser/XDMParser;->m_szParserElement:Ljava/lang/String;

    iput-object v3, p0, Lcom/policydm/eng/parser/XDMParserMeta;->m_szVersion:Ljava/lang/String;

    goto :goto_3

    .line 130
    :pswitch_8
    invoke-virtual {p1, v1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseElement(I)I

    move-result v2

    .line 131
    iget-object v3, p1, Lcom/policydm/eng/parser/XDMParser;->m_szParserElement:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/policydm/eng/parser/XDMParserMeta;->maxmsgsize:I

    goto :goto_3

    .line 134
    :pswitch_9
    invoke-virtual {p1, v1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseElement(I)I

    move-result v2

    .line 135
    iget-object v3, p1, Lcom/policydm/eng/parser/XDMParser;->m_szParserElement:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/policydm/eng/parser/XDMParserMeta;->maxobjsize:I

    goto :goto_3

    .line 138
    :pswitch_a
    invoke-virtual {p1, v1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseElement(I)I

    move-result v2

    .line 139
    iget-object v3, p1, Lcom/policydm/eng/parser/XDMParser;->m_szParserElement:Ljava/lang/String;

    iput-object v3, p0, Lcom/policydm/eng/parser/XDMParserMeta;->m_szEmi:Ljava/lang/String;

    goto :goto_3

    .line 142
    :pswitch_b
    new-instance v3, Lcom/policydm/eng/parser/XDMParserMem;

    invoke-direct {v3}, Lcom/policydm/eng/parser/XDMParserMem;-><init>()V

    iput-object v3, p0, Lcom/policydm/eng/parser/XDMParserMeta;->mem:Lcom/policydm/eng/parser/XDMParserMem;

    .line 143
    iget-object v3, p0, Lcom/policydm/eng/parser/XDMParserMeta;->mem:Lcom/policydm/eng/parser/XDMParserMem;

    invoke-virtual {v3, p1}, Lcom/policydm/eng/parser/XDMParserMem;->xdmParParseMem(Lcom/policydm/eng/parser/XDMParser;)I

    goto :goto_3

    .line 146
    :pswitch_c
    new-instance v3, Lcom/policydm/eng/parser/XDMParserAnchor;

    invoke-direct {v3}, Lcom/policydm/eng/parser/XDMParserAnchor;-><init>()V

    iput-object v3, p0, Lcom/policydm/eng/parser/XDMParserMeta;->anchor:Lcom/policydm/eng/parser/XDMParserAnchor;

    .line 147
    iget-object v3, p0, Lcom/policydm/eng/parser/XDMParserMeta;->anchor:Lcom/policydm/eng/parser/XDMParserAnchor;

    invoke-virtual {v3, p1}, Lcom/policydm/eng/parser/XDMParserAnchor;->xdmParParseAnchor(Lcom/policydm/eng/parser/XDMParser;)I

    goto/16 :goto_3

    .line 96
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_c
        :pswitch_a
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_8
        :pswitch_b
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_5
        :pswitch_2
        :pswitch_7
        :pswitch_9
    .end packed-switch
.end method
