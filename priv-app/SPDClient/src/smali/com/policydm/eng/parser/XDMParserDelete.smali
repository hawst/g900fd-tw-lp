.class public Lcom/policydm/eng/parser/XDMParserDelete;
.super Lcom/policydm/agent/XDMHandleCmd;
.source "XDMParserDelete.java"

# interfaces
.implements Lcom/policydm/eng/core/XDMWbxml;


# instance fields
.field public cmdid:I

.field public cred:Lcom/policydm/eng/parser/XDMParserCred;

.field public is_archive:I

.field public is_noresp:I

.field public is_sftdel:I

.field public itemlist:Lcom/policydm/eng/core/XDMList;

.field public meta:Lcom/policydm/eng/parser/XDMParserMeta;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/policydm/agent/XDMHandleCmd;-><init>()V

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/policydm/eng/parser/XDMParserDelete;->itemlist:Lcom/policydm/eng/core/XDMList;

    .line 10
    return-void
.end method


# virtual methods
.method public xdmParParseDelete(Lcom/policydm/eng/parser/XDMParser;)I
    .locals 5
    .param p1, "p"    # Lcom/policydm/eng/parser/XDMParser;

    .prologue
    const/4 v3, 0x0

    .line 27
    const/4 v1, -0x1

    .line 28
    .local v1, "id":I
    const/4 v2, 0x0

    .line 30
    .local v2, "res":I
    const/16 v4, 0x10

    invoke-virtual {p1, v4}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseCheckElement(I)I

    move-result v2

    .line 31
    if-eqz v2, :cond_1

    move v3, v2

    .line 118
    :cond_0
    :goto_0
    return v3

    .line 36
    :cond_1
    invoke-virtual {p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseZeroBitTagCheck()I

    move-result v2

    .line 37
    const/16 v4, 0x8

    if-eq v2, v4, :cond_0

    .line 41
    if-eqz v2, :cond_2

    .line 43
    const-string v3, "not WBXML_ERR_OK"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move v3, v2

    .line 44
    goto :goto_0

    .line 51
    :cond_2
    :try_start_0
    invoke-virtual {p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseCurrentElement()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 58
    :goto_1
    const/4 v4, 0x1

    if-ne v1, v4, :cond_3

    .line 60
    invoke-virtual {p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseReadElement()I

    move-result v1

    .line 117
    iget-object v4, p1, Lcom/policydm/eng/parser/XDMParser;->userdata:Ljava/lang/Object;

    invoke-virtual {p0, v4, p0}, Lcom/policydm/eng/parser/XDMParserDelete;->xdmAgentHdlCmdDelete(Ljava/lang/Object;Lcom/policydm/eng/parser/XDMParserDelete;)V

    goto :goto_0

    .line 53
    :catch_0
    move-exception v0

    .line 55
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 64
    .end local v0    # "e":Ljava/io/IOException;
    :cond_3
    sparse-switch v1, :sswitch_data_0

    .line 107
    const/4 v2, 0x2

    .line 111
    :goto_2
    if-eqz v2, :cond_2

    move v3, v2

    .line 113
    goto :goto_0

    .line 67
    :sswitch_0
    invoke-virtual {p1, v1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseElement(I)I

    move-result v2

    .line 68
    iget-object v4, p1, Lcom/policydm/eng/parser/XDMParser;->m_szParserElement:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/policydm/eng/parser/XDMParserDelete;->cmdid:I

    goto :goto_2

    .line 72
    :sswitch_1
    new-instance v4, Lcom/policydm/eng/parser/XDMParserCred;

    invoke-direct {v4}, Lcom/policydm/eng/parser/XDMParserCred;-><init>()V

    iput-object v4, p0, Lcom/policydm/eng/parser/XDMParserDelete;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    .line 73
    iget-object v4, p0, Lcom/policydm/eng/parser/XDMParserDelete;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    invoke-virtual {v4, p1}, Lcom/policydm/eng/parser/XDMParserCred;->xdmParParseCred(Lcom/policydm/eng/parser/XDMParser;)I

    move-result v2

    .line 74
    iget-object v4, p1, Lcom/policydm/eng/parser/XDMParser;->Cred:Lcom/policydm/eng/parser/XDMParserCred;

    iput-object v4, p0, Lcom/policydm/eng/parser/XDMParserDelete;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    goto :goto_2

    .line 78
    :sswitch_2
    new-instance v4, Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-direct {v4}, Lcom/policydm/eng/parser/XDMParserMeta;-><init>()V

    iput-object v4, p0, Lcom/policydm/eng/parser/XDMParserDelete;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    .line 79
    iget-object v4, p0, Lcom/policydm/eng/parser/XDMParserDelete;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-virtual {v4, p1}, Lcom/policydm/eng/parser/XDMParserMeta;->xdmParParseMeta(Lcom/policydm/eng/parser/XDMParser;)I

    move-result v2

    .line 80
    iget-object v4, p1, Lcom/policydm/eng/parser/XDMParser;->Meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iput-object v4, p0, Lcom/policydm/eng/parser/XDMParserDelete;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    goto :goto_2

    .line 84
    :sswitch_3
    iget-object v4, p0, Lcom/policydm/eng/parser/XDMParserDelete;->itemlist:Lcom/policydm/eng/core/XDMList;

    invoke-virtual {p1, v4}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseItemlist(Lcom/policydm/eng/core/XDMList;)Lcom/policydm/eng/core/XDMList;

    move-result-object v4

    iput-object v4, p0, Lcom/policydm/eng/parser/XDMParserDelete;->itemlist:Lcom/policydm/eng/core/XDMList;

    goto :goto_2

    .line 88
    :sswitch_4
    invoke-virtual {p1, v1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseBlankElement(I)I

    move-result v4

    iput v4, p0, Lcom/policydm/eng/parser/XDMParserDelete;->is_noresp:I

    goto :goto_2

    .line 92
    :sswitch_5
    invoke-virtual {p1, v1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseBlankElement(I)I

    move-result v4

    iput v4, p0, Lcom/policydm/eng/parser/XDMParserDelete;->is_archive:I

    goto :goto_2

    .line 96
    :sswitch_6
    invoke-virtual {p1, v1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseBlankElement(I)I

    move-result v4

    iput v4, p0, Lcom/policydm/eng/parser/XDMParserDelete;->is_sftdel:I

    goto :goto_2

    .line 100
    :sswitch_7
    invoke-virtual {p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseReadElement()I

    move-result v1

    .line 101
    invoke-virtual {p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseReadElement()I

    move-result v1

    .line 103
    iput v1, p1, Lcom/policydm/eng/parser/XDMParser;->codePage:I

    goto :goto_2

    .line 64
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_7
        0x7 -> :sswitch_5
        0xb -> :sswitch_0
        0xe -> :sswitch_1
        0x14 -> :sswitch_3
        0x1a -> :sswitch_2
        0x1d -> :sswitch_4
        0x26 -> :sswitch_6
    .end sparse-switch
.end method
