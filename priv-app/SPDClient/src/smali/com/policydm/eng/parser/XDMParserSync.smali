.class public Lcom/policydm/eng/parser/XDMParserSync;
.super Ljava/lang/Object;
.source "XDMParserSync.java"

# interfaces
.implements Lcom/policydm/eng/core/XDMWbxml;


# instance fields
.field public cmdid:I

.field public cred:Lcom/policydm/eng/parser/XDMParserCred;

.field public is_noresp:Z

.field public is_noresults:Z

.field public m_szSource:Ljava/lang/String;

.field public m_szTarget:Ljava/lang/String;

.field public meta:Lcom/policydm/eng/parser/XDMParserMeta;

.field public numofchanges:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public xdmParParseSync(Lcom/policydm/eng/parser/XDMParser;)I
    .locals 13
    .param p1, "p"    # Lcom/policydm/eng/parser/XDMParser;

    .prologue
    .line 26
    const/4 v6, -0x1

    .line 27
    .local v6, "id":I
    const/4 v10, 0x0

    .line 28
    .local v10, "res":I
    const/4 v2, 0x1

    .line 30
    .local v2, "call_start_sync":Z
    const-string v12, "xdmParParseSync"

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 32
    const/16 v12, 0x2a

    invoke-virtual {p1, v12}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseCheckElement(I)I

    move-result v10

    .line 33
    if-eqz v10, :cond_0

    move v12, v10

    .line 193
    :goto_0
    return v12

    .line 38
    :cond_0
    invoke-virtual {p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseZeroBitTagCheck()I

    move-result v10

    .line 39
    const/16 v12, 0x8

    if-ne v10, v12, :cond_1

    .line 41
    const/4 v12, 0x0

    goto :goto_0

    .line 43
    :cond_1
    if-eqz v10, :cond_2

    .line 45
    const-string v12, "not WBXML_ERR_OK"

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move v12, v10

    .line 46
    goto :goto_0

    .line 53
    :cond_2
    :try_start_0
    invoke-virtual {p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseCurrentElement()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    .line 60
    :goto_1
    const/4 v12, 0x1

    if-ne v6, v12, :cond_4

    .line 62
    invoke-virtual {p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseReadElement()I

    move-result v6

    .line 188
    if-eqz v2, :cond_3

    .line 190
    const/4 v2, 0x0

    .line 193
    :cond_3
    const/4 v12, 0x0

    goto :goto_0

    .line 55
    :catch_0
    move-exception v5

    .line 57
    .local v5, "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 66
    .end local v5    # "e":Ljava/io/IOException;
    :cond_4
    sparse-switch v6, :sswitch_data_0

    .line 179
    const/4 v10, 0x2

    .line 182
    :goto_2
    if-eqz v10, :cond_2

    move v12, v10

    .line 184
    goto :goto_0

    .line 69
    :sswitch_0
    invoke-virtual {p1, v6}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseElement(I)I

    move-result v10

    .line 70
    iget-object v12, p1, Lcom/policydm/eng/parser/XDMParser;->m_szParserElement:Ljava/lang/String;

    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    iput v12, p0, Lcom/policydm/eng/parser/XDMParserSync;->cmdid:I

    goto :goto_2

    .line 74
    :sswitch_1
    invoke-virtual {p1, v6}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseBlankElement(I)I

    move-result v7

    .line 75
    .local v7, "noresp":I
    const/4 v12, 0x1

    if-ne v7, v12, :cond_5

    .line 76
    const/4 v12, 0x1

    iput-boolean v12, p0, Lcom/policydm/eng/parser/XDMParserSync;->is_noresp:Z

    goto :goto_2

    .line 78
    :cond_5
    const/4 v12, 0x0

    iput-boolean v12, p0, Lcom/policydm/eng/parser/XDMParserSync;->is_noresp:Z

    goto :goto_2

    .line 82
    .end local v7    # "noresp":I
    :sswitch_2
    invoke-virtual {p1, v6}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseBlankElement(I)I

    move-result v8

    .line 83
    .local v8, "noresults":I
    const/4 v12, 0x1

    if-ne v8, v12, :cond_6

    .line 84
    const/4 v12, 0x1

    iput-boolean v12, p0, Lcom/policydm/eng/parser/XDMParserSync;->is_noresults:Z

    goto :goto_2

    .line 86
    :cond_6
    const/4 v12, 0x0

    iput-boolean v12, p0, Lcom/policydm/eng/parser/XDMParserSync;->is_noresults:Z

    goto :goto_2

    .line 90
    .end local v8    # "noresults":I
    :sswitch_3
    new-instance v12, Lcom/policydm/eng/parser/XDMParserCred;

    invoke-direct {v12}, Lcom/policydm/eng/parser/XDMParserCred;-><init>()V

    iput-object v12, p0, Lcom/policydm/eng/parser/XDMParserSync;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    .line 91
    iget-object v12, p0, Lcom/policydm/eng/parser/XDMParserSync;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    invoke-virtual {v12, p1}, Lcom/policydm/eng/parser/XDMParserCred;->xdmParParseCred(Lcom/policydm/eng/parser/XDMParser;)I

    move-result v10

    .line 92
    iget-object v12, p1, Lcom/policydm/eng/parser/XDMParser;->Cred:Lcom/policydm/eng/parser/XDMParserCred;

    iput-object v12, p0, Lcom/policydm/eng/parser/XDMParserSync;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    goto :goto_2

    .line 96
    :sswitch_4
    invoke-virtual {p1, v6}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseElement(I)I

    move-result v10

    .line 97
    iget-object v12, p1, Lcom/policydm/eng/parser/XDMParser;->m_szTarget:Ljava/lang/String;

    iput-object v12, p0, Lcom/policydm/eng/parser/XDMParserSync;->m_szTarget:Ljava/lang/String;

    goto :goto_2

    .line 101
    :sswitch_5
    invoke-virtual {p1, v6}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseElement(I)I

    move-result v10

    .line 102
    iget-object v12, p1, Lcom/policydm/eng/parser/XDMParser;->m_szSource:Ljava/lang/String;

    iput-object v12, p0, Lcom/policydm/eng/parser/XDMParserSync;->m_szSource:Ljava/lang/String;

    goto :goto_2

    .line 106
    :sswitch_6
    new-instance v12, Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-direct {v12}, Lcom/policydm/eng/parser/XDMParserMeta;-><init>()V

    iput-object v12, p0, Lcom/policydm/eng/parser/XDMParserSync;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    .line 107
    iget-object v12, p0, Lcom/policydm/eng/parser/XDMParserSync;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-virtual {v12, p1}, Lcom/policydm/eng/parser/XDMParserMeta;->xdmParParseMeta(Lcom/policydm/eng/parser/XDMParser;)I

    move-result v10

    .line 108
    iget-object v12, p1, Lcom/policydm/eng/parser/XDMParser;->Meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iput-object v12, p0, Lcom/policydm/eng/parser/XDMParserSync;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    goto :goto_2

    .line 112
    :sswitch_7
    invoke-virtual {p1, v6}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseElement(I)I

    move-result v10

    .line 113
    iget-object v12, p1, Lcom/policydm/eng/parser/XDMParser;->m_szParserElement:Ljava/lang/String;

    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    iput v12, p0, Lcom/policydm/eng/parser/XDMParserSync;->numofchanges:I

    goto :goto_2

    .line 117
    :sswitch_8
    if-eqz v2, :cond_7

    .line 119
    const/4 v2, 0x0

    .line 121
    :cond_7
    new-instance v3, Lcom/policydm/eng/parser/XDMParserCopy;

    invoke-direct {v3}, Lcom/policydm/eng/parser/XDMParserCopy;-><init>()V

    .line 122
    .local v3, "copy":Lcom/policydm/eng/parser/XDMParserCopy;
    invoke-virtual {v3, p1}, Lcom/policydm/eng/parser/XDMParserCopy;->xdmParParseCopy(Lcom/policydm/eng/parser/XDMParser;)I

    move-result v10

    .line 123
    goto :goto_2

    .line 126
    .end local v3    # "copy":Lcom/policydm/eng/parser/XDMParserCopy;
    :sswitch_9
    if-eqz v2, :cond_8

    .line 128
    const/4 v2, 0x0

    .line 130
    :cond_8
    new-instance v11, Lcom/policydm/eng/parser/XDMParserSequence;

    invoke-direct {v11}, Lcom/policydm/eng/parser/XDMParserSequence;-><init>()V

    .line 131
    .local v11, "sequence":Lcom/policydm/eng/parser/XDMParserSequence;
    invoke-virtual {v11, p1}, Lcom/policydm/eng/parser/XDMParserSequence;->xdmParParseSequence(Lcom/policydm/eng/parser/XDMParser;)I

    move-result v10

    .line 132
    goto/16 :goto_2

    .line 135
    .end local v11    # "sequence":Lcom/policydm/eng/parser/XDMParserSequence;
    :sswitch_a
    if-eqz v2, :cond_9

    .line 137
    const/4 v2, 0x0

    .line 139
    :cond_9
    new-instance v1, Lcom/policydm/eng/parser/XDMParserAtomic;

    invoke-direct {v1}, Lcom/policydm/eng/parser/XDMParserAtomic;-><init>()V

    .line 140
    .local v1, "atomic":Lcom/policydm/eng/parser/XDMParserAtomic;
    invoke-virtual {v1, p1}, Lcom/policydm/eng/parser/XDMParserAtomic;->xdmParParseAtomic(Lcom/policydm/eng/parser/XDMParser;)I

    move-result v10

    .line 141
    goto/16 :goto_2

    .line 144
    .end local v1    # "atomic":Lcom/policydm/eng/parser/XDMParserAtomic;
    :sswitch_b
    const-string v12, "xdmParParseSync : WBXML_TAG_ADD"

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 145
    if-eqz v2, :cond_a

    .line 147
    const/4 v2, 0x0

    .line 149
    :cond_a
    new-instance v0, Lcom/policydm/eng/parser/XDMParserAdd;

    invoke-direct {v0}, Lcom/policydm/eng/parser/XDMParserAdd;-><init>()V

    .line 150
    .local v0, "add":Lcom/policydm/eng/parser/XDMParserAdd;
    invoke-virtual {v0, p1}, Lcom/policydm/eng/parser/XDMParserAdd;->xdmParParseAdd(Lcom/policydm/eng/parser/XDMParser;)I

    move-result v10

    .line 151
    goto/16 :goto_2

    .line 154
    .end local v0    # "add":Lcom/policydm/eng/parser/XDMParserAdd;
    :sswitch_c
    if-eqz v2, :cond_b

    .line 156
    const/4 v2, 0x0

    .line 158
    :cond_b
    new-instance v9, Lcom/policydm/eng/parser/XDMParserReplace;

    invoke-direct {v9}, Lcom/policydm/eng/parser/XDMParserReplace;-><init>()V

    .line 159
    .local v9, "replace":Lcom/policydm/eng/parser/XDMParserReplace;
    invoke-virtual {v9, p1}, Lcom/policydm/eng/parser/XDMParserReplace;->xdmParParseReplace(Lcom/policydm/eng/parser/XDMParser;)I

    move-result v10

    .line 160
    goto/16 :goto_2

    .line 163
    .end local v9    # "replace":Lcom/policydm/eng/parser/XDMParserReplace;
    :sswitch_d
    if-eqz v2, :cond_c

    .line 165
    const/4 v2, 0x0

    .line 167
    :cond_c
    new-instance v4, Lcom/policydm/eng/parser/XDMParserDelete;

    invoke-direct {v4}, Lcom/policydm/eng/parser/XDMParserDelete;-><init>()V

    .line 168
    .local v4, "delete":Lcom/policydm/eng/parser/XDMParserDelete;
    invoke-virtual {v4, p1}, Lcom/policydm/eng/parser/XDMParserDelete;->xdmParParseDelete(Lcom/policydm/eng/parser/XDMParser;)I

    move-result v10

    .line 169
    goto/16 :goto_2

    .line 172
    .end local v4    # "delete":Lcom/policydm/eng/parser/XDMParserDelete;
    :sswitch_e
    invoke-virtual {p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseReadElement()I

    move-result v6

    .line 173
    invoke-virtual {p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseReadElement()I

    move-result v6

    .line 175
    iput v6, p1, Lcom/policydm/eng/parser/XDMParser;->codePage:I

    goto/16 :goto_2

    .line 66
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_e
        0x5 -> :sswitch_b
        0x8 -> :sswitch_a
        0xb -> :sswitch_0
        0xd -> :sswitch_8
        0xe -> :sswitch_3
        0x10 -> :sswitch_d
        0x1a -> :sswitch_6
        0x1d -> :sswitch_1
        0x1e -> :sswitch_2
        0x20 -> :sswitch_c
        0x24 -> :sswitch_9
        0x27 -> :sswitch_5
        0x2e -> :sswitch_4
        0x33 -> :sswitch_7
    .end sparse-switch
.end method
