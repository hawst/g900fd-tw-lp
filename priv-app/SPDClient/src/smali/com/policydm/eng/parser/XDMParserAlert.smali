.class public Lcom/policydm/eng/parser/XDMParserAlert;
.super Lcom/policydm/agent/XDMHandleCmd;
.source "XDMParserAlert.java"

# interfaces
.implements Lcom/policydm/eng/core/XDMWbxml;


# instance fields
.field public cmdid:I

.field public cred:Lcom/policydm/eng/parser/XDMParserCred;

.field public is_noresp:I

.field public itemlist:Lcom/policydm/eng/core/XDMList;

.field public m_szCorrelator:Ljava/lang/String;

.field public m_szData:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Lcom/policydm/agent/XDMHandleCmd;-><init>()V

    .line 13
    const/4 v0, 0x0

    iput v0, p0, Lcom/policydm/eng/parser/XDMParserAlert;->is_noresp:I

    .line 14
    iput-object v1, p0, Lcom/policydm/eng/parser/XDMParserAlert;->m_szCorrelator:Ljava/lang/String;

    .line 17
    iput-object v1, p0, Lcom/policydm/eng/parser/XDMParserAlert;->itemlist:Lcom/policydm/eng/core/XDMList;

    .line 10
    return-void
.end method


# virtual methods
.method public xdmParParseAlert(Lcom/policydm/eng/parser/XDMParser;)I
    .locals 5
    .param p1, "p"    # Lcom/policydm/eng/parser/XDMParser;

    .prologue
    const/4 v3, 0x0

    .line 26
    const/4 v1, -0x1

    .line 27
    .local v1, "id":I
    const/4 v2, 0x0

    .line 29
    .local v2, "res":I
    const/4 v4, 0x6

    invoke-virtual {p1, v4}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseCheckElement(I)I

    move-result v2

    .line 30
    if-eqz v2, :cond_1

    move v3, v2

    .line 108
    :cond_0
    :goto_0
    return v3

    .line 35
    :cond_1
    invoke-virtual {p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseZeroBitTagCheck()I

    move-result v2

    .line 36
    const/16 v4, 0x8

    if-eq v2, v4, :cond_0

    .line 40
    if-eqz v2, :cond_2

    .line 42
    const-string v3, "not WBXML_ERR_OK"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move v3, v2

    .line 43
    goto :goto_0

    .line 50
    :cond_2
    :try_start_0
    invoke-virtual {p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseCurrentElement()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 57
    :goto_1
    const/4 v4, 0x1

    if-ne v1, v4, :cond_3

    .line 59
    invoke-virtual {p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseReadElement()I

    move-result v1

    .line 106
    iget-object v4, p1, Lcom/policydm/eng/parser/XDMParser;->userdata:Ljava/lang/Object;

    invoke-virtual {p0, v4, p0}, Lcom/policydm/eng/parser/XDMParserAlert;->xdmAgentHdlCmdAlert(Ljava/lang/Object;Lcom/policydm/eng/parser/XDMParserAlert;)V

    goto :goto_0

    .line 52
    :catch_0
    move-exception v0

    .line 54
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 63
    .end local v0    # "e":Ljava/io/IOException;
    :cond_3
    sparse-switch v1, :sswitch_data_0

    .line 97
    const/4 v2, 0x2

    .line 101
    :goto_2
    if-eqz v2, :cond_2

    move v3, v2

    .line 103
    goto :goto_0

    .line 66
    :sswitch_0
    invoke-virtual {p1, v1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseElement(I)I

    move-result v2

    .line 67
    iget-object v4, p1, Lcom/policydm/eng/parser/XDMParser;->m_szParserElement:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/policydm/eng/parser/XDMParserAlert;->cmdid:I

    goto :goto_2

    .line 71
    :sswitch_1
    new-instance v4, Lcom/policydm/eng/parser/XDMParserCred;

    invoke-direct {v4}, Lcom/policydm/eng/parser/XDMParserCred;-><init>()V

    iput-object v4, p0, Lcom/policydm/eng/parser/XDMParserAlert;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    .line 72
    iget-object v4, p0, Lcom/policydm/eng/parser/XDMParserAlert;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    invoke-virtual {v4, p1}, Lcom/policydm/eng/parser/XDMParserCred;->xdmParParseCred(Lcom/policydm/eng/parser/XDMParser;)I

    move-result v2

    .line 73
    iget-object v4, p1, Lcom/policydm/eng/parser/XDMParser;->Cred:Lcom/policydm/eng/parser/XDMParserCred;

    iput-object v4, p0, Lcom/policydm/eng/parser/XDMParserAlert;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    goto :goto_2

    .line 77
    :sswitch_2
    invoke-virtual {p1, v1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseElement(I)I

    move-result v2

    .line 78
    iget-object v4, p1, Lcom/policydm/eng/parser/XDMParser;->m_szParserElement:Ljava/lang/String;

    iput-object v4, p0, Lcom/policydm/eng/parser/XDMParserAlert;->m_szData:Ljava/lang/String;

    goto :goto_2

    .line 82
    :sswitch_3
    iget-object v4, p0, Lcom/policydm/eng/parser/XDMParserAlert;->itemlist:Lcom/policydm/eng/core/XDMList;

    invoke-virtual {p1, v4}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseItemlist(Lcom/policydm/eng/core/XDMList;)Lcom/policydm/eng/core/XDMList;

    move-result-object v4

    iput-object v4, p0, Lcom/policydm/eng/parser/XDMParserAlert;->itemlist:Lcom/policydm/eng/core/XDMList;

    goto :goto_2

    .line 86
    :sswitch_4
    invoke-virtual {p1, v1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseBlankElement(I)I

    move-result v4

    iput v4, p0, Lcom/policydm/eng/parser/XDMParserAlert;->is_noresp:I

    goto :goto_2

    .line 90
    :sswitch_5
    invoke-virtual {p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseReadElement()I

    move-result v1

    .line 91
    invoke-virtual {p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseReadElement()I

    move-result v1

    .line 93
    iput v1, p1, Lcom/policydm/eng/parser/XDMParser;->codePage:I

    goto :goto_2

    .line 63
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_5
        0xb -> :sswitch_0
        0xe -> :sswitch_1
        0xf -> :sswitch_2
        0x14 -> :sswitch_3
        0x1d -> :sswitch_4
    .end sparse-switch
.end method
